<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ClockAttendanceDetails.aspx.vb" Inherits="ClockAttendanceDetails" %>

<%@ Register TagPrefix="ew" Assembly="eWorld.UI" Namespace="eWorld.UI" %>
<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Attendance Details</title>
    <link href="../css/localhost.css" rel="stylesheet" type="text/css" />
    <base target="_self" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <style type="text/css">
        .AttTblHeader {
            font: bold 11px verdana;
            color: #000066;
            background-color: #E9EDF2;
            padding: 6px 6px 2px 6px;
            border: 1px solid #ebebeb;
            vertical-align: top;
            text-align: center;
        }
    </style>
    <script language="javascript" type="text/javascript">
        var setvalOnFly1 = setInterval(function () {
            var dayOfWeekVal = localStorage.getItem("dayOfWeekVal") ? localStorage.getItem("dayOfWeekVal").toString() : null;
            if (dayOfWeekVal != null) {
                var ActualVal = parseFloat(localStorage.getItem("ValAct"));
                var ValAbs = parseFloat(localStorage.getItem("ValAbs"));
                var ValMakeup = parseFloat(localStorage.getItem("ValMakeup"));
                var ValScheduled = parseFloat(localStorage.getItem("ValScheduled"));
                var ValTardy = localStorage.getItem("ValTardy");
                if (ActualVal > 0) {
                    document.getElementById("txtAHD" + dayOfWeekVal).value = ActualVal.toFixed(2);
                } else {
                    document.getElementById("txtAHD" + dayOfWeekVal).value = "";
                }

                if (ValAbs > 0) {
                    document.getElementById("txtABHD" + dayOfWeekVal).value = ValAbs.toFixed(2);
                } else {
                    document.getElementById("txtABHD" + dayOfWeekVal).value = "";
                }

                if (ValMakeup > 0) {
                    document.getElementById("txtMUHD" + dayOfWeekVal).value = ValMakeup.toFixed(2);
                } else {
                    document.getElementById("txtMUHD" + dayOfWeekVal).value = "";
                }

                if (ValTardy == "True") {
                    document.getElementById("chkT" + dayOfWeekVal).checked = true;
                } else {
                    document.getElementById("chkT" + dayOfWeekVal).checked = false;
                }

                document.getElementById("txtSHD" + dayOfWeekVal).value = ValScheduled.toFixed(2);
            }
            //clearInterval(setvalOnFly1);
        },
            2000);

        function modifyAB(schedule, actual, absent, makeup) {
            var schedulevalue = document.getElementById(schedule).value;
            var actualvalue = document.getElementById(actual).value;
            if (schedulevalue.length == 0) { alert("please select schedule value"); return (false); }
            if (schedulevalue == 1 && actualvalue == 1) {
                document.getElementById(absent).value = 0;
                document.getElementById(makeup).value = "";
            }
            else if (schedulevalue == 1 && actualvalue == 0) {
                document.getElementById(absent).value = 1;
                document.getElementById(makeup).value = "";
            }
            else if (schedulevalue == 0 && actualvalue == 1) {
                document.getElementById(absent).value = "";
                document.getElementById(makeup).value = 1;
            }
            else if (schedulevalue == 0 && actualvalue == 0) {
                document.getElementById(absent).value = "";
                document.getElementById(makeup).value = "";
            }
        }
        function TrackTardies(tardy, schedule, actual, absent, makeup) {
            var checkvalue = document.getElementById(tardy).checked;
            var schedulevalue = document.getElementById(schedule).value;
            if (schedulevalue.length == 0) { alert("please select schedule value"); return (false); }
            if (checkvalue == true) {
                document.getElementById(actual).value = 1;
                document.getElementById(absent).value = 0;
                if (schedulevalue == 0) {
                    document.getElementById(makeup).value = 1;
                }
                else {
                    document.getElementById(makeup).value = "";
                }
            }
        }
    </script>
</head>
<body style="background-color: White">
    <form id="form1" runat="server">
        <div>
            <br />
            <table>
                <tr>
                    <td class="twocolumnlabelcell" width="80px" align="right">
                        <asp:Label ID="lblStudent" runat="server" CssClass="Label" Text="Student" /></td>
                    <td class="twocolumncontentcell" align="left" width="130px">
                        <asp:TextBox ID="txtStudent" runat="server" Width="125px" CssClass="TextBox" ReadOnly="True" /></td>
                </tr>
            </table>

            <br />
            <div style="width: 100%; text-align: center">
                <table width="90%">
                    <tr>
                        <td style="width: 125px"></td>
                        <td class="AttTblHeader" width="70px">
                            <asp:Label ID="lblD0" runat="server" /></td>
                        <td class="AttTblHeader" width="70px">
                            <asp:Label ID="lblD1" runat="server" /></td>
                        <td class="AttTblHeader" width="70px">
                            <asp:Label ID="lblD2" runat="server" /></td>
                        <td class="AttTblHeader" width="70px">
                            <asp:Label ID="lblD3" runat="server" /></td>
                        <td class="AttTblHeader" width="70px">
                            <asp:Label ID="lblD4" runat="server" /></td>
                        <td class="AttTblHeader" width="70px">
                            <asp:Label ID="lblD5" runat="server" /></td>
                        <td class="AttTblHeader" width="70px">
                            <asp:Label ID="lblD6" runat="server" /></td>
                    </tr>
                    <tr>
                        <td width="135px" align="right">
                            <span class="Label">Scheduled</span></td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtSHD0" DecimalPlaces="2" TextAlign="Left" PositiveNumber="True" CssClass="TextBox" Width="80%" />
                            <asp:DropDownList ID="ddlSHD0" runat="server" CssClass="DropDownLists" Visible="false" Width="100%">
                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSDH0Changed" runat="Server" Visible="false"></asp:TextBox>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtSHD1" DecimalPlaces="2" TextAlign="Left" PositiveNumber="True" CssClass="TextBox" Width="80%" />
                            <asp:DropDownList ID="ddlSHD1" runat="server" CssClass="DropDownLists" Visible="false" Width="100%">
                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSDH1Changed" runat="Server" Visible="false"></asp:TextBox>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtSHD2" DecimalPlaces="2" TextAlign="Left" PositiveNumber="True" CssClass="TextBox" Width="80%" />
                            <asp:DropDownList ID="ddlSHD2" runat="server" CssClass="DropDownLists" Visible="false">
                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSDH2Changed" runat="Server" Visible="false"></asp:TextBox>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtSHD3" DecimalPlaces="2" TextAlign="Left" PositiveNumber="True" CssClass="TextBox" Width="80%" />
                            <asp:DropDownList ID="ddlSHD3" runat="server" CssClass="DropDownLists" Visible="false">
                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSDH3Changed" runat="Server" Visible="false"></asp:TextBox>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtSHD4" DecimalPlaces="2" TextAlign="Left" PositiveNumber="True" CssClass="TextBox" Width="80%" />
                            <asp:DropDownList ID="ddlSHD4" runat="server" CssClass="DropDownLists" Visible="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSDH4Changed" runat="Server" Visible="false"></asp:TextBox>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtSHD5" DecimalPlaces="2" TextAlign="Left" PositiveNumber="True" CssClass="TextBox" Width="80%" />
                            <asp:DropDownList ID="ddlSHD5" runat="server" CssClass="DropDownLists" Visible="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSDH5Changed" runat="Server" Visible="false"></asp:TextBox>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtSHD6" DecimalPlaces="2" TextAlign="Left" PositiveNumber="True" CssClass="TextBox" Width="80%" />
                            <asp:DropDownList ID="ddlSHD6" runat="server" CssClass="DropDownLists" Visible="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSDH6Changed" runat="Server" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="125px" align="right">
                            <span class="Label">Actual</span></td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtAHD0" TextAlign="Left" PositiveNumber="True" CssClass="TextBox" Width="80%" />
                            <asp:DropDownList ID="ddlAHD0" runat="server" CssClass="DropDownLists" Visible="false" language="javascript" onChange="modifyAB('ddlSHD0','ddlAHD0','ddlABHD0','ddlMUHD0');">
                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtAHD1" TextAlign="Left" PositiveNumber="True" CssClass="TextBox" Width="80%" />
                            <asp:DropDownList ID="ddlAHD1" runat="server" CssClass="DropDownLists" Visible="false" language="javascript" onChange="modifyAB('ddlSHD1','ddlAHD1','ddlABHD1','ddlMUHD1');">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtAHD2" TextAlign="Left" PositiveNumber="True" CssClass="TextBox" Width="80%" />
                            <asp:DropDownList ID="ddlAHD2" runat="server" CssClass="DropDownLists" Visible="false" language="javascript" onChange="modifyAB('ddlSHD2','ddlAHD2','ddlABHD2','ddlMUHD2');">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtAHD3" TextAlign="Left" PositiveNumber="True" CssClass="TextBox" Width="80%" />
                            <asp:DropDownList ID="ddlAHD3" runat="server" CssClass="DropDownLists" Visible="false" language="javascript" onChange="modifyAB('ddlSHD3','ddlAHD3','ddlABHD3','ddlMUHD3');">
                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtAHD4" TextAlign="Left" PositiveNumber="True" CssClass="TextBox" Width="80%" />
                            <asp:DropDownList ID="ddlAHD4" runat="server" CssClass="DropDownLists" Visible="false" language="javascript" onChange="modifyAB('ddlSHD4','ddlAHD4','ddlABHD4','ddlMUHD4');">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtAHD5" TextAlign="Left" PositiveNumber="True" CssClass="TextBox" Width="80%" />
                            <asp:DropDownList ID="ddlAHD5" runat="server" CssClass="DropDownLists" Visible="false" language="javascript" onChange="modifyAB('ddlSHD5','ddlAHD5','ddlABHD5','ddlMUHD5');">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtAHD6" TextAlign="Left" PositiveNumber="True" CssClass="TextBox" Width="80%" />
                            <asp:DropDownList ID="ddlAHD6" runat="server" CssClass="DropDownLists" Visible="false" language="javascript" onChange="modifyAB('ddlSHD6','ddlAHD6','ddlABHD6','ddlMUHD6');">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="125px" align="right">
                            <span class="Label">Absent</span></td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtABHD0" CssClass="TextBox" ReadOnly="True" PositiveNumber="True" Width="80%" />
                            <asp:DropDownList ID="ddlABHD0" runat="server" CssClass="DropDownLists" Visible="false" Enabled="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtABHD1" CssClass="TextBox" ReadOnly="True" PositiveNumber="True" Width="80%" />
                            <asp:DropDownList ID="ddlABHD1" runat="server" CssClass="DropDownLists" Visible="false" Enabled="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtABHD2" CssClass="TextBox" ReadOnly="True" PositiveNumber="True" Width="80%" />
                            <asp:DropDownList ID="ddlABHD2" runat="server" CssClass="DropDownLists" Visible="false" Enabled="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtABHD3" CssClass="TextBox" ReadOnly="True" PositiveNumber="True" Width="80%" />
                            <asp:DropDownList ID="ddlABHD3" runat="server" CssClass="DropDownLists" Visible="false" Enabled="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtABHD4" CssClass="TextBox" ReadOnly="True" PositiveNumber="True" Width="80%" />
                            <asp:DropDownList ID="ddlABHD4" runat="server" CssClass="DropDownLists" Visible="false" Enabled="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtABHD5" CssClass="TextBox" ReadOnly="True" PositiveNumber="True" Width="80%" />
                            <asp:DropDownList ID="ddlABHD5" runat="server" CssClass="DropDownLists" Visible="false" Enabled="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtABHD6" CssClass="TextBox" ReadOnly="True" PositiveNumber="True" Width="80%" />
                            <asp:DropDownList ID="ddlABHD6" runat="server" CssClass="DropDownLists" Visible="false" Enabled="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="125px" align="right">
                            <span class="Label">Makeup</span></td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtMUHD0" CssClass="TextBox" ReadOnly="True" PositiveNumber="True" Width="80%" />
                            <asp:DropDownList ID="ddlMUHD0" runat="server" CssClass="DropDownLists" Visible="false" Enabled="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtMUHD1" CssClass="TextBox" ReadOnly="True" PositiveNumber="True" Width="80%" />
                            <asp:DropDownList ID="ddlMUHD1" runat="server" CssClass="DropDownLists" Visible="false" Enabled="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtMUHD2" CssClass="TextBox" ReadOnly="True" PositiveNumber="True" Width="80%" />
                            <asp:DropDownList ID="ddlMUHD2" runat="server" CssClass="DropDownLists" Visible="false" Enabled="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtMUHD3" CssClass="TextBox" ReadOnly="True" PositiveNumber="True" Width="80%" />
                            <asp:DropDownList ID="ddlMUHD3" runat="server" CssClass="DropDownLists" Visible="false" Enabled="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtMUHD4" CssClass="TextBox" ReadOnly="True" PositiveNumber="True" Width="80%" />
                            <asp:DropDownList ID="ddlMUHD4" runat="server" CssClass="DropDownLists" Visible="false" Enabled="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtMUHD5" CssClass="TextBox" ReadOnly="True" PositiveNumber="True" Width="80%" />
                            <asp:DropDownList ID="ddlMUHD5" runat="server" CssClass="DropDownLists" Visible="false" Enabled="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="70px">
                            <ew:NumericBox runat="server" ID="txtMUHD6" CssClass="TextBox" ReadOnly="True" PositiveNumber="True" Width="80%" />
                            <asp:DropDownList ID="ddlMUHD6" runat="server" CssClass="DropDownLists" Visible="false" Enabled="false">

                                <asp:ListItem Value="1">yes</asp:ListItem>
                                <asp:ListItem Value="0">no</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="125px" align="right">
                            <span class="Label">
                                <asp:Label ID="lblTardy" runat="server" Text="Tardy" Visible="false"></asp:Label></span></td>
                        <td align="center" width="70px">
                            <asp:CheckBox ID="chkT0" runat="Server" CssClass="CheckBox" Visible="false" language="javascript" onClick="TrackTardies('chkT0','ddlSHD0','ddlAHD0','ddlABHD0','ddlMUHD0')" />
                            <asp:TextBox ID="txtT0" runat="server" CssClass="TextBox" Visible="false" Width="80%"></asp:TextBox>
                        </td>
                        <td align="center" width="70px">
                            <asp:CheckBox ID="chkT1" runat="Server" CssClass="CheckBox" Visible="false" language="javascript" onClick="TrackTardies('chkT1','ddlSHD1','ddlAHD1','ddlABHD1','ddlMUHD1')" />
                            <asp:TextBox ID="txtT1" runat="server" CssClass="TextBox" Visible="false" Width="80%"></asp:TextBox>
                        </td>
                        <td align="center" width="70px">
                            <asp:CheckBox ID="chkT2" runat="Server" CssClass="CheckBox" Visible="false" language="javascript" onClick="TrackTardies('chkT2','ddlSHD2','ddlAHD2','ddlABHD2','ddlMUHD2')" />
                            <asp:TextBox ID="txtT2" runat="server" CssClass="TextBox" Visible="false" Width="80%"></asp:TextBox>
                        </td>
                        <td align="center" width="70px">
                            <asp:CheckBox ID="chkT3" runat="Server" CssClass="CheckBox" Visible="false" language="javascript" onClick="TrackTardies('chkT3','ddlSHD3','ddlAHD3','ddlABHD3','ddlMUHD3')" />
                            <asp:TextBox ID="txtT3" runat="server" CssClass="TextBox" Visible="false" Width="80%"></asp:TextBox>
                        </td>
                        <td align="center" width="70px">
                            <asp:CheckBox ID="chkT4" runat="Server" CssClass="CheckBox" Visible="false" language="javascript" onClick="TrackTardies('chkT4','ddlSHD4','ddlAHD4','ddlABHD4','ddlMUHD4')" />
                            <asp:TextBox ID="txtT4" runat="server" CssClass="TextBox" Visible="false" Width="80%"></asp:TextBox>
                        </td>
                        <td align="center" width="70px">
                            <asp:CheckBox ID="chkT5" runat="Server" CssClass="CheckBox" Visible="false" language="javascript" onClick="TrackTardies('chkT5','ddlSHD5','ddlAHD5','ddlABHD5','ddlMUHD5')" />
                            <asp:TextBox ID="txtT5" runat="server" CssClass="TextBox" Visible="false" Width="80%"></asp:TextBox>
                        </td>
                        <td align="center" width="70px">
                            <asp:CheckBox ID="chkT6" runat="Server" CssClass="CheckBox" Visible="false" language="javascript" onClick="TrackTardies('chkT6','ddlSHD6','ddlAHD6','ddlABHD6','ddlMUHD6')" />
                            <asp:TextBox ID="txtT6" runat="server" CssClass="TextBox" Visible="false" Width="80%"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <p></p>
            <div style="width: 100%; text-align: center">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
            <asp:Button ID="btnOk" runat="server" Text="Ok" Width="80px" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="80px" />
                <asp:Button ID="btntimeclock" runat="server" Text="Add/Edit Punches" Width="126px" Visible="False" />&nbsp;
                                                      
            </div>
            <asp:TextBox ID="txtStudentStartDate" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtScheduleChange" runat="Server" CssClass="textbox" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtD0" runat="server" Visible="false">
            </asp:TextBox>
            <asp:TextBox ID="txtD00" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtD1" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtD2" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtD3" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtD4" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtD5" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtD6" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtStuEnrollId" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtSource" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtHol0" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtHol1" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtHol2" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtHol3" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtHol4" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtHol5" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtHol6" runat="server" Visible="false"></asp:TextBox>&nbsp;
        <input runat="server" id="hdnfromtimeclockpunch" type="hidden" />
        </div>
    </form>
    <script type="text/javascript">
        // Variables for MasterPageUserOptionsPanelBar
        var XMASTER_GET_BASE_URL = "<%= Page.ResolveUrl("~")%>";
        var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL = '<%= Page.ResolveUrl("~")%>';
        var XMASTER_PAGE_USER_OPTIONS_USERNAME = "<%=Session("UserName") %>";
        var XMASTER_PAGE_USER_OPTIONS_USERID = '<%=Session("UserId")%>';
        var XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID = '';
    </script>

    <%: Scripts.Render("~/bundles/popupscripts") %>

    <script type="text/javascript">
        $(document).ready(function () {
            var tokenRefresher = new MasterPage.TokenRefresher();
            tokenRefresher.Begin();
        });
    </script>
</body>
</html>
