﻿
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Data
Imports AdvWeb.usercontrols
Imports BO = Advantage.Business.Objects
Imports BL = Advantage.Business.Logic.Layer
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Reflection
Imports System.Diagnostics
Imports FAME.Advantage.Common
Imports Advantage.Business.Logic.Layer



Partial Public Class NewSiteSearchTest
    Inherits MasterPage
#Region "Class Member Variables"
    Private mruProvider As MRURoutines

    Protected state As AdvantageSessionState

    Public StatusBarState As Boolean

    Protected ChildResourceURL As String
    Protected strStudentObjectPointer As String = ""
    Protected strLeadObjectPointer As String = ""
    Protected strEmployerObjectPointer As String = ""
    Protected strEmployeeObjectPointer As String = ""

    Protected intMRUType As Integer = 0


#End Region

#Region "Class Properties"
    ''' <summary>
    ''' Return Current Campus ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>Read only, this property is updated in the Init Event of Control.</remarks>
    Public ReadOnly Property CurrentCampusId() As String
        Get
            Dim currentcamp As String = Me.MasterPageCampusDD.CurrentCampusId
            'jagg: this session is only used by AdvAppSettings, because we dont have access to request or master from there.
            Session("CurrentCampus") = currentcamp
            Return currentcamp
        End Get
    End Property

    ''' <summary>
    ''' Return Previous Campus ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>Read only, this property is updated in the Init Event of Control.</remarks>
    Public ReadOnly Property PreviusCampusId() As String
        Get
            Return Me.MasterPageCampusDD.PreviusCampusId
        End Get
    End Property

    ''' <summary>
    ''' ISSwitchedCampus property
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property IsSwitchedCampus() As Boolean
        Get
            Return Me.MasterPageCampusDD.PreviusCampusId <> Me.MasterPageCampusDD.CurrentCampusId
        End Get
    End Property




#End Region

#Region "Class Events"

    ''' <summary>
    ''' page init event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        mruProvider = New BL.MRURoutines(Me.Context, GetAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)

        Dim currentUrl As String = HttpContext.Current.Request.Url.ToString
        Session("domainName") = currentUrl.Substring(0, currentUrl.LastIndexOf("/", System.StringComparison.Ordinal))

        ' some of the busines layer and db access classes are using these session variables so we put them back in 
        Session("UserId") = AdvantageSession.UserState.UserId.ToString

        IdentityMruType()

        ResetObjectPointers()

        BuildMRUListByUserForPoweruser()

        Session("SwitchEntity") = False

        hdnKendoPane.Value = kendoMenuPane.ClientID

        If Not AdvantageSession.UserState.UserId.IsEmpty() Then
            hdnUserId.Value = Session("UserId").ToString()
        End If

    End Sub

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            MasterPageUserOptionsPanelBar.CurrentCampusId = CurrentCampusId
            ' jguirado Get special values used in the page master....
            AdvantageSession.UserId = AdvantageSession.UserState.UserId.ToString
            AdvantageSession.BuildVersion = GetBuildVersion()
            MasterPageUserOptionsPanelBar.CurrentBuildVersion = AdvantageSession.BuildVersion

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Get last entities accessed and add them to the AdvantageSession
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                AdvantageSession.MasterStudentId = getLastStudentEntityFromStateObject()
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                AdvantageSession.MasterLeadId = getLastLeadEntityFromStateObject()
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterEmployerId) Then
                AdvantageSession.MasterEmployerId = getLastEmployerEntityFromStateObject()
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterEmployeeId) Then
                AdvantageSession.MasterEmployeeId = getLastEmployeeEntityFromStateObject()
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        End If



        BuildBreadCrumb()

        ShowHideStatusBar()
        'SchoolLogo.PostBackUrl = "dash.aspx?RESID=264&mod=SY&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=dashboard"
        homeLinkImage.Src = "images/AdvantageLogo.png"
        SetHiddenControlForFerpa()

        'Handle for event og masterPageControlDropDown: This event is fired when the user change the campus in the client.
        AddHandler Me.MasterPageCampusDD.MasterPageCampusDropDownHandler, AddressOf MasterPageDropDownChangeEvent
    End Sub

    Protected Sub AdvToolBar_ButtonClick(ByVal sender As Object, ByVal e As RadToolBarEventArgs)
        If e.Item.Index = 1 Then
            If e.Item.ToolTip = "Search Students" Then
                e.Item.ImageUrl = "images/gear1.png"
                e.Item.ToolTip = "Search Leads"
            Else
                e.Item.ImageUrl = "images/hr2.png"
                e.Item.ToolTip = "Search Students"
            End If
        End If
    End Sub

    'NOTE:  This is the new handler for the combobox, here must be concentrate all the activities related to Campus change.
    Private Sub MasterPageDropDownChangeEvent(sender As Object, e As MasterPageCampusDropDownEventArgs)

        CampusObjects.CampusSelectorchanged(Session, e.CurrentCampusId)
        Dim myAdvAppSettings As AdvAppSettings = HttpContext.Current.Session("AdvAppSettings")
        If intMRUType >= 1 And intMRUType <= 4 Then
            'Call NCO.AdvAppSettings.GetInstance()
            myAdvAppSettings.RetrieveSettings(AdvantageSession.UserState.CampusId.ToString)
            HttpContext.Current.Session("AdvAppSettings") = myAdvAppSettings
            GetLastEntityWhileSwitchingCampus("switchcampus")
        Else
            Call myAdvAppSettings.RetrieveSettings(AdvantageSession.UserState.CampusId.ToString)
            HttpContext.Current.Session("AdvAppSettings") = myAdvAppSettings
            'BuildNavigation(e.CurrentCampusId.ToString)
            'Me.MasterPageCampusDropDown.RandomSessionPersist = ram.Next '     hdRandomSessionId.Value = ram.Next()

            'Dim strNewurl = ReloadURL(Me.MasterPageCampusDropDown.RandomSessionPersist, Request.QueryString.ToString, Request.Url.AbsolutePath)
            Dim strNewurl = ReloadUrl(e.CurrentCampusId, Request.QueryString.ToString, Request.Url.AbsolutePath)

            Response.Redirect(strNewurl)
        End If

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Protected Sub btnChangeEntity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChangeEntity.Click
        AdvantageSession.MasterStudentId = hdnCurrentEntityId.Value
    End Sub

#End Region


#Region "Methods"

    Private Sub IdentityMruType()
        Select Case Request.QueryString("ResId").ToString
            ''Modified for DE8076
            Case "90", "92", "95", "97", "112", _
                               "114", "116", "155", "159", "169", _
                                   "175", "177", "200", "203", "213", _
                                   "230", "286", "301", "303", "327", _
                                   "372", "374", "380", "479", "530", _
                                   "531", "532", "534", "535", "538", _
                                   "539", "541", "542", "543", "614", _
                                   "625", "628", "652", "113", "334" ' Student Page , "308"
                intMRUType = 1
            Case "79", "82", "86", "111" 'Employer Tabs , "197"
                intMRUType = 2
            Case "52", "55", "69", "281" 'Employee Tabs , "309"
                intMRUType = 3
            Case "145", "146", "147", "148", "170", _
                   "225", "313", "456", "484", "793" 'Lead Tabs , "153"
                intMRUType = 4
        End Select
    End Sub
    Private Sub ResetObjectPointers()
        Session("EmployerObjectPointer") = ""
        Session("EmployeeObjectPointer") = ""
    End Sub

    Private Sub ShowHideStatusBar()
        IdentityMruType()
        If intMRUType >= 1 And intMRUType <= 4 Then
            StatusBarState = True
        Else
            StatusBarState = False
        End If

        Dim resId As Int32
        Dim hidStatusBar As Boolean = False
        resId = Int32.Parse(Request.QueryString("ResId").ToString)


        'DE8777 - added isadhoc check - adhoc report passes reportid as resourceid
        If Request.QueryString.ToString.Contains("&bar") Or Request.QueryString.ToString.Contains("&isadhoc") Or hidStatusBar = True Then
            StatusBarState = False
        End If

        If StatusBarState = True Then
            MyStatusBar.Visible = True
            TopPane.Height = 105
        Else
            MyStatusBar.Visible = False
            TopPane.Height = 30
        End If
    End Sub



    Private Sub BuildBreadCrumb()
        Dim breadcrumb As New StringBuilder
        breadcrumb.Append("<span style='margin-left:5px;vertical-align:top;'>")
        breadcrumb.Append(AdvantageSession.PageBreadCrumb)
        breadcrumb.Append("</span>")
        'Dim x As RadToolBarItem = AdvToolBar.FindItemByValue("ToolBarItem1")
        'Dim y As Control = x.FindControl("bc1")
        'CType(RadButtonBar1.FindItemByValue("ToolBarItem1").FindControl("bc1"), Literal).Text = Breadcrumb.ToString
        bc1.Text = breadcrumb.ToString
    End Sub

    Public Function ReloadUrl(ByVal currentCampusId As Guid, ByVal originalUrl As String, ByVal urlPath As String) As String
        Dim nameValues = HttpUtility.ParseQueryString(originalUrl)
        'nameValues.[Set]("VSI", strVSI)
        nameValues.[Set]("cmpId", currentCampusId.ToString())
        Dim url As String = urlPath
        Dim updatedQueryString As String = "?" + nameValues.ToString()
        Dim newUrl As String = url + updatedQueryString
        Return newUrl
    End Function

    Protected Sub GetLastEntityWhileSwitchingCampus(Optional ByVal source As String = "")
        Session("MenuLoaded") = False  'Need to rebuild menu while switching campus for the campus level config setting to take effect
        Try
            Select Case intMRUType
                Case 1
                    BuildStudentStateObject("") 'Passing a empty studentid will enforce the function to get the last student from the selected campus
                Case 2
                    BuildEmployerStateObject("", "", source)
                Case 3
                    BuildEmployeeStateObject("", "")
                Case 4
                    BuildLeadStateObject("", True)
            End Select
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Try
                Select Case intMRUType
                    Case 1
                        'Redirect to student search page
                        RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
                    Case 2
                        RedirectToEmployerSearchPage(AdvantageSession.UserState.CampusId.ToString)
                    Case 3
                        RedirectToEmployeeSearchPage(AdvantageSession.UserState.CampusId.ToString)
                    Case 4
                        RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
                    Case Else
                        Response.Redirect("~/dash.aspx?RESID=264&mod=SY&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=dashboard")
                End Select
            Catch ex1 As Exception
            	exTracker.TrackExceptionWrapper(ex1)

                Response.Redirect("~/dash.aspx?RESID=264&mod=SY&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=dashboard")
            End Try

        End Try
    End Sub


    Private Function getLastStudentEntityFromStateObject() As String

        Dim mruProv As MRURoutines
        Dim strStudentId As String = ""

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        mruProv = New MRURoutines(System.Web.HttpContext.Current, MyAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)

        Return mruProv.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, 1, Me.CurrentCampusId)
    End Function


    Private Function getLastLeadEntityFromStateObject() As String

        Dim mruProv As MRURoutines

        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        mruProv = New MRURoutines(System.Web.HttpContext.Current, MyAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)

        Return mruProv.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, 4, AdvantageSession.UserState.CampusId.ToString)

    End Function
    Private Function getLastEmployerEntityFromStateObject() As String

        Dim mruProv As MRURoutines


        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        mruProv = New MRURoutines(System.Web.HttpContext.Current, MyAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)

        Return mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, 2, AdvantageSession.UserState.CampusId.ToString)


    End Function
    Private Function getLastEmployeeEntityFromStateObject() As String

        Dim mruProv As MRURoutines


        Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        mruProv = New MRURoutines(System.Web.HttpContext.Current, MyAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)

        Return mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, 3, AdvantageSession.UserState.CampusId.ToString)

    End Function


#End Region


#Region "MRU"
    Private Sub BuildMRUListByUserAfterDelete(ByVal strType As String)

        Dim MRUToolBar As RadToolBar
        Dim strMRUType As String = ""
        Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
        MRUToolBar = CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar)

        strMRUType = strType

        Select Case strMRUType.ToString
            Case "1"
                BuildStudentMRU()
                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 1 'Students
            Case "2"
                BuildEmployerMRU()
                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 2 'Employer
            Case "3"
                BuildEmployeeMRU()
                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 3 'Employees
            Case "4"
                BuildLeadMRU()
                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 4 'Leads
        End Select
    End Sub
    Private Sub BuildMRUListByUserForPoweruser(Optional ByVal strType As String = "", Optional ByVal deleteAction As Boolean = False)
        If AdvantageSession.UserState.FullPermission = True Then
            'If user is sa then all MRUs will be visible
            Dim MRUToolBar As RadToolBar
            Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
            MRUToolBar = CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar)
            Dim strMRUType As String = ""
            Try
                If Request.QueryString.ToString.Contains("Type") And deleteAction = False Then
                    If Not (Session("Type") Is Nothing) Then


                        If (Not Session("Type").ToString.Trim = Request.QueryString("Type").ToString.Trim) Then 'When user switches from Employer to Student and tries to delete a student from MRU
                            strMRUType = Session("Type")
                        Else
                            strMRUType = Request.QueryString("Type").ToString
                        End If
                        Session("Type") = strMRUType
                        Select Case strMRUType.ToString
                            Case "1"
                                BuildStudentMRU()
                                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 1 'Students
                            Case "2"
                                BuildEmployerMRU()
                                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 2 'Employer
                            Case "3"
                                BuildEmployeeMRU()
                                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 3 'Employee
                            Case "4"
                                BuildLeadMRU()
                                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 0 'Leads
                        End Select
                    ElseIf Not Request.QueryString.ToString.Contains("Type") And deleteAction = False Then
                        strMRUType = Session("Type")
                        If strMRUType Is Nothing Then
                            ResolveNullTypeSituation(MRUToolBar)
                        Else

                            Select Case strMRUType.ToString
                                Case "1"
                                    BuildStudentMRU()
                                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 1 'Students
                                Case "2"
                                    BuildEmployerMRU()
                                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 2 'Employer
                                Case "3"
                                    BuildEmployeeMRU()
                                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 3 'Employee
                                Case "4"
                                    BuildLeadMRU()
                                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 0 'Leads
                            End Select
                        End If
                    ElseIf deleteAction = True Then
                        strMRUType = strType
                        Select Case strMRUType.ToString
                            Case "1"
                                BuildStudentMRU()
                                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 1 'Students
                            Case "2"
                                BuildEmployerMRU()
                                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 2 'Employer
                            Case "3"
                                BuildEmployeeMRU()
                                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 3 'Employee
                            Case "4"
                                BuildLeadMRU()
                                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 0 'Leads
                        End Select
                    Else
                        BuildLeadMRU()
                        CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 0 'Leads
                    End If
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'DE7568 Fix: New Session "Type" has been added to keep track of the last entity user chooses in MRU
                'The entity pages already have Type value embeded in the URL, it is the non-entity pages that don't have Type value
                'and this session variable will address non-entity pages
                'This Session value is being set in the FHToolBar_ButtonClick, BuildMRUListForRegularUsers and BuildMRUListByUser Routines
                If Session("Type") = "1" Then
                    BuildStudentMRU()
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 1 'Students
                ElseIf Session("Type") = "2" Then
                    BuildEmployerMRU()
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 2 'Employer
                ElseIf Session("Type") = "3" Then
                    BuildEmployeeMRU()
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 3 'Employee
                Else
                    BuildLeadMRU()
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 0 'Leads
                End If
            End Try

        Else
            BuildMRUListForRegularUsers(strType)
        End If
    End Sub

    Private Sub ResolveNullTypeSituation(mRUToolBar As RadToolBar)
        BuildLeadMRU()
        CType(mRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 0 'Leads
    End Sub

    Private Sub BuildMRUListByUser(Optional ByVal strType As String = "")
        If AdvantageSession.UserState.FullPermission = True Then
            'If user is sa then all MRUs will be visible
            Dim MRUToolBar As RadToolBar
            Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
            MRUToolBar = CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar)
            Dim strMRUType As String = ""
            Try
                strMRUType = Request.QueryString("Type").ToString
                Select Case strMRUType.ToString
                    Case "1"
                        BuildStudentMRU()
                    Case "2"
                        BuildEmployerMRU()
                    Case "3"
                        BuildEmployeeMRU()
                    Case "4"
                        BuildLeadMRU()
                End Select
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'DE7568 Fix: New Session "Type" has been added to keep track of the last entity user chooses in MRU
                'The entity pages already have Type value embeded in the URL, it is the non-entity pages that don't have Type value
                'and this session variable will address non-entity pages
                'This Session value is being set in the FHToolBar_ButtonClick, BuildMRUListForRegularUsers and BuildMRUListByUser Routines
                If Session("Type") = "1" Then
                    BuildStudentMRU()
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 1 'Students
                ElseIf Session("Type") = "2" Then
                    BuildEmployerMRU()
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 2 'Employer
                ElseIf Session("Type") = "3" Then
                    BuildEmployeeMRU()
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 3 'Employee
                Else
                    BuildLeadMRU()
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 0 'Leads
                End If
            End Try

            If Request.QueryString.ToString.Contains("&Type") Then
                Session("Type") = Request.QueryString("Type").ToString
                Select Case Request.QueryString("Type").ToString
                    Case "1"
                        CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 1 'Students
                        BuildStudentMRU()
                    Case "3"
                        CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 3 'Employee
                        BuildEmployeeMRU()
                    Case "2"
                        CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 2 'Employer
                        BuildEmployerMRU()
                    Case "4"
                        CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 0 'Leads
                        BuildLeadMRU()
                End Select
            Else
                'If Type is not part of the URL, during deletions
                Select Case strType
                    Case "1"
                        CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 1 'Students
                        BuildStudentMRU()
                    Case "3"
                        CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 3 'Employee
                        BuildEmployeeMRU()
                    Case "2"
                        CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 2 'Employer
                        BuildEmployerMRU()
                    Case "4"
                        CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 0 'Leads
                        BuildLeadMRU()
                End Select
            End If
        Else
            BuildMRUListForRegularUsers(strType)
        End If
    End Sub
    Private Sub BuildMRUListForRegularUsers(Optional ByVal strType As String = "") 'Restrict MRUs based on User roles

        'Trace.Warn("Begin non-sa mru build")
        ' Dim leadsMRU, studentsMRU, employersMRU As RadToolBarButton
        Dim MRUToolBar As RadToolBar
        Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
        MRUToolBar = CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar)

        Dim intMRUList As List(Of Integer)
        If Session("BuildMRUEntitiesList" & AdvantageSession.UserState.UserId.ToString) Is Nothing Then
            Session("BuildMRUEntitiesList" & AdvantageSession.UserState.UserId.ToString) = mruProvider.BuildMRUEntitiesListForNonSAUser(AdvantageSession.UserState)
            intMRUList = Session("BuildMRUEntitiesList" & AdvantageSession.UserState.UserId.ToString)
        Else
            intMRUList = Session("BuildMRUEntitiesList" & AdvantageSession.UserState.UserId.ToString)
        End If

        Dim intLastIndex As Integer = 0
        If intMRUList.Count = 0 Then
            DirectCast(MRUToolBar.Controls(0), Telerik.Web.UI.RadToolBarSplitButton).EnableDefaultButton = False
            DirectCast(MRUToolBar.Controls(0), Telerik.Web.UI.RadToolBarSplitButton).Enabled = False
            DirectCast(MRUToolBar.Controls(0), Telerik.Web.UI.RadToolBarSplitButton).ToolTip = "This user has no access to leads, students, employees or employers "
            MRUToolBar.ToolTip = "This user has no access to leads, students, employees or employers"
            Exit Sub
        Else
            DirectCast(MRUToolBar.Controls(0), Telerik.Web.UI.RadToolBarSplitButton).EnableDefaultButton = True
            DirectCast(MRUToolBar.Controls(0), Telerik.Web.UI.RadToolBarSplitButton).Enabled = True
            CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar).Visible = True
            intMRUList.Reverse() 'Need to reverse to show the right default MRU item
            intLastIndex = intMRUList.Count - 1
        End If

        CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar).FindItemByValue("Leads").Visible = False 'Leads MRU
        CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar).FindItemByValue("Students").Visible = False
        CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar).FindItemByValue("Employers").Visible = False
        CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar).FindItemByValue("Employees").Visible = False
        'CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar).FindItemByValue("Lenders").Visible = False
        For Each item As Integer In intMRUList
            If item = 1 Then 'Students
                CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar).FindItemByValue("Students").Visible = True
                If intMRUList.Item(intLastIndex) = 1 Then
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 1
                End If
                BuildStudentMRU()
            End If
            If item = 3 Then ' Employees
                CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar).FindItemByValue("Employees").Visible = True
                If intMRUList.Item(intLastIndex) = 3 Then
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 3
                End If
                BuildEmployeeMRU()
            End If
            If item = 2 Then 'Employers
                CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar).FindItemByValue("Employers").Visible = True
                If intMRUList.Item(intLastIndex) = 2 Then
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 2
                End If
                BuildEmployerMRU()
            End If
            If item = 4 Then 'Leads
                CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar).FindItemByValue("Leads").Visible = True
                If intMRUList.Item(intLastIndex) = 4 Then
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 0
                End If
                BuildLeadMRU()
            End If
            'If item = 5 Then 'Lenders
            '    CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar).FindItemByValue("Lenders").Visible = True
            'End If
        Next
        If Request.QueryString.ToString.Contains("&Type") Then
            If Session("Type") Is Nothing Or Session("Type") < 1 Then
                Session("Type") = Request.QueryString("Type").ToString
            End If

            Select Case Session("Type") 'Request.QueryString("Type").ToString
                Case "1"
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 1 'Students
                    BuildStudentMRU()
                Case "3"
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 3 'Employee
                    BuildEmployeeMRU()
                Case "2"
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 2 'Employer
                    BuildEmployerMRU()
                Case "4"
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 0 'Leads
                    BuildLeadMRU()
            End Select
        ElseIf Not Request.QueryString.ToString.Contains("&Type") Then
            'If Type is not part of the URL, during deletions
            If String.IsNullOrEmpty(strType) Then strType = Session("Type")
            Select Case strType
                Case "1"
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 1 'Students
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).Text = "Recent Students"
                    BuildStudentMRU()
                Case "3"
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 3 'Employee
                    BuildEmployeeMRU()
                Case "2"
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 2 'Employer
                    BuildEmployerMRU()
                Case "4"
                    CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 0 'Leads
                    BuildLeadMRU()
            End Select
        End If
    End Sub
    Public Sub BuildEmployerMRU()
        Dim lstEmployerMRU As List(Of BO.EmployerMRU) = mruProvider.BuildEmployerMRU(AdvantageSession.UserState)
        Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
        Dim grdMRU As RadGrid = DirectCast(DirectCast(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0), Telerik.Web.UI.RadPanelItem).Controls(3), Telerik.Web.UI.RadGrid)
        grdMRU.ToolTip = ""
        With grdMRU
            .DataSource = lstEmployerMRU
            .DataBind()
        End With
        'Change the Icons to match the MRU Dropdown icon
        Try
            'If there is only one item in the MRU hide delete button
            If grdMRU.Items.Count = 1 Then
                CType(grdMRU.Items(0).FindControl("Image1"), Image).ImageUrl = "~/images/placement.png"
                CType(grdMRU.Items(0).FindControl("imgDeleteMRU"), ImageButton).Visible = False
                Exit Try
            End If
            For i As Integer = 0 To 19 'The List will always have 20 or less than 20 employers
                CType(grdMRU.Items(i).FindControl("imgDeleteMRU"), ImageButton).Visible = True
                CType(grdMRU.Items(i).FindControl("Image1"), Image).ImageUrl = "~/images/placement.png"
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

    End Sub
    Public Sub BuildEmployeeMRU()
        Dim lstEmployeeMRU As List(Of BO.EmployeeMRU) = mruProvider.BuildEmployeeMRU(AdvantageSession.UserState)
        Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
        Dim grdMRU As RadGrid = DirectCast(DirectCast(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0), Telerik.Web.UI.RadPanelItem).Controls(3), Telerik.Web.UI.RadGrid)
        grdMRU.ToolTip = ""
        With grdMRU
            .DataSource = lstEmployeeMRU
            .DataBind()
        End With
        Try
            'If there is only one item in the MRU hide delete button
            If grdMRU.Items.Count = 1 Then
                CType(grdMRU.Items(0).FindControl("imgDeleteMRU"), ImageButton).Visible = False
                CType(grdMRU.Items(0).FindControl("Image1"), Image).ImageUrl = "~/images/hr4.png"
                Exit Try
            End If

            For i As Integer = 0 To 19 'The List will always have 20 or less than 20 employees
                CType(grdMRU.Items(i).FindControl("imgDeleteMRU"), ImageButton).Visible = True
                CType(grdMRU.Items(i).FindControl("Image1"), Image).ImageUrl = "~/images/hr4.png"
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
    Private Sub BuildStudentMRU()
        Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
        Dim grdMRU As RadGrid = DirectCast(DirectCast(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0), Telerik.Web.UI.RadPanelItem).Controls(3), Telerik.Web.UI.RadGrid)
        grdMRU.ToolTip = ""
        With grdMRU
            .DataSource = mruProvider.BuildStudentMRU(AdvantageSession.UserState) 'AdvantageSession.StudentMRU
            .DataBind()
        End With
        Try
            'If there is only one item in the MRU hide delete button
            If grdMRU.Items.Count = 1 Then
                CType(grdMRU.Items(0).FindControl("imgDeleteMRU"), ImageButton).Visible = False
                CType(grdMRU.Items(0).FindControl("Image1"), Image).ImageUrl = "~/images/academic.png"
                Exit Try
            End If
            For i As Integer = 0 To 19 'The List will always have 20 or less than 20 students
                CType(grdMRU.Items(i).FindControl("imgDeleteMRU"), ImageButton).Visible = True
                CType(grdMRU.Items(i).FindControl("Image1"), Image).ImageUrl = "~/images/academic.png"
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
    Public Sub BuildLeadMRU()
        Dim lstLeadMRU As List(Of BO.LeadMRU) = Nothing
        lstLeadMRU = mruProvider.BuildLeadMRU(AdvantageSession.UserState)

        Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
        Dim grdMRU As RadGrid = DirectCast(DirectCast(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0), Telerik.Web.UI.RadPanelItem).Controls(3), Telerik.Web.UI.RadGrid)

        grdMRU.ToolTip = ""
        With grdMRU
            .DataSource = lstLeadMRU 'AdvantageSession.LeadMRU
            .DataBind()
        End With
        Try
            'If there is only one item in the MRU hide delete button
            If grdMRU.Items.Count = 1 Then
                CType(grdMRU.Items(0).FindControl("Image1"), Image).ImageUrl = "~/images/leads2.png"
                CType(grdMRU.Items(0).FindControl("imgDeleteMRU"), ImageButton).Visible = False
                Exit Try
            End If
            For i As Integer = 0 To 19 'The List will always have 20 or less than 20 leads
                CType(grdMRU.Items(i).FindControl("imgDeleteMRU"), ImageButton).Visible = True
                If CType(grdMRU.Items(i).FindControl("Label2"), Label).Text.ToLower.ToString.Contains("enrolled") Then
                    CType(grdMRU.Items(i).FindControl("Label2"), Label).Text = "Status:"
                    CType(grdMRU.Items(i).FindControl("lnkStatus"), LinkButton).Visible = True
                Else
                    CType(grdMRU.Items(i).FindControl("Label2"), Label).Text = "Status:" & CType(grdMRU.Items(i).FindControl("Label2"), Label).Text
                End If
                CType(grdMRU.Items(i).FindControl("Image1"), Image).ImageUrl = "~/images/leads2.png"
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
    Protected Sub FHToolBar_ButtonClick(ByVal sender As Object, ByVal e As RadToolBarEventArgs)
        Dim MRUToolBar As RadToolBar
        Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
        MRUToolBar = CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar)
        'CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar).FindItemByValue("Students").Visible = True
        Session("SelectedEntity") = e.Item.Value.ToLower
        Select Case e.Item.Value.ToLower
            Case "leads"
                Session("Type") = 4
                BuildLeadMRU()
                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 0
            Case "students"
                Session("Type") = 1
                BuildStudentMRU()
                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 1
            Case "employers"
                Session("Type") = 2
                BuildEmployerMRU()
                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 2
            Case "employees"
                Session("Type") = 3
                BuildEmployeeMRU()
                CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 3
        End Select
        Session("SwitchEntity") = True
    End Sub
    Protected Sub NavigateToEntityPage(ByVal sender As Object, ByVal e As EventArgs)
        Dim objStateInfo As New AdvantageStateInfo
        Dim MRUType As Integer = CInt(CType(sender, LinkButton).CommandName)

        Session("LeadMRUFlag") = ""
        Session("StudentMRUFlag") = ""
        Session("SwitchEntity") = False
        Select Case MRUType
            Case 1 'Students
                Dim StudentId As String = CType(sender, LinkButton).CommandArgument.ToString
                Dim MRUId As String = CType(sender, LinkButton).ValidationGroup.Substring(1, 35)
                Dim CampusId As String = CType(sender, LinkButton).ValidationGroup.Substring(37, 36)
                Session("StudentMRUFlag") = "true"
                Session("Type") = "1"
                BuildStudentStateObject(StudentId, CampusId)
            Case 2 'Employers
                Dim EmployerId As String = CType(sender, LinkButton).CommandArgument.ToString
                Dim EmployerDescrip As String = CType(sender, LinkButton).Text.ToString
                Session("Type") = "2"
                BuildEmployerStateObject(EmployerId, EmployerDescrip)
            Case 3 'Employees
                Dim EmployeeId As String = CType(sender, LinkButton).CommandArgument.ToString
                Dim EmployeeDescrip As String = CType(sender, LinkButton).Text.ToString
                Session("Type") = "3"
                BuildEmployeeStateObject(EmployeeId, EmployeeDescrip)
            Case 4 'Leads
                Dim LeadId As String = CType(sender, LinkButton).CommandArgument.ToString
                Session("LeadMRUFlag") = "true"
                Session("Type") = "4"
                BuildLeadStateObject(LeadId)
        End Select

        'Set relevant properties on the state object
        objStateInfo.StudentId = CType(sender, LinkButton).CommandArgument.ToString
        objStateInfo.NameCaption = "Student : "
        objStateInfo.NameValue = CType(sender, LinkButton).CommandName.ToString
    End Sub
    Protected Sub NavigateToStudentInfoPage(ByVal sender As Object, ByVal e As EventArgs)
        Dim objStateInfo As New AdvantageStateInfo
        Dim LeadId As String = CType(sender, LinkButton).ValidationGroup.ToString

        Dim StudentId_CampusId As String = mruProvider.getStuEnrollmentDetails(LeadId)
        Dim StudentId As String = StudentId_CampusId.Substring(0, 36)
        Dim CampusId As String = StudentId_CampusId.Substring(37, 36)

        BuildStudentStateObject(StudentId, CampusId)
    End Sub

    Public Sub BuildEmployeeStateObject(ByVal employeeId As String, ByVal employeeName As String)
        Dim objStateInfo As New AdvantageStateInfo
        Dim objGetStudentStatusBar As New AdvantageStateInfo
        Dim strVid As String
        'Dim facInputMasks As New InputMasksFacade
        Dim strEmployeeId As String = ""

        Session("EmployeeObjectPointer") = ""
        Session("EmployeeMRUFlag") = "false"


        If employeeId.ToString.Trim = "" Then 'No Student was selected from MRU
            strEmployeeId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, 3, AdvantageSession.UserState.CampusId.ToString)
        Else
            strEmployeeId = employeeId
        End If

        If Not strEmployeeId = "" Then
            objGetStudentStatusBar = mruProvider.BuildEmployeeStatusBar(strEmployeeId)
            With objStateInfo
                AdvantageSession.MasterEmployeeId = strEmployeeId
                .EmployeeId = objGetStudentStatusBar.EmployeeId
                .NameValue = objGetStudentStatusBar.NameValue
                .Address1 = objGetStudentStatusBar.Address1
                .Address2 = objGetStudentStatusBar.Address2
                .City = objGetStudentStatusBar.City
                .State = objGetStudentStatusBar.State
                .Zip = objGetStudentStatusBar.Zip
                .CampusId = objGetStudentStatusBar.CampusId
            End With

            'Create a new guid to be associated with the Employee pages
            strVid = Guid.NewGuid.ToString

            strEmployeeObjectPointer = strVid
            Session("EmployeeObjectPointer") = strEmployeeObjectPointer 'Needed as master page doesn't retain variable values when refreshed
            Session("EmployeeMRUFlag") = "true"

            'Add an entry to AdvantageSessionState for this guid and object
            'load Advantage state
            state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
            state(strVid) = objStateInfo
            'save current State
            CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
            If strEmployeeId.ToString.Trim = "" Then 'No Employer was selected from MRU
                Dim strRedirectUrl As String = ChildResourceURL + "&VID=" + strVid
                Response.Redirect(strRedirectUrl, True)
            Else
                Response.Clear()

                'Balaji 02/23/2012
                'The MRU list may come from different campuses, so make sure the QueryString always contains the 
                'lead or student's default campus
                'Reset AdvantageSession User State Object's CampusId Property
                '   Reason to reset user state: All Pages in Advantage are getting the campusid from the AdvantageSession.UserState Object
                '   So resetting the object here will impact all pages
                Dim strRedirectUrl As String = ""
                Try
                    Dim advResetUserState As New BO.User
                    advResetUserState = AdvantageSession.UserState
                    With advResetUserState
                        .CampusId = New Guid(objStateInfo.CampusId.ToString) 'New Guid(Request.QueryString("cmpid"))
                    End With
                    AdvantageSession.UserState = advResetUserState

                    strRedirectUrl = BuildRedirectURLForEmployeePages(AdvantageSession.UserState.CampusId.ToString, strVid)
                    'Session("Type") = "3"
                    If strRedirectUrl.Trim = "" Then
                        ShowNotificationWhenUserHasNoAccessToEntityPage("employee")
                        Dim mainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
                        Dim grdMru As RadGrid = DirectCast(DirectCast(mainPanelBar.FindItemByValue("Recently Viewed").Controls(0), Telerik.Web.UI.RadPanelItem).Controls(3), Telerik.Web.UI.RadGrid)
                        grdMru.ToolTip = "User has insufficient permissions to access employee pages"
                        Exit Sub
                    End If

                    Dim fac As New UserSecurityFacade
                    fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)

                    'UpdateMRUTable
                    'Reason: We need to keep track of the last student record the user worked with
                    'Scenario1 : User can log in and click on a student page without using MRU 
                    'and we need to display the data of the last student the user worked with
                    'If the user is a first time user, we will load the student who was last added to advantage
                    mruProvider.InsertMRU(3, strEmployeeId, AdvantageSession.UserState.UserId.ToString, _
                                              AdvantageSession.UserState.CampusId.ToString)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Throw New ArgumentException(ex.Message.ToString)
                End Try
                'ReBuildEmployeeTabs(strVID)


                Try
                    'Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
                    'MasterPageCampusDropDown.RandomSessionPersist = R.Next()     'hdRandomSessionId.Value = R.Next()
                    'strRedirectURL = strRedirectURL  + "&VSI=" + MasterPageCampusDropDown.RandomSessionPersist 'hdRandomSessionId.Value
                    Response.Redirect(strRedirectUrl, False)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
            End If
        Else
            RedirectToEmployeeSearchPage(AdvantageSession.UserState.CampusId.ToString)
        End If
    End Sub

    Public Sub BuildEmployerStateObject(ByVal EmployerId As String, ByVal EmployerName As String, Optional ByVal source As String = "")
        Dim objStateInfo As New AdvantageStateInfo
        Dim objGetStudentStatusBar As New AdvantageStateInfo
        Dim strVID As String
        Dim facInputMasks As New InputMasksFacade
        Dim strEmployerId As String = ""

        'Reset Employer Object Session Variables
        Session("EmployerObjectPointer") = ""
        Session("EmployerMRUFlag") = "false"

        If EmployerId.ToString.Trim = "" Then 'No Student was selected from MRU
            strEmployerId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
                                                                   2, _
                                                                   AdvantageSession.UserState.CampusId.ToString)
        Else
            strEmployerId = EmployerId
        End If

        If Not strEmployerId.Trim = "" Then
            objGetStudentStatusBar = mruProvider.BuildEmployerStatusBar(strEmployerId)
            With objStateInfo
                AdvantageSession.MasterEmployerId = strEmployerId
                .EmployerId = objGetStudentStatusBar.EmployerId
                .NameValue = objGetStudentStatusBar.NameValue
                .Address1 = objGetStudentStatusBar.Address1
                .Address2 = objGetStudentStatusBar.Address2
                .City = objGetStudentStatusBar.City
                .State = objGetStudentStatusBar.State
                .Zip = objGetStudentStatusBar.Zip
                .Phone = objGetStudentStatusBar.Phone
                .CampusDescrip = objGetStudentStatusBar.CampusDescrip 'This property stores campus group description and not campus description
                If source.Trim = "" AndAlso Not .CampusDescrip.ToLower.Trim = "all" Then
                    .CampusId = objGetStudentStatusBar.CampusId
                Else
                    .CampusId = AdvantageSession.UserState.CampusId.ToString 'comes from campus selector
                End If

            End With

            'Create a new guid to be associated with the employer pages
            strVID = Guid.NewGuid.ToString

            strEmployerObjectPointer = strVID

            Session("EmployerObjectPointer") = strEmployerObjectPointer 'Needed as master page doesn't retain variable values when refreshed
            Session("EmployerMRUFlag") = "true"

            'Add an entry to AdvantageSessionState for this guid and object
            'load Advantage state
            state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
            state(strVID) = objStateInfo
            'save current State
            CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

            If strEmployerId.ToString.Trim = "" Then 'No Employer was selected from MRU
                Dim strRedirectURL As String = ChildResourceURL + "&VID=" + strVID
                Response.Redirect(strRedirectURL, True)
            Else
                Response.Clear()

                'Balaji 02/23/2012
                'The MRU list may come from different campuses, so make sure the QueryString always contains the 
                'lead or student's default campus
                'Reset AdvantageSession User State Object's CampusId Property
                '   Reason to reset user state: All Pages in Advantage are getting the campusid from the AdvantageSession.UserState Object
                '   So resetting the object here will impact all pages
                Dim strRedirectURL As String = ""
                Try
                    Dim advResetUserState As New BO.User
                    advResetUserState = AdvantageSession.UserState
                    With advResetUserState
                        .CampusId = New Guid(objStateInfo.CampusId.ToString) 'New Guid(Request.QueryString("cmpid"))
                    End With
                    AdvantageSession.UserState = advResetUserState

                    strRedirectURL = BuildRedirectURLForEmployerPages(AdvantageSession.UserState.CampusId.ToString, strVID)
                    If strRedirectURL.Trim = "" Then
                        ShowNotificationWhenUserHasNoAccessToEntityPage("employer")
                        Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
                        Dim grdMRU As RadGrid = DirectCast(DirectCast(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0), Telerik.Web.UI.RadPanelItem).Controls(3), Telerik.Web.UI.RadGrid)
                        grdMRU.ToolTip = "User has insufficient permissions to access employer pages"
                        Exit Sub
                    End If

                    Dim fac As New UserSecurityFacade
                    fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)

                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Throw New ArgumentException(ex.Message.ToString)
                End Try

                'UpdateMRUTable
                'Reason: We need to keep track of the last student record the user worked with
                'Scenario1 : User can log in and click on a student page without using MRU 
                'and we need to display the data of the last student the user worked with
                'If the user is a first time user, we will load the student who was last added to advantage
                mruProvider.UpdateEmployerMRUList(strEmployerId, AdvantageSession.UserState.UserId.ToString, _
                                          AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)

                ' ReBuildEmployerTabs(strVID)

                Try
                    'Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
                    'Me.MasterPageCampusDropDown.RandomSessionPersist = R.Next() ' hdRandomSessionId.Value = R.Next()
                    'strRedirectURL = strRedirectURL + "&VSI=" + Me.MasterPageCampusDropDown.RandomSessionPersist 'hdRandomSessionId.Value
                    Response.Redirect(strRedirectURL, False)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)


                End Try
            End If
        Else
            RedirectToEmployerSearchPage(AdvantageSession.UserState.CampusId.ToString)
        End If
    End Sub

    Public Sub BuildStudentStateObject(ByVal studentId As String, Optional ByVal campusId As String = "")
        Dim objStateInfo As New AdvantageStateInfo
        Dim strVID As String
        Dim strStudentId As String = ""
        Dim strCampusId As String = ""

        If studentId.ToString.Trim = "" Then 'No Student was selected from MRU
            strStudentId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
                                                                   1, _
                                                                   AdvantageSession.UserState.CampusId.ToString)
            strCampusId = AdvantageSession.UserState.CampusId.ToString
        Else
            strStudentId = studentId
            strCampusId = campusId



        End If

        If Not strStudentId.Trim = "" Then
            objStateInfo = mruProvider.BuildStudentStatusBar(strStudentId, strCampusId)
            With objStateInfo
                If .StudentIdentifierCaption.ToLower = "ssn" Then
                    Dim ssnMask As String
                    Dim facInputMasks As New InputMasksFacade
                    ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
                    If .StudentIdentifier <> "" Then
                        .StudentIdentifier = facInputMasks.ApplyMask(ssnMask, .StudentIdentifier)
                    End If
                End If

                'add to the session
                AdvantageSession.MasterStudentId = strStudentId
                AdvantageSession.MasterName = objStateInfo.NameValue
            End With

            'Create a new guid to be associated with the employer pages
            strVID = Guid.NewGuid.ToString

            strStudentObjectPointer = strVID

            Session("StudentObjectPointer") = strStudentObjectPointer 'Needed as master page doesn't retain variable values when refreshed
            Session("LeadObjectPointer") = strStudentObjectPointer '""
            Session("LeadMRFlag") = "false"

            'Add an entry to AdvantageSessionState for this guid and object
            'load Advantage state
            state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
            state(strVID) = objStateInfo
            'save current State
            CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

            If strStudentId.ToString.Trim = "" Then 'No Student was selected from MRU
                Dim strRedirectURL As String = ChildResourceURL + "&VID=" + strVID
                Response.Redirect(strRedirectURL, True)
            Else
                'Balaji 02/23/2012
                'The MRU list may come from different campuses, so make sure the QueryString always contains the 
                'lead or student's default campus
                'Reset AdvantageSession User State Object's CampusId Property
                '   Reason to reset user state: All Pages in Advantage are getting the campusid from the AdvantageSession.UserState Object
                '   So resetting the object here will impact all pages
                Dim strRedirectURL As String = ""
                Try
                    Dim advResetUserState As New BO.User
                    advResetUserState = AdvantageSession.UserState
                    With advResetUserState
                        .CampusId = New Guid(objStateInfo.CampusId.ToString)
                    End With
                    AdvantageSession.UserState = advResetUserState

                    strRedirectURL = BuildRedirectUrlForStudentPages(AdvantageSession.UserState.CampusId.ToString, strVID)

                    If strRedirectURL.Trim = "" Then
                        ShowNotificationWhenUserHasNoAccessToEntityPage("student")
                        Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
                        Dim grdMRU As RadGrid = DirectCast(DirectCast(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0), Telerik.Web.UI.RadPanelItem).Controls(3), Telerik.Web.UI.RadGrid)
                        grdMRU.ToolTip = "User has insufficient permissions to access student pages"
                        Exit Sub
                    End If

                    Dim fac As New UserSecurityFacade
                    fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Throw New ArgumentException(ex.Message.ToString)
                End Try
                mruProvider.InsertMRU(1, strStudentId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                AdvantageSession.StudentMRU = Nothing
                Try
                    Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
                    'Me.MasterPageCampusDropDown.RandomSessionPersist = R.Next() 'hdRandomSessionId.Value = R.Next()
                    'strRedirectURL = strRedirectURL + "&VSI=" + Me.MasterPageCampusDropDown.RandomSessionPersist 'hdRandomSessionId.Value
                    Response.Redirect(strRedirectURL, False)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
            End If
        Else
            RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
        End If
    End Sub
    Private Sub RedirectToStudentSearchPage(ByVal modulecode As String, ByVal campusId As String)
        Dim strSearchUrl As String = ""
        strSearchUrl = "~/PL/StudentSearch.aspx?RESID=308&mod=" + modulecode + "&cmpId=" + campusId + "&desc=View Existing Students"
        Response.Redirect(strSearchUrl, False)
    End Sub
    Public Sub RedirectToLeadSearchPage(ByVal campusId As String)
        Dim strSearchUrl As String = ""
        strSearchUrl = "~/PL/SearchLead.aspx?RESID=153&mod=AD&cmpId=" + campusId + "&desc=View Existing Leads"
        Response.Redirect(strSearchUrl, False)
    End Sub
    Public Sub RedirectToEmployerSearchPage(ByVal campusId As String)
        Dim strSearchURL As String = ""
        strSearchURL = "~/PL/EmployerInfo.aspx?RESID=79&mod=PL&cmpid=" + campusId + "&desc=View Existing Employers"
        Response.Redirect(strSearchURL, False)
    End Sub
    Public Sub RedirectToEmployeeSearchPage(ByVal campusId As String)
        Dim strSearchUrl As String = ""
        strSearchUrl = "~/SY/EmployeeInfo.aspx?RESID=52&mod=HR&cmpid=" + campusId + "&desc=View Existing Employees"
        Response.Redirect(strSearchUrl, False)
    End Sub
    Private Function BuildRedirectUrlForStudentPages(ByVal campusId As String, ByVal strVid As String) As String
        Dim resIdToRedirect As Integer
        Dim strRedirectUrl As String

        resIdToRedirect = CInt(Request.QueryString("RESID"))

        'If user doesn't have access to default page, don't redirect user to the page, return an empty string
        Dim pObj As New UserPagePermissionInfo
        Select Case resIdToRedirect.ToString
            Case "90", "92", "95", "97", "112", _
                               "114", "116", "155", "159", "169", _
                               "175", "177", "200", "213", _
                               "230", "286", "301", "303", "327", _
                               "372", "374", "380", "479", "530", _
                               "531", "532", "534", "535", "538", _
                               "539", "541", "542", "543", "614", _
                               "625", "628", "652", "113", "334" 'Student Pages
                pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageSession.UserState, resIdToRedirect.ToString, campusId)
                If pObj.HasNone = True Then
                    Return ""
                    Exit Function
                End If
                strRedirectUrl = mruProvider.getURL(resIdToRedirect).ToString + "?RESID=" + resIdToRedirect.ToString + "&mod=AR&cmpid=" + campusId + "&VID=" + strVid + "&Type=1" + "&Source=MRU"
            Case Else
                'Student Info Page
                If AdvantageSession.UserState.FullPermission = True Then
                    resIdToRedirect = 203
                    strRedirectUrl = mruProvider.getURL(resIdToRedirect).ToString + "?RESID=" + resIdToRedirect.ToString + "&mod=AR&cmpid=" + campusId + "&VID=" + strVid + "&Type=1" + "&Source=MRU"
                    Return strRedirectUrl
                    Exit Function
                End If

                Dim iResource As New List(Of String)(New String() {"203", "90", "92", "95", "97", "112", _
                               "114", "116", "155", "159", "169", _
                               "175", "177", "200", "213", _
                               "230", "286", "301", "303", "327", _
                               "372", "374", "380", "479", "530", _
                               "531", "532", "534", "535", "538", _
                               "539", "541", "542", "543", "614", _
                               "625", "628", "652", "113", "334"})
                pObj = SecurityRoutines.CheckUserPermissionByCampusAndResourceForEntity(AdvantageSession.UserState, iResource, campusId)
                If pObj.HasNone = True Then
                    Return ""
                    Exit Function
                End If
                strRedirectUrl = mruProvider.getURL(pObj.ResourceId).ToString + "?RESID=" + pObj.ResourceId.ToString + "&mod=AR&cmpid=" + campusId + "&VID=" + strVid + "&Type=1" + "&Source=MRU"
        End Select
        Return strRedirectUrl
    End Function
    Private Function BuildRedirectUrlForLeadPagesWhileSwitchingCampus(ByVal campusId As String, ByVal strVid As String) As String
        Dim resIdToRedirect As Integer
        Dim strRedirectUrl As String
        resIdToRedirect = CInt(Request.QueryString("RESID"))
        Select Case resIdToRedirect.ToString
            Case "145", "146", "147", "148", _
                          "225", "313", "456", "484", "793" 'Lead Tabs
                strRedirectUrl = mruProvider.getURL(resIdToRedirect).ToString + "?RESID=" + resIdToRedirect.ToString + "&mod=AD&cmpid=" + campusId + "&VID=" + strVid + "&Type=4" + "&Source=MRU" + "&switch=true"
            Case Else
                'Lead Info Page
                resIdToRedirect = 170
                strRedirectUrl = mruProvider.getURL(resIdToRedirect).ToString + "?RESID=" + resIdToRedirect.ToString + "&mod=AD&cmpid=" + campusId + "&VID=" + strVid + "&Type=4" + "&Source=MRU" + "&switch=true"
        End Select
        Return strRedirectUrl
    End Function
    Private Function BuildRedirectURLForLeadPages(ByVal CampusId As String, ByVal strVID As String) As String
        Dim resIdToRedirect As Integer
        Dim strRedirectUrl As String
        resIdToRedirect = CInt(Request.QueryString("RESID"))

        'If user doesn't have access to default page, don't redirect user to the page, return an empty string
        Dim pObj As New UserPagePermissionInfo
        Select Case resIdToRedirect.ToString
            Case "145", "146", "147", "148", _
                          "225", "313", "456", "484" 'Lead Tabs
                pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageSession.UserState, resIdToRedirect.ToString, CampusId)
                If pObj.HasNone = True Then
                    Return ""
                    Exit Function
                End If
                strRedirectUrl = mruProvider.getURL(resIdToRedirect).ToString + "?RESID=" + resIdToRedirect.ToString + "&mod=AD&cmpid=" + CampusId + "&VID=" + strVID + "&Type=4" + "&Source=MRU"
            Case Else
                'Lead Info Page
                'resIdToRedirect = 170
                If AdvantageSession.UserState.FullPermission = True Then
                    resIdToRedirect = 170
                    strRedirectUrl = mruProvider.getURL(resIdToRedirect).ToString + "?RESID=" + resIdToRedirect.ToString + "&mod=AD&cmpid=" + CampusId + "&VID=" + strVID + "&Type=4" + "&Source=MRU"
                    Return strRedirectUrl
                    Exit Function
                End If
                Dim iResource As New List(Of String)(New String() {"170", "145", "146", "147", "148", "225", "313", "456", "484"})
                pObj = SecurityRoutines.CheckUserPermissionByCampusAndResourceForEntity(AdvantageSession.UserState, iResource, CampusId)
                If pObj.HasNone = True Then
                    Return ""
                    Exit Function
                End If
                strRedirectUrl = mruProvider.getURL(pObj.ResourceId).ToString + "?RESID=" + pObj.ResourceId.ToString + "&mod=AD&cmpid=" + CampusId + "&VID=" + strVID + "&Type=4" + "&Source=MRU"
        End Select
        Return strRedirectUrl
    End Function
    Private Function BuildRedirectURLForEmployerPages(ByVal CampusId As String, ByVal strVID As String) As String
        Dim resIdToRedirect As Integer
        Dim strRedirectURL As String
        resIdToRedirect = CInt(Request.QueryString("RESID"))
        'If user doesn't have access to default page, don't redirect user to the page, return an empty string
        Dim pObj As New UserPagePermissionInfo
        Select Case resIdToRedirect.ToString
            Case "82", "86", "111"
                pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageSession.UserState, resIdToRedirect.ToString, CampusId)
                If pObj.HasNone = True Then
                    Return ""
                    Exit Function
                End If
                strRedirectURL = mruProvider.getURL(resIdToRedirect).ToString + "?RESID=" + resIdToRedirect.ToString + "&mod=PL&cmpid=" + CampusId + "&VID=" + strVID + "&Type=2" + "&Source=MRU"
            Case Else
                'resIdToRedirect = 79
                If AdvantageSession.UserState.FullPermission = True Then
                    resIdToRedirect = 79
                    strRedirectURL = mruProvider.getURL(resIdToRedirect).ToString + "?RESID=" + resIdToRedirect.ToString + "&mod=PL&cmpid=" + CampusId + "&VID=" + strVID + "&Type=2" + "&Source=MRU"
                    Return strRedirectURL
                    Exit Function
                End If
                Dim iResource As New List(Of String)(New String() {"79", "82", "86", "111"})
                pObj = SecurityRoutines.CheckUserPermissionByCampusAndResourceForEntity(AdvantageSession.UserState, iResource, CampusId)
                If pObj.HasNone = True Then
                    Return ""
                    Exit Function
                End If
                strRedirectURL = mruProvider.getURL(pObj.ResourceId).ToString + "?RESID=" + pObj.ResourceId.ToString + "&mod=PL&cmpid=" + CampusId + "&VID=" + strVID + "&Type=2" + "&Source=MRU"
        End Select
        Return strRedirectURL
    End Function
    Private Function BuildRedirectURLForEmployeePages(ByVal CampusId As String, ByVal strVID As String) As String
        Dim resIdToRedirect As Integer
        Dim strRedirectURL As String
        resIdToRedirect = CInt(Request.QueryString("RESID"))
        'If user doesn't have access to default page, don't redirect user to the page, return an empty string
        Dim pObj As New UserPagePermissionInfo
        Select Case resIdToRedirect.ToString
            Case "55", "69", "281"
                strRedirectURL = mruProvider.getURL(resIdToRedirect).ToString + "?RESID=" + resIdToRedirect.ToString + "&mod=HR&cmpid=" + CampusId + "&VID=" + strVID + "&Type=3" + "&Source=MRU"
                pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageSession.UserState, resIdToRedirect.ToString, CampusId)
                If pObj.HasNone = True Then
                    Return ""
                    Exit Function
                End If
            Case Else
                'Employee Info Page
                If AdvantageSession.UserState.FullPermission = True Then
                    resIdToRedirect = 52
                    strRedirectURL = mruProvider.getURL(resIdToRedirect).ToString + "?RESID=" + resIdToRedirect.ToString + "&mod=HR&cmpid=" + CampusId + "&VID=" + strVID + "&Type=3" + "&Source=MRU"
                    Return strRedirectURL
                    Exit Function
                End If
                Dim iResource As New List(Of String)(New String() {"52", "55", "69", "281"})
                pObj = SecurityRoutines.CheckUserPermissionByCampusAndResourceForEntity(AdvantageSession.UserState, iResource, CampusId)
                If pObj.HasNone = True Then
                    Return ""
                    Exit Function
                End If
                strRedirectURL = mruProvider.getURL(pObj.ResourceId).ToString + "?RESID=" + pObj.ResourceId.ToString + "&mod=HR&cmpid=" + CampusId + "&VID=" + strVID + "&Type=3" + "&Source=MRU"
        End Select
        Return strRedirectURL
    End Function
    Public Sub BuildLeadStateObject(ByVal LeadId As String, Optional ByVal boolSwitchCampus As Boolean = False)
        Dim objStateInfo As New AdvantageStateInfo
        Dim strVID As String
        Dim strLeadId As String = ""

        If LeadId.ToString.Trim = "" Then 'No Student was selected from MRU
            strLeadId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
                                                                   4, _
                                                                   AdvantageSession.UserState.CampusId.ToString)
        Else
            strLeadId = LeadId
        End If

        If Not strLeadId.Trim = "" Then
            objStateInfo = mruProvider.BuildLeadStatusBar(strLeadId)

            'Create a new guid to be associated with the employer pages
            strVID = Guid.NewGuid.ToString

            strLeadObjectPointer = strVID 'Set VID to Lead Object Pointer

            Session("LeadObjectPointer") = strVID ' This step is required, as master page loses values stored in class variables

            If objStateInfo.SystemStatus = 6 Then 'If lead is enrolled set the student pointer
                Session("StudentObjectPointer") = strVID
            Else
                Session("StudentObjectPointer") = ""
            End If

            'Add an entry to AdvantageSessionState for this guid and object
            'load Advantage state
            state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
            state(strVID) = objStateInfo
            'save current State
            CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

            'add to the session
            AdvantageSession.MasterLeadId = strLeadId
            AdvantageSession.MasterName = objStateInfo.NameValue

            'UpdateMRUTable
            'Reason: We need to keep track of the last student record the user worked with
            'Scenario1 : User can log in and click on a student page without using MRU 
            'and we need to display the data of the last student the user worked with
            'If the user is a first time user, we will load the student who was last added to advantage
            'mruProvider.UpdateMRUList(strLeadId, AdvantageSession.UserState.UserId.ToString, _
            '                          AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)

            If strLeadId.ToString.Trim = "" Then 'No Student was selected from MRU
                Dim strRedirectURL As String = ChildResourceURL + "&VID=" + strVID
                Response.Redirect(strRedirectURL, True)
                'AdvantageSession.UserState.RedirectURL = AdvantageSession.UserState.RedirectURL + "&VID=" + strVID
            Else
                'Balaji 02/23/2012
                'The MRU list may come from different campuses, so make sure the QueryString always contains the 
                'lead or student's default campus
                'Reset AdvantageSession User State Object's CampusId Property
                '   Reason to reset user state: All Pages in Advantage are getting the campusid from the AdvantageSession.UserState Object
                '   So resetting the object here will impact all pages
                Dim strRedirectURL As String = ""
                Try
                    Dim advResetUserState As New BO.User
                    advResetUserState = AdvantageSession.UserState
                    With advResetUserState
                        .CampusId = New Guid(objStateInfo.CampusId.ToString)
                    End With
                    AdvantageSession.UserState = advResetUserState

                    strRedirectURL = BuildRedirectURLForLeadPages(AdvantageSession.UserState.CampusId.ToString, strVID)
                    If strRedirectURL.Trim = "" Then
                        ShowNotificationWhenUserHasNoAccessToEntityPage("lead")
                        Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
                        Dim grdMRU As RadGrid = DirectCast(DirectCast(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0), Telerik.Web.UI.RadPanelItem).Controls(3), Telerik.Web.UI.RadGrid)
                        grdMRU.ToolTip = "User has insufficient permissions to access lead pages"
                        Exit Sub
                    End If

                    Dim fac As New UserSecurityFacade

                    fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Throw New ArgumentException(ex.Message.ToString)
                End Try

                mruProvider.InsertMRU(4, strLeadId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                AdvantageSession.LeadMRU = Nothing
                If strRedirectURL.Trim = "" Then
                    ShowNotificationWhenUserHasNoAccessToEntityPage("lead")
                    Dim mainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
                    Dim grdMru As RadGrid = DirectCast(DirectCast(mainPanelBar.FindItemByValue("Recently Viewed").Controls(0), Telerik.Web.UI.RadPanelItem).Controls(3), Telerik.Web.UI.RadGrid)
                    grdMru.ToolTip = "User has insufficient permissions to access lead pages"
                    Exit Sub
                End If

                Try
                    'Dim r As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
                    'Me.MasterPageCampusDropDown.RandomSessionPersist = R.Next() 'hdRandomSessionId.Value = R.Next()
                    'strRedirectURL = strRedirectURL + "&VSI=" + Me.MasterPageCampusDropDown.RandomSessionPersist ' hdRandomSessionId.Value
                    Response.Redirect(strRedirectURL, False)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)


                End Try
            End If
        Else
            RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
        End If
    End Sub

    Protected Sub deleteMRU(ByVal sender As Object, ByVal e As GridCommandEventArgs)
        If e.CommandName = "deletemruitem" Then
            Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
            Dim grdMRU As RadGrid = DirectCast(DirectCast(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0), Telerik.Web.UI.RadPanelItem).Controls(3), Telerik.Web.UI.RadGrid)
            Dim strMRUId As String = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("Id").ToString

            If strMRUId.ToString.Trim.Length > 36 Then
                strMRUId = strMRUId.Substring(0, 36)
            End If
            Dim strType As String = mruProvider.DeleteMRUFromList(strMRUId) 'Delete from MRU
            'BuildMRUListByUser(strType) 'Re-build the MRU List
            'BuildMRUListByUserAfterDelete(strType)
            BuildMRUListByUserForPoweruser(strType, True)
        End If
    End Sub
    Protected Sub ChangeImageIcon(ByVal sender As Object, ByVal e As GridItemEventArgs)
    End Sub
#End Region

#Region "Status Bar"
    Private Overloads Function LoadControl(ByVal UserControlPath As String, ByVal ParamArray constructorParameters As Object()) As UserControl
        Dim constParamTypes As New List(Of Type)()
        For Each constParam As Object In constructorParameters
            constParamTypes.Add(constParam.[GetType]())
        Next

        Dim ctl As UserControl = TryCast(Page.LoadControl(UserControlPath), UserControl)

        ' Find the relevant constructor
        Dim constructor As ConstructorInfo = ctl.[GetType]().BaseType.GetConstructor(constParamTypes.ToArray())

        'And then call the relevant constructor
        If constructor Is Nothing Then
            Throw New MemberAccessException("The requested constructor was not found on : " & ctl.[GetType]().BaseType.ToString())
        Else
            constructor.Invoke(ctl, constructorParameters)
        End If

        ' Finally return the fully initialized UC
        Return ctl
    End Function


    ''' <summary>
    ''' new function to replace the one above
    ''' </summary>
    ''' <param name="StatusBarState"></param>
    ''' <remarks></remarks>
    Public Sub ShowHideStatusBarControl(ByVal StatusBarState As Boolean)
        ''Trace.Warn("Begin Type")
        intMRUType = CInt(Request.QueryString("Type"))


        Try
            'While clicking on Magnifying glass in the info pages we set &bar attribute
            'if this condition is not checked, the info bar will show up in all 
            'search pages. The behavior is info bar should be hidden for search pages
            'Commented by Balaji on Mar 12 2012
            If Request.QueryString.ToString.Contains("&bar") Or Request.QueryString("RESID") = 308 Then
                StatusBarState = False
                Exit Try
            End If
            If StatusBarState = True Then
                Select Case Request.QueryString("ResId").ToString
                    Case "145", "146", "147", "148", "170", _
                            "225", "313", "456", "484", "793" 'Lead Tabs
                        Dim toAdd As Control = LoadControl("~/usercontrols/LeadInfoBar.ascx")
                        MyStatusBar.Controls.Add(toAdd)
                    Case "264", "90", "92", "95", "97", "112", _
                               "114", "116", "155", "159", "169", _
                                           "175", "177", "200", "203", "213", _
                                           "230", "286", "301", "303", "327", _
                                           "372", "374", "380", "479", "530", _
                                           "531", "532", "534", "535", "538", _
                                           "539", "541", "542", "543", "614", _
                                           "625", "628", "652", "113", "334" ' Student Page
                        Dim toAdd As Control = LoadControl("~/usercontrols/StudentInfoBar.ascx", "", CurrentCampusId)
                        MyStatusBar.Controls.Add(toAdd)
                    Case "79", "82", "86", "111" 'Employer Tabs
                        Dim toAdd As Control = LoadControl("~/usercontrols/EmployerInfoBar.ascx")
                        MyStatusBar.Controls.Add(toAdd)
                    Case "52", "55", "69", "281" 'Employee Tabs
                        Dim toAdd As Control = LoadControl("~/usercontrols/EmployeeInfoBar.ascx")
                        MyStatusBar.Controls.Add(toAdd)
                End Select
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            MyStatusBar.Visible = False
            TopPane.Height = 30
        End Try

        If StatusBarState = True Then
            MyStatusBar.Visible = True
            TopPane.Height = 105
        Else
            MyStatusBar.Visible = False
            TopPane.Height = 30
        End If
        'Trace.Warn("End Type")
    End Sub

    'TODO:  REMOVE THIS FUNCTION AND ALL REFERENCES
    Public Function CheckIfPageIsPartofMenuWhileSwitchingCampus(ByVal intPageResourceId As Integer, _
                                                ByVal intPageType As Integer) As Boolean
        Return True
    End Function
    Public Sub shownotification(ByVal messageid As Integer, ByVal strName As String)

        If String.IsNullOrEmpty(strName) Then
            Exit Sub
        End If


        strName = "(" + strName + ")"


        Dim MRUToolBar As RadToolBar
        Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
        MRUToolBar = CType(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0).FindControl("FHToolBar"), RadToolBar)

        Select Case messageid
            Case 1
                If CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 1 And Session("SwitchEntity") = False Then 'This will prevent message showing for wrong entities
                    RadNotification1.Show()
                    RadNotification1.Text = "You are currently viewing the details of student " & strName '& " as the lead you were viewing earlier is still not enrolled as a student"
                End If
            Case 2
                If CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 2 And Session("SwitchEntity") = False Then
                    RadNotification1.Show()
                    RadNotification1.Text = "You are currently viewing the details of employer " & strName '& " as the lead you were viewing earlier is still not enrolled as a student"
                End If
            Case 4
                If CType(MRUToolBar.Items(0), RadToolBarSplitButton).DefaultButtonIndex = 0 And Session("SwitchEntity") = False Then
                    RadNotification1.Show()
                    RadNotification1.Text = "You are currently viewing the details of lead " & strName '& " as the student you were viewing earlier did not have any lead details"
                End If
        End Select
    End Sub

    Private Sub ShowNotificationWhenUserHasNoAccessToEntityPage(ByVal entity As String)
        RadNotification1.Text = "User has insufficient permission to access " & entity & " pages"
        RadNotification1.Show()
    End Sub
#End Region


#Region "Audit History"
    Dim _PageObjectId As String
    Dim _PageResourceId As Integer
    Public Property PageObjectId() As String
        Set(ByVal value As String)
            _PageObjectId = value
        End Set
        Get
            Return _PageObjectId
        End Get
    End Property
    Public Property PageResourceId() As Integer
        Set(ByVal value As Integer)
            _PageResourceId = value
        End Set
        Get
            Return _PageResourceId
        End Get
    End Property
    Public Sub setHiddenControlForAudit()
        hdnObjectId.Value = PageObjectId
        hdnPageResourceId.Value = PageResourceId
        If hdnObjectId.Value <> "" Then
            btnAudit.Visible = True
        End If
    End Sub
#End Region


#Region "Get Build Version"
    Public Function GetBuildVersion() As String
        Dim versionString As String
        Try
            Dim myFileVersionInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Server.MapPath("~/Bin/Common.dll"))
            versionString = myFileVersionInfo.FileVersion.ToString()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            versionString = String.Empty
        End Try
        Return versionString
    End Function
#End Region

#Region "FERPA"
    Public Sub SetHiddenControlForFerpa()
        Dim strVid As String = ""
        Dim resId As Integer = 0
        Dim studentId As String = ""
        Dim dt As New DataTable

        Try
            strVid = Request.QueryString("VID")
            state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
            resId = Request.QueryString("ResId").ToString


            If intMRUType = 1 Then      'student page
                If Not String.IsNullOrEmpty(strVid) Then
                    If strVid = "1" Then    'valid VID
                        Exit Sub
                    Else
                        If state.Count > 0 Then
                            studentId = DirectCast(state(strVid), AdvantageStateInfo).StudentId
                            If Not studentId Is Nothing Then

                                If (New StudentFERPA).HasFERPAPermission(resId, studentId) Then
                                    FERPA.Visible = True
                                    dt = (New StudentFERPA).GetFERPAPermission(resId, studentId)
                                    Dim rp As New Repeater
                                    rp = Repeater1

                                    rp.DataSource = dt
                                    rp.DataBind()

                                Else
                                    If (New StudentFERPA).HasGivenFERPAPermission(studentId) Then
                                        FERPA.Visible = True
                                        dt = (New StudentFERPA).GetFERPAPermission(0, studentId)
                                        Dim rp As New Repeater
                                        rp = Repeater1
                                        rp.DataSource = dt
                                        rp.DataBind()
                                    End If

                                End If

                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Response.Redirect(FormsAuthentication.LoginUrl)
        End Try

    End Sub
#End Region

#Region "TenantConnectionfrom Advsetting"
    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region

End Class

