﻿namespace FAME.Advantage.Messages.Widget
{
    public class VoyantWidgetOutputModel
    {
        /// <summary>
        /// The URL to show in the widget i-frame 
        /// </summary>
        public string UrlWidget { get; set; }

        /// <summary>
        /// URL to be show in the maximize windows of widget.
        /// </summary>
        public string UrlWidGetMaximized { get; set; }

        /// <summary>
        /// Show the custom command icon
        /// </summary>
        public bool ShowCustomCommand { get; set; }

        /// <summary>
        /// Show the maximize command icon
        /// </summary>
        public bool ShowMaximizeCommand { get; set; }

        /// <summary>
        /// Show the minimize Command icon
        /// </summary>
        public bool ShowMinimizeCommand { get; set; }

        /// <summary>
        /// Load the Widget or not.
        /// </summary>
        public bool ShowWidget { get; set; }
    }
}
