﻿namespace FAME.Advantage.Messages.Widget
{
    /// <summary>
    /// Input filter
    /// </summary>
    public class VoyantWidgetInputFilter
    {
        /// <summary>
        /// The Guid of the user that want to use the Widget.
        /// </summary>
        public string UserGuid { get; set; }

        /// <summary>
        /// The campus ID
        /// </summary>
        public string CampusGuid { get; set; }

        /// <summary>
        /// The Windows Service to consulting
        /// </summary>
        public string WindowServiceName { get; set; }
    }
}
