﻿using FAME.Advantage.Messages.CampusGroups;
using FAME.Advantage.Messages.StatusOptions;

namespace FAME.Advantage.Messages.Requirements
{
    public class EducationLevelOutputMap
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public StatusOptionOutputModel Status { get; set; }
        public CampusGroupOutputModel CampusGroup { get; protected set; }
    }
}
