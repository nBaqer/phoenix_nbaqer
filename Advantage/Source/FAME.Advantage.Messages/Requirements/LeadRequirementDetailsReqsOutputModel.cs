﻿using System;

namespace FAME.Advantage.Messages.Lead
{
    public class LeadReqDetailsReqsOutputModel
    {
        
        public Guid Id { get; set; }
        public Guid LeadId { get; set; }
        public Guid DocumentId { get; set; }
        public string Description { get; set; }
        public string DateReceived { get; set; }
        public bool IsApproved { get; set; }
        public string ApprovalDate { get; set; }
        public bool IsOverridden { get; set; }
        public string OverrideReason { get; set; }
    }
}
