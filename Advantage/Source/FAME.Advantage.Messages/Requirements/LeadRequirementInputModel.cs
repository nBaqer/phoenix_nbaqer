﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadRequirementInputModel.cs" company="FAME Inc">
//   2016
//   FAME.Advantage.Messages.Requirements.LeadRequirementInputModel
// </copyright>
// <summary>
//   Defines the LeadRequirementInputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Requirements
{
    using System;

    /// <summary>
    /// The lead requirement input model.
    /// </summary>
    public class LeadRequirementInputModel
    {
        /// <summary>
        /// Gets or sets the document requirement id.
        /// </summary>
        public Guid DocReqId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is overridden.
        /// </summary>
        public bool IsOverridden { get; set; }

        /// <summary>
        /// Gets or sets the overide reason.
        /// </summary>
        public string overReason { get; set; }

        /// <summary>
        /// Gets or sets the reason.
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Gets or sets the date received.
        /// </summary>
        public DateTime? DateReceived { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is approved.
        /// </summary>
        public bool IsApproved { get; set; }

        /// <summary>
        /// Gets or sets the approval date.
        /// </summary>
        public DateTime? ApprovalDate { get; set; }

        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        public Guid LeadId { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the requirement id.
        /// </summary>
        public Guid ReqId { get; set; }

        /// <summary>
        /// Gets or sets the requirement type.
        /// </summary>
        public string RequirementType { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        public string Score { get; set; }

        /// <summary>
        /// Gets or sets the min score.
        /// </summary>
        public string MinScore { get; set; }

        /// <summary>
        /// Gets or sets the transaction code id.
        /// </summary>
        public Guid TransCodeId { get; set; }

        /// <summary>
        /// Gets or sets the transaction reference.
        /// </summary>
        public string TransReference { get; set; }

        /// <summary>
        /// Gets or sets the transaction description.
        /// </summary>
        public string TransDescription { get; set; }

        /// <summary>
        /// Gets or sets the transaction amount.
        /// </summary>
        public decimal TransAmount { get; set; }

        /// <summary>
        /// Gets or sets the transaction date.
        /// </summary>
        public DateTime TransDate { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the payment reference.
        /// </summary>
        public string PaymentReference { get; set; }

        /// <summary>
        /// Gets or sets the pay type id.
        /// </summary>
        public int PayTypeId { get; set; }

        /// <summary>
        /// Gets or sets the post transaction id.
        /// </summary>
        public string PostTransactionId { get; set; }

        /// <summary>
        /// Gets or sets the payment transaction id.
        /// </summary>
        public string PaymentTransactionId { get; set; }

        /// <summary>
        /// Gets or sets the document status id.
        /// </summary>
        public string DocumentStatusId { get; set; }
    }
}
