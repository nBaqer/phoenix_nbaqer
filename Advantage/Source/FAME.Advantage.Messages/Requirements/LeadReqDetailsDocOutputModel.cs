﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadReqDetailsDocOutputModel.cs" company="FAME Inc">
//   2018
// </copyright>
// <summary>
//   Defines the LeadReqDetailsDocOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    using System;

    /// <summary>
    /// The lead requirements details doc output model.
    /// </summary>
    public class LeadReqDetailsDocOutputModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the file name.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the file name with extension.
        /// </summary>
        public string FileNameWithExtension { get; set; }

        /// <summary>
        /// Gets or sets the extension.
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Gets or sets the upload date.
        /// </summary>
        public string UploadDate { get; set; }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Gets or sets the user who uploaded the document.
        /// </summary>
        public string User { get; set; }
    }
}
