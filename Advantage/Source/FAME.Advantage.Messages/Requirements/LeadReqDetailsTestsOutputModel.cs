﻿using System;

namespace FAME.Advantage.Messages.Lead
{
    public class LeadReqDetailsTestsOutputModel
    {
        
        public Guid Id { get; set; }
        public Guid LeadId { get; set; }
        public Guid DocumentId { get; set; }
        public string Score { get; set; }
        public string Pass { get; set; }
        public string RequirementType { get; set; }
        public string DateReceived { get; set; }

       
        public bool IsApproved { get; set; }
        public string ApprovalDate { get; set; }

        public bool IsOverridden { get; set; }
        
       
    }
}
