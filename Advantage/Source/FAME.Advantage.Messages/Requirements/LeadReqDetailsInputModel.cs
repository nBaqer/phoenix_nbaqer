﻿using System;
using System.Collections.Generic;

namespace FAME.Advantage.Messages.Lead
{
    public class LeadReqDetailsInputModel
    {
        
        public Guid Id { get; set; }
        public string RequirementType { get; set; }
        public string FilePath { get; set; }
       
    }
}
