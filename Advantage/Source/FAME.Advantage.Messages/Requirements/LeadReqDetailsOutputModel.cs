﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadReqDetailsOutputModel.cs" company="FAME Inc.">
//   FAME 2016
//   // FAME.Advantage.Messages.Lead.LeadReqDetailsOutputModel
// </copyright>
// <summary>
//   Defines the LeadReqDetailsOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Messages.Requirements
{
    using System;
    using System.Collections.Generic;
    using Lead;

    /// <summary>
    /// The lead requirement details output model.
    /// </summary>
    public class LeadReqDetailsOutputModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        public Guid LeadId { get; set; }

        /// <summary>
        /// Gets or sets the document id when requirement type is a Document otherwise is an Empty GUID.
        /// </summary>
        public Guid DocumentId { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the date received.
        /// </summary>
        public string DateReceived { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is approved.
        /// </summary>
        public bool IsApproved { get; set; }

        /// <summary>
        /// Gets or sets the approval date.
        /// </summary>
        public string ApprovalDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is overridden.
        /// </summary>
        public bool IsOverridden { get; set; }

        /// <summary>
        /// Gets or sets the override reason.
        /// </summary>
        public string OverrideReason { get; set; }

        /// <summary>
        /// Gets or sets the document status id when requirement type is a Document otherwise is null GUID
        /// </summary>
        public Guid? DocumentStatusId { get; set; }

        /// <summary>
        /// Gets or sets the document history.
        /// </summary>
        public List<LeadReqDetailsDocOutputModel> DocumentHistory { get; set; }

        /// <summary>
        /// Gets or sets the fee history.
        /// </summary>
        public List<LeadReqDetailsFeeOutputModel> FeeHistory { get; set; }

        /// <summary>
        /// Gets or sets the test history.
        /// </summary>
        public List<LeadReqDetailsTestsOutputModel> TestHistory { get; set; }

        /// <summary>
        /// Gets or sets the requirement history.
        /// </summary>
        public List<LeadReqDetailsReqsOutputModel> ReqsHistory { get; set; }

    }
}
