﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadReqDetailsFeeOutputModel.cs" company="FAME Inc.">
//   FAME Inc 2016
//   FAME.Advantage.Messages.Requirements.LeadReqDetailsFeeOutputModel
// </copyright>
// <summary>
//   The lead requirement details fee output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Messages.Requirements
{
    using System;

    /// <summary>
    /// The lead requirement details fee output model.
    /// </summary>
    public class LeadReqDetailsFeeOutputModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the code (Fee Type).
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// Gets or sets the transaction description.
        /// </summary>
        public virtual string TranDescription { get; set; }

        /// <summary>
        /// Gets or sets the transaction date.
        /// </summary>
        public virtual string TransDate { get;  set; }

        /// <summary>
        /// Gets or sets the reference.
        /// </summary>
        public virtual string Reference { get;  set; }

        /// <summary>
        /// Gets or sets the transaction document id.
        /// </summary>
        public virtual string TransDocumentId { get;  set; }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        public virtual string User { get;  set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        public virtual string Amount { get;  set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public virtual string Type { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is voided.
        /// </summary>
        public virtual bool IsVoided { get; set; }
    }
}
