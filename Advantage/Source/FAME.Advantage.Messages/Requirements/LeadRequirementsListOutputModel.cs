﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadRequirementsListOutputModel.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Messages.Requirements.LeadRequirementsListOutputModel
// </copyright>
// <summary>
//   The lead requirements list output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Requirements
{
    using System.Collections.Generic;

    using Lead;

    /// <summary>
    /// The lead requirements list output model.
    /// </summary>
    public class LeadRequirementsListOutputModel
    {
        /// <summary>
        /// Gets or sets the mandatory requirements.
        /// </summary>
        public IEnumerable<LeadReqEntityOutputModel> MandatoryRequirements { get; set; }

        /// <summary>
        /// Gets or sets the requirement groups.
        /// </summary>
        public IEnumerable<LeadReqEntityOutputModel> RequirementGroups { get; set; }

        /// <summary>
        /// Gets or sets the optional requirements.
        /// </summary>
        public IEnumerable<LeadReqEntityOutputModel> OptionalRequirements { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether have met requirements.
        /// </summary>
        public bool HaveMetRequirements { get; set; }
    }
}
