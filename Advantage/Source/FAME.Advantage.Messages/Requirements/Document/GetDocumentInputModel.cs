﻿using System;

namespace FAME.Advantage.Messages.Requirements.Document
{
    public class GetDocumentInputModel
    {
        public Guid? DocumentStatusId { get; set; }
        public bool? IsApproved { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public Guid CampusId { get; set; }
        public Guid? ProgramVersionId { get; set; }
        public Guid? EnrollmentStatusId { get; set; }
        public Guid? StudentGroupId { get; set; }
        public DateTime? StartDate { get; set; }
        public double? ShowLastValue { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? FileUploaded { get; set; }
        public Guid UserId { get; set; }
    }
}
