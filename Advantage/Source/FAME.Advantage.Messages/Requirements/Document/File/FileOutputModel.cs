﻿using System;

namespace FAME.Advantage.Messages.Requirements.Document.File
{
    public class FileOutputModel
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
