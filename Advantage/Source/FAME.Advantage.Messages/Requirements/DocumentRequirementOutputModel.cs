﻿using System;

namespace FAME.Advantage.Messages.Requirements
{
    public class DocumentRequirementOutputModel
    {
        public DocumentRequirementOutputModel(Guid studentId)
        {
            StudentId = studentId;
        }

        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int Type { get;  set; }
        public string RequiredFor { get; set; }
        public string RequirementType { get; set; }
        public string Status { get; set; }        
        public string Module { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? DateRequested { get; set; }
        public DateTime? DateReceived { get; set; }
        public bool IsApproved { get; set; }
        public Guid? DocumentStatusId { get; set; }
        public string DocumentStatusDescription { get; set; }

        public Guid? DocumentRequirementId { get; set; }
        public Guid? StudentId { get; set; }
        public Guid? StudentDocumentId { get; set; }
        public bool ShowUpdate { get; set; }
    }
}
