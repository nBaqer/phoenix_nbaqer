﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TranscriptDto.cs" company="FAME">
//   2016
// </copyright>
// <summary>
// JAGG - The transcript DTO.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Wapi.KlassApp
{
    using System;

    /// <summary>
    ///  The transcript DTO.
    /// </summary>
    [Serializable]
    public class TranscriptDto
    {
        /// <summary>
        /// Gets or sets the program description.
        /// </summary>
        public string ProgDescrip { get; set; }

        /// <summary>
        /// Gets or sets the program version ID.
        /// </summary>
        public Guid PrgVerId { get; set; }

        /// <summary>
        /// Gets or sets the program version description.
        /// </summary>
        public string PrgVerDescrip { get; set; }

        /// <summary>
        /// Gets or sets the program version track credits.
        /// </summary>
        public int PrgVersionTrackCredits { get; set; }

        /// <summary>
        /// Gets or sets the program version type ID.
        /// </summary>
        public int ProgramVersionTypeId { get; set; }

        /// <summary>
        /// Gets or sets the description program version type.
        /// </summary>
        public string DescriptionProgramVersionType { get; set; }

        /// <summary>
        /// Gets or sets the degree description.
        /// </summary>
        public string DegreeDescrip { get; set; }

        /// <summary>
        /// Gets or sets the overall average.
        /// </summary>
        public decimal? OverallAverage { get; set; }

        /// <summary>
        /// Gets or sets the weighted average cumulative GPA.
        /// </summary>
        public decimal WeightedAverage_CumGPA { get; set; }

        /// <summary>
        /// Gets or sets the simple average cumulative GPA.
        /// </summary>
        public decimal SimpleAverage_CumGPA { get; set; }

        /// <summary>
        /// Gets or sets the student start date.
        /// </summary>
        public DateTime StudentStartDate { get; set; }

        /// <summary>
        /// Gets or sets the graduation date.
        /// </summary>
        public DateTime GraduantionDate { get; set; }

        /// <summary>
        /// Gets or sets the status description.
        /// </summary>
        public string StatusDescrip { get; set; }

        /// <summary>
        /// Gets or sets the student last date attended.
        /// </summary>
        public DateTime StudentLastDateAttended { get; set; }

        /// <summary>
        /// Gets or sets the unit type description from attendance unit type.
        /// </summary>
        public string UnitTypeDescripFrom_arAttUnitType { get; set; }

        /// <summary>
        /// Gets or sets the unit type description.
        /// </summary>
        public string UnitTypeDescrip { get; set; }

        /// <summary>
        /// Gets or sets the total days attended.
        /// it is the total days than can be attended until now
        /// </summary>
        public decimal? TotalDaysAttended { get; set; }

        /// <summary>
        /// Gets or sets the makeup days.
        /// </summary>
        public decimal? MakeupDays { get; set; }

        /// <summary>
        /// Gets or sets the GPA method.
        /// </summary>
        public string GPAMethod { get; set; }

        /// <summary>
        /// Gets or sets the grades format.
        /// </summary>
        public string GradesFormat { get; set; }

        /// <summary>
        /// Gets or sets the track SAP attendance.
        /// </summary>
        public string TrackSapAttendance { get; set; }

        /// <summary>
        /// Gets or sets the display hours.
        /// </summary>
        public string DisplayHours { get; set; }

        /// <summary>
        /// Gets or sets the majors minors concentrations.
        /// </summary>
        public string MajorsMinorsConcentrations { get; set; }
    }
}
