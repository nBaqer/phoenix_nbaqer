﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SummaryDto.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the SummaryDto type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Wapi.KlassApp
{
    using System;

    /// <summary>
    /// The summary DTO All custom and other grass fields.
    /// </summary>
    public class SummaryDto
    {
        /// <summary>
        /// Gets or sets the SSN.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public string SSN { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the status code description.
        /// </summary>
        public string StatusCodeDescrip { get; set; }

        /// <summary>
        /// Gets or sets the student start date.
        /// </summary>
        public DateTime StudentStartDate { get; set; }

        /// <summary>
        /// Gets or sets the student expected graduation date.
        /// </summary>
        public DateTime StudentExpectedGraduationDate { get; set; }

        /// <summary>
        /// Gets or sets the program version ID (GUID).
        /// </summary>
        public Guid PrgVerId { get; set; }

        /// <summary>
        /// Gets or sets the program version description.
        /// </summary>
        public string PrgVerDescrip { get; set; }

        /// <summary>
        /// Gets or sets the unit type ID.
        /// </summary>
        public Guid UnitTypeId { get; set; }

        /// <summary>
        /// Gets or sets the program version track credits.
        /// </summary>
        public int PrgVersionTrackCredits { get; set; }

        /// <summary>
        /// Gets or sets the unit type description from attribute unit type.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public string UnitTypeDescripFrom_arAttUnitType { get; set; }

        /// <summary>
        /// Gets or sets the student last date attended.
        /// </summary>
        public DateTime StudentLastDateAttended { get; set; }

        /// <summary>
        /// Gets or sets the weekly schedule.
        /// </summary>
        public decimal WeeklySchedule { get; set; }

        /// <summary>
        /// Gets or sets the total days attended.
        /// </summary>
        public decimal TotalDaysAttended { get; set; }

        /// <summary>
        /// Gets or sets the scheduled days.
        /// </summary>
        public decimal ScheduledDays { get; set; }

        /// <summary>
        /// Gets or sets the days absent.
        /// </summary>
        public decimal DaysAbsent { get; set; }

        /// <summary>
        /// Gets or sets the makeup days.
        /// </summary>
        public decimal MakeupDays { get; set; }

        /// <summary>
        /// Gets or sets the student enroll ID.
        /// </summary>
        public Guid StuEnrollId { get; set; }

        /// <summary>
        /// Gets or sets the credits earned.
        /// </summary>
        public decimal CreditsEarned { get; set; }

        /// <summary>
        /// Gets or sets Credit Attempted
        /// </summary>
        public decimal CreditsAttempted { get; set; }

        /// <summary>
        /// Gets or sets the FA credits earned.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public decimal FACreditsEarned { get; set; }

        /// <summary>
        /// Gets or sets the date determined.
        /// </summary>
        public DateTime DateDetermined { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether in school.
        /// </summary>
        public int InSchool { get; set; }

        /// <summary>
        /// Gets or sets the program description.
        /// </summary>
        public string ProgDescrip { get; set; }

        /// <summary>
        /// Gets or sets the total cost.
        /// </summary>
        public decimal TotalCost { get; set; }

        /// <summary>
        /// Gets or sets the current balance.
        /// </summary>
        public decimal CurrentBalance { get; set; }

        /// <summary>
        /// Gets or sets the weighted average cumulative GPA.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public decimal WeightedAverage_CumGPA { get; set; }

        /// <summary>
        /// Gets or sets the simple average cumulative GPA.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public decimal SimpleAverage_CumGPA { get; set; }

        /// <summary>
        /// Gets or sets the contracted grad date.
        /// </summary>
        public DateTime ContractedGradDate { get; set; }

        /// <summary>
        /// Gets or sets the student groups.
        /// </summary>
        public string StudentGroups { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether clock hour program.
        /// </summary>
        public string ClockHourProgram { get; set; }

        /// <summary>
        /// Gets or sets the unit type description.
        /// </summary>
        public string UnitTypeDescrip { get; set; }

        /// <summary>
        /// Gets or sets the transfer hours.
        /// </summary>
        public decimal TransferHours { get; set; }

        /// <summary>
        /// Gets or sets the overall average.
        /// </summary>
        public decimal OverallAverage { get; set; }
    }
}
