﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigDto.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the ConfigDto type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Wapi.KlassApp
{
    /// <summary>
    /// The configuration DTO.
    /// </summary>
    public class ConfigDto
    {
        /// <summary>
        /// Gets or sets the school name.
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// Gets or sets the display attendance unit for progress report by class.
        /// </summary>
        public string DisplayAttendanceUnitForProgressReportByClass { get; set; }

        /// <summary>
        /// Gets or sets the student identifier.
        /// </summary>
        public string StudentIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the GPA method.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public string GPAMethod { get; set; }

        /// <summary>
        /// Gets or sets the grades format.
        /// </summary>
        public string GradesFormat { get; set; }

        /// <summary>
        /// Gets or sets the track sap attendance.
        /// </summary>
        public string TrackSapAttendance { get; set; }

        /// <summary>
        /// Gets or sets the grade book weighting level.
        /// </summary>
        public string GradeBookWeightingLevel { get; set; }
    }
}
