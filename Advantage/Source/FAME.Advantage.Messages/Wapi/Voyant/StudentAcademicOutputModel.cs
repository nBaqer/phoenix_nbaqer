﻿using System;

namespace FAME.Advantage.Messages.Wapi.Voyant
{
    public class StudentAcademicOutputModel
    {
        public static StudentAcademicOutputModel Factory()
        {
            var fac = new StudentAcademicOutputModel {Gpa = "0"};
            return fac;
        }
        
        public string Code { get; set; }
        public string Descrip { get; set; }
        public DateTime EndDate { get; set; }
        public string Gpa { get; set; }
        public string Grade { get; set; }
        public string InstructorId { get; set; }
        public DateTime StartDate { get; set; }
    }
}
