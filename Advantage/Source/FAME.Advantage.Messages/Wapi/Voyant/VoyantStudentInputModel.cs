﻿namespace FAME.Advantage.Messages.Wapi.Voyant
{
    public class VoyantStudentInputModel
    {
        /// <summary>
        /// 1: Request historical data: Get graduated, dropped out, withdraw, transferred or probation
        /// 0: Student currently attended.
        /// </summary>
        public int RequestHistorical { get; set; }
        public string DashboardUrl { get; set; }
    }
}
