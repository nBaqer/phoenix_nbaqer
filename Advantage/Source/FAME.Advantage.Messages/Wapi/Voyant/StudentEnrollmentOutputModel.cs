﻿using System;

namespace FAME.Advantage.Messages.Wapi.Voyant
{
    public class StudentEnrollmentOutputModel
    {

        public static StudentEnrollmentOutputModel Factory()
        {
            var fac = new StudentEnrollmentOutputModel();

            return fac;
        }

        public string CampusId { get; set; }
        public DateTime EnrollDate { get; set; }
        public DateTime ModDate { get; set; }
        public string ProgDescrip { get; set; }
        public DateTime StartDate { get; set; }
        public string StatusCode { get; set; }
        public string StuEnrollId { get; set; }
    }
}
