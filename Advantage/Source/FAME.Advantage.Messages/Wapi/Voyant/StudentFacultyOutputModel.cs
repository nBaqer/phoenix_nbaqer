﻿namespace FAME.Advantage.Messages.Wapi.Voyant
{
    public class StudentFacultyOutputModel
    {
        public static StudentFacultyOutputModel Factory()
        {
            var fac = new StudentFacultyOutputModel();
            fac.GrossAmount = "0";
            return fac;
        }
        
        public string FundSourceCode { get; set;}
        public string  GrossAmount { get; set; }
    }
   
}
