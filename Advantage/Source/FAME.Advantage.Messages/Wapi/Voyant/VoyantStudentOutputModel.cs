﻿using System.Collections.Generic;

namespace FAME.Advantage.Messages.Wapi.Voyant
{
    public class VoyantStudentOutputModel
    {
        public static VoyantStudentOutputModel Factory()
        {
            var fac = new VoyantStudentOutputModel
            {
                AcademicsDataList = new List<StudentAcademicOutputModel>(),
                EnrollmentDataList = new List<StudentEnrollmentOutputModel>(),
                FacultyDataList = new List<StudentFacultyOutputModel>()
            };

            return fac;
        }
        
        
        public string CitizenGuid { get; set; }
        public string FamilyIncoming { get; set; }
        public string FirstName { get; set; }
        public string Gender { get; set; }
        public string LastName { get; set; }
        public string MaritalStatus { get; set; }
        public string Race { get; set; }
        public string StudentId { get; set; }
        public IList<StudentAcademicOutputModel> AcademicsDataList { get; set; }
        public IList<StudentEnrollmentOutputModel> EnrollmentDataList { get; set; }
        public IList<StudentFacultyOutputModel> FacultyDataList { get; set; } 

    }
}
