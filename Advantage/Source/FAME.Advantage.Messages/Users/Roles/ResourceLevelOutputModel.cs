﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;

namespace FAME.Advantage.Messages.Users.Roles
{
    public class ResourceLevelOutputModel
    {
        public Guid RoleId { get; protected set; }
        public long? AccessLevel { get; protected set; }
        public long ResourceId { get; set; }
        public DateTime? ModDate { get; set; }
        public string ModUser { get; set; }
        public int? ParentId { get; set; }
        public ResourceOutputModel Resource { get; set; }
    }
}
