﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FAME.Advantage.Messages.Users.Roles
{
    public class ResourceOutputModel
    {
        public long Id { get; protected set; }
        public  string Resource { get; protected set; }
        public  string ResourceUrl { get; protected set; }
        public  string DisplayName { get; protected set; }
        public  long AccessLevel { get; protected set; }
        public  DateTime? ModDate { get; set; }
        public  string ModUser { get; set; }

        public virtual IList<ResourceLevelOutputModel> ResourceLevels { get; set; }
    }
}
