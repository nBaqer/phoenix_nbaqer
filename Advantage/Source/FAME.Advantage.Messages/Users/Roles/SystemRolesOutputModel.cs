﻿using FAME.Advantage.Messages.StatusOptions;
using System;

namespace FAME.Advantage.Messages.Users.Roles
{
    public class SystemRolesOutputModel
    {
        public  string Description { get; protected set; }
        public  StatusOptionOutputModel Status { get; set; }
        public  DateTime? ModDate { get; set; }
        public  string ModUser { get; set; }

    }
}
    