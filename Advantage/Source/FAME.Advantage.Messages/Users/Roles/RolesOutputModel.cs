﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Messages.StatusOptions;

namespace FAME.Advantage.Messages.Users.Roles
{
    public class RolesOutputModel
    {
        public Guid Id { get; set; }
        public string Role { get; set; }
        public string Code { get; set; }
        public StatusOptionOutputModel Status { get; set; }
        public DateTime? ModDate { get; set; }
        public string ModUser { get; set; }
        public SystemRolesOutputModel SystemRole { get; set; }
        public IList<ResourceLevelOutputModel> ResourceLevels { get;  set; }
    }
}




        