﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FAME.Advantage.Messages.CampusGroups;

namespace FAME.Advantage.Messages.Users.Roles
{
    public class UserRolesCampusGroupBridgeOutputModel
    {
        public UserOutputModel User { get; set; }
        public RolesOutputModel Role { get; set; }
        public CampusGroupOutputModel CampusGroup { get; set; }
        public virtual DateTime? ModDate { get; set; }
        public virtual string ModUser { get; set; }
    }
}
