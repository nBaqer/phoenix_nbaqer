﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FAME.Advantage.Messages.Users.Roles
{
    public class GetRolesInputModel
    {
        public Guid CampusId { get; set; }
        public int? ResourceId { get; set; }
    }
}
    