﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FAME.Advantage.Messages.CampusGroups;

namespace FAME.Advantage.Messages.Users
{
    public class UserOutputModel
    {
        public Guid Id { get; set; }
        public  string UserName { get;  set; }
        public  string Email { get; protected set; }
        public  IList<CampusGroupOutputModel> CampusGroup { get; protected set; } // Many to many with CampGrps
        public bool? AccountActive { get; protected set; }
        public string UserAndName { get; set; }
        public string FullName { get; set; }
        public int? ActiveLeadCount { get; set; }
    }
}
