﻿using System;

namespace FAME.Advantage.Messages.Users
{
    public class GetUsersInputModel
    {
        public Guid CampusId { get; set; }
        public Guid UserId { get; set; }
        public bool? Active { get; set; }
        public bool? ReturnSupport { get; set; }
        public bool? ReturnReps { get; set; }
    }
}