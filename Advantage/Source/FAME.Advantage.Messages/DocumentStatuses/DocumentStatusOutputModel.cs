﻿using System;

namespace FAME.Advantage.Messages.DocumentStatuses
{
    public class DocumentStatusOutputModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsApproved { get; set; }
    }
}
