﻿using System;

namespace FAME.Advantage.Messages.Campuses
{
    /// <summary>
    /// Class DTO for Campus
    /// </summary>
    public class CampusOutputModel
    {
        /// <summary>
        /// Id Campus
        /// </summary>
        public Guid ID { get; set; }
        
        /// <summary>
        /// Campus Code
        /// </summary>
        public string Code { get; set; }
        
        /// <summary>
        /// Campus Description
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// Campus Address 1
        /// </summary>
        public string Address1 { get; set; }
        
        /// <summary>
        /// Campus Address 2
        /// </summary>
        public string Address2 { get; set; }
        
        /// <summary>
        /// Campus City
        /// </summary>
        public string City { get; set; }
        
        /// <summary>
        /// Campus Id of US State
        /// </summary>
        public Guid StateId { get; set; }
        
        /// <summary>
        /// ZIP Code of the campus
        /// </summary>
        public string Zip { get; set; }
    }
}
    