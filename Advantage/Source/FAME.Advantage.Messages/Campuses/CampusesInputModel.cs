﻿using System;

namespace FAME.Advantage.Messages.Campuses
{
    public class CampusesInputModel
    {
        public Guid CampusGrpId { get; set; }
        public bool IncludeAll { get; set; }
        public Guid UserId { get; set; }
        

        public bool IsSet
        {
            get
            {
                return CampusGrpId != Guid.Empty;
            }
        }
    }
}
