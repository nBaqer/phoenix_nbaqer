﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Messages.Campuses
{
    public class CampusWithLeadsOutputModel
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public int ActiveLeadCount { get; set; }
    }
}
