﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FAME.Advantage.Messages.CampusGroups
{
    public class CampusGroupOutputModel
    {
        public string CampusGrpCode { get; set; }
        public string CampusGrpDescription { get; set; }
        public Guid StatusId { get; set; }
        public bool? IsAllCampusGrp { get; set; }
        public Guid? CampusId { get; set; }



    }
}


