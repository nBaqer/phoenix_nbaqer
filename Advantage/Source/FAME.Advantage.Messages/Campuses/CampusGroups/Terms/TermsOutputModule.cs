﻿using System;

namespace FAME.Advantage.Messages.Campuses.CampusGroups.Terms
{
    public class TermsOutputModel
    {
        public Guid Id { get; set; }
        public string TermCode { get; set; }
        public string TermDescription{ get; set; }
    }
}



  