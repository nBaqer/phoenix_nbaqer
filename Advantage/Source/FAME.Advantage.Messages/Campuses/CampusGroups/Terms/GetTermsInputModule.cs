﻿using System;

namespace FAME.Advantage.Messages.Campuses.CampusGroups.Terms
{
    public class GetTermsInputModel
    {
        public string StatusCode{ get; set; }
        public Guid StuEnrollmentId { get; set; } 
    }
}
