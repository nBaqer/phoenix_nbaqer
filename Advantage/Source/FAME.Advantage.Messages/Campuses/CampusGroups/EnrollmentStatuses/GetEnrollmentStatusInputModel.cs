﻿namespace FAME.Advantage.Messages.Campuses.CampusGroups.EnrollmentStatuses
{
    public class GetEnrollmentStatusInputModel
    {
        public string StatusCode { get; set; }
        public int? StatusLevelId { get; set; }
    }
}
