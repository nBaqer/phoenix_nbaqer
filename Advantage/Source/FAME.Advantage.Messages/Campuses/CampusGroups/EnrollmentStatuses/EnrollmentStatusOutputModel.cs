﻿using System;
using FAME.Advantage.Messages.CampusGroups;
using FAME.Advantage.Messages.StatusOptions;
using FAME.Advantage.Messages.SystemStuff.SystemStatuses;

namespace FAME.Advantage.Messages.Campuses.CampusGroups.EnrollmentStatuses
{
    public class EnrollmentStatusOutputModel
    {
        public Guid Id { get; set; }
        public  string StatusCode { get; protected set; }
        public  string Description { get; protected set; }
        public Guid CampGrpId { get; protected set; }
        public int? SystemStatusId { get; protected set; }
        public  bool? IsAcademicProbation { get; protected set; }
        public  bool? DiscloseProbation { get; protected set; }
        public  bool? IsDefaultLeadStatus { get; protected set; }

        public  StatusOptionOutputModel Status { get; protected set; }
        public  CampusGroupOutputModel CampusGroup { get; protected set; }
        public SystemStatusOutputModel SystemStatus { get; protected set; }
                
    }
}
