﻿using System;
using FAME.Advantage.Messages.CampusGroups;
using FAME.Advantage.Messages.StatusOptions;

namespace FAME.Advantage.Messages.Campuses.CampusGroups.ProgramVersions
{
    public class ProgramVersionOutputModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Guid CampGrpId { get; set; }
        public StatusOptionOutputModel Status { get; set; }
        public CampusGroupOutputModel CampusGroup { get; set; }  
    }
}
