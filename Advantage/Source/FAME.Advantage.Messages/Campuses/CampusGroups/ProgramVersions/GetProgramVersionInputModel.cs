﻿
namespace FAME.Advantage.Messages.Campuses.CampusGroups.ProgramVersions
{
    public class GetProgramVersionInputModel
    {        
        public string StatusCode { get; set; }   
    }
}
