﻿namespace FAME.Advantage.Messages.MultiTenant
{
    public class CredentialsModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
