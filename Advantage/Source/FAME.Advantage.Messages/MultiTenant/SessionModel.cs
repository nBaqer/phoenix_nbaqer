﻿using System;

namespace FAME.Advantage.Messages.MultiTenant
{
    public class SessionModel
    {
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
        public bool IsPasswordChangeRequired { get; set; }
        public bool IsSupportUser { get; set; }
    }
}
