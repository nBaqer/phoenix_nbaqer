﻿
namespace FAME.Advantage.Messages.Session
{
    public class CreateSessionInputModel
    {
        public string User { get; set; }
        public string AuthenticationKey { get; set; }
    }
}
