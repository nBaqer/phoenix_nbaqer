﻿using System;
using FAME.Advantage.Messages.StatusOptions;
using FAME.Advantage.Messages.CampusGroups;

namespace FAME.Advantage.Messages.StudentGroups
{
    public class StudentGroupOutputModel
    {
        public Guid Id { get; set; }

        public string Description { get; set; }
        public string Code { get; set; }
        public bool UseForScheduling { get; set; }
        public Guid CampGrpId { get; set; }

        public StatusOptionOutputModel Status { get; protected set; }
        public CampusGroupOutputModel CampusGroup { get; protected set; }
    }
}
