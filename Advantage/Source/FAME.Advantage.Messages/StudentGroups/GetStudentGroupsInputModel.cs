﻿using System;

namespace FAME.Advantage.Messages.StudentGroups
{
    public class GetStudentGroupsInputModel
    {
        public string StatusCode { get; set; }
        public Guid CampusId { get; set; }
    }
}
