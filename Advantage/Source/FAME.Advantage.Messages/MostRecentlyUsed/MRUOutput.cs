﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Messages.MostRecentlyUsed
{
    public class MruOutput
    {
        public Guid EntityId { get; set; }
        public DateTime? ModDate { get; set; }
    }
}
