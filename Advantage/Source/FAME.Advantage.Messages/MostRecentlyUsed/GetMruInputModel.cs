﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Messages.MostRecentlyUsed
{
    public class GetMruInputModel
    {
        public Guid UserId { get; set; }
        public Guid CampusId { get; set; }
    }
}
