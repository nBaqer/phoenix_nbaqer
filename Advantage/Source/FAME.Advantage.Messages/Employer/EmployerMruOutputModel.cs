﻿using System;

namespace FAME.Advantage.Messages.Employer
{
    public class EmployerMruOutputModel
    {
        public Guid Id { get;  set; }
        public string EmployerName { get;  set; }
        public string Address { get;  set; }//Address1
        
        public string City { get;  set; }//City
        public string State { get;  set; }//State
        public string Zip { get; set; }// Zip
        public string  CityStateZip { get; set; }
        public string Phone { get;  set; }//Phone
        public string ImageUrl { get;  set; }
        public string NotFoundImageUrl { get;  set; }
        public Guid? SearchCampusId { get;  set; }
        public DateTime? ModDate { get; set; }
        public string MruModDate { get; set; }
    }
}