﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmployerOutputModel.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the EmployerSearchOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Employer
{
    using System;

    /// <summary>
    /// The employer search output model.
    /// </summary>
    public class EmployerSearchOutputModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; protected set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        public string FullName { get; protected set; }

        /// <summary>
        /// Gets or sets the field 1.
        /// </summary>
        public string Field1 { get; protected set; } // Address1

        /// <summary>
        /// Gets or sets the field 2.
        /// </summary>
        public string Field2 { 
            get { return ""; }
            set { value = ""; }
        }//nothing 

        /// <summary>
        /// Gets or sets the field 3.
        /// </summary>
        public string Field3 { get; protected set; } // City

        /// <summary>
        /// Gets or sets the field 4.
        /// </summary>
        public string Field4 { get; protected set; } // State Zip

        /// <summary>
        /// Gets or sets the field 5.
        /// </summary>
        public string Field5 { get; protected set; } // Phone

        /// <summary>
        /// Gets or sets the image url.
        /// </summary>
        public string ImageUrl { get; protected set; }

        /// <summary>
        /// Gets or sets the image 64.
        /// </summary>
        public string Image64 { get; protected set; }

        /// <summary>
        /// Gets or sets the not found image url.
        /// </summary>
        public string NotFoundImageUrl { get; protected set; }

        /// <summary>
        /// Gets or sets the search campus string list.
        /// </summary>
        public string SearchCampusStringList { get; protected set; }
    }
}