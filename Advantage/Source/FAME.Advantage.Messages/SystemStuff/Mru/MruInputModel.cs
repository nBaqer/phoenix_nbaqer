﻿namespace FAME.Advantage.Messages.SystemStuff.Mru
{
    /// <summary>
    /// MRU Input Model
    /// </summary>
    public class MruInputModel
    {
        /// <summary>
        /// entity to enter in the MRU list
        /// </summary>
        public string EntityId { get; set; }

        /// <summary>
        /// Type of entity
        /// </summary>
        public int TypeEntity { get; set; }

        /// <summary>
        /// User that enter the entity
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// The entity Campus ID.
        /// </summary>
        public string CampusId { get; set; }
    }
    
}
