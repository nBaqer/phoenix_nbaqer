﻿namespace FAME.Advantage.Messages.SystemStuff.Reports
{
    /// <summary>
    /// Input command for the Service
    /// </summary>
    public class ReportInputModel
    {
        /// <summary>
        /// Command to service
        /// For now 1: Get list of report
        /// </summary>
        public int Command { get; set; }

        /// <summary>
        /// Send the directory to begin the exploration
        /// Can also have the command incorporate.
        /// </summary>
        public string Directory { get; set; }
    }
}
