﻿using System.Collections.Generic;

namespace FAME.Advantage.Messages.SystemStuff.Reports
{
    /// <summary>
    /// Implementation of IReport Output Model
    /// </summary>
    public class ReportOutputModel : IReportOutputModel
    {
        /// <summary>
        /// Class Factory
        /// </summary>
        /// <returns></returns>
        public static IReportOutputModel Factory()
        {
            IReportOutputModel output = new ReportOutputModel();
            output.ReportInfo = new List<ReportInformation>();
            output.Filter = new ReportInputModel();
            return output;
        }       
        
        #region Implementation of IReportOutputModel

        /// <summary>
        /// List of report that are in the SSRS
        /// </summary>
        public IList<ReportInformation> ReportInfo { get; set; }

        /// <summary>
        /// Report Server name
        /// </summary>
        public string ReportServerName { get; set; }

        /// <summary>
        /// Html Code of the page
        /// </summary>
        public string SsrsHtmlPage { get; set; }

        /// <summary>
        /// The input parameters of the report service
        /// </summary>
        public ReportInputModel Filter { get; set; }

        #endregion

 
    }
}
