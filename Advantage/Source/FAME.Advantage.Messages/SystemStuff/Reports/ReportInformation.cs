﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReportInformation.cs" company="Fame">
//   2017
// </copyright>
// <summary>
//   Get the information about the address of the report in the server report
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Reports
{
    using System.Collections.Generic;

    /// <summary>
    /// Get the information about the address of the report in the server report
    /// </summary>
    public class ReportInformation
    {
        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets Display name of the report
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Address of the report in 
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets parent ID
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether hidden the  report.
        /// Useful to hidden sub-reports
        /// </summary>
        public bool Hidden { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// Gets or sets Optional string parameters separated by comma
        /// </summary>
        public IList<ReportInformation> ChildrenList { get; set; }
    }
}
