﻿using System.Collections.Generic;

namespace FAME.Advantage.Messages.SystemStuff.Reports
{
    /// <summary>
    /// The output model for report
    /// </summary>
    public interface IReportOutputModel
    {
        /// <summary>
        /// Gets or sets List of report that are in the SSRS
        /// </summary>
        IList<ReportInformation> ReportInfo { get; set; }

        /// <summary>
        /// Gets or sets Report Server name
        /// </summary>
        string ReportServerName { get; set; }

        /// <summary>
        /// Gets or sets HTML Code of the page
        /// </summary>
        string SsrsHtmlPage { get; set; }

        /// <summary>
        /// Gets or sets The input parameters of the report service
        /// </summary>
        ReportInputModel Filter { get; set; }

    }
}
