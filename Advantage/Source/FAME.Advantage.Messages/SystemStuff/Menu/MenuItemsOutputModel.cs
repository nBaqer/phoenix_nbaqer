﻿using System.Collections.Generic;

namespace FAME.Advantage.Messages.SystemStuff.Menu
{
    public class MenuItemsOutputModel
    {
        public int Id { get; set; }

        public string text { get; set; }
        public string url { get; set; }
        public string IsEnabled { get; set; }
        public IList<MenuItemsOutputModel> items { get; set; }
    
    }
}
