﻿using System;

namespace FAME.Advantage.Messages.SystemStuff.Menu
{
    public class GetMenuItemInputModel
    {
        public Guid CampusId { get; set; }
        public Guid UserId { get; set; }
        public string MenuWithLinks { get; set; }
        public string PageName { get; set; }
        public bool? IsImpersonating { get; set; }
    }
}
