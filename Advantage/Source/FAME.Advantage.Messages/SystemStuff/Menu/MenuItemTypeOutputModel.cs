﻿namespace FAME.Advantage.Messages.SystemStuff.Menu
{
    public class MenuItemTypeOutputModel
    {
        public int Id { get; set; }
        public string ItemType { get; set; }
    }
}
