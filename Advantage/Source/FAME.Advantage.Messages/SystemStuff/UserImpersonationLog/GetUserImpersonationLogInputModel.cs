﻿using System;

namespace FAME.Advantage.Messages.SystemStuff.UserImpersonationLog
{
    public class GetUserImpersonationLogInputModel
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}


