﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Messages.SystemStuff.UserImpersonationLog
{
    public class PutUserImpersonationLogInputModel
    {
        public string LogStart { get; set; }
        public string LogEnd { get; set; }
        public string ImpersonatedUser { get; set; }
    }
}
