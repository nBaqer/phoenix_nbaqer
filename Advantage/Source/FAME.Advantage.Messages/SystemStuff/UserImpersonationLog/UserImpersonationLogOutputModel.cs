﻿using System;

namespace FAME.Advantage.Messages.SystemStuff.UserImpersonationLog
{
    public class UserImpersonationLogOutputModel
    {
        public int Id { get; protected set; }
        public string ImpersonatedUser { get; protected set; }
        public DateTime? LogStart { get; protected set; }
        public DateTime? LogEnd { get; protected set; }
        public string ModUser { get; protected set; }
        public string LStart { get; protected set; }
        public string LEnd { get; protected set; }
        

    }
}
