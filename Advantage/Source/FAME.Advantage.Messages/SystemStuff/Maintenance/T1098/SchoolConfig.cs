﻿using System.Configuration;

namespace FAME.Advantage.Messages.SystemStuff.Maintenance.T1098
{
    public class SchoolConfig
    {
        ///// <summary>
        ///// Use this to initialize the class.
        ///// </summary>
        ///// <returns></returns>
        //public static SchoolConfig Factory()
        //{
        //    var schoolInfo = ConfigurationManager.AppSettings["SCHOOLCODE_KISS_SCHOOLID"];
            
        //    // The value exists the information is stored in schoolInfo
        //    char[] sep = { '_' };
        //    string[] values = schoolInfo.Split(sep);
        //    SchoolConfig output = new SchoolConfig
        //    {
        //        School = values[0],
        //        KissKey = values[1],
        //        SchoolId = values[2]
        //    };

        //    return output;
        //}

        public static SchoolConfig Factory(string token, string schoolInfo)
        {
            // The value exists the information is stored in schoolInfo
            char[] sep = { '_' };
            string[] values = schoolInfo.Split(sep);
            SchoolConfig output = new SchoolConfig
            {
                School = values[0],
                KissKey = values[1],
                SchoolId = values[2],
                CampusToken = token
            };



            return output;
        }

        public string CampusToken { get; set; }

        /// <summary>
        /// School  School_xxxx_xxxxxx
        /// </summary>
        public string School { get; set; }

        /// <summary>
        /// School KissKey xxxxx_KissKey_xxxxxxx
        /// </summary>
        public string KissKey { get; set; }

        /// <summary>
        /// School Id Code xxxx_xxxx_SchoolIdCode
        /// </summary>
        public string SchoolId { get; set; }

    }
}
