﻿namespace FAME.Advantage.Messages.SystemStuff.Maintenance.T1098.Tables
{
    /// <summary>
    /// Exclusion DTO to convert to JET 4.0 table
    /// </summary>
    public class ExclusionsDto
    {
        public string StuAcctNum { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SocSecNum { get; set; }
        public string Addr1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public int ExclusionCode { get; set; }
    }
}
