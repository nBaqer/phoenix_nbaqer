﻿using System;

namespace FAME.Advantage.Messages.SystemStuff.Maintenance.T1098.Tables
{
    /// <summary>
    /// DTO Archive Table to be exported to JET 4.0
    /// </summary>
    public class ArchiveDto
    {
        public string SchoolId { get; set; }
        public string SchoolName { get; set; }
        public string StuEnrollId { get; set; }
        public DateTime ExtractedDate { get; set; }
        public string TaxYear { get; set; }
    }
}
