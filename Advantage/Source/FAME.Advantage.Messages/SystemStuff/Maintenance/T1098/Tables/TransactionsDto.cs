﻿using System;

namespace FAME.Advantage.Messages.SystemStuff.Maintenance.T1098.Tables
{
    /// <summary>
    /// Transaction DTO to be use to conversion to JET 4.0 tables
    /// </summary>
    public class TransactionsDto
    {
        public string StuAcctNum { get; set; }
        public DateTime TransDate { get; set; }
        public int TransAmt { get; set; }
        public string TransCode { get; set; }
        public float IRSDataLine { get; set; }
        public string PayType { get; set; }
        public string Comment { get; set; }
    }
}