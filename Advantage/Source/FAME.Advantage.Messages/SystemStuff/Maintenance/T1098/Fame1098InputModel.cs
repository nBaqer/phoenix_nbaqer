﻿using System.Collections.Generic;
using FAME.Advantage.Messages.SystemStuff.Maintenance.T1098.Tables;

namespace FAME.Advantage.Messages.SystemStuff.Maintenance.T1098
{
    /// <summary>
    /// Used to send to FAME the 1098T information
    /// </summary>
    public class Fame1098InputModel
    {
        /// <summary>
        /// Use this to initialize the class
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public static Fame1098InputModel Factory(string year)
        {
            var output = new Fame1098InputModel
            {
                ArchiveList = new List<ArchiveDto>(),
                ExclusionsList = new List<ExclusionsDto>(),
                TransactionsList = new List<TransactionsDto>(),
                ValidEnrollmentsList = new List<ValidEnrollmentsDto>(),
                Year = year
            };
            return output;
        }
        
        public IList<ArchiveDto> ArchiveList { get; set; }
        public IList<ExclusionsDto> ExclusionsList { get; set; }
        public IList<TransactionsDto> TransactionsList { get; set; }
        public IList<ValidEnrollmentsDto> ValidEnrollmentsList { get; set; }
 
        public string Year { get; set; }
    }
}
