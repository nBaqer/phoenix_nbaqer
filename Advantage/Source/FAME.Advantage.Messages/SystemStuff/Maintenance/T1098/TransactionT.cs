﻿using System;

namespace FAME.Advantage.Messages.SystemStuff.Maintenance.T1098
{
    /// <summary>
    /// Get the table with the information of the transaction used
    /// </summary>
    public class TransactionT
    {
        public DateTime TransDate { get; set; }
        public decimal TransAmount { get; set; }
        public string TransCodeCode { get; set; }
        public string EnrollmentId { get; set; }
        public int? PayType { get; set; }
    }
}
