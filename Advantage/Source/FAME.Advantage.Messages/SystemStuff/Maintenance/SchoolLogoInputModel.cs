﻿namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    public class SchoolLogoInputModel
    {
        /// <summary>
        /// Specified this if you want a specific logo.
        /// </summary>
        public int? ImageId { get; set; }

        /// <summary>
        /// Specified this if you want to filter by image code.
        /// </summary>
        public string ImageCode { get; set; }

    }
}
