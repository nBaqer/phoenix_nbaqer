﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="T1098InputModel.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Input filter for 1098T Controller
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    /// <summary>
    /// Input filter for 1098T Controller
    /// </summary>
    public class T1098InputModel
    {
        /// <summary>
        /// Gets or sets User ID
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///  Gets or sets Campus actual
        /// </summary>
        public string CampusId { get; set; }

        /// <summary>
        ///  Gets or sets User the year to collect the 1098T information
        /// </summary>
        public int Year { get; set; }
    }
}
