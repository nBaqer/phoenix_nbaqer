﻿using System;

namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    /// <summary>
    /// This model accept a integer Id and a Value type DateTime.
    /// </summary>
    public class WapiSetDateTimeInputModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
