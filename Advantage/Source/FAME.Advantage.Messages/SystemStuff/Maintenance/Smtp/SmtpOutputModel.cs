﻿using System.Collections.Generic;
using System.Web;

namespace FAME.Advantage.Messages.SystemStuff.Maintenance.Smtp
{
    /// <summary>
    /// The output model to SMTP Service
    /// </summary>
    public class SmtpOutputModel : ISmtpOutputModel
    {
        /// <summary>
        /// Factory class. Use this to get the object
        /// </summary>
        /// <returns></returns>
        public static ISmtpOutputModel Factory()
        {
            ISmtpOutputModel model = new SmtpOutputModel();
            model.Filter = new SmtpInputModel();
            model.Settings = new SmtpConfigurationSettings();
            return model;
        }
        
        #region Implementation of ISmtpOutputModel

        /// <summary>
        /// The SMTP Settings values
        /// </summary>
        public SmtpConfigurationSettings Settings { get; set; }

        /// <summary>
        /// Used to configure the POST operations
        /// </summary>
        public SmtpInputModel Filter { get; set; }

        /// <summary>
        /// Used to returned information from server
        /// </summary>
        public IEnumerable<HttpPostedFileBase> Files { get; set; }

        #endregion
    }
}
