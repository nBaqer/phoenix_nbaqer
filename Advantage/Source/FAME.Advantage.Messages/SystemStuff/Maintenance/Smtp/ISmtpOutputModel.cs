﻿using System.Collections.Generic;
using System.Web;

namespace FAME.Advantage.Messages.SystemStuff.Maintenance.Smtp
{
    /// <summary>
    ///  DTO for SmtpController
    /// </summary>
    public interface ISmtpOutputModel
    {
        /// <summary>
        /// The SMTP Settings values
        /// </summary>
        SmtpConfigurationSettings Settings { get; set; }

        /// <summary>
        /// Used to configure the POST operations
        /// </summary>
        SmtpInputModel Filter { get; set; }

        /// <summary>
        /// Used to returned information from server
        /// </summary>
        IEnumerable<HttpPostedFileBase> Files { get; set; }
    }
}
