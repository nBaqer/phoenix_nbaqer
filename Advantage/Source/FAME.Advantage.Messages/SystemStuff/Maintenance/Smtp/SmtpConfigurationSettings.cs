﻿namespace FAME.Advantage.Messages.SystemStuff.Maintenance.Smtp
{
    /// <summary>
    /// this class is used to send setting to configuration
    /// page and to update its.
    /// </summary>
    public class SmtpConfigurationSettings
    {
        /// <summary>
        /// The user password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// The user-name (must be a email address)
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// The server to be used as SMTP server
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        /// The Port to be used
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// 1 If the connexion must be in SSL
        /// </summary>
        public int IsSsl { get; set; }

        /// <summary>
        /// 1 If the connexion used OAuth protocol
        /// </summary>
        public int IsOauth { get; set; }

        /// <summary>
        /// email to be post as from mail.
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// A fraction of the client secret.
        /// For security reason no all is show.
        /// </summary>
        public string AutClientSecret { get; set; }

        /// <summary>
        /// Only used in internal operation
        /// the token to send messages to Google
        /// </summary>
        public string Token { get; set; }
    }
}
