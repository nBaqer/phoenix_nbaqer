﻿namespace FAME.Advantage.Messages.SystemStuff.Maintenance.Smtp
{
    public class SmtpSendMessageInputModel
    {
        public string Body64 { get; set; }
        public string Subject { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string From { get; set; }

        /// <summary>
        /// Leave blank for plain text
        /// Use "text/html" to send html aware message
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// 1: Send emails without Attachments
        /// (other options not implemented)
        /// </summary>
        public int Command { get; set; }

    }
}
