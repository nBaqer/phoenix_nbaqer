﻿namespace FAME.Advantage.Messages.SystemStuff.Maintenance.Smtp
{
    /// <summary>
    /// Input filter for SMTP
    /// </summary>
    public class SmtpInputModel
    {
        /// <summary>
        /// 1: Get all Configuration Setting for SMTP
        /// </summary>
        public int Command { get; set; }

        /// <summary>
        /// The user Id 
        /// (used to notify application who modify the setting)
        /// </summary>
        public string UserId { get; set; }
    }
}
