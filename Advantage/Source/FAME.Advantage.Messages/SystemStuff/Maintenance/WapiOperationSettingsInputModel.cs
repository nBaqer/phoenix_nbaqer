﻿namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    /// <summary>
    /// Input filters for WAPI Operation Settings
    /// </summary>
    public class WapiOperationSettingsInputModel
    {
        /// <summary>
        /// Get the specific operation by Id
        /// id = 0 Get All Operations
        /// </summary>
        public int IdOperation { get; set; }

        /// <summary>
        /// Filter if we want to get all services, or 
        /// only the actives.
        /// 0: Get all
        /// 1: Get only actives
        /// </summary>
        public int OnlyActives { get; set; }
        
        /// <summary>
        /// Get all operations of the same value type
        /// string.empty ignore parameter
        /// codeOperationMode: Get only with this operation mode.
        /// </summary>
        public string OperationMode { get; set; }

        /// <summary>
        /// The company code associated with the Operation.
        /// If the company does not match any a empty record is returned.
        /// all = no companies is filtered.. 
        /// </summary>
        /// <remark>This flag is not used</remark>
        public string CompanyCode { get; set; }
    }
    
}
