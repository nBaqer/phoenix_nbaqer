﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SchoolLogoOutputModel.cs" company="FAME">
//  2016 
// </copyright>
// <summary>
//   Output message to send logo information.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    /// <summary>
    /// Output message to send logo information.
    /// </summary>
    public class SchoolLogoOutputModel
    {
        /// <summary>
        /// Gets or sets Unique Id
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets Image in byte array
        /// </summary>
        public byte[] ImagenBytes { get; set; }

        /// <summary>
        /// Gets or sets Image size
        /// </summary>
        public int ImgLenth { get; set; }

        /// <summary>
        /// Gets or sets Type de Image <code>jpg, png, bmp</code>
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// Gets or sets True is it official logo
        /// </summary>
        public bool OfficialUse { get; set; }

        /// <summary>
        /// Gets or sets Code used to identified the image in the application
        /// Use this to reference the image in the application
        /// and do not use the id. 
        /// </summary>
        public string ImageCode { get; set; }

        /// <summary>
        /// Gets or sets Explain in what context you use the image.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets The name of the file image.
        /// </summary>
        public string ImageFile { get; set; }
    }
}
