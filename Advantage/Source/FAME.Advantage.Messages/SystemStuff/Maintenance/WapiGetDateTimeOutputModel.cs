﻿using System;

namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    public class WapiGetDateTimeOutputModel
    {
        public int Id { get; set; }
        public DateTime Value { get; set; }
    }

}
