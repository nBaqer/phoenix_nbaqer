﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiLoggerOutputModel.cs" company="FAME">
//   2015 2017
// </copyright>
// <summary>
//   Defines the WapiLoggerOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    using System;

    /// <summary>
    /// The WAPI logger output model.
    /// </summary>
    public class WapiLoggerOutputModel
    {
        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the service code.
        /// </summary>
        public string ServiceCode { get; set; }

        /// <summary>
        /// Gets or sets the company code.
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// Gets or sets the date execution.
        /// </summary>
        public DateTime DateExecution { get; set; }

        /// <summary>
        /// Gets or sets the next planning date.
        /// </summary>
        public DateTime NextPlanningDate { get; set; }

        /// <summary>
        /// Gets or sets the is error.
        /// </summary>
        public int IsError { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        public string Comment { get; set; }
    }
}
