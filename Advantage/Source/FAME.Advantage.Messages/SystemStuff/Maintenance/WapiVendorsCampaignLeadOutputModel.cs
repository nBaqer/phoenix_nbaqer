﻿using System;
using FAME.Advantage.Messages.Common;

namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    public class WapiVendorsCampaignLeadOutputModel
    {
        /// <summary>
        /// Campaign ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Vendor proprietary of the campaign.
        /// </summary>
        public  int VendorId { get;  set; }

        /// <summary>
        /// The code of this campaign
        /// The campaign code and VendorIbj combination must be unique
        /// </summary>
        public  string CampaignCode { get;  set; }

        /// <summary>
        /// Account Id with the vendor for this campaign
        /// </summary>
        public  string AccountId { get;  set; }

        /// <summary>
        /// True: The Campaign lead service is active on the system
        /// False: The Campaign lead service is inactive. 
        /// They can not send lead info to Advantage
        /// </summary>
        public  int IsActive { get;  set; }
        /// <summary>
        /// Campaign date and time of Init
        /// </summary>
        public  DateTime DateCampaignBegin { get;  set; }

        /// <summary>
        /// Campaign Date an Time to finish.
        /// </summary>
        public  DateTime DateCampaignEnd { get;  set; }

        /// <summary>
        /// Cost of the campaign
        /// </summary>
        public  float Cost { get;  set; }

        /// <summary>
        /// 1: reject leads that are not  from certain counties defined by the school
        /// </summary>
        public  int FilterRejectByCounty { get;  set; }

        /// <summary>
        /// 1: reject the leads that are considered as duplicated.
        /// </summary>
        public  int FilterRejectDuplicates { get;  set; }

        /// <summary>
        /// Unit of Cost of the campaign
        /// </summary>
        public  DropDownIntegerOutputModel PayFor { get;  set; }

    }
}
