﻿using System;
using System.Collections.Generic;

namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    /// <summary>
    /// Info to communicate with FAME
    /// </summary>
    [Serializable]
    public class T1098FameInputModel
    {
        public IList<T1098Dto> ListStudentDto { get; set; }
        public string Year { get; set; }
    }
}
