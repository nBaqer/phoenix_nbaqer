﻿using System;

namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    public class WapiLoggerInputModel
    {
        /// <summary>
        /// Id of the operation
        /// </summary>
        public string WapiOperationCode { get; set; }


        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }

        /// <summary>
        /// If the operation was success or error or all.
        /// ERROR SUCCESS ALL
        /// </summary>
        public string LogType { get; set; }
       
    }
}
