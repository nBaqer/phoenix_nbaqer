﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="T1098Dto.cs" company="FAME">
//  2015-2016 
// </copyright>
// <summary>
// JAGG - DTO for 1098T query
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    using System;

    /// <summary>
    /// DTO for 1098T query
    /// </summary>
    [Serializable]
    public class T1098Dto
    {
        /// <summary>
        /// Gets or sets the student enroll ID.
        /// </summary>
        public Guid StuEnrollId { get; set; }

        /// <summary>
        /// Gets or sets the enrollment ID.
        /// </summary>
        public string EnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// Gets or sets the LDA.
        /// </summary>
        public string LDA { get; set; }

        /// <summary>
        ///  Gets or sets Is Student no Start 
        /// false (0): if the student started
        /// true  (1): if the student not started 
        /// </summary>
        public int IsStudentNoStart { get; set; }

        /// <summary>
        /// Gets or sets the trans date.
        /// </summary>
        public string TransDate { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the SSN.
        /// </summary>
        public string SSN { get; set; }

        /// <summary>
        /// Gets or sets the address 1.
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the zip.
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        /// Gets or sets the total cost.
        /// </summary>
        public decimal TotalCost { get; set; }

        /// <summary>
        /// Gets or sets the total paid.
        /// </summary>
        public decimal TotalPaid { get; set; }

        /// <summary>
        /// Gets or sets the prior year payments.
        /// </summary>
        public decimal PriorYearPayments { get; set; }

        /// <summary>
        /// Gets or sets the prior year refunds.
        /// </summary>
        public decimal PriorYearRefunds { get; set; }

        /// <summary>
        /// Gets or sets the gross payments.
        /// </summary>
        public decimal GrossPayments { get; set; }

        /// <summary>
        /// Gets or sets the gross refunds.
        /// </summary>
        public decimal GrossRefunds { get; set; }

        /// <summary>
        /// Gets or sets the grant refunds.
        /// </summary>
        public decimal GrantRefunds { get; set; }

        /// <summary>
        /// Gets or sets the loan refunds.
        /// </summary>
        public decimal LoanRefunds { get; set; }

        /// <summary>
        /// Gets or sets the student refunds.
        /// </summary>
        public decimal StudentRefunds { get; set; }

        /// <summary>
        /// Gets or sets the grant payments.
        /// </summary>
        public decimal GrantPayments { get; set; }

        /// <summary>
        /// Gets or sets the loan payments.
        /// </summary>
        public decimal LoanPayments { get; set; }

        /// <summary>
        /// Gets or sets the scholarships.
        /// </summary>
        public decimal Scholarships { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is continuing ed.
        /// </summary>
        public bool IsContinuingEd { get; set; }

        /// <summary>
        /// Gets or sets the student payments.
        /// </summary>
        public decimal StudentPayments { get; set; }

        /// <summary>
        /// Gets or sets the cost for enrollment exist.
        /// </summary>
        public int CostForEnrollmentExist { get; set; }

        /// <summary>
        /// Gets or sets the current year institutional charges.
        /// </summary>
        public decimal CurrentYrInstitutionalCharges { get; set; }

        /// <summary>
        /// Gets or sets the prior year institutional charges.
        /// </summary>
        public decimal PriorYrInstitutionalCharges { get; set; }
    }
}
