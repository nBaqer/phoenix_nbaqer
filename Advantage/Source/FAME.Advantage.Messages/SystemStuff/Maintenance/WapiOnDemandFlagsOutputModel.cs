﻿namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    public class WapiOnDemandFlagsOutputModel
    {
        public int Id { get; set; }
        /// <summary>
        /// Flag to indicate that the windows servicesshould initiate a on demand operation.
        /// Advantage can put this variable in 1 and the service should put in 0
        /// to indicate the complexion the on demand operation.
        /// </summary>
        public int OnDemandOperation { get; set; }

        /// <summary>
        /// Only when OnDemandOperation flag return 0
        /// Return the result of on demand operation from log file
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// Only wnen OnDemandOperation flag return 0
        /// return the result of the operation
        /// </summary>
        public bool Error { get; set; }
    }
}
