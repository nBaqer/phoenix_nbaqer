﻿namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    public class WapiWindowsServiceInputModel
    {
        /// <summary>
        /// The name of service
        /// </summary>
        public string ServiceName { get; set; }
        
        /// <summary>
        /// Admited Values
        /// start : Start the service
        /// stop  : Stop the service
        /// init  : Init the service according to WEB.CONFIG
        ///         app setting:
        /// if a different value is used the service 
        /// return without error but does nothing.
        /// </summary>
        public string Command { get; set; }
    }
}
