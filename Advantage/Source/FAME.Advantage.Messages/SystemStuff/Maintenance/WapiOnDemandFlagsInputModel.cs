﻿namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    public class WapiOnDemandFlagsInputModel
    {
        public int Id { get; set; }
        public int Value { get; set; }
    }
   
}
