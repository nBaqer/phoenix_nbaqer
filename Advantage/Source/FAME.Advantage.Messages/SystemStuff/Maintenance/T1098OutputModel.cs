﻿namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    /// <summary>
    /// Number of records processed to FAME
    /// </summary>
    public class T1098OutputModel
    {
        /// <summary>
        /// Number of records processed
        /// </summary>
        public int NumberOfStudentProcessed { get; set; }

        /// <summary>
        /// Generic Message
        /// </summary>
        public string Message { get; set; }
    }
}
