﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Messages.SystemStuff.Maintenance
{
    public class WapiVendorsLeadOutputModel
    {
        /// <summary>
        /// Vendor Id
        /// </summary>
        public int ID { get; set; }
        
        /// <summary>
        /// Vendor Name. More explicative name for Vendor.
        /// </summary>
        public virtual string VendorName { get;  set; }

        /// <summary>
        /// Vendor Code. This should be unique for each vendor
        /// This code must be only a word.
        /// </summary>
        public virtual string VendorCode { get;  set; }

        /// <summary>
        /// True: The Vendor lead service is active on the system
        /// False: The vendor lead service is inactive. 
        /// They can not send lead info to Advantage
        /// </summary>
        public virtual bool IsActive { get;  set; }

        /// <summary>
        /// Description of the Vendor
        /// </summary>
        public virtual string Description { get;  set; }

        /// <summary>
        /// Date and time that begin the relation with the Vendor
        /// </summary>
        public virtual DateTime DateOperationBegin { get;  set; }

        /// <summary>
        /// Propose time to finish the Vendor Relations
        /// </summary>
        public virtual DateTime DateOperationEnd { get;  set; }

    }
}
