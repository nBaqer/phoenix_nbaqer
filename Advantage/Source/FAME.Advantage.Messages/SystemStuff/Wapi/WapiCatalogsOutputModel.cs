﻿namespace FAME.Advantage.Messages.SystemStuff.Wapi
{
    /// <summary>
    /// This class is use to pass the catalog information of wapi database
    /// External Companies, Operation Mode, Allowed Services
    /// </summary>
    public class WapiCatalogsOutputModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
       
        /// <summary>
        /// this is only used by allowed services
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// This is only used by allowed services
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// This is only used by AUTHORIZED
        /// return if the User is authorized for the given Service.
        /// </summary>
        public bool Authorized { get; set; }
    }
}
