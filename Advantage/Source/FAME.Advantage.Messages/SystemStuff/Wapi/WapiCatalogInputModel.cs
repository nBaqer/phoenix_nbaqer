﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiCatalogInputModel.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the WapiCatalogInputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Wapi
{
    using System.Collections.Generic;

    /// <summary>
    /// The WAPI catalog input model.
    /// </summary>
    public class WapiCatalogInputModel
    {
        /// <summary>
        /// Gets or sets Identify the operation to execute
        /// </summary>
        public string CatalogOperation { get; set; }

        /// <summary>
        /// Gets or sets Extra Id used to make relation between
        /// Allow service and Company Number.
        /// </summary>
        public int IdParent { get; set; }

        /// <summary>
        /// Gets or sets the changes.
        /// </summary>
        public IList<WapiCatalogsOutputModel> Changes { get; set; }
    }
}
