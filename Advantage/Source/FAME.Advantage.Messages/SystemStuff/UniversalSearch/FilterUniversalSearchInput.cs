﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FilterUniversalSearchInput.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Messages.SystemStuff.UniversalSearch.FilterUniversalSearchInput
// </copyright>
// <summary>
//   The filter universal search input.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.UniversalSearch
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The filter universal search input.
    /// </summary>
    public class FilterUniversalSearchInput
    {
        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        public Guid LeadId { get; set; }

        /// <summary>
        /// Gets or sets the campuses.
        /// </summary>
        public List<Guid> Campuses { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether show inactive.
        /// </summary>
        public bool ShowInactive { get; set; }
    }
}
