﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationAppSettingValuesOutputModel.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the ConfigurationAppSettingValuesOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.ConfigurationAppSettings
{
    using FAME.Advantage.Messages.Campuses;

    /// <summary>
    /// The configuration application setting values output model.
    /// </summary>
    public class ConfigurationAppSettingValuesOutputModel
    {
        /// <summary>
        /// Gets or sets the setting id.
        /// </summary>
        public int? SettingId { get; set; }

        /// <summary>
        /// Gets or sets the campus.
        /// </summary>
        public CampusOutputModel Campus { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether active.
        /// </summary>
        public bool Active { get; set; }
    }
}
