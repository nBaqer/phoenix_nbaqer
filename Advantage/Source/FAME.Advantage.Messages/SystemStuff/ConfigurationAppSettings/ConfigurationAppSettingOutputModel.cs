﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationAppSettingOutputModel.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the ConfigurationAppSettingOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.ConfigurationAppSettings
{
    using System.Collections.Generic;

    /// <summary>
    /// The configuration application setting output model.
    /// </summary>
    public class ConfigurationAppSettingOutputModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the key name.
        /// </summary>
        public string KeyName { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether campus specific.
        /// </summary>
        public bool CampusSpecific { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether extra confirmation.
        /// </summary>
        public bool ExtraConfirmation { get; set; }

        /// <summary>
        /// Gets or sets the configuration values.
        /// </summary>
        public IList<ConfigurationAppSettingValuesOutputModel> ConfigurationValues { get; set; }
    }
}
