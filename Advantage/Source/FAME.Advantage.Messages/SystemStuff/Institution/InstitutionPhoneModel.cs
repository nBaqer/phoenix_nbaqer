﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionPhoneModel.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Messages.SystemStuff.Institution.InstitutionPhoneModel
// </copyright>
// <summary>
//   The institution phone model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Institution
{
    using System;

    /// <summary>
    /// The institution phone model.
    /// </summary>
    public class InstitutionPhoneModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the institution id.
        /// </summary>
        public Guid InstitutionId { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the type id.
        /// </summary>
        public Guid TypeId { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the status id.
        /// </summary>
        public Guid StatusId { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is foreign phone.
        /// </summary>
        public bool IsForeignPhone { get; set; }
    }
}
