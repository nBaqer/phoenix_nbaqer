﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionOutputModel.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Messages.SystemStuff.Institution.InstitutionOutputModel
// </copyright>
// <summary>
//   The institution output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Institution
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The institution output model.
    /// </summary>
    public class InstitutionOutputModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the level id.
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Gets or sets the import type id.
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// Gets or sets the addresses.
        /// </summary>
        public List<InstitutionAddressModel> Addresses { get; set; }

        /// <summary>
        /// Gets or sets the contacts.
        /// </summary>
        public List<InstitutionContactModel> Contacts { get; set; }

        /// <summary>
        /// Gets or sets the phones.
        /// </summary>
        public List<InstitutionPhoneModel> Phones { get; set; }
    }
}
