﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionAddressModel.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Messages.SystemStuff.Institution.InstitutionAddressModel
// </copyright>
// <summary>
//   The institution address model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Institution
{
    using System;

    /// <summary>
    /// The institution address model.
    /// </summary>
    public class InstitutionAddressModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the institution id.
        /// </summary>
        public Guid InstitutionId { get; set; }

        /// <summary>
        /// Gets or sets the type id.
        /// </summary>
        public Guid TypeId { get; set; }

        /// <summary>
        /// Gets or sets the address 1.
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the address 2.
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state id.
        /// </summary>
        public Guid StateId { get; set; }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets or sets the country id.
        /// </summary>
        public Guid CountryId { get; set; }

        /// <summary>
        /// Gets or sets the status id.
        /// </summary>
        public Guid StatusId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is mailing address.
        /// </summary>
        public bool IsMailingAddress { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is international.
        /// </summary>
        public bool IsInternational { get; set; }

        /// <summary>
        /// Gets or sets the county id.
        /// </summary>
        public Guid CountyId { get; set; }

        /// <summary>
        /// Gets or sets the foreign country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the county.
        /// </summary>
        public string County { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the address APTO.
        /// </summary>
        public string AddressApto { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether is default.
        /// </summary>
        public bool IsDefault { get; set; }
    }
}
