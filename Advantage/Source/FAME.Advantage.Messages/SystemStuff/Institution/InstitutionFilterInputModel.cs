﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionFilterInputModel.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Messages.SystemStuff.Institution.InstitutionFilterInputModel
// </copyright>
// <summary>
//   The institution filter input model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Institution
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.Messages.Common;

    /// <summary>
    /// The institution filter input model.
    /// </summary>
    public class InstitutionFilterInputModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the Level.
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// Gets or sets the Institution Type.
        /// </summary>
        public int InstitutionType { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        public KendoFilter filter { get; set; }
    }
}
