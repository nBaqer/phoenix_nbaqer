﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionInputModel.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Messages.SystemStuff.Institution.InstitutionInputModel
// </copyright>
// <summary>
//   The institution input model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Institution
{
    using System;

    /// <summary>
    /// The institution input model.
    /// </summary>
    public class InstitutionInputModel
    {

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the level id.
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Gets or sets the type id.
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }
    }
}
