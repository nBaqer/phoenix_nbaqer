﻿using FAME.Advantage.Messages.StatusOptions;

namespace FAME.Advantage.Messages.SystemStuff.SystemStatuses
{
    public class SystemStatusOutputModel
    {
        public string Description { get; protected set; }
        public int? StatusLevelId { get; protected set; }
        public bool? IsInSchool { get; protected set; }
        public string GEProgramStatus { get; protected set; }
        
        public StatusOptionOutputModel Status { get; protected set; }
    }
}


