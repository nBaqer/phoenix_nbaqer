﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Messages.SystemStuff.SystemStatuses
{
    public class SystemPhoneTypeOuputModel
    {
        public string ID { get; set; }
        public string Description { get; set; }
    }
}
