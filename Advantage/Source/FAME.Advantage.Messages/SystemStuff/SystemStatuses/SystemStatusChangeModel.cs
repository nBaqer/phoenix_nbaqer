﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemStatusChangeModel.cs" company="FAME Inc.">
// FAME.Advantage.Messages.SystemStuff.SystemStatuses
// </copyright>
// <summary>
//   Defines the SystemStatusChangeFactory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.SystemStatuses
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.Messages.Common;

    /// <summary>
    /// Data Transfer Object for Inputs/Output of System Status Change type
    /// </summary>
    public class SystemStatusChangeModel
    {
        /// <summary>
        /// Gets or sets The System Status Code Id
        /// </summary>
        public Guid StatusCodeId { get; set; }

        /// <summary>
        /// Gets or sets The System Status Id mapped to the System Status Code 
        /// </summary>
        public int StatusId { get; set; }

        /// <summary>
        /// Gets or sets Effective Date is the Date of Change in the DataBase
        /// </summary>
        public string EffectiveDate { get; set; }

        /// <summary>
        /// Gets or sets The Previous Status Code Id
        /// </summary>
        public Guid PreviousStatusCodeId { get; set; }

        /// <summary>
        /// Gets or sets The Previous System Status Id mapped to the Previous System Status Code 
        /// </summary>
        public int PreviousStatusId { get; set; }

        /// <summary>
        /// Gets or sets Status is the Description of New Status in the Database
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets The Description of the Reason if is a LOA or Dropped or the Reason if is a Suspension
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Gets or sets The Modified User
        /// </summary>
        public string PostedBy { get; set; }

        /// <summary>
        /// Gets or sets Date Of Change is the Modified Date in the Database
        /// </summary>
        public string DateOfChange { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether allow insert.
        /// </summary>
        public bool AllowInsert { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether should confirm action.
        /// </summary>
        public bool ShouldConfirmAction { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether allow edit.
        /// </summary>
        public bool AllowEdit { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether allow delete.
        /// </summary>
        public bool AllowDelete { get; set; }

        /// <summary>
        /// Gets or sets the LOA, Suspension, Probation End Date
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// Gets or sets the Student Enrollment Id
        /// </summary>
        public string StudentEnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the Campus Id
        /// </summary>
        public string CampusId { get; set; }

        /// <summary>
        /// Gets or sets the Requested By
        /// </summary>
        public string RequestedBy { get; set; }

        /// <summary>
        /// Gets or sets the Case Number
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the Unique Identifier of the User
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the User Name 
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the enumeration to determinate the type of validation
        /// 1 = Insert
        /// 2 = Edit
        /// 3 = Delete 
        /// </summary>
        public int ValidationMode { get; set; }

        /// <summary>
        /// Gets or sets the List of Status
        /// </summary>
        public List<DropDownOutputModel> StatusList { get; set; }

        /// <summary>
        /// Gets or sets Student Enrollment Start Date
        /// </summary>
        public string StudentEnrollmentStartDate { get; set; }

        /// <summary>
        /// Gets or sets Student Enrollment Graduation Date
        /// </summary>
        public string StudentEnrollmentGraduationDate { get; set; }
        
        /// <summary>
        /// Gets or sets The Id of the Status Change History (required in edit or delete mode)
        /// </summary>
        public Guid? StatusChangeHistoryId { get;set; }

        /// <summary>
        /// Gets or sets a value indicating whether is support tool.
        /// </summary>
        public bool IsSupportTool { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether have attendance posted.
        /// </summary>
        public bool HaveAttendancePosted { get; set; }

        /// <summary>
        /// Gets or sets the student id
        /// </summary>
        public Guid StudentId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether have backup.
        /// </summary>
        public bool HaveBackup { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether have client confirmation.
        /// </summary>
        public bool HaveClientConfirmation { get; set; }
    }
}
