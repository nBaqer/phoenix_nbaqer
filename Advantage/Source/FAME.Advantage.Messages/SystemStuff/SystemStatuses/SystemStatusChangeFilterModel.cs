﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Messages.SystemStuff.SystemStatuses
{
    public class SystemStatusChangeFilterModel
    {
        public string StudentEnrollmentId { get; set; }
        public string CampusId { get; set; }
        public string UserId { get; set; }  
    }
}
