﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Messages.SystemStuff.SystemStatuses
{
    public class SystemStatusCodeModel
    {
        public Guid ID { get; set; }
        public string Description { get; set; }
        public int StatusId { get; set; }
    }
}
