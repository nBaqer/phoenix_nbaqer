﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SdfOutputModel.cs" company="FAME">
//   FAME 2016
// </copyright>
// <summary>
//   The SDF (School Defined Fields ) Output Model
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.SchoolDefinedFields
{
    using System.Collections.Generic;

    /// <summary>
    /// The SDF (School Defined Fields ) Output Model
    /// </summary>
    public class SdfOutputModel
    {
        /// <summary>
        /// Gets or sets custom field Id for the specific School Define Fields
        /// This field exists in the view. Store it in order to be able
        /// to post the change value to database.
        /// </summary>
        public string SdfId { get; set; }

        /// <summary>
        /// Gets or sets The control type
        /// </summary>
        public string ControlType { get; set; }

        /// <summary>
        /// Gets or sets The Description of the control (label)
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets The value assigned
        /// </summary>
        public string AssignedValue { get; set; }

        /// <summary>
        /// Gets or sets If the control is required or not
        /// </summary>
        public string Required { get; set; }

        /// <summary>
        /// Gets or sets List of values (case of combo box or other list controls)
        /// </summary>
        public IList<string> ListOfValues { get; set; }

        /// <summary>
        /// Gets or sets The Action of the control can be two
        /// NEW
        /// LEAD
        /// </summary>
        public string Actions { get; set; }

        /// <summary>
        ///  Gets or sets Size of the controls in characters
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// Gets or sets The number of decimal if the field has a number.
        /// </summary>
        public int Decimals { get; set; }

        /// <summary>
        /// Gets or sets the data type id.
        /// </summary>
        public int DtypeId { get; set; }

        /// <summary>
        /// Gets or sets the range list.
        /// </summary>
        public SdfRangeOutputModel Range { get; set; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        public int Position { get; set; }
    }
}
