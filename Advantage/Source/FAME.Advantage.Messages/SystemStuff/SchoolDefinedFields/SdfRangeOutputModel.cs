﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SdfRangeOutputModel.cs" company="FAME">
//   Fame 2016
// </copyright>
// <summary>
//   Defines the SdfRangeOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.SchoolDefinedFields
{
    /// <summary>
    /// Custom Field Range values
    /// </summary>
    public class SdfRangeOutputModel
    {
        /// <summary>
        /// Gets or sets the school defined id.
        /// </summary>
        public string SdfId { get; set; }

        /// <summary>
        /// Gets or sets the min val.
        /// </summary>
        public string MinVal { get; set; }

        /// <summary>
        /// Gets or sets the max val.
        /// </summary>
        public string MaxVal { get; set; }
    }
}
