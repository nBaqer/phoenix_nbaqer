﻿
namespace FAME.Advantage.Messages.SystemStuff.SystemModules
{
    public class SystemModuleOutputModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
