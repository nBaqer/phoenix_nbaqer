﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UdfDto.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the UdfDtoModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Messages.SystemStuff.Udf
{
    /// <summary>
    /// The UDF DTO
    /// </summary>
    public class UdfDto
    {
        /// <summary>
        /// Gets or sets UDF ID (GUID)
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///  Gets or sets SDF Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///  Gets or sets The position a number from 1 to 12 (0 is not position)
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether SDF visibility.
        /// (true Edit / false View)
        /// </summary>
        public bool SdfVisibility { get; set; }
    }
}