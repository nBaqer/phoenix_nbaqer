﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUdfOutputModel.cs" company="FAME">
//  2016 
// </copyright>
// <summary>
//   Defines the IUdfOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Udf
{
    using System.Collections.Generic;

    using Common;

    /// <summary>
    /// The <code>UdfOutputModel</code> interface.
    /// </summary>
    public interface IUdfOutputModel
    {
        /// <summary>
        /// Gets or sets the entity / page List.
        /// </summary>
        IList<DropDownIntegerOutputModel> EntityList { get; set; }

        /// <summary>
        ///  Gets or sets the List of UDF assigned to the filter page
        /// </summary>
        IList<UdfDto> AssignedToPageUdfList { get; set; }

        /// <summary>
        ///  Gets or sets the List of UDF no assigned to filter page
        /// </summary>
        IList<UdfDto> UnAssignedToPageUdfList { get; set; }

        /// <summary>
        ///  Gets or sets the Input Filter
        /// </summary>
        UdfInputModel Filter { get; set; }

    }
}
