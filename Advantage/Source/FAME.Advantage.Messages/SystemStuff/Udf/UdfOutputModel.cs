﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UdfOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the UdfOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Udf
{
    using System.Collections.Generic;
    using Common;

    /// <summary>
    ///  The UDF output model.
    /// </summary>
    public class UdfOutputModel : IUdfOutputModel
    {
        #region Implementation of IUdfOutputModel

        /// <summary>
        /// Gets or sets the entity list.
        /// </summary>
        public IList<DropDownIntegerOutputModel> EntityList { get; set; }

        /// <summary>
        /// Gets or sets the assigned to page UDF list.
        /// </summary>
        public IList<UdfDto> AssignedToPageUdfList { get; set; }

        /// <summary>
        /// Gets or sets the no assigned to page UDF list.
        /// </summary>
        public IList<UdfDto> UnAssignedToPageUdfList { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        public UdfInputModel Filter { get; set; }

        #endregion

        /// <summary>
        /// Use this to create the class
        /// </summary>
        /// <returns>
        /// A full initialized class
        /// </returns>
        public static IUdfOutputModel Factory()
        {
            IUdfOutputModel output = new UdfOutputModel();
            output.AssignedToPageUdfList = new List<UdfDto>();
            output.UnAssignedToPageUdfList = new List<UdfDto>();
            output.EntityList = new List<DropDownIntegerOutputModel>();
            output.Filter = new UdfInputModel();
            return output;
        }
    }
}
