﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UdfInputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the UdfInputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Udf
{
    /// <summary>
    /// The UDF Input Model
    /// </summary>
    public class UdfInputModel
    {
        /// <summary>
        /// Gets or sets The command
        /// </summary>
        public int Command { get; set; }

        /// <summary>
        /// Gets or sets Selected Entity
        /// </summary>
        public int LeadEntity { get; set; }

        /// <summary>
        /// Gets or sets The Page Id that we need to get the related UDF.
        /// </summary>
        public int PageResourceId { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///  Create a new object and clone with this value
        /// </summary>
        /// <returns>
        /// return a clone object <code>UdfInputModel</code>
        /// </returns>
        public UdfInputModel Clone()
        {
            var clone = new UdfInputModel
            {
                PageResourceId = this.PageResourceId,
                Command = this.Command,
                LeadEntity = this.LeadEntity
            };
            return clone;
        }
    }
}
