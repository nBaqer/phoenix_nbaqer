﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassAppConfigurationOutputModel.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The class application configuration output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Messages.SystemStuff.Mobile
{
    using System;

    /// <summary>
    ///  The class application configuration output model.
    /// </summary>
    public class ClassAppConfigurationOutputModel : IClassAppConfigurationOutputModel
    {
        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string AdvantageIdentification { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public int KlassAppIdentification { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string CodeEntity { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string CodeOperation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is custom field.
        /// </summary>
        public bool IsCustomField { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the item status.
        /// </summary>
        public char ItemStatus { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string ItemValue { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string ItemLabel { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string ItemValueType { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public DateTime ModDate { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        /// Gets or sets the last operation log.
        /// Gets or sets the last operation log.
        /// Contains the result of the last operation over <code>Klass_App</code>
        /// configuration system.        
        /// </summary>
        public string LastOperationLog { get; set; }
    }
}