﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassAppInputModel.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The class application input model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Mobile
{
    /// <summary>
    ///  The class application input model.
    /// </summary>
    public class ClassAppInputModel
    {
        /// <summary>
        /// Gets or sets the command.
        /// 0 or 1: get all
        /// </summary>
        public int Command { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public string UserId { get; set; }
    }
}
