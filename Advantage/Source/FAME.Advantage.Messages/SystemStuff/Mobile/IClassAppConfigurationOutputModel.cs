﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IClassAppConfigurationOutputModel.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The ClassAppConfigurationOutputModel interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Mobile
{
    using System;

    /// <summary>
    /// The ClassAppConfigurationOutputModel interface.
    /// </summary>
    public interface IClassAppConfigurationOutputModel
    {
        /// <summary>
        /// Gets the advantage identification.
        /// </summary>
        string AdvantageIdentification { get; }

        /// <summary>
        /// Gets the code entity.
        /// </summary>
        string CodeEntity { get; }

        /// <summary>
        /// Gets the code operation.
        /// </summary>
        string CodeOperation { get; }

        /// <summary>
        /// Gets the creation date.
        /// </summary>
        DateTime CreationDate { get; }

        /// <summary>
        /// Gets a value indicating whether is active.
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// Gets a value indicating whether is custom field.
        /// </summary>
        bool IsCustomField { get; }

        /// <summary>
        /// Gets the item label.
        /// </summary>
        string ItemLabel { get; }

        /// <summary>
        /// Gets the item status.
        /// </summary>
        char ItemStatus { get; }

        /// <summary>
        /// Gets the item value.
        /// </summary>
        string ItemValue { get; }

        /// <summary>
        /// Gets the item value type.
        /// </summary>
        string ItemValueType { get; }

        /// <summary>
        /// Gets the <code>klass app</code> identification.
        /// </summary>
        int KlassAppIdentification { get; }

        /// <summary>
        /// Gets the mod date.
        /// </summary>
        DateTime ModDate { get; }

        /// <summary>
        /// Gets the mod user.
        /// </summary>
        string ModUser { get; }
    }
}