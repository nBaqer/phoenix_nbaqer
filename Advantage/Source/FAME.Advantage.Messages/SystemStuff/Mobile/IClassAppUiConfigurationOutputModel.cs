﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IClassAppUiConfigurationOutputModel.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The <code>ClassAppUiConfigurationOutputModel</code> interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Mobile
{
    using System.Collections.Generic;

    /// <summary>
    /// The <code>ClassAppUiConfigurationOutputModel</code> interface.
    /// </summary>
    public interface IClassAppUiConfigurationOutputModel
    {
        /// <summary>
        /// Gets or sets the configuration setting.
        /// </summary>
        IList<ClassAppConfigurationOutputModel> ConfigurationSetting { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        ClassAppInputModel Filter { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is any configuration pending.
        /// </summary>
        bool IsAnyConfigurationPending { get; set; }
    }
}