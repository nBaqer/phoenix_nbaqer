﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IGeneralResponse.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The GeneralResponse interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Mobile.ApiKlass
{
    /// <summary>
    /// The GeneralResponse interface.
    /// </summary>
    public interface IGeneralResponse
    {
        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        string ErrorCode { get; set; }

        /// <summary>
        /// Gets or sets the explanation.
        /// </summary>
        string Explanation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is deleted.
        /// </summary>
        bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the <code>klass_app</code> identification.
        /// </summary>
        int KlassAppIdentification { get; set; }

        /// <summary>
        /// Gets or sets the object from server.
        /// this can be any list or object read in server.
        /// </summary>
        dynamic ObjectFromServer { get; set; }
    }
}