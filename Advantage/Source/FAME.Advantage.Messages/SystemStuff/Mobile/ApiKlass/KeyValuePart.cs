﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KeyValuePart.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the Department type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Mobile.ApiKlass
{
    using System;

    /// <summary>
    /// The department.
    /// </summary>
    public class KeyValuePart
    {
        // ReSharper disable StyleCop.SA1300

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        // ReSharper disable InconsistentNaming
        public int id { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string name { get; set; } // "My campus",

        /// <summary>
        /// Gets or sets the deleted.
        /// </summary>
        public int deleted { get; set; } // "deleted": 0,

        /// <summary>
        /// Gets or sets the created_at.
        /// </summary>
        public DateTime created_at { get; set; } // "created_at": "2016-11-03T19:40:00.000Z",

        /// <summary>
        /// Gets or sets the updated_at.
        /// </summary>
        public DateTime updated_at { get; set; } // "updated_at": "2017-02-06T18:41:48.000Z",
        
        // ReSharper restore InconsistentNaming
        // ReSharper restore StyleCop.SA1300
    }
}
