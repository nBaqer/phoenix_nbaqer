﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Error.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The response location.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

// ReSharper disable InconsistentNaming
namespace FAME.Advantage.Messages.SystemStuff.Mobile.ApiKlass
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// The error.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Reviewed. Suppression is OK here.")]
    public class Error
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// Gets or sets the stack.
        /// </summary>
        public string stack { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string code { get; set; }

        /// <summary>
        /// Gets or sets the SQL.
        /// </summary>
        public string sql { get; set; }
    }
}
