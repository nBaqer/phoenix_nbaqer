﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GeneralResponse.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the GeneralResponse type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Mobile.ApiKlass
{
    /// <summary>
    /// The general response.
    /// </summary>
    public class GeneralResponse : IGeneralResponse
    {
        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        public string ErrorCode { get; set; }

        /// <summary>
        /// Gets or sets the <code>klass_app</code> identification.
        /// </summary>
        public int KlassAppIdentification { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the explanation of the error if exists.
        /// </summary>
        public string Explanation { get; set; }

        /// <summary>
        /// Gets or sets the object from server.
        /// this can be any list or object read in server.
        /// </summary>
        public dynamic ObjectFromServer { get; set; }
        
        /// <summary>
        /// The factory.
        /// </summary>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        public static IGeneralResponse Factory()
        {
            IGeneralResponse res = new GeneralResponse();
            res.ErrorCode = string.Empty;
            res.IsDeleted = false;
            res.KlassAppIdentification = 0;
            res.Explanation = string.Empty;
            return res;
        }
    }
}
