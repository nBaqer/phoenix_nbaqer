﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWapiOperation.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Interface for External Operation Settings <br />
//   Store all necessary information to configure the external operation.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Mobile.ApiKlass
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// Interface for External Operation Settings <br/>
    /// Store all necessary information to configure the external operation.
    /// </summary>
    public interface IWapiOperation
    {
        /// <summary>
        /// Gets or sets Unique ID
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// Gets or sets Name of the operation
        /// </summary>
        string CodeOperation { get; set; }

        /// <summary>
        /// Gets or sets Code of the company that owner the web service
        /// in the case that the operation access a external
        /// resource. 
        /// </summary>
        string SecretCodeCompany { get; set; }

        /// <summary>
        /// Gets or sets The type of interaction with the external resource if exists can be:
        /// <ul><il>WAPI_TEST_SYSTEM</il>
        /// <il>VOYANT_CURRENT</il>
        /// <il>VOYANT_HISTORICAL</il></ul> 
        /// </summary>
        string CodeExtOperationMode { get; set; }

        /// <summary>
        /// Gets or sets External URL to looking for, if exists.
        /// </summary>
        string ExternalUrl { get; set; }

        /// <summary>
        /// Gets or sets Security key to interact with external service if exists
        /// </summary>
        string ConsumerKey { get; set; }

        /// <summary>
        /// Gets or sets Security key to interact with external service
        /// if exists.
        /// </summary>
        string PrivateKey { get; set; }

        /// <summary>
        /// Gets or sets The code of operation in the WAPI
        /// The operation to be invoked by the WAPI
        /// </summary>
        string CodeWapiServ { get; set; }

        /// <summary>
        /// Gets or sets Some operation require two operation to be completed
        /// In this case this field is used.
        /// </summary>
        string SecondCodeWapiServ { get; set; }

        /// <summary>
        /// Gets or sets Time that a windows service should repeat the operation
        /// if it is needed. -1 if inactive.0 one time operation.
        /// </summary>
        int OperationSecInterval { get; set; }

        /// <summary>
        /// Gets or sets This property stored the last point of time  
        /// that the operation was executed
        /// Date + Time.
        /// </summary>
        DateTime DateLastExecution { get; set; }

        /// <summary>
        /// Gets or sets Poll WAPI from windows service to look for a configuration change.
        /// </summary>
        int PollSecOnDemandOperation { get; set; }

        /// <summary>
        /// Gets or sets  0: inactive, 1: active
        /// </summary>
        int FlagOnDemandOperation { get; set; }

        /// <summary>
        /// Gets or sets 0: inactive, 1: active
        /// </summary>
        int FlagRefreshConfig { get; set; }

        #region Specific to communicate with WAPI PROXY and log info

        /// <summary>
        /// Gets or sets The address of the proxy
        /// </summary>
        /// <remarks>
        /// Should be the same for all operations.
        /// but for now is in each operation
        /// </remarks>
        string UrlProxy { get; set; }

        /// <summary>
        /// Gets or sets The address of the log file
        /// </summary>
        /// <remarks>
        /// Should be the same for all operations.
        /// but for now is in each operation
        /// </remarks>
        string LogFile { get; set; }

        /// <summary>
        /// Gets or sets The size of the log file
        /// </summary>
        /// <remarks>
        /// Should be the same for all operations.
        /// but for now is in each operation
        /// </remarks>
        int SizeLogFile { get; set; }

        /// <summary>
        /// Gets or sets You can put here a trace instance to pass trace information
        /// to other class
        /// WARINIG if you don't use this. be sure that the class use
        /// some method to instantiate by default the trace.
        /// </summary>
        TraceSource Tracer { get; set; }

        #endregion

        #region Added to Klass_App 

        /// <summary>
        /// Gets or sets the <code>klass_app</code> days to consider.
        /// </summary>
        int KlassAppDaysToConsider { get; set; }

        /// <summary>
        /// Gets or sets the <code>klass_app</code> URL configuration custom fields.
        /// </summary>
        string KlassAppUrlConfigCustomFields { get; set; }

        /// <summary>
        /// Gets or sets the <code>klass_app</code> URL configuration key values pairs.
        /// </summary>
        string KlassAppUrlConfigKeyValuesPairs { get; set; }

        /// <summary>
        /// Gets or sets the <code>klass_app</code> URL configuration majors.
        /// </summary>
        string KlassAppUrlConfigMajors { get; set; }

        /// <summary>
        /// Gets or sets the <code>klass_app</code> URL configuration locations.
        /// </summary>
        string KlassAppUrlConfigLocations { get; set; }

        /// <summary>
        /// Gets or sets the campus active list.
        /// </summary>
        IList<Guid> CampusActiveList { get; set; }

        #endregion
    }

    /// <summary>
    /// The WAPI operation.
    /// </summary>
    public class WapiOperation : IWapiOperation
    {
        /// <summary>
        /// Gets or sets Unique ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Name of the operation
        /// </summary>
        public string CodeOperation { get; set; }

        /// <summary>
        /// Gets or sets Code of the company that owner the web service
        /// in the case that the operation access a external
        /// resource. 
        /// </summary>
        public string SecretCodeCompany { get; set; }

        /// <summary>
        /// Gets or sets The type of interaction with the external resource if exists can be:
        /// <ul><il>WAPI_TEST_SYSTEM</il>
        /// <il>VOYANT_CURRENT</il>
        /// <il>VOYANT_HISTORICAL</il></ul> 
        /// </summary>
        public string CodeExtOperationMode { get; set; }

        /// <summary>
        /// Gets or sets External URL to looking for, if exists.
        /// </summary>
        public string ExternalUrl { get; set; }

        /// <summary>
        /// Gets or sets Security key to interact with external service if exists
        /// </summary>
        public string ConsumerKey { get; set; }

        /// <summary>
        /// Gets or sets Security key to interact with external service
        /// if exists.
        /// </summary>
        public string PrivateKey { get; set; }

        /// <summary>
        /// Gets or sets The code of operation in the WAPI
        /// The operation to be invoked by the WAPI
        /// </summary>
        public string CodeWapiServ { get; set; }

        /// <summary>
        /// Gets or sets Some operation require two operation to be completed
        /// In this case this field is used.
        /// </summary>
        public string SecondCodeWapiServ { get; set; }

        /// <summary>
        /// Gets or sets Time that a windows service should repeat the operation
        /// if it is needed. -1 if inactive.0 one time operation.
        /// </summary>
        public int OperationSecInterval { get; set; }

        /// <summary>
        /// Gets or sets This property stored the last point of time  
        /// that the operation was executed
        /// Date + Time.
        /// </summary>
        public DateTime DateLastExecution { get; set; }

        /// <summary>
        /// Gets or sets Poll WAPI from windows service to look for a configuration change.
        /// </summary>
        public int PollSecOnDemandOperation { get; set; }

        /// <summary>
        /// Gets or sets  0: inactive, 1: active
        /// </summary>
        public int FlagOnDemandOperation { get; set; }

        /// <summary>
        /// Gets or sets 0: inactive, 1: active
        /// </summary>
        public int FlagRefreshConfig { get; set; }

        /// <summary>
        /// Gets or sets The address of the proxy
        /// </summary>
        /// <remarks>
        /// Should be the same for all operations.
        /// but for now is in each operation
        /// </remarks>
        public string UrlProxy { get; set; }

        /// <summary>
        /// Gets or sets The address of the log file
        /// </summary>
        /// <remarks>
        /// Should be the same for all operations.
        /// but for now is in each operation
        /// </remarks>
        public string LogFile { get; set; }

        /// <summary>
        /// Gets or sets The size of the log file
        /// </summary>
        /// <remarks>
        /// Should be the same for all operations.
        /// but for now is in each operation
        /// </remarks>
        public int SizeLogFile { get; set; }

        /// <summary>
        /// Gets or sets You can put here a trace instance to pass trace information
        /// to other class
        /// WARINIG if you don't use this. be sure that the class use
        /// some method to instantiate by default the trace.
        /// </summary>
        public TraceSource Tracer { get; set; }

        /// <summary>
        /// Gets or sets the <code>klass_app</code> days to consider.
        /// </summary>
        public int KlassAppDaysToConsider { get; set; }

        /// <summary>
        /// Gets or sets the <code>klass_app</code> URL configuration custom fields.
        /// </summary>
        public string KlassAppUrlConfigCustomFields { get; set; }

        /// <summary>
        /// Gets or sets the <code>klass_app</code> URL configuration key values pairs.
        /// </summary>
        public string KlassAppUrlConfigKeyValuesPairs { get; set; }

        /// <summary>
        /// Gets or sets the <code>klass_app</code> URL configuration locations.
        /// </summary>
        public string KlassAppUrlConfigLocations { get; set; }

        /// <summary>
        /// Gets or sets the campus active list.
        /// </summary>
        public IList<Guid> CampusActiveList { get; set; }

        /// <summary>
        /// Gets or sets the <code>klass_app</code> URL configuration majors.
        /// </summary>
        public string KlassAppUrlConfigMajors { get; set; }
        
        /// <summary>
        /// The factory.
        /// </summary>
        /// <returns>
        /// The <see cref="IWapiOperation"/>.
        /// </returns>
        public static IWapiOperation Factory()
        {
            IWapiOperation wapi = new WapiOperation();
            return wapi;
        }
    }
}
