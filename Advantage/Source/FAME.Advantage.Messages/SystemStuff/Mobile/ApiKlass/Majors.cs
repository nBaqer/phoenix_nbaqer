﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Majors.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the Majors type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Mobile.ApiKlass
{
    using System;
    // ReSharper disable StyleCop.SA1300
    // ReSharper disable InconsistentNaming
     
    /// <summary>
    /// The majors.
    /// </summary>
    public class Majors
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Gets or sets the created_at.
        /// </summary>
        public DateTime created_at { get; set; }

        /// <summary>
        /// Gets or sets the updated_at.
        /// </summary>
        public DateTime updated_at { get; set; }

        /// <summary>
        /// Gets or sets the deleted.
        /// </summary>
        public int deleted { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// Gets or sets the auto_disabled.
        /// </summary>
        public int auto_disabled { get; set; }
    }
    // ReSharper restore InconsistentNaming
    // ReSharper restore StyleCop.SA1300
}
