﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResponseBase.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the ResponseBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

// ReSharper disable InconsistentNaming
namespace FAME.Advantage.Messages.SystemStuff.Mobile.ApiKlass
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// The response base.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Reviewed. Suppression is OK here.")]
    public class ResponseBase
    {
        /// <summary>
        /// Gets or sets a value indicating whether success.
        /// </summary>
        public bool success { get; set; }

        /// <summary>
        /// Gets or sets the time-stamp.
        /// </summary>
        public DateTime timestamp { get; set; }

        /// <summary>
        /// Gets or sets the <code>auth_id</code>.
        /// </summary>
        public string auth_id { get; set; }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        public int total { get; set; }

        /// <summary>
        /// Gets or sets the offset.
        /// </summary>
        public int offset { get; set; }

        /// <summary>
        /// Gets or sets the limit.
        /// </summary>
        public int limit { get; set; }

        /// <summary>
        /// Gets or sets the response.
        /// </summary>
        public dynamic response { get; set; }

        /// <summary>
        /// Gets or sets the errors.
        /// </summary>
        public dynamic[] errors { get; set; }

        /// <summary>
        /// Gets or sets the info.
        /// </summary>
        public string info { get; set; }
    }
}
