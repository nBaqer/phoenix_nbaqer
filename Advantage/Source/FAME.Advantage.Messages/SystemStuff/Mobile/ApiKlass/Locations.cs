﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Locations.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the Locations type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Mobile.ApiKlass
{
    using System;

    /// <summary>
    /// The locations.
    /// </summary>
    public class Locations
    {
        // ReSharper disable StyleCop.SA1300
        
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        // ReSharper disable InconsistentNaming
        public int id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string name { get; set; } // "My campus",

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string address { get; set; } // "Address two or three",

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string phone { get; set; }  // "phone": "0800 123-436",

        /// <summary>
        /// Gets or sets the location_type.
        /// </summary>
        public string location_type { get; set; } // "location_type": "online",

        /// <summary>
        /// Gets or sets the contact_email.
        /// </summary>
        public string contact_email { get; set; } // "contact_email": "mick@email.com",

        /// <summary>
        /// Gets or sets the contact_person.
        /// </summary>
        public string contact_person { get; set; } // "contact_person": "Mick henry Tudor",

        /// <summary>
        /// Gets or sets the contact_phone.
        /// </summary>
        public string contact_phone { get; set; } // "contact_phone": "0800 123-456-789",

        /// <summary>
        /// Gets or sets the contact_position.
        /// </summary>
        public string contact_position { get; set; } // "contact_position": "Manager",

        /// <summary>
        /// Gets or sets the deleted.
        /// </summary>
        public int deleted { get; set; } // "deleted": 0,

        /// <summary>
        /// Gets or sets the referrals_email_address.
        /// </summary>
        public string referrals_email_address { get; set; } // "referrals_email_address": null,

        /// <summary>
        /// Gets or sets the created_at.
        /// </summary>
        public DateTime created_at { get; set; } // "created_at": "2016-11-03T19:40:00.000Z",

        /// <summary>
        /// Gets or sets the updated_at.
        /// </summary>
        public DateTime updated_at { get; set; } // "updated_at": "2017-02-06T18:41:48.000Z",

        /// <summary>
        /// Gets or sets the payments_on.
        /// </summary>
        public string payments_on { get; set; } // "payments_on": null,

        /// <summary>
        /// Gets or sets the payments_currency.
        /// </summary>
        public string payments_currency { get; set; } // "payments_currency": null,

        /// <summary>
        /// Gets or sets the payments_world pay_id.
        /// </summary>
        public string payments_worldpay_id { get; set; } // "payments_worldpay_id": null,

        /// <summary>
        /// Gets or sets the payments_email_address.
        /// </summary>
        public string payments_email_address { get; set; } // "payments_email_address": null,

        /// <summary>
        /// Gets or sets the payments_world pay_gateway_id.
        /// </summary>
        public string payments_worldpay_gateway_id { get; set; } // "payments_worldpay_gateway_id": null,

        /// <summary>
        /// Gets or sets the payments_world pay_form_id.
        /// </summary>
        public string payments_worldpay_form_id { get; set; } // "payments_worldpay_form_id": null,

        /// <summary>
        /// Gets or sets the payments_world pay_session.
        /// </summary>
        public string payments_worldpay_session { get; set; } // "payments_worldpay_session": null,

        /// <summary>
        /// Gets or sets the payments_world pay_key.
        /// </summary>
        public string payments_worldpay_key { get; set; } // "payments_worldpay_key": null

        // ReSharper restore InconsistentNaming
        // ReSharper restore StyleCop.SA1300
    }
}