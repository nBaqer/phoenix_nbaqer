﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassAppUiConfigurationOutputModel.cs" company="FAME">
//  2017
// </copyright>
// <summary>
//   The class application UI configuration output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Mobile
{
    using System.Collections.Generic;

    /// <summary>
    ///  The class application UI configuration output model.
    /// </summary>
    public class ClassAppUiConfigurationOutputModel : IClassAppUiConfigurationOutputModel
    {
        /// <summary>
        /// Gets or sets the configuration setting.
        /// </summary>
        public IList<ClassAppConfigurationOutputModel> ConfigurationSetting { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        public ClassAppInputModel Filter { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is any configuration pending.
        /// </summary>
        public bool IsAnyConfigurationPending { get; set; }

        /// <summary>
    /// The factory.
    /// </summary>
    /// <returns>
    /// The <see cref="IClassAppUiConfigurationOutputModel"/>.
    /// </returns>
    public static IClassAppUiConfigurationOutputModel Factory()
        {
            var om = new ClassAppUiConfigurationOutputModel
                         {
                             ConfigurationSetting =
                                 new List<ClassAppConfigurationOutputModel>()
                         };
            return om;
        }
    }
}
