﻿using FAME.Advantage.Messages.StatusOptions;

namespace FAME.Advantage.Messages.SystemStuff.SystemStates
{
    using System;

    public class SystemStatesOutputModel
    {
        public Guid ID { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public string FullDescription { get; set; }
    }
}


