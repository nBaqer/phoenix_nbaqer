﻿namespace FAME.Advantage.Messages.SystemStuff.Resources
{
    /// <summary>
    /// The page resource ID. The resource are organized by page.you need to put here the resource id of the page
    /// The standard code id for country language. Actually only USA (EN-US)
    /// </summary>
    public class CaptionRequiredInputModel
    {
        /// <summary>
        /// The page resource ID. The resource are organized by page.
        /// you need to put here the resource id of the page
        /// </summary>
        public int PageResourceId { get; set; }

        /// <summary>
        /// The standard code id for country language.
        /// Actually only USA (EN-US)
        /// </summary>
        public string CodeLang  { get; set; }

    }
   
}
