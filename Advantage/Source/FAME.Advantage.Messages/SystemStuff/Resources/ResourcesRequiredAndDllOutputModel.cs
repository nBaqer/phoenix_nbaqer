﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourcesRequiredAndDllOutputModel.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Output model for set control captions and required fields.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.SystemStuff.Resources
{
    using System.Collections.Generic;
    using Common;

    /// <summary>
    /// Output model for set control captions and required fields.
    /// </summary>
    public class ResourcesRequiredAndDllOutputModel
    {
        /// <summary>
        /// Gets or sets The name of the field in Advantage DB
        /// </summary>
        public string FldName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether required.
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        ///  Gets or sets If the control associated it is a combo box get the 
        /// combo-box list.
        /// </summary>
        public IList<DropDownOutputModel> Dll { get; set; }
   }
}
