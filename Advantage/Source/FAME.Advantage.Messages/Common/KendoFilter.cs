﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KendoFilter.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Messages.Common.KendoFilters
// </copyright>
// <summary>
//   The kendo filters. This is pass as input from Kendo Data Source used in Kendo Auto Complete
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The kendo filters. This is pass as input from Kendo Data Source used in Kendo Auto Complete
    /// </summary>
    public class KendoFilter
    {
        /// <summary>
        /// Gets or sets the filters.
        /// </summary>
        public List<KendoFilterItem> filters { get; set; }

        /// <summary>
        /// Gets or sets the logic.
        /// </summary>
        public string logic { get; set; }
    }
}
