﻿namespace FAME.Advantage.Messages.Common
{
    public class DropDownIntegerOutputModel
    {
        public int ID { get; set; }
        public string Description { get; set; }
    }
}
