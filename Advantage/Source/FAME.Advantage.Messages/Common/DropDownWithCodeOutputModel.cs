﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropDownWithCodeOutputModel.cs" company="FAME Inc">
//   2018
// </copyright>
// <summary>
//   The drop down with code output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Common
{
    /// <summary>
    /// The drop down with code output model.
    /// </summary>
    public class DropDownWithCodeOutputModel : DropDownWithCampusOutputModel
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }
    }
}
