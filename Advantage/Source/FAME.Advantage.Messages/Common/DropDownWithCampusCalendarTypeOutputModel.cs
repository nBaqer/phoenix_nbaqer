﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropDownWithCampusCalendarTypeOutputModel.cs" company="FAME Inc">
//   2016
// </copyright>
// <summary>
//   Defines the DropDownWithCampusCalendarTypeOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Common
{
    /// <summary>
    /// The drop down with campus calendar type output model.
    /// </summary>
    public class DropDownWithCampusCalendarTypeOutputModel : DropDownWithCampusOutputModel
    {
        /// <summary>
        /// Gets or sets the calendar type.
        /// </summary>
        public string CalendarType { get; set; }

        /// <summary>
        /// Gets or sets the program id.
        /// </summary>
        public string ProgramId { get; set; }

        /// <summary>
        /// Gets or sets the Area id.
        /// </summary>
        public string AreaId { get; set; }
    }
}
