﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KendoFilterItem.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Messages.Common.KendoFilter
// </copyright>
// <summary>
//   The kendo filter item. This is pass as input from Kendo Data Source used in Kendo Auto Complete
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The kendo filter item. This is pass as input from Kendo Data Source used in Kendo Auto Complete
    /// </summary>
    public class KendoFilterItem
    {
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// Gets or sets the field.
        /// </summary>
        public string field { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether ignore case.
        /// </summary>
        public bool ignoreCase { get; set; }
    }
}
