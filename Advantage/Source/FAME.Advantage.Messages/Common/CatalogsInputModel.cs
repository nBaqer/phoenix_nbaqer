﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CatalogsInputModel.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Filter for get catalogs
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Common
{
    using System;

    /// <summary>
    /// Filter for get catalogs
    /// </summary>
    public class CatalogsInputModel
    {
        /// <summary>
        /// Gets or sets <code>FldName</code> is used to describe a catalog.
        /// see table of <code>FldName</code> to get legal values
        /// <code>FldName</code> is on table <code>syFields</code> in <code>AdvantageDb</code>
        /// </summary>
        public string FldName { get; set; }

        /// <summary>
        ///  Gets or sets The campus ID. the item must be in the campusID
        /// </summary>
        public string CampusId { get; set; }

        /// <summary>
        ///  Gets or sets Some time we need to filter by other condition.
        /// in this case use this
        /// </summary>
        public string AdditionalFilter { get; set; }

        /// <summary>
        /// Gets or sets the command.
        /// </summary>
        public int? Command { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid? UserId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether disable sort by description.
        /// </summary>
        public bool DisableSort { get; set; }
    }
}
