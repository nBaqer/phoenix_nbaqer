﻿using System.Collections.Generic;

namespace FAME.Advantage.Messages.Common
{

    /// <summary>
    /// Campus output
    /// </summary>
    public class CampusOutput
    {
         /// <summary>
        /// Gets or sets the campus ID
        /// </summary>
        public string CampusId { get; set; }
    }
   
    /// <summary>
    /// This class enhanced DropDownIntegerOutputModel to support campus Id 
    /// </summary>
    public class DropDownWithCampusOutputModel: DropDownOutputModel
    {
        /// <summary>
        /// The list of campus id that belong the output model
        /// </summary>
        public IList<CampusOutput> CampusesIdList { get; set; }
    }
}
