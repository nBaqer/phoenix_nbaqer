﻿namespace FAME.Advantage.Messages.Common
{
    /// <summary>
    /// Use this to generic output in controllers
    /// </summary>
    public class GenericOutput : IGenericOutput
    {
        public static IGenericOutput Factory()
        {
            var ig = new GenericOutput();
            return ig;
        }
        
       public int IsPassed { get; set; }
       public string Note { get; set; }
    }
}
