﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropDownOutputModel.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   This is common to all drop-down.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Common
{
    /// <summary>
    /// This is common to all drop-down.
    /// </summary>
    public class DropDownOutputModel
    {
        /// <summary>
        /// Gets or sets the ID (Guid)
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        ///  Gets or sets Description
        /// </summary>
        public string Description { get; set; }
    }
}
