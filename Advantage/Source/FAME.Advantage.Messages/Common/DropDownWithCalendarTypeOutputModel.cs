﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropDownWithCalendarTypeOutputModel.cs" company="FAME Inc">
//   2016
// </copyright>
// <summary>
//   The drop down with calendar type output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Common
{
    /// <summary>
    /// The drop down with calendar type output model.
    /// </summary>
    public class DropDownWithCalendarTypeOutputModel : DropDownOutputModel
    {
        /// <summary>
        /// Gets or sets the calendar type.
        /// </summary>
        public string CalendarType { get; set; }
    }
}
