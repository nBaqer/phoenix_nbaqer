﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Messages.Common
{
    /// <summary>
    /// the drp down that has campus and view order for family income
    /// </summary>
    public class DropDownWithCampusViewOrder : DropDownWithCampusOutputModel
    {
        /// <summary>
        /// view order is the sort order for family income
        /// </summary>
        public int ViewOrder { get; set; }
    }
}
