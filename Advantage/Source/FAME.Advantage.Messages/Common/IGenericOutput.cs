﻿namespace FAME.Advantage.Messages.Common
{
    /// <summary>
    /// Use this to generic output in controllers
    /// </summary>
    public interface IGenericOutput
    {
        int IsPassed { get; set; }
        string Note { get; set; }
    }
}