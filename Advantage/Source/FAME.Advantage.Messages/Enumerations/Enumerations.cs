﻿namespace FAME.Advantage.Messages.Enumerations
{
    /// <summary>
    ///  Enumerations used in messages.
    /// </summary>
    public class Enumerations
    {
        /// <summary>
        /// Attendance Status enumeration for Present Absent School.
        /// Observe that MakeUp is not a valid state for a period!
        /// </summary>
        public enum AttendanceStatusEnum
        {
            Unselected = 0, Present = 1, Absent = 2, Tardy = 3, Excused = 4, MakeUp = 5
        }

        public enum MRuTypes
        {
            Students = 1,
            Employers = 2,
            Employees = 3,
            Leads = 4
        }

        /// <summary>
        /// Arbitrary module division of Advantage
        /// Module are also resources (Resource type 1 Modules) 
        /// </summary>
        public enum AdvantageModulesEnum
        {
            AD = 189, //Admission
            AR = 26,  //Students
            HR = 192,
            PL = 193,
            SA = 194,
            FA = 191
        }

        public enum AdvantageEntitiesEnum
        {
            Lead = 395,
            Student = 394
        }

        public enum PagesResourceEnum
        {
            InfoLeadEdit = 170,
            InfoLeadNew = 206
        }

        public enum RequirementToUse
        {

            DocRequirements,
            TestRequirements,
            InterviewRequirements,
            EventRequirements,
            TourRequirements
        }

       

        public enum RequirementTypeLevel
        {
            School,
            RequirementGroup,
            StudentGroup,
            Program
        }

        public enum RequiredFor
        {
            Enrollment,
            FinancialAid,
            Graduation,
            None
        }

        /// <summary>
        /// Give information about Notes Source and fields Catalog
        /// <code>syNotesPageFields</code>
        /// </summary>
        public enum EnumNotePageField
        {
            /// <summary>
            /// (1) Page Field for Page Notes, Notes entered in the grid
            /// </summary>
            NotesNote = 1,

            /// <summary>
            /// (2) Page Lead Info Field Comment
            /// </summary>
            InfoComment,

            /// <summary>
            /// (3) Page Lead Info Field Notes Advertisement
            /// </summary>
            InfoNote,
            
            /// <summary>
            /// (4) Overrides in requirements
            /// </summary>
            RequirementsOverride,
            
            /// <summary>
            /// (5) Task message is task
            /// </summary>
            TaskMessage,

            /// <summary>
            /// (6) Page Contact Other - Field Comment
            /// </summary>
            ContactComment,

            /// <summary>
            /// (7) Comment is Skill Page
            /// </summary>
            SkillComment,

            /// <summary>
            /// (8) Comment in Extracurricular Page
            /// </summary>
            ExtraCurricularComment,

            /// <summary>
            /// (9) Comment in Extracurricular Page
            /// </summary>
            EmailTemplate,

            /// <summary>
            /// (10) Prior Work Comment
            /// </summary>
            PriorWorkComment,

            /// <summary>
            /// (11) The prior education.
            /// </summary>
            PriorEducation
        }
    }
}
