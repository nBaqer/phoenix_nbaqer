﻿using System;

namespace FAME.Advantage.Messages.StatusOptions
{
    public class StatusOptionOutputModel
    {
        public Guid ID { get; set; }        
        public string Status { get; set; }
        public string StatusCode { get; set; }   
    }
}
