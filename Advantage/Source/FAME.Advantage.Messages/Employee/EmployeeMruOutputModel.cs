﻿using System;

namespace FAME.Advantage.Messages.Employee
{
    public class EmployeeMruOutputModel
    {
        public Guid Id { get;  set; }
        public string FullName { get;  set; }
        public string Address { get;  set; }//address
        
        public string City { get;  set; }//City
        public string State { get;  set; }//State
        public string Zip { get; set; }//Zip
        public string CityStateZip { get; set; }
        public string WorkPhone { get;  set; }//Work Phone

        public string MruModDate { get; set; }
        public DateTime? ModDate { get; set; }

        public string Campus { get; set; }
        public string Department { get; set; }
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the image 64.
        /// </summary>
        public string Image64 { get; set; }

        public string ImageUrl { get;  set; }
        public string NotFoundImageUrl { get;  set; }
        public Guid? SearchCampusId { get;  set; }

    }
}