﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmployeeOutputModel.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the EmployeeSearchOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Messages.Employee
{
    using System;

    /// <summary>
    /// The employee search output model.
    /// </summary>
    public class EmployeeSearchOutputModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; protected set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        public string FullName { get; protected set; }

        /// <summary>
        /// Gets or sets the field 1.
        /// </summary>
        public string Field1 { get; protected set; } // address

        /// <summary>
        /// Gets or sets the field 2.
        /// </summary>
        public string Field2
        {
            get
            {
                return string.Empty;
            }

            set
            {
            }
        } // nothing 

        /// <summary>
        /// Gets or sets the field 3.
        /// </summary>
        public string Field3 { get; protected set; } // City

        /// <summary>
        /// Gets or sets the field 4.
        /// </summary>
        public string Field4 { get; protected set; } // State Zip

        /// <summary>
        /// Gets or sets the field 5.
        /// </summary>
        public string Field5 { get; protected set; } // Work Phone

        /// <summary>
        /// Gets or sets the image url.
        /// </summary>
        public string ImageUrl { get; protected set; }

        /// <summary>
        /// Gets or sets the image 64.
        /// </summary>
        public string Image64 { get; protected set; }

        /// <summary>
        /// Gets or sets the not found image URL.
        /// </summary>
        public string NotFoundImageUrl { get; protected set; }

        /// <summary>
        /// Gets or sets the search campus id.
        /// </summary>
        public Guid? SearchCampusId { get; protected set; }
      
    }
}