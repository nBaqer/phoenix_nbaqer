﻿
namespace FAME.Advantage.Messages.Employee
{
    public class EmployeeContactOutputModel
    {
        public string WorkPhone { get; set; }
    }
}
