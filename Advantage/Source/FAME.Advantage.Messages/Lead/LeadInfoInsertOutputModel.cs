﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadInfoInsertOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Returned information when a Lead want to be inserted.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    using System.Collections.Generic;

    /// <summary>
    /// Returned information when a Lead want to be inserted.
    /// </summary>
    public class LeadInfoInsertOutputModel
    {
        /// <summary>
        /// Gets or sets Validation error messages. If this is not string empty 
        /// the Insert was rejected for validation
        /// </summary>
        public string ValidationErrorMessages { get; set; }

        /// <summary>
        /// Gets or sets If this is not null The Insert statement was rejected because a possible 
        /// duplicate. User should decide if insert or not
        /// </summary>
        public string DuplicatedMessages { get; set; }

        /// <summary>
        /// Gets or sets List of conflict duplicate in the DB.
        /// </summary>
        public IList<DuplicateOutputModel> PossiblesDuplicatesList { get; set; }

        /// <summary>
        /// Gets or sets Valid status change list (used in update lead)
        /// </summary>
        public IList<string> ValidStatusChangeList { get; set; } 
    }
}
