﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadAddressInputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the LeadAddressInputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// The lead address input model.
    /// </summary>
    public class LeadAddressInputModel
    {
        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        public string LeadId { get; set; }
        
        /// <summary>
        /// Gets or sets The Campus this object belongs to.
        /// </summary>
        public string CampusId { get; set; }
        
        /// <summary>
        /// Gets or sets The User Name that Created/Modified this object
        /// </summary>
        public string UserName { get; set; }
        
        /// <summary>
        /// Gets or sets The User Id that Created/Modified this object
        /// </summary>
        public string UserId { get; set; }
        
        /// <summary>
        /// Gets or sets The Type of Email
        /// </summary>
        public string TypeId { get; set; }
        
        /// <summary>
        /// Gets or sets The Id of the Status (Active or Inactive)
        /// </summary>
        public string StatusId { get; set; }
        
        /// <summary>
        /// Gets or sets The Unique Id of this Record
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// Gets or sets Boolean to determine if should show the record in the lead page
        /// </summary>
        public bool ShowOnLeadPage { get; set; }
        
        /// <summary>
        /// Gets or sets Boolean to determine if this is the mailing address of the lead
        /// </summary>
        public bool IsMaillingAddress { get; set; }
        
        /// <summary>
        /// Gets or sets Address Part 1. Max 250 Characters
        /// </summary>
        public string Address1 { get; set; }
        
        /// <summary>
        /// Gets or sets Address Part 2. Max 250 Character
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the address apartment or unit.
        /// </summary>
        public string AddressApto { get; set; }
        
        /// <summary>
        /// Gets or sets City associated to the address 1 and address 2
        /// </summary>
        public string City { get; set; }
        
        /// <summary>
        /// Gets or sets Zip Code associated to the address 1 and address 2
        /// </summary>
        public string ZipCode { get; set; }
        
        /// <summary>
        /// Gets or sets Id of the selected country
        /// </summary>
        public string CountryId { get; set; }
        
        /// <summary>
        /// Gets or sets Id of the selected state
        /// </summary>
        public string StateId { get; set; }
        
        /// <summary>
        /// Gets or sets The State of an international address
        /// </summary>
        public string State { get; set; }
        
        /// <summary>
        /// Gets or sets Flag indication if this address is an international one.
        /// </summary>
        public bool IsInternational { get; set; }
        
        /// <summary>
        /// Gets or sets The County Unique Id
        /// </summary>
        public string CountyId { get; set; }
        
        /// <summary>
        /// Gets or sets The County if this Address is an international one
        /// </summary>
        public string County { get; set; }
        
        /// <summary>
        /// Gets or sets The Country if this Address is an international one
        /// </summary>
        public string Country { get; set; }

    }
}
