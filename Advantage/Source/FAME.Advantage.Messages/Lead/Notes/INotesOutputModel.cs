﻿using System.Collections.Generic;
using FAME.Advantage.Messages.Common;

namespace FAME.Advantage.Messages.Lead.Notes
{
    /// <summary>
    /// The output model for notes page
    /// has a list for all necessary data used in
    /// different commands
    /// </summary>
    public interface INotesOutputModel
    {
        /// <summary>
        /// List of notes for a determinate lead
        /// </summary>
        IList<NotesModel> NotesList { get; set; }

        /// <summary>
        /// List of Advantage modules items
        /// </summary>
        IList<DropDownOutputModel> ModulesList { get; set; }

        ///// <summary>
        ///// List of Type of Notes items
        ///// </summary>
        //IList<DropDownIntegerOutputModel> TypeNotesList { get; set; }

        /// <summary>
        /// Use in post operation only
        /// </summary>
        LeadNotesInputFilter Filter { get; set; }
    }
}
