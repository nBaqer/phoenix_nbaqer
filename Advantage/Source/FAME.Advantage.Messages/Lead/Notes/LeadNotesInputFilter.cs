﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadNotesInputFilter.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Input filter class to command the service
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.Notes
{
    /// <summary>
    /// Input filter class to command the service
    /// </summary>
    public class LeadNotesInputFilter
    {
        /// <summary>
        /// Gets or sets The command to be used to get different operation
        /// 1: Get all notes related to the lead
        /// </summary>
        public int Command { get; set; }

        /// <summary>
        /// Gets or sets The lead ID that the notes refers
        /// </summary>
        public string LeadId { get; set; }

        /// <summary>
        /// Gets or sets Module Code
        /// AD for Admission
        /// </summary>
        public string ModuleCode { get; set; }

        /// <summary>
        /// Gets or sets The user Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets The campus Id
        /// </summary>
        public string CampusId { get; set; }
    }
}
