﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NotesModel.cs" company="FAME">
//   2015,2016
// </copyright>
// <summary>
//   Output model for Notes page
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Messages.Lead.Notes
{
    using System;

    /// <summary>
    /// Output model for Notes page
    /// </summary>
    public class NotesModel
    {
        /// <summary>
        /// Gets or sets ID of the notes entered in the Lead Notes Page
        /// The other notes are not editable. 
        /// </summary>
        public int IdNote { get; set; }

        /// <summary>
        ///  Gets or sets The Date of emission or change
        /// </summary>
        public DateTime NoteDate { get; set; }

        /// <summary>
        ///  Gets or sets The module were capture the notes
        /// </summary>
        public string ModuleName { get; set; }

        /// <summary>
        ///  Gets or sets The module were capture the notes
        /// </summary>
        public string ModuleCode { get; set; }

        /// <summary>
        ///  Gets or sets The Page where wrote the notes
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        ///  Gets or sets The type of notes
        /// it is free text in some pages
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        ///  Gets or sets The field caption in the page where the notes was wrote.
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        ///  Gets or sets Id of page field
        /// </summary>
        public int PageFieldId { get; set; }

        /// <summary>
        ///  Gets or sets The text of the notes
        /// </summary>
        public string NoteText { get; set; }

        /// <summary>
        ///  Gets or sets The people that wrote or modified the notes
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        ///  Gets or sets Used to post User Id to server
        /// </summary>
        public string UserId { get; set; }
    }
}
