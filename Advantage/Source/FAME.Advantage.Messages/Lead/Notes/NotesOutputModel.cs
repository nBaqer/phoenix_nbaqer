﻿using System.Collections.Generic;
using FAME.Advantage.Messages.Common;

namespace FAME.Advantage.Messages.Lead.Notes
{
    /// <summary>
    /// Notes output Model
    /// </summary>
    public class NotesOutputModel : INotesOutputModel
    {
        /// <summary>
        /// Use this to instantiate the class.
        /// </summary>
        /// <returns></returns>
        public static INotesOutputModel Factory()
        {
            var notes = new NotesOutputModel
            {
                ModulesList = new List<DropDownOutputModel>(),
                NotesList = new List<NotesModel>(),
                //TypeNotesList = new List<DropDownIntegerOutputModel>()
            };
            return notes;
        }
        
        #region Implementation of INotesOutputModel

        /// <summary>
        /// List of notes for a determinate lead
        /// </summary>
        public IList<NotesModel> NotesList { get; set; }

        /// <summary>
        /// List of Advantage modules items
        /// </summary>
        public IList<DropDownOutputModel> ModulesList { get; set; }

        ///// <summary>
        ///// List of Type of Notes items
        ///// </summary>
        //public IList<DropDownIntegerOutputModel> TypeNotesList { get; set; }

        /// <summary>
        /// Use in post operation only
        /// </summary>
        public LeadNotesInputFilter Filter { get; set; }

        #endregion
    }
}
