﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The image output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.Image
{
    /// <summary>
    /// The image output model.
    /// </summary>
    public class ImageOutputModel
    {
        
        /// <summary>
        /// Gets or sets Image in byte array
        /// </summary>
        public string ImageBase64 { get; set; }

        /// <summary>
        /// Gets or sets Image size
        /// </summary>
        public int ImgLenth { get; set; }

        /// <summary>
        /// Gets or sets Type de Image <code>jpg, png, bmp</code>
        /// </summary>
        public string ExtensionType { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        public ImageInputModel Filter { get; set; }
    }
}
