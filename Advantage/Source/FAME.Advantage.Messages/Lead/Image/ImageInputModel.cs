﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageInputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The image input model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.Image
{
    /// <summary>
    /// The image input model.
    /// </summary>
    public class ImageInputModel
    {
        /// <summary>
        /// Gets or sets the image id.
        /// </summary>
        public string ImageId { get; set; }
        
        /// <summary>
        /// Gets or sets the entity id.
        /// </summary>
        public string EntityId { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the module id.
        /// </summary>
        public string ModuleId { get; set; }

        /// <summary>
        /// Gets or sets the command.
        /// </summary>
        public int Command { get; set; }
    }
}
