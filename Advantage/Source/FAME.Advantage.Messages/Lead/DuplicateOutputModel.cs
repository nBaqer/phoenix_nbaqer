﻿namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// Used to show duplicated student or Lead in Lead Page
    /// </summary>
    public class DuplicateOutputModel
    {

        /// <summary>
        /// LastName
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// First Name
        /// </summary>
        public string  FirstName { get; set; }

        /// <summary>
        /// SNN
        /// </summary>
        public string Ssn { get; set; }

        /// <summary>
        /// DOB
        /// </summary>
        public string  Dob { get; set; }

        /// <summary>
        /// Address 1
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Best Phone
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Phone1
        /// </summary>
        public string Phone1 { get; set; }

        /// <summary>
        /// Phone 2
        /// </summary>
        public string Phone2 { get; set; }

        /// <summary>
        /// Email 
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Email 2
        /// </summary>
        public string Email1 { get; set; }

        /// <summary>
        /// Campus Name
        /// </summary>
        public string Campus { get; set; }

        /// <summary>
        /// Admission Rep name
        /// </summary>
        public string AdmissionRep { get; set; }

        /// <summary>
        /// Status of the entity
        /// </summary>
        public string Status { get; set; }
    }
}
