﻿using System;

namespace FAME.Advantage.Messages.Lead
{
    public class LeadMruOutputModel
    {
        public Guid Id { get;  set; }
        public string FullName { get;  set; }
        public string SSN { get;  set; }//SSN
        public string StartDate { get;  set; }//StartDate
        public string Status { get;  set; }//Status
        public string Program { get; set; }//Status
        public string Campus { get;  set; }//Campus   
        public string MruModDate { get; set; }
        public DateTime? ModDate { get; set; }

        /// <summary>
        /// Gets or sets the image 64.
        /// </summary>
        public string Image64 { get; set; }

        public string ImageUrl { get;  set; }
        public string NotFoundImageUrl { get;  set; }
        public Guid? SearchCampusId { get;  set; }

    }
}