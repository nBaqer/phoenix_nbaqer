﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadDuplicatesOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Output model for Duplicate Lead Analysis
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    using System;

    /// <summary>
    /// Output model for Duplicate Lead Analysis
    /// </summary>
    public class LeadDuplicatesOutputModel
    {
        /// <summary>
        /// Gets or sets the header.
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the address 1.
        /// </summary>
        public string Address1 { get;  set; }

        /// <summary>
        /// Gets or sets the address 2.
        /// </summary>
        public string Address2 { get;  set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get;  set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State { get;  set; }

        /// <summary>
        /// Gets or sets the zip.
        /// </summary>
        public string Zip { get;  set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string Status { get;  set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string Phone { get;  set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get;  set; }

        /// <summary>
        /// Gets or sets the source info.
        /// </summary>
        public string SourceInfo { get;  set; }

        /// <summary>
        /// Gets or sets the admissions rep.
        /// </summary>
        public string AdmissionsRep { get;  set; }

        /// <summary>
        /// Gets or sets the campus of interest.
        /// </summary>
        public string CampusOfInterest { get;  set; }

        /// <summary>
        /// Gets or sets the program of interest.
        /// </summary>
        public string ProgramOfInterest { get;  set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the acquired date.
        /// </summary>
        public string AcquiredDate { get; set; }

        /// <summary>
        /// Gets or sets the from.
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// Gets or sets the campus.
        /// </summary>
        public string Campus { get; set; }

        /// <summary>
        /// Clean null values from fields and replace by empty string
        /// </summary>
        /// <param name="mod">
        /// Lead Duplicates Output Model
        /// </param>
        /// <returns>
        /// Return Lead Duplicates Output Model with out null
        /// </returns>
        public static LeadDuplicatesOutputModel CleanNull(LeadDuplicatesOutputModel mod)
        {
            mod.FullName = mod.FullName ?? string.Empty;
            mod.Address1 = mod.Address1 ?? string.Empty;
            mod.Address2 = mod.Address2 ?? string.Empty;
            mod.City = mod.City ?? string.Empty;
            mod.State = mod.State ?? string.Empty;
            mod.Status = mod.Status ?? string.Empty;
            mod.Phone = mod.Phone ?? string.Empty;
            mod.Email = mod.Email ?? string.Empty;
            mod.SourceInfo = mod.SourceInfo ?? string.Empty;
            mod.AdmissionsRep = mod.AdmissionsRep ?? string.Empty;
            mod.CampusOfInterest = mod.CampusOfInterest ?? string.Empty;
            mod.ProgramOfInterest = mod.ProgramOfInterest ?? string.Empty;
            mod.Comments = mod.Comments ?? string.Empty;
            mod.AcquiredDate = mod.AcquiredDate ?? string.Empty;
            mod.From = mod.From ?? string.Empty;
            mod.Campus = mod.Campus ?? string.Empty;
            return mod;
        }
    }
}
