﻿using System;

namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// Get Lead Input Model used as filter in get operations
    /// </summary>
    public class GetLeadInputModel
    {
        public int? VendorId { get; set; }
        public string AreaOfInterest { get; set; }
        public Guid CampusId { get; set; }
        public Guid RepId { get; set; }
        public string LastModDate { get; set; }
        
        /// <summary>
        /// Lead ID : use in Lead Info Bar
        /// </summary>
        public Guid LeadGuid { get; set; }
       
    }
}
