﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEducationFilterInputModel.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Messages.Lead.Education.LeadEducationFilterInputModel
// </copyright>
// <summary>
//   The lead education filter input model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.Education
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The lead education filter input model.
    /// </summary>
    public class LeadEducationFilterInputModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        public Guid LeadId { get; set; }

        /// <summary>
        /// Gets or sets the student id.
        /// </summary>
        public Guid StudentId { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }
    }
}
