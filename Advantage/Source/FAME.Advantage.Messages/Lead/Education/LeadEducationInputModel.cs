﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEducationInputModel.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Messages.Lead.Education.LeadEducationInputModel
// </copyright>
// <summary>
//   The lead education input model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.Education
{
    using System;

    /// <summary>
    /// The lead education input model.
    /// </summary>
    public class LeadEducationInputModel
    {
        /// <summary>
        /// Gets or sets the lead education id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the lead Id
        /// </summary>
        public Guid LeadId { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the Level Id
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Gets or sets the type id.
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// Gets or sets the Institution
        /// </summary>
        public Guid InstitutionId { get; set; }

        /// <summary>
        /// Gets or sets the institution name (if institution id is empty, used this to create a new institution).
        /// </summary>
        public string InstitutionName { get; set; }

        /// <summary>
        /// Gets or sets the major.
        /// </summary>
        public string Major { get; set; }

        /// <summary>
        /// Gets or sets the final grade.
        /// </summary>
        public string FinalGrade { get; set; }

        /// <summary>
        /// Gets or sets the GPA.
        /// </summary>
        public decimal? Gpa { get; set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        public int? Rank { get; set; }

        /// <summary>
        /// Gets or sets the percentile.
        /// </summary>
        public int? Percentile { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether graduate.
        /// </summary>
        public bool Graduate { get; set; }

        /// <summary>
        /// Gets or sets the graduation date.
        /// </summary>
        public string GraduationDate { get; set; }

        /// <summary>
        /// Gets or sets the certificate or degree.
        /// </summary>
        public string CertificateOrDegree { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is institution added to mru.
        /// </summary>
        public bool IsInstitutionAddedToMru { get; set; }
    }
}
