﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEducationOutputModel.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Messages.Lead.Education.LeadEducationOutputModel
// </copyright>
// <summary>
//   The lead education output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.Education
{
    using System;
    using SystemStuff.Institution;

    /// <summary>
    /// The lead education output model.
    /// </summary>
    public class LeadEducationOutputModel
    {
        /// <summary>
        /// Gets or sets the Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        public Guid LeadId { get; set; }

        /// <summary>
        /// Gets or sets the Level Id
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Gets or sets the Level Description
        /// </summary>
        public string Level { get; set; }

        /// <summary>
        /// Gets or sets the Institution
        /// </summary>
        public InstitutionOutputModel Institution { get; set; }

        /// <summary>
        /// Gets or sets the major.
        /// </summary>
        public string Major { get; set; }

        /// <summary>
        /// Gets or sets the final grade.
        /// </summary>
        public string FinalGrade { get; set; }

        /// <summary>
        /// Gets or sets the GPA.
        /// </summary>
        public decimal Gpa { get; set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        public int Rank { get; set; }

        /// <summary>
        /// Gets or sets the percentile.
        /// </summary>
        public int Percentile { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether graduate.
        /// </summary>
        public bool Graduate { get; set; }

        /// <summary>
        /// Gets or sets the graduation date.
        /// </summary>
        public string GraduationDate { get; set; }

        /// <summary>
        /// Gets or sets the certificate or degree.
        /// </summary>
        public string CertificateOrDegree { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Gets the institution name.
        /// </summary>
        public string InstitutionName { get { return this.Institution != null ? this.Institution.Name : string.Empty; }
        }
    }
}
