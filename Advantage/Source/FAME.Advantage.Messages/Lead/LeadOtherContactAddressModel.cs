﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FAME.Advantage.Messages.Lead
{
    public class LeadOtherContactAddressModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public string OtherContactsAddresesId { get; set; }

        /// <summary>
        /// ID Of the Lead Other Contact
        /// </summary>
        public string OtherContactId { get; set; }
        /// <summary>
        /// Id of the Lead
        /// </summary>
        public string LeadId { get; set; }
        /// <summary>
        /// Address Type Id
        /// </summary>
        public string AddressTypeId { get; set; }
        /// <summary>
        /// Address Type
        /// </summary>
        public string AddressType { get; set; }
        /// <summary>
        /// Address 1
        /// </summary>
        public string Address1 { get; set; }
        /// <summary>
        /// Address 2
        /// </summary>
        public string Address2 { get; set; }
        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// State ID
        /// </summary>
        public string StateId { get; set; }
        /// <summary>
        /// State
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// The States Name if the address is an international address
        /// </summary>
        public string StateInternational { get; set; }
        /// <summary>
        /// The Country Name if the address is an international adddress
        /// </summary>
        public string CountryInternational { get; set; }
        /// <summary>
        /// /// The County Name if the address is an international adddress
        /// </summary>
        public string CountyInternational { get; set; }
        /// <summary>
        /// Zip Code
        /// </summary>
        public string ZipCode { get; set; }
        /// <summary>
        /// Country ID
        /// </summary>
        public string CountryId { get; set; }
        /// <summary>
        /// Country
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// County Id
        /// </summary>
        public string CountyId { get; set; }
        /// <summary>
        /// County
        /// </summary>
        public string County { get; set; }
        /// <summary>
        /// Status ID
        /// </summary>
        public string StatusId { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Flag Indicating if this is the mailing address
        /// </summary>
        public bool IsMailingAddress { get; set; }
        /// <summary>
        /// Flag indicating if this is an international address
        /// </summary>
        public bool IsInternational { get; set; }
        /// <summary>
        /// User last modified this object
        /// </summary>
        public string ModUser { get; set; }
        /// <summary>
        /// Last Modified Date
        /// </summary>
        public DateTime ModDate { get; set; }
    }
}
