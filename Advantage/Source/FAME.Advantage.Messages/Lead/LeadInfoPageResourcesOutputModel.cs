﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadInfoPageResourcesOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Get Resources for Lead Info Page
//   This resources are used to configure
//   the general architecture of the page.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    using System.Collections.Generic;
    using SystemStuff.Resources;
    using SystemStuff.SchoolDefinedFields;

    /// <summary>
    /// Get Resources for Lead Info Page
    /// This resources are used to configure
    /// the general architecture of the page.
    /// </summary>
    public class LeadInfoPageResourcesOutputModel
    {
        /// <summary>
        /// Gets or sets the information to construct the controls for School defined field
        /// </summary>
        public IList<SdfOutputModel> SdfList { get; set; }

        /// <summary>
        ///  Gets or sets the information to defined the Required Field and the combo-box items list
        /// </summary>
        public IList<ResourcesRequiredAndDllOutputModel> RequiredList { get; set; }
        
        /// <summary>
        ///  Gets or sets the Lead Group list
        /// </summary>
        public IList<LeadGroupInfoOutputModel> LeadGroupList { get; set; }
    }
}
