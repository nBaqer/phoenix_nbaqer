﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadTransactionDetailOutput.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Messages.Lead.LeadTransactionDetailOuput
// </copyright>
// <summary>
//   The lead transaction detail ouput.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The lead transaction detail output.
    /// </summary>
    public class LeadTransactionDetailOutput
    {
        /// <summary>
        /// Gets or sets the transaction id.
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// Gets or sets the payment type.
        /// </summary>
        public string PaymentType { get; set; }

        /// <summary>
        /// Gets or sets the transaction code.
        /// </summary>
        public string TransactionCode { get; set; }

        /// <summary>
        /// Gets or sets the document id.
        /// </summary>
        public string DocumentId { get; set; }

        /// <summary>
        /// Gets or sets the amount received.
        /// </summary>
        public string AmountReceived { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether voided.
        /// </summary>
        public bool Voided { get; set; }
    }
}
