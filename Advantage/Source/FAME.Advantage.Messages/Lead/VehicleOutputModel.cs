﻿using System;

namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// Output Class for Vehicles
    /// </summary>
    public class VehicleOutputModel
    {
        /// <summary>
        /// vehicle unique ID
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// 1,2 are show in Vehicle1 Vehicle 2 in the interface
        /// Other values are not show
        /// LeadId and Position together are uniques.
        /// </summary>
        public int Position { get;  set; }

        /// <summary>
        /// Parking Permit
        /// </summary>
        public string Permit { get; set; }

        /// <summary>
        /// Vehicle Maker
        /// </summary>
        public  string Make { get;  set; }

        /// <summary>
        /// Vehicle Model
        /// </summary>
        public  string Model { get;  set; }

        /// <summary>
        /// Vehicle Color
        /// </summary>
        public  string Color { get;  set; }

        /// <summary>
        /// Vehicle Plate
        /// </summary>
        public  string Plate { get;  set; }

        /// <summary>
        /// Modification user.
        /// </summary>
        public string ModUser { get;  set; }

        /// <summary>
        /// Modification Date
        /// </summary>
        public DateTime ModDate { get; set; }


        /// <summary>
        /// Lead associated with the vehicle.
        /// </summary>
        public  string LeadId { get;  set; }

    }
}
