﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadNewDuplicatesOutputModel.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   This class hold old possible duplicates with the possible duplicate
//   if the possible duplicate has more than one, then the appear repeated
//   the possible duplicate each time that duplicates has.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    using System;

    /// <summary>
    /// This class hold old possible duplicates with the possible duplicate
    /// if the possible duplicate has more than one, then the appear repeated
    /// the possible duplicate each time that duplicates has. 
    /// </summary>
    public class LeadNewDuplicatesOutputModel
    {
        /// <summary>
        /// Gets or sets New Lead GUID
        /// </summary>
        public Guid NewLeadGuid { get;  set; }

        /// <summary>
        /// Gets or sets the Lead GUID of possible duplicate
        /// </summary>
        public Guid DuplicateGuid { get;  set; }

        /// <summary>
        /// Gets or sets the possible duplicate full Name
        /// </summary>
        public string PosibleDuplicateFullName { get;  set; }
    }
}
