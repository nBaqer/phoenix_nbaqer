﻿using System;

namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// LEad Group Output Model
    /// </summary>
    public class LeadGroupInfoOutputModel
    {
        /// <summary>
        /// The group Id
        /// </summary>
        public Guid GroupId { get; set; }

        /// <summary>
        /// Description of the group
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The campus ID.
        /// </summary>
        public string CampusId { get; set; }

    }
}
