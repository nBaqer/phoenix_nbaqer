﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadTransactionOutput.cs" company="FAME Inc.">
//   FAME Inc 2016
//   FAME.Advantage.Messages.Lead.LeadTransactionOutput
// </copyright>
// <summary>
//   The lead transaction output.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The lead transaction output.
    /// </summary>
    public class LeadTransactionOutput
    {
        /// <summary>
        /// Gets or sets the school name.
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the city state zip.
        /// </summary>
        public string CityStateZip { get; set; }

        /// <summary>
        /// Gets or sets the lead name.
        /// </summary>
        public string LeadName { get; set; }

        /// <summary>
        /// Gets or sets the transaction date.
        /// </summary>
        public string TransDate { get; set; }

        /// <summary>
        /// Gets or sets the transactions.
        /// </summary>
        public List<LeadTransactionDetailOutput> Transactions { get; set; }

        /// <summary>
        /// Gets or sets the lead address.
        /// </summary>
        public string LeadAddress { get; set; }
        /// <summary>
        /// Gets or sets the lead address.
        /// </summary>
        public string LeadAddress2 { get; set; }

    }
}
