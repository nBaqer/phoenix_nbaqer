﻿using System.Collections.Generic;
using FAME.Advantage.Messages.Common;

namespace FAME.Advantage.Messages.Lead
{

    /// <summary>
    /// 
    /// </summary>
    public class LeadContactsOutputModel
    {
        public string LeadId { get; set; }
        /// <summary>
        /// List of Existing Lead Phone Numbers
        /// </summary>
        public List<LeadPhonesOutputModel> LeadPhonesList { get; set; }
        /// <summary>
        /// List of Existing Lead Emails 
        /// </summary>
        public List<LeadEmailOutputModel> LeadEmailList { get; set; }
        /// <summary>
        /// List of Existing Lead Address
        /// </summary>
        public List<LeadAddressOutputModel> LeadAddressList { get; set; }
        /// <summary>
        /// List of Phone Types
        /// </summary>
        public List<SystemStuff.SystemStatuses.SystemPhoneTypeOuputModel> PhoneTypes { get; set; }
        /// <summary>
        /// List of Address Types
        /// </summary>
        public List<SystemStuff.SystemStatuses.SystemAddressTypeOutputModel> AddressTypes { get; set; }
        /// <summary>
        /// List of Email types
        /// </summary>
        public List<SystemStuff.SystemStatuses.SystemEmailTypeOutputModel> EmailTypes { get; set; }
        /// <summary>
        /// List of Statuses
        /// </summary>
        public List<DropDownOutputModel> Statuses { get; set; }
        /// <summary>
        /// List of Countries
        /// </summary>
        public List<DropDownOutputModel> Countries { get; set; }
        /// <summary>
        /// List of States
        /// </summary>
        public List<DropDownOutputModel> States { get; set; }
        /// <summary>
        /// Lead Comments
        /// </summary>
        public string Comments { get; set; }
        /// <summary>
        /// List of Counties
        /// </summary>
        public List<DropDownOutputModel> Counties { get; set; }
        /// <summary>
        /// List of Contact Types
        /// </summary>
        public List<DropDownOutputModel> ContactTypes { get; set; }
        /// <summary>
        /// List of Sufixes
        /// </summary>
        public List<DropDownOutputModel> Sufixes { get; set; }
        /// <summary>
        /// List of Prefixes
        /// </summary>
        public List<DropDownOutputModel> Prefixes { get; set; }
        /// <summary>
        /// List of Relationships
        /// </summary>
        public List<DropDownOutputModel> Relationships { get; set; }
    }
}
