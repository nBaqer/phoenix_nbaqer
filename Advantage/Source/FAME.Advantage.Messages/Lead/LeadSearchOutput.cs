﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadSearchOutput.cs"  company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the LeadSearchOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    using System;

    /// <summary>
    ///  The lead search output model.
    /// </summary>
    public class LeadSearchOutputModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; protected set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        public string FullName { get; protected set; }

        /// <summary>
        /// Gets or sets the field 1.
        /// </summary>
        public string Field1 { get; protected set; } // SSN

        /// <summary>
        /// Gets or sets the field 2.
        /// </summary>
        public string Field2 { get; protected set; } // ExpectedStart

        /// <summary>
        /// Gets or sets the field 3.
        /// </summary>
        public string Field3 { get; protected set; } // status

        /// <summary>
        /// Gets or sets the field 4.
        /// </summary>
        public string Field4
        {
            get { return string.Empty; }
            set { value = string.Empty; }
        } // nothing

        /// <summary>
        /// Gets or sets the field 5.
        /// </summary>
        public string Field5 { get; protected set; } // campus

        /// <summary>
        /// Gets or sets the Image 64.
        /// return the image code in Base 64 
        /// </summary>
        public string Image64 { get; protected set; }

        // <summary>
        // Gets or sets the image URL.
        // </summary>
        public string ImageUrl { get; protected set; }

        // <summary>
        // Gets or sets the not found image URL.
        // </summary>
        public string NotFoundImageUrl { get; protected set; }

        /// <summary>
        /// Gets or sets the search campus ID.
        /// </summary>
        public string SearchCampusId { get; protected set; }
    }
}