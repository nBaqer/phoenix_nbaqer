﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Messages.Common;

namespace FAME.Advantage.Messages.Lead
{
    public class LeadOtherContactModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public string OtherContactId { get; set; }

        /// <summary>
        /// Lead ID
        /// </summary>
        public string LeadId { get; set; }

        /// <summary>
        /// Status ID
        /// </summary>
        public string StatusId { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Relationship ID
        /// </summary>
        public string RelationshipId { get; set; }

        /// <summary>
        /// Relationship 
        /// </summary>
        public string Relationship { get; set; }

        /// <summary>
        /// Contact Type Id
        /// </summary>
        public string ContactTypeId { get; set; }

        /// <summary>
        /// Contact Type
        /// </summary>
        public string ContactType { get; set; }

        /// <summary>
        /// Prefix Id
        /// </summary>
        public string PrefixId { get; set; }

        /// <summary>
        /// Prefix
        /// </summary>
        public string Prefix { get; set; }

        /// <summary>
        /// Sufix ID
        /// </summary>
        public string SufixId { get; set; }

        /// <summary>
        /// Sufix
        /// </summary>
        public string Sufix { get; set; }

        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName { get; set; }


        /// <summary>
        /// Full Name(including Sufix and Prefix)
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Middle Name
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Comments
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// User last modified this object
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        /// Last Modified Date
        /// </summary>
        public DateTime ModDate { get; set; }

        /// <summary>
        /// Lead Other Contact Phones 
        /// </summary>
        public IList<LeadOtherContactPhoneModel> Phones { get; set; }

        /// <summary>
        /// Lead Other Contact Emails 
        /// </summary>
        public IList<LeadOtherContactEmailModel> Emails { get; set; }

        /// <summary>
        /// Lead Other Contact Addresses
        /// </summary>
        public IList<LeadOtherContactAddressModel> Addresses { get; set; }


    }
}
