﻿#region header
// // --------------------------------------------------------------------------------------------
// // Advantage - FAME.Advantage.Messages - IExtraCurricularOutputModel.cs
// // Author: JAGG
// // © Copyright FAME 2014, 2016
// //---------------------------------------------------------------------------------------------
#endregion

using System.Collections.Generic;
using FAME.Advantage.Messages.Common;

namespace FAME.Advantage.Messages.Lead.ExtraCurricular
{
    /// <summary>
    /// Info to be returned to client
    /// </summary>
    public interface IExtraInfoOutputModel
    {
        /// <summary>
        /// List of Extra Curricular Info
        /// </summary>
        IList<ExtraInfo> ExtraInfoList { get; set; }

        /// <summary>
        /// List of Levels items 
        /// </summary>
        IList<DropDownOutputModel> LevelsItemsList { get; set; }

        /// <summary>
        /// List of Group items
        /// </summary>
        IList<DropDownOutputModel> GroupItemsList { get; set; }

        /// <summary>
        /// Used on Post Operation
        /// </summary>
        ExtraInfoInputModel Filter { get; set; }
        
        ///// <summary>
        ///// This is used only when the class is used as input in Post
        ///// 1: Update ExtraCurricular
        ///// 2: Delete Extracurricular
        ///// 3: Insert ExtraCurricular
        ///// </summary>
        //int Command { get; set; }

        ///// <summary>
        ///// UserId responsible for the operation.
        ///// Used only when output model is used as filter.
        ///// </summary>
        //string UserId { get; set; }

        ///// <summary>
        ///// Lead Id associated wit the extra curricular
        ///// Used only when output model is used as input
        ///// </summary>
        //string LeadId { get; set; }
    }
}