﻿namespace FAME.Advantage.Messages.Lead.ExtraCurricular
{
    /// <summary>
    /// Filter input model for Extracurricular
    /// </summary>
    public class ExtraInfoInputModel
    {
        /// <summary>
        /// The Lead Id
        /// </summary>
        public string LeadId { get; set; }

        /// <summary>
        /// The CampusId
        /// </summary>
        public string CampusId { get; set; }

        /// <summary>
        /// User id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///  GET Command =
        ///  1: Get the Extra values
        ///  2: Get the Levels
        ///  3: Get the Groups
        ///  4: Get Levels and Groups
        ///  POST Commands
        /// 1: Update ExtraCurricular
        /// 2: Delete Extracurricular
        /// 3: Insert ExtraCurricular
        /// </summary>
        public int Command { get; set; }
    }
}
