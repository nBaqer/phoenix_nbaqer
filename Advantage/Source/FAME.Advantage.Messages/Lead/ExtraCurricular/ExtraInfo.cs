﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExtraInfo.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Info to be returned to client about the Extra curricular info
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.ExtraCurricular
{
    using Common;

    /// <summary>
    /// Info to be returned to client about the Extra curricular info
    /// </summary>
    public class ExtraInfo
    {
        /// <summary>
        /// Gets or sets ID of Extra Curricular
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///  Gets or sets Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///  Gets or sets Comment
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        ///  Gets or sets Level Description
        /// </summary>
        public DropDownOutputModel Level { get; set; }

        /// <summary>
        ///  Gets or sets Group Description
        /// </summary>
        public DropDownOutputModel Group { get; set; }
    }
}
