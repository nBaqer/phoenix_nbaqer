﻿using System.Collections.Generic;
using FAME.Advantage.Messages.Common;

namespace FAME.Advantage.Messages.Lead.ExtraCurricular
{
    /// <summary>
    /// Extra curricular output model
    /// </summary>
    public class ExtraInfoOutputModel : IExtraInfoOutputModel
    {
        /// <summary>
        /// Class Factory.
        /// </summary>
        /// <returns></returns>
        public static IExtraInfoOutputModel Factory()
        {
            var item = new ExtraInfoOutputModel
            {
                ExtraInfoList = new List<ExtraInfo>(),
                GroupItemsList = new List<DropDownOutputModel>(),
                LevelsItemsList = new List<DropDownOutputModel>(),
                Filter = new ExtraInfoInputModel()
            };

            return item;
        }
        
        #region Implementation of IExtraCurricularOutputModel

        /// <summary>
        /// List of Extra Curricular Info
        /// </summary>
        public IList<ExtraInfo> ExtraInfoList { get; set; }

        /// <summary>
        /// List of Levels items 
        /// </summary>
        public IList<DropDownOutputModel> LevelsItemsList { get; set; }

        /// <summary>
        /// List of Group items
        /// </summary>
        public IList<DropDownOutputModel> GroupItemsList { get; set; }

        /// <summary>
        /// Used on Post Operation
        /// </summary>
        public ExtraInfoInputModel Filter { get; set; }

        ///// <summary>
        ///// This is used only when the class is used as input in Post
        ///// 1: Update ExtraCurricular
        ///// 2: Delete Extracurricular
        ///// 3: Insert ExtraCurricular
        ///// </summary>
        //public int Command { get; set; }

        ///// <summary>
        ///// UserId responsible for the operation.
        ///// Used only when output model is used as filter.
        ///// </summary>
        //public string UserId { get; set; }

        ///// <summary>
        ///// Lead Id associated wit the extra curricular
        ///// Used only when output model is used as input
        ///// </summary>
        //public string LeadId { get; set; }

        #endregion
    }
}
