﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPriorWorkItemOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the IPriorWorkItemOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.PriorWork
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.Messages.Common;

    /// <summary>
    /// The PriorWorkItemOutputModel interface.
    /// </summary>
    public interface IPriorWorkItemOutputModel
    {
        /// <summary>
        /// Gets or sets the address output model.
        /// </summary>
        PriorWorkAddressOutputModel AddressOutputModel { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        string Comments { get; set; }

        /// <summary>
        /// Gets or sets the contacts.
        /// </summary>
        IList<PriorWorkContactOutputModel> Contacts { get; set; }

        /// <summary>
        /// Gets or sets the employer.
        /// </summary>
        string Employer { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        string Id { get; set; }

        /// <summary>
        /// Gets or sets the job responsibilities.
        /// </summary>
        string JobResponsabilities { get; set; }

        /// <summary>
        /// Gets or sets the job status.
        /// </summary>
        DropDownOutputModel JobStatus { get; set; }

        /// <summary>
        /// Gets or sets the job title.
        /// </summary>
        string JobTitle { get; set; }

        /// <summary>
        /// Gets or sets the school job title.
        /// </summary>
        DropDownOutputModel SchoolJobTitle { get; set; }
    }
}