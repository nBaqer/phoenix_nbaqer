﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PriorWorkItemOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The prior work item output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.PriorWork
{
    using System;
    using System.Collections.Generic;
    using Common;

    /// <summary>
    /// The prior work item output model.
    /// </summary>
    public class PriorWorkItemOutputModel : IPriorWorkItemOutputModel
    {
        /// <summary>
        /// Gets or sets The Prior Work Id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the address output model.
        /// </summary>
        public PriorWorkAddressOutputModel AddressOutputModel { get; set; }

        /// <summary>
        /// Gets or sets Start Date for work
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the job status.
        /// </summary>
        public DropDownOutputModel JobStatus { get; set; }

        /// <summary>
        /// Gets or sets the job title.
        /// </summary>
        public string JobTitle { get; set; }

        /// <summary>
        /// Gets or sets the employer.
        /// </summary>
        public string Employer { get; set; }

        /// <summary>
        /// Gets or sets the school job title.
        /// </summary>
        public DropDownOutputModel SchoolJobTitle { get; set; }

        /// <summary>
        /// Gets or sets the contacts.
        /// </summary>
        public IList<PriorWorkContactOutputModel> Contacts { get; set; }

        /// <summary>
        /// Gets or sets the job responsibilities.
        /// </summary>
        public string JobResponsabilities { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        ///  The factory.
        /// </summary>
        /// <returns>
        /// The <see cref="IPriorWorkItemOutputModel"/>.
        /// </returns>
        public static IPriorWorkItemOutputModel Factory()
        {
            IPriorWorkItemOutputModel fac = new PriorWorkItemOutputModel();
            fac.AddressOutputModel = new PriorWorkAddressOutputModel();
            fac.JobStatus = new DropDownOutputModel();
            fac.SchoolJobTitle = new DropDownOutputModel();
            return fac;
        }
    }
}
