﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PriorWorkInputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the PriorWorkInputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.PriorWork
{
    /// <summary>
    /// The prior work input model.
    /// </summary>
    public class PriorWorkInputModel
    {
        /// <summary>
        /// Gets or sets the command.
        /// </summary>
        public int Command { get; set; }

        /// <summary>
        /// Gets or sets the Lead
        /// </summary>
        public string LeadId { get; set; }

        /// <summary>
        /// Gets or sets the campus Id
        /// </summary>
        public string CampusId { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///  Gets or sets the Prior Work id to delete
        /// </summary>
        public string PriorWorkId { get; set; }
    }
}
