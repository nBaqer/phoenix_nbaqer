﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PriorWorkOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The prior work output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.PriorWork
{
    using System.Collections.Generic;
    using Common;

    /// <summary>
    /// The prior work output model.
    /// </summary>
    public class PriorWorkOutputModel : IPriorWorkOutputModel
    {
        #region Implementation of IPriorWorkOutputModel

        /// <summary>
        /// Gets or sets the input model.
        /// </summary>
        public PriorWorkInputModel InputModel { get; set; }

        /// <summary>
        /// Gets or sets the prior work item list.
        /// </summary>
        public IList<PriorWorkItemOutputModel> PriorWorkItemList { get; set; }

        /// <summary>
        /// Gets or sets the job status item list.
        /// </summary>
        public IList<DropDownOutputModel> JobStatusItemList { get; set; }

        /// <summary>
        /// Gets or sets the ad titles item list.
        /// </summary>
        public IList<DropDownOutputModel> AdSchoolJobTitlesItemList { get; set; }

        /// <summary>
        /// Gets or sets the states list.
        /// </summary>
        public IEnumerable<DropDownOutputModel> StatesList { get; set; }

        /// <summary>
        /// Gets or sets the countries list.
        /// </summary>
        public IEnumerable<DropDownOutputModel> CountriesList { get; set; }

        /// <summary>
        /// Gets or sets the prior work detail object.
        /// this contain Prior Responsibilities, Comments and Contact
        /// </summary>
        public PriorWorkDetailItemOutputModel PriorWorkDetailObject { get; set; }

        /// <summary>
        /// Gets or sets the custom list.
        /// </summary>
        public IList<LeadSdfOutputModel> SdfList { get; set; }

        /// <summary>
        /// Gets or sets the prefix list.
        /// </summary>
        public IList<DropDownOutputModel> PrefixList { get; set; }
        #endregion

        /// <summary>
        ///  The factory.
        /// </summary>
        /// <returns>
        /// The <see cref="IPriorWorkOutputModel"/>.
        /// A formatted Prior Output Model object
        /// </returns>
        public static IPriorWorkOutputModel Factory()
        {
            IPriorWorkOutputModel fac = new PriorWorkOutputModel();
            fac.PriorWorkItemList = new List<PriorWorkItemOutputModel>();
            fac.AdSchoolJobTitlesItemList = new List<DropDownOutputModel>();
            fac.CountriesList = new List<DropDownOutputModel>();
            fac.StatesList = new List<DropDownOutputModel>();
            fac.InputModel = new PriorWorkInputModel();
            return fac;
        }
    }
}
