﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PriorWorkDetailItemOutputModel.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Defines the PriorWorkDetailItemOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.PriorWork
{
    using System.Collections.Generic;

    /// <summary>
    ///  The prior work detail item output model.
    /// </summary>
    public class PriorWorkDetailItemOutputModel
    {
        /// <summary>
        /// Gets or sets the contact id.
        /// </summary>
        public int ContactId { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the employer.
        /// </summary>
        public string Employer { get; set; }

        /// <summary>
        /// Gets or sets the job responsibilities.
        /// </summary>
        public string JobResponsabilities { get; set; }

        /// <summary>
        /// Gets or sets the contact list.
        /// </summary>
        public IList<PriorWorkContactOutputModel> ContactList { get; set; }
    }
}