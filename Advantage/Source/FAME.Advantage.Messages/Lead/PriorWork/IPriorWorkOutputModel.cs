﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPriorWorkOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the IPriorWorkOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Messages.Lead.PriorWork
{
    using System.Collections.Generic;

    using FAME.Advantage.Messages.Common;

    /// <summary>
    /// The PriorWorkOutputModel interface.
    /// </summary>
    public interface IPriorWorkOutputModel
    {
        /// <summary>
        /// Gets or sets the input model.
        /// </summary>
        PriorWorkInputModel InputModel { get; set; }

        /// <summary>
        /// Gets or sets the prior work item list.
        /// </summary>
        IList<PriorWorkItemOutputModel> PriorWorkItemList { get; set; }

        /// <summary>
        /// Gets or sets the job status item list.
        /// </summary>
        IList<DropDownOutputModel> JobStatusItemList { get; set; }

        /// <summary>
        /// Gets or sets the ad titles item list.
        /// </summary>
        IList<DropDownOutputModel> AdSchoolJobTitlesItemList { get; set; }

        /// <summary>
        /// Gets or sets the states list.
        /// </summary>
        IEnumerable<DropDownOutputModel> StatesList { get; set; }

        /// <summary>
        /// Gets or sets the countries list.
        /// </summary>
        IEnumerable<DropDownOutputModel> CountriesList { get; set; }

        /// <summary>
        /// Gets or sets the prior work detail object.
        /// </summary>
        PriorWorkDetailItemOutputModel PriorWorkDetailObject { get; set; }

        /// <summary>
        /// Gets or sets the custom field list.
        /// </summary>
        IList<LeadSdfOutputModel> SdfList { get; set; }
    }
}
