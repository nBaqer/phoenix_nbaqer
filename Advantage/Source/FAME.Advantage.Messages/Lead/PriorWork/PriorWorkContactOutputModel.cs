﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PriorWorkContactOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The prior work contact output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.PriorWork
{
    using FAME.Advantage.Messages.Common;

    /// <summary>
    /// The prior work contact output model.
    /// </summary>
    public class PriorWorkContactOutputModel
    {
        /// <summary>
        /// Gets or sets the contact id.
        /// </summary>
        public int ContactId { get; set; }

        /// <summary>
        /// Gets or sets the prior work id.
        /// This is the prior Work ID that the
        /// contact is linked
        /// </summary>
        public string PriorWorkId { get; set; }

        /// <summary>
        /// Gets or sets the title of the contact.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the middle name.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is phone international.
        /// </summary>
        public bool IsPhoneInternational { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the is active.
        /// </summary>
        public int IsActive { get; set; }

        /// <summary>
        /// Gets or sets the prefix.
        /// </summary>
        public DropDownOutputModel Prefix { get; set; }
    }
}
