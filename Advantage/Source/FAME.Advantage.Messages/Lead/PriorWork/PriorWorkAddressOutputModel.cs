﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PriorWorkAddressOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the PriorWorkAddressOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.PriorWork
{
    using System;

    using FAME.Advantage.Messages.Common;

    /// <summary>
    /// The prior work address output model.
    /// </summary>
    public class PriorWorkAddressOutputModel
    {
        /// <summary>
        /// Gets or sets the address id.
        /// </summary>
        public int AddressId { get; set; }

        /// <summary>
        /// Gets or sets the prior work id.
        /// This is the prior Work ID that the
        /// contact is linked
        /// </summary>
        public string PriorWorkId { get; set; }

        /// <summary>
        /// Gets or sets the complete address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets First Part of the Address (max 250 characters)
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the address Apartment.
        /// </summary>
        public string AddressApto { get; set; }

        /// <summary>
        /// Gets or sets Second Part of the Address (max 250 characters)
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets City of the Address
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is international.
        /// </summary>
        public bool IsInternational { get; set; }

        /// <summary>
        /// Gets or sets State Name of the Address
        /// </summary>
        public DropDownOutputModel State { get; set; }

        /// <summary>
        ///  Gets or sets the international state
        /// </summary>
        public string StateInternational { get; set; }

        /// <summary>
        /// Gets or sets Country name of the Address
        /// </summary>
        public DropDownOutputModel Country { get; set; }

        /// <summary>
        /// Gets or sets Zip Code of the Address
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets or sets Last Modification Date
        /// </summary>
        public DateTime ModDate { get; set; }

        /// <summary>
        /// Gets or sets Last User Modified this record.
        /// </summary>
        public string Moduser { get; set; }
    }
}
