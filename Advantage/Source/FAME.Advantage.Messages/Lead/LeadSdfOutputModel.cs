﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadSdfOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Output Model for SDF fields.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// Output Model for SDF fields.
    /// </summary>
    public class LeadSdfOutputModel 
    {
        /// <summary>
        ///  Gets or sets the ID of the SDF control
        /// Represent the SDFID field in the View
        /// <code>VIEW_SySDFModuleValue_Lead</code>
        /// </summary>
        public string SdfId { get; set; }

        /// <summary>
        ///  Gets or sets the Value of the control
        /// (this value is the text in text box
        /// and the text field in combo box)
        /// </summary>
        public string SdfValue { get; set; }

        /// <summary>
        /// Gets or sets the data type id.
        /// </summary>
        public int DtypeId { get; set; }

        /// <summary>
        /// Gets or sets the length.
        /// </summary>
        public int Len { get; set; }
    }
}