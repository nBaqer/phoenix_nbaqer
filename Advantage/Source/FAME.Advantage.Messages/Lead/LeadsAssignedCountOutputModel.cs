﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadsAssignedCountOutputModel.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   The leads assigned count output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// The leads assigned count output model.
    /// </summary>
    public class LeadsAssignedCountOutputModel
    {
        /// <summary>
        /// Gets or sets the not assigned campus count.
        /// </summary>
        public int NotAssignedCampusCount { get; set; }

        /// <summary>
        /// Gets or sets the not assigned represent count.
        /// </summary>
        public int NotAssignedRepCount { get; set; }
    }
}
