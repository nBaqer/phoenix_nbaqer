﻿namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// Requirement output model for Info Bar.
    /// </summary>
    public class LeadInfoRequirementOutputModel
    {
        /// <summary>
        /// The name of the requirement
        /// </summary>
        public string RequirementName { get; set; }

        /// <summary>
        /// The type of requirement: 
        /// 1: Test
        /// 2: Documents
        /// </summary>
        public int TypeOfRequirement { get; set; }

        /// <summary>
        /// Status of the requirement
        /// 1: NO PASSED
        /// 2: OVERRIDED
        /// 3: PASSED
        /// Category 2 and 3 are PASSED
        /// </summary>
        public int RequirementStatus { get; set; }

        /// <summary>
        /// true if it is mandatory
        /// false optional
        /// </summary>
        public bool IsMandatory { get; set; }

        /// <summary>
        /// Give a image associate with Requirement Status
        /// </summary>
        public string ImagePath { get; set; }
    }
    
}
