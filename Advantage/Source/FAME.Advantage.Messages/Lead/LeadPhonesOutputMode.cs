﻿namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// The Phone output for Leads
    /// Each of this field are associated with a lead in a list.
    /// </summary>
    public class LeadPhonesOutputModel
    {
        /// <summary>
        /// Phone ID.
        /// </summary>
        public string ID { get; set; }
        
        /// <summary>
        /// The phone number
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// The extension of the phone
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// If the phone is not USA format
        /// </summary>
        public bool IsForeignPhone { get; set; }

        /// <summary>
        /// The relative position of the phone in the screen
        /// Example the position 1 in LeadInfoPage is the default phone
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// The phone type ID guid with the identifier of type
        /// </summary>
        public string PhoneTypeId { get; set; }
        /// <summary>
        /// The phone type description
        /// </summary>
        public string PhoneType { get; set; }
        /// <summary>
        /// The phone status description
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// The phone status id
        /// </summary>
        public string StatusId { get; set; }
        /// <summary>
        /// Flag indicating wether to show the phone on lead page or not
        /// </summary>
        public string ShowOnLeadPage { get; set; }
        /// <summary>
        /// Flag indicating wether this phone is the best one to contact or not
        /// </summary>
        public string Best { get; set; }
    }
}
