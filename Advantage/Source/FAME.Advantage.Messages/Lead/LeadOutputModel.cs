﻿using System;

namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// Other class for lead Output
    /// </summary>
    public class LeadOutputModel
    {
        /// <summary>
        /// id
        /// </summary>
        public Guid Id { get;  set; }
        /// <summary>
        /// full name
        /// </summary>
        public string FullName { get;  set; }
        /// <summary>
        /// Middle name
        /// </summary>
        public string FirstMiddleLastName { get; set; }
        
        /// <summary>
        /// Program of interest
        /// </summary>
        //public string ProgramOfInterest { get;  set; }
        
        /// <summary>
        /// Program version
        /// </summary>
        public string ProgramVersion { get;  set; }
        
        /// <summary>
        /// area of interest
        /// </summary>
        public string AreaOfInterest { get;  set; }
        
        /// <summary>
        /// Campus
        /// </summary>
        public string Campus { get; set; }
        
        /// <summary>
        /// City
        /// </summary>
        public string City { get;  set; }
        
        /// <summary>
        /// State
        /// </summary>
        public string State { get;  set; }
        
        /// <summary>
        /// Zip
        /// </summary>
        public string Zip { get;  set; }
        

      
    }
}