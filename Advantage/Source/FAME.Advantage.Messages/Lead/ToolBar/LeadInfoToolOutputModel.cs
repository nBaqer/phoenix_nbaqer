﻿namespace FAME.Advantage.Messages.Lead.ToolBar
{
    /// <summary>
    /// Info tool bar get Alerts
    /// </summary>
    public class LeadInfoToolOutputModel
    {
        /// <summary>
        /// Alert exists info in new lead status
        /// </summary>
        public bool FlagLead { get; set; }
        
        /// <summary>
        /// Alerts exists appointment to be due
        /// </summary>
        public bool FlagAppointment { get; set; }

        /// <summary>
        /// Alert exists task due
        /// </summary>
        public bool FlagTask { get; set; }

        /// <summary>
        /// true if the operation was successful.
        /// </summary>
        public bool OkChangeLeadOperation { get; set; }
    }
}
