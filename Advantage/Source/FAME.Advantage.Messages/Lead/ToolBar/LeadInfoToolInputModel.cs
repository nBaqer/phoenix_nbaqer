﻿using System;

namespace FAME.Advantage.Messages.Lead.ToolBar
{
    /// <summary>
    /// Input filter for lead tool bar
    /// </summary>
    public class LeadInfoToolInputModel
    {
        /// <summary>
        /// The user id
        /// </summary>
        public string  UserId { get; set; }

        /// <summary>
        /// The date time of the current queue showed lead in client
        /// </summary>
        public DateTime LeadAdmissionDataTime { get; set; }

        /// <summary>
        /// The LeadId of the current queue showed lead in client
        /// </summary>
        public string LeadId { get; set; }

        /// <summary>
        /// The Guid of the current campus ID
        /// </summary>
        public string CampusId { get; set; }

        /// <summary>
        /// The command
        /// 0: get flags of alerts.
        /// 1: Get Previous Lead from QUEUE
        /// 2: Get Next Lead from QUEUE
        /// </summary>
        public string Command { get; set; }

    }
}
