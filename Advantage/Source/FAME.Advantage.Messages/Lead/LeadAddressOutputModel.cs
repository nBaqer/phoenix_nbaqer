﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadAddressOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The Phone output for Leads
//   Each of this field are associated with a lead in a list.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// The Phone output for Leads
    /// Each of this field are associated with a lead in a list.
    /// </summary>
    public class LeadAddressOutputModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string ID { get; set; }
        
        /// <summary>
        /// Gets or sets The Address Type Description
        /// </summary>
        public string AddressType { get; set; }
        
        /// <summary>
        /// Gets or sets The Address Type Id
        /// </summary>
        public string AddressTypeId { get; set; }
        
        /// <summary>
        /// Gets or sets First Part of the Address (max 250 characters)
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the address Apartment.
        /// </summary>
        public string AddressApto { get; set; }

        /// <summary>
        /// Gets or sets Second Part of the Address (max 250 characters)
        /// </summary>
        public string Address2 { get; set; }
        
        /// <summary>
        /// Gets or sets City of the Address
        /// </summary>
        public string City { get; set; }
        
        /// <summary>
        /// Gets or sets State Name of the Address
        /// </summary>
        public string State { get; set; }
        
        /// <summary>
        /// Gets or sets The State Id
        /// </summary>
        public string StateId { get; set; }

        /// <summary>
        /// Gets or sets the state international.
        /// </summary>
        public string StateInternational { get; set; }

        /// <summary>
        /// Gets or sets Zip Code of the Address
        /// </summary>
        public string ZipCode { get; set; }
        
        /// <summary>
        /// Gets or sets Country name of the Address
        /// </summary>
        public string Country { get; set; }
        
        /// <summary>
        /// Gets or sets The Country Id
        /// </summary>
        public string CountryId { get; set; }

        /// <summary>
        /// Gets or sets Status description
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets The Status Id
        /// </summary>
        public string StatusId { get; set; }

        /// <summary>
        /// Gets or sets Flag indicating if this address is the mailing one
        /// </summary>
        public string IsMailingAddress { get; set; }
        
        /// <summary>
        /// Gets or sets 
        /// Flag indication if this address show ID be shown on the Lead page
        /// </summary>
        public string IsShowOnLeadPage { get; set; }
        
        /// <summary>
        /// Gets or sets Last Modification Date
        /// </summary>
        public string ModDate { get; set; }
        
        /// <summary>
        /// Gets or sets Last User Modified this record.
        /// </summary>
        public string Moduser { get; set; }
        
        /// <summary>
        /// Gets or sets The County name
        /// </summary>
        public string County { get; set; }
        
        /// <summary>
        /// Gets or sets The County Unique Id
        /// </summary>
        public string CountyId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is international.
        /// </summary>
        public bool IsInternational { get; set; }
    }
}
