﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FAME.Advantage.Messages.Lead
{
    public class LeadOtherContactEmailModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public string OtherContactsEmailId { get; set; }

        /// <summary>
        /// ID of the Lead Other Contact
        /// </summary>
        public string OtherContactId { get; set; }
        /// <summary>
        /// ID of the Lead
        /// </summary>
        public string LeadId { get; set; }
        /// <summary>
        /// Email Address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Emil Type Id
        /// </summary>
        public string EmailTypeId { get; set; }

        /// <summary>
        /// Email Type
        /// </summary>
        public string EmailType { get; set; }

        /// <summary>
        /// User last modified this object
        /// </summary>
        public string ModUser { get; set; }
        /// <summary>
        /// Last Modified Date
        /// </summary>
        public DateTime ModDate { get; set; }
        /// <summary>
        /// Status ID
        /// </summary>
        public string StatusId { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }


    }
}
