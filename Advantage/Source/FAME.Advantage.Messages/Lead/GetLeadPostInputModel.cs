﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Messages.Lead
{
    public class GetLeadUpdateInputModel
    {
        public List<GetLeadAssignmentnputModel> LeadIds { get; set; }
        public Guid CampusId { get; set; }
        public Guid UserId { get; set; }
        public Guid RepId { get; set; }
    }
}
