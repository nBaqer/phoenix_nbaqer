﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Messages.Lead
{
    public class LeadInfoCommentInputModel
    {
        /// <summary>
        /// The Id of the Lead this object belongs to.
        /// </summary>
        public string LeadId { get; set; }
        /// <summary>
        /// The Campus this object belongs to.
        /// </summary>
        public string CampusId { get; set; }
        /// <summary>
        /// The User Name that Created/Modified this object
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// The Unique Id of this Record
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// Lead Information Comment
        /// </summary>
        public string Comment { get; set; }
    }
}
