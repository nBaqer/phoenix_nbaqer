﻿using System.Collections.Generic;
using FAME.Advantage.Messages.Common;

namespace FAME.Advantage.Messages.Lead.History
{   
    /// <summary>
    /// The output model for history page
    /// has a list for all necessary data used in
    /// different commands
    /// </summary>
    public interface IHistoryOutputModel
    {
        /// <summary>
        /// List of history records for a lead
        /// </summary>
        IList<HistoryModel> HistoryList { get; set; }

        /// <summary>
        /// List of Advantage modules items
        /// </summary>
        IList<DropDownOutputModel> ModulesList { get; set; }

        /// <summary>
        /// Use in get operation only
        /// </summary>
        LeadHistoryInputFilter Filter { get; set; }
    
    }
}
