﻿using System.Collections.Generic;
using FAME.Advantage.Messages.Common;

namespace FAME.Advantage.Messages.Lead.History
{
    /// <summary>
    /// History output Model
    /// </summary>
    public class HistoryOutputModel : IHistoryOutputModel
    {
        /// <summary>
        /// Use this to instantiate the class.
        /// </summary>
        /// <returns></returns>
        public static IHistoryOutputModel Factory()
        {
            var history = new HistoryOutputModel
            {
                ModulesList = new List<DropDownOutputModel>(),
                HistoryList = new List<HistoryModel>()
            };
            return history;
        }

        #region Implementation of IHistoryOutputModel

        /// <summary>
        /// List of history records for a given lead
        /// </summary>
        public IList<HistoryModel> HistoryList { get; set; }

        /// <summary>
        /// List of Advantage modules items
        /// </summary>
        public IList<DropDownOutputModel> ModulesList { get; set; }

        /// <summary>
        /// Use in post operation only
        /// </summary>
        public LeadHistoryInputFilter Filter { get; set; }

        #endregion

    }
}
