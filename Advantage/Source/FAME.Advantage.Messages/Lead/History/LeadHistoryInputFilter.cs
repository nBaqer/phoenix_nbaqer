﻿namespace FAME.Advantage.Messages.Lead.History
{    /// <summary>
     /// Input filter class to command the service
     /// </summary>
    public class LeadHistoryInputFilter
    {
        /// <summary>
        /// The command to be used to get different operation
        /// 1: Get all History records related to the lead
        /// </summary>
        public int Command { get; set; }

        /// <summary>
        /// The lead ID that the notes refers
        /// </summary>
        public string LeadId { get; set; }

        /// <summary>
        /// Module Code
        /// AD for Admission
        /// </summary>
        public string ModuleCode { get; set; }

        /// <summary>
        /// The user Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// The campus Id
        /// </summary>
        public string CampusId { get; set; }
    }
}