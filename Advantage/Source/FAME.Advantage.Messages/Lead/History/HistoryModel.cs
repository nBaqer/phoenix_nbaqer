﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HistoryModel.cs" company="FAME">
//   2015,2016
// </copyright>
// <summary>
//    model for History page
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.History
{
    using System;

    /// <summary>
    /// Output model for History page
    /// </summary>
    public class HistoryModel
    {
        /// <summary>
        /// Gets ID of the History records drawn from various audit history and other historical tables
        /// </summary>
        public Guid HistoryId { get; set; }

        /// <summary>
        ///  Gets the Date of historical event
        /// </summary>
        public DateTime HistoryDate { get; set; }

        /// <summary>
        ///  Gets the module asssocaited with the historical event
        /// </summary>
        public string HistoryModule { get; set; }

        /// <summary>
        ///  Gets the module code asssocaited with the historical event
        /// </summary>
        public string ModuleCode { get; set; }

        /// <summary>
        ///  Gets the type of historical record
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        ///  Gets the description of the historical record
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///  Gets the person that entered the historical event in the system
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        ///  Gets the additional content associated with the historucal event e.g. email content
        /// </summary>
        public string AdditionalContent { get; set; }
    }
}
