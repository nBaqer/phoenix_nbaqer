﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Messages.Lead
{
    public class LeadEmailInputModel
    {
        /// <summary>
        /// The Id of the Lead this object belongs to.
        /// </summary>
        public string LeadId { get; set; }
        /// <summary>
        /// The Campus this object belongs to.
        /// </summary>
        public string CampusId { get; set; }
        /// <summary>
        /// The User Name that Created/Modified this object
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// The Type of Email
        /// </summary>
        public string TypeId { get; set; }
        /// <summary>
        /// The Id of the Status (Active or Inactive)
        /// </summary>
        public string StatusId { get; set; }
        /// <summary>
        /// The Unique Id of this Record
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// Boolean to determine if should show the record in the lead page
        /// </summary>
        public bool ShowOnLeadPage { get; set; }
        /// <summary>
        /// Boolean to determine if this is the best email to contact the lead
        /// </summary>
        public bool IsBest { get; set; }
        /// <summary>
        /// The Email Address
        /// </summary>
        public string Email { get; set; }
    }
}
