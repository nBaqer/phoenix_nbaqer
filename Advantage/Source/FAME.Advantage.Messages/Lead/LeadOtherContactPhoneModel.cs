﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FAME.Advantage.Messages.Lead
{
    public class LeadOtherContactPhoneModel
    {

        /// <summary>
        /// ID
        /// </summary>
        public string OtherContactsPhoneId { get; set; }

        /// <summary>
        /// ID Of the Lead Other Other Contact Instance
        /// </summary>
        public string OtherContactId { get; set; }
        /// <summary>
        /// Lead ID
        /// </summary>
        public string LeadId { get; set; }
        /// <summary>
        /// Phone Type Id
        /// </summary>
        public string PhoneTypeId { get; set; }
        /// <summary>
        /// Phone Type
        /// </summary>
        public string PhoneType { get; set; }
        /// <summary>
        /// Phone
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Phone Extension
        /// </summary>
        public string Extension { get; set; }
        /// <summary>
        /// Flag Indicating if Phone Number is Foreign
        /// </summary>
        public bool IsForeignPhone { get; set; }
        /// <summary>
        /// Status ID
        /// </summary>
        public string StatusId { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// User last modified this object
        /// </summary>
        public string ModUser { get; set; }
        /// <summary>
        /// Last Modified Date
        /// </summary>
        public DateTime ModDate { get; set; }
    }
}
