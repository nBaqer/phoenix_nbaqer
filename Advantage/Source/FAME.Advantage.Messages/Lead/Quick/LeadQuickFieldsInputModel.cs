﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadQuickFieldsInputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The lead quick fields input model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.Quick
{
    using FAME.Advantage.Messages.Common;

    /// <summary>
    /// The lead quick fields input model.
    /// </summary>
    public class LeadQuickFieldsInputModel
    {
        /// <summary>
        /// Gets or sets the command.
        /// 1 = Get the Quick Fields
        /// 2 = Get DropDown Values
        /// </summary>
        public int Command { get; set; }

        /// <summary>
        /// Gets or sets the control name of the drop down        
        /// </summary>
        public string CtrlName { get; set; }

        /// <summary>
        /// Gets or sets input filter for the drop down
        /// </summary>
        public CatalogsInputModel Input { get; set; }
    }
}
