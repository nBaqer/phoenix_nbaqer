﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadQuickFieldsOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The lead quick fields output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.Quick
{
    /// <summary>
    /// The lead quick fields output model.
    /// </summary>
    public class LeadQuickFieldsOutputModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the table field id.
        /// </summary>
        public virtual int TblFldsId { get; set; }

        /// <summary>
        /// Gets or sets the field id.
        /// </summary>
        public virtual int FldId { get; set; }

        /// <summary>
        /// Gets or sets the section id.
        /// </summary>
        public virtual int SectionId { get; set; }

        /// <summary>
        /// Gets or sets the value if the field is a dropdown
        /// </summary>
        public virtual int? DdlId { get; protected set; }

        /// <summary>
        /// Gets or sets the field name
        /// </summary>
        public virtual string FldName { get; protected set; }

        /// <summary>
        /// Gets or sets if the field is required or not
        /// </summary>
        public virtual int Required { get; protected set; }

        /// <summary>
        /// Gets or sets the length of the field
        /// </summary>
        public virtual int FldLen { get; protected set; }

        /// <summary>
        /// Gets or sets the data type of the field
        /// </summary>
        public virtual string FldType { get; protected set; }

        /// <summary>
        /// Gets or sets the name given to the label in UI
        /// </summary>
        public virtual string Caption { get; protected set; }

        /// <summary>
        /// Gets or sets the value if it has dropdown mostly it is null
        /// </summary>
        public virtual string ControlName { get; protected set; }

        /// <summary>
        /// Gets or sets the field type id.
        /// </summary>
        public virtual int FldTypeId { get; protected set; }

        /// <summary>
        /// Gets or sets the control id name. this is the name of the static control in Lead Info page
        /// </summary>
        public virtual string CtrlIdName { get; protected set; }

        /// <summary>
        /// Gets or sets the property name. this is the name in the mapper
        /// </summary>
        public virtual string PropName { get; protected set; }

        /// <summary>
        /// Gets or sets the Parent control Id. this is the name in the mapper
        /// </summary>
        public virtual string ParentCtrlId { get; protected set; }
        #endregion
    }
}
