﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QuickLeadDropdownDataSource.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the QuickLeadDropdownDataSource type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.Quick
{
    using System.Collections.Generic;

    /// <summary>
    /// The quick lead dropdown data source.
    /// </summary>
    /// <typeparam name="T">
    /// any template. this is used for state drop down
    /// </typeparam>
    public class QuickLeadDropdownDataSource<T>
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        public IEnumerable<T> DataSource { get; set; }
    }
}
