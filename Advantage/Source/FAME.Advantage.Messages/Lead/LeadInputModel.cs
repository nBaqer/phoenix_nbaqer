﻿using System;

namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// Lead input Model
    /// Admitted Command
    /// ALLOWED_STATUS
    /// DELETE
    /// INACTIVE
    /// </summary>
    public class LeadInputModel
    {
       /// <summary>
       /// Lead Id
       /// </summary>
        public string LeadId { get; set; }

        /// <summary>
        /// Campus ID
        /// </summary>
        public string CampusId { get; set; }

        /// <summary>
        /// Actual Lead Status Id
        /// </summary>
        public string LeadStatusId { get; set; }

        /// <summary>
        /// Actual User ID
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Page Resource ID. Each page in advantage has a resource.
        /// </summary>
        public int PageResourceId { get; set; }

        /// <summary>
        /// Used to define a specific operation in the controller service
        /// </summary>
        public string CommandString { get; set; }
    }
}
