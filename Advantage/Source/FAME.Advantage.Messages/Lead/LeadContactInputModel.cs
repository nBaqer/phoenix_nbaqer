﻿namespace FAME.Advantage.Messages.Lead
{
    public class LeadContactInputModel
    {
        /// <summary>
        /// The Id of the Lead this object belongs to.
        /// </summary>
        public string LeadId { get; set; }
        /// <summary>
        /// The Campus this object belongs to.
        /// </summary>
        public string CampusId { get;set; }
        /// <summary>
        /// The User Name that Created/Modified this object
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// The User Name that Created/Modified this object
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// The Type of Phone
        /// </summary>
        public string TypeId { get; set; }
        /// <summary>
        /// The Id of the Status (Active or Inactive)
        /// </summary>
        public string StatusId { get; set; }
        /// <summary>
        /// The Unique Id of this Record
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// Boolean to determine if should show the record in the lead page
        /// </summary>
        public bool ShowOnLeadPage { get; set; }
        /// <summary>
        /// Boolean to determine if this is the best phone to contact the lead
        /// </summary>
        public bool IsBest { get; set; }
        /// <summary>
        /// The Phone Number
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// The Extension of the Phone Number 
        /// </summary>
        public string Extension { get; set; }
        /// <summary>
        /// Flag indicating the phone is international or not.
        /// </summary>
        public bool IsForeignPhone { get; set; }
        /// <summary>
        /// The Lead Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Lead Comment
        /// </summary>
        public string Comments { get; set; }
    }
}
