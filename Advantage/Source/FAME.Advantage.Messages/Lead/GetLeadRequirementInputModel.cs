﻿using System;

namespace FAME.Advantage.Messages.Lead
{
    public class GetLeadRequirementInputModel
    {
        public string StatusCode { get; set; }
        public Guid CampusId { get; set; }
        public Guid UserId { get; set; }
        public bool? IsMandatory { get; set; }
        public Guid? leadId { get; set; }
        public string RequirementType { get; set; }
        public string TransactionId { get; set; }
    }
}
