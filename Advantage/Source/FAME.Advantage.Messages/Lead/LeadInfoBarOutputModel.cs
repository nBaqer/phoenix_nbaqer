﻿using System;

namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// Basic Lead Info
    /// </summary>
    public class LeadInfoBarOutputModel
    {
        /// <summary>
        /// Lead Id (guid) as string
        /// </summary>
        public string LeadGuid { get; set; }
        
        /// <summary>
        /// The full name of the lead
        /// </summary>
        public string LeadFullName { get; set; }
        
        /// <summary>
        /// The program Version Name
        /// </summary>
        public string ProgramVersionName { get; set; }
        
        /// <summary>
        /// The Expected Start Date
        /// </summary>
        public DateTime? ExpectedStart { get; set; }
        
        /// <summary>
        /// The name of the admission rep.
        /// </summary>
        public string AdmissionRep { get; set; }
        
        /// <summary>
        /// The date that was assigned to the admission rep.
        /// </summary>
        public DateTime? DateAssigned { get; set; }
        
        /// <summary>
        /// Path to the image
        /// </summary>
        public string LeadImagePath { get; set; }
        
        /// <summary>
        /// The Image Type
        /// </summary>
        public string ContentType { get; set; }
        
        /// <summary>
        /// The lead Current Status
        /// </summary>
        public string CurrentStatus { get; set; }

        /// <summary>
        /// The Campus Name
        /// </summary>
        public string CampusName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is lead enrolled.
        /// </summary>
        public bool IsLeadEnrolled { get; set; }
       
    }
}
