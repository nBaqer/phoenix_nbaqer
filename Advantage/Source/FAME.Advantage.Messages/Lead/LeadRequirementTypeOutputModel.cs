﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadRequirementTypeOutputModel.cs" company="FAME Inc.">
//   FAME Inc. 2016
//    FAME.Advantage.Messages.Lead.Lead
// </copyright>
// <summary>
//   Defines The lead requirement type output type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The lead requirement type output model.
    /// </summary>
    public class LeadRequirementTypeOutputModel
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is met.
        /// </summary>
        public bool IsMet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether at least one requirement with this type is defined.
        /// </summary>
        public bool IsDefined { get; set; }

        /// <summary>
        /// The set is met to be used in a LINQ Query.
        /// </summary>
        /// <param name="isMet">
        /// The is met.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool SetIsMet(bool isMet)
        {
            this.IsMet = isMet;
            return true;
        }
    }
}
