﻿using System;

namespace FAME.Advantage.Messages.Lead
{
    public class LeadVendorOutputModel
    {
        public int Id { get;  set; }
        public string VendorName { get;  set; }
        public string VendorCode { get;  set; }
        public string Description { get;  set; }
        
    }
}