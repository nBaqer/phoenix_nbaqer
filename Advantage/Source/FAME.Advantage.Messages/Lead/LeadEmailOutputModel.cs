﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEmailOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Get the email and features
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// Get the email and features
    /// </summary>
    public class LeadEmailOutputModel
    {
        /// <summary>
        /// Gets or sets Email id in the email table.
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// Gets or sets Type of Email Description
        /// </summary>
        public string EmailType { get; set; }

        /// <summary>
        /// Gets or sets The Type of Email Id
        /// </summary>
        public string EmailTypeId { get; set; }

        /// <summary>
        /// Gets or sets E-mail
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets The Email Status description
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets The Email Status id
        /// </summary>
        public string StatusId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is preferred.
        /// </summary>
        public bool IsPreferred { get; set; }

        /// <summary>
        /// Gets or sets flag that indicate if the email should show on lead page
        /// Yes / No
        /// </summary>
        public string IsShowOnLeadPage { get; set; }
    }
}
