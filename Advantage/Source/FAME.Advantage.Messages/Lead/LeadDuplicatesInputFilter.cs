﻿namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// Filter Input for Duplicates
    /// </summary>
    public class LeadDuplicatesInputFilter
    {
        /// <summary>
        /// New lead
        /// </summary>
        public string LeadNewGuid { get; set; }

        /// <summary>
        /// Guid of the duplicated lead
        /// </summary>
        public string LeadDuplicateGuid { get; set; }
        
        /// <summary>
        /// Possibles values:
        /// 1: Update Advantage with Lead A
        /// 2: Delete Lead A
        /// 3: Create new Lead with Lead A
        /// </summary>
        public int Command   { get; set; }

        /// <summary>
        /// The user id
        /// </summary>
        public string UserId { get; set; }
    }
}
