﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadInfoPageOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Output Model for Lead Demographic Information
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Output Model for Lead Demographic Information
    /// </summary>
    public class LeadInfoPageOutputModel
    {
        /// <summary>
        /// Gets or sets the lead id that the information is referred.
        /// </summary>
        public string LeadId { get; set; }

        /// <summary>
        ///  Gets or sets The user that does the modification.
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        ///  Gets or sets Use to send parameters in post operations
        /// </summary>
        public LeadInputModel InputModel { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether avoid duplicate analysis.
        /// true is not executed.
        /// This is to permit enter a possible duplicate value for the user
        /// </summary>
        public bool AvoidDuplicateAnalysis { get; set; }

        /// <summary>
        ///  Gets or sets List of Id State Changes that can be possible
        /// with the actual lead status.
        /// </summary>
        public IList<string> StateChangeIdsList { get; set; }

        #region Demographic

        /// <summary>
        ///  Gets or sets First Name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        ///  Gets or sets Middle Name
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        ///  Gets or sets Last Name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        ///  Gets or sets Name Prefix 
        /// </summary>
        public string Prefix { get; set; }

        /// <summary>
        /// Gets or sets Name Suffix I, II, Junior
        /// </summary>
        public string Suffix { get; set; }

        /// <summary>
        ///  Gets or sets Nick Name
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        ///  Gets or sets Social Security Number
        /// </summary>
        public string SSN { get; set; }

        /// <summary>
        ///  Gets or sets Date of Birth
        /// </summary>
        public DateTime? Dob { get; set; }

        /// <summary>
        ///  Gets or sets Citizen ship (ID)
        /// </summary>
        public string Citizenship { get; set; }

        /// <summary>
        ///  Gets or sets The USA Alien Number if any
        /// </summary>
        public string AlienNumber { get; set; }

        /// <summary>
        ///  Gets or sets If live independent or depend of other person
        /// </summary>
        public string Dependency { get; set; }

        /// <summary>
        ///  Gets or sets Marital Status Description
        /// </summary>
        public string MaritalStatus { get; set; }

        /// <summary>
        ///  Gets or sets other family that depend from the Lead.
        /// </summary>
        public string Dependants { get; set; }

        /// <summary>
        ///  Gets or sets Incoming Description
        /// </summary>
        public string FamilyIncoming { get; set; }

        /// <summary>
        ///  Gets or sets Example OffCampus, On Campus, Incarcerated ,with parent
        /// This is defined by school
        /// </summary>
        public string HousingType { get; set; }

        /// <summary>
        ///  Gets or sets State Code of driver license.
        /// </summary>
        public string DrvLicStateCode { get; set; }

        /// <summary>
        ///  Gets or sets Driver License Number
        /// </summary>
        public string DriverLicenseNumber { get; set; }

        /// <summary>
        ///  Gets or sets The transportation that the lead owner or use.
        /// </summary>
        public string Transportation { get; set; }

        /// <summary>
        ///  Gets or sets Gender
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        ///  Gets or sets Age
        /// </summary>
        public string Age { get; set; }

        /// <summary>
        ///  Gets or sets Miles from school to student house (-1 is not set)
        /// </summary>
        public int? DistanceToSchool { get; set; }

        /// <summary>
        ///  Gets or sets If the lead is disabled or not.
        /// </summary>
        public bool? IsDisabled { get; set; }

        /// <summary>
        ///  Gets or sets Race GUID
        /// </summary>
        public string RaceId { get; set; }

        /// <summary>
        ///  Gets or sets History of the leads official last name changes
        /// </summary>
        public IList<LeadLastNameHistoryOutputModel> LeadLastNameHistoryList { get; set; }

        /// <summary>
        ///  Gets or sets List of vehicles associated with the lead
        /// </summary>
        public IList<VehicleOutputModel> Vehicles { get; set; }

        #endregion

        #region Contact Info

        /// <summary>
        ///  Gets or sets Preferred Contact Id
        /// 1:phone 2:email 3:text
        /// </summary>
        public int PreferredContactId { get; set; }

        /// <summary>
        /// Gets or sets the lead address.
        /// </summary>
        public LeadAddressOutputModel LeadAddress { get; set; }  

        /// <summary>
        ///  Gets or sets A list of phones associated with the lead.
        /// </summary>
        public IList<LeadPhonesOutputModel> PhonesList { get; set; }

        /// <summary>
        ///  Gets or sets List of lead emails.
        /// </summary>
        public IList<LeadEmailOutputModel> EmailList { get; set; }

        /// <summary>
        ///  Gets or sets Best time to contact the lead
        /// </summary>
        public string BestTime { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether none email.
        /// </summary>
        public bool NoneEmail { get; set; }

        #endregion

        #region Source

        /// <summary>
        ///  Gets or sets Id of the category of the source
        /// </summary>
        public string SourceCategoryId { get; set; }

        /// <summary>
        ///  Gets or sets The type of the lead source
        /// </summary>
        public string SourceTypeId { get; set; }

        /// <summary>
        ///  Gets or sets if was a advertisement, the type of it.
        /// </summary>
        public string AdvertisementId { get; set; }

        /// <summary>
        /// Gets or sets the vendor source if the Lead was inserted by a third party vendor.
        /// </summary>
        public string VendorSource { get; set; }

        /// <summary>
        ///  Gets or sets A text referred to advertisement 
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        ///  Gets or sets Source Date Time. It is the moment that the lead
        /// is enter in the system.
        /// </summary>
        public DateTime? SourceDateTime { get; set; }

        #endregion

        #region Others

        /// <summary>
        ///  Gets or sets Date lead apply
        /// </summary>
        public DateTime? DateApplied { get; set; }

        /// <summary>
        ///  Gets or sets Date that is assigned to rep ad-min.
        /// </summary>
        public DateTime? AssignedDate { get; set; }

        /// <summary>
        ///  Gets or sets the admission rep id assigned
        /// </summary>
        public string AdmissionRepId { get; set; }

        /// <summary>
        ///  Gets or sets Agency Sponsor Id
        /// </summary>
        public string AgencySponsorId { get; set; }

        /// <summary>
        ///  Gets or sets Free comments about lead 100 characters null
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        ///  Gets or sets Lead previous education ID
        /// </summary>
        public string PreviousEducationId { get; set; }

        /// <summary>
        ///  Gets or sets HighSchool Id
        /// </summary>
        public string HighSchoolId { get; set; }

        /// <summary>
        /// Gets or sets the high school name.
        /// </summary>
        public string HighSchoolName { get; set; }

        /// <summary>
        ///  Gets or sets Lead Graduation day from high school
        /// </summary>
        public DateTime? HighSchoolGradDate { get; set; }

        /// <summary>
        ///  Gets or sets Is the lead attending high school?
        /// </summary>
        public int? AttendingHs { get; set; }

        /// <summary>
        ///  Gets or sets Administrative criteria to accept the lead.
        /// </summary>
        public string AdminCriteriaId { get; set; }

        /// <summary>
        ///  Gets or sets Reason id because was not enrolled the student
        /// </summary>
        public string ReasonNotEnrolled { get; set; }

        #endregion

        #region Academics

        /// <summary>
        ///  Gets or sets Campus for the lead.
        /// </summary>
        public string CampusId { get; set; }

        /// <summary>
        ///  Gets or sets Area of interest <code>PrgGroupdEscription in arPrgGrp</code>
        /// </summary>
        public string AreaId { get; set; }

        /// <summary>
        ///  Gets or sets <code>proDescrip in ArPrograms</code>
        /// </summary>
        public string ProgramId { get; set; }

        /// <summary>
        ///  Gets or sets <code>PrgVerDescrip in ArPrgVersions</code>
        /// </summary>
        public string PrgVerId { get; set; }

        /// <summary>
        ///   Gets or sets Description from <code>ArAttendTypes</code>
        /// </summary>
        public string AttendTypeId { get; set; }

        /// <summary>
        ///   Gets or sets Lead Status
        /// </summary>
        public string LeadStatusId { get; set; }

        /// <summary>
        ///   Gets or sets Expected start Date, 
        /// This should be transformed in a string to compare
        /// with the begin of a string in the client.
        /// </summary>
        public DateTime? ExpectedStart { get; set; }

        /// <summary>
        /// Gets or sets Description from <code>ArProgSchedules</code>
        /// The value is droved by program version
        /// </summary>
        public string ScheduleId { get; set; }

        #endregion

        #region Custom Fields

        /// <summary>
        ///   Gets or sets Information about the SDF specific to the lead.
        /// </summary>
        public IList<LeadSdfOutputModel> SdfList { get; set; }

        #endregion

        #region Groups

        /// <summary>
        ///   Gets or sets List of identifier of group that belong to lead.
        /// </summary>
        public IList<string> GroupIdList { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the lead is enrolled.
        /// </summary>
        public bool IsEnrolled { get; set; }
        #endregion
    }
}
