﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnrollOutputModel.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the EnrollOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.Enroll
{
    using System;

    /// <summary>
    /// The enroll output model.
    /// </summary>
    public class EnrollOutputModel
    {
        /// <summary>
        /// Gets or sets entrance interview date
        /// </summary>
        public DateTime? EntranceInterviewDate { get;  set; }

        /// <summary>
        /// Gets or sets the is first time in school.
        /// </summary>
        public bool? IsFirstTimeInSchool { get;  set; }

        /// <summary>
        /// Gets or sets the is first time post sec school.
        /// </summary>
        public bool? IsFirstTimePostSecSchool { get;  set; }

        /// <summary>
        /// Gets or sets the shift id.
        /// </summary>
        public string ShiftId { get;  set; }

        /// <summary>
        /// Gets or sets the academic calendar id.
        /// </summary>
        public int AcademicCalendarId { get; set; }

        /// <summary>
        /// Gets or sets the international lead.
        /// </summary>
        public bool? InternationalLead { get;  set; }

        /// <summary>
        /// Gets or sets the nationality.
        /// </summary>
        public string Nationality { get;  set; }

        /// <summary>
        /// Gets or sets the geographic type id.
        /// </summary>
        public string GeographicTypeId { get;  set; }

        /// <summary>
        /// Gets or sets the degree certificate seeking id.
        /// </summary>
        public string DegCertSeekingId { get;  set; }

        /// <summary>
        /// Gets or sets the billing(charging in UI) method id.
        /// </summary>
        public string ChargingMethodId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether disable auto charge.
        /// </summary>
        public bool DisableAutoCharge { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether third party contract.
        /// </summary>
        public bool ThirdPartyContract { get; set; }

        /// <summary>
        /// Gets or sets the program version type.
        /// </summary>
        public string ProgramVersionType { get; set; }

        /// <summary>
        /// Gets or sets the lead info.
        /// </summary>
        public LeadInfoPageOutputModel LeadInfo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether requirement met.
        /// </summary>
        public bool RequirementMet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is valid lead.
        /// </summary>
        public bool IsValidLead { get; set; }

        /// <summary>
        /// Gets or sets the validation messages.
        /// </summary>
        public string ValidationMessages { get; set; }

        /// <summary>
        /// Gets or sets the enrollment date.
        /// </summary>
        public DateTime EnrollmentDate { get; set; }

        /// <summary>
        /// Gets or sets the expected start date.
        /// </summary>
        public DateTime? ExpectedStartDate { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the mid point date.
        /// </summary>
        public DateTime? MidPointDate { get; set; }

        /// <summary>
        /// Gets or sets the contracted grad date.
        /// </summary>
        public DateTime ContractedGradDate { get;  set; }

        /// <summary>
        /// Gets or sets the transfer date.
        /// </summary>
        public DateTime? TransferDate { get;  set; }

        /// <summary>
        /// Gets or sets the tuition category.
        /// </summary>
        public string TuitionCategory { get;  set; }

        /// <summary>
        /// Gets or sets the fin aid advisor.
        /// </summary>
        public string FinAidAdvisor { get; set; }

        /// <summary>
        /// Gets or sets the academic advisor.
        /// </summary>
        public string AcademicAdvisor { get; set; }

        /// <summary>
        /// Gets or sets the badge number.
        /// </summary>
        public string BadgeNumber { get;  set; }

        /// <summary>
        /// Gets or sets the transfer hours.
        /// </summary>
        public decimal TransferHours { get;  set; }

        /// <summary>
        /// Gets or sets the future start status mapped.
        /// </summary>
        public string FutureStartStatusMapped { get; set; }

        /// <summary>
        /// Gets or sets the not mapped status.
        /// </summary>
        public string NotMappedStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Lead is disabled.
        /// </summary>
        public bool? IsDisabled { get; set; }

        /// <summary>
        /// Gets or sets the student number.
        /// </summary>
        public string StudentNumber { get; set; }

        /// <summary>
        /// Gets or sets the student status.
        /// </summary>
        public string StudentStatus { get; set; }

        /// <summary>
        /// Gets or sets the student enroll id.
        /// </summary>
        public string StuEnrollId { get; set; }

        /// <summary>
        /// Gets or sets the student id.
        /// </summary>
        public string StudentId { get; set; }

        /// <summary>
        /// Gets or sets the enroll number displayed on the enroll page.
        /// </summary>
        public string EnrollId { get; set; }

        /// <summary>
        /// Gets or sets the education level.
        /// </summary>
        public string EducationLevel { get; set; }

        /// <summary>
        /// Gets or sets the enroll state id.
        /// </summary>
        public string EnrollStateId { get; set; }

        /// <summary>
        /// Gets or sets the international state.
        /// </summary>
       // public bool? InternationalState { get; set; }
        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets a value if lead status belongs to system defined enroll status, disable the page
        /// </summary>
        public bool DisableEnrolLeadStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is duplicate ssn.
        /// </summary>
        public bool IsDuplicateSsn { get; set; }

        //public bool IsIpeds { get; set; }

        public string ApiUrl { get; set; }

        public string ApiToken { get; set; }
    }
}
