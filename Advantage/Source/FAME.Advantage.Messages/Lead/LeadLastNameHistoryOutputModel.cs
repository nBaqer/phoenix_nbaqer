﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// Output model for record
    /// </summary>
    public class LeadLastNameHistoryOutputModel
    {
        /// <summary>
        /// Id of the record
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Lead Previous Last Name
        /// </summary>
        public string LastName { get;  set; }

        /// <summary>
        /// Lead associated with the employment
        /// </summary>
        public string LeadId { get;  set; }

    }
}
