﻿namespace FAME.Advantage.Messages.Lead
{
    /// <summary>
    /// Input model for Operations wit Vehicles
    /// </summary>
    public class VehicleInputModel
    {
        /// <summary>
        /// Unique Vehicle Id
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// Lead Id
        /// </summary>
        public string LeadId { get; set; }

        /// <summary>
        /// User that make the modification
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Command
        /// 1: Get all vehicles for a LeadId
        /// </summary>
        public int Command { get; set; }
    }
}
