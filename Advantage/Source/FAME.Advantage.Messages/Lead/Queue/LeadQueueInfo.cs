﻿using System;

namespace FAME.Advantage.Messages.Lead.Queue
{
    /// <summary>
    /// Lead Queue Info for Lead Queue Page.
    /// </summary>
    public class LeadQueueInfo
    {
        /// <summary>
        /// Lead Id 
        /// </summary>
        public string Id { get; set; }
        
        /// <summary>
        /// Lead First Name
        /// </summary>
        public string FirstName  { get; set; }

        /// <summary>
        /// Lead Last Name
        /// </summary>
        public string  LastName { get; set; }

        /// <summary>
        /// Lead School Status description
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Lead Advantage system status Id
        /// </summary>
        public int StatusSysId { get; set; }

        /// <summary>
        /// Program Name
        /// </summary>
        public string ProgramInterest { get; set; }

        /// <summary>
        /// The date received but in text format, and formated special
        /// </summary>
        public string Received { get; set; }

        /// <summary>
        /// The date and time that the lead was assigned to the Admission Rep
        /// Use this to detect if the lead is new...
        /// </summary>
        public DateTime DateReceived { get; set; }

        /// <summary>
        /// Last Activity assigned to this lead in task manager
        /// </summary>
        public string LastActivity { get; set; }

        /// <summary>
        /// The code of the last activity can be for example Appointment
        /// To be used to detected the type of last task due.
        /// </summary>
        public string LastActivityCode { get; set; }

        /// <summary>
        /// Date Time of finished the activity
        /// Due Date
        /// </summary>
        public DateTime? DateLast { get; set; }

        /// <summary>
        /// Days or hour and minutes that the lead has not Last Activity. This is a calculate field
        /// Not yet defined!
        /// </summary>
        public string Age { get; set; }

        /// <summary>
        /// Name of the last activity task if it is defined
        /// </summary>
        public string NextActivity { get; set; }

        /// <summary>
        /// Date of the next task if it is defined
        /// </summary>
        public DateTime? DateNext { get; set; }

        /// <summary>
        /// The Lead Primary Phone
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Phone is international?
        /// </summary>
        public bool IsInternational { get; set; }
    }
}
