﻿using System.Collections.Generic;
using FAME.Advantage.Messages.Common;

namespace FAME.Advantage.Messages.Lead.Queue
{
    /// <summary>
    /// Model DTO to be send to Client Queue Page
    /// </summary>
    public interface IQueueOutputModel
    {
        /// <summary>
        /// List of Admission Rep for a specified Campus
        /// </summary>
        IList<DropDownOutputModel> AdmissionRepList { get; set; }

        /// <summary>
        /// Information relative to each Lead assigned to specific Admission Rep.
        /// </summary>
        IList<LeadQueueInfo> LeadQueueInfoList { get; set; }

        /// <summary>
        /// information relative to command the service, and relative to Queue work.
        /// </summary>
        QueueInfo QueueInfoObj { get; set; }
    }
}