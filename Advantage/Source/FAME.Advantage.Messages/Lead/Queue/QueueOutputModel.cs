﻿using System.Collections.Generic;
using FAME.Advantage.Messages.Common;

namespace FAME.Advantage.Messages.Lead.Queue
{
    /// <summary>
    /// Output Model for Queue Page
    /// </summary>
    public class QueueOutputModel : IQueueOutputModel
    {
        /// <summary>
        /// Protected the constructor to make the factory mandatory
        /// </summary>
        protected QueueOutputModel(){}

        /// <summary>
        /// Gets or sets List of Admission Representable for a specified Campus
        /// </summary>
        public IList<DropDownOutputModel> AdmissionRepList { get; set; }
        
        /// <summary>
        /// Information relative to each Lead assigned to specific Admission Rep.
        /// </summary>
        public IList<LeadQueueInfo> LeadQueueInfoList { get; set; }

        /// <summary>
        /// information relative to command the service, and relative to Queue work.
        /// </summary>
        public QueueInfo QueueInfoObj { get; set; }

        /// <summary>
        /// Factory to create a instance of the class
        /// </summary>
        /// <returns>A Initiate Object</returns>
        public static IQueueOutputModel Factory()
        {
            IQueueOutputModel info = new QueueOutputModel();
            info.AdmissionRepList = new List<DropDownOutputModel>();
            info.LeadQueueInfoList = new List<LeadQueueInfo>();
            info.QueueInfoObj = new QueueInfo { RefreshQueueInterval = 300 };

            // 5 minutes
            return info;
        }
    }

}
