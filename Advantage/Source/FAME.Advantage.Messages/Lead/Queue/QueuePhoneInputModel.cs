﻿namespace FAME.Advantage.Messages.Lead.Queue
{
    /// <summary>
    /// Input model to modify the phone for a lead
    /// </summary>
    public class QueuePhoneInputModel
    {
        /// <summary>
        /// ID off the phone table
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// New phone for lead
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Lead Guid
        /// </summary>
        public string LeadId { get; set; }

        /// <summary>
        /// 1 to 3 (1) is primary phone
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// User Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// If the phone is foreign or USA.
        /// </summary>
        public bool IsForeignPhone  { get; set; }

        /// <summary>
        /// PHone extension
        /// </summary>
        public string Extension { get; set; }


        
    }
}
