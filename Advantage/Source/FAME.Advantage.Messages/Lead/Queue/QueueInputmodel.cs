﻿namespace FAME.Advantage.Messages.Lead.Queue
{
    /// <summary>
    /// input filter for page Lead queue
    /// </summary>
    public class QueueInputmodel
    {
        /// <summary>
        /// The user Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// The Assigned Rep Id to search
        /// </summary>
        public string AssingRepId { get; set; }

        /// <summary>
        /// The CampusId
        /// </summary>
        public string CampusId { get; set; }

        /// <summary>
        /// 0:Get All 
        /// 1: get only admission rep list. 
        /// 2: get only LeadInfo
        /// </summary>
        public int Command { get; set; }
    
    }
}
