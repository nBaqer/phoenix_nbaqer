﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QueueInfo.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Info relative a configure the Lead Queue page
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Lead.Queue
{
    using System;

    /// <summary>
    /// Info relative a configure the Lead Queue page
    /// </summary>
    public class QueueInfo
    {
        /// <summary>
        /// Gets or sets the flag to show the admission combo-box
        /// if true, instruct queue page to show the Admission Rep combo-box.
        /// </summary>
        public bool ShowAdmissionRep { get; set; }

        /// <summary>
        ///  Gets or sets The time that we send this server response
        /// </summary>
        public DateTime DeliveryTime { get; set; }

        /// <summary>
        ///  Gets or sets The interval of time between refresh the query in the client
        /// In Seconds.
        /// </summary>
        public int RefreshQueueInterval { get; set; }

        /// <summary>
        /// Gets or sets the number of lead pending.
        /// </summary>
        public int NumberOfLeadPending { get; set; }
    }
}
