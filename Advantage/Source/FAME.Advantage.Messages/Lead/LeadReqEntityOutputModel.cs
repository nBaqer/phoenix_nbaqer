﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadReqEntityOutputModel.cs" company="FAME Inc.">
//   FAME 2016
//   FAME.Advantage.Messages.Lead.LeadReqEntityOutputModel
// </copyright>
// <summary>
//   Defines the LeadReqEntityOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Messages.Lead
{
    using System;

    /// <summary>
    /// The lead requirement entity output model.
    /// </summary>
    public class LeadReqEntityOutputModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the display type.
        /// </summary>
        public string DisplayType { get; set; }

        /// <summary>
        /// Gets or sets the date received.
        /// </summary>
        public string DateReceived { get; set; }

        /// <summary>
        /// Gets or sets the approval date.
        /// </summary>
        public string ApprovalDate { get; set; }

        /// <summary>
        /// Gets or sets the approved by.
        /// </summary>
        public string ApprovedBy { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is approved.
        /// </summary>
        public bool IsApproved { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is overridden.
        /// </summary>
        public bool IsOverridden { get; set; }

        /// <summary>
        /// Gets or sets the override reason.
        /// </summary>
        public string OverReason { get; set; }

        /// <summary>
        /// Gets or sets the document requirement id.
        /// </summary>
        public Guid? DocumentRequirementId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is mandatory.
        /// </summary>
        public bool IsMandatory { get; set; }

        /// <summary>
        /// Gets or sets the requirement level type.
        /// </summary>
        public string RequirementLevelType { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        public string Score { get; set; }

        /// <summary>
        /// Gets or sets the min score.
        /// </summary>
        public string MinScore { get; set; }

        /// <summary>
        /// Gets or sets the document status id. Returns null if the requirement type is not Document.
        /// </summary>
        public Guid? DocumentStatusId { get; set; }
    }
}