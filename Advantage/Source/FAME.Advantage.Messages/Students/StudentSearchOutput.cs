﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentSearchOutput.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the StudentSearchOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Students
{
    using System;

    /// <summary>
    /// The student search output model.
    /// </summary>
    public class StudentSearchOutputModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; protected set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        public string FullName { get; protected set; }

        /// <summary>
        /// Gets or sets the field 1.
        /// </summary>
        public string Field1 { get; protected set; } // SSN

        /// <summary>
        /// Gets or sets the field 2.
        /// </summary>
        public string Field2 { get; protected set; } // StudentNumber

        /// <summary>
        /// Gets or sets the field 3.
        /// </summary>
        public string Field3 { get; protected set; } // StartDate

        /// <summary>
        /// Gets or sets the field 4.
        /// </summary>
        public string Field4 { get; protected set; } // Status

        /// <summary>
        /// Gets or sets the field 5.
        /// </summary>
        public string Field5 { get; protected set; } // Campus        

        /// <summary>
        /// Gets or sets the image 64 encode.
        /// </summary>
        public string Image64 { get; protected set; }

        /// <summary>
        /// Gets or sets the image URL.
        /// </summary>
        public string ImageUrl { get; protected set; }

        /// <summary>
        /// Gets or sets the not found image URL.
        /// </summary>
        public string NotFoundImageUrl { get; protected set; }

        /// <summary>
        /// Gets or sets the search campus id.
        /// </summary>
        public Guid? SearchCampusId { get; protected set; }
    }
}