﻿using System;

namespace FAME.Advantage.Messages.Student
{
    public class GetStudentInputModel
    {
        public Guid CampusId { get; set; }
        public Guid ProgramVersionId { get; set; }
        public Guid TermId { get; set; }
        public Guid StudentGroupId { get; set; }
        public Guid EnrollmentStatusId { get; set; }
        public DateTime? StartDate { get; set; }
        public string StudentNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }
        public string filter { get; set; }
    }
}
