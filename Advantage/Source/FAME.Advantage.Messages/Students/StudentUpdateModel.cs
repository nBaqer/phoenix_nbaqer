﻿using System;

namespace FAME.Advantage.Messages.Student
{
    public class StudentUpdateModel
    {
        public Guid Id { get; set; }
        public Decimal Balance { get; set; }
    }
}
