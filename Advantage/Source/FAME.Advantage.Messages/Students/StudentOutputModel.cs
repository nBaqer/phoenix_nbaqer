﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Messages.Student.Enrollments;
using FAME.Advantage.Messages.Students.Enrollments;

namespace FAME.Advantage.Messages.Student
{
    public class StudentOutputModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string SSN { get; protected set; }
        public string StudentNumber { get; protected set; }
        //public string WorkEmail { get; protected set; }
        //public string HomeEmail { get; protected set; }
        public DateTime? DOB { get; protected set; }
        public string Gender { get; protected set; }

        public IEnumerable<EnrollmentOutputModel> Enrollments { get; set; }

    }
}