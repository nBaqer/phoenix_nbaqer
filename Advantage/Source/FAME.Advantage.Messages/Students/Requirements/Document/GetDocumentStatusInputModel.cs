﻿using System;

namespace FAME.Advantage.Messages.Student.Requirements.Document
{
    public class GetDocumentStatusInputModel
    {
        public bool? IsApproved { get; set; }
       
    }
}
