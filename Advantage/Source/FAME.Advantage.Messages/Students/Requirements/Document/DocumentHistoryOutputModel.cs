﻿using System;

namespace FAME.Advantage.Messages.Students.Requirements.Document
{
    public class DocumentHistoryOutputModel
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public string DocumentType { get; set; }
        
        
    }
}




