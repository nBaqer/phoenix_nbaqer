﻿using System;

namespace FAME.Advantage.Messages.Students.Requirements.Document
{
    public class PostDocumentOutputModel
    {
        public Guid Id { get; set; }
        public DateTime? DateRequested { get; set; }
        public DateTime? DateReceived { get; set; }
        public DateTime? DateModified { get; set; }
        public bool IsApproved { get; set; }
    }
}
