﻿using System;

namespace FAME.Advantage.Messages.Student.Requirements.Document
{
    public class PutDocumentInputModel
    {
        public DateTime? DateRequested { get; set; }
        public DateTime? DateReceived { get; set; }
        public Guid? DocumentStatusId { get; set; }
        public bool IsApproved { get; set; }
    }
}
