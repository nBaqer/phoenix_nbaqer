﻿using System;
using System.Collections.Generic;
using System.Security.AccessControl;
using FAME.Advantage.Messages.DocumentStatuses;
using FAME.Advantage.Messages.Students.Requirements.Document.File;

namespace FAME.Advantage.Messages.Student.Requirements.Document
{
    public class DocumentOutputModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string SystemModuleName { get; set; }
        public string RequiredFor { get; set; }
        public DateTime? DateRequested { get; set; }
        public DateTime? DateReceived { get; set; }
        public DateTime? DateModified { get; set; }
        public string ModUser { get; set; }
        public string DocumentName { get; set; }
        public StudentOutputModel Student { get; set; }
        public DocumentStatusOutputModel DocumentStatus { get; set; }
        public IEnumerable<FileOutputModel> Files { get; set; }
        public int Count { get; set; }
        public bool IsApproved { get; set; }
        public bool ShowUpdate { get; set; }
    }
}