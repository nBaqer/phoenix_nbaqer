﻿using System;

namespace FAME.Advantage.Messages.Students.Requirements.Document
{
    public class PostDocumentInputModel
    {
        public Guid? DocumentStatusId { get; set; }
        public bool IsApproved { get; set; }
        public DateTime? DateRequested { get; set; }
        public DateTime? DateReceived { get; set; }
    }
}
