﻿using System;

namespace FAME.Advantage.Messages.Student.Requirements
{
    public class RequirementEffectiveDatesOutputModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? MinimumScore { get; set; }
        public bool? IsMandatory { get; set; }
        public int? ValidDays { get; set; }
    }
}
