﻿using System;

namespace FAME.Advantage.Messages.Requirements
{
    public class TestRequirementOutputModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
