﻿using System;

namespace FAME.Advantage.Messages.Student.Requirements
{
    public class GetDocumentRequirementInputModel
    {
        public string StatusCode { get; set; }
        public Guid CampusId { get; set; }
        public Guid UserId { get; set; }
    }
}
