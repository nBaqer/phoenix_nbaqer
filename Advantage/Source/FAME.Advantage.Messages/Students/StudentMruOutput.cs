﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentMruOutput.cs" company="FAME">
//   2014, 2016
// </copyright>
// <summary>
//   Defines the StudentMruOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Students
{
    using System;

    /// <summary>
    ///  The student MRU output model.
    /// </summary>
    public class StudentMruOutputModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get;  set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        public string FullName { get;  set; }

        /// <summary>
        /// Gets or sets the SSN.
        /// </summary>
        public string SSN { get;  set; } // SSN

        /// <summary>
        /// Gets or sets the student number.
        /// </summary>
        public string StudentNumber { get;  set; } // StudentNumber

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public string StartDate { get;  set; } // StartDate

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string Status { get;  set; } // Status

        /// <summary>
        /// Gets or sets the program.
        /// </summary>
        public string Program { get; set; } // Status

        /// <summary>
        /// Gets or sets the campus.
        /// </summary>
        public string Campus { get;  set; } // Campus   

        /// <summary>
        /// Gets or sets the MRU mod date.
        /// </summary>
        public string MruModDate { get; set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public DateTime? ModDate { get; set; }

        /// <summary>
        /// Gets or sets the image URL.
        /// </summary>
        public string ImageUrl { get;  set; }
        
        /// <summary>
        /// Gets or sets the image 64.
        /// </summary>
        public string Image64 { get; set; }

        /// <summary>
        /// Gets or sets the not found image url.
        /// </summary>
        public string NotFoundImageUrl { get;  set; }

        /// <summary>
        /// Gets or sets the search campus id.
        /// </summary>
        public Guid? SearchCampusId { get;  set; }
    }
}