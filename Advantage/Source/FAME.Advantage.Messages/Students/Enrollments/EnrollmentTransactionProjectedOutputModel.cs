﻿using System;


namespace FAME.Advantage.Messages.Students.Enrollments
{
    public class EnrollmentTransactionProjectedOutputModel
    {
        public Guid PmtPeriodId { get; set; }
        public Guid StudentId { get; set; }
        public Guid StuEnrollId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; set; }
        public string ProgramVersionDescription { get; set; }
        public string ProgramVersionCode { get; set; }
        public string CampusDescription { get; set; }
        public decimal ChargeAmount { get; set; }
        public decimal CreditsHoursValue { get; set; }
        public decimal CumulativeValue { get; set; }
        public decimal? CreditHoursLeft { get; set; }
        public decimal Threshhold { get; set; }
        public int IncrementType { get; set; }
        public int InputIncrementType { get; set; }
    }
}



