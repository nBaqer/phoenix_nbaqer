﻿using System;


namespace FAME.Advantage.Messages.Students.Enrollments
{
    public class EnrollmentTransactionProjectedSummaryOutputModel
    {
     
        public decimal CreditsHoursLeft { get; set; }
        public decimal Amount { get; set; }

        public decimal CumulativeAmount { get; set; }
        public decimal CumulativeDifference { get; set; }

       
    }
}



