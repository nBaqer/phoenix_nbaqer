﻿using System;

namespace FAME.Advantage.Messages.Student.Enrollments
{
    public class EnrollmentStudentOutputModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; protected set; }
        public string StudentNumber { get; protected set; }
        //public string WorkEmail { get; protected set; }
        //public string HomeEmail { get; protected set; }
        public DateTime? DOB { get; protected set; }
        public string Gender { get; protected set; }
    }
}
