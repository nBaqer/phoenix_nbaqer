﻿using System;
using System.Collections.Generic;

namespace FAME.Advantage.Messages.Students.Enrollments
{
    /// <summary>
    /// Output for Attendance Enrollment Summary
    /// </summary>
    public class AttendanceSummaryEnrollmentOutput
    {
        public decimal PercentagePresent { get; set; }
        public decimal ScheduleHours { get; set; }
        public IList<SummaryEnrollmentTable> SummaryEnrollmentTableList { get; set; }
        public IList<SpecialAttendanceTable> SummaryEnrollmentStatusList { get; set; }
        public IList<SpecialAttendanceTable> SummaryEnrollmentLoaList { get; set; }
        public IList<SpecialAttendanceTable> SummarySuspensionList { get; set; }
    }

    /// <summary>
    /// Data for enrollment table
    /// </summary>
    public class SummaryEnrollmentTable
    {
        public string LabelField { get; set; }
        public string Present { get; set; }
        public string Absent { get; set; }
        public string Tardy { get; set; }
        public string Excused { get; set; }
        public string Makeup { get; set; }
    }

    public class SpecialAttendanceTable
    {
        public DateTime DateIni  { get; set; }
        public DateTime? DateEnd { get; set; }
        public DateTime? DateRequested { get; set; }
        public string StatusReason { get; set; }
    }
 
}
