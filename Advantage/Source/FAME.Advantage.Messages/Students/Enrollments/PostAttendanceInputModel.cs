﻿using System;

namespace FAME.Advantage.Messages.Students.Enrollments
{
    /// <summary>
    /// This model is to input changes of states of attendance or for enter new make up hours.
    /// This model is supposed to be used for update / insert a record in Attendance.
    /// </summary>
    public class PostAttendanceInputModel
    {
        public Guid IdGuid { get; set; }
        public Guid ClassSectionMeetingGuid { get; set; }
        public DateTime MeetDate { get; set; }
        public Guid EnrollmentGuid { get; set; }
        public Guid ClsSectionIdGuid { get; set; }
        public Guid PeriodIdGuid { get; set; }
        public Enumerations.Enumerations.AttendanceStatusEnum   StatusEnum { get; set; }
        public bool IsTardy { get; set; }
        public bool IsExcused { get; set; }
        public int MakeUp { get; set; }
        public int Actual { get; set; }
        public int Schedule { get; set; }
    }
}
