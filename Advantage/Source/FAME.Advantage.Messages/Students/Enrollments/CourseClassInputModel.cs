﻿using System;

namespace FAME.Advantage.Messages.Students.Enrollments
{
    public class CourseClassInputModel
    {
        public Guid TermId { get; set; }
        public Guid StuEnrollmentId { get; set; }
    }
}
