﻿using System;

namespace FAME.Advantage.Messages.Students.Enrollments
{
    /// <summary>
    ///    Create a record associated with one period and attendance
    /// </summary>
    public class AttendancePresentAbsentOutputModel : BasicAttendanceModel
    {
        public Enumerations.Enumerations.AttendanceStatusEnum StatusEnum { get; set; }
    }
}
