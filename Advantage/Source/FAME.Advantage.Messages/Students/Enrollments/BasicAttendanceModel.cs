﻿using System;

namespace FAME.Advantage.Messages.Students.Enrollments
{
    public class BasicAttendanceModel
    {
        public Guid AttendanceGuid { get; set; }
        public Guid ClassSectionMeetingGuid { get; set; }
        public DateTime AttendanceDate { get; set; }
        public Guid? PeriodId { get; set; }
        public string PeriodDescription { get; set; }
    }
}
