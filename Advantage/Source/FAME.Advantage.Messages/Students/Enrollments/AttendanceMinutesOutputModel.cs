﻿using System;

namespace FAME.Advantage.Messages.Students.Enrollments
{
    public class AttendanceMinutesOutputModel : BasicAttendanceModel
    {
        /// <summary>
        /// ParentGrid: Schedule Hours for a day or week.
        /// Child Grid: Schedule hours for a determinate period in the day.
        /// </summary>
        public int Schedule { get; set; }

        /// <summary>
        /// ParentGrid: Absent Hours for the day or week
        /// </summary>
        public int Absent { get; set; }

        /// <summary>
        /// ParentGrid: MakeUP hours for the day or week
        /// Child Grid: Not used
        /// </summary>
        public int MakeUp { get; set; }

        /// <summary>
        /// ParentGrid: PeridoActualHours = PeriodScheduleHours - PeriodAbsentHours
        /// Child Grid: (Editable) should meets the equation -> PeridoActualHours = PeriodScheduleHours - PeriodAbsentHours 
        /// </summary>
        public int Actual { get; set; }

        /// <summary>
        /// ParentGrid: Number of tardies
        /// ChildGrid: 1 tardy 0 no tardy.
        /// </summary>
        public int Tardies { get; set; }

        /// <summary>
        /// ParentGrid: Number of Excused
        /// ChildGrid: 1 excused 0 no excused.
        /// </summary>
        public int Excused { get; set; }
    }
}
