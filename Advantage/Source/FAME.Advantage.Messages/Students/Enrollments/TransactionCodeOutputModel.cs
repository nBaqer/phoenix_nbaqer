﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Messages.Students.Enrollments
{
    /// <summary>
    /// 
    /// </summary>
    public class TransactionCodeOutputModel
    {
        public Guid Id { get; set; }
        public string Description { get; protected set; }
    }
}
