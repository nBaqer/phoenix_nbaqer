﻿using System;

namespace FAME.Advantage.Messages.Students.Enrollments
{
    public class CourseClassOutputModel
    {
        /// <summary>
        /// Seccion of course ID
        /// </summary>
        public Guid ClassSectionGuid { get; set; }

        /// <summary>
        /// arReqs Guid: class Course assigned to classSection
        /// </summary>
        public Guid RequerimentGuid { get; set; }

        /// <summary>
        /// Class Course Description
        /// </summary>
        public string RequerimentDescription { get; set; }

        /// <summary>
        /// Unit type description
        /// </summary>
        public string UnitTypeDescription { get; set; }

        /// <summary>
        /// Unit Type Guid
        /// </summary>
        public Guid UnitTypeGuid { get; set; }

        public bool IsTrackTardies { get; set; }

        public int TardiesMakingAbsence { get; set; } 
    }
}
