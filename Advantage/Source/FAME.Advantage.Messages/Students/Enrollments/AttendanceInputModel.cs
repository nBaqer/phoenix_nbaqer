﻿using System;

namespace FAME.Advantage.Messages.Students.Enrollments
{
    /// <summary>
    /// Filters for StudentAttendance.
    /// </summary>
    public class AttendanceInputModel
    {
        /// <summary>
        /// Student Enrollment Id Guid...
        /// </summary>
        public Guid StuEnrollmentId { get; set; }
        
        /// <summary>
        /// Class or course Id.
        /// </summary>
        public Guid ClsSectionGuid { get; set; }
        
        /// <summary>
        /// true is day attendance summary, go to parent table in UI
        /// false is simple data for a day.
        /// </summary>
        public bool IsSummary { get; set; }

        /// <summary>
        /// true if daily if false is weekly.
        /// </summary>
        public bool IsDaily { get; set; }

        /// <summary>
        /// ChildGrid: Day that begin the filter 
        /// </summary>
        /// <remarks> In daily report the two date must be the same</remarks>
        public DateTime FilterIni { get; set; }

        /// <summary>
        /// ChildGrid: Day that end the filter
        /// </summary>
        public DateTime FilterEnd { get; set; }

        /// <summary>
        /// Campus Id needed in summary attendance.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Used in report filters.
        /// </summary>
        public Guid UserIdGuid { get; set; }
    }
}
