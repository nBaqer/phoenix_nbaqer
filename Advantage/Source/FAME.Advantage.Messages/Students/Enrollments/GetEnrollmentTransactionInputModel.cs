﻿using System;


namespace FAME.Advantage.Messages.Students.Enrollments
{
    public class GetEnrollmentTransactionInputModel
    {
       
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public Guid? UserId { get; set; }
        public Guid? CampusId { get; set; }
        public string SummaryType { get; set; }


    }
}
