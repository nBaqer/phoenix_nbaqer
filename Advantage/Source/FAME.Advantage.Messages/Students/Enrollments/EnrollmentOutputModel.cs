﻿using System;
using FAME.Advantage.Messages.Campuses.CampusGroups.EnrollmentStatuses;
using FAME.Advantage.Messages.Campuses.CampusGroups.ProgramVersions;
using FAME.Advantage.Messages.Student.Enrollments;

namespace FAME.Advantage.Messages.Students.Enrollments
{
    public class EnrollmentOutputModel
    {
        public Guid StudentId { get; protected set; }
        public DateTime EnrollmentDate { get; protected set; }

        public Guid CampusId { get; protected set; }
        public Guid EnrollmentStatusId { get; protected set; }
        public string EnrollmentId { get; protected set; }
        public DateTime? StartDate { get; set; }

        public EnrollmentStudentOutputModel Student { get; protected set; }
        public ProgramVersionOutputModel ProgramVersion { get; protected set; }
        public EnrollmentStatusOutputModel EnrollmentStatus { get; protected set; }
    }
}
