﻿using System;

namespace FAME.Advantage.Messages.Students.Enrollments
{
    public class EnrollmentAttendanceOutputModel
    {
        public Guid ID { get; set; }
        public string Description { get; set; }
        public int TardiesMakingAusence { get; set; }
        public int TrackTardies { get; set; }
        public string AttendanceType { get; set; }
    }
}
