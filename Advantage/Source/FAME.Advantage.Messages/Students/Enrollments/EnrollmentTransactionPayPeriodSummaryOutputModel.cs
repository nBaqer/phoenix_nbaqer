﻿using System;

namespace FAME.Advantage.Messages.Students.Enrollments
{
    public class EnrollmentTransactionPayPeriodSummaryOutputModel
    {
        
        public decimal ChargeAmountSum { get; set; }
        public string TransDate { get; set; }
        public DateTime TDate { get; set; }
        public string ChargeAmountSumString { get; set; }
        public DateTime date { get; set; }


    }
}
