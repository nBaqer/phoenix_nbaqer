﻿using System;
using System.Runtime;

namespace FAME.Advantage.Messages.Student.Enrollments
{
    public class GetEnrollmentsInputModel
    {
        public Guid CampusId { get; set; }
        public Guid ProgramVersionId { get; set; }
        public Guid EnrollmentStatusId { get; set; }
        public Guid StudentGroupId { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public Guid? StudentId { get; set; }
        public string SearchTerm { get; set; }
    }
}
