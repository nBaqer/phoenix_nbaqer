﻿using System;


namespace FAME.Advantage.Messages.Students.Enrollments
{
    public class GetEnrollmentTransactionProjectedInputModel
    {
        public Decimal? ThreshValue { get; set; }
        public int? IncrementType { get; set; }
    }
}



