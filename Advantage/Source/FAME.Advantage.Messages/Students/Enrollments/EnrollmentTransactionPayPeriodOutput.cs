﻿using System;


namespace FAME.Advantage.Messages.Students.Enrollments
{
    public class EnrollmentTransactionPayPeriodOutputModel
    {
        public string LastName { get; protected set; }
        public string FirstName { get; protected set; }
        public string FullName { get; protected set; }
        public string StudentNumber { get; protected set; }
        public Int32 PeriodNumber { get; protected set; }
        public decimal ChargeAmount { get; protected set; }
        public string TranDate { get; set; }

        
    }
}
