﻿
namespace FAME.Advantage.Messages
{
    public sealed class PagedOutputModel<T>
    {
        public PagedOutputModel(int total, T data)
        {
            Total = total;
            Data = data;
        }

        public int Total { get; private set; }
        public T Data { get; private set; }
    }
}
