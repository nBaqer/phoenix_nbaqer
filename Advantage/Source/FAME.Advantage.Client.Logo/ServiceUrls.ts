// This has all references to API.WEB in Advantage.....
module Logo {
  
    export var XGET_UPDATELOGO_URL = "../proxy/api/SystemStuff/Maintenance";
    export var XPOST_NEWLOGO_URL = "../proxy/api/SystemStuff/Maintenance/Logo/PostLogoFile";
    export var XPOST_UPDATE_LOGO_URL = "../proxy/api/SystemStuff/Maintenance/Logo/PostUpdateLogo";
    export var XDELETE_LOGO_URL = "../proxy/api/SystemStuff/Maintenance/Logo/Delete";
}