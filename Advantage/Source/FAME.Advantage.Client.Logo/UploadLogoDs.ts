module UploadLogo {
    export class AjaxSetup {

        userId: string;

        constructor(userIdent: string) {
            this.userId = userIdent;
        }

        // Call server service to get list of campus id.
        // Optional Error parameter: if you use it you must declare in your aspx form
        // <div id="ajaxWindowError">< /div >
        public AjaxGet(url: string, datafilter: any, callback, complete = this.CompleteAjax, error = this.ShowErrorWindow) {
        $.ajax({
            type: "GET",
            processData: false,
            cache: true,
            contentType: "application/json",
            timeout: 25000, 
            url: url,
            dataType: "json",
            headers: { Username: this.userId },
            data: datafilter,
            success: data=> { callback(data); },
            error: e=> { error(e); },
            complete: e=> { complete(e); }
        });
        return ("");
    }
        private CompleteAjax(e: any) {
           // Do nothing. 
        }

        /// To used this you need to declare in aspx html -> <div id="ajaxWindowError">< /div >
        public ShowErrorWindow(e: any) {
            var status = e.status;
            var statusText = e.statusText;
            var windowsContent = "<table id='table1' width='98%' >" + 
                "<tr>" +
                "<td colspan='2' align='center'><span class='tblLabels'>The following error occurred while communicating with the server:</span></td>" +
                "</tr>" +
                "<tr>" +
                "<td align='right' ><span class='tblLabels'>Status Code: </span></td>" +
                "<td align='left' >" + status + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td align='right' valign='top' ><span class='tblLabels'>Error Message</span></td>" +
                "<td align='left' >" + statusText +"</td>" +
                "</tr>" +
                "<tr><td>" +
                '<input id=\"btnCloseAjaxErrorWindow\" class=\"k-button btninput\" style=\"width: 60px;margin-left:230px;\" type=\"button\" value=\"Ok\" />' +
                "</td></tr>" +
                "</table>";

            $('#ajaxWindowError').html(windowsContent);
            var contentWindow = $('#ajaxWindowError').kendoWindow({
                title: 'Server Communication Error',
                visible: false,
                modal: true,
                resizable: true,
                width: '550px'
            }).data("kendoWindow");
            contentWindow.open();
            contentWindow.center();
        } 
    }
}