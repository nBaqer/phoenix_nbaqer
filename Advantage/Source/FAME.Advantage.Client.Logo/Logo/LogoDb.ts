﻿/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../serviceurls.ts" />

module Logo {
    export class LogoDb {

        // GridShow Logo Data Source
        public GetLogoGridDataSource() {
            var url = XGET_UPDATELOGO_URL;
            var ds = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: url,
                    dataType: "json",
                    type: "GET"
                }
                },
                schema: {
                    model: {
                        id: "ID",
                        Description: "Description",
                        ImagenBytes: "ImagenBytes",
                        ImgLenth: "ImgLenth",
                        ContentType: "ContentType",
                        OfficialUse: "OfficialUse",
                        ImageFile: "ImageFile",
                        ImageCode: "ImageCode"
                    }
                },
                pageSize: 10
            });
            return ds;
        }

        // Post update to the logo row.
        public PostLogoEdit(id: number, logocode: string, observations: string, officialUse: boolean) {
            $.ajax({
                url: XPOST_UPDATE_LOGO_URL,
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify({ ID: id, ImageCode: logocode, Description: observations, OfficialUse: officialUse })
            }).always(msg => {
                var window:any = $("#EditImageWindows").data("kendoWindow");
                window.close();
                window.setOptions({
                    visible: false
                });

                // window.visible = false;
                if (msg.status === 200) {
                    // Refresh Grid
                    alert("Image Updated!");
                    ($('#UploadLogo_grid').data('kendoGrid') as any).dataSource.read();
                    ($('#UploadLogo_grid').data('kendoGrid')as any).refresh();
                } else {
                    alert("Error Received: Code:" + msg.status + " " + msg.statusText + ". " + msg.responseText);
                }
               
            });
        }


        // Post update to the logo row.
        public deleteLogo(id: number) {
            $.ajax({
                url: XDELETE_LOGO_URL,
                type: 'DELETE',
                dataType: 'json',
                data: JSON.stringify({ ID: id })
            }).always(msg => {
                var window:any = $("#DeleteImageWindows").data("kendoWindow");
                    window.close();
                window.setOptions({
                    visible: false
                });
                // window.visible = false;
                    if (msg.status == 200) {
                        // Refresh grid
                        alert("Image Deleted!");
                    ($('#UploadLogo_grid').data('kendoGrid')as any).dataSource.read();
                    ($('#UploadLogo_grid').data('kendoGrid')as any).refresh();
                   } else {
                        alert("Error Received: Code:" + msg.status + " " + msg.statusText + ". " + msg.responseText);
                    }

                });
        }

    //#region Common function to display errors  .
        public showDataSourceError(e) {
        try {
            if (e.xhr != undefined) {
                if (e.xhr.responseText == undefined) {
                    alert("Error: " + e.xhr.statusText + ", " + e.xhr.responseXML);
                } else {
                    alert("Error: " + e.xhr.statusText + ", " + e.xhr.responseText);
                }
            } else {
                if (e.responseText == undefined) {
                    alert("Error: " + e.statusText + ", " + e.responseXML);
                } else {
                    alert("Error: " + e.statusText + ", " + e.responseText);
                }
            }
        } catch (ex) {
            alert("Server returned an undefined error");
        }
    }
    }
}