﻿/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
module Logo {

    export class Logo {
        public ViewModel: LogoViewModel;

        public SelectedEnrollment: kendo.Observable;

        // Class Constructor
        constructor() {
            this.ViewModel = new LogoViewModel();
            var viewModel = this.ViewModel; 
            
            // Configure Image Grid.............................................................
            $("#UploadLogo_grid").kendoGrid({
                dataSource: this.ViewModel.LogoGridDataSource,
                rowTemplate: kendo.template($("#UploadLogo_grid_rowTemplate").html()),
                altRowTemplate: kendo.template($("#UploadLogo_grid_altrowTemplate").html()),
                
                //height: 500,
                pageable: {
                    pageSize: 5,
                    pageSizes: true
                }
            });

            // Edit Image button in logo Grid
            $("#UploadLogo_grid").on("click", ".k-grid-edit", e => {
                var row = $(e.target).closest("tr");
                var grid = $("#UploadLogo_grid").data("kendoGrid") as any;
                var dataItem = grid.dataItem(row);
                viewModel.editImage(dataItem);
            });

            // Delete Image button in Logo Grid
            $("#UploadLogo_grid").on("click", ".k-grid-delete", e => {
                var row = $(e.target).closest("tr");
                var grid = $("#UploadLogo_grid").data("kendoGrid") as any;
                var dataItem = grid.dataItem(row);
                viewModel.deleteImage(dataItem);
            });

            //Configure the windows to edit Image when the Grid Edit button was clicked
            $("#EditImageWindows").kendoWindow(
                {
                    actions: ["Close"],
                    title: "Advantage: Edit Image",
                    width: "400px",
                    height: "200px",
                    visible: false,
                    modal: true,
                    resizable: false
                    
                });

            // Save Button in the windows of update image
            $("#btnEditSave").kendoButton({
                click: e => { viewModel.updateImageServer(e); }
            });

            // Configure Windows to delete image when the grid delete button was clicked
            $("#DeleteImageWindows").kendoWindow(
                {
                    actions: ["Close"],
                    title: "Advantage: Delete Image",
                    width: "400px",
                    height: "200px",
                    visible: false,
                    modal: true,
                    resizable: false

                });

            // Delete button in the windows for delete image.
            $("#btnDeleteImage").kendoButton({
                click: e => { viewModel.deleteImageServer(e); }
            });
          
            // Configure Get new Logo Button  ----------------------------------------------------------------
            $("#UploadLogo_btnInsert").kendoButton(
                {
                   
                    enable: true,
                    click: this.ViewModel.loadLogoFromFileClick
                });

            //Configure the windows with the upload file control
            $("#UploadWindows").kendoWindow(
                {
                    actions: ["Close"],
                    title: "Advantage: Load the new Image",
                    width: "400px",
                    height:"200px",
                    visible: false,
                    close: this.ViewModel.windowsClosed
                });

            // Configure the Upload Control, inside the windows
            $("#files").kendoUpload({
                async: {
                    saveUrl:  XPOST_NEWLOGO_URL,
                    
                    autoUpload: true
                },
                success: viewModel.fileUploaded
            });

            // This bind the view model with all.-------------------------------------------------------------------------
            kendo.init($("#LogoViewModelRegion"));
            kendo.bind($("#LogoViewModelRegion"), this.ViewModel);
            //#endregion
           
        }

    }


    //This is in StudentAttendance.aspx page in document ready.....

    $(document).ready(() => {

        var manager = new Logo();
        
    });


}