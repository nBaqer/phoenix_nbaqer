﻿/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
module Logo {

    export class LogoViewModel extends kendo.Observable {

        public LogoDataServices: LogoDb;
        public LogoGridDataSource: kendo.data.DataSource;
        private selectedRow: any;

        constructor() {
            super();
            this.LogoDataServices = new LogoDb();
            this.LogoGridDataSource = this.LogoDataServices.GetLogoGridDataSource();

        }

        // Event Click for button load new image.
        public loadLogoFromFileClick = () => {
            // Show the windows 
            var window:any = $('#UploadWindows').data("kendoWindow");
            window.setOptions({
                visible: true
            });
            //window.visible = true;
            window.open();
            window.center();
            // hidden this button
            var buttonInsert:any = $("#UploadLogo_btnInsert").data("kendoButton");
            buttonInsert.enable(false);
        };

       public windowsClosed() {
            var buttonInsert:any = $("#UploadLogo_btnInsert").data("kendoButton");
           buttonInsert.enable(true);
           ($('#UploadLogo_grid').data('kendoGrid') as any).dataSource.read();
           ($('#UploadLogo_grid').data('kendoGrid') as any).refresh();
       }

        // Click button Edit image in Grid
        public editImage(row: any ) {
            this.selectedRow = row;
            var window:any = $("#EditImageWindows").data("kendoWindow");
            window.setOptions({
                visible: true
            });
            //window.visible = true;
            window.open();
            window.center();
           
           // window.content(row); //window.dataSource = row;
            var wimage = $("#wImage");
            wimage.attr("src", "data:" + row.ContentType + ";base64," + row.ImagenBytes);
            $("#labelFileName").text(row.ImageFile);
            $("#cbOfficialLogo").prop("checked", row.OfficialUse);
            $("#tbObservations").text(row.Description);
            $("#tbImageCode").val(row.ImageCode);
            return 0;
        }

        // Click button delete image in grid
        deleteImage(row: any) {
            this.selectedRow = row;
            var window:any = $("#DeleteImageWindows").data("kendoWindow");
            window.setOptions({
                visible: true
            });
            //window.visible = true;
            window.open();
            window.center();
            //window.content(row); //window.dataSource = row; 
            var wimage = $("#deleteImage");
            wimage.attr("src", "data:" + row.ContentType + ";base64," + row.ImagenBytes);
            $("#deleteFileName").text(row.ImageFile);
            return 0;
        }

        // Click over Save button in Edit Dialog
        updateImageServer(object: Object) {
            this.selectedRow.Observations = $("#tbObservations").val();
            this.selectedRow.ImageCode = $("#tbImageCode").val();
            this.selectedRow.OfficialUse = $("#cbOfficialLogo").prop("checked");
            this.LogoDataServices.PostLogoEdit(this.selectedRow.ID, this.selectedRow.ImageCode, this.selectedRow.Observations, this.selectedRow.OfficialUse);
        }

        // Click button on windows delete image
        deleteImageServer(object: Object) {
            this.LogoDataServices.deleteLogo(this.selectedRow.ID);
        }

        fileUploaded() {
           
        }

    } // End Class
} // End module