/// <reference path="../Common.TypeScript/References/kendo.all.d.ts" />
/// <reference path="../Common.TypeScript/References/jQuery.d.ts" />

module UploadLogo {
    export class LogoManager {

        public dataSource: kendo.data.DataSource;
        public viewModel: StudentAttendanceViewModel;
        public ajaxCall: AjaxSetup;

        constructor() {

            var userId: string = jQuery('#hdnUserName').val();
            this.ajaxCall = new AjaxSetup(userId);

            //this.dataSource = new kendo.data.DataSource({
            //    schema: {
            //        model: {
            //            id: "ID",
            //            Description: "Description",
            //            ImagenBytes: "ImagenBytes",
            //            ImgLenth: "ImgLenth",
            //            ContentType: "ContentType",
            //            OfficialUse: "OfficialUse",
            //            ImageFile: "ImageFile",
            //            ImageCode: "ImageCode"
            //        }
            //    },

            //});

         


        }

        //public kendoUpload = $("#files").kendoUpload({
        //    async: {
        //        saveUrl: "save",
        //        removeUrl: "remove",
        //        autoUpload: true
        //    }
        //});


        //public UploadLogoViewModel = kendo.observable({
        //    items: this.dataSource

        //});

        public ManageResponse(dataLoaded) {
            if (this.dataSource == null) {
                this.dataSource = new kendo.data.DataSource({
                    schema: {
                        model: {
                            id: "ID",
                            Description: "Description",
                            ImagenBytes: "ImagenBytes",
                            ImgLenth: "ImgLenth",
                            ContentType: "ContentType",
                            OfficialUse: "OfficialUse",
                            ImageFile: "ImageFile",
                            ImageCode: "ImageCode"
                        }
                    },

                });
            }
            for (var i = 0; i < dataLoaded.length; i++) {
                if (dataLoaded[i].Description == null) {
                    dataLoaded[i].Description = "No info available";
                }
                this.dataSource.add(dataLoaded[i]);
                this.dataSource.sync();
            }

            //$("#UploadLogo_grid").kendoGrid({
            //    dataSource: this.dataSource,
            //    rowTemplate: kendo.template($("#UploadLogo_grid_rowTemplate").html()),
            //    altRowTemplate: kendo.template($("#UploadLogo_grid_altrowTemplate").html()),
            //    sorteable: false,
            //    width: 800,
            //    height: 500,
            //    pageable: {
            //        pageSize: 5,
            //        pageSizes: true
            //    }


            //});

        }

    }
}



$(document).ready(() => {
    var manager = new UploadLogo.LogoManager();
   
    //kendo.bind($("#UploadLogo_grid"), manager.UploadLogoViewModel);
    manager.ajaxCall.AjaxGet(UploadLogo.XGET_UPDATELOGO_URL, null, manager.ManageResponse);

    var viewModel = kendo.observable({
        isEnabled: true,
        isVisible: true,
        fileBin: null,
        onSelect: e=> {
            var message = $.map(e.files, function (file) { return file.name; }).join(", ");

        }
    });
    kendo.bind($("#UploadLogoUpload"), viewModel);

});






