﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />

module Help{


    export class WhatIsNew {

        constructor() {
            $.ajax({
                url: XGET_ADVANTAGE_VERSIONS,
                type: 'GET',
                dataType: 'text',
            }).done(msg => {
                var versions  = msg.split(",");
                    $("#versionLabel").text("What's new in version " + versions[2].replace("\"","")+ " ?");
                })
                .fail(msg => {
                    $("#versionLabel").text("What's new in version 3.7: (" + msg.responseText + ")"); 
                });
        }
    }
} 