﻿Imports Fame.Integration.Adv.Afa.Messages.WebApi.Entities
Imports Fame.Advantage.DataAccess.LINQ
Imports Fame.Advantage.DataAccess.LINQ.FAME.Advantage.DataAccess.LINQ.Common.Constants

Public Class LocationsHelper
    Private ReadOnly connectionString As String
    Private ReadOnly userName As String
    Public Sub New(connectionString As String, Optional userName As String = "")
        Me.connectionString = connectionString
        Me.userName = userName
    End Sub
    Public Function GetAFALocationsByCampusGroup(campGrpId As String) As List(Of String)
        Dim campusDA = New CampusDA(Me.connectionString)
        Dim appSettingsDA = New AppSettingsDA(Me.connectionString)

        Dim campusIds = campusDA.GetCampusesForCampusGroups(New List(Of String) From {campGrpId}).Select(Function(x) x.CampusId).ToList()
        Dim campusesWithAFAIntegration = Me.GetCampusesWithAFAIntegration(campusIds)

        Dim locations = campusDA.GetCampusesById(campusesWithAFAIntegration).Select(Function(c) c.CmsId).ToList()
        Return locations
    End Function


    Public Function GetAFALocationByCampus(campusId As String) As List(Of String)
        Dim campusDA = New CampusDA(Me.connectionString)

        Dim campusIds = New List(Of Guid) From {Guid.Parse(campusId)}
        Dim campusesWithAFAIntegration = Me.GetCampusesWithAFAIntegration(campusIds)

        Dim locations = campusDA.GetCampusesById(campusesWithAFAIntegration).Select(Function(c) c.CmsId).ToList()
        Return locations
    End Function

    Public Function GetCampusesWithAFAIntegration(campusIds As IEnumerable(Of Guid)) As List(Of Guid)
        Dim appSettingsDA = New AppSettingsDA(Me.connectionString)
        Dim campuses = appSettingsDA.GetAppSettingValueByCampus(AppSettingKeyNames.EnableAFAIntegration, campusIds).Where(Function(cvp) cvp.Value.Equals("yes", StringComparison.InvariantCultureIgnoreCase)).Select(Function(c) c.CampusId).ToList
        Return campuses
    End Function
    Public Function SyncEnrollmentsWithAFA(enrollments As IEnumerable(Of AfaEnrollment)) As List(Of AdvStagingEnrollmentV3)
        Dim stuEnrollDA = New StudentEnrollmentDA(connectionString)
        Dim time = DateTime.Now

        Dim campusesWithAFAIntegration = Me.GetCampusesWithAFAIntegration(enrollments.Select(Function(enr) enr.CampusId).Distinct())
        enrollments = enrollments.Where(Function(enr) campusesWithAFAIntegration.Contains(enr.CampusId) AndAlso enr.StartDate.HasValue AndAlso enr.ExpectedGradDate.HasValue AndAlso enr.AFAStudentId.HasValue AndAlso Not String.IsNullOrEmpty(enr.CmsId)).ToList()

        Dim afaEnrollments = enrollments _
                .Select(Function(enrollment) New AdvStagingEnrollmentV3 With {.SISEnrollmentID = enrollment.StudentEnrollmentId, .IDStudent = enrollment.AFAStudentId,
                               .LocationCMSID = enrollment.CmsId, .ProgramVersionID = enrollment.ProgramVersionId, .ProgramName = enrollment.ProgramName, .StartDate = enrollment.StartDate.Value, .GradDate = enrollment.ExpectedGradDate.Value,
                               .DateCreated = time, .UserCreated = Me.userName, .DateUpdated = time, .UserUpdated = Me.userName, .AdmissionsCriteria = enrollment.AdmissionsCriteria, .DroppedDate = enrollment.DroppedDate,
                               .EnrollmentStatus = enrollment.EnrollmentStatus, .LastDateAttendance = enrollment.LastDateAttendance, .LeaveOfAbsenceDate = enrollment.LeaveOfAbsenceDate, .ReturnFromLOADate = enrollment.ReturnFromLOADate,
                               .SSN = enrollment.SSN, .StatusEffectiveDate = enrollment.StatusEffectiveDate, .AdvStudentNumber=enrollment.AdvStudentNumber, .HoursPerWeek = enrollment.HoursPerWeek, .InSchoolTransferHours = enrollment.InSchoolTransferHrs, .PriorSchoolHours = enrollment.PriorSchoolHrs}).ToList()
        Return afaEnrollments
    End Function
    Public Function SyncEnrollmentsWithAFA(stuEnrollIds As IEnumerable(Of String)) As List(Of AdvStagingEnrollmentV1)
        Dim stuEnrollDA = New StudentEnrollmentDA(connectionString)
        Dim enrollments = stuEnrollDA.GetEnrollmentsForAFA(stuEnrollIds)
        Dim time = DateTime.Now

        Dim campusesWithAFAIntegration = Me.GetCampusesWithAFAIntegration(enrollments.Select(Function(enr) enr.CampusId).Distinct())
        enrollments = enrollments.Where(Function(enr) campusesWithAFAIntegration.Contains(enr.CampusId) AndAlso enr.StartDate.HasValue AndAlso enr.ExpectedGradDate.HasValue AndAlso enr.AFAStudentId.HasValue AndAlso Not String.IsNullOrEmpty(enr.CmsId)).ToList()

        Dim afaEnrollments = enrollments _
                .Select(Function(enr) New AdvStagingEnrollmentV1 With {.SISEnrollmentID = enr.StudentEnrollmentId, .IDStudent = enr.AFAStudentId.Value, .LocationCMSID = enr.CmsId, .ProgramVersionID = enr.ProgramVersionId, .ProgramName = enr.ProgramName, .StartDate = enr.StartDate.Value, .GradDate = enr.ExpectedGradDate.Value,
                           .DateCreated = time, .UserCreated = Me.userName, .DateUpdated = time, .UserUpdated = Me.userName}).ToList()
        Return afaEnrollments
    End Function

    Public Function SyncEnrollmentsWithAFA(enrollment As AfaEnrollment) As List(Of AdvStagingEnrollmentV3)
        Dim afaStudentId = New LeadDA(connectionString).GetAfaStudentId(enrollment.StudentId)
        Dim time = DateTime.Now
        Dim locations = New LocationsHelper(connectionString).GetAFALocationByCampus(enrollment.CampusId.ToString)
        If (enrollment.StartDate.HasValue AndAlso enrollment.ExpectedGradDate.HasValue AndAlso afaStudentId.HasValue) Then
            Dim afaEnrollments = locations _
                    .Select(Function(loc) New AdvStagingEnrollmentV3 With {.SISEnrollmentID = enrollment.StudentEnrollmentId, .IDStudent = afaStudentId.Value,
                               .LocationCMSID = loc, .ProgramVersionID = enrollment.ProgramVersionId, .ProgramName = enrollment.ProgramName, .StartDate = enrollment.StartDate.Value, .GradDate = enrollment.ExpectedGradDate.Value,
                               .DateCreated = time, .UserCreated = Me.userName, .DateUpdated = time, .UserUpdated = Me.userName, .AdmissionsCriteria = enrollment.AdmissionsCriteria, .DroppedDate = enrollment.DroppedDate,
                               .EnrollmentStatus = enrollment.EnrollmentStatus, .LastDateAttendance = enrollment.LastDateAttendance, .LeaveOfAbsenceDate = enrollment.LeaveOfAbsenceDate, .ReturnFromLOADate = enrollment.ReturnFromLOADate,
                               .SSN = enrollment.SSN, .StatusEffectiveDate = enrollment.StatusEffectiveDate,.AdvStudentNumber = enrollment.AdvStudentNumber, .HoursPerWeek = enrollment.HoursPerWeek, .InSchoolTransferHours = enrollment.InSchoolTransferHrs, .PriorSchoolHours = enrollment.PriorSchoolHrs}).ToList()
            Return afaEnrollments
        End If
        Return New List(Of AdvStagingEnrollmentV3)
    End Function



End Class
