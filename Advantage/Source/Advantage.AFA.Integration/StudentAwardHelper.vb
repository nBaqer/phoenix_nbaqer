﻿Imports Fame.Integration.Adv.Afa.Messages.WebApi.Entities
Imports Fame.Advantage.DataAccess.LINQ
Imports Fame.Advantage.DataAccess.LINQ.FAME.Advantage.DataAccess.LINQ.Common.Constants

Public Class StudentAwardHelper
    Private ReadOnly connectionString As String
    Private ReadOnly userName As String
    Private ReadOnly apiUrl As String
    Private ReadOnly token As String

    Public Sub New(connectionString As String, apiUrl As String, token As String, Optional userName As String = "")
        Me.connectionString = connectionString
        Me.apiUrl = apiUrl
        Me.token = token
        Me.userName = userName
    End Sub
    Public Async Function PostDisbursementAdjustment(campusId As String, refunds As IEnumerable(Of Refund)) As Task(Of Boolean)
        Try
            Dim location = New LocationsHelper(connectionString, userName).GetAFALocationByCampus(campusId).FirstOrDefault()
            If (Not String.IsNullOrEmpty(location)) Then

                Dim wapiSettingsDA = New WapiSettingsDA(connectionString)
                Dim wapiSetting = wapiSettingsDA.GetWapiSettingsByCode(WapiCodes.AFA_INTEGRATION)
                Dim time = DateTime.Now
                Dim result = True
                If (wapiSetting IsNot Nothing) Then

                    Dim afaDisReq = New Fame.AFA.Api.Library.FinancialAid.DisbursementRequest(wapiSetting.ClientKey, wapiSetting.UserName, wapiSetting.Password, wapiSetting.ExternalUrl)
                    For Each refund As Refund In refunds
                        If (refund IsNot Nothing) Then
                            Dim afaStagingDisbursementRefund = New AdvStagingRefundV1 With {.SISEnrollmentID = refund.StudentEnrollmentId, .AwardID = refund.FinancialAidId,
                                    .DateCreated = time, .DateUpdated = time, .DisbursementSequenceNumber = refund.DisbursementNumber, .RefundAmount = refund.AdjustmentAmount,
                                    .RefundDate = refund.AdjustmentDate, .SSN = refund.SSN,
                                    .LocationCMSID = location, .UserCreated = userName, .UserUpdated = userName}
                            result = Await afaDisReq.PostDisbursementAdjustment(New List(Of AdvStagingRefundV1) From {afaStagingDisbursementRefund})
                        End If

                    Next
                    Return result
                End If

            End If
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function


End Class
