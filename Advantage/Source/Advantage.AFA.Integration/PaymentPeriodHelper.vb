﻿Imports Fame.Integration.Adv.Afa.Messages.WebApi.Entities
Imports Fame.Advantage.DataAccess.LINQ
Imports Fame.Advantage.DataAccess.LINQ.FAME.Advantage.DataAccess.LINQ.Common.Constants

Public Class PaymentPeriodHelper
    Private ReadOnly connectionString As String
    Private ReadOnly userName As String
    Private ReadOnly apiUrl As String
    Private ReadOnly token As String

    Public Sub New(connectionString As String, apiUrl As String, token As String, Optional userName As String = "")
        Me.connectionString = connectionString
        Me.apiUrl = apiUrl
        Me.token = token
        Me.userName = userName
    End Sub
    Public Async Function PostPaymentPeriodAttendance(campusId As String, studentEnrollmentId As Guid) As Task(Of Boolean)
        Try
            Dim wapiSettingsDA = New WapiSettingsDA(connectionString)
            Dim wapiSetting = wapiSettingsDA.GetWapiSettingsByCode(WapiCodes.AFA_INTEGRATION)
            Dim time = DateTime.Now
            If (wapiSetting IsNot Nothing) Then

                Dim location = New LocationsHelper(connectionString, userName).GetAFALocationByCampus(campusId).FirstOrDefault()
                If (Not String.IsNullOrEmpty(location)) Then
                    Dim advPPReq = New Fame.Advantage.Api.Library.AcademicRecords.PaymentPeriodRequest(apiUrl, token)
                    Dim afaPPReq = New Fame.AFA.Api.Library.AcademicRecords.PaymentPeriodRequest(wapiSetting.ClientKey, wapiSetting.UserName, wapiSetting.Password, wapiSetting.ExternalUrl)
                    Dim attendance =  advPPReq.CalculatePaymentPeriodAttendance(studentEnrollmentId)
                    If (attendance IsNot Nothing) Then
                        Dim afaStagingPaymentPeriod = New AdvStagingAttendanceV1 With {.SISEnrollmentID = attendance.SISEnrollmentID, .IDStudent = attendance.IDStudent,
                                .DateCreated = time, .DateUpdated = time, .EffectiveDate = attendance.EffectiveDate,
                                .EndDate = attendance.EndDate, .HoursCreditEarned = attendance.HoursCreditEarned, .SSN = attendance.SSN,
                                .LocationCMSID = location, .PaymentPeriodName = attendance.PaymentPeriodName, .SAP = attendance.SAP,
                                .StartDate = attendance.StartDate, .UserCreated = userName, .UserUpdated = userName}
                        Return  afaPPReq.PostPaymentPeriodAttendanceUpdate(afaStagingPaymentPeriod)
                    End If


                End If

            End If
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function
    Public Async Function GetAttendanceSummary(studentEnrollmentId As Guid) As Task(Of Boolean)
        Try
                    Dim advPPReq = New Fame.Advantage.Api.Library.AcademicRecords.PaymentPeriodRequest(apiUrl, token)
                    advPPReq.GetAttendanceSummary(studentEnrollmentId)
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function

    Public Async Function PostPaymentPeriodAttendance(campusId As String, studentEnrollmentIds As List(Of Guid)) As Task(Of Boolean)
        Try
            Dim wapiSettingsDA = New WapiSettingsDA(connectionString)
            Dim wapiSetting = wapiSettingsDA.GetWapiSettingsByCode(WapiCodes.AFA_INTEGRATION)
            Dim time = DateTime.Now
            If (wapiSetting IsNot Nothing) Then

                Dim location = New LocationsHelper(connectionString, userName).GetAFALocationByCampus(campusId).FirstOrDefault()
                If (Not String.IsNullOrEmpty(location)) Then
                    Dim result = True
                    Dim advPPReq = New Fame.Advantage.Api.Library.AcademicRecords.PaymentPeriodRequest(apiUrl, token)
                    Dim afaPPReq = New Fame.AFA.Api.Library.AcademicRecords.PaymentPeriodRequest(wapiSetting.ClientKey, wapiSetting.UserName, wapiSetting.Password, wapiSetting.ExternalUrl)
                    For Each enrollmentId As Guid In studentEnrollmentIds
                        Try
                            Dim attendance = advPPReq.CalculatePaymentPeriodAttendance(enrollmentId)
                            If (attendance IsNot Nothing) Then
                                Dim afaStagingPaymentPeriod = New AdvStagingAttendanceV1 With {.SISEnrollmentID = attendance.SISEnrollmentID, .IDStudent = attendance.IDStudent,
                                                               .DateCreated = time, .DateUpdated = time, .EffectiveDate = attendance.EffectiveDate,
                                                               .EndDate = attendance.EndDate, .HoursCreditEarned = attendance.HoursCreditEarned,.SSN = attendance.SSN,
                                                               .LocationCMSID = location, .PaymentPeriodName = attendance.PaymentPeriodName, .SAP = attendance.SAP,
                                                               .StartDate = attendance.StartDate, .UserCreated = userName, .UserUpdated = userName}
                                If (Not afaPPReq.PostPaymentPeriodAttendanceUpdate(afaStagingPaymentPeriod)) Then
                                    result = False
                                End If
                            End If
                        Catch ex As Exception
                            result = False
                            Continue For
                        End Try

                    Next
                    Return result
                End If

            End If
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function
End Class
