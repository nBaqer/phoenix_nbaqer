﻿using System;
using System.Collections.Generic;
using System.Linq;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Users;
using FAME.Advantage.Domain.Users.UserRoles;

namespace FAME.Advantage.Services.Lib.BusinessLogic.Users
{
    /// <summary>
    /// User Roles 
    /// </summary>
    public class UserRolesBo
    {
        private IRepositoryWithTypedID<Guid> repositoryguid;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        public UserRolesBo(IRepositoryWithTypedID<Guid> repository)
        {
            repositoryguid = repository;
        }


        /// <summary>
        /// Return true if one role of a determinate user in a specific campusId, is in the supply list of roles codes.
        /// </summary>
        /// <param name="syrolesIds"></param>
        /// <param name="userId"></param>
        /// <param name="campusId"></param>
        /// <returns></returns>
        public bool IsUserAuthorized(IList<int> syrolesIds, string userId, string campusId)
        {
                var campusGuid = new Guid(campusId);
                var userGuid = new Guid(userId);

                // Get the roles associates with the user....

                var roles = repositoryguid.Query<UserRolesCampusGroupBridge>()
                                          .Where(n => n.CampusGroup.Campuses.Any(c => c.ID == campusGuid) && n.User.ID == userGuid)
                                              .Select(n => n.Role).Distinct();

            foreach (Roles role in roles)
            {
                Roles role1 = role;
                if (syrolesIds.Any(rolesAuthorizedId => rolesAuthorizedId == role1.SystemRole.ID))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Test if the user has the confidential role.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="campusId"></param>
        /// <returns></returns>
        public bool IsRolConfidential( string userId, string campusId)
        {
            var campusGuid = new Guid(campusId);
            var userGuid = new Guid(userId);

            // Get the roles associates with the user....

            var roles = repositoryguid.Query<UserRolesCampusGroupBridge>()
                                      .Where(n => n.CampusGroup.Campuses.Any(c => c.ID == campusGuid) && n.User.ID == userGuid)
                                          .Select(n => n.Role).Distinct();
            foreach (Roles role in roles)
            {
                if (role.SystemRole.ID == 16)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// return true if the user is support.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool IsSupport(string userId)
        {
            var isSupport = repositoryguid.Query<User>().Any(x => x.ID.ToString() == userId && x.UserName.ToUpper() == "SUPPORT");
            return isSupport;
        }
    }
}
