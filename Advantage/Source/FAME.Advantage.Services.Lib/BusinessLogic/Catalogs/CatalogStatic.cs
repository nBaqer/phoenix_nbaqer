﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CatalogStatic.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   This class hold procedures statics used in different part of the application
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.Catalogs
{
    using System;
    using System.Collections.Generic;
    using SystemStuff;
    using Campuses;
    using Domain.Infrastructure.Entities;
    using Messages.Common;

    /// <summary>
    /// This class hold procedures statics used in different part of the application
    /// </summary>
    public static class CatalogStatic
    {
        /// <summary>
        /// This procedure return the item list assigned to each <code>FldName</code>
        /// they return the item filtered only with status active.
        /// Some items return with the Campus ID. (no filtered, only added the information)
        /// </summary>
        /// <param name="filter">contain the fieldName that identified the resource</param>
        /// <param name="repository">repository with GUID</param>
        /// <param name="repositoryWithInt">repository INT</param>
        /// <returns>The list of item assigned to the control as DropDownOutputModel</returns>
        /// <exception cref="ApplicationException">You need to supply a parameter </exception>
        /// <remarks>If the parameter no match a empty list is returned</remarks>
        public static IEnumerable<DropDownOutputModel> ResourcesDropDownOutputModels(CatalogsInputModel filter, IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            // Constrains...............
            if (filter.FldName != null)
            {
                switch (filter.FldName)
                {
                    case "AddressType":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllAddressTypeItems();
                            return result;
                        }

                    case "admincriteriaid":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllAdminCriteriaItems();
                            return result;
                        }

                    case "AdmissionsRep":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllAdmissionRepItems();
                            return result;
                        }

                    case "AreaId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllProgramGroupsItems();
                            return result;
                        }

                    case "AttendTypeId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllAttendTypesItems();
                            return result;
                        }

                    case "CampusId":
                        {
                            var bo1 = new CampusesBo(repository, repositoryWithInt);
                            var result = bo1.GetAllActiveCampusItems(filter.UserId.ToString());
                            return result;
                        }

                    case "LeadAssignedToId":
                        {
                            var bo1 = new CampusesBo(repository, repositoryWithInt);
                            var result = bo1.GetAllActiveCampusItems(filter.UserId.ToString());
                            return result;
                        }

                    case "Citizen":
                        {
                            var bo1 = new CampusesBo(repository, repositoryWithInt);
                            var result = bo1.GetAllCitizenItems();
                            return result;
                        }

                    case "Country":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllCountriesItems();
                            return result;
                        }

                    case "County":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllCountiesItems();
                            return result;
                        }

                    case "DependencyTypeId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllDependencyTypeIdItems();
                            return result;
                        }

                    case "TuitionCategoryId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllTuitionCategoryIdItems();
                            return result;
                        }

                    case "ShiftId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllShiftIdItems();
                            return result;
                        }

                    case "GeographicTypeId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllGeographicTypeIdItems();
                            return result;
                        }

                    case "Nationality":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllNationalityItems();
                            return result;
                        }

                    case "BillingMethodId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllChargingMethodItems();
                            return result;
                        }

                    case "Description":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllReasonLeadNotEnrolled();
                            return result;
                        }

                    case "EmailTypeId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllEmailTypeIdItems();
                            return result;
                        }

                    case "FamilyIncome":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllFamilyIncomeItems(filter.CampusId.ToString());
                            return result;
                        }

                    case "FamilyIncomeID":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllFamilyIncomeItems(filter.CampusId.ToString());
                            return result;
                        }

                    case "Gender":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllGenderItems(filter.CampusId.ToString());
                            return result;
                        }

                    case "HighSchool":
                        {
                            //var bo = new CatalogsBo(repository, repositoryWithInt);
                            //var result = bo.GetAllHighSchoolItems();
                           // return result;
                            return new List<DropDownOutputModel>();
                        }

                    case "HousingId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllHousingItems();
                            return result;
                        }

                    case "LeadStatus":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllLeadStatusItems();
                            return result;
                        }

                    case "MaritalStatus":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllMaritalStatusItems();
                            return result;
                        }

                    case "PhoneType":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllPhoneTypeItems();
                            return result;
                        }

                    case "PreferredContactId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllPreferredContactItems();
                            return result;
                        }

                    case "Prefix":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllPrefixItems();
                            return result;
                        }

                    case "PreviousEducation":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetPreviousEducationItems();
                            return result;
                        }

                    case "ProgramScheduleId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllProgramScheduleItems();
                            return result;
                        }

                    case "ScheduleId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllProgramScheduleItems();
                            return result;
                        }

                    case "Race":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllRaceItems();
                            return result;
                        }

                    case "StateId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllStatesItems();
                            return result;
                        }
                    case "EnrollStateId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllStatesItems();
                            return result;
                        }
                    case "SourceAdvertisement":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllSourceAdvertisementItems(filter.CampusId.ToString());
                            return result;
                        }

                    case "SourceCategoryID":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllCategoriesItems(filter.CampusId.ToString());
                            return result;
                        }

                    case "SourceTypeID":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllCategoriesTypesItems(filter.CampusId.ToString());
                            return result;
                        }

                    case "Sponsor":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllSponsorsItems();
                            return result;
                        }

                    case "Suffix":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllSuffixItems();
                            return result;
                        }

                    case "TransportationId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllTransportationItems();
                            return result;
                        }

                    case "EdLvlId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllEducationLevelItems();
                            return result;
                        }

                    case "DegCertSeekingId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllDegSeekingItems(filter.CampusId);
                            return result;
                        }

                    case "PrgVersionTypeId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllProgramVersionTypeItems();
                            return result;
                        }

                    case "PrgVerId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllActiveProgramVersionItems();
                            return result;
                        }

                    case "FAAdvisorId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllActiveFinAidAdvisor();
                            return result;
                        }

                    case "AcademicAdvisor":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllActiveAcademicAdvisor();
                            return result;
                        }

                    default:
                        {
                            return new List<DropDownOutputModel>();
                        }
                }
            }

            // If other values
            throw new ApplicationException("You need to supply a parameter.");
        }

        /// <summary>
        /// This procedure two get filtered drop-down output model for catalogs
        /// The catalog is filtered by CampusId, and a optional filter that vary for each catalog
        /// Also only active items are gotten.
        /// </summary>
        /// <param name="filter">filter to select the catalog</param>
        /// <param name="repository">repository in GUID</param>
        /// <param name="repositoryWithInt">repository with integer</param>
        /// <returns>Return the catalog content with the list of campus associated to it. </returns>
        public static IEnumerable<DropDownOutputModel> FilteredDropDownOutputModels(CatalogsInputModel filter, IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            // Constrains...............
            if (filter.FldName != null)
            {
                switch (filter.FldName)
                {
                    case "AddressType":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllAddressTypeItems();
                            return result;
                        }

                    case "admincriteriaid":
                        {
                            throw new NotImplementedException();
                        }

                    case "AdmissionsRep":
                        {
                            throw new NotImplementedException();
                        }

                    case "AreaId":
                        {
                            // var bo = new CatalogsBo(repository, repositoryWithInt);
                            // var result = bo.GetFilteredProgramGroupsItems(filter);
                            // return result;
                            break;
                        }

                    case "AttendTypeId":
                        {
                            throw new NotImplementedException();
                        }

                    case "BestTimeId":
                        {
                            throw new NotImplementedException();
                        }

                    case "CampusId":
                        {
                            throw new NotImplementedException();
                        }

                    case "Citizen":
                        {
                            throw new NotImplementedException();
                        }

                    case "Country":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetFilteredCountries(filter);
                            return result;
                        }

                    case "CountryWithCode":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetFilteredCountries(filter, true);
                            return result;
                        }

                    case "County":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllCountiesItems();
                            return result;
                        }

                    case "DependencyTypeId":
                        {
                            throw new NotImplementedException();
                        }

                    case "EmailTypeId":
                        {
                            throw new NotImplementedException();
                        }

                    case "ExpectedStart":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetFilteredExpectedStartTermDateItems(filter);
                            return result;
                        }

                    case "FamilyIncome":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllFamilyIncomeItems(filter.CampusId.ToString());
                            return result;
                        }

                    case "Gender":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllGenderItems(filter.CampusId.ToString());
                            return result;
                        }

                    case "HighSchool":
                        {
                            throw new NotImplementedException();
                        }


                    case "HousingId":
                        {
                            throw new NotImplementedException();
                        }
                    case "JobStatusId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetFilteredJobStatus(filter);
                            return result;
                        }
                    case "JobTitleId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetFilteredJobSTitles(filter);
                            return result;
                        }

                    case "LeadStatus":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllLeadStatusItems();
                            return result;
                        }

                    // Levels are referred to Skills and Extra Curricular Level
                    case "Levels":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetFilteredLevelstems(filter);
                            return result;
                        }

                    case "MaritalStatus":
                        {
                            throw new NotImplementedException();
                        }
                    case "PreferredContactId":
                        {
                            throw new NotImplementedException();
                        }


                    case "Prefix":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllPrefixItems();
                            return result;
                        }

                    case "PreviousEducation":
                        {
                            throw new NotImplementedException();
                        }

                    case "PrgVerId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetFilteredProgramVersionItems(filter);
                            return result;
                        }

                    case "ProgramID":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetFilteredProgramItems(filter);
                            return result;
                        }

                    case "Program": // gets all the program with their calender type to know if it is a clock hour program
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetFilteredProgramwithCalendar(filter);
                            return result;
                        }

                    case "ProgramScheduleId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetFilteredProgramScheduleItems(filter);
                            return result;
                        }
                    
                    case "StateId":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllStatesItems();
                            return result;
                        }

                    case "SystemStatus":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetSystemStatus();
                            return result;
                        }

                    case "StatesWithCode":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllStatesItems(true);
                            return result;
                        }
                    case "PhoneType":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllPhoneTypeItems();
                            return result;
                        }

                    case "SourceAdvertisement":
                        {
                            throw new NotImplementedException();
                        }


                    case "SourceCategoryID":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllCategoriesItems(filter.CampusId.ToString());
                            return result;
                        }

                    case "SourceTypeID":
                        {
                            throw new NotImplementedException();
                        }

                    case "Sponsor":
                        {
                            throw new NotImplementedException();
                        }

                    case "Suffix":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetAllSuffixItems();
                            return result;
                        }

                    case "TransportationId":
                        {
                            throw new NotImplementedException();
                        }

                    case "DocumentStatus":
                        {
                            var bo = new DocumentStatusBo(repositoryWithInt, repository);
                            return bo.GetAll();
                        }

                    case "UniversalSearchModules":
                        {
                            var bo = new CatalogsBo(repository, repositoryWithInt);
                            var result = bo.GetUniversalSearchModules(filter);
                            return result;
                        }
                    default:
                        {
                            return new List<DropDownOutputModel>();
                        }
                }
            }

            // If other values
            throw new ApplicationException("You need to supply a parameter.");
        }
    }
}
