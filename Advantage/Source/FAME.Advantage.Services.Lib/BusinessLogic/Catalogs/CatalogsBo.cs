﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CatalogsBo.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Get info from catalogs.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.Catalogs
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Security.Cryptography.X509Certificates;

    using AutoMapper;

    using Domain.Campuses;
    using Domain.Catalogs;
    using Domain.Infrastructure.Entities;
    using Domain.Lead;
    using Domain.Requirements;
    using Domain.Student;
    using Domain.Student.Enrollments;
    using Domain.SystemStuff;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.SystemStuff.Intitution;
    using FAME.Advantage.Domain.Users;
    using FAME.Advantage.Domain.Users.UserRoles;
    using FAME.AdvantageV1.Common;

    using FluentNHibernate.Utils;

    using Messages.Common;
    using Messages.SystemStuff.SystemStates;

    /// <summary>
    /// Get info from catalogs.
    /// </summary>
    public class CatalogsBo
    {
        /// <summary>
        ///  The repository.
        /// </summary>
        private readonly IRepository repository;

        // ReSharper disable NotAccessedField.Local

        /// <summary>
        /// The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        // ReSharper restore NotAccessedField.Local

        /// <summary>
        /// Initializes a new instance of the <see cref="CatalogsBo"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="repository">
        /// The repository GUID
        /// </param>
        /// <param name="repositoryWithInt">
        /// The repository with integer
        /// </param>
        public CatalogsBo(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            this.repositoryWithInt = repositoryWithInt;
        }

        /// <summary>
        /// Enumeration for preferred contact for Lead
        /// </summary>
        public enum PreferredContactEnum
        {
            /// <summary>
            /// preferred contact is by phone
            /// </summary>
            Phone = 1,

            /// <summary>
            /// preferred contact is by email
            /// </summary>
            Email = 2,

            /// <summary>
            /// preferred contact is by text
            /// </summary>
            Text = 3
        }

        /// <summary>
        /// Get all campus
        /// </summary>
        /// <param name="addDefaultValue">
        /// The Add Default Value.
        /// </param>
        /// <returns>
        /// The drop-down item with campus ID
        /// </returns>
        [SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1503:CurlyBracketsMustNotBeOmitted", Justification = "Reviewed. Suppression is OK here.")]
        public IEnumerable<DropDownWithCampusOutputModel> GetAllProgramGroupsItems(bool addDefaultValue = false)
        {
            var queriable = this.repository.Query<ArProgramGroup>().Where(x => x.SyStatusesObj.StatusCode == "A");

            // var campuses = repository.Query<Campus>().Where(x => x.Status.StatusCode == "A").ToList();
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (ArProgramGroup c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.PrgGrpDescrip));
            }
            if (addDefaultValue)
            {
                var firstEntry = queriable.FirstOrDefault();
                if (firstEntry != null)
                    list.Add(
                        this.CreateDropDownWithCampusItem(
                            firstEntry.CampusGroupObj.Campuses,
                            Guid.Empty.ToString(),
                            "No Interest"));
            }

            return list;
        }

        /// <summary>
        /// The get all program version type items.
        /// </summary>
        /// <returns>
        /// The list of program version type/>.
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetAllProgramVersionTypeItems()
        {
            var queriable = this.repositoryWithInt.Query<ProgramVersionType>();
            var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Descrip));
            var res = result.ToList();
            var item = res.Find(x => x.Description.Equals("Select"));
            res.Remove(item);
            return res;
        }
        ///// <summary>
        ///// Return the filtered Program Group (Interest Area)
        ///// </summary>
        ///// <param name="filter"></param>
        ///// <returns></returns>
        // public IEnumerable<DropDownOutputModel> GetFilteredProgramGroupsItems(CatalogsInputModel filter)
        // {
        // IQueryable<ProgramSchedule> queriable = null;

        // if (filter.AdditionalFilter == null)
        // {
        // queriable = repository.Query<ProgramSchedule>().Where(x => x.Active && x.);
        // }

        // if (filter.AdditionalFilter != null)
        // {
        // queriable = repository.Query<ProgramSchedule>().Where(x => x.Active
        // && x.ProgramVersionObj.ID.ToString() == filter.AdditionalFilter);
        // }

        // var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Descrip));
        // return result;
        // }

        /// <summary>
        /// Retrieve the list of System states from USA
        /// </summary>
        /// <param name="shouldLoadCode">
        /// The should Load Code.
        /// </param>
        /// <returns>
        /// A drop down list of System states from USA
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetAllStatesItems(bool shouldLoadCode = false)
        {

            var queriable = this.repository.Query<SystemStates>().Where(x => x.StatusObj.StatusCode == "A");
            if (!shouldLoadCode)
            {
                var result =
                    Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Description));

                return result;
            }
            else
            {
                var result = queriable.OrderBy(n => n.Description).Select(n => new DropDownOutputModel
                {
                    ID = n.ID.ToString(),
                    Description = n.Code + " - " + n.Description
                });
                return result;
            }
        }

        /// <summary>
        /// Retrieve the list of System states from USA. States will have the description - 
        /// </summary>
        /// <returns>
        /// A drop down list of System states from USA
        /// </returns>
        public IEnumerable<SystemStatesOutputModel> GetAllStatesItemsWithCode()
        {
            var queriable = this.repository.Query<SystemStates>();
            var result =
                Mapper.Map<IEnumerable<SystemStatesOutputModel>>(queriable.ToArray().OrderBy(n => n.Description));
            return result;
        }

        /// <summary>
        /// Retrieve the list of Lead active possible suffix
        /// </summary>
        /// <returns>
        /// A drop down list of possible active Lead suffix
        /// </returns>
        /// <remarks>
        /// This does not filter by campusId
        /// </remarks>
        public IEnumerable<DropDownOutputModel> GetAllSuffixItems()
        {
            var queriable = this.repository.Query<Suffixes>();
            var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.SuffixDescrip));
            return result;
        }

        /// <summary>
        /// Retrieve the items of type of Name prefix 
        /// </summary>
        /// <returns>list of items as DropDownOutputModel</returns>
        public IEnumerable<DropDownOutputModel> GetAllPrefixItems()
        {
            var queriable = this.repository.Query<Prefixes>();
            var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.PrefixDescrip));
            return result;
        }

        /// <summary>
        ///  The get prefix by campus id.
        /// </summary>
        /// <param name="campusId">
        ///  The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;DropDownOutputModel&gt;"/>.
        /// </returns>
        public IList<DropDownOutputModel> GetPrefixByCampusId(string campusId)
        {
            var queriable = this.repository.Query<Prefixes>()
                .Where(x => x.CampusGroupObject.Campuses.Any(y => y.ID == new Guid(campusId))
                && x.SyStatusesObj.StatusCode == "A");

            var result = Mapper.Map<IList<DropDownOutputModel>>(queriable.ToList().OrderBy(n => n.PrefixDescrip));
            return result;
        }

        /// <summary>
        /// Retrieve the items of type of dependency (example : independent, depended)
        /// </summary>
        /// <returns>The drop down output model</returns>
        public IEnumerable<DropDownOutputModel> GetAllDependencyTypeIdItems()
        {
            var queriable = this.repository.Query<DependencyType>();
            var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Descrip));
            return result;
        }

        /// <summary>
        /// The get all tuition category id items.
        /// </summary>
        /// <returns>
        /// The list of TuitionCategory/>.
        /// </returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllTuitionCategoryIdItems()
        {
            var queriable = this.repository.Query<TuitionCategories>().Where(x => x.SyStatusesObj.StatusCode == "A");

            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (TuitionCategories c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.Descrip));
            }

            return list.OrderBy(x => x.Description).ToList();
        }

        /// <summary>
        /// The get all shift id items.
        /// </summary>
        /// <returns>
        /// The list of Shift/>.
        /// </returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllShiftIdItems()
        {
            var queriable = this.repository.Query<Shifts>().Where(x => x.SyStatusesObj.StatusCode == "A");

            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (Shifts c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.Descrip));
            }

            return list.OrderBy(x => x.Description).ToList();
        }

        /// <summary>
        /// The get all geographic type items.
        /// </summary>
        /// <returns>
        /// The list of geographic />.
        /// </returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllGeographicTypeIdItems()
        {
            var queriable = this.repository.Query<GeographicTypes>().Where(x => x.SyStatusesObj.StatusCode == "A");

            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (GeographicTypes c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.Descrip));
            }

            return list.OrderBy(x => x.Description).ToList();
        }

        /// <summary>
        /// The get all nationality items.
        /// </summary>
        /// <returns>
        /// The List of nationality/>.
        /// </returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllNationalityItems()
        {
            var queriable = this.repository.Query<Nationalities>().Where(x => x.SyStatusesObj.StatusCode == "A");

            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (Nationalities c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.Descrip));
            }

            return list.OrderBy(x => x.Description).ToList();
        }

        /// <summary>
        /// The get all charging method items.
        /// </summary>
        /// <returns>
        /// The list of billing/charging method />.
        /// </returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllChargingMethodItems()
        {
            var queriable = this.repository.Query<ChargingMethod>().Where(x => x.SyStatusesObj.StatusCode == "A");

            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (ChargingMethod c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.Descrip, c.BillingMethod.ToString()));
            }

            return list.OrderBy(x => x.Description).ToList();
        }

        /// <summary>
        /// Retrieve the items of type of Driver license emitted by state description 
        /// </summary>
        /// <returns>list of items as DropDownOutputModel</returns>
        public IEnumerable<DropDownOutputModel> GetAllDriveLicStateItems()
        {
            var queriable = this.repository.Query<SystemStates>();
            var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Description));
            return result;
        }

        /// <summary>
        /// Retrieve the items of type of Family incoming
        /// </summary>
        /// <returns>list of items as DropDownOutputModel</returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllFamilyIncomeItems(string campusId = null)
        {
            IList<DropDownWithCampusViewOrder> listViewOrder = new List<DropDownWithCampusViewOrder>();
            if (!string.IsNullOrEmpty(campusId))
            {
                var queriable = this.repository.Query<FamilyIncoming>().Where(x => x.SyStatusesObj.StatusCode == "A" && (x.CampusGrp.Campuses.Any(y => y.ID == Guid.Parse(campusId)) || x.CampusGrp.IsAllCampusGrp == true));
                foreach (FamilyIncoming c in queriable)
                {
                    listViewOrder.Add(this.CreateDropDownWithCampusItem(c.CampusGrp.Campuses, c.ID.ToString(), c.FamilyIncomeDescrip,c.ViewOrder));
                }
                return listViewOrder.OrderBy(n => n.ViewOrder);
            }
            else
            {
                var queriable = this.repository.Query<FamilyIncoming>().Where(x => x.SyStatusesObj.StatusCode == "A" && (x.CampusGrp.IsAllCampusGrp == true));
                foreach (FamilyIncoming c in queriable)
                {
                    listViewOrder.Add(this.CreateDropDownWithCampusItem(c.CampusGrp.Campuses, c.ID.ToString(), c.FamilyIncomeDescrip,c.ViewOrder));
                }
                return listViewOrder.OrderBy(n => n.ViewOrder);
            }
        }

        /// <summary>
        /// Retrieve the items of type of Gender
        /// </summary>
        /// <returns>list of items as DropDownOutputModel</returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllGenderItems(string campusId = null)
        {
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            if (!string.IsNullOrEmpty(campusId))
            {
                var queriable = this.repository.Query<Gender>().Where(x => x.SyStatusesObj.StatusCode == "A" && (x.CampusGrp.Campuses.Any(y => y.ID == Guid.Parse(campusId)) || x.CampusGrp.IsAllCampusGrp == true));
                foreach (Gender c in queriable)
                {
                    list.Add(this.CreateDropDownWithCampusItem(c.CampusGrp.Campuses, c.ID.ToString(), c.Description));
                }
                return list.OrderBy(n => n.Description);
            }
            else
            {
                var queriable = this.repository.Query<Gender>().Where(x => x.SyStatusesObj.StatusCode == "A" && (x.CampusGrp.IsAllCampusGrp == true));
                foreach (Gender c in queriable)
                {
                    list.Add(this.CreateDropDownWithCampusItem(c.CampusGrp.Campuses, c.ID.ToString(), c.Description));
                }
                return list.OrderBy(n => n.Description);
            }
        }

        /// <summary>
        /// Retrieve the items of type of Housing Type example (off campus, on campus, with parent etc)
        /// </summary>
        /// <returns>list of items as DropDownOutputModel</returns>
        public IEnumerable<DropDownOutputModel> GetAllHousingItems()
        {
            var queriable = this.repository.Query<HousingType>();
            var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Descrip));
            return result;
        }

        /// <summary>
        /// Retrieve the items of type of Marital Status
        /// </summary>
        /// <returns>list of items as DropDownOutputModel</returns>
        public IEnumerable<DropDownOutputModel> GetAllMaritalStatusItems()
        {
            var queriable = this.repository.Query<MaritalStatus>().Where(x => x.SyStatusesObj != null && x.SyStatusesObj.StatusCode == "A");
            var result =
                Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.MaritalStatDescrip));
            return result;
        }

        /// <summary>
        /// Retrieve the items of type of Transportation (public, owned auto
        /// </summary>
        /// <returns>list of items as DropDownOutputModel</returns>
        public IEnumerable<DropDownOutputModel> GetAllTransportationItems()
        {
            var queriable = this.repository.Query<Transportation>();
            var result =
                Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.TransportationDescrip));
            return result;
        }

        /// <summary>
        /// The get all education level items.
        /// </summary>
        /// <returns>
        /// The list of education level for enroll page"/>.
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetAllEducationLevelItems()
        {
            var queriable = this.repository.Query<EducationLevel>();
            var result =
                Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Description));
            return result;
        }

        /// <summary>
        /// The get all degree seeking items.
        /// </summary>
        /// <returns>
        /// The List of Degree Certificate Seeking"/>.
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetAllDegSeekingItems(string campusId = null)
        {
            if (!string.IsNullOrEmpty(campusId))
            {
                var queriable = this.repository.Query<DegCertSeeking>().Where(x => x.SyStatusesObj.StatusCode == "A" && (x.CampusGroupObj.Campuses.Any(y => y.ID == Guid.Parse(campusId)) || x.CampusGroupObj.IsAllCampusGrp == true));
                var result =
                    Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Descrip));
                return result;
            }
            else
            {
                var queriable = this.repository.Query<DegCertSeeking>().Where(x => x.SyStatusesObj.StatusCode == "A" && (x.CampusGroupObj.IsAllCampusGrp == true));
                var result =
                    Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Descrip));
                return result;
            }
        }

        /// <summary>
        /// Retrieve the list of Lead active possible status
        /// </summary>
        /// <returns>
        /// A drop down list of possible active Lead Status
        /// </returns>
        /// <remarks>
        /// This does not filter by campusId
        /// </remarks>
        public IEnumerable<DropDownOutputModel> GetAllLeadStatusItems()
        {
            var queriable =
                this.repository.Query<UserDefStatusCode>()
                    .Where(
                        x => x.SystemStatus.StatusLevelId == 1
                        && x.Status.StatusCode == "A" && x.SystemStatus.ID != 26); // duplicate

            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();

            foreach (UserDefStatusCode statusCode in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(
                        statusCode.CampusGroup.Campuses,
                        statusCode.ID.ToString(),
                       (statusCode.IsDefaultLeadStatus == true) ? ("**" + statusCode.Description) : statusCode.Description));
            }

            // see if the default checkbox is checked for some state
            if (!queriable.Any(x => x.IsDefaultLeadStatus == true))
            {
                // get the first that is new lead system status....
                var status = queriable.FirstOrDefault(x => x.SystemStatus.ID == 1);
                if (status != null)
                {
                    foreach (DropDownWithCampusOutputModel model in list)
                    {
                        if (model.ID == status.ID.ToString())
                        {
                            model.Description = "**" + model.Description;
                        }
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Program version comes from 
        /// </summary>
        /// <returns>
        /// List of items as DropDownOutputModel
        /// </returns>
        public IEnumerable<DropDownWithCampusCalendarTypeOutputModel> GetAllActiveProgramVersionItems()
        {
            //had not null check for IDTI3.8
            var queriable = this.repository.Query<ProgramVersion>().Where(x => x.Status.StatusCode == "A" && x.ProgramObj != null);
            IList<DropDownWithCampusCalendarTypeOutputModel> list = new List<DropDownWithCampusCalendarTypeOutputModel>();
            foreach (ProgramVersion c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusCalendarTypeItem(c.CampusGroup.Campuses, c.ID.ToString(), c.Description, c.ProgramObj.Calendartype.Descrip, c.ProgramObj.ID.ToString(), c.ProgramGroupObj.ID.ToString()));
            }

            return list.OrderBy(x => x.Description).ToList();
        }

        /// <summary>
        /// The get all active financial aid advisor.
        /// </summary>
        /// <returns>
        /// The list of financial aid advisors for enroll page/>.
        /// </returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllActiveFinAidAdvisor()
        {
            var queriable = this.repository.Query<Roles>().FirstOrDefault(x => x.SystemRole.ID == 7);
            var listout = new List<DropDownWithCampusOutputModel>();
            if (queriable != null)
            {
                var result = queriable.Users.ToArray().OrderBy(n => n.FullName).Distinct();

                foreach (var adv in result)
                {
                    listout.Add(this.CreateDropDownWithCampusItem(adv.CampusGroup.SelectMany(x => x.Campuses).Distinct(), adv.ID.ToString(), adv.FullName));
                }
            }

            return listout;
        }

        public IEnumerable<DropDownWithCampusOutputModel> GetAllActiveAcademicAdvisor()
        {
            var queriable = this.repository.Query<Roles>().FirstOrDefault(x => x.SystemRole.ID == 4);
            var listout = new List<DropDownWithCampusOutputModel>();
            if (queriable != null)
            {
                var result = (queriable.Users.ToArray().OrderBy(n => n.FullName));

                foreach (var adv in result)
                {
                    listout.Add(this.CreateDropDownWithCampusItem(adv.CampusGroup.SelectMany(x => x.Campuses).Distinct(), adv.ID.ToString(), adv.FullName));
                }
            }

            return listout;
        }

        /// <summary>
        ///  The get filtered program version items.
        /// </summary>
        /// <param name="filter">
        /// TOO The filter.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable&lt;DropDownOutputModel&gt; "/>.
        /// the drop down list
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetFilteredProgramVersionItems(CatalogsInputModel filter)
        {
            IQueryable<ProgramVersion> queriable = null;

            if (filter.CampusId == null && filter.AdditionalFilter == null)
            {
                queriable = this.repository.Query<ProgramVersion>().Where(x => x.Status.StatusCode == "A");
            }

            if (filter.CampusId == null && filter.AdditionalFilter != null)
            {
                queriable =
                    this.repository.Query<ProgramVersion>()
                        .Where(x => x.Status.StatusCode == "A" && x.ProgramObj.ID.ToString() == filter.AdditionalFilter);
            }

            if (filter.CampusId != null && filter.AdditionalFilter == null)
            {
                queriable =
                    this.repository.Query<ProgramVersion>()
                        .Where(
                            x =>
                            x.Status.StatusCode == "A"
                            && (x.CampusGroup.IsAllCampusGrp == true
                                || x.CampusGroup.Campuses.Any(z => z.ID.ToString() == filter.CampusId)));
            }

            if (filter.CampusId != null && filter.AdditionalFilter != null)
            {
                queriable =
                    this.repository.Query<ProgramVersion>()
                        .Where(
                            x =>
                            x.Status.StatusCode == "A" && x.ProgramObj.ID.ToString() == filter.AdditionalFilter
                            && (x.CampusGroup.IsAllCampusGrp == true
                                || x.CampusGroup.Campuses.Any(z => z.ID.ToString() == filter.CampusId)));
            }

            if (queriable != null)
            {
                var result =
                    Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Description));
                return result;
            }

            return new List<DropDownOutputModel>();
        }

        /// <summary>
        /// Get all active program items
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// A list of active programs item for combo box.
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetFilteredProgramItems(CatalogsInputModel filter)
        {
            IQueryable<ArPrograms> queriable = null;

            if (filter.CampusId == null && filter.AdditionalFilter == null)
            {
                queriable = this.repository.Query<ArPrograms>().Where(x => x.StatusesObj.StatusCode == "A");
            }

            if (filter.CampusId == null && filter.AdditionalFilter != null)
            {
                queriable =
                    this.repository.Query<ArPrograms>()
                        .Where(
                            x =>
                            x.StatusesObj.StatusCode == "A"
                            && x.ProgramVersionList.Any(y => y.ProgramGroupObj.ID.ToString() == filter.AdditionalFilter));
            }

            if (filter.CampusId != null && filter.AdditionalFilter == null)
            {
                queriable =
                    this.repository.Query<ArPrograms>()
                        .Where(
                            x =>
                            x.StatusesObj.StatusCode == "A"
                            && (x.CampusGroupObj.IsAllCampusGrp == true
                                || x.CampusGroupObj.Campuses.Any(z => z.ID.ToString() == filter.CampusId)));
            }

            if (filter.CampusId != null && filter.AdditionalFilter != null)
            {
                queriable =
                    this.repository.Query<ArPrograms>()
                        .Where(
                            x =>
                            x.StatusesObj.StatusCode == "A"
                            && x.ProgramVersionList.Any(y => y.ProgramGroupObj.ID.ToString() == filter.AdditionalFilter)
                            && (x.CampusGroupObj.IsAllCampusGrp == true
                                || x.CampusGroupObj.Campuses.Any(z => z.ID.ToString() == filter.CampusId)));
            }

            if (queriable != null)
            {
                var result =
                    Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.ProgDescrip));
                return result;
            }

            return new List<DropDownOutputModel>();
        }

        /// <summary>
        /// The get filtered program with calendar type to indicate if it is clock hour or not.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<DropDownWithCalendarTypeOutputModel> GetFilteredProgramwithCalendar(CatalogsInputModel filter)
        {
            IQueryable<ArPrograms> queriable = null;

            if (filter.CampusId == null && filter.AdditionalFilter == null)
            {
                queriable = this.repository.Query<ArPrograms>().Where(x => x.StatusesObj.StatusCode == "A");
            }

            if (filter.CampusId == null && filter.AdditionalFilter != null)
            {
                queriable =
                    this.repository.Query<ArPrograms>()
                        .Where(
                            x =>
                            x.StatusesObj.StatusCode == "A"
                            && x.ProgramVersionList.Any(y => y.ProgramGroupObj.ID.ToString() == filter.AdditionalFilter));
            }

            if (filter.CampusId != null && filter.AdditionalFilter == null)
            {
                queriable =
                    this.repository.Query<ArPrograms>()
                        .Where(
                            x =>
                            x.StatusesObj.StatusCode == "A"
                            && (x.CampusGroupObj.IsAllCampusGrp == true
                                || x.CampusGroupObj.Campuses.Any(z => z.ID.ToString() == filter.CampusId)));
            }

            if (filter.CampusId != null && filter.AdditionalFilter != null)
            {
                queriable =
                    this.repository.Query<ArPrograms>()
                        .Where(
                            x =>
                            x.StatusesObj.StatusCode == "A"
                            && x.ProgramVersionList.Any(y => y.ProgramGroupObj.ID.ToString() == filter.AdditionalFilter)
                            && (x.CampusGroupObj.IsAllCampusGrp == true
                                || x.CampusGroupObj.Campuses.Any(z => z.ID.ToString() == filter.CampusId)));
            }

            if (queriable != null)
            {
                return queriable.OrderBy(d => d.ProgDescrip).Select(c => new DropDownWithCalendarTypeOutputModel() { Description = c.ProgDescrip, ID = c.ID.ToString().ToLowerInvariant(), CalendarType = c.Calendartype.Descrip }).ToList();
            }

            return new List<DropDownWithCalendarTypeOutputModel>();
        }

        /// <summary>
        /// Get all expected start date and term description items from <code>arTerm</code>
        /// </summary>
        /// <returns>
        /// The drop down with campus input model.
        /// </returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllExpectedStartTermDateItems()
        {
            var queriable = this.repository.Query<Term>().Where(x => x.Status.StatusCode == "A");

            // var campuses = repository.Query<Campus>().Where(x => x.Status.StatusCode == "A").ToList();
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (Term c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroup.Campuses, c.ID.ToString(), c.TermDescription));
            }

            return list;
        }

        /// <summary>
        ///  get filtered expected start term date items.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable&lt;DropDownOutputModel&gt;"/>.
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetFilteredExpectedStartTermDateItems(CatalogsInputModel filter)
        {
            IQueryable<Term> queriable = null;

            if (filter.CampusId == null && filter.AdditionalFilter == null)
            {
                queriable =
                    this.repository.Query<Term>().Where(x => x.Status.StatusCode == "A" && x.EndDate > DateTime.Now);
            }

            if (filter.CampusId == null && filter.AdditionalFilter != null)
            {
                queriable =
                    this.repository.Query<Term>()
                        .Where(
                            x =>
                            x.Status.StatusCode == "A" && x.EndDate > DateTime.Now
                            && (x.ProgramObj == null || x.ProgramObj.ID.ToString() == filter.AdditionalFilter));
            }

            if (filter.CampusId != null && filter.AdditionalFilter == null)
            {
                queriable =
                    this.repository.Query<Term>()
                        .Where(
                            x =>
                            x.Status.StatusCode == "A" && x.EndDate > DateTime.Now
                            && (x.CampusGroup.IsAllCampusGrp == true
                                || x.CampusGroup.Campuses.Any(z => z.ID.ToString() == filter.CampusId)));
            }

            if (filter.CampusId != null && filter.AdditionalFilter != null)
            {
                queriable =
                    this.repository.Query<Term>()
                        .Where(
                            x =>
                            x.Status.StatusCode == "A" && x.EndDate > DateTime.Now
                            && (x.ProgramObj == null || x.ProgramObj.ID.ToString() == filter.AdditionalFilter)
                            && (x.CampusGroup.IsAllCampusGrp == true
                                || x.CampusGroup.Campuses.Any(z => z.ID.ToString() == filter.CampusId)));
            }

            if (queriable != null)
            {
                var result =
                    Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.TermDescription));
                return result;
            }

            return new List<DropDownOutputModel>();
        }

        /// <summary>
        /// Get all items active of type of student in school attendance
        /// </summary>
        /// <returns>
        /// Get all attended types
        /// </returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllAttendTypesItems()
        {
            var queriable = this.repository.Query<ArAttendTypes>().Where(x => x.StatusObj.StatusCode == "A");

            // var campuses = repository.Query<Campus>().Where(x => x.Status.StatusCode == "A").ToList();
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (ArAttendTypes c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGrpObj.Campuses, c.ID.ToString(), c.Descrip));
            }

            return list;
        }

        /// <summary>
        /// Get from database all categories items active.
        /// </summary>
        /// <returns>
        /// Get All Categories items
        /// </returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllCategoriesItems(string campusId = null)
        {
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            if (!string.IsNullOrEmpty(campusId))
            {
                var queriable = this.repository.Query<SourceCategory>().Where(x => x.SyStatusesObj.StatusCode == "A" && (x.CampusGroupObj.Campuses.Any(y => y.ID == Guid.Parse(campusId)) || x.CampusGroupObj.IsAllCampusGrp == true));
                foreach (SourceCategory c in queriable)
                {
                    list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.SourceCatagoryDescrip));
                }
                return list.OrderBy(n => n.Description);
            }
            else
            {
                var queriable = this.repository.Query<SourceCategory>().Where(x => x.SyStatusesObj.StatusCode == "A" && (x.CampusGroupObj.IsAllCampusGrp == true));
                foreach (SourceCategory c in queriable)
                {
                    list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.SourceCatagoryDescrip));
                }
                return list.OrderBy(n => n.Description);
            }
        }

        /// <summary>
        /// Get all types of categories. Categories is related to Category Sources.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllCategoriesTypesItems(string campusId = null)
        {
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            if (!string.IsNullOrEmpty(campusId))
            {
                var queriable = this.repository.Query<SourceType>().Where(x => x.SyStatusesObj.StatusCode == "A" && (x.CampusGroupObj.Campuses.Any(y => y.ID == Guid.Parse(campusId)) || x.CampusGroupObj.IsAllCampusGrp == true));
                foreach (SourceType c in queriable)
                {
                    list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.SourceTypeDescrip));
                }
                return list.OrderBy(n => n.Description);
            }
            else
            {
                var queriable = this.repository.Query<SourceType>().Where(x => x.SyStatusesObj.StatusCode == "A" && (x.CampusGroupObj.IsAllCampusGrp == true));
                foreach (SourceType c in queriable)
                {
                    list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.SourceTypeDescrip));
                }
                return list.OrderBy(n => n.Description);
            }
        }

        /// <summary>
        /// Get all type of source advertisement item.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllSourceAdvertisementItems(string campusId = null)
        {
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            if (!string.IsNullOrEmpty(campusId))
            {
                var queriable = this.repository.Query<SourceAdvertisement>().Where(x => x.SyStatusesObj.StatusCode == "A" && (x.CampusGroupObj.Campuses.Any(y => y.ID == Guid.Parse(campusId)) || x.CampusGroupObj.IsAllCampusGrp == true));
                foreach (SourceAdvertisement c in queriable)
                {
                    list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.SourceAdvDescrip));
                }
                return list.OrderBy(n => n.Description);
            }
            else
            {
                var queriable = this.repository.Query<SourceAdvertisement>().Where(x => x.SyStatusesObj.StatusCode == "A" && (x.CampusGroupObj.IsAllCampusGrp == true));
                foreach (SourceAdvertisement c in queriable)
                {
                    list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.SourceAdvDescrip));
                }
                return list.OrderBy(n => n.Description);
            }
        }

        /// <summary>
        /// Get by CampusId all admission rep items
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DropDownOutputModel> GetAdmissionRepItems(string campusId)
        {
            var queriable =
                this.repositoryWithInt.Query<AdmissionRepByCampusView>()
                    .Where(x => x.CampusObj.ID.ToString() == campusId);
            IList<DropDownWithCampusOutputModel> outlist = new List<DropDownWithCampusOutputModel>();

            foreach (AdmissionRepByCampusView view in queriable)
            {
                var output = new DropDownWithCampusOutputModel();
                output.Description = view.FullName;
                output.ID = view.UserObj.ID.ToString();
                output.CampusesIdList = new List<CampusOutput>
                                            {
                                                new CampusOutput
                                                    {
                                                        CampusId =
                                                            view.CampusObj.ID.ToString()
                                                    }
                                            };
                outlist.Add(output);
            }

            // var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.FullName));
            return outlist;
        }

        /// <summary>
        /// Return also the campus id
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllAdmissionRepItems()
        {
            var listout = new List<DropDownWithCampusOutputModel>();
            var queriable = this.repositoryWithInt.Query<AdmissionRepByCampusView>();
            foreach (AdmissionRepByCampusView view in queriable)
            {
                var list = new List<Campus> { view.CampusObj };
                listout.Add(this.CreateDropDownWithCampusItem(list, view.UserObj.ID.ToString(), view.FullName));
            }

            // var result = Mapper.Map<IEnumerable<DropDownWithCampusOutputModel>>(queriable.ToArray().OrderBy(n => n.FullName));
            return listout;
        }

        /// <summary>
        /// This get from enum all preferred contact items
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DropDownOutputModel> GetAllPreferredContactItems()
        {
            var list = new List<DropDownOutputModel>();
            var len = Enum.GetValues(typeof(PreferredContactEnum)).Length;
            for (int i = 1; i < len + 1; i++)
            {
                var output = new DropDownOutputModel
                {
                    Description = ((PreferredContactEnum)i).ToString(),
                    ID = i.ToString(CultureInfo.InvariantCulture)
                };
                list.Add(output);
            }

            return list.OrderBy(x => x.Description);
        }

        /// <summary>
        /// This get from enum all preferred contact items
        /// </summary>
        /// <returns>
        /// Return drop down items Type for Email
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetAllEmailTypeIdItems()
        {
            var queriable = this.repository.Query<EmailType>();
            var result =
                Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.EmailTypeDescrip));
            return result;
        }

        /// <summary>
        /// Phone Types
        /// </summary>
        /// <returns>
        /// return a list of drop down item for phone type.
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetAllPhoneTypeItems()
        {
            var queriable = this.repository.Query<SyPhoneType>().Where(x => x.SyStatusesObj.StatusCode == "A");
            var result =
                Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.PhoneTypeDescrip));
            return result;
        }

        /// <summary>
        /// Get all countries items actives
        /// </summary>
        /// <returns>
        /// Return the list of countries with the Campus Associated.
        /// </returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllCountriesItems()
        {
            var queriable = this.repository.Query<Country>().Where(x => x.SyStatusesObj.StatusCode == "A");
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (Country c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.CountryDescrip));
            }

            return list;
        }

        /// <summary>
        ///  The get countries by campus ID.
        /// </summary>
        /// <param name="filter">
        ///  The filter. Must be a object with a CampusId field type string
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;DropDownOutputModel&gt;"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Fired when the parameter is not compatible
        /// </exception>
        public IList<DropDownOutputModel> GetCountriesByCampusId(dynamic filter)
        {
            if (filter.CampusId != null)
            {
                string campusId = filter.CampusId;
                var list =
                    this.repository.Query<Country>()
                        .Where(
                            x =>
                                x.SyStatusesObj.StatusCode == "A"
                                && (x.CampusGroupObj.Campuses.Any(y => y.ID.ToString() == campusId)
                                 || x.CampusGroupObj.IsAllCampusGrp == true))
                        .Select(
                            info =>
                                new DropDownOutputModel()
                                {
                                    ID = info.ID.ToString(),
                                    Description = info.CountryDescrip
                                });
                return list.OrderBy(t => t.Description).ToList();
            }

            throw new Exception("filter is not valid. field Campus not present");
        }

        /// <summary>
        /// Get all counties items
        /// </summary>
        /// <returns>The drop down item list of the Counties</returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllCountiesItems()
        {
            var queriable = this.repository.Query<County>().Where(x => x.SyStatusesObj.StatusCode == "A");

            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (County c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.CountyDescrip));
            }

            return list.OrderBy(x => x.Description).ToList();
        }

        /// <summary>
        /// Get the list of Address type and campus id.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllAddressTypeItems()
        {
            var queriable = this.repository.Query<AddressType>().Where(x => x.SyStatusesObj.StatusCode == "A");

            // var campuses = repository.Query<Campus>().Where(x => x.Status.StatusCode == "A").ToList();
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (AddressType c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.AddessDescrip));
            }

            return list;
        }

        /// <summary>
        /// Create a list of object time interval with campus and Id.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllBestTimeIntervalItems()
        {
            var queriable = this.repository.Query<TimeInterval>().Where(x => x.SyStatusesObj.StatusCode == "A");

            // var campuses = repository.Query<Campus>().Where(x => x.Status.StatusCode == "A").ToList();
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (TimeInterval c in queriable)
            {
                var timeinter = c.TimeIntervalDescrip.GetValueOrDefault();
                list.Add(this.CreateDropDownWithCampusItem(
                        c.CampusGroupObj.Campuses,
                        c.ID.ToString(),
                        timeinter.ToShortTimeString()));
            }

            return list;
        }

        /// <summary>
        /// Get all sponsors item from table
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllSponsorsItems()
        {
            var queriable = this.repository.Query<AgencySponsors>().Where(x => x.SyStatusesObj.StatusCode == "A");

            // var campuses = repository.Query<Campus>().Where(x => x.Status.StatusCode == "A").ToList();
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (AgencySponsors c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.AgencySpDescrip));

                // if (c.CampusGroupObj.IsAllCampusGrp == true)
                // {

                // var item = c.AgencySpDescrip;
                // CreateDropDownWithCampusItem(campuses, c.ID.ToString(), item, list);
                // }
                // else
                // {
                // var campuslist = c.CampusGroupObj.Campuses;
                // var item = c.AgencySpDescrip;
                // CreateDropDownWithCampusItem(campuslist, c.ID.ToString(), item, list);
                // }
            }

            return list.OrderBy(x => x.Description);
        }

        /// <summary>
        /// Get all previous education item for lead (Education level)
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetPreviousEducationItems()
        {
            var queriable = this.repository.Query<EducationLevel>().Where(x => x.Status.StatusCode == "A");

            // var campuses = repository.Query<Campus>().Where(x => x.Status.StatusCode == "A").ToList();
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (EducationLevel c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroup.Campuses, c.ID.ToString(), c.Description));
            }

            return list;
        }

        /// <summary>
        /// Get high school items
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllHighSchoolItems()
        {
            var queriable = this.repository.Query<HighSchools>().Where(x => x.SyStatusesObj.StatusCode == "A");

            // var campuses = repository.Query<Campus>().Where(x => x.Status.StatusCode == "A").ToList();
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (HighSchools c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.HsDescrip));

                // if (c.CampusGroupObj.IsAllCampusGrp == true)
                // {

                // var item = c.HsDescrip;
                // CreateDropDownWithCampusItem(campuses, c.ID.ToString(), item, list);
                // }
                // else
                // {
                // var campuslist = c.CampusGroupObj.Campuses;
                // var item = c.HsDescrip;
                // CreateDropDownWithCampusItem(campuslist, c.ID.ToString(), item, list);
                // }
            }

            return list.OrderBy(x => x.Description);
        }

        /// <summary>
        /// Get all administrative criteria items
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DropDownWithCampusOutputModel> GetAllAdminCriteriaItems()
        {
            var queriable = this.repository.Query<AdminCriteria>().Where(x => x.SyStatusesObj.StatusCode == "A");

            // var campuses = repository.Query<Campus>().Where(x => x.Status.StatusCode == "A").ToList();
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (AdminCriteria c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.Description));
            }

            return list.OrderBy(x => x.Description);
        }

        /// <summary>
        /// Get all active items or get filtered by Program Version.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<DropDownOutputModel> GetFilteredProgramScheduleItems(CatalogsInputModel filter)
        {
            IQueryable<ProgramSchedule> queriable = null;

            if (filter.AdditionalFilter == null)
            {
                queriable = this.repository.Query<ProgramSchedule>().Where(x => x.Active);
            }

            if (filter.AdditionalFilter != null)
            {
                queriable =
                    this.repository.Query<ProgramSchedule>()
                        .Where(x => x.Active && x.ProgramVersionObj.ID.ToString() == filter.AdditionalFilter);
            }

            if (queriable != null)
            {
                var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Descrip));
                return result;
            }

            return new List<DropDownOutputModel>();
        }

        /// <summary>
        /// Get all active items or get filtered by Program Version.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DropDownOutputModel> GetAllProgramScheduleItems()
        {
            var queriable = this.repository.Query<ProgramSchedule>().Where(x => x.Active);
            var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Descrip));
            return result;
        }

        /// <summary>
        ///  Get all races items with list of campuses
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DropDownOutputModel> GetAllRaceItems()
        {
            var queriable = this.repository.Query<Ethnicity>().Where(x => x.SyStatusesObj.StatusCode == "A");
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (Ethnicity c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), c.Description));
            }

            return list.OrderBy(x => x.Description);
        }

        /// <summary>
        /// Get all levels from catalog for Skill, Extra Curricular
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<DropDownOutputModel> GetFilteredLevelstems(CatalogsInputModel filter)
        {
            var q =
                this.repositoryWithInt.Query<AdLevels>()
                    .Where(x => x.StatusObj.StatusCode == "A")
                    .Select(
                        info => new DropDownOutputModel() { Description = info.Description, ID = info.ID.ToString() });
            var output = q.OrderBy(x => x.Description).ToList();
            return output;
        }

        /// <summary>
        ///  The get all reason lead not enrolled.
        /// </summary>
        /// <returns>
        /// The <see cref="DropDownOutputModel"/>.
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetAllReasonLeadNotEnrolled()
        {
            var queriable = this.repository.Query<SyReasonNotEnrolled>().Where(x => x.StatusObj.StatusCode == "A");
            IList<DropDownWithCampusOutputModel> list = new List<DropDownWithCampusOutputModel>();
            foreach (SyReasonNotEnrolled c in queriable)
            {
                list.Add(this.CreateDropDownWithCampusItem(c.CampGrpObj.Campuses, c.ID.ToString(), c.Description));
            }

            return list.OrderBy(x => x.Description);
        }

        /// <summary>
        /// The get filtered countries.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// if you give a 
        /// <code>
        /// CampusId
        /// </code>
        /// different to empty GUID or null
        /// a country list is returned with all campus associated to it
        /// If you enter a specific 
        /// <code>
        /// CampusId
        /// </code>
        /// only is returned the Country for the specific campusID
        /// </param>
        /// <param name="shouldLoadCode">
        /// The should Load Code.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable&lt;DropDownOutputModel&gt;"/>.
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetFilteredCountries(CatalogsInputModel filter, bool shouldLoadCode = false)
        {
            if (filter.CampusId != Guid.Empty.ToString())
            {
                var q =
                    this.repository.Query<Country>()
                        .Where(
                            x =>
                                x.SyStatusesObj.StatusCode == "A"
                                && x.CampusGroupObj.Campuses.Any(y => y.ID.ToString() == filter.CampusId))
                        .Select(
                            info =>
                                new DropDownOutputModel()
                                {
                                    Description = shouldLoadCode ? info.CountryCode + " - " + info.CountryDescrip : info.CountryDescrip,
                                    ID = info.ID.ToString()
                                });
                var output = q.OrderBy(x => x.Description).ToList();
                return output;
            }
            else
            {
                // Get all Countries with the campus associated
                var q = this.repository.Query<Country>().Where(x => x.SyStatusesObj.StatusCode == "A");
                var list = new List<DropDownWithCampusOutputModel>();
                foreach (Country c in q)
                {
                    list.Add(this.CreateDropDownWithCampusItem(c.CampusGroupObj.Campuses, c.ID.ToString(), shouldLoadCode ? c.CountryCode + " - " + c.CountryDescrip : c.CountryDescrip));
                    list = list.OrderBy(x => x.Description).ToList();
                }

                return list;
            }
        }

        /// <summary>
        ///  The get filtered job status.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// if you give a <code>CampusId</code> different to empty GUID or null
        /// a country list is returned with all campus associated to it
        /// If you enter a specific <code>CampusId</code> only is returned the Country for the specific campusID
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable&lt;DropDownOutputModel&gt;"/>.
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetFilteredJobStatus(CatalogsInputModel filter)
        {
            if (filter.CampusId != Guid.Empty.ToString())
            {
                var q =
                    this.repository.Query<PlJobStatus>()
                        .Where(
                            x =>
                                x.StatusObj.StatusCode == "A"
                                && x.CampGrpObj.Campuses.Any(y => y.ID.ToString() == filter.CampusId))
                        .Select(
                            info =>
                                new DropDownOutputModel()
                                {
                                    Description = info.JobStatusDescrip,
                                    ID = info.ID.ToString()
                                });
                var output = q.OrderBy(x => x.Description).ToList();
                return output;
            }
            else
            {
                // Get all Countries with the campus associated
                var q = this.repository.Query<PlJobStatus>().Where(x => x.StatusObj.StatusCode == "A");
                var list = new List<DropDownWithCampusOutputModel>();
                foreach (PlJobStatus c in q)
                {
                    list.Add(this.CreateDropDownWithCampusItem(c.CampGrpObj.Campuses, c.ID.ToString(), c.JobStatusDescrip));
                    list = list.OrderBy(x => x.Description).ToList();
                }

                return list;
            }
        }

        /// <summary>
        /// The get filtered job s titles.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// if you give a <code>CampusId</code> different to empty GUID or null
        /// a country list is returned with all campus associated to it
        /// If you enter a specific <code>CampusId</code> only is returned the Country for the specific campusID
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable&lt;DropDownOutputModel&gt;"/>.
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetFilteredJobSTitles(CatalogsInputModel filter)
        {
            if (filter.CampusId != Guid.Empty.ToString())
            {
                var q =
                    this.repository.Query<AdTitles>()
                        .Where(
                            x =>
                                x.StatusObj.StatusCode == "A"
                                && x.CampGrpObj.Campuses.Any(y => y.ID.ToString() == filter.CampusId))
                        .Select(
                            info =>
                                new DropDownOutputModel()
                                {
                                    Description = info.TitleDescrip,
                                    ID = info.ID.ToString()
                                });
                var output = q.OrderBy(x => x.Description).ToList();
                return output;
            }
            else
            {
                // Get all Countries with the campus associated
                var q = this.repository.Query<PlJobStatus>().Where(x => x.StatusObj.StatusCode == "A");
                var list = new List<DropDownWithCampusOutputModel>();
                foreach (PlJobStatus c in q)
                {
                    list.Add(this.CreateDropDownWithCampusItem(c.CampGrpObj.Campuses, c.ID.ToString(), c.JobStatusDescrip));
                    list = list.OrderBy(x => x.Description).ToList();
                }

                return list;
            }
        }

        /// <summary>
        /// The get system status.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetSystemStatus()
        {
            var query = this.repository.Query<SyStatuses>();
            var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(query.ToArray().OrderBy(n => n.Status));
            return result;
        }

        /// <summary>
        /// The get universal search modules.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// An un-handle exception
        /// </exception>
        public IEnumerable<DropDownOutputModel> GetUniversalSearchModules(CatalogsInputModel filter)
        {
            if (filter == null)
            {
                throw new Exception("Filter can't be null");
            }

            if (filter.CampusId == string.Empty)
            {
                throw new Exception("Campus can't be empty");
            }

            if (filter.UserId == null || filter.UserId == Guid.Empty)
            {
                throw new Exception("User can't be empty");
            }


            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == filter.UserId.Value && x.AccountActive == true);

            if (user == null)
            {
                throw new Exception("User Acount can't be found");
            }

            List<UniversalSearchModule> universalSearchModules = null;

            if (!user.IsAdvantageSuperUser)
            {
                var campusGroup =
                    this.repository.Query<CampusGroup>()
                        .Where(x => x.Campuses.Any(c => c.ID == Guid.Parse(filter.CampusId))).Select(x => x.ID).ToList();

                if (!campusGroup.Any())
                {
                    throw new Exception("Campus can't be found");
                }

                var customRoles =
                    user.RolesByCampusList.Where(x => campusGroup.Contains(x.CampusGroup.ID)).Distinct().Select(x => x.Role).ToList();

                var roleResources =
                    customRoles.SelectMany(x => x.Resources)
                        .Where(x => x != null && x.ResourceTypeObj.ID == 1)
                        .Distinct()
                        .Select(x => x.ID)
                        .ToList();

                universalSearchModules =
                    this.repositoryWithInt.Query<UniversalSearchModule>()
                        .Where(x => roleResources.Contains(x.Resource.ID))
                        .Distinct()
                        .ToList();
            }
            else
            {
                universalSearchModules =
                    this.repositoryWithInt.Query<UniversalSearchModule>()
                        .Distinct()
                        .ToList();
            }

            var output = universalSearchModules.Select(x => new DropDownOutputModel
            {
                ID = x.ID.ToString(),
                Description = x.Description
            });

            return output.OrderBy(x => x.ID);
        }

        /// <summary>
        ///  The create drop down with campus item.
        /// </summary>
        /// <param name="campuslist">
        ///  The campus list.
        /// </param>
        /// <param name="id">
        ///  The ID.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <returns>
        /// The <see cref="DropDownWithCampusOutputModel"/>.
        /// Return a Drop down item with campusId
        /// </returns>
        private DropDownWithCampusOutputModel CreateDropDownWithCampusItem(
       IEnumerable<Campus> campuslist,
       string id,
       string description)
        {
            var item = new DropDownWithCampusOutputModel()
            {
                Description = description,
                ID = id,
                CampusesIdList = new List<CampusOutput>()
            };
            foreach (Campus campus in campuslist)
            {
                var co = new CampusOutput { CampusId = campus.ID.ToString() };
                item.CampusesIdList.Add(co);
            }

            return item;
        }

        /// <summary>
        /// The create drop down with campus item.
        /// </summary>
        /// <param name="campuslist">
        /// The campuslist.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <returns>
        /// The <see cref="DropDownWithCampusOutputModel"/>.
        /// </returns>
        private DropDownWithCampusOutputModel CreateDropDownWithCampusItem(
            IEnumerable<Campus> campuslist,
            string id,
            string description, 
            string code)
        {
            var item = new DropDownWithCodeOutputModel()
            {
                Code = code,
                Description = description,
                ID = id,
                CampusesIdList = new List<CampusOutput>()
            };
            foreach (Campus campus in campuslist)
            {
                var co = new CampusOutput { CampusId = campus.ID.ToString() };
                item.CampusesIdList.Add(co);
            }

            return item;
        }
        private DropDownWithCampusViewOrder CreateDropDownWithCampusItem(
            IEnumerable<Campus> campuslist,
            string id,
            string description,
            int viewOrder)
        {
            var item = new DropDownWithCampusViewOrder()
            {
                ViewOrder = viewOrder,
                Description = description,
                ID = id,
                CampusesIdList = new List<CampusOutput>()
            };
            foreach (Campus campus in campuslist)
            {
                var co = new CampusOutput { CampusId = campus.ID.ToString() };
                item.CampusesIdList.Add(co);
            }

            return item;
        }

        /// <summary>
        /// The create drop down with campus calendar type item.
        /// </summary>
        /// <param name="campuslist">
        /// The campus list.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="calendarType">
        /// The calendar type.
        /// </param>
        /// <param name="programId">
        /// The program Id.
        /// </param>
        /// <param name="areaId">
        /// The area Id.
        /// </param>
        /// <returns>
        /// The <see cref="DropDownWithCampusOutputModel"/>.
        /// </returns>
        private DropDownWithCampusCalendarTypeOutputModel CreateDropDownWithCampusCalendarTypeItem(IEnumerable<Campus> campuslist, string id, string description, string calendarType, string programId, string areaId)
        {
            var item = new DropDownWithCampusCalendarTypeOutputModel()
            {
                Description = description,
                ID = id,
                CalendarType = calendarType,
                ProgramId = programId,
                AreaId = areaId,
                CampusesIdList = new List<CampusOutput>()
            };
            foreach (Campus campu in campuslist)
            {
                var co = new CampusOutput { CampusId = campu.ID.ToString() };
                item.CampusesIdList.Add(co);
            }

            return item;
        }

    }
}
