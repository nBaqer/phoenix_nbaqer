﻿using System.Collections.Generic;

namespace FAME.Advantage.Services.Lib.BusinessLogic.Wapi
{
    internal static class LeadSandbox
    {
        internal static readonly IList<LeadExtended> Sandbox;

        static LeadSandbox()
        {
            Sandbox = new List<LeadExtended>();
        }
    }
}
