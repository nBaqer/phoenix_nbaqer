﻿using System;
using FAME.Advantage.Messages.Wapi.WapiLeads;

namespace FAME.Advantage.Services.Lib.BusinessLogic.Wapi
{
    public class LeadExtended : WapiLeadsOutputModel
    {
        /// <summary>
        /// Date time that the leads was register
        /// </summary>
        public DateTime RegisterTime { get; set; }

    }
}
