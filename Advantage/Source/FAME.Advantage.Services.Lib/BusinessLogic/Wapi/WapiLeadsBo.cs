﻿using System;
using System.Collections.Generic;
using System.Linq;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.Wapi.WapiLeads;

namespace FAME.Advantage.Services.Lib.BusinessLogic.Wapi
{
    /// <summary>
    /// Business Object for WAPI Leads
    /// </summary>
    public class WapiLeadsBo
    {
        enum EnumStatus
        {
            // ReSharper disable UnusedMember.Local
            Incomplete = 1, Duplicate = 2, Notvalid = 3
            // ReSharper restore UnusedMember.Local
        }
        //private read only IList<LeadExtended> sandbox;

        //repository object that represents data store
        // ReSharper disable NotAccessedField.Local
        private readonly IRepositoryWithTypedID<Guid> repository;
        // ReSharper restore NotAccessedField.Local

        public WapiLeadsBo(IRepositoryWithTypedID<Guid> repository)
        {
            this.repository = repository;
           // sandbox = new List<LeadExtended>();

        }

        #region Production
        public string GetLastLead()
        {
            throw new NotImplementedException();
        }

        public IList<WapiLeadsOutputModel> ProcessListOfLeads(IList<WapiLeadsOutputModel> leadsList)
        {
            throw new NotImplementedException();
        }

        #endregion


        #region SandBox

        /// <summary>
        /// Return the last prospect Id of a Lead inputed an accepted by the system.
        /// </summary>
        /// <returns></returns>
        internal string GetLastLeadSandBox(string vendorCode)
        {
            if (LeadSandbox.Sandbox.Count == 0)
            {
                return string.Empty;
            }

            var last = LeadSandbox.Sandbox.Max(x => x.RegisterTime);
            LeadExtended firstOrDefault = LeadSandbox.Sandbox.Where(x => x.VendorCode == vendorCode).FirstOrDefault(x => x.RegisterTime == last);
            if (firstOrDefault != null)
            {
                var id = firstOrDefault.ProspectID;
                return id;
            }

            return string.Empty;
        }

        /// <summary>
        /// Validate 
        /// </summary>
        /// <param name="leadsList"></param>
        /// <returns></returns>
        internal IList<WapiLeadsOutputModel> ProcessListOfLeadsSandBox(IList<WapiLeadsOutputModel> leadsList)
        {
            // Validate if all mandatory fields are filled...
            IList<WapiLeadsOutputModel> listRejection = ValidateMandatoryFields(ref leadsList);

            // TODO rejection for county selection

            // TODO marks as possible duplicate

            //TODO rejection for duplicate value

            // Store valid values in SandBox
            foreach (WapiLeadsOutputModel wapiLeadsOutputModel in listRejection)
            {
                var s = (LeadExtended)wapiLeadsOutputModel;
                s.RegisterTime = DateTime.Now;
                LeadSandbox.Sandbox.Add(s);
            }

            return listRejection;
        }

        /// <summary>
        /// Required fields validations
        /// </summary>
        /// <param name="leadsList"></param>
        /// <returns></returns>
        private IList<WapiLeadsOutputModel> ValidateMandatoryFields(ref IList<WapiLeadsOutputModel> leadsList)
        {
            var rejectedList = new List<WapiLeadsOutputModel>();
            foreach (WapiLeadsOutputModel wl in leadsList)
            {
                if (string.IsNullOrWhiteSpace(wl.ProspectID))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.VendorName))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.VendorCode))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.CampaignId))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.AccountId))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.FirstName))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.LastName))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.Phone))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.PhoneType))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.Email))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.EmailType))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.Address1))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.County))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.City))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.State))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.Country))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.ProgramOfInterest))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if ((wl.DateOfContact.AddYears(1) < DateTime.Now))
                {
                    RejectionLead(wl, ref rejectedList);
                    continue;
                }
                if (string.IsNullOrWhiteSpace(wl.Zip))
                {
                    RejectionLead(wl, ref rejectedList);
                }

                // Remove form list o input all rejected

            }

            leadsList = leadsList.Where(x => x.LeadStatus != EnumStatus.Incomplete.ToString()).ToList();
            return rejectedList;
        }


        void RejectionLead(WapiLeadsOutputModel wl, ref List<WapiLeadsOutputModel> rejectedList)
        {
            wl.LeadStatus = EnumStatus.Incomplete.ToString();
            rejectedList.Add(wl);
        }


        #endregion



    }
}
