﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusesBo.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Business Object for Campus
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.Campuses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Domain.Campuses;
    using Domain.Campuses.CampusGroups;
    using Domain.Infrastructure.Entities;
    using Domain.Student;
    using Domain.Users;

    using Infrastructure.Exceptions;
    using Infrastructure.Extensions;

    using Messages.Campuses;
    using Messages.Common;

    /// <summary>
    /// Business Object for Campus
    /// </summary>
    public class CampusesBo
    {
        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepository repository;

        // ReSharper disable NotAccessedField.Local
        
            /// <summary>
        /// The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        //// ReSharper restore NotAccessedField.Local

        /// <summary>
        /// Initializes a new instance of the <see cref="CampusesBo"/> class. 
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="repositoryWithInt">
        /// The repository with integer.
        /// </param>
        public CampusesBo(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            this.repositoryWithInt = repositoryWithInt;
        }

        /// <summary>
        /// Get the list of campus by Campus Group
        /// </summary>
        /// <param name="filter">campus group filter</param>
        /// <returns>a list of ID Description</returns>
        public IEnumerable<DropDownOutputModel> GetCampusItemsByCampusGrpId(CampusesInputModel filter)
        {
            // if campusgrpId passed in, get the campuses by campus group id
            if (filter.CampusGrpId.IsEmpty())
            {
                throw new HttpBadUsageOfControllerFunctionException("Controller processing has not a valid parameter");
            }

            var campusList =
                this.repository.Query<CampusGroup>().Where(n => n.ID == filter.CampusGrpId).SelectMany(n => n.Campuses);
            var campusArray = Mapper.Map<IEnumerable<DropDownOutputModel>>(campusList.ToArray());
            return campusArray;
        }

        /// <summary>
        /// Get the list of campus by User Id
        /// </summary>
        /// <param name="filter">user id filter</param>
        /// <returns>a list of ID Description</returns>
        public IEnumerable<DropDownOutputModel> GetCampusItemsByUserId(CampusesInputModel filter)
        {
            if (filter.UserId == Guid.Empty)
            {
                throw new HttpBadRequestResponseException("User Id is required");
            }

            var listItems =
                this.repository.Query<User>()
                    .Where(n => n.ID == filter.UserId)
                    .SelectMany(n => n.CampusGroup)
                    .SelectMany(y => y.Campuses)
                    .Where(y => y.Status.StatusCode == "A")
                    .Distinct(); // .SelectMany(y => y.Description).Distinct();

            // if (listItems == null)
            // {
            //    return new List<DropDownOutputModel>();
            //// }

            var listItemsEnd = Mapper.Map<IEnumerable<DropDownOutputModel>>(listItems.ToArray());
            return listItemsEnd;
        }

        /// <summary>
        /// Get all campus
        /// </summary>
        /// <returns>
        /// Drop down output model for campus
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetAllCampusItems()
        {
            var queriable = this.repository.Query<Campus>();
            var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Description));
            return result;
        }

        /// <summary>
        /// Get all campus
        /// </summary>
        /// <returns>
        /// Drop down output model for campus
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetAllActiveCampusItems(string userId = null)
        {
            if (!string.IsNullOrEmpty(userId))
            {
                // if support user then get all active campus else get only active the campuses that belongs to the campusgrp to which the user belongs.
                var bou = new BusinessLogic.Users.UserRolesBo(this.repository);
                if (bou.IsSupport(userId))
                {
                    var queriable = this.repository.Query<Campus>().Where(x => x.Status.StatusCode == "A");
                    var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Description));
                    return result;
                }
                else
                {
                    // get the list of campus grp to which the user belongs
                    var result = this.GetCampusItemsByUserId(new CampusesInputModel {UserId = Guid.Parse(userId)});
                    return result;
                }
            }
            else
            {
                var queriable = this.repository.Query<Campus>().Where(x => x.Status.StatusCode == "A");
                var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.Description));
                return result;
            }
        }

        /// <summary>
        /// Get All Citizen items
        /// </summary>
        /// <returns>
        /// Get all citizen items
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetAllCitizenItems()
        {
            var queriable = this.repository.Query<Citizenships>();
            var result =
                Mapper.Map<IEnumerable<DropDownOutputModel>>(queriable.ToArray().OrderBy(n => n.CitizenshipsDescrip));
            return result;
        }
    }
}
