﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using AutoMapper;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings;
using FAME.Advantage.Domain.SystemStuff.Menu;
using FAME.Advantage.Messages.Enumerations;
using FAME.Advantage.Messages.Student.Enrollments;
using FAME.Advantage.Messages.Students.Enrollments;
using FAME.Advantage.Services.Lib.BusinessLogic.Students.Dto;
using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;
using DataException = System.Data.DataException;

namespace FAME.Advantage.Services.Lib.BusinessLogic.Students.Academic
{
    public class StudentAttendanceBo
    {
        // Constants
        private const string X_ACTUAL_ATTENDANCE = "Actual"; private const string X_ADJUSTED_ATTENDACE = "Adjusted";
        private const string X_ABSENT_PRESENT = "ABSENTPRESENT";
        private const string X_MINUTES = "MINUTES";


        //represents data store
        private readonly IRepository repository;
        private readonly IRepositoryWithTypedID<int> repositoryWithTypedId;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="repositorytyped"></param>
        public StudentAttendanceBo(IRepository repository, IRepositoryWithTypedID<int> repositorytyped)
        {
            this.repository = repository;
            repositoryWithTypedId = repositorytyped;
        }

        #region Filters

        public IEnumerable<EnrollmentAttendanceOutputModel> GetAttendanceEnrollmentsByStudentId(GetEnrollmentsInputModel filter)
        {
            if (filter.StudentId.IsEmpty()) { throw new HttpBadUsageOfControllerFunctionException("Controller processing has not a valid parameter"); }

            // Query data
            var enrollmentList = repository.Query<Enrollment>().Where(n => n.StudentId == filter.StudentId).Distinct();
            var enrollmentArray = Mapper.Map<IEnumerable<EnrollmentAttendanceOutputModel>>(enrollmentList.ToList().OrderByDescending(x => x.StartDate));
            return enrollmentArray;
        }

        public IEnumerable<EnrollmentAttendanceOutputModel> GetAllAttendanceEnrollment()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Present Absent Attendance Type.................

        /// <summary>
        /// This is use to calculate attendance for Absent PResent. (parent and detail grid)
        /// Get all periods for a single Course...........................
        /// </summary>
        /// <param name="filter">ClsSectionGuidID = The selected Class Section</param>
        /// <returns>The list of attendance slots for the course</returns>
        public IEnumerable<StudentAttendanceDto> GetAllPeriodsforCourseAbsentPresent(AttendanceInputModel filter)
        {
            // Validate if is a historical attendance and the switch Imported Attendance is ON.
            //if (IsImportedAttendanceAndDataHistorical(filter))
            //{
            //    throw new HttpBadUsageOfControllerFunctionException("We can not posted attendance in historical data");
            //}

            // Define Output...............................
            IList<StudentAttendanceDto> model = new List<StudentAttendanceDto>();

            // Get all class section for a course....
            var classSection = repository.Query<ArClassSections>().Select(x => x).FirstOrDefault(x => x.ID == filter.ClsSectionGuid);
            if (classSection == null)
            {
                return model;
            }

            // get the registered ttendance
            IList<StudentAttendanceDto> attendanceOutputList = new List<StudentAttendanceDto>();
            GetAttendanceForStudentList(filter, classSection, ref attendanceOutputList, X_ABSENT_PRESENT);

            // Get all days of attendance
            var calendardays = GetCalendarDaysList(filter, classSection, X_ABSENT_PRESENT);

            // Fill the days of attendance with the register attendance and include the make up hours
            model = MergeCalendarDayWithAttendance(filter, calendardays, attendanceOutputList);
            return model;
        }

        public AttendanceSummaryEnrollmentOutput GetStudentAttendanceEnrollmentSummary(AttendanceInputModel filter)
        {
            var enrollment = repository.Query<Enrollment>().Select(x => x).FirstOrDefault(x => x.ID == filter.StuEnrollmentId && x.CampusId == filter.CampusId);
            // Get if program Version attendance is tracking.... (that is attendance different to none)
            if (enrollment == null) { throw new ArgumentException("Enrollment not found"); }

            // Create Output structure.....................................................
            var response = new AttendanceSummaryEnrollmentOutput();
            List<SummaryEnrollmentTable> resAttendanceTableList;

            // Get the attendance Type.....
            var attendancetype = enrollment.ProgramVersion.AttendanceType.Description.ToUpper(CultureInfo.InvariantCulture);
            string attendancetoken;
            switch (attendancetype)
            {
                case "PRESENT ABSENT":
                    {
                        decimal porcentage;
                        resAttendanceTableList = CalculateAttendanceEnrollmentPresentAbsent(enrollment, out porcentage);
                        response.PercentagePresent = porcentage;
                        attendancetoken = X_ABSENT_PRESENT;
                        break;
                    }

                case "MINUTES":
                    {
                        attendancetoken = X_MINUTES;
                        throw new NotImplementedException();
                    }

                case "CLOCK HOURS":
                    {
                        attendancetoken = X_MINUTES;
                        throw new NotImplementedException();
                    }

                case "NONE":
                    {
                        // Program version attendance is not tracked
                        throw new NotSupportedException(enrollment.ProgramVersion.Description + " does not tracks attendance");
                    }
                default:
                    {
                        throw new NotSupportedException(string.Format("Attendance Type: {0} is unknow", attendancetype));
                    }
            }

            response.SummaryEnrollmentTableList = resAttendanceTableList;

            // Get Attendance in minutes
            // Make calculations...
            // Get LOA days
            response.SummaryEnrollmentLoaList = LoaDays(filter);
            // Get Suspended days
            response.SummarySuspensionList = SuspensionDays(filter);

            // Get Enrollment change of status
            response.SummaryEnrollmentStatusList = GetLogChangesStatusEnrollments(filter);

            // Get Schedules days
            var resultList = enrollment.ResultList;
            int schedule = 0;
            foreach (ArResults arResultse in resultList)
            {
                if (arResultse.ClassSections != null)
                {
                    filter.FilterIni = arResultse.ClassSections.StartDate;
                    filter.FilterEnd = arResultse.ClassSections.EndDate;
                    var list = GetCalendarDaysList(filter, arResultse.ClassSections, attendancetoken);
                    schedule += list.Count;
                }
            }

            response.ScheduleHours = schedule;


            return response;
        }

        /// <summary>
        /// Post a attendance in Present Absent.
        /// </summary>
        /// <param name="input"></param>
        public void PostAttendanceAbsentPresent(PostAttendanceInputModel input)
        {
            // If Attendance guid is empty insert operation.....
            if (input.IdGuid == Guid.Empty)
            {
                // Insert Operation.....
                var sect = repository.Load<ArClassSections>(input.ClsSectionIdGuid);
                var meeting = repository.Load<ClassSectionMeeting>(input.ClassSectionMeetingGuid);
                var enroll = repository.Load<Enrollment>(input.EnrollmentGuid);

                // Set the time to begin the operation
                var period = repository.Load<Periods>(input.PeriodIdGuid);
                var time = period.StartTimeIntervalObj.TimeIntervalDescrip.GetValueOrDefault();
                var mt = new DateTime(input.MeetDate.Year, input.MeetDate.Month, input.MeetDate.Day, time.Hour, time.Minute, time.Second);

                var atten = new ClassSectionAttendance(
                    Guid.Empty,
                    meeting,
                    enroll,
                    sect,
                    mt,
                    1, false, false, "MU", 0,"sa");
                atten.UpdateAttendanceStatus(input.StatusEnum);
                repository.Save(atten);

            }
            else
            {
                // If attendance guid is valid Guid update operation.
                if (input.StatusEnum == Enumerations.AttendanceStatusEnum.Unselected)
                {
                    // Delete the record.
                    var attendance = repository.Load<ClassSectionAttendance>(input.IdGuid);
                    repository.Delete(attendance);
                }
                else
                {
                    // Update the record with the attendance.
                    var attendance = repository.Load<ClassSectionAttendance>(input.IdGuid);
                    attendance.UpdateAttendanceStatus(input.StatusEnum);
                }

            }
        }


        private IList<SpecialAttendanceTable> GetLogChangesStatusEnrollments(AttendanceInputModel filter)
        {
            var output = new List<SpecialAttendanceTable>();
            var logList = repository.Query<StudentStatusChanges>().Where(x => x.EnrollmentObj.ID == filter.StuEnrollmentId).ToList();
            if (logList.Count > 0)
            {
                foreach (StudentStatusChanges changes in logList)
                {
                    var satten = new SpecialAttendanceTable
                    {
                        DateIni = changes.DateOfChange,
                        StatusReason = changes.NewEnrollmentStatusObj.Description
                    };

                    if (changes.DropReasonObj != null)
                    {
                        satten.StatusReason += " [ " + changes.DropReasonObj.Description + " ]";
                    }
                    output.Add(satten);
                }
            }
            return output.OrderBy(x => x.DateIni).ToList();
        }

        /// <summary>
        /// Check is setting UseImportedAttendance and the term description contain Historical.
        /// In this case return true.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        private bool IsImportedAttendanceAndDataHistorical(AttendanceInputModel filter)
        {
            var isImportedAttendance = ConfigurationAppSettings.Setting("UseImportedAttendance", repositoryWithTypedId);
            var clas = repository.Load<ArClassSections>(filter.ClsSectionGuid);
            var desc = clas.TermEntity.TermDescription;
            bool isHistorical = (desc.ToUpper(CultureInfo.InvariantCulture).Contains("HISTORICAL"));
            return (isImportedAttendance.ToUpper(CultureInfo.InvariantCulture) == "TRUE" & isHistorical);
        }

        private List<SummaryEnrollmentTable> CalculateAttendanceEnrollmentPresentAbsent(Enrollment enrollment, out decimal porcentage)
        {
            var attendanceList = enrollment.ClassSectionAttendanceList.Where(x => x.Actual != 99 && x.Actual != 999 && x.Actual != 9999);
            var summaryList = new List<SummaryEnrollmentTable>();
            var excused = 0;
            var tardies = 0;
            var absent = 0;
            var present = 0;
            foreach (ClassSectionAttendance attendance in attendanceList)
            {
                if (attendance.Actual == 0)
                {
                    if (attendance.Excused) excused++;
                    else absent++;
                }
                else
                {
                    if (attendance.Tardy) tardies++;
                    present++;
                }
            }

            // Fill Actual Attendance....
            var summary = new SummaryEnrollmentTable
            {
                Absent = absent.ToString(CultureInfo.InvariantCulture),
                Excused = excused.ToString(CultureInfo.InvariantCulture),
                Present = present.ToString(CultureInfo.InvariantCulture),
                Tardy = tardies.ToString(CultureInfo.InvariantCulture),
                LabelField = X_ACTUAL_ATTENDANCE
            };
            summaryList.Add(summary);
            // Analisys of Tardies....
            int tardiesmakeausence = enrollment.ProgramVersion.TardiesMakingAbsence.GetValueOrDefault(0);
            if ((enrollment.ProgramVersion.TrackTardies == 1) & (tardiesmakeausence > 0))
            {
                var tardyAbsence = tardies / tardiesmakeausence;
                absent += tardyAbsence;
                tardies = tardies - (tardyAbsence * tardiesmakeausence);
            }

            // Fill Adjusted Attendance
            summary = new SummaryEnrollmentTable
            {
                Absent = absent.ToString(CultureInfo.InvariantCulture),
                Excused = excused.ToString(CultureInfo.InvariantCulture),
                Present = present.ToString(CultureInfo.InvariantCulture),
                Tardy = tardies.ToString(CultureInfo.InvariantCulture),
                LabelField = X_ADJUSTED_ATTENDACE
            };

            // Calculate the porcentage
            decimal attend = (absent + present + tardies);
            if (attend == 0)
            {
                porcentage = 0;
            }
            else
            {
                porcentage = ((present + tardies) / attend) * 100;
            }

            // Add the list
            summaryList.Add(summary);
            return summaryList;
        }
        /// <summary>
        /// Merge calendar days wit registers attendance and make up hours.
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="calendardays"></param>
        /// <param name="attendanceOutputList"></param>
        /// <returns></returns>
        private IList<StudentAttendanceDto> MergeCalendarDayWithAttendance(AttendanceInputModel filter, IList<StudentAttendanceDto> calendardays, IList<StudentAttendanceDto> attendanceOutputList)
        {
            IList<StudentAttendanceDto> additionaList = new List<StudentAttendanceDto>();

            // Get the attendance and add to calendar days........
            foreach (StudentAttendanceDto calendarday in calendardays)
            {
                foreach (StudentAttendanceDto attendance in attendanceOutputList)
                {

                    if (attendance.AttendanceDate == calendarday.AttendanceDate &
                        attendance.PeriodId == calendarday.PeriodId & attendance.StatusEnum != Enumerations.AttendanceStatusEnum.MakeUp)
                    {
                        if (calendarday.StatusEnum == Enumerations.AttendanceStatusEnum.Unselected)
                        {
                            calendarday.StatusEnum = attendance.StatusEnum;
                            calendarday.AttendanceGuid = attendance.AttendanceGuid;
                        }
                        else
                        {
                            var outputItem = new StudentAttendanceDto
                            {
                                StatusEnum = attendance.StatusEnum,
                                PeriodId = attendance.PeriodId,
                                PeriodDescription = attendance.PeriodDescription,
                                AttendanceDate = attendance.AttendanceDate,
                                AttendanceGuid = attendance.AttendanceGuid,
                            };
                            additionaList.Add(outputItem);
                        }
                    }
                }
            }

            // Get the make up hours only if we are in summary ..........................
            if (filter.IsSummary)
            {
                foreach (StudentAttendanceDto attendance in attendanceOutputList)
                {
                    if (attendance.StatusEnum == Enumerations.AttendanceStatusEnum.MakeUp)
                    {
                        var outputItem = new StudentAttendanceDto
                        {
                            StatusEnum = attendance.StatusEnum,
                            PeriodId = attendance.PeriodId,
                            PeriodDescription = attendance.PeriodDescription,
                            AttendanceDate = attendance.AttendanceDate
                        };
                        additionaList.Add(outputItem);
                    }
                }
            }

            // Insert the new records in the output list
            foreach (var addi in additionaList)
            {
                calendardays.Add(addi);
            }

            return calendardays;
        }

        ///// <summary>
        ///// Get all ttendance in the date range
        ///// </summary>
        ///// <param name="filter">Date range</param>
        ///// <param name="classSection"></param>
        ///// <param name="model"></param>
        //private void GetAttendanceForStudentList(AttendanceInputModel filter, ArClassSections classSection, ref IList<StudentAttendanceDto> model)
        //{
        //    // Get the list of attendance for a particular student....
        //    // Set interval or adquisition of Attendance from 1/1/1990 to today at 23:59:59
        //    DateTime startDate; DateTime endDate;
        //    SetDateBeginAndEndForAttendance(out startDate, out endDate);

        //    // Get the attendance list for the particular range of dates 
        //    IList<ClassSectionAttendance> attendanceList = classSection.ClassSectionAttendanceList.Select(x => x)
        //        .Where(x => x.EnrollmentObj.ID == filter.StuEnrollmentId && x.MeetDate >= startDate && x.MeetDate <= endDate).ToList();

        //    foreach (ClassSectionAttendance attendance in attendanceList)
        //    {
        //        // Put info in the Output

        //        var classMeeting = attendance.ClassSectionMeetingObj;
        //        var output = new StudentAttendanceDto();
        //        if (classMeeting != null && classMeeting.PeriodObj != null)
        //        {
        //            output.PeriodId = classMeeting.PeriodObj.ID;
        //            output.PeriodDescription = classMeeting.PeriodObj.PeriodDescrip;
        //        }
        //        else
        //        {
        //            output.PeriodId = null;
        //            output.PeriodDescription = string.Empty;
        //        }

        //        output.AttendanceDate = attendance.MeetDate.Date;
        //        output.StatusEnum = ProcessDayAttendanceAbsentPresent(attendance);
        //        output.AttendanceGuid = attendance.ID;

        //        // Process the attendance information.
        //        // Add the model to output
        //        model.Add(output);
        //    }
        //}


        /// <summary>
        /// Get all possible scheduled day for the given Course....
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="classSection"></param>
        /// <param name="attendanceType"></param>
        /// <returns></returns>
        private IList<StudentAttendanceDto> GetCalendarDaysList(AttendanceInputModel filter, ArClassSections classSection, string attendanceType)
        {
            //Days to be used to calculate the calendar. (this is because the resume class table should report all scheduled class)
            // Client should hidden the calendar day after today.
            // Initial day must be > = ClassSection StartDate.
            // Final day must be ClassSection end date.
            DateTime initialDay;
            DateTime endDay;
            if (filter.IsSummary)
            {
                initialDay = (filter.FilterIni < classSection.StartDate) ? classSection.StartDate : filter.FilterIni;
                // NOTE: We should send calendar day from begin course to end course
                //var today = DateTime.Now;
                //today = new DateTime(today.Year, today.Month, today.Day, 23, 59, 59);
                //endDay = (classSection.EndDate.Date > DateTime.Now.Date) ? today : classSection.EndDate;
                //endDay = (filter.FilterEnd > endDay) ? endDay : filter.FilterEnd;
                endDay = new DateTime(filter.FilterEnd.Year, filter.FilterEnd.Month, filter.FilterEnd.Day, 23, 59, 59);
            }
            else
            {
                initialDay = new DateTime(filter.FilterIni.Year, filter.FilterIni.Month, filter.FilterIni.Day, 0, 0, 0);
                endDay = new DateTime(filter.FilterEnd.Year, filter.FilterEnd.Month, filter.FilterEnd.Day, 23, 59, 59);
            }


            // Get All Meetings for this class attendance...................
            var meetingList = classSection.ClassSectionMeetingList;
            IList<StudentAttendanceDto> model = new List<StudentAttendanceDto>();
            foreach (ClassSectionMeeting meeting in meetingList)
            {
                // Get the syPeriods Works Days
                if (meeting.PeriodObj != null)
                {
                    var workdayslist = meeting.PeriodObj.WorkDaysList;
                    for (DateTime i = initialDay; i <= endDay; i = i.AddDays(1))
                    {
                        foreach (WorkDays days in workdayslist)
                        {
                            if (days.ViewOrder == (int)i.DayOfWeek)
                            {
                                // Create the output record in accordance with the works days and initial end day
                                // Put info in the Output
                                var output = new StudentAttendanceDto
                                {
                                    PeriodId = (meeting.PeriodObj != null) ? meeting.PeriodObj.ID : Guid.Empty,
                                    AttendanceDate = i,
                                    ClassSectionMeetingGuid = meeting.ID,
                                    PeriodDescription = (meeting.PeriodObj != null) ? meeting.PeriodObj.PeriodDescrip : string.Empty,
                                    StatusEnum = Enumerations.AttendanceStatusEnum.Unselected,
                                    AttendanceGuid = Guid.Empty,
                                    StarTime = (meeting.PeriodObj != null) ? meeting.PeriodObj.StartTimeIntervalObj.TimeIntervalDescrip : null,
                                    EndTime = (meeting.PeriodObj != null) ? meeting.PeriodObj.EndTimeIntervalObj.TimeIntervalDescrip : null,
                                    IsMarkToRemove = false
                                };

                                model.Add(output);
                            }

                        }
                    }
                }
            }

            // Get the enrollment information...
            Enrollment enrollment = repository.Query<Enrollment>().SingleOrDefault(x => x.ID == filter.StuEnrollmentId);

            // Filter the DropInfo...........................
            model = FilterTheDropInfo(enrollment, model);

            // Filter the holiday days if the setting is YES....
            model = FilterHolidaysDays(model, filter);

            // Student LOA Filter LOA Days........ They should not appear in calendar.
            model = FilterLoaDays(model, filter);

            // Student Suspension Days.... They should not appear in calendar.
            model = FilterSuspensionDays(model, filter);

            // In the case of attendance differents to AbsentPresent, calculate the period minutes...
            if (attendanceType == X_MINUTES)
            {
                model = AddSheduledMinutes(model);
            }

            return model;
        }

        /// <summary>
        /// Minutes and timeclock attendance function to calculate the Shedule time of a meeting class.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private IList<StudentAttendanceDto> AddSheduledMinutes(IList<StudentAttendanceDto> model)
        {
            foreach (StudentAttendanceDto dto in model)
            {
                if (dto.PeriodId == null)
                {
                    throw new DataException("DataBase Error, Class Section Meeting has not defined Period of Time");
                }

                var period = repository.Load<Periods>(dto.PeriodId.GetValueOrDefault());
                var inittime = period.StartTimeIntervalObj.TimeIntervalDescrip.GetValueOrDefault();
                var endtime = period.EndTimeIntervalObj.TimeIntervalDescrip.GetValueOrDefault();
                dto.Schedule = Convert.ToInt32((endtime.TimeOfDay - inittime.TimeOfDay).TotalMinutes);
            }

            return model;
        }

        private IList<StudentAttendanceDto> FilterTheDropInfo(Enrollment enrollment, IList<StudentAttendanceDto> model)
        {
            DateTime limitDay;
            //Annalize if it is Dropped
            if (enrollment.EnrollmentStatus.StatusCode == "Dropped")
            {
                // it is dropped, elimine attendance post dropped.
                if (enrollment.DateDetermined == null)
                {
                    throw new ArgumentNullException("The enrollment has not fill the day of enrollment dropping! Please fill this field before fill attendance", new Exception("Parameter was DateDetermined in enrollment"));
                }

                limitDay = enrollment.DateDetermined.GetValueOrDefault();
                var res = DropDays(limitDay, model);
                return res;
            }

            // Analize is it Transfer Out
            if (enrollment.EnrollmentStatus.StatusCode == "Transfer Out")
            {
                limitDay = CalculateLda(enrollment);
                var res = DropDays(limitDay, model);
                return res;
            }

            return model;
        }

        private DateTime CalculateLda(Enrollment enrollment)
        {
            IList<DateTime> candidates = new List<DateTime>();

            DateTime val = DateTime.MinValue;
            var listExt = enrollment.ExternalShipAttendanceList;
            if (listExt != null && listExt.Count > 0)
            {
                val = listExt.Max(x => x.AttendedDate);
                candidates.Add(val);
            }

            var listAtted = enrollment.ClassSectionAttendanceList.Where(x => x.Actual >= 1 & x.Actual != 99 & x.Actual != 999 & x.Actual != 9999).ToList();
            if (listAtted.Count > 0)
            {
                val = listAtted.Max(x => x.MeetDate);
                candidates.Add(val);
            }

            var listAt = enrollment.AttendanceList.Where(x => x.Actual > 1).ToList();
            if (listAt.Count > 0)
            {
                val = listAt.Max(x => x.AttendanceDate.GetValueOrDefault());
                candidates.Add(val);
            }

            var listClock =
                enrollment.StudentClockAttendanceList.Where(x => x.ActualHours >= 1 & x.ActualHours != 99 & x.ActualHours != 999 & x.ActualHours != 9999).ToList();

            if (listClock.Count > 0)
            {
                val = listClock.Max(x => x.RecordDate);
                candidates.Add(val);
            }

            // Get the maximun value
            if (candidates.Count > 0)
            {
                var lda = candidates.Max();
                return lda;
            }

            return val;
        }

        private IList<StudentAttendanceDto> DropDays(DateTime limitDay, IList<StudentAttendanceDto> modelList)
        {
            foreach (StudentAttendanceDto model in modelList)
            {

                if (model.AttendanceDate.Date >= limitDay.Date && model.AttendanceDate.Date <= limitDay.Date)
                {
                    model.IsMarkToRemove = true;
                }
            }


            var res = modelList.Where(dto => dto.IsMarkToRemove == false).ToList();
            return res;
        }



        /// <summary>
        /// Get the suspension days and filtered it from the response.
        /// </summary>
        /// <param name="modelList"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        private IList<StudentAttendanceDto> FilterSuspensionDays(IList<StudentAttendanceDto> modelList, AttendanceInputModel filter)
        {
            // get suspensions days
            //var suspensions = repository.Query<StudentSuspensions>().Where(x => x.ID == filter.StuEnrollmentId).ToList();
            var suspensions = SuspensionDays(filter);

            if (suspensions.Count == 0)
            {
                return modelList;
            }

            // Filter the suspensions....
            foreach (SpecialAttendanceTable sus in suspensions)
            {
                foreach (StudentAttendanceDto model in modelList)
                {
                    var endDay = sus.DateEnd.GetValueOrDefault();

                    if (model.AttendanceDate.Date >= sus.DateIni.Date && model.AttendanceDate.Date <= endDay.Date)
                    {
                        model.IsMarkToRemove = true;
                    }
                }
            }

            var res = modelList.Where(dto => dto.IsMarkToRemove == false).ToList();
            return res;
        }

        /// <summary>
        /// Return the list of suspension days....
        /// </summary>
        /// <param name="filter">Enrollment Id</param>
        /// <returns>list of interval of suspension and reason</returns>
        private List<SpecialAttendanceTable> SuspensionDays(AttendanceInputModel filter)
        {
            var suspensionsList = repository.Query<StudentSuspensions>().Where(x => x.StuEnrollment.ID == filter.StuEnrollmentId).ToList();

            var resultList = suspensionsList.Select(sus => new SpecialAttendanceTable
            {
                DateEnd = sus.EndDate,
                DateIni = sus.StartDate,
                DateRequested = null,
                StatusReason = sus.Reason
            }).ToList();

            return resultList;
        }

        private IList<StudentAttendanceDto> FilterLoaDays(IList<StudentAttendanceDto> modelList, AttendanceInputModel filter)
        {
            // get loas days
            var loas = LoaDays(filter);
            if (loas.Count == 0)
            {
                return modelList;
            }

            // Filter the loas....
            foreach (SpecialAttendanceTable loa in loas)
            {
                foreach (StudentAttendanceDto model in modelList)
                {
                    var endDay = loa.DateEnd.GetValueOrDefault();

                    if (model.AttendanceDate.Date >= loa.DateIni.Date && model.AttendanceDate.Date <= endDay.Date)
                    {
                        model.IsMarkToRemove = true;
                    }
                }
            }

            var res = modelList.Where(dto => dto.IsMarkToRemove == false).ToList();
            return res;
        }

        /// <summary>
        /// Return the list of suspension days....
        /// </summary>
        /// <param name="filter">Enrollment Id</param>
        /// <returns>list of interval of suspension and reason</returns>
        private List<SpecialAttendanceTable> LoaDays(AttendanceInputModel filter)
        {
            var loaList = repository.Query<StudentLoa>().Where(x => x.StuEnrollment.ID == filter.StuEnrollmentId).ToList();

            var resultList = loaList.Select(sus => new SpecialAttendanceTable
            {
                DateEnd = sus.EndDate,
                DateIni = sus.StartDate,
                DateRequested = null,
                StatusReason = sus.LoaReasonsObj.Description
            }).ToList();

            return resultList;
        }

        private IList<StudentAttendanceDto> FilterHolidaysDays(IList<StudentAttendanceDto> modelList, AttendanceInputModel filter)
        {
            // This folter determine if you need to filter the holidays or not. Some school work in holidays days.
            var isAttendanceInHolidays = ConfigurationAppSettings.Setting("postattendanceonholiday", repositoryWithTypedId);
            if (isAttendanceInHolidays.ToUpper(CultureInfo.InvariantCulture) == "NO")
            {
                return modelList;
            }

            // Get holiday days
            var holidaysList = GetHolidaysDays(filter);

            // Filter the holidays.....
            if (holidaysList == null || holidaysList.Count == 0)
            {
                return modelList;
            }

            foreach (Holidays holiday in holidaysList)
            {
                foreach (StudentAttendanceDto model in modelList)
                {
                    var endDay = holiday.EndDate ?? DateTime.Now;

                    if (model.AttendanceDate.Date >= holiday.StartDate.Date && model.AttendanceDate.Date <= endDay.Date)
                    {
                        if (holiday.AllDay)
                        {
                            model.IsMarkToRemove = true;
                            continue;
                        }

                        // The Holidays is not for all the day....
                        if (holiday.StartTimeIntervalObj != null && holiday.EndTimeIntervalObj != null && model.StarTime != null)
                        {
                            var permodel = model.StarTime.GetValueOrDefault().TimeOfDay;
                            var holini = holiday.StartTimeIntervalObj.TimeIntervalDescrip.GetValueOrDefault().TimeOfDay;
                            var holend = holiday.EndTimeIntervalObj.TimeIntervalDescrip.GetValueOrDefault().TimeOfDay;
                            if (permodel >= holini && permodel <= holend)
                            {
                                model.IsMarkToRemove = true;
                            }
                        }
                    }
                }
            }

            // Remove the mark to remove days...
            var res = modelList.Where(dto => dto.IsMarkToRemove == false).ToList();
            return res;
        }

        /// <summary>
        /// Analisys of Attendance for Present Absent School.
        /// THe logic to determinate the student attendance for a recorded attendance.
        /// </summary>
        /// <param name="dayAttendance">Class Section Attendance</param>
        /// <returns>Enumeration with the class attendance value</returns>
        /// <remarks>Make up is not associated with period remmember that.</remarks>
        private Enumerations.AttendanceStatusEnum ProcessDayAttendanceAbsentPresent(ClassSectionAttendance dayAttendance)
        {
            if (dayAttendance == null)
            {
                return Enumerations.AttendanceStatusEnum.Unselected;
            }

            if (dayAttendance.Scheduled <= 0)
            {
                var result = (dayAttendance.Actual == 1 & dayAttendance.Tardy == false)
                    ? Enumerations.AttendanceStatusEnum.MakeUp
                    : Enumerations.AttendanceStatusEnum.Unselected;
                return result;
            }

            switch (Convert.ToInt32(dayAttendance.Actual))
            {
                case 0:
                    {
                        var result = (dayAttendance.Excused)
                            ? Enumerations.AttendanceStatusEnum.Excused
                            : Enumerations.AttendanceStatusEnum.Absent;
                        return result;
                    }
                case 1:
                    {
                        var result = (dayAttendance.Tardy)
                            ? Enumerations.AttendanceStatusEnum.Tardy
                            : Enumerations.AttendanceStatusEnum.Present;
                        return result;
                    }
                default:
                    return Enumerations.AttendanceStatusEnum.Unselected;
            }
        }

        private IList<Holidays> GetHolidaysDays(AttendanceInputModel filter)
        {
            var campus = repository.Query<Enrollment>().Where(x => x.ID == filter.StuEnrollmentId).Select(x => x.CampusObj).SingleOrDefault();
            IList<Holidays> holidaysList = new List<Holidays>();
            if (campus != null)
            {
                foreach (CampusGroup campusGroups in campus.CampusGroup)
                {
                    if (campusGroups.HolidaysList != null)
                    {
                        foreach (Holidays holidays in campusGroups.HolidaysList)
                        {
                            holidaysList.Add(holidays);
                        }
                    }
                }
            }

            holidaysList = holidaysList.Distinct().ToList();
            return holidaysList;
        }


        #endregion

        #region Attendance Minutes Clock Hours

        /// <summary>
        /// Get all period for course minutes. This work for CLock and minutes class.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IList<StudentAttendanceDto> GetAllPeriodsforCourseMinutes(AttendanceInputModel filter)
        {
            // Validate if is a historical attendance and the switch Imported Attendance is ON.
            //if (IsImportedAttendanceAndDataHistorical(filter))
            //{
            //    throw new ArgumentException("We can not display details attendance in historical data. Refer to Summary Enrollment section");
            //}

            // Define Output...............................
            IList<StudentAttendanceDto> model = new List<StudentAttendanceDto>();

            // Get all class section for a course....
            var classSection = repository.Query<ArClassSections>().Select(x => x).FirstOrDefault(x => x.ID == filter.ClsSectionGuid);
            if (classSection == null)
            {
                return model;
            }

            // get the registered ttendance
            IList<StudentAttendanceDto> attendanceOutputList = new List<StudentAttendanceDto>();
            GetAttendanceForStudentList(filter, classSection, ref attendanceOutputList, X_MINUTES);

            // Get all days of attendance
            var calendardays = GetCalendarDaysList(filter, classSection, X_MINUTES);

            // Fill the days of attendance with the register attendance and include the make up hours
            model = MergeCalendarDayWithAttendanceClock(calendardays, attendanceOutputList);
            return model;
        }

        public void PostAttendanceMinutes(PostAttendanceInputModel input)
        {
            //Compose the store value for Actual = MakeUp + Actual
            var actual = input.MakeUp + input.Actual;

            // Check if the Guid comes empty, is empty you need to enter a new attendance reecord.
            if (input.IdGuid == Guid.Empty)
            {
                // Insert Operation.....
                var sect = repository.Load<ArClassSections>(input.ClsSectionIdGuid);
                var meeting = repository.Load<ClassSectionMeeting>(input.ClassSectionMeetingGuid);
                var enroll = repository.Load<Enrollment>(input.EnrollmentGuid);

                // Set the time to begin the operation
                var period = repository.Load<Periods>(input.PeriodIdGuid);
                var time = period.StartTimeIntervalObj.TimeIntervalDescrip.GetValueOrDefault();
                var mt = new DateTime(input.MeetDate.Year, input.MeetDate.Month, input.MeetDate.Day, time.Hour, time.Minute, time.Second);

                var atten = new ClassSectionAttendance(
                    Guid.Empty,
                    meeting,
                    enroll,
                    sect,
                    mt,
                    actual,
                    input.IsTardy,
                    input.IsExcused,
                    "CA",
                    input.Schedule, "sa");
                repository.Save(atten);
                return;
            }

            // UPDATE: Get the record and update it!
            var attendance = repository.Load<ClassSectionAttendance>(input.IdGuid);
            if (attendance == null)
            {
                throw new ApplicationException("Something go wrong the record should exists but not: Please Contact FAME");
            }

            attendance.UpdateClockMinutes(actual, input.IsTardy, input.IsExcused);
        }


        private IList<StudentAttendanceDto> MergeCalendarDayWithAttendanceClock(IList<StudentAttendanceDto> calendardays, IList<StudentAttendanceDto> attendanceOutputList)
        {
            // IList<StudentAttendanceDto> additionaList = new List<StudentAttendanceDto>();

            // Get the attendance and add to calendar days........
            foreach (StudentAttendanceDto attendance in attendanceOutputList)
            {
                attendance.IsMarkToRemove = false; // In this case mark to remove meaning true: was found in calendar day // false: was not found
                foreach (StudentAttendanceDto calendarday in calendardays)
                {

                    if (attendance.AttendanceDate == calendarday.AttendanceDate & attendance.PeriodId == calendarday.PeriodId)
                    {
                        //Fill with the attendance information..
                        if (attendance.IsMarkToRemove)
                        {
                            throw new DuplicateNameException("Exist to different attendance for the same period and date!");
                        }
                        calendarday.AttendanceGuid = attendance.AttendanceGuid;
                        calendarday.Excused = attendance.Excused;
                        calendarday.Actual = attendance.Actual;
                        calendarday.Absent = attendance.Absent;
                        calendarday.MakeUp = attendance.MakeUp;
                        calendarday.Schedule = attendance.Schedule;
                        calendarday.Tardies = attendance.Tardies;
                        attendance.IsMarkToRemove = true;
                    }
                }
            }

            // Add attendance that was not found (make up hours possible)
            foreach (StudentAttendanceDto dto in attendanceOutputList)
            {
                if (dto.IsMarkToRemove == false)
                {
                    var outputItem = new StudentAttendanceDto
                    {
                        StatusEnum = dto.StatusEnum,
                        PeriodId = dto.PeriodId,
                        PeriodDescription = dto.PeriodDescription,
                        AttendanceDate = dto.AttendanceDate,
                        AttendanceGuid = dto.AttendanceGuid,
                        Actual = dto.Actual,
                        Excused = dto.Excused,
                        Absent = dto.Absent,
                        MakeUp = dto.MakeUp,
                        Schedule = dto.Schedule,
                        Tardies = dto.Tardies,
                        ClassSectionMeetingGuid = Guid.Empty,
                    };

                    calendardays.Add(outputItem);
                }
            }

            //// Get the make up hours only if we are in summary ..........................
            //if (filter.IsSummary)
            //{
            //    foreach (StudentAttendanceDto attendance in attendanceOutputList)
            //    {
            //        if (attendance.StatusEnum == Enumerations.AttendanceStatusEnum.MakeUp)
            //        {
            //            var outputItem = new StudentAttendanceDto
            //            {
            //                StatusEnum = attendance.StatusEnum,
            //                PeriodId = attendance.PeriodId,
            //                PeriodDescription = attendance.PeriodDescription,
            //                AttendanceDate = attendance.AttendanceDate,
            //                Excused = attendance.Excused,
            //                Actual = attendance.Actual,
            //                Absent = attendance.Absent,
            //                MakeUp = attendance.MakeUp,
            //                Schedule = attendance.Schedule
            //            };
            //            additionaList.Add(outputItem);
            //        }
            //    }
            //}

            //// Insert the new records in the output list
            //foreach (var addi in additionaList)
            //{
            //    calendardays.Add(addi);
            //}

            return calendardays;
        }

        /// <summary>
        /// Get the attendance for Clock school of a individual student
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="classSection"></param>
        /// <param name="model"></param>
        /// <param name="attendanceType">
        /// Valid values: MINUTES / ABSENTPRESENT
        /// Note: Use minutes for clock attendance and minutes attendance.
        /// </param>
        /// <remarks>Probally we need to change the date of begin... and the day of end</remarks>
        private void GetAttendanceForStudentList(
            AttendanceInputModel filter,
            ArClassSections classSection,
            ref IList<StudentAttendanceDto> model,
            string attendanceType)
        {
            // Get the list of attendance for a particular student....


            DateTime startDate; DateTime endDate;
            if (filter.IsSummary)
            {
                // Set interval or adquisition of Attendance from 1/1/1990 to today at 23:59:59
                SetDateBeginAndEndForAttendance(out startDate, out endDate);
            }
            else
            {
                // You want a specific range of days.. put the filter ini at 00:00:00 hours and filter ini to 23:59:59 hours
                startDate = new DateTime(filter.FilterIni.Year, filter.FilterIni.Month, filter.FilterIni.Day, 0, 0, 0);
                endDate = new DateTime(filter.FilterEnd.Year, filter.FilterEnd.Month, filter.FilterEnd.Day, 23, 59, 59);
            }

            // Get the attendance list for the particular range of dates 
            IList<ClassSectionAttendance> attendanceList = classSection.ClassSectionAttendanceList.Select(x => x)
                .Where(x => x.EnrollmentObj.ID == filter.StuEnrollmentId
                    && x.MeetDate >= startDate
                    && x.MeetDate <= endDate
                    && x.Actual != 9999
                    && x.Actual != 999).ToList();

            foreach (ClassSectionAttendance attendance in attendanceList)
            {
                // Put info in the Output

                var classMeeting = attendance.ClassSectionMeetingObj;
                var output = new StudentAttendanceDto();
                if (classMeeting != null && classMeeting.PeriodObj != null)
                {
                    output.PeriodId = classMeeting.PeriodObj.ID;
                    output.PeriodDescription = classMeeting.PeriodObj.PeriodDescrip;
                }
                else
                {
                    output.PeriodId = null;
                    output.PeriodDescription = string.Empty;
                }

                output.AttendanceDate = attendance.MeetDate.Date;
                switch (attendanceType.ToUpper(CultureInfo.InvariantCulture))
                {
                    // MINUTES and CLOCK HOURS will be trated as the same in server....
                    case X_MINUTES:
                        {
                            ProcessDayAttendanceClockMinutes(attendance, ref output);
                            break;
                        }

                    case X_ABSENT_PRESENT:
                        {
                            output.StatusEnum = ProcessDayAttendanceAbsentPresent(attendance);
                            break;
                        }
                    default:
                        {
                            throw new ArgumentException("Internal Error: Bad Attendance Type Argument. Please call FAME");
                        }

                }

                output.AttendanceGuid = attendance.ID;
                // Process the attendance information.
                // Add the model to output
                model.Add(output);
            }
        }

        /// <summary>
        /// Process the attendance following minutes & clock rules. The result is expresed in minutes.
        /// </summary>
        /// <param name="attendance">The attendance record</param>
        /// <param name="output">The processed output with attendance information.</param>
        private void ProcessDayAttendanceClockMinutes(ClassSectionAttendance attendance, ref StudentAttendanceDto output)
        {
            output.Tardies = (attendance.Tardy) ? 1 : 0;
            output.Excused = (attendance.Excused) ? 1 : 0;

            if (attendance.Scheduled == 0)
            {
                // It is make up hour
                output.StatusEnum = Enumerations.AttendanceStatusEnum.MakeUp;
                output.MakeUp = Convert.ToInt32(attendance.Actual);
                output.Absent = 0;
                output.Schedule = 0;
            }
            else
            {
                // It is a scheduled day.....
                // Well here we have two different cases. If Schedule - Actual < 0 the rest is makeup
                // If Schedule - actual > 0 the rest is Absent
                var diff = Convert.ToInt32(attendance.Scheduled - attendance.Actual);
                if (diff <= 0)
                {
                    output.Absent = 0;
                    output.MakeUp = -diff;
                    output.Actual = Convert.ToInt32(attendance.Scheduled.GetValueOrDefault(0));
                }
                else
                {
                    output.Absent = diff;
                    output.MakeUp = 0;
                    output.Actual = Convert.ToInt32(attendance.Actual);
                }
            }

            output.Schedule = Convert.ToInt32(attendance.Scheduled.GetValueOrDefault(0));
        }

        #endregion

        #region Make Up Hours

        public void PostMakeUpHours(PostAttendanceInputModel input)
        {
            // TODO: Validate if the make up hour is inside the range of the class (ask if it is possible other dates?
            // prepare to delete if exists the record in database....
            var enrollment = repository.Query<Enrollment>().SingleOrDefault(x => x.ID == input.EnrollmentGuid);
            if (enrollment != null)
            {
                var attendance = enrollment.ClassSectionAttendanceList.SingleOrDefault(x => x.ClassSectionObj.ID == input.ClsSectionIdGuid && x.MeetDate.Date == input.MeetDate.Date &&
                                                                                            x.Excused == false && x.Tardy == false && x.Scheduled == 0);
                if (attendance != null)
                {
                    if (attendance.Actual == 999 | attendance.Actual == 9999)
                    {
                        // Update The field.
                        attendance.UpdateMakeUp(1, false, false, 0, "MU");
                        return;
                    }

                    throw new DuplicateNameException("Already exists a make up in this day");
                }
                // Do not exists create and save
                var sect = repository.Load<ArClassSections>(input.ClsSectionIdGuid);

                var atten = new ClassSectionAttendance(
                    Guid.Empty,
                    null,
                    enrollment,
                    sect,
                    input.MeetDate,
                    1, false, false, "MU", 0, "sa");
                repository.Save(atten);
            }
        }

        public void DeleteMakeUpHours(PostAttendanceInputModel input)
        {
            // it is possible form infinitus - to today
            // prepare to delete if exists the record in database....
            var enrollment = repository.Query<Enrollment>().SingleOrDefault(x => x.ID == input.EnrollmentGuid);
            if (enrollment != null)
            {
                var attendance = enrollment.ClassSectionAttendanceList.SingleOrDefault(x => x.ClassSectionObj.ID == input.ClsSectionIdGuid && x.MeetDate.Date == input.MeetDate.Date &&
                                                                                            x.Excused == false && x.Tardy == false && x.Scheduled == 0);
                if (attendance != null)
                {
                    // Delete the field
                    repository.Delete(attendance);
                }
                else
                {
                    // Do not exists create and save
                    throw new KeyNotFoundException("EL make up does not exists");
                }
            }
        }

        /// <summary>
        /// Post make up hours for minutes and clock hours
        /// </summary>
        /// <param name="input">Make information</param>
        public void PostMinutesMakeUpHours(PostAttendanceInputModel input)
        {
            // See if exists a record in the same day with the same class
            var enrollment = repository.Query<Enrollment>().SingleOrDefault(x => x.ID == input.EnrollmentGuid);
            if (enrollment != null)
            {
                var attendance = enrollment.ClassSectionAttendanceList.SingleOrDefault(x => x.ClassSectionObj.ID == input.ClsSectionIdGuid && x.MeetDate.Date == input.MeetDate.Date
                                                                                       && x.Actual != 999 && x.Actual != 9999);
                if (attendance != null)
                {
                    // Exists: Add the make up hour to the record
                    var actual = Convert.ToInt32(attendance.Actual);
                    actual += input.MakeUp;
                    attendance.UpdateMakeUp(actual, attendance.Tardy, attendance.Excused, Convert.ToInt32(attendance.Scheduled), "MU");
                }
                else
                {
                    // Si if it is schedule day...
                    var filter = new AttendanceInputModel
                    {
                        StuEnrollmentId = input.EnrollmentGuid,
                        IsSummary = false,
                        FilterIni = input.MeetDate,
                        FilterEnd = input.MeetDate,
                        ClsSectionGuid = input.ClsSectionIdGuid
                    };

                    var sect = repository.Load<ArClassSections>(input.ClsSectionIdGuid);

                    var classSection = repository.Load<ArClassSections>(input.ClsSectionIdGuid);
                    var calendardays = GetCalendarDaysList(filter, classSection, X_MINUTES);

                    if (calendardays != null && calendardays.Any())
                    {
                        // Exist a calendar day create with Schedule 1
                        var day = calendardays.First();
                        var cal = repository.Load<ClassSectionMeeting>(day.ClassSectionMeetingGuid);
                        var atten = new ClassSectionAttendance(
                              Guid.Empty,
                              cal,
                              enrollment,
                              sect,
                              input.MeetDate,
                              input.MakeUp, false, false, "MU", day.Schedule, "sa");
                        repository.Save(atten);
                    }
                    else
                    {
                        // It is not a calendar day create with schedule = 0
                        var atten = new ClassSectionAttendance(
                              Guid.Empty,
                              null,
                              enrollment,
                              sect,
                              input.MeetDate,
                              input.MakeUp, false, false, "MU", 0, "sa");
                        repository.Save(atten);
                    }
                }
            }
        }

        public void DeleteMinutesMakeUpHours(PostAttendanceInputModel input)
        {
            // it is possible form infinitus - to today
            // prepare to delete if exists the record in database....
            var enrollment = repository.Query<Enrollment>().SingleOrDefault(x => x.ID == input.EnrollmentGuid);
            if (enrollment != null)
            {
                var attendance = enrollment.ClassSectionAttendanceList.SingleOrDefault(x => x.ClassSectionObj.ID == input.ClsSectionIdGuid && x.MeetDate.Date == input.MeetDate.Date);
                if (attendance != null)
                {
                    // Delete the Make up
                    if (attendance.Actual <= input.MakeUp)
                    {
                        // Delete the record 
                        repository.Delete(attendance);
                        return;
                    }

                    // Substract the part
                    var actual = Convert.ToInt32(attendance.Actual);
                    actual -= input.MakeUp;
                    attendance.UpdateMakeUp(actual, attendance.Tardy, attendance.Excused, Convert.ToInt32(attendance.Scheduled), "MU");
                }
                else
                {
                    // Do not exists create and save
                    throw new KeyNotFoundException("The make up does not exists");
                }
            }
        }

        #endregion

        #region Report List

        public List<MenuItem> GetStudentAttendanceReportList(AttendanceInputModel filter)
        {
            const string X_MODULENAME = "ATTENDANCE REPORTS";
            const string X_MENUNAME = "ACADEMICS";
            const string X_TOPLEVELMENU = "Reports";
            var resultList = new List<MenuItem>();
            //get the website base url.  This will be used to construct the full url for the menu links
            ConfigurationAppSetting configurationAppSetting =
                repositoryWithTypedId.Query<ConfigurationAppSetting>()
                    .FirstOrDefault(n => n.KeyName.ToUpper() == "ADVANTAGESITEURI");
            if (configurationAppSetting != null)
            {
                string siteUri = configurationAppSetting.ConfigurationValues[0].Value;

                //validate that site url exists in config settings
                if (string.IsNullOrEmpty(siteUri))
                    throw new HttpBadRequestResponseException("Site Url not found in Config settings");

                // get top level menu item, and retrieve the id and module code
                var module =
                    repositoryWithTypedId.Query<MenuItem>().FirstOrDefault(n => n.MenuName.ToUpper() == X_TOPLEVELMENU);
                if (module == null)
                {
                    return resultList;
                }

                var moduleId = module.ID;
                //var moduleCode = module.ModuleCode;

                // get the id of the sublevel item based on the submenuname and moduleid as parent
                var subMenuId =
                    repositoryWithTypedId.Query<MenuItem>()
                        .Where(n => n.MenuName.ToUpper() == X_MENUNAME && n.ParentId == moduleId)
                        .Select(n => n.ID)
                        .FirstOrDefault();

                //get the sub level items where the parent is the sub menu id just retrieved
                var subLevelMenuItems = repositoryWithTypedId.Query<MenuItem>().Where(n => n.ParentId == subMenuId);

                // Get group of menus for reports
                var attendanceReportItems = subLevelMenuItems.Where(x => x.MenuName == X_MODULENAME).ToList();
                return attendanceReportItems;
            }

            return resultList;
        }

        #endregion

        #region Validations

        /// <summary>
        /// Check if the filter is null.
        /// throw a exception if it is.
        /// </summary>
        /// <param name="filter">filter to be checked</param>
        public void CheckFilterNull(AttendanceInputModel filter)
        {
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("Filter should have a value");
            }
        }

        public void CkeckPostAttendanceInputModel(PostAttendanceInputModel filter)
        {
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("Filter missing: Contact Fame");
            }

            if (filter.ClsSectionIdGuid == Guid.Empty)
            {
                throw new HttpBadRequestResponseException("Section class parameter missing: Contact Fame");
            }

            if (filter.EnrollmentGuid == Guid.Empty)
            {
                throw new HttpBadRequestResponseException("Enrollment parameter missing: Contact Fame");
            }
        }

        #endregion

        #region Helpers, Refactoring

        /// <summary>
        /// Set interval days between 1/1/1990 to today at 23:59:59
        /// </summary>
        /// <param name="beginDate"></param>
        /// <param name="endDate"></param>
        private void SetDateBeginAndEndForAttendance(out DateTime beginDate, out DateTime endDate)
        {
            beginDate = new DateTime(1990, 1, 1, 0, 0, 0);
            var end = DateTime.Now;
            endDate = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59);
        }

        #endregion

        /// <summary>
        /// Flag to validate if student enrollment have attendance or grades posted.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student Enrollment Id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool HaveAttendanceOrGradesPosted(Guid studentEnrollmentId)
        {
            var haveGradeBooks = this.repository.Query<GradeBookResult>().Any(x => x.Enrollment.ID == studentEnrollmentId && x.PostDate.HasValue && x.Score.HasValue);

            var haveResults = this.repository.Query<ArResults>().Any(x => x.StuEnrollment.ID == studentEnrollmentId && (x.GradeSystemDetailsObj != null || x.Score.HasValue));

            if (!haveGradeBooks && !haveResults)
            {
                haveResults = this.repository.Query<CreditSummary>().Any(x => x.StuEnrollId == studentEnrollmentId &&
                    (x.TermGpaSimple > 0 || x.TermGpaWeighted > 0 || x.CumulativeGpa > 0 || x.CumulativeGpaSimple > 0 || x.FinancialAidCreditsEarned > 0 ||
                     x.Average > 0 || x.CumAverage > 0 || x.CreditsEarned > 0 || x.CreditsAttempted > 0 || x.CurrentScore > 0 || x.CurrentGrade.Length > 0 || x.FinalScore > 0 ||
                     x.FinalGrade.Length > 0 || x.FinalGpa > 0 || x.ProductWeightedAverageCreditsGpa > 0 || x.CountWeightedAverageCredits > 0 || x.ProductSimpleAverageCreditsGpa > 0 || x.CountSimpleAverageCredits > 0));
            }

            /*TODO
             Add Validation to Credit Summary here*/

            var haveAttendance = this.repositoryWithTypedId.Query<StudentAttendanceSummary>().Any(x => x.EnrollmentObj.ID == studentEnrollmentId);

            if (!haveAttendance)
            {
                haveAttendance = this.repository.Query<ClassSectionAttendance>().Any(x => x.EnrollmentObj.ID == studentEnrollmentId);

                if (!haveAttendance)
                {
                    haveAttendance = this.repository.Query<StudentClockAttendance>().Any(x => x.StuEnrollmentObj.ID == studentEnrollmentId && x.ActualHours > 0);
                }
            }

            return haveGradeBooks || haveResults || haveAttendance;
        }

        /// <summary>
        /// Flag to validate if student enrollment have attendance or grades posted.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student Enrollment Id.
        /// </param>
        /// <param name="startDate">
        /// The start Date.
        /// </param>
        /// <param name="endDate">
        /// The end Date.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool HaveAttendance(Guid studentEnrollmentId, DateTime startDate, DateTime endDate)
        {

            var attendanceSummary = this.repositoryWithTypedId.Query<StudentAttendanceSummary>().Count(x => x.EnrollmentObj.ID == studentEnrollmentId && x.StudentAttendedDate >= startDate && x.StudentAttendedDate <= endDate);
            bool hasAttendance = attendanceSummary > 0;

            if (!hasAttendance)
            {
                var attendance = this.repository.Query<ClassSectionAttendance>().Count(x => x.EnrollmentObj.ID == studentEnrollmentId && x.MeetDate >= startDate && x.MeetDate <= endDate);
                hasAttendance = attendance > 0;
            }

            if (!hasAttendance)
            {
                var attendance = this.repository.Query<StudentClockAttendance>().Count(x => x.StuEnrollmentObj.ID == studentEnrollmentId && x.RecordDate >= startDate && x.RecordDate <= endDate && x.ActualHours > 0);
                hasAttendance = attendance > 0;
            }

            return hasAttendance;
        }

        /// <summary>
        /// The get last date attended.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime?"/>.
        /// </returns>
        public DateTime? GetLastDateAttended(Guid studentEnrollmentId)
        {
            var enrollment = this.repository.Query<Enrollment>().FirstOrDefault(n => n.ID == studentEnrollmentId);
            return this.GetLastDateAttended(enrollment);
        }

        /// <summary>
        /// The get last date attended.
        /// </summary>
        /// <param name="enrollment">
        /// The student enrollment.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime?"/>.
        /// </returns>
        public DateTime? GetLastDateAttended(Enrollment enrollment)
        {
            if (enrollment != null)
            {
                List<DateTime> ldas =
                    enrollment.ExternalShipAttendanceList.Select(x => x.AttendedDate)
                    .Concat(enrollment.ClassSectionAttendanceList.Where(x => x.Actual != 99 && x.Actual != 999 && x.Actual != 9999).Select(x => x.MeetDate))
                    .Concat(enrollment.AttendanceList.Where(x => x.Actual >= 1).Select(x => x.AttendanceDate.HasValue == true ? x.AttendanceDate.Value : DateTime.MinValue))
                    .Concat(enrollment.StudentClockAttendanceList.Where(x => x.ActualHours >= 1 && x.ActualHours != 99 && x.ActualHours != 999 && x.ActualHours != 9999).Select(x => x.RecordDate)).ToList();

                if (enrollment.Lda.HasValue)
                {
                    ldas.Add(enrollment.Lda.Value);
                }

                if (ldas.Count > 0)
                {
                    return ldas.Max();
                }
            }

            return null;
        }



    }
}
