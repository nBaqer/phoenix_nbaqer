﻿using System;
using FAME.Advantage.Messages.Students.Enrollments;

namespace FAME.Advantage.Services.Lib.BusinessLogic.Students.Dto
{
    public class StudentAttendanceDto: BasicAttendanceModel
    {
        /// <summary>
        /// Guid of the attendance slot, if does not exists put Guid.Empty.
        /// </summary>
        public Messages.Enumerations.Enumerations.AttendanceStatusEnum StatusEnum { get; set; }
        public DateTime? StarTime { get; set; }
        public DateTime? EndTime { get; set; }
        public bool IsMarkToRemove { get; set; }

        public int Schedule { get; set; }
        public int Absent { get; set; }
        public int MakeUp { get; set; }
        public int Actual { get; set; }
        public int Tardies { get; set; }
        public int Excused { get; set; }
    }
}
