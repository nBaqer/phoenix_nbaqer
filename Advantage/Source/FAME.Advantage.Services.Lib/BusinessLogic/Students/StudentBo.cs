﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentBo.cs" company="FAME">
//   2013
// </copyright>
// <summary>
//   Defines the StudentBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.Students
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;

    using AutoMapper;

    using Domain.Campuses.CampusGroups.Subresources;
    using Domain.Infrastructure.Entities;
    using Domain.MostRecentlyUsed;
    using Domain.Student;
    using Domain.Student.Enrollments;
    using Domain.SystemStuff.ConfigurationAppSettings;
    using Domain.Users;

    using FAME.Advantage.Domain.Lead;

    using Messages;
    using Messages.MostRecentlyUsed;
    using Messages.Student;
    using Messages.Students;
    using Services.Lib.Infrastructure.Exceptions;
    using Services.Lib.Infrastructure.Extensions;

    public class StudentBo
    {
        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The repository with int.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        private readonly string _studentImagePath;
        private readonly string _advantageSiteUrl;

        const int StudentMruTypeId = (int)Messages.Enumerations.Enumerations.MRuTypes.Students;


        public StudentBo(IRepository studentRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = studentRepository;
            this.repositoryWithInt = repositoryWithInt;



        }

        /// <summary>
        /// Called by either GetAll or GetByStudentId
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public PagedOutputModel<IEnumerable<StudentOutputModel>> Get(GetStudentInputModel filter, Guid? studentId = null)
        {
            // To retrieve results, we must have either a campus id or student id.  This is done to reduce the number of records retrieved
            if (filter.CampusId.IsEmpty() && studentId.IsEmpty()) throw new HttpBadRequestResponseException("Campus Id or Student Id is required");

            // initial query for students
            var students = this.repository.Query<Student>();

            // if student id passed in, get the student based on id.  If not, continue filtering
            if (!studentId.IsEmpty())
            {
                students = students.Where(n => n.ID == studentId);
            }
            else
            {
                // campus id passed in
                if (!filter.CampusId.IsEmpty())
                {
                    var studentsfromEnrollment = this.repository.Query<Enrollment>().Where(n => n.CampusId == filter.CampusId).Select(n => n.Student.ID);
                    students = students.Where(n => studentsfromEnrollment.Contains(n.ID));

                }

                // program version id passed in
                if (!filter.ProgramVersionId.IsEmpty())
                {
                    var studentsfromEnrollmentByProgram = this.repository.Query<Enrollment>()
                                                .Where(n => n.ProgramVersion.ID == filter.ProgramVersionId)
                                                .Select(n => n.Student.ID);

                    students = students.Where(n => studentsfromEnrollmentByProgram.Contains(n.ID));
                }

                // enrollment status id passed in
                if (!filter.EnrollmentStatusId.IsEmpty())
                {

                    var studentsfromEnrollmentByEnrollStatus = this.repository.Query<Enrollment>()
                                                .Where(n => n.EnrollmentStatus.ID == filter.EnrollmentStatusId)
                                                .Select(n => n.Student.ID);


                    students = students.Where(n => studentsfromEnrollmentByEnrollStatus.Contains(n.ID));
                }

                // Student Group id passed in
                if (!filter.StudentGroupId.IsEmpty())
                {
                    var studentIds = this.repository.Query<Enrollment>().Where(r => this.repository.Query<LeadGroupBridge>()
                                                            .Where(n => n.StudentGroupId == filter.StudentGroupId)
                                                            .Select(n => n.StudentEnrollmentId)
                                                            .Contains(r.ID))
                                                                .Select(n => n.StudentId);


                    students = students.Where(n => studentIds.Contains(n.ID));
                }

                // Start Date passed in
                if (filter.StartDate != null)
                {
                    var studentsfromEnrollmentByStartDate = this.repository.Query<Enrollment>()
                                                .Where(n => n.StartDate == filter.StartDate)
                                                .Select(n => n.Student.ID);

                    students = students.Where(n => studentsfromEnrollmentByStartDate.Contains(n.ID));
                }

                // Student Number passed in
                if (!string.IsNullOrEmpty(filter.StudentNumber))
                    students = students.Where(n => n.StudentNumber.Contains(filter.StudentNumber));

                // First Name passed in
                if (!string.IsNullOrEmpty(filter.FirstName))
                    students = students.Where(n => n.FirstName.Contains(filter.FirstName));

                // Last Name passed in
                if (!string.IsNullOrEmpty(filter.LastName))
                    students = students.Where(n => n.LastName.Contains(filter.LastName));

            }

            // map the final results to output model object and return to client
            var total = students.Count();
            students = students.OrderBy(n => n.LastName).ThenBy(n => n.FirstName).ThenBy(n => n.StudentNumber);

            var pagedStudents = Mapper.Map<IEnumerable<StudentOutputModel>>(students.Page(filter.Page, filter.PageSize).ToArray().OrderBy(n => n.LastName).ThenBy(n => n.FirstName).ThenBy(n => n.StudentNumber));
            return new PagedOutputModel<IEnumerable<StudentOutputModel>>(total, pagedStudents);
        }

        /// <summary>
        /// Get the Students based on the search term passed in from the universal search
        /// </summary>
        /// <param name="searchTermInit">
        /// The search Term Initialization.
        /// </param>
        /// <returns>
        /// The student selected information
        /// </returns>
        public IEnumerable<StudentSearchOutputModel> GetStudentSearch(string[] searchTermInit)
        {
            var searchTerm = searchTermInit[0];

            if (string.IsNullOrEmpty(searchTerm))
            {
                return null;
            }

            var campusesList = new List<Guid>();
            if (searchTermInit.Count() > 1)
            {
                var list = searchTermInit[1].ToString(CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(list))
                {
                    campusesList = list.Split(',').Select(Guid.Parse).ToList();
                }
            }

            // Search the shadow lead
            var shadowLead = this.repository.Query<Lead>()
                    .Where(n => n.StudentId != Guid.Empty && ((n.LastName ?? string.Empty)
                    + string.Empty + (n.FirstName ?? string.Empty)
                    + " " + (n.SSN ?? string.Empty)
                    + " " + (n.StudentNumber ?? " "
                    + " ")).Contains(searchTerm)).Take(100);

            //// shadowLead = shadowLead.Where(x => x.StudentId.ToString() != "00000000-0000-0000-0000-000000000000").Take(100);

            var shadowListPhones =
                this.repository.Query<Lead>()
                    .Where(n => n.LeadPhoneList.Any(r => r.Phone.Contains(searchTerm)))
                    .Take(100);

            ////// get the list of students using the search term
            ////var students = this.repository.Query<Student>()
            ////        .Where(n => ((n.LastName ?? "") + string.Empty + (n.FirstName ?? string.Empty) + " " + (n.SSN ?? string.Empty) + " " + (n.StudentNumber ?? " " + " ")).Contains(searchTerm)).Take(100);


            ////var studentsPhones = this.repository.Query<Student>()
            ////        .Where(n => n.StudentPhones.Any(r => r.Phone.Contains(searchTerm))).Take(100);

            var finalShadowList = new List<Lead>();
            finalShadowList.AddRange(shadowLead);
            finalShadowList.AddRange(shadowListPhones);
            finalShadowList = finalShadowList.Distinct().ToList();

            //// var finalStudents = new List<Student>();

            ////finalStudents.AddRange(students);
            ////finalStudents.AddRange(studentsPhones);
            ////finalStudents = finalStudents.Distinct().ToList();

            var finalShadowList2 = campusesList.Count > 0 ? (from s in finalShadowList from e in s.EnrollmentList where campusesList.Contains(e.CampusId) select s).ToList() : finalShadowList;
            finalShadowList2.ForEach(r => r.GetImageUrl(this.StudentImagePath, this.AdvantageSiteUrl));
            var output = Mapper.Map<IEnumerable<StudentSearchOutputModel>>(finalShadowList2.Distinct().OrderBy(n => n.LastName).ThenBy(n => n.FirstName).ToArray());
            return output;

            ////var finalStudents2 = campusesList.Count > 0 ? (from s in finalStudents from e in s.Enrollments where campusesList.Contains(e.CampusId) select s).ToList() : finalStudents;
            ////finalStudents2 = finalStudents2.Distinct().ToList();


            // return the list of students
            ////return Mapper.Map<IEnumerable<StudentSearchOutputModel>>(finalStudents2.OrderBy(n => n.LastName).ThenBy(n => n.FirstName).ToArray());
        }

        public IEnumerable<StudentMruOutputModel> GetStudentMru(Guid userId, Guid campusId)
        {
            var mrus = this.repository.Query<Mru>().Where(n => n.MruTypeObj.ID == StudentBo.StudentMruTypeId && n.UserObj.ID == userId).OrderByDescending(n => n.ModDate);
            var mruGuids = mrus.Select(m => m.EntityId).ToList();
            var mruList = mrus.Select(l => new MruOutput { EntityId = l.EntityId, ModDate = l.ModDate }).ToList();

            if (mrus.Any())
            {
                var students = this.repository.Query<Student>().Where(n => mruGuids.Contains(n.ID))
                    .ForEach(r => r.GetImageUrl(this.StudentImagePath, this.AdvantageSiteUrl));

                return this.FillMruOutput(students, mruList);
            }
            else
            {
                var campusIdList = this.GetCampusItemsByUserId(userId);

                var st2 = this.repository.Query<Student>().Where(n => n.Enrollments.Any(c => campusIdList.Contains(c.CampusId)))
                    .OrderByDescending(n => n.ModDate)
                        .ForEach(r => r.GetImageUrl(this.StudentImagePath, this.AdvantageSiteUrl));


                return this.FillMruOutput(st2, null);
            }
        }



        private IEnumerable<StudentMruOutputModel> FillMruOutput(IEnumerable<Student> students, IEnumerable<MruOutput> mruList)
        {
            var mruOutput = new List<StudentMruOutputModel>();

            DateTime? nullModDate = null;

            foreach (var s in students)
            {
                Student s1 = s;
                string image64;
                if (mruOutput.All(n => n.Id != s1.ID))
                {

                    var lead = this.repository.Query<Lead>().SingleOrDefault(x => x.StudentId == s.ID);
                    if (lead != null)
                    {
                        var imgObject = lead.ImageList64.FirstOrDefault(x => x.Type == 1);
                        image64 = (imgObject != null)
                                      ? string.Format(
                                          "data:{0};base64,{1}",
                                          imgObject.MediaType,
                                          Convert.ToBase64String(imgObject.Image))
                                      : this.ImageDefault();
                    }
                    else
                    {
                        image64 = this.ImageDefault();
                    }

                    var enr = s.Enrollments.OrderByDescending(n => n.LeadObj == null ? n.ModDate : n.LeadObj.ModDate ?? n.ModDate).FirstOrDefault();
                    if (enr != null)
                    {
                        var mru = new StudentMruOutputModel
                        {
                            Id = s.ID,
                            Campus = enr == null ? "None" : enr.CampusObj.Description,
                            FullName = s.FullName,
                            SSN = s.SSN,
                            StartDate =
                                              enr == null
                                                  ? null
                                                  : (enr.StartDate != null ? enr.StartDate.Value.ToShortDateString() : "None"),
                            Status = enr == null ? "None" : enr.EnrollmentStatus.Description,
                            Program =
                                              enr == null
                                                  ? "None"
                                                  : enr.ProgramVersion == null ? "None" : enr.ProgramVersion.Description,
                            StudentNumber = s.StudentNumber,
                            Image64 = image64, // s.ImageUrl,
                            NotFoundImageUrl = s.NotFoundImageUrl,
                            MruModDate =
                                              enr == null
                                                  ? null
                                                  : enr.LeadObj == null
                                                      ? enr.ModDate.ToString()
                                                      : enr.LeadObj.ModDate == null
                                                          ? enr.ModDate.ToString()
                                                          : enr.LeadObj.ModDate.ToString(),

                            // ModDate = enr == null ? null : enr.LeadObj == null ? enr.ModDate : enr.LeadObj.ModDate?? enr.ModDate
                            ModDate = enr == null ? nullModDate : enr.ModDate
                        };

                        if (enr != null)
                            mru.SearchCampusId = enr.CampusObj.ID;

                        if (mru.Program.Length > 35)
                            mru.Program = enr.ProgramVersion.Description.Substring(0, 32) + "...";

                        if (!string.IsNullOrEmpty(mru.SSN))
                        {
                            var ssn = mru.SSN;
                            var ssnRegex = new Regex("(?:[0-9]{3})(?:[0-9]{2})(?<last>[0-9]{4})");
                            mru.SSN = ssnRegex.Replace(ssn, "***-**-${last}");
                        }

                        if (mruList != null)
                        {
                            var mruL = mruList.ToList();

                            var mruItem = mruL.FirstOrDefault(n => n.EntityId == s.ID);
                            if (mruItem != null)
                            {
                                mru.MruModDate = mruItem.ModDate.ToString();
                                mru.ModDate = mruItem.ModDate;
                            }
                        }

                        mruOutput.Add(mru);
                    }
                }
            }

            mruOutput = mruOutput.Distinct().OrderByDescending(n => n.ModDate).ToList();

            return mruOutput.Take(5);
        }


        /// <summary>
        /// Get the list of campus by User Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>a list of ID Description</returns>
        private IEnumerable<Guid> GetCampusItemsByUserId(Guid userId)
        {
            if (userId == Guid.Empty) return null;

            var listItems = this.repository.Query<User>().Where(n => n.ID == userId).SelectMany(n => n.CampusGroup)
                            .SelectMany(y => y.Campuses).Where(y => y.Status.StatusCode == "A").Select(l => l.ID).ToList(); // .SelectMany(y => y.Description).Distinct();


            return listItems;
        }


        private string StudentImagePath
        {
            get { return string.IsNullOrEmpty(this._studentImagePath) ? this.GetConfigurationSetting("STUDENTIMAGEPATH") : this._studentImagePath; }
        }

        private string AdvantageSiteUrl
        {
            get { return string.IsNullOrEmpty(this._studentImagePath) ? this.GetConfigurationSetting("ADVANTAGESITEURI") : this._studentImagePath; }
        }

        private string GetConfigurationSetting(string keyName)
        {
            string retValue = null;
            var configurationAppSetting = this.repositoryWithInt.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == keyName.ToUpper());
            if (configurationAppSetting != null) retValue = configurationAppSetting.ConfigurationValues[0].Value;

            return retValue;
        }

        private string ImageDefault()
        {
            var image64 = @"data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAEsAAABLCAIAAAC3LO29AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAiESURBVHhe3Vv3V1RHFLa32GJijJrkaIrR5KScREVBRFFEooKFqLGXKL1JU0BEDKIICIoiAgIqKiogAoKgossiUpe6LM0FFlg6WP6AfGZPzJ5F3PfmzS5Gzh5+ujPvfnP7nTsDO5+9HPB+/wHh+/0boGp4HT1vPsGOnhctnc/wX9UMqBBhe8+Lgor6a2l5Men5d7JKc8qe5gnrHuSJkvklmYJqUV1LZOLjlOyy9BxhWW0TiFUEVVUIxc0daU+E+4Pil5ifXGoRsMYxeI/XJVPvK5tcw1c7BG/3iHQ+Faf9p6+xc8iGA6EOATeDYx/eflQsFDdTh0oZYcezl61dz7KKa0Nv8dcfCJ1i4DRgtonS3xANs0nLHJaaB/heSq9qaG3rfk5RnjQRwqia2rtzy8VG9mc/WrJv0BxTpdjkCQbNNf1U3zEsgV9Y2dDY1k3LRGkiBFup2eU6Jn4jtSwHMhBdb/yD5piMWmAFYd5Iz6+XdrZ3UzBOOghx3tmlT12DE37Y6AkWB7KUnjxUrP1A23raKhc9i0C/6HTg5KixFBC2db24eb9gq/uF6YauQ+eZs9LMvoiBc7S2tbFTCDxwPyOE64t9UGjsfO4TPQcq2OQ3+Xad+9ELd/4Jm+RpCScZAh78+ya3sI+X2lOHhw1Halkh0lTWt3AJIZwQSjt6whP4X692UwU82Z4zjQ8l8UsQgZjrqoLAyRHC/MpqmgysT43VsVEdwknLHO38r8NLM0eoQPkGhAyVvrS2yfpEzJiF1gPnKI/pxEcweqH1ErOTdc0d5AgZ4lH4gKS168a9gikGzmzDOluocM5QVCQ6xAkAoZaW1DR6hiYhC2HLMVt6fGLyciekgdLOHjIxEmopigNkz2zZJaMfv9g2OiW3TkqoqIQI72SVwTzIOGa7Cqboe/lVRk5Nhkw2unG/4PsNh9nySkaPJM4jNFFUJ2XCWG8aQjuMTs35fMV+Mo7ZrkIev/vIxeKqRhn3bF0jIcKrd/O+NFJhoJc/BbjTBbt9npQSJqiECBN5xZq7fNhKg4x+2HwLfctTeeVitWppyuOyperyNMM1LTfsDxWI6tWKELmizh5fMpmwXTVC03L7ociiyga1Ikx4VDRv5zG2vJLRj9Cy3OUJTyNRK8LEzGKt3WqyQ8hwm3uEoFK9CNH/1DXzJ5MJ21XD51ugtYXWq1plmJZTsdLuDFteyegRLZA/5ZSp15fyCqu3HLzApePEHC1kuM7pXIGcL2UV9AnjYXVDa1g8f+Y6dzRzmfNKQInic4yOjXdECnoZatVSdDIFogbTo9Eo8FVaAQ+bbz5jnfvDgsrmDnrVE8OjamjpissQ/LrVC3kjgXAYLhm3yHat4zkuXVNCLZWdAnr47iG3pxo4M2SXgGzqb86eYclNdPs0DGUIstau52jjz/rdg4B1hktmrD0Ye78AJsicK+WdKOZ7oXdSLWlD6B+iQafVrQB7hKbFYlP/2sa29v7qCL8SY+dzhA3cNDGUCSsyuLE1DsEc72c42aFM2ufjM3/Z4kW96TZBd5/WLp/DoUnMdeqNlBQQltY02gfcnG7kyko+byeGf15lFxQSy+MJqvofIThIzxUa7w8ZNJda9Id/9o9OJ+6Ryh8KBRliu+LqRrNj0bSu1iDen/74K/w2n6P0ZMvpIIzLKFxuFUh27/tGddU19UdPvR8Q9pXyImcMup7x8+YjVDK46YYuFj5XM/Ir+wFhX5/ECEb502aA1Njpzd3foJK4lpaPMNj/CBVEiss276hUtKiJIwfKMSQPx6NScS/CMQy+Ph1OdqiAEEnco8KqHzd6Ersc2U0TGnnElURvsXNC2Hu74moJRp7Iqg3YMCoJx8DYCtIGvqoivvy+RVUSdDfIEA7WMJtu5JZbXocrGCoWSDNavGZIUNmwzCoQ3TECfzNmoY2eZYCklesADc3aovdJoyOms9cPnRUChEjfN7uFU7RAFjJk3vnJF9bN3uo9jGhuaKKew16vS5jvoKii1HIaGU9II3FDNMv4EFl7CkM5Ow9HYvzy3UWICMYrrPrMwHkwUQo+dpHNCtvTaP+8uwhxSZuSVTZa24Ys4g+db/7deo9coZhLz4JdPGRufrJ9MRcccTuLwMe8XjJOx+ZIWBISQIYJDRMOaUb8rOKabYciuCBEhTlRz/5Y5F2MI9HSVWoIRfVSn4t3J+szGnt+yynAhjEhgKuY83GZQrEUOT1HqBQQQlXKxdKTV+7N3e49mNIMEWZo0PvZeTgqJI5Xgn5eF/nkN1eEbd0v7ueJ3IITAA/X0VxUVGEtpqE+1LXDtiZHL5+OeZCWUy6qbyFwQuQIca6Y4onPEOCkv1h5gMx/Kj0R1FPIkDBKu9ElFM3vyKTHSZklPEE1EmCM8zGRLQlCVEm1je2ZRdWnYjJwxhjsVsooFQLUVnjeoLnr+Ca3cNezCfDbeNKRJxRXiKWYJOyrbcUOoewpD84v6PrDRXv9yCI7FbSyTSbo2i3cc8L5dOytR0U1kjbkQ5i1xUCxvH9ihxAzz5ifX2zqN0HXHmMuFHkl2wqmATYw8z5luZPGjmPWvjFRydl5FXUSuecayhHiBQvsLYFX5HImfpllwFerXbGjem5/mcOGW0JROknf8Zu1B2E4hvvOHAiKT+SXII/vEyGyikJRA95leZxP3HwwXHvPCbTARi2wVJFHYQ5GKSVsBzKYZuiibxUIBVZECO8EhU7ml/pH34Ob1jU7iVcUZBWtUlZUTTBc0wJTzP8hhNBwO383u9zvcjp6LZjMVdGdmaqBKe4vy4lQWRdVSs7F8gysT6vN+6sJKuDhXUGmoAYznKiy1fRVondfhLwBYXRKzmIz//GLbPs9vhFiePt5IbWdt+M4+lzvWgCgh3br0f+pq2R6BO+v6P59ycP0JNTpG+h+671H+DcL5YizGcw8pwAAAABJRU5ErkJggg==";
            return image64;
        }
    }
}
