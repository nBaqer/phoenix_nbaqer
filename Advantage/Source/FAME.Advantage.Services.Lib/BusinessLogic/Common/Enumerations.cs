﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Services.Lib.BusinessLogic.Common
{
    public class Enumerations
    {
        public enum eValidationMode
        {
            Insert = 1,
            Edit = 2,
            Delete = 3
        }
        public enum eSystemStatus
        {
            NoHistory = 0,
            FutureStart = 7,
            NoStart = 8,
            CurrentlyAttending = 9,
            LeaveOfAbsence = 10,
            Suspension = 11,
            Dropped = 12,
            Graduated = 14,
            TransferOut = 19,
            AcademicProbationSap = 20,
            Externship = 22,
            DisciplinaryProbation = 23,
            WarningProbation = 24,
        }

        public enum eSystemStatusLevel
        {
            Lead = 1,
            StudentEnrollment = 2
        }
    }
}
