﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Externship.cs" company="FAME Inc.">
// FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus  
// </copyright>
// <summary>
//   Defines the Externship type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus
{
    using System;
    using System.Net;

    using FAME.Advantage.Domain.Enumerations;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Student.Enrollments;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Messages.SystemStuff.SystemStatuses;
    using FAME.Advantage.Messages.Web;

    /// <summary>
    ///  Business Object for handling Validations and CRUD of Externship Status Change
    /// </summary>
    public class Externship : SystemStatusChange, ISystemStatusChange
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Externship"/> class. 
        /// </summary>
        /// <param name="systemStatusChangeModel">Input Model of the Future Start Status Change</param>
        /// <param name="repository">The nHibernate Repository for Guids</param>
        /// <param name="repositoryWithInt">The nHibernate Repository for integers</param>
        public Externship(SystemStatusChangeModel systemStatusChangeModel, IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
            : base(systemStatusChangeModel, repository, repositoryWithInt)
        {
        }

        /// <summary>
        /// Inserts a new Externship
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> Insert()
        {
            Response<SystemStatusChangeModel> response = this.ValidateStatusChange();

            try
            {
                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    bool isStatusChangeTopSequence = this.IsStatusChangeTopSequence();

                    response = this.InsertStatusChange();

                    if (!isStatusChangeTopSequence && response.ResponseCode == (int)HttpStatusCode.OK)
                    {
                        response = this.UpdateFutureSiblingStatus();
                    }

                    if (isStatusChangeTopSequence)
                    {
                        response = this.UpdateEnrollmentStatusAndFlush();
                    }
                    else
                    {
                        response.ResponseCode = (int)HttpStatusCode.OK;
                    }
                }
            }
            catch (Exception exception)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = exception.Message;
                this.Repository.Session.Transaction.Rollback();
            }

            return response;
        }

        /// <summary>
        /// Deletes the Externship Status Change instance
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> Delete()
        {
            Response<SystemStatusChangeModel> response = this.ValidateStatusChange();
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    SystemStatusChangeHistory statusToDelete = this.GetStatusToDelete();

                    SystemStatusChangeHistory previousStatus = null;

                    if (statusToDelete != null && statusToDelete.DateOfChange != null)
                    {
                        previousStatus = this.GetPreviousStatus(statusToDelete);
                    }

                    if (previousStatus != null)
                    {
                        this.Model.EffectiveDate = statusToDelete.DateOfChange.ToString();

                        bool isStatusChangeTopSequence = this.IsStatusChangeTopSequence();

                        if (isStatusChangeTopSequence)
                        {
                            if (previousStatus.NewStatus != null)
                            {
                                this.Model.StatusCodeId = previousStatus.NewStatus.ID;

                                switch ((Enumerations.eSystemStatus)previousStatus.NewStatus.SystemStatus.ID)
                                {
                                    case Enumerations.eSystemStatus.Dropped:
                                        {
                                            string deleteReason = this.Model.Reason;

                                            if (previousStatus.DropReasonId != null)
                                            {
                                                this.Model.Reason = previousStatus.DropReasonId.Value.ToString();
                                            }

                                            response = this.UpdateEnrollmentStatusToDropped();
                                            if (response.ResponseCode == (int)HttpStatusCode.OK)
                                            {
                                                this.InactivateStudentBaseOnEnrollments();
                                            }

                                            if (response.ResponseCode == (int)HttpStatusCode.OK)
                                            {
                                                response = this.DeleteFutureScheduledResults();
                                            }
                                            this.Model.Reason = deleteReason;
                                            break;
                                        }

                                    case Enumerations.eSystemStatus.FutureStart:
                                        {
                                            Response<Enrollment> responseEnrollment = this.UpdateEnrollmentFutureStart();
                                            response.ResponseCode = responseEnrollment.ResponseCode;
                                            responseEnrollment.ResponseMessage = responseEnrollment.ResponseMessage;
                                            break;
                                        }

                                    case Enumerations.eSystemStatus.NoStart:
                                        {
                                            Response<Enrollment> responseEnrollment =
                                                this.UpdateEnrollmentStatusNoStart();
                                            response.ResponseCode = responseEnrollment.ResponseCode;
                                            response.ResponseMessage = responseEnrollment.ResponseMessage;
                                            break;
                                        }

                                    default:
                                        {
                                            Response<Enrollment> responseEnrollment = this.UpdateEnrollmentStatus();
                                            response.ResponseCode = responseEnrollment.ResponseCode;
                                            response.ResponseMessage = responseEnrollment.ResponseMessage;
                                            break;
                                        }
                                }

                                if (response.ResponseCode == (int)HttpStatusCode.OK)
                                {
                                    response = this.DeleteStatusChange();
                                }
                            }
                            else
                            {
                                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                                response.ResponseMessage = "Unable to delete the Status. Please correct the previous status that have an empty Status Code.";
                            }
                        }
                        else
                        {
                            response = this.DeleteStatusChange();
                            if (response.ResponseCode == (int)HttpStatusCode.OK)
                            {
                                this.Model.EffectiveDate = statusToDelete.DateOfChange.Value.ToString();
                                response = this.UpdateFutureSiblingStatus();
                            }
                        }
                    }
                    else
                    {
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Unable to delete the Status. At least one status must exist prior to this status.";
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// Update an existing Externship
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> Update()
        {
            Response<SystemStatusChangeModel> response = this.ValidateStatusChange();
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
            }

            return response;
        }

        /// <summary>
        /// Validate the required fields and global business object validations for CRUD Operations.
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> ValidateStatusChange()
        {
            Response<SystemStatusChangeModel> response = this.ValidateChange();
            return response;
        }
    }
}
