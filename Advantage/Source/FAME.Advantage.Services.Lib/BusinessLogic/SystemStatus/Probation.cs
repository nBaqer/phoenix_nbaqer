﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Probation.cs" company="FAME Inc.">
// FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus
// </copyright>
// <summary>
//   Business Object for handling Validations and CRUD of Probation Start Status Change
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus
{
    using System.Net;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.SystemStuff.SystemStatuses;
    using FAME.Advantage.Messages.Web;

    /// <summary>
    /// Business Object for handling Validations and CRUD of Probation Start Status Change
    /// </summary>
    public class Probation: SystemStatusChange, ISystemStatusChange
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Probation"/> class. 
        /// </summary>
        /// <param name="systemStatusChangeModel">Input Model of the Future Start Status Change</param>
        /// <param name="repository">The nHibernate Repository for Guids</param>
        /// <param name="repositoryWithInt">The nHibernate Repository for integers</param>
        public Probation(SystemStatusChangeModel systemStatusChangeModel, IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
            : base(systemStatusChangeModel, repository, repositoryWithInt)
        {
        }

        /// <summary>
        /// Inserts a new Probation
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> Insert()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>() { ResponseCode = (int)HttpStatusCode.InternalServerError, ResponseMessage = "No Impemented Exception" };
            return response;
        }

        /// <summary>
        /// Deletes the Probation Status Change instance
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> Delete()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>() { ResponseCode = (int)HttpStatusCode.InternalServerError, ResponseMessage = "No Impemented Exception" };
            return response;
        }

        /// <summary>
        /// Update an existing Probation
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> Update()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>() { ResponseCode = (int)HttpStatusCode.InternalServerError, ResponseMessage = "No Impemented Exception" };
            return response;
        }

        /// <summary>
        /// Validate the required fields and global business object validations for CRUD Operations.
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> ValidateStatusChange()
        {
            Response<SystemStatusChangeModel> response = this.ValidateChange();
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
            }

            return response;
        }
    }
}
