﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Suspension.cs" company="FAME Inc.">
// FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus
// </copyright>
// <summary>
//   Business Object for handling Validations and CRUD of Suspension Start Status Change
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus
{
    using System;
    using System.Linq;
    using System.Net;

    using FAME.Advantage.Domain.Enumerations;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Student.Enrollments;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Messages.SystemStuff.SystemStatuses;
    using FAME.Advantage.Messages.Web;

    /// <summary>
    /// Business Object for handling Validations and CRUD of Suspension Start Status Change
    /// </summary>
    public class Suspension : SystemStatusChange, ISystemStatusChange
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Suspension"/> class. 
        /// </summary>
        /// <param name="systemStatusChangeModel">Input Model of the Future Start Status Change</param>
        /// <param name="repository">The nHibernate Repository for Guids</param>
        /// <param name="repositoryWithInt">The nHibernate Repository for integers</param>
        public Suspension(SystemStatusChangeModel systemStatusChangeModel, IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
            : base(systemStatusChangeModel, repository, repositoryWithInt)
        {
        }

        /// <summary>
        /// Inserts a new Suspension
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> Insert()
        {
            Response<SystemStatusChangeModel> response = this.ValidateStatusChange();

            try
            {
                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    if (this.StudentAttendance.HaveAttendance(Guid.Parse(this.Model.StudentEnrollmentId), DateTime.Parse(this.Model.EffectiveDate), DateTime.Parse(this.Model.EndDate)))
                    {
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "The change status action was not performed for the student. Student Attendance has been posted between the selected Suspension dates.";
                    }
                    else
                    {
                        bool isStatusChangeTopSequence = this.IsStatusChangeTopSequence();

                        if (DateTime.Parse(this.Model.EffectiveDate) <= DateTime.Now)
                        {
                            response = this.InsertStatusChange();
                        }
                        else
                        {
                            response.ResponseCode = (int)HttpStatusCode.OK;
                        }

                        if (!isStatusChangeTopSequence && response.ResponseCode == (int)HttpStatusCode.OK)
                        {
                            response = this.UpdateFutureSiblingStatus();
                        }

                        if (isStatusChangeTopSequence && DateTime.Parse(this.Model.EffectiveDate) <= DateTime.Now)
                        {
                            response = this.UpdateEnrollmentStatusAndFlush();
                        }
                        else
                        {
                            response.ResponseCode = (int)HttpStatusCode.OK;
                        }

                        var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));

                        if (this.Model.StatusChangeHistoryId != null && enrollment != null)
                        {
                            StudentSuspensions studenSuspension = new StudentSuspensions(enrollment, DateTime.Parse(this.Model.EffectiveDate), DateTime.Parse(this.Model.EndDate), this.Model.UserName, this.Model.Reason, this.Model.StatusChangeHistoryId);

                            if (enrollment.StudentSuspensionsList == null)
                            {
                                enrollment.InitializeSuspensionList();
                            }

                            enrollment.StudentSuspensionsList.Add(studenSuspension);

                            this.Repository.SaveAndFlush(enrollment);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = exception.Message;
                this.Repository.Session.Transaction.Rollback();
            }

            return response;
        }

        /// <summary>
        /// Deletes the Suspension Status Change instance
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> Delete()
        {
            Response<SystemStatusChangeModel> response = this.ValidateStatusChange();
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
                SystemStatusChangeHistory statusToDelete = this.GetStatusToDelete();

                SystemStatusChangeHistory previousStatus = null;

                if (statusToDelete != null && statusToDelete.DateOfChange != null)
                {
                    previousStatus = this.GetPreviousStatus(statusToDelete);
                }

                if (previousStatus != null)
                {
                    this.Model.EffectiveDate = statusToDelete.DateOfChange.ToString();

                    bool isStatusChangeTopSequence = this.IsStatusChangeTopSequence();

                    if (isStatusChangeTopSequence)
                    {
                        if (previousStatus.NewStatus != null)
                        {
                            this.Model.StatusCodeId = previousStatus.NewStatus.ID;

                            switch ((Enumerations.eSystemStatus)previousStatus.NewStatus.SystemStatus.ID)
                            {
                                case Enumerations.eSystemStatus.Dropped:
                                    {
                                        string deleteReason = this.Model.Reason;

                                        if (previousStatus.DropReasonId != null)
                                        {
                                            this.Model.Reason = previousStatus.DropReasonId.Value.ToString();
                                        }

                                        response = this.UpdateEnrollmentStatusToDropped();

                                        if (response.ResponseCode == (int)HttpStatusCode.OK)
                                        {
                                            this.InactivateStudentBaseOnEnrollments();
                                        }

                                        if (response.ResponseCode == (int)HttpStatusCode.OK)
                                        {
                                            response = this.DeleteFutureScheduledResults();
                                        }
                                        this.Model.Reason = deleteReason;
                                        break;
                                    }

                                case Enumerations.eSystemStatus.FutureStart:
                                    {
                                        Response<Enrollment> responseEnrollment = this.UpdateEnrollmentFutureStart();
                                        response.ResponseCode = responseEnrollment.ResponseCode;
                                        responseEnrollment.ResponseMessage = responseEnrollment.ResponseMessage;
                                        break;
                                    }

                                case Enumerations.eSystemStatus.NoStart:
                                    {
                                        Response<Enrollment> responseEnrollment =
                                            this.UpdateEnrollmentStatusNoStart();
                                        response.ResponseCode = responseEnrollment.ResponseCode;
                                        response.ResponseMessage = responseEnrollment.ResponseMessage;
                                        break;
                                    }

                                default:
                                    {
                                        Response<Enrollment> responseEnrollment = this.UpdateEnrollmentStatus();
                                        response.ResponseCode = responseEnrollment.ResponseCode;
                                        response.ResponseMessage = responseEnrollment.ResponseMessage;
                                        break;
                                    }
                            }

                            var entity = this.Repository.Query<StudentSuspensions>().FirstOrDefault(x =>
                                        x.StuEnrollment.ID == Guid.Parse(this.Model.StudentEnrollmentId)
                                        && x.StudentStatusChangeId == statusToDelete.ID);

                            if (entity != null)
                            {
                                this.Repository.Delete(entity);
                            }

                            if (response.ResponseCode == (int)HttpStatusCode.OK)
                            {
                                response = this.DeleteStatusChange();
                            }
                        }
                        else
                        {
                            response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                            response.ResponseMessage = "Unable to delete the Status. Please correct the previous status that have an empty Status Code.";
                        }
                    }
                    else
                    {
                        var entity = this.Repository.Query<StudentSuspensions>().FirstOrDefault(x =>
                                          x.StuEnrollment.ID == Guid.Parse(this.Model.StudentEnrollmentId)
                                          && x.StudentStatusChangeId == statusToDelete.ID);

                        if (entity != null)
                        {
                            this.Repository.Delete(entity);
                        }

                        response = this.DeleteStatusChange();
                        if (response.ResponseCode == (int)HttpStatusCode.OK)
                        {
                            this.Model.EffectiveDate = statusToDelete.DateOfChange.Value.ToString();
                            response = this.UpdateFutureSiblingStatus();
                        }
                    }
                }
                else
                {
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Unable to delete the Status. At least one status must exist prior to this status.";
                }
            }

            return response;
        }

        /// <summary>
        /// Update an existing Suspension
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> Update()
        {
            Response<SystemStatusChangeModel> response = this.ValidateStatusChange();
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {

            }

            return response;
        }

        /// <summary>
        /// Validate the required fields and global business object validations for CRUD Operations.
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> ValidateStatusChange()
        {
            Response<SystemStatusChangeModel> response = this.ValidateChange();
            if ((Common.Enumerations.eValidationMode)this.Model.ValidationMode
                == Common.Enumerations.eValidationMode.Insert)
            {
                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    if (this.HaveSuspensions())
                    {
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage =
                            "The change status action was not performed for the student. An active Suspension exists for this student..";
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// Gets if student enrollment have an existing Suspension based on the given effective date and end date
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        protected bool HaveSuspensions()
        {
            var loa = this.Repository.Query<StudentSuspensions>().Count(x => x.StuEnrollment.ID == Guid.Parse(this.Model.StudentEnrollmentId) && x.StartDate >= DateTime.Parse(this.Model.EffectiveDate) && x.EndDate <= DateTime.Parse(this.Model.EndDate));
            return loa > 0;
        }
    }
}
