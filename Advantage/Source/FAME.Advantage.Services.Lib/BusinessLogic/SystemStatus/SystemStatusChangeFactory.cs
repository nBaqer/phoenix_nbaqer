﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemStatusChangeFactory.cs" company="FAME Inc.">
// FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus  
// </copyright>
// <summary>
//   Defines the SystemStatusChangeFactory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus
{
    using FAME.Advantage.Domain.Enumerations;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.SystemStuff.SystemStatuses;

    /// <summary>
    /// Business Object Factory to Return the Type of Status Change based on the Model.StatusId
    /// </summary>
    public class SystemStatusChangeFactory
    {
        /// <summary>
        /// Gets a new instance of the <see cref="ISystemStatusChange"/> interface. 
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <param name="repository">
        /// The nHibernate Repository for Guids
        /// </param>
        /// <param name="repositoryWithInt">
        /// The nHibernate Repository for integers
        /// </param>
        /// <returns>
        /// The <see cref="ISystemStatusChange"/>.
        /// </returns>
        public ISystemStatusChange GetStatusChange(SystemStatusChangeModel model, IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            switch ((Enumerations.eSystemStatus)model.StatusId)
            {
                case Enumerations.eSystemStatus.NoStart:
                {
                    return new NoStart(model, repository, repositoryWithInt);
                }

                case Enumerations.eSystemStatus.FutureStart:
                {
                    return new FutureStart(model, repository, repositoryWithInt);
                }

                case Enumerations.eSystemStatus.CurrentlyAttending:
                {
                    return new CurrentlyAttending(model, repository, repositoryWithInt);
                }

                case Enumerations.eSystemStatus.Suspension:
                {
                    return new Suspension(model, repository, repositoryWithInt);
                }

                case Enumerations.eSystemStatus.LeaveOfAbsence:
                {
                    return new LeaveOfAbsence(model, repository, repositoryWithInt);
                }

                case Enumerations.eSystemStatus.DisciplinaryProbation:
                {
                    return new Probation(model, repository, repositoryWithInt);
                }

                case Enumerations.eSystemStatus.WarningProbation:
                {
                    return new Probation(model, repository, repositoryWithInt);
                }

                case Enumerations.eSystemStatus.TransferOut:
                {
                    return new TransferOut(model, repository, repositoryWithInt);
                }

                case Enumerations.eSystemStatus.Dropped:
                {
                    return new Dropped(model, repository, repositoryWithInt);
                }
                case Enumerations.eSystemStatus.Graduated:
                {
                    return new Graduated(model, repository, repositoryWithInt);
                }
                case Enumerations.eSystemStatus.Externship:
                {
                    return new Externship(model, repository, repositoryWithInt);
                }
            }

            return null;
        }
    }
}
