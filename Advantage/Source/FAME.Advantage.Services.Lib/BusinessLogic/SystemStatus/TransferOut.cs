﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TransferOut.cs" company="FAME Inc.">
//   
// </copyright>
// <summary>
//   Defines the TransferOut type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus
{
    using System;
    using System.Linq;
    using System.Net;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Student.Enrollments;
    using FAME.Advantage.Messages.SystemStuff.SystemStatuses;
    using FAME.Advantage.Messages.Web;

    /// <summary>
    /// The transfer out.
    /// </summary>
    public class TransferOut: SystemStatusChange, ISystemStatusChange
    {

        public TransferOut(SystemStatusChangeModel systemStatusChangeModel, IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
            : base(systemStatusChangeModel, repository, repositoryWithInt)
        {

        }

        /// <summary>
        /// The insert.
        /// </summary>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        public Response<SystemStatusChangeModel> Insert()
        {
            ////Response<SystemStatusChangeModel> response = this.ValidateStatusChange();
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>() { ResponseCode = (int)HttpStatusCode.InternalServerError, ResponseMessage = "No Impemented Exception" };
            return response;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        public Response<SystemStatusChangeModel> Delete()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>() { ResponseCode = (int)HttpStatusCode.InternalServerError, ResponseMessage = "No Impemented Exception" };
            return response;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        public Response<SystemStatusChangeModel> Update()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>() { ResponseCode = (int)HttpStatusCode.InternalServerError, ResponseMessage = "No Impemented Exception" };

            return response;
        }

        /// <summary>
        /// The validate status change.
        /// </summary>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        public Response<SystemStatusChangeModel> ValidateStatusChange()
        {
            Response<SystemStatusChangeModel> response = this.ValidateChange();
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {

            }

            return response;
        }
    }
}
