﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FutureStart.cs" company="FAME Inc.">
// FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus
// </copyright>
// <summary>
//   Business Object for handling Validations and CRUD of Future Start Status Change
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;

    using FAME.Advantage.Domain.Enumerations;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Student.Enrollments;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Messages.SystemStuff.SystemStatuses;
    using FAME.Advantage.Messages.Web;

    /// <summary>
    /// Business Object for handling Validations and CRUD of Future Start Status Change
    /// </summary>
    public class FutureStart : SystemStatusChange, ISystemStatusChange
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="FutureStart"/> class. 
        /// </summary>
        /// <param name="systemStatusChangeModel">Input Model of the Future Start Status Change</param>
        /// <param name="repository">The nHibernate Repository for Guids</param>
        /// <param name="repositoryWithInt">The nHibernate Repository for integers</param>
        public FutureStart(SystemStatusChangeModel systemStatusChangeModel, IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
            : base(systemStatusChangeModel, repository, repositoryWithInt)
        {
        }

        /// <summary>
        /// Gets or sets the last status change history.
        /// </summary>
        private SystemStatusChangeHistory LastStatusChangeHistory { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether should update date of change last status.
        /// </summary>
        private bool ShouldUpdateDateOfChangeLastStatus { get; set; }

        /// <summary>
        /// Inserts a new Future Start
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> Insert()
        {
            Response<SystemStatusChangeModel> response = this.ValidateStatusChange();

            try
            {
                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    response = this.ValidateAttendanceAndGrades();

                    if (response.ResponseCode == (int)HttpStatusCode.OK)
                    {
                        if (this.Model.AllowInsert && this.Model.HaveAttendancePosted
                            && this.Model.ShouldConfirmAction)
                        {
                            if (!this.Model.HaveBackup && !this.Model.HaveClientConfirmation)
                            {
                                response.ResponseCode = (int)HttpStatusCode.InternalServerError;

                                if (this.Model.IsSupportTool)
                                {
                                    response.ResponseMessage = "The change status action was not performed for the student. Please confirm you have a Database Backup and Client Have Confirm to remove the attendance and/or grades before changing their status.";
                                }
                                else
                                {
                                    response.ResponseMessage = "The change status action was not performed for the student. Please remove the attendance and/or grades before changing their status.";
                                }
                            }
                        }

                        if (response.ResponseCode == (int)HttpStatusCode.OK)
                        {
                            bool isStatusChangeTopSequence = this.IsStatusChangeTopSequence();

                            Response<Enrollment> responseEnrollment = new Response<Enrollment>();
                            if (isStatusChangeTopSequence)
                            {
                                responseEnrollment = this.UpdateEnrollmentFutureStart();
                            }
                            else
                            {
                                responseEnrollment.ResponseCode = (int)HttpStatusCode.OK;
                            }

                            if (responseEnrollment.ResponseCode == (int)HttpStatusCode.OK)
                            {
                                response = this.UpdateStudentStatus(Enumerations.EStatus.Active);
                            }

                            if (responseEnrollment.ResponseCode == (int)HttpStatusCode.OK)
                            {
                                response = this.InsertStatusChange();
                            }

                            if (this.ShouldUpdateDateOfChangeLastStatus)
                            {
                                this.Repository.Update(this.LastStatusChangeHistory);
                            }

                            if (!isStatusChangeTopSequence && response.ResponseCode == (int)HttpStatusCode.OK)
                            {
                                response = this.UpdateFutureSiblingStatus();
                            }

                            if (response.ResponseCode == (int)HttpStatusCode.OK && (this.Model.AllowInsert && this.Model.HaveAttendancePosted
                                && this.Model.ShouldConfirmAction && this.Model.HaveBackup && this.Model.HaveClientConfirmation) && isStatusChangeTopSequence)
                            {
                                this.DeleteScheduledClassCoursesAndAttendance();
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = exception.Message;
                this.Repository.Session.CancelQuery();
            }

            return response;
        }

        /// <summary>
        /// Deletes the Future Start Status Change instance
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> Delete()
        {
            Response<SystemStatusChangeModel> response = this.ValidateStatusChange();
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
                response = this.IsNotFirstInstanceWithAttendance();

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    SystemStatusChangeHistory statusToDelete = this.GetStatusToDelete();

                    SystemStatusChangeHistory previousStatus = null;

                    if (statusToDelete != null && statusToDelete.DateOfChange != null)
                    {
                        previousStatus = this.GetPreviousStatus(statusToDelete);
                    }

                    if (previousStatus != null)
                    {
                        this.Model.EffectiveDate = statusToDelete.DateOfChange.ToString();

                        bool isStatusChangeTopSequence = this.IsStatusChangeTopSequence();

                        if (isStatusChangeTopSequence)
                        {
                            if (previousStatus.NewStatus != null)
                            {
                                this.Model.StatusCodeId = previousStatus.NewStatus.ID;

                                switch ((Enumerations.eSystemStatus)previousStatus.NewStatus.SystemStatus.ID)
                                {
                                    case Enumerations.eSystemStatus.Dropped:
                                        {
                                            string deleteReason = this.Model.Reason;

                                            if (previousStatus.DropReasonId != null)
                                            {
                                                this.Model.Reason = previousStatus.DropReasonId.Value.ToString();
                                            }

                                            response = this.UpdateEnrollmentStatusToDropped();

                                            if (response.ResponseCode == (int)HttpStatusCode.OK)
                                            {
                                                this.InactivateStudentBaseOnEnrollments();
                                            }

                                            if (response.ResponseCode == (int)HttpStatusCode.OK)
                                            {
                                                response = this.DeleteFutureScheduledResults();
                                            }
                                            this.Model.Reason = deleteReason;
                                            break;
                                        }

                                    case Enumerations.eSystemStatus.FutureStart:
                                        {
                                            Response<Enrollment> responseEnrollment = this.UpdateEnrollmentFutureStart();
                                            response.ResponseCode = responseEnrollment.ResponseCode;
                                            responseEnrollment.ResponseMessage = responseEnrollment.ResponseMessage;
                                            break;
                                        }

                                    case Enumerations.eSystemStatus.NoStart:
                                        {
                                            Response<Enrollment> responseEnrollment =
                                                this.UpdateEnrollmentStatusNoStart();
                                            response.ResponseCode = responseEnrollment.ResponseCode;
                                            response.ResponseMessage = responseEnrollment.ResponseMessage;
                                            break;
                                        }

                                    default:
                                        {
                                            Response<Enrollment> responseEnrollment = this.UpdateEnrollmentStatus();
                                            response.ResponseCode = responseEnrollment.ResponseCode;
                                            response.ResponseMessage = responseEnrollment.ResponseMessage;
                                            break;
                                        }
                                }

                                if (response.ResponseCode == (int)HttpStatusCode.OK)
                                {
                                    response = this.DeleteStatusChange();
                                }
                            }
                            else
                            {
                                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                                response.ResponseMessage = "Unable to delete the Status. Please correct the previous status that have an empty Status Code.";
                            }
                        }
                        else
                        {
                            response = this.DeleteStatusChange();
                            if (response.ResponseCode == (int)HttpStatusCode.OK)
                            {
                                this.Model.EffectiveDate = statusToDelete.DateOfChange.Value.ToString();
                                response = this.UpdateFutureSiblingStatus();
                            }
                        }
                    }
                    else
                    {
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Unable to delete the Status. At least one status must exist prior to this status.";
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// Update an existing Future Start
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> Update()
        {
            Response<SystemStatusChangeModel> response = this.ValidateStatusChange();
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
            }

            return response;
        }

        /// <summary>
        /// Validate the required fields and global business object validations for CRUD Operations.
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> ValidateStatusChange()
        {
            Response<SystemStatusChangeModel> response = this.ValidateChange();
            return response;
        }

        /// <summary>
        /// The validate attendance and grades posted.
        /// This function must be called before an insert of the status.
        /// Also this function can be called as a pre-check before inserting as part of a validation form.
        /// A Future Start can only be inserted if they are not grades or attendance posted with following exceptions:
        /// The future start intended to be inserted will be the first instance.
        /// The future start intended to be inserted will be the second instance, and the previous status was a currently attending and there was no other status on top of the  
        /// currently attending (which means the intended future start to be inserted would be the 3rd and last status, and prior that there is only a currently attending and a future start before the currently attending)
        /// and Upon insert, if the enrollment have attendance or grade posted they must be clear out.
        /// </summary>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        public Response<SystemStatusChangeModel> ValidateAttendanceAndGrades()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            this.ShouldUpdateDateOfChangeLastStatus = false;

            Enrollment enrollment = null;
            List<UserDefStatusCode> statusCodeList = null;
            List<SystemStatusChangeHistory> statusChangeHistory = null;

            enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));
            statusCodeList = this.Repository.Query<UserDefStatusCode>().Where(x => x.SystemStatus.StatusLevelId == (int)Common.Enumerations.eSystemStatusLevel.StudentEnrollment).ToList();
            statusChangeHistory = this.Repository.Query<SystemStatusChangeHistory>().Where(x => x.Enrollment.ID == Guid.Parse(this.Model.StudentEnrollmentId)).OrderBy(x => x.ModifiedDate).ToList();

            DateTime effectiveDate = DateTime.Parse(this.Model.EffectiveDate);

            if (enrollment != null)
            {
                UserDefStatusCode newStatusCode = statusCodeList.FirstOrDefault(x => x.ID == this.Model.StatusCodeId);

                this.LastStatusChangeHistory = statusChangeHistory.Where(x => x.DateOfChange <= DateTime.Parse(this.Model.EffectiveDate))
                        .OrderByDescending(x => x.ModifiedDate)
                        .FirstOrDefault();

                foreach (var statusChange in statusChangeHistory.Where(x => x.DateOfChange == effectiveDate))
                {
                    statusChange.AddMillisecondToDateOfChange();
                }

                Guid newStatusChangeId = Guid.NewGuid();
                SystemStatusChangeHistory newStatusChangeHistory = new SystemStatusChangeHistory(
                    newStatusChangeId,
                    enrollment,
                    enrollment.CampusObj,
                    this.LastStatusChangeHistory != null ? this.LastStatusChangeHistory.NewStatus : null,
                    newStatusCode,
                    DateTime.Parse(this.Model.DateOfChange),
                    this.Model.UserName,
                    DateTime.Parse(this.Model.EffectiveDate),
                    this.Model.CaseNumber,
                    this.Model.RequestedBy,
                    null,
                    null);

                statusChangeHistory.Add(newStatusChangeHistory);

                List<SystemStatusChangeHistory> sortedStatusChangeHistory = statusChangeHistory.OrderBy(x => x.DateOfChange).ThenBy(x => x.ModifiedDate).ToList();

                if (sortedStatusChangeHistory.IndexOf(newStatusChangeHistory) == 0)
                {
                    this.Model.AllowInsert = true;
                    response.Data = this.Model;
                }
                else
                {
                    if (sortedStatusChangeHistory.IndexOf(newStatusChangeHistory) == 2
                        && sortedStatusChangeHistory.Count == 3
                        && (sortedStatusChangeHistory[1].NewStatus != null && sortedStatusChangeHistory[1].NewStatus.SystemStatus.ID
                        == (int)Enumerations.eSystemStatus.CurrentlyAttending))
                    {
                       
                        this.Model.AllowInsert = true;
                        this.Model.ShouldConfirmAction = true;
                        this.Model.HaveAttendancePosted =
                            this.StudentAttendance.HaveAttendanceOrGradesPosted(Guid.Parse(this.Model.StudentEnrollmentId));
                        response.Data = this.Model;
                    }
                    else
                    {
                        
                        this.Model.HaveAttendancePosted =
                            this.StudentAttendance.HaveAttendanceOrGradesPosted(Guid.Parse(this.Model.StudentEnrollmentId));
                        if (this.Model.HaveAttendancePosted)
                        {
                            this.Model.AllowInsert = false;
                            response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                            response.ResponseMessage =
                                "The change status action was not performed for the student. Please remove the attendance and/or grades before changing their status.";
                        }
                        else
                        {
                            this.Model.AllowInsert = true;
                        }
                        response.Data = this.Model;
                    }
                }
            }

            return response;
        }
    }
}
