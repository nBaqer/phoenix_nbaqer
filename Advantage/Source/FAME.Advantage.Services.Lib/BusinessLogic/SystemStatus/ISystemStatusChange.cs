﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISystemStatusChange.cs" company="FAME Inc.">
// FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus 
// </copyright>
// <summary>
//   Defines the ISystemStatusChange type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus
{
    using FAME.Advantage.Messages.SystemStuff.SystemStatuses;
    using FAME.Advantage.Messages.Web;

    /// <summary>
    /// Interface for System Status Changes Types
    /// </summary>
    public interface ISystemStatusChange
    {
        /// <summary>
        /// Inserts a new System Status Changes Type
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        Response<SystemStatusChangeModel> Insert();

        /// <summary>
        /// Delete a System Status Changes Type
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        Response<SystemStatusChangeModel> Delete();

        /// <summary>
        /// Update a System Status Changes Type
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        Response<SystemStatusChangeModel> Update();

        /// <summary>
        /// Validate the required fields and global business object validations for CRUD Operations.
        /// </summary>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        Response<SystemStatusChangeModel> ValidateStatusChange();
    }
}
