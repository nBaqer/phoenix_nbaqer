﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemStatusChange.cs" company="FAME Inc.">
// FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus  
// </copyright>
// <summary>
//   Defines the Dropped type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;

    using AutoMapper;

    using FAME.Advantage.Domain.Campuses;
    using FAME.Advantage.Domain.Enumerations;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.StatusesOptions;
    using FAME.Advantage.Domain.Student.Enrollments;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Domain.Users;
    using FAME.Advantage.Messages.SystemStuff.SystemStatuses;
    using FAME.Advantage.Messages.Web;
    using FAME.Advantage.Services.Lib.BusinessLogic.Students.Academic;
    using FAME.Advantage.Services.Lib.Infrastructure.Extensions;

    using NHibernate.Util;

    /// <summary>
    /// Business Object for handling Validations and Insert/Delete operations of System Status Change History (Edits are not implemented)
    /// </summary>
    public abstract class SystemStatusChange
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemStatusChange"/> class. 
        /// </summary>
        /// <param name="systemStatusChangeModel">
        /// The Input Model for the System Status Change
        /// </param>
        /// <param name="repository">
        /// The nHibernate Repository for Guids
        /// </param>
        /// <param name="repositoryWithInt">
        /// The nHibernate Repository for integers
        /// </param>
        protected SystemStatusChange(SystemStatusChangeModel systemStatusChangeModel, IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.Model = systemStatusChangeModel;
            this.Repository = repository;
            this.RepositoryWithInt = repositoryWithInt;
            if (this.Model != null)
            {
                this.StudentAttendance = new StudentAttendanceBo(this.Repository, this.RepositoryWithInt);
            }
        }

        /// <summary>
        /// Gets the Repository
        /// </summary>
        protected IRepository Repository { get; private set; }

        /// <summary>
        /// Gets the Repository
        /// </summary>
        protected IRepositoryWithTypedID<int> RepositoryWithInt { get; private set; }

        /// <summary>
        /// Gets the Model provided in the constructor
        /// </summary>
        protected SystemStatusChangeModel Model { get; private set; }

        /// <summary>
        /// Gets the student attendance. This object is null if this.Model is null.
        /// </summary>
        protected StudentAttendanceBo StudentAttendance { get; private set; }

        /// <summary>
        /// The validate change.
        /// </summary>
        /// <returns>
        /// The <see cref="Response&lt;SystemStatusChangeModel&gt;"/>.
        /// </returns>
        protected Response<SystemStatusChangeModel> ValidateChange()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;
            switch ((Common.Enumerations.eValidationMode)this.Model.ValidationMode)
            {
                case Common.Enumerations.eValidationMode.Insert:
                    {
                        response = this.ValidateInsert();

                        if (response.ResponseCode == (int)HttpStatusCode.OK)
                        {
                            response = this.ValidateEffectiveDate();
                        }

                        break;
                    }

                case Common.Enumerations.eValidationMode.Edit:
                    {
                        response = this.ValidateEdit();
                        break;
                    }

                case Common.Enumerations.eValidationMode.Delete:
                    {
                        response = this.ValidateDelete();
                        break;
                    }
            }

            return response;
        }

        /// <summary>
        /// The get last date of attendance.
        /// </summary>
        /// <returns>
        /// The <see cref="DateTime?"/>.
        /// </returns>
        protected DateTime? GetLastDateOfAttendance()
        {
            if (this.StudentAttendance == null)
            {
                this.StudentAttendance = new StudentAttendanceBo(this.Repository, this.RepositoryWithInt);
            }

            return this.StudentAttendance.GetLastDateAttended(Guid.Parse(this.Model.StudentEnrollmentId));
        }

        /// <summary>
        /// The update enrollment status to no start.
        /// </summary>
        /// <returns>
        /// The <see cref="Response&lt;Enrollment&gt;"/>.
        /// </returns>
        protected Response<Enrollment> UpdateEnrollmentStatusNoStart()
        {
            Response<Enrollment> response = new Response<Enrollment>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(
                    n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));

            if (enrollment != null)
            {
                var enrollmentStatus = this.Repository.Query<UserDefStatusCode>().FirstOrDefault(x => x.ID == this.Model.StatusCodeId);
                enrollment.Update(null, null, enrollmentStatus, DateTime.Now, this.Model.UserName);
                ////this.Repository.Update(enrollment);
                response.Data = enrollment;
            }
            else
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Invalid Enrollment!";
            }

            return response;
        }

        /// <summary>
        /// The update enrollment status to Currently Attending 
        /// </summary>
        /// <returns>
        /// The <see cref="Response&lt;Enrollment&gt;"/>
        /// </returns>
        protected Response<Enrollment> UpdateEnrollmentStatus()
        {
            Response<Enrollment> response = new Response<Enrollment>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(
                    n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));

            if (enrollment != null)
            {
                var enrollmentStatus = this.Repository.Query<UserDefStatusCode>().FirstOrDefault(x => x.ID == this.Model.StatusCodeId);
                enrollment.Update(enrollmentStatus, DateTime.Now, this.Model.UserName);
                ////this.Repository.Update(enrollment);
                response.Data = enrollment;
            }
            else
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Invalid Enrollment!";
            }

            return response;
        }

        /// <summary>
        /// The update enrollment status to Currently Attending 
        /// </summary>
        /// <returns>
        /// The <see cref="Response&lt;Enrollment&gt;"/>
        /// </returns>
        protected Response<Enrollment> UpdateEnrollmentStatusCurrentlyAttendending()
        {
            Response<Enrollment> response = new Response<Enrollment>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(
                    n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));

            if (enrollment != null)
            {
                var enrollmentStatus = this.Repository.Query<UserDefStatusCode>().FirstOrDefault(x => x.ID == this.Model.StatusCodeId);
                enrollment.Update(null, null, null, enrollmentStatus, DateTime.Now, this.Model.UserName);
                ////this.Repository.Update(enrollment);
                response.Data = enrollment;
            }
            else
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Invalid Enrollment!";
            }

            return response;
        }

        /// <summary>
        /// The update enrollment status and commit repository.
        /// Updates Enrollment Status Code, Date Modified, Mode Date
        /// </summary>
        /// <returns>
        /// The <see cref="Response&lt;SystemStatusChangeModel&gt;"/>
        /// </returns>
        protected Response<SystemStatusChangeModel> UpdateEnrollmentStatusAndFlush()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(
                    n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));

            if (enrollment != null)
            {
                var enrollmentStatus = this.Repository.Query<UserDefStatusCode>().FirstOrDefault(x => x.ID == this.Model.StatusCodeId);
                enrollment.Update(enrollmentStatus, DateTime.Now, this.Model.UserName);

                ////this.Repository.UpdateAndFlush(enrollment);
            }
            else
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Invalid Enrollment!";
            }

            return response;
        }


        /// <summary>
        /// The update enrollment status to Currently Attending 
        /// </summary>
        /// <returns>
        /// The <see cref="Response&lt;SystemStatusChangeModel&gt;"/>
        /// </returns>
        protected Response<SystemStatusChangeModel> UpdateEnrollmentStatusToDropped()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(
                    n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));

            if (enrollment != null)
            {
                this.Model.StudentId = enrollment.StudentId;

                var enrollmentStatus = this.Repository.Query<UserDefStatusCode>().FirstOrDefault(x => x.ID == this.Model.StatusCodeId);

                var dropReason = this.Repository.Query<DropReason>().FirstOrDefault(x => x.ID == Guid.Parse(this.Model.Reason));
                if (dropReason != null)
                {
                    var lda = this.StudentAttendance.GetLastDateAttended(enrollment);

                    enrollment.Update(
                        DateTime.Parse(this.Model.EffectiveDate),
                        lda,
                        dropReason.ID,
                        enrollmentStatus,
                        DateTime.Now,
                        this.Model.UserName,
                        null);

                    ////this.Repository.Update(enrollment);
                }
                else
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Drop Reason not found!";
                }
            }
            else
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Invalid Enrollment!";
            }

            return response;
        }

        /// <summary>
        /// The inactivate student base on enrollments.
        /// </summary>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        protected Response<SystemStatusChangeModel> InactivateStudentBaseOnEnrollments()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            int activeStudentEnrollments =
                                this.Repository.Query<Enrollment>()
                                    .Count(
                                        x =>
                                        x.StudentId == this.Model.StudentId
                                        && (x.EnrollmentStatus.SystemStatus.ID
                                            != (int)Enumerations.eSystemStatus.Dropped
                                            || x.EnrollmentStatus.SystemStatus.ID
                                            != (int)Enumerations.eSystemStatus.Graduated));

            if (activeStudentEnrollments == 0)
            {
                this.UpdateStudentStatus(Enumerations.EStatus.Inactive);
            }

            return response;
        }

        /// <summary>
        /// Update the Student Enrollment and set the Re-Enroll Date and Date Determined. 
        /// </summary>
        /// <returns>
        /// The <see cref="Response&lt;Enrollment&gt;"/>.
        /// </returns>
        protected Response<Enrollment> UpdateEnrollmentFutureStart(bool shouldCommitChanges = false)
        {
            Response<Enrollment> response = new Response<Enrollment>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            Enrollment enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(
                    n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));

            if (enrollment != null)
            {
                var enrollmentStatus = this.Repository.Query<UserDefStatusCode>().FirstOrDefault(x => x.ID == this.Model.StatusCodeId);

                if (this.Model.PreviousStatusId == (int)Enumerations.eSystemStatus.CurrentlyAttending)
                {
                    enrollment.Update(null, null, enrollmentStatus, DateTime.Now, this.Model.UserName, DateTime.Parse(this.Model.EffectiveDate));
                }
                else
                {
                    enrollment.Update(null, null, null, enrollmentStatus, DateTime.Now, this.Model.UserName, DateTime.Parse(this.Model.EffectiveDate), DateTime.Parse(this.Model.EffectiveDate));
                }

                if (shouldCommitChanges)
                {
                    this.Repository.Update(enrollment);
                }
                response.Data = enrollment;
            }
            else
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Invalid Enrollment!";
            }

            return response;
        }

        /// <summary>
        /// The is status change top sequence.
        /// Returns true if the given status effective date is at the top of the sequence of status history
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        protected bool IsStatusChangeTopSequence()
        {
            List<SystemStatusChangeHistory> statusChangeHistory = null;
            statusChangeHistory = this.Repository.Query<SystemStatusChangeHistory>().Where(x => x.Enrollment.ID == Guid.Parse(this.Model.StudentEnrollmentId)).OrderBy(x => x.DateOfChange).ToList();

            if (statusChangeHistory.Count > 0)
            {
                var sequence =
                    statusChangeHistory.Count(
                        x => x.DateOfChange > DateTime.Parse(this.Model.EffectiveDate));
                if (sequence == 0)
                {
                    return true;
                }

                return false;
            }

            return true;
        }

        /// <summary>
        /// The is not first instance with attendance.
        /// </summary>
        /// <returns>
        /// The <see cref="Response&lt;Enrollment&gt;"/>.
        /// </returns>
        protected Response<SystemStatusChangeModel> IsNotFirstInstanceWithAttendance()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel> { ResponseCode = (int)HttpStatusCode.OK };
            var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));
            if (enrollment != null && this.Model.StatusChangeHistoryId != null)
            {
                var history = this.Repository.Query<SystemStatusChangeHistory>().Where(x => x.Enrollment.ID == enrollment.ID).ToList();
                SystemStatusChangeHistory statusToDelete = history.FirstOrDefault(x => x.ID == this.Model.StatusChangeHistoryId.Value);

                if (statusToDelete != null)
                {
                    if (statusToDelete.NewStatus != null)
                    {
                        List<SystemStatusChangeHistory> statusHistory = history.Where(x => x.NewStatus != null && x.NewStatus.SystemStatus.ID == this.Model.StatusId).OrderBy(x => x.DateOfChange).ThenBy(x => x.ModifiedDate).ToList();
                        if (statusHistory.IndexOf(statusToDelete) == 0)
                        {
                            if (statusHistory.Count > 1)
                            {
                                if (statusHistory[0].DateOfChange != statusHistory[1].DateOfChange
                                    || statusHistory[0].ModifiedDate != statusHistory[1].ModifiedDate)
                                {
                                    bool haveAttendance = this.StudentAttendance.HaveAttendanceOrGradesPosted(Guid.Parse(this.Model.StudentEnrollmentId));

                                    if (haveAttendance)
                                    {
                                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                                        response.ResponseMessage = "This status cannot be deleted. The selected student has attendance or grade posted postings for this enrollment.";
                                    }
                                }
                            }
                            else
                            {
                                bool haveAttendance = this.StudentAttendance.HaveAttendanceOrGradesPosted(Guid.Parse(this.Model.StudentEnrollmentId));

                                if (haveAttendance)
                                {
                                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                                    response.ResponseMessage = "This status cannot be deleted. The selected student has attendance or grade posted postings for this enrollment.";
                                }
                            }
                        }
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// The get status to delete.
        /// </summary>
        /// <returns>
        /// The <see cref="SystemStatusChangeHistory"/>.
        /// </returns>
        protected SystemStatusChangeHistory GetStatusToDelete()
        {
            var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));
            if (enrollment != null && this.Model.StatusChangeHistoryId != null)
            {
                var history = this.Repository.Query<SystemStatusChangeHistory>().Where(x => x.Enrollment.ID == enrollment.ID).ToList();
                SystemStatusChangeHistory statusToDelete = history.FirstOrDefault(x => x.ID == this.Model.StatusChangeHistoryId.Value);

                return statusToDelete;
            }

            return null;
        }

        /// <summary>
        /// The get previous status.
        /// </summary>
        /// <param name="systemStatusChangeHistory">
        /// The system status change history.
        /// </param>
        /// <returns>
        /// The <see cref="SystemStatusChangeHistory"/>.
        /// </returns>
        protected SystemStatusChangeHistory GetPreviousStatus(SystemStatusChangeHistory systemStatusChangeHistory)
        {
            var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));
            if (enrollment != null && this.Model.StatusChangeHistoryId != null)
            {
                var history = this.Repository.Query<SystemStatusChangeHistory>().Where(x => x.Enrollment.ID == enrollment.ID).OrderByDescending(x => x.DateOfChange).ThenByDescending(x => x.ModifiedDate).ToList();

                int previousId = history.IndexOf(systemStatusChangeHistory) + 1;

                if (previousId < history.Count)
                {
                    return history[previousId];
                }
            }

            return null;
        }


        /// <summary>
        /// Update the original status field of the student status change that is next in the sequence of status history based on the newly inserted/deleted status. This fix the order of parent/child between status changes.
        /// </summary>
        /// <returns>
        /// Response.ResponseCode = Ok (200) if success. Exception may be raised if StudentEnrollmentId, UserId, EffectiveDate or DateOfChange are null or invalid format
        /// </returns>
        protected Response<SystemStatusChangeModel> UpdateFutureSiblingStatus()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            List<SystemStatusChangeHistory> statusChangeHistory = null;
            statusChangeHistory = this.Repository.Query<SystemStatusChangeHistory>().Where(x => x.Enrollment.ID == Guid.Parse(this.Model.StudentEnrollmentId)).OrderBy(x => x.DateOfChange).ThenBy(x => x.ModifiedDate).ToList();

            if (statusChangeHistory.Count > 0)
            {
                var sequence =
                    statusChangeHistory.Where(
                        x => x.DateOfChange > DateTime.Parse(this.Model.EffectiveDate)).OrderBy(x => x.DateOfChange).ThenBy(x => x.ModifiedDate).FirstOrDefault();

                if (sequence != null)
                {
                    UserDefStatusCode statusCode = this.Repository.Query<UserDefStatusCode>().FirstOrDefault(x => x.ID == this.Model.StatusCodeId);

                    sequence.Update(statusCode, DateTime.Now, this.Model.UserName);

                    ////this.Repository.Update(sequence);
                }
            }

            return response;
        }


        /// <summary>
        ///  Update the Student Status. 
        /// </summary>
        /// <param name="isActve">Boolean indicating if Status is Active = true (<b>default</b>) or Inactive  = False</param>
        /// <returns>
        /// Response.ResponseCode = Ok (200) if success. Exception may be raised if StudentEnrollmentId, UserId, EffectiveDate or DateOfChange are null or invalid format
        /// </returns>
        protected Response<SystemStatusChangeModel> UpdateStudentStatus(Enumerations.EStatus isActve)
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(
                  n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));

            if (enrollment != null)
            {
                var enrollmentLead = this.Repository.Query<Domain.Lead.Lead>().FirstOrDefault(
                    n => n.StudentId == enrollment.StudentId);
                var status = this.Repository.Query<StatusOption>().FirstOrDefault(x => x.Status == Enumerations.GetStatus(isActve));
                if (status != null && enrollmentLead != null)
                {
                    enrollmentLead.UpdateStatus(status);
                    ////this.Repository.Update(enrollment);
                }
            }

            return response;
        }

        /// <summary>
        /// The insert status change.
        /// </summary>
        /// <returns>
        /// The <see cref="Response&lt;Enrollment&gt;"/>.
        /// </returns>
        protected Response<SystemStatusChangeModel> InsertStatusChange()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.Data = this.Model;
            response.ResponseCode = (int)HttpStatusCode.OK;

            var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(
                  n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));

            var campus = this.Repository.Query<Campus>().FirstOrDefault(x => x.ID == Guid.Parse(this.Model.CampusId));

            var requestedBy = this.Repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(this.Model.RequestedBy));

            var newStatusCode = this.Repository.Query<UserDefStatusCode>()
                          .FirstOrDefault(x => x.ID == this.Model.StatusCodeId);

            UserDefStatusCode previousStatusCode = this.Repository.Query<UserDefStatusCode>()
                          .FirstOrDefault(x => x.ID == this.Model.PreviousStatusCodeId && x.ID != Guid.Empty);

            if (enrollment != null && campus != null && newStatusCode != null &&
               requestedBy != null)
            {
                SystemStatusChangeHistory statusChangeHistory = null;
                switch ((Enumerations.eSystemStatus)this.Model.StatusId)
                {
                    case Enumerations.eSystemStatus.Dropped:
                        {
                            statusChangeHistory = new SystemStatusChangeHistory(enrollment, campus, previousStatusCode, newStatusCode, DateTime.Now, this.Model.UserName, DateTime.Parse(this.Model.EffectiveDate), this.Model.CaseNumber, requestedBy.UserName, Guid.Parse(this.Model.Reason), this.GetLastDateOfAttendance());
                            break;
                        }

                    case Enumerations.eSystemStatus.LeaveOfAbsence:
                        {
                            statusChangeHistory = new SystemStatusChangeHistory(enrollment, campus, previousStatusCode, newStatusCode, DateTime.Now, this.Model.UserName, DateTime.Parse(this.Model.EffectiveDate), this.Model.CaseNumber, requestedBy.UserName, Guid.Parse(this.Model.Reason), null);
                            break;
                        }

                    default:
                        {
                            statusChangeHistory = new SystemStatusChangeHistory(enrollment, campus, previousStatusCode, newStatusCode, DateTime.Now, this.Model.UserName, DateTime.Parse(this.Model.EffectiveDate), this.Model.CaseNumber, requestedBy.UserName, null, null);
                            statusChangeHistory.Update(this.Model.HaveBackup, this.Model.HaveClientConfirmation);
                            break;
                        }
                }

                this.Repository.Save(statusChangeHistory);
                this.Model.StatusChangeHistoryId = statusChangeHistory.ID;

            }
            else
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Invalid Data! Please verify required field!";
            }

            return response;
        }

        /// <summary>
        /// The delete status change.
        /// </summary>
        /// <returns>
        /// The <see cref="Response&lt;Enrollment&gt;"/>.
        /// </returns>
        protected Response<SystemStatusChangeModel> DeleteStatusChange()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.Data = this.Model;
            response.ResponseCode = (int)HttpStatusCode.OK;

            var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(
                  n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));

            var campus = this.Repository.Query<Campus>().FirstOrDefault(x => x.ID == Guid.Parse(this.Model.CampusId));

            var requestedBy = this.Repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(this.Model.RequestedBy));

            SystemStatusChangeHistory statusChange = this.Repository.Query<SystemStatusChangeHistory>().FirstOrDefault(x => x.Enrollment.ID == enrollment.ID && x.ID == this.Model.StatusChangeHistoryId.Value);

            var deletedReason = this.Repository.Query<StatusChangeDeleteReasons>().FirstOrDefault(x => x.ID == Guid.Parse(this.Model.Reason));

            if (enrollment != null && campus != null &&
               requestedBy != null && statusChange != null && deletedReason != null)
            {
                StatusChangesDeleted statusChangeDeleted = new StatusChangesDeleted(statusChange.Enrollment, statusChange.OriginalStatus, statusChange.NewStatus, statusChange.Campus, statusChange.ModifiedDate, statusChange.ModifiedUser, statusChange.IsReversal, statusChange.DropReasonId, statusChange.DateOfChange, statusChange.Lda, this.Model.CaseNumber, this.Model.RequestedBy, this.Model.HaveBackup, this.Model.HaveClientConfirmation, statusChange.ID);

                if (statusChange.DateOfChange != null)
                {
                    this.Model.EffectiveDate = statusChange.DateOfChange.Value.ToString();
                }

                statusChangeDeleted.Update(this.Model.UserName, deletedReason);

                this.Repository.Save(statusChangeDeleted);
                this.Repository.Delete(statusChange);

            }
            else
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Invalid Data! Please verify required field!";
            }

            return response;
        }

        /// <summary>
        /// Delete Scheduled Class, Attendance and Courses
        /// </summary>
        protected void DeleteScheduledClassCoursesAndAttendance()
        {
            Guid studentEnrollmentId = Guid.Parse(this.Model.StudentEnrollmentId);
            var query = this.Repository.Session.CreateSQLQuery("exec USP_DeleteEnrollmentAttendanceAndGradesPosted :StuEnrollId").SetParameter("StuEnrollId", studentEnrollmentId).List().First();
            if (query != null)
            {
                if (int.Parse(query.ToString()) != 1)
                {
                    throw new Exception("Unable to remove attendance and grades posted");
                }
            }
        }

        /// <summary>
        /// Delete Scheduled Class and Courses
        /// </summary>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        protected Response<SystemStatusChangeModel> DeleteScheduledClassAndCourses()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.Data = this.Model;
            response.ResponseCode = (int)HttpStatusCode.OK;
            var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(
                   n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));
            if (enrollment != null)
            {
                var results = this.Repository.Query<ArResults>().Where(n => n.StuEnrollment.ID == Guid.Parse(this.Model.StudentEnrollmentId));
                foreach (var item in results)
                {
                    this.Repository.Delete(item);
                }

                var gradeBooksResults = this.Repository.Query<GradeBookResult>().Where(n => n.Enrollment.ID == Guid.Parse(this.Model.StudentEnrollmentId));
                foreach (var item in gradeBooksResults)
                {
                    this.Repository.Delete(item);
                }
                List<CreditSummary> creditSummary = this.Repository.Query<CreditSummary>().Where(n => n.StuEnrollId == Guid.Parse(this.Model.StudentEnrollmentId)).ToList();
                foreach (var item in creditSummary)
                {
                    this.Repository.Delete(item);
                }
            }
            else
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Invalid Data! Please verify required field!";
            }

            return response;
        }

        /// <summary>
        /// Deletes the Future Scheduled Results to clear out classes seat
        /// </summary>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        protected Response<SystemStatusChangeModel> DeleteFutureScheduledResults()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.Data = this.Model;
            response.ResponseCode = (int)HttpStatusCode.OK;
            var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(
                   n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));
            if (enrollment != null)
            {
                var results = this.Repository.Query<ArResults>().Where(n => n.StuEnrollment.ID == Guid.Parse(this.Model.StudentEnrollmentId) && n.ClassSections.StartDate > DateTime.Now);
                foreach (var item in results)
                {
                    this.Repository.Delete(item);
                }
            }
            else
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Invalid Data! Please verify required field!";
            }

            return response;
        }

        /// <summary>
        /// The validate insert.
        /// </summary>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        private Response<SystemStatusChangeModel> ValidateInsert()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;
            bool isSupport = false;
            if (this.Model == null)
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Model can't be null";
            }
            else
            {
                if (this.Model.StatusCodeId.IsEmpty())
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "New Status is a required Field!";
                }
                else if (string.IsNullOrEmpty(this.Model.CampusId))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Campus is a required Field!";
                }
                else if (string.IsNullOrEmpty(this.Model.UserId))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "User is a required Field!";
                }
                else if (string.IsNullOrEmpty(this.Model.DateOfChange))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Date of change is a required Field!";
                }
                else if (string.IsNullOrEmpty(this.Model.EffectiveDate))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Effective Date is a required Field!";
                }
                else if (string.IsNullOrEmpty(this.Model.StudentEnrollmentId))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Student Enrollment is a required Field!";
                }
                else if (string.IsNullOrEmpty(this.Model.UserId))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "User is a required Field!";
                }
                else
                {
                    var user = this.IsSupport();
                    isSupport = user.IsAdvantageSuperUser;
                    this.Model.UserName = user.UserName;
                }

                if (isSupport && this.Model.IsSupportTool)
                {
                    if (string.IsNullOrEmpty(this.Model.CaseNumber))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Case Number is a required Field!";
                    }

                    if (string.IsNullOrEmpty(this.Model.RequestedBy))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Requested By is a required Field!";
                    }
                }

                DateTime? effectiveDate = null;

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    Guid campusId, studentEnrollmentId, userId;
                    DateTime dateOfChange, effectiveDateTime;

                    if (!Guid.TryParse(this.Model.CampusId, out campusId))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Invalid format for Campus field!";
                    }
                    else if (!Guid.TryParse(this.Model.StudentEnrollmentId, out studentEnrollmentId))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Invalid format for Student Enrollment field!";
                    }
                    else if (!Guid.TryParse(this.Model.UserId, out userId))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Invalid format for User field!";
                    }
                    else if (!DateTime.TryParse(this.Model.DateOfChange, out dateOfChange))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Invalid format for Date of Change field!";
                    }
                    else if (!DateTime.TryParse(this.Model.EffectiveDate, out effectiveDateTime))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Invalid format for Effective Date field!";
                    }
                    else
                    {
                        effectiveDate = effectiveDateTime;
                    }

                    if (isSupport)
                    {
                        Guid requestedBy;
                        if (!Guid.TryParse(this.Model.RequestedBy, out requestedBy))
                        {
                            response.ResponseCode = (int)HttpStatusCode.BadRequest;
                            response.ResponseMessage = "Invalid format for Requested by field!";
                        }
                    }
                }

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    if (this.Model.StatusId == (int)Enumerations.eSystemStatus.Suspension
                        || this.Model.StatusId == (int)Enumerations.eSystemStatus.LeaveOfAbsence
                        || this.Model.StatusId == (int)Enumerations.eSystemStatus.Dropped)
                    {
                        if (string.IsNullOrEmpty(this.Model.Reason))
                        {
                            response.ResponseCode = (int)HttpStatusCode.BadRequest;
                            response.ResponseMessage = "Reason is a required field!";
                        }
                        else
                        {
                            if (this.Model.StatusId == (int)Enumerations.eSystemStatus.Suspension
                                || this.Model.StatusId == (int)Enumerations.eSystemStatus.LeaveOfAbsence)
                            {
                                if (string.IsNullOrEmpty(this.Model.EndDate))
                                {
                                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                                    response.ResponseMessage = "End Date is a required field!";
                                }
                                else
                                {
                                    DateTime endDate;
                                    if (!DateTime.TryParse(this.Model.EndDate, out endDate))
                                    {
                                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                                        response.ResponseMessage = "End Date have an invalid format!";
                                    }
                                    else
                                    {
                                        if (endDate < effectiveDate)
                                        {
                                            response.ResponseCode = (int)HttpStatusCode.BadRequest;
                                            response.ResponseMessage = "End Date must be higher than Effective Date!";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// The validate edit.
        /// </summary>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        private Response<SystemStatusChangeModel> ValidateEdit()
        {
            // TODO
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.BadRequest;
            response.ResponseMessage = "Method not implemented yet!";
            return response;
        }

        /// <summary>
        /// The validate delete.
        /// </summary>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        private Response<SystemStatusChangeModel> ValidateDelete()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;
            if (this.Model == null)
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Model can't be null";
            }
            else
            {

                var user = this.IsSupport();
                bool isSupport = user.IsAdvantageSuperUser;
                this.Model.UserName = user.UserName;

                if (isSupport && this.Model.IsSupportTool)
                {
                    if (this.Model.StatusChangeHistoryId == null)
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Status Change History Id is a required Field!";
                    }
                    else if (this.Model.StatusChangeHistoryId.Value == Guid.Empty)
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Status Change History Id is a required Field!";
                    }
                    else if (string.IsNullOrEmpty(this.Model.CampusId))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Campus is a required Field!";
                    }
                    else if (string.IsNullOrEmpty(this.Model.UserId))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "User is a required Field!";
                    }
                    else if (string.IsNullOrEmpty(this.Model.DateOfChange))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Date of change is a required Field!";
                    }
                    else if (string.IsNullOrEmpty(this.Model.StudentEnrollmentId))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Student Enrollment is a required Field!";
                    }
                    else if (string.IsNullOrEmpty(this.Model.UserId))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "User is a required Field!";
                    }
                    else if (string.IsNullOrEmpty(this.Model.CaseNumber))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Case Number is a required Field!";
                    }
                    else if (string.IsNullOrEmpty(this.Model.RequestedBy))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Requested By is a required Field!";
                    }
                    else
                    {
                        Guid campusId, studentEnrollmentId, userId;
                        DateTime dateOfChange;

                        Guid requestedBy;
                        if (!Guid.TryParse(this.Model.CampusId, out campusId))
                        {
                            response.ResponseCode = (int)HttpStatusCode.BadRequest;
                            response.ResponseMessage = "Invalid format for Campus field!";
                        }
                        else if (!Guid.TryParse(this.Model.StudentEnrollmentId, out studentEnrollmentId))
                        {
                            response.ResponseCode = (int)HttpStatusCode.BadRequest;
                            response.ResponseMessage = "Invalid format for Student Enrollment field!";
                        }
                        else if (!Guid.TryParse(this.Model.UserId, out userId))
                        {
                            response.ResponseCode = (int)HttpStatusCode.BadRequest;
                            response.ResponseMessage = "Invalid format for User field!";
                        }
                        else if (!DateTime.TryParse(this.Model.DateOfChange, out dateOfChange))
                        {
                            response.ResponseCode = (int)HttpStatusCode.BadRequest;
                            response.ResponseMessage = "Invalid format for Date of Change field!";
                        }
                        else if (!Guid.TryParse(this.Model.RequestedBy, out requestedBy))
                        {
                            response.ResponseCode = (int)HttpStatusCode.BadRequest;
                            response.ResponseMessage = "Invalid format for Requested by field!";
                        }
                    }
                }
                else
                {
                    response.ResponseCode = (int)HttpStatusCode.Forbidden;
                    response.ResponseMessage = "Invalid request!";
                }
            }

            return response;
        }

        /// <summary>
        /// The is support.
        /// </summary>
        /// <returns>
        /// The <see cref="User"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Invalid User when user is not found based on the Model.UserId
        /// </exception>
        private User IsSupport()
        {
            var user = this.Repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(this.Model.UserId));

            if (user == null)
            {
                throw new Exception("Invalid User");
            }

            return user;
        }

        /// <summary>
        /// Effective date can’t be before Student Enrollment Start Date
        /// Effective date can’t be higher than the Student Enrollment Graduation Date
        /// </summary>
        /// <returns>
        /// Response.ResponseCode = Ok (200) if validation pass, otherwise Response.ResponseCode = BadRequest(=400) and the reason of the failure is returned in Response.ResponseMessage
        /// </returns>
        private Response<SystemStatusChangeModel> ValidateEffectiveDate()
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            var enrollment = this.Repository.Query<Enrollment>().FirstOrDefault(
                          n => n.ID == Guid.Parse(this.Model.StudentEnrollmentId));

            if (enrollment != null)
            {

                if (enrollment.StartDate.HasValue)
                {
                    if (enrollment.StartDate.Value > DateTime.Parse(this.Model.EffectiveDate) && this.Model.StatusId != (int)Enumerations.eSystemStatus.FutureStart)
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "The selected effective date cannot be before the Student Enrollment Start Date!";
                    }
                }

                if (enrollment.ExpectedGraduationDate.HasValue)
                {
                    if (enrollment.ExpectedGraduationDate.Value < DateTime.Parse(this.Model.EffectiveDate))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "The selected effective date cannot be higher than the Student Enrollment Graduation Date!";
                    }
                }
            }
            else
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Invalid Enrollment!";
            }

            return response;
        }

    }
}
