﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmployeeBo.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   The employee bo.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.Employees
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using AutoMapper;

    using Domain.Employee;
    using Domain.Infrastructure.Entities;
    using Domain.MostRecentlyUsed;
    using Domain.SystemStuff.ConfigurationAppSettings;
    using Domain.Users;
    using Messages.Employee;
    using Messages.MostRecentlyUsed;
    using Services.Lib.Infrastructure.Extensions;

    /// <summary>
    /// The employee bo.
    /// </summary>
    public class EmployeeBo
    {
        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        ///  The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        private readonly string _studentImagePath = null;
        private readonly string _advantageSiteUrl = null;

        const int EmployeeMruTypeId = (int)Messages.Enumerations.Enumerations.MRuTypes.Employees;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="repositoryWithInt"></param>
        public EmployeeBo(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            this.repositoryWithInt = repositoryWithInt;
            
        }

        /// <summary>
        /// Get the Students based on the search term passed in from the universal search
        /// </summary>
        /// <param name="searchTermInit">
        /// The search Term Initialization.
        /// </param>
        /// <returns>
        /// The employee search
        /// </returns>
        public IEnumerable<EmployeeSearchOutputModel> GetemployeeSearch(string[] searchTermInit)
        {
            var searchTerm = searchTermInit[0];

            if (string.IsNullOrEmpty(searchTerm))
            {
                return null;
            }

            var campusesList = new List<Guid>();

            if (searchTermInit.Count() > 1)
            {
                var list = searchTermInit[1].ToString(CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(list))
                {
                    campusesList = list.Split(',').Select(Guid.Parse).ToList();
                }
            }

            // get the employees using the search term
            var employees =
                this.repository.Query<Employee>()
                    .Where(
                        n =>
                            ((n.LastName ?? string.Empty) + " " + (n.FirstName ?? string.Empty) + " "
                             + (n.LastName ?? string.Empty) + " " + (n.Address ?? string.Empty) + " "
                             + (n.City ?? string.Empty) + (n.State.Description ?? string.Empty) + " "
                             + (n.Zip ?? string.Empty)).Contains(searchTerm))
                    .Take(100);

            // var empWithDepartments =_repository.Query<FAME.Advantage.Domain.Employee.Employee>();

            // .Where(n => n.EmployeeInfo.Any(r => r.Department.Description.Contains(searchTerm))).Take(100);
            var finalEmployees = new List<Employee>();

            finalEmployees.AddRange(employees);

            // finalEmployees.AddRange(empWithDepartments);
            finalEmployees = finalEmployees.Distinct().ToList();

            var finalEmployees2 = campusesList.Count > 0
                                      ? (from s in finalEmployees where campusesList.Contains(s.Campus.ID) select s)
                                          .ToList()
                                      : finalEmployees;

            finalEmployees2.ForEach(n => n.GetCampusStringList());

            // return the employee list
            return
                Mapper.Map<IEnumerable<EmployeeSearchOutputModel>>(
                    finalEmployees2.Distinct().OrderBy(n => n.LastName).ThenBy(n => n.FirstName).ToArray());
        }

        /// <summary>
        ///  The get employee MRU.
        /// </summary>
        /// <param name="userId">
        ///  The user id.
        /// </param>
        /// <param name="campusId">
        ///  The campus id.
        /// </param>
        /// <returns>
        /// The EmployeeMruOutputModel list.
        /// </returns>
        public IEnumerable<EmployeeMruOutputModel> GetEmployeeMru(Guid userId, Guid campusId)
        {

            var mrus = this.repository.Query<Mru>().Where(n => n.MruTypeObj.ID == EmployeeBo.EmployeeMruTypeId && n.UserObj.ID == userId).OrderByDescending(n => n.ModDate);
            var mruGuids = mrus.Select(m => m.EntityId).ToList();
            var mruList = mrus.Select(l => new MruOutput { EntityId = l.EntityId, ModDate = l.ModDate }).ToList();

            if (mrus.Any())
            {
                var employees = this.repository.Query<Employee>().Where(n => mruGuids.Contains(n.ID))
                    .ForEach(r => r.GetCampusStringList());

                return this.FillMruOutput(employees, mruList);
            }
            else
            {
                var campusIdList = this.GetCampusItemsByUserId(userId);

                var st2 = this.repository.Query<Employee>().Where(n => campusIdList.Contains(n.Campus.ID))
                    .OrderByDescending(n => n.ModDate)
                        .ForEach(r => r.GetCampusStringList());


                return this.FillMruOutput(st2, null);
            }
        }



        private IEnumerable<EmployeeMruOutputModel> FillMruOutput(IEnumerable<Employee> employees, IEnumerable<MruOutput> mruList)
        {
            var mruOutput = new List<EmployeeMruOutputModel>();

            foreach (var s in employees)
            {
                var s1 = s;
                if (mruOutput.All(n => n.Id != s1.ID))
                {
                    EmployeeContact cont = null;
                    if ( s1.EmpContacts != null)
                        cont = s1.EmpContacts.FirstOrDefault();

                    // Note: until now the web page HRInfo work for only one record for each employee (hrEmployees [1] to hrEmployees [0,1])  
                    string depDescription = string.Empty;
                    if (s1.EmployeeInfo.Count() > 0)
                    {
                        if (s1.EmployeeInfo.FirstOrDefault(n => n.Department != null) != null)
                        {
                            depDescription = s1.EmployeeInfo.FirstOrDefault(n => n.Department != null).Department.Description; 
                        }
                    }

                    var mru = new EmployeeMruOutputModel
                    {
                        Id = s1.ID,
                        FullName = s1.FullName,
                        Address = s1.Address ?? string.Empty,
                        City = s1.City ?? string.Empty,
                        State = s1.State != null ? s1.State.Code : string.Empty,
                        Zip = s1.Zip ?? string.Empty,
                        CityStateZip = string.Empty,
                        WorkPhone = cont != null ? cont.WorkPhone.FormatPhoneNumber() : string.Empty,
                        Image64 = @"data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAEsAAABLCAIAAAC3LO29AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAiESURBVHhe3Vv3V1RHFLa32GJijJrkaIrR5KScREVBRFFEooKFqLGXKL1JU0BEDKIICIoiAgIqKiogAoKgossiUpe6LM0FFlg6WP6AfGZPzJ5F3PfmzS5Gzh5+ujPvfnP7nTsDO5+9HPB+/wHh+/0boGp4HT1vPsGOnhctnc/wX9UMqBBhe8+Lgor6a2l5Men5d7JKc8qe5gnrHuSJkvklmYJqUV1LZOLjlOyy9BxhWW0TiFUEVVUIxc0daU+E+4Pil5ifXGoRsMYxeI/XJVPvK5tcw1c7BG/3iHQ+Faf9p6+xc8iGA6EOATeDYx/eflQsFDdTh0oZYcezl61dz7KKa0Nv8dcfCJ1i4DRgtonS3xANs0nLHJaaB/heSq9qaG3rfk5RnjQRwqia2rtzy8VG9mc/WrJv0BxTpdjkCQbNNf1U3zEsgV9Y2dDY1k3LRGkiBFup2eU6Jn4jtSwHMhBdb/yD5piMWmAFYd5Iz6+XdrZ3UzBOOghx3tmlT12DE37Y6AkWB7KUnjxUrP1A23raKhc9i0C/6HTg5KixFBC2db24eb9gq/uF6YauQ+eZs9LMvoiBc7S2tbFTCDxwPyOE64t9UGjsfO4TPQcq2OQ3+Xad+9ELd/4Jm+RpCScZAh78+ya3sI+X2lOHhw1Halkh0lTWt3AJIZwQSjt6whP4X692UwU82Z4zjQ8l8UsQgZjrqoLAyRHC/MpqmgysT43VsVEdwknLHO38r8NLM0eoQPkGhAyVvrS2yfpEzJiF1gPnKI/pxEcweqH1ErOTdc0d5AgZ4lH4gKS168a9gikGzmzDOluocM5QVCQ6xAkAoZaW1DR6hiYhC2HLMVt6fGLyciekgdLOHjIxEmopigNkz2zZJaMfv9g2OiW3TkqoqIQI72SVwTzIOGa7Cqboe/lVRk5Nhkw2unG/4PsNh9nySkaPJM4jNFFUJ2XCWG8aQjuMTs35fMV+Mo7ZrkIev/vIxeKqRhn3bF0jIcKrd/O+NFJhoJc/BbjTBbt9npQSJqiECBN5xZq7fNhKg4x+2HwLfctTeeVitWppyuOyperyNMM1LTfsDxWI6tWKELmizh5fMpmwXTVC03L7ociiyga1Ikx4VDRv5zG2vJLRj9Cy3OUJTyNRK8LEzGKt3WqyQ8hwm3uEoFK9CNH/1DXzJ5MJ21XD51ugtYXWq1plmJZTsdLuDFteyegRLZA/5ZSp15fyCqu3HLzApePEHC1kuM7pXIGcL2UV9AnjYXVDa1g8f+Y6dzRzmfNKQInic4yOjXdECnoZatVSdDIFogbTo9Eo8FVaAQ+bbz5jnfvDgsrmDnrVE8OjamjpissQ/LrVC3kjgXAYLhm3yHat4zkuXVNCLZWdAnr47iG3pxo4M2SXgGzqb86eYclNdPs0DGUIstau52jjz/rdg4B1hktmrD0Ye78AJsicK+WdKOZ7oXdSLWlD6B+iQafVrQB7hKbFYlP/2sa29v7qCL8SY+dzhA3cNDGUCSsyuLE1DsEc72c42aFM2ufjM3/Z4kW96TZBd5/WLp/DoUnMdeqNlBQQltY02gfcnG7kyko+byeGf15lFxQSy+MJqvofIThIzxUa7w8ZNJda9Id/9o9OJ+6Ryh8KBRliu+LqRrNj0bSu1iDen/74K/w2n6P0ZMvpIIzLKFxuFUh27/tGddU19UdPvR8Q9pXyImcMup7x8+YjVDK46YYuFj5XM/Ir+wFhX5/ECEb502aA1Njpzd3foJK4lpaPMNj/CBVEiss276hUtKiJIwfKMSQPx6NScS/CMQy+Ph1OdqiAEEnco8KqHzd6Ersc2U0TGnnElURvsXNC2Hu74moJRp7Iqg3YMCoJx8DYCtIGvqoivvy+RVUSdDfIEA7WMJtu5JZbXocrGCoWSDNavGZIUNmwzCoQ3TECfzNmoY2eZYCklesADc3aovdJoyOms9cPnRUChEjfN7uFU7RAFjJk3vnJF9bN3uo9jGhuaKKew16vS5jvoKii1HIaGU9II3FDNMv4EFl7CkM5Ow9HYvzy3UWICMYrrPrMwHkwUQo+dpHNCtvTaP+8uwhxSZuSVTZa24Ys4g+db/7deo9coZhLz4JdPGRufrJ9MRcccTuLwMe8XjJOx+ZIWBISQIYJDRMOaUb8rOKabYciuCBEhTlRz/5Y5F2MI9HSVWoIRfVSn4t3J+szGnt+yynAhjEhgKuY83GZQrEUOT1HqBQQQlXKxdKTV+7N3e49mNIMEWZo0PvZeTgqJI5Xgn5eF/nkN1eEbd0v7ueJ3IITAA/X0VxUVGEtpqE+1LXDtiZHL5+OeZCWUy6qbyFwQuQIca6Y4onPEOCkv1h5gMx/Kj0R1FPIkDBKu9ElFM3vyKTHSZklPEE1EmCM8zGRLQlCVEm1je2ZRdWnYjJwxhjsVsooFQLUVnjeoLnr+Ca3cNezCfDbeNKRJxRXiKWYJOyrbcUOoewpD84v6PrDRXv9yCI7FbSyTSbo2i3cc8L5dOytR0U1kjbkQ5i1xUCxvH9ihxAzz5ifX2zqN0HXHmMuFHkl2wqmATYw8z5luZPGjmPWvjFRydl5FXUSuecayhHiBQvsLYFX5HImfpllwFerXbGjem5/mcOGW0JROknf8Zu1B2E4hvvOHAiKT+SXII/vEyGyikJRA95leZxP3HwwXHvPCbTARi2wVJFHYQ5GKSVsBzKYZuiibxUIBVZECO8EhU7ml/pH34Ob1jU7iVcUZBWtUlZUTTBc0wJTzP8hhNBwO383u9zvcjp6LZjMVdGdmaqBKe4vy4lQWRdVSs7F8gysT6vN+6sJKuDhXUGmoAYznKiy1fRVondfhLwBYXRKzmIz//GLbPs9vhFiePt5IbWdt+M4+lzvWgCgh3br0f+pq2R6BO+v6P59ycP0JNTpG+h+671H+DcL5YizGcw8pwAAAABJRU5ErkJggg==",
                        NotFoundImageUrl = s1.NotFoundImageUrl,
                        MruModDate = s1.ModDate.ToString(),
                        ModDate = s1.ModDate,
                        Campus = s1.Campus != null ? s1.Campus.Description : string.Empty,
                        Department = depDescription != null ? depDescription : string.Empty,
                        Status = s1.Status.Status
                    };

                    if (s1.Campus != null)
                        mru.SearchCampusId = s1.Campus.ID;

                    if (!string.IsNullOrEmpty(mru.City))
                        mru.CityStateZip = mru.City;
                    if (!string.IsNullOrEmpty(mru.State))
                        mru.CityStateZip += ", " + mru.State;
                    if (!string.IsNullOrEmpty(mru.Zip))
                        mru.CityStateZip += " " + mru.Zip;

                    if (mruList != null)
                    {
                        var mruL = mruList.ToList();

                        var mruItem = mruL.FirstOrDefault(n => n.EntityId == s.ID);
                        if (mruItem != null)
                        {
                            mru.MruModDate = mruItem.ModDate.ToString();
                            mru.ModDate = mruItem.ModDate;
                        }
                    }

                    mruOutput.Add(mru);
                }
            }

            mruOutput = mruOutput.Distinct().OrderByDescending(n => n.ModDate).ToList();

            return mruOutput.Take(5);
        }

        /// <summary>
        /// Get the list of campus by User Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>a list of ID Description</returns>
        private IEnumerable<Guid> GetCampusItemsByUserId(Guid userId)
        {
            if (userId == Guid.Empty) return null;

            var listItems = this.repository.Query<User>().Where(n => n.ID == userId).SelectMany(n => n.CampusGroup)
                            .SelectMany(y => y.Campuses).Where(y => y.Status.StatusCode == "A").Select(l => l.ID).ToList(); // .SelectMany(y => y.Description).Distinct();


            return listItems;
        }

        //private string StudentImagePath
        //{
        //    get { return string.IsNullOrEmpty(this._studentImagePath) ? this.GetConfigurationSetting("STUDENTIMAGEPATH") : this._studentImagePath; }
        //}

        //private string AdvantageSiteUrl
        //{
        //    get { return string.IsNullOrEmpty(this._studentImagePath) ? this.GetConfigurationSetting("ADVANTAGESITEURI") : this._studentImagePath; }
        //}

        //private string GetConfigurationSetting(string keyName)
        //{
        //    string retValue = null;
        //    var configurationAppSetting = this.repositoryWithInt.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == keyName.ToUpper());
        //    if (configurationAppSetting != null) retValue = configurationAppSetting.ConfigurationValues[0].Value;

        //    return retValue;
        //}
    }
}
