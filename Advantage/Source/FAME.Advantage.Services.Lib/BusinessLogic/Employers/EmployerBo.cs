﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmployerBo.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   The employer bo.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.Employers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using AutoMapper;

    using FAME.Advantage.Domain.Employer;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.MostRecentlyUsed;
    using FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings;
    using FAME.Advantage.Domain.Users;
    using FAME.Advantage.Messages.Employer;
    using FAME.Advantage.Messages.MostRecentlyUsed;
    using FAME.Advantage.Services.Lib.Infrastructure.Extensions;

    /// <summary>
    /// The employer bo.
    /// </summary>
    public class EmployerBo
    {
        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        ///  The repository with int.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        /// The student image path.
        /// </summary>
        private readonly string studentImagePath = null;

        /// <summary>
        ///  The advantage site URL.
        /// </summary>
        private readonly string advantageSiteUrl = null;


        private const int EmployerMruTypeId = (int)FAME.Advantage.Messages.Enumerations.Enumerations.MRuTypes.Employers;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployerBo"/> class. 
        /// The Constructor
        /// </summary>
        /// <param name="repository">
        /// The repository
        /// </param>
        /// <param name="repositoryWithInt">
        /// the repository with integer
        /// </param>
        public EmployerBo(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            this.repositoryWithInt = repositoryWithInt;
       }


        /// <summary>
        /// Get the Students based on the search term passed in from the universal search
        /// </summary>
        /// <param name="searchTermInit">
        /// The search Term Initialization.
        /// </param>
        /// <returns>
        /// The searched employers
        /// </returns>
        public IEnumerable<EmployerSearchOutputModel> GetemployerSearch(string[] searchTermInit)
        {
            var searchTerm = searchTermInit[0];
            if (string.IsNullOrEmpty(searchTerm))
            {
                return null;
            }

            var campusesList = new List<Guid>();
            if (searchTermInit.Count() > 1)
            {
                var list = searchTermInit[1].ToString(CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(list))
                {
                    campusesList = list.Split(',').Select(Guid.Parse).ToList();
                }
            }

            // get the employers using the search passed in
            var employers = this.repository.Query<FAME.Advantage.Domain.Employer.Employer>()
                .Where(n => ((n.EmployerName ?? string.Empty) + (n.Address ?? string.Empty) + " " + (n.City ?? string.Empty) + (n.State.Code ?? string.Empty) + " " + (n.Zip ?? string.Empty) + " " + (n.Phone ?? string.Empty) + " " + (n.Industry.Description ?? string.Empty))
                    .Contains(searchTerm)).Take(100);

            var finalEmployers = new List<Employer>();

            if (campusesList.Count > 0)
            {
                foreach (var e in employers.ToList())
                {
                    var isAll = false;
                    if (e.CampusGroup.IsAllCampusGrp != null)
                    {
                        isAll = (bool)e.CampusGroup.IsAllCampusGrp;
                    }

                    if (isAll)
                    {
                        finalEmployers.Add(e);
                    }
                    else
                    {
                        var list = e.CampusGroup.Campuses.Select(n => n.ID).ToList();
                        finalEmployers.AddRange(from c in list where campusesList.Contains(c) select e);
                    }
                }
            }
            else
            {
                finalEmployers = employers.ToList();
            }

            finalEmployers.ForEach(n => n.GetCampusStringList());

            // return the list of employers
            return Mapper.Map<IEnumerable<EmployerSearchOutputModel>>(finalEmployers.Distinct().OrderBy(n => n.EmployerName).ToArray());
        }

        public IEnumerable<EmployerMruOutputModel> GetEmployerMru(Guid userId, Guid campusId)
        {

            var mrus = this.repository.Query<Mru>().Where(n => n.MruTypeObj.ID == EmployerMruTypeId && n.UserObj.ID == userId).OrderByDescending(n => n.ModDate);
            var mruGuids = mrus.Select(m => m.EntityId).ToList();
            var mruList = mrus.Select(l => new MruOutput { EntityId = l.EntityId, ModDate = l.ModDate }).ToList();

            if (mrus.Any())
            {
                var employers = this.repository.Query<Employer>().Where(n => mruGuids.Contains(n.ID))
                    .ForEach(r => r.GetImageUrl(AdvantageSiteUrl));

                return FillMruOutput(employers, mruList);
            }
            else
            {
                var campusIdList = GetCampusItemsByUserId(userId).ToList();

                var st2 = this.repository.Query<Employer>();
                var finalEmployers = new List<FAME.Advantage.Domain.Employer.Employer>();

                if (campusIdList.Count > 0)
                {
                    foreach (var e in st2.ToList())
                    {
                        var isAll = false;
                        if (e.CampusGroup.IsAllCampusGrp != null)
                            isAll = (bool)e.CampusGroup.IsAllCampusGrp;

                        if (isAll)
                        {
                            finalEmployers.Add(e);
                        }
                        else
                        {
                            var cList = e.CampusGroup.Campuses.Select(n => n.ID).ToList();
                            finalEmployers.AddRange(from c in cList where campusIdList.Contains(c) select e);
                        }
                    }
                }
                else
                {
                    finalEmployers = st2.ToList();
                }


                return FillMruOutput(finalEmployers, null);
            }
        }

        private IEnumerable<EmployerMruOutputModel> FillMruOutput(IEnumerable<Employer> employers, IEnumerable<MruOutput> mruList)
        {
            var mruOutput = new List<EmployerMruOutputModel>();

            foreach (var s in employers)
            {
                Employer s1 = s;
                if (mruOutput.All(n => n.Id != s1.ID))
                {


                    var mru = new EmployerMruOutputModel
                    {
                        Id = s1.ID,
                        EmployerName = s1.EmployerName,
                        Address = s1.Address ?? string.Empty,
                        City = s1.City ?? string.Empty,
                        State = s1.State != null ? s1.State.Description : string.Empty,
                        Zip = s1.Zip ?? string.Empty,
                        CityStateZip = string.Empty,
                        Phone = s1.Phone == null ? string.Empty : s1.Phone.FormatPhoneNumber(),
                        NotFoundImageUrl = s1.NotFoundImageUrl,
                        MruModDate = s1.ModDate.ToString(),
                        ModDate = s1.ModDate
                    };

                    if (s1.CampusGroup != null)
                    {
                        var firstOrDefault = s1.CampusGroup.Campuses.FirstOrDefault(n => n.Status.StatusCode == "A");
                        if (firstOrDefault != null)
                            mru.SearchCampusId = firstOrDefault.ID;
                    }

                    if (!string.IsNullOrEmpty(mru.City))
                        mru.CityStateZip = mru.City;
                    if (!string.IsNullOrEmpty(mru.State))
                        mru.CityStateZip += ", " + mru.State;
                    if (!string.IsNullOrEmpty(mru.Zip))
                        mru.CityStateZip += " " + mru.Zip;


                    if (mruList != null)
                    {
                        var mruL = mruList.ToList();

                        var mruItem = mruL.FirstOrDefault(n => n.EntityId == s.ID);
                        if (mruItem != null)
                        {
                            mru.MruModDate = mruItem.ModDate.ToString();
                            mru.ModDate = mruItem.ModDate;
                        }
                    }

                    mruOutput.Add(mru);
                }
            }

            mruOutput = mruOutput.Distinct().OrderByDescending(n => n.ModDate).ToList();

            return mruOutput.Take(5);
        }

        /// <summary>
        /// Get the list of campus by User Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>a list of ID Description</returns>
        private IEnumerable<Guid> GetCampusItemsByUserId(Guid userId)
        {
            if (userId == Guid.Empty) return null;

            var listItems = this.repository.Query<User>().Where(n => n.ID == userId).SelectMany(n => n.CampusGroup)
                            .SelectMany(y => y.Campuses).Where(y => y.Status.StatusCode == "A").Select(l => l.ID).ToList(); //.SelectMany(y => y.Description).Distinct();


            return listItems;
        }


        //private string StudentImagePath
        //{
        //    get { return String.IsNullOrEmpty(this.studentImagePath) ? GetConfigurationSetting("STUDENTIMAGEPATH") : this.studentImagePath; }
        //}

        private string AdvantageSiteUrl
        {
            get { return String.IsNullOrEmpty(this.studentImagePath) ? GetConfigurationSetting("ADVANTAGESITEURI") : this.studentImagePath; }
        }

        private string GetConfigurationSetting(string keyName)
        {
            string retValue = null;
            var configurationAppSetting = this.repositoryWithInt.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == keyName.ToUpper());
            if (configurationAppSetting != null) retValue = configurationAppSetting.ConfigurationValues[0].Value;

            return retValue;
        }
    }
}
