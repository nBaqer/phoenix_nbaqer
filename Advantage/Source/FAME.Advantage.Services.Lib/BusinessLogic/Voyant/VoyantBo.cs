﻿using System;
using System.Collections.Generic;
using System.Linq;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Messages.Wapi.Voyant;

namespace FAME.Advantage.Services.Lib.BusinessLogic.Voyant
{
    public class VoyantBo
    {
        //repository object that represents data store
        private readonly IRepositoryWithTypedID<Guid> repository;

        public VoyantBo(IRepositoryWithTypedID<Guid> repository)
        {
            this.repository = repository;

        }

        public IList<VoyantStudentOutputModel> GetVoyantStudentInfo(VoyantStudentInputModel filter)
        {
            var result = new List<VoyantStudentOutputModel>();
            var student = repository.Query<Student>();

            // Status 9 is currently attending.
            var studentlist = (filter.RequestHistorical == 1)
                ? (from s in student
                    let enroll = s.Enrollments
                    where enroll.Any(enrollment => enrollment.EnrollmentStatus.SystemStatusId != 9)
                    select s)
                : (from s in student
                    let enroll = s.Enrollments
                   where enroll.Any(enrollment => enrollment.EnrollmentStatus.SystemStatusId == 9)
                    select s);
           
            foreach (Student s in studentlist)
            {
                var v = VoyantStudentOutputModel.Factory();
                v.FirstName = s.FirstName;
                v.LastName = s.LastName;
                v.FamilyIncoming = s.FamilyIncoming;
                v.CitizenGuid = (s.CitizenshipsObj == null)? string.Empty: s.CitizenshipsObj.CitizenshipCode;
                v.Gender = (s.Gender == null)?string.Empty: s.Gender.Code;
                v.MaritalStatus = s.MaritalStatus;
                v.Race = (s.Ethnicity == null)?string.Empty: s.Ethnicity.Code;
                v.StudentId = s.ID.ToString(); // s.StudentId;
                foreach (Enrollment e in s.Enrollments)
                {
                     var en = StudentEnrollmentOutputModel.Factory();
                    en.CampusId = e.CampusId.ToString();
                    en.EnrollDate = e.EnrollmentDate;
                    en.ModDate = (filter.RequestHistorical == 1) ? e.DateDetermined.GetValueOrDefault() : e.EnrollmentDate;
                    en.ProgDescrip = (e.ProgramVersion == null)? string.Empty: e.ProgramVersion.Description;
                    en.StartDate = e.StartDate.GetValueOrDefault();
                    en.StatusCode = e.EnrollmentStatus.StatusCode;
                    en.StuEnrollId = e.EnrollmentId;
                    v.EnrollmentDataList.Add(en);

                    foreach (StudentAwards aw in e.StudentAwardList)
                    {
                        var fa = StudentFacultyOutputModel.Factory();
                        fa.GrossAmount = aw.GrossAmount.ToString("0.00");
                        fa.FundSourceCode = aw.AwardTypeObj.FundSourceCode;
                        v.FacultyDataList.Add(fa);
                    }

                    foreach (ArResults res in e.ResultList)
                    {
                        var re = StudentAcademicOutputModel.Factory();
                        re.Gpa = (res.GradeSystemDetailsObj == null)? string.Empty: res.GradeSystemDetailsObj.Gpa.ToString("0.00");
                        re.Grade = (res.GradeSystemDetailsObj == null)?string.Empty: res.GradeSystemDetailsObj.Grade;
                        re.StartDate = res.ClassSections.TermEntity.StartDate;
                        re.EndDate = res.ClassSections.TermEntity.EndDate;
                        re.Code = res.ClassSections.RequerimentCourse.Code;
                        re.Descrip = res.ClassSections.RequerimentCourse.Description;
                        re.InstructorId = res.ClassSections.InstructorId;
                        v.AcademicsDataList.Add(re);
                    }

                }

                result.Add(v);

            }

            return result.ToList();
        }

      
    }
}
