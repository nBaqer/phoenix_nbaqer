﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadBo.Post.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the LeadBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using FAME.Advantage.Domain.Campuses;
    using FAME.Advantage.Domain.Catalogs;
    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Domain.MostRecentlyUsed;
    using FAME.Advantage.Domain.Requirements;
    using FAME.Advantage.Domain.StatusesOptions;
    using FAME.Advantage.Domain.Student;
    using FAME.Advantage.Domain.Student.Enrollments;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings;
    using FAME.Advantage.Domain.SystemStuff.Intitution;
    using FAME.Advantage.Domain.SystemStuff.Resources;
    using FAME.Advantage.Domain.SystemStuff.SchoolDefinedFields;
    using FAME.Advantage.Domain.Users;
    using FAME.Advantage.Messages.Enumerations;
    using FAME.Advantage.Messages.Lead;
    using FAME.Advantage.Services.Lib.Comparators;

    using FluentNHibernate.Utils;

    /// <summary>
    /// The lead bo.
    /// </summary>
    public partial class LeadBo
    {
        /// <summary>
        /// This is use to classified the lead Notes as Notes
        /// </summary>
        public const string XNoteType = "Note";

        /// <summary>
        /// This is use to classified the lead Comment as Comment
        /// </summary>
        public const string XCommentType = "Comment";

        /// <summary>
        ///  The constant student age limit.
        /// </summary>
        private const string ConstStudentAgeLimit = "StudentAgeLimit";

        /// <summary>
        ///  The constant edit other leads.
        /// </summary>
        private const string ConstEditOtherLeads = "EditOtherLeads";

        /// <summary>
        /// Update the information of existing Lead.
        /// </summary>
        /// <param name="input">
        /// The info from client to need be modified.
        /// </param>
        /// <returns>
        /// The <see cref="LeadInfoPageOutputModel"/>.
        /// </returns>
        public LeadInfoPageOutputModel UpdateLeadInformation(LeadInfoPageOutputModel input)
        {
            // Flag to recorded if the status of the lead was changed
            var statusChanged = false;

            // Get Lead to be updated...
            var lead = this.repository.Query<Lead>().Single(x => x.ID.ToString() == input.InputModel.LeadId);

            // Validate input if something is wrong a exception is raise
            var validation = this.ValidateCommonLeadInformation(input);
            validation += this.ValidateUpdateLeadInformation(input, lead);
            if (validation != string.Empty)
            {
                throw new ApplicationException(validation);
            }

            // If we are here we validated
            try
            {
                // Test if the status changed.
                // The validate procedure should before validate if the change is valid.
                if (lead.LeadStatus.ID.ToString() != input.LeadStatusId)
                {
                    // You need to add a status change
                    var newStatus = this.repository.Get<UserDefStatusCode>(new Guid(input.LeadStatusId));
                    var newStatusRecord = new LeadStatusesChanges(
                                              lead.LeadStatus,
                                              newStatus,
                                              DateTime.Now,
                                              DateTime.Now,
                                              input.ModUser,
                                              lead);
                    lead.StatusesChangesList.Add(newStatusRecord);
                    statusChanged = true;
                }

                // Process the Admission Date to part time now
                // Input date get the time of now. if also is greater than today is converter to now.
                if (input.AssignedDate != null)
                {
                    var dt = DateTime.Now;
                    var a = input.AssignedDate.GetValueOrDefault();
                    input.AssignedDate = a.Date > dt.Date
                                             ? new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second)
                                             : input.AssignedDate;
                }
                else
                {
                    input.AssignedDate = DateTime.Now;
                }

                // Store single values
                lead.UpdateLeadInfoPageSingleValues(input);

                // Store entities
                // Lead status. 
                var status =
                    this.repository.Query<UserDefStatusCode>().Single(x => x.ID.ToString() == input.LeadStatusId);
                lead.UpdateObject("LeadStatus", status);

                // Update Lead Program Group
                var grp = this.repository.Query<ArProgramGroup>().SingleOrDefault(x => x.ID.ToString() == input.AreaId);
                lead.UpdateObject("ProgramGroupObj", grp);

                // Update Program
                var program =
                    this.repository.Query<ArPrograms>().SingleOrDefault(x => x.ID.ToString() == input.ProgramId);
                lead.UpdateObject("ProgramObj", program);

                // Update Program Version
                var programV =
                    this.repository.Query<ProgramVersion>().SingleOrDefault(x => x.ID.ToString() == input.PrgVerId);
                lead.UpdateObject("PrgVersionObj", programV);

                // Admission Rep
                var admidrepObj =
                    this.repository.Query<User>().SingleOrDefault(x => x.ID.ToString() == input.AdmissionRepId);
                lead.UpdateObject("AdmissionRepUserObj", admidrepObj);

                // AttendTypeObj
                var attent =
                    this.repository.Query<ArAttendTypes>().SingleOrDefault(x => x.ID.ToString() == input.AttendTypeId);
                lead.UpdateObject("AttendTypeObj", attent);

                // ProgramScheduleObj
                var tem1 =
                    this.repository.Query<ProgramSchedule>().SingleOrDefault(x => x.ID.ToString() == input.ScheduleId);
                lead.UpdateObject("ProgramScheduleObj", tem1);

                var tem2 = this.repository.Query<Prefixes>().SingleOrDefault(x => x.ID.ToString() == input.Prefix);
                lead.UpdateObject("PrefixObj", tem2);

                var tem3 = this.repository.Query<Suffixes>().SingleOrDefault(x => x.ID.ToString() == input.Suffix);
                lead.UpdateObject("SuffixObj", tem3);

                var tem4 = this.repository.Query<Gender>().SingleOrDefault(x => x.ID.ToString() == input.Gender);
                lead.UpdateObject("GenderObj", tem4);

                var tem44 = this.repository.Query<Ethnicity>().SingleOrDefault(x => x.ID.ToString() == input.RaceId);
                lead.UpdateObject("EthnicityObj", tem44);

                var tem5 =
                    this.repository.Query<Citizenships>().SingleOrDefault(x => x.ID.ToString() == input.Citizenship);
                lead.UpdateObject("CitizenshipsObj", tem5);

                var tem6 =
                    this.repository.Query<DependencyType>().SingleOrDefault(x => x.ID.ToString() == input.Dependency);
                lead.UpdateObject("DependencyTypeObj", tem6);

                var tem7 =
                    this.repository.Query<MaritalStatus>().SingleOrDefault(x => x.ID.ToString() == input.MaritalStatus);
                lead.UpdateObject("MaritalStatusObj", tem7);

                var tem8 =
                    this.repository.Query<FamilyIncoming>()
                        .SingleOrDefault(x => x.ID.ToString() == input.FamilyIncoming);
                lead.UpdateObject("FamilyIncomingObj", tem8);

                var tem9 =
                    this.repository.Query<HousingType>().SingleOrDefault(x => x.ID.ToString() == input.HousingType);
                lead.UpdateObject("HousingTypeObj", tem9);

                var tem99 =
                    this.repository.Query<SystemStates>().SingleOrDefault(x => x.ID.ToString() == input.DrvLicStateCode);
                lead.UpdateObject("DriverLicenseStateCodeObj", tem99);

                var tem10 =
                    this.repository.Query<Transportation>()
                        .SingleOrDefault(x => x.ID.ToString() == input.Transportation);
                lead.UpdateObject("TransportationObj", tem10);

                // Source
                var s1 =
                    this.repository.Query<SourceCategory>()
                        .SingleOrDefault(x => x.ID.ToString() == input.SourceCategoryId);
                lead.UpdateObject("SourceCategoryObj", s1);

                var s2 = this.repository.Query<SourceType>().SingleOrDefault(x => x.ID.ToString() == input.SourceTypeId);
                lead.UpdateObject("SourceTypeObj", s2);

                var s3 =
                    this.repository.Query<SourceAdvertisement>()
                        .SingleOrDefault(x => x.ID.ToString() == input.AdvertisementId);
                lead.UpdateObject("SourceAdvertisementObj", s3);

                var s5 =
                    this.repository.Query<AgencySponsors>()
                        .SingleOrDefault(x => x.ID.ToString() == input.AgencySponsorId);
                lead.UpdateObject("AgencySponsorsObj", s5);

                var s51 =
                    this.repository.Query<SyReasonNotEnrolled>()
                        .SingleOrDefault(x => x.ID.ToString() == input.ReasonNotEnrolled);
                lead.UpdateObject("ReasonNotEnrolledObj", s51);

                var s6 =
                    this.repository.Query<EducationLevel>()
                        .SingleOrDefault(x => x.ID.ToString() == input.PreviousEducationId);
                lead.UpdateObject("PreviousEducationObj", s6);

                var s7 = this.repository.Query<HighSchools>()
                    .SingleOrDefault(x => x.ID.ToString() == input.HighSchoolId);
                lead.UpdateObject("HighSchoolsObj", s7);

                var s8 =
                    this.repository.Query<AdminCriteria>()
                        .SingleOrDefault(x => x.ID.ToString() == input.AdminCriteriaId);
                lead.UpdateObject("AdminCriteriaObj", s8);

                var systemStatus = this.repository.Query<SyStatuses>()
                    .FirstOrDefault(x => x.StatusCode.ToUpper() == "A");

                // Phone load of entities
                var phonetypes = this.repository.Query<SyPhoneType>();
                lead.UpdatePhoneList(input.PhonesList, phonetypes, systemStatus);

                // Email update of entities
                var emailtype = this.repository.Query<EmailType>();
                lead.UpdateEmailList(input.NoneEmail, input.EmailList, emailtype, systemStatus);

                // Update Address................
                var addressTypeIq = this.repository.Query<AddressType>();
                var countryIq = this.repository.Query<Country>();
                var countyIq = this.repository.Query<County>();
                var statesIq = this.repository.Query<SystemStates>();
                var statusIq = this.repository.Query<SyStatuses>();
                input.LeadAddress.Moduser = input.ModUser;
                lead.UpdateAddresslist(input.LeadAddress, addressTypeIq, countyIq, statesIq, countryIq, statusIq);

                // Store Notes and Comments
                var user = this.repository.Query<User>()
                    .SingleOrDefault(x => x.ID.ToString() == input.InputModel.UserId);

                this.UpdateInsertNoteCommentText(
                    input.Comments,
                    user,
                    lead,
                    Enumerations.EnumNotePageField.InfoComment,
                    LeadBo.XCommentType);
                this.UpdateInsertNoteCommentText(
                    input.Note,
                    user,
                    lead,
                    Enumerations.EnumNotePageField.InfoNote,
                    LeadBo.XNoteType);

                // Insert in LastName Historic List 
                if (input.LeadLastNameHistoryList != null && input.LeadLastNameHistoryList.Count > 0)
                {
                    var newLastName = input.LeadLastNameHistoryList.First();
                    var gg = new LeadLastNameHistory(newLastName.LastName, lead, input.ModUser, DateTime.Now);
                    lead.LeadLastNameList.Add(gg);
                }

                // Update vehicle list
                lead.UpdateVehiclesList(input.Vehicles);

                // Store the SdfList...........................................................................
                if (input.SdfList != null)
                {
                    // Loop through all fields that comes from Client
                    foreach (LeadSdfOutputModel model in input.SdfList)
                    {
                        // Debug.WriteLine(string.Format("loop ext - value {0} ", model.SdfValue));
                        var flag = false;
                        foreach (SdfModuleValueLead sdf in lead.SdfModuleValueList)
                        {
                            // Update Field if exists
                            if (model.SdfId == sdf.SdfObj.ID.ToString())
                            {
                                sdf.UpdateSdf(model);
                                flag = true;
                                break;
                            }
                        }

                        if (flag)
                        {
                            continue;
                        }

                        LeadSdfOutputModel model1 = model;
                        var sdf1 = this.repository.Query<Sdf>().SingleOrDefault(x => x.ID.ToString() == model1.SdfId);

                        //// Debug.WriteLine(string.Format("sdf to use -- {0}",sdf1.SdfDescrip));
                        var sdfnew = new SdfModuleValueLead(sdf1, model.SdfValue, lead, string.Empty, DateTime.Now);
                        lead.SdfModuleValueList.Add(sdfnew);
                    }
                }

                // Store the Group List.......................................................................
                if (input.GroupIdList != null)
                {
                    // Get Lead Group assigned to Lead
                    var groups =
                        this.repository.Query<LeadGroups>()
                            .Where(x => x.CampusGroup.Campuses.Any(y => y.ID.ToString() == input.CampusId));
                    IList<LeadGroups> active = new List<LeadGroups>();

                    foreach (LeadGroups gr in groups)
                    {
                        // <- do not change to LinQ!
                        foreach (string s in input.GroupIdList)
                        {
                            if (gr.ID.ToString() == s)
                            {
                                active.Add(gr);
                            }
                        }
                    }

                    var actual =
                        lead.LeadGroupsList.AsQueryable()
                            .Where(x => x.CampusGroup.Campuses.Any(y => y.ID.ToString() == input.CampusId))
                            .ToList();

                    // Remove all actual group for the campus Id
                    foreach (LeadGroups g in actual)
                    {
                        lead.LeadGroupsList.Remove(g);
                    }

                    // Add the new active groups
                    foreach (LeadGroups g in active)
                    {
                        lead.LeadGroupsList.Add(g);
                    }
                }

                // If the Lead has change the campus, then we need to do the following actions
                // Lead Campus
                if (lead.Campus.ID.ToString() != input.CampusId)
                {
                    // Delete the entries in MRU related to the old campus
                    lead.MruList.Clear(); // Delete all entries in the MRU for the old Campus

                    // Add the new entry in MRU related to the new campus
                    // var user = repository.Query<User>().Single(x => x.ID.ToString() == input.ModUser);
                    var type = this.repositoryWithInt.Query<MruType>().Single(x => x.ID == 4);
                    var campus = this.repository.Query<Campus>().Single(x => x.ID.ToString() == input.CampusId);
                    var mru = new ViewLeadMru(type, lead, user, campus, input.ModUser, DateTime.Now);
                    lead.MruList.Add(mru);

                    // Update Campus
                    lead.UpdateObject("Campus", campus);
                }

                // Update the entity lead.
                this.repository.UpdateAndFlush(lead);

                // If the status changed return the new acceptable status
                input.StateChangeIdsList = null;
                if (statusChanged)
                {
                    var inputpar = new LeadInputModel
                    {
                        CampusId = input.CampusId,
                        UserId = input.ModUser,
                        LeadStatusId = input.LeadStatusId
                    };
                    input.StateChangeIdsList = this.GetLegalStatusChangesIds(inputpar);
                }

                return input;
            }
            catch (Exception)
            {
                // Avoid any update if a error occurred
                this.repository.Evict(lead);
                throw;
            }
        }

        /// <summary>
        /// Use this to set any comment or notes type in Admission Module
        /// Do not use in any other module!!!
        /// </summary>
        /// <param name="notetext">The text to be store in the table</param>
        /// <param name="user">the user that made the change</param>
        /// <param name="lead">lead associated with the notes</param>
        /// <param name="pageFieldId">View Enumeration <see cref="Enumerations.EnumNotePageField"/> </param>
        /// <param name="noteType">The code type. See Type of Notes table for valid values</param>
        public void UpdateInsertNoteCommentText(
            string notetext,
            User user,
            Lead lead,
            Enumerations.EnumNotePageField pageFieldId,
            string noteType)
        {
            var noteObj = lead.NotesList.FirstOrDefault(x => x.PageFieldsObj.ID == (int)pageFieldId);
            if (noteObj == null)
            {
                // Test if the notes text is empty. Is empty does not insert it
                if (string.IsNullOrWhiteSpace(notetext))
                {
                    return;
                }

                // insert the notes.
                var module = this.repositoryWithInt.Query<SystemModule>().SingleOrDefault(x => x.Code == "AD");

                // var noteType = repositoryWithInt.Query<NotesType>().SingleOrDefault(x => x.Code == noteTypeCode);
                var pageField =
                    this.repositoryWithInt.Query<NotesPageFields>().SingleOrDefault(x => x.ID == (int)pageFieldId);
                var nnote = new AllNotes(module, noteType, pageField, user, notetext, user.UserName);
                lead.NotesList.Add(nnote);
            }
            else
            {
                // Update the notes if was modified
                if (noteObj.NoteText != notetext)
                {
                    if (string.IsNullOrWhiteSpace(notetext))
                    {
                        // Delete the notes
                        lead.NotesList.Remove(noteObj);
                    }
                    else
                    {
                        // Update the notes
                        lead.NotesList.Remove(noteObj);
                        noteObj.Update(notetext, user.UserName, noteType, user); 
                        lead.NotesList.Add(noteObj);
                    }
                 }
            }
        }

        /// <summary>
        /// Insert a new lead
        /// </summary>
        /// <param name="input">
        /// new lead information
        /// </param>
        /// <returns>
        /// The <see cref="LeadInfoInsertOutputModel"/>.
        /// </returns>
        public LeadInfoInsertOutputModel InsertNewLead(LeadInfoPageOutputModel input)
        {
            // Prepare return object
            var output = new LeadInfoInsertOutputModel { PossiblesDuplicatesList = new List<DuplicateOutputModel>() };

            // Common validations...
            var validation = this.ValidateCommonLeadInformation(input);
            validation += this.ValidateInsertLeadOperation(input);
            if (validation != string.Empty)
            {
                output.ValidationErrorMessages = validation;
                return output;
            }

            // We are validated here... Now duplicate analysis
            if (input.AvoidDuplicateAnalysis == false)
            {
                output = this.AnalysisDuplicates(input);
                if (string.IsNullOrWhiteSpace(output.DuplicatedMessages) == false)
                {
                    return output;
                }
            }

            // If here is validated and it is not duplicate...

            // Lead status
            var status = this.repository.Query<UserDefStatusCode>().Single(x => x.ID.ToString() == input.LeadStatusId);

            // Campus
            var campus = this.repository.Query<Campus>().Single(x => x.ID.ToString() == input.CampusId);

            // Program Group
            var grp = this.repository.Query<ArProgramGroup>().SingleOrDefault(x => x.ID.ToString() == input.AreaId);

            // Program
            var program = this.repository.Query<ArPrograms>().SingleOrDefault(x => x.ID.ToString() == input.ProgramId);

            // Update Program Version
            var programV =
                this.repository.Query<ProgramVersion>().SingleOrDefault(x => x.ID.ToString() == input.PrgVerId);

            // Admission Rep
            var admidrepObj = this.repository.Query<User>()
                .SingleOrDefault(x => x.ID.ToString() == input.AdmissionRepId);

            // AttendTypeObj
            var attendTypes =
                this.repository.Query<ArAttendTypes>().SingleOrDefault(x => x.ID.ToString() == input.AttendTypeId);

            // ProgramScheduleObj
            var programSchedule =
                this.repository.Query<ProgramSchedule>().SingleOrDefault(x => x.ID.ToString() == input.ScheduleId);

            // Prefixes
            var prefixes = this.repository.Query<Prefixes>().SingleOrDefault(x => x.ID.ToString() == input.Prefix);

            // Suffixes
            var suffixes = this.repository.Query<Suffixes>().SingleOrDefault(x => x.ID.ToString() == input.Suffix);

            // Gender
            var gender = this.repository.Query<Gender>().SingleOrDefault(x => x.ID.ToString() == input.Gender);

            // Race
            var race = this.repository.Query<Ethnicity>().SingleOrDefault(x => x.ID.ToString() == input.RaceId);

            // Citizenships
            var citizen =
                this.repository.Query<Citizenships>().SingleOrDefault(x => x.ID.ToString() == input.Citizenship);

            // DependencyType
            var dependencyType =
                this.repository.Query<DependencyType>().SingleOrDefault(x => x.ID.ToString() == input.Dependency);

            // MaritalStatus
            var maritalStatus =
                this.repository.Query<MaritalStatus>().SingleOrDefault(x => x.ID.ToString() == input.MaritalStatus);

            // FamilyIncoming
            var familyIncoming =
                this.repository.Query<FamilyIncoming>().SingleOrDefault(x => x.ID.ToString() == input.FamilyIncoming);

            // HousingType
            var housingType =
                this.repository.Query<HousingType>().SingleOrDefault(x => x.ID.ToString() == input.HousingType);

            // Driver license State
            var driverLicenseState =
                this.repository.Query<SystemStates>().SingleOrDefault(x => x.ID.ToString() == input.DrvLicStateCode);

            // Transportation
            var transportation =
                this.repository.Query<Transportation>().SingleOrDefault(x => x.ID.ToString() == input.Transportation);

            // Contact Information.....

            // County
            var county = this.repository.Query<County>();

            // Country
            var country = this.repository.Query<Country>();

            // SystemStates
            var systemStates = this.repository.Query<SystemStates>();

            // SourceCategory
            var sourceCategory =
                this.repository.Query<SourceCategory>().SingleOrDefault(x => x.ID.ToString() == input.SourceCategoryId);

            // SourceType
            var sourceType =
                this.repository.Query<SourceType>().SingleOrDefault(x => x.ID.ToString() == input.SourceTypeId);

            // SourceAdvertisement
            var sourceAdvertisement =
                this.repository.Query<SourceAdvertisement>()
                    .SingleOrDefault(x => x.ID.ToString() == input.AdvertisementId);

            // AgencySponsors 
            var agencySponsors =
                this.repository.Query<AgencySponsors>().SingleOrDefault(x => x.ID.ToString() == input.AgencySponsorId);

            var reasonNotEnrolled =
                this.repository.Query<SyReasonNotEnrolled>()
                    .SingleOrDefault(x => x.ID.ToString() == input.ReasonNotEnrolled);

            // EducationLevel
            var educationLevel =
                this.repository.Query<EducationLevel>()
                    .SingleOrDefault(x => x.ID.ToString() == input.PreviousEducationId);

            // HighSchools
            var highSchools =
                this.repository.Query<HighSchools>().SingleOrDefault(x => x.ID.ToString() == input.HighSchoolId);

            // AdminCriteria
            var adminCriteria =
                this.repository.Query<AdminCriteria>().SingleOrDefault(x => x.ID.ToString() == input.AdminCriteriaId);

            // Phone load of entities
            var phonetypes = this.repository.Query<SyPhoneType>();

            // Email update of entities
            var emailtypes = this.repository.Query<EmailType>();

            var addresstypes = this.repository.Query<AddressType>();

            var statustypes = this.repository.Query<SyStatuses>();

            // Enter Notes and comments
            // ReSharper disable CollectionNeverQueried.Local
            IList<AllNotes> notesList = new List<AllNotes>();

            // ReSharper restore CollectionNeverQueried.Local
            var user = this.repository.Query<User>().SingleOrDefault(x => x.ID.ToString() == input.InputModel.UserId);

            // var noteType = repositoryWithInt.Query<NotesType>().SingleOrDefault(x => x.Code == "OR");
            var module = this.repositoryWithInt.Query<SystemModule>().SingleOrDefault(x => x.Code == "AD");
            if (user == null || module == null)
            {
                throw new ApplicationException("Parameter null. please review userId");
            }

            if (!string.IsNullOrEmpty(input.Note))
            {
                // insert the notes.
                var pageField = this.repositoryWithInt.Query<NotesPageFields>().SingleOrDefault(x => x.ID == 3);
                var nnote = new AllNotes(module, LeadBo.XNoteType, pageField, user, input.Note, user.UserName);
                notesList.Add(nnote);
            }

            if (!string.IsNullOrEmpty(input.Comments))
            {
                // insert the notes.
                var pageField = this.repositoryWithInt.Query<NotesPageFields>().SingleOrDefault(x => x.ID == 2);
                var nnote = new AllNotes(module, LeadBo.XCommentType, pageField, user, input.Comments, user.UserName);
                notesList.Add(nnote);
            }

            // Store the Group List.............................................................................
            IList<LeadGroups> activeGroup = new List<LeadGroups>();
            if (input.GroupIdList != null)
            {
                // Get Lead Group assigned to Lead
                var groups =
                    this.repository.Query<LeadGroups>()
                        .Where(x => x.CampusGroup.Campuses.Any(y => y.ID.ToString() == input.CampusId));

                foreach (LeadGroups gr in groups)
                {
                    // <- do not change to LinQ!
                    foreach (string s in input.GroupIdList)
                    {
                        if (gr.ID.ToString() == s)
                        {
                            activeGroup.Add(gr);
                        }
                    }
                }
            }

            // Store the SdfList...............................................................................
            var sdfModuleList = new List<SdfModuleValueLead>();
            if (input.SdfList != null)
            {
                // Loop through all fields that comes from Client
                foreach (LeadSdfOutputModel model in input.SdfList)
                {
                    LeadSdfOutputModel model1 = model;

                    // read the contained SDF
                    var sdf1 = this.repository.Query<Sdf>().SingleOrDefault(x => x.ID.ToString() == model1.SdfId);

                    // create a SDFModuleValue
                    var sdfnew = new SdfModuleValueLead(sdf1, model.SdfValue, null, string.Empty, DateTime.Now);
                    sdfModuleList.Add(sdfnew);
                }
            }

            // Process the Admission Date to part time now
            // Input date get the time of now. if also is greater than today is converter to now.
            if (input.AssignedDate != null)
            {
                var dt = DateTime.Now;
                var a = input.AssignedDate.GetValueOrDefault();
                input.AssignedDate = a.Date > dt.Date
                                         ? new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second)
                                         : new DateTime(a.Year, a.Month, a.Day, dt.Hour, dt.Minute, dt.Second);
            }
            else
            {
                input.AssignedDate = DateTime.Now;
            }

            var activeSystemStatus =
                this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode.ToUpper() == "A");

            // student status id (by default the value is inactive status as it is just a lead.
            var studentStatusId =
                this.repository.Query<StatusOption>().SingleOrDefault(x => x.StatusCode.ToUpper() == "I");

            // Create the new lead
            var lead = new Lead(
                           input,
                           status, // LeadInfoPageOutputModel input, 
                           campus, // UserDefStatusCode status, 
                           grp, // Campus , 
                           program, // ArProgramGroup 
                           programV, // ArPrograms  
                           admidrepObj, // ProgramVersion 
                           attendTypes, // User admidrepObj, 
                           programSchedule, // ArAttendTypes attendTypes,
                           prefixes, // ProgramSchedule programSchedule, 
                           suffixes, // Prefixes  
                           gender, // Suffixes  
                           race,  // Gender 
                           citizen,     // Ethnicity (race)
                           dependencyType, // Citizenships citizen, 
                           maritalStatus, // DependencyType dependencyType, 
                           familyIncoming,  // MaritalStatus maritalStatus,
                           housingType, // FamilyIncoming familyIncoming, 
                           driverLicenseState, // HousingType housingType, 
                           transportation, // SystemStates driverLicState, 
                           sourceCategory, // Transportation 
                           sourceType,   // SourceCategory 
                           sourceAdvertisement, // SourceType 
                           agencySponsors, // SourceAdvertisement sourceAdvertisement,
                           reasonNotEnrolled, // AgencySponsors agencySponsors,
                           educationLevel,  // Reason not enrolled
                           highSchools, // EducationLevel educationLevel, 
                           adminCriteria, // HighSchools highSchools, 
                           phonetypes, // AdminCriteria adminCriteria, 
                           addresstypes, // IQueryable<SyPhoneType> phoneTypeList, 
                           emailtypes, // IQueryable<AddressType> addressTypeList, 
                           systemStates, // IQueryable<EmailType> emailTypeList, 
                           county, // IQueryable<SystemStates> usaStatesList,
                           statustypes,  // IQueryable<County> countyList,
                           country, // IQueryable<SyStatuses> syStatusesList, 
                           activeGroup, // IQueryable<Country> countryList, 
                           sdfModuleList, // ICollection<AdLeadGroups> leadGroups,
                           activeSystemStatus, // List<SdfModuleValueLead> sdfModuleList)
                           activeSystemStatus, // SyStatuses phoneStatus
                           studentStatusId, // SyStatuses emailStatus
                           string.Empty,  // syStatuses studentstatusId
                           new Guid("00000000-0000-0000-0000-000000000000"), // student number
                           false, // studentId
                           false); // is first time in post secondary school

            // You need to add a status change from NULL to the new status
            var newStatus = this.repository.Get<UserDefStatusCode>(new Guid(input.LeadStatusId));
            var newStatusRecord = new LeadStatusesChanges(
                                      null,
                                      newStatus,
                                      DateTime.Now,
                                      DateTime.Now,
                                      input.ModUser,
                                      lead);
            lead.StatusesChangesList.Add(newStatusRecord);

            // Add The notes list
            lead.SetNotesList(notesList);

            // Put the Lead in the MRU List
            var type = this.repositoryWithInt.Query<MruType>().Single(x => x.ID == 4);
            var mru = new ViewLeadMru(type, lead, user, lead.Campus, lead.ModUser, DateTime.Now);
            lead.MruList.Add(mru);

            // save it
            this.repository.SaveOnly(lead);

            // Put the Lead in MRU
            return output;
        }

        /// <summary>
        /// Return true if the lead is not read only. 
        /// Condition to be not read only. 
        ///  - You can not delete a Lead is it is enrolled
        ///  - You can delete a Lead if the lead has not document or Fees associated with him.
        ///  - If you have document or fees you can only inactivate the lead.
        /// </summary>
        /// <param name="input">
        /// The Lead input model
        /// </param>
        /// <returns>
        /// True if you can not delete the lead
        /// </returns>
        public bool IsLeadErasable(LeadInputModel input)
        {
            // Test if exists transactions or documents or it is enrolled
            var exists = this.repository.Query<Lead>().Any(x => x.ID.ToString() == input.LeadId
                && (x.LeadDocumentsList.Any() || x.TransactionsList.Any() || x.LeadStatus.SystemStatus.Description == "Enrolled"));

            return !exists;
        }

        /// <summary>
        /// This procedure should delete the lead and all related fields.
        /// </summary>
        /// <param name="leadId">The lead GUID as string</param>
        public void DeleteLead(string leadId)
        {
            var lead = this.repository.Query<Lead>().Single(x => x.ID.ToString() == leadId);
            if (lead.LeadDocumentsList.Any() || lead.TransactionsList.Any() || lead.LeadStatus.Description == "Enrolled")
            {
                throw new ApplicationException("This lead record contains uploaded documents or processed fees and cannot be deleted.");
            }

            this.repository.Delete(lead);
        }

        /// <summary>
        /// The validate new lead.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <param name="validation">
        /// The validation.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ValidateNewLead(LeadInfoPageOutputModel input, out string validation)
        {
            validation = string.Empty;

            validation += this.ValidateCommonLeadInformation(input);

            validation += this.ValidateInsertLeadOperation(input);

            return validation == string.Empty;
        }

        /// <summary>
        /// The validate ssn.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ValidateSsn(LeadInfoPageOutputModel input)
        {
            return  !string.IsNullOrEmpty(this.ValidateInsertLeadOperation(input));
        }

        /// <summary>
        /// Validation common to UPDATE DELETE Operation
        /// </summary>
        /// <param name="input">
        /// The info page Information
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string ValidateCommonLeadInformation(LeadInfoPageOutputModel input)
        {
            string output = string.Empty;

            // Validate the UserId first
            if (string.IsNullOrWhiteSpace(input.ModUser))
            {
                output = "The user is not defined" + Environment.NewLine;
            }

            // Test if the status was entered
            Guid test;
            if (!Guid.TryParse(input.LeadStatusId, out test))
            {
                output += "You must enter a Lead Status!" + Environment.NewLine;
            }

            // Test if dependents are between 0 and 99
            int dep;
            if (int.TryParse(input.Dependants, out dep))
            {
                if (dep > 99)
                {
                    output += "Number of Dependents can not be greater than 99" + Environment.NewLine;
                }
            }

            if (input.PhonesList == null)
            {
                output += "Phone List can not be null" + Environment.NewLine;
            }
            else
            {
                foreach (LeadPhonesOutputModel model in input.PhonesList)
                {
                    if (!string.IsNullOrWhiteSpace(model.Phone))
                    {
                        Guid temp;
                        if (!Guid.TryParse(model.PhoneTypeId, out temp))
                        {
                            output += "You must enter the Phone Type" + Environment.NewLine;
                        }

                        if (!model.IsForeignPhone)
                        {
                            var phone = !string.IsNullOrEmpty(model.Phone) ?  model.Phone.Replace("(", string.Empty).Replace(")", string.Empty).Replace(".", string.Empty).Replace("+", string.Empty).Replace("-", string.Empty).Replace(" ", string.Empty) : string.Empty;
                            if (string.IsNullOrEmpty(phone) ||  phone.Length != 10)
                            {
                                output += "Local Phone must have 10 digits" + Environment.NewLine;
                            }
                        }
                    }
                }
            }

            if (input.EmailList == null)
            {
                output += "E-mail List can not be null" + Environment.NewLine;
            }
            else
            {
                foreach (LeadEmailOutputModel model in input.EmailList)
                {
                    if (!string.IsNullOrWhiteSpace(model.Email))
                    {
                        Guid temp;
                        if (!Guid.TryParse(model.EmailTypeId, out temp))
                        {
                            output += "You must enter the E-mail Type" + Environment.NewLine;
                        }

                        if (model.Email.Length < 6 & model.Email.IndexOf("@", StringComparison.Ordinal) < 1
                            & model.Email.IndexOf("@", StringComparison.Ordinal) < (model.Email.Length - 2)
                            & model.Email.IndexOf(".", StringComparison.Ordinal) != -1)
                        {
                            output += "Email has incorrect format" + Environment.NewLine;
                        }
                    }
                }
            }

            if (input.LeadAddress != null)
            {
                // Address type
                if (string.IsNullOrWhiteSpace(input.LeadAddress.Address1) == false)
                {
                    Guid temp;
                    if (Guid.TryParse(input.LeadAddress.AddressTypeId, out temp) == false)
                    {
                        output += "You must enter a Address Type" + Environment.NewLine;
                    }
                }

                // USA ZIP Code
                if (input.LeadAddress.IsInternational == false)
                {
                    int zip;
                    if (string.IsNullOrWhiteSpace(input.LeadAddress.ZipCode) == false)
                    {
                        if (input.LeadAddress.ZipCode.Trim().Length != 5
                            || int.TryParse(input.LeadAddress.ZipCode, out zip) == false)
                        {
                            output += "USA ZIP code must have 5 numbers" + Environment.NewLine;
                        }
                    }
                }
            }

            // At least in one Lead Group
            if (input.GroupIdList == null || input.GroupIdList.Count == 0)
            {
                output += "Lead must be in at least one Lead Group" + Environment.NewLine;
            }

            // Minimal Age
            var configurationAppSetting =
                this.repositoryWithInt.Query<ConfigurationAppSetting>()
                    .FirstOrDefault(n => n.KeyName.ToUpper() == LeadBo.ConstStudentAgeLimit);
            Debug.Assert(configurationAppSetting != null, "configurationAppSetting != null");
            var setting = configurationAppSetting.ConfigurationValues[0].Value;

            if (string.IsNullOrWhiteSpace(input.Age) == false && int.Parse(setting) > int.Parse(input.Age))
            {
                output += string.Format("Lead must be at least {0} years old {1}", setting, Environment.NewLine);
            }

            // Check if FirstName and Last Name are not empty
            if (string.IsNullOrWhiteSpace(input.FirstName) || string.IsNullOrWhiteSpace(input.LastName))
            {
                output += "First Name and Last Name are required" + Environment.NewLine;
            }

            return output;
        }

        /// <summary>
        /// Validation for UPDATE Lead Operation
        /// </summary>
        /// <param name="input">
        /// Lead info Page Information
        /// </param>
        /// <param name="lead">
        /// The lead object
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string ValidateUpdateLeadInformation(LeadInfoPageOutputModel input, Lead lead)
        {
            string output = string.Empty;

            // Check if the status change is valid in the work-flow
            if (lead != null && lead.LeadStatus.ID.ToString() != input.LeadStatusId)
            {
                // check if the status change is from imported do not do any kind of workflow validation.
                if (lead.LeadStatus.SystemStatusId != 25)
                {
                    // Test if the status change is valid.........
                    var isWorkflow = this.repository.Query<LeadWorkflowStatusChanges>()
                        .Any(
                            x => x.OrigStatusObj.ID == lead.LeadStatus.ID
                                 && x.NewStatusObj.ID.ToString() == input.LeadStatusId);
                    if (isWorkflow == false)
                    {
                        var defStatusCode = this.repository.Query<UserDefStatusCode>()
                            .SingleOrDefault(x => x.ID.ToString() == input.LeadStatusId);
                        if (defStatusCode != null)
                        {
                            var oldStatusDes = defStatusCode.Description;
                            output = string.Format(
                                "The change of status from {0} to {1} is not allowed",
                                lead.LeadStatus.Description,
                                oldStatusDes);
                        }
                        else
                        {
                            output = string.Format(
                                "The change of status from {0} to {1} is not allowed",
                                lead.LeadStatus.Description,
                                string.Empty);
                        }

                        return output;
                    }
                }

                // In case that is not allowed the change of status for others. Validate if the actual user has right to do that.
                var configurationAppSetting = this.repositoryWithInt.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == LeadBo.ConstEditOtherLeads);
                var setting = configurationAppSetting.ConfigurationValues[0].Value;
                var user = this.repository.Query<User>().SingleOrDefault(x => x.ID.ToString() == input.InputModel.UserId);
                if (user != null)
                {
                    if (!user.IsAdvantageSuperUser)
                    {
                        if (setting.ToLower() == "no")
                        {
                            // Check if the actual user has right to do the operation
                    
                            var haspermission = user.RolesByCampusList.Any(x => x.CampusGroup.Campuses.Any(t => t.ID.ToString() == input.CampusId) &&
                               x.Role.LeadStatusChangePermissionList.Any(
                                    y => y.LeadWorkflowStatusChangesObj.OrigStatusObj.ID == lead.LeadStatus.ID &&
                                         y.LeadWorkflowStatusChangesObj.NewStatusObj.ID.ToString() == input.LeadStatusId));

                            if (haspermission == false)
                            {
                                output = string.Format("The user {0} has no right to do the operation", input.ModUser);
                                return output;
                            }
                        }
                    }
                }

                // Analysis of duplicates SSN
                if (string.IsNullOrWhiteSpace(input.SSN) == false && input.SSN != lead.SSN)
                {
                    output = this.AnalysisDuplicateSsn(input);
                }
            }

            return output;
        }

        /// <summary>
        /// Validation for INSERT Lead Operation
        /// </summary>
        /// <param name="input">
        /// Lead info Page Information
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string ValidateInsertLeadOperation(LeadInfoPageOutputModel input)
        {
            string output = string.Empty;

            // Validation of SSN this validation is fail the insert because the system can not
            // work with repeated SSN
            // Check if the SSN is duplicated with another lead or another student
            if (string.IsNullOrWhiteSpace(input.SSN) == false)
            {
                output = this.AnalysisDuplicateSsn(input);
            }

            return output;
        }

        /// <summary>
        /// Analysis duplicate of SSN in Lead and Student
        /// </summary>
        /// <param name="input">
        /// Lead info Page Information
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string AnalysisDuplicateSsn(LeadInfoPageOutputModel input)
        {
            string output = string.Empty;
            var leadId = !string.IsNullOrEmpty(input.LeadId) ? input.LeadId : input.InputModel != null ? input.InputModel.LeadId : string.Empty;

            //lead is not being passed, cannot compare with other lead by just ssn
            if (string.IsNullOrEmpty(leadId))
            {
                return output;
            }

            // Check if the SSN is duplicated with another lead or another student
            var isdup = this.repository.Query<Lead>().Any(x => x.SSN == input.SSN && x.ID.ToString() != leadId);
            if (isdup)
            {
                output = "SSN Duplicate with other lead";
                return output;
            }

            isdup = this.repository.Query<Student>().Any(x => x.SSN == input.SSN);
            if (isdup)
            {
                output = "SSN Duplicate with a Existent Student";
                return output;
            }

            return output;
        }

        /// <summary>
        /// This is to be used when a lead is inserted.
        /// </summary>
        /// <param name="input">
        /// Lead info Page Information
        /// </param>
        /// <returns>
        /// The <see cref="LeadInfoInsertOutputModel"/>.
        /// </returns>
        private LeadInfoInsertOutputModel AnalysisDuplicates(LeadInfoPageOutputModel input)
        {
            var output = new LeadInfoInsertOutputModel
            {
                PossiblesDuplicatesList = new List<DuplicateOutputModel>()
            };

            // Check for possible duplicates. this validation is not definitive.
            // The user can decide to enter the lead.
            var lead1List = this.repository.Query<Lead>()
               .Where(x => (x.FirstName == input.FirstName && x.LastName == input.LastName) ||
                                       (x.AddressList.Any(y => y.Address1 == input.LeadAddress.Address1) &&
                                       string.IsNullOrWhiteSpace(input.LeadAddress.Address1) == false)).ToList();

            // var lead1List = this.repository.Query<Lead>()
            // .Where(x => ((x.FirstName == input.FirstName) && (x.LastName == input.LastName)) ||
            // (x.AddressList.Select(y => y.IsShowOnLeadPage).Any(t => t.Address1 == input.LeadAddress.Address1 )&& 
            // string.IsNullOrWhiteSpace(input.LeadAddress.Address1) == false)).ToList();

            // Phone get leads duplicates
            // var leadList = new List<Domain.Lead.Lead>();
            foreach (LeadPhonesOutputModel model in input.PhonesList)
            {
                LeadPhonesOutputModel model1 = model;
                var listphone = this.repository.Query<LeadPhone>().Where(x => x.Phone == model1.Phone && string.IsNullOrWhiteSpace(model1.Phone) == false);
                lead1List.AddRange(listphone.Select(phone => phone.NewLeadObj));
            }

            // Email Get duplicates
            foreach (LeadEmailOutputModel model in input.EmailList)
            {
                LeadEmailOutputModel model1 = model;
                var listMail = this.repository.Query<LeadEmail>().Where(x => x.Email == model1.Email && string.IsNullOrWhiteSpace(model1.Email) == false);
                lead1List.AddRange(listMail.Select(mail => mail.LeadObj));
            }

            var leadoutList = lead1List.Distinct(new LeadDistinctComparator());

            IList<Lead> leadList = leadoutList as IList<Lead> ?? leadoutList.ToList();
            if (leadList.Any())
            {
                output.DuplicatedMessages = "Possibles duplicates were found";
                foreach (Lead lead in leadList)
                {
                    Lead lead1 = lead;
                    Lead theLead = this.repository.Query<Lead>().SingleOrDefault(x => x.ID == lead1.ID);
                    Debug.Assert(theLead != null, "theLead != null");
                    var phones = theLead.LeadPhoneList.ToList();
                    var emails = theLead.LeadEmailList.ToList();
                    var domainEntityWithTypedID = lead.AddressList.SingleOrDefault(x => x.IsShowOnLeadPage);

                    // if (domainEntityWithTypedID != null)
                    // {
                    var dup = new DuplicateOutputModel
                    {
                        // Address1 =  domainEntityWithTypedID.Address1,
                        Address1 = domainEntityWithTypedID == null ? string.Empty : domainEntityWithTypedID.Address1,
                        AdmissionRep = lead.AdmissionRepUserObj == null ? string.Empty : lead.AdmissionRepUserObj.FullName,
                        Campus = lead.Campus == null ? string.Empty : lead.Campus.Description,
                        Dob = lead.BirthDate == null ? string.Empty : lead.BirthDate.ToString(),
                        FirstName = lead.FirstName,
                        LastName = lead.LastName,
                        Phone = (phones.Count > 0) ? phones[0].Phone : string.Empty,
                        Phone1 = (phones.Count > 1) ? phones[1].Phone : string.Empty,
                        Phone2 = (phones.Count > 2) ? phones[2].Phone : string.Empty,
                        Ssn = lead.SSN ?? string.Empty,
                        Status = lead.LeadStatus.Description,
                        Email = (emails.Count > 0) ? emails[0].Email : string.Empty,
                        Email1 = (emails.Count > 1) ? emails[1].Email : string.Empty
                    };

                    output.PossiblesDuplicatesList.Add(dup);

                    // }
                }
            }

            return output;
        }
    }
}
