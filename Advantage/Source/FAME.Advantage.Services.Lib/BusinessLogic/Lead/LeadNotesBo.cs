﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadNotesBo.cs" company="FAME">
//   2015, 2016
// </copyright>
// <summary>
//   Business object to lead page
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using FAME.Advantage.Domain.Catalogs;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Domain.Requirements.Documents;
    using FAME.Advantage.Domain.SystemStuff.Resources;
    using FAME.Advantage.Domain.Users;
    using FAME.Advantage.Messages.Common;
    using FAME.Advantage.Messages.Enumerations;
    using FAME.Advantage.Messages.Lead.Notes;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    /// Business object to lead page
    /// </summary>
    public class LeadNotesBo
    {
        // ReSharper disable NotAccessedField.Local
        
        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepositoryWithTypedID<Guid> repository;

        // ReSharper restore NotAccessedField.Local
        
        /// <summary>
        ///  The repository integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryint;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadNotesBo"/> class. 
        /// Constructor Public
        /// </summary>
        /// <param name="leadRepository">
        /// the GUID Repository
        /// </param>
        /// <param name="repositoryWithInt">
        /// The INT repository
        /// </param>
        public LeadNotesBo(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = leadRepository;
            this.repositoryint = repositoryWithInt;
        }

        /// <summary>
        /// Return the list of notes associates to page notes
        /// </summary>
        /// <param name="filter">Filter LeadId, Module Code</param>
        /// <param name="authorizeConfidential">true authorize confidential</param>
        /// <returns>
        /// Only the page notes.
        /// </returns>
        /// <remarks>
        /// futures version should return other notes
        /// </remarks>
        public IList<NotesModel> GetLeadNotesList(LeadNotesInputFilter filter, bool authorizeConfidential)
        {
            IList<NotesModel> notes;
            var lead = this.repository.Query<Lead>().SingleOrDefault(x => x.ID == Guid.Parse(filter.LeadId));
            if (lead == null)
            {
                throw new ApplicationException("The Lead does not appear in your system. Please reload your page ");
            }

            if (authorizeConfidential)
            {
                notes =
                    lead.NotesList.Select(
                        info =>
                        new NotesModel
                            {
                                IdNote = info.ID, 
                                NoteText = info.NoteText, 
                                ModuleName = info.ModuleObj.Name, 
                                ModuleCode = info.ModuleObj.Code,
                                NoteDate = info.ModDate, 
                                UserName = info.UserObj.FullName, 
                                UserId = info.UserObj.ID.ToString(), 
                                Source = info.PageFieldsObj.PageName, 
                                Field = info.PageFieldsObj.FieldCaption, 
                                PageFieldId = info.PageFieldsObj.ID, 
                                Type = info.NoteType, 

                                // IsConfidential = info.IsConfidential
                            }).ToList();
            }
            else
            {
                notes =
                    lead.NotesList.Where(x => x.NoteType.ToUpperInvariant() != "CONFIDENTIAL")
                        .Select(
                            info =>
                            new NotesModel
                                {
                                    IdNote = info.ID, 
                                    NoteText = info.NoteText, 
                                    ModuleName = info.ModuleObj.Name,
                                    ModuleCode = info.ModuleObj.Code,
                                    NoteDate = info.ModDate, 
                                    UserName = info.UserObj.FullName, 
                                    UserId = info.UserObj.ID.ToString(), 
                                    Source = info.PageFieldsObj.PageName, 
                                    Field = info.PageFieldsObj.FieldCaption, 
                                    PageFieldId = info.PageFieldsObj.ID, 
                                    Type = info.NoteType, 

                                    // IsConfidential = info.IsConfidential
                                }).ToList();
            }

            return filter.ModuleCode != "ALL" 
                ? notes.Where(x => x.ModuleCode == filter.ModuleCode).OrderByDescending(x => x.NoteDate).ToList() 
                : notes.OrderByDescending(x => x.NoteDate).ToList();
        }

        /// <summary>
        /// Get Comments field from Skill Page
        /// </summary>
        /// <param name="filter">
        /// Filter LeadId
        /// </param>
        /// <returns>List o notes of skills</returns>
        public IList<NotesModel> GetSkillNotes(LeadNotesInputFilter filter)
        {
            var lead = this.repository.Query<Lead>().SingleOrDefault(x => x.ID == Guid.Parse(filter.LeadId));
            if (lead == null)
            {
                throw new ApplicationException("The Lead does not appear in your system. Please reload your page ");
            }

            if (filter.ModuleCode != "AD" & filter.ModuleCode != "PL" & filter.ModuleCode != "ALL")
            {
                // Return empty list if other module is invoked
                return new List<NotesModel>();
            }

            var pageField =
                this.repositoryint.Query<NotesPageFields>()
                    .SingleOrDefault(x => x.ID == (int)Enumerations.EnumNotePageField.SkillComment);
            IList<NotesModel> notes =
                this.repositoryint.Query<AdSkills>()
                    .Where(x => x.LeadObj.ID.ToString() == filter.LeadId)
                    .Select(
                        info =>
                        new NotesModel
                            {
                                IdNote = info.ID, 
                                NoteText = info.Comment, 
                                ModuleName = this.GetModuleString(filter.ModuleCode, "Placement"), 
                                NoteDate = info.ModDate, 
                                UserName = info.ModUser, 
                                UserId = Guid.Empty.ToString(), 
                                Source = pageField.PageName, 
                                Field = pageField.FieldCaption, 
                                PageFieldId = pageField.ID, 
                                Type = info.Description, 

                                // IsConfidential = 0
                            }).ToList();

            return notes;
        }

        /// <summary>
        /// Get all extracurricular notes
        /// </summary>
        /// <param name="filter">The filter LeadId</param>
        /// <returns>
        /// List of NotesModel
        /// </returns>
        public IList<NotesModel> GetExtraCurricularNotes(LeadNotesInputFilter filter)
        {
            var lead = this.repository.Query<Lead>().SingleOrDefault(x => x.ID == Guid.Parse(filter.LeadId));
            if (lead == null)
            {
                throw new ApplicationException("The Lead does not appear in your system. Please reload your page ");
            }

            if (filter.ModuleCode != "AD" & filter.ModuleCode != "PL" & filter.ModuleCode != "ALL")
            {
                // Return empty list if other module is invoked
                return new List<NotesModel>();
            }

            var pageField =
                this.repositoryint.Query<NotesPageFields>()
                    .SingleOrDefault(x => x.ID == (int)Enumerations.EnumNotePageField.ExtraCurricularComment);
            IList<NotesModel> notes =
                this.repositoryint.Query<ExtraCurriculars>()
                    .Where(x => x.LeadObj.ID.ToString() == filter.LeadId)
                    .Select(
                        info =>
                        new NotesModel
                            {
                                IdNote = info.ID, 
                                NoteText = info.ExtraCurrComments, 
                                ModuleName = this.GetModuleString(filter.ModuleCode, "Placement"), 
                                NoteDate = info.ModDate, 
                                UserName = info.ModUser, 
                                UserId = Guid.Empty.ToString(), 
                                Source = pageField.PageName, 
                                Field = pageField.FieldCaption, 
                                PageFieldId = pageField.ID, 
                                Type = info.ExtraCurrDescription, 
                            })
                    .ToList();

            return notes;
        }

        /// <summary>
        /// The get prior work comments list.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;NotesModel&gt;"/>.
        /// </returns>
        public IList<NotesModel> GetPriorWorkCommentsList(LeadNotesInputFilter filter)
        {
            if (filter.ModuleCode != "AD" & filter.ModuleCode != "PL" & filter.ModuleCode != "ALL")
            {
                // Return empty list if other module is invoked
                return new List<NotesModel>();
            }

            var pageField =
               this.repositoryint.Query<NotesPageFields>()
                   .SingleOrDefault(x => x.ID == (int)Enumerations.EnumNotePageField.PriorWorkComment);
          
            // Only get the information if you are in placement or in Admission
            var prior =
                this.repository.Query<AdLeadEmployment>()
                    .Where(x => x.LeadObj.ID.ToString() == filter.LeadId)
                    .Select(
                        info =>
                            new NotesModel()
                                {
                                    NoteText = info.Comments,
                                    NoteDate = info.ModDate,
                                    ModuleName = this.GetModuleString(filter.ModuleCode, "Placement"),
                                    UserId = filter.UserId,
                                    Field = pageField.FieldCaption,
                                    PageFieldId = pageField.ID,
                                    ModuleCode = filter.ModuleCode,
                                    UserName = info.ModUser,
                                    Source = pageField.PageName,
                                    Type = "Comment",
                                }).ToList();
            var output = new List<NotesModel>();
            foreach (NotesModel model in prior)
            {
                if (string.IsNullOrEmpty(model.NoteText))
                {
                    continue;
                }

                model.UserName = this.GetUserAndName(model.UserName);
                output.Add(model);
            }

            return output;
        }

        /// <summary>
        /// Get the list of overrides 
        /// </summary>
        /// <param name="filter">The filter LeadId</param>
        /// <param name="initCount">Not Used</param>
        /// <returns>List of Notes</returns>
        public IList<NotesModel> GetRequirementsOverrideReasonsList(LeadNotesInputFilter filter, int initCount = 1)
        {

            if (filter.ModuleCode.ToUpper().Trim() != "AD")
            {
                return new List<NotesModel>();
            }

            var lead = this.repository.Query<Lead>().SingleOrDefault(x => x.ID == Guid.Parse(filter.LeadId));
            if (lead == null)
            {
                throw new ApplicationException("The Lead does not appear in your system. Please reload your page ");
            }
            // var count = initCount;
            var pageField =
                this.repositoryint.Query<NotesPageFields>()
                    .SingleOrDefault(x => x.ID == (int)Enumerations.EnumNotePageField.RequirementsOverride);
            var notes =
                this.repository.Query<AdLeadTranReceived>()
                    .Where(
                        x =>
                        (x.Lead.ID.ToString() == filter.LeadId) && x.Override
                        && !(x.OverrideReason == null || x.OverrideReason.Equals(string.Empty)))
                    .Select(
                        info => new NotesModel
                                    {
                                        // IdNote = 1,
                                        NoteText = info.OverrideReason, 
                                        ModuleName = "Admissions", 
                                        NoteDate = info.ModDate.GetValueOrDefault(DateTime.Now), 
                                        UserName = info.ModUser, 
                                        UserId = Guid.Empty.ToString(), 
                                        Source = pageField.PageName, 
                                        Field = pageField.FieldCaption, 
                                        PageFieldId = pageField.ID, 
                                        Type = info.Requirement.Type.Description, 
                                    });

            var notes2 =
                this.repository.Query<AdLeadReqsReceived>()
                    .Where(
                        x =>
                        (x.Lead.ID.ToString() == filter.LeadId) && x.Override
                        && !(x.OverrideReason == null || x.OverrideReason.Equals(string.Empty)))
                    .Select(
                        info => new NotesModel
                                    {
                                        // IdNote = 1,
                                        NoteText = info.OverrideReason, 
                                        ModuleName = "Admissions", 
                                        NoteDate = info.ModDate.GetValueOrDefault(DateTime.Now), 
                                        UserName = info.ModUser, 
                                        UserId = Guid.Empty.ToString(), 
                                        Source = pageField.PageName, 
                                        Field = pageField.FieldCaption, 
                                        PageFieldId = pageField.ID, 
                                        Type = info.Requirement.Type.Description, 
                                    });

            var notes3 =
                this.repository.Query<LeadEntranceTest>()
                    .Where(
                        x =>
                        (x.LeadObj.ID.ToString() == filter.LeadId) && !(x.Override == null || x.Override == false)
                        && !(x.OverrideReason == null || x.OverrideReason.Equals(string.Empty)))
                    .Select(
                        info => new NotesModel
                                    {
                                        // IdNote = 1,
                                        NoteText = info.OverrideReason, 
                                        ModuleName = "Admissions", 
                                        NoteDate = info.ModDate.GetValueOrDefault(DateTime.Now), 
                                        UserName = info.ModUser, 
                                        UserId = Guid.Empty.ToString(), 
                                        Source = pageField.PageName, 
                                        Field = pageField.FieldCaption, 
                                        PageFieldId = pageField.ID, 
                                        Type = info.Requirement.Type.Description, 
                                    });

            var notes4 =
                this.repository.Query<LeadDocument>()
                    .Where(
                        x =>
                        (x.Lead.ID.ToString() == filter.LeadId) && x.Override
                        && !(x.OverrideReason == null || x.OverrideReason == string.Empty))
                    .Select(
                        info => new NotesModel
                                    {
                                        // IdNote = 1,
                                        NoteText = info.OverrideReason, 
                                        ModuleName = "Admissions", 
                                        NoteDate = info.ModDate, 
                                        UserName = info.ModUser, 
                                        UserId = Guid.Empty.ToString(), 
                                        Source = pageField.PageName, 
                                        Field = pageField.FieldCaption, 
                                        PageFieldId = pageField.ID, 
                                        Type = info.Requirement.Type.Description, 
                                    });

            IList<NotesModel> output = notes.ToList();
            foreach (NotesModel model in notes2)
            {
                output.Add(model);
            }

            foreach (NotesModel model in notes3)
            {
                output.Add(model);
            }

            foreach (NotesModel model in notes4)
            {
                output.Add(model);
            }

            return output;
        }

        /// <summary>
        /// Get task and appointment from Task tables
        /// </summary>
        /// <param name="filter">The filter LeadId</param>
        /// <returns>The Notes Model list</returns>
        public IEnumerable<NotesModel> GetTaskAndAppointmentList(LeadNotesInputFilter filter)
        {
            var lead = this.repository.Query<Lead>().SingleOrDefault(x => x.ID == Guid.Parse(filter.LeadId));
            if (lead == null)
            {
                throw new ApplicationException("The Lead does not appear in your system. Please reload your page ");
            }

            var pageField =
                this.repositoryint.Query<NotesPageFields>()
                    .SingleOrDefault(x => x.ID == (int)Enumerations.EnumNotePageField.TaskMessage);

            IList<NotesModel> notes =
                this.repository.Query<ViewTaskNotes>()
                    
                   // note In the future we need to add here the module to filter per module. but now they are a legacy defect in TASK to avoid that
                    .Where(x => x.LeadObj.ID.ToString() == filter.LeadId)
                    .Select(
                        info =>
                        new NotesModel
                            {
                                IdNote = 1, 
                                NoteText = info.Message, 
                                ModuleName = "Admissions", 
                                NoteDate = info.ModDate.GetValueOrDefault(DateTime.Now), 
                                UserName = info.UserObj.UserName, 
                                UserId = info.UserObj.ID.ToString(), 
                                Source = pageField.PageName, 
                                Field = pageField.FieldCaption, 
                                PageFieldId = pageField.ID, 
                                Type = info.Description, 
                            })
                    .ToList();

            return notes;
        }

        /// <summary>
        /// Get list of messages sent or printed to lead
        /// </summary>
        /// <param name="filter">The filter LeadId</param>
        /// <returns>The NotesModel</returns>
        public IEnumerable<NotesModel> GetLeadMessagesList(LeadNotesInputFilter filter)
        {
            var lead = this.repository.Query<Lead>().SingleOrDefault(x => x.ID == Guid.Parse(filter.LeadId));
            if (lead == null)
            {
                throw new ApplicationException("The Lead does not appear in your system. Please reload your page ");
            }

            var pageField =
                this.repositoryint.Query<NotesPageFields>()
                    .SingleOrDefault(x => x.ID == (int)Enumerations.EnumNotePageField.EmailTemplate);

            IList<NotesModel> notes =
                this.repository.Query<ViewMessages>()
                    .Where(x => x.RecipientObj.ID.ToString() == filter.LeadId)
                    .Select(
                        info =>
                        new NotesModel
                            {
                                IdNote = 1, 
                                NoteText = info.TemplateObj.Descrip, 
                                ModuleName = "Admissions", 
                                NoteDate = info.DateDelivered, 
                                UserName = info.FromObj.UserName, 
                                UserId = info.FromObj.ID.ToString(), 
                                Source = pageField.PageName, 
                                Field = pageField.FieldCaption, 
                                PageFieldId = pageField.ID, 
                                Type = info.DeliveryType, 
                            })
                    .ToList();

            return notes;
        }

        /// <summary>
        /// Get the list of modules description / code
        /// </summary>
        /// <param name="filter">Not Used</param>
        /// <returns>The Drop down list of Modules : Name and Code</returns>
        public IList<DropDownOutputModel> GetModulesItemList(LeadNotesInputFilter filter)
        {
            var moduls =
                this.repositoryint.Query<SystemModule>()
                    .Select(info => new DropDownOutputModel { Description = info.Name, ID = info.Code });

            return moduls.OrderBy(x => x.Description).ToList();
        }

        /// <summary>
        /// Update the notes list
        /// </summary>
        /// <param name="filter">
        /// The filter
        /// </param>
        public void UpdateLeadNotesList(NotesOutputModel filter)
        {
            this.ValidateOperation(filter);

            foreach (NotesModel info in filter.NotesList)
            {
                var allnote = this.repositoryint.Query<AllNotes>().SingleOrDefault(x => x.ID == info.IdNote);
                if (allnote != null)
                {
                    // var noteType = repositoryint.Query<NotesType>().SingleOrDefault(x => x.ID == info.Type.ID);
                    var user =
                        this.repository.Query<User>().SingleOrDefault(x => x.ID.ToString() == filter.Filter.UserId);
                    if (user == null)
                    {
                        throw new ApplicationException("filter value for user is wrong!");
                    }

                    allnote.Update(info.NoteText, user.UserName, info.Type, user);
                    this.repositoryint.UpdateAndFlush(allnote);
                }
                else
                {
                    throw new ApplicationException("The Extracurricular info selected does not exists in the database");
                }
            }
        }

        /// <summary>
        /// Insert a notes in notes table
        /// </summary>
        /// <param name="filter">
        /// The filter <see cref="INotesOutputModel"/>.
        /// </param>
        /// <returns>
        /// The <see cref="INotesOutputModel"/>.
        /// </returns>
        public INotesOutputModel InsertNotesList(NotesOutputModel filter)
        {
            var outputList = NotesOutputModel.Factory();

            foreach (NotesModel info in filter.NotesList)
            {
                var lead = this.repository.Query<Lead>().SingleOrDefault(x => x.ID.ToString() == filter.Filter.LeadId);

                if (lead != null)
                {
                    // var noteType = repositoryint.Query<NotesType>().SingleOrDefault(x => x.ID == info.Type.ID);
                    var user =
                        this.repository.Query<User>().SingleOrDefault(x => x.ID.ToString() == filter.Filter.UserId);
                    var module = this.repositoryint.Query<SystemModule>()
                        .SingleOrDefault(x => x.Name == info.ModuleName);
                    var pageField =
                        this.repositoryint.Query<NotesPageFields>().SingleOrDefault(x => x.ID == info.PageFieldId);
                    if (module == null || user == null || pageField == null)
                    {
                        throw new ApplicationException("A filter value has a invalid value. Check module, user, or Page-Field");
                    }

                    var nnote = new AllNotes(module, info.Type, pageField, user, info.NoteText, user.UserName);
                    lead.NotesList.Add(nnote);
                    this.repository.SaveAndFlush(lead);
                    Debug.WriteLine("Lead Note Id {0}", nnote.ID);
                    var newRecord = this.repositoryint.Query<AllNotes>().SingleOrDefault(x => x.ID == nnote.ID);
                    if (newRecord != null)
                    {
                        var output = new NotesModel
                                         {
                                             IdNote = newRecord.ID, 
                                             NoteText = newRecord.NoteText, 
                                             ModuleName = newRecord.ModuleObj.Name, 
                                             NoteDate = newRecord.ModDate, 
                                             UserName = newRecord.UserObj.FullName, 
                                             UserId = newRecord.UserObj.ID.ToString(), 
                                             Source = newRecord.PageFieldsObj.PageName, 
                                             Field = newRecord.PageFieldsObj.FieldCaption, 
                                             PageFieldId = newRecord.PageFieldsObj.ID, 
                                             Type = newRecord.NoteType, 
                                         };

                        outputList.NotesList.Add(output);
                    }
                }
                else
                {
                    throw new ApplicationException("The Given Lead does not exists in the database");
                }
            }

            return outputList;
        }

        /// <summary>
        ///  The validate operation.
        /// </summary>
        /// <param name="filter">
        ///  The filter UserId.
        /// </param>
        /// <exception cref="HttpInformativeException">
        /// Exception: You can not modify notes from other user
        /// </exception>
        private void ValidateOperation(NotesOutputModel filter)
        {
            foreach (NotesModel model in filter.NotesList)
            {
                if (!string.Equals(model.UserId, filter.Filter.UserId, StringComparison.CurrentCultureIgnoreCase))
                {
                    throw new ApplicationException("You can not modify notes from other user");
                }
            }
        }

        /// <summary>
        /// The get module string.
        /// </summary>
        /// <param name="moduleCode">
        /// The module code.
        /// </param>
        /// <param name="allDefault">
        /// The all Default.
        /// The value to be set if the combo-box filter in notes is ALL
        /// </param>
        /// <returns>
        /// The <see cref="string"/> name of the module.
        /// </returns>
        private string GetModuleString(string moduleCode, string allDefault)
        {
            switch (moduleCode)
            {
                case "AD":
                    {
                        return "Admission";
                    }

                case "PL":
                    {
                        return "Placement";
                    }

                case "AR":
                    {
                        return "Academics";
                    }

                case "ALL":
                    {
                        return allDefault;
                    }

                default:
                    {
                        return "Not defined";
                    }
            }
        }

        /// <summary>
        /// The get user and name.
        /// </summary>
        /// <param name="infoModUser">
        /// The info mod user.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetUserAndName(string infoModUser)
        {
            if (string.IsNullOrWhiteSpace(infoModUser))
            {
                return string.Empty;
            }

            var user = this.repository.Query<User>().SingleOrDefault(x => x.UserName.ToLowerInvariant() == infoModUser.ToLowerInvariant());
            if (user == null)
            {
                return string.Empty;
            }

            return user.FullName;
        }
    }
}
