﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadDuplicatesBo.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Business Objects for duplicated leads
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Domain.Infrastructure.Entities;
    using Domain.Lead;
    using Domain.SystemStuff;
    using Domain.Users;

    using Messages.Lead;

    /// <summary>
    /// Business Objects for duplicated leads
    /// </summary>
    public class LeadDuplicatesBo
    {
        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepositoryWithTypedID<Guid> repository;

        /// <summary>
        /// The repository integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryint;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadDuplicatesBo"/> class. 
        /// Constructor Public
        /// </summary>
        /// <param name="leadRepository">
        /// the GUID Repository
        /// </param>
        /// <param name="repositoryWithInt">
        /// The INT repository
        /// </param>
        public LeadDuplicatesBo(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = leadRepository;
            this.repositoryint = repositoryWithInt;
        }

        /// <summary>
        /// Get a list of new lead with possible duplicates.
        /// </summary>
        /// <returns>
        /// The list of possible duplicates
        /// </returns>
        public IEnumerable<LeadNewDuplicatesOutputModel> GetNewDuplicatesList()
        {
            var duplicatelist = this.repositoryint.Query<AdLeadDuplicates>().OrderBy(x => x.NewLeadObj.LastName).ThenBy(x => x.NewLeadObj.FirstName);

            var list = duplicatelist.Select(dup => new LeadNewDuplicatesOutputModel
            {
                NewLeadGuid = dup.NewLeadObj.ID,
                DuplicateGuid = dup.PosibleLeadDuplicateObj.ID,
                PosibleDuplicateFullName = string.Format("{0}, {1}", dup.NewLeadObj.LastName, dup.NewLeadObj.FirstName)
            }).ToList();

            return list;
        }

        /// <summary>
        /// Get the header list, the new lead, and it first possible duplicate.
        /// </summary>
        /// <param name="filter">
        /// The new lead and the possible duplicate lead
        /// </param>
        /// <returns>
        /// The output model for duplicates
        /// </returns>
        public IEnumerable<LeadDuplicatesOutputModel> GetDuplicate(LeadDuplicatesInputFilter filter)
        {
            var list = new List<LeadDuplicatesOutputModel>();
            var newlead = this.repository.Query<Lead>().FirstOrDefault(x => x.ID == new Guid(filter.LeadNewGuid));
            var duplead = this.repository.Query<Lead>().FirstOrDefault(x => x.ID == new Guid(filter.LeadDuplicateGuid));
            list.Add(this.GetLabelsModel());
            var om1 = Mapper.Map<LeadDuplicatesOutputModel>(newlead);
            var om2 = Mapper.Map<LeadDuplicatesOutputModel>(duplead);
            om1.Header = "Lead A";
            om2.Header = "Lead B";
            om1 = LeadDuplicatesOutputModel.CleanNull(om1);
            om2 = LeadDuplicatesOutputModel.CleanNull(om2);

            list.Add(om1);
            list.Add(om2);
            return list;
        }

        /// <summary>
        /// Execute duplicate command
        /// </summary>
        /// <param name="filter">
        /// The lead and possible duplicates ID
        /// </param>
        /// <returns>
        /// The list of duplicates output model
        /// </returns>
        /// <exception cref="ApplicationException">
        /// Lead does not exists in DB. Operation was canceled
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Command not recognized
        /// </exception>
        public IEnumerable<LeadNewDuplicatesOutputModel> ExecuteDuplicateCommand(LeadDuplicatesInputFilter filter)
        {
            // Filter by command
            switch (filter.Command)
            {
                case 1:
                    {
                        // Import information from A to B, disregard A
                        var user = this.repository.Query<User>().FirstOrDefault(n => n.ID.ToString() == filter.UserId);
                        var leadA = this.repository.Query<Lead>()
                                .FirstOrDefault(x => x.ID == new Guid(filter.LeadNewGuid));
                        var leadB = this.repository.Query<Lead>()
                                .FirstOrDefault(x => x.ID == new Guid(filter.LeadDuplicateGuid));
                        if (leadA != null & leadB != null)
                        {
                            // Update all fields
                            if (user != null)
                            {
                                leadB.UpdateFieldsDuplicates(leadA, user.UserName);
                            }

                            // Mark A as Duplicated
                            leadA = this.SetLeadStatus(leadA, "DUPLICATED");

                            // for all MRU LeadsId = A replace MRU.LeadsId for MRU BLeadsId
                            // Save all
                            var list = new List<Lead> { leadA, leadB };
                            this.repository.SaveAll<Lead>(list);
                           
                            // Clean from Duplicates table leadA
                            this.DeleteUsedDuplicatedFromDuplicateTable(filter.LeadNewGuid);
                        }
                        else
                        {
                            throw new ApplicationException("Lead does not exists in Db. Operation was canceled");
                        }

                        break;
                    }

                case 2:
                    {
                        // Disregards A
                        var leadA = this.repository.Query<Lead>().FirstOrDefault(x => x.ID == new Guid(filter.LeadNewGuid));
                        if (leadA != null)
                        {
                            var leadStatus =
                                this.repository.Query<UserDefStatusCode>()
                                    .FirstOrDefault(x => x.SystemStatus.Description.ToUpper() == "DUPLICATED");
                            leadA.ChangeLeadStatus(leadStatus);
                            this.repository.SaveAndFlush(leadA);

                            // Delete the occurrence of LeadA in duplicate grid
                            var list =
                                this.repositoryint.Query<AdLeadDuplicates>()
                                    .Where(
                                        x =>
                                        x.NewLeadObj.ID == new Guid(filter.LeadNewGuid)
                                        || x.PosibleLeadDuplicateObj.ID == new Guid(filter.LeadNewGuid));
                            foreach (var leadDuplicatese in list)
                            {
                                this.repositoryint.Delete(leadDuplicatese);
                            }
                        }
                        else
                        {
                            throw new ApplicationException("Lead does not exists in Db. Operation was canceled");
                        }

                        break;
                    }

                case 3:
                    {
                        // Select Lead A
                        var leadA = this.repository.Query<Lead>().FirstOrDefault(x => x.ID == new Guid(filter.LeadNewGuid));
                        var leadB = this.repository.Query<Lead>().FirstOrDefault(x => x.ID == new Guid(filter.LeadDuplicateGuid));
                        if (leadA != null & leadB != null)
                        {
                            // Put A as New Lead
                            leadA = this.SetLeadStatus(leadA, "IMPORTED");

                            // Put B as Duplicated Lead is actual status is imported if not don't change it.
                            if (leadB.LeadStatus.SystemStatus.Description.ToUpper() == "IMPORTED")
                            {
                                leadB = this.SetLeadStatus(leadB, "DUPLICATED");
                            }

                            // Update repository table
                            var list = new List<Lead> { leadA, leadB };
                            this.repository.SaveAll<Lead>(list);
                           
                            // Clean List Duplicates
                            this.DeleteUsedDuplicatedFromDuplicateTable(filter.LeadNewGuid);
                        }
                        else
                        {
                            throw new ApplicationException("Lead does not exists in Db. Operation was canceled");
                        }

                        break;
                    }

                case 4:
                    {
                        // Select Lead B
                        // Select B and disregards A
                        var leadA = this.repository.Query<Lead>().FirstOrDefault(x => x.ID == new Guid(filter.LeadNewGuid));
                        var leadB = this.repository.Query<Lead>().FirstOrDefault(x => x.ID == new Guid(filter.LeadDuplicateGuid));
                        if (leadA != null & leadB != null)
                        {
                            // Disregard Lead A (put it to DUPLICATED)
                            leadA = this.SetLeadStatus(leadA, "DUPLICATED");

                            // Update repository table
                            var list = new List<Lead> { leadA, leadB };
                            this.repository.SaveAll<Lead>(list);

                            // Clean List Duplicates
                            this.DeleteUsedDuplicatedFromDuplicateTable(filter.LeadNewGuid);
                        }
                        else
                        {
                            throw new ApplicationException("Lead does not exists in Db. Operation was canceled");
                        }

                        break;
                    }

                default:
                    {
                        throw new Exception("Command not recognized");
                    }
            }

            return this.GetNewDuplicatesList();
        }

        /// <summary>
        ///  The set lead status.
        /// </summary>
        /// <param name="lead">
        ///  The lead.
        /// </param>
        /// <param name="statusCode">
        ///  The status code.
        /// </param>
        /// <returns>
        /// The <see cref="Lead"/>.
        /// </returns>
        private Lead SetLeadStatus(Lead lead, string statusCode)
        {
            var leadStatus = this.repository.Query<UserDefStatusCode>()
                               .FirstOrDefault(x => x.SystemStatus.Description.ToUpper() == statusCode);
            lead.ChangeLeadStatus(leadStatus);
            return lead;
        }

        /// <summary>
        /// The delete used duplicated from duplicate table.
        /// </summary>
        /// <param name="leadGuid">
        /// The lead GUID.
        /// </param>
        private void DeleteUsedDuplicatedFromDuplicateTable(string leadGuid)
        {
            var list = this.repositoryint.Query<AdLeadDuplicates>()
                                .Where(
                                    x =>
                                        x.NewLeadObj.ID == new Guid(leadGuid) ||
                                        x.PosibleLeadDuplicateObj.ID == new Guid(leadGuid));
            foreach (var leadDuplicatese in list)
            {
                this.repositoryint.Delete(leadDuplicatese);
            }
        }

        /// <summary>
        ///  The get labels model.
        ///  Labels to be show in the page
        /// </summary>
        /// <returns>
        /// The <see cref="LeadDuplicatesOutputModel"/>.
        /// </returns>
        private LeadDuplicatesOutputModel GetLabelsModel()
        {
            var label = new LeadDuplicatesOutputModel
            {
                Header = "Details:", 
                AcquiredDate = "Acquired:", 
                From = "From:", 
                FullName = "Full Name:", 
                Address1 = "Address 1:", 
                Address2 = "Address 2:", 
                City = "City:", 
                State = "State:", 
                Zip = "Zip:", 
                Status = "Status:", 
                Phone = "Phone:", 
                Email = "Email:", 
                SourceInfo = "Source Info:", 
                AdmissionsRep = "Admission Rep:", 
                Campus = "Campus", 
                ProgramOfInterest = "Program of Interest:", 
                Comments = "Comments:", 
                CampusOfInterest = "Campus of Interest:", 
                Id = Guid.Empty, 
            };
            return label;
        }
    }
}
