﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadToolBarBo.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Lead Tool Bar Business Object
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Domain.Infrastructure.Entities;
    using Domain.Lead;
    using Messages.Lead.ToolBar;

    /// <summary>
    /// Lead Tool Bar Business Object
    /// </summary>
    public class LeadToolBarBo
    {
        /// <summary>
        ///  No additional lead information message
        /// </summary>
        private const string XNoAdditionalLeadInformation = "No additional leads"; 

        /// <summary>
        /// No lead information.
        /// </summary>
        private const string XNoLeadInformation = "No Lead Information";
 
        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepository repository;
        
        // ReSharper disable NotAccessedField.Local

        /// <summary>
        /// The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        // ReSharper restore NotAccessedField.Local

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadToolBarBo"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="repository">
        /// The repository GUID
        /// </param>
        /// <param name="repositoryWithInt">
        /// The repository with integer
        /// </param>
        public LeadToolBarBo(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            this.repositoryWithInt = repositoryWithInt;
        }

        /// <summary>
        /// Get the alert flags status for lead task and appointment
        /// </summary>
        /// <param name="filter">
        /// filters users and status
        /// </param>
        /// <returns>
        /// The lead information output model
        /// </returns>
        public LeadInfoToolOutputModel GetAlertFlags(LeadInfoToolInputModel filter)
        {
            var output = new LeadInfoToolOutputModel();
            var flaglead = this.repository.Query<Lead>()
                    .Any(x => x.AdmissionRepUserObj.ID.ToString() == filter.UserId && x.LeadStatus.SystemStatus.ID == 1);
            output.FlagLead = flaglead;

            var tasks = this.repository.Query<LeadTaskView>()
                    .Where(x => x.LeadsObj.AdmissionRepUserObj.ID.ToString() == filter.UserId && x.StartDate <= DateTime.Now && x.Status == 0);

            output.FlagAppointment = tasks.Any(x => x.Code == "Appointment");
            output.FlagTask = tasks.Any(x => x.Code != "Appointment");

            return output;
        }

        /// <summary>
        /// Get the next lead in the Queue if exists....
        /// </summary>
        /// <param name="filter">Previous lead ID</param>
        /// <returns>The selected lead id</returns>
        public string GetNextLeadInMru(LeadInfoToolInputModel filter)
        {
            IList<LeadInformation> leads =
                this.LeadInformations(filter).OrderByDescending(x => x.AdmissionDate).ToList();

            var index =
                ((List<LeadInformation>)leads).FindIndex(
                    x => x.Id.Equals(filter.LeadId, StringComparison.InvariantCultureIgnoreCase));

            if (index == -1)
            {
                // The actual Lead does not exists in your QUEUE, then take the more recent
                var lead = leads[0];
                return lead.Id;
            }

            // Select the most near element by date
            if (index == 0)
            {
                throw new ApplicationException(LeadToolBarBo.XNoAdditionalLeadInformation);
            }

            var lad = leads[index - 1];
            return lad.Id;
        }

        /// <summary>
        /// Get the previous lead in the queue if exists.
        /// </summary>
        /// <param name="filter">Next Lead ID in MRU</param>
        /// <returns>The lead </returns>
        public string GetPreviousLeadInMru(LeadInfoToolInputModel filter)
        {
            IList<LeadInformation> leads = this.LeadInformations(filter).OrderByDescending(x => x.AdmissionDate).ToList();
            var index = ((List<LeadInformation>)leads).FindIndex(x => x.Id.Equals(filter.LeadId, StringComparison.InvariantCultureIgnoreCase));
            if (index == -1)
            {
                // The actual Lead does not exists in your QUEUE, then take the more older
                var lead = leads[leads.Count - 1];
                return lead.Id;
            }

            // Select the most near element by date
            if (index == leads.Count - 1)
            {
                throw new ApplicationException(LeadToolBarBo.XNoAdditionalLeadInformation);
            }

            var lad = leads[index + 1];
            return lad.Id;
        }

        /// <summary>
        /// Get the Lead Queue. Return special local class LeadInformation only.
        /// </summary>
        /// <param name="filter">
        /// User ID
        /// </param>
        /// <returns>A list of list information</returns>
        private IList<LeadInformation> LeadInformations(LeadInfoToolInputModel filter)
        {
            // Get the lead queue for the user.
            IList<LeadInformation> leads = this.repository.Query<Lead>().Where(x => x.AdmissionRepUserObj.ID.ToString() == filter.UserId
                                                                && (x.LeadStatus.SystemStatus.ID == 1
                                                                    || x.LeadStatus.SystemStatus.ID == 2
                                                                    || x.LeadStatus.SystemStatus.ID == 4
                                                                    || x.LeadStatus.SystemStatus.ID == 5)
                                                                && x.Campus.ID.ToString() == filter.CampusId)
                    .Select(lead => new LeadInformation { AdmissionDate = lead.AssignedDate.Value, Id = lead.ID.ToString() })
                    .ToList();

            if (leads.Count == 0)
            {
                throw new ApplicationException(LeadToolBarBo.XNoLeadInformation);
            }

            return leads;
        }

        /// <summary>
        /// Private class to optimize the query
        /// </summary>
        private class LeadInformation
        {
            /// <summary>
            /// Gets or sets the id.
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// Gets or sets the admission date.
            /// </summary>
            public DateTime AdmissionDate { get; set; }
        }
    }
}
