﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageBo.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The image bo.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;

    using Domain.Infrastructure.Entities;
    using Domain.Lead;
    using Domain.Users;
    using Messages.Lead.Image;
    using Services.Lib.Formatters.MultipartDataMediaFormatter.Infrastructure;

    /// <summary>
    /// The image bo.
    /// </summary>
    public class ImageBo
    {
        /// <summary>
        /// The repository.
        /// </summary>
        private IRepository repository;

        /// <summary>
        /// The repository with integer.
        /// </summary>
        private IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageBo"/> class.
        /// </summary>
        /// <param name="repository">
        ///  The repository.
        /// </param>
        /// <param name="repositoryWithInt">
        /// The repository with integer.
        /// </param>
        public ImageBo(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            this.repositoryWithInt = repositoryWithInt;
        }

        /// <summary>
        ///  The get image.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <returns>
        /// The <see cref="ImageOutputModel"/>.
        /// </returns>
        public ImageOutputModel GetImage(ImageInputModel filter)
        {
            // Get the image from the DB
            var imageArray =
                this.repositoryWithInt.Query<LeadImage>()
                    .SingleOrDefault(x => x.LeadObj.ID.ToString() == filter.EntityId && x.Type == 1);
            if (imageArray == null)
            {
                var output = new ImageOutputModel
                {
                    ImageBase64 = @"iVBORw0KGgoAAAANSUhEUgAAAEsAAABLCAIAAAC3LO29AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAiESURBVHhe3Vv3V1RHFLa32GJijJrkaIrR5KScREVBRFFEooKFqLGXKL1JU0BEDKIICIoiAgIqKiogAoKgossiUpe6LM0FFlg6WP6AfGZPzJ5F3PfmzS5Gzh5+ujPvfnP7nTsDO5+9HPB+/wHh+/0boGp4HT1vPsGOnhctnc/wX9UMqBBhe8+Lgor6a2l5Men5d7JKc8qe5gnrHuSJkvklmYJqUV1LZOLjlOyy9BxhWW0TiFUEVVUIxc0daU+E+4Pil5ifXGoRsMYxeI/XJVPvK5tcw1c7BG/3iHQ+Faf9p6+xc8iGA6EOATeDYx/eflQsFDdTh0oZYcezl61dz7KKa0Nv8dcfCJ1i4DRgtonS3xANs0nLHJaaB/heSq9qaG3rfk5RnjQRwqia2rtzy8VG9mc/WrJv0BxTpdjkCQbNNf1U3zEsgV9Y2dDY1k3LRGkiBFup2eU6Jn4jtSwHMhBdb/yD5piMWmAFYd5Iz6+XdrZ3UzBOOghx3tmlT12DE37Y6AkWB7KUnjxUrP1A23raKhc9i0C/6HTg5KixFBC2db24eb9gq/uF6YauQ+eZs9LMvoiBc7S2tbFTCDxwPyOE64t9UGjsfO4TPQcq2OQ3+Xad+9ELd/4Jm+RpCScZAh78+ya3sI+X2lOHhw1Halkh0lTWt3AJIZwQSjt6whP4X692UwU82Z4zjQ8l8UsQgZjrqoLAyRHC/MpqmgysT43VsVEdwknLHO38r8NLM0eoQPkGhAyVvrS2yfpEzJiF1gPnKI/pxEcweqH1ErOTdc0d5AgZ4lH4gKS168a9gikGzmzDOluocM5QVCQ6xAkAoZaW1DR6hiYhC2HLMVt6fGLyciekgdLOHjIxEmopigNkz2zZJaMfv9g2OiW3TkqoqIQI72SVwTzIOGa7Cqboe/lVRk5Nhkw2unG/4PsNh9nySkaPJM4jNFFUJ2XCWG8aQjuMTs35fMV+Mo7ZrkIev/vIxeKqRhn3bF0jIcKrd/O+NFJhoJc/BbjTBbt9npQSJqiECBN5xZq7fNhKg4x+2HwLfctTeeVitWppyuOyperyNMM1LTfsDxWI6tWKELmizh5fMpmwXTVC03L7ociiyga1Ikx4VDRv5zG2vJLRj9Cy3OUJTyNRK8LEzGKt3WqyQ8hwm3uEoFK9CNH/1DXzJ5MJ21XD51ugtYXWq1plmJZTsdLuDFteyegRLZA/5ZSp15fyCqu3HLzApePEHC1kuM7pXIGcL2UV9AnjYXVDa1g8f+Y6dzRzmfNKQInic4yOjXdECnoZatVSdDIFogbTo9Eo8FVaAQ+bbz5jnfvDgsrmDnrVE8OjamjpissQ/LrVC3kjgXAYLhm3yHat4zkuXVNCLZWdAnr47iG3pxo4M2SXgGzqb86eYclNdPs0DGUIstau52jjz/rdg4B1hktmrD0Ye78AJsicK+WdKOZ7oXdSLWlD6B+iQafVrQB7hKbFYlP/2sa29v7qCL8SY+dzhA3cNDGUCSsyuLE1DsEc72c42aFM2ufjM3/Z4kW96TZBd5/WLp/DoUnMdeqNlBQQltY02gfcnG7kyko+byeGf15lFxQSy+MJqvofIThIzxUa7w8ZNJda9Id/9o9OJ+6Ryh8KBRliu+LqRrNj0bSu1iDen/74K/w2n6P0ZMvpIIzLKFxuFUh27/tGddU19UdPvR8Q9pXyImcMup7x8+YjVDK46YYuFj5XM/Ir+wFhX5/ECEb502aA1Njpzd3foJK4lpaPMNj/CBVEiss276hUtKiJIwfKMSQPx6NScS/CMQy+Ph1OdqiAEEnco8KqHzd6Ersc2U0TGnnElURvsXNC2Hu74moJRp7Iqg3YMCoJx8DYCtIGvqoivvy+RVUSdDfIEA7WMJtu5JZbXocrGCoWSDNavGZIUNmwzCoQ3TECfzNmoY2eZYCklesADc3aovdJoyOms9cPnRUChEjfN7uFU7RAFjJk3vnJF9bN3uo9jGhuaKKew16vS5jvoKii1HIaGU9II3FDNMv4EFl7CkM5Ow9HYvzy3UWICMYrrPrMwHkwUQo+dpHNCtvTaP+8uwhxSZuSVTZa24Ys4g+db/7deo9coZhLz4JdPGRufrJ9MRcccTuLwMe8XjJOx+ZIWBISQIYJDRMOaUb8rOKabYciuCBEhTlRz/5Y5F2MI9HSVWoIRfVSn4t3J+szGnt+yynAhjEhgKuY83GZQrEUOT1HqBQQQlXKxdKTV+7N3e49mNIMEWZo0PvZeTgqJI5Xgn5eF/nkN1eEbd0v7ueJ3IITAA/X0VxUVGEtpqE+1LXDtiZHL5+OeZCWUy6qbyFwQuQIca6Y4onPEOCkv1h5gMx/Kj0R1FPIkDBKu9ElFM3vyKTHSZklPEE1EmCM8zGRLQlCVEm1je2ZRdWnYjJwxhjsVsooFQLUVnjeoLnr+Ca3cNezCfDbeNKRJxRXiKWYJOyrbcUOoewpD84v6PrDRXv9yCI7FbSyTSbo2i3cc8L5dOytR0U1kjbkQ5i1xUCxvH9ihxAzz5ifX2zqN0HXHmMuFHkl2wqmATYw8z5luZPGjmPWvjFRydl5FXUSuecayhHiBQvsLYFX5HImfpllwFerXbGjem5/mcOGW0JROknf8Zu1B2E4hvvOHAiKT+SXII/vEyGyikJRA95leZxP3HwwXHvPCbTARi2wVJFHYQ5GKSVsBzKYZuiibxUIBVZECO8EhU7ml/pH34Ob1jU7iVcUZBWtUlZUTTBc0wJTzP8hhNBwO383u9zvcjp6LZjMVdGdmaqBKe4vy4lQWRdVSs7F8gysT6vN+6sJKuDhXUGmoAYznKiy1fRVondfhLwBYXRKzmIz//GLbPs9vhFiePt5IbWdt+M4+lzvWgCgh3br0f+pq2R6BO+v6P59ycP0JNTpG+h+671H+DcL5YizGcw8pwAAAABJRU5ErkJggg==",
                    ExtensionType = "image/png",
                    Filter = filter,
                    ImgLenth = 1550
                };
                return output;
            }
            else
            {
                var output = new ImageOutputModel
                {
                    ImageBase64 = Convert.ToBase64String(imageArray.Image),
                    Filter = filter,
                    ExtensionType = imageArray.MediaType,
                    ImgLenth = imageArray.ImgSize
                };
                return output;
            }
        }

        /// <summary>
        /// The store Image file from a file.
        /// </summary>
        /// <param name="formData">
        /// The form Data.
        /// </param>
        /// <param name="filter">
        /// The filter (entity Id (lead Id).
        /// </param>
        public void StoreImageFile(FormData formData, ImageInputModel filter)
        {
            // Get the user entity
            var user = this.repository.Get<User>(new Guid(filter.UserId));
            if (user == null)
            {
                throw new Exception("Invalid User-name");
            }

            // Admit more than one file, but this implementation only hold 1
            foreach (FormData.ValueFile valueFile in formData.Files)
            {
                HttpFile file;
                if (formData.TryGetValue(valueFile.Name, out file) == false)
                {
                    throw new ApplicationException("Please repeat the operation");
                }

                if (file.Buffer.Length > 30000)
                {
                    file.Buffer = this.CreateThumbNail(file.Buffer, file.MediaType);
                }

                // Test if the image exists in db
                var leadImg =
                    this.repositoryWithInt.Query<LeadImage>()
                        .SingleOrDefault(x => x.LeadObj.ID.ToString() == filter.EntityId && x.Type == 1);
                if (leadImg != null)
                {
                    // the photo exists update it
                    leadImg.UpdateImage(
                        leadImg.LeadObj,
                        file.Buffer,
                        file.FileName,
                        file.Buffer.Length,
                        file.MediaType,
                        1,
                        DateTime.Now,
                        user.UserName);
                    this.repositoryWithInt.Update(leadImg);
                }
                else
                {
                    // Insert the photo
                    var lead = this.repository.Get<Lead>(new Guid(filter.EntityId));
                    if (lead == null)
                    {
                        throw new Exception("Lead does not exists");
                    }

                    var image = new LeadImage(
                                    lead,
                                    file.Buffer,
                                    file.FileName,
                                    file.Buffer.Length,
                                    file.MediaType,
                                    1,
                                    DateTime.Now,
                                    user.UserName);

                    this.repositoryWithInt.Save(image);
                }
            }
        }

        /// <summary>
        /// The store base 64 image.
        /// </summary>
        /// <param name="data">
        ///  The data.
        /// </param>
        public void StoreBase64Image(ImageOutputModel data)
        {
            // Get the user entity
            var filter = data.Filter;
            var user = this.repository.Get<User>(new Guid(filter.UserId));
            if (user == null)
            {
                throw new Exception("Invalid User-name");
            }

            var lead = this.repository.Get<Lead>(new Guid(filter.EntityId));
            if (lead == null)
            {
                throw new Exception("Invalid Entity");
            }

            // Convert the image from Base 64 to byte[]
            if (data.ImageBase64.IndexOf(",", StringComparison.Ordinal) > 0)
            {
                // Extract the header
                char[] sep = { ',' };

                var part = data.ImageBase64.Split(sep, 2);
                data.ImageBase64 = part[1];
            }

            var img = this.CreateThumbNail(data.ImageBase64, data.ExtensionType);

            // Test if the image exists in db
            var leadImg =
                this.repositoryWithInt.Query<LeadImage>()
                    .SingleOrDefault(x => x.LeadObj.ID.ToString() == filter.EntityId && x.Type == 1);
            if (leadImg != null)
            {
                // Update
                // the photo exists update it
                leadImg.UpdateImage(
                    leadImg.LeadObj,
                    img,
                    lead.LastName + "_" + lead.FirstName,
                    img.Length,
                    data.ExtensionType,
                    1,
                    DateTime.Now,
                    user.UserName);
                this.repositoryWithInt.Update(leadImg);
            }
            else
            {
                // Insert the photo
                var image = new LeadImage(
                                lead,
                                img,
                                lead.LastName + "_" + lead.FirstName,
                                img.Length,
                                data.ExtensionType,
                                1,
                                DateTime.Now,
                                user.UserName);

                this.repositoryWithInt.Save(image);
            }
        }

        /// <summary>
        ///  The create thumbnail.
        /// </summary>
        /// <param name="imageArray">
        ///  The image array.
        /// </param>
        /// <param name="encoderType">
        ///  The encoder type.
        /// </param>
        /// <returns>
        /// The image in bytes array
        /// </returns>
        private byte[] CreateThumbNail(byte[] imageArray, string encoderType)
        {
            Image image;
            using (MemoryStream ms = new MemoryStream(imageArray))
            {
                image = Image.FromStream(ms);
            }

            var output = this.CreateThumbnailBase(image, encoderType);
            return output;
        }

        /// <summary>
        /// The create thumb nail.
        /// </summary>
        /// <param name="image64">
        /// The image 64.
        /// </param>
        /// <param name="encoderType">
        /// The encoder type.
        /// </param>
        /// <returns>
        /// The image in byte array
        /// </returns>
        private byte[] CreateThumbNail(string image64, string encoderType)
        {
            // Convert the base64 to image
            var image = this.Base64ToImage(image64);
            var output = this.CreateThumbnailBase(image, encoderType);
            return output;
        }

        /// <summary>
        ///  The create thumbnail base.
        /// </summary>
        /// <param name="image">
        ///  The image.
        /// </param>
        /// <param name="encoderType">
        ///  The encoder type.
        /// </param>
        /// <returns>
        /// The image in bytes
        /// </returns>
        private byte[] CreateThumbnailBase(Image image, string encoderType)
        {
            // Create the thumbnail
            Image.GetThumbnailImageAbort myCallback = this.ThumbnailCallback;

            Image thumb = image.GetThumbnailImage(100, 100, myCallback, IntPtr.Zero);
            var encoder = ImageFormat.Png;

            if (encoderType == "image/jpeg")
            {
                encoder = ImageFormat.Jpeg;
            }

            var output = this.ImageToBytesArray(thumb, encoder);
            return output;
        }

        /// <summary>
        /// The thumbnail callback.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool ThumbnailCallback()
        {
            return false;
        }

        /// <summary>
        /// The base 64 to image.
        /// </summary>
        /// <param name="base64String">
        /// The base 64 string.
        /// </param>
        /// <returns>
        /// The <see cref="Image"/>.
        /// </returns>
        private Image Base64ToImage(string base64String)
        {
            // Convert base 64 string to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);

            // Convert byte[] to Image
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                Image image = Image.FromStream(ms, true);
                return image;
            }
        }

        /// <summary>
        /// The image to bytes array.
        /// </summary>
        /// <param name="image">
        /// The image.
        /// </param>
        /// <param name="format">
        /// The format.
        /// </param>
        /// <returns>
        /// The array
        /// </returns>
        private byte[] ImageToBytesArray(Image image, ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();
                return imageBytes;
            }
        }

 
    }
}