﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEducationBo.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.BusinessLogic.Lead.LeadEducationBo
// </copyright>
// <summary>
//   The lead education (lead prior education) business object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;

    using AutoMapper;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.Enumerations;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Domain.SystemStuff.Intitution;
    using FAME.Advantage.Domain.Users;
    using FAME.Advantage.Messages.Lead.Education;
    using FAME.Advantage.Messages.Web;

    /// <summary>
    /// The lead education (lead prior education) business object.
    /// </summary>
    public class LeadEducationBo
    {
        #region Class Private members

        /// <summary>
        /// The NHibernate repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The repository with type id.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithTypeId;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadEducationBo"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="repositoryWithTypeId">
        /// The repository with type id.
        /// </param>
        public LeadEducationBo(IRepository repository, IRepositoryWithTypedID<int> repositoryWithTypeId)
        {
            this.repository = repository;
            this.repositoryWithTypeId = repositoryWithTypeId;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public LeadEducationOutputModel Get(LeadEducationFilterInputModel input)
        {
            if (input == null)
            {
                throw new Exception("Filter can't be null");
            }

            if (input.CampusId == Guid.Empty)
            {
                throw new Exception("Campus can't be empty");
            }

            if (input.UserId == Guid.Empty)
            {
                throw new Exception("User can't be empty");
            }

            if (input.Id == Guid.Empty)
            {
                throw new Exception("Lead Education Id can't be empty");
            }

            if (input.LeadId == Guid.Empty)
            {
                throw new Exception("Lead Id can't be empty");
            }

            LeadEducation leadEducation =
                this.repository.Query<LeadEducation>()
                .FirstOrDefault(x => x.Lead.ID == input.LeadId && x.ID == input.Id && x.Lead.Campus.ID == input.CampusId && x.Status.StatusCode == "A");

            LeadEducationOutputModel output = new LeadEducationOutputModel();

            output = Mapper.Map(leadEducation, output);

            return output;
        }

        /// <summary>
        /// The find prior education.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public List<LeadEducationOutputModel> Find(LeadEducationFilterInputModel input)
        {
            if (input == null)
            {
                throw new Exception("Filter can't be null");
            }

            if (input.CampusId == Guid.Empty)
            {
                throw new Exception("Campus can't be empty");
            }

            if (input.UserId == Guid.Empty)
            {
                throw new Exception("User can't be empty");
            }

            if (input.LeadId == Guid.Empty)
            {
                throw new Exception("Lead Id can't be empty");
            }

            var leadEducation =
                this.repository.Query<LeadEducation>()
                .Where(x => x.Lead.ID == input.LeadId && x.Lead.Campus.ID == input.CampusId && x.Status.StatusCode == "A" && x.Institution.SyStatusesObj.StatusCode == "A").AsEnumerable();

            var output = leadEducation.Select(r =>
            {
                var outputModel = new LeadEducationOutputModel();
                return Mapper.Map(r, outputModel);
            });

            return output.OrderBy(x => x.InstitutionName).ToList();
        }

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public Response<string> Insert(LeadEducationInputModel input)
        {
            Response<string> response = new Response<string>();
            response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;

            if (input == null)
            {
                response.ResponseMessage = "Filter can't be null";
                return response;
            }

            if (input.CampusId == Guid.Empty)
            {
                response.ResponseMessage = "Campus can't be empty";
                return response;
            }

            if (input.UserId == Guid.Empty)
            {
                response.ResponseMessage = "User can't be empty";
                return response;
            }

            if (input.LeadId == Guid.Empty)
            {
                response.ResponseMessage = "Lead Id can't be empty";
                return response;
            }

            if (input.TypeId == 0)
            {
                response.ResponseMessage = "Please Select School Defined or National fora a drop down list, or New to enter a new Institution manually";
                return response;
            }

            if (input.TypeId == (int)Enumerations.EInstitutionImportType.New)
            {
                if (string.IsNullOrEmpty(input.InstitutionName))
                {
                    response.ResponseMessage = "Institution Name can't be empty";
                    return response;
                }
            }
            else
            {
                if (input.InstitutionId == Guid.Empty)
                {
                    response.ResponseMessage = "Institution Name does not exist. Please select New to add a new institution.";
                    return response;
                }
            }

            var campusGroup = this.repository.Query<CampusGroup>().FirstOrDefault(x => x.Campuses.Any(c => c.ID == input.CampusId));

            if (campusGroup == null)
            {
                response.ResponseMessage = "Campus can't be found";
                return response;
            }

            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

            if (user == null)
            {
                response.ResponseMessage = "User Acount can't be found";
                return response;
            }

            var lead = this.repository.Query<Lead>().FirstOrDefault(x => x.ID == input.LeadId && x.Campus.ID == input.CampusId);
            var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode == "A");
            var level = this.repositoryWithTypeId.Query<AdLevels>().FirstOrDefault(x => x.ID == input.LevelId);

            HighSchools institution;

            InstitutionImportType importType;

            DateTime? graduationDate = null;
            if (!string.IsNullOrEmpty(input.GraduationDate))
            {
                graduationDate = DateTime.Parse(input.GraduationDate);
            }

            if (input.TypeId == (int)Enumerations.EInstitutionImportType.New)
            {
                if (string.IsNullOrEmpty(input.InstitutionName.Trim()))
                {
                    response.ResponseMessage = "Institution Name is a required field.";
                    return response;
                }

                input.TypeId = (int)Enumerations.EInstitutionImportType.SchoolDefined;
                importType = this.repositoryWithTypeId.Query<InstitutionImportType>().FirstOrDefault(x => x.ID == input.TypeId);
                institution = new HighSchools(
                    string.Empty, 
                    input.InstitutionName.Trim(),
                    user.UserName,
                    DateTime.Now,
                    status,
                    campusGroup,
                    level,
                    null,
                    importType);
                this.repository.Save(institution);
            }
            else
            {
                institution =
                this.repository.Query<HighSchools>().FirstOrDefault(x => x.ID == input.InstitutionId && x.CampusGroupObj.Campuses.Any(c => c.ID == input.CampusId) && x.SyStatusesObj.StatusCode == "A");

                var existGradDate =
                    this.repository.Query<LeadEducation>()
                        .Any(x => x.Lead.ID == lead.ID && x.GraduatedDate == graduationDate && x.Institution.ID == institution.ID);

                if (existGradDate)
                {
                    response.ResponseMessage = "A record with same institution and graduation date already exist.";
                    return response;
                }
            }

            string institutionType = string.Empty;

            if (input.TypeId == (int)Enumerations.EInstitutionImportType.SchoolDefined || input.TypeId == (int)Enumerations.EInstitutionImportType.New)
            {
                institutionType = "Schools";
            }
            else if (input.TypeId == (int)Enumerations.EInstitutionImportType.Imported)
            {
                institutionType = "Imported";
            }

            if (input.IsInstitutionAddedToMru && input.TypeId == (int)Enumerations.EInstitutionImportType.Imported)
            {
                if (institution != null)
                {
                    institutionType = "Schools";
                    importType = this.repositoryWithTypeId.Query<InstitutionImportType>().FirstOrDefault(x => x.ID == (int)Enumerations.EInstitutionImportType.SchoolDefined);
                    institution.Update(importType);
                    this.repository.SaveAndFlush(institution);
                }
            }

            LeadEducation leadEducation = new LeadEducation(institutionType, lead, institution, graduationDate, input.FinalGrade.Replace("_", string.Empty), input.Comments, user.UserName, DateTime.Now, input.Major, status, input.Graduate, input.Gpa, input.Rank, input.Percentile, input.CertificateOrDegree);

            this.repository.Save(leadEducation);

            response.ResponseCode = (int)HttpStatusCode.OK;

            return response;
        }

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public Response<string> Update(LeadEducationInputModel input)
        {
            Response<string> response = new Response<string>();
            response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;

            if (input == null)
            {
                response.ResponseMessage = "Filter can't be null";
                return response;
            }

            if (input.Id == Guid.Empty)
            {
                response.ResponseMessage = "Lead Education can't be empty";
                return response;
            }

            if (input.CampusId == Guid.Empty)
            {
                response.ResponseMessage = "Campus can't be empty";
                return response;
            }

            if (input.UserId == Guid.Empty)
            {
                response.ResponseMessage = "User can't be empty";
                return response;
            }

            if (input.LeadId == Guid.Empty)
            {
                response.ResponseMessage = "Lead Id can't be empty";
                return response;
            }

            if (input.TypeId == 0)
            {
                response.ResponseMessage = "Please Select School Defined or National fora a drop down list, or New to enter a new Institution manually";
                return response;
            }

            if (input.TypeId == (int)Enumerations.EInstitutionImportType.New)
            {
                if (string.IsNullOrEmpty(input.InstitutionName))
                {
                    response.ResponseMessage = "Institution Name can't be empty";
                    return response;
                }
            }
            else
            {
                if (input.InstitutionId == Guid.Empty)
                {
                    response.ResponseMessage = "Institution Name can't be empty";
                    return response;
                }
            }

            var campusGroup = this.repository.Query<CampusGroup>().FirstOrDefault(x => x.Campuses.Any(c => c.ID == input.CampusId));

            if (campusGroup == null)
            {
                response.ResponseMessage = "Campus can't be found";
                return response;
            }

            var lead = this.repository.Query<Lead>().FirstOrDefault(x => x.ID == input.LeadId && x.Campus.ID == input.CampusId);

            if (lead == null)
            {
                response.ResponseMessage = "Lead can't be found";
                return response;
            }

            var leadEducation = this.repository.Query<LeadEducation>().FirstOrDefault(x => x.ID == input.Id && x.Lead.ID == input.LeadId && x.Status.StatusCode == "A");

            if (leadEducation == null)
            {
                response.ResponseMessage = "Lead Education can't be found";
                return response;
            }

            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

            if (user == null)
            {
                response.ResponseMessage = "User Acount can't be found";
                return response;
            }

            var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode == "A");
            var level = this.repositoryWithTypeId.Query<AdLevels>().FirstOrDefault(x => x.ID == input.LevelId);

            DateTime? graduationDate = null;
            if (!string.IsNullOrEmpty(input.GraduationDate))
            {
                graduationDate = DateTime.Parse(input.GraduationDate);
            }

            HighSchools institution = null;
            InstitutionImportType importType = null;
            if (input.TypeId == (int)Enumerations.EInstitutionImportType.New)
            {
                input.TypeId = (int)Enumerations.EInstitutionImportType.SchoolDefined;
                importType = this.repositoryWithTypeId.Query<InstitutionImportType>().FirstOrDefault(x => x.ID == input.TypeId);
                institution = new HighSchools(
                    string.Empty,
                    input.InstitutionName,
                    user.UserName,
                    DateTime.Now,
                    status,
                    campusGroup,
                    level,
                    null,
                    importType);
                this.repository.Save(institution);
            }
            else
            {
                institution =
                this.repository.Query<HighSchools>().FirstOrDefault(x => x.ID == input.InstitutionId && x.CampusGroupObj.Campuses.Any(c => c.ID == input.CampusId) && x.SyStatusesObj.StatusCode == "A");
                if (institution != null)
                {
                    var existGradDate = this.repository.Query<LeadEducation>()
                            .Any(x => x.Lead.ID == lead.ID && x.GraduatedDate == graduationDate && x.Institution.ID == institution.ID) && leadEducation.Institution.ID != institution.ID;

                    if (existGradDate)
                    {
                        response.ResponseMessage = "A record with same institution and graduation date already exist.";
                        return response;
                    }
                }
            }

            string institutionType = string.Empty;

            if (input.TypeId == (int)Enumerations.EInstitutionImportType.SchoolDefined)
            {
                institutionType = "Schools";
            }
            else if (input.TypeId == (int)Enumerations.EInstitutionImportType.Imported)
            {
                institutionType = "Imported";
            }

            bool updatedInstitution = false;

            if (input.IsInstitutionAddedToMru && input.TypeId == (int)Enumerations.EInstitutionImportType.Imported)
            {
                if (institution != null)
                {
                    institutionType = "Schools";
                    importType = this.repositoryWithTypeId.Query<InstitutionImportType>().FirstOrDefault(x => x.ID == (int)Enumerations.EInstitutionImportType.SchoolDefined);
                    institution.Update(importType);
                    updatedInstitution = true;
                }
            }

            if (institution != null && !string.IsNullOrEmpty(input.InstitutionName))
            {
                if (institution.HsDescrip != input.InstitutionName && input.TypeId == (int)Enumerations.EInstitutionImportType.SchoolDefined)
                {
                    institution.Update(input.InstitutionName);
                    updatedInstitution = true;
                }
            }

            if (updatedInstitution)
            {
                this.repository.SaveAndFlush(institution);
            }

            leadEducation.Update(institutionType, lead, institution, graduationDate, input.FinalGrade.Replace("_", string.Empty), input.Comments, user.UserName, DateTime.Now, input.Major, status, input.Graduate, input.Gpa, input.Rank, input.Percentile, input.CertificateOrDegree);

            this.repository.Save(leadEducation);

            response.ResponseCode = (int)HttpStatusCode.OK;

            return response;
        }

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public void Delete(LeadEducationFilterInputModel input)
        {
            if (input == null)
            {
                throw new Exception("Filter can't be null");
            }

            if (input.CampusId == Guid.Empty)
            {
                throw new Exception("Campus can't be empty");
            }

            if (input.UserId == Guid.Empty)
            {
                throw new Exception("User can't be empty");
            }

            if (input.Id == Guid.Empty)
            {
                throw new Exception("Lead Education Id can't be empty");
            }

            if (input.LeadId == Guid.Empty)
            {
                throw new Exception("Lead Id can't be empty");
            }

            var campusGroup = this.repository.Query<CampusGroup>().FirstOrDefault(x => x.Campuses.Any(c => c.ID == input.CampusId));

            if (campusGroup == null)
            {
                throw new Exception("Campus can't be found");
            }

            var lead = this.repository.Query<Lead>().FirstOrDefault(x => x.ID == input.LeadId && x.Campus.ID == input.CampusId);

            if (lead == null)
            {
                throw new Exception("Lead can't be found");
            }

            var leadEducation = this.repository.Query<LeadEducation>().FirstOrDefault(x => x.ID == input.Id && x.Lead.ID == input.LeadId && x.Status.StatusCode == "A");

            if (leadEducation == null)
            {
                throw new Exception("Lead Education can't be found");
            }

            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

            if (user == null)
            {
                throw new Exception("User Acount can't be found or user do not have permission to complete this action!");
            }

            var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode == "I");

            leadEducation.Delete(user.UserName, status);

            this.repository.Save(leadEducation);
        }
    }
}
