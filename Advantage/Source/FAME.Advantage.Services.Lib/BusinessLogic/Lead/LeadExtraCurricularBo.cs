﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadExtraCurricularBo.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Business Object for Extra Curricular service
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Domain.Infrastructure.Entities;
    using Domain.Lead;
    using Domain.Users;

    using Infrastructure.Exceptions;

    using Messages.Common;
    using Messages.Lead.ExtraCurricular;

    /// <summary>
    /// Business Object for Extra Curricular service
    /// </summary>
    public class LeadExtraCurricularBo
    {
        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepositoryWithTypedID<Guid> repository;

        /// <summary>
        ///  The repository integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryint;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadExtraCurricularBo"/> class. 
        /// </summary>
        /// <param name="leadRepository">
        /// The repository.
        /// </param>
        /// <param name="repositoryWithInt">
        /// The repository integer.
        /// </param>
        public LeadExtraCurricularBo(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = leadRepository;
            this.repositoryint = repositoryWithInt;
        }

        /// <summary>
        ///  The get extracurricular values.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <returns>
        /// The <see cref=" IList&lt;ExtraInfo&gt;"/>.
        /// </returns>
        public IList<ExtraInfo> GetExtracurricularValues(ExtraInfoInputModel filter)
        {
            var q = this.repositoryint.Query<ExtraCurriculars>()
                    .Where(x => x.LeadObj.ID.ToString() == filter.LeadId)

                            .Select(info => new ExtraInfo
                                                {
                                                    Comment = info.ExtraCurrComments, 
                                                    Description = info.ExtraCurrDescription, 
                                                    Group =
                                                        new DropDownOutputModel
                                                            {
                                                                Description =
                                                                    info
                                                                    .ExtraCurricularGroupObj
                                                                    .ExtraCurrGrpDescrip, 
                                                                ID =
                                                                    info
                                                                    .ExtraCurricularGroupObj
                                                                    .ID.ToString()
                                                            }, 
                                                    Id = info.ID, 

                                                    // LeadId = info.LeadObj.ID.ToString(),
                                                    Level =
                                                        new DropDownOutputModel
                                                            {
                                                                Description =
                                                                    info.LevelObj
                                                                    .Description, 
                                                                ID =
                                                                    info.LevelObj.ID
                                                                    .ToString()
                                                            }
                                                });

            var output = q.OrderBy(x => x.Group.Description).ToList();
            return output;
        }

        /// <summary>
        /// Get Extracurricular levels Drop down items
        /// </summary>
        /// <param name="filter">Filter parameter Id lead</param>
        /// <returns>
        /// A list of extracurricular items.
        /// </returns>
        public IList<DropDownOutputModel> GetLevelsItemsExtracurricularValues(ExtraInfoInputModel filter)
        {
            var q = this.repositoryint.Query<AdLevels>()
                    .Where(x => x.StatusObj.StatusCode == "A")
                    .Select(info => new DropDownOutputModel()
                    {
                        Description = info.Description, 
                        ID = info.ID.ToString()
                    });
            var output = q.OrderBy(x => x.Description).ToList();
            return output;
        }

        /// <summary>
        /// Return drop down items for Extracurricular Groups
        /// </summary>
        /// <param name="filter">The filters </param>
        /// <returns>
        ///  list of  IList&lt;DropDownOutputModel&gt;
        /// </returns>
        public IList<DropDownOutputModel> GetGroupItemsExtracurricularValues(ExtraInfoInputModel filter)
        {
            var q = this.repository.Query<ExtraCurricularGroup>()
                    .Where(
                        x =>
                            x.CampusGroupObj.Campuses.Any(d => d.ID.ToString() == filter.CampusId) &&
                            x.StatusObj.StatusCode == "A")
                    .Select(info => new DropDownOutputModel()
                    {
                        Description = info.ExtraCurrGrpDescrip, 
                        ID = info.ID.ToString()
                    });
            var output = q.OrderBy(x => x.Description).ToList();
            return output;
        }

        /// <summary>
        /// Update a existing extra curricular value in the list of lead.
        /// </summary>
        /// <param name="filter">The filter lead ID </param>
        public void UpdateExtraCurriculars(IExtraInfoOutputModel filter)
        {
            foreach (ExtraInfo info in filter.ExtraInfoList)
            {
                var leadId = filter.Filter.LeadId;
                var q = this.repository.Query<Lead>().Single(x => x.ID.ToString() == leadId);
                var user = this.repository.Query<User>().Single(x => x.ID.ToString() == filter.Filter.UserId);
                var extra = q.ExtraCurricularList.SingleOrDefault(x => x.ID == info.Id);
                if (extra != null)
                {
                    var group = this.repository.Query<ExtraCurricularGroup>().SingleOrDefault(x => x.ID.ToString() == info.Group.ID);
                    var level = this.repositoryint.Query<AdLevels>()
                            .SingleOrDefault(x => x.ID.ToString() == info.Level.ID);
                    var nextra = new ExtraCurriculars(
                        info.Description, 
                        info.Comment, 
                        extra.LeadObj, 
                        group, 
                        level, 
                        user.UserName,
                        DateTime.Now);

                    q.ExtraCurricularList.Remove(extra);
                    q.ExtraCurricularList.Add(nextra);
                    this.repository.Update(q);
                }
                else
                {
                    throw new ApplicationException("The Extra Curricular info selected does not exists in the database");
                }
            }
        }

        /// <summary>
        /// Delete a Extra Curricular value from the lead list
        /// </summary>
        /// <param name="filter">The filter lead ID</param>
        public void DeleteExtraCurriculars(IExtraInfoOutputModel filter)
        {
            foreach (ExtraInfo info in filter.ExtraInfoList)
            {
                var leadId = filter.Filter.LeadId;
                var q = this.repository.Query<Lead>().Single(x => x.ID.ToString() == leadId && x.ExtraCurricularList.Any(t => t.ID == info.Id));

                var extra = q.ExtraCurricularList.SingleOrDefault(x => x.ID == info.Id);
                if (extra != null)
                {
                    q.ExtraCurricularList.Remove(extra);
                    this.repository.Update(q);
                }
                else
                {
                    throw new ApplicationException("The Extracurricular info selected does not exists in the database");
                }
           }
        }

        /// <summary>
        /// Insert a new ExtraCurricular
        /// </summary>
        /// <param name="filter">
        /// The filter lead Id, user Id
        /// </param>
        /// <returns>
        /// The <see cref="IExtraInfoOutputModel"/>.
        /// </returns>
        public IExtraInfoOutputModel InsertExtraCurriculars(IExtraInfoOutputModel filter)
        {
            var output = ExtraInfoOutputModel.Factory();
            foreach (ExtraInfo info in filter.ExtraInfoList)
            {
                var leadId = filter.Filter.LeadId;
                var q = this.repository.Query<Lead>().Single(x => x.ID.ToString() == leadId);
                var group = this.repository.Query<ExtraCurricularGroup>().SingleOrDefault(x => x.ID.ToString() == info.Group.ID);
                var user = this.repository.Query<User>().Single(x => x.ID.ToString() == filter.Filter.UserId);
                var level = this.repositoryint.Query<AdLevels>().SingleOrDefault(x => x.ID.ToString() == info.Level.ID);
                var nextra = new ExtraCurriculars(
                    info.Description, 
                    info.Comment, 
                    q, 
                    group, 
                    level, 
                    user.UserName, 
                    DateTime.Now);
                q.ExtraCurricularList.Add(nextra);
                this.repository.SaveAndFlush(q);
                var newExtra = new ExtraInfo()
                {
                    Comment = nextra.ExtraCurrComments, 
                    Description = nextra.ExtraCurrDescription, 
                    Group = new DropDownOutputModel()
                    {
                        Description = nextra.ExtraCurricularGroupObj.ExtraCurrGrpDescrip, 
                        ID = nextra.ExtraCurricularGroupObj.ID.ToString()
                    }, 
                    Id = nextra.ID, 
                    Level = new DropDownOutputModel()
                    {
                        Description = nextra.LevelObj.Description, 
                        ID = nextra.LevelObj.ID.ToString()
                    }
                };
                output.ExtraInfoList.Add(newExtra);
            }

            return output;
        }
    }
}
