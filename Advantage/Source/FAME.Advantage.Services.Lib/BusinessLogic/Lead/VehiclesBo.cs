﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VehiclesBo.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Vehicles business Object
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Domain.Infrastructure.Entities;
    using Domain.Lead;

    using Infrastructure.Exceptions;

    using Messages.Lead;

    /// <summary>
    /// Vehicles business Object
    /// </summary>
    public class VehiclesBo
    {
        /// <summary>
        ///  The repository.
        /// </summary>
        private readonly IRepositoryWithTypedID<Guid> repository;

        /// <summary>
        ///  The repository integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryint;

        /// <summary>
        /// Initializes a new instance of the <see cref="VehiclesBo"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="leadRepository">
        /// The repository GUID.
        /// </param>
        /// <param name="repositoryWithInt">
        /// The repository integer.
        /// </param>
        public VehiclesBo(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = leadRepository;
            this.repositoryint = repositoryWithInt;
        }

        /// <summary>
        /// Get All vehicles for Lead
        /// </summary>
        /// <param name="filter">
        /// Lead ID and vehicle position
        /// </param>
        /// <returns>
        /// A List of vehicles
        /// </returns>
        public IList<VehicleOutputModel> GetAllVehiclesForTheLead(VehicleInputModel filter)
        {
            var q = this.repositoryint.Query<AdVehicles>()
                    .Where(x => x.LeadObj.ID.ToString() == filter.LeadId && x.Position == 1 && x.Position == 2)
                    .Select(info => new VehicleOutputModel()
                    {
                        Permit = info.Permit,
                        Make = info.Make,
                        Model = info.Model,
                        Id = info.ID,
                        LeadId = info.LeadObj.ID.ToString(),
                        Color = info.Color,
                        Plate = info.Plate,
                        ModDate = info.ModDate,
                        ModUser = info.ModUser
                    });

            var output = q.OrderBy(x => x.Position).ToList();
            return output;
        }

        /// <summary>
        /// Update Insert the list of Vehicles.
        /// </summary>
        /// <param name="vehiclesList">Vehicles to be inserted</param>
        /// <exception cref="HttpBadRequestResponseException">
        /// The returned Exception 
        /// </exception>
        public void UpdateInsertVehicle(List<VehicleOutputModel> vehiclesList)
        {
            foreach (VehicleOutputModel vehicle in vehiclesList)
            {
                var ve = this.repositoryint.Query<AdVehicles>().SingleOrDefault(x => x.ID == vehicle.Id);
                if (ve != null)
                {
                    // Update
                    ve.Update(vehicle);
                    this.repositoryint.Update(ve);
                }
                else
                {
                    // Insert
                    var lead = this.repository.Query<Lead>().SingleOrDefault(x => x.ID.ToString() == vehicle.LeadId);
                    if (lead == null)
                    {
                        throw new Exception("The Lead does not exists");
                    }
                   
                    var domainvehicle = new AdVehicles(0, vehicle.Position, lead, vehicle.Permit, vehicle.Make, vehicle.Model, vehicle.Color, vehicle.Plate, vehicle.ModUser);
                    this.repositoryint.Save(domainvehicle);
                }
            }
        }
    }
}
