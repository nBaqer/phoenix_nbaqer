﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadQuickBo.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The lead quick bo.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Catalogs;
    using Domain.Infrastructure.Entities;
    using Domain.Lead;
    using Messages.Common;
    using Messages.Lead.Quick;
    using Messages.SystemStuff.SystemStates;

    /// <summary>
    /// The lead quick bo.
    /// </summary>
    public class LeadQuickBo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadQuickBo"/> class. 
        /// Get 
        /// </summary>
        /// <param name="leadRepository">
        /// Lead Repository
        /// </param>
        /// <param name="repositoryWithInt">
        /// Lead Repository With Integer
        /// </param>
        public LeadQuickBo(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.Repository = leadRepository;
            this.RepositoryWithTypedId = repositoryWithInt;
        }

        /// <summary>
        /// Gets or sets the repository.
        /// </summary>
        public IRepository Repository { get; set; }

        /// <summary>
        /// Gets or sets the repository with typed id.
        /// </summary>
        public IRepositoryWithTypedID<int> RepositoryWithTypedId { get; set; }

        /// <summary>
        /// The get quick lead fields.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable&lt;LeadQuick&gt;"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// throw error 
        /// </exception>
        public IEnumerable<LeadQuick> GetQuickLeadFields(LeadQuickFieldsInputModel filter)
        {
            var list = this.RepositoryWithTypedId.Query<LeadQuick>().OrderBy(x => x.Sequence).ToList();

            return list;
        }

        /// <summary>
        /// Get state data source as Code - Description.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable&lt;SystemStatesOutputModel&gt;"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// throw error 
        /// </exception>
        public IEnumerable<SystemStatesOutputModel> GetQuickLeadStateValues()
        {
            CatalogsBo bo = new CatalogsBo(this.Repository, this.RepositoryWithTypedId);
            var result = bo.GetAllStatesItemsWithCode();
            var orderedRes = result.OrderBy(x => x.Code);
            var res = orderedRes.ToList();
            var selectObj = new SystemStatesOutputModel
            {
                Description = "Select",
                ID = Guid.Empty,
                Code = "Select",
                FullDescription = "Select"
            };
            res.Insert(0, selectObj);

            return res;
        }

        /// <summary>
        /// The get quick lead fields.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable&lt;DropDownOutputModel&gt;"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// throw error 
        /// </exception>
        public IEnumerable<DropDownOutputModel> GetQuickLeadDropDownValues(CatalogsInputModel filter)
        {
            // var catalog = new CatalogsInputModel { FldName = filter.FldName };
            if (filter.FldName == "PrgVerId")
            {
                var res = Mapper.Map<IEnumerable<DropDownOutputModel>>(CatalogStatic.FilteredDropDownOutputModels(
                    filter,
                    this.Repository,
                    this.RepositoryWithTypedId));
                return res;
            }
            else
            {
                var result = Mapper.Map<IEnumerable<DropDownOutputModel>>(CatalogStatic.ResourcesDropDownOutputModels(
                    filter,
                    this.Repository,
                    this.RepositoryWithTypedId));
                return result;
            }
        }
    }
}
