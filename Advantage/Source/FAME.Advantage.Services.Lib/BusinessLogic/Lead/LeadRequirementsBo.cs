﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadRequirementsBo.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.BusinessLogic.Lead.LeadRequirementsBo
// </copyright>
// <summary>
//   Business object for Lead Controller
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.Domain.Student;

namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;

    using AutoMapper;

    using Domain.Infrastructure.Entities;
    using Domain.Lead;
    using Domain.Requirements;
    using Domain.Requirements.Documents;
    using Domain.SystemStuff.ConfigurationAppSettings;
    using Domain.Users;

    using FAME.Advantage.Domain.Campuses;

    using FluentNHibernate.Conventions;
    using Formatters.MultipartDataMediaFormatter.Infrastructure;
    using Infrastructure.Exceptions;
    using Infrastructure.Extensions;
    using Messages.Lead;
    using Messages.Requirements;
    using Messages.Users;

    using NHibernate.Criterion;

    /// <summary>
    /// Business object for Lead Controller
    /// </summary>
    public class LeadRequirementsBo
    {
        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadRequirementsBo"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="repository">
        /// The nHibernate repository for entities with Identity of type GUID
        /// </param>
        /// <param name="repositoryWithInt">
        /// The nHibernate repository for entities with Identity of type INT
        /// </param>
        public LeadRequirementsBo(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            this.repositoryWithInt = repositoryWithInt;
        }

        /// <summary>
        /// Main get method, if student id is empty get for all students.
        /// If student id has value get for specific student
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        /// <param name="leadId">
        /// The Lead Id
        /// </param>
        /// <returns>
        /// An IEnumerable of type Lead Requirement Entity Output Model
        /// </returns>
        public IEnumerable<LeadReqEntityOutputModel> GetRequirements(GetLeadRequirementInputModel filter, Guid leadId)
        {
            //---------------------------------------------------------
            // Validate required fields
            //---------------------------------------------------------
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("No search parameters passed.");
            }

            if (filter.CampusId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("CampusId is required.");
            }

            if (leadId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("Lead Id required.");
            }

            //---------------------------------------------------------
            var finalOutput = this.GetMandatoryRequirements(leadId, filter.CampusId);

            //// var leadReqEntityOutputModels = finalOutput as LeadReqEntityOutputModel[] ?? finalOutput.ToArray();

            return finalOutput;
        }

        /// <summary>
        /// Get Lead Group Optional Requirements
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        /// <param name="leadId">
        /// The Lead Id
        /// </param>
        /// <returns>
        /// An IEnumerable of type Lead Requirement Entity Output Model
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// A HttpBadRequestResponseException with the reason. Mostly an exception with the reason of a Missing Required Field.
        /// </exception>
        public IEnumerable<LeadReqEntityOutputModel> GetLeadGroupOptionalRequirements(GetLeadRequirementInputModel filter, Guid leadId)
        {
            //---------------------------------------------------------
            // Validate required fields
            //---------------------------------------------------------
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("No search parameters passed.");
            }

            if (filter.CampusId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("CampusId is required.");
            }

            if (leadId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("LeadId required.");
            }

            var finalOutput = this.GetOptionalRequirements(leadId, filter.CampusId);

            return finalOutput;
        }

        /// <summary>
        /// Main get method, if student id is empty get for all students.
        /// If student id has value get for specific student
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        /// <param name="leadId">
        /// The Lead Id
        /// </param>
        /// <returns>
        /// An IEnumerable of type Lead Requirement Entity Output Model
        /// </returns>
        public IEnumerable<LeadReqEntityOutputModel> GetRequirementGroups(GetLeadRequirementInputModel filter, Guid leadId)
        {
            //---------------------------------------------------------
            // Validate required fields
            //---------------------------------------------------------
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("No search parameters passed.");
            }

            if (filter.CampusId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("CampusId is required.");
            }

            if (leadId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("LeadId required.");
            }

            var requirements = this.GetRequirementGroupLevelRequirements(leadId, filter.CampusId);

            return requirements;
        }

        /// <summary>
        /// Main get method, if student id is empty get for all students.
        /// If student id has value get for specific student
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        /// <param name="grpId">
        /// The Group Id
        /// </param>
        /// <returns>
        /// An IEnumerable of type Lead Requirement Entity Output Model
        /// </returns>
        public IEnumerable<LeadReqEntityOutputModel> GetRequirementByGroupId(GetLeadRequirementInputModel filter, Guid grpId)
        {
            //---------------------------------------------------------
            // Validate required fields
            //---------------------------------------------------------
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("No search parameters passed.");
            }

            if (filter.CampusId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("CampusId is required.");
            }

            if (grpId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("grpid required.");
            }

            if (filter.leadId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("LeadId is required.");
            }

            //---------------------------------------------------------
            if (filter.leadId == null)
            {
                return null;
            }

            var reqs = this.GetRequirementGroupLevelReqByGroup(grpId, filter.CampusId, (Guid)filter.leadId);

            return reqs.ToArray().OrderBy(n => n.Description);
        }

        /// <summary>
        /// Get Requirement Details By Requirement Id
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        /// <param name="reqId">
        /// The requirement id
        /// </param>
        /// <returns>
        /// A Lead Requirement Details Output Model
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// A HttpBadRequestResponseException with the reason. Mostly an exception with the reason of a Missing Required Field.
        /// </exception>
        public LeadReqDetailsOutputModel GetRequirementDetailsByReqId(GetLeadRequirementInputModel filter, Guid reqId)
        {
            //---------------------------------------------------------
            // Validate required fields
            //---------------------------------------------------------
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("No search parameters passed.");
            }

            if (filter.CampusId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("CampusId is required.");
            }

            if (reqId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("reqId required.");
            }

            if (filter.leadId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("LeadId is required.");
            }

            //---------------------------------------------------------
            if (filter.leadId == null)
            {
                return null;
            }

            var reqs = this.GetDetailsByReqId(reqId, filter.RequirementType, filter.CampusId, (Guid)filter.leadId);

            return reqs;
        }

        /// <summary>
        /// Update Existing Requirement
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        /// <param name="docReqid">
        /// The document requirement id
        /// </param>
        public void UpdateExistingRequirement(LeadRequirementInputModel filter, Guid docReqid)
        {
            if (filter == null)
            {
                throw new Exception("No search parameters passed.");
            }

            if (filter.LeadId.IsEmpty())
            {
                throw new Exception("Missing LeadId.");
            }

            if (filter.UserId.IsEmpty())
            {
                throw new Exception("Missing UserId.");
            }

            if (docReqid.IsEmpty())
            {
                throw new Exception("Missing reqId.");
            }

            if (string.IsNullOrEmpty(filter.RequirementType))
            {
                throw new Exception("Missing RequirementType.");
            }

            switch (filter.RequirementType.ToUpper())
            {
                case "DOCUMENT":
                    this.UpdateExistingDocumentRequirement(filter, docReqid);
                    break;
                case "FEE":
                    this.UpdateExistingTranRequirement(filter, docReqid);
                    break;
                default:
                    throw new Exception("unsupported type.");
            }
        }

        /// <summary>
        /// Insert New Requirement
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        /// <exception cref="HttpBadRequestResponseException">
        /// A HttpBadRequestResponseException with the reason. Mostly an exception with the reason of a Missing Required Field.
        /// </exception>
        public void InsertNewRequirement(LeadRequirementInputModel filter)
        {
            if (filter == null)
            {
                throw new Exception("No search parameters passed.");
            }

            if (filter.LeadId.IsEmpty())
            {
                throw new Exception("Missing LeadId.");
            }

            if (filter.UserId.IsEmpty())
            {
                throw new Exception("Missing UserId.");
            }

            if (filter.ReqId.IsEmpty())
            {
                throw new Exception("Missing ReqId.");
            }

            if (string.IsNullOrEmpty(filter.RequirementType))
            {
                throw new Exception("Missing RequirementType.");
            }

            switch (filter.RequirementType.ToUpper())
            {
                case "DOCUMENT":
                    this.InsertDocumentRequirement(filter);
                    break;
                case "TEST":
                    this.InsertTestRequirement(filter);
                    break;
                case "INTERVIEW":
                case "TOUR":
                case "EVENT":
                    this.InsertReqEventRequirement(filter);
                    break;
                case "FEE":
                    this.InsertFeeRequirement(filter);
                    break;
            }
        }

        /// <summary>
        /// Delete Requirement History Record
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        /// <exception cref="HttpBadRequestResponseException">
        /// A HttpBadRequestResponseException with the reason. Mostly an exception with the reason of a Missing Required Field.
        /// </exception>
        /// <exception cref="NotImplementedException">
        /// A method not implementation exception
        /// </exception>
        public void DeleteRequirementHistoryRecord(LeadReqDetailsInputModel filter)
        {
            if (filter.Id.IsEmpty())
            {
                throw new HttpBadRequestResponseException("Missing ReqId.");
            }

            if (string.IsNullOrEmpty(filter.RequirementType))
            {
                throw new HttpBadRequestResponseException("Missing RequirementType.");
            }

            switch (filter.RequirementType.ToUpper())
            {
                case "DOCUMENT":
                    this.DeleteDocumentFileRecord(filter.Id, filter.FilePath);
                    break;
                case "TEST":
                    this.DeleteTestRecord(filter.Id);
                    break;
                case "INTERVIEW":
                case "TOUR":
                case "EVENT":
                    this.DeleteEventReqRecord(filter.Id);
                    break;
                case "FEE":
                    throw new NotImplementedException("TODO");
            }
        }

        /// <summary>
        /// Save Document File
        /// </summary>
        /// <param name="requirementInfo">
        /// The requirement
        /// </param>
        /// <param name="formData">
        /// The form data
        /// </param>
        /// <exception cref="HttpBadRequestResponseException">
        /// A HttpBadRequestResponseException with the reason. Mostly an exception with the reason of a Missing Required Field.
        /// </exception>
        public void SaveDocumentFile(string requirementInfo, FormData formData)
        {
            if (formData == null)
            {
                throw new HttpBadRequestResponseException("Missing Form Data");
            }

            if (string.IsNullOrEmpty(requirementInfo))
            {
                throw new HttpBadRequestResponseException("Missing requirement parameter");
            }

            //-------------------------------------------------------------------------------
            // Get the data from the parameter and split into individual items
            //-------------------------------------------------------------------------------
            var parameters = requirementInfo.Split(new[] { '_' }, StringSplitOptions.RemoveEmptyEntries);

            if (parameters.Count() < 3)
            {
                throw new HttpBadRequestResponseException("Missing one or more requirement parameters");
            }

            var leadId = new Guid(parameters[0]);
            var reqId = new Guid(parameters[1]);
            var userId = new Guid(parameters[2]);


            //-------------------------------------------------------------------------------

            //-------------------------------------------------------------------------------
            // Get lead and user objects
            //-------------------------------------------------------------------------------
            var lead = this.repository.Query<Lead>().Single(n => n.ID == leadId);
            var user = this.repository.Query<User>().Single(n => n.ID == userId);
            var requirement = this.repository.Query<Requirement>().FirstOrDefault(x => x.ID == reqId);
            string reqName = requirement != null ? requirement.Description : string.Empty;

            //-------------------------------------------------------------------------------
            var leadName = lead.FirstName + " " + lead.MiddleName + " " + lead.LastName;
            string fileName = null;
            HttpFile file = null;

            //-------------------------------------------------------------------------------
            // Get File Data
            //-------------------------------------------------------------------------------
            var fileObj = formData.Files.FirstOrDefault(f => f.Value != null && !string.IsNullOrEmpty(f.Value.FileName));
            if (fileObj == null)
            {
                throw new HttpBadRequestResponseException("no file found!");
            }

            fileName = fileObj.Value.FileName;
            formData.TryGetValue(fileObj.Name, out file);

            var fileNameWithoutExt = Path.GetFileNameWithoutExtension(fileName);

            var extension = Path.GetExtension(fileName);

            /*var urlEncode = HttpContext.Current.Server.UrlEncode(leadName + "_" + leadId.ToString() + "_" + DateTime.UtcNow.ToString("yyyyMMddhhmmss"));

            if (urlEncode == null)
            {
                return;
            }

            var fileNameToSave = urlEncode.ToLower();*/

            //-------------------------------------------------------------------------------

            //-------------------------------------------------------------------------------
            // Save new document history object
            //-------------------------------------------------------------------------------
            var docHistory = new DocumentHistory(string.Empty, extension, reqName, reqId, leadId, user.UserName, 2, fileNameWithoutExt);
            this.repository.Save(docHistory);

            //-------------------------------------------------------------------------------
            //-------------------------------------------------------------------------------
            // Get Folder to save document and create path
            //-------------------------------------------------------------------------------
            var configurationAppSetting = this.repositoryWithInt.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == "CREATEDOCUMENTPATH");
            if (configurationAppSetting == null)
            {
                return;
            }

            var parentFolder = configurationAppSetting.ConfigurationValues[0].Value;
            var finalFolder = Path.Combine(parentFolder, reqName);

            //-------------------------------------------------------------------------------
            //-------------------------------------------------------------------------------
            // Create folder if necessary
            //-------------------------------------------------------------------------------
            if (!Directory.Exists(finalFolder))
            {
                Directory.CreateDirectory(finalFolder);
            }

            //-------------------------------------------------------------------------------
            //-------------------------------------------------------------------------------
            // Create file name on disk
            //-------------------------------------------------------------------------------
            var docFileName = docHistory.ID + extension;
            var finalPath = Path.Combine(finalFolder, docFileName);

            //-------------------------------------------------------------------------------
            //-------------------------------------------------------------------------------
            // save file to disk
            //-------------------------------------------------------------------------------
            if (file != null)
            {
                File.WriteAllBytes(finalPath, file.Buffer);
            }

            //-------------------------------------------------------------------------------
        }

        /// <summary>
        /// Get Lead Transaction Receipt
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        /// <returns>
        /// A Lead Transaction Output
        /// </returns>
        public LeadTransactionOutput GetLeadTransactionReceipt(GetLeadRequirementInputModel filter)
        {
            if (filter.CampusId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("Missing Campus Id parameter");
            }

            if (filter.leadId == null || filter.leadId.Value.IsEmpty())
            {
                throw new HttpBadRequestResponseException("Missing Lead Id parameter");
            }

            if (filter.TransactionId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("Missing Transaction Id parameter");
            }

            var campusId = filter.CampusId;
            var leadId = filter.leadId.Value;
            var transactionId = new Guid(filter.TransactionId);

            var transaction =
                this.repository.Query<LeadTransactions>()
                    .FirstOrDefault(x => x.LeadObj.ID == leadId && x.Campus.ID == campusId && x.ID == transactionId);

            if (transaction == null)
            {
                throw new HttpBadRequestResponseException("Transaction not found");
            }

            var lead = this.repository.Query<Lead>().FirstOrDefault(x => x.ID == leadId && x.Campus.ID == campusId);

            if (lead == null)
            {
                throw new HttpBadRequestResponseException("Lead not found");
            }

            LeadTransactionOutput output = new LeadTransactionOutput();

            output.LeadName = lead.LastName + ", " + lead.FirstName + (string.IsNullOrEmpty(lead.MiddleName) ? string.Empty : " " + lead.MiddleName);
            output.LeadAddress = string.Empty;
            output.LeadAddress2 = string.Empty;
            if (lead.AddressList.Count > 0)
            {
                var leadAddress = lead.AddressList.OrderByDescending(x => x.IsShowOnLeadPage).FirstOrDefault();
                output.LeadAddress = (string.IsNullOrEmpty(leadAddress.AddressApto) ? string.Empty : " " + leadAddress.AddressApto);
                output.LeadAddress += leadAddress.Address1 + (string.IsNullOrEmpty(leadAddress.Address2) ? string.Empty : ", " + leadAddress.Address2);
                output.LeadAddress2 += (string.IsNullOrEmpty(leadAddress.City) ? string.Empty : leadAddress.City);
                if (leadAddress.IsInternational)
                {
                    output.LeadAddress2 += string.IsNullOrEmpty(leadAddress.StateInternational) ? string.Empty : ", " + leadAddress.StateInternational;
                    output.LeadAddress2 += string.IsNullOrEmpty(leadAddress.ZipCode) ? string.Empty : " " + leadAddress.ZipCode;
                    output.LeadAddress2 += string.IsNullOrEmpty(leadAddress.CountryInternational) ? string.Empty : " " + leadAddress.CountryInternational;
                }
                else
                {
                    output.LeadAddress2 += (leadAddress.StateObj == null) ? string.Empty : ", " + leadAddress.StateObj.Code;
                    output.LeadAddress2 += string.IsNullOrEmpty(leadAddress.ZipCode) ? string.Empty : " " + leadAddress.ZipCode;
                    output.LeadAddress2 += (leadAddress.CountryObj == null) ? string.Empty : (leadAddress.CountryObj.CountryCode.ToUpper().Equals("USA") ? string.Empty : " " + leadAddress.CountryObj.CountryCode);
                }
            }

            bool isAddresCorporate = false;


            var setting = this.repository.Query<ConfigurationAppSettingValues>()
                         .FirstOrDefault(x => x.ConfigurationAppSetting != null && x.ConfigurationAppSetting.KeyName.Trim().ToLower() == "AddressToBePrintedInReceipts".ToLower());

            if (setting != null)
            {
                if (setting.Value.Trim().ToLower() == "CorporateAddress".ToLower())
                {
                    isAddresCorporate = true;
                }
            }

            if (isAddresCorporate)
            {
                // Find Address in settings
                var addressInfo =
                    this.repository.Query<ConfigurationAppSettingValues>()
                        .Where(
                            x =>
                            x.ConfigurationAppSetting != null
                            && (string.Equals(
                                x.ConfigurationAppSetting.KeyName.Trim(),
                                "CorporateName",
                                StringComparison.CurrentCultureIgnoreCase)
                                || string.Equals(
                                    x.ConfigurationAppSetting.KeyName.Trim(),
                                    "CorporateAddress1",
                                    StringComparison.CurrentCultureIgnoreCase)
                                || string.Equals(
                                    x.ConfigurationAppSetting.KeyName.Trim(),
                                    "CorporateAddress2",
                                    StringComparison.CurrentCultureIgnoreCase)
                                || string.Equals(
                                    x.ConfigurationAppSetting.KeyName.Trim(),
                                    "CorporateCity",
                                    StringComparison.CurrentCultureIgnoreCase)
                                || string.Equals(
                                    x.ConfigurationAppSetting.KeyName.Trim(),
                                    "CorporateState",
                                    StringComparison.CurrentCultureIgnoreCase)
                                || string.Equals(
                                    x.ConfigurationAppSetting.KeyName.Trim(),
                                    "CorporateZip",
                                    StringComparison.CurrentCultureIgnoreCase)
                                || string.Equals(
                                    x.ConfigurationAppSetting.KeyName.Trim(),
                                    "CorporateCountry",
                                    StringComparison.CurrentCultureIgnoreCase)))
                        .ToList();

                output.SchoolName = this.GetSettingValue(addressInfo, "CorporateName");
                output.Address = this.GetSettingValue(addressInfo, "CorporateAddress1");
                output.Address += this.GetSettingValue(addressInfo, "CorporateAddress2").Length > 0 ? ", " + this.GetSettingValue(addressInfo, "CorporateAddress2") : string.Empty;
                output.CityStateZip = this.GetSettingValue(addressInfo, "CorporateCity");
                output.CityStateZip += this.GetSettingValue(addressInfo, "CorporateState").Length > 0 ? ", " + this.GetSettingValue(addressInfo, "CorporateState") : string.Empty;
                output.CityStateZip += this.GetSettingValue(addressInfo, "CorporateZip").Length > 0 ? " " + this.GetSettingValue(addressInfo, "CorporateZip") : string.Empty;
                output.CityStateZip += this.GetSettingValue(addressInfo, "CorporateCountry").Length > 0 ? " " + this.GetSettingValue(addressInfo, "CorporateCountry") : string.Empty;
            }
            else
            {
                // Find address in Campus
                var campus = this.repository.Query<Campus>().FirstOrDefault(x => x.ID == campusId);
                if (campus != null)
                {
                    output.SchoolName = campus.Description;
                    output.Address = campus.Address1;
                    output.Address += !string.IsNullOrEmpty(campus.Address2) ? ", " + campus.Address2 : string.Empty;
                    output.CityStateZip = campus.City;
                    output.CityStateZip += campus.State != null ? ", " + campus.State.Code : string.Empty;
                    output.CityStateZip += !string.IsNullOrEmpty(campus.Zip) ? " " + campus.Zip : string.Empty;
                    output.CityStateZip += campus.Country != null ? (campus.Country.CountryCode.ToUpper().Equals("USA") ? string.Empty : " " + campus.Country.CountryCode) : string.Empty;
                }
            }

            output.Transactions = new List<LeadTransactionDetailOutput>();
            output.TransDate = transaction.CreatedDate.ToString("MM/dd/yyyy");
            LeadTransactionDetailOutput transactionOpuput = new LeadTransactionDetailOutput();

            transactionOpuput.Voided = transaction.Voided != null ? transaction.Voided.Value : false;

            transactionOpuput.AmountReceived = Math.Abs(transaction.TransAmount).ToString();
            transactionOpuput.DocumentId = transaction.Payment != null ? transaction.Payment.CheckNumber : string.Empty;
            transactionOpuput.PaymentType = transaction.Payment != null ? transaction.Payment.PaymentType.Description : string.Empty;
            transactionOpuput.TransactionCode = transaction.TransactionCode != null ? transaction.TransactionCode.Description : string.Empty;
            transactionOpuput.TransactionId = this.GetTransactionIdHex(transactionId);
            transactionOpuput.PaymentType = transaction.Payment != null ? transaction.Payment.PaymentType.Description : string.Empty;

            if (transactionOpuput.Voided && transaction.MapTransactionId != null)
            {
                transactionOpuput.PaymentType += " (Voided)";
                output.Transactions.Add(transactionOpuput);
                var transactionCharge =
                    this.repository.Query<LeadTransactions>()
                        .FirstOrDefault(x => x.ID == transaction.MapTransactionId.Value);
                if (transactionCharge != null)
                {
                    transactionOpuput = new LeadTransactionDetailOutput();

                    transactionOpuput.Voided = transactionCharge.Voided != null ? transactionCharge.Voided.Value : false;
                    transactionOpuput.AmountReceived = Math.Abs(transactionCharge.TransAmount).ToString();

                    var payment = this.repository.Query<LeadPayments>()
                        .FirstOrDefault(x => x.ID == transactionCharge.ID);
                    if (payment != null)
                    {
                        transactionOpuput.DocumentId = payment.CheckNumber;
                        transactionOpuput.PaymentType = payment.PaymentType.Description;
                    }
                    else
                    {
                        transactionOpuput.DocumentId = string.Empty;
                        transactionOpuput.PaymentType = transactionCharge.TransactionType.Description;
                    }

                    transactionOpuput.TransactionCode = transactionCharge.TransactionCode != null
                                                            ? transactionCharge.TransactionCode.Description
                                                            : string.Empty;
                    transactionOpuput.TransactionId = this.GetTransactionIdHex(transactionCharge.ID);

                    output.Transactions.Insert(0, transactionOpuput);
                }
            }
            else
            {
                output.Transactions.Add(transactionOpuput);
            }

            return output;
        }

        /// <summary>
        /// void transaction
        /// </summary>
        /// <param name="filter">
        /// The input filter value
        /// </param>
        public void VoidTransaction(GetLeadRequirementInputModel filter)
        {
            if (string.IsNullOrEmpty(filter.TransactionId))
            {
                throw new HttpBadRequestResponseException("Missing Transaction Id");
            }

            if (filter.UserId == Guid.Empty)
            {
                throw new HttpBadRequestResponseException("Missing User Id");
            }

            var transactionId = new Guid(filter.TransactionId);
            this.VoidTransaction(transactionId, filter.UserId);
        }

        /// <summary>
        /// The have met requirements.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public string HaveMetRequirements(Guid leadId, Guid campusId)
        {
            Lead lead = this.repository.Query<Lead>().FirstOrDefault(n => n.ID == leadId && n.Campus.ID == campusId);
            if (lead != null)
            {
                if (lead.LeadStatus.SystemStatusId == 6)
                {
                    return "enrolled";
                }
                else
                {
                    GetLeadRequirementInputModel filter = new GetLeadRequirementInputModel();
                    filter.CampusId = campusId;
                    filter.leadId = leadId;

                    var mandatoryRequirements = this.GetRequirements(filter, leadId).ToList();
                    var requirementGroups = this.GetRequirementGroups(filter, leadId).ToList();

                    return this.HaveMetRequirements(leadId, campusId, mandatoryRequirements, requirementGroups).ToString();
                }
            }
            else
            {
                return "False";
            }
        }


        /// <summary>
        /// The have met requirements.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="mandatoryRequirements">
        /// The mandatory Requirements.
        /// </param>
        /// <param name="requirementGroups">
        /// The requirement Groups.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool HaveMetRequirements(Guid leadId, Guid campusId, List<LeadReqEntityOutputModel> mandatoryRequirements, List<LeadReqEntityOutputModel> requirementGroups)
        {
            GetLeadRequirementInputModel filter = new GetLeadRequirementInputModel();
            filter.CampusId = campusId;
            filter.leadId = leadId;
            List<LeadRequirementTypeOutputModel> requirementTypesMet = this.GetRequirementTypesMetOutputModel(leadId, filter, mandatoryRequirements, requirementGroups);

            return requirementTypesMet.Count(x => x.IsDefined) == requirementTypesMet.Count(x => x.IsMet);
        }

        /// <summary>
        /// The get requirement types met output model.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<LeadRequirementTypeOutputModel> GetRequirementTypesMetOutputModel(
            Guid leadId,
            GetLeadRequirementInputModel filter)
        {

            var mandatoryRequirements = this.GetRequirements(filter, leadId).ToList();
            var requirementGroups = this.GetRequirementGroups(filter, leadId).ToList();

            return this.GetRequirementTypesMetOutputModel(leadId, filter, mandatoryRequirements, requirementGroups);
        }


        /// <summary>
        /// The get requirement types met output model.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="mandatoryRequirements">
        /// The mandatory Requirements.
        /// </param>
        /// <param name="requirementGroups">
        /// The requirement Groups.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<LeadRequirementTypeOutputModel> GetRequirementTypesMetOutputModel(Guid leadId, GetLeadRequirementInputModel filter, List<LeadReqEntityOutputModel> mandatoryRequirements, List<LeadReqEntityOutputModel> requirementGroups)
        {
            // var mandatoryRequirements = this.GetRequirements(filter, leadId).ToList();
            // var requirementGroups = this.GetRequirementGroups(filter, leadId).ToList();
            var requirementTypes = this.repositoryWithInt.Query<RequirementType>().ToList();

            List<LeadRequirementTypeOutputModel> list = new List<LeadRequirementTypeOutputModel>();

            foreach (var type in requirementTypes)
            {
                var totalMet = 0;
                var totalType = 0;
                LeadRequirementTypeOutputModel output = new LeadRequirementTypeOutputModel();

                output.Name = type.Description;

                totalType = mandatoryRequirements.Count(x => x.DisplayType.ToLower().Trim() == type.Description.ToLower().Trim());
                totalMet = mandatoryRequirements.Count(x => x.DisplayType.ToLower().Trim() == type.Description.ToLower().Trim() && (x.IsApproved || x.IsOverridden));

                if (totalType > 0)
                {
                    output.IsDefined = true;
                    output.IsMet = totalType == totalMet;
                }

                list.Add(output);
            }


            foreach (var group in requirementGroups)
            {
                var isGroupMet = false;
                List<LeadReqEntityOutputModel> requirements = null;

                RequirementGroup requirementGroup = this.repository.Query<RequirementGroup>().FirstOrDefault(x => x.ID == group.Id);

                if (requirementGroup == null)
                {
                    continue;
                }

                requirements = this.GetRequirementByGroupId(filter, group.Id).ToList();

                var isRequirementsMet = requirements.Count(x => x.IsApproved || x.IsOverridden)
                                         >= requirementGroup.GetMinimumRequirementRequired();
                var isMandatoryRequirementsMet = requirements.Count(x => x.IsMandatory) == requirements.Count(x => x.IsMandatory && (x.IsApproved || x.IsOverridden));

                if (isRequirementsMet && isMandatoryRequirementsMet)
                {
                    isGroupMet = true;
                }

                if (isGroupMet)
                {
                    // Find requirements types in group that are met
                    var requirementTypeMet = requirements.Select(x => x.DisplayType).ToList();

                    foreach (var item in list)
                    {
                        if (requirementTypeMet.Contains(item.Name.ToLower().Trim()))
                        {
                            if (!item.IsDefined)
                            {
                                item.IsMet = true;
                            }
                        }
                    }
                }
                else
                {
                    // Find requirements types in group that are not met
                    var requirementTypeMet = requirements.Select(x => x.DisplayType.ToLower().Trim()).ToList();

                    foreach (var item in list)
                    {
                        if (requirementTypeMet.Contains(item.Name.ToLower().Trim()))
                        {
                            item.IsMet = false;
                        }
                    }
                }
            }

            return list.OrderBy(x => x.Name).ToList();
        }

        /// <summary>
        /// Get Mandatory Requirements
        /// </summary>
        /// <param name="leadId">
        /// The lead Id
        /// </param>
        /// <param name="campusId">
        /// The campus id
        /// </param>
        /// <returns>
        /// An IEnumerable of type Lead Requirement Entity Output Model
        /// </returns>
        private IEnumerable<LeadReqEntityOutputModel> GetMandatoryRequirements(Guid leadId, Guid campusId)
        {
            Lead lead = this.repository.Query<Lead>().FirstOrDefault(n => n.ID == leadId && n.Campus.ID == campusId);

            if (lead == null)
            {
                return null;
            }

            var leadGroupsList = lead.LeadGroupsList.Where(x => x.Status.StatusCode == "A").ToList();

            List<Requirement> requirementsInLeadGroups = leadGroupsList.SelectMany(x => x.GetRequirementsForEnrollment(true)).ToList();

            List<Requirement> requirements = this.repository.Query<RequirementEffectiveDates>()
                                                                            .Where(x => x.IsMandatory == true)
                                                                            .Select(x => x.Requirement)
                                                                            .Where(x => x.Status.StatusCode == "A")
                                                                            .ToList().Where(n => n.GetIsRequirementForAdmissionEnrollment()).ToList();
            requirements = requirements.Concat(requirementsInLeadGroups).ToList();

            var schoolreqs = requirements.Where(x => x.Status.StatusCode == "A").Distinct().Where(x => x.CampusGroup.Campuses.Select(y => y.ID).Contains(campusId) || (x.CampusGroup.IsAllCampusGrp != null && x.CampusGroup.IsAllCampusGrp.Value == true)).ToList();

            var usersList = this.GetUserList();

            if (lead.ProgramVersion != null)
            {
                var requirementProgramVersion = lead.ProgramVersion.RequirementProgramVersionBridge.Select(x => x.Requirement).Where(x => x != null && x.Status.StatusCode == "A").ToList().Where(n => n != null && n.GetIsRequirementForAdmissionEnrollment());

                requirementProgramVersion = requirementProgramVersion.Where(x => x.CampusGroup.Campuses.Select(y => y.ID).Contains(campusId) || (x.CampusGroup.IsAllCampusGrp != null && x.CampusGroup.IsAllCampusGrp.Value == true));

                var output = schoolreqs.Union(requirementProgramVersion).Distinct().Where(x => x.CampusGroup.Campuses.Select(y => y.ID).Contains(campusId) || (x.CampusGroup.IsAllCampusGrp != null && x.CampusGroup.IsAllCampusGrp.Value == true));

                output = output.ForEach(n => n.SetLeadGuid(leadId));

                output = output.ForEach(n => n.UserList = usersList);

                var finalOutput = output.Select(r =>
                {
                    var outputModel = new LeadReqEntityOutputModel();
                    return Mapper.Map(r, outputModel);
                });

                return finalOutput.OrderBy(x => x.DisplayType).ThenBy(x => x.Description).ToArray();
            }
            else
            {
                var output = schoolreqs.Distinct();

                output = output.ForEach(n => n.SetLeadGuid(leadId));

                output = output.ForEach(n => n.UserList = usersList);

                var finalOutput = output.Select(r =>
                {
                    var outputModel = new LeadReqEntityOutputModel();
                    return Mapper.Map(r, outputModel);
                });

                return finalOutput.OrderBy(x => x.DisplayType).ThenBy(x => x.Description).ToArray();
            }
        }

        /// <summary>
        /// The get optional requirements.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        private IEnumerable<LeadReqEntityOutputModel> GetOptionalRequirements(Guid leadId, Guid campusId)
        {
            Lead lead = this.repository.Query<Lead>().FirstOrDefault(n => n.ID == leadId && n.Campus.ID == campusId);

            if (lead == null)
            {
                return null;
            }

            List<Requirement> requirementsInLeadGroups = lead.LeadGroupsList.Where(x => x.Status.StatusCode == "A").SelectMany(x => x.GetOptionalRequirementsForEnrollment()).ToList();

            List<Guid> requirementsInProgramVersion = new List<Guid>();

            if (lead.ProgramVersion != null)
            {
                requirementsInProgramVersion = this.repository.Query<RequirementProgramVersionBridge>()
                                               .Where(x => x.ProgramVersion.ID == lead.ProgramVersion.ID)
                                               .Select(x => x.Requirement)
                                               .Where(x => x != null && x.Status.StatusCode == "A").ToList()
                                               .Where(n => n != null && n.GetIsRequirementForAdmissionEnrollment()).Select(x => x.ID).ToList();
            }

            /*NOT PART OF A PROGRAM VERSION*/
            var requirements = requirementsInLeadGroups
                               .Where(x => !requirementsInProgramVersion.Contains(x.ID)).ToList()
                               .Where(x => x.CampusGroup.Campuses.Select(y => y.ID).Contains(campusId) || (x.CampusGroup.IsAllCampusGrp != null && x.CampusGroup.IsAllCampusGrp.Value == true))
                               .Where(x => x.Status.StatusCode == "A").Distinct().ToList();

            var usersList = this.GetUserList();

            var output = requirements.Distinct();

            output = output.ForEach(n => n.SetLeadGuid(leadId));

            output = output.ForEach(n => n.UserList = usersList);

            var finalOutput = output.OrderBy(x => x.Type.Description).ThenBy(x => x.Description).Select(r =>
            {
                var outputModel = new LeadReqEntityOutputModel();
                return Mapper.Map(r, outputModel);
            });

            return finalOutput.ToArray();
        }

        /// <summary>
        /// Get Requirement Group Level Requirements
        /// </summary>
        /// <param name="leadId">
        /// The lead id
        /// </param>
        /// <param name="campusId">
        /// The campus id
        /// </param>
        /// <returns>
        /// An IEnumerable of type Lead Requirement Entity Output Model
        /// </returns>
        private IEnumerable<LeadReqEntityOutputModel> GetRequirementGroupLevelRequirements(Guid leadId, Guid campusId)
        {
            var lead = this.repository.Query<Lead>().FirstOrDefault(n => n.ID == leadId && n.Campus.ID == campusId);
            if (lead == null)
            {
                return null;
            }

            var leadGroupsList = lead.LeadGroupsList.Where(x => x.Status.StatusCode == "A").Select(x => x.ID).ToList();

            var requirementGroupsForEnrollment = lead.LeadGroupsList.Where(x => x.Status.StatusCode == "A").SelectMany(x => x.GetRequirementGroupsForEnrollment()).Where(x => x.Status.StatusCode == "A");

            List<RequirementGroup> requirementGroups = requirementGroupsForEnrollment.Where(n => n.CampusGroup.Campuses.Any(c => c.ID == campusId)).ToList();

            List<RequirementGroup> groupsInProgramVersion = new List<RequirementGroup>();

            if (lead.ProgramVersion != null)
            {
                groupsInProgramVersion = this.repository.Query<RequirementProgramVersionBridge>()
                        .Where(x => x.ProgramVersion.ID == lead.ProgramVersion.ID && x.RequirementGroup != null)
                        .Select(x => x.RequirementGroup)
                        .Where(x => x != null && x.Status.StatusCode == "A").ToList()
                        .Where(x => x != null && x.RequirementGroupDefinitions.Any(n => n.Requirement.GetIsRequirementForAdmissionEnrollment()))
                        .Where(x => x.Status.StatusCode == "A").ToList();
            }

            requirementGroups = requirementGroups.Union(groupsInProgramVersion).ToList();

            requirementGroups = requirementGroups.Where(x => x.SetListOfLeadGroupsId(leadGroupsList)).ToList();

            var finalOutput = requirementGroups.OrderBy(x => x.Description).Select(r =>
            {
                var outputModel = new LeadReqEntityOutputModel();
                r.SetLeadGuid(leadId);
                return Mapper.Map(r, outputModel);
            });

            return finalOutput.ToArray();
        }

        /// <summary>
        /// Get Requirement Group Level Requirement By Group
        /// </summary>
        /// <param name="reqGrpId">
        /// The requirement group id
        /// </param>
        /// <param name="campusId">
        /// The campus id
        /// </param>
        /// <param name="leadId">
        /// The lead id
        /// </param>
        /// <returns>
        /// An IEnumerable of type Lead Requirement Entity Output Model
        /// </returns>
        private IEnumerable<LeadReqEntityOutputModel> GetRequirementGroupLevelReqByGroup(Guid reqGrpId, Guid campusId, Guid leadId)
        {
            var reqGroups = this.repository.Query<RequirementGroup>().FirstOrDefault(x => x.ID == reqGrpId);
            if (reqGroups == null)
            {
                return null;
            }

            var newReqs = this.GetRequirementByGroupId(reqGrpId, campusId, leadId);
            var finalOutput = newReqs.OrderBy(x => x.Type.Description).ThenBy(x => x.Description).Select(r =>
            {
                var outputModel = new LeadReqEntityOutputModel();
                return Mapper.Map(r, outputModel);
            });

            return finalOutput.ToArray();
        }

        /// <summary>
        /// The get requirement by group id.
        /// </summary>
        /// <param name="requirementGroupId">
        /// The requirement group id.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        private IEnumerable<Requirement> GetRequirementByGroupId(Guid requirementGroupId, Guid campusId, Guid leadId)
        {
            var reqGroups = this.repository.Query<RequirementGroup>().FirstOrDefault(x => x.ID == requirementGroupId);
            if (reqGroups == null)
            {
                return null;
            }

            Lead lead = this.repository.Query<Lead>().FirstOrDefault(n => n.ID == leadId && n.Campus.ID == campusId);

            if (lead == null)
            {
                return null;
            }

            var leadGroupsList = lead.LeadGroupsList.Where(x => x.Status.StatusCode == "A").Select(x => x.ID).ToList();

            var reqDefinition = reqGroups.RequirementGroupDefinitions.Where(x => x.RequirementGroup.Status.StatusCode == "A" && x.Requirement != null && x.Requirement.Status.StatusCode == "A" && leadGroupsList.Contains(x.LeadGroup.ID)).ToList();
            reqDefinition = reqDefinition.Where(x => x.SetRequirementIsRequired()).ToList();

            var newReqs = reqDefinition.Select(n => n.Requirement).ToList()
                .Where(x => x.CampusGroup.Campuses.Select(y => y.ID).Contains(campusId) || (x.CampusGroup.IsAllCampusGrp != null && x.CampusGroup.IsAllCampusGrp.Value == true))
                .Where(x => x.GetIsRequirementForAdmissionEnrollment());

            newReqs = newReqs.ForEach(n => n.SetLeadGuid(leadId));

            var usersList = this.GetUserList();
            newReqs = newReqs.ForEach(n => n.UserList = usersList).Distinct();

            return newReqs;
        }

        /// <summary>
        /// Get Details By Requirement Id
        /// </summary>
        /// <param name="reqId">
        /// The requirement id
        /// </param>
        /// <param name="requirementType">
        /// The requirement type
        /// </param>
        /// <param name="campusId">
        /// The campus id
        /// </param>
        /// <param name="leadId">
        /// The lead id
        /// </param>
        /// <returns>
        /// A Lead Requirement Details Output Model
        /// </returns>
        private LeadReqDetailsOutputModel GetDetailsByReqId(Guid reqId, string requirementType, Guid campusId, Guid leadId)
        {
            var reqParent = new LeadReqDetailsOutputModel();

            switch (requirementType)
            {
                case "Document":

                    var reqs = this.repository.Query<LeadDocument>().Where(n => n.Requirement.ID == reqId && n.Lead.ID == leadId).OrderBy(n => n.DateReceived).FirstOrDefault();
                    var history = this.repository.Query<DocumentHistory>().Where(n => n.DocumentId == reqs.Requirement.ID && n.LeadId == leadId).OrderByDescending(x => x.UploadDate);

                    if (reqs != null)
                    {
                        reqParent.Id = reqs.ID;

                        reqParent.DocumentId = reqs.Requirement.ID;
                        reqParent.DateReceived = reqs.DateReceived != null ? reqs.DateReceived.Value.ToShortDateString() : string.Empty;
                        reqParent.Description = reqs.Requirement.Description;

                        if (reqs.DocumentStatus != null)
                        {
                            reqParent.IsApproved = reqs.DocumentStatus.IsApproved;
                            reqParent.DocumentStatusId = reqs.DocumentStatus.ID;
                        }

                        reqParent.ApprovalDate = string.Empty;

                        if (reqParent.IsApproved)
                        {
                            reqParent.ApprovalDate = reqs.ApprovalDate != null ? reqs.ApprovalDate.Value.ToShortDateString() : string.Empty;
                        }

                        reqParent.IsOverridden = reqs.Override;
                        reqParent.OverrideReason = reqs.OverrideReason;
                        reqParent.LeadId = reqs.Lead.ID;

                        reqParent.DocumentHistory = new List<LeadReqDetailsDocOutputModel>();

                        string parentFolder = null;
                        var configurationAppSetting = this.repositoryWithInt.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == "CREATEDOCUMENTPATH");
                        if (configurationAppSetting != null)
                        {
                            parentFolder = Path.Combine(configurationAppSetting.ConfigurationValues[0].Value, reqParent.Description);
                        }

                        if (history.Any())
                        {
                            foreach (var h in history)
                            {
                                var reqHistoryOutput = new LeadReqDetailsDocOutputModel
                                {
                                    Id = h.ID,
                                    FileName = h.FileName,
                                    Extension = h.FileExtension,
                                    FileNameWithExtension = h.DisplayName + h.FileExtension,
                                    UploadDate = h.UploadDate != null ? h.UploadDate.Value.ToShortDateString() : string.Empty,
                                    FilePath = parentFolder + "\\" + h.ID.ToString() + h.FileExtension,
                                    User = h.ModUser
                                };

                                reqParent.DocumentHistory.Add(reqHistoryOutput);
                            }
                        }
                    }

                    break;

                case "Test":
                    var reqTests = this.repository.Query<LeadEntranceTest>().Where(n => n.Requirement.ID == reqId && n.LeadObj.ID == leadId);
                    var reqTestFirst = reqTests.FirstOrDefault();

                    if (reqTestFirst != null)
                    {
                        reqParent.Id = reqTestFirst.ID;
                        reqParent.Id = reqTestFirst.ID;
                        reqParent.DocumentId = reqTestFirst.Requirement.ID;
                        reqParent.Description = reqTestFirst.Requirement.Description;
                        reqParent.LeadId = reqTestFirst.LeadObj.ID;

                        reqParent.TestHistory = new List<LeadReqDetailsTestsOutputModel>();

                        foreach (var r in reqTests.OrderByDescending(x => x.ModDate).ThenByDescending(x => x.CompletedDate))
                        {
                            var reqTesHistoryOutput = new LeadReqDetailsTestsOutputModel
                            {
                                Id = r.ID,
                                DateReceived = r.CompletedDate.HasValue ? r.CompletedDate.Value.ToShortDateString() : string.Empty,
                                DocumentId = r.Requirement.ID,
                                RequirementType = r.Requirement.Type.Description
                            };

                            bool isApproved = false;
                            if (r.Pass != null)
                            {
                                isApproved = (bool)r.Pass;
                            }

                            reqTesHistoryOutput.IsApproved = isApproved;

                            reqTesHistoryOutput.Pass = "No";

                            reqTesHistoryOutput.Score = r.ActualScore != null ? r.ActualScore.ToString() : string.Empty;
                            if (isApproved)
                            {
                                reqTesHistoryOutput.Pass = "Yes";
                            }

                            reqTesHistoryOutput.IsOverridden = r.Override != null && (bool)r.Override;

                            reqParent.TestHistory.Add(reqTesHistoryOutput);
                        }
                    }
                    break;

                case "Interview":
                case "Tour":
                case "Event":
                    var reqsReceived = this.repository.Query<AdLeadReqsReceived>().Where(n => n.Requirement.ID == reqId && n.Lead.ID == leadId);
                    var reqsReceivedFirst = reqsReceived.FirstOrDefault();

                    if (reqsReceivedFirst != null)
                    {
                        reqParent.Id = reqsReceivedFirst.ID;
                        reqParent.DocumentId = reqsReceivedFirst.Requirement.ID;
                        reqParent.Description = reqsReceivedFirst.Requirement.Description;
                        reqParent.LeadId = reqsReceivedFirst.Lead.ID;

                        reqParent.ReqsHistory = new List<LeadReqDetailsReqsOutputModel>();

                        foreach (var r in reqsReceived.OrderByDescending(x => x.ModDate).ThenByDescending(x => x.ReceivedDate))
                        {
                            var requirement = new LeadReqDetailsReqsOutputModel
                            {
                                Id = r.ID,
                                DocumentId = r.Requirement.ID,
                                Description = r.Requirement.Description,
                                IsOverridden = r.Override,
                                OverrideReason = r.OverrideReason
                            };

                            if (r.ReceivedDate != null)
                            {
                                requirement.DateReceived = r.ReceivedDate != null ? r.ReceivedDate.Value.ToShortDateString() : string.Empty;
                            }

                            requirement.IsApproved = r.IsApproved != null && (bool)r.IsApproved;

                            reqParent.ReqsHistory.Add(requirement);
                        }
                    }

                    break;

                case "Fee":
                    var reqsTranReceived = this.repository.Query<AdLeadTranReceived>().Where(n => n.Requirement.ID == reqId && n.Lead.ID == leadId);
                    var reqsTranReceivedFirst = reqsTranReceived.FirstOrDefault();

                    if (reqsTranReceivedFirst != null)
                    {
                        reqParent.Id = reqsTranReceivedFirst.ID;
                        reqParent.DocumentId = reqsTranReceivedFirst.Requirement.ID;
                        reqParent.Description = reqsTranReceivedFirst.Requirement.Description;
                        reqParent.LeadId = reqsTranReceivedFirst.Lead.ID;

                        var transactions = this.repository.Query<LeadTransactions>()
                        .Where(n => n.TransactionCode.SysTransCodeId == 20 && n.LeadObj.ID == leadId && n.TransactionType.Description.ToUpper() == "PAYMENT" && n.Requirement != null && n.Requirement.ID == reqId);

                        reqParent.FeeHistory = new List<LeadReqDetailsFeeOutputModel>();
                        var sortedTransactions = transactions.OrderByDescending(x => x.ModDate).ThenByDescending(x => x.TransDate).ThenByDescending(x => x.Voided);
                        foreach (var r in sortedTransactions)
                        {
                            // Transaction converted into Requirement for Output model
                            var requirement = new LeadReqDetailsFeeOutputModel
                            {
                                Id = r.ID,
                            };

                            requirement.TransDate = r.TransDate.ToShortDateString();

                            if (r.Payment != null)
                            {
                                requirement.TransDocumentId = r.Payment.CheckNumber;
                                requirement.Type = r.Payment.PaymentType.ID.ToString();
                            }

                            requirement.Code = r.TransactionCode.Description;
                            requirement.TranDescription = r.TransDescription;
                            requirement.Reference = r.TransReference;
                            requirement.User = r.ModUser;

                            if ((r.Voided == null) || (r.Voided != null && r.Voided.Value == false))
                            {
                                requirement.Amount = Math.Abs(r.TransAmount).ToString("C");
                                requirement.IsVoided = false;
                            }
                            else
                            {
                                requirement.Amount = "(" + Math.Abs(r.TransAmount).ToString("C") + ")";
                                requirement.IsVoided = true;
                            }


                            reqParent.FeeHistory.Add(requirement);
                        }
                    }

                    break;
            }

            return reqParent;
        }

        /// <summary>
        /// Update Existing Document Requirement
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        /// <param name="reqId">
        /// The requirement id
        /// </param>
        private void UpdateExistingDocumentRequirement(LeadRequirementInputModel filter, Guid reqId)
        {
            if (!filter.IsOverridden)
            {
                if (string.IsNullOrEmpty(filter.DocumentStatusId))
                {
                    throw new HttpBadRequestResponseException("Document Status is required.");
                }
            }
            else
            {
                filter.DateReceived = DateTime.Now;
                filter.ApprovalDate = DateTime.Now;
            }

            var leadDocReq = this.repository.Query<LeadDocument>().Single(n => n.ID == reqId);

            var user = this.repository.Query<User>().FirstOrDefault(n => n.ID == filter.UserId);
            string modUser = null;
            if (user != null)
            {
                modUser = user.UserName;
            }

            DocumentStatus docStatus = null;
            if (!string.IsNullOrEmpty(filter.DocumentStatusId))
            {
                docStatus = this.repository.Query<DocumentStatus>().FirstOrDefault(n => n.ID == Guid.Parse(filter.DocumentStatusId));
            }
            else
            {
                if (filter.IsOverridden)
                {
                    docStatus = this.repository.Query<DocumentStatus>().OrderBy(x => x.ModDate).FirstOrDefault(x => x.SystemDocumentStatus.ID == 1);
                }
            }

            if (docStatus != null)
            {
                leadDocReq.SaveDocumentRecord(
                    filter.DateReceived,
                    docStatus,
                    filter.IsOverridden,
                    filter.overReason,
                    filter.ApprovalDate,
                    modUser);
            }
            else
            {
                leadDocReq.SaveDocumentRecord(
                    filter.DateReceived,
                    filter.IsOverridden,
                    filter.overReason,
                    filter.ApprovalDate,
                    modUser);
            }
            //if the lead is not student and/or is unenrrolled lead. the following if is for the case where student is unenrolled.
            if (leadDocReq.Lead.StudentId.Equals(Guid.Empty))
            {
                var entranceOverride = this.repository.Query<AdEntrTestOverRide>()
                    .FirstOrDefault(x => x.LeadObj.ID == leadDocReq.Lead.ID &&
                                         x.Requirement.ID == leadDocReq.Requirement.ID); //&&
                                         //x.OverRide == ((bool) leadDocReq.Override ? "1" : "0"));
                if (entranceOverride != null)
                {
                    entranceOverride.SaveEntrTestOverRideRecord(modUser, (bool)leadDocReq.Override ? "1" : "0");
                    this.repository.Save(entranceOverride);
                }
            }
            else
            {
                var entranceOverride = this.repository.Query<AdEntrTestOverRide>()
                    .FirstOrDefault(x => x.LeadObj.ID == leadDocReq.Lead.ID &&
                                         x.Requirement.ID == leadDocReq.Requirement.ID && x.LeadObj.StudentId == leadDocReq.Lead.StudentId);
                //x.OverRide == ((bool)leadDocReq.Override ? "1" : "0") && x.LeadObj.StudentId == leadDocReq.Lead.StudentId);
                if (entranceOverride != null)
                {
                    entranceOverride.SaveEntrTestOverRideRecord(modUser, (bool) leadDocReq.Override ? "1" : "0");
                    this.repository.Save(entranceOverride);
                }
            }    
            this.repository.Save(leadDocReq);
        }

        /// <summary>
        /// Update Existing Transaction Requirement (Fee)
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        /// <param name="reqId">
        /// The requirement id
        /// </param>
        private void UpdateExistingTranRequirement(LeadRequirementInputModel filter, Guid reqId)
        {
            var leadTranReq = this.repository.Query<AdLeadTranReceived>().Single(n => n.ID == reqId);

            var lead = this.repository.Query<Lead>().Single(n => n.ID == filter.LeadId);
            var user = this.repository.Query<User>().FirstOrDefault(n => n.ID == filter.UserId);
            var req = this.repository.Query<Requirement>().Single(n => n.ID == filter.ReqId);

            string modUser = null;
            if (user != null)
            {
                modUser = user.UserName;
            }

            leadTranReq.SaveTransRecord(lead, req, DateTime.Now, modUser, filter.IsOverridden, filter.overReason);
            this.repository.Save(leadTranReq);
            if (leadTranReq.Lead.StudentId.Equals(Guid.Empty))
            {
                var entranceOverride = this.repository.Query<AdEntrTestOverRide>()
                    .FirstOrDefault(x => x.LeadObj.ID == leadTranReq.Lead.ID &&
                                         x.Requirement.ID == leadTranReq.Requirement.ID); //&&
                                         //x.OverRide == ((bool)leadTranReq.Override ? "1" : "0"));
                if (entranceOverride != null)
                {
                    entranceOverride.SaveEntrTestOverRideRecord(modUser, (bool) leadTranReq.Override ? "1" : "0");
                    this.repository.Save(entranceOverride);
                }
            }
            else
            {
                var entranceOverride = this.repository.Query<AdEntrTestOverRide>()
                    .FirstOrDefault(x => x.LeadObj.ID == leadTranReq.Lead.ID &&
                                         x.Requirement.ID == leadTranReq.Requirement.ID && x.LeadObj.StudentId == leadTranReq.Lead.StudentId);
                //x.OverRide == ((bool)leadTranReq.Override ? "1" : "0") && x.LeadObj.StudentId == leadTranReq.Lead.StudentId);
                if (entranceOverride != null)
                {
                    entranceOverride.SaveEntrTestOverRideRecord(modUser, (bool) leadTranReq.Override ? "1" : "0");
                    this.repository.Save(entranceOverride);
                }
            }
                if (!string.IsNullOrEmpty(filter.TransDescription) && !filter.TransCodeId.IsEmpty() && filter.TransAmount > 0)
            {
                this.InsertFeeTransactions(filter, modUser);
            }
        }

        /// <summary>
        /// Insert Test Requirement
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        private void InsertTestRequirement(LeadRequirementInputModel filter)
        {
            var lead = this.repository.Query<Lead>().Single(n => n.ID == filter.LeadId);
            var req = this.repository.Query<Requirement>().Single(n => n.ID == filter.ReqId);
            var user = this.repository.Query<User>().FirstOrDefault(n => n.ID == filter.UserId);
            string modUser = null;
            if (user != null)
            {
                modUser = user.UserName;
            }

            var leadTestReq = new LeadEntranceTest(lead, req, filter.DateReceived, filter.Score, filter.MinScore, filter.IsOverridden, filter.overReason, modUser);
            this.repository.Save(leadTestReq);
            if (lead.StudentId.Equals(Guid.Empty))
            {
                var leadTestOverrideReq = new AdEntrTestOverRide(lead, req, modUser, filter.IsOverridden ? "1" : "0");
                this.repository.Save(leadTestOverrideReq);
            }
            else
            {
                var student = this.repository.Query<Student>().Single(n => n.ID == lead.StudentId);
                var leadTestOverrideReq = new AdEntrTestOverRide(lead, req, modUser, filter.IsOverridden ? "1" : "0", student);
                this.repository.Save(leadTestOverrideReq);
            }
        }

        /// <summary>
        /// Insert a new document requirement 
        /// </summary>
        /// <param name="filter">
        /// The input filter value
        /// </param>
        private void InsertDocumentRequirement(LeadRequirementInputModel filter)
        {
            if (!filter.IsOverridden)
            {
                if (string.IsNullOrEmpty(filter.DocumentStatusId))
                {
                    throw new HttpBadRequestResponseException("Document Status is required.");
                }
            }
            else
            {
                filter.DateReceived = DateTime.Now;
                filter.ApprovalDate = DateTime.Now;
            }

            var user = this.repository.Query<User>().FirstOrDefault(n => n.ID == filter.UserId);
            string modUser = null;
            if (user != null)
            {
                modUser = user.UserName;
            }

            DocumentStatus docStatus = null;
            if (!string.IsNullOrEmpty(filter.DocumentStatusId))
            {
                docStatus =
                    this.repository.Query<DocumentStatus>()
                        .FirstOrDefault(n => n.ID == Guid.Parse(filter.DocumentStatusId));
            }
            else
            {
                if (filter.IsOverridden)
                {
                    docStatus = this.repository.Query<DocumentStatus>().OrderBy(x => x.ModDate).FirstOrDefault(x => x.SystemDocumentStatus.ID == 1);
                }
            }

            var requirement = this.repository.Query<Requirement>().Single(n => n.ID == filter.ReqId);

            var lead = this.repository.Query<Lead>().Single(n => n.ID == filter.LeadId);

            LeadDocument leadDocReq = null;
            if (docStatus != null)
            {
                leadDocReq = new LeadDocument(
                    filter.DateReceived,
                    docStatus,
                    requirement,
                    lead,
                    filter.IsOverridden,
                    filter.overReason,
                    filter.ApprovalDate,
                    modUser);
            }
            else
            {
                leadDocReq = new LeadDocument(
                    filter.DateReceived,
                    requirement,
                    lead,
                    filter.IsOverridden,
                    filter.overReason,
                    filter.ApprovalDate,
                    modUser);
            }

            this.repository.Save(leadDocReq);


            if (lead.StudentId.Equals(Guid.Empty))
            {
                var leadTestOverrideReq = new AdEntrTestOverRide(lead, requirement, modUser, filter.IsOverridden ? "1" : "0");
                this.repository.Save(leadTestOverrideReq);
            }
            else
            {
                var student = this.repository.Query<Student>().Single(n => n.ID == lead.StudentId);
                var leadTestOverrideReq = new AdEntrTestOverRide(lead, requirement, modUser, filter.IsOverridden ? "1" : "0", student);
                this.repository.Save(leadTestOverrideReq);
            }
        }

        /// <summary>
        /// Delete Document File Record
        /// </summary>
        /// <param name="docReqId">
        /// The document requirement id
        /// </param>
        /// <param name="filePath">
        /// The document requirement file path
        /// </param>
        private void DeleteDocumentFileRecord(Guid docReqId, string filePath)
        {
            var leadDoc = this.repository.Query<DocumentHistory>().Single(n => n.ID == docReqId);
            this.repository.Delete(leadDoc);

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        /// <summary>
        /// Delete Test Record
        /// </summary>
        /// <param name="docReqId">
        /// The document requirement id
        /// </param>
        private void DeleteTestRecord(Guid docReqId)
        {
            var leadReq = this.repository.Query<LeadEntranceTest>().Single(n => n.ID == docReqId);
            this.repository.Delete(leadReq);
            //if the lead is not student and/or is unenrrolled lead. the following if is for the case where student is unenrolled.
            if (leadReq.LeadObj.StudentId.Equals(Guid.Empty))
            {
                var leadEntrOverRide = this.repository.Query<AdEntrTestOverRide>().FirstOrDefault(n => n.LeadObj.ID == leadReq.LeadObj.ID && n.OverRide == ((bool)leadReq.Override ? "1" : "0") && n.Requirement.ID == leadReq.Requirement.ID && n.ModDate == leadReq.ModDate);
                if (leadEntrOverRide!=null)
                {
                    this.repository.Delete(leadEntrOverRide);
                }
                else
                {
                    leadEntrOverRide = this.repository.Query<AdEntrTestOverRide>().FirstOrDefault(n => n.LeadObj.ID == leadReq.LeadObj.ID && n.OverRide == ((bool)leadReq.Override ? "1" : "0") && n.Requirement.ID == leadReq.Requirement.ID);
                    if (leadEntrOverRide != null)
                    {
                        this.repository.Delete(leadEntrOverRide);
                    }
                }
            }
            else
            {
                var leadEntrOverRide = this.repository.Query<AdEntrTestOverRide>().FirstOrDefault(n => n.LeadObj.ID == leadReq.LeadObj.ID && n.OverRide == ((bool)leadReq.Override ? "1" : "0") && n.Requirement.ID == leadReq.Requirement.ID && n.LeadObj.StudentId == leadReq.LeadObj.StudentId);
                if (leadEntrOverRide != null)
                {
                    this.repository.Delete(leadEntrOverRide);
                }
            }
            
        }

        /// <summary>
        /// Delete Event Requirement Record 
        /// </summary>
        /// <param name="docReqId">
        /// The document requirement id
        /// </param>
        private void DeleteEventReqRecord(Guid docReqId)
        {
            var leadReq = this.repository.Query<AdLeadReqsReceived>().Single(n => n.ID == docReqId);
            this.repository.Delete(leadReq);
            //if the lead is not student and/or is unenrrolled lead. the following if is for the case where student is unenrolled.
            if (leadReq.Lead.StudentId.Equals(Guid.Empty))
            {
                var leadEntrOverRide = this.repository.Query<AdEntrTestOverRide>().FirstOrDefault(n => n.LeadObj.ID == leadReq.Lead.ID && n.OverRide == ((bool)leadReq.Override ? "1" : "0") && n.Requirement.ID == leadReq.Requirement.ID && n.ModDate == leadReq.ModDate);
                if (leadEntrOverRide != null)
                {
                    this.repository.Delete(leadEntrOverRide);
                }
                else
                {
                    leadEntrOverRide = this.repository.Query<AdEntrTestOverRide>().FirstOrDefault(n => n.LeadObj.ID == leadReq.Lead.ID && n.OverRide == ((bool)leadReq.Override ? "1" : "0") && n.Requirement.ID == leadReq.Requirement.ID);
                    if (leadEntrOverRide != null)
                    {
                        this.repository.Delete(leadEntrOverRide);
                    }
                }
            }
            else
            {
                var leadEntrOverRide = this.repository.Query<AdEntrTestOverRide>().FirstOrDefault(n => n.LeadObj.ID == leadReq.Lead.ID && n.OverRide == ((bool)leadReq.Override ? "1" : "0") && n.Requirement.ID == leadReq.Requirement.ID && n.LeadObj.StudentId == leadReq.Lead.StudentId);
                if (leadEntrOverRide != null)
                {
                    this.repository.Delete(leadEntrOverRide);
                }
            }
        }

        /// <summary>
        /// Insert Requirement Event Requirement
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        private void InsertReqEventRequirement(LeadRequirementInputModel filter)
        {
            var lead = this.repository.Query<Lead>().Single(n => n.ID == filter.LeadId);
            var req = this.repository.Query<Requirement>().Single(n => n.ID == filter.ReqId);
            var user = this.repository.Query<User>().FirstOrDefault(n => n.ID == filter.UserId);
            string modUser = null;
            if (user != null)
            {
                modUser = user.UserName;
            }

            var leadReq = new AdLeadReqsReceived(lead, req, filter.DateReceived, filter.IsOverridden, filter.overReason, modUser);

            this.repository.Save(leadReq);


            if (lead.StudentId.Equals(Guid.Empty))
            {
                var leadTestOverrideReq = new AdEntrTestOverRide(lead, req, modUser, filter.IsOverridden ? "1" : "0");
                this.repository.Save(leadTestOverrideReq);
            }
            else
            {
                var student = this.repository.Query<Student>().Single(n => n.ID == lead.StudentId);
                var leadTestOverrideReq = new AdEntrTestOverRide(lead, req, modUser, filter.IsOverridden ? "1" : "0", student);
                this.repository.Save(leadTestOverrideReq);
            }
        }

        /// <summary>
        /// Insert Fee Requirement
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        private void InsertFeeRequirement(LeadRequirementInputModel filter)
        {
            if (!string.IsNullOrWhiteSpace(filter.TransReference) && filter.TransReference.Length > 50)
            {
                switch (filter.PayTypeId)
                {
                    case 2:
                        {
                            throw new HttpBadRequestResponseException("Check Number max length is 50 characters.");
                        }

                    case 3:
                        {
                            throw new HttpBadRequestResponseException("C/C Authorization max length is 50 characters.");
                        }

                    case 4:
                        {
                            throw new HttpBadRequestResponseException("EFT Number max length is 50 characters.");
                        }

                    case 5:
                        {
                            throw new HttpBadRequestResponseException("Money Order Number max length is 50 characters.");
                        }

                    case 6:
                        {
                            throw new HttpBadRequestResponseException("Non Cash Reference # max length is 50 characters.");
                        }
                }
            }

            var lead = this.repository.Query<Lead>().Single(n => n.ID == filter.LeadId);
            var req = this.repository.Query<Requirement>().Single(n => n.ID == filter.ReqId);
            var user = this.repository.Query<User>().FirstOrDefault(n => n.ID == filter.UserId);
            string modUser = null;
            if (user != null)
            {
                modUser = user.UserName;
            }

            var leadReq = new AdLeadTranReceived(
                lead,
                req,
                DateTime.Now,
                modUser,
                filter.IsOverridden,
                filter.overReason);
            this.repository.Save(leadReq);

            if (lead.StudentId.Equals(Guid.Empty))
            {
                var leadTestOverrideReq = new AdEntrTestOverRide(lead, req, modUser, filter.IsOverridden ? "1" : "0");
                this.repository.Save(leadTestOverrideReq);
            }
            else
            {
                var student = this.repository.Query<Student>().Single(n => n.ID == lead.StudentId);
                var leadTestOverrideReq = new AdEntrTestOverRide(lead, req, modUser, filter.IsOverridden ? "1" : "0", student);
                this.repository.Save(leadTestOverrideReq);
            }

            if (!string.IsNullOrEmpty(filter.TransDescription) && !filter.TransCodeId.IsEmpty() && filter.TransAmount > 0)
            {
                this.InsertFeeTransactions(filter, modUser);
            }
        }

        /// <summary>
        /// Insert Fee Transactions
        /// </summary>
        /// <param name="filter">
        /// The filter input value
        /// </param>
        /// <param name="modUser">
        /// The user that is inserting the fee transaction
        /// </param>
        /// <returns>
        /// The payment transaction id
        /// </returns>
        private Guid InsertFeeTransactions(LeadRequirementInputModel filter, string modUser)
        {
            var chargeGuid = new Guid(filter.PostTransactionId);
            var paymentGuid = new Guid(filter.PaymentTransactionId);

            var result = this.repository.Session.CreateSQLQuery(
               "exec USP_AD_PostLeadTransactions_New :LeadTransactionId, :LeadId, :TransCodeId, :TransReference, :TransDescrip, :TransAmount, :TransTypeId, :IsEnrolled, :TransDate, :IsVoided, :CampusId, :PaymentTransactionId, :PaymentTypeId, :CheckNumber, :CreatedDate, :ModUser, :ModDate, :LeadRequirementId")
               .SetGuid("LeadTransactionId", chargeGuid)
               .SetGuid("LeadId", filter.LeadId)
               .SetGuid("TransCodeId", filter.TransCodeId)
               .SetString("TransReference", filter.TransReference)
               .SetString("TransDescrip", filter.TransDescription)
               .SetDecimal("TransAmount", filter.TransAmount)
               .SetInt32("TransTypeId", 0)
               .SetInt32("IsEnrolled", 0)
               .SetDateTime("TransDate", filter.TransDate)
               .SetBoolean("IsVoided", false)
               .SetGuid("CampusId", filter.CampusId)
               .SetGuid("PaymentTransactionId", paymentGuid)
               .SetInt32("PaymentTypeId", filter.PayTypeId)
               .SetString("CheckNumber", filter.PaymentReference)
               .SetDateTime("CreatedDate", DateTime.Now)
               .SetString("ModUser", modUser)
               .SetDateTime("ModDate", DateTime.Now)
               .SetGuid("LeadRequirementId", filter.ReqId)
               .ExecuteUpdate();

            return paymentGuid;
        }

        /// <summary>
        /// Void Transaction
        /// </summary>
        /// <param name="transactionId">
        /// The transaction id 
        /// </param>
        /// <param name="userId">
        /// The user Id.
        /// </param>
        private void VoidTransaction(Guid transactionId, Guid userId)
        {
            var leadTransaction = this.repository.Query<LeadTransactions>().FirstOrDefault(n => n.ID == transactionId);
            var user = this.repository.Query<User>().FirstOrDefault(n => n.ID == userId);

            if (leadTransaction == null)
            {
                throw new HttpBadRequestResponseException("No transaction found");
            }

            leadTransaction.SetTransactionAsVoid(user.UserName);

            if (leadTransaction.MapTransactionId != null)
            {
                var chargeTransaction = this.repository.Query<LeadTransactions>().FirstOrDefault(n => n.ID == leadTransaction.MapTransactionId.Value);
                if (chargeTransaction != null)
                {
                    chargeTransaction.SetTransactionAsVoid(user.UserName);
                    this.repository.Save(chargeTransaction);
                }
            }

            this.repository.Save(leadTransaction);

            var requirement = leadTransaction.Requirement;
            var voidedTransactions = requirement.LeadTransactions.Count(n => n.TransactionCode.SysTransCodeId == 20 && n.TransactionType.Description.ToUpper() == "PAYMENT" && (n.Voided != null && n.Voided.Value));
            var totalTransaction = requirement.LeadTransactions.Count(n => n.TransactionCode.SysTransCodeId == 20 && n.TransactionType.Description.ToUpper() == "PAYMENT");
            if (voidedTransactions == totalTransaction)
            {
                var transactionReceipt = requirement.LeadTranReceived.OrderByDescending(x => x.ReceivedDate).FirstOrDefault();
                if (transactionReceipt != null)
                {
                    transactionReceipt.SetIsApproved(false);
                    transactionReceipt.SetModifiedDate(DateTime.Now);
                    this.repository.Save(transactionReceipt);
                }
            }
        }

        /// <summary>
        /// Get User List
        /// </summary>
        /// <returns>
        /// A List of user output model.
        /// </returns>
        private List<UserOutputModel> GetUserList()
        {
            var users = this.repository.Query<User>();

            var userList = new List<UserOutputModel>();
            foreach (var user in users)
            {
                var userOutput = new UserOutputModel
                {
                    Id = user.ID,
                    UserName = user.UserName,
                    FullName = user.FullName
                };

                userList.Add(userOutput);
            }

            return userList;
        }


        /// <summary>
        /// Get Transaction Id HEX
        /// </summary>
        /// <param name="transactionId">
        /// The transaction id
        /// </param>
        /// <returns>
        /// The transaction id hex
        /// </returns>
        private string GetTransactionIdHex(Guid transactionId)
        {
            var ba = Encoding.Default.GetBytes(transactionId.ToString().Substring(0, 6));

            var hexString = BitConverter.ToString(ba);
            return hexString.Replace("-", string.Empty);
        }

        /// <summary>
        /// The get setting value.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="keyName">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetSettingValue(List<ConfigurationAppSettingValues> settings, string keyName)
        {
            var output = settings.FirstOrDefault(x => string.Equals(x.ConfigurationAppSetting.KeyName.Trim(), keyName, StringComparison.CurrentCultureIgnoreCase));
            return output != null ? output.Value : string.Empty;
        }
    }
}