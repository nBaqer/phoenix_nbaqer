﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadQueueBo.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the LeadQueueBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Domain.Infrastructure.Entities;
    using Domain.Lead;

    using FAME.Advantage.Domain.SystemStuff;

    using Messages.Lead.Queue;

    /// <summary>
    /// The lead queue bo.
    /// </summary>
    internal class LeadQueueBo
    {
        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepository repository;

        // ReSharper disable NotAccessedField.Local

        /// <summary>
        /// The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        /// The status list.
        /// </summary>
        private readonly ICollection<int> statusList = new List<int> { 1, 2, 4, 5 };

        // ReSharper restore NotAccessedField.Local

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadQueueBo"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="repository">
        /// The GUID repository
        /// </param>
        /// <param name="repositoryWithInt">
        /// The integer repository
        /// </param>
        public LeadQueueBo(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            this.repositoryWithInt = repositoryWithInt;
        }

        /// <summary>
        ///  The get queue lead info.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <returns>
        /// The list of queue items<see cref="LeadQueueInfo"/>.
        /// </returns>
        public IList<LeadQueueInfo> GetQueueLeadInfo(QueueInputmodel filter)
        {
            var leadlist = this.repository.Query<Lead>()
                .Where(
                    x =>
                    x.AdmissionRepUserObj.ID.ToString() == filter.AssingRepId
                    && x.Campus.ID.ToString() == filter.CampusId).Where(e => this.statusList.Contains(e.LeadStatus.SystemStatus.ID));

            // Create the list 
            var leadInfoList = new List<LeadQueueInfo>();

            foreach (Lead lead in leadlist)
            {
                var leadrow = new LeadQueueInfo { Id = lead.ID.ToString() };
                var leadtasks = lead.TasksList;
                var donetask =
                    lead.TasksList.Where(x => x.Status == 2).OrderByDescending(t => t.EndDate).FirstOrDefault();

                var pendingtask = leadtasks.Where(x => x.Status == 0).OrderBy(t => t.StartDate).FirstOrDefault();
                leadrow.DateReceived = lead.AssignedDate.GetValueOrDefault();
                if (donetask != null)
                {
                    leadrow.LastActivity = donetask.Activity;
                    leadrow.LastActivityCode = donetask.Code;
                    leadrow.DateLast = donetask.EndDate;

                    // calculate the Age
                    leadrow.Age = this.CalculateAge(Convert.ToDateTime(donetask.EndDate));
                }
                else
                {
                    leadrow.Age = this.CalculateAge(leadrow.DateReceived); // calculate according to the date received
                }

                if (pendingtask != null)
                {
                    leadrow.NextActivity = pendingtask.Activity;
                    leadrow.DateNext = pendingtask.StartDate;
                }

                leadrow.FirstName = lead.FirstName;
                leadrow.LastName = lead.LastName;
                leadrow.Status = lead.LeadStatus.Description;
                var phoneobj = lead.LeadPhoneList;
                if (phoneobj == null || phoneobj.Count == 0)
                {
                    leadrow.Phone = string.Empty;
                    leadrow.IsInternational = false;
                }
                else
                {
                    // take the 1st phone that is displayed on the page. so removed the condition of x => x.Position == 1 in the First()
                    var phoneItem = phoneobj.OrderBy(x => x.Position).First();
                    leadrow.Phone = phoneItem.Phone;
                    leadrow.IsInternational = phoneItem.IsForeignPhone;

                    // MASK the phone
                    if (leadrow.IsInternational == false)
                    {
                        if (leadrow.Phone.Length == 10)
                        {
                            leadrow.Phone = string.Format(
                                "({0}) {1}-{2}",
                                leadrow.Phone.Substring(0, 3),
                                leadrow.Phone.Substring(3, 3),
                                leadrow.Phone.Substring(6));
                        }
                    }
                }

                leadrow.ProgramInterest = (lead.ProgramObj == null) ? string.Empty : lead.ProgramObj.ProgDescrip;

                if (leadrow.DateReceived > DateTime.Now)
                {
                    // This condition should not exists!! The Date of receiving must be always minor that now
                    leadrow.Received = leadrow.DateReceived.Date.ToString("hh:mm tt").ToLowerInvariant();

                    // leadrow.Age = "00:00";
                }
                else
                {
                    leadrow.Received = leadrow.DateReceived.Date == DateTime.Now.Date
                                           ? leadrow.DateReceived.ToString("hh:mm tt").ToLowerInvariant()
                                           : leadrow.DateReceived.Date.ToString("MM/dd/yy").ToLowerInvariant();
                }

                leadrow.StatusSysId = lead.LeadStatus.SystemStatus.ID;

                leadInfoList.Add(leadrow);
            }

            // leadInfoList = leadInfoList.OrderBy(x => x.StatusSysId).ThenByDescending(x => x.DateReceived).ToList();
            leadInfoList = leadInfoList.OrderByDescending(x => x.DateReceived).ToList();

            return leadInfoList;
        }

        /// <summary>
        /// Set the new phone
        /// </summary>
        /// <param name="filter">
        /// The filter
        /// </param>
        /// <returns>
        /// The phone input model
        /// </returns>
        public QueuePhoneInputModel SetUpdateLeadPhone(QueuePhoneInputModel filter)
        {
            // Phone can exists or not in the list of the lead
            var lead = this.repository.Query<Lead>().SingleOrDefault(x => x.ID.ToString() == filter.LeadId);
            if (lead == null)
            {
                throw new ApplicationException("Operation was aborted: Lead was not found in the database");
            }

            var phoneList = lead.LeadPhoneList;
            if (phoneList != null && phoneList.Count > 0)
            {
                var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode.ToUpper() == "A");
                var phoneItem = phoneList.OrderBy(x => x.Position).First();
                var newphone = new LeadPhone(
                    phoneItem.ID,
                    filter.Phone,
                    phoneItem.Extension,
                    filter.IsForeignPhone,
                    filter.Position,
                    filter.UserId,
                    DateTime.Now,
                    phoneItem.SyPhoneTypeObj,
                    lead,
                    status);

                phoneItem.UpdatePhone(newphone, filter.UserId, phoneItem.SyPhoneTypeObj);
                this.repository.UpdateAndFlush(lead);

                // MASK the phone
                if (filter.IsForeignPhone == false)
                {
                    if (filter.Phone.Length == 10)
                    {
                        filter.Phone = string.Format(
                            "({0}) {1}-{2}",
                            filter.Phone.Substring(0, 3),
                            filter.Phone.Substring(3, 3),
                            filter.Phone.Substring(6));
                    }
                }

                return filter;
            }

            throw new ApplicationException(
                "You can not create a new Phone here. Please go to Lead Page to create a new phone");
        }

        /// <summary>
        ///  The get info object.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <returns>
        /// The number of lead in query
        /// </returns>
        public int GetInfoObject(QueueInputmodel filter)
        {
            var numberOfLeadPending =
                this.repository.Query<Lead>()
                    .Where(
                        x =>
                        x.AdmissionRepUserObj.ID.ToString() == filter.AssingRepId
                        && x.Campus.ID.ToString() == filter.CampusId)
                    .Count(e => this.statusList.Contains(e.LeadStatus.SystemStatus.ID));

            return numberOfLeadPending;
        }

        /// <summary>
        /// The calculate age.
        /// </summary>
        /// <param name="dt">
        /// The date with respect to calculate the age.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string CalculateAge(DateTime dt)
        {
            string age;
            if (dt > DateTime.Now)
            {
                // This condition should not exists!! The Date of receiving must be always minor that now
                age = "00:00";
            }
            else
            {
                if (dt.Date == DateTime.Now.Date)
                {
                    // Today but before now......
                    age = (DateTime.Now.TimeOfDay - dt.TimeOfDay).ToString();
                    age = age.Remove(age.IndexOf('.'));
                    age = age.Substring(0, 5);
                }
                else
                {
                    // calculate the age for >24 hrs, it show in days else in hrs 
                    if ((DateTime.Now - dt).Days > 0)
                    {
                        age = (DateTime.Now - dt).Days.ToString().ToLowerInvariant();
                    }
                    else
                    {
                        age = (DateTime.Now - dt).ToString();
                        age = age.Substring(0, 5);
                    }
                }
            }

            return age;
        }
    }
}
