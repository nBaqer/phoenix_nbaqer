﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadSkillBo.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Business Object for Lead Skills
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Domain.Infrastructure.Entities;
    using Domain.Lead;
    using Domain.Users;
    using Messages.Common;
    using Messages.Lead.ExtraCurricular;

    /// <summary>
    /// Business Object for Lead Skills
    /// </summary>
    public class LeadSkillBo
    {
        /// <summary>
        ///  The repository.
        /// </summary>
        private readonly IRepositoryWithTypedID<Guid> repository;

        /// <summary>
        ///  The repository integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryint;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadSkillBo"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="leadRepository">
        /// the repository GUID
        /// </param>
        /// <param name="repositoryWithInt">
        /// the repository Integer
        /// </param>
        public LeadSkillBo(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = leadRepository;
            this.repositoryint = repositoryWithInt;
        }

        /// <summary>
        /// Get the Skills given the lead
        /// </summary>
        /// <param name="filter">
        /// The lead ID
        /// </param>
        /// <returns>
        /// The list of Extracurricular info
        /// </returns>
        public IList<ExtraInfo> GetSkillsValues(ExtraInfoInputModel filter)
        {
            var q = this.repositoryint.Query<AdSkills>()
                    .Where(x => x.LeadObj.ID.ToString() == filter.LeadId)

                            .Select(info => new ExtraInfo()
                                                {
                                                    Comment = info.Comment,
                                                    Description = info.Description,
                                                    Group =
                                                        new DropDownOutputModel
                                                            {
                                                                Description = info.SkillGroupObj.SkillGrpName,
                                                                ID = info.SkillGroupObj.ID.ToString()
                                                            },
                                                    Id = info.ID,

                                                    // LeadId = info.LeadObj.ID.ToString(),
                                                    Level =
                                                        new DropDownOutputModel
                                                            {
                                                                Description = info.LevelObj.Description,
                                                                ID = info.LevelObj.ID.ToString()
                                                            }
                                                });

            var output = q.OrderBy(x => x.Group.Description).ToList();
            return output;
        }

        /// <summary>
        /// Return drop down items for Skills Groups
        /// </summary>
        /// <param name="filter">
        /// The campus ID
        /// </param>
        /// <returns>
        /// List of drop-down items
        /// </returns>
        public IList<DropDownOutputModel> GetGroupItemsSkillsValues(ExtraInfoInputModel filter)
        {
            var q = this.repository.Query<SkillGroups>()
                    .Where(
                        x =>
                            x.CampusGroupObj.Campuses.Any(d => d.ID.ToString() == filter.CampusId) &&
                            x.StatusObj.StatusCode == "A")
                    .Select(info => new DropDownOutputModel()
                    {
                        Description = info.SkillGrpName,
                        ID = info.ID.ToString()
                    });
            var output = q.OrderBy(x => x.Description).ToList();
            return output;
        }

        /// <summary>
        /// Update a existing Skill value in the list of lead.
        /// </summary>
        /// <param name="filter">
        /// The Lead Id
        /// </param>
        public void UpdateSkills(IExtraInfoOutputModel filter)
        {
            foreach (ExtraInfo info in filter.ExtraInfoList)
            {
                var leadId = filter.Filter.LeadId;
                var q = this.repository.Query<Lead>().Single(x => x.ID.ToString() == leadId);
                var user = this.repository.Query<User>().Single(x => x.ID.ToString() == filter.Filter.UserId);
                var extra = q.SkillList.SingleOrDefault(x => x.ID == info.Id);
                if (extra != null)
                {
                    var group = this.repository.Query<SkillGroups>().SingleOrDefault(x => x.ID.ToString() == info.Group.ID);
                    var level = this.repositoryint.Query<AdLevels>()
                            .SingleOrDefault(x => x.ID.ToString() == info.Level.ID);
                    var nextra = new AdSkills(
                        info.Description,
                        info.Comment,
                        extra.LeadObj,
                        group,
                        level,
                        user.UserName,
                        DateTime.Now);

                    q.SkillList.Remove(extra);
                    q.SkillList.Add(nextra);
                    this.repository.Update(q);
                }
                else
                {
                    throw new ApplicationException("The Skill selected does not exists in the database");
                }
            }
        }

        /// <summary>
        /// Delete a Skills value from the lead list
        /// </summary>
        /// <param name="filter">
        /// The item ID to be delete
        /// </param>
        public void DeleteSkills(IExtraInfoOutputModel filter)
        {
            foreach (ExtraInfo info in filter.ExtraInfoList)
            {
                var leadId = filter.Filter.LeadId;
                var q = this.repository.Query<Lead>().Single(x => x.ID.ToString() == leadId && x.SkillList.Any(t => t.ID == info.Id));

                var extra = q.SkillList.SingleOrDefault(x => x.ID == info.Id);
                if (extra != null)
                {
                    q.SkillList.Remove(extra);
                    this.repository.Update(q);
                }
                else
                {
                    throw new ApplicationException("The Skill selected does not exists in the database");
                }
           }
        }

        /// <summary>
        /// Insert a new Skill
        /// </summary>
        /// <param name="filter">
        /// The item to be inserted
        /// </param>
        /// <returns>
        /// The <see cref="IExtraInfoOutputModel"/>.
        /// Return the deleted item
        /// </returns>
        public IExtraInfoOutputModel InsertSkills(IExtraInfoOutputModel filter)
        {
            var output = ExtraInfoOutputModel.Factory();
            foreach (ExtraInfo info in filter.ExtraInfoList)
            {
                var leadId = filter.Filter.LeadId;
                var q = this.repository.Query<Lead>().Single(x => x.ID.ToString() == leadId);
                var group = this.repository.Query<SkillGroups>().SingleOrDefault(x => x.ID.ToString() == info.Group.ID);
                var user = this.repository.Query<User>().Single(x => x.ID.ToString() == filter.Filter.UserId);
                var level = this.repositoryint.Query<AdLevels>().SingleOrDefault(x => x.ID.ToString() == info.Level.ID);
                var nextra = new AdSkills(
                    info.Description,
                    info.Comment,
                    q,
                    group,
                    level,
                    user.UserName,
                    DateTime.Now);
                q.SkillList.Add(nextra);
                this.repository.SaveAndFlush(q);
                var newskill = new ExtraInfo()
                {
                    Comment = nextra.Comment,
                    Description = nextra.Description,
                    Group = new DropDownOutputModel()
                    {
                        Description = nextra.SkillGroupObj.SkillGrpName,
                        ID = nextra.SkillGroupObj.ID.ToString()
                    },
                    Id = nextra.ID,
                    Level = new DropDownOutputModel()
                    {
                        Description = nextra.LevelObj.Description,
                        ID = nextra.LevelObj.ID.ToString()
                    }
                };
                output.ExtraInfoList.Add(newskill);
            }

            return output;
        }
    }
}
