﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadHistoryBo.cs" company="FAME Inc.">
//  FAME Inc. 2016
//  FAME.Advantage.Services.Lib.BusinessLogic.Lead.LeadHistoryBo
// </copyright>
// <summary>
//   Business object for Lead History 
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using FAME.Advantage.Domain.Catalogs;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Domain.Requirements.Documents;
    using FAME.Advantage.Domain.SystemStuff.Resources;
    using FAME.Advantage.Domain.Users;
    using FAME.Advantage.Messages.Common;
    using FAME.Advantage.Messages.Enumerations;
    using FAME.Advantage.Messages.Lead.History;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    /// Business object for Lead History
    /// </summary>
    class LeadHistoryBo
    {
        // ReSharper disable NotAccessedField.Local

        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepositoryWithTypedID<Guid> repository;

        // ReSharper restore NotAccessedField.Local

        /// <summary>
        ///  The repository integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryint;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadHistoryBo"/> class. 
        /// Constructor Public
        /// </summary>
        /// <param name="leadRepository">
        /// the GUID Repository
        /// </param>
        /// <param name="repositoryWithInt">
        /// The INT repository
        /// </param>
        public LeadHistoryBo(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = leadRepository;
            this.repositoryint = repositoryWithInt;
        }

        /// <summary>
        /// Return the list of notes associates to page notes
        /// </summary>
        /// <param name="filter">Filter LeadId, Module Code</param>
        /// <returns>
        /// Only the page notes.
        /// </returns>
        /// <remarks>
        /// futures version should return other notes
        /// </remarks>
        public IList<HistoryModel> GetLeadHistoryList(LeadHistoryInputFilter filter)//, bool authorizeConfidential)
        {
           IList<HistoryModel> history;
            history = this.repository.Query<AllHistory>()
                                                .Where(x => x.LeadId == Guid.Parse(filter.LeadId) && x.ModuleCode == filter.ModuleCode)
                                                .OrderByDescending(x => x.ModDate)
                                                .Select(info => new HistoryModel()
                                                {
                                                    HistoryId = info.ID,
                                                    ModuleCode = info.ModuleCode,
                                                    HistoryDate = info.ModDate,
                                                    HistoryModule = info.HistoryModule,
                                                    Type = info.HistoryType,
                                                    Description = info.Description,
                                                    ModUser = info.ModUserFullName,
                                                    AdditionalContent = info.AdditionalContent
                                                }).ToList();
            return history;
        }

        /// <summary>
        /// Get the list of modules description / code
        /// </summary>
        /// <param name="filter">Not Used</param>
        /// <returns>The Drop down list of Modules : Name and Code</returns>
        public IList<DropDownOutputModel> GetModulesItemList(LeadHistoryInputFilter filter)
        {
            var moduls =
                this.repositoryint.Query<SystemModule>()
                    .Select(info => new DropDownOutputModel { Description = info.Name, ID = info.Code });

            return moduls.OrderBy(x => x.Description).ToList(); 
        }

    }
}
