﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEnrollmentBo.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the LeadEnrollmentBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Diagnostics;
    using System.Linq;

    using Domain.Infrastructure.Entities;
    using Domain.Lead;
    using Domain.Student;
    using Domain.Student.Enrollments;
    using Domain.SystemStuff;
    using Domain.SystemStuff.ConfigurationAppSettings;

    using FluentNHibernate.Utils;

    using Messages.Lead;
    using Messages.Lead.Enroll;

    using NHibernate;

    /// <summary>
    /// The lead enrollment bo.
    /// </summary>
    public class LeadEnrollmentBo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadEnrollmentBo"/> class. 
        /// Get 
        /// </summary>
        /// <param name="leadRepository">
        /// Lead Repository
        /// </param>
        /// <param name="repositoryWithInt">
        /// Lead Repository With Integer
        /// </param>
        public LeadEnrollmentBo(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.Repository = leadRepository;
            this.RepositoryWithTypedId = repositoryWithInt;
        }

        /// <summary>
        /// Gets or sets the repository.
        /// </summary>
        public IRepository Repository { get; set; }

        /// <summary>
        /// Gets or sets the repository with typed id.
        /// </summary>
        public IRepositoryWithTypedID<int> RepositoryWithTypedId { get; set; }

        /// <summary>
        /// gets all the data to display
        /// </summary>
        /// <param name="filter">
        /// pass leadId,CampusId
        /// </param>
        /// <returns>
        /// returns all the data related to the page
        /// </returns>
        public EnrollOutputModel GetLeadData(LeadInputModel filter)
        {
            var output = new EnrollOutputModel();
            var leadsBo = new LeadBo(this.Repository, this.RepositoryWithTypedId);
            var requirementBo = new LeadRequirementsBo(this.Repository, this.RepositoryWithTypedId);

            // lead demographics
            output.LeadInfo = leadsBo.GetLeadInfoPageInformation(filter);

            ////// ipeds activated?
            ////var configurationAppSetting = this.RepositoryWithTypedId.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == "IPEDS");
            ////Debug.Assert(configurationAppSetting != null, "configurationAppSetting != null");
            ////var setting = configurationAppSetting.ConfigurationValues[0].Value;

            ////if (setting.ToLower() == "yes")
            ////{
            ////    output.IsIpeds = true;
            ////}
            ////else
            ////{
            ////    output.IsIpeds = false;
            ////}

            // international lead?
            var lead = this.Repository.Query<Lead>().SingleOrDefault(x => x.ID == new Guid(filter.LeadId));
            output.InternationalLead = false;
            if (lead != null)
            {
                if (lead.LeadGroupsList != null)
                {
                    foreach (var leadGrp in lead.LeadGroupsList)
                    {
                        var grpDesc = leadGrp.Description.ToLowerInvariant();
                        if (grpDesc.Contains("international") || grpDesc.Equals("intl".ToLowerInvariantString()) || grpDesc.Contains("int'l"))
                        {
                            output.InternationalLead = true;
                            break;
                        }
                    }
                }

                // other fields related to lead
                if (lead.FullName != null)
                {
                    output.FullName = lead.FullName;
                }

                if (lead.Nationality != null)
                {
                    output.Nationality = lead.Nationality.ID.ToString();
                }

                // output.InternationalState = false;
                if (lead.AddressList != null)
                {
                    var add = lead.AddressList.AsQueryable<LeadAddress>().FirstOrDefault(x => x.IsShowOnLeadPage);
                    if (add != null)
                    {
                        if (add.IsInternational != null)
                        {
                            if (add.IsInternational)
                            {
                                if (lead.EnrollState != null)
                                {
                                    output.EnrollStateId = lead.EnrollState.ID.ToString();
                                }
                            }
                            else
                            {
                                if (add.StateObj != null)
                                {
                                    if (lead.EnrollState != null)
                                    {
                                        output.EnrollStateId = lead.EnrollState.ID.ToString();
                                    }
                                    else
                                    {
                                        output.EnrollStateId = add.StateObj.ID.ToString();
                                    }
                                }
                                else
                                {
                                    if (lead.EnrollState != null)
                                    {
                                        output.EnrollStateId = lead.EnrollState.ID.ToString();
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (lead.EnrollState != null)
                        {
                            output.EnrollStateId = lead.EnrollState.ID.ToString();
                        }
                    }
                }

                if (lead.EntranceInterviewDate != null)
                {
                    output.EntranceInterviewDate = lead.EntranceInterviewDate;
                }

                if (lead.IsFirstTimePostSecSchool != null)
                {
                    output.IsFirstTimePostSecSchool = lead.IsFirstTimePostSecSchool;
                }

                if (lead.IsFirstTimeInSchool != null)
                {
                    output.IsFirstTimeInSchool = lead.IsFirstTimeInSchool;
                }

                if (lead.GeographicTypeId != null)
                {
                    output.GeographicTypeId = lead.GeographicTypeId.ID.ToString();
                }

                if (lead.DegCertSeekingId != null)
                {
                    output.DegCertSeekingId = lead.DegCertSeekingId.ID.ToString();
                }

                //// enrollment related fields
                var enroll =
                    this.Repository.Query<Enrollment>()
                        .OrderBy(o => o.ModDate)
                        .FirstOrDefault(x => x.LeadObj.ID == new Guid(filter.LeadId));
                if (enroll != null)
                {
                    if (enroll.ChargingMethod != null)
                    {
                        output.ChargingMethodId = enroll.ChargingMethod.ID.ToString();
                    }

                    if (enroll.ProgramVersionType != null)
                    {
                        output.ProgramVersionType = enroll.ProgramVersionType.ID.ToString();
                    }

                    output.EnrollmentDate = enroll.EnrollmentDate;
                    output.ExpectedStartDate = enroll.ExpectedStartDate;
                    if (enroll.StartDate != null)
                    {
                        output.StartDate = enroll.StartDate.Value;
                    }

                    output.MidPointDate = enroll.MidPointDate;
                    if (enroll.ContractedGradDate != null)
                    {
                        output.ContractedGradDate = enroll.ContractedGradDate.Value;
                    }

                    output.TransferDate = enroll.TransferDate;
                    output.EntranceInterviewDate = enroll.EntranceInterviewDate;
                    if (enroll.TuitionCategory != null)
                    {
                        output.TuitionCategory = enroll.TuitionCategory.ID.ToString();
                    }

                    if (enroll.Shift != null)
                    {
                        output.ShiftId = enroll.Shift.ID.ToString();
                    }

                    if (enroll.FinAidAdvisor != null)
                    {
                        output.FinAidAdvisor = enroll.FinAidAdvisor.ID.ToString();
                    }

                    if (enroll.AcademicAdvisor != null)
                    {
                        output.AcademicAdvisor = enroll.AcademicAdvisor.ID.ToString();
                    }

                    if (enroll.BadgeNumber != null)
                    {
                        output.BadgeNumber = enroll.BadgeNumber;
                    }

                    if (enroll.EnrollmentId != null)
                    {
                        output.EnrollId = enroll.EnrollmentId;
                    }

                    if (enroll.EducationLevel != null)
                    {
                        output.EducationLevel = enroll.EducationLevel.ID.ToString();
                    }

                    output.DisableAutoCharge = enroll.DisableAutoCharge;
                    output.ThirdPartyContract = enroll.ThirdPartyContract;
                    output.TransferHours = enroll.TransferHours;
                }

                //valid lead
                var phones = lead.LeadPhoneList.Select(x => new LeadPhonesOutputModel()
                {
                    Phone = x.Phone,
                    PhoneType = x.SyPhoneTypeObj != null ? x.SyPhoneTypeObj.PhoneTypeDescrip : string.Empty,
                    IsForeignPhone = x.IsForeignPhone,
                    PhoneTypeId = x.SyPhoneTypeObj != null ? x.SyPhoneTypeObj.ID.ToString() : string.Empty
                }).ToList();
                var emails = lead.LeadEmailList.Select(x => new LeadEmailOutputModel()
                {
                    Email = x.Email,
                    EmailType = x.EmailTypeObj != null ? x.EmailTypeObj.EmailTypeDescrip : string.Empty,
                    EmailTypeId = x.EmailTypeObj != null ? x.EmailTypeObj.ID.ToString() : string.Empty
                }).ToList();
                var address = lead.AddressList.Where(ad => ad.IsShowOnLeadPage)
                    .Select(
                        x => new LeadAddressOutputModel()
                        {
                            Address1 = x.Address1,
                            Address2 = x.Address2,
                            AddressTypeId = x.AddressType != null ? x.AddressType.ID.ToString() : string.Empty,
                            AddressType = x.AddressType != null ? x.AddressType.AddessDescrip : string.Empty,
                            AddressApto = x.AddressApto,
                            ZipCode = x.ZipCode,
                            State = x.StateObj != null ? x.StateObj.Description : string.Empty,
                            Country = x.CountryObj != null ? x.CountryObj.CountryDescrip : string.Empty,
                            County = x.CountyObj != null ? x.CountyObj.CountyDescrip : string.Empty,
                            IsMailingAddress = x.IsMailingAddress.ToString(),
                            IsShowOnLeadPage = x.IsShowOnLeadPage.ToString(),
                            IsInternational = x.IsInternational
                        }).FirstOrDefault();

                var leadOutput = new LeadInfoPageOutputModel()
                {
                    ModUser = lead.ModUser,
                    FirstName = lead.FirstName,
                    MiddleName = lead.MiddleName,
                    LastName = lead.LastName,
                    Age = lead.Age,
                    AlienNumber = lead.AlienNumber,
                    GroupIdList = lead.LeadGroupsList.Select(g => g.ID.ToString()).ToList(),
                    LeadAddress = address,
                    PhonesList = phones,
                    EmailList = emails,
                    SSN = lead.SSN,
                    ExpectedStart = lead.ExpectedStart,
                    Dependants = lead.Dependants,
                    LeadStatusId = lead.LeadStatus != null ? lead.LeadStatus.ID.ToString() : string.Empty,
                    InputModel = new LeadInputModel() { LeadId = lead.ID.ToString() }
                };

                output.IsValidLead = leadsBo.ValidateNewLead(leadOutput, out var validation);
                output.IsDuplicateSsn = leadsBo.ValidateSsn(leadOutput);
                output.ValidationMessages = validation;

                // requirements met
                string reqMet = requirementBo.HaveMetRequirements(lead.ID, lead.Campus.ID);
                if (reqMet.Equals("True") || reqMet.Equals("enrolled"))
                {
                    output.RequirementMet = true;
                }
                else
                {
                    output.RequirementMet = false;
                }

                // is school defined future start status mapped to system defined future start status. 7= "Future Start" of systemStatus. 2= "Student Enrollments" of status level
                var map = this.Repository.Query<UserDefStatusCode>().Where(s => s.Status.StatusCode == "A" && s.SystemStatusId == 7 && s.SystemStatus.StatusLevelId == 2 && (s.CampusGroup.CampusId == lead.Campus.ID || s.CampusGroup.IsAllCampusGrp == true)).OrderByDescending(x => x.IsDefaultLeadStatus).ThenBy(x => x.Description).FirstOrDefault();
                if (map != null)
                {
                    output.FutureStartStatusMapped = map.Description; // means it is mapped properly and continue for enroll
                }
                else
                {
                    // means not mapped, do not enroll. find if any other school defined status is mapped? if yes return that unmapped thing.
                    var notmappedStatus = this.Repository.Query<UserDefStatusCode>().Where(s => s.Status.StatusCode == "A" && s.SystemStatus.StatusLevelId == 2 && s.Description.Contains("Future".ToLowerInvariant()) && (s.CampusGroup.CampusId == lead.Campus.ID || s.CampusGroup.IsAllCampusGrp == true)).OrderByDescending(x => x.IsDefaultLeadStatus).ThenBy(x => x.Description).FirstOrDefault();
                    if (notmappedStatus != null)
                    {
                        output.NotMappedStatus = notmappedStatus.Description;
                    }
                }

                if (output.EnrollId != null)
                {
                    output.DisableEnrolLeadStatus = true;
                }
                else
                {
                    output.DisableEnrolLeadStatus = false;
                    string enrollId = this.GenerateEnrollNumber(lead.FirstName, lead.LastName);
                    while (this.Repository.Query<Enrollment>().Any(x => x.EnrollmentId == enrollId))
                    {
                        enrollId = this.GenerateEnrollNumber(lead.FirstName, lead.LastName);
                    }

                    output.EnrollId = enrollId;
                }
            }

            return output;
        }

        /// <summary>
        /// the Server side validations
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string ServerSideValidation(EnrollOutputModel input)
        {
            string output = "good";
            var studentInfo = this.RepositoryWithTypedId.Query<SyStudentFormat>().FirstOrDefault();
            if (studentInfo != null)
            {
                var formatType = studentInfo.FormatType;
                if (formatType.Equals("4", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (string.IsNullOrEmpty(input.BadgeNumber))
                    {

                        output = "The badge number is required when the Student ID generation is based on Enrollment Badge ID.";
                        return output;

                    }
                }

            }

            // Check if the badge number is duplicated with another lead or another student
            if (!string.IsNullOrEmpty(input.BadgeNumber))
            {
                var isdup = this.Repository.Query<Enrollment>().Any(x => x.BadgeNumber == input.BadgeNumber && x.ID.ToString() != input.LeadInfo.LeadId);
                if (isdup)
                {
                    output = "The entered badge number is currently used by another student.  Please choose another badge number.";
                    return output;
                }
            }

            if (input.InternationalLead != null)
            {
                if (!input.InternationalLead.Value)
                {
                    if (string.IsNullOrEmpty(input.LeadInfo.SSN))
                    {
                        output = "SSN is required";
                        return output;
                    }
                }
            }

            // Check if the SSN is duplicated with another lead or another student
            if (!string.IsNullOrEmpty(input.LeadInfo.SSN))
            {
                var isdupSsn = this.Repository.Query<Lead>().Any(x => x.SSN == input.LeadInfo.SSN && x.ID.ToString() != input.LeadInfo.LeadId);
                if (isdupSsn)
                {
                    output = "SSN Duplicate with other lead";
                    return output;
                }

                isdupSsn = this.Repository.Query<Student>().Any(x => x.SSN == input.LeadInfo.SSN);
                if (isdupSsn)
                {
                    output = "SSN Duplicate with a Existent Student";
                    return output;
                }
            }

            if (input.LeadInfo.LeadAddress.IsInternational != null)
            {
                if (!input.LeadInfo.LeadAddress.IsInternational)
                {
                    if (string.IsNullOrEmpty(input.EnrollStateId))
                    {
                        output = "State is required";
                        return output;
                    }
                }
            }

            var configurationAppSetting =
               this.RepositoryWithTypedId.Query<ConfigurationAppSetting>()
                   .FirstOrDefault(n => n.KeyName.ToUpper() == "StudentAgeLimit");
            Debug.Assert(configurationAppSetting != null, "configurationAppSetting != null");
            var setting = configurationAppSetting.ConfigurationValues[0].Value;

            if (string.IsNullOrWhiteSpace(input.LeadInfo.Age) == false && int.Parse(setting) > int.Parse(input.LeadInfo.Age))
            {
                output = string.Format("Lead must be at least {0} years old.", setting);
            }

            if (string.IsNullOrEmpty(input.LeadInfo.ScheduleId))
            {
                // check if the programversion is time clock  if yes then return putput.
                var prgVer = this.Repository.Query<ProgramVersion>().SingleOrDefault(x => x.ID == new Guid(input.LeadInfo.PrgVerId));
                var isUsingTimeclock = prgVer != null && prgVer.UseTimeClock;
                if (isUsingTimeclock)
                {
                    output = "Time clock programs must have a schedule.";
                    return output;
                }
            }

            if (input.ContractedGradDate <= DateTime.Today)
            {
                output = "The Contracted Grad date cannot be before or equal to today's date.";
                return output;
            }

            if (input.StartDate >= input.ContractedGradDate)
            {
                output = "The Contracted Grad date cannot be before or equal to start date.";
                return output;
            }

            return output;
        }

        /// <summary>
        /// The enroll lead.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string EnrollLead(EnrollOutputModel input)
        {
            string validateResult = this.ServerSideValidation(input);
            if (validateResult.Equals("good"))
            {
                string query =
                    "exec USP_EnrollLead :leadId,:campus, :gender,:familyIncome,:housingType,:educationLevel,:adminCriteria,:degSeekCert"
                    + ",:attendType,:race,:citizenship,:prgVersion,:programId,:areaId,:prgVerType,:startDt,:expectedStartDt,:contractGrdDt,:tuitionCategory"
                    + ",:enrollDt,:transferhr,:dob,:ssn,:depencytype,:maritalstatus,:nationality,:geographicType,:state,:disabled,:disableAutoCharge,:thirdPartyContract"
                    + ",:firsttimeSchool,:firsttimePostSec,:schedule,:shift,:midptDate,:transferDt,:entranceInterviewDt,:chargingMethod"
                    + ",:fAAvisor,:acedemicAdvisor,:badgeNum,:studentNumber,:modUser,:enrollId";
                var result = this.Repository.Session.CreateSQLQuery(query);

                result.SetGuid("leadId", Guid.Parse(input.LeadInfo.LeadId))
                    .SetGuid("campus", Guid.Parse(input.LeadInfo.CampusId));
                if (!string.IsNullOrEmpty(input.LeadInfo.Gender))
                {
                    result.SetGuid("gender", Guid.Parse(input.LeadInfo.Gender));
                }
                else
                {
                    result.SetParameter("gender", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.LeadInfo.FamilyIncoming))
                {
                    result.SetGuid("familyIncome", Guid.Parse(input.LeadInfo.FamilyIncoming));
                }
                else
                {
                    result.SetParameter("familyIncome", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.LeadInfo.HousingType))
                {
                    result.SetGuid("housingType", Guid.Parse(input.LeadInfo.HousingType));
                }
                else
                {
                    result.SetParameter("housingType", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.EducationLevel))
                {
                    result.SetGuid("educationLevel", Guid.Parse(input.EducationLevel));
                }
                else
                {
                    result.SetParameter("educationLevel", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.LeadInfo.AdminCriteriaId))
                {
                    result.SetGuid("adminCriteria", Guid.Parse(input.LeadInfo.AdminCriteriaId));
                }
                else
                {
                    result.SetParameter("adminCriteria", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.DegCertSeekingId))
                {
                    result.SetGuid("degSeekCert", Guid.Parse(input.DegCertSeekingId));
                }
                else
                {
                    result.SetParameter("degSeekCert", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.LeadInfo.AttendTypeId))
                {
                    result.SetGuid("attendType", Guid.Parse(input.LeadInfo.AttendTypeId));
                }
                else
                {
                    result.SetParameter("attendType", null, NHibernateUtil.Guid);
                }
                if (!string.IsNullOrEmpty(input.LeadInfo.RaceId))
                {
                    result.SetGuid("race", Guid.Parse(input.LeadInfo.RaceId));
                }
                else
                {
                    result.SetParameter("race", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.LeadInfo.Citizenship))
                {
                    result.SetGuid("citizenship", Guid.Parse(input.LeadInfo.Citizenship));
                }
                else
                {
                    result.SetParameter("citizenship", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.LeadInfo.PrgVerId))
                {
                    result.SetGuid("prgVersion", Guid.Parse(input.LeadInfo.PrgVerId));
                }
                else
                {
                    result.SetParameter("prgVersion", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.LeadInfo.ProgramId))
                {
                    result.SetGuid("programId", Guid.Parse(input.LeadInfo.ProgramId));
                }
                else
                {
                    result.SetParameter("programId", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.LeadInfo.AreaId))
                {
                    result.SetGuid("areaId", Guid.Parse(input.LeadInfo.AreaId));
                }
                else
                {
                    result.SetParameter("areaId", null, NHibernateUtil.Guid);
                }

                result.SetInt32("prgVerType", int.Parse(input.ProgramVersionType))
                        .SetDateTime("startDt", input.StartDate);
                if (input.ExpectedStartDate != null)
                {
                    result.SetDateTime("expectedStartDt", input.ExpectedStartDate.Value);
                }
                else
                {
                    result.SetParameter("expectedStartDt", null, NHibernateUtil.DateTime);
                }

                result.SetDateTime("contractGrdDt", input.ContractedGradDate)
                    .SetGuid("tuitionCategory", Guid.Parse(input.TuitionCategory))
                    .SetDateTime("enrollDt", (input.EnrollmentDate).Date)
                    .SetDecimal("transferhr", input.TransferHours);
                if (input.LeadInfo.Dob != null)
                {
                    result.SetDateTime("dob", input.LeadInfo.Dob.Value);
                }
                else
                {
                    result.SetParameter("dob", null, NHibernateUtil.DateTime);
                }

                if (!string.IsNullOrEmpty(input.LeadInfo.SSN))
                {
                    result.SetString("ssn", input.LeadInfo.SSN);
                }
                else
                {
                    result.SetParameter("ssn", null, NHibernateUtil.String);
                }

                if (!string.IsNullOrEmpty(input.LeadInfo.Dependency))
                {
                    result.SetGuid("depencytype", Guid.Parse(input.LeadInfo.Dependency));
                }
                else
                {
                    result.SetParameter("depencytype", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.LeadInfo.MaritalStatus))
                {
                    result.SetGuid("maritalstatus", Guid.Parse(input.LeadInfo.MaritalStatus));
                }
                else
                {
                    result.SetParameter("maritalstatus", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.Nationality))
                {
                    result.SetGuid("nationality", Guid.Parse(input.Nationality));
                }
                else
                {
                    result.SetParameter("nationality", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.GeographicTypeId))
                {
                    result.SetGuid("geographicType", Guid.Parse(input.GeographicTypeId));
                }
                else
                {
                    result.SetParameter("geographicType", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.EnrollStateId))
                {
                    result.SetGuid("state", Guid.Parse(input.EnrollStateId));
                }
                else
                {
                    result.SetParameter("state", null, NHibernateUtil.Guid);
                }

                if (input.IsDisabled != null)
                {
                    result.SetBoolean("disabled", input.IsDisabled.Value);
                }
                else
                {
                    result.SetParameter("disabled", null, NHibernateUtil.Boolean);
                }

                result.SetBoolean("disableAutoCharge", input.DisableAutoCharge);
                result.SetBoolean("thirdPartyContract", input.ThirdPartyContract);
                if (input.IsFirstTimeInSchool != null)
                {
                    result.SetBoolean("firsttimeSchool", input.IsFirstTimeInSchool.Value);
                }
                else
                {
                    result.SetParameter("firsttimeSchool", null, NHibernateUtil.Boolean);
                }

                if (input.IsFirstTimePostSecSchool != null)
                {
                    result.SetBoolean("firsttimePostSec", input.IsFirstTimePostSecSchool.Value);
                }
                else
                {
                    result.SetParameter("firsttimePostSec", null, NHibernateUtil.Boolean);
                }

                if (!string.IsNullOrEmpty(input.LeadInfo.ScheduleId))
                {
                    result.SetGuid("schedule", Guid.Parse(input.LeadInfo.ScheduleId));
                }
                else
                {
                    result.SetParameter("schedule", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.ShiftId))
                {
                    result.SetGuid("shift", Guid.Parse(input.ShiftId));
                }
                else
                {
                    result.SetParameter("shift", null, NHibernateUtil.Guid);
                }

                if (input.MidPointDate != null)
                {
                    result.SetDateTime("midptDate", input.MidPointDate.Value);
                }
                else
                {
                    result.SetParameter("midptDate", null, NHibernateUtil.DateTime);
                }

                if (input.TransferDate != null)
                {
                    result.SetDateTime("transferDt", input.TransferDate.Value);
                }
                else
                {
                    result.SetParameter("transferDt", null, NHibernateUtil.DateTime);
                }

                if (input.EntranceInterviewDate != null)
                {
                    result.SetDateTime("entranceInterviewDt", input.EntranceInterviewDate.Value);
                }
                else
                {
                    result.SetParameter("entranceInterviewDt", null, NHibernateUtil.DateTime);
                }

                if (!string.IsNullOrEmpty(input.ChargingMethodId))
                {
                    result.SetGuid("chargingMethod", Guid.Parse(input.ChargingMethodId));
                }
                else
                {
                    result.SetParameter("chargingMethod", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.FinAidAdvisor))
                {
                    result.SetGuid("fAAvisor", Guid.Parse(input.FinAidAdvisor));
                }
                else
                {
                    result.SetParameter("fAAvisor", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.AcademicAdvisor))
                {
                    result.SetGuid("acedemicAdvisor", Guid.Parse(input.AcademicAdvisor));
                }
                else
                {
                    result.SetParameter("acedemicAdvisor", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.BadgeNumber))
                {
                    result.SetString("badgeNum", input.BadgeNumber);
                }
                else
                {
                    result.SetParameter("badgeNum", null, NHibernateUtil.String);
                }

                if (input.StudentNumber != null)
                {
                    result.SetString("studentNumber", input.StudentNumber);
                }
                else
                {
                    result.SetString("studentNumber", string.Empty);
                }

                if (!string.IsNullOrEmpty(input.LeadInfo.ModUser))
                {
                    result.SetGuid("modUser", Guid.Parse(input.LeadInfo.ModUser));
                }
                else
                {
                    result.SetParameter("modUser", null, NHibernateUtil.Guid);
                }

                if (!string.IsNullOrEmpty(input.EnrollId))
                {
                    result.SetString("enrollId", input.EnrollId);
                }
                else
                {
                    result.SetParameter("enrollId", null, NHibernateUtil.String);
                }
                string msg = result.UniqueResult().ToString();
                if (msg.Equals("Successful COMMIT TRANSACTION"))
                {
                    return "enrolled";
                }
                else
                {
                    throw new ApplicationException(msg);
                }
            }
            else
            {
                throw new ApplicationException(validateResult);
            }
        }

        /// <summary>
        /// The generate student id
        /// </summary>
        /// <param name="startSeqNumber">
        /// The start sequence number.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GenerateStudentIdSeq(int startSeqNumber)
        {
            var result = this.Repository.Session.CreateSQLQuery("exec USP_GenerateStudentSeqId :startSeqNumber")
                .SetInt32("startSeqNumber", startSeqNumber)
                .UniqueResult();
            int x;
            return int.TryParse(result.ToString(), out x) ? x : x;
        }

        /// <summary>
        /// The generate student id.
        /// </summary>
        /// <param name="stuFirstName">
        /// The stu First Name.
        /// </param>
        /// <param name="stuLastName">
        /// The stu Last Name.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public string GenerateStudentId(string stuFirstName, string stuLastName)
        {
            var studentInfo = this.Repository.Query<Domain.Student.Enrollments.StudentFormat>().SingleOrDefault();
            if (studentInfo != null)
            {
                var formatType = studentInfo.FormatType;
                switch (formatType)
                {
                    case "1": break;
                    case "3":
                        return this.GenerateFullStudentNumber(
                            stuLastName,
                            stuFirstName,
                            studentInfo.YearNumber,
                            studentInfo.MonthNumber,
                            studentInfo.DateNumber,
                            studentInfo.LNameNumber,
                            studentInfo.FNameNumber);
                    default:
                        return this.GenerateStudentIdSeq(studentInfo.SeqStartingNumber).ToString();
                }
            }
            else
            {
                return string.Empty;
            }
            return string.Empty;
        }

        /// <summary>
        /// The generate full student number.
        /// </summary>
        /// <param name="stuLastName">
        /// The student last name.
        /// </param>
        /// <param name="stuFirstName">
        /// The student first name.
        /// </param>
        /// <param name="yearNumber">
        /// The year Number.
        /// </param>
        /// <param name="monthNumber">
        /// The month Number.
        /// </param>
        /// <param name="dateNumber">
        /// The date Number.
        /// </param>
        /// <param name="lNameNumber">
        /// The l Name Number.
        /// </param>
        /// <param name="fNameNumber">
        /// The f Name Number.
        /// </param>
        /// <returns>
        /// The student guid
        /// </returns>
        public string GenerateFullStudentNumber(string stuLastName, string stuFirstName, int yearNumber, int monthNumber, int dateNumber, int lNameNumber, int fNameNumber)
        {
            // Get The LastName Based On The Number Of Characters. Example : LastName is Kennedy and if School wants 2 characters then lastName = KE
            var lastName = stuLastName.Substring(0, lNameNumber).ToUpper();
            var firstName = stuFirstName.Substring(0, fNameNumber).ToUpper();
            string yr;
            switch (yearNumber)
            {
                case 4:
                    yr = DateTime.Now.Year.ToString().Substring(0, 4);
                    break;
                case 3:
                    yr = DateTime.Now.Year.ToString().Substring(1, 3);
                    break;
                case 2:
                    yr = DateTime.Now.Year.ToString().Substring(2, 2);
                    break;
                case 1:
                    yr = DateTime.Now.Year.ToString().Substring(3, 1);
                    break;
                default:
                    yr = string.Empty;
                    break;
            }

            var mon = DateTime.Now.Month.ToString().Substring(0, monthNumber);
            var dt = DateTime.Now.Day.ToString().Substring(0, dateNumber);

            if (mon.Length == 1 && monthNumber == 2)
            {
                mon = "0" + mon;
            }

            if (dt.Length == 1 && dateNumber == 2)
            {
                dt = "0" + dt;
            }

            var stuId = this.Repository.Session.CreateSQLQuery("exec USP_GenerateStudentFormatId").UniqueResult();
            int lseqNo = int.Parse(stuId.ToString());
            if (yr == string.Empty)
            {
                return mon + dt + lastName + firstName + lseqNo;
            }
            else
            {
                return yr + mon + dt + lastName + firstName + lseqNo;
            }
        }

        /// <summary>
        /// The generate enroll number.
        /// </summary>
        /// <param name="stuFirstName">
        /// The stu first name.
        /// </param>
        /// <param name="stuLastName">
        /// The stu last name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GenerateEnrollNumber(string stuFirstName, string stuLastName)
        {
            string lastName = (stuLastName.Length < 2) ? (stuLastName + stuLastName) : stuLastName;
            lastName = lastName.Substring(0, 2).ToUpper();
            string firstName = stuFirstName.Substring(0, 1).ToUpper();
            string yr = DateTime.Now.Year.ToString().Substring(2, 2);
            string mon = DateTime.Now.Month.ToString().Length > 1
                             ? DateTime.Now.Month.ToString().Substring(1, 1)
                             : DateTime.Now.Month.ToString().Substring(0, 1);

            string day = DateTime.Now.Day.ToString();
            day = day.Length < 2 ? "0" + day : day.Substring(0, 2);
            int l_seq = 2;
            string i = this.Repository.Session.CreateSQLQuery("exec USP_GenerateStudentSequence").UniqueResult().ToString();
            int g = i.Length;
            int seqDiff = l_seq - g;
            string k = string.Empty;
            if (seqDiff > 0)
            {
                for (int l = 1; l < l_seq; l++)
                {
                    k = k + "0";
                }

                i = k + i;
            }

            var f = g - 2 <= 0 ? i : i.Substring(g - 2, l_seq);

            if (yr == string.Empty)
            {
                return mon + day + lastName + firstName + f;
            }
            else
            {
                return yr + mon + day + lastName + firstName + f;
            }
        }
    }
}
