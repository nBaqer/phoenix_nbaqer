﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadPriorWorkBo.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the LeadPriorWorkBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;
    using Domain.Infrastructure.Entities;
    using Domain.Lead;

    using FAME.Advantage.Domain.Catalogs;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Domain.SystemStuff.SchoolDefinedFields;
    using FAME.Advantage.Domain.Users;
    using FAME.Advantage.Messages.Lead;

    using Messages.Common;
    using Messages.Lead.PriorWork;

    /// <summary>
    /// The lead prior work bo.
    /// </summary>
    public class LeadPriorWorkBo
    {
        /// <summary>
        /// The lead repository.
        /// </summary>
        private readonly IRepository leadRepository;

        /// <summary>
        /// The repository with integer.
        /// </summary>
        // ReSharper disable NotAccessedField.Local
        private IRepositoryWithTypedID<int> repositoryWithInt;

        // ReSharper restore NotAccessedField.Local

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadPriorWorkBo"/> class.
        /// </summary>
        /// <param name="leadRepository">
        ///  The lead repository.
        /// </param>
        /// <param name="repositoryWithInt">
        ///  The repository with integer.
        /// </param>
        public LeadPriorWorkBo(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.leadRepository = leadRepository;
            this.repositoryWithInt = repositoryWithInt;
        }

        /// <summary>
        /// The get the prior work list for a particular lead. Order by descending EndDate
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;PriorWorkItemOutputModel&gt;"/>.
        /// </returns>
        public IList<PriorWorkItemOutputModel> GetThePriorWorkItemList(PriorWorkInputModel filter)
        {
            // Get the list of prior works from table
            var priorWork =
                this.leadRepository.Query<AdLeadEmployment>().Where(x => x.LeadObj.ID.ToString() == filter.LeadId);
            var output =
                Mapper.Map<IList<PriorWorkItemOutputModel>>(priorWork).OrderByDescending(t => t.EndDate).ToList();
            foreach (PriorWorkItemOutputModel model in output)
            {
                if (model.AddressOutputModel == null)
                {
                    model.AddressOutputModel = new PriorWorkAddressOutputModel();
                }
            }

            return output;
        }

        /// <summary>
        /// The insert the new prior work.
        /// </summary>
        /// <param name="filter">
        /// The filter with the information to insert and the filter parameters.
        /// </param>
        public void InsertTheNewPriorWork(IPriorWorkOutputModel filter)
        {
            if (filter.PriorWorkItemList != null && filter.PriorWorkItemList.Count > 0)
            {
                // Get the user 
                var user = this.leadRepository.Get<User>(new Guid(filter.InputModel.UserId));

                // Get the associate lead
                var leadobj = this.leadRepository.Get<Lead>(new Guid(filter.InputModel.LeadId));

                foreach (PriorWorkItemOutputModel model in filter.PriorWorkItemList)
                {
                    // Check by required fields and Date relation
                    this.CommonValidationsPriorWork(model);
                   
                    AdTitles schooltitleobj = (model.SchoolJobTitle != null && model.SchoolJobTitle.ID != null)
                                                  ? this.leadRepository.Get<AdTitles>(new Guid(model.SchoolJobTitle.ID))
                                                  : null;
                    PlJobStatus jobstatus = (model.JobStatus != null && model.JobStatus.ID != null)
                                                ? this.leadRepository.Get<PlJobStatus>(new Guid(model.JobStatus.ID))
                                                : null;

                    // Create a new instance of domain PriorWork
                    var domain = new AdLeadEmployment(
                                     leadobj,
                                     schooltitleobj,
                                     jobstatus,
                                     model.StartDate,
                                     model.EndDate,
                                     string.Empty,
                                     user.UserName,
                                     DateTime.Now,
                                     string.Empty,
                                     model.Employer,
                                     model.JobTitle);

                    // Analysis to update the address if exists or was added or deleted.
                    if (model.AddressOutputModel != null
                        && string.IsNullOrWhiteSpace(model.AddressOutputModel.Address1) == false)
                    {
                        // Add the address object
                        var address = model.AddressOutputModel;

                        // Get the country and USA states
                        var country = this.GetCountry(this.leadRepository, address);
                        var state = this.GetUsaStates(this.leadRepository, address);

                        PriorWorkAddress paddress = new PriorWorkAddress(
                                                        domain,
                                                        address.Address1,
                                                        address.AddressApto ?? string.Empty,
                                                        address.Address2 ?? string.Empty,
                                                        address.City ?? string.Empty,
                                                        address.IsInternational ? null : state,
                                                        address.IsInternational,
                                                        address.IsInternational ? address.StateInternational ?? string.Empty : string.Empty,
                                                        address.ZipCode ?? string.Empty,
                                                        country,
                                                        user.UserName,
                                                        DateTime.Now);
                        domain.PriorWorkAddressesList.Add(paddress);
                    }

                    // Add to lead the new prior work
                    leadobj.LeadEmploymentsList.Add(domain);
                }

                // Save ....
                this.leadRepository.SaveAndFlush(leadobj);
            }
            else
            {
                throw new ApplicationException("Nothing to Insert");
            }
        }

        /// <summary>
        ///  The insert the new contact info.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <exception cref="Exception">
        /// if they are nothing to insert, or the prior work is null
        /// </exception>
        public void InsertTheNewContactInfo(IPriorWorkOutputModel filter)
        {
            var contactList = filter.PriorWorkDetailObject.ContactList;
            if (contactList == null || contactList.Count == 0)
            {
                throw new Exception("Nothing to Insert");
            }

            var userID = this.leadRepository.Load<User>(new Guid(filter.InputModel.UserId));
            var contact = contactList.First();
            if (string.IsNullOrEmpty(contact.FirstName) || string.IsNullOrEmpty(contact.LastName) || string.IsNullOrEmpty(contact.Title))
            {
                throw new ApplicationException("Fields Title, First Name and Last Name are mandatory");
            }

            var prefix = contact.Prefix == null ? null : this.leadRepository.Query<Prefixes>().SingleOrDefault(x => x.ID == new Guid(contact.Prefix.ID));
            var priorWorkId = filter.InputModel.PriorWorkId;
            var pw = this.leadRepository.Query<AdLeadEmployment>().SingleOrDefault(x => x.ID == new Guid(priorWorkId));
            if (pw == null)
            {
                throw new Exception("The prior work does not exists");
            }

            // Var create the new contact
            var contactObject = new PriorWorkContact(
                pw,
                contact.Title,
                contact.FirstName,
                contact.MiddleName,
                contact.LastName,
                prefix,
                contact.IsPhoneInternational,
                contact.Phone,
                contact.Email,
                contact.IsActive,
                userID.UserName,
                DateTime.Now,
                0);

            // Insert the contact in Prior Work
            pw.PriorWorkContactsList.Add(contactObject);
            this.leadRepository.SaveAndFlush(pw);
        }

        /// <summary>
        /// Command 3: Update The prior Work
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="PriorWorkItemOutputModel"/>.
        /// The first modified (that should be the unique)
        /// </returns>
        internal PriorWorkItemOutputModel UpdatePriorWork(IPriorWorkOutputModel filter)
        {
            if (filter.PriorWorkItemList != null && filter.PriorWorkItemList.Count > 0)
            {
                // Get the user 
                var user = this.leadRepository.Get<User>(new Guid(filter.InputModel.UserId));
                var listOfChanges = new List<AdLeadEmployment>();

                // If exists Items to update enter here..........................................
                foreach (PriorWorkItemOutputModel model in filter.PriorWorkItemList)
                {
                    // Check by required fields and Date relation
                    this.CommonValidationsPriorWork(model);

                    // Check if the Id is coming...
                    if (string.IsNullOrWhiteSpace(model.Id))
                    {
                        throw new Exception("The Prior Work ID is not valid");
                    }

                    // Get the Prior Work
                    var prior = this.leadRepository.Get<AdLeadEmployment>(new Guid(model.Id));
                    if (prior == null)
                    {
                        throw new Exception("The Prior Work was not found");
                    }

                    AdTitles schooltitleobj = (model.SchoolJobTitle != null && model.SchoolJobTitle.ID != null)
                                                  ? this.leadRepository.Get<AdTitles>(new Guid(model.SchoolJobTitle.ID))
                                                  : null;
                    PlJobStatus jobstatus = (model.JobStatus != null && model.JobStatus.ID != null)
                                                ? this.leadRepository.Get<PlJobStatus>(new Guid(model.JobStatus.ID))
                                                : null;

                    var address = model.AddressOutputModel;

                    // Check if the SERVER Prior work has an ADDRESS
                    if (prior.PriorWorkAddressesList != null && prior.PriorWorkAddressesList.Count > 0)
                    {
                        // case there are a value
                        if (address == null || string.IsNullOrWhiteSpace(address.Address1))
                        {
                            // Existed but no more. Delete
                            while (prior.PriorWorkAddressesList.Count > 0)
                            {
                                var item = prior.PriorWorkAddressesList.First();
                                prior.PriorWorkAddressesList.Remove(item);
                                this.repositoryWithInt.Delete(item);
                            }
                        }
                        else
                        {
                            // Existed and continue. update.......................................................
                            // Get the country and USA states
                            var country = this.GetCountry(this.leadRepository, address);
                            var state = this.GetUsaStates(this.leadRepository, address);

                            var priorWorkAddress = prior.PriorWorkAddressesList.First();
                            prior.PriorWorkAddressesList[0].UpdatePriorWorkAddress(
                                priorWorkAddress.StEmploymentObj,
                                address.Address1,
                                address.AddressApto ?? string.Empty,
                                address.Address2 ?? string.Empty,
                                address.City ?? string.Empty,
                                address.IsInternational ? null : state,
                                address.IsInternational,
                                address.IsInternational ? address.StateInternational ?? string.Empty : string.Empty,
                                address.ZipCode ?? string.Empty,
                                country,
                                user.UserName,
                                DateTime.Now);
                            prior.PriorWorkAddressesList.Add(priorWorkAddress);
                        }
                    }
                    else
                    {
                        // SERVER Prior Work has not a Address. Check now if the client prior work has ONE
                        if (address != null && string.IsNullOrWhiteSpace(address.Address1) == false)
                        {
                            // A new address was created insert it
                            var country = this.GetCountry(this.leadRepository, address);
                            var state = this.GetUsaStates(this.leadRepository, address);

                            // Create the address
                            var priorWorkAddress = new PriorWorkAddress(
                                                       prior,
                                                       address.Address1,
                                                       address.AddressApto ?? string.Empty,
                                                       address.Address2,
                                                       address.City ?? string.Empty,
                                                       address.IsInternational ? null : state,
                                                       address.IsInternational,
                                                       address.IsInternational ? address.StateInternational : string.Empty,
                                                       address.ZipCode ?? string.Empty,
                                                       country,
                                                       user.UserName,
                                                       DateTime.Now);

                            prior.PriorWorkAddressesList.Add(priorWorkAddress);
                        }
                    }

                    // Update the SERVER Prior Work
                    prior.UpdateAdLeadEmployment(
                        prior.LeadObj,
                        schooltitleobj,
                        jobstatus,
                        model.StartDate,
                        model.EndDate,
                        prior.Comments,
                        user.UserName,
                        DateTime.Now,
                        prior.JobResponsibilities,
                        model.Employer,
                        model.JobTitle,
                        prior.PriorWorkAddressesList);

                    listOfChanges.Add(prior);
                }

                // Save all in one transaction....
                this.leadRepository.UpdateAll(listOfChanges);

                var changed = filter.PriorWorkItemList.First();
                changed.Id = listOfChanges.First().ID.ToString();
                return changed;


            }
            else
            {
                throw new ApplicationException("Nothing to Update");
            }
        }

        /// <summary>
        ///  The update contact information.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <exception cref="Exception">
        /// validation of Nothing to save and another
        /// </exception>
        internal void UpdateContactInfo(IPriorWorkOutputModel filter)
        {
            // Valida the contact list exists
            if (filter.PriorWorkDetailObject == null || filter.PriorWorkDetailObject.ContactList == null)
            {
                throw new Exception("Nothing to save!");
            }

            // Get the detail output
            var contact = filter.PriorWorkDetailObject.ContactList.FirstOrDefault();
            if (contact == null)
            {
                throw new Exception("No contact to save!");
            }

            // Get the contact to update
            var conta = this.repositoryWithInt.Query<PriorWorkContact>().SingleOrDefault(x => x.ID == contact.ContactId);
            if (conta == null)
            {
                throw new Exception("The contact does not exists in database");
            }

            var user = this.leadRepository.Query<User>()
                .SingleOrDefault(x => x.ID == new Guid(filter.InputModel.UserId));
            if (user == null)
            {
                throw new Exception("User does not exists in database");
            }

            var prefixObject = contact.Prefix == null ? null : this.leadRepository.Load<Prefixes>(new Guid(contact.Prefix.ID));

            // Update it
            conta.UpdateContact(
                conta.StEmploymentObj,
                contact.Title,
                contact.FirstName,
                contact.MiddleName,
                contact.LastName,
                prefixObject,
                contact.IsPhoneInternational,
                contact.Phone,
                contact.Email,
                contact.IsActive,
                user.UserName,
                DateTime.Now,
                0);

            // Save and flush
            this.repositoryWithInt.UpdateAndFlush(conta);
        }

        /// <summary>
        ///  The update the comment (command 6).
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <exception cref="Exception">
        /// Fired if a input parameter is wrong
        /// </exception>
        internal void UpdateTheComment(IPriorWorkOutputModel filter)
        {
            // Validate input
            var id = filter.InputModel.PriorWorkId;
            if (id == null)
            {
                throw new Exception("The Identification of Prior Work can not be null");
            }

            var prior = filter.PriorWorkItemList.FirstOrDefault();
            if (prior == null)
            {
                throw new Exception("Missing prior work member");
            }

            var pw = this.leadRepository.Query<AdLeadEmployment>().SingleOrDefault(x => x.ID == new Guid(id));

            if (pw == null)
            {
                throw new Exception("The given Prior Work does not exists");
            }

            var user = this.leadRepository.Query<User>().SingleOrDefault(x => x.ID == new Guid(filter.InputModel.UserId));
            if (user == null)
            {
                throw new Exception("User does not exists in database");
            }

            // Update value
            pw.UpdateAdLeadEmployment(
                pw.LeadObj,
                pw.JobTitleObj,
                pw.JobStatusObj,
                pw.StartDate,
                pw.EndDate,
                prior.Comments,
                user.UserName,
                DateTime.Now,
                pw.JobResponsibilities,
                pw.EmployerName,
                pw.EmployerJobTitle,
                pw.PriorWorkAddressesList);

            this.leadRepository.UpdateAndFlush(pw);
        }

        /// <summary>
        ///  The update job responsibilities.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <exception cref="Exception">
        /// Fired if a input parameter is wrong
        /// </exception>
        internal void UpdateJobResponsibilities(IPriorWorkOutputModel filter)
        {
            // Validate input
            var id = filter.InputModel.PriorWorkId;
            if (id == null)
            {
                throw new Exception("The Identification of Prior Work can not be null");
            }

            var prior = filter.PriorWorkItemList.FirstOrDefault();
            if (prior == null)
            {
                throw new Exception("Missing prior work member");
            }

            var pw = this.leadRepository.Query<AdLeadEmployment>().SingleOrDefault(x => x.ID == new Guid(id));

            if (pw == null)
            {
                throw new Exception("The given Prior Work does not exists");
            }

            var user = this.leadRepository.Query<User>().SingleOrDefault(x => x.ID == new Guid(filter.InputModel.UserId));
            if (user == null)
            {
                throw new Exception("User does not exists in database");
            }

            // Update value
            pw.UpdateAdLeadEmployment(
                pw.LeadObj,
                pw.JobTitleObj,
                pw.JobStatusObj,
                pw.StartDate,
                pw.EndDate,
                pw.Comments,
                user.UserName,
                DateTime.Now,
                prior.JobResponsabilities,
                pw.EmployerName,
                pw.EmployerJobTitle,
                pw.PriorWorkAddressesList);

            this.leadRepository.UpdateAndFlush(pw);
        }

        /// <summary>
        ///  The delete prior work.
        /// Command 7:  Delete the prior work. Must also clean records 
        /// in Prior Work Contact and Prior Work Address
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        internal void DeletePriorWork(IPriorWorkOutputModel filter)
        {
            // Validate that the filter comes with the PriorWorkId
            if (filter.InputModel.PriorWorkId == null)
            {
                throw new Exception("The Prior ID can not be null");
            }

            var prior = this.leadRepository.Get<AdLeadEmployment>(new Guid(filter.InputModel.PriorWorkId));
            if (prior == null)
            {
                throw new ApplicationException("The prior Work does not exists");
            }

            this.leadRepository.Delete(prior);
        }

        /// <summary>
        ///  The delete contact info.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <exception cref="Exception">
        /// If the contact Id is missing or the contact referred does not exists
        /// </exception>
        internal void DeleteContactInfo(IPriorWorkOutputModel filter)
        {
            // Validate...
            if (filter.PriorWorkDetailObject == null || filter.PriorWorkDetailObject.ContactId == 0)
            {
                throw new Exception("Contact to be delete was not supplied");
            }

            // Get the contact id
            var contact = this.repositoryWithInt.Query<PriorWorkContact>().SingleOrDefault(x => x.ID == filter.PriorWorkDetailObject.ContactId);
            if (contact == null)
            {
                throw new Exception("The selected contact does not exists");
            }

            // Delete the item
            this.repositoryWithInt.Delete(contact);
        }

        ////internal void DeleteTheComment(IPriorWorkOutputModel filter)
        ////{
        ////    throw new NotImplementedException();
        ////}

        ////internal void DeleteJobResponsabilities(IPriorWorkOutputModel filter)
        ////{
        ////    throw new NotImplementedException();
        ////}

        /// <summary>
        /// The get ad titles items list.
        /// filtered by Active - CampusId and order by description
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="DropDownOutputModel"/>.
        /// List of Drop Down item titles order by description
        /// </returns>
        internal IList<DropDownOutputModel> GetSchoolJobtitlesList(PriorWorkInputModel filter)
        {
            var titlesList =
                this.leadRepository.Query<AdTitles>()
                    .Where(
                        x =>
                            (x.CampGrpObj.Campuses.Any(y => y.ID.ToString() == filter.CampusId)
                             || x.CampGrpObj.IsAllCampusGrp == true) && x.StatusObj.StatusCode == "A")
                    .Select(
                        info => new DropDownOutputModel() { ID = info.ID.ToString(), Description = info.TitleDescrip })
                    .OrderBy(t => t.Description)
                    .ToList();
            return titlesList;
        }

        /// <summary>
        /// The get job status item list.
        /// with the active items by the campusId
        /// and order by Description
        /// </summary>
        /// <param name="filter">
        /// The filter. (need to have CampusId)
        /// </param>
        /// <returns>
        /// The list of <see cref="DropDownOutputModel"/>.
        /// </returns>
        /// <returns>
        /// The list of DropDownWithCampusOutputModel
        /// </returns>
        internal IList<DropDownOutputModel> GetJobStatusItemList(PriorWorkInputModel filter)
        {
            var jobsta =
                this.leadRepository.Query<PlJobStatus>()
                    .Where(
                        x =>
                            (x.CampGrpObj.Campuses.Any(y => y.ID.ToString() == filter.CampusId)
                             || x.CampGrpObj.IsAllCampusGrp == true) && x.StatusObj.StatusCode == "A")
                    .Select(
                        info =>
                            new DropDownOutputModel()
                            {
                                ID = info.ID.ToString(),
                                Description = info.JobStatusDescrip
                            });
            return jobsta.OrderBy(x => x.Description).ToList();
        }

        /// <summary>
        ///  The get prior work details and contacts.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <returns>
        /// The <see cref="PriorWorkDetailItemOutputModel"/>.
        /// </returns>
        internal PriorWorkDetailItemOutputModel GetPriorWorkDetailsAndContacts(PriorWorkInputModel filter)
        {
            // Validate Input
            Guid priorWorkGuid;
            if (Guid.TryParse(filter.PriorWorkId, out priorWorkGuid) == false)
            {
                throw new Exception("Format Exception in Prior Work identifier");
            }

            // Get the Prior Work
            var prior = this.leadRepository.Query<AdLeadEmployment>().SingleOrDefault(x => x.ID == priorWorkGuid);
            var output = new PriorWorkDetailItemOutputModel { ContactList = new List<PriorWorkContactOutputModel>() };
            if (prior != null)
            {
                output.Comments = prior.Comments;
                output.JobResponsabilities = prior.JobResponsibilities;
                output.Employer = prior.EmployerName;
                foreach (PriorWorkContact contact in prior.PriorWorkContactsList)
                {
                    output.ContactList.Add(
                        new PriorWorkContactOutputModel()
                        {
                            PriorWorkId = prior.ID.ToString(),
                            ContactId = contact.ID,
                            Email = contact.Email,
                            FirstName = contact.FirstName,
                            IsActive = contact.IsActive,
                            LastName = contact.LastName,
                            MiddleName = contact.MiddleName,
                            IsPhoneInternational = contact.IsPhoneInternational,
                            Phone = contact.Phone,
                            Name = string.Empty,
                            Prefix =
                                    contact.PrefixObj == null
                                        ? null
                                        : new DropDownOutputModel()
                                        {
                                            Description = contact.PrefixObj.PrefixDescrip,
                                            ID = contact.PrefixObj.ID.ToString()
                                        },
                            Title = contact.JobTitle
                        });
                }
            }
            else
            {
                throw new Exception("The given prior work does not exists in database");
            }

            return output;
        }

        /// <summary>
        ///  The get USA states.
        /// </summary>
        /// <param name="repository">
        ///  The repository.
        /// </param>
        /// <param name="address">
        ///  The address.
        /// </param>
        /// <returns>
        /// The <see cref="SystemStates"/>.
        /// </returns>
        private SystemStates GetUsaStates(IRepository repository, PriorWorkAddressOutputModel address)
        {
            var stateObj =
                repository.Query<SystemStates>()
                    .SingleOrDefault(
                        x => x.Description == (address.State == null ? string.Empty : address.State.Description));
            return stateObj;
        }

        /// <summary>
        ///  The get country.
        /// </summary>
        /// <param name="repository">
        ///  The repository.
        /// </param>
        /// <param name="address">
        ///  The address.
        /// </param>
        /// <returns>
        /// The <see cref="Country"/>.
        /// </returns>
        private Country GetCountry(IRepository repository, PriorWorkAddressOutputModel address)
        {
            // Get the country and USA states
            var countryObj =
                repository.Query<Country>()
                    .SingleOrDefault(
                        x => x.CountryDescrip == (address.Country == null ? string.Empty : address.Country.Description));
            return countryObj;
        }

        /// <summary>
        /// The common validations prior work.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <exception cref="ApplicationException">
        /// if the validation fail a Exception is fired
        /// </exception>
        private void CommonValidationsPriorWork(PriorWorkItemOutputModel model)
        {
            // Check by required fields
            if (string.IsNullOrWhiteSpace(model.Employer) || string.IsNullOrWhiteSpace(model.JobTitle))
            {
                throw new ApplicationException("Employer and Job Title are required fields");
            }

            // Validate the range of date end date must not be minor than Start Date
            if (model.StartDate != null & model.EndDate != null)
            {
                if (model.StartDate > model.EndDate)
                {
                    throw new ApplicationException("End Date cannot be less than Start Date");
                }
            }
        }
    }
}
