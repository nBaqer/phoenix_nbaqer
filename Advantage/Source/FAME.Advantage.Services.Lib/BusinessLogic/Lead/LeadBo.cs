﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadBo.cs" company="FAME">
//   FAME 2016
// </copyright>
// <summary>
//   Business object for Lead Controller
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.AdvantageV1.Common;

namespace FAME.Advantage.Services.Lib.BusinessLogic.Lead
{
    using AutoMapper;
    using Domain.Campuses;
    using Domain.Catalogs;
    using Domain.Infrastructure.Entities;
    using Domain.Lead;
    using Domain.MostRecentlyUsed;
    using Domain.SystemStuff;
    using Domain.SystemStuff.ConfigurationAppSettings;
    using Domain.SystemStuff.SchoolDefinedFields;
    using Domain.Users;
    using FluentNHibernate.Conventions;
    using FluentNHibernate.Utils;
    using Infrastructure.Extensions;
    using Messages.Common;
    using Messages.Enumerations;
    using Messages.Lead;
    using Messages.MostRecentlyUsed;
    using Messages.SystemStuff.SchoolDefinedFields;
    using Messages.SystemStuff.SystemStatuses;
    using Messages.Web;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using Utils;

    /// <summary>
    /// Business object for Lead Controller
    /// </summary>
    public partial class LeadBo
    {
        /// <summary>
        ///  The lead MRU type ID.
        /// </summary>
        public const int LeadMruTypeID = (int)Enumerations.MRuTypes.Leads;

        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        /// The student image path.
        /// </summary>
        private string studentImagePath;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadBo"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="repository">
        /// The repository
        /// </param>
        /// <param name="repositoryWithInt">
        /// The repository integer
        /// </param>
        public LeadBo(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            this.repositoryWithInt = repositoryWithInt;
        }

        /// <summary>
        /// Get the Lead info to be show in Lead Info Bar.
        /// </summary>
        /// <param name="filter">Get Lead Input Model</param>
        /// <returns>
        /// Lead InfoBar Output Model
        /// </returns>
        public LeadInfoBarOutputModel GetLeadInfo(GetLeadInputModel filter)
        {
            // Get path for images from configuration file
            var path = this.StudentImagePath;
            var sitepath = this.AdvantageSiteUrl;
            var lead = this.repository.Query<Lead>().SingleOrDefault(x => x.ID == filter.LeadGuid);
            if (lead == null)
            {
                throw new ArgumentNullException("filter", "The lead requested does not exists");
            }

            var ou = new LeadInfoBarOutputModel
            {
                LeadFullName = lead.LeadInfoBarName,
                CurrentStatus = lead.LeadStatus.Description,
                ProgramVersionName = (lead.ProgramVersion == null) ? string.Empty : lead.ProgramVersion.Description,
                AdmissionRep = (lead.AdmissionRepUserObj == null) ? string.Empty : lead.AdmissionRepUserObj.FullName,
                LeadGuid = lead.ID.ToString(),
                ExpectedStart = lead.ExpectedStart,
                LeadImagePath =
                 (lead.ImagePartialUrl == string.Empty)
                     ? sitepath + "/images/face75.png"
                     : path + lead.ImagePartialUrl,
                DateAssigned = lead.AssignedDate,
                ContentType = lead.EncodeImage,
                CampusName = (lead.Campus == null) ? string.Empty : lead.Campus.Description,
                IsLeadEnrolled = (lead.LeadStatus.SystemStatusId == 6) ? true : false
            };

            return ou;
        }

        /// <summary>
        /// The get lead.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <returns>
        /// The <see cref="LeadOutputModel"/>.
        /// </returns>
        public LeadOutputModel GetLead(Guid leadId)
        {
            var lead = this.repository.Query<Lead>().FirstOrDefault(n => n.ID == leadId);
            return Mapper.Map<LeadOutputModel>(lead);
        }

        /// <summary>
        /// Get the Students based on the search term passed in from the universal search
        /// </summary>
        /// <param name="searchTermInit">
        /// The search Term Initialization.
        /// </param>
        /// <param name="leadStatuses">
        /// The lead Statuses.
        /// </param>
        /// <param name="adminReps">
        /// The admin Reps.
        /// </param>
        /// <returns>
        /// Return a list of output model
        /// </returns>
        public IEnumerable<LeadSearchOutputModel> GetLeadSearch(string[] searchTermInit, List<Guid> leadStatuses, List<Guid> adminReps)
        {
            var searchTerm = searchTermInit[0];
            if (string.IsNullOrEmpty(searchTerm))
            {
                return null;
            }

            var campusesList = new List<Guid>();

            if (searchTermInit.Count() > 1)
            {
                var list = searchTermInit[1].ToString(CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(list))
                {
                    campusesList = list.Split(',').Select(Guid.Parse).ToList();
                }
            }

            // var leads = repository.Query<FAME.Advantage.Domain.Lead.Lead>().Where(n => ((n.LastName ?? "") + " " + (n.FirstName ?? "") + " " + (n.LastName ?? "") + " " + (n.SSN ?? "") + " " + (n.Phone ?? "") + " " + (n.Phone2 ?? "")).Contains(searchTerm));
            var leads = this.repository.Query<Lead>()
                    .Where(
                        n =>
                            ((n.LastName ?? string.Empty) + " " + (n.FirstName ?? string.Empty) + " " + (n.LastName ?? string.Empty) + " " +
                             (n.SSN ?? string.Empty)).Contains(searchTerm))
                    .Take(100);
            var leadPhones = this.repository.Query<LeadPhone>()
                    .Where(x => x.Phone.Contains(searchTerm))
                    .Select(n => n.NewLeadObj)
                    .Take(100);

            var contLeads = new List<Lead>();

            contLeads.AddRange(leads);
            contLeads.AddRange(leadPhones);
            contLeads = contLeads.Distinct().ToList();

            var finalLeads = campusesList.Count > 0
                ? contLeads.ToList().Where(l => l.Campus != null && campusesList.Contains(l.Campus.ID)).ToList()
                : contLeads.ToList();

            var duplicatedLeadStatus = this.repositoryWithInt.Query<SystemStatus>().FirstOrDefault(
                                          x => x.Description.ToUpper() == "DUPLICATED"
                                               && (x.StatusLevelId.HasValue
                                                   && x.StatusLevelId.Value == (int)StatusLevel.Lead))
                                       ?? this.repositoryWithInt.Query<SystemStatus>().FirstOrDefault(x => x.ID == 26);


            if (duplicatedLeadStatus != null)
            {
                // exclude leads with duplicated system status
                var duplicatedId = duplicatedLeadStatus.ID;
                finalLeads = finalLeads.Where(
                    l => l.LeadStatus.SystemStatus.ID != duplicatedId).ToList();
            }

            finalLeads =
                finalLeads.Where(n => (leadStatuses == null || leadStatuses.Count == 0 || leadStatuses.Contains(n.LeadStatus.ID)) && (adminReps == null || adminReps.Count == 0 || adminReps.Contains(n.AdmissionRepUserObj.ID))).ToList();

            this.studentImagePath = string.Empty;

            string siteUrl = string.Empty;

            var configurationAppSetting = this.repositoryWithInt.Query<ConfigurationAppSetting>()
                    .FirstOrDefault(n => n.KeyName.ToUpper() == "STUDENTIMAGEPATH");
            if (configurationAppSetting != null)
            {
                this.studentImagePath = configurationAppSetting.ConfigurationValues[0].Value;
            }

            configurationAppSetting = this.repositoryWithInt.Query<ConfigurationAppSetting>()
                    .FirstOrDefault(n => n.KeyName.ToUpper() == "ADVANTAGESITEURI");
            if (configurationAppSetting != null)
            {
                siteUrl = configurationAppSetting.ConfigurationValues[0].Value;
            }

            finalLeads.ForEach(r => r.GetImageUrl(this.studentImagePath, siteUrl));

            // return the list of leads 
            var output =
                Mapper.Map<IEnumerable<LeadSearchOutputModel>>(
                    finalLeads.Distinct().OrderBy(n => n.LastName).ThenBy(n => n.FirstName).ToArray());
            return output;
        }

        /// <summary>
        /// get leads that are unassigned to a campus
        /// </summary>
        /// <param name="vendorId">
        /// The vendor Id.
        /// </param>
        /// <param name="areaOfInterest">
        /// The area Of Interest.
        /// </param>
        /// <returns>
        /// </returns>
        public IEnumerable<LeadOutputModel> GetLeadsUnassignedToCampus(int? vendorId, string areaOfInterest)
        {
            var duplicatelist = this.repositoryWithInt.Query<AdLeadDuplicates>().ToList();
            var dupGuids = duplicatelist.Select(dup => dup.NewLeadObj.ID).ToList();

            //foreach (Lead lead in leadWithoutCampus)
            //{
            //    var exist = duplicatelist.Any(duplicates => lead.ID == duplicates.NewLeadObj.ID || lead.ID == duplicates.PosibleLeadDuplicateObj.ID);
            //    if (exist == false)
            //    {
            //        campusCount++;
            //    }
            //}

            ////var leads = this.repository.Query<Lead>().Where(n => n.Campus == null
            ////            && n.LeadStatus.SystemStatusId == 25
            ////            && !dupGuids.Contains(n.ID));
            IList<Lead> leadList = new List<Lead>();

            // get the leads using the search term passed in
            var leads =
                this.repository.Query<Lead>()
                    .Where(n => n.Campus == null && n.LeadStatus.SystemStatusId == 25);
            ////   && n.DuplicateLeadsList.Count == 0); // !dupGuids.Contains(n.ID));

            foreach (Lead lead in leads)
            {
                var exist = duplicatelist.Any(duplicates => lead.ID == duplicates.NewLeadObj.ID || lead.ID == duplicates.PosibleLeadDuplicateObj.ID);
                if (exist == false)
                {
                    leadList.Add(lead);
                }
            }

            leads = leadList.AsQueryable();

            if (vendorId != null && vendorId != -1)
            {
                if (vendorId == 0)
                {
                    leads = leads.Where(n => n.CampaignObj == null);
                }
                else
                {
                    var campaigns = this.repositoryWithInt.Query<AdVendorCampaign>().Where(n => n.VendorObj.ID == vendorId);
                    //// IList<int> campaignIds = campaigns.Select(c => c.ID).ToList();
                    leadList.Clear();
                    foreach (Lead ld in leads)
                    {
                        foreach (AdVendorCampaign campaign in campaigns)
                        {
                            if (ld.CampaignObj.ID == campaign.ID)
                            {
                                leadList.Add(ld);
                                break;
                            }
                        }
                    }

                    leads = leadList.AsQueryable();

                    ////leadList = leadList.OrderBy(n => n.LastName).ThenBy(n => n.LastName).Select(n => n).Take(1000).ToList();
                    ////return Mapper.Map<IEnumerable<LeadOutputModel>>(leadList);
                    ////// leads = leads.Where(n => campaignIds.Contains(n.CampaignObj.ID));
                }
            }
            if (!string.IsNullOrEmpty(areaOfInterest))
            {
                leads = areaOfInterest == Guid.Empty.ToString()
                            ? leads.Where(n => n.ProgramGroupObj == null)
                            : leads.Where(n => n.ProgramGroupObj != null && n.ProgramGroupObj.ID.ToLowerInvariantString() == areaOfInterest.ToLowerInvariantString());
            }

            leads = leads.OrderBy(n => n.LastName).ThenBy(n => n.FirstName).Take(1000);


            // return the list of leads 
            return Mapper.Map<IEnumerable<LeadOutputModel>>(leads).ToArray();
        }

        /// <summary>
        /// Get the leads that are unassigned to a rep
        /// </summary>
        /// <param name="vendorId">
        /// The vendor Id.
        /// </param>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The list of output model lead without admission represent assigned
        /// </returns>
        public IEnumerable<LeadOutputModel> GetLeadsUnassignedToRep(int? vendorId, Guid campusId)
        {
            var duplicatelist = this.repositoryWithInt.Query<AdLeadDuplicates>().ToList();
            IList<Lead> leadList = new List<Lead>();

            // get the leads using the search term passed in
            var leads = this.repository.Query<Lead>().Where(n => n.Campus.ID == campusId
                                    && n.AdmissionRepUserObj == null
                                    && n.LeadStatus.SystemStatusId != 4
                                    && n.LeadStatus.SystemStatusId != 17
                                    && n.LeadStatus.SystemStatusId != 26);

            foreach (Lead lead in leads)
            {
                var exist = duplicatelist.Any(duplicates => lead.ID == duplicates.NewLeadObj.ID || lead.ID == duplicates.PosibleLeadDuplicateObj.ID);
                if (exist == false)
                {
                    leadList.Add(lead);
                }
            }

            leads = leadList.AsQueryable();

            if (vendorId != null && vendorId != -1)
            {
                if (vendorId == 0)
                {
                    leads = leads.Where(n => n.CampaignObj == null);
                }
                else
                {
                    var campaigns = this.repositoryWithInt.Query<AdVendorCampaign>().Where(n => n.VendorObj.ID == vendorId);
                    IList<int> campaignIds = campaigns.Select(c => c.ID).ToList();

                    leads = leads.Where(n => campaignIds.Contains(n.CampaignObj.ID));
                }
            }

            leads = leads.OrderBy(n => n.LastName).ThenBy(n => n.FirstName).Take(1000);

            // return the list of leads 
            return Mapper.Map<IEnumerable<LeadOutputModel>>(leads).ToArray();
        }

        /// <summary>
        /// get the leads to reassign to a campus
        /// </summary>
        /// <returns></returns>
        public IEnumerable<LeadOutputModel> GetLeadsReAssignToCampus(Guid campusId, string lastModDate)
        {
            // get the leads using the search term passed zz
            var leads = this.repository.Query<Lead>().
                            Where(n => n.Campus.ID == campusId
                            && n.AdmissionRepUserObj != null
                            && n.LeadStatus.SystemStatus.StatusLevelId == 1
                            && (n.LeadStatus.SystemStatus.ID == 1 ||
                                n.LeadStatus.SystemStatusId == 2 ||
                                n.LeadStatus.SystemStatusId == 4 ||
                               n.LeadStatus.SystemStatusId == 5));

            // var outputLeads = new List<LeadOutputModel>();
            if (!string.IsNullOrEmpty(lastModDate) && lastModDate.ToUpper() != "ALL")
            {
                var lModDate = Convert.ToInt32(lastModDate);
                var filterDate = DateTime.Now.Date.AddDays(-lModDate);

                leads = leads.Where(n => n.ModDate != null && n.ModDate >= filterDate);
            }

            leads = leads.OrderBy(n => n.LastName).ThenBy(n => n.FirstName).Take(1000);
            return Mapper.Map<IEnumerable<LeadOutputModel>>(leads).ToArray();
        }

        /// <summary>
        /// Get the leads to reassign to a rep
        /// </summary>
        /// <returns></returns>
        public IEnumerable<LeadOutputModel> GetLeadsReAssignToRep(Guid campusId, Guid adminRepId, string lastModDate)
        {
            // get the leads using the search term passed in
            var leads = this.repository.Query<Lead>().
                Where(n => n.Campus.ID == campusId
                && n.AdmissionRepUserObj.ID == adminRepId
                && n.LeadStatus.SystemStatus.StatusLevelId == 1
                && (n.LeadStatus.SystemStatus.ID == 1 ||
                    n.LeadStatus.SystemStatusId == 2 ||
                    n.LeadStatus.SystemStatusId == 4 ||
                               n.LeadStatus.SystemStatusId == 5));

            // var outputLeads = new List<LeadOutputModel>();
            if (!string.IsNullOrEmpty(lastModDate) && lastModDate.ToUpper() != "ALL")
            {
                var lModDate = Convert.ToInt32(lastModDate);
                var filterDate = DateTime.Now.Date.AddDays(-lModDate);

                leads = leads.Where(n => n.ModDate != null && n.ModDate >= filterDate);

            }

            leads = leads.OrderBy(n => n.LastName).ThenBy(n => n.FirstName).Take(1000);


            return Mapper.Map<IEnumerable<LeadOutputModel>>(leads).ToArray();
        }

        /// <summary>
        /// Get lead assignment count
        /// </summary>
        /// <returns>
        /// The lead Assigned to campus and Represent
        /// </returns>
        public LeadsAssignedCountOutputModel GetLeadAssignmentCount()
        {
            var duplicatelist = this.repositoryWithInt.Query<AdLeadDuplicates>().ToList();
            ////var dupGuids = duplicatelist.Select(dup => dup.NewLeadObj.ID).ToList();

            // Calculate the lead count without campus...
            var leadsquery = this.repository.Query<Lead>().Where(x => x.LeadStatus.SystemStatusId == 25);
            var leadWithoutCampus = leadsquery.Where(x => x.Campus == null);
            var campusCount = 0;
            foreach (Lead lead in leadWithoutCampus)
            {
                var exist = duplicatelist.Any(duplicates => lead.ID == duplicates.NewLeadObj.ID || lead.ID == duplicates.PosibleLeadDuplicateObj.ID);
                if (exist == false)
                {
                    campusCount++;
                }
            }

            // Calculate the lead without admission rep
            // get the leads using the search term passed in
            var leads = this.repository.Query<Lead>().Where(n => n.Campus.ID != null
                                    && n.AdmissionRepUserObj == null
                                    && n.LeadStatus.SystemStatusId != 4
                                    && n.LeadStatus.SystemStatusId != 17
                                    && n.LeadStatus.SystemStatusId != 26);

            var admissRepCount = leads.Count();

            var leadsNotAssignedCounts = new LeadsAssignedCountOutputModel
            {
                NotAssignedCampusCount = campusCount,
                NotAssignedRepCount = admissRepCount
            };

            return leadsNotAssignedCounts;
        }

        /// <summary>
        /// get lead assignment count
        /// </summary>
        /// <returns></returns>
        public LeadContactsOutputModel GetLeadContactInfo(Guid leadId)
        {
            var lead = this.repository.Query<Lead>().FirstOrDefault(n => n.ID == leadId);
            if (lead != null)
            {
                var leadPhones = this.repository.Query<LeadPhone>().Where(n => n.NewLeadObj.ID == leadId).OrderBy(x => x.ModDate);
                var leadEmails = this.repository.Query<LeadEmail>()
                    .Where(n => n.LeadObj.ID == leadId)
                    .OrderBy(x => x.ModDate);
                var leadAddresses = this.repository.Query<LeadAddress>().Where(n => n.LeadObj.ID == leadId).OrderBy(x => x.ModDate);
                var phoneTypes = this.repository.Query<SyPhoneType>().Where(x => x.SyStatusesObj.StatusCode == "A").OrderBy(x => x.PhoneTypeDescrip);
                var addressTypes = this.repository.Query<PlAddressTypes>().Where(x => x.SyStatusesObj.StatusCode == "A").OrderBy(x => x.AddressDescrip);
                var emailTypes = this.repository.Query<SyEmailType>().OrderBy(x => x.EMailTypeDescription);
                var countries = this.repository.Query<Country>().Where(x => x.SyStatusesObj.StatusCode == "A").OrderBy(x => x.CountryDescrip);
                var states = this.repository.Query<SystemStates>().Where(x => x.StatusObj.StatusCode == "A").OrderBy(x => x.Code);
                var status = this.repository.Query<SyStatuses>().OrderBy(x => x.Status);
                var counties = this.repository.Query<County>().Where(x => x.SyStatusesObj.StatusCode == "A").OrderBy(x => x.CountyDescrip);
                var contactTypes = this.repository.Query<ContactType>().Where(x => x.Status.StatusCode == "A").OrderBy(x => x.Description);
                var sufixes = this.repository.Query<Suffix>().Where(x => x.Status.StatusCode == "A").OrderBy(x => x.Description);
                var prefixes = this.repository.Query<Prefix>().Where(x => x.Status.StatusCode == "A").OrderBy(x => x.Description);
                var relationships = this.repository.Query<Relationship>().Where(x => x.Status.StatusCode == "A").OrderBy(x => x.Description);
                var comment = this.repositoryWithInt.Query<AllNotes>()
                        .SingleOrDefault(x => x.LeadList.Any(y => y.ID == leadId) && x.PageFieldsObj.ID == (int)Enumerations.EnumNotePageField.ContactComment);

                var contactList = new LeadContactsOutputModel()
                {
                    LeadId = leadId.ToString(),
                    LeadPhonesList =
                                              Mapper.Map<IEnumerable<LeadPhonesOutputModel>>(
                                                  leadPhones).ToList(),
                    LeadEmailList =
                                              Mapper.Map<IEnumerable<LeadEmailOutputModel>>(
                                                  leadEmails).ToList(),
                    LeadAddressList =
                                              Mapper.Map<IEnumerable<LeadAddressOutputModel>>(
                                                  leadAddresses).ToList(),
                    PhoneTypes =
                                              Mapper.Map<IEnumerable<SystemPhoneTypeOuputModel>>(
                                                  phoneTypes).ToList(),
                    AddressTypes =
                                              Mapper.Map<IEnumerable<SystemAddressTypeOutputModel>>(
                                                  addressTypes).ToList(),
                    EmailTypes =
                                              Mapper.Map<IEnumerable<SystemEmailTypeOutputModel>>(
                                                  emailTypes).ToList(),
                    Countries =
                                              Mapper.Map<IEnumerable<DropDownOutputModel>>(
                                                  countries).ToList(),
                    States =
                                              Mapper.Map<IEnumerable<DropDownOutputModel>>(states)
                                              .ToList(),
                    Statuses =
                                              Mapper.Map<IEnumerable<DropDownOutputModel>>(status)
                                              .ToList(),
                    Comments =
                                              (comment != null) ? comment.NoteText : string.Empty,

                    // repository.Query<Domain.Lead.Lead>().SingleOrDefault(x => x.NotesList)
                    Counties =
                                              Mapper.Map<IEnumerable<DropDownOutputModel>>(counties)
                                              .ToList(),
                    ContactTypes =
                                              Mapper.Map<IEnumerable<DropDownOutputModel>>(
                                                  contactTypes).ToList(),
                    Sufixes =
                                              Mapper.Map<IEnumerable<DropDownOutputModel>>(sufixes)
                                              .ToList(),
                    Prefixes =
                                              Mapper.Map<IEnumerable<DropDownOutputModel>>(prefixes)
                                              .ToList(),
                    Relationships =
                                              Mapper.Map<IEnumerable<DropDownOutputModel>>(
                                                  relationships).ToList(),
                };

                return contactList;
            }

            return null;
        }

        /// <summary>
        /// Get List of Other Contacts
        /// </summary>
        /// <returns></returns>
        public Response<List<LeadOtherContactModel>> GetLeadOtherContacts(Guid leadId)
        {
            Response<List<LeadOtherContactModel>> response = new Response<List<LeadOtherContactModel>>();
            var lead = this.repository.Query<Lead>().FirstOrDefault(n => n.ID == leadId);
            if (lead != null)
            {

                var data = this.repository.Query<LeadOtherContact>().Where(n => n.Lead.ID == leadId).OrderBy(x => x.ModDate);

                List<LeadOtherContactModel> results = Mapper.Map<IEnumerable<LeadOtherContactModel>>(data).ToList();

                response.Data = results;

                response.ResponseCode = (int)HttpStatusCode.OK;
            }
            else
            {
                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                response.ResponseMessage = "Missing Lead in Request!";
            }

            return response;
        }


        /// <summary>
        /// update the campuses for a list of leads
        /// </summary>
        /// <param name="leadIds"></param>
        /// <param name="campusId"></param>
        /// <param name="userId"></param>
        public void AssignLeadsToCampus(IEnumerable<GetLeadAssignmentnputModel> leadIds, Guid campusId, Guid userId)
        {
            string userName = null;
            var user = this.repository.Query<User>().FirstOrDefault(n => n.ID == userId);
            if (user != null) userName = user.UserAndName;

            foreach (var i in leadIds)
            {
                var lead = this.repository.Get<Lead>(i.Id);
                if (lead != null)
                {
                    var campus = this.repository.Get<Campus>(campusId);

                    lead.UpdateAssignLeadToCampus(campus, userName);
                    this.repository.Save(lead);
                }

            }
        }

        /// <summary>
        /// update the admission rep for a list of leads
        /// </summary>
        /// <param name="leadIds"></param>
        /// <param name="repId"></param>
        /// <param name="userId"></param>
        public void AssignLeadsToRep(IEnumerable<GetLeadAssignmentnputModel> leadIds, Guid repId, Guid userId)
        {
            string userName = null;
            var user = this.repository.Query<User>().FirstOrDefault(n => n.ID == userId);
            if (user != null)
            {
                userName = user.UserName;
            }

            foreach (var i in leadIds)
            {
                var lead = this.repository.Get<Lead>(i.Id);

                if (lead != null)
                {

                    var adminRep = this.repository.Get<User>(repId);
                    if (adminRep == null)
                    {
                        throw new Exception("Admission Rep object not found");
                    }

                    var leadStatus = this.repository.Query<UserDefStatusCode>()
                            .FirstOrDefault(x => x.IsDefaultLeadStatus == true && (x.SystemStatus.StatusLevelId.HasValue && x.SystemStatus.StatusLevelId.Value == (int)StatusLevel.Lead)) ?? this.repository.Query<UserDefStatusCode>()
                            .FirstOrDefault(x => x.Description.ToUpper() == "NEW LEAD") ?? this.repository.Query<UserDefStatusCode>()
                                .FirstOrDefault(x => x.SystemStatus.ID == 1);


                    lead.UpdateAssignLeadToRep(adminRep, leadStatus, userName);
                    this.repository.Save(lead);
                }

            }
        }


        /// <summary>
        /// Get the lead in MRU List
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="campusId"></param>
        /// <returns></returns>
        public IEnumerable<LeadMruOutputModel> GetLeadMru(Guid userId, Guid campusId)
        {
            var mrus = this.repository.Query<Mru>()
                    .Where(n => n.MruTypeObj.ID == LeadMruTypeID && n.UserObj.ID == userId)
                    .OrderByDescending(n => n.ModDate);
            var mruGuids = mrus.Select(m => m.EntityId).ToList();
            var mruList = mrus.Select(l => new MruOutput { EntityId = l.EntityId, ModDate = l.ModDate }).ToList();

            if (mrus.Any())
            {
                var leads =
                    IEnumerableExtensions.ForEach(
                        this.repository.Query<Lead>().Where(n => mruGuids.Contains(n.ID)),
                        r => r.GetImageUrl(this.StudentImagePath, this.AdvantageSiteUrl));

                return this.FillMruOutput(leads, mruList);
            }
            else
            {
                var campusIdList = this.GetCampusItemsByUserId(userId);

                var st2 =
                    IEnumerableExtensions.ForEach(
                        this.repository.Query<Lead>()
                            .Where(n => campusIdList.Contains(n.Campus.ID))
                            .OrderByDescending(n => n.ModDate),
                        r => r.GetImageUrl(this.StudentImagePath, this.AdvantageSiteUrl));

                return this.FillMruOutput(st2, null);
            }
        }




        #region Lead Info Page

        /// <summary>
        /// Get the list group information associated with campus ID.
        /// </summary>
        /// <param name="input">
        ///  The filter 
        /// </param>
        /// <returns>
        /// A list of The lead Group Info Output Model
        /// </returns>
        public IList<LeadGroupInfoOutputModel> GetLeadGroupInformation(LeadInputModel input)
        {
            var queriable = this.repository.Query<LeadGroups>().Where(x => x.Status.StatusCode == "A" && x.UseForStudentGroupTracking == false);
            var campuses = this.repository.Query<Campus>().Where(x => x.Status.StatusCode == "A").ToList();
            IList<LeadGroupInfoOutputModel> list = new List<LeadGroupInfoOutputModel>();
            foreach (LeadGroups c in queriable)
            {

                if (c.CampusGroup.IsAllCampusGrp == true)
                {
                    foreach (Campus campus in campuses)
                    {
                        var group = new LeadGroupInfoOutputModel();
                        group.CampusId = campus.ID.ToString();
                        group.Description = c.Description;
                        group.GroupId = c.ID;
                        list.Add(group);
                    }
                }
                else
                {
                    var campuslist = c.CampusGroup.Campuses;
                    foreach (Campus campus in campuslist)
                    {
                        var group = new LeadGroupInfoOutputModel();
                        group.CampusId = campus.ID.ToString();
                        group.Description = c.Description;
                        group.GroupId = c.ID;
                        list.Add(group);
                    }
                }
            }

            return list.OrderBy(x => x.Description).ToList();
        }

        /// <summary>
        /// Get school custom fields. The lead information is not used
        /// </summary>
        /// <param name="input">
        /// The filter for get the SDF:
        /// PageResourceId, 
        /// LeadId (only for Lead Info Page) 0 New Lead 1: Edit
        /// </param>
        /// <returns>
        /// A list of SDF Fields
        /// </returns>
        public IList<SdfOutputModel> GetLeadSchoolCustomFields(LeadInputModel input)
        {
            var pageResource = input.PageResourceId;

            // Get the list of SDF for the actual page  
            IQueryable sdfs;
            if (input.PageResourceId == 92 || input.PageResourceId == 90)
            {
                sdfs =
               this.repository.Query<Sdf>()
                   .Where(
                       x =>
                       x.ResourcesList.AsQueryable()
                           .Where(
                               y =>
                               y.EntityObj.ID == (int)Enumerations.AdvantageEntitiesEnum.Student
                               && y.ResourceObj.ID == pageResource) != null)
                   .Where(t => t.StatusObj.StatusCode == "A");
            }
            else
            {
                sdfs =
               this.repository.Query<Sdf>()
                   .Where(
                       x =>
                       x.ResourcesList.AsQueryable()
                           .Where(
                               y =>
                               y.EntityObj.ID == (int)Enumerations.AdvantageEntitiesEnum.Lead
                               && y.ResourceObj.ID == pageResource) != null)
                   .Where(t => t.StatusObj.StatusCode == "A");
            }

            IList<SdfOutputModel> listOutput = new List<SdfOutputModel>();

            // Fill the output
            foreach (Sdf sdf in sdfs)
            {
                var output = new SdfOutputModel
                {
                    Actions = (input.LeadId == "0") ? "New" : "Edit",
                    ControlType = sdf.ValTypeId.ToString(CultureInfo.InvariantCulture),
                    SdfId = sdf.ID.ToString(),
                    AssignedValue = string.Empty,
                    Decimals = sdf.Decimals,
                    Description = sdf.SdfDescrip,
                    DtypeId = sdf.DtypeId,
                    Size = sdf.Len,
                    Position = sdf.ResourcesList.Single(x => x.ResourceObj.ID == pageResource).Position
                };

                Sdf sdf1 = sdf;
                var singleOrDefault = sdf.ValuesList.AsQueryable().SingleOrDefault(x => x.SdfObj.ID == sdf1.ID);
                if (singleOrDefault != null)
                {
                    var scvstr = singleOrDefault.ValList;
                    output.ListOfValues = scvstr.Split(',');
                    output.ListOfValues = output.ListOfValues.OrderBy(x => x).ToList();
                }

                var rangeList = sdf.RangeList.AsQueryable().SingleOrDefault(x => x.SdfObj.ID == sdf1.ID);
                if (rangeList != null)
                {
                    output.Range = new SdfRangeOutputModel { MaxVal = rangeList.MaxVal, MinVal = rangeList.MinVal };
                }

                output.Required = sdf.IsRequired.ToString();
                output.Size = sdf.Len;
                listOutput.Add(output);
            }

            return listOutput;
        }

        /// <summary>
        /// Get the lead information from the given Lead Id.
        /// </summary>
        /// <param name="input">
        /// Lead Id mandatory<br/>
        /// Campus Id : Optional
        ///  </param>
        /// <returns>All Information referent to the lead and status</returns>
        public LeadInfoPageOutputModel GetLeadInfoPageInformation(LeadInputModel input)
        {
            // Get the Lead Information 
            var leadGuid = new Guid(input.LeadId);
            var lead = this.repository.Query<Lead>().SingleOrDefault(x => x.ID == leadGuid);
            var output = Mapper.Map<LeadInfoPageOutputModel>(lead);
            output.ModUser = lead.ModUser;
            output.IsEnrolled = false;
            // Get the SDF Fields
            var sdfFields = this.repository.Query<SdfModuleValueLead>().Where(x => x.LeadObj.ID.ToString() == input.LeadId);
            if (sdfFields.Any())
            {
                output.SdfList = new List<LeadSdfOutputModel>();
                foreach (SdfModuleValueLead field in sdfFields)
                {
                    var sdf = new LeadSdfOutputModel { SdfId = field.SdfObj.ID.ToString(), SdfValue = field.SdfValue, DtypeId = field.SdfObj.DtypeId };
                    output.SdfList.Add(sdf);
                }
            }

            // Get the Lead Groups 
            if (lead != null)
            {
                output.GroupIdList = lead.LeadGroupsList.Select(x => x.ID.ToString()).ToList();
                if (lead.LeadStatus.SystemStatusId == 6)
                {
                    output.IsEnrolled = true;
                }
            }

            return output;
        }

        #endregion

        /// <summary>
        /// Get legal status change for a determinate campus, actual status and user.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public List<string> GetLegalStatusChangesIds(LeadInputModel input)
        {
            var importedStatus = this.repository.Query<UserDefStatusCode>()
                .Where(x => x.ID == Guid.Parse(input.LeadStatusId)).Select(x => x.SystemStatusId).ToList();

            // it means its imported
            if (importedStatus[0].ToString() == "25")
            {
                var resultImport = this.repository.Query<UserDefStatusCode>()
                    .Where(
                        x => x.SystemStatus.StatusLevelId == 1 && x.Status.StatusCode == "A" && x.SystemStatus.ID == 25);
                var resultNewLead = this.repository.Query<UserDefStatusCode>()
                    .Where(
                        x => x.SystemStatus.StatusLevelId == 1 && x.Status.StatusCode == "A" && x.SystemStatus.ID == 1 && x.IsDefaultLeadStatus == true);
                if (resultNewLead.Any(x => x.SystemStatusId == 1 && x.IsDefaultLeadStatus == true))
                {
                    var result = resultImport.Select(x => x.ID.ToString()).ToList();
                    result.Add(resultNewLead.FirstOrDefault().ID.ToString());
                    return result;
                }
                else
                {
                    var result  = resultImport.Select(x => x.ID.ToString()).ToList();
                    result.Add(this.repository.Query<UserDefStatusCode>().Where(
                        x => x.SystemStatus.StatusLevelId == 1 && x.Status.StatusCode == "A" && x.SystemStatus.ID == 1).FirstOrDefault().ID.ToString());
                    return result;
                }
                }

            var lisr = this.repository.Query<ViewLeadStatusChangesPermission>()
                .Where(x => x.CampusObj.ID.ToString() == input.CampusId
                            && x.OrigStatusObj.ID.ToString() == input.LeadStatusId
                            && x.UserObj.ID.ToString() == input.UserId)
                .Select(x => x.NewStatusObj.ID.ToString())
                .ToList();
            return lisr;
        }

        public Response<LeadContactsOutputModel> CreateOrUpdateLeadPhone(LeadContactInputModel input)
        {
            Response<LeadContactsOutputModel> response = new Response<LeadContactsOutputModel>();
            try
            {
                LeadPhone leadPhone = null;
                var phoneType = this.repository.Query<SyPhoneType>().FirstOrDefault(x => x.ID == Guid.Parse(input.TypeId));
                var lead = this.repository.Query<Lead>().FirstOrDefault(x => x.ID == Guid.Parse(input.LeadId));
                var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.ID == Guid.Parse(input.StatusId));
                bool canContinue = true;
                int position = 0;
                int maxPosition = 1;
                string strOutput;

                if (!FormatValidator.IsValidPhoneNumber(input.Phone.Replace("_", string.Empty)))
                {
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage =
                        "Invalid Phone Number Format. A valid phone number should only include numbers or special phone characters (*,+,-,/,#)";
                    return response;
                }

                if (!string.IsNullOrEmpty(input.Extension))
                {
                    input.Extension = input.Extension.Trim().Replace("_", string.Empty);
                    if (!FormatValidator.IsOnlyNumbers(input.Extension))
                    {
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage =
                            "Invalid ExtensionFormat. A valid extension should only include numbers";
                        return response;
                    }
                }

                if (status == null)
                {
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Invalid Status";
                    return response;
                }

                if (!string.IsNullOrEmpty(input.ID))
                {
                    leadPhone = this.repository.Query<LeadPhone>().FirstOrDefault(x => x.ID == Guid.Parse(input.ID));
                }
                bool showError = false;
                if (status.StatusCode == "A")
                {
                    IQueryable<LeadPhone> existingPhoneIsBest =
                        this.repository.Query<LeadPhone>()
                            .Where(
                                x =>
                                x.NewLeadObj.ID == Guid.Parse(input.LeadId) && x.Status.StatusCode == "A"
                                && (x.Position == (int)LeadPhone.ELeadPhonePosition.IsBest || x.IsBest)).AsQueryable();

                    IQueryable<LeadPhone> existingPhoneShowOnLeadPage = this.repository.Query<LeadPhone>()
                            .Where(
                                x =>
                                x.NewLeadObj.ID == Guid.Parse(input.LeadId) && x.Status.StatusCode == "A" && x.IsBest == false
                                && (x.Position == (int)LeadPhone.ELeadPhonePosition.ShowOnLeadPage || x.Position == (int)LeadPhone.ELeadPhonePosition.ShowOnLeadPageThird || x.IsShowOnLeadPage)).AsQueryable();

                    if (input.IsBest && existingPhoneIsBest.Any())
                    {
                        if ((input.IsBest || input.ShowOnLeadPage)
                            && (string.IsNullOrEmpty(input.ID)
                                || existingPhoneIsBest.Count(x => x.ID == Guid.Parse(input.ID)) == 0))
                        {
                            canContinue = false;
                            showError = true;
                        }
                    }

                    if (leadPhone != null)
                    {
                        if (leadPhone.IsBest && !input.IsBest && input.ShowOnLeadPage && existingPhoneShowOnLeadPage.Count() >= 2)
                        {
                            canContinue = false;
                            showError = true;
                        }
                        else if ((!leadPhone.IsBest && input.IsBest && existingPhoneIsBest.Any())
                            || (!leadPhone.IsBest && input.ShowOnLeadPage && existingPhoneShowOnLeadPage.Count() >= 2 && !input.IsBest))
                        {
                            if (leadPhone.IsBest != input.IsBest || input.ShowOnLeadPage != leadPhone.IsShowOnLeadPage)
                            {
                                canContinue = false;
                                showError = true;
                            }
                        }
                    }
                    else
                    {
                        if ((existingPhoneShowOnLeadPage.Count() >= 2 && existingPhoneIsBest.Any() && input.IsBest)
                            || (existingPhoneShowOnLeadPage.Count() >= 2 && input.ShowOnLeadPage && !input.IsBest))
                        {
                            canContinue = false;
                            showError = true;
                        }
                    }
                }

                if (showError == true)
                {
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Only 2 phone numbers can be set as shown on lead page and only 1 phone numbers can be set as is best.";
                }

                if (!string.IsNullOrEmpty(input.UserId))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.UserId));
                    if (user != null)
                    {
                        input.UserName = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Invalid user.";
                }

                input.Phone = input.Phone.Replace("_", string.Empty).Replace("+", string.Empty);

                if (canContinue)
                {
                    if (input.Extension == null)
                        input.Extension = string.Empty;

                    if (status.StatusCode != "A")
                    {
                        input.ShowOnLeadPage = false;
                        input.IsBest = false;
                    }


                    if (leadPhone != null)
                    {
                        position = this.GetPositionNewLeadAndUpdateExisting(input);
                        if (position > 3 && input.IsBest)
                        {
                            input.IsBest = false;
                        }

                        if (position > 3 && input.ShowOnLeadPage)
                        {
                            input.ShowOnLeadPage = false;
                        }

                        if (position >= 2 && !input.ShowOnLeadPage)
                        {
                            input.ShowOnLeadPage = false;
                            position = 4;
                        }

                        if (position <= 3 && status.StatusCode != "A")
                        {
                            position = 4;
                            input.IsBest = false;
                            input.ShowOnLeadPage = false;
                        }

                        leadPhone.UpdatePhone(input, position, lead, phoneType, status);
                        this.repository.Update(leadPhone);
                    }
                    else
                    {
                        position = this.GetPositionNewLeadAndUpdateExisting(input);

                        if (position > 3 && input.IsBest)
                        {
                            input.IsBest = false;
                        }

                        if (position > 3 && input.ShowOnLeadPage)
                        {
                            input.IsBest = false;
                        }

                        if (position <= 3 && status.StatusCode != "A")
                        {
                            position = 4;
                        }

                        leadPhone = new LeadPhone(Guid.NewGuid(), input.Phone, input.Extension, input.IsForeignPhone,
                            position, input.UserName,
                            DateTime.Now, phoneType, lead, status, input.IsBest, input.ShowOnLeadPage);
                        this.repository.SaveOnly(leadPhone);
                    }

                    response.ResponseCode = (int)HttpStatusCode.OK;
                }

                return response;
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        public Response<LeadContactsOutputModel> DeleteLeadPhone(LeadContactInputModel input)
        {
            Response<LeadContactsOutputModel> response = new Response<LeadContactsOutputModel>();
            try
            {
                bool canContinue = true;
                if (!string.IsNullOrEmpty(input.UserId))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.UserId));
                    if (user != null)
                    {
                        input.UserName = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Invalid user.";
                }

                if (string.IsNullOrEmpty(input.ID))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead Phone must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.LeadId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead must be Provided!";
                }

                if (canContinue)
                {
                    var leadPhone = this.repository.Query<LeadPhone>()
                            .FirstOrDefault(
                                x => x.ID == Guid.Parse(input.ID) && x.NewLeadObj.ID == Guid.Parse(input.LeadId));
                    this.repository.Delete(leadPhone);
                    response.ResponseCode = (int)HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        private int GetPositionNewLeadAndUpdateExisting(LeadContactInputModel input)
        {
            var existingPhone = this.repository.Query<LeadPhone>().Where(x => x.NewLeadObj.ID == Guid.Parse(input.LeadId)).AsQueryable();
            int position = 0;
            if (existingPhone.Any())
            {
                if (input.IsBest)
                {
                    position = (int)LeadPhone.ELeadPhonePosition.IsBest;
                }
                else if (input.ShowOnLeadPage)
                {
                    position = (int)LeadPhone.ELeadPhonePosition.ShowOnLeadPage;
                    Guid id = Guid.Empty;
                    if (!string.IsNullOrEmpty(input.ID))
                    {
                        id = Guid.Parse(input.ID);
                    }

                    if (existingPhone.Where(x => x.ID != id).Count(x => x.Position == position) > 0)
                    {
                        position = (int)LeadPhone.ELeadPhonePosition.ShowOnLeadPageThird;
                        if (existingPhone.Where(x => x.ID != id).Count(x => x.Position == position) > 0)
                        {
                            position = existingPhone.Max(x => x.Position) + 1;
                        }
                    }
                }
                else
                {
                    position = existingPhone.Max(x => x.Position) + 1;
                }
            }
            else
            {
                if (input.IsBest)
                {
                    position = (int)LeadPhone.ELeadPhonePosition.IsBest;
                }
                else if (input.ShowOnLeadPage)
                {
                    position = (int)LeadPhone.ELeadPhonePosition.ShowOnLeadPage;
                }
            }

            return position;
        }

        /// <summary>
        /// Create or Update Email. Use in Other Contact Page
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        public Response<LeadContactsOutputModel> CreateOrUpdateLeadEmail(LeadContactInputModel input)
        {
            Response<LeadContactsOutputModel> response = new Response<LeadContactsOutputModel>();
            try
            {
                bool canContinue = true;
                if (!string.IsNullOrEmpty(input.UserId))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.UserId));
                    if (user != null)
                    {
                        input.UserName = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Invalid user.";
                }

                if (string.IsNullOrEmpty(input.LeadId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.Email))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead Email is required!";
                }
                else if (!FormatValidator.IsValidEmail(input.Email.Trim()))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Invalid Email Format!";
                }

                LeadEmail leadEmail = null;

                if (!string.IsNullOrEmpty(input.ID))
                {
                    leadEmail =
                        this.repository.Query<LeadEmail>()
                            .FirstOrDefault(
                                x => x.ID == Guid.Parse(input.ID) && x.LeadObj.ID == Guid.Parse(input.LeadId));

                    if (leadEmail == null)
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Invalid Email ID!";
                    }
                }
                IQueryable<LeadEmail> existingEmailBest = null;

                if (input.IsBest)
                {
                    if (string.IsNullOrEmpty(input.ID))
                    {
                        existingEmailBest = this.repository.Query<LeadEmail>()
                                .Where(x => x.Status.StatusCode == "A" && x.LeadObj.ID == Guid.Parse(input.LeadId) && x.IsPreferred)
                                .AsQueryable();
                    }
                    else
                    {
                        existingEmailBest = this.repository.Query<LeadEmail>()
                                .Where(
                                    x =>
                                    x.Status.StatusCode == "A" &&
                                        x.LeadObj.ID == Guid.Parse(input.LeadId) && x.ID != Guid.Parse(input.ID) &&
                                        x.IsPreferred)
                                .AsQueryable();
                    }

                    if (existingEmailBest.Any())
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Only 1 email address can be set as shown on lead page and only 1 email address can be set as is best.";
                    }
                }

                if (input.ShowOnLeadPage || input.IsBest)
                {
                    IQueryable<LeadEmail> existingEmailShownOnLead;
                    if (string.IsNullOrEmpty(input.ID))
                    {
                        existingEmailShownOnLead = this.repository.Query<LeadEmail>()
                                .Where(
                                    x =>
                                    x.Status.StatusCode == "A" &&
                                        x.LeadObj.ID == Guid.Parse(input.LeadId) &&
                                        (x.IsShowOnLeadPage && x.IsPreferred == false))
                                .AsQueryable();
                    }
                    else
                    {
                        existingEmailShownOnLead = this.repository.Query<LeadEmail>()
                                .Where(
                                    x =>

                                        x.LeadObj.ID == Guid.Parse(input.LeadId) && x.ID != Guid.Parse(input.ID) &&
                                        (x.IsShowOnLeadPage && x.IsPreferred == false))
                                .AsQueryable();
                    }

                    if (leadEmail != null)
                    {
                        if ((leadEmail.IsPreferred || leadEmail.IsShowOnLeadPage)
                            && (input.IsBest || input.ShowOnLeadPage) && existingEmailShownOnLead.Any())
                        {
                            if (input.IsBest != leadEmail.IsPreferred
                                || input.ShowOnLeadPage != leadEmail.IsShowOnLeadPage)
                            {
                                if (string.IsNullOrEmpty(response.ResponseMessage) && input.ID != leadEmail.ID.ToString())
                                {
                                    canContinue = false;
                                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                                    response.ResponseMessage =
                                        "Only 1 email address can be set as shown on lead page and only 1 email address can be set as is best";
                                }
                            }
                        }
                        else if ((existingEmailBest != null && existingEmailBest.Any() && !input.IsBest && input.ShowOnLeadPage && existingEmailShownOnLead.Any())
                            || (!input.IsBest && input.ShowOnLeadPage && existingEmailShownOnLead.Any()))
                        {
                            if (input.IsBest != leadEmail.IsPreferred || input.ShowOnLeadPage != leadEmail.IsShowOnLeadPage)
                            {
                                if (string.IsNullOrEmpty(response.ResponseMessage))
                                {
                                    canContinue = false;
                                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                                    response.ResponseMessage =
                                        "Only 1 email address can be set as shown on lead page and only 1 email address can be set as is best";
                                }
                            }
                        }
                    }
                    else
                    {
                        if (existingEmailShownOnLead.Any() && !input.IsBest && input.ShowOnLeadPage)
                        {
                            canContinue = false;
                            response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                            if (string.IsNullOrEmpty(response.ResponseMessage))
                            {
                                response.ResponseMessage = "Only 1 email address can be set as shown on lead page and only 1 email address can be set as is best";
                            }
                        }
                    }
                }

                if (canContinue)
                {
                    var type = this.repository.Query<EmailType>().FirstOrDefault(x => x.ID == Guid.Parse(input.TypeId));
                    var lead = this.repository.Query<Lead>()
                        .FirstOrDefault(x => x.ID == Guid.Parse(input.LeadId));
                    var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.ID == Guid.Parse(input.StatusId));

                    if (status == null)
                    {
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Invalid Status!";

                        return response;
                    }

                    if (status.StatusCode != "A")
                    {
                        input.IsBest = false;
                        input.ShowOnLeadPage = false;
                    }

                    if (!string.IsNullOrEmpty(input.ID))
                    {
                        leadEmail.UpdateEmail(input, input.UserName, type, status);
                        this.repository.Update(leadEmail);
                    }
                    else
                    {
                        leadEmail = new LeadEmail(new Guid(), input.Email.Trim(), input.IsBest, input.ShowOnLeadPage, input.UserName, DateTime.Now, lead, type, status);

                        this.repository.SaveOnly(leadEmail);
                    }

                    response.ResponseCode = (int)HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Delete Email Use in Lead Contact Page
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Response<LeadContactsOutputModel> DeleteLeadEmail(LeadContactInputModel input)
        {
            Response<LeadContactsOutputModel> response = new Response<LeadContactsOutputModel>();
            try
            {
                bool canContinue = true;
                if (!string.IsNullOrEmpty(input.UserId))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.UserId));
                    if (user != null)
                    {
                        input.UserName = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Invalid user.";
                }

                if (string.IsNullOrEmpty(input.ID))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead Email must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.LeadId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead must be Provided!";
                }

                if (canContinue)
                {
                    var leadEmail = this.repository.Query<LeadEmail>()
                            .FirstOrDefault(
                                x => x.ID == Guid.Parse(input.ID) && x.LeadObj.ID == Guid.Parse(input.LeadId));
                    this.repository.Delete(leadEmail);
                    response.ResponseCode = (int)HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Used in Lead Contact Page 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Response<LeadContactsOutputModel> CreateOrUpdateLeadAddress(LeadAddressInputModel input)
        {
            Response<LeadContactsOutputModel> response = new Response<LeadContactsOutputModel>();
            try
            {
                bool canContinue = true;
                if (!string.IsNullOrEmpty(input.UserId))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.UserId));
                    if (user != null)
                    {
                        input.UserName = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Invalid user.";
                }

                canContinue = this.ValidateAddressInput(input, ref response);
                if (canContinue)
                {
                    LeadAddress leadAddress = null;
                    var type = this.repository.Query<AddressType>().FirstOrDefault(x => x.ID == Guid.Parse(input.TypeId));
                    var lead = this.repository.Query<Lead>()
                        .FirstOrDefault(x => x.ID == Guid.Parse(input.LeadId));
                    var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.ID == Guid.Parse(input.StatusId));
                    SystemStates state = null;
                    Country country = null;
                    County county = null;

                    if (status.StatusCode != "A")
                    {
                        input.IsMaillingAddress = false;
                        input.ShowOnLeadPage = false;
                    }

                    if (!string.IsNullOrEmpty(input.CountyId))
                        county = this.repository.Query<County>().FirstOrDefault(x => x.ID == Guid.Parse(input.CountyId));

                    if (!string.IsNullOrEmpty(input.StateId))
                        state = this.repository.Query<SystemStates>().FirstOrDefault(x => x.ID == Guid.Parse(input.StateId));

                    if (!string.IsNullOrEmpty(input.CountryId))
                        country = this.repository.Query<Country>().FirstOrDefault(x => x.ID == Guid.Parse(input.CountryId));

                    if (!string.IsNullOrEmpty(input.ID))
                    {
                        leadAddress = this.repository.Query<LeadAddress>()
                                .FirstOrDefault(
                                    x => x.ID == Guid.Parse(input.ID) && x.LeadObj.ID == Guid.Parse(input.LeadId));
                        if (leadAddress != null)
                        {
                            leadAddress.Update(input, type, status, state, country, county);
                            this.repository.UpdateAndFlush(leadAddress);
                        }
                    }
                    else
                    {
                        leadAddress = new LeadAddress(
                            type,
                            input.Address1,
                            input.Address2,
                            input.AddressApto ?? string.Empty,
                            input.City,
                            state,
                            input.ZipCode,
                            country,
                            status,
                            input.IsMaillingAddress,
                            input.ShowOnLeadPage,
                            input.UserName,
                            lead,
                            input.IsInternational,
                            input.State,
                            county,
                            input.IsInternational ? input.County : null,
                            input.IsInternational ? input.Country : null);
                        this.repository.SaveAndFlush(leadAddress);
                    }

                    response.ResponseCode = (int)HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Used in Lead Contact Page
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Response<LeadContactsOutputModel> DeleteLeadAdddress(LeadContactInputModel input)
        {
            Response<LeadContactsOutputModel> response = new Response<LeadContactsOutputModel>();
            try
            {
                bool canContinue = true;
                if (!string.IsNullOrEmpty(input.UserId))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.UserId));
                    if (user != null)
                    {
                        input.UserName = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Invalid user.";
                }

                if (string.IsNullOrEmpty(input.ID))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead Address must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.LeadId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead must be Provided!";
                }

                if (canContinue)
                {
                    var leadAddress = this.repository.Query<LeadAddress>()
                            .FirstOrDefault(
                                x => x.ID == Guid.Parse(input.ID) && x.LeadObj.ID == Guid.Parse(input.LeadId));

                    var defa = false;
                    if (leadAddress != null)
                    {
                        defa = leadAddress.IsShowOnLeadPage;
                        this.repository.Delete(leadAddress);
                        ////this.repository.Save(leadAddress);

                        // analysis it was the default
                        if (defa)
                        {
                            var address = this.repository.Query<LeadAddress>()
                                .FirstOrDefault(x => x.LeadObj.ID == Guid.Parse(input.LeadId));
                            if (address != null)
                            {
                                address.UpdateIsShowInLeadPage(true);
                                this.repository.Update(address);
                            }

                        }
                    }

                    response.ResponseCode = (int)HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Update Lead Comment used in Other Contact Page.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Response<LeadContactsOutputModel> UpdateLeadComment(LeadContactInputModel input)
        {
            Response<LeadContactsOutputModel> response = new Response<LeadContactsOutputModel>();
            try
            {
                bool canContinue = true;
                User user = null;
                if (!string.IsNullOrEmpty(input.UserId))
                {
                    user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.UserId));
                    if (user != null)
                    {
                        input.UserName = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Invalid user.";
                }

                if (string.IsNullOrEmpty(input.LeadId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead must be Provided!";
                }

                if (canContinue)
                {
                    var lead = this.repository.Query<Lead>().FirstOrDefault(x => x.ID == Guid.Parse(input.LeadId));

                    this.UpdateInsertNoteCommentText(
                        input.Comments,
                        user,
                        lead,
                        Enumerations.EnumNotePageField.ContactComment,
                        XCommentType);

                    // lead.UpdateComments(input.Comments, input.UserName);
                    this.repository.UpdateAndFlush(lead);
                    response.ResponseCode = (int)HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        private bool ValidateAddressInput(LeadAddressInputModel input, ref Response<LeadContactsOutputModel> response)
        {
            bool canContinue = true;
            if (string.IsNullOrEmpty(input.LeadId))
            {
                canContinue = false;
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = "A Lead must be Provided!";
            }
            else if (string.IsNullOrEmpty(input.Address1))
            {
                canContinue = false;
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = "Address 1 is a required field!";
            }
            else if (string.IsNullOrEmpty(input.City))
            {
                canContinue = false;
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = "City is a required field!";
            }
            else if (!input.IsInternational)
            {
                if (string.IsNullOrEmpty(input.StateId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "State is a required field!";
                }
                else if (string.IsNullOrEmpty(input.CountryId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Country is a required field!";
                }
            }
            else
            {
                if (string.IsNullOrEmpty(input.Country))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Country is a required field!";
                }

            }

            if (string.IsNullOrEmpty(input.ZipCode))
            {
                canContinue = false;
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = "Zip Code is a required field!";
            }
            else if (!FormatValidator.IsValidZipCode(input.ZipCode.Trim(), input.IsInternational))
            {
                canContinue = false;
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = "Invalid Zip Code Format!";
            }

            if (input.ShowOnLeadPage)
            {
                IQueryable<LeadAddress> existingPhone;
                if (string.IsNullOrEmpty(input.ID))
                    existingPhone = this.repository.Query<LeadAddress>()
                            .Where(
                                x =>
                                    x.LeadObj.ID == Guid.Parse(input.LeadId) && x.IsShowOnLeadPage)
                            .AsQueryable();
                else
                    existingPhone = this.repository.Query<LeadAddress>()
                            .Where(
                                x =>
                                    x.LeadObj.ID == Guid.Parse(input.LeadId) && x.ID != Guid.Parse(input.ID) &&
                                    x.IsShowOnLeadPage)
                            .AsQueryable();

                if (existingPhone.Any())
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Only 1 address can show on Lead page.";
                }
            }

            if (input.IsMaillingAddress)
            {
                IQueryable<LeadAddress> existingPhone;
                if (string.IsNullOrEmpty(input.ID))
                    existingPhone = this.repository.Query<LeadAddress>()
                            .Where(
                                x =>
                                    x.LeadObj.ID == Guid.Parse(input.LeadId) && x.IsMailingAddress)
                            .AsQueryable();
                else
                    existingPhone = this.repository.Query<LeadAddress>()
                            .Where(
                                x =>
                                    x.LeadObj.ID == Guid.Parse(input.LeadId) && x.ID != Guid.Parse(input.ID) &&
                                    x.IsMailingAddress)
                            .AsQueryable();

                if (existingPhone.Any())
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Only 1 address can be saved as Mailing Address.";
                }
            }

            return canContinue;
        }

        /// <summary>
        /// Add or Update a Lead Contact Information
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Response<List<LeadOtherContactModel>> CreateOrUpdateOtherContact(LeadOtherContactModel input)
        {
            Response<List<LeadOtherContactModel>> response = new Response<List<LeadOtherContactModel>>();
            try
            {
                bool canContinue = true;



                if (!string.IsNullOrEmpty(input.ModUser))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.ModUser));
                    if (user != null)
                    {
                        input.ModUser = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Invalid user.";
                }

                if (string.IsNullOrEmpty(input.LeadId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "A Lead must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.ContactTypeId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Contact Type is a required Field!";
                }
                else if (string.IsNullOrEmpty(input.StatusId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Status is a required Field!";
                }
                else if (string.IsNullOrEmpty(input.RelationshipId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Relationship is a required Field!";
                }
                else if (string.IsNullOrEmpty(input.FirstName))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "First Name is a required Field!";
                }
                else if (string.IsNullOrEmpty(input.LastName))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Last Name is a required Field!";
                }
                else if (string.IsNullOrEmpty(input.OtherContactId))
                {
                    if ((input.Emails == null || input.Emails.Count == 0) &&
                        (input.Phones == null || input.Phones.Count == 0))
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                        response.ResponseMessage = "A Phone Number or Email must be provided!";
                    }
                    else
                    {
                        if (input.Phones != null && input.Phones.Count > 0)
                        {
                            foreach (var phone in input.Phones)
                            {
                                if (string.IsNullOrEmpty(phone.PhoneTypeId))
                                {
                                    canContinue = false;
                                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                    response.ResponseMessage = "Phone Type is a required Field!";
                                }
                                else if (string.IsNullOrEmpty(phone.StatusId))
                                {
                                    canContinue = false;
                                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                    response.ResponseMessage = "Phone Status is a required Field!";
                                }
                                else if (string.IsNullOrEmpty(phone.Phone))
                                {
                                    canContinue = false;
                                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                    response.ResponseMessage = "Phone is a required Field!";
                                }
                                else if (!FormatValidator.IsValidPhoneNumber(phone.Phone.Replace("_", string.Empty)))
                                {
                                    canContinue = false;
                                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                    response.ResponseMessage = "Invalid Phone Format!";
                                }
                                else if (!string.IsNullOrEmpty(phone.Extension) &&
                                         !FormatValidator.IsOnlyNumbers(phone.Extension.Replace("_", string.Empty)))
                                {
                                    canContinue = false;
                                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                    response.ResponseMessage = "Invalid Extension Format!";
                                }
                            }
                        }
                        else if (input.Emails != null && input.Emails.Count > 0)
                        {
                            foreach (var email in input.Emails)
                            {
                                if (string.IsNullOrEmpty(email.EmailTypeId))
                                {
                                    canContinue = false;
                                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                    response.ResponseMessage = "Email Type is a required Field!";
                                }
                                else if (string.IsNullOrEmpty(email.StatusId))
                                {
                                    canContinue = false;
                                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                    response.ResponseMessage = "Email Status is a required Field!";
                                }
                                else if (string.IsNullOrEmpty(email.Email))
                                {
                                    canContinue = false;
                                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                    response.ResponseMessage = "Email is a required Field!";
                                }
                                else if (!FormatValidator.IsValidEmail(email.Email))
                                {
                                    canContinue = false;
                                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                    response.ResponseMessage = "Invalid Email Format!";
                                }
                            }
                        }

                    }

                    if (input.Addresses != null && input.Addresses.Count > 0)
                    {

                        foreach (var address in input.Addresses)
                        {
                            if (string.IsNullOrEmpty(address.Address1))
                            {
                                canContinue = false;
                                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                response.ResponseMessage = "Address is a required Field!";
                            }
                            else if (string.IsNullOrEmpty(address.AddressTypeId))
                            {
                                canContinue = false;
                                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                response.ResponseMessage = "Address Type is a required Field!";
                            }
                            else if (string.IsNullOrEmpty(address.StatusId))
                            {
                                canContinue = false;
                                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                response.ResponseMessage = "Status is a required Field!";
                            }
                            else if (string.IsNullOrEmpty(address.City))
                            {
                                canContinue = false;
                                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                response.ResponseMessage = "City is a required Field!";
                            }
                            else if (string.IsNullOrEmpty(address.ZipCode))
                            {
                                canContinue = false;
                                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                response.ResponseMessage = "Zip Code is a required Field!";
                            }
                            else if (!string.IsNullOrEmpty(address.ZipCode) &&
                                     !FormatValidator.IsValidZipCode(address.ZipCode.Replace("_", string.Empty), address.IsInternational))
                            {
                                canContinue = false;
                                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                response.ResponseMessage = "Invalid Zip Code Format!";
                            }
                            else
                            {
                                if (!address.IsInternational &&
                                    string.IsNullOrEmpty(address.StateId))
                                {
                                    canContinue = false;
                                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                    response.ResponseMessage = "State is a required Field!";
                                }
                                if (string.IsNullOrEmpty(address.CountryInternational) &&
                                         string.IsNullOrEmpty(address.CountryId))
                                {
                                    canContinue = false;
                                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                                    response.ResponseMessage = "Country is a required Field!";
                                }
                            }
                        }
                    }
                }



                if (canContinue)
                {
                    bool isEditMode = !string.IsNullOrEmpty(input.OtherContactId);
                    LeadOtherContact leadOtherContact = null;

                    var lead = this.repository.Query<Lead>()
                        .FirstOrDefault(x => x.ID == Guid.Parse(input.LeadId));
                    var contactType = this.repository.Query<ContactType>().FirstOrDefault(x => x.ID == Guid.Parse(input.ContactTypeId));
                    var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.ID == Guid.Parse(input.StatusId));
                    var relationship = this.repository.Query<Relationship>().FirstOrDefault(x => x.ID == Guid.Parse(input.RelationshipId));
                    Prefix prefix = !string.IsNullOrEmpty(input.PrefixId)
                        ? this.repository.Query<Prefix>().FirstOrDefault(x => x.ID == Guid.Parse(input.PrefixId))
                        : null;
                    Suffix sufix = !string.IsNullOrEmpty(input.SufixId)
                        ? this.repository.Query<Suffix>().FirstOrDefault(x => x.ID == Guid.Parse(input.SufixId))
                        : null;

                    if (isEditMode)
                    {
                        leadOtherContact = this.repository.Query<LeadOtherContact>()
                                .FirstOrDefault(x => x.ID == Guid.Parse(input.OtherContactId));

                        if (leadOtherContact != null)
                        {
                            leadOtherContact.Update(status, relationship, contactType, input.FirstName, input.LastName,
                                input.ModUser);
                            leadOtherContact.UpdateComments(input.Comments);
                            leadOtherContact.UpdateMiddleName(input.MiddleName);
                            leadOtherContact.UpdatePrefix(prefix);
                            leadOtherContact.UpdateSufix(sufix);
                        }

                        this.repository.UpdateAndFlush(leadOtherContact);
                    }
                    else
                    {
                        leadOtherContact = new LeadOtherContact(lead, status, relationship, contactType, input.FirstName,
                            input.LastName, input.ModUser);
                        leadOtherContact.setComments(input.Comments);
                        leadOtherContact.setMiddleName(input.MiddleName);
                        leadOtherContact.setPrefix(input.PrefixId);
                        leadOtherContact.setSufix(input.SufixId);
                        if (input.Phones != null)
                        {
                            foreach (var model in input.Phones)
                            {
                                var phoneType = this.repository.Query<SyPhoneType>()
                                        .FirstOrDefault(x => x.ID == Guid.Parse(model.PhoneTypeId));
                                var phoneStatus = this.repository.Query<SyStatuses>()
                                        .FirstOrDefault(x => x.ID == Guid.Parse(model.StatusId));

                                LeadOtherContactPhone phone = new LeadOtherContactPhone(leadOtherContact, lead,
                                    phoneType,
                                    phoneStatus, model.Phone, model.Extension, model.IsForeignPhone, input.ModUser);
                                leadOtherContact.AddPhone(phone);
                            }
                        }

                        if (input.Emails != null)
                        {
                            foreach (var model in input.Emails)
                            {
                                var type = this.repository.Query<EmailType>()
                                        .FirstOrDefault(x => x.ID == Guid.Parse(model.EmailTypeId));
                                var emailStatus = this.repository.Query<SyStatuses>()
                                        .FirstOrDefault(x => x.ID == Guid.Parse(model.StatusId));

                                LeadOtherContactEmail email = new LeadOtherContactEmail(
                                    leadOtherContact, lead, type, emailStatus, model.Email, input.ModUser);

                                leadOtherContact.AddEmail(email);
                            }
                        }

                        if (input.Addresses != null)
                        {
                            foreach (var model in input.Addresses)
                            {
                                var type = this.repository.Query<AddressType>()
                                        .FirstOrDefault(x => x.ID == Guid.Parse(model.AddressTypeId));
                                var addressStatus = this.repository.Query<SyStatuses>()
                                        .FirstOrDefault(x => x.ID == Guid.Parse(model.StatusId));

                                SystemStates State = null;
                                County County = null;
                                Country Country = null;
                                if (!model.IsInternational)
                                {
                                    State = this.repository.Query<SystemStates>().FirstOrDefault(x => x.ID == Guid.Parse(model.StateId));
                                    if (!string.IsNullOrEmpty(model.CountyId))
                                    {
                                        County = this.repository.Query<County>().FirstOrDefault(x => x.ID == Guid.Parse(model.CountyId));
                                    }
                                    Country = this.repository.Query<Country>().FirstOrDefault(x => x.ID == Guid.Parse(model.CountryId));
                                }

                                LeadOtherContactAddress address = new LeadOtherContactAddress(
                                    leadOtherContact, lead, type,
                                    model.Address1, model.Address2, model.City, State, County,
                                    Country, model.ZipCode, model.IsInternational, model.IsMailingAddress,
                                    addressStatus, input.ModUser, model.StateInternational,
                                    model.CountyInternational, model.CountryInternational);

                                leadOtherContact.AddAddress(address);
                            }
                        }

                        this.repository.SaveAndFlush(leadOtherContact);
                    }

                    response.ResponseCode = (int)HttpStatusCode.OK;
                }

            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }


        public Response<List<LeadOtherContactModel>> DeleteLeadOtherContact(LeadOtherContactModel input)
        {
            Response<List<LeadOtherContactModel>> response = new Response<List<LeadOtherContactModel>>();
            try
            {
                bool canContinue = true;
                if (!string.IsNullOrEmpty(input.ModUser))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.ModUser));
                    if (user != null)
                    {
                        input.ModUser = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Invalid user.";
                }

                if (string.IsNullOrEmpty(input.OtherContactId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead Contact must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.LeadId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead must be Provided!";
                }

                if (canContinue)
                {
                    var leadOtherContact = this.repository.Query<LeadOtherContact>()
                            .FirstOrDefault(
                                x => x.ID == Guid.Parse(input.OtherContactId) && x.Lead.ID == Guid.Parse(input.LeadId));
                    this.repository.Delete(leadOtherContact);
                    response.ResponseCode = (int)HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }


        public Response<List<LeadOtherContactModel>> DeleteLeadOtherContactPhone(LeadOtherContactPhoneModel input)
        {
            Response<List<LeadOtherContactModel>> response = new Response<List<LeadOtherContactModel>>();
            try
            {
                bool canContinue = true;
                if (!string.IsNullOrEmpty(input.ModUser))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.ModUser));
                    if (user != null)
                    {
                        input.ModUser = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Invalid user.";
                }

                if (string.IsNullOrEmpty(input.OtherContactId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead Contact must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.LeadId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.OtherContactsPhoneId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Phone must be Provided!";
                }

                if (canContinue)
                {
                    var model = this.repository.Query<LeadOtherContact>()
                            .FirstOrDefault(
                                x => x.ID == Guid.Parse(input.OtherContactId) && x.Lead.ID == Guid.Parse(input.LeadId));
                    LeadOtherContactPhone item = null;
                    if (model != null)
                    {
                        item = model.Phones.FirstOrDefault(x => x.ID == Guid.Parse(input.OtherContactsPhoneId));
                        if (item != null)
                        {
                            model.Phones.Remove(item);
                            this.repository.UpdateAndFlush(model);
                        }
                    }

                    response.ResponseCode = (int)HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        public Response<List<LeadOtherContactModel>> DeleteLeadOtherContactEmail(LeadOtherContactEmailModel input)
        {
            Response<List<LeadOtherContactModel>> response = new Response<List<LeadOtherContactModel>>();
            try
            {
                bool canContinue = true;
                if (!string.IsNullOrEmpty(input.ModUser))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.ModUser));
                    if (user != null)
                    {
                        input.ModUser = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Invalid user.";
                }

                if (string.IsNullOrEmpty(input.OtherContactId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead Contact must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.LeadId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.OtherContactsEmailId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "An Email must be Provided!";
                }

                if (canContinue)
                {
                    var model = this.repository.Query<LeadOtherContact>()
                            .FirstOrDefault(
                                x => x.ID == Guid.Parse(input.OtherContactId) && x.Lead.ID == Guid.Parse(input.LeadId));
                    LeadOtherContactEmail item = null;
                    if (model != null)
                    {
                        item = model.Emails.FirstOrDefault(x => x.ID == Guid.Parse(input.OtherContactsEmailId));
                        if (item != null)
                        {
                            model.Emails.Remove(item);
                            this.repository.UpdateAndFlush(model);
                        }
                    }

                    response.ResponseCode = (int)HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        public Response<List<LeadOtherContactModel>> DeleteLeadOtherContactAddress(LeadOtherContactAddressModel input)
        {
            Response<List<LeadOtherContactModel>> response = new Response<List<LeadOtherContactModel>>();
            try
            {
                bool canContinue = true;
                if (!string.IsNullOrEmpty(input.ModUser))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.ModUser));
                    if (user != null)
                    {
                        input.ModUser = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Invalid user.";
                }

                if (string.IsNullOrEmpty(input.OtherContactId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead Contact must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.LeadId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.OtherContactsAddresesId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "An Address must be Provided!";
                }

                if (canContinue)
                {
                    var model = this.repository.Query<LeadOtherContact>()
                            .FirstOrDefault(
                                x => x.ID == Guid.Parse(input.OtherContactId) && x.Lead.ID == Guid.Parse(input.LeadId));
                    LeadOtherContactAddress item = null;
                    if (model != null)
                    {
                        item = model.Addresses.FirstOrDefault(x => x.ID == Guid.Parse(input.OtherContactsAddresesId));
                        if (item != null)
                        {
                            model.Addresses.Remove(item);
                            this.repository.UpdateAndFlush(model);
                        }
                    }

                    response.ResponseCode = (int)HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        public Response<List<LeadOtherContactModel>> UpdateLeadOtherContactsComments(LeadOtherContactModel input)
        {
            Response<List<LeadOtherContactModel>> response = new Response<List<LeadOtherContactModel>>();
            try
            {
                bool canContinue = true;
                if (!string.IsNullOrEmpty(input.ModUser))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.ModUser));
                    if (user != null)
                    {
                        input.ModUser = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "Invalid user.";
                }

                if (string.IsNullOrEmpty(input.OtherContactId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead Contact must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.LeadId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                    response.ResponseMessage = "A Lead must be Provided!";
                }

                if (canContinue)
                {
                    var model = this.repository.Query<LeadOtherContact>()
                            .FirstOrDefault(
                                x => x.ID == Guid.Parse(input.OtherContactId) && x.Lead.ID == Guid.Parse(input.LeadId));

                    if (model != null)
                    {
                        model.UpdateComments(input.Comments);
                        this.repository.UpdateAndFlush(model);

                    }

                    response.ResponseCode = (int)HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Add or Update a Lead Contact Phone
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Response<List<LeadOtherContactModel>> CreateOrUpdateOtherContactPhone(LeadOtherContactPhoneModel input)
        {
            Response<List<LeadOtherContactModel>> response = new Response<List<LeadOtherContactModel>>();
            try
            {
                bool canContinue = true;
                if (!string.IsNullOrEmpty(input.ModUser))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.ModUser));
                    if (user != null)
                    {
                        input.ModUser = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Invalid user.";
                }

                if (string.IsNullOrEmpty(input.LeadId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "A Lead must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.OtherContactId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "A Lead Contact must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.PhoneTypeId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Phone Type is a required Field!";
                }
                else if (string.IsNullOrEmpty(input.StatusId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Phone Status is a required Field!";
                }
                else if (string.IsNullOrEmpty(input.Phone))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Phone is a required Field!";
                }
                else if (!FormatValidator.IsValidPhoneNumber(input.Phone.Replace("_", string.Empty)))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Invalid Phone Format!";
                }
                else if (!string.IsNullOrEmpty(input.Extension) &&
                         !FormatValidator.IsOnlyNumbers(input.Extension.Replace("_", string.Empty)))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Invalid Extension Format!";
                }

                if (!string.IsNullOrEmpty(input.Extension))
                {
                    input.Extension = input.Extension.Replace("_", string.Empty);
                }

                input.Phone = input.Phone.Replace("_", string.Empty).Replace("+", string.Empty);

                if (canContinue)
                {
                    bool isEditMode = !string.IsNullOrEmpty(input.OtherContactsPhoneId);

                    var lead = this.repository.Query<Lead>()
                        .FirstOrDefault(x => x.ID == Guid.Parse(input.LeadId));
                    var leadOtherContact = this.repository.Query<LeadOtherContact>()
                        .FirstOrDefault(x => x.ID == Guid.Parse(input.OtherContactId));

                    var type = this.repository.Query<SyPhoneType>()
                                       .FirstOrDefault(x => x.ID == Guid.Parse(input.PhoneTypeId));
                    var status = this.repository.Query<SyStatuses>()
                            .FirstOrDefault(x => x.ID == Guid.Parse(input.StatusId));

                    LeadOtherContactPhone phone = null;

                    if (isEditMode)
                    {
                        phone = this.repository.Query<LeadOtherContactPhone>()
                                       .FirstOrDefault(x => x.ID == Guid.Parse(input.OtherContactsPhoneId));
                        if (phone != null)
                        {
                            phone.Update(type, status, input.Phone, input.Extension, input.IsForeignPhone, input.ModUser);
                            this.repository.UpdateAndFlush(phone);
                        }
                    }
                    else
                    {
                        phone = new LeadOtherContactPhone(leadOtherContact, lead, type, status, input.Phone, input.Extension, input.IsForeignPhone, input.ModUser);
                        this.repository.SaveAndFlush(phone);
                    }

                    response.ResponseCode = (int)HttpStatusCode.OK;
                }

            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }


        /// <summary>
        /// Add or Update a Lead Contact Email
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Response<List<LeadOtherContactModel>> CreateOrUpdateOtherContactEmail(LeadOtherContactEmailModel input)
        {
            Response<List<LeadOtherContactModel>> response = new Response<List<LeadOtherContactModel>>();
            try
            {
                bool canContinue = true;
                if (!string.IsNullOrEmpty(input.ModUser))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.ModUser));
                    if (user != null)
                    {
                        input.ModUser = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Invalid user.";
                }

                if (string.IsNullOrEmpty(input.LeadId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "A Lead must be Provided!";
                }
                else
                    if (string.IsNullOrEmpty(input.OtherContactId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "A Lead Contact must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.EmailTypeId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Email Type is a required Field!";
                }
                else if (string.IsNullOrEmpty(input.StatusId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Email Status is a required Field!";
                }
                else if (string.IsNullOrEmpty(input.Email))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Email is a required Field!";
                }
                else if (!FormatValidator.IsValidEmail(input.Email.Trim()))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Invalid Email Format!";
                }

                if (canContinue)
                {
                    bool isEditMode = !string.IsNullOrEmpty(input.OtherContactsEmailId);

                    var lead = this.repository.Query<Lead>()
                        .FirstOrDefault(x => x.ID == Guid.Parse(input.LeadId));
                    var leadOtherContact = this.repository.Query<LeadOtherContact>()
                        .FirstOrDefault(x => x.ID == Guid.Parse(input.OtherContactId));

                    var type = this.repository.Query<EmailType>()
                                       .FirstOrDefault(x => x.ID == Guid.Parse(input.EmailTypeId));
                    var status = this.repository.Query<SyStatuses>()
                            .FirstOrDefault(x => x.ID == Guid.Parse(input.StatusId));

                    LeadOtherContactEmail model = null;

                    if (isEditMode)
                    {
                        model = this.repository.Query<LeadOtherContactEmail>()
                                       .FirstOrDefault(x => x.ID == Guid.Parse(input.OtherContactsEmailId));
                        if (model != null)
                        {
                            model.Update(type, status, input.Email, input.ModUser);
                            this.repository.UpdateAndFlush(model);
                        }
                    }
                    else
                    {
                        model = new LeadOtherContactEmail(leadOtherContact, lead, type, status, input.Email, input.ModUser);
                        this.repository.SaveAndFlush(model);
                    }

                    response.ResponseCode = (int)HttpStatusCode.OK;
                }

            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }


        /// <summary>
        /// Add or Update a Lead Contact Address
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Response<List<LeadOtherContactModel>> CreateOrUpdateOtherContactAddress(LeadOtherContactAddressModel input)
        {
            Response<List<LeadOtherContactModel>> response = new Response<List<LeadOtherContactModel>>();
            try
            {
                bool canContinue = true;
                if (!string.IsNullOrEmpty(input.ModUser))
                {
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.ModUser));
                    if (user != null)
                    {
                        input.ModUser = user.UserName;
                    }
                    else
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                        response.ResponseMessage = "Invalid user.";
                    }
                }
                else
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Invalid user.";
                }

                if (string.IsNullOrEmpty(input.LeadId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "A Lead must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.OtherContactId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "A Lead Contact must be Provided!";
                }
                else if (string.IsNullOrEmpty(input.AddressTypeId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Address Type is a required Field!";
                }
                else if (string.IsNullOrEmpty(input.StatusId))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Address Status is a required Field!";
                }
                else if (string.IsNullOrEmpty(input.Address1))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Address is a required Field!";
                }
                else if (string.IsNullOrEmpty(input.City))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "City is a required Field!";
                }
                else if (string.IsNullOrEmpty(input.ZipCode))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Zip Code is a required Field!";
                }
                else if (!string.IsNullOrEmpty(input.ZipCode) &&
                         !FormatValidator.IsValidZipCode(input.ZipCode, input.IsInternational))
                {
                    canContinue = false;
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    response.ResponseMessage = "Invalid Zip Code Format!";
                }
                else
                {
                    if (!input.IsInternational &&
                        string.IsNullOrEmpty(input.StateId))
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                        response.ResponseMessage = "State is a required Field!";
                    }

                    if (string.IsNullOrEmpty(input.CountryInternational) &&
                             string.IsNullOrEmpty(input.CountryId))
                    {
                        canContinue = false;
                        response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                        response.ResponseMessage = "Country is a required Field!";
                    }
                }

                if (canContinue)
                {
                    bool isEditMode = !string.IsNullOrEmpty(input.OtherContactsAddresesId);

                    var lead = this.repository.Query<Lead>()
                        .FirstOrDefault(x => x.ID == Guid.Parse(input.LeadId));
                    var leadOtherContact = this.repository.Query<LeadOtherContact>()
                        .FirstOrDefault(x => x.ID == Guid.Parse(input.OtherContactId));

                    var type = this.repository.Query<AddressType>()
                                       .FirstOrDefault(x => x.ID == Guid.Parse(input.AddressTypeId));
                    var status = this.repository.Query<SyStatuses>()
                            .FirstOrDefault(x => x.ID == Guid.Parse(input.StatusId));

                    LeadOtherContactAddress model = null;

                    SystemStates State = null;
                    County County = null;
                    Country Country = null;
                    if (!input.IsInternational)
                    {
                        State = this.repository.Query<SystemStates>().FirstOrDefault(x => x.ID == Guid.Parse(input.StateId));
                        Country = this.repository.Query<Country>().FirstOrDefault(x => x.ID == Guid.Parse(input.CountryId));
                        if (!string.IsNullOrEmpty(input.CountyId))
                            County = this.repository.Query<County>().FirstOrDefault(x => x.ID == Guid.Parse(input.CountyId));
                    }


                    if (isEditMode)
                    {
                        model = this.repository.Query<LeadOtherContactAddress>()
                                       .FirstOrDefault(x => x.ID == Guid.Parse(input.OtherContactsAddresesId));
                        if (model != null)
                        {
                            model.Update(type, status, input.Address1, input.Address2, input.City, State, County,
                                Country, input.ZipCode, input.IsInternational, input.IsMailingAddress, input.ModUser,
                                input.StateInternational, input.CountyInternational, input.CountryInternational);
                            this.repository.UpdateAndFlush(model);
                        }
                    }
                    else
                    {
                        model = new LeadOtherContactAddress(
                                    leadOtherContact, lead, type,
                                    input.Address1, input.Address2, input.City, State, County,
                                  Country, input.ZipCode, input.IsInternational, input.IsMailingAddress,
                                    status, input.ModUser, input.StateInternational,
                                    input.CountyInternational, input.CountryInternational);

                        this.repository.SaveAndFlush(model);
                    }

                    response.ResponseCode = (int)HttpStatusCode.OK;
                }

            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Get the list of campus by User Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>a list of ID Description</returns>
        private IEnumerable<Guid> GetCampusItemsByUserId(Guid userId)
        {
            if (userId == Guid.Empty) return null;

            var listItems = this.repository.Query<User>().Where(n => n.ID == userId).SelectMany(n => n.CampusGroup)
                .SelectMany(y => y.Campuses).Where(y => y.Status.StatusCode == "A").Select(l => l.ID).ToList();

            // .SelectMany(y => y.Description).Distinct();
            return listItems;
        }


        private string StudentImagePath
        {
            get
            {
                return string.IsNullOrEmpty(this.studentImagePath)
                    ? this.GetConfigurationSetting("STUDENTIMAGEPATH")
                    : this.studentImagePath;
            }
        }

        /// <summary>
        /// Gets the advantage site URL.
        /// </summary>
        private string AdvantageSiteUrl
        {
            get
            {
                return string.IsNullOrEmpty(this.studentImagePath)
                    ? this.GetConfigurationSetting("ADVANTAGESITEURI")
                    : this.studentImagePath;
            }
        }

        private string GetConfigurationSetting(string keyName)
        {
            string retValue = null;
            var configurationAppSetting = this.repositoryWithInt.Query<ConfigurationAppSetting>()
                    .FirstOrDefault(n => n.KeyName.ToUpper() == keyName.ToUpper());
            if (configurationAppSetting != null) retValue = configurationAppSetting.ConfigurationValues[0].Value;

            return retValue;
        }

        /// <summary>
        ///  The fill mru output.
        /// </summary>
        /// <param name="leads">
        ///  The leads.
        /// </param>
        /// <param name="mruList">
        ///  The mru list.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        private IEnumerable<LeadMruOutputModel> FillMruOutput(IEnumerable<Lead> leads, IEnumerable<MruOutput> mruList)
        {
            var mruOutput = new List<LeadMruOutputModel>();

            foreach (var s in leads)
            {
                Lead s1 = s;
                if (mruOutput.All(n => n.Id != s1.ID))
                {
                    var imgObject = s1.ImageList64.FirstOrDefault(x => x.Type == 1);
                    string image64;
                    if (imgObject == null)
                    {
                        image64 = @"data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAEsAAABLCAIAAAC3LO29AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAiESURBVHhe3Vv3V1RHFLa32GJijJrkaIrR5KScREVBRFFEooKFqLGXKL1JU0BEDKIICIoiAgIqKiogAoKgossiUpe6LM0FFlg6WP6AfGZPzJ5F3PfmzS5Gzh5+ujPvfnP7nTsDO5+9HPB+/wHh+/0boGp4HT1vPsGOnhctnc/wX9UMqBBhe8+Lgor6a2l5Men5d7JKc8qe5gnrHuSJkvklmYJqUV1LZOLjlOyy9BxhWW0TiFUEVVUIxc0daU+E+4Pil5ifXGoRsMYxeI/XJVPvK5tcw1c7BG/3iHQ+Faf9p6+xc8iGA6EOATeDYx/eflQsFDdTh0oZYcezl61dz7KKa0Nv8dcfCJ1i4DRgtonS3xANs0nLHJaaB/heSq9qaG3rfk5RnjQRwqia2rtzy8VG9mc/WrJv0BxTpdjkCQbNNf1U3zEsgV9Y2dDY1k3LRGkiBFup2eU6Jn4jtSwHMhBdb/yD5piMWmAFYd5Iz6+XdrZ3UzBOOghx3tmlT12DE37Y6AkWB7KUnjxUrP1A23raKhc9i0C/6HTg5KixFBC2db24eb9gq/uF6YauQ+eZs9LMvoiBc7S2tbFTCDxwPyOE64t9UGjsfO4TPQcq2OQ3+Xad+9ELd/4Jm+RpCScZAh78+ya3sI+X2lOHhw1Halkh0lTWt3AJIZwQSjt6whP4X692UwU82Z4zjQ8l8UsQgZjrqoLAyRHC/MpqmgysT43VsVEdwknLHO38r8NLM0eoQPkGhAyVvrS2yfpEzJiF1gPnKI/pxEcweqH1ErOTdc0d5AgZ4lH4gKS168a9gikGzmzDOluocM5QVCQ6xAkAoZaW1DR6hiYhC2HLMVt6fGLyciekgdLOHjIxEmopigNkz2zZJaMfv9g2OiW3TkqoqIQI72SVwTzIOGa7Cqboe/lVRk5Nhkw2unG/4PsNh9nySkaPJM4jNFFUJ2XCWG8aQjuMTs35fMV+Mo7ZrkIev/vIxeKqRhn3bF0jIcKrd/O+NFJhoJc/BbjTBbt9npQSJqiECBN5xZq7fNhKg4x+2HwLfctTeeVitWppyuOyperyNMM1LTfsDxWI6tWKELmizh5fMpmwXTVC03L7ociiyga1Ikx4VDRv5zG2vJLRj9Cy3OUJTyNRK8LEzGKt3WqyQ8hwm3uEoFK9CNH/1DXzJ5MJ21XD51ugtYXWq1plmJZTsdLuDFteyegRLZA/5ZSp15fyCqu3HLzApePEHC1kuM7pXIGcL2UV9AnjYXVDa1g8f+Y6dzRzmfNKQInic4yOjXdECnoZatVSdDIFogbTo9Eo8FVaAQ+bbz5jnfvDgsrmDnrVE8OjamjpissQ/LrVC3kjgXAYLhm3yHat4zkuXVNCLZWdAnr47iG3pxo4M2SXgGzqb86eYclNdPs0DGUIstau52jjz/rdg4B1hktmrD0Ye78AJsicK+WdKOZ7oXdSLWlD6B+iQafVrQB7hKbFYlP/2sa29v7qCL8SY+dzhA3cNDGUCSsyuLE1DsEc72c42aFM2ufjM3/Z4kW96TZBd5/WLp/DoUnMdeqNlBQQltY02gfcnG7kyko+byeGf15lFxQSy+MJqvofIThIzxUa7w8ZNJda9Id/9o9OJ+6Ryh8KBRliu+LqRrNj0bSu1iDen/74K/w2n6P0ZMvpIIzLKFxuFUh27/tGddU19UdPvR8Q9pXyImcMup7x8+YjVDK46YYuFj5XM/Ir+wFhX5/ECEb502aA1Njpzd3foJK4lpaPMNj/CBVEiss276hUtKiJIwfKMSQPx6NScS/CMQy+Ph1OdqiAEEnco8KqHzd6Ersc2U0TGnnElURvsXNC2Hu74moJRp7Iqg3YMCoJx8DYCtIGvqoivvy+RVUSdDfIEA7WMJtu5JZbXocrGCoWSDNavGZIUNmwzCoQ3TECfzNmoY2eZYCklesADc3aovdJoyOms9cPnRUChEjfN7uFU7RAFjJk3vnJF9bN3uo9jGhuaKKew16vS5jvoKii1HIaGU9II3FDNMv4EFl7CkM5Ow9HYvzy3UWICMYrrPrMwHkwUQo+dpHNCtvTaP+8uwhxSZuSVTZa24Ys4g+db/7deo9coZhLz4JdPGRufrJ9MRcccTuLwMe8XjJOx+ZIWBISQIYJDRMOaUb8rOKabYciuCBEhTlRz/5Y5F2MI9HSVWoIRfVSn4t3J+szGnt+yynAhjEhgKuY83GZQrEUOT1HqBQQQlXKxdKTV+7N3e49mNIMEWZo0PvZeTgqJI5Xgn5eF/nkN1eEbd0v7ueJ3IITAA/X0VxUVGEtpqE+1LXDtiZHL5+OeZCWUy6qbyFwQuQIca6Y4onPEOCkv1h5gMx/Kj0R1FPIkDBKu9ElFM3vyKTHSZklPEE1EmCM8zGRLQlCVEm1je2ZRdWnYjJwxhjsVsooFQLUVnjeoLnr+Ca3cNezCfDbeNKRJxRXiKWYJOyrbcUOoewpD84v6PrDRXv9yCI7FbSyTSbo2i3cc8L5dOytR0U1kjbkQ5i1xUCxvH9ihxAzz5ifX2zqN0HXHmMuFHkl2wqmATYw8z5luZPGjmPWvjFRydl5FXUSuecayhHiBQvsLYFX5HImfpllwFerXbGjem5/mcOGW0JROknf8Zu1B2E4hvvOHAiKT+SXII/vEyGyikJRA95leZxP3HwwXHvPCbTARi2wVJFHYQ5GKSVsBzKYZuiibxUIBVZECO8EhU7ml/pH34Ob1jU7iVcUZBWtUlZUTTBc0wJTzP8hhNBwO383u9zvcjp6LZjMVdGdmaqBKe4vy4lQWRdVSs7F8gysT6vN+6sJKuDhXUGmoAYznKiy1fRVondfhLwBYXRKzmIz//GLbPs9vhFiePt5IbWdt+M4+lzvWgCgh3br0f+pq2R6BO+v6P59ycP0JNTpG+h+671H+DcL5YizGcw8pwAAAABJRU5ErkJggg==";
                    }
                    else
                    {
                        image64 = string.Format("data:{0};base64,{1}", imgObject.MediaType, Convert.ToBase64String(imgObject.Image));
                    }
                    var mru = new LeadMruOutputModel
                    {
                        Id = s1.ID,
                        Campus = s1.Campus != null ? s1.Campus.Description : "None",
                        FullName = s1.FullName,
                        SSN = string.IsNullOrEmpty(s1.SSN) ? "None" : s1.SSN,
                        StartDate = s1.ExpectedStart.ToString(),
                        Status = s1.LeadStatus == null ? "None" : s1.LeadStatus.Description,
                        Program = s1.ProgramVersion == null ? "None" : s1.ProgramVersion.Description,
                        Image64 = image64,
                        ImageUrl = s1.ImageUrl,
                        NotFoundImageUrl = s1.NotFoundImageUrl,
                        MruModDate = s1.ModDate.ToString(),
                        ModDate = s1.ModDate
                    };

                    if (s1.Campus != null)
                        mru.SearchCampusId = s1.Campus.ID;

                    if (mru.Program.Length > 30)
                        mru.Program = s1.ProgramVersion.Description.Substring(0, 27) + "...";

                    if (mruList != null)
                    {
                        var mruL = mruList.ToList();

                        var mruItem = mruL.FirstOrDefault(n => n.EntityId == s.ID);
                        if (mruItem != null)
                        {
                            mru.MruModDate = mruItem.ModDate.ToString();
                            mru.ModDate = mruItem.ModDate;
                        }
                    }

                    mruOutput.Add(mru);
                }
            }

            mruOutput = mruOutput.Distinct().OrderByDescending(n => n.ModDate).ToList();

            return mruOutput.Take(5);
        }
    }
}
