﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionBo.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.InstitutionBo
// </copyright>
// <summary>
//   The institution (also known as HighSchool) business object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;

    using AutoMapper;

    using Domain.Infrastructure.Entities;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.Catalogs;
    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Domain.SystemStuff.Intitution;
    using FAME.Advantage.Domain.Users;
    using FAME.Advantage.Services.Lib.Utils;

    using Messages.SystemStuff.Institution;
    using Messages.Web;

    /// <summary>
    /// The institution (also known as HighSchool) business object.
    /// </summary>
    public class InstitutionBo
    {
        /// <summary>
        /// The NHibernate repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The repository with type id.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithTypeId;

        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionBo"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="repositoryWithTypeId">
        /// The repository with type id.
        /// </param>
        public InstitutionBo(IRepository repository, IRepositoryWithTypedID<int> repositoryWithTypeId)
        {
            this.repository = repository;
            this.repositoryWithTypeId = repositoryWithTypeId;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="InstitutionOutputModel"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public InstitutionOutputModel Get(InstitutionFilterInputModel input)
        {
            if (input == null)
            {
                throw new Exception("Filter can't be null");
            }

            if (input.CampusId == Guid.Empty)
            {
                throw new Exception("Campus can't be empty");
            }

            if (input.UserId == Guid.Empty)
            {
                throw new Exception("User can't be empty");
            }

            if (input.Id == Guid.Empty)
            {
                throw new Exception("Institution Id can't be empty");
            }

            var campusGroup = this.repository.Query<CampusGroup>().FirstOrDefault(x => x.Campuses.Any(c => c.ID == input.CampusId));

            if (campusGroup == null)
            {
                throw new Exception("Campus was not found");
            }

            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

            if (user == null)
            {
                throw new Exception("User Acount was not found");
            }

            HighSchools institution =
                this.repository.Query<HighSchools>()
                    .FirstOrDefault(x => x.ID == input.Id && x.CampusGroupObj.Campuses.Any(c => c.ID == input.CampusId) && x.SyStatusesObj.StatusCode == "A");
            if (institution != null)
            {
                return Mapper.Map<InstitutionOutputModel>(institution);
            }

            return null;
        }

        /// <summary>
        /// The find.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public List<InstitutionOutputModel> Find(InstitutionFilterInputModel input)
        {
            if (input == null)
            {
                throw new Exception("Filter can't be null");
            }

            if (input.CampusId == Guid.Empty)
            {
                throw new Exception("Campus can't be empty");
            }

            if (input.UserId == Guid.Empty)
            {
                throw new Exception("User can't be empty");
            }

            if (input.InstitutionType < 1 || input.InstitutionType >= 3)
            {
                throw new Exception("Institution Type is a required field!");
            }

            if (input.filter == null)
            {
                throw new Exception("Enter a name to search for an institution");
            }

            if (input.filter.filters == null)
            {
                throw new Exception("Enter a name to search for an institution");
            }

            if (input.filter.filters.Count == 0)
            {
                throw new Exception("Enter a name to search for an institution");
            }

            var campusGroup = this.repository.Query<CampusGroup>().FirstOrDefault(x => x.Campuses.Any(c => c.ID == input.CampusId));

            if (campusGroup == null)
            {
                throw new Exception("Campus was not found");
            }

            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

            if (user == null)
            {
                throw new Exception("User Acount was not found");
            }

            string value = input.filter.filters.FirstOrDefault().value;

            if (string.IsNullOrEmpty(value))
            {
                return new List<InstitutionOutputModel>();
            }

            if (value.Length < 3)
            {
                return new List<InstitutionOutputModel>();
            }

            var institutions =
                this.repository.Query<HighSchools>()
                    .Where(x => x.CampusGroupObj.Campuses.Any(c => c.ID == input.CampusId) && x.SyStatusesObj.StatusCode == "A"
                    && x.ImportType.ID == input.InstitutionType
                    && (x.HsDescrip.ToLower().StartsWith(value.ToLower()) || x.HsDescrip.ToLower().EndsWith(value.ToLower()) || x.HsDescrip.ToLower().Contains(value.ToLower()))).OrderBy(x => x.HsDescrip).AsEnumerable();

            if (input.Level > 0)
            {
                institutions = institutions.Where(x => x.Level.ID == input.Level);
            }

            var output = institutions.Select(r =>
            {
                var outputModel = new InstitutionOutputModel();
                return Mapper.Map(r, outputModel);
            });

            return output.ToList();
        }

        /// <summary>
        /// The exist by import type (call this to get a flag whether they are institutions with the given TypeId passed in the input).
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ExistByImportType(InstitutionFilterInputModel input)
        {
            if (input == null)
            {
                throw new Exception("Filter can't be null");
            }

            if (input.CampusId == Guid.Empty)
            {
                throw new Exception("Campus can't be empty");
            }

            if (input.UserId == Guid.Empty)
            {
                throw new Exception("User can't be empty");
            }

            if (input.InstitutionType < 1 || input.InstitutionType >= 3)
            {
                throw new Exception("Institution Type is a required field!");
            }

            var campusGroup = this.repository.Query<CampusGroup>().FirstOrDefault(x => x.Campuses.Any(c => c.ID == input.CampusId));

            if (campusGroup == null)
            {
                throw new Exception("Campus was not found");
            }

            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

            if (user == null)
            {
                throw new Exception("User Acount was not found");
            }

            return this.repository.Query<HighSchools>().Any(x => x.ImportType.ID == input.InstitutionType && x.CampusGroupObj.Campuses.Any(c => c.ID == input.CampusId) && x.SyStatusesObj.StatusCode == "A");
        }

        /// <summary>
        /// The exist by name (call this to get a flag whether they are institutions with the given name passed in the input).
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool GetExistByName(InstitutionFilterInputModel input)
        {
            if (input == null)
            {
                throw new Exception("Filter can't be null");
            }

            if (input.CampusId == Guid.Empty)
            {
                throw new Exception("Campus can't be empty");
            }

            if (input.UserId == Guid.Empty)
            {
                throw new Exception("User can't be empty");
            }

            if (input.InstitutionType < 1 || input.InstitutionType >= 3)
            {
                throw new Exception("Institution Type is a required field!");
            }

            var campusGroup = this.repository.Query<CampusGroup>().FirstOrDefault(x => x.Campuses.Any(c => c.ID == input.CampusId));

            if (campusGroup == null)
            {
                throw new Exception("Campus was not found");
            }

            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

            if (user == null)
            {
                throw new Exception("User Acount was not found");
            }

            return this.repository.Query<HighSchools>().Any(x => x.HsDescrip.ToLower().Trim() == input.Name.ToLower().Trim() && x.Level.ID == input.Level && x.CampusGroupObj.Campuses.Any(c => c.ID == input.CampusId) && x.SyStatusesObj.StatusCode == "A");
        }

        /// <summary>
        /// The find phone.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The List of InstitutionPhoneModel
        /// </returns>
        /// <exception cref="Exception">
        /// An un-handle exception
        /// </exception>
        public List<InstitutionPhoneModel> FindPhone(InstitutionPhoneModel input)
        {
            if (input == null)
            {
                throw new Exception("Filter can't be null");
            }

            if (input.CampusId == Guid.Empty)
            {
                throw new Exception("Campus can't be empty");
            }

            if (input.UserId == Guid.Empty)
            {
                throw new Exception("User can't be empty");
            }

            if (input.InstitutionId == null)
            {
                throw new Exception("Institution Id is a required field");
            }

            var campusGroup = this.repository.Query<CampusGroup>().FirstOrDefault(x => x.Campuses.Any(c => c.ID == input.CampusId));

            if (campusGroup == null)
            {
                throw new Exception("Campus was not found");
            }

            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

            if (user == null)
            {
                throw new Exception("User Acount was not found");
            }

            var institution = this.repository.Query<HighSchools>()
                    .FirstOrDefault(x => x.CampusGroupObj.Campuses.Any(c => c.ID == input.CampusId) && x.SyStatusesObj.StatusCode == "A" && x.ID == input.InstitutionId);

            if (institution != null)
            {
                var output = institution.Phones.Where(x => x.Status.StatusCode == "A").Select(
                    r =>
                        {
                            var outputModel = new InstitutionPhoneModel();
                            return Mapper.Map(r, outputModel);
                        }).ToList();
                return output;
            }

            return new List<InstitutionPhoneModel>();
        }

        /// <summary>
        /// The find address.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The List of InstitutionAddressModel
        /// </returns>
        /// <exception cref="Exception">
        /// An un-handle exception
        /// </exception>
        public List<InstitutionAddressModel> FindAddress(InstitutionAddressModel input)
        {
            if (input == null)
            {
                throw new Exception("Filter can't be null");
            }

            if (input.CampusId == Guid.Empty)
            {
                throw new Exception("Campus can't be empty");
            }

            if (input.UserId == Guid.Empty)
            {
                throw new Exception("User can't be empty");
            }

            if (input.InstitutionId == null)
            {
                throw new Exception("Institution Id is a required field");
            }

            var campusGroup = this.repository.Query<CampusGroup>().FirstOrDefault(x => x.Campuses.Any(c => c.ID == input.CampusId));

            if (campusGroup == null)
            {
                throw new Exception("Campus was not found");
            }

            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

            if (user == null)
            {
                throw new Exception("User Acount was not found");
            }

            var institution = this.repository.Query<HighSchools>()
                    .FirstOrDefault(x => x.CampusGroupObj.Campuses.Any(c => c.ID == input.CampusId) && x.SyStatusesObj.StatusCode == "A" && x.ID == input.InstitutionId);

            if (institution != null)
            {
                var output = institution.Addresses.Where(x => x.Status.StatusCode == "A").Select(
                    r =>
                    {
                        var outputModel = new InstitutionAddressModel();
                        return Mapper.Map(r, outputModel);
                    }).ToList();
                return output;
            }

            return new List<InstitutionAddressModel>();
        }

        /// <summary>
        /// The find contact.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The List of InstitutionContactModel
        /// </returns>
        /// <exception cref="Exception">
        /// An un-handle exception
        /// </exception>
        public List<InstitutionContactModel> FindContact(InstitutionContactModel input)
        {
            if (input == null)
            {
                throw new Exception("Filter can't be null");
            }

            if (input.CampusId == Guid.Empty)
            {
                throw new Exception("Campus can't be empty");
            }

            if (input.UserId == Guid.Empty)
            {
                throw new Exception("User can't be empty");
            }

            if (input.InstitutionId == null)
            {
                throw new Exception("Institution Id is a required field");
            }

            var campusGroup = this.repository.Query<CampusGroup>().FirstOrDefault(x => x.Campuses.Any(c => c.ID == input.CampusId));

            if (campusGroup == null)
            {
                throw new Exception("Campus was not found");
            }

            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

            if (user == null)
            {
                throw new Exception("User Acount was not found");
            }

            var institution = this.repository.Query<HighSchools>()
                    .FirstOrDefault(x => x.CampusGroupObj.Campuses.Any(c => c.ID == input.CampusId) && x.SyStatusesObj.StatusCode == "A" && x.ID == input.InstitutionId);

            if (institution != null)
            {
                var output = institution.Contacts.Where(x => x.Status.StatusCode == "A").Select(
                    r =>
                    {
                        var outputModel = new InstitutionContactModel();
                        return Mapper.Map(r, outputModel);
                    }).ToList();
                return output;
            }

            return new List<InstitutionContactModel>();
        }

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public Response<string> Insert(InstitutionInputModel input)
        {
            Response<string> response = new Response<string>();
            response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;

            if (input == null)
            {
                response.ResponseMessage = "Filter can't be null";
                return response;
            }

            if (input.CampusId == Guid.Empty)
            {
                response.ResponseMessage = "Campus can't be empty";
                return response;
            }

            if (input.UserId == Guid.Empty)
            {
                response.ResponseMessage = "User can't be empty";
                return response;
            }

            if (string.IsNullOrEmpty(input.Name))
            {
                response.ResponseMessage = "Institution Name can't be empty";
                return response;
            }

            var campusGroup = this.repository.Query<CampusGroup>().FirstOrDefault(x => x.Campuses.Any(c => c.ID == input.CampusId));

            if (campusGroup == null)
            {
                response.ResponseMessage = "Campus was not found";
                return response;
            }

            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

            if (user == null)
            {
                response.ResponseMessage = "User Acount was not found";
                return response;
            }

            var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode == "A");
            var level = this.repositoryWithTypeId.Query<AdLevels>().FirstOrDefault(x => x.ID == input.LevelId && x.StatusObj.StatusCode == "A");
            var importType = this.repositoryWithTypeId.Query<InstitutionImportType>().FirstOrDefault(x => x.ID == input.TypeId);

            HighSchools highSchools = new HighSchools(string.Empty, input.Name, user.UserName, DateTime.Now, status, campusGroup, level, null, importType);

            this.repository.Save(highSchools);

            response.ResponseCode = (int)HttpStatusCode.OK;

            return response;
        }

        /// <summary>
        /// The insert phone.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public Response<string> InsertPhone(InstitutionPhoneModel input)
        {
            Response<string> response = this.ValidateInsertPhone(input);
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
                var highSchools =
                    this.repository.Query<HighSchools>()
                        .FirstOrDefault(x => x.ID == input.InstitutionId && x.SyStatusesObj.StatusCode == "A");

                if (highSchools == null)
                {
                    response.ResponseMessage = "Institution was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

                if (user == null)
                {
                    response.ResponseMessage = "User Acount was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode == "A");
                var type = this.repository.Query<SyPhoneType>().FirstOrDefault(x => x.ID == input.TypeId && x.SyStatusesObj.StatusCode == "A");

                InstitutionPhone institution = new InstitutionPhone(highSchools, type, input.Phone.Replace("_", string.Empty), DateTime.Now, user.UserName, input.IsForeignPhone, status, null);
                this.repository.Save(institution);

                response.ResponseMessage = "The phone was inserted!";
            }

            return response;
        }

        /// <summary>
        /// The delete phone.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public Response<string> DeletePhone(InstitutionPhoneModel input)
        {
            Response<string> response = this.ValidateDeletePhone(input);
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
                var highSchools =
                    this.repository.Query<HighSchools>()
                        .FirstOrDefault(x => x.ID == input.InstitutionId && x.SyStatusesObj.StatusCode == "A");

                if (highSchools == null)
                {
                    response.ResponseMessage = "Institution was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

                if (user == null)
                {
                    response.ResponseMessage = "User Acount was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode == "I");

                InstitutionPhone phone =
                    this.repository.Query<InstitutionPhone>()
                        .FirstOrDefault(x => x.Institution.ID == highSchools.ID && x.ID == input.Id && x.Status.StatusCode == "A");

                if (phone == null)
                {
                    response.ResponseMessage = "Phone was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                phone.Delete(user.UserName, status);

                this.repository.Save(phone);

                response.ResponseMessage = "The phone was deleted!";
            }

            return response;
        }

        /// <summary>
        /// The update phone.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public Response<string> UpdatePhone(InstitutionPhoneModel input)
        {
            Response<string> response = this.ValidateUpdatePhone(input);
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
                var highSchools =
                    this.repository.Query<HighSchools>()
                        .FirstOrDefault(x => x.ID == input.InstitutionId && x.SyStatusesObj.StatusCode == "A");

                if (highSchools == null)
                {
                    response.ResponseMessage = "Institution was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

                if (user == null)
                {
                    response.ResponseMessage = "User Acount was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode == "A");
                var type = this.repository.Query<SyPhoneType>().FirstOrDefault(x => x.ID == input.TypeId && x.SyStatusesObj.StatusCode == "A");

                InstitutionPhone phone = this.repository.Query<InstitutionPhone>()
                        .FirstOrDefault(x => x.Institution.ID == highSchools.ID && x.ID == input.Id);

                if (phone == null)
                {
                    response.ResponseMessage = "Phone was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                phone.Update(highSchools, type, input.Phone.Replace("_", string.Empty), DateTime.Now, user.UserName, input.IsForeignPhone, status, null);

                this.repository.Save(phone);

                response.ResponseMessage = "The phone was updated!";
            }

            return response;
        }

        /// <summary>
        /// The insert address.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public Response<string> InsertAddress(InstitutionAddressModel input)
        {
            Response<string> response = this.ValidateInsertAddress(input);
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
                var highSchools =
                    this.repository.Query<HighSchools>()
                        .FirstOrDefault(x => x.ID == input.InstitutionId && x.SyStatusesObj.StatusCode == "A");

                if (highSchools == null)
                {
                    response.ResponseMessage = "Institution was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

                if (user == null)
                {
                    response.ResponseMessage = "User Acount was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode == "A");
                var type = this.repository.Query<PlAddressTypes>().FirstOrDefault(x => x.ID == input.TypeId);

                SystemStates state = null;

                Country country = null;


                County county = null;

                if (!input.IsInternational)
                {
                    state = this.repository.Query<SystemStates>().FirstOrDefault(x => x.ID == input.StateId && x.StatusObj.StatusCode == "A");
                    country = this.repository.Query<Country>().FirstOrDefault(x => x.ID == input.CountryId && x.SyStatusesObj.StatusCode == "A");
                    county = this.repository.Query<County>().FirstOrDefault(x => x.ID == input.CountyId && x.SyStatusesObj.StatusCode == "A");
                }

                InstitutionAddress address = new InstitutionAddress(
                    highSchools,
                    type,
                    input.Address1,
                    input.Address2,
                    input.City,
                    state,
                    input.ZipCode.Replace("_", string.Empty),
                    input.IsMailingAddress,
                    user.UserName,
                    DateTime.Now,
                    country,
                    status,
                    input.IsInternational,
                    county,
                    input.Country,
                    input.AddressApto,
                    input.State,
                    input.IsDefault,
                    input.County);

                this.repository.Save(address);

                response.ResponseMessage = "The address was inserted!";
            }

            return response;
        }

        /// <summary>
        /// The update address.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public Response<string> UpdateAddress(InstitutionAddressModel input)
        {
            Response<string> response = this.ValidateUpdateAddress(input);
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
                var highSchools =
                    this.repository.Query<HighSchools>()
                        .FirstOrDefault(x => x.ID == input.InstitutionId && x.SyStatusesObj.StatusCode == "A");

                if (highSchools == null)
                {
                    response.ResponseMessage = "Institution was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

                if (user == null)
                {
                    response.ResponseMessage = "User Acount was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode == "A");
                var type = this.repository.Query<PlAddressTypes>().FirstOrDefault(x => x.ID == input.TypeId && x.SyStatusesObj.StatusCode == "A");

                SystemStates state = null;

                Country country = null;


                County county = null;

                if (!input.IsInternational)
                {
                    state = this.repository.Query<SystemStates>().FirstOrDefault(x => x.ID == input.StateId && x.StatusObj.StatusCode == "A");
                    country = this.repository.Query<Country>().FirstOrDefault(x => x.ID == input.CountryId && x.SyStatusesObj.StatusCode == "A");
                    county = this.repository.Query<County>().FirstOrDefault(x => x.ID == input.CountyId && x.SyStatusesObj.StatusCode == "A");
                }

                InstitutionAddress address = this.repository.Query<InstitutionAddress>().FirstOrDefault(x => x.Institution.ID == highSchools.ID && x.ID == input.Id);

                if (address == null)
                {
                    response.ResponseMessage = "Address was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                address.Update(
                    highSchools,
                    type,
                    input.Address1,
                    input.Address2,
                    input.City,
                    state,
                    input.ZipCode.Replace("_", string.Empty),
                    input.IsMailingAddress,
                    user.UserName,
                    DateTime.Now,
                    country,
                    status,
                    input.IsInternational,
                    county,
                    input.Country,
                    input.AddressApto,
                    input.State,
                    input.IsDefault,
                    input.County);

                this.repository.Save(address);

                response.ResponseMessage = "The address was updated!";
            }

            return response;
        }

        /// <summary>
        /// The delete address.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public Response<string> DeleteAddress(InstitutionAddressModel input)
        {
            Response<string> response = this.ValidateDeleteAddress(input);
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
                HighSchools institution = this.repository.Query<HighSchools>().FirstOrDefault(x => x.ID == input.InstitutionId && x.CampusGroupObj.Campuses.Any(c => c.ID == input.CampusId) && x.SyStatusesObj.StatusCode == "A");

                if (institution == null)
                {
                    response.ResponseMessage = "Institution was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

                if (user == null)
                {
                    response.ResponseMessage = "User Acount was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode == "I");

                InstitutionAddress address =
                    this.repository.Query<InstitutionAddress>()
                        .FirstOrDefault(x => x.Institution.ID == institution.ID && x.ID == input.Id && x.Status.StatusCode == "A");

                if (address == null)
                {
                    response.ResponseMessage = "Address was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                address.Delete(user.UserName, status);

                this.repository.Save(address);

                response.ResponseMessage = "The address was deleted!";
            }

            return response;
        }

        /// <summary>
        /// The insert contact.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public Response<string> InsertContact(InstitutionContactModel input)
        {
            Response<string> response = this.ValidateInsertContact(input);
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
                var highSchools =
                    this.repository.Query<HighSchools>()
                        .FirstOrDefault(x => x.ID == input.InstitutionId && x.SyStatusesObj.StatusCode == "A");

                if (highSchools == null)
                {
                    response.ResponseMessage = "Institution was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

                if (user == null)
                {
                    response.ResponseMessage = "User Acount was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode == "A");
                var suffix = this.repository.Query<Suffix>().FirstOrDefault(x => x.ID == input.SuffixId && x.Status.StatusCode == "A");
                var prefix = this.repository.Query<Prefix>().FirstOrDefault(x => x.ID == input.PrefixId && x.Status.StatusCode == "A");

                InstitutionContact contact = new InstitutionContact(
                    highSchools,
                    input.FirstName,
                    input.LastName,
                    input.MiddleName,
                    status,
                    suffix,
                    prefix,
                    input.Title,
                    input.Phone,
                    input.PhoneExtension,
                    input.IsForeignPhone,
                    input.Email,
                    user.UserName,
                    DateTime.Now,
                    input.IsDefault);

                this.repository.Save(contact);

                response.ResponseMessage = "The contact was inserted!";
            }

            return response;
        }

        /// <summary>
        /// The delete contact.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public Response<string> DeleteContact(InstitutionContactModel input)
        {
            Response<string> response = this.ValidateDeleteContact(input);
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
                HighSchools institution = this.repository.Query<HighSchools>().FirstOrDefault(x => x.ID == input.InstitutionId && x.CampusGroupObj.Campuses.Any(c => c.ID == input.CampusId) && x.SyStatusesObj.StatusCode == "A");

                if (institution == null)
                {
                    response.ResponseMessage = "Institution was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

                if (user == null)
                {
                    response.ResponseMessage = "User Acount was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode == "I");

                InstitutionContact contact =
                    this.repository.Query<InstitutionContact>()
                        .FirstOrDefault(x => x.Institution.ID == institution.ID && x.ID == input.Id && x.Status.StatusCode == "A");

                if (contact == null)
                {
                    response.ResponseMessage = "Institution contact was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                contact.Delete(user.UserName, status);

                this.repository.Save(contact);

                response.ResponseMessage = "The contact was deleted!";
            }

            return response;
        }

        /// <summary>
        /// The update contact.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        public Response<string> UpdateContact(InstitutionContactModel input)
        {
            Response<string> response = this.ValidateUpdateContact(input);
            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
                var highSchools =
                    this.repository.Query<HighSchools>()
                        .FirstOrDefault(x => x.ID == input.InstitutionId && x.SyStatusesObj.StatusCode == "A");

                if (highSchools == null)
                {
                    response.ResponseMessage = "Institution was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

                if (user == null)
                {
                    response.ResponseMessage = "User Acount was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                var status = this.repository.Query<SyStatuses>().FirstOrDefault(x => x.StatusCode == "A");
                var suffix = this.repository.Query<Suffix>().FirstOrDefault(x => x.ID == input.SuffixId && x.Status.StatusCode == "A");
                var prefix = this.repository.Query<Prefix>().FirstOrDefault(x => x.ID == input.PrefixId && x.Status.StatusCode == "A");

                InstitutionContact contact = this.repository.Query<InstitutionContact>().FirstOrDefault(x => x.Institution.ID == highSchools.ID && x.ID == input.Id && x.Status.StatusCode == "A");

                if (contact == null)
                {
                    response.ResponseMessage = "Institution Conact was not found";
                    response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
                    return response;
                }

                contact.Update(highSchools, input.FirstName, input.LastName, input.MiddleName, status, suffix, prefix, input.Title, input.Phone, input.PhoneExtension, input.IsForeignPhone, input.Email, user.UserName, DateTime.Now, input.IsDefault);

                this.repository.Save(contact);

                response.ResponseMessage = "The contact was updated!";
            }

            return response;
        }

        /// <summary>
        /// The validate insert phone.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        private Response<string> ValidateInsertPhone(InstitutionPhoneModel input)
        {
            Response<string> response = new Response<string>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            if (string.IsNullOrEmpty(input.Phone))
            {
                response.ResponseMessage = "Phone is a required field";
            }
            else if (!FormatValidator.IsValidPhoneNumber(input.Phone.Replace("_", string.Empty)))
            {
                response.ResponseMessage = "Invalid Phone Number Format. A valid phone number should only include numbers or special phone characters (*,+,-,/,#)";
            }
            else if (input.CampusId == Guid.Empty)
            {
                response.ResponseMessage = "Campus is a required field";
            }
            else if (input.InstitutionId == Guid.Empty)
            {
                response.ResponseMessage = "Institution is a required field";
            }
            else if (input.UserId == Guid.Empty)
            {
                response.ResponseMessage = "User is a required field";
            }

            if (!string.IsNullOrEmpty(response.ResponseMessage))
            {
                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
            }

            return response;
        }

        /// <summary>
        /// The validate delete phone.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        private Response<string> ValidateDeletePhone(InstitutionPhoneModel input)
        {
            Response<string> response = new Response<string>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            if (input.Id == Guid.Empty)
            {
                response.ResponseMessage = "Phone is a required field";
            }
            else if (input.CampusId == Guid.Empty)
            {
                response.ResponseMessage = "Campus is a required field";
            }
            else if (input.InstitutionId == Guid.Empty)
            {
                response.ResponseMessage = "Institution is a required field";
            }
            else if (input.UserId == Guid.Empty)
            {
                response.ResponseMessage = "User is a required field";
            }

            if (!string.IsNullOrEmpty(response.ResponseMessage))
            {
                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
            }

            return response;
        }

        /// <summary>
        /// The validate update phone.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        private Response<string> ValidateUpdatePhone(InstitutionPhoneModel input)
        {
            Response<string> response = new Response<string>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            if (string.IsNullOrEmpty(input.Phone))
            {
                response.ResponseMessage = "Phone is a required field";
            }
            else if (!FormatValidator.IsValidPhoneNumber(input.Phone.Replace("_", string.Empty)))
            {
                response.ResponseMessage = "Invalid Phone Number Format. A valid phone number should only include numbers or special phone characters (*,+,-,/,#)";
            }
            else if (input.Id == Guid.Empty)
            {
                response.ResponseMessage = "Phone Id a required field";
            }
            else if (input.CampusId == Guid.Empty)
            {
                response.ResponseMessage = "Campus is a required field";
            }
            else if (input.InstitutionId == Guid.Empty)
            {
                response.ResponseMessage = "Institution is a required field";
            }
            else if (input.UserId == Guid.Empty)
            {
                response.ResponseMessage = "User is a required field";
            }

            if (!string.IsNullOrEmpty(response.ResponseMessage))
            {
                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
            }

            return response;
        }

        /// <summary>
        /// The validate insert address.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        private Response<string> ValidateInsertAddress(InstitutionAddressModel input)
        {
            Response<string> response = new Response<string>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            if (string.IsNullOrEmpty(input.Address1))
            {
                response.ResponseMessage = "Address 1 is a required field";
            }
            else if (input.CampusId == Guid.Empty)
            {
                response.ResponseMessage = "Campus is a required field";
            }
            else if (input.InstitutionId == Guid.Empty)
            {
                response.ResponseMessage = "Institution is a required field";
            }
            else if (input.UserId == Guid.Empty)
            {
                response.ResponseMessage = "User is a required field";
            }
            else
            {
                if (!input.IsInternational)
                {
                    if (input.CountryId == Guid.Empty)
                    {
                        response.ResponseMessage = "Country is a required field";
                    }
                    else if (input.StateId == Guid.Empty)
                    {
                        response.ResponseMessage = "State is a required field";
                    }
                    else if (string.IsNullOrEmpty(input.City))
                    {
                        response.ResponseMessage = "City is a required field";
                    }
                    else if (string.IsNullOrEmpty(input.ZipCode))
                    {
                        response.ResponseMessage = "Zip Code is a required field";
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(input.Country))
                    {
                        response.ResponseMessage = "Country is a required field";
                    }
                    else if (string.IsNullOrEmpty(input.State))
                    {
                        response.ResponseMessage = "State is a required field";
                    }
                    else if (string.IsNullOrEmpty(input.City))
                    {
                        response.ResponseMessage = "City is a required field";
                    }
                }

                if (!FormatValidator.IsValidZipCode(input.ZipCode.Trim(), input.IsInternational))
                {
                    response.ResponseMessage = "Invalid Zip Code Format!";
                }
            }

            if (!string.IsNullOrEmpty(response.ResponseMessage))
            {
                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
            }

            return response;
        }

        /// <summary>
        /// The validate delete address.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        private Response<string> ValidateDeleteAddress(InstitutionAddressModel input)
        {
            Response<string> response = new Response<string>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            if (input.Id == Guid.Empty)
            {
                response.ResponseMessage = "Address is a required field";
            }
            else if (input.CampusId == Guid.Empty)
            {
                response.ResponseMessage = "Campus is a required field";
            }
            else if (input.InstitutionId == Guid.Empty)
            {
                response.ResponseMessage = "Institution is a required field";
            }
            else if (input.UserId == Guid.Empty)
            {
                response.ResponseMessage = "User is a required field";
            }

            if (!string.IsNullOrEmpty(response.ResponseMessage))
            {
                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
            }

            return response;
        }

        /// <summary>
        /// The validate update address.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        private Response<string> ValidateUpdateAddress(InstitutionAddressModel input)
        {
            Response<string> response = new Response<string>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            if (string.IsNullOrEmpty(input.Address1))
            {
                response.ResponseMessage = "Address 1 is a required field";
            }
            else if (input.Id == Guid.Empty)
            {
                response.ResponseMessage = "Address Id is a required field";
            }
            else if (input.CampusId == Guid.Empty)
            {
                response.ResponseMessage = "Campus is a required field";
            }
            else if (input.InstitutionId == Guid.Empty)
            {
                response.ResponseMessage = "Institution is a required field";
            }
            else if (input.UserId == Guid.Empty)
            {
                response.ResponseMessage = "User is a required field";
            }
            else
            {
                if (!input.IsInternational)
                {
                    if (input.CountryId == Guid.Empty)
                    {
                        response.ResponseMessage = "Country is a required field";
                    }
                    if (input.StateId == Guid.Empty)
                    {
                        response.ResponseMessage = "State is a required field";
                    }
                    else if (string.IsNullOrEmpty(input.City))
                    {
                        response.ResponseMessage = "City is a required field";
                    }
                    else if (string.IsNullOrEmpty(input.ZipCode))
                    {
                        response.ResponseMessage = "Zip Code is a required field";
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(input.Country))
                    {
                        response.ResponseMessage = "Country is a required field";
                    }
                    if (string.IsNullOrEmpty(input.State))
                    {
                        response.ResponseMessage = "State is a required field";
                    }
                    else if (string.IsNullOrEmpty(input.City))
                    {
                        response.ResponseMessage = "City is a required field";
                    }
                }

                if (!FormatValidator.IsValidZipCode(input.ZipCode.Trim(), input.IsInternational))
                {
                    response.ResponseMessage = "Invalid Zip Code Format!";
                }
            }

            if (!string.IsNullOrEmpty(response.ResponseMessage))
            {
                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
            }

            return response;
        }

        /// <summary>
        /// The validate insert contact.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        private Response<string> ValidateInsertContact(InstitutionContactModel input)
        {
            Response<string> response = new Response<string>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            if (string.IsNullOrEmpty(input.FirstName))
            {
                response.ResponseMessage = "First name is a required field";
            }
            else if (string.IsNullOrEmpty(input.LastName))
            {
                response.ResponseMessage = "Last name is a required field";
            }
            else if (input.CampusId == Guid.Empty)
            {
                response.ResponseMessage = "Campus is a required field";
            }
            else if (input.InstitutionId == Guid.Empty)
            {
                response.ResponseMessage = "Institution is a required field";
            }
            else if (input.UserId == Guid.Empty)
            {
                response.ResponseMessage = "User is a required field";
            }

            if (!string.IsNullOrEmpty(response.ResponseMessage))
            {
                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
            }

            return response;
        }

        /// <summary>
        /// The validate delete contact.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        private Response<string> ValidateDeleteContact(InstitutionContactModel input)
        {
            Response<string> response = new Response<string>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            if (input.Id == Guid.Empty)
            {
                response.ResponseMessage = "Contact Id is a required field";
            }
            else if (input.CampusId == Guid.Empty)
            {
                response.ResponseMessage = "Campus is a required field";
            }
            else if (input.InstitutionId == Guid.Empty)
            {
                response.ResponseMessage = "Institution is a required field";
            }
            else if (input.UserId == Guid.Empty)
            {
                response.ResponseMessage = "User is a required field";
            }

            if (!string.IsNullOrEmpty(response.ResponseMessage))
            {
                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
            }

            return response;
        }

        /// <summary>
        /// The validate update contact.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        private Response<string> ValidateUpdateContact(InstitutionContactModel input)
        {
            Response<string> response = new Response<string>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            if (string.IsNullOrEmpty(input.FirstName))
            {
                response.ResponseMessage = "First name is a required field";
            }
            else if (string.IsNullOrEmpty(input.LastName))
            {
                response.ResponseMessage = "Last name is a required field";
            }
            else if (input.Id == Guid.Empty)
            {
                response.ResponseMessage = "Contact Id is a required field";
            }
            else if (input.CampusId == Guid.Empty)
            {
                response.ResponseMessage = "Campus is a required field";
            }
            else if (input.InstitutionId == Guid.Empty)
            {
                response.ResponseMessage = "Institution is a required field";
            }
            else if (input.UserId == Guid.Empty)
            {
                response.ResponseMessage = "User is a required field";
            }

            if (!string.IsNullOrEmpty(response.ResponseMessage))
            {
                response.ResponseCode = (int)HttpStatusCode.ExpectationFailed;
            }

            return response;
        }
    }
}
