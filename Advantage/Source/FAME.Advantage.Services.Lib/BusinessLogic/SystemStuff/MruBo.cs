﻿using System;
using System.Linq;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.MostRecentlyUsed;
using FAME.Advantage.Domain.Users;
using FAME.Advantage.Messages.SystemStuff.Mru;

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff
{
    /// <summary>
    /// Business Object for MRU.
    /// </summary>
    public class MruBo
    {
        //represents data store
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;
        private readonly IRepositoryWithTypedID<Guid> repositoryWithGuid;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="repositorytyped"></param>
        public MruBo(IRepositoryWithTypedID<int> repository, IRepositoryWithTypedID<Guid> repositorytyped)
        {
            repositoryWithInt = repository;
            repositoryWithGuid = repositorytyped;
        }

        /// <summary>
        /// Insert the entity in the MRU
        /// </summary>
        /// <param name="input"></param>
        public void InsertEntityInMru(MruInputModel input)
        {

            var user = repositoryWithGuid.Query<User>().Single(x => x.ID.ToString() == input.UserId);
            var type = repositoryWithInt.Query<MruType>().Single(x => x.ID == input.TypeEntity);

            // Put the Lead in the MRU List
            if (input.TypeEntity == 4)
            {

                // Check if the lead is already in the MRU
                var lead = repositoryWithGuid.Query<Domain.Lead.Lead>().SingleOrDefault(x => x.ID.ToString() == input.EntityId);
                if (lead == null)
                {
                    throw new ApplicationException("Lead not in Table");
                }

                var mruobj = lead.MruList.SingleOrDefault(x => x.UserObj.ID.ToString() == input.UserId && x.CampusObj.ID.ToString() == input.CampusId);
                if (mruobj != null)
                {
                    // Update the MRU to top
                    var maxCounter = repositoryWithGuid.Query<ViewLeadMru>().Where(x => x.UserObj.ID.ToString() == input.UserId).Max(y => y.Counter);
                    if (mruobj.Counter == maxCounter)
                    {
                        return;
                    }
                        lead.MruList.Remove(mruobj);
                       // mruobj.ChangeCounter(maxCounter + 1);

                       // repositoryWithGuid.Update(lead);
                   // return;
                }

                // If not exists or was delete create a new and insert it.
                var newmru = new ViewLeadMru(type, lead, user, lead.Campus, user.UserName, DateTime.Now);
                lead.MruList.Add(newmru);
                // save it
                repositoryWithGuid.SaveOnly(lead);
                return;
            }

            throw new NotImplementedException("Insert entity is only implemented for Lead Entity");
        }
    }
}
