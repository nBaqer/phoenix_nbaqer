﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemStatusHistory.cs" company="FAME Inc.">
// FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff
// </copyright>
// <summary>
//   Defines the Dropped type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;

    using AutoMapper;

    using FAME.Advantage.Domain.Campuses;
    using FAME.Advantage.Domain.Enumerations;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Student.Enrollments;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Domain.Users;
    using FAME.Advantage.Messages.Common;
    using FAME.Advantage.Messages.SystemStuff.SystemStatuses;
    using FAME.Advantage.Messages.Web;
    using FAME.Advantage.Services.Lib.BusinessLogic.Students.Academic;
    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus;
    using FAME.Advantage.Services.Lib.Models.SystemStatus;

    /// <summary>
    /// Business Object for handling Validations and CRUD Operations for System Status History
    /// </summary>
    public class SystemStatusHistory
    {
        /// <summary>
        /// The nHibernate Repository for Guids
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The nHibernate Repository for integers
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemStatusHistory"/> class. 
        /// </summary>
        /// <param name="repository">
        /// The nHibernate Repository for Guids
        /// </param>
        /// <param name="repositoryWithInt">
        /// The nHibernate Repository for integers
        /// </param>
        public SystemStatusHistory(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            this.repositoryWithInt = repositoryWithInt;
        }

        /// <summary>
        /// Get a List of the Status Change History based on Campus, Enrollment and Student
        /// </summary>
        /// <param name="input">The Input parameters to query the data(Campus, Enrollment and Student)</param>
        /// <returns>
        /// A Response Object of Type IEnumerable of SystemStatusChangeModel
        /// Return Response OK if data is found otherwise return the error code associated to bad Request or Exception.
        /// If Response Code is different than OK(200) use the ResponseMessage to view the error details.
        /// </returns>
        public Response<IEnumerable<SystemStatusChangeModel>> GetHistory(SystemStatusChangeFilterModel input)
        {
            Response<IEnumerable<SystemStatusChangeModel>> response = new Response<IEnumerable<SystemStatusChangeModel>>();
            response.ResponseCode = (int)HttpStatusCode.OK;
            if (input != null)
            {
                bool canContinue = true;

                if (string.IsNullOrEmpty(input.CampusId))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Campus is a required field!";
                    canContinue = false;
                }
                else
                    if (string.IsNullOrEmpty(input.StudentEnrollmentId))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Enrollment is a required field!";
                    canContinue = false;
                }
                else
                        if (string.IsNullOrEmpty(input.UserId))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "User is a required field!";
                    canContinue = false;
                }


                if (canContinue)
                {
                    try
                    {
                        var enrollment = this.repository.Query<Enrollment>().FirstOrDefault(
                               n => n.ID == Guid.Parse(input.StudentEnrollmentId));
                        if (enrollment != null)
                        {
                            List<SystemStatusChangeHistory> statusChangeHistory = this.repository.Query<SystemStatusChangeHistory>().Where(x => x.Enrollment.ID == enrollment.ID).OrderByDescending(x => x.DateOfChange).ThenByDescending(x => x.ModifiedDate).ToList();

                            SystemStatusChangeModel model;
                            List<SystemStatusChangeModel> statusChangeList = new List<SystemStatusChangeModel>();
                            foreach (var source in statusChangeHistory)
                            {
                                model = new SystemStatusChangeModel();
                                model.CampusId = source.Campus.ID.ToString();
                                model.CaseNumber = source.CaseNumber;
                                if (source.DateOfChange != null)
                                {
                                    model.EffectiveDate = source.DateOfChange.Value.ToString("MM/dd/yyyy");
                                }
                                if (source.ModifiedDate != null)
                                {
                                    model.DateOfChange = source.ModifiedDate.Value.ToString("MM/dd/yyyy");
                                }

                                model.PostedBy = source.ModifiedUser;
                                model.StudentEnrollmentId = source.Enrollment.ID.ToString();
                                model.RequestedBy = source.RequestedBy;

                                model.StatusCodeId = source.ID;
                                if (source.NewStatus != null)
                                {
                                    model.Status = source.NewStatus.Description;
                                    model.StatusId = source.NewStatus.SystemStatus.ID;

                                    switch ((Enumerations.eEnrollmentSystemStatus)source.NewStatus.SystemStatus.ID)
                                    {
                                        case Enumerations.eEnrollmentSystemStatus.Dropped:
                                            {
                                                var reason = this.repository.Query<DropReason>().FirstOrDefault(x => x.ID == source.DropReasonId);
                                                model.Reason = reason != null ? reason.Description : string.Empty;
                                                break;
                                            }

                                        case Enumerations.eEnrollmentSystemStatus.LeaveOfAbsence:
                                            {
                                                var loa = this.repository.Query<StudentLoa>().FirstOrDefault(x => x.StudentStatusChangeId == source.ID);

                                                if (loa != null)
                                                {
                                                    model.Reason = loa.LoaReasonsObj != null ? loa.LoaReasonsObj.Description : string.Empty;
                                                }

                                                break;
                                            }

                                        case Enumerations.eEnrollmentSystemStatus.Suspension:
                                            {
                                                var reason = this.repository.Query<StudentSuspensions>()
                                                        .FirstOrDefault(x => x.StudentStatusChangeId == source.ID);
                                                model.Reason = reason != null ? reason.Reason : string.Empty;
                                                break;
                                            }
                                    }
                                }

                                statusChangeList.Add(model);
                            }

                            response.Data = statusChangeList;
                        }
                        else
                        {
                            response.ResponseCode = (int)HttpStatusCode.BadRequest;
                            response.ResponseMessage = "Invalid Student or Enrolment Id!";
                        }
                    }
                    catch (Exception ex)
                    {
                        response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                        response.ResponseMessage = ex.Message;
                    }

                }
            }
            else
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Input can not be null!";
            }

            return response;
        }

        /// <summary>
        /// The Get the school defined system status based on the system defined status
        /// </summary>
        /// <param name="status">
        /// The status.
        /// </param>
        /// <returns>
        /// The <see cref="UserDefStatusCode"/>.
        /// </returns>
        public UserDefStatusCode GetSystemStatus(Enumerations.eSystemStatus status)
        {
            return this.repository.Query<UserDefStatusCode>().OrderBy(x => x.ModifiedDate).FirstOrDefault(x => x.SystemStatus.ID == (int)status);
        }

        /// <summary>
        /// Gets a List of Status Codes to be shown in dropdown
        /// </summary>
        /// <param name="input">The Input Model for the System Status Change</param>
        /// <returns>
        /// An object of the Response&lt;IEnumerable&lt;SystemStatusCodeModel&gt;&gt; class. 
        /// </returns>
        public Response<IEnumerable<SystemStatusCodeModel>> GetStatusCodesList(SystemStatusChangeModel input)
        {
            Response<IEnumerable<SystemStatusCodeModel>> response = new Response<IEnumerable<SystemStatusCodeModel>>();
            response.ResponseCode = (int)HttpStatusCode.OK;
            try
            {
                bool canContinue = true;

                if (string.IsNullOrEmpty(input.CampusId))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Campus is a required field!";
                    canContinue = false;
                }
                else
                    if (string.IsNullOrEmpty(input.StudentEnrollmentId))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Student Enrollment is a required field!";
                    canContinue = false;
                }
                else
                        if (string.IsNullOrEmpty(input.UserId))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "User is a required field!";
                    canContinue = false;
                }
                else
                            if (string.IsNullOrEmpty(input.EffectiveDate))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Effective Date is a required field!";
                    canContinue = false;
                }
                else
                {
                    DateTime effectiveDate;
                    canContinue = DateTime.TryParse(input.EffectiveDate, out effectiveDate);
                }

                if (canContinue)
                {
                    var enrollment = this.repository.Query<Enrollment>().FirstOrDefault(
                              n => n.ID == Guid.Parse(input.StudentEnrollmentId));
                    if (enrollment != null)
                    {
                        DateTime effectiveDate = DateTime.Parse(input.EffectiveDate);

                        var statusChangeRules = this.repositoryWithInt.Query<SystemStatusWorkflowRules>().AsQueryable();

                        List<SystemStatusChangeHistory> statusHistory =
                            this.repository.Query<SystemStatusChangeHistory>()
                                .Where(x => x.Enrollment.ID == enrollment.ID)
                                .OrderByDescending(x => x.DateOfChange)
                                .ThenByDescending(x => x.ModifiedDate).ToList();

                        SystemStatusChangeHistory lastStatusChange = null;
                        switch ((Common.Enumerations.eValidationMode)input.ValidationMode)
                        {
                            case Common.Enumerations.eValidationMode.Insert:
                                {
                                    lastStatusChange = statusHistory.FirstOrDefault(
                                        x => x.DateOfChange <= effectiveDate);
                                    break;
                                }
                            default:
                                {
                                    lastStatusChange = statusHistory.FirstOrDefault(x => x.ID == input.StatusCodeId);
                                    break;
                                }
                        }

                        var statusCodeList =
                            this.repository.Query<UserDefStatusCode>()
                                .Where(
                                    x =>
                                    x.SystemStatus.StatusLevelId
                                    == (int)Common.Enumerations.eSystemStatusLevel.StudentEnrollment);

                        if (lastStatusChange != null && lastStatusChange.NewStatus != null)
                        {
                            switch ((Common.Enumerations.eValidationMode)input.ValidationMode)
                            {
                                case Common.Enumerations.eValidationMode.Insert:
                                    {
                                        var statusToMoveTo = from status in statusChangeRules
                                                             where
                                                                 status.AllowInsert
                                                                 && status.StatusFrom.ID
                                                                 == lastStatusChange.NewStatus.SystemStatus.ID
                                                             select status.StatusTo.ID;

                                        var statusCodesToMoveTo =
                                            statusCodeList.Where(x => statusToMoveTo.Contains(x.SystemStatus.ID))
                                                .OrderBy(x => x.SystemStatus.ID);
                                        response.Data =
                                            Mapper.Map<IEnumerable<SystemStatusCodeModel>>(statusCodesToMoveTo);
                                        break;
                                    }

                                case Common.Enumerations.eValidationMode.Edit:
                                    {
                                        var statusToMoveTo = from status in statusChangeRules
                                                             where
                                                                 status.AllowEdit
                                                                 && status.StatusFrom.ID
                                                                 == lastStatusChange.NewStatus.SystemStatus.ID
                                                             select status.StatusTo.ID;
                                        var statusCodesToMoveTo =
                                            statusCodeList.Where(x => statusToMoveTo.Contains(x.SystemStatus.ID))
                                                .OrderBy(x => x.SystemStatus.ID);
                                        response.Data =
                                            Mapper.Map<IEnumerable<SystemStatusCodeModel>>(statusCodesToMoveTo);
                                        break;
                                    }

                                case Common.Enumerations.eValidationMode.Delete:
                                    {
                                        var statusToMoveTo = from status in statusChangeRules
                                                             where
                                                                 status.AllowDelete
                                                                 && status.StatusFrom.ID
                                                                 == lastStatusChange.NewStatus.SystemStatus.ID
                                                             select status.StatusTo.ID;
                                        var statusCodesToMoveTo =
                                            statusCodeList.Where(x => statusToMoveTo.Contains(x.SystemStatus.ID))
                                                .OrderBy(x => x.SystemStatus.ID);
                                        response.Data =
                                            Mapper.Map<IEnumerable<SystemStatusCodeModel>>(statusCodesToMoveTo);
                                        break;
                                    }
                            }
                        }
                        else
                        {
                            switch ((Common.Enumerations.eValidationMode)input.ValidationMode)
                            {
                                case Common.Enumerations.eValidationMode.Insert:
                                    {
                                        var statusToMoveTo = from status in statusChangeRules
                                                             where
                                                                 status.AllowInsert && status.StatusFrom == null
                                                             select status.StatusTo.ID;
                                        var statusCodesToMoveTo =
                                            statusCodeList.Where(x => statusToMoveTo.Contains(x.SystemStatus.ID));
                                        response.Data =
                                            Mapper.Map<IEnumerable<SystemStatusCodeModel>>(statusCodesToMoveTo);
                                        break;
                                    }
                                case Common.Enumerations.eValidationMode.Edit:
                                    {
                                        var statusToMoveTo = from status in statusChangeRules
                                                             where
                                                                 status.AllowEdit && status.StatusFrom == null
                                                             select status.StatusTo.ID;
                                        var statusCodesToMoveTo =
                                            statusCodeList.Where(x => statusToMoveTo.Contains(x.SystemStatus.ID));
                                        response.Data =
                                            Mapper.Map<IEnumerable<SystemStatusCodeModel>>(statusCodesToMoveTo);
                                        break;
                                    }
                            }
                        }

                        // }
                    }
                }

            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }


        /// <summary>
        /// Verify if the Status Change is allowed and meets all the required field.
        /// </summary>
        /// <param name="input">The Input Model for the System Status Change</param>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> ValidateStatusChange(SystemStatusChangeModel input)
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;
            try
            {
                input.DateOfChange = DateTime.Now.ToString();
                response = this.ValidateStatusChangeModelRequiredFields(input);

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    response.Data = input;
                    var enrollment = this.repository.Query<Enrollment>().FirstOrDefault(
                              n => n.ID == Guid.Parse(input.StudentEnrollmentId));
                    var campus = this.repository.Query<Campus>().FirstOrDefault(x => x.ID == Guid.Parse(input.CampusId));
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.UserId));

                    if (enrollment != null && campus != null && user != null)
                    {
                        DateTime effectiveDate = DateTime.Parse(input.EffectiveDate);

                        if (effectiveDate < enrollment.StartDate && input.StatusId != (int)Enumerations.eSystemStatus.FutureStart)
                        {
                            response.ResponseMessage = "Effective Date can't be before the Student Enrollment Start Date.";
                            response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        }
                        else if (effectiveDate > enrollment.ExpectedGraduationDate)
                        {
                            response.ResponseMessage =
                                string.Copy(
                                    "Effective Date can't be higher than the Student Enrollment Graduation Date.");
                            response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        }
                        else
                        {
                            if (input.StatusId == (int)Enumerations.eSystemStatus.NoStart)
                            {
                                NoStart noStart = new NoStart(input, this.repository, this.repositoryWithInt);
                                response = noStart.ValidateAttendanceAndGrades();
                            }
                            else if (input.StatusId == (int)Enumerations.eSystemStatus.FutureStart)
                            {
                                FutureStart futureStart = new FutureStart(input, this.repository, this.repositoryWithInt);
                                response = futureStart.ValidateAttendanceAndGrades();
                            }
                        }

                        if (response.ResponseCode == (int)HttpStatusCode.OK)
                        {
                            var statusChangeRules = this.repositoryWithInt.Query<SystemStatusWorkflowRules>().AsQueryable();

                            var history = this.repository.Query<SystemStatusChangeHistory>()
                                    .Where(x => x.Enrollment.ID == enrollment.ID)
                                    .OrderBy(x => x.ModifiedDate)
                                    .ToList();


                            List<SystemStatusChangeHistoryModel> statusChangeHistory = Mapper.Map<List<SystemStatusChangeHistoryModel>>(history);

                            var statusCodeList = this.repository.Query<UserDefStatusCode>()
                                    .Where(
                                        x =>
                                            x.SystemStatus.StatusLevelId ==
                                            (int)Common.Enumerations.eSystemStatusLevel.StudentEnrollment);

                            SystemStatusChangeHistoryModel lastStatusChange =
                                statusChangeHistory.Where(
                                    x => x.DateOfChange <= DateTime.Parse(input.EffectiveDate)).OrderByDescending(x => x.DateOfChange).ThenByDescending(x => x.ModifiedDate).FirstOrDefault();

                            var newStatusCode = statusCodeList.FirstOrDefault(x => x.ID == input.StatusCodeId);

                            if (lastStatusChange != null && newStatusCode != null)
                            {
                                response.Data.StatusId = newStatusCode.SystemStatus.ID;
                                if (lastStatusChange.NewStatus != null)
                                {
                                    response.Data.PreviousStatusCodeId = lastStatusChange.NewStatus.ID;
                                    response.Data.PreviousStatusId = lastStatusChange.NewStatus.SystemStatus.ID;
                                }
                                response.Data.StudentEnrollmentStartDate = enrollment.StartDate.HasValue
                                    ? enrollment.StartDate.Value.ToString()
                                    : null;
                                response.Data.StudentEnrollmentGraduationDate =
                                    enrollment.ExpectedGraduationDate.HasValue
                                        ? enrollment.ExpectedGraduationDate.Value.ToString()
                                        : null;

                                SystemStatusChangeHistoryModel newStatusChangeHistory = new SystemStatusChangeHistoryModel();

                                Guid newStatusChangeId = Guid.NewGuid();
                                newStatusChangeHistory.Create(newStatusChangeId, enrollment, campus, lastStatusChange.NewStatus, newStatusCode, DateTime.Parse(input.DateOfChange), user.Email, DateTime.Parse(input.EffectiveDate), input.CaseNumber, input.RequestedBy, null, null);

                                statusChangeHistory.Add(newStatusChangeHistory);

                                if (lastStatusChange.DateOfChange == newStatusChangeHistory.DateOfChange)
                                {
                                    statusChangeHistory.Remove(
                                        statusChangeHistory.FirstOrDefault(x => x.ID == lastStatusChange.ID));
                                    lastStatusChange.AddMillisecondToDateOfChange();
                                    statusChangeHistory.Add(lastStatusChange);
                                }

                                response = this.ValidateStatusChangeSequence(input, statusChangeHistory, newStatusCode.ID, statusChangeRules, newStatusChangeId);
                            }
                            else if (newStatusCode != null)
                            {
                                SystemStatusWorkflowRules statusRuleAllowed = null;
                                statusRuleAllowed = statusChangeRules.FirstOrDefault(x => x.StatusFrom == null && x.StatusTo.ID == newStatusCode.SystemStatus.ID);

                                if (statusRuleAllowed == null)
                                {
                                    response.Data.AllowInsert = false;
                                }
                                else
                                {
                                    response.Data.AllowInsert = statusRuleAllowed.AllowInsert;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }


        /// <summary>
        /// Validate if a Student Enrollment have attendance and/or grades posted
        /// </summary>
        /// <param name="input">The SystemStatusChangeModel Input model </param>
        /// <returns>
        ///  An object of the <see cref="Response&lt;bool&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> ValidateAttendanceAndGrades(SystemStatusChangeModel input)
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;
            try
            {
                if (string.IsNullOrEmpty(input.CampusId))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Campus is a required field!";
                }
                else if (string.IsNullOrEmpty(input.StudentEnrollmentId))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Student Enrollment is a required field!";
                }
                else if (string.IsNullOrEmpty(input.UserId))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "User is a required field!";
                }

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    var enrollment = this.repository.Query<Enrollment>().FirstOrDefault(
                            n => n.ID == Guid.Parse(input.StudentEnrollmentId));
                    var campus = this.repository.Query<Campus>().FirstOrDefault(x => x.ID == Guid.Parse(input.CampusId));
                    var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.UserId));

                    input.DateOfChange = DateTime.Now.ToString();

                    if (enrollment != null && campus != null && user != null)
                    {
                        if (input.StatusId == (int)Enumerations.eSystemStatus.NoStart)
                        {
                            NoStart noStart = new NoStart(input, this.repository, this.repositoryWithInt);
                            response = noStart.ValidateAttendanceAndGrades();
                        }
                        else if (input.StatusId == (int)Enumerations.eSystemStatus.FutureStart)
                        {
                            FutureStart futureStart = new FutureStart(input, this.repository, this.repositoryWithInt);
                            response = futureStart.ValidateAttendanceAndGrades();
                        }
                        else if (input.ValidationMode == (int)Common.Enumerations.eValidationMode.Delete && input.StatusId == (int)Enumerations.eSystemStatus.CurrentlyAttending)
                        {
                            StudentAttendanceBo attendanceBo = new StudentAttendanceBo(this.repository, this.repositoryWithInt);
                            bool haveAttendance = attendanceBo.HaveAttendanceOrGradesPosted(enrollment.ID);
                            response.Data = input;
                            response.Data.HaveAttendancePosted = haveAttendance;
                            if (haveAttendance)
                            {
                                response.Data.AllowInsert = false;
                                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                                response.ResponseMessage = "The change status action was not performed for the student. Please remove the attendance and/or grades before changing their status.";
                            }
                            else
                            {
                                response.Data.AllowInsert = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }


        /// <summary>
        /// Apply a System Status Change based on the Validation Mode(Insert, Edit, Delete)
        /// </summary>
        /// <param name="systemStatusChangeModel">
        /// The Input Model for the System Status Change
        /// </param>
        /// <param name="isSupportTool">
        /// The is Support Tool.
        /// </param>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        public Response<SystemStatusChangeModel> ApplySystemStatusChange(SystemStatusChangeModel systemStatusChangeModel, bool isSupportTool = true)
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            try
            {
                systemStatusChangeModel.IsSupportTool = isSupportTool;
                response = this.ValidateStatusChange(systemStatusChangeModel);
                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    systemStatusChangeModel.IsSupportTool = isSupportTool;
                    systemStatusChangeModel.DateOfChange = DateTime.Now.ToString();

                    if (!string.IsNullOrEmpty(systemStatusChangeModel.CaseNumber))
                    {
                        systemStatusChangeModel.CaseNumber = systemStatusChangeModel.CaseNumber.Replace("_", string.Empty);
                    }

                    ISystemStatusChange systemStatusChange =
                        new SystemStatusChangeFactory().GetStatusChange(systemStatusChangeModel, this.repository, this.repositoryWithInt);

                    if ((Common.Enumerations.eValidationMode)systemStatusChangeModel.ValidationMode
                        == Common.Enumerations.eValidationMode.Insert)
                    {
                        response = systemStatusChange.Insert();
                    }
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Return List of Active Users in the System. Excluding SA, 
        /// </summary>
        /// <returns>
        /// An object of the Response&lt;<see cref="IEnumerable&lt;DropDownOutputModel&gt;"/>&gt; class. 
        /// </returns>
        public Response<IEnumerable<DropDownOutputModel>> GetRequestedBy()
        {
            Response<IEnumerable<DropDownOutputModel>> response = new Response<IEnumerable<DropDownOutputModel>>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            try
            {
                var query = this.repository.Query<User>().Where(x => x.AccountActive == true && x.IsAdvantageSuperUser == false).OrderBy(x => x.FullName);
                response.Data = Mapper.Map<IEnumerable<DropDownOutputModel>>(query);
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Gets a List of Leave of Absence Reasons
        /// </summary>
        /// <returns>
        /// An object of the Response&lt;<see cref="IEnumerable&lt;DropDownOutputModel&gt;"/>&gt; class. 
        /// </returns>
        public Response<IEnumerable<DropDownOutputModel>> GetLoaReasons()
        {
            Response<IEnumerable<DropDownOutputModel>> response = new Response<IEnumerable<DropDownOutputModel>>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            try
            {
                var query = this.repository.Query<LoaReasons>().OrderBy(x => x.Description);
                response.Data = Mapper.Map<IEnumerable<DropDownOutputModel>>(query);
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Gets a List of Drop Reasons
        /// </summary>
        /// <returns>
        /// An object of the Response&lt;<see cref="IEnumerable&lt;DropDownOutputModel&gt;"/>&gt; class. 
        /// </returns>
        public Response<IEnumerable<DropDownOutputModel>> GetDroppedReasons()
        {
            Response<IEnumerable<DropDownOutputModel>> response = new Response<IEnumerable<DropDownOutputModel>>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            try
            {
                var query = this.repository.Query<DropReason>().OrderBy(x => x.Description);
                response.Data = Mapper.Map<IEnumerable<DropDownOutputModel>>(query);
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Gets a List of Delete Reasons
        /// </summary>
        /// <returns>
        /// An object of the Response&lt;<see cref="IEnumerable&lt;DropDownOutputModel&gt;"/>&gt; class. 
        /// </returns>
        public Response<IEnumerable<DropDownOutputModel>> GetDeleteReasons()
        {
            Response<IEnumerable<DropDownOutputModel>> response = new Response<IEnumerable<DropDownOutputModel>>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            try
            {
                var query = this.repository.Query<StatusChangeDeleteReasons>().OrderBy(x => x.Description);
                response.Data = Mapper.Map<IEnumerable<DropDownOutputModel>>(query);
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// The validate delete status change.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        public Response<SystemStatusChangeModel> ValidateDelete(SystemStatusChangeModel input)
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;
            try
            {
                if (string.IsNullOrEmpty(input.StudentEnrollmentId))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Student Enrollment is a required field!";
                }
                else if (input.StatusChangeHistoryId == null)
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Status Change Id is a required field!";
                }
                else if (input.StatusChangeHistoryId.Value == Guid.Empty)
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Status Change Id is a required field!";
                }

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    var enrollment = this.repository.Query<Enrollment>().FirstOrDefault(n => n.ID == Guid.Parse(input.StudentEnrollmentId));
                    if (enrollment != null)
                    {
                        var history = this.repository.Query<SystemStatusChangeHistory>().Where(x => x.Enrollment.ID == enrollment.ID).ToList();

                        List<SystemStatusChangeHistoryModel> statusChangeHistory = Mapper.Map<List<SystemStatusChangeHistoryModel>>(history);

                        SystemStatusChangeHistoryModel statusToDelete = statusChangeHistory.FirstOrDefault(x => x.ID == input.StatusChangeHistoryId.Value);

                        if (statusToDelete != null)
                        {
                            /*Validate is not trying to delete the first instance of future start*/
                            if (statusToDelete.NewStatus != null && statusToDelete.NewStatus.SystemStatus.ID == (int)Enumerations.eSystemStatus.FutureStart)
                            {
                                List<SystemStatusChangeHistoryModel> futureStartStatuses = statusChangeHistory.Where(x => x.NewStatus != null && x.NewStatus.SystemStatus.ID == (int)Enumerations.eSystemStatus.FutureStart).OrderBy(x => x.DateOfChange).ThenBy(x => x.ModifiedDate).ToList();
                                if (futureStartStatuses.IndexOf(statusToDelete) == 0)
                                {
                                    if (futureStartStatuses.Count > 1)
                                    {
                                        if (futureStartStatuses[0].DateOfChange != futureStartStatuses[1].DateOfChange
                                            || futureStartStatuses[0].ModifiedDate
                                            != futureStartStatuses[1].ModifiedDate)
                                        {
                                            response.ResponseCode = (int)HttpStatusCode.BadRequest;
                                            response.ResponseMessage =
                                                "This status cannot be deleted. Histories must contain an initial Future Start status!";
                                        }
                                    }
                                    else
                                    {
                                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                                        response.ResponseMessage = "This status cannot be deleted. Histories must contain an initial Future Start status!";
                                    }
                                }
                            }
                            else if (statusToDelete.NewStatus != null
                                     && statusToDelete.NewStatus.SystemStatus.ID
                                     == (int)Enumerations.eSystemStatus.CurrentlyAttending)
                            {
                                List<SystemStatusChangeHistoryModel> currentlyAttending = statusChangeHistory.Where(x => x.NewStatus != null && x.NewStatus.SystemStatus.ID == (int)Enumerations.eSystemStatus.CurrentlyAttending).OrderBy(x => x.DateOfChange).ThenBy(x => x.ModifiedDate).ToList();
                                if (currentlyAttending.IndexOf(statusToDelete) == 0)
                                {
                                    if (currentlyAttending.Count > 1)
                                    {
                                        if (currentlyAttending[0].DateOfChange != currentlyAttending[1].DateOfChange
                                            || currentlyAttending[0].ModifiedDate
                                            != currentlyAttending[1].ModifiedDate)
                                        {
                                            response = this.ValidateAttendanceAndGrades(input);
                                        }
                                    }
                                    else
                                    {
                                        response = this.ValidateAttendanceAndGrades(input);
                                    }

                                    if (response.Data != null && response.Data.HaveAttendancePosted)
                                    {
                                        response.ResponseMessage =
                                            "This status cannot be deleted. The selected student has attendance or grade posted postings for this enrollment.";
                                    }
                                }
                            }

                            if (response.ResponseCode == (int)HttpStatusCode.OK)
                            {
                                if (response.Data == null)
                                {
                                    response.Data = new SystemStatusChangeModel();
                                }

                                response.Data.AllowDelete = true;
                            }
                        }
                        else
                        {
                            response.ResponseCode = (int)HttpStatusCode.BadRequest;
                            response.ResponseMessage = "Unable to find Status Change!";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// The validate delete status change sequence.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        public Response<SystemStatusChangeModel> ValidateDeleteSequence(SystemStatusChangeModel input)
        {

            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;
            try
            {
                response.Data = input;
                var enrollment = this.repository.Query<Enrollment>().FirstOrDefault(
                          n => n.ID == Guid.Parse(input.StudentEnrollmentId));
                var campus = this.repository.Query<Campus>().FirstOrDefault(x => x.ID == Guid.Parse(input.CampusId));
                var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == Guid.Parse(input.UserId));
                if (enrollment != null && campus != null && user != null && input.StatusChangeHistoryId != null)
                {
                    var statusChangeRules = this.repositoryWithInt.Query<SystemStatusWorkflowRules>().AsQueryable();

                    var history = this.repository.Query<SystemStatusChangeHistory>()
                            .Where(x => x.Enrollment.ID == enrollment.ID)
                            .OrderBy(x => x.ModifiedDate)
                            .ToList();

                    List<SystemStatusChangeHistoryModel> statusChangeHistory = Mapper.Map<List<SystemStatusChangeHistoryModel>>(history);

                    SystemStatusChangeHistoryModel statusToDelete = statusChangeHistory.Where(x => x.ID == input.StatusChangeHistoryId.Value).OrderByDescending(x => x.DateOfChange).ThenByDescending(x => x.ModifiedDate).FirstOrDefault();

                    if (statusToDelete != null)
                    {
                        Guid statusCode = Guid.Empty;

                        if (statusToDelete.NewStatus != null)
                        {
                            statusCode = statusToDelete.NewStatus.ID;
                        }

                        SystemStatusChangeHistoryModel lastStatusChange = statusChangeHistory.Where(x => x.DateOfChange < statusToDelete.DateOfChange).OrderByDescending(x => x.DateOfChange).ThenByDescending(x => x.ModifiedDate).FirstOrDefault();
                        SystemStatusChangeHistoryModel nextStatusInSequence = statusChangeHistory.Where(x => x.DateOfChange > statusToDelete.DateOfChange).OrderByDescending(x => x.DateOfChange).ThenByDescending(x => x.ModifiedDate).FirstOrDefault();

                        if (statusToDelete.NewStatus != null && lastStatusChange != null
                            && lastStatusChange.NewStatus != null)
                        {
                            SystemStatusWorkflowRules statusRuleAllowed =
                                statusChangeRules.FirstOrDefault(
                                    x =>
                                    x.StatusFrom.ID == statusToDelete.NewStatus.SystemStatus.ID
                                    && x.StatusTo.ID == lastStatusChange.NewStatus.SystemStatus.ID);

                            if (statusRuleAllowed != null)
                            {
                                response.Data.AllowDelete = statusRuleAllowed.AllowDelete;
                                if (response.Data.AllowDelete)
                                {
                                    statusChangeHistory.Remove(statusToDelete);
                                    input.ValidationMode = (int)Common.Enumerations.eValidationMode.Insert;
                                    response = this.ValidateStatusChangeSequence(input, statusChangeHistory, statusCode, statusChangeRules, lastStatusChange.ID);
                                    response.Data.AllowDelete = true;
                                }
                            }
                            else
                            {
                                response.Data.AllowDelete = true;
                                response.Data.ShouldConfirmAction = true;
                            }
                        }
                        else
                        {
                            response = this.ValidateStatusChangeSequence(input, statusChangeHistory, statusCode, statusChangeRules, statusToDelete.ID);
                            if (response.Data.AllowDelete)
                            {
                                statusChangeHistory.Remove(statusToDelete);

                                string statusMessage = response.ResponseMessage;
                                input.ValidationMode = (int)Common.Enumerations.eValidationMode.Insert;

                                statusCode = Guid.Empty;
                                Guid statusChangeId = Guid.Empty;

                                if (nextStatusInSequence != null)
                                {
                                    statusChangeId = nextStatusInSequence.ID;
                                    if (nextStatusInSequence.NewStatus != null)
                                    {
                                        statusCode = nextStatusInSequence.NewStatus.ID;
                                    }
                                }
                                else if (lastStatusChange != null)
                                {
                                    statusChangeId = lastStatusChange.ID;
                                    if (lastStatusChange.NewStatus != null)
                                    {
                                        statusCode = lastStatusChange.NewStatus.ID;
                                    }
                                }

                                response = this.ValidateStatusChangeSequence(input, statusChangeHistory, statusCode, statusChangeRules, statusChangeId);
                                response.Data.AllowDelete = true;
                                response.ResponseMessage = statusMessage + response.ResponseMessage;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// The delete System Status Change History.
        /// </summary>
        /// <param name="systemStatusChangeModel">
        /// The System Status Change to delete.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        public Response<SystemStatusChangeModel> Delete(SystemStatusChangeModel systemStatusChangeModel)
        {
            Response<SystemStatusChangeModel> response = this.ValidateDelete(systemStatusChangeModel);
            try
            {
                if (response != null)
                {
                    if (response.ResponseCode == (int)HttpStatusCode.OK)
                    {
                        systemStatusChangeModel.IsSupportTool = true;
                        systemStatusChangeModel.DateOfChange = DateTime.Now.ToString();

                        if (!string.IsNullOrEmpty(systemStatusChangeModel.CaseNumber))
                        {
                            systemStatusChangeModel.CaseNumber = systemStatusChangeModel.CaseNumber.Replace("_", string.Empty);
                        }

                        /*This is to enable the system to delete an status change if the newStatusId is null which cause the statusId to be 0 because no current status code was found.*/
                        if (systemStatusChangeModel.StatusId == 0)
                        {
                            systemStatusChangeModel.StatusId = (int)Enumerations.eSystemStatus.CurrentlyAttending;
                        }

                        ISystemStatusChange systemStatusChange =
                            new SystemStatusChangeFactory().GetStatusChange(systemStatusChangeModel, this.repository, this.repositoryWithInt);

                        if ((Common.Enumerations.eValidationMode)systemStatusChangeModel.ValidationMode
                            == Common.Enumerations.eValidationMode.Delete)
                        {
                            response = systemStatusChange.Delete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (response == null)
                {
                    response = new Response<SystemStatusChangeModel>();
                }

                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Validates the Input Model required Fields
        /// </summary>
        /// <param name="input">The Input Model for the System Status Change</param>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class. 
        /// </returns>
        private Response<SystemStatusChangeModel> ValidateStatusChangeModelRequiredFields(SystemStatusChangeModel input)
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;
            if (string.IsNullOrEmpty(input.CampusId))
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Campus is a required field!";
            }
            else if (string.IsNullOrEmpty(input.StudentEnrollmentId))
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Student Enrollment is a required field!";
            }
            else if (string.IsNullOrEmpty(input.UserId))
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "User is a required field!";
            }
            else if (string.IsNullOrEmpty(input.EffectiveDate))
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Effective Date is a required field!";
            }
            else
            {
                DateTime date;
                if (!DateTime.TryParse(input.EffectiveDate, out date))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Effective Date have an invalid format!";
                }
                else if (DateTime.Parse(input.EffectiveDate) > DateTime.Now)
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Cannot add a Status Change with a Future Effective Date!";
                }
            }

            if (string.IsNullOrEmpty(input.DateOfChange))
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Date of Change is a required field!";
            }
            else
            {
                DateTime date;
                if (!DateTime.TryParse(input.DateOfChange, out date))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Date of Change have an invalid format!";
                }
            }
            if (input.IsSupportTool)
            {
                if (string.IsNullOrEmpty(input.CaseNumber))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Case Number is a required field!";
                }
                else if (string.IsNullOrEmpty(input.RequestedBy))
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Requested By is a required field!";
                }
            }

            if (string.IsNullOrEmpty(input.PostedBy))
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Posted By is a required field!";
            }
            else if (Guid.Empty == input.StatusCodeId)
            {
                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                response.ResponseMessage = "Status is a required field!";
            }

            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
                if (input.StatusId == (int)Enumerations.eSystemStatus.Suspension
                    || input.StatusId == (int)Enumerations.eSystemStatus.LeaveOfAbsence
                    || input.StatusId == (int)Enumerations.eSystemStatus.Dropped)
                {
                    if (string.IsNullOrEmpty(input.Reason))
                    {
                        response.ResponseCode = (int)HttpStatusCode.BadRequest;
                        response.ResponseMessage = "Reason is a required field!";
                    }
                    else
                    {
                        if (input.StatusId == (int)Enumerations.eSystemStatus.Suspension
                            || input.StatusId == (int)Enumerations.eSystemStatus.LeaveOfAbsence)
                        {
                            if (string.IsNullOrEmpty(input.EndDate))
                            {
                                response.ResponseCode = (int)HttpStatusCode.BadRequest;
                                response.ResponseMessage = "End Date is a required field!";
                            }
                            else
                            {
                                DateTime format;
                                if (!DateTime.TryParse(input.EndDate, out format))
                                {
                                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                                    response.ResponseMessage = "End Date have an invalid format!";
                                }
                            }
                        }
                    }
                }
            }

            return response;
        }


        /// <summary>
        /// The validate status change sequence.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <param name="statusChangeHistory">
        /// The status change history.
        /// </param>
        /// <param name="statusCodeId">
        /// The status code id.
        /// </param>
        /// <param name="statusChangeRules">
        /// The status change workflow rules rules.
        /// </param>
        /// <param name="statusChangeId">
        /// The status Change Id that was inserted or deleted.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        private Response<SystemStatusChangeModel> ValidateStatusChangeSequence(SystemStatusChangeModel input, List<SystemStatusChangeHistoryModel> statusChangeHistory, Guid statusCodeId, IQueryable<SystemStatusWorkflowRules> statusChangeRules, Guid statusChangeId)
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            response.ResponseCode = (int)HttpStatusCode.OK;

            response.Data = input;
            SystemStatusChangeHistoryModel previewsChange = null;
            List<SystemStatusChangeHistoryModel> brokenStatus = new List<SystemStatusChangeHistoryModel>();
            List<SystemStatusChangeHistoryModel> brokenPreviousStatus = new List<SystemStatusChangeHistoryModel>();
            UserDefStatusCode statusToValidate = null;
            bool shouldValidate = false;
            List<SystemStatusChangeHistoryModel> sortedStatusChangeHistory = statusChangeHistory.OrderBy(x => x.DateOfChange).ThenBy(x => x.ModifiedDate).ToList();

            foreach (var item in sortedStatusChangeHistory)
            {
                if (item.NewStatus != null)
                {
                    if (previewsChange == null)
                    {
                        previewsChange = item;
                        statusToValidate = item.NewStatus;
                        if (item.NewStatus.ID == statusCodeId)
                        {
                            shouldValidate = true;
                        }
                    }
                    else
                    {
                        statusToValidate = item.NewStatus;
                        shouldValidate = true;
                    }
                }
                else if ((Common.Enumerations.eValidationMode)input.ValidationMode
                         == Common.Enumerations.eValidationMode.Insert)
                {
                    shouldValidate = false;
                    response.Data.AllowInsert = true;
                    brokenStatus.Add(item);
                    brokenPreviousStatus.Add(previewsChange);
                }
                else if ((Common.Enumerations.eValidationMode)input.ValidationMode
                         == Common.Enumerations.eValidationMode.Delete)
                {
                    shouldValidate = false;
                    response.Data.AllowDelete = true;
                    brokenStatus.Add(item);
                    brokenPreviousStatus.Add(previewsChange);
                }

                if (shouldValidate)
                {
                    SystemStatusWorkflowRules statusRuleAllowed =
                        statusChangeRules.FirstOrDefault(
                            x =>
                            x.StatusFrom.ID == previewsChange.NewStatus.SystemStatus.ID
                            && x.StatusTo.ID == statusToValidate.SystemStatus.ID);

                    switch ((Common.Enumerations.eValidationMode)input.ValidationMode)
                    {
                        case Common.Enumerations.eValidationMode.Insert:
                            {
                                if (statusRuleAllowed == null)
                                {
                                    brokenStatus.Add(item);
                                    brokenPreviousStatus.Add(previewsChange);
                                    if (item.ID == statusChangeId)
                                    {
                                        response.Data.AllowInsert = true;
                                    }
                                }
                                else
                                {
                                    if (previewsChange.NewStatus.SystemStatus.ID == item.NewStatus.SystemStatus.ID)
                                    {
                                        response.Data.AllowInsert = statusRuleAllowed.AllowInsertSameStatusType;
                                        if (!response.Data.AllowInsert)
                                        {
                                            brokenStatus.Add(item);
                                            brokenPreviousStatus.Add(previewsChange);
                                        }
                                        else
                                        {
                                            if (previewsChange.NewStatus.ID == item.NewStatus.ID)
                                            {
                                                brokenStatus.Add(item);
                                                brokenPreviousStatus.Add(previewsChange);
                                            }
                                        }
                                    }
                                    else if (item.ID == statusChangeId)
                                    {
                                        response.Data.AllowInsert = statusRuleAllowed.AllowInsert;
                                        if (!statusRuleAllowed.AllowInsert)
                                        {
                                            brokenStatus.Add(item);
                                            brokenPreviousStatus.Add(previewsChange);
                                        }
                                    }
                                }

                                break;
                            }

                        case Common.Enumerations.eValidationMode.Edit:
                            {
                                //// Edit Functionality was leave outsite of scope
                                response.Data.AllowEdit = false;
                                /*
                                if (statusRuleAllowed == null)
                                {
                                    brokenStatus.Add(item);
                                    brokenPreviousStatus.Add(previewsChange);
                                    if (item.ID == newStatusChangeId)
                                    {
                                        response.Data.AllowEdit = true;
                                    }
                                }
                                else
                                {
                                    if (item.ID == newStatusChangeId)
                                    {
                                        response.Data.AllowEdit = statusRuleAllowed.AllowEdit;
                                    }
                                }
                                */
                                break;
                            }

                        case Common.Enumerations.eValidationMode.Delete:
                            {
                                if (statusRuleAllowed == null)
                                {
                                    brokenStatus.Add(item);
                                    brokenPreviousStatus.Add(previewsChange);
                                    if (item.ID == statusChangeId)
                                    {
                                        response.Data.AllowDelete = true;
                                    }
                                }
                                else
                                {
                                    if (item.ID == statusChangeId)
                                    {
                                        response.Data.AllowDelete = statusRuleAllowed.AllowDelete;
                                        if (!statusRuleAllowed.AllowDelete)
                                        {
                                            brokenStatus.Add(item);
                                            brokenPreviousStatus.Add(previewsChange);
                                        }
                                    }
                                }

                                break;
                            }
                    }
                }

                if (item.NewStatus != null)
                {
                    previewsChange = item;
                }
            }

            if (brokenStatus.Count > 0 && (response.Data.AllowInsert || response.Data.AllowEdit || response.Data.AllowDelete))
            {
                response.Data.ShouldConfirmAction = true;
            }
            else
            {
                if (!response.Data.AllowInsert && !response.Data.AllowEdit && !response.Data.AllowDelete)
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage =
                        "Status Change is not allowed. Please correct the status sequence for the following statuses: <br/> <br/>";

                    for (int i = 0; i <= brokenStatus.Count - 1; i++)
                    {
                        if (brokenPreviousStatus[i] != null)
                        {
                            response.ResponseMessage += " Previous Status: ";
                            response.ResponseMessage += brokenPreviousStatus[i].NewStatus != null
                                                            ? brokenPreviousStatus[i].NewStatus.Description
                                                            : string.Empty;
                            response.ResponseMessage += "<br/>";
                        }

                        if (brokenStatus[i] != null)
                        {
                            response.ResponseMessage += " New Status: ";
                            response.ResponseMessage += brokenStatus[i].NewStatus != null
                                                            ? brokenStatus[i].NewStatus.Description
                                                            : string.Empty;
                            response.ResponseMessage += "<br/>";
                        }

                        response.ResponseMessage += "<br/>";
                    }
                }
            }

            return response;
        }
    }
}
