﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationAppSettings.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Configuration Settings business rules...
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff
{
    using System.Collections.Generic;
    using System.Linq;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings;

    /// <summary>
    /// Configuration Settings business rules...
    /// </summary>
    public static class ConfigurationAppSettings
    {
        /// <summary>
        /// Return the configuration settings given a key-name.
        /// </summary>
        /// <param name="keyName">The Setting name</param>
        /// <param name="repository">The repository</param>
        /// <returns>The key-value, if return null the key doesn't exist</returns>
        public static string Setting(string keyName, IRepositoryWithTypedID<int> repository)
        {
            var configurationAppSetting =
                repository.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == keyName);
            if (configurationAppSetting != null)
            {
                var setting = configurationAppSetting.ConfigurationValues[0].Value;
                return setting;
            }

            return null;
        }

        /// <summary>
        /// The setting by campus.
        /// Use this only in settings associated with campus.
        /// </summary>
        /// <param name="keyName">
        /// The key name.
        /// </param>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;ConfigurationAppSettingValues&gt;"/>.
        /// That is the setting value and the Campus Associated
        /// </returns>
        public static IList<ConfigurationAppSettingValues> SettingByCampus(string keyName, IRepositoryWithTypedID<int> repository)
        {
            var configurationAppSetting =
                repository.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == keyName);
            if (configurationAppSetting != null)
            {
                var setting = configurationAppSetting.ConfigurationValues;
                return setting;
            }

            return null;
        }

        /// <summary>
        /// Store the setting string in DB
        /// </summary>
        /// <param name="keyName">The name of the configuration setting key</param>
        /// <param name="repository">The repository</param>
        /// <param name="value">The value to be stored</param>
        /// <returns>Success or setting not found string</returns>
        public static string StoreSettingString(string keyName, IRepositoryWithTypedID<int> repository, string value)
        {
            var configurationAppSetting =
                repository.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == keyName);
            if (configurationAppSetting != null)
            {
                configurationAppSetting.ConfigurationValues[0].Value = value;
                repository.SaveAndFlush(configurationAppSetting);
                return "Success";
            }

            return "Setting Not Found";
        }
    }
}