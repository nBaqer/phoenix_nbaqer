﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentStatusBo.cs" company="FAME Inc">
//  FAME Inc 2016 
//  FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.DocumentStatusBo 
// </copyright>
// <summary>
//   Defines the DocumentStatusBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using AutoMapper;

    using Domain.Infrastructure.Entities;

    using FAME.Advantage.Domain.Enumerations;
    using FAME.Advantage.Domain.Requirements.Documents;
    using FAME.Advantage.Messages.Common;

    /// <summary>
    /// System Document Status Business Object
    /// </summary>
    public class DocumentStatusBo 
    {
        /// <summary>
        /// The nHibernate repository
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repository;

        /// <summary>
        /// The nHibernate repository for entities with identity of type integer
        /// </summary>
        private readonly IRepositoryWithTypedID<Guid> repositoryWithGuid;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentStatusBo"/> class. 
        /// </summary>
        /// <param name="repository">
        /// The nHibernate repository
        /// </param>
        /// <param name="repositoryWithGuid">
        /// The nHibernate repository for entities with identity of type integer
        /// </param>
        public DocumentStatusBo(IRepositoryWithTypedID<int> repository, IRepositoryWithTypedID<Guid> repositoryWithGuid)
        {
            this.repository = repository;
            this.repositoryWithGuid = repositoryWithGuid;
        }
        
        /// <summary>
        /// Get All Document Statuses
        /// </summary>
        /// <returns>
        /// An IEnumerable of Document Status
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetAll()
        {
            var query = this.repositoryWithGuid.Query<DocumentStatus>().OrderBy(x => x.Description);
            return Mapper.Map<IEnumerable<DropDownOutputModel>>(query);
        }

        /// <summary>
        /// Get All Document Statuses
        /// </summary>
        /// <param name="status">
        /// The status.
        /// </param>
        /// <returns>
        /// An IEnumerable of Document Status
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetAllByStatus(Enumerations.EStatus status)
        {
            var query = this.repositoryWithGuid.Query<DocumentStatus>().Where(x => x.SystemDocumentStatus.ID == (int)status);
            return Mapper.Map<IEnumerable<DropDownOutputModel>>(query);
        }

        /// <summary>
        /// Find a document status based on it's Id
        /// </summary>
        /// <param name="id">
        /// The id of the document status
        /// </param>
        /// <returns>
        /// The document status found otherwise returns null
        /// </returns>
        public DocumentStatus FindById(Guid id)
        {
            return this.repositoryWithGuid.Query<DocumentStatus>().FirstOrDefault(x => x.ID == id);
        }
    }
}
