﻿namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff
{
    using System;
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Security.Permissions;
    using System.Security.Principal;

    /// <summary>
    /// This class can be used to impersonate a defined user in process that require 
    /// a higher level of security than the user context has.
    /// Used in Start Stop Service.
    /// </summary>
    public class WrapperImpersonationContext
    {
        /// <summary>
        /// The log-on user.
        /// </summary>
        /// <param name="lpszUsername">
        /// The LPSZ user name.
        /// </param>
        /// <param name="lpszDomain">
        /// The LPSZ domain.
        /// </param>
        /// <param name="lpszPassword">
        /// The LPSZ password.
        /// </param>
        /// <param name="dwLogonType">
        /// The DW log-on type.
        /// </param>
        /// <param name="dwLogonProvider">
        /// The DW log-on provider.
        /// </param>
        /// <param name="phToken">
        /// The PH token.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern bool LogonUser(
            string lpszUsername,
            string lpszDomain,
        string lpszPassword,
        int dwLogonType,
        int dwLogonProvider,
        ref IntPtr phToken);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public extern static bool CloseHandle(IntPtr handle);

        private const int LOGON32_PROVIDER_DEFAULT = 0;
        private const int LOGON32_LOGON_INTERACTIVE = 2;

        private readonly string mDomain;
        private readonly string mPassword;
        private readonly string mUsername;
        private IntPtr mToken;

        private WindowsImpersonationContext mContext;


        protected bool IsInContext
        {
            get { return this.mContext != null; }
        }

        /// <summary>
        /// Impersonator context constructor.
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public WrapperImpersonationContext(string domain, string username, string password)
        {
            this.mDomain = domain;
            this.mUsername = username;
            this.mPassword = password;
        }

        /// <summary>
        /// Use this to begin the impersonate. All instruction before this use
        /// the new context.
        /// </summary>
        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public void Enter()
        {
            if (this.IsInContext) return;
            this.mToken = new IntPtr(0);
            try
            {
                this.mToken = IntPtr.Zero;
                bool logonSuccessfull = LogonUser(
                    this.mUsername,
                    this.mDomain, 
                    this.mPassword,
                   LOGON32_LOGON_INTERACTIVE,
                   LOGON32_PROVIDER_DEFAULT,
                   ref this.mToken);
                if (logonSuccessfull == false)
                {
                    int error = Marshal.GetLastWin32Error();
                    throw new Win32Exception(error);
                }

                var identity = new WindowsIdentity(this.mToken);
                this.mContext = identity.Impersonate();
            }
            catch (Exception exception)
            {
                throw new ApplicationException($"Exception in Enter impersonator context: {exception.Message} ", exception);
            }
        }


        /// <summary>
        /// Finish the impersonation.
        /// </summary>
        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public void Leave()
        {
            if (this.IsInContext == false)
            {
                return;
            }

            this.mContext.Undo();

            if (this.mToken != IntPtr.Zero)
            {
                CloseHandle(this.mToken);
            }
            this.mContext = null;
        }
    }
}
