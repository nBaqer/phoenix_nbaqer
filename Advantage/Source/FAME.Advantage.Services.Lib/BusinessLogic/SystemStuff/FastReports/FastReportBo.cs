﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FastReportBo.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the FastReportBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.FastReports
{
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using Domain.Infrastructure.Entities;
    using Messages.SystemStuff.Reports;

    using Telerik.Web.UI;

    /// <summary>
    /// The fast report bo.
    /// </summary>
    public class FastReportBo
    {
        // ReSharper disable NotAccessedField.Local
        
        /// <summary>
        /// The repository integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryInt;

        /// <summary>
        /// The repository GUID.
        /// </summary>
        private readonly IRepository repository;
        // ReSharper restore NotAccessedField.Local

        /// <summary>
        /// Initializes a new instance of the <see cref="FastReportBo"/> class. 
        /// </summary>
        /// <param name="repository">
        /// repository GUID
        /// </param>
        /// <param name="repositoryInt">
        /// repository integer
        /// </param>
        public FastReportBo(IRepository repository, IRepositoryWithTypedID<int> repositoryInt)
        {
            this.repository = repository;
            this.repositoryInt = repositoryInt;
        }

        ///// <summary>
        ///// Query the report server to get the report info in a specific directory
        ///// </summary>
        ///// <param name="queryString"></param>
        ///// <param name="username"></param>
        ///// <param name="password"></param>
        ///// <returns></returns>
        ////public string GetResultFromSsrServer(string queryString, string username, string password)
        ////{
        ////    var servicesUrl = queryString;
        ////    CredentialCache credentialCache = new CredentialCache
        ////    {
        ////        {new Uri(servicesUrl), "NTLM", new NetworkCredential(username, password)}
        ////    };
        ////    var handler = new HttpClientHandler { Credentials = credentialCache };
        ////    var client = new HttpClient(handler);
        ////    var request = new HttpRequestMessage
        ////    {
        ////        RequestUri = new Uri(servicesUrl),
        ////        Method = HttpMethod.Get
        ////    };
        ////    string response = string.Empty;
        ////    HttpResponseMessage httpResponse = null;
        ////    var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
        ////     {
        ////         httpResponse = taskwithmsg.Result;
        ////         Debug.WriteLine(httpResponse.ToString());
        ////         Debug.WriteLine(response);
        ////         response = httpResponse.Content.ReadAsStringAsync().Result;
        ////     });
        ////    task.Wait();
        ////    if (httpResponse != null && httpResponse.StatusCode != HttpStatusCode.OK)
        ////    {
        ////        throw new Exception(string.Format("SSRS Reason: {0}", httpResponse.ReasonPhrase));
        ////    }
        ////    return response;
        ////}

        ///// <summary>
        ///// Get the tree node for the ssrs folder base
        ///// </summary>
        ///// <param name="reports">The parent node</param>
        ///// <param name="ssrsAddress">the basic address of SSRS Server (http://... somethings)</param>
        ///// <param name="ssrsfolderbase">the folder where we begin the search</param>
        ///// <param name="username">An Authorized user name</param>
        ///// <param name="password">An Authorized password</param>
        ///// <returns></returns>
        ////public ReportInformation GetNodeTreeFromSsrs(ReportInformation reports, string ssrsAddress, string ssrsfolderbase, string username, string password)
        ////{
        ////    var ssrscommand = "ListChildren";
        ////    var realAddress = ssrsAddress;
        ////    string queryString;
        ////    ssrsfolderbase = ssrsfolderbase ?? string.Empty;
        ////    if (ssrsfolderbase.Contains("Command"))
        ////    {
        ////        queryString = string.Format("{0}{1}", realAddress, ssrsfolderbase);
        ////    }
        ////    else
        ////    {
        ////        queryString = string.Format("{0}?/{1}&rs:Command={2}", realAddress, ssrsfolderbase,
        ////       ssrscommand);
        ////    }

        ////    var html = GetResultFromSsrServer(queryString, username, password);
        ////    var ini = html.IndexOf("<title>", StringComparison.Ordinal);
        ////    var fin = html.IndexOf("</title>", StringComparison.Ordinal);
        ////    var firstName = html.Substring(ini + 7, (fin - ini) - 7);

        ////    ini = html.IndexOf("<pre>", StringComparison.Ordinal);
        ////    fin = html.IndexOf("</pre>", StringComparison.Ordinal);

        ////    XmlDocument doc = new XmlDocument();
        ////    doc.LoadXml(html.Substring(ini, (fin - ini) + 6));

        ////    var links = doc.GetElementsByTagName("A");
        ////    foreach (XmlNode link in links)
        ////    {
        ////        var rep = new ReportInformation { Name = link.InnerText };
        ////        rep.ChildrenList = new List<ReportInformation>();
        ////        if (link.Attributes != null) rep.Address = link.Attributes["HREF"].Value;
        ////        if (!rep.Name.Contains("[To Parent Directory]"))
        ////        {
        ////            var children = (rep.Address.Contains("ListChildren"))
        ////        ? GetNodeTreeFromSsrs(rep, ssrsAddress, rep.Address, username, password)
        ////        : null;
        ////            if (children != null)
        ////            {
        ////                rep.ChildrenList.Add(children);
        ////            }
        ////            reports.ChildrenList.Add(rep);
        ////        }
        ////    }
        ////    return reports;
        ////}

        /// <summary>
        /// Construct The node Tree
        /// </summary>
        /// <param name="output">The input parameters object</param>
        /// <param name="conexionString">The connection string with the report server</param>
        /// <param name="ssrsfolderbase">The initial folder to read the reports</param>
        /// <returns>The list of the report in the SSRS below the given the initial directory</returns>
        public IReportOutputModel ConstructNodeTree(IReportOutputModel output, string conexionString, string ssrsfolderbase)
        {
            var conn = new SqlConnection(conexionString);
            var sql = "SELECT [itemID], [Path] ,[Name] ,[ParentID],[Type], [Description], [Hidden] FROM [Catalog]";
            var sqlcomm = new SqlCommand(sql, conn);
            IList<ReportInformation> list = new List<ReportInformation>();
            try
            {
                conn.Open();
                var reader = sqlcomm.ExecuteReader();
                while (reader.Read())
                {
                    var node = new ReportInformation();

                    node.Id = reader.GetGuid(0).ToString();
                    node.Address = reader.GetString(1);
                    node.Name = reader.GetString(2);
                    node.ParentId = reader["ParentID"] == null ? string.Empty : reader["ParentID"].ToString();
                    node.Type = reader.GetInt32(4);
                    node.Description = reader.IsDBNull(5) ? string.Empty : reader.GetString(5);
                    node.Hidden = reader.IsDBNull(6) ? true : reader.GetBoolean(6);
       
                    // Only add nodes type 1 and 2 (folder and reports)
                    if ((node.Type == 2 || node.Type == 1) & node.Hidden == false)
                    {
                        list.Add(node);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            var lookup = list.ToLookup(x => x.ParentId);
            foreach (ReportInformation helper in list)
            {
                var subitems = lookup[helper.Id].ToList();
                helper.ChildrenList = subitems;
            }

            // Get your root element
            var rootdir = string.IsNullOrWhiteSpace(ssrsfolderbase) ? string.Empty : ssrsfolderbase;

            if (rootdir == string.Empty)
            {
                // Get all information............................................
                var root = lookup[rootdir].ToList();
                if (root.Count > 0 && string.IsNullOrEmpty(root[0].Address))
                {
                    root[0].Name = "Reports List";
                    output.ReportInfo = root;
                }
            }
            else
            {
                // Get only filtered by root dir
                foreach (ReportInformation info in list)
                {
                    if (info.Name == rootdir)
                    {
                        output.ReportInfo = new List<ReportInformation> { info };
                    }
                }
            }

            return output;
        }
    }
}