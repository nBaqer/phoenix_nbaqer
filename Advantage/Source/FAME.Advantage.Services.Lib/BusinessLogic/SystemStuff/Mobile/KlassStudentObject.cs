﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassStudentObject.cs" company="FAME">
//   2016
// </copyright>
// <summary>
// JAGG -  The <code>klassApp</code> student object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Mobile
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.Messages.SystemStuff.ConfigurationAppSettings;
    using FAME.Advantage.Services.Lib.BusinessLogic.Wapi;

    /// <summary>
    /// The <code>klassApp</code> student object.
    /// </summary>
    public class KlassStudentObject : BaseKlassObject, IKlassStudentObject
    {
        /// <summary>
        /// Gets or sets the <code>klass</code> post student info.
        /// </summary>
        public string KlassPostStudentInfo { get; set; }

        /// <summary>
        /// Gets or sets the campus active GUIDs.
        /// </summary>
        public IList<Guid> CampusActiveGuidList { get; set; }

        /// <summary>
        /// Gets or sets the <code>klassApp</code> days to considerer
        /// to take student with modifications
        /// </summary>
        public int KlassAppDaysToConsiderer { get; set; }

        /// <summary>
        /// The factory.
        /// </summary>
        /// <returns>
        /// The <see cref="IKlassStudentObject"/>.
        /// </returns>
        public static IKlassStudentObject Factory()
        {
            var fac = new KlassStudentObject();
            fac.CampusActiveGuidList = new List<Guid>();
            return fac;
        }
    }
}