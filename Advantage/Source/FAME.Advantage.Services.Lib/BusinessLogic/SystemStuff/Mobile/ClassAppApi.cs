﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassAppApi.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The <code>klass_app</code> API.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Mobile
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Net;
    using System.Net.Http;

    using FAME.Advantage.Messages.SystemStuff.Mobile.ApiKlass;

    /// <summary>
    /// The <code>klass app</code> API.
    /// </summary>
    public class ClassAppApi
    {
         /// <summary>
        /// The operation code program version.
        /// </summary>
        public const string OperationCodeProgramVersion = "department";

        /// <summary>
        /// The operation code locations.
        /// </summary>
        public const string OperationCodeLocations = "locations";

        /// <summary>
        /// The operation student status.
        /// </summary>
        public const string OperationStudentStatus = "studentStatus";

        /////// <summary>
        /////// The post location URI portion.
        /////// </summary>
        ////public const string PostLocationUri = "locations/1.0";

        /////// <summary>
        /////// The post key value part URI portion.
        /////// </summary>
        ////public const string PostKeyValuePartUri = "keyvaluepairs/1.0";

        /////// <summary>
        /////// The post custom field URI portion
        /////// </summary>
        ////public const string PostCustomFieldUri = "customfields/1.0";

        #region Student
        ////public HttpResponseMessage PostStudentToKlassApp(SyKlassAppConfigurationSetting payload, IWapiOperation operation)
        ////{
        ////    // Sending information to KlassApp ...........................................
        ////    // Create the client
        ////    var client = new HttpClient();

        ////    // Create the headers
        ////    client.DefaultRequestHeaders.Add("auth_id", operation.ConsumerKey);
        ////    client.DefaultRequestHeaders.Add("auth_token", operation.PrivateKey);

        ////    HttpResponseMessage response = null;

        ////    // Post to Service............
        ////    var task = client.PostAsJsonAsync(operation.ExternalUrl, payload).ContinueWith(taskwithmsg =>
        ////    {
        ////        response = taskwithmsg.Result;
        ////        Debug.WriteLine("Code: {0} - Reason: {1}", response.StatusCode, response.ReasonPhrase);
        ////    });
        ////    task.Wait();

        ////    return response;
        ////}
        #endregion

        #region Get from klass_App

        /// <summary>
        /// The get all for command in class application.
        /// </summary>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <param name="url">
        /// The API URI Address
        /// </param>
        /// <param name="errorType">
        /// The error type.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        internal IGeneralResponse GetAllForCommandInClassApplication(IWapiOperation operation, Uri url, string errorType)
        {
            // Get all Custom field operation
            ////var url = operation.ExternalUrl + klassCommand; // "customfields/1.0?limit=700";

            // Create the client
            var client = this.CreateHttpClient(operation);
            HttpResponseMessage result = null;

            // Get from Service Klass_App............
            var task = client.GetAsync(url).ContinueWith(taskwithmsg =>
            {
                result = taskwithmsg.Result;
                Debug.WriteLine($"Code: {result.StatusCode} - Reason: {result.ReasonPhrase}");
            });
            task.Wait();
            var output = GeneralResponse.Factory();
            if (result.StatusCode == HttpStatusCode.Unauthorized)
            {
                output.ErrorCode = result.ReasonPhrase;
                output.Explanation =
                    "You are not authorized. Please review Authorization token in WAPI External Operations";
                return output;
            }

            var message = result.Content.ReadAsAsync(typeof(ResponseBase)).Result;
            var res = message as ResponseBase;
            if (res == null || res.response == null)
            {
                output = this.AnalysisExistenceOfError(result, res, output, errorType);
                return output;
            }

            if (res != null)
            {
                output.ObjectFromServer = res.response;
            }

            return output;
        }


        internal IGeneralResponse GetClearAllTokenFromClassApplication(IWapiOperation operation, Uri url, string errorType)
        {
            // Create the client
            var client = this.CreateHttpClient(operation);
            HttpResponseMessage result = null;

            // Get from Service Klass_App............
            var task = client.GetAsync(url).ContinueWith(taskwithmsg =>
                {
                    result = taskwithmsg.Result;
                    Debug.WriteLine($"Code: {result.StatusCode} - Reason: {result.ReasonPhrase}");
                });
            task.Wait();
            var output = GeneralResponse.Factory();
            if (result.StatusCode == HttpStatusCode.Unauthorized)
            {
                output.ErrorCode = result.ReasonPhrase;
                output.Explanation =
                    "You are not authorized. Please review Authorization token in WAPI External Operations";
                return output;
            }

            if (result.StatusCode != HttpStatusCode.OK)
            {
                output.ErrorCode = result.ReasonPhrase;
                output.Explanation = "Some Error was found";
                return output;
            }

            var message = result.Content.ReadAsAsync(typeof(ResponseBase)).Result;
            var res = message as ResponseBase;
            if (res == null || res.response == null)
            {
                output = this.AnalysisExistenceOfError(result, res, output, errorType);
                return output;
            }

            if (res != null)
            {
                output.ObjectFromServer = res.response;
            }

            return output;
        }


        public IGeneralResponse SendTokenToClassApplication(IWapiOperation operation, Uri uri, string errorType)
        {
            // Create the client
            var client = this.CreateHttpClient(operation);
            HttpResponseMessage result = null;

            // Get from Service Klass_App............
            var task = client.GetAsync(uri).ContinueWith(taskwithmsg =>
                {
                    result = taskwithmsg.Result;
                    Debug.WriteLine($"Code: {result.StatusCode} - Reason: {result.ReasonPhrase}");
                });
            task.Wait();
            var output = GeneralResponse.Factory();
            if (result.StatusCode == HttpStatusCode.Unauthorized)
            {
                output.ErrorCode = result.ReasonPhrase;
                output.Explanation =
                    "You are not authorized. Please review Authorization token in WAPI External Operations";
                return output;
            }

            if (result.StatusCode != HttpStatusCode.OK)
            {
                output.ErrorCode = result.ReasonPhrase;
                output.Explanation = "Some Error was found";
                return output;
            }

            var message = result.Content.ReadAsAsync(typeof(ResponseBase)).Result;
            var res = message as ResponseBase;
            if (res == null || res.response == null)
            {
                output = this.AnalysisExistenceOfError(result, res, output, errorType);
                return output;
            }

            if (res != null)
            {
                output.ObjectFromServer = res.response;
            }

            return output;
        }



        #endregion

        #region Post (insert) in Klass_App
        /// <summary>
        /// The post location to <code>klass_app</code>.
        /// </summary>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        /// <exception cref="ApplicationException">
        /// if something is wrong and can not be corrected
        /// </exception>
        internal IGeneralResponse PostLocationToKlassApp(Locations payload, IWapiOperation operation)
        {
            // Sending information to KlassApp ...........................................
            // Create the client
            var client = this.CreateHttpClient(operation);
 
            HttpResponseMessage response = null;
            var uri = operation.KlassAppUrlConfigLocations; ////  new Uri(operation.ExternalUrl + PostLocationUri);
            
            // Post to Service............
            var task = client.PostAsJsonAsync(uri, payload).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
            });
            task.Wait();

            return this.InsertResponseAnalysis(operation, response, client);
        }

        /// <summary>
        /// The post majors to <code>klass_app</code>.
        /// </summary>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        internal IGeneralResponse PostMajorsToKlassApp(Majors payload, IWapiOperation operation)
        {
            // Sending information to KlassApp ...........................................
            // Create the client
            var client = this.CreateHttpClient(operation);

            HttpResponseMessage response = null;
            var uri = operation.KlassAppUrlConfigMajors; ////  new Uri(operation.ExternalUrl + PostLocationUri);

            // Post to Service............
            var task = client.PostAsJsonAsync(uri, payload).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
            });
            task.Wait();

            return this.InsertResponseAnalysis(operation, response, client);
        }

        /// <summary>
        /// The post key value pairs to <code>klass_app</code>.
        /// </summary>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        internal IGeneralResponse PostKeyValuePairsToKlassApp(KeyValuePart payload, IWapiOperation operation)
        {
            // Create the client
            var client = this.CreateHttpClient(operation);
            HttpResponseMessage response = null;
            var uri = operation.KlassAppUrlConfigKeyValuesPairs; //// new Uri(operation.ExternalUrl + PostKeyValuePartUri);

            // Post to Service............
            var task = client.PostAsJsonAsync(uri, payload).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
            });
            task.Wait();
            var output = this.InsertResponseAnalysis(operation, response, client);
            return output;
        }

        /// <summary>
        /// The post custom field to <code>klass_app</code>.
        /// </summary>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        internal IGeneralResponse PostCustomFieldToKlassApp(CustomField payload, IWapiOperation operation)
        {
            // Create the client
            var client = this.CreateHttpClient(operation);
            HttpResponseMessage response = null;
            var uri = operation.KlassAppUrlConfigCustomFields; //// operation.ExternalUrl + PostCustomFieldUri;

            // Post to Service............
            var task = client.PostAsJsonAsync(new Uri(uri), payload).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
            });
            task.Wait();
            var output = this.InsertResponseAnalysis(operation, response, client);
            return output;
        }

        #endregion

        #region Put (update) Methods 
        /// <summary>
        /// The put location to <code>klass_app</code>.
        /// </summary>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// Return the result of the operation
        /// No message error or ID greater than 0 indicate no error condition
        /// </returns>
        internal IGeneralResponse PutLocationToKlassApp(Locations payload, IWapiOperation operation)
        {
            // Sending information to KlassApp ...........................................
            // Create the client
            var client = this.CreateHttpClient(operation);

            HttpResponseMessage response = null;
            var uri = new Uri(operation.KlassAppUrlConfigLocations + "/" + payload.id);

            // Post to Service............
            var task = client.PutAsJsonAsync(uri, payload).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                Debug.WriteLine("Code: {0} - Reason: {1}", response.StatusCode, response.ReasonPhrase);
            });
            task.Wait();

            var message = response.Content.ReadAsAsync(typeof(ResponseBase)).Result;
            var res = message as ResponseBase;
            var output = GeneralResponse.Factory();

            if (res == null || res.response == null)
            {
                output = this.AnalysisExistenceOfError(response, res, output, "UPDATE");
                output.KlassAppIdentification = payload.id;
                return output;
            }

            if (res != null)
            {
                output.ObjectFromServer = res.response;
                output.KlassAppIdentification = Convert.ToInt32(output.ObjectFromServer.id);
            }

            return output;
        }

        /// <summary>
        /// The put majors to <code>klass_app</code>.
        /// </summary>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        internal IGeneralResponse PutMajorsToKlassApp(Majors payload, IWapiOperation operation)
        {
            // Sending information to KlassApp ...........................................
            // Create the client
            var client = this.CreateHttpClient(operation);

            HttpResponseMessage response = null;
            var uri = new Uri(operation.KlassAppUrlConfigMajors + "/" + payload.id);

            // Post to Service............
            var task = client.PutAsJsonAsync(uri, payload).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                Debug.WriteLine("Code: {0} - Reason: {1}", response.StatusCode, response.ReasonPhrase);
            });
            task.Wait();

            var message = response.Content.ReadAsAsync(typeof(ResponseBase)).Result;
            var res = message as ResponseBase;

            var output = GeneralResponse.Factory();
            if (res == null || res.response == null)
            {
                output = this.AnalysisExistenceOfError(response, res, output, "UPDATE");
                output.KlassAppIdentification = payload.id;
                return output;
            }

            if (res != null)
            {
                output.ObjectFromServer = res.response;
                output.KlassAppIdentification = Convert.ToInt32(output.ObjectFromServer.id);
            }

            return output;
        }

        /// <summary>
        /// The put key value pair to <code>klass_app</code>.
        /// </summary>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        internal IGeneralResponse PutKeyValuePairToKlassApp(KeyValuePart payload, IWapiOperation operation)
        {
            // Sending information to KlassApp ...........................................
            // Create the client
            var client = this.CreateHttpClient(operation);

            HttpResponseMessage response = null;
            var uri = new Uri(operation.KlassAppUrlConfigKeyValuesPairs + "/" + payload.id);

            // Post to Service............
            var task = client.PutAsJsonAsync(uri, payload).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                Debug.WriteLine("Code: {0} - Reason: {1}", response.StatusCode, response.ReasonPhrase);
            });
            task.Wait();

            var message = response.Content.ReadAsAsync(typeof(ResponseBase)).Result;
            var res = message as ResponseBase;
            var output = GeneralResponse.Factory();
            if (res == null || res.response == null)
            {
                output = this.AnalysisExistenceOfError(response, res, output, "UPDATE");
                output.KlassAppIdentification = payload.id;
                return output;
            }

            if (res != null)
            {
                output.ObjectFromServer = res.response;
                output.KlassAppIdentification = Convert.ToInt32(output.ObjectFromServer.id);
            }

            return output;

            ////var output = this.AnalysisResponseUpdateFromKlassApp(payload.id, response);
            ////return output;
        }

        /// <summary>
        /// The put custom field to <code>klass_app</code>.
        /// </summary>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        internal IGeneralResponse PutCustomFieldToKlassApp(CustomField payload, IWapiOperation operation)
        {
            // Sending information to KlassApp ...........................................
            // Create the client
            var client = this.CreateHttpClient(operation);
            var uri = new Uri(operation.KlassAppUrlConfigCustomFields + "/" + payload.id);
            HttpResponseMessage response = null;

            // Post to Service............
            var task = client.PutAsJsonAsync(uri, payload).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                Debug.WriteLine("Code: {0} - Reason: {1}", response.StatusCode, response.ReasonPhrase);
            });
            task.Wait();

            var message = response.Content.ReadAsAsync(typeof(ResponseBase)).Result;
            var res = message as ResponseBase;
            var output = GeneralResponse.Factory();
            if (res == null || res.response == null)
            {
                output = this.AnalysisExistenceOfError(response, res, output, "UPDATE");
                output.KlassAppIdentification = payload.id;
                return output;
            }

            if (res != null)
            {
                output.ObjectFromServer = res.response;
                output.KlassAppIdentification = Convert.ToInt32(output.ObjectFromServer.id);
            }

            return output;
            ////var output = this.AnalysisResponseUpdateFromKlassApp(payload.id, response);
            ////return output;
        }

        #endregion

        #region Delete in Klass_app

        /// <summary>
        /// The delete custom field to <code>klass_app</code>.
        /// </summary>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        internal IGeneralResponse DeleteCustomFieldToKlassApp(CustomField payload, IWapiOperation operation)
        {
            // Sending information to KlassApp ...........................................
            // Create the client
            var client = this.CreateHttpClient(operation);
            var uri = new Uri(operation.KlassAppUrlConfigCustomFields + "/" + payload.id);
            HttpResponseMessage response = null;

            // Post to Service............
            var task = client.DeleteAsync(uri).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                Debug.WriteLine("Code: {0} - Reason: {1}", response.StatusCode, response.ReasonPhrase);
            });
            task.Wait();

            var output = this.AnalysisResponseDeleteFromKlassApp(payload.id, response);
            return output;
        }

        /// <summary>
        /// The delete location to <code>klass_app</code>.
        /// </summary>
        /// <param name="klassAppId">
        /// The <code>klassAppId</code>.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        internal IGeneralResponse DeleteLocationToKlassApp(int klassAppId, IWapiOperation operation)
        {
            // Sending information to KlassApp ...........................................
            // Create the client
            var client = this.CreateHttpClient(operation);
            var uri = new Uri(operation.KlassAppUrlConfigLocations + "/" + klassAppId);
            HttpResponseMessage response = null;

            // Post to Service............
            var task = client.DeleteAsync(uri).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                Debug.WriteLine("Code: {0} - Reason: {1}", response.StatusCode, response.ReasonPhrase);
            });
            task.Wait();

            var output = this.AnalysisResponseDeleteFromKlassApp(klassAppId, response);
            return output;
        }

        /// <summary>
        /// The delete major to <code>klass_app</code>.
        /// </summary>
        /// <param name="klassAppId">
        /// The <code>klass_app</code> id.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        internal IGeneralResponse DeleteMajorToKlassApp(int klassAppId, IWapiOperation operation)
        {
            // Sending information to KlassApp ...........................................
            // Create the client
            var client = this.CreateHttpClient(operation);
            var uri = new Uri(operation.KlassAppUrlConfigMajors + "/" + klassAppId);
            HttpResponseMessage response = null;

            // Post to Service............
            var task = client.DeleteAsync(uri).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                Debug.WriteLine("Code: {0} - Reason: {1}", response.StatusCode, response.ReasonPhrase);
            });
            task.Wait();

            var output = this.AnalysisResponseDeleteFromKlassApp(klassAppId, response);
            return output;
        }

        /// <summary>
        /// The delete key value to <code>KLASS_APP</code>
        /// </summary>
        /// <param name="klassAppId">
        /// The ID to delete.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        internal IGeneralResponse DeleteKeyValueToKlassApp(int klassAppId, IWapiOperation operation)
        {
            // Create the client
            var client = this.CreateHttpClient(operation);
            var uri = new Uri(operation.KlassAppUrlConfigKeyValuesPairs + "/" + klassAppId);
            HttpResponseMessage response = null;

            // Post to Service............
            var task = client.DeleteAsync(uri).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                Debug.WriteLine("Code: {0} - Reason: {1}", response.StatusCode, response.ReasonPhrase);
            });
            task.Wait();

            var output = this.AnalysisResponseDeleteFromKlassApp(klassAppId, response);
            return output;
        }

        #endregion

        /// <summary>
        /// The insert response analysis.
        /// </summary>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <param name="response">
        /// The response.
        /// </param>
        /// <param name="client">
        /// The client.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        private IGeneralResponse InsertResponseAnalysis(IWapiOperation operation, HttpResponseMessage response, HttpClient client)
        {
            var output = GeneralResponse.Factory();

            // See if was success the transmission
            if (response.StatusCode != HttpStatusCode.OK)
            {
                output.ErrorCode = "INSERT1";
                output.Explanation =
                    $"KlassApp response {response.StatusCode} reason: {response.ReasonPhrase} - URL: {operation.ExternalUrl} Headers: {client.DefaultRequestHeaders}";

                return output;
            }

            var message = response.Content.ReadAsAsync(typeof(ResponseBase)).Result;

            // See if they are some error in logic...
            var res = message as ResponseBase;
            if (res == null)
            {
                output.ErrorCode = "INSERT2";
                output.Explanation = $"Unspecific error {response.StatusCode} reason: {response.ReasonPhrase}";
                return output;
            }

            if (res.success)
            {
                var location = res.response;
                if (location == null)
                {
                    output.ErrorCode = "INSERT3";
                    output.Explanation = $"Unspecific error {response.StatusCode} reason: {response.ReasonPhrase}";
                    return output;
                }

                output.KlassAppIdentification = res.response.id;
                return output;
            }

            output = this.AnalysisExistenceOfError(response, res, output, "INSERT");
            return output;
        }

        /// <summary>
        /// The analysis existence of error.
        /// </summary>
        /// <param name="response">
        /// The response.
        /// </param>
        /// <param name="res">
        /// The res.
        /// </param>
        /// <param name="output">
        /// The output.
        /// </param>
        /// <param name="errorText">
        /// The error text.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        private IGeneralResponse AnalysisExistenceOfError(HttpResponseMessage response, ResponseBase res, IGeneralResponse output, string errorText)
        {
            var errorlist = res.errors;
            if (errorlist == null || !errorlist.Any())
            {
                output.ErrorCode = errorText + "4";
                output.Explanation =
                    $"Response is {response.StatusCode} reason: {response.ReasonPhrase}, success: {res.success}, info: {res.info} ";
                return output;
            }

            if (errorlist[0] is string)
            {
                output.ErrorCode = errorText + "7";
                output.Explanation = Convert.ToString(errorlist[0]);
                return output;
            }

            if (errorlist[0].code == null)
            {
                var error = errorlist[0];
                if (Convert.ToBoolean(error.HasValues))
                {
                    output.ErrorCode = errorText + "5";
                    output.Explanation = Convert.ToString(error.type[0]);
                }
                else
                {
                    output.ErrorCode = errorText + "6";
                    if (errorlist[0].type != null)
                    {
                        string[] errors = errorlist[0].type.ToObject<string[]>();
                        foreach (string s in errors)
                        {
                            output.Explanation += s;
                        }
                    }
                    else
                    {
                        output.Explanation = "Required field missing";
                    }
                }
            }
            else
            {
                output.ErrorCode = Convert.ToString(errorlist[0].code);
                output.Explanation = Convert.ToString(errorlist[0].message);
            }

            return output;
        }

        /// <summary>
        /// The analysis response delete from <code>klass_app</code>.
        /// There is not form to know if the record was delete.
        /// Delete operation always return success from <code>klass_app</code>.
        /// unless a communication error happen.
        /// </summary>
        /// <param name="idOriginal">
        /// The id original.
        /// </param>
        /// <param name="response">
        /// The response.
        /// </param>
        /// <returns>
        /// The <see cref="IGeneralResponse"/>.
        /// </returns>
        private IGeneralResponse AnalysisResponseDeleteFromKlassApp(int idOriginal, HttpResponseMessage response)
        {
           var output = GeneralResponse.Factory();

            // See if was success the transmission
            if (response.StatusCode != HttpStatusCode.OK)
            {
                if (idOriginal == 0)
                {
                    output.ErrorCode = "DELETE1";
                    output.Explanation =
                        $"Error KlassApp response {response.StatusCode} reason: KlassApp id can not be 0";
                    return output;
                }

                output.ErrorCode = "DELETE2";
                output.Explanation =
                    $"Error in transmission: KlassApp response {response.StatusCode} reason: {response.ReasonPhrase}";
                return output;
            }

            output.IsDeleted = true;
            output.KlassAppIdentification = idOriginal;
            return output;
        }

        #region Helpers
        /// <summary>
        /// The create HTTP client.
        /// </summary>
        /// <param name="operation">
        /// The operation. Get the header information
        /// </param>
        /// <returns>
        /// The <see cref="HttpClient"/>.
        /// </returns>
        private HttpClient CreateHttpClient(IWapiOperation operation)
        {
            var client = new HttpClient();

            // Create the headers
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("auth_id", operation.ConsumerKey);
            client.DefaultRequestHeaders.Add("auth_token", operation.PrivateKey);
            return client;
        }
        #endregion
    }
}
