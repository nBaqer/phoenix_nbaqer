﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseKlassObject.cs" company="FAME">
//   2016
// </copyright>
// <summary>
// JAGG -  The base <code>klassApp</code> object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Mobile
{
    using FAME.Advantage.Domain.SystemStuff;

    /// <summary>
    /// The base <code>klassApp</code> object.
    /// </summary>
    public class BaseKlassObject
    {
        /// <summary>
        /// Gets or sets the WAPI Operation.
        /// </summary>
        public SyWapiSettings Operation { get; set; }
    }
}