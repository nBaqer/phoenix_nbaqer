﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppCustomFieldOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the KlassAppCustomFieldOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Wapi.KlassApp
{
    using System;
    using System.Globalization;

    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Mobile;

    /// <summary>
    ///  The KLASS application custom field output model.
    /// </summary>
    public class KlassAppCustomFieldOutputModel
    {
        /// <summary>
        /// Gets or sets the exams.
        /// </summary>
        public decimal Exams { get; set; }

        /// <summary>
        /// Gets or sets the total hours.
        /// </summary>
        public int TotalHours { get; set; }

        /// <summary>
        /// Gets or sets the absent hours.
        /// </summary>
        public int AbsentHours { get; set; }

        /// <summary>
        /// Gets or sets the make up hours.
        /// </summary>
        public int MakeUpHours { get; set; }

        /// <summary>
        /// Gets or sets the attendance %.
        /// </summary>
        public decimal AttendancePercentage { get; set; }

        /// <summary>
        /// Gets or sets the last day attendance.
        /// </summary>
        public DateTime LastDayAttendance { get; set; }

        /// <summary>
        ///  The set custom fields.
        /// Use this to set consistently the custom fields
        /// </summary>
        /// <param name="input">
        ///  The input.
        /// </param>
        /// <returns>
        /// The <see cref="KlassAppStudentOutputModel"/>.
        /// </returns>
        ////public KlassAppStudentOutputModel SetCustomFields(KlassAppStudentOutputModel input)
        ////{
        ////    input.custom_fields = string.Format("1:{0}", this.Exams);
        ////    input.custom_fields += string.Format(",2:{0}", this.TotalHours);
        ////    input.custom_fields += string.Format(",3:{0}", this.AbsentHours);
        ////    input.custom_fields += string.Format(",4:{0}", this.MakeUpHours);
        ////    input.custom_fields += string.Format(",5:{0}", this.AttendancePercentage.ToString(CultureInfo.CurrentCulture));
        ////    input.custom_fields += string.Format(",6:{0}", this.LastDayAttendance.Date.ToString(CultureInfo.CurrentCulture));
        ////    return input;
        ////}
    }
}
