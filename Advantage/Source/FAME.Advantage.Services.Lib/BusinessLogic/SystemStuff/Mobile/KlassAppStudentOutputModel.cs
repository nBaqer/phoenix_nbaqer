﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppStudentOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The KLASS APP Output Model
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Mobile
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    using FAME.Advantage.Messages.SystemStuff.Mobile.ApiKlass;

    /// <summary>
    /// The KLASS APP Output Model
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:CodeAnalysisSuppressionMustHaveJustification", Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("ReSharper", "StyleCop.SA1300"), SuppressMessage("ReSharper", "InconsistentNaming")]
    public class KlassAppStudentOutputModel
    {
        /// <summary>
        /// Gets or sets the college_id.
        /// </summary>
        public string college_id { get; set; }

        /// <summary>
        /// Gets or sets the first_name.
        /// </summary>
        public string first_name { get; set; }

        /// <summary>
        /// Gets or sets the last_name.
        /// </summary>
        public string last_name { get; set; }

        /// <summary>
        /// Gets or sets the role.
        /// The role for now must be student
        /// </summary>
        public string role { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// Just a random 8 digit string
        /// </summary>
        public string password { get; set; }

        /// <summary>
        /// Gets or sets the major.
        /// </summary>
        public string major { get; set; }

        /// <summary>
        /// Gets or sets the enrollment id
        /// </summary>
        public string enrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// Gets or sets the date_of_birth.
        /// </summary>
        public DateTime? date_of_birth { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        public string gender { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// Gets or sets the phone_other.
        /// </summary>
        public string phone_other { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string address { get; set; }

        /// <summary>
        /// Gets or sets the address_city.
        /// </summary>
        public string address_city { get; set; }

        /// <summary>
        /// Gets or sets the address_state.
        /// </summary>
        public string address_state { get; set; }

        /// <summary>
        /// Gets or sets the address_zip.
        /// </summary>
        public string address_zip { get; set; }

        /// <summary>
        /// Gets or sets the start_date.
        /// </summary>
        public DateTime start_date { get; set; }

        /// <summary>
        /// Gets or sets the graduation_date.
        /// </summary>
        public DateTime? graduation_date { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        public string locations { get; set; }

        /// <summary>
        /// Gets or sets the Last Day of Attendance.
        /// </summary>
        public DateTime lda { get; set; }

        /// <summary>
        /// Gets or sets the custom_fields.
        /// </summary>
        ////public IList<KeyValuePair<int, object>> custom_fields { get; set; }
        public Dictionary<int, string> cf_dictionary { get; set; }
    }
}
