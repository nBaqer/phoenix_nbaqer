﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppCarrierOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//  JAGG Defines the KlassAppCarrierOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Wapi.KlassApp
{
    using System.Collections.Generic;

    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Mobile;

    /// <summary>
    ///  The <code>klass</code> application carrier output model.
    /// </summary>
    public class KlassAppCarrierOutputModel
    {
        /// <summary>
        /// Gets or sets the output.
        /// </summary>
        public KlassAppStudentOutputModel Output { get; set; }

        /// <summary>
        /// Gets or sets the enrollment.
        /// </summary>
        public dynamic TheEnrollment { get; set; }
    }
}
