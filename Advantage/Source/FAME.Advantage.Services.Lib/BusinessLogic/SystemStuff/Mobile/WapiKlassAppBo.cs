﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiKlassAppBo.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the WapiKlassAppBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Mobile
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;

    using AutoMapper;

    using Domain.Infrastructure.Entities;
    using Domain.Lead;
    using Domain.Student.Enrollments;
    using Domain.SystemStuff.Mobile;

    using Messages.Wapi.KlassApp;
    using NHibernate.Transform;

    /// <summary>
    ///  The WAPI KLASS APP Business Object.
    /// </summary>
    internal class WapiKlassAppBo
    {
        /// <summary>
        /// The repository integer
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryInteger;

        /// <summary>
        ///  The repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="WapiKlassAppBo"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="repositoryInteger">
        /// The repository Integer.
        /// </param>
        public WapiKlassAppBo(IRepository repository, IRepositoryWithTypedID<int> repositoryInteger)
        {
            this.repository = repository;
            this.repositoryInteger = repositoryInteger;
        }

        /// <summary>
        /// The get student info.
        /// To be used in production mode
        /// </summary>
        /// <param name="klassObject">
        ///     The object with the operation configuration</param>
        /// <param name="settingList">
        /// The list of configuration settings used with KLASSAPP
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;KlassAppCarrierOutputModel&gt;"/>.
        /// The list of student with changes and the selected enrollment
        /// </returns>
        public IList<KlassAppStudentOutputModel> GetStudentInfo(IKlassStudentObject klassObject, IList<SyKlassAppConfigurationSetting> settingList)
        {
            Debug.WriteLine("Begin get Student Info");

            // Calculate date time to begin to considerer changes
            var previousExecution = klassObject.Operation.DateLastExecution;

            ////Guid[] listId = new Guid[] { };
            IQueryable studentsQ = this.repository.Query<Lead>()
                .Where(
                    x => x.StudentId != Guid.Empty && x.LeadEmailList.Any() && x.EnrollmentList.Any(
                             t =>
                                 (t.EnrollmentStatus.SystemStatus.IsInSchool != null
                                  && t.EnrollmentStatus.SystemStatus.IsInSchool == true)
                                 && (t.ModDate > previousExecution
                                     || t.ResultList.Any(c => c.ModDate != null && c.ModDate > previousExecution)
                                     || t.AttendanceList.Any(
                                         c => c.AttendanceDate != null && c.AttendanceDate > previousExecution)
                                     || t.ConversionAttendanceList.Any(c => c.ModDate > previousExecution)
                                     || t.ClassSectionAttendanceList.Any(c => c.ModDate > previousExecution)
                                     || t.StudentClockAttendanceList.Any(c => c.ModDate > previousExecution)
                                     || t.ExternalShipAttendanceList.Any(c => c.ModDate > previousExecution)
                                     || t.GradeBookResults.Any(c => c.ModifiedDate > previousExecution)
                                     || t.LeadObj.ModDate > previousExecution
                                     || t.LeadObj.AddressList.Any(y => y.ModDate > previousExecution)
                                     || t.LeadObj.LeadPhoneList.Any(y => y.ModDate > previousExecution))));
            ////|| t.StudentAttendanceSummaryList.Any(y => y.ModifiedDate > previousExecution)

            // Mapper the relations
            var studentsListB = Mapper.Map<IList<KlassAppStudentOutputModel>>(studentsQ);

            var studentsList = new List<KlassAppStudentOutputModel>();

            if (klassObject.CampusActiveGuidList.Any(x => x == Guid.Empty))
            {
                // If GUID empty is present get all is selected
                foreach (KlassAppStudentOutputModel model in studentsListB)
                {
                    if (this.VerifyStudents(model))
                    {
                        studentsList.Add(model);
                    }

                    ////// Test if exists repeated email
                    ////string[] group = this.repository.Query<LeadEmail>()
                    ////    .Where(x => x.Email == model.email)
                    ////    .Select(x => x.Email)
                    ////    .ToArray();
                    ////if (group.Length == 1)
                    ////{
                    ////    studentsList.Add(model);
                    ////}
                }
            }
            else
            {
                // If not get only that campus are in the list
                var listGuid = klassObject.CampusActiveGuidList.ToList();
                foreach (KlassAppStudentOutputModel model in studentsListB)
                {
                    if (this.VerifyStudents(model))
                    {
                        foreach (Guid id in listGuid)
                        {
                            if (new Guid(model.locations) == id)
                            {
                                studentsList.Add(model);
                                break;
                            }
                        }
                    }
                }
            }

            Debug.WriteLine("End Get Student...");
            return studentsList;
        }

        /// <summary>
        /// The execute stored procedure summary.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="SummaryDto"/>.
        /// </returns>
        public SummaryDto ExecuteStoredProcedureSummary(object id)
        {
            var result =
                this.repository.Session.CreateSQLQuery("exec Usp_PR_Sub2_Enrollment_Summary :pEnrollmentId")
                    .SetResultTransformer(Transformers.AliasToBean<SummaryDto>())
                    .SetParameter("pEnrollmentId", id).UniqueResult<SummaryDto>();

            return result;
        }

        /// <summary>
        ///  The get student attendance.
        /// </summary>
        /// <param name="id">
        ///  The ID.
        /// </param>
        /// <returns>
        /// The <see cref="StudentAttendanceSummary"/>.
        /// </returns>
        public StudentAttendanceSummary GetStudentAttendance(string id)
        {
            var attendance = this.repositoryInteger.Query<StudentAttendanceSummary>().Where(x => x.EnrollmentObj.ID == new Guid(id)).OrderByDescending(x => x.StudentAttendedDate).FirstOrDefault();
            return attendance;
        }

        /// <summary>
        /// The apply settings.
        /// </summary>
        /// <param name="settingList">
        /// The setting list.
        /// </param>
        /// <param name="output">
        /// The output.
        /// </param>
        /// <param name="configValues">
        /// The configuration values.
        /// </param>
        /// <param name="configCampus">
        /// The dictionary of GPO method by campus id
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;KlassAppStudentOutputModel&gt;"/>.
        /// </returns>
        public IList<KlassAppStudentOutputModel> ApplySettings(
            IList<SyKlassAppConfigurationSetting> settingList,
            IList<KlassAppStudentOutputModel> output,
            IKlassStudentObject configValues,
            Dictionary<Guid, string> configCampus)
        {
            // Get the campus configuration related to GPA method ..............................................
            ////var configCampus = this.GetGpoMethodDictionary();

            // Loop for students
            foreach (KlassAppStudentOutputModel model in output)
            {
                // Get the setting values
                SummaryDto info = this.ExecuteStoredProcedureSummary(model.enrollmentId);

                // Loop to setting to assign values
                foreach (SyKlassAppConfigurationSetting setting in settingList)
                {
                    if (setting.IsCustomField == 1)
                    {
                        switch (setting.KlassOperationTypeObject.Code)
                        {
                            case "totalhours":
                                {
                                    // Total Hours
                                    ////AttendanceSummary attendance = this.bo.GetStudentAttendance(model.enrollmentId);
                                    ////customfields.Append($"\"{attendance.ActualRunningScheduledDays}\"");
                                    ////model.custom_fields += customfields.ToString();
                                    //// var par = new KeyValuePair<int, object>(setting.KlassAppIdentification, 0);

                                    model.cf_dictionary.Add(
                                        setting.KlassAppIdentification,
                                        (info.MakeupDays + info.TotalDaysAttended).ToString("N2", CultureInfo.InvariantCulture));
                                    break;
                                }

                            case "absenthours":
                                {
                                    model.cf_dictionary.Add(setting.KlassAppIdentification, info.DaysAbsent.ToString("N2", CultureInfo.InvariantCulture));
                                    break;
                                }

                            case "makeuphours":
                                {
                                    model.cf_dictionary.Add(setting.KlassAppIdentification, info.MakeupDays.ToString("N2", CultureInfo.InvariantCulture));
                                    break;
                                }

                            case "lda":
                                {
                                    model.cf_dictionary.Add(setting.KlassAppIdentification, $"{model.lda.Date.ToShortDateString()}");
                                    break;
                                }

                            case "attendancepercent":
                                {
                                    decimal percent = 0;
                                    if (info.ScheduledDays > 0)
                                    {
                                        percent = ((info.TotalDaysAttended + info.MakeupDays) / info.ScheduledDays) * 100;
                                    }

                                    var percentStr = $"{percent.ToString("N2", CultureInfo.InstalledUICulture)} %";
                                    model.cf_dictionary.Add(setting.KlassAppIdentification, percentStr);
                                    break;
                                }

                            case "gpo":
                                {
                                    // GPA
                                    var gpaMethod = configCampus.ContainsKey(Guid.Empty)
                                                        ? configCampus[Guid.Empty]
                                                        : configCampus[new Guid(model.locations)];
                                    var gpa = gpaMethod.ToLowerInvariant() == "weightedavg"
                                                  ? info.WeightedAverage_CumGPA
                                                  : info.SimpleAverage_CumGPA;
                                    model.cf_dictionary.Add(
                                        setting.KlassAppIdentification,
                                        gpa.ToString(CultureInfo.InvariantCulture));
                                    break;
                                }

                            case "credEarned":
                                {
                                    var credit = info.CreditsEarned;
                                    model.cf_dictionary.Add(setting.KlassAppIdentification, credit.ToString("0.00"));
                                    break;
                                }

                            case "credAttemped":
                                {
                                    var credit = info.CreditsAttempted;
                                    model.cf_dictionary.Add(setting.KlassAppIdentification, credit.ToString("0.00"));
                                    break;
                                }
                        }
                    }
                }
            }

            return output;
        }

        /// <summary>
        /// The apply major status locations.
        /// </summary>
        /// <param name="settingList">
        /// The setting list.
        /// </param>
        /// <param name="output">
        /// The output.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;KlassAppStudentOutputModel&gt;"/>.
        /// </returns>
        public IList<KlassAppStudentOutputModel> ApplyMajorStatusLocations(IList<SyKlassAppConfigurationSetting> settingList, IList<KlassAppStudentOutputModel> output)
        {
            foreach (KlassAppStudentOutputModel model in output)
            {
                model.locations =
                    settingList.First(x => x.AdvantageIdentification == model.locations)
                        .KlassAppIdentification.ToString();
                model.major =
                    settingList.First(x => x.AdvantageIdentification == model.major).KlassAppIdentification.ToString();
                model.status =
                    settingList.First(x => x.AdvantageIdentification == model.status).KlassAppIdentification.ToString();
            }

            return output;
        }

        /// <summary>
        /// The get custom fields.
        /// </summary>
        /// <returns>
        /// The <see cref="IList&lt;KlassAppCustomFields&gt;"/>.
        /// </returns>
        internal IList<SyKlassAppConfigurationSetting> GetConfigurationFields()
        {
            var customs = this.repositoryInteger.Query<SyKlassAppConfigurationSetting>().OrderBy(x => x.ID).ToList();
            return customs;
        }

        /// <summary>
        /// The verify students.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool VerifyStudents(KlassAppStudentOutputModel model)
        {
            // Test if exists repeated email
            bool isOk = true;
            string[] group = this.repository.Query<LeadEmail>()
                .Where(x => x.Email == model.email)
                .Select(x => x.Email)
                .ToArray();
            isOk &= !(group.Length > 1);
            isOk &= !string.IsNullOrWhiteSpace(model.college_id);
            isOk &= !string.IsNullOrWhiteSpace(model.major);
            return isOk;
        }
    }
}