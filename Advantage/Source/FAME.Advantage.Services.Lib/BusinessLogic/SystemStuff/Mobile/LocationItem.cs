﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocationItem.cs" company="FAME">
//   2016
// </copyright>
// <summary>
// JAGG - The location item.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Wapi.KlassApp
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// The location item.
    /// </summary>
    [SuppressMessage("ReSharper", "StyleCop.SA1300", Justification = "Valid here to send Json")]
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "Valid here to send Json")]
    public class LocationItem
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string address { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// Gets or sets the location_type.
        /// </summary>
        public string location_type { get; set; }

        /// <summary>
        /// Gets or sets the contact_mail.
        /// </summary>
        public string contact_mail { get; set; }

        /// <summary>
        /// Gets or sets the contact_person.
        /// </summary>
        public string contact_person { get; set; }

        /// <summary>
        /// Gets or sets the contact_phone.
        /// </summary>
        public string contact_phone { get; set; }

        /// <summary>
        /// Gets or sets the contact_position.
        /// </summary>
        public string contact_position { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether deleted.
        /// </summary>
        public bool deleted { get; set; }

        /// <summary>
        /// Gets or sets the referrals_email_address.
        /// </summary>
        public string referrals_email_address { get; set; }

        /// <summary>
        /// Gets or sets the created_at.
        /// </summary>
        public string created_at { get; set; }

        /// <summary>
        /// Gets or sets the update_at.
        /// </summary>
        public string update_at { get; set; }

        /// <summary>
        /// Gets or sets the payment_on.
        /// </summary>
        public string payment_on { get; set; }

        /// <summary>
        /// Gets or sets the payment_currency.
        /// </summary>
        public string payment_currency { get; set; }

        /// <summary>
        /// Gets or sets the <code>payment_worldpay_id</code>.
        /// </summary>
        public string payment_worldpay_id { get; set; }

        /// <summary>
        /// Gets or sets the payment_email_address.
        /// </summary>
        public string payment_email_address { get; set; }

        /// <summary>
        /// Gets or sets the <code>payments_worldpay_gateway_id</code>.
        /// </summary>
        public string payments_worldpay_gateway_id { get; set; }

        /// <summary>
        /// Gets or sets the <code>payments_worldpay_form_id</code>.
        /// </summary>
        public string payments_worldpay_form_id { get; set; }

        /// <summary>
        /// Gets or sets the <code>payments_worldpay_session</code>.
        /// </summary>
        public string payments_worldpay_session { get; set; }

        /// <summary>
        /// Gets or sets the <code>payments_worldpay_key</code>.
        /// </summary>
        public string payments_worldpay_key { get; set; }
    }
}
