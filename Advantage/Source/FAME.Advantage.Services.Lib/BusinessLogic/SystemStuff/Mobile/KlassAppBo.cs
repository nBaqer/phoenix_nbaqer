﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppBo.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the KlassAppBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Mobile
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using Domain.Campuses;
    using Domain.Infrastructure.Entities;
    using Domain.Student.Enrollments;
    using Domain.SystemStuff;
    using Domain.SystemStuff.Mobile;
    using Domain.Users;

    using Messages.SystemStuff.Mobile;
    using Messages.SystemStuff.Mobile.ApiKlass;

    using Org.BouncyCastle.Security;

    /// <summary>
    ///  The <code>klassApp</code> BO.
    /// </summary>
    public class KlassAppBo
    {
        #region Private fields

        /// <summary>
        /// The NHibernate repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The repository with type id.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithTypeId;

        /// <summary>
        /// The API to interact with <code>KlassApp</code>.
        /// </summary>
        private readonly ClassAppApi api;

        /// <summary>
        /// The all settings. Hold a copy of all setting actives in 
        /// <code>SyKlassAppConfigurationSetting</code>
        /// </summary>
        private IList<SyKlassAppConfigurationSetting> allSettings;

        /// <summary>
        /// The user name.
        /// </summary>
        private string userName;

        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="KlassAppBo"/> class. 
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="repositoryWithTypeId">
        /// The repository with type id.
        /// </param>
        public KlassAppBo(IRepository repository, IRepositoryWithTypedID<int> repositoryWithTypeId)
        {
            this.repository = repository;
            this.repositoryWithTypeId = repositoryWithTypeId;
            this.api = new ClassAppApi();
        }

        #endregion

        #region Public operations

        /// <summary>
        ///  The get all configuration settings.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;ClassAppConfigurationOutputModel&gt;"/>.
        /// </returns>
        public IList<ClassAppConfigurationOutputModel> GetAllConfigurationSettings(ClassAppInputModel filter)
        {
            var settings = this.repositoryWithTypeId.Query<SyKlassAppConfigurationSetting>().OrderBy(x => x.KlassOperationTypeObject.Code);
            var list = Mapper.Map<IList<ClassAppConfigurationOutputModel>>(settings.ToList());
            return list;
        }

        /// <summary>
        /// Command 2: The send configuration settings to mobile.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;ClassAppConfigurationOutputModel&gt;"/>.
        /// </returns>
        public IList<ClassAppConfigurationOutputModel> SendConfigurationSettingsToMobile(ClassAppInputModel filter, IWapiOperation operation)
        {
            // Get all settings
            this.allSettings =
               this.repositoryWithTypeId.Query<SyKlassAppConfigurationSetting>()
                   .Where(x => x.IsActive == 1 && x.ItemStatus != 'N').ToList();

            // Only execute if exists settings to process....
            if (this.allSettings.Any())
            {
                // Get user name
                if (filter.UserId == null)
                {
                    this.userName = "SUPPORT";
                }
                else
                {
                    var user = this.repository.Get<User>(new Guid(filter.UserId));
                    this.userName = user.UserName;
                }

                // Process Custom Fields...
                this.ProcessCustomFields(filter, operation);

                // Process the Locations Fields (campus)...
                this.ProcessLocations(operation);

                // Process the key values
                this.ProcessKeyValues(operation);

                // This is department
                this.ProcessMajors(operation);
            }

            // Return the list of settings with or without changes...
            var listSetting = this.GetAllConfigurationSettings(filter);
            return listSetting;
        }

        /// <summary>
        /// The configure advantage KLASSAPP table.
        /// This truncate the table and repopulate with all values in insert state.
        /// </summary>
        /// <param name="filter">
        /// The filter:  User ID
        /// </param>
        public void ConfigureAdvantageKlassAppTable(ClassAppInputModel filter)
        {
            // Get the user name
            var username = this.repository.Get<User>(new Guid(filter.UserId)).UserName;

            // Truncate and populate again SyKlassAppConfigurationSetting
            // All configuration settings are now in insert status
            var query = this.repositoryWithTypeId.Session.CreateSQLQuery("exec USP_SY_KlassApp_InitializeConfigurationSettings @UserName=:UserName");
            query.SetParameter("UserName", username);
            query.ExecuteUpdate();
        }

        /// <summary>
        /// The update configuration table.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="ClassAppConfigurationOutputModel"/>.
        /// </returns>
        public ClassAppConfigurationOutputModel UpdateConfigurationTable(ClassAppConfigurationOutputModel model)
        {
            var setting = this.repositoryWithTypeId.Query<SyKlassAppConfigurationSetting>().SingleOrDefault(x => x.ID == model.Id);
            if (setting == null)
            {
                throw new InvalidParameterException("The setting ID is not found");
            }

            var user = this.repository.Get<User>(new Guid(model.ModUser)).UserName; // Loaded with userId
            model.ModUser = user;
            setting.UpdateTableFromUi(model);
            this.repositoryWithTypeId.UpdateAndFlush(setting);
            var output = Mapper.Map<ClassAppConfigurationOutputModel>(setting);
            return output;
        }

        /// <summary>
        /// The get actual service configuration status.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// true: Configuration is needed
        /// false: Configuration is completed
        /// </returns>
        public IClassAppUiConfigurationOutputModel GetActualServiceConfigurationStatus(ClassAppInputModel filter, IWapiOperation operation)
        {
            // Create return value
            IClassAppUiConfigurationOutputModel output = ClassAppUiConfigurationOutputModel.Factory();
            output.ConfigurationSetting = new List<ClassAppConfigurationOutputModel>();
            output.Filter = filter;

            // Determine if they are value in the table that is different to 'N'
            bool isAny = this.repositoryWithTypeId.Query<SyKlassAppConfigurationSetting>().Any(predicate: x => x.ItemStatus != 'N');
            if (isAny)
            {
                // If the configuration is not complete ask KlassApp for the configuration
                var listofConfiguration = this.SendConfigurationSettingsToMobile(filter, operation);
                bool isNotconfig = listofConfiguration.Any(x => x.ItemStatus != 'N');
                output.IsAnyConfigurationPending = isNotconfig;
                ////if (isNotconfig == false)
                ////{
                ////    output.ConfigurationSetting = listofConfiguration;
                ////}
            }
            else
            {
                // Get the configuration and return false the is any configuration setting pending.
                ////output.ConfigurationSetting = this.GetAllConfigurationSettings(null);
                output.IsAnyConfigurationPending = false;
            }

            return output;
        }

        #endregion

        #region Custom Field Processing................

        /// <summary>
        /// The process custom fields.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        internal void ProcessCustomFields(ClassAppInputModel filter, IWapiOperation operation)
        {
            // Get all setting that has custom fields
            var settingsCustom = this.allSettings.Where(x => x.IsCustomField == 1).ToList();

            if (settingsCustom.Any())
            {
                Uri uri = new Uri(operation.KlassAppUrlConfigCustomFields + "?limit=700");
                var cfa = this.api.GetAllForCommandInClassApplication(operation, uri, "GETCUSTOM");
                if (!string.IsNullOrWhiteSpace(cfa.ErrorCode))
                {
                    // Error in get All Custom Fields
                    var errorText = string.Format("{0} - {1}", cfa.ErrorCode, cfa.Explanation);
                    foreach (SyKlassAppConfigurationSetting app in settingsCustom)
                    {
                        app.UpdateSettingConfiguration(app.KlassAppIdentification, errorText, this.userName, app.IsActive, app.ItemStatus);
                        this.repositoryWithTypeId.Update(app);
                        return;
                    }
                }

                // Get all Custom Field with Status I
                var settingsI = settingsCustom.Where(x => x.ItemStatus == 'I');

                // Process Insertion of Custom Fields
                this.ProcessInsertionCustomField(settingsI.ToList(), cfa, operation);

                // Get all Custom Field with Status U
                var settingsU = settingsCustom.Where(x => x.ItemStatus == 'U');

                // Process Update of Custom Fields
                this.ProcessUpdateCustomField(settingsU.ToList(), cfa, operation);

                // Get all Custom Field with Status D
                var settingsD = settingsCustom.Where(x => x.ItemStatus == 'D');

                // Process Delete of Custom Fields
                this.ProcessDeleteCustomField(settingsD.ToList(), cfa, operation);
            }
        }

        /// <summary>
        /// The process insertion custom field.
        /// </summary>
        /// <param name="settingList">
        /// The setting list.
        /// </param>
        /// <param name="customFieldList">
        /// The custom Field List.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessInsertionCustomField(IList<SyKlassAppConfigurationSetting> settingList, IGeneralResponse customFieldList, IWapiOperation operation)
        {
            if (settingList.Count == 0)
            {
                return; // do nothing...
            }

            ////var customFieldList = response.ObjectFromServer;
            foreach (SyKlassAppConfigurationSetting setting in settingList)
            {
                try
                {
                    // Exists in the list
                    bool isFound = false;
                    foreach (dynamic din in customFieldList.ObjectFromServer)
                    {
                        if (din.item_label.ToString() == setting.ItemLabel & din.field_type.ToString() == setting.ItemValueType)
                        {
                            setting.UpdateSettingConfiguration(Convert.ToInt32(din.id), "OK", this.userName, 1, 'N');
                            this.repositoryWithTypeId.Update(setting);
                            isFound = true;
                            break;
                        }
                    }

                    if (isFound)
                    {
                        continue;
                    }

                    // No exists in the list. Insert in server....
                    CustomField custom = Mapper.Map<CustomField>(setting);
                    IGeneralResponse gr = this.api.PostCustomFieldToKlassApp(custom, operation);
                    this.UpdateSettingValuesInTable(gr, setting);
                    this.repositoryWithTypeId.Update(setting);
                }
                catch (Exception e)
                {
                    // Error Code happens Store it
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, e.Message, this.userName, 1, setting.ItemStatus);
                }
            }
        }

        /// <summary>
        /// The process delete custom field.
        /// </summary>
        /// <param name="settingList">
        /// The setting list.
        /// </param>
        /// <param name="customFieldList">
        /// The custom field list.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessDeleteCustomField(IList<SyKlassAppConfigurationSetting> settingList, IGeneralResponse customFieldList, IWapiOperation operation)
        {
            if (settingList.Count == 0)
            {
                return; // do nothing...
            }

            ////var customFieldList = response.ObjectFromServer;
            foreach (SyKlassAppConfigurationSetting setting in settingList)
            {
                try
                {
                    // Exists in the list
                    ////CustomField custom = Mapper.Map<CustomField>(setting);
                    ////bool isFound = false;
                    ////foreach (dynamic din in customFieldList.ObjectFromServer)
                    ////{
                    ////    if (Convert.ToInt32(din.id) == setting.KlassAppIdentification)
                    ////    {
                    ////        // Update Field
                    ////        IGeneralResponse gr = this.api.DeleteCustomFieldToKlassApp(custom, operation);
                    ////        this.UpdateSettingValuesInTable(gr, setting, 0);
                    ////        this.repositoryWithTypeId.Update(setting);
                    ////        isFound = true;
                    ////        break;
                    ////    }
                    ////}

                    ////if (isFound)
                    ////{
                    ////    continue;
                    ////}

                    // No exists in the list. Inactive custom setting status to N log OK....
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, "OK", this.userName, 0, 'N');
                    this.repositoryWithTypeId.Update(setting);
                }
                catch (Exception e)
                {
                    // Error Code happens Store it
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, e.Message, this.userName, 1, setting.ItemStatus);
                }
            }
        }

        /// <summary>
        /// The process update custom field.
        /// </summary>
        /// <param name="settingList">
        /// The setting list.
        /// </param>
        /// <param name="customFieldList">
        /// The custom field list.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessUpdateCustomField(IList<SyKlassAppConfigurationSetting> settingList, IGeneralResponse customFieldList, IWapiOperation operation)
        {
            if (settingList.Count == 0)
            {
                return; // do nothing...
            }

            ////var customFieldList = response.ObjectFromServer;
            foreach (SyKlassAppConfigurationSetting setting in settingList)
            {
                try
                {
                    // Exists in the list
                    CustomField custom = Mapper.Map<CustomField>(setting);
                    bool isFound = false;
                    foreach (dynamic din in customFieldList.ObjectFromServer)
                    {
                        if (Convert.ToInt32(din.id) == setting.KlassAppIdentification)
                        {
                            // Update Field
                            IGeneralResponse gr = this.api.PutCustomFieldToKlassApp(custom, operation);
                            this.UpdateSettingValuesInTable(gr, setting);
                            this.repositoryWithTypeId.Update(setting);
                            isFound = true;
                            break;
                        }
                    }

                    if (isFound)
                    {
                        continue;
                    }

                    // No exists in the list. Insert in server....
                    IGeneralResponse gr1 = this.api.PostCustomFieldToKlassApp(custom, operation);
                    this.UpdateSettingValuesInTable(gr1, setting);
                    this.repositoryWithTypeId.Update(setting);
                }
                catch (Exception e)
                {
                    // Error Code happens Store it
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, e.Message, this.userName, 1, setting.ItemStatus);
                }
            }
        }

        #endregion

        #region Location Processing....................

        /// <summary>
        /// The process locations fields.
        /// </summary>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessLocations(IWapiOperation operation)
        {
            // Get all setting that has locations
            var settingsCustom = this.allSettings.Where(x => x.IsCustomField == 0 && x.KlassOperationTypeObject.Code == ClassAppApi.OperationCodeLocations).ToList();

            if (settingsCustom.Any())
            {
                Uri uri = new Uri(operation.KlassAppUrlConfigLocations + "?limit=700");
                var cfa = this.api.GetAllForCommandInClassApplication(operation, uri, "GETLOCATION");
                if (!string.IsNullOrWhiteSpace(cfa.ErrorCode))
                {
                    // Error in get All Locations Fields
                    var errorText = $"{cfa.ErrorCode} - {cfa.Explanation}";
                    foreach (SyKlassAppConfigurationSetting app in settingsCustom)
                    {
                        app.UpdateSettingConfiguration(app.KlassAppIdentification, errorText, this.userName, app.IsActive, app.ItemStatus);
                        this.repositoryWithTypeId.Update(app);
                        return;
                    }
                }

                // Get all Locations Field with Status I
                var settingsI = settingsCustom.Where(x => x.ItemStatus == 'I');

                // Process Insertion of Locations Fields
                this.ProcessInsertionLocations(settingsI.ToList(), cfa, operation);

                // Get all Locations Field with Status U
                var settingsU = settingsCustom.Where(x => x.ItemStatus == 'U');

                // Process Locations Fields
                this.ProcessUpdateLocations(settingsU.ToList(), cfa, operation);

                // Get all Locations Field with Status D
                var settingsD = settingsCustom.Where(x => x.ItemStatus == 'D');

                // Process Delete of Locations Fields
                this.ProcessDeleteLocations(settingsD.ToList(), cfa, operation);
            }
        }

        /// <summary>
        /// The process insertion locations.
        /// </summary>
        /// <param name="settingList">
        /// The setting list.
        /// </param>
        /// <param name="cfa">
        /// The CFA.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessInsertionLocations(List<SyKlassAppConfigurationSetting> settingList, IGeneralResponse cfa, IWapiOperation operation)
        {
            if (settingList.Count == 0)
            {
                return; // do nothing...
            }

            ////var customFieldList = response.ObjectFromServer;
            foreach (SyKlassAppConfigurationSetting setting in settingList)
            {
                try
                {
                    // Exists in the list
                    bool isFound = false;
                    foreach (dynamic din in cfa.ObjectFromServer)
                    {
                        var name = Convert.ToString(din.name);
                        if (name == setting.ItemValue)
                        {
                            setting.UpdateSettingConfiguration(Convert.ToInt32(din.id), "OK", this.userName, 1, 'N');
                            this.repositoryWithTypeId.Update(setting);
                            isFound = true;
                            break;
                        }
                    }

                    if (isFound)
                    {
                        continue;
                    }

                    // No exists in the list. Insert in server....
                    // Get the campus associated...
                    // Fill location class
                    var campus = this.repository.Get<Campus>(new Guid(setting.AdvantageIdentification));
                    var location = Mapper.Map<Locations>(campus);
                    this.FillMandatoryCampusName(location);
                    ////Locations custom = Mapper.Map<Locations>(setting);
                    IGeneralResponse gr = this.api.PostLocationToKlassApp(location, operation);
                    this.UpdateSettingValuesInTable(gr, setting);
                    this.repositoryWithTypeId.Update(setting);
                }
                catch (Exception e)
                {
                    // Error Code happens Store it
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, e.Message, this.userName, 1, setting.ItemStatus);
                }
            }
        }

        /// <summary>
        /// The process update locations.
        /// </summary>
        /// <param name="settingList">
        /// The setting list.
        /// </param>
        /// <param name="cfa">
        /// The Advantage configuration list.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessUpdateLocations(List<SyKlassAppConfigurationSetting> settingList, IGeneralResponse cfa, IWapiOperation operation)
        {
            if (settingList.Count == 0)
            {
                return; // do nothing...
            }

            foreach (SyKlassAppConfigurationSetting setting in settingList)
            {
                try
                {
                    // Exists in the list
                    ////Locations loc = Mapper.Map<Locations>(setting);
                    bool isFound = false;
                    foreach (dynamic din in cfa.ObjectFromServer)
                    {
                        if (Convert.ToInt32(din.id) == setting.KlassAppIdentification)
                        {
                            // Update locations in KLASSAPP...
                            var campus = this.repository.Get<Campus>(new Guid(setting.AdvantageIdentification));
                            var location = Mapper.Map<Locations>(campus);
                            location.id = setting.KlassAppIdentification;
                            this.FillMandatoryCampusName(location);
                            IGeneralResponse gr = this.api.PutLocationToKlassApp(location, operation);
                            this.UpdateSettingValuesInTable(gr, setting);
                            this.repositoryWithTypeId.Update(setting);
                            isFound = true;
                            break;
                        }
                    }

                    if (isFound)
                    {
                        continue;
                    }

                    // No exists in the list. Insert in server....
                    var campus1 = this.repository.Get<Campus>(new Guid(setting.AdvantageIdentification));
                    var location1 = Mapper.Map<Locations>(campus1);
                    this.FillMandatoryCampusName(location1);
                    IGeneralResponse gr1 = this.api.PostLocationToKlassApp(location1, operation);
                    this.UpdateSettingValuesInTable(gr1, setting);
                    this.repositoryWithTypeId.Update(setting);
                }
                catch (Exception e)
                {
                    // Error Code happens Store it
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, e.Message, this.userName, 1, setting.ItemStatus);
                }
            }
        }

        /// <summary>
        /// The process delete locations.
        /// </summary>
        /// <param name="settingList">
        /// The setting list.
        /// </param>
        /// <param name="cfa">
        /// The CFA.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessDeleteLocations(List<SyKlassAppConfigurationSetting> settingList, IGeneralResponse cfa, IWapiOperation operation)
        {
            if (settingList.Count == 0)
            {
                return; // do nothing...
            }

            ////var customFieldList = response.ObjectFromServer;
            foreach (SyKlassAppConfigurationSetting setting in settingList)
            {
                try
                {
                    ////    // Exists in the list
                    ////    ////Locations loc = Mapper.Map<Locations>(setting);
                    ////    bool isFound = false;
                    ////    foreach (dynamic din in cfa.ObjectFromServer)
                    ////    {
                    ////        if (Convert.ToInt32(din.id) == setting.KlassAppIdentification)
                    ////        {
                    ////            // Delete Field (the delete is logical not physic)
                    ////            IGeneralResponse gr = this.api.DeleteLocationToKlassApp(setting.KlassAppIdentification, operation);
                    ////            this.UpdateSettingValuesInTable(gr, setting, 0);
                    ////            this.repositoryWithTypeId.Update(setting);
                    ////            isFound = true;
                    ////            break;
                    ////        }
                    ////    }

                    ////    if (isFound)
                    ////    {
                    ////        continue;
                    ////    }

                    // No exists in the list. Inactive custom setting status to N log OK....
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, "OK", this.userName, 0, 'N');
                    this.repositoryWithTypeId.Update(setting);
                }
                catch (Exception e)
                {
                    // Error Code happens Store it
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, e.Message, this.userName, 1, setting.ItemStatus);
                }
            }
        }

        #endregion

        #region Process Majors

        /// <summary>
        /// The process majors.
        /// </summary>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessMajors(IWapiOperation operation)
        {
            // Get all setting that has majors
            var settingsMajors = this.allSettings.Where(x => x.IsCustomField == 0 && x.KlassOperationTypeObject.Code == ClassAppApi.OperationCodeProgramVersion).ToList();

            if (settingsMajors.Any())
            {
                Uri uri = new Uri(operation.KlassAppUrlConfigMajors + "?limit=700");
                var cfa = this.api.GetAllForCommandInClassApplication(operation, uri, "GETPROGRAM");
                if (!string.IsNullOrWhiteSpace(cfa.ErrorCode))
                {
                    // Error in get All Locations Fields
                    var errorText = $"{cfa.ErrorCode} - {cfa.Explanation}";
                    foreach (SyKlassAppConfigurationSetting app in settingsMajors)
                    {
                        app.UpdateSettingConfiguration(app.KlassAppIdentification, errorText, this.userName, app.IsActive, app.ItemStatus);
                        this.repositoryWithTypeId.Update(app);
                        return;
                    }
                }

                // Get all Locations Field with Status I
                var settingsI = settingsMajors.Where(x => x.ItemStatus == 'I');

                // Process Insertion of Locations Fields
                this.ProcessInsertionMajors(settingsI.ToList(), cfa, operation);

                // Get all Locations Field with Status U
                var settingsU = settingsMajors.Where(x => x.ItemStatus == 'U');

                // Process Locations Fields
                this.ProcessUpdateMajors(settingsU.ToList(), cfa, operation);

                // Get all Locations Field with Status D
                var settingsD = settingsMajors.Where(x => x.ItemStatus == 'D');

                // Process Delete of Locations Fields
                this.ProcessDeleteMajors(settingsD.ToList(), cfa, operation);
            }
        }

        /// <summary>
        /// The process delete majors.
        /// </summary>
        /// <param name="settingList">
        /// The setting list.
        /// </param>
        /// <param name="cfa">
        /// The CFA.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessDeleteMajors(List<SyKlassAppConfigurationSetting> settingList, IGeneralResponse cfa, IWapiOperation operation)
        {
            if (settingList.Count == 0)
            {
                return; // do nothing...
            }

            ////var customFieldList = response.ObjectFromServer;
            foreach (SyKlassAppConfigurationSetting setting in settingList)
            {
                try
                {
                    // Exists in the list
                    ////Locations loc = Mapper.Map<Locations>(setting);
                    ////bool isFound = false;
                    ////foreach (dynamic din in cfa.ObjectFromServer)
                    ////{
                    ////    if (Convert.ToInt32(din.id) == setting.KlassAppIdentification)
                    ////    {
                    ////        // Delete Field (the delete is logical not physic)
                    ////        IGeneralResponse gr = this.api.DeleteMajorToKlassApp(setting.KlassAppIdentification, operation);
                    ////        this.UpdateSettingValuesInTable(gr, setting, 0);
                    ////        this.repositoryWithTypeId.Update(setting);
                    ////        isFound = true;
                    ////        break;
                    ////    }
                    ////}

                    ////if (isFound)
                    ////{
                    ////    continue;
                    ////}

                    // No exists in the list. Inactive custom setting status to N log OK....
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, "OK", this.userName, 0, 'N');
                    this.repositoryWithTypeId.Update(setting);
                }
                catch (Exception e)
                {
                    // Error Code happens Store it
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, e.Message, this.userName, 1, setting.ItemStatus);
                }
            }
        }

        /// <summary>
        /// The process update majors.
        /// </summary>
        /// <param name="settingList">
        /// The setting list.
        /// </param>
        /// <param name="cfa">
        /// The CFA.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessUpdateMajors(List<SyKlassAppConfigurationSetting> settingList, IGeneralResponse cfa, IWapiOperation operation)
        {
            if (settingList.Count == 0)
            {
                return; // do nothing...
            }

            foreach (SyKlassAppConfigurationSetting setting in settingList)
            {
                try
                {
                    // Exists in the list
                    bool isFound = false;
                    foreach (dynamic din in cfa.ObjectFromServer)
                    {
                        if (Convert.ToInt32(din.id) == setting.KlassAppIdentification)
                        {
                            // Update Field Majors
                            var programVersion = this.repository.Get<ProgramVersion>(new Guid(setting.AdvantageIdentification));
                            var majors = Mapper.Map<Majors>(programVersion);
                            majors.id = setting.KlassAppIdentification;
                            IGeneralResponse gr = this.api.PutMajorsToKlassApp(majors, operation);
                            this.UpdateSettingValuesInTable(gr, setting);
                            this.repositoryWithTypeId.Update(setting);
                            isFound = true;
                            break;
                        }
                    }

                    if (isFound)
                    {
                        continue;
                    }

                    // No exists in the list. Insert in server....
                    var majorlist = this.repository.Get<ProgramVersion>(new Guid(setting.AdvantageIdentification));
                    var major1 = Mapper.Map<Majors>(majorlist);
                    IGeneralResponse gr1 = this.api.PostMajorsToKlassApp(major1, operation);
                    this.UpdateSettingValuesInTable(gr1, setting);
                    this.repositoryWithTypeId.Update(setting);
                }
                catch (Exception e)
                {
                    // Error Code happens Store it
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, e.Message, this.userName, 1, setting.ItemStatus);
                }
            }
        }

        /// <summary>
        /// The process insertion majors.
        /// </summary>
        /// <param name="settingList">
        /// The setting list.
        /// </param>
        /// <param name="cfa">
        /// The CFA.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessInsertionMajors(List<SyKlassAppConfigurationSetting> settingList, IGeneralResponse cfa, IWapiOperation operation)
        {
            if (settingList.Count == 0)
            {
                return; // do nothing...
            }

            ////var customFieldList = response.ObjectFromServer;
            foreach (SyKlassAppConfigurationSetting setting in settingList)
            {
                try
                {
                    // Exists in the list
                    bool isFound = false;
                    foreach (dynamic din in cfa.ObjectFromServer)
                    {
                        var name = Convert.ToString(din.name);
                        if (name == setting.ItemValue)
                        {
                            setting.UpdateSettingConfiguration(Convert.ToInt32(din.id), "OK", this.userName, 1, 'N');
                            this.repositoryWithTypeId.Update(setting);
                            isFound = true;
                            break;
                        }
                    }

                    if (isFound)
                    {
                        continue;
                    }

                    // No exists in the list. Insert in server....
                    // Get the majors associated...
                    // Fill location class
                    var majorList = this.repository.Get<ProgramVersion>(new Guid(setting.AdvantageIdentification));
                    var major = Mapper.Map<Majors>(majorList);
                    IGeneralResponse gr = this.api.PostMajorsToKlassApp(major, operation);
                    this.UpdateSettingValuesInTable(gr, setting);
                    this.repositoryWithTypeId.Update(setting);
                }
                catch (Exception e)
                {
                    // Error Code happens Store it
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, e.Message, this.userName, 1, setting.ItemStatus);
                }
            }
        }

        #endregion

        #region Key Values Processing..........

        /// <summary>
        /// The process key values.
        /// </summary>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessKeyValues(IWapiOperation operation)
        {
            // Get all setting that has valid key values
            var settingsKeyValue = this.allSettings.Where(x => x.IsCustomField == 0 && (x.KlassOperationTypeObject.Code == ClassAppApi.OperationStudentStatus)).ToList();

            if (settingsKeyValue.Any())
            {
                Uri uri = new Uri(operation.KlassAppUrlConfigKeyValuesPairs + "?limit=700");
                var cfa = this.api.GetAllForCommandInClassApplication(operation, uri, "GETKEYVALUE");
                if (!string.IsNullOrWhiteSpace(cfa.ErrorCode))
                {
                    // Error in get All Custom Fields
                    var errorText = $"{cfa.ErrorCode} - {cfa.Explanation}";
                    foreach (SyKlassAppConfigurationSetting app in settingsKeyValue)
                    {
                        app.UpdateSettingConfiguration(app.KlassAppIdentification, errorText, this.userName, app.IsActive, app.ItemStatus);
                        this.repositoryWithTypeId.Update(app);
                        return;
                    }
                }

                // Get all Custom Field with Status I
                var settingsI = settingsKeyValue.Where(x => x.ItemStatus == 'I');

                // Process Insertion of Custom Fields
                this.ProcessInsertionKeyValue(settingsI.ToList(), cfa, operation);

                // Get all Custom Field with Status U
                var settingsU = settingsKeyValue.Where(x => x.ItemStatus == 'U');

                // Process Update of Custom Fields
                this.ProcessUpdateKeyValue(settingsU.ToList(), cfa, operation);

                // Get all Custom Field with Status D
                var settingsD = settingsKeyValue.Where(x => x.ItemStatus == 'D');

                // Process Delete of Custom Fields
                this.ProcessDeleteKeyValues(settingsD.ToList(), cfa, operation);
            }
        }

        /// <summary>
        /// The process insertion key value.
        /// </summary>
        /// <param name="settingList">
        /// The setting list.
        /// </param>
        /// <param name="cfa">
        /// The CFA.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessInsertionKeyValue(List<SyKlassAppConfigurationSetting> settingList, IGeneralResponse cfa, IWapiOperation operation)
        {
            if (settingList.Count == 0)
            {
                return; // do nothing...
            }

            ////var customFieldList = response.ObjectFromServer;
            foreach (SyKlassAppConfigurationSetting setting in settingList)
            {
                try
                {
                    // Exists in the list
                    bool isFound = false;
                    foreach (dynamic din in cfa.ObjectFromServer)
                    {
                        string name = Convert.ToString(din.name).ToLowerInvariant();
                        string sett = setting.ItemValue.ToLowerInvariant();
                        if (name == sett)
                        {
                            setting.UpdateSettingConfiguration(Convert.ToInt32(din.id), "OK", this.userName, 1, 'N');
                            this.repositoryWithTypeId.UpdateAndFlush(setting);
                            isFound = true;
                            break;
                        }
                    }

                    if (isFound)
                    {
                        continue;
                    }

                    // No exists in the list. Insert in server....
                    this.InsertUpdateAllTypeKeyValueToServer(operation, setting, true);
                }
                catch (Exception e)
                {
                    // Error Code happens Store it
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, e.Message, this.userName, 1, setting.ItemStatus);
                }
            }
        }

        /// <summary>
        /// The process update key value.
        /// </summary>
        /// <param name="settingList">
        /// The setting list.
        /// </param>
        /// <param name="cfa">
        /// The CFA.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessUpdateKeyValue(List<SyKlassAppConfigurationSetting> settingList, IGeneralResponse cfa, IWapiOperation operation)
        {
            if (settingList.Count == 0)
            {
                return; // do nothing...
            }

            foreach (SyKlassAppConfigurationSetting setting in settingList)
            {
                try
                {
                    // Exists in the list
                    bool isFound = false;
                    foreach (dynamic din in cfa.ObjectFromServer)
                    {
                        if (Convert.ToInt32(din.id) == setting.KlassAppIdentification)
                        {
                            // Update Field
                            var del = Convert.ToInt32(din.deleted);
                            this.InsertUpdateAllTypeKeyValueToServer(operation, setting, false);
                            setting.UpdateSettingConfiguration(Convert.ToInt32(din.id), "OK", this.userName, del, 'N');
                            isFound = true;
                            break;
                        }
                    }

                    if (isFound)
                    {
                        continue;
                    }

                    // No exists in the list. Insert in server....
                    this.InsertUpdateAllTypeKeyValueToServer(operation, setting, true);
                }
                catch (Exception e)
                {
                    // Error Code happens Store it
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, e.Message, this.userName, 1, setting.ItemStatus);
                }
            }
        }

        /// <summary>
        /// The process delete key values.
        /// </summary>
        /// <param name="settingList">
        /// The setting list.
        /// </param>
        /// <param name="cfa">
        /// The CFA.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        private void ProcessDeleteKeyValues(List<SyKlassAppConfigurationSetting> settingList, IGeneralResponse cfa, IWapiOperation operation)
        {
            if (settingList.Count == 0)
            {
                return; // do nothing...
            }

            ////var customFieldList = response.ObjectFromServer;
            foreach (SyKlassAppConfigurationSetting setting in settingList)
            {
                try
                {
                    // Exists in the list
                    ////KeyValuePart loc = Mapper.Map<KeyValuePart>(setting);
                    ////bool isFound = false;
                    ////foreach (dynamic din in cfa.ObjectFromServer)
                    ////{
                    ////    if (Convert.ToInt32(din.id) == setting.KlassAppIdentification)
                    ////    {
                    ////        // Delete Field (the delete is logical not physic)
                    ////        IGeneralResponse gr = this.api.DeleteKeyValueToKlassApp(setting.KlassAppIdentification, operation);
                    ////        this.UpdateSettingValuesInTable(gr, setting, 0);
                    ////        this.repositoryWithTypeId.Update(setting);
                    ////        isFound = true;
                    ////        break;
                    ////    }
                    ////}

                    ////if (isFound)
                    ////{
                    ////    continue;
                    ////}

                    // No exists in the list. Inactive custom setting status to N log OK....
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, "OK", this.userName, 0, 'N');
                    this.repositoryWithTypeId.Update(setting);
                }
                catch (Exception e)
                {
                    // Error Code happens Store it
                    setting.UpdateSettingConfiguration(setting.KlassAppIdentification, e.Message, this.userName, 1, setting.ItemStatus);
                }
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// The insert update all type key value to server.
        /// </summary>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <param name="setting">
        /// The setting.
        /// </param>
        /// <param name="isInsert">
        /// The is insert.
        /// </param>
        private void InsertUpdateAllTypeKeyValueToServer(IWapiOperation operation, SyKlassAppConfigurationSetting setting, bool isInsert)
        {
            // Determine if it is a Program Version or a Status
            KeyValuePart kv;
            if (setting.KlassOperationTypeObject.Code == ClassAppApi.OperationStudentStatus)
            {
                // Fill The Student Status
                var studentStatus = this.repository.Get<UserDefStatusCode>(new Guid(setting.AdvantageIdentification));
                kv = Mapper.Map<KeyValuePart>(studentStatus);
                kv.type = setting.KlassOperationTypeObject.Code;
                kv.id = setting.KlassAppIdentification;
                var gr = isInsert ? this.api.PostKeyValuePairsToKlassApp(kv, operation) : this.api.PutKeyValuePairToKlassApp(kv, operation);

                this.UpdateSettingValuesInTable(gr, setting);
                this.repositoryWithTypeId.UpdateAndFlush(setting);
                return;
            }

            // Unknown Type
            IGeneralResponse genError = GeneralResponse.Factory();
            genError.ErrorCode = "KEYVALUE";
            genError.Explanation = "Key Value Type is unknown";
            this.UpdateSettingValuesInTable(genError, setting);
            this.repositoryWithTypeId.UpdateAndFlush(setting);
        }

        /// <summary>
        /// The update setting values in table.
        /// </summary>
        /// <param name="gr">
        /// The gr.
        /// </param>
        /// <param name="setting">
        /// The setting.
        /// </param>
        /// <param name="isActive">
        /// The is active.
        /// </param>
        private void UpdateSettingValuesInTable(IGeneralResponse gr, SyKlassAppConfigurationSetting setting, int isActive = 1)
        {
            if (string.IsNullOrWhiteSpace(gr.ErrorCode))
            {
                setting.UpdateSettingConfiguration(gr.KlassAppIdentification, "OK", this.userName, isActive, 'N');
            }
            else
            {
                var err = gr.ErrorCode + " " + gr.Explanation;
                err = err.Length > 499 ? err.Substring(0, 498) : err;
                setting.UpdateSettingConfiguration(setting.KlassAppIdentification, err, this.userName, isActive, setting.ItemStatus);
            }
        }

        /// <summary>
        /// The fill mandatory campus name.
        /// </summary>
        /// <param name="location">
        /// The location.
        /// </param>
        private void FillMandatoryCampusName(Locations location)
        {
            location.address = string.IsNullOrEmpty(location.address) ? "-" : location.address;
            location.phone = string.IsNullOrEmpty(location.phone) ? "0" : location.phone;
            location.location_type = string.IsNullOrEmpty(location.location_type) ? "campus" : location.location_type;
            location.contact_email = string.IsNullOrEmpty(location.contact_email) ? "fame@fameinc.com" : location.contact_email;
            location.contact_person = string.IsNullOrEmpty(location.contact_person) ? "-" : location.contact_person;
            location.contact_phone = string.IsNullOrEmpty(location.contact_phone) ? "0" : location.contact_phone;
            location.contact_position = string.IsNullOrEmpty(location.contact_position) ? "0" : location.contact_position;
        }
        #endregion

        public bool SendClearOperationToKlassApp(ClassAppInputModel filter, IWapiOperation operation)
        {
            Uri uri = new Uri(operation.ExternalUrl + "data/1.0/clearall");
            
            // Get from KlassApp the token
            
            var result = this.api.GetClearAllTokenFromClassApplication(operation, uri, "GETTOKEN");

            if (!string.IsNullOrEmpty(result.Explanation))
            {
                throw new ApplicationException($"A exception is returned. Error Code: {result.ErrorCode} - {result.Explanation}");
            }

            string cfa = result.ObjectFromServer.auth_token.ToString();

            // Send the token to KlassApp
            uri = new Uri($"{operation.ExternalUrl}data/1.0/clearall/{cfa}");
            this.api.SendTokenToClassApplication(operation, uri, "SENDCLEARTOKEN");
            return true;

        }
    }
}
