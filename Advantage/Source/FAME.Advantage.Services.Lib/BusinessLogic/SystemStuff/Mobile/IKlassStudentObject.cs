﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IKlassStudentObject.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the IKlassStudentObject type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Mobile
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.Domain.SystemStuff;

    /// <summary>
    /// The <code>KlassStudentObject</code> interface.
    /// </summary>
    public interface IKlassStudentObject
    {
        /// <summary>
        /// Gets or sets the campus active GUID list.
        /// </summary>
        IList<Guid> CampusActiveGuidList { get; set; }

        /// <summary>
        /// Gets or sets the KLASS APP days to considerer.
        /// </summary>
        int KlassAppDaysToConsiderer { get; set; }

        /// <summary>
        /// Gets or sets the KLASS APP post student info.
        /// </summary>
        string KlassPostStudentInfo { get; set; }

        /// <summary>
        /// Gets or sets the operation.
        /// </summary>
        SyWapiSettings Operation { get; set; }
    }
}