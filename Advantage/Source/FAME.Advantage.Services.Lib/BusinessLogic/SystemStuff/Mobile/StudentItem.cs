﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentItem.cs" company="FAME">
//   2016
// </copyright>
// <summary>
// JAGG - The student info item.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Messages.Wapi.KlassApp
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// The student info item.
    /// Can be referrer to status or locations or other student information
    /// The field: field_type define the use
    /// </summary>
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "Valid here to be send to Json"),
     SuppressMessage("ReSharper", "StyleCop.SA1300", Justification = "Valid here to be send to Json")]
    public class StudentItem
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public DateTime name { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether deleted.
        /// </summary>
        public bool deleted { get; set; }

        /// <summary>
        /// Gets or sets the created_at.
        /// </summary>
        public DateTime created_at { get; set; }

        /// <summary>
        /// Gets or sets the updated_at.
        /// </summary>
        public DateTime updated_at { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether auto_disable.
        /// </summary>
        public bool auto_disable { get; set; }  
    }
}