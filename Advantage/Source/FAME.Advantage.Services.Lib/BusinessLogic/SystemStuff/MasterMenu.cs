﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MasterMenu.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The master menu.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings;
    using FAME.Advantage.Domain.SystemStuff.Menu;
    using FAME.Advantage.Domain.Users; 
    using FAME.Advantage.Messages.SystemStuff.Menu;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
    using FAME.Advantage.Services.Lib.Infrastructure.Extensions;

    /// <summary>
    /// The master menu.
    /// </summary>
    public class MasterMenu
    { 

        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repository;

        /// <summary>
        /// The repository with guid.
        /// </summary>
        private readonly IRepositoryWithTypedID<Guid> repositoryWithGuid;

        /// <summary>
        /// The exclusions.
        /// </summary>
        private List<int> exclusions = new List<int>();

        /// <summary>
        /// Initializes a new instance of the <see cref="MasterMenu"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="repositorytyped">
        /// The repositorytyped.
        /// </param>
        public MasterMenu(IRepositoryWithTypedID<int> repository, IRepositoryWithTypedID<Guid> repositorytyped)
        {
            this.repository = repository;
            this.repositoryWithGuid = repositorytyped;
        }

        /// <summary>
        /// The sub level menu items.
        /// </summary>
        /// <param name="moduleName">
        /// The module name.
        /// </param>
        /// <param name="subMenuName">
        /// The sub menu name.
        /// </param>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// return exceptions when siteUri is empty
        /// </exception>
        public IQueryable<MenuItem> SubLevelMenuItems(string moduleName, string subMenuName, GetMenuItemInputModel filter)
        {
            // get the website base url.  This will be used to construct the full url for the menu links
            var configurationAppSetting = this.repository.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == "ADVANTAGESITEURI");
            if (configurationAppSetting != null)
            {
                string siteUri =
                    configurationAppSetting
                        .ConfigurationValues[0].Value;

                // validate that site url exists in config settings
                if (string.IsNullOrEmpty(siteUri))
                {
                    throw new HttpBadRequestResponseException("Site Url not found in Config settings");
                }

                // get top level menu item, and retrieve the id and module code
                var module = this.repository.Query<MenuItem>().FirstOrDefault(n => n.MenuName.ToUpper() == moduleName.ToUpper());
                var moduleId = module?.ID;
                var moduleCode = module?.ModuleCode;

                // get the id of the sublevel item based on the submenuname and moduleid as parent
                var subMenuId =
                    this.repository.Query<MenuItem>()
                        .Where(n => n.MenuName.ToUpper() == subMenuName.ToUpper() && n.ParentId == moduleId)
                        .Select(n => n.ID)
                        .FirstOrDefault();

                // get the sub level items where the parent is the sub menu id just retrieved
                var subLevelMenuItems = this.repository.Query<MenuItem>().Where(n => n.ParentId == subMenuId);

                IList<short> enabledResources = null;
                bool isSupportUser = true;

                if (!this.IsSupportUser(filter.UserId))
                {
                    enabledResources = this.GetEnabledResourcesForUser(filter.UserId, filter.CampusId);
                    isSupportUser = false;
                }

                // build the urls for the sublevel children (page level)
                subLevelMenuItems.ForEach(r => r.BuildUrl(siteUri, moduleCode, filter.CampusId, enabledResources, isSupportUser, filter.IsImpersonating));

                this.GetExclusions(filter.CampusId, filter.UserId);

                bool isImp = filter.IsImpersonating != null && filter.IsImpersonating == true;
                if (!this.IsSupportUser(filter.UserId) && !isImp)
                {
                    this.exclusions.Add(806);
                }

                // get only the items with children
                subLevelMenuItems = subLevelMenuItems.Where(n => n.ChildMenuItems.Any());

                // filter out any menu items excluded by config settings
                subLevelMenuItems.ForEach(n => n.FilterOutExclusions(this.exclusions))
                    .ForEach(r => r.ChildMenuItems.ForEach(n => n.FilterOutExclusions(this.exclusions)));
                return subLevelMenuItems;
            }

            throw new HttpBadRequestResponseException("Configuration setting is missing in Config settings");
        }

        /// <summary>
        /// Return a list of submenu items (link to pages directly)
        /// </summary>
        /// <param name="menusItems">
        /// The menus items.
        /// </param>
        /// <param name="submoduleName">
        /// The submodule name.
        /// </param>
        /// <returns>
        /// The List of MenuItem.
        /// List of MenuItem
        /// </returns>
        public IList<MenuItem> GetLastLevelItems(IQueryable<MenuItem> menusItems, string submoduleName)
        {
            IList<MenuItem> attendanceList = new List<MenuItem>();
            var firstOrDefault = menusItems.FirstOrDefault(x => x.MenuName.ToUpper() == submoduleName);
            if (firstOrDefault != null)
            {
                attendanceList = firstOrDefault.ChildrenForOutput;
            }

            return attendanceList;
        }

        /// <summary>
        /// The is page enabled for user.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsPageEnabledForUser(GetMenuItemInputModel filter)
        {
            if (this.IsSupportUser(filter.UserId))
            {
                return true;
            }

            if (filter.PageName.ToUpper() == "MAINTENANCEHOME.ASPX" || filter.PageName.ToUpper() == "REPORTHOME.ASPX"
                                                                    || filter.PageName.ToUpper()
                                                                    == "CHANGEPASSWORD.ASPX")
            {
                return true;
            }

            var enabledResources = this.GetEnabledResourcesForUser(filter.UserId, filter.CampusId);
            var pages = this.repository.Query<MenuItem>().Where(n => n.ResourceUrl.ToUpper().Contains(filter.PageName.ToUpper()) && n.ItemType.ItemType.ToUpper() == "PAGE");
            bool result = Enumerable.Any(pages.Where(p => p.ResourceId != null), p => p.ResourceId != null && enabledResources.Contains((short)p.ResourceId));
            return result;
        }

        /// <summary>
        /// The get sub level menu items.
        /// </summary>
        /// <param name="menuName">
        /// The menu name.
        /// </param>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<MenuItem> GetSubLevelMenuItems(string menuName, GetMenuItemInputModel filter)
        {
            var parentId =
                this.repository.Query<MenuItem>()
                    .Where(n => n.MenuName.ToUpper() == menuName.ToUpper())
                    .Select(n => n.ID)
                    .FirstOrDefault();

            // get the sublevel items using the parent's menu id
            var subLevelMenuItems = this.repository.Query<MenuItem>().Where(n => n.ParentId == parentId);

            this.GetExclusions(filter.CampusId, filter.UserId);

            // filter out any menu items that should not be shown based on config settings
            subLevelMenuItems.ForEach(n => n.FilterOutExclusions(this.exclusions))
                .ForEach(r => r.ChildMenuItems.ForEach(n => n.FilterOutExclusions(this.exclusions))); 

            // only get menu items that have children
            subLevelMenuItems = subLevelMenuItems.Where(n => n.ChildMenuItems.Any());
            return subLevelMenuItems;
        }

        /// <summary>
        /// The get sub level menu items with links.
        /// </summary>
        /// <param name="menuName">
        /// The menu name.
        /// </param>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The  List of MenuItem
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Http Bad Request Response Exception
        /// </exception>
        public List<MenuItem> GetSubLevelMenuItemsWithLinks(string menuName, GetMenuItemInputModel filter)
        {
            // get the website base url.  This will be used to construct the full url for the menu links
            var configurationAppSetting = this.repository.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == "ADVANTAGESITEURI");
            if (configurationAppSetting == null)
            {
                throw new HttpBadRequestResponseException("Configuration setting is missing in Config settings");
            }

            var siteUri = configurationAppSetting.ConfigurationValues[0].Value;

            // validate that site url exists in config settings
            if (string.IsNullOrEmpty(siteUri))
            {
                throw new HttpBadRequestResponseException("Site Url not found in Config settings");
            }

            // get top level menu item, and retrieve the id and module code
            var module = this.repository.Query<MenuItem>().FirstOrDefault(n => n.MenuName.ToUpper() == menuName.ToUpper());
            if (module == null)
            {
                throw new HttpBadRequestResponseException("Module could not be found");
            }

            var moduleCode = module.ModuleCode;
            IList<short> enabledResources = null;
            var isSupportUser = true;

            if (!this.IsSupportUser(filter.UserId))
            {
                enabledResources = this.GetEnabledResourcesForUser(filter.UserId, filter.CampusId);
                isSupportUser = false;
            }

            // build the urls for the sublevel children (page level)
            module.BuildUrl(siteUri, moduleCode, filter.CampusId, enabledResources, isSupportUser, filter.IsImpersonating);

            this.GetExclusions(filter.CampusId, filter.UserId);

            var isImp = filter.IsImpersonating != null && filter.IsImpersonating == true;
            if (!this.IsSupportUser(filter.UserId) && !isImp)
            {
                this.exclusions.Add(806);
            }

            // get only the items with children
            var subLevelMenuItems = module.ChildMenuItems;

            // filter out any menu items excluded by config settings
            subLevelMenuItems.ForEach(n => n.FilterOutExclusions(this.exclusions));

            return subLevelMenuItems.ToList();
        }

        #region Private Procedures

        /// <summary>
        /// The get config app setting value.
        /// </summary>
        /// <param name="keyName">
        /// The key name.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetConfigAppSettingValue(string keyName, Guid campusId)
        {
            var configurationAppSetting = this.repository.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName == keyName);

            if (configurationAppSetting != null)
            {
                var configSetting = configurationAppSetting.ConfigurationValues.Where(n => n.Campus != null && n.Campus.ID == campusId).ToList();
                if (!configSetting.Any())
                {
                    configSetting = configurationAppSetting.ConfigurationValues.Where(n => n.Campus == null).ToList();
                }

                if (configSetting.Any())
                {
                    return configSetting.FirstOrDefault()?.Value;
                }
            }

            return string.Empty;
        }

        // private bool IsSysAdmin(Guid userId, Guid campusId)
        // {
        // var userRoles = this.repositoryWithGuid.Query<UserRolesCampusGroupBridge>()
        // .Where(n => n.CampusGroup.Campuses.Any(c => c.ID == campusId) && n.User.ID == userId)
        // .Select(n => n.Role).Distinct();

        // return Enumerable.Any(userRoles, role => role.SystemRole.Description.ToUpper() == "SYSTEM ADMINISTRATOR");
        // }

        /// <summary>
        /// Determine if the userid is the support user
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool IsSupportUser(Guid userId)
        {
            var user = this.repositoryWithGuid.Query<User>().FirstOrDefault(n => n.ID == userId);
            return user != null && user.UserName.ToUpper() == "SUPPORT";
        }

        /// <summary>
        /// retrieve and fill exclusions list of menu items to
        /// exclude based on config setting values
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        private void GetExclusions(Guid campusId, Guid userId)
        {
            // cache object
            this.exclusions = new List<int>();

            // ShowRossOnlyTabs
            // JG - 02/26/2014 As per Lori, this config setting is being opened so these pages will not be hidden
            // FillExclusions(68, null, campusId, "FALSE", "TRUE", new[] {541, 542, 532, 534, 535, 538, 543, 539},
            // new[] {142, 375, 330, 476, 508, 102, 107, 237, 217, 290, 230, 286, 200});
            // JG - 03/06/2014 - As per Lori and Chris, we are now to show all of these pages
            // TrackSapAttendance
            // FillExclusions(72, null, campusId, "BYDAY", "BYCLASS", new[] { 800, 327 }, new[] { 633, 539 });

            // SchedulingMethod
            this.FillExclusions(65, null, campusId, "REGULARTRADITIONAL", null, new[] { 91, 497 }, null);

            // ShowCollegeOfCourtReporting
            this.FillExclusions(118, null, campusId, "NO", "YES", new[] { 614, 615, 729 }, new[] { 497 });

            // FameESP
            this.FillExclusions(37, null, campusId, "NO", null, new[] { 517, 523, 525, 699 }, null);

            // EDExpress
            this.FillExclusions(91, null, campusId, "NO", null, new[] { 603, 604, 605, 619, 698 }, null);

            // GradeBookWeightingLevel
            this.FillExclusions(43, null, campusId, "COURSELEVEL", "INSTRUCTORLEVEL", new[] { 107, 96, 222 }, new[] { 476, 541 });

            // JG 03/17/204 -removing as per lori and chris.  This menu item is not in menu anymore.
            ////ShowExternshipTabs
            // FillExclusions(71, null, campusId, "NO", null, new[] { 538 }, null);

            // TrackFASAP
            this.FillExclusions(null, "TRACKFASAP", campusId, "FALSE", null, new[] { 801 }, null);
            
            // ConsolidateCourseComponents
            this.FillExclusions(147, null, campusId, "FALSE", null, new[] { 796, 799 }, null);
           
            // TimeClockClassSection
            this.FillExclusions(null, "TimeClockClassSection", campusId, "NO", null, new[] { 7, 769 }, null);
            
            // ShowApportioningCreditBalance
            this.FillExclusions(148, null, campusId, "FALSE", null, new[] { 798 }, null);

            // PrintAttendanceSheet
            this.FillExclusions(null, "PrintAttendanceSheet", campusId, "FALSE", null, new[] { 591, 580 }, null);

            this.FillExclusions(null, "ShowStateBoardAccreditationAgencyReports", campusId, "NO", null, new[] { 808, 863 }, null);

            // PostServicesByClass
            this.FillExclusionsForPostServicesByClass(campusId);

            this.FillSpecialExclusions(userId);
        }

        /// <summary>
        /// fill exclusions list based on parameters passed in
        /// </summary>
        /// <param name="id">id of config value</param>
        /// <param name="keyName">keyname of config value</param>
        /// <param name="campusId"> campus for campus specific values</param>
        /// <param name="firstValue"> first value to check</param>
        /// <param name="secondValue">second value to check</param>
        /// <param name="firstValueItems">exclude these items if first value found</param>
        /// <param name="secondValueItems">exclude these items if second value found</param>
        private void FillExclusions(int? id, string keyName, Guid campusId, string firstValue, string secondValue, int[] firstValueItems, int[] secondValueItems)
        {
            var configurationAppSetting = this.repository.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.ID == id);

            if (!string.IsNullOrEmpty(keyName))
            {
                configurationAppSetting = this.repository.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == keyName);
            }

            if (configurationAppSetting != null)
            {
                var configSetting = configurationAppSetting.ConfigurationValues.Where(n => n.Campus != null && n.Campus.ID == campusId).ToList();
                if (!configSetting.Any())
                {
                    configSetting = configurationAppSetting.ConfigurationValues.Where(n => n.Campus == null).ToList();
                }

                if (configSetting.Any())
                {
                    string configValue = configSetting.Select(n => n.Value).Single();

                    if (string.IsNullOrEmpty(secondValue))
                    {
                        if (configValue.ToUpper() == firstValue)
                        {
                            this.exclusions.AddRange(firstValueItems);
                        }
                    }
                    else
                    {
                        if (configValue.ToUpper() == firstValue)
                        {
                            this.exclusions.AddRange(firstValueItems);
                        }
                        else if (configValue.ToUpper() == secondValue)
                        {
                            this.exclusions.AddRange(secondValueItems);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Retrieve and fill exclusion for post services by class config setting value
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        private void FillExclusionsForPostServicesByClass(Guid campusId)
        {
            var configurationAppSetting = this.repository.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == "POSTSERVICESBYCLASS");

            if (configurationAppSetting != null)
            {
                var configSetting = configurationAppSetting.ConfigurationValues.Where(n => n.Campus != null && n.Campus.ID == campusId).ToList();
                if (!configSetting.Any())
                {
                    configSetting = configurationAppSetting.ConfigurationValues.Where(n => n.Campus == null).ToList();
                }

                if (configSetting.Any())
                {
                    string configValue = configSetting.Select(n => n.Value).Single();

                    if (configValue.ToUpper() == "FALSE")
                    {
                        var configurationAppSettingRoss =
                            this.repository.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.ID == 68);
                        if (configurationAppSettingRoss != null)
                        {
                            var configSettingRoss =
                                configurationAppSettingRoss.ConfigurationValues.Where(
                                    n => n.Campus != null && n.Campus.ID == campusId).ToList();
                            if (!configSettingRoss.Any())
                            {
                                configSettingRoss =
                                    configurationAppSettingRoss.ConfigurationValues.Where(n => n.Campus == null).ToList(); 
                            }

                            if (configSettingRoss.Any())
                            {
                                // string configValueRoss = configSettingRoss.Select(n => n.Value).Single();
                                if (configValue.ToUpper() == "FALSE")
                                {
                                    this.exclusions.Add(508);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Exclude manage configuration settings and upload logo pages if the
        /// user is not the support user
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        private void FillSpecialExclusions(Guid userId)
        {
            // manage configuration settings and upload logo should not be shown.
            if (!this.IsSupportUser(userId))
            {
                this.exclusions.AddRange(new List<int> { 616, 797, 235, 246, 326, 797, 833, 867 });
            }
        }

        /// <summary>
        /// The get enabled resources for user.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The List of short.
        /// </returns>
        private IList<short> GetEnabledResourcesForUser(Guid userId, Guid campusId)
        {
            // Master list of resource level resource ids for this
            // user and campus id
            var resourceLevels = this.repository.Session.CreateSQLQuery("exec GetAvailableResourcesByUserAndCampus :UserId, :CampusId")
                .SetGuid("UserId", userId)
                .SetGuid("CampusId", campusId)
                .List<short>();

            // return resource levels object
            resourceLevels.Add(264);
            return resourceLevels;
        }

        #endregion Private Procedures
    }
}