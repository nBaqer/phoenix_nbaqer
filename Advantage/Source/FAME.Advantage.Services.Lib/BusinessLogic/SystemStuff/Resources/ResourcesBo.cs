﻿using System;
using System.Collections.Generic;
using System.Linq;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff.Resources;
using FAME.Advantage.Messages.Common;
using FAME.Advantage.Messages.Lead;
using FAME.Advantage.Messages.SystemStuff.Resources;

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Resources
{
    using System.Diagnostics;

    using FAME.Advantage.Domain.Catalogs;
    using FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings;
    using FAME.Advantage.Services.Lib.BusinessLogic.Lead;

    /// <summary>
    /// Class for resources...
    /// </summary>
    public class ResourcesBo
    {
        // ReSharper disable NotAccessedField.Local
        private readonly IRepository repository;

        // ReSharper restore NotAccessedField.Local
        private readonly IRepositoryWithTypedID<int> repositoryint;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="leadRepository"></param>
        /// <param name="repositoryWithInt"></param>
        public ResourcesBo(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = leadRepository;
            this.repositoryint = repositoryWithInt;
        }

        /// <summary>
        /// The get required IPEDS list.
        /// </summary>
        /// <returns>
        /// The list of fields that are required for IPEDS
        /// </returns>
        public IList<ResourcesRequiredAndDllOutputModel> GetRequiredIpedsList()
        {
            var configurationAppSetting = this.repositoryint.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == "IPEDS");
            Debug.Assert(configurationAppSetting != null, "configurationAppSetting != null");
            var setting = configurationAppSetting.ConfigurationValues[0].Value;
            
            if (setting.ToLower() == "yes")
            {
               var query = this.repositoryint.Query<RptAgencyFields>()
                    .Where(x => x.RptAgencyId.Descrip.Equals("IPEDS"))
                    .Select(
                        x => new ResourcesRequiredAndDllOutputModel()
                        {
                            FldName = x.Fields.Field.FldName,
                            Required = true
                        }).ToList();
                return query;
            }
            else
            {
                return null;
            }   
        }

        /// <summary>
        /// Get a list of captions and Required flags
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The list of possible duplicates
        /// </returns>
        public IList<ResourcesRequiredAndDllOutputModel> GetCaptionAndRequiredList(LeadInputModel filter)
        {
            IList<ResourcesRequiredAndDllOutputModel> list = new List<ResourcesRequiredAndDllOutputModel>();

            var query =
                this.repositoryint.Query<ResourceTableFields>().Where(x => x.ResourceObj.ID == filter.PageResourceId);
            
            var configurationAppSetting = this.repositoryint.Query<ConfigurationAppSetting>().FirstOrDefault(n => n.KeyName.ToUpper() == "MAJORSMINORSCONCENTRATIONS");
            var setting = configurationAppSetting.ConfigurationValues[0].Value;

            foreach (ResourceTableFields resourceTableFieldse in query)
            {
                var input = new ResourcesRequiredAndDllOutputModel();
                var fields = resourceTableFieldse.TableFieldsObj.FieldObj;

                // input.Caption = fields.FieldsCaptionsList.AsQueryable()
                // .SingleOrDefault(x => x.LenguagesObj.LangName == "EN-US")
                // .Caption;
                input.FldName = fields.FldName;
               
                if (!string.IsNullOrEmpty(fields.FldName) && fields.FldName.ToUpper() == "PRGVERSIONTYPEID") 
                {
                    if(!string.IsNullOrEmpty(setting) && setting.ToUpper() == "YES")
                    {
                        input.Required = true;
                    }
                    else
                    {
                        input.Required = false;
                    }
                }
                else
                {
                    input.Required = resourceTableFieldse.IsRequired;
                }

                var catlog = new CatalogsInputModel();
                catlog.FldName = fields.FldName;
                if (!string.IsNullOrEmpty(filter.UserId))
                {
                    catlog.UserId = Guid.Parse(filter.UserId);
                }

                catlog.CampusId = filter.CampusId;

                input.Dll =
                    Catalogs.CatalogStatic.ResourcesDropDownOutputModels(catlog, this.repository, this.repositoryint)
                        .ToList();
                list.Add(input);
            }

            // return query.ToList();
            return list;
        }
    }
}