﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UniversalSearchBo.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.UniversalSearchBo
// </copyright>
// <summary>
//   The universal search business object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Domain.Campuses.CampusGroups;
    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Domain.Users;
    using FAME.Advantage.Messages.Common;
    using FAME.Advantage.Messages.SystemStuff.UniversalSearch;
    using AutoMapper;

    /// <summary>
    /// The universal search business object.
    /// </summary>
    public class UniversalSearchBo
    {
        /// <summary>
        /// The NHibernate repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The repository with type id.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithTypeId;

        /// <summary>
        /// Initializes a new instance of the <see cref="UniversalSearchBo"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="repositoryWithTypeId">
        /// The repository with type id.
        /// </param>
        public UniversalSearchBo(IRepository repository, IRepositoryWithTypedID<int> repositoryWithTypeId)
        {
            this.repository = repository;
            this.repositoryWithTypeId = repositoryWithTypeId;
        }

        /// <summary>
        /// The get campuses.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetCampuses(FilterUniversalSearchInput input)
        {
            if (input == null)
            {
                throw new Exception("Filter can't be null");
            }
            if (input.UserId == Guid.Empty)
            {
                throw new Exception("User can't be empty");
            }

            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

            if (user == null)
            {
                throw new Exception("User Acount can't be found");
            }

            var listItems =
                this.repository.Query<User>()
                    .Where(n => n.ID == input.UserId)
                    .SelectMany(n => n.CampusGroup)
                    .SelectMany(y => y.Campuses)
                    .Where(y => y.Status.StatusCode == (input.ShowInactive ? "I" : "A"))
                    .OrderBy(x => x.Description)
                    .Distinct().ToList();

            var output = listItems.Select(x => new DropDownOutputModel() { ID = x.ID.ToString(), Description = x.Description });

            return output;
        }

        /// <summary>
        /// The get lead status.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetLeadStatus(FilterUniversalSearchInput input)
        {
            if (input == null)
            {
                throw new Exception("Filter can't be null");
            }

            if (input.Campuses == null || input.Campuses.Count == 0)
            {
                throw new Exception("At least one campus must be selected");
            }

            if (input.UserId == Guid.Empty)
            {
                throw new Exception("User can't be empty");
            }

            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

            if (user == null)
            {
                throw new Exception("User Account can't be found");
            }

            var campusGroup = this.repository.Query<CampusGroup>().Where(x => x.Campuses.Any(c => input.Campuses.Contains(c.ID))).Select(x => x.ID);

            var queriable =
               this.repository.Query<UserDefStatusCode>()
                   .Where(
                       x => x.SystemStatus.StatusLevelId == 1
                       && x.SystemStatus.ID != 26 && campusGroup.Contains(x.CampGrpId)); // duplicated

            if (!input.ShowInactive)
            {
                queriable = queriable.Where(x => x.Status.StatusCode == "A");
            }
            else
            {
                queriable = queriable.Where(x => x.Status.StatusCode == "I");
            }

            IList<DropDownOutputModel> list = queriable.OrderByDescending(x => x.IsDefaultLeadStatus).ThenBy(x => x.Description).Select(x => new DropDownOutputModel
            {
                ID = x.ID.ToString(),
                Description = x.Description
            }).ToList();


            return list;
        }

        /// <summary>
        /// The get admin reps.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<DropDownOutputModel> GetAdminReps(FilterUniversalSearchInput input)
        {
            if (input == null)
            {
                throw new Exception("Filter can't be null");
            }

            if (input.Campuses == null || input.Campuses.Count == 0)
            {
                throw new Exception("At least one campus must be selected");
            }

            if (input.UserId == Guid.Empty)
            {
                throw new Exception("User can't be empty");
            }

            var user = this.repository.Query<User>().FirstOrDefault(x => x.ID == input.UserId && x.AccountActive == true);

            if (user == null)
            {
                throw new Exception("User Acount can't be found");
            }

            var campusGroup = this.repository.Query<CampusGroup>().Where(x => x.Campuses.Any(c => input.Campuses.Contains(c.ID))).Select(x => x.ID);

            var users =
                this.repository.Query<User>()
                    .Where(
                        x =>
                        !x.IsAdvantageSuperUser
                        && ((x.AccountActive == null && !input.ShowInactive)
                            || (x.AccountActive.Value == !input.ShowInactive)))
                    .Where(
                        x => x.RolesByCampusList.Any(n => campusGroup.Contains(n.CampusGroup.ID) && n.Role.SystemRole.ID == 3)).OrderBy(x => x.FullName).ToList();

            var output = users.Select(x => new DropDownOutputModel() { ID = x.ID.ToString(), Description = x.FullName });

            return output;
        }
    }
}
