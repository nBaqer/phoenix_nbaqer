﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.Common;
using FAME.Advantage.Messages.SystemStuff.Maintenance.Smtp;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Json;
using Google.Apis.Services;
using Google.Apis.Util.Store;


namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance
{
    public class SmtpSendEmailBo
    {
        // ReSharper disable NotAccessedField.Local
        private readonly IRepositoryWithTypedID<int> repositoryInt;
        private readonly IRepository repository;
        // ReSharper restore NotAccessedField.Local

        /// <summary>
        /// Application Name. This is use in all advantage application.
        /// </summary>
        private static string ApplicationName = "AdvantageService";

        /// <summary>
        /// Scope of the permission, in this case is send emails
        /// </summary>
        static readonly string[] Scopes = { GmailService.Scope.GmailSend };

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository">repository guid</param>
        /// <param name="repositoryInt">repository integer</param>
        public SmtpSendEmailBo(IRepository repository, IRepositoryWithTypedID<int> repositoryInt)
        {
            this.repository = repository;
            this.repositoryInt = repositoryInt;
        }

        /// <summary>
        /// Send mail using OAuth protocol
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="filter"></param>
        public IGenericOutput SendMailOauth(SmtpConfigurationSettings settings, SmtpSendMessageInputModel filter)
        {
            UserCredential credential;
            var appBinDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            var storeFilePath = new Uri(appBinDirectory + @"/GmailOauth/gmailAdvantageService.json").LocalPath;

            // Validate secret...
            if (string.IsNullOrWhiteSpace(settings.AutClientSecret))
            {
                throw new HttpRequestException("The secret setting does not exists in the server. Please first create the secret setting before use this."); 
            }

            if (string.IsNullOrWhiteSpace(settings.Token))
            {
                throw new HttpRequestException("The gmail token does not exists in the server. Please first create the token before use this."); 
 
            }
 
            //using (var stream = new FileStream(secretPath, FileMode.Open, FileAccess.Read))
            using (Stream secret = GenerateStreamFromString(settings.AutClientSecret))
            {
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(secret).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(storeFilePath, true)).Result;
                Console.WriteLine(@"Credential file saved to: " + storeFilePath);
            }

            // Create Gmail API service.
            var service = new GmailService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            // Create the message....................................
            var bodyByte = Convert.FromBase64String(filter.Body64);
            var body = Encoding.UTF8.GetString(bodyByte);

            var msg = new AE.Net.Mail.MailMessage // MailMessage
            {
                Subject = filter.Subject,
                Body = body,
                ContentType = filter.ContentType,
                Sender = string.IsNullOrWhiteSpace(filter.From) ? new MailAddress(settings.From) : new MailAddress(filter.From),
            };

            // Given the possibility to set the 'From', from filter (user defined)
            // or from settings (stored in Configuration setting)
            if (!string.IsNullOrWhiteSpace(filter.From))
            {
                msg.From = new MailAddress(filter.From);
                msg.ReplyTo.Add(new MailAddress(filter.From));
            }
            else
            {
                msg.From = new MailAddress(settings.From);
                msg.ReplyTo.Add(new MailAddress(settings.From));
            }

            if (!string.IsNullOrWhiteSpace(filter.Cc))
            {
                msg.Cc.Add(new MailAddress(filter.Cc));
            }
            msg.To.Add(new MailAddress(filter.To));
            msg.Encoding = Encoding.UTF8;

            var msgStr = new StringWriter();
            msg.Save(msgStr);


            var res = service.Users.Messages.Send(new Message
            {
                Raw = Base64UrlEncode(msgStr.ToString())
            }, "me").Execute();

            Debug.WriteLine("Message ID {0} sent.", res.Id);
            var result = GenericOutput.Factory();
            var snipperLength = (body.Length > 20) ? 20 : body.Length - 1;
            result.Note = res.Snippet ?? body.Substring(0, snipperLength);
            result.IsPassed = 1;
            return result;
        }

        /// <summary>
        /// Post Command 2: est if the authorization file exists.
        /// </summary>
        /// <param name="filter">No used</param>
        /// <returns></returns>
        public IGenericOutput TestIfOAuthMailIsAuthorized(SmtpSendMessageInputModel filter)
        {
            var appBinDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            var storeFilePath = new Uri(appBinDirectory + @"/GmailOauth/gmailAdvantageService.json").LocalPath;
            var fil = Directory.GetFiles(storeFilePath);
            var result = GenericOutput.Factory();
            result.IsPassed = (fil.Length > 0) ? 1 : 0;
            return result;
        }

        /// <summary>
        /// Send the email using normal SMTP protocol.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IGenericOutput SendMailUsingSmtpProtocol(SmtpConfigurationSettings settings, SmtpSendMessageInputModel filter)
        {
            // Create Smtp Client
            var smtpClient = new SmtpClient(settings.Server, settings.Port)
            {
                EnableSsl = (settings.IsSsl == 1),
                Credentials = new NetworkCredential(settings.Username, settings.Password)
            };

            // Create Email message
            var messageFrom = new MailAddress(settings.From);
            var messageTo = new MailAddress(filter.To);
            var message = new MailMessage(messageFrom, messageTo);
            byte[] data = Convert.FromBase64String(filter.Body64);
            string body = Encoding.UTF8.GetString(data);
            message.Body = body;
            message.Subject = filter.Subject;
            message.IsBodyHtml = true;

            // Send the message
            smtpClient.Send(message);
            var result = GenericOutput.Factory();
            result.IsPassed = 1;
            result.Note = (body.Length > 20)? body.Substring(0, 20): String.Empty;
            return result;
        }

        /// <summary>
        /// Get the file token
        /// </summary>
        /// <returns></returns>
        public string GetFileToken()
        {
            // Get the file token...
            string tokenFile = string.Empty;
            var appBinDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            var storeFilePath = new Uri(appBinDirectory + @"/GmailOauth/gmailAdvantageService.json").LocalPath;
            if (Directory.Exists(storeFilePath))
            {
                var filesList = Directory.GetFiles(storeFilePath);
                if (filesList.Length > 0)
                {
                    var file0 = filesList[0];
                    if (file0.Contains("Google.Apis.Auth"))
                    {
                        // Read the file content
                        var filepath = Path.Combine(storeFilePath, file0);
                        tokenFile = File.ReadAllText(filepath);
                    }
                }
            }
            return tokenFile;
        }

        public void SetfileToken(string token)
        {
            //Create the file data-store
            var appBinDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            var storeFilePath = new Uri(appBinDirectory + @"/GmailOauth/gmailAdvantageService.json").LocalPath;
            var secc = NewtonsoftJsonSerializer.Instance.Deserialize<Google.Apis.Auth.OAuth2.Responses.TokenResponse>(token);
            var filestore = new FileDataStore(storeFilePath, true);
            var wait = filestore.StoreAsync("user", secc);
            wait.Wait();
        }


        private static string Base64UrlEncode(string input)
        {
            var inputBytes = Encoding.UTF8.GetBytes(input);
            // Special "url-safe" base64 encode.
            return Convert.ToBase64String(inputBytes)
              .Replace('+', '-')
              .Replace('/', '_')
              .Replace("=", "");
        }

        private MemoryStream GenerateStreamFromString(string value)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(value ?? ""));
        }


    }
}
