﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UdfBo.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Defines the UdfBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Domain.Infrastructure.Entities;
    using Domain.SystemStuff.SchoolDefinedFields;
    using Domain.Users;
    using Domain.Users.UserRoles;

    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Messages.Lead;

    using Messages.Common;
    using Messages.SystemStuff.Udf;

    using NHibernate.Util;

    /// <summary>
    ///  The UDF bo.
    /// </summary>
    public class UdfBo
    {
        /// <summary>
        /// The lead repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryInt;

        /// <summary>
        /// Initializes a new instance of the <see cref="UdfBo"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="repository">
        /// repository GUID
        /// </param>
        /// <param name="repositoryInt">
        /// repository integer
        /// </param>
        public UdfBo(IRepository repository, IRepositoryWithTypedID<int> repositoryInt)
        {
            this.repository = repository;
            this.repositoryInt = repositoryInt;
        }

        /// <summary>
        /// The get entities items.
        /// The lender Entity must be eliminated manually (Resource Id = 434)
        /// </summary>
        /// <returns>
        /// The <see cref="IList&lt;DropDownIntegerOutputModel&gt;"/>.
        /// The drop down list of application entities 
        /// </returns>
        public IList<DropDownIntegerOutputModel> GetAdvantageEntityItems()
        {
            var list =
                this.repositoryInt.Query<Resources>()
                    .Where(x => x.ResourceTypeObj.ID == 8 && x.ID != 434)
                    .Select(info => new DropDownIntegerOutputModel() { ID = info.ID, Description = info.Resource })
                    .OrderBy(y => y.Description)
                    .ToList();
            return list;
        }

        /// <summary>
        /// The get pages associated with the entities that support UDF.
        /// </summary>
        /// <param name="filter">
        /// The entity ID 
        /// </param>
        /// <returns>
        /// Return the pages that can have UDFs.
        /// </returns>
        public IList<DropDownIntegerOutputModel> GetAdvantagePagesSupportUdfEntityItems(UdfInputModel filter)
        {
            var list =
                this.repositoryInt.Query<Resources>()
                    .Where(
                        x =>
                            x.ResourceTypeObj.ID != 1 && x.AllowSchlReqFields == true && x.ResourceRelatedUdfList.Any(
                                y => y.RelatedResourceObj.ID == filter.LeadEntity))
                    .Select(info => new DropDownIntegerOutputModel() { ID = info.ID, Description = info.Resource })
                    .OrderBy(y => y.Description)
                    .ToList();
            return list;
        }

        /// <summary>
        /// Get the list of assigned and unassigned UDF
        /// </summary>
        /// <param name="filter">
        /// Filter, the page resource Id.
        /// </param>
        /// <param name="output">
        /// The lists of no assigned UDF and the list of Assigned UDF
        /// </param>
        /// <returns>
        /// Get the list of no-assigned and assigned UDF to the page.
        /// </returns>
        public IUdfOutputModel GetUdfListsAssignedAndUnassigned(UdfInputModel filter, IUdfOutputModel output)
        {
            var list = this.repository.Query<Sdf>().Where(x => x.StatusObj.StatusCode == "A");
            if (list.Any())
            {
                var listass =
                    list.Where(
                            x =>
                                x.ResourcesList.Any(
                                    y => y.ResourceObj.ID == filter.PageResourceId && y.EntityObj.ID == filter.LeadEntity))
                        .ToList();
                output.AssignedToPageUdfList = listass.Select(
                    info =>
                        {
                            SdfResources sdfResources =
                                info.ResourcesList.SingleOrDefault(x => x.ResourceObj.ID == filter.PageResourceId);
                            if (sdfResources != null)
                            {
                                return new UdfDto()
                                           {
                                               Description = info.IsRequired ? info.SdfDescrip + " <span style='color:red;'> (Required)</span>" : info.SdfDescrip,
                                               Id = info.ID.ToString(),
                                               Position = sdfResources.Position,
                                               SdfVisibility = sdfResources.SdfVisibility == "Edit"
                                           };
                            }

                            throw new Exception("Spurious Info in DB. Please call support");
                        }).ToList();

                output.UnAssignedToPageUdfList =
                    list.Where(x => x.ResourcesList.All(y => y.ResourceObj.ID != filter.PageResourceId))
                        .Select(
                            info =>
                                new UdfDto
                                    {
                                        Description = info.IsRequired ? info.SdfDescrip + " <span style='color:red;'> (Required)</span>" : info.SdfDescrip,
                                        Id = info.ID.ToString(),
                                        Position = 0,
                                        SdfVisibility = false
                                    })
                        .OrderBy(x => x.Description)
                        .ToList();
            }

            return output;
        }

        /// <summary>
        /// The update insert school defined field to page.
        /// </summary>
        /// <param name="data">
        ///  The filter get the page resource. 
        /// The output model has the list of assigned to page UDF items
        /// </param>
        public void UpdateInsertSchoolDefinedFieldToPage(UdfOutputModel data)
        {
            var filter = data.Filter;
            var assigned = data.AssignedToPageUdfList;
            var user = this.repository.Query<User>().Single(x => x.ID.ToString() == filter.UserId);

            // note entity is not more used. need to be discussed if we eliminated it or not. for now fixed number
            // var moduleId = filter.LeadEntity == 395 ? 189 : 26;
            // var module = this.repositoryInt.Query<Resources>().SingleOrDefault(x => x.ID == moduleId);
            var entity = this.repositoryInt.Query<Resources>().SingleOrDefault(x => x.ID == filter.LeadEntity);
            var page = this.repositoryInt.Query<Resources>().SingleOrDefault(x => x.ID == filter.PageResourceId);

            var sysResources =
                this.repository.Query<SdfResources>()
                    .Where(x => x.ResourceObj.ID == filter.PageResourceId && x.EntityObj.ID == filter.LeadEntity).ToList();

            foreach (UdfDto dto in assigned)
            {
                var itis = sysResources.SingleOrDefault(x => x.SdfObj.ID == Guid.Parse(dto.Id));
                if (itis != null)
                {
                    // exists UPDATE
                    itis.UpdatePositionAndVisibility(dto);
                    continue;
                }

                // If does not exists add
                var sdf = this.repository.Query<Sdf>().SingleOrDefault(x => x.ID.ToString() == dto.Id);

                var sdfRes = new SdfResources(
                    page,
                    sdf,
                    "Edit",//dto.SdfVisibility ? "Edit" : "View",
                    user.UserName,
                    entity,
                    dto.Position);

                 dto.Id = sdfRes.SdfObj.ID.ToString(); // Update assigned list
                this.repository.SaveOnly(sdfRes);
            }

            // Remove those that are in syResources but not in assigned
            var deletelist = new List<Guid>();
            foreach (SdfResources item in sysResources)
            {
                var exists = assigned.Any(x => x.Id == item.SdfObj.ID.ToString());
                if (exists == false)
                {
                    deletelist.Add(item.ID);
                }
            }

            foreach (Guid guid in deletelist)
            {
                var item = sysResources.SingleOrDefault(x => x.ID == guid);
                 this.repository.Delete(item);
            }
        }

        /// <summary>
        /// The get custom field values.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <returns>
        /// The the list of the custom fields and their respective lead specific values
        /// </returns>
        public IList<LeadSdfOutputModel> GetSdfValues(string leadId)
        {
            var sdfList = new List<LeadSdfOutputModel>();

            // Get the SDF Fields
            var sdfFields = this.repository.Query<SdfModuleValueLead>().Where(x => x.LeadObj.ID.ToString() == leadId);
            if (sdfFields.Any())
            {
                foreach (SdfModuleValueLead field in sdfFields)
                {
                    var sdf = new LeadSdfOutputModel { SdfId = field.SdfObj.ID.ToString(), SdfValue = field.SdfValue, DtypeId = field.SdfObj.DtypeId };
                    sdfList.Add(sdf);
                }
            }

            return sdfList;
        }

        /// <summary>
        /// The insert custom field values.
        /// </summary>
        /// <param name="modUser">
        /// The user that has modified the data.
        /// </param>
        /// <param name="leadId">
        /// The lead Id.
        /// </param>
        /// <param name="sdfList">
        /// The custom fields list.
        /// </param>
        public void SaveSdfValues(string modUser, string leadId, IList<LeadSdfOutputModel> sdfList)
        {
            var leadObj = this.repository.Query<Lead>().SingleOrDefault(x => x.ID.ToString() == leadId);
            try
            {
                if (leadObj != null)
                {
                    if (sdfList != null)
                    {
                        // Loop through all fields that comes from Client
                        foreach (LeadSdfOutputModel model in sdfList)
                        {
                            var flag = false;

                            foreach (SdfModuleValueLead sdf in leadObj.SdfModuleValueList)
                            {
                                // Update Field if exists
                                if (model.SdfId == sdf.SdfObj.ID.ToString())
                                {
                                    sdf.UpdateSdfModuleValueLead(model.SdfValue, leadObj, modUser, DateTime.Now);
                                    flag = true;
                                    break;
                                }
                            }

                            if (flag)
                            {
                                continue;
                            }

                            LeadSdfOutputModel model1 = model;
                            var sdf1 = this.repository.Query<Sdf>()
                                .SingleOrDefault(x => x.ID.ToString() == model1.SdfId);

                            var sdfnew = new SdfModuleValueLead(sdf1, model.SdfValue, leadObj, modUser, DateTime.Now);
                            leadObj.SdfModuleValueList.Add(sdfnew);
                        }

                        this.repository.UpdateAndFlush(leadObj);
                    }
                }
            }
            catch (Exception)
            {
                // Avoid any update if a error occurred
                this.repository.Evict(leadObj);
                throw;
            }
        }
    }
}
