﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Messages.SystemStuff.Wapi;
using FAME.Advantage.Services.Lib.DataLayer.Wapi;

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance
{
    public class WapiCompanyNameBo
    {
        /// <summary>
        /// Update the two record of Company names in one transaction.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="comp"></param>
        /// <param name="repository"></param>
        /// <param name="tenantId"></param>
        public void UpdateExternalCompanyName(WapiCatalogsOutputModel model, SyWapiExternalCompanies comp, IRepositoryWithTypedID<int> repository, int tenantId)
        {
            var oldcode = comp.Code;
            // Validate if the old name exists and is assigned to this tenant, if problem trow exception.
            ValidateExistCompanyTenantRowWex(oldcode, tenantId);

            // Validate if the new name exists assigned to the actual tenant
            // if not exists execute all procedure, if exist only update Advantage.
            if (ValidateExistCompanyTenantRow(model.Code, tenantId) == 0)
            {


                try
                {
                    var tenconn = ConfigurationManager.ConnectionStrings["TenantAuthDBConnection"].ToString();
                    const string SQL =
                        "UPDATE dbo.WAPITenantCompanySecret SET ExternalCompanyCode = @NewName WHERE ExternalCompanyCode = @OldName AND TenantId = @TenantId";
                    var teconex = new SqlConnection(tenconn);

                    try
                    {
                        var tencomm = new SqlCommand(SQL, teconex);
                        tencomm.Parameters.AddWithValue("@NewName", model.Code);
                        tencomm.Parameters.AddWithValue("@OldName", comp.Code);
                        tencomm.Parameters.AddWithValue("@TenantId", tenantId);
                        teconex.Open();

                        tencomm.ExecuteNonQuery();
                    }
                    finally
                    {
                        teconex.Close();
                    }

                    // Update Advantage
                    try
                    {
                        comp.Update(model);
                        repository.UpdateAndFlush(comp);
                    }
                    catch (Exception)
                    {
                        // Rollback first change
                        try
                        {
                            var tencomm = new SqlCommand(SQL, teconex);
                            tencomm.Parameters.AddWithValue("@NewName", oldcode);
                            tencomm.Parameters.AddWithValue("@OldName", model.Code);
                            teconex.Open();

                            tencomm.ExecuteNonQuery();
                        }
                        finally
                        {
                            teconex.Close();
                        }

                        throw;
                    }
                }

                catch (Exception ex)
                {
                    throw new Exception(string.Format("One error in Update Company Code: {0}", ex.Message));
                }
            }
            else
            {
                // Only update Advantage
                try
                {
                    comp.Update(model);
                    repository.UpdateAndFlush(comp);
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("One error in Update Company Code in Advantage: {0}", ex.Message));
                }
            }
        }

        /// <summary>
        /// Insert a new operation
        /// </summary>
        /// <param name="model"></param>
        /// <param name="repository"></param>
        /// <param name="tenantId"></param>
        public WapiCatalogsOutputModel InsertNewCompanyExternalName(WapiCatalogsOutputModel model, IRepositoryWithTypedID<int> repository, int tenantId)
        {
            try
            {
                SqlConnection teconex = null;
                // See if exists for the same tenant the value
                if (ValidateExistCompanyTenantRow(model.Code, tenantId) == 0)
                {
                    var db = new WapiCompanyNameDb();
                     teconex = db.InsertCompanyExternalNameInTenant(model, tenantId);
                }
                // Update Advantage
                try
                {
                    var dom = new SyWapiExternalCompanies();
                    dom.Update(model);
                    repository.SaveAndFlush(dom);
                    model.Id = dom.ID;
                    return model;
                }
                catch (Exception)
                {
                    if (teconex != null)
                    {
                        // Rollback first change
                        try
                        {

                            const string SQLDEL =
                                "DELETE FROM WAPITenantCompanySecret WHERE ExternalCompanyCode = @NewName AND TenantId = @TenantId";
                            var tencomm = new SqlCommand(SQLDEL, teconex);
                            tencomm.Parameters.AddWithValue("@NewName", model.Code);
                            tencomm.Parameters.AddWithValue("@TenantId", tenantId);
                            teconex.Open();

                            tencomm.ExecuteNonQuery();
                        }
                        finally
                        {
                            teconex.Close();
                        }
                    }
                    throw;
                }
            }

            catch (Exception ex)
            {
                throw new Exception(string.Format("One error in Update Company Code: {0}", ex.Message));
            }
        }

        public WapiCatalogsOutputModel DeleteCompaniesObject(WapiCatalogsOutputModel model, IRepositoryWithTypedID<int> repository, int tenantId)
        {
            var dom = repository.Get<SyWapiExternalCompanies>(model.Id);

            if (dom != null)
            {
                // Check that the company has not dependencies with actual settings operations...
                if (dom.WapiSettingsList.Count > 0)
                {
                    throw new ApplicationException("You can not delete a Company that owner operation settings. First delete the operation or change the owner.");
                }

                repository.Delete(dom);
            }

            var db = new WapiCompanyNameDb();
            db.DeleteCompanyTenantRow(model.Code, tenantId);
            return model;
        }

        /// <summary>
        /// Synchro the tables in tenant and advantage with the companies names.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="repository"></param>
        /// <param name="tenantId"></param>
        /// <returns></returns>
        public WapiCatalogsOutputModel SynchroCompaniesName(WapiCatalogsOutputModel model, IRepositoryWithTypedID<int> repository, int tenantId)
        {
            var db = new WapiCompanyNameDb();
            
            // See if exists.....
            var res = db.GetExistsCompanyNameTenant(model.Code);

            if (res == 0)
            {
                // Does not exists insert with the actual tenant
                db.InsertCompanyExternalNameInTenant(model, tenantId);
                return model;
            }

            // Test if exists the name and with the same tenant
            res = db.GetExistsCompanyAndTenantRow(model.Code, tenantId);
            if (res != 0)
            {
                // Do nothing it is synchronized!
                return model;
            }

            // The name exists but for other tenant. change the name
            model.Code = this.ChangeCompanyName(model.Code, tenantId, repository);
            var domain = repository.Get<SyWapiExternalCompanies>(model.Id);
            domain.Update(model);
            repository.UpdateAndFlush(domain);

            // Insert in TenantDB
            db.InsertCompanyExternalNameInTenant(model, tenantId);
            return model;
        }


        #region Helper and validation functions

        /// <summary>
        /// Validate if exists the company name in TenantDb or in the current 
        /// Advantage Db.
        /// </summary>
        /// <param name="companyName">The name to be tested</param>
        /// <param name="repository"></param>
        /// <param name="tenantid"></param>
        /// <returns>
        /// true if pass the validation
        /// false if found the name and then is invalid to use.
        /// </returns>
        public bool ValidateExistCompanyName(string companyName, IRepositoryWithTypedID<int> repository, int tenantid)
        {
            var db = new WapiCompanyNameDb();
            var res = db.GetExistsCompanyNameTenant(companyName);
            var res1 = db.GetExistsCompanyAndTenantRow(companyName, tenantid);
            res = res - res1; // Exclude if exists the same company code for the present tenant!
            res += repository.Query<SyWapiExternalCompanies>().Count(x => x.Code == companyName);
            return (res == 0);
        }


        /// <summary>
        /// Validate if the given company name has a record in db with the same tenant id.
        /// </summary>
        /// <param name="companyName"></param>
        /// <param name="tenantId"></param>
        /// <returns>
        /// 0 if not exists company  code associate with the tenant
        /// 1 if exist company associated with the tenant.
        /// </returns>
        public int ValidateExistCompanyTenantRow(string companyName, int tenantId)
        {
            var db = new WapiCompanyNameDb();
            var res = db.GetExistsCompanyAndTenantRow(companyName, tenantId);
            return res;

        }

        /// <summary>
        /// Validate if the given company name has a record in db with the same tenant id.
        /// </summary>
        /// <param name="companyName"></param>
        /// <param name="tenantId"></param>
        /// <exception cref="ApplicationException">
        /// YOU NEED TO RUN SYNCHRO COMPANY NAMES BEFORE RUN THIS OPERATION
        /// </exception>
        public void ValidateExistCompanyTenantRowWex(string companyName, int tenantId)
        {
            var res = ValidateExistCompanyTenantRow(companyName, tenantId);
            if (res == 0)
            {
                throw new ApplicationException("YOU NEED TO RUN SYNCHRO COMPANY NAMES BEFORE RUN THIS OPERATION");
            }
        }


        /// <summary>
        /// Change the name of conflictive operation for other valid
        /// </summary>
        /// <param name="companyName"></param>
        /// <param name="tenantId"></param>
        /// <param name="repository"></param>
        /// <returns>The new name</returns>
        /// <exception cref="ApplicationException"> throw exception if the new name grow bigger than 50 characters</exception>
        public string ChangeCompanyName(string companyName, int tenantId, IRepositoryWithTypedID<int> repository)
        {
            bool valid = false;
            var nc = companyName;
            while (valid == false)
            {
                nc += tenantId.ToString(CultureInfo.InvariantCulture);
                if (nc.Length > 50)
                {
                    throw new ApplicationException("WAS NOT POSSIBLE TO FOUND A ADECUATE NAME IN THE SYNCHRO OPERATION. CALL SUPPORT TO ADRESS THIS DIRECTLY IN THE DATABASE");
                }

                valid = ValidateExistCompanyName(nc, repository, tenantId);
            }

            return nc;
        }

        #endregion
    }
}
