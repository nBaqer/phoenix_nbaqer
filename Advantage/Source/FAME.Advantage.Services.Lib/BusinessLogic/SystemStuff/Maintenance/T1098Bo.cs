﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="T1098Bo.cs" company="FAME">
//   2015 -2016
// </copyright>
// <summary>
// JAGG -  Business class for 1098T Controller
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Net.Http;

    using Domain.Campuses;
    using Domain.Infrastructure.Entities;
    using Messages.SystemStuff.Maintenance;
    using Messages.SystemStuff.Maintenance.T1098;
    using Messages.SystemStuff.Maintenance.T1098.Tables;

    using NHibernate.Transform;

    /// <summary>
    /// Business class for 1098T Controller
    /// </summary>
    public class T1098Bo
    {
        // ReSharper disable NotAccessedField.Local
        
        /// <summary>
        ///  The repository integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryInt;

        // ReSharper restore NotAccessedField.Local
        
        /// <summary>
        ///  The repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="T1098Bo"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="repository">
        /// repository guid
        /// </param>
        /// <param name="repositoryInt">
        /// repository integer
        /// </param>
        public T1098Bo(IRepository repository, IRepositoryWithTypedID<int> repositoryInt)
        {
            this.repository = repository;
            this.repositoryInt = repositoryInt;
        }

        /// <summary>
        /// Collected the info and send the information to FAME
        /// </summary>
        /// <param name="filter">input filter</param>
        /// <returns>the output response model</returns>
        public T1098OutputModel SendInfoToFame(T1098InputModel filter)
        {
            var result = new T1098OutputModel { NumberOfStudentProcessed = 0 };

            // Get from advantage Database the student to be processed
            var info1098 = this.GetInfo1098(filter);

            // Initialize output to FAME
            var payload = Fame1098InputModel.Factory(filter.Year.ToString());

            // Get School Active Campus Parameters
            // Get the actual campus value
            var campusObj = this.repository.Query<Campus>().SingleOrDefault(x => x.ID.ToString() == filter.CampusId);
            if (campusObj == null)
            {
                throw new ApplicationException("Campus Not Found or Invalid");
            }

            // Fill the school object
            var schoolConfig = SchoolConfig.Factory(campusObj.Token1098TService, campusObj.SchoolCodeKissSchoolId);

            // Fill the table objects to be send to FAME....
            payload = this.FillingDtoListOfObjects(payload, info1098, schoolConfig);

            // Send to FAME
            result.Message = this.SendInfo1098ToFame(payload, schoolConfig);

            // Compose returned message.
            var message =
                string.Format(
                    "Processing by Advantage:{3} -Valid Enrollments: {0}{3} -Exclusion:{1}{3} -Archive: {2}{3} FAME Return: ",
                    payload.ValidEnrollmentsList.Count,
                    payload.ExclusionsList.Count,
                    payload.ArchiveList.Count,
                    Environment.NewLine);
            result.Message = message + result.Message;
            result.NumberOfStudentProcessed = info1098.Count;
            return result;
        }

        #region Private Methods
        /// <summary>
        ///  The exclusions DTO.
        /// </summary>
        /// <param name="schoolConfig">
        ///  The school configuration.
        /// </param>
        /// <param name="t1098">
        ///  The T1098.
        /// </param>
        /// <returns>
        /// The <see cref="ExclusionsDto"/>.
        /// </returns>
        private static ExclusionsDto ExclusionsDto(SchoolConfig schoolConfig, T1098Dto t1098)
        {
            var exc = new ExclusionsDto
            {
                StuAcctNum = schoolConfig.KissKey + ":" + t1098.EnrollmentId,
                FirstName = t1098.FirstName,
                LastName = t1098.LastName,
                SocSecNum = T1098Bo.SocSecNum(t1098),
                Addr1 = t1098.Address1,
                City = t1098.City,
                State = t1098.State,
                ZipCode = t1098.Zip,
            };
            return exc;
        }

        /// <summary>
        ///  The social security number.
        /// </summary>
        /// <param name="t1098">
        ///  The T1098.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string SocSecNum(T1098Dto t1098)
        {
            return string.IsNullOrEmpty(t1098.SSN)
                       ? string.Empty
                       : (t1098.SSN.IndexOf("-", StringComparison.Ordinal) == -1)
                           ? t1098.SSN.Insert(5, "-").Insert(3, "-")
                           : t1098.SSN;
        }

        /// <summary>
        /// The get info 1098.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;T1098Dto&gt;"/>.
        /// </returns>
        private IList<T1098Dto> GetInfo1098(T1098InputModel filter)
        {
            var result =
                this.repository.Session.CreateSQLQuery("exec USP_SA_Get1098TDataForTaxYear :TaskYear, :CampusId")
                    .SetResultTransformer(Transformers.AliasToBean<T1098Dto>())
                    .SetParameter("TaskYear", filter.Year)
                    .SetParameter("CampusId", filter.CampusId)
                    .List<T1098Dto>()
                    .ToList();

            return result;
        }

        /// <summary>
        /// The send info 1098 to fame.
        /// </summary>
        /// <param name="payload">
        ///  The payload.
        /// </param>
        /// <param name="schoolConfig">
        ///  The school config.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string SendInfo1098ToFame(Fame1098InputModel payload, SchoolConfig schoolConfig)
        {
            // Sending information to FAME...........................................
            // Getting Configuration Channel
            var url = ConfigurationManager.AppSettings["FAME1098TServiceAddress"];

            // Create the client
            var client = new HttpClient();

            // Get Token from web Config
            // string token = WebConfigurationManager.AppSettings["SCHOOL_TOKEN"];
            client.DefaultRequestHeaders.Add("SCHOOL_TOKEN", schoolConfig.CampusToken);

            string message = string.Empty;

            // Call service............
            var task = client.PostAsJsonAsync(url, payload).ContinueWith(
                taskwithmsg =>
                    {
                        var response = taskwithmsg.Result;
                        message = response.ReasonPhrase;
                    });
            task.Wait();

            return message;
        }

        /// <summary>
        ///  The filling DTO list of objects.
        /// </summary>
        /// <param name="tablelist">
        /// The table list.
        /// </param>
        /// <param name="studentList">
        ///  The student list.
        /// </param>
        /// <param name="schoolConfig">
        ///  The school config.
        /// </param>
        /// <returns>
        /// The <see cref="Fame1098InputModel"/>.
        /// </returns>
        private Fame1098InputModel FillingDtoListOfObjects(
            Fame1098InputModel tablelist,
            IList<T1098Dto> studentList,
            SchoolConfig schoolConfig)
        {
            // Get configuration values and other initializations
            var strFirstCalendarDay = "01/01/" + tablelist.Year;
            var strFirstTaxDeclarationDayNextYear = new DateTime(int.Parse(tablelist.Year) + 1, 1, 1);
            var strLastTaxDeclarationDayNextYear = new DateTime(int.Parse(tablelist.Year) + 1, 3, 31);
            //// IList<string> enrollmentValidList = new List<string>();

            foreach (T1098Dto t1098 in studentList)
            {
                // records exclusion.....
                if (string.IsNullOrEmpty(t1098.TransDate))
                {
                    continue;
                }

                if (t1098.CostForEnrollmentExist < 1)
                {
                    continue;
                }

                // Exclusion Table  population.. Continuing Education Program
                if (t1098.IsContinuingEd)
                {
                    var exc = T1098Bo.ExclusionsDto(schoolConfig, t1098);
                    exc.ExclusionCode = 5; // Program is a continuing ed program
                    tablelist.ExclusionsList.Add(exc);
                    continue;
                }

                // Exclusion Table  population... The student is a no start
                if (t1098.IsStudentNoStart == 1)
                {
                    var exc = T1098Bo.ExclusionsDto(schoolConfig, t1098);
                    exc.ExclusionCode = 4; // The student is a no start
                    tablelist.ExclusionsList.Add(exc);
                    continue;
                }

                if (DateTime.Parse(t1098.TransDate) < DateTime.Parse(strFirstCalendarDay))
                {
                    var exc = T1098Bo.ExclusionsDto(schoolConfig, t1098);
                    exc.ExclusionCode = 2; // No payments or refunds to report in current year
                    tablelist.ExclusionsList.Add(exc);
                    continue;
                }
               
                decimal priorYrInstitutionalChargesBalance = 0;
                var netPriorYrPayments = t1098.PriorYearPayments - t1098.PriorYearRefunds;
                if (t1098.PriorYrInstitutionalCharges > netPriorYrPayments)
                {
                    priorYrInstitutionalChargesBalance = t1098.PriorYrInstitutionalCharges - netPriorYrPayments;
                }

                // Valid enrollment enter the enrollment in the EnrollmentDto
                var enr = new ValidEnrollmentsDto();
                var grossPaid = t1098.GrossPayments - t1098.GrossRefunds;
                var decyrNetCost = t1098.CurrentYrInstitutionalCharges + priorYrInstitutionalChargesBalance;
                ////var decyrNetCost = t1098.TotalCost - (t1098.PriorYearPayments - t1098.PriorYearRefunds);
                var decAdjustmentsMadeForPriorYear = (t1098.StudentRefunds + t1098.LoanRefunds)
                                                     - (t1098.StudentPayments + t1098.LoanPayments);
                var decScholarShipsorGrants = (t1098.Scholarships + t1098.GrantPayments) - t1098.GrantRefunds;
                var decAdjustmentsToScholarship = t1098.GrantRefunds - t1098.GrantPayments;

                // Fill the enrollment DTO
                enr.StuAcctNum = schoolConfig.KissKey + ":" + t1098.EnrollmentId;
                enr.FirstName = t1098.FirstName;
                enr.LastName = t1098.LastName;
                enr.SocSecNum = T1098Bo.SocSecNum(t1098);
                enr.Addr1 = t1098.Address1;
                enr.City = t1098.City;
                enr.State = t1098.State;
                enr.ZipCode = t1098.Zip;

                if (grossPaid < decyrNetCost)
                {
                    enr.Line1Data = grossPaid < 0 ? 0 : Convert.ToInt32(grossPaid);
                }
                else
                {
                    enr.Line1Data = decyrNetCost < 0 ? 0 : Convert.ToInt32(decyrNetCost);
                }

                enr.Line3Data = decAdjustmentsMadeForPriorYear < 0 ? 0 : Convert.ToInt32(decAdjustmentsMadeForPriorYear);
                enr.Line4Data = decScholarShipsorGrants < 0 ? 0 : Convert.ToInt32(decScholarShipsorGrants);
                enr.Line5Data = decAdjustmentsToScholarship < 0 ? 0 : Convert.ToInt32(decAdjustmentsToScholarship);

                var startDate = Convert.ToDateTime(t1098.StartDate);
                if (startDate >= strFirstTaxDeclarationDayNextYear)
                {
                    enr.Line6Data = startDate <= strLastTaxDeclarationDayNextYear ? 1 : 0;
                }
                else
                {
                    enr.Line6Data = 0;
                }

                enr.RfndGRs = Convert.ToInt32(Math.Round(t1098.GrantRefunds));

                enr.RfndLNs = Convert.ToInt32(Math.Round(t1098.LoanRefunds));
                enr.RfndXXs = Convert.ToInt32(Math.Round(t1098.StudentRefunds));
                enr.PaidGRs = Convert.ToInt32(Math.Round(t1098.GrantPayments));
                enr.PaidSPs = Convert.ToInt32(Math.Round(t1098.StudentPayments));
                enr.PaidSHs = Convert.ToInt32(Math.Round(t1098.Scholarships));
                enr.PaidLNs = Convert.ToInt32(Math.Round(t1098.LoanPayments));
                enr.GrossPaid = Convert.ToInt32(Math.Round(t1098.GrossPayments));
                enr.XHrsPerWeek = 0;
                enr.Line8Data = true;
                enr.TotalCost = Convert.ToInt32(Math.Round(t1098.TotalCost));
                enr.PriorYrsPdAmt = Convert.ToInt32(Math.Round(t1098.PriorYearPayments));
                enr.UnpaidCosts = Convert.ToInt32(Math.Round(t1098.TotalCost - t1098.TotalPaid));

                // Check if student has made any payment or there were loan payments
                // If newRow("PaidSPs") = 0 And newRow("PaidLNs") = 0 Then
                // Troy:Check if all payments are from grants or scholarships
                if (enr.Line1Data >= 1)
                {
                    var intTotalPaid = Convert.ToInt32(Math.Round(t1098.TotalPaid));
                    if (intTotalPaid == (enr.PaidSHs + enr.PaidGRs))
                    {
                        // Create a exclusion Table
                        var exc = T1098Bo.ExclusionsDto(schoolConfig, t1098);
                        exc.ExclusionCode = 3; // ??
                        tablelist.ExclusionsList.Add(exc);
                    }
                    else
                    {
                        // Create the enrollment record
                        tablelist.ValidEnrollmentsList.Add(enr);
                        ////enrollmentValidList.Add(t1098.EnrollmentId);
                    }
                }
                else
                {
                    // Create the enrollment record
                    tablelist.ValidEnrollmentsList.Add(enr);
                    ////enrollmentValidList.Add(t1098.EnrollmentId);
                }

                var archive = new ArchiveDto
                                  {
                                      SchoolId = schoolConfig.SchoolId,
                                      SchoolName = schoolConfig.School,
                                      StuEnrollId = t1098.StuEnrollId.ToString(),
                                      ExtractedDate = DateTime.Now,
                                      TaxYear = tablelist.Year
                                  };
                tablelist.ArchiveList.Add(archive);
            }

            return tablelist;
        }

        #endregion
    }
}
