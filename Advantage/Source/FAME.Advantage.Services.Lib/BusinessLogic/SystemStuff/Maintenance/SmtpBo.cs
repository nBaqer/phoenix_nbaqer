﻿using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Services.Lib.Formatters.MultipartDataMediaFormatter.Infrastructure;

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance
{
    public class SmtpBo
    {
         // ReSharper disable NotAccessedField.Local
        private readonly IRepositoryWithTypedID<int> repositoryInt;
        // ReSharper restore NotAccessedField.Local
        private readonly IRepository repository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository">repository guid</param>
        /// <param name="repositoryInt">repository integer</param>
        public SmtpBo(IRepository repository, IRepositoryWithTypedID<int> repositoryInt)
        {
            this.repository = repository;
            this.repositoryInt = repositoryInt;
        }


        /// <summary>
        /// Store in database the logos files content in formData.
        /// </summary>
        /// <param name="formData">binary with the logo information</param>
        public string ProcessSecretFile(FormData formData)
        {
            foreach (FormData.ValueFile valueFile in formData.Files)
            {
                HttpFile file;
                formData.TryGetValue(valueFile.Name, out file);
                var fileContent = System.Text.Encoding.UTF8.GetString(file.Buffer);
                return fileContent;
            }

            return string.Empty;
        }
    }
}
