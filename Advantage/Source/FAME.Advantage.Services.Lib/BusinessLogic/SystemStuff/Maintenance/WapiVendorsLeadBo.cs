﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Lead;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Messages.Common;
using FAME.Advantage.Messages.SystemStuff.Maintenance;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance
{
    public class WapiVendorsLeadBo
    {
        //repository object that represents data store
        private readonly IRepositoryWithTypedID<int> repository;

        #region Constructor
        public WapiVendorsLeadBo(IRepositoryWithTypedID<int> repository)
        {
            this.repository = repository;
        }
        #endregion

        #region Vendors

        /// <summary>
        /// Get the vendors list or a individual vendor. 
        /// </summary>
        /// <param name="filter">
        /// if filter is null all vendor are returned. Else the vendor 
        /// with the filter is returned as a list.
        /// </param>
        /// <returns>
        /// Always return a IEnumerable.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">The filter is incorrect.</exception>
        public IEnumerable<WapiVendorsLeadOutputModel> GetVendorsLead(WapiVendorsLeadOutputInputModel filter)
        {
            if (filter == null || filter.VendorId == 0)
            {
                var list = repository.Query<AdVendors>().Where(x => x.IsDeleted == false).ToList();
                var output = Mapper.Map<IEnumerable<WapiVendorsLeadOutputModel>>(list);
                return output;
            }

            var vendor = repository.Get<AdVendors>(filter.VendorId);
            var vendors = new List<AdVendors> { vendor };
            var outp = Mapper.Map<IEnumerable<WapiVendorsLeadOutputModel>>(vendors);
            return outp;
        }

        public WapiVendorsLeadOutputModel UpdateVendorsLead(WapiVendorsLeadOutputModel data)
        {
            var error = ValidationsVendorUpdate(data);
            if (error != String.Empty)
            {
                throw new HttpRequestValidationException(error);
            }

            var record = repository.Get<AdVendors>(data.ID);
            if (record == null)
            {
                throw new HttpBadRequestResponseException("The Vendor to update does not exist.");
            }
            
            record.UpdateRecord(data);

            repository.Update(record);
            repository.SaveAndFlush(record);
            return data;
        }

        private string ValidationsVendorUpdate(WapiVendorsLeadOutputModel data)
        {
            var result = string.Empty;

            if (data.VendorName == string.Empty)
            {
                result = "The Vendor need a Name <br/>";
            }

            if (data.DateOperationBegin > data.DateOperationEnd)
            {
                result += "The Date begin can not be greater than Date end <br/>";
            }

           return result;
        }

        #endregion

        #region Campaigns
        public IEnumerable<WapiVendorsCampaignLeadOutputModel> GetVendorsCampaigns(WapiVendorsLeadOutputInputModel filter)
        {
            if (filter.CampaignId == 0 & filter.VendorId == 0)
            {
                //Get all Campaign Id
                var list = repository.Query<AdVendorCampaign>().Where(x => x.IsDeleted == 0).ToList();
                var output = Mapper.Map<IEnumerable<WapiVendorsCampaignLeadOutputModel>>(list);
                return output;
            }

            if (filter.VendorId == 0 & filter.CampaignId > 0)
            {
                // Note Get a specific campaign Id. Can return multiple campaign id for different vendor!!!
                var camp = repository.Get<AdVendorCampaign>(filter.CampaignId);
                var vendors = new List<AdVendorCampaign> { camp };
                var outp = Mapper.Map<IEnumerable<WapiVendorsCampaignLeadOutputModel>>(vendors);
                return outp;

            }

            if (filter.VendorId > 0 & filter.CampaignId == 0)
            {
                // Get All Campaign Id for a specific Vendor
                var camp = repository.Query<AdVendorCampaign>().Where(x => x.VendorObj.ID == filter.VendorId && x.IsDeleted == 0).ToList();
                var outp = Mapper.Map<IEnumerable<WapiVendorsCampaignLeadOutputModel>>(camp);
                return outp;

            }

            if (filter.VendorId > 0 & filter.CampaignId > 0)
            {
                // Get specific campaign for a specific Vendor
                var camp = repository.Query<AdVendorCampaign>().Where(x => x.VendorObj.ID == filter.VendorId & x.ID == filter.CampaignId && x.IsDeleted == 0).ToList();
                var outp = Mapper.Map<IEnumerable<WapiVendorsCampaignLeadOutputModel>>(camp);
                return outp;

            }

            throw new HttpBadRequestResponseException("You should not get to here!");
        }

        public WapiVendorsCampaignLeadOutputModel InsertVendorsCampaignLead(WapiVendorsCampaignLeadOutputModel data)
        {
            var error = Validations(data);
            if (error != String.Empty)
            {
                throw new HttpRequestValidationException(error);
            }

            var objPayFor = repository.Query<AdVendorPayFor>().Single(x => x.ID == data.PayFor.ID);
            var objVendor = repository.Query<AdVendors>().Single(x => x.ID == data.VendorId);

            var record = new AdVendorCampaign();
            record.UpdateRecord(data, objPayFor, objVendor);

            repository.SaveAndFlush(record);
            var campaigns = new List<AdVendorCampaign> { record };
            var outp = Mapper.Map<IEnumerable<WapiVendorsCampaignLeadOutputModel>>(campaigns);
            return outp.First();
        }


        public WapiVendorsCampaignLeadOutputModel UpdateVendorsCampaignLead(WapiVendorsCampaignLeadOutputModel data)
        {
            var error = ValidationsUpdate(data);
            if (error != String.Empty)
            {
                throw new HttpRequestValidationException(error);
            }

            var record = repository.Get<AdVendorCampaign>(data.ID);
            if (record == null)
            {
                throw new HttpBadRequestResponseException("The Campaign to update does not exist.");
            }
            var objPayFor = repository.Query<AdVendorPayFor>().Single(x => x.ID == data.PayFor.ID);
            var objVendor = repository.Query<AdVendors>().Single(x => x.ID == data.VendorId);

            record.UpdateRecord(data, objPayFor, objVendor);

            repository.Update(record);
            repository.SaveAndFlush(record);
            return data;
        }

        /// <summary>
        /// Delete the operation
        /// </summary>
        /// <param name="id"></param>
        public WapiVendorsCampaignLeadOutputModel DeleteCampaignLead(int id)
        {
            var record = repository.Get<AdVendorCampaign>(id);
            if (record == null)
            {
                throw new HttpBadRequestResponseException("The Campaign to delete does not exist.");
            }

            record.Delete();
            repository.SaveAndFlush(record);
            var campaigns = new List<AdVendorCampaign> { record };
            var outp = Mapper.Map<IEnumerable<WapiVendorsCampaignLeadOutputModel>>(campaigns);
            return outp.First();

        }


        private string Validations(WapiVendorsCampaignLeadOutputModel data)
        {
            var result = ValidationCommon(data);

            // Check campaign duplicate for the same vendor.
            var isany = repository.Query<AdVendorCampaign>().Count(x => x.CampaignCode == data.CampaignCode && x.VendorObj.ID == data.VendorId);
            if (isany > 0)
            {
                result += string.Format("The Campaign Code {0} is in use. You can not duplicate it! {1}", data.CampaignCode, "<br/>");
            }

            return result;
        }

        private string ValidationsUpdate(WapiVendorsCampaignLeadOutputModel data)
        {
            

            var result = ValidationCommon(data);

            // Check campaign duplicate for the same vendor.
            var isany = repository.Query<AdVendorCampaign>().Where(x => x.ID != data.ID).Count(x => x.CampaignCode == data.CampaignCode && x.VendorObj.ID == data.VendorId);
            if (isany > 0)
            {
                result += string.Format("The Campaign Code {0} is in use. You can not duplicate it! {1}", data.CampaignCode, "<br/>");
            }

            return result;
        }

        private string ValidationCommon(WapiVendorsCampaignLeadOutputModel data)
        {
            var result = string.Empty;

            if (data.CampaignCode == string.Empty)
            {
                result = "The Campaign need a Name <br/>";
            }

            if (data.AccountId == string.Empty)
            {
                result += "The Campaign need a Account <br/>";
            }

            if (data.VendorId == 0)
            {
                result += "The Campaign need be associate with a Vendor <br/>";
            }

            if (data.DateCampaignBegin > data.DateCampaignEnd)
            {
                result += "The Date begin can not be greater than Date end <br/>";
            }

            return result;
        }

        #endregion

        #region Pay for schema
        /// <summary>
        /// Return a list of Vendor pay For schema.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DropDownIntegerOutputModel> GetPayForLists()
        {
            var list = repository.Query<AdVendorPayFor>().ToList();
            var outp = Mapper.Map<IEnumerable<DropDownIntegerOutputModel>>(list);
            return outp.OrderBy(x => x.Description);
        }
        #endregion

     
    }
}
