﻿using System;
using System.IO;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Infrastructure.Entities;
using Google.Apis.Json;
using Google.Apis.Util.Store;


namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance
{
    public class DbGoogleDataStore : IDataStore
    {

        private readonly string folderPath;
        private string token;

        /// <summary>
        /// Gets the full folder path.
        /// </summary>
        public string Token
        {
            get
            {
                return this.token;
            }
        }

        public static IDataStore Factory(string token)
        {
            IDataStore store = new DbGoogleDataStore(token);
            return store;
        }

        public DbGoogleDataStore(string token)
        {
            // Create the directory
            this.token = token;

        }



        #region Implementation of IDataStore

        /// <summary>
        /// Asynchronously stores the given value for the given key (replacing any existing value).
        /// </summary>
        /// <typeparam name="T">The type to store in the data store.</typeparam><param name="key">The key.</param><param name="value">The value to store.</param>
        public Task StoreAsync<T>(string key, T value)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentException("Key MUST have a value");
            string contents = NewtonsoftJsonSerializer.Instance.Serialize((object)value);
            token = contents;
            //ConfigurationAppSettings.StoreSettingString()
            File.WriteAllText(Path.Combine(this.folderPath, FileDataStore.GenerateStoredKey(key, typeof(T))), contents);
            return TaskEx.Delay(0);
        }

        /// <summary>
        /// Asynchronously deletes the given key. The type is provided here as well because the "real" saved key should
        ///             contain type information as well, so the data store will be able to store the same key for different types.
        /// </summary>
        /// <typeparam name="T">The type to delete from the data store.</typeparam><param name="key">The key to delete.</param>
        public Task DeleteAsync<T>(string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentException("Key MUST have a value");
            token = string.Empty;
            return TaskEx.Delay(0);
        }

        /// <summary>
        /// Asynchronously returns the stored value for the given key or <c>null</c> if not found.
        /// </summary>
        /// <typeparam name="T">The type to retrieve from the data store.</typeparam><param name="key">The key to retrieve its value.</param>
        /// <returns>
        /// The stored object.
        /// </returns>
        public Task<T> GetAsync<T>(string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentException("Key MUST have a value");
            TaskCompletionSource<T> completionSource = new TaskCompletionSource<T>();
            if (!string.IsNullOrWhiteSpace(token))
            {
                try
                {
                    completionSource.SetResult(NewtonsoftJsonSerializer.Instance.Deserialize<T>(token));
                }
                catch (Exception ex)
                {
                    completionSource.SetException(ex);
                }
            }
            else
                completionSource.SetResult(default(T));
            return completionSource.Task;
        }

        /// <summary>
        /// Asynchronously clears all values in the data store.
        /// </summary>
        public Task ClearAsync()
        {
            token = string.Empty;
            return TaskEx.Delay(0);
        }

        #endregion

        ///// <summary>
        ///// Creates a unique stored key based on the key and the class type.
        ///// </summary>
        ///// <param name="key">The object key.</param><param name="t">The type to store or retrieve.</param>
        //public static string GenerateStoredKey(string key, Type t)
        //{
        //    return string.Format("{0}-{1}", (object)t.FullName, (object)key);
        //}
    }
}
