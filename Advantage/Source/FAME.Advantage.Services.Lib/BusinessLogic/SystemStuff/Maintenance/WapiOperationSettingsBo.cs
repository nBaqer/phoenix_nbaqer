﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiOperationSettingsBo.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the WapiOperationSettingsBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Security.AccessControl;
    using System.Security.Cryptography;

    using AutoMapper;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Messages.SystemStuff.Maintenance;
    using FAME.Advantage.Messages.SystemStuff.Wapi;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    /// The WAPI operation settings business object.
    /// </summary>
    public class WapiOperationSettingsBo
    {
        /// <summary>
        /// The repository object that represents data store
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repository;

        public WapiOperationSettingsBo(IRepositoryWithTypedID<int> repository)
        {
            this.repository = repository;
        }

        #region  Operation Settings CRUD

        /// <summary>
        /// Get the operation settings for windows service
        /// </summary>
        /// <param name="filter">
        /// Filter Id and OperationMode can not be used together.
        /// if you use together the Id filter take precedence over the 
        /// Operation Mode and Operation Mode is ignored.
        /// Id = 0 ignored // OperationMode = string.empty ignored 
        /// OnlyActives can be used in combination with the others
        /// 1 get only active operation 0: is ignored.
        /// </param>
        /// <returns></returns>
        public IEnumerable<WapiOperationSettingOutputModel> GetOperationSettings(WapiOperationSettingsInputModel filter)
        {
            IList<SyWapiSettings> listresult = new List<SyWapiSettings>();
            if (filter.IdOperation > 0)
            {
                var item = this.repository.Load<SyWapiSettings>(filter.IdOperation);
                listresult.Add(item);
            }
            else
            {
                if (string.IsNullOrEmpty(filter.OperationMode) == false)
                {
                    // Get the list of the same type of operation
                    listresult = this.repository.Query<SyWapiSettings>().Where(x => x.ExternalOperationModeObj.Code == filter.OperationMode).ToList();
                }
                else
                {
                    // get all wapi settings
                    listresult = this.repository.Query<SyWapiSettings>().ToList();
                }
            }

            if (filter.OnlyActives == 1)
            {
                listresult = listresult.Where(x => x.IsActive).ToList();
            }

            // return module output object
            var result = Mapper.Map<IEnumerable<WapiOperationSettingOutputModel>>(listresult.OrderBy(x => x.ID));
            return result;
        }

        public void UpdateOperationSetting(WapiOperationSettingOutputModel data)
        {
            var record = this.repository.Get<SyWapiSettings>(data.ID);
            if (record == null)
            {
                throw new HttpBadRequestResponseException("The operation to update does not exist.");
            }

            var extCompany = this.repository.Query<SyWapiExternalCompanies>().Single(x => x.Code == data.CodeExtCompany);
            var extMode = this.repository.Query<SyWapiExternalOperationMode>().Single(x => x.Code == data.CodeExtOperationMode);
            var allowServ = this.repository.Query<SyWapiAllowedServices>().Single(x => x.Code == data.CodeWapiServ);
            var sAllowServ = this.repository.Query<SyWapiAllowedServices>().SingleOrDefault(x => x.Code == data.SecondCodeWapiServ);

            record.UpdateRecord(data, extCompany, extMode, allowServ, sAllowServ);

            this.repository.Update(record);
        }

        /// <summary>
        /// Delete the operation
        /// </summary>
        /// <param name="id"></param>
        public void DeleteOperationSetting(int id)
        {
            var entity = this.repository.Load<SyWapiSettings>(id);
            this.repository.Delete(entity);
        }

        /// <summary>
        /// Insert a new operation record
        /// </summary>
        /// <param name="data"></param>
        public string InsertOperationSetting(WapiOperationSettingOutputModel data)
        {
            var error = this.Validations(data);
            if (error != string.Empty)
            {
                return error;
            }

            var extCompany = this.repository.Query<SyWapiExternalCompanies>().Single(x => x.Code == data.CodeExtCompany);
            var extMode = this.repository.Query<SyWapiExternalOperationMode>().Single(x => x.Code == data.CodeExtOperationMode);
            var allowServ = this.repository.Query<SyWapiAllowedServices>().Single(x => x.Code == data.CodeWapiServ);
            var sAllowServ = this.repository.Query<SyWapiAllowedServices>().SingleOrDefault(x => x.Code == data.SecondCodeWapiServ);

            var record = new SyWapiSettings();
            record.UpdateRecord(data, extCompany, extMode, allowServ, sAllowServ);

            this.repository.SaveAndFlush(record);
            return error;
        }

        #endregion

        #region Get catalog information COMPANY, MODE, WSERVICES

        /// <summary>
        /// Get the catalog determinate by the filter. See parameters to see valid filter values
        /// </summary>
        /// <param name="filter">
        /// COMPANY, MODE, WSERVICES,OPERATION, OPERATION_ACTIVES 
        /// COMPANIES_ASSOCIATED_WITH_SERVICE:  return a list of companies code and the relation wit the allowed service (parentId).
        /// SERVICES_ASSOCIATED_WITH_COMPANY: return a list of services code associated with the company name (parentId)
        /// </param>
        /// <returns></returns>
        public IEnumerable<WapiCatalogsOutputModel> GetWapiCatalogs(WapiCatalogInputModel filter)
        {
            IEnumerable<WapiCatalogsOutputModel> list = new List<WapiCatalogsOutputModel>();

            switch (filter.CatalogOperation.ToUpper())
            {
                case "COMPANIES_ASSOCIATED_WITH_SERVICE":
                    {
                        // and the web method return the list of company codes and the association with 
                        // the given service.
                        var service = this.repository.Query<SyWapiAllowedServices>().SingleOrDefault(x => x.ID == filter.IdParent);
                        var companies = this.repository.Query<SyWapiExternalCompanies>();
                        var output = new List<WapiCatalogsOutputModel>();
                        foreach (SyWapiExternalCompanies company in companies)
                        {
                            var ou = new WapiCatalogsOutputModel
                            {
                                Code = company.Code,
                                Description = company.Description,
                                Id = company.ID,
                                IsActive = company.IsActive
                            };
                            if (service != null && service.WapiExternalCompaniesList.Any(x => x.ID == ou.Id))
                            {
                                ou.Authorized = true;
                            }
                            else
                            {
                                ou.Authorized = false;
                            }

                            output.Add(ou);
                        }

                        list = output;
                        break;
                    }

                case "SERVICES_ASSOCIATED_WITH_COMPANY":
                    {
                        // and the web method return the list of company codes and the association with 
                        // the given service.
                        var company = this.repository.Query<SyWapiExternalCompanies>().SingleOrDefault(x => x.ID == filter.IdParent);
                        var services = this.repository.Query<SyWapiAllowedServices>();
                        var output = new List<WapiCatalogsOutputModel>();
                        foreach (SyWapiAllowedServices serv in services)
                        {
                            var ou = new WapiCatalogsOutputModel
                            {
                                Code = serv.Code,
                                Description = serv.Description,
                                Id = serv.ID,
                                IsActive = serv.IsActive,
                                Url = serv.Url
                            };
                            if (company != null && company.WapiAllowedServicesList.Any(x => x.ID == ou.Id))
                            {
                                ou.Authorized = true;
                            }
                            else
                            {
                                ou.Authorized = false;
                            }

                            output.Add(ou);
                        }

                        list = output;
                        break;
                    }



                case "COMPANY":
                    {
                        var result = this.repository.Query<SyWapiExternalCompanies>();
                        list = Mapper.Map<IEnumerable<WapiCatalogsOutputModel>>(result.ToList().OrderBy(x => x.Code));

                        break;
                    }

                case "MODE":
                    {
                        var result = this.repository.Query<SyWapiExternalOperationMode>();
                        list = Mapper.Map<IEnumerable<WapiCatalogsOutputModel>>(result.ToList().OrderBy(x => x.Code));
                        break;
                    }

                case "WSERVICES":
                    {
                        var result = this.repository.Query<SyWapiAllowedServices>();
                        if (result != null)
                        {
                            list = Mapper.Map<IEnumerable<WapiCatalogsOutputModel>>(result.ToList().OrderBy(x => x.Code));
                        }

                        break;
                    }

                case "OPERATION":
                    {
                        var result = this.repository.Query<SyWapiSettings>();
                        list = Mapper.Map<IEnumerable<WapiCatalogsOutputModel>>(result.ToList().OrderBy(x => x.CodeOperation));
                        break;
                    }

                case "OPERATION_ACTIVES":
                    {
                        var result = this.repository.Query<SyWapiSettings>().Where(x => x.IsActive);
                        if (result != null)
                        {
                            
                            list = Mapper.Map<IEnumerable<WapiCatalogsOutputModel>>(result.ToList().OrderBy(x => x.CodeOperation));
                        }
                        break;
                    }

                default:
                    {
                        throw new Exception("Invalid input filter");
                    }
            }

            return list;
        }

        /// <summary>
        /// Projected to implement different save information
        /// </summary>
        /// <param name="filter">
        ///     CatalogOperation:  COMPANY_ALLOWSERVICE_RELATION: 
        ///     Save changes only in the relation between Allow services table and External Companies
        ///     IdParent = Allowed service Id
        ///     Id = Company to be related
        ///     Authorized = true or false if the relation exists or not.
        ///     INSERT_COMPANIES: insert the list of companies  new
        /// </param>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<WapiCatalogsOutputModel> PostWapiCatalogs(WapiCatalogInputModel filter, string key)
        {
            // Determine what type of operation
            switch (filter.CatalogOperation.ToUpper())
            {
                case "COMPANY_ALLOWSERVICE_RELATION":
                    {
                        var service = this.repository.Load<SyWapiAllowedServices>(filter.IdParent);
                        foreach (WapiCatalogsOutputModel model in filter.Changes)
                        {
                            var company = this.repository.Load<SyWapiExternalCompanies>(model.Id);
                            if (model.Authorized)
                            {
                                if (service.WapiExternalCompaniesList.Any(x => x.ID == company.ID) == false)
                                {
                                    service.WapiExternalCompaniesList.Add(company);
                                }
                            }
                            else
                            {
                                service.WapiExternalCompaniesList.Remove(company);
                            }
                        }

                        this.repository.UpdateAndFlush(service);

                        // Return the updated records
                        // filter.CatalogOperation = "COMPANIES_ASSOCIATED_WITH_SERVICE";
                        // var list = GetWapiCatalogs(filter);
                        return filter.Changes;
                    }

                case "UPDATE_ALLOWED_SERVICES":
                    {
                        // Validation loop. If not validate throw exception
                        this.ValidateAllowedOperation(filter);

                        foreach (WapiCatalogsOutputModel model in filter.Changes)
                        {
                            var ser = this.repository.Get<SyWapiAllowedServices>(model.Id);
                            ser.Update(model);
                            this.repository.UpdateAndFlush(ser);
                        }

                        return filter.Changes;
                    }

                case "INSERT_ALLOWED_SERVICES":
                    {
                        IList<WapiCatalogsOutputModel> outputModels = new List<WapiCatalogsOutputModel>();

                        // Validation loop. If not validate throw exception
                        this.ValidateAllowedOperation(filter);

                        foreach (WapiCatalogsOutputModel model in filter.Changes)
                        {
                            var dom = new SyWapiAllowedServices();
                            dom.Update(model);
                            this.repository.SaveAndFlush(dom);
                            model.Id = dom.ID;
                            outputModels.Add(model);
                        }

                        return outputModels;
                    }

                case "DELETE_ALLOWED_SERVICES":
                    {
                        IList<WapiCatalogsOutputModel> outputModels = new List<WapiCatalogsOutputModel>();
                        foreach (WapiCatalogsOutputModel model in filter.Changes)
                        {
                            // Se if exists relation with operation setting, in this case you can not delete
                            var dom = this.repository.Get<SyWapiAllowedServices>(model.Id);
                            if (dom.WapiSettingsList.Count > 0 || dom.SecondWapingSettingList.Count > 0)
                            {
                                // you can not delete.
                                throw new ApplicationException("You can not delete a Allowed Service that owner operation settings. First delete the operation or change the owner.");
                            }

                            this.repository.Delete(dom);
                            outputModels.Add(model);
                        }

                        return outputModels;
                    }

                case "ALLOWSERVICE_COMPANY_RELATION":
                    {
                        var comp = this.repository.Load<SyWapiExternalCompanies>(filter.IdParent);
                        foreach (WapiCatalogsOutputModel model in filter.Changes)
                        {
                            var serv = this.repository.Load<SyWapiAllowedServices>(model.Id);
                            if (model.Authorized)
                            {
                                if (comp.WapiAllowedServicesList.Any(x => x.ID == serv.ID) == false)
                                {
                                    comp.WapiAllowedServicesList.Add(serv);
                                }
                            }
                            else
                            {
                                comp.WapiAllowedServicesList.Remove(serv);
                            }
                        }

                        this.repository.UpdateAndFlush(comp);
                        return filter.Changes;
                    }

                case "UPDATE_COMPANIES":
                    {
                        var cbo = new WapiCompanyNameBo();

                        foreach (WapiCatalogsOutputModel model in filter.Changes)
                        {
                            var com = this.repository.Get<SyWapiExternalCompanies>(model.Id);
                            if (com.Code != model.Code)
                            {
                                var tenantId = this.GentTenantId(key);
                                if (cbo.ValidateExistCompanyName(model.Code, this.repository, tenantId) == false)
                                {
                                    // The name already exists, return exception
                                    throw new Exception("The Company Code Name already is in use. You can not duplicate it");
                                }


                                // The update include the name, then we need to modify also TenantDb.
                                // Use special update....
                                cbo.UpdateExternalCompanyName(model, com, this.repository, tenantId);

                            }
                            else
                            {
                                com.Update(model);
                                this.repository.UpdateAndFlush(com);
                            }

                        }

                        return filter.Changes;
                    }

                case "INSERT_COMPANIES":
                    {
                        var cbo = new WapiCompanyNameBo();
                        IList<WapiCatalogsOutputModel> outputModels = new List<WapiCatalogsOutputModel>();
                        foreach (WapiCatalogsOutputModel model in filter.Changes)
                        {
                            var tenantId = this.GentTenantId(key);
                            if (cbo.ValidateExistCompanyName(model.Code, this.repository, tenantId) == false)
                            {
                                // The name already exists, return exception
                                throw new Exception("The Company Code Name already is in use. You can not duplicate it");
                            }

                            var inserted = cbo.InsertNewCompanyExternalName(model, this.repository, tenantId);

                            outputModels.Add(inserted);
                        }

                        return outputModels;
                    }

                case "SYNCHRO_COMPANIES":
                    {
                        var cbo = new WapiCompanyNameBo();
                        var tenantId = this.GentTenantId(key);
                        IList<WapiCatalogsOutputModel> outputModels = new List<WapiCatalogsOutputModel>();
                        filter.CatalogOperation = "COMPANY";
                        filter.Changes = this.GetWapiCatalogs(filter).ToList();
                        foreach (WapiCatalogsOutputModel model in filter.Changes)
                        {
                            var output = cbo.SynchroCompaniesName(model, this.repository, tenantId);
                            outputModels.Add(output);
                        }

                        return outputModels;
                    }

                case "DELETE_COMPANIES":
                    {
                        var cbo = new WapiCompanyNameBo();
                        var tenantId = this.GentTenantId(key);
                        IList<WapiCatalogsOutputModel> outputModels = new List<WapiCatalogsOutputModel>();
                        foreach (WapiCatalogsOutputModel model in filter.Changes)
                        {
                            var output = cbo.DeleteCompaniesObject(model, this.repository, tenantId);
                            outputModels.Add(output);
                        }

                        return outputModels;


                    }

                default:
                    {
                        throw new HttpBadUsageOfControllerFunctionException("No Implemented");
                    }
            }

        }

        #endregion

        #region Security Service Key

        public string CreateSecurityKey(string containerName)
        {
            // Test first if the key exists
            string key = this.GetSecurityKey(containerName);
            if (key.ToUpper().Contains("ERROR"))
            {
                // Rule to access the cryptographic key
                CryptoKeyAccessRule rule = new CryptoKeyAccessRule("everyone", CryptoKeyRights.FullControl, AccessControlType.Allow);
                

                // The key does not exists create it
                var cp = new CspParameters
                {
                    KeyContainerName = containerName,
                    Flags = CspProviderFlags.UseMachineKeyStore,
                    CryptoKeySecurity = new CryptoKeySecurity()
                };
                cp.CryptoKeySecurity.SetAccessRule(rule);
                var rsa = new RSACryptoServiceProvider(cp);
                return rsa.ToXmlString(false); // Only show the public key
            }

            // If exists, warning 
            return "Error: The key exists, please delete it first";

        }

        public string DeleteSecurityKey(string containerName)
        {
            // Create the CspParameters object and set the key container 
            // name used to store the RSA key pair.
            var cp = new CspParameters
            {
                KeyContainerName = containerName,
                Flags = CspProviderFlags.UseMachineKeyStore
            };

            // Create a new instance of RSACryptoServiceProvider that accesses
            // the key container.
            var rsa = new RSACryptoServiceProvider(cp) { PersistKeyInCsp = false };

            // Delete the key entry in the container.
            // Call Clear to release resources and delete the key from the container.
            rsa.Clear();
            return "Key Deleted";
        }

        /// <summary>
        /// Get the key if exists, if not return a Error message.
        /// </summary>
        /// <returns>
        /// the public key OR
        /// Error: Security key does not exists
        /// </returns>
        public string GetSecurityKey(string containerName)
        {
            // Create the CspParameters object and set the key container 
            // name used to store the RSA key pair.
            var cp = new CspParameters
            {
                KeyContainerName = containerName,
                Flags =
                                 CspProviderFlags.UseExistingKey | CspProviderFlags.UseMachineKeyStore

                // If key does not exists, does not create it!!!
            };

            // Create a new instance of RSACryptoServiceProvider that accesses
            // the key container MyKeyContainerName.
            try
            {
                var rsa = new RSACryptoServiceProvider(cp);
                return rsa.ToXmlString(false);
            }
            catch (Exception)
            {
                return "Error: Security key does not exists";
            }

        }

        #endregion

        #region Flags On Demand

        /// <summary>
        /// Return the list of flags for all operation in system.
        /// </summary>
        /// <returns>the list of status of flags</returns>
        public IEnumerable<WapiOnDemandFlagsOutputModel> GetOnDemandFlags(int id)
        {
            // get module menu items
            var items = (id == -1)
                ? this.repository.Query<SyWapiSettings>()
                : this.repository.Query<SyWapiSettings>().Where(x => x.ID == id);
            var flags = from x in items
                        select new WapiOnDemandFlagsOutputModel { OnDemandOperation = x.FlagOnDemandOperation, Id = x.ID };
            return flags.ToList();

        }

        public WapiOnDemandFlagsOutputModel GetSingleOnDemandFlag(int id)
        {
            // get module menu items
            if (id < 1)
            {
                throw new Exception("Filter value not valid");
            }

            var op = this.repository.Get<SyWapiSettings>(id);
            var output = new WapiOnDemandFlagsOutputModel { OnDemandOperation = op.FlagOnDemandOperation, Id = op.ID };
            if (output.OnDemandOperation == 1)
            {
                output.Result = "Pending";
                output.Error = false;
                return output;
            }

            var log = this.repository.Query<SyWapiOperationLogger>()
                             .Where(x => x.CompanyCode == op.ExternalCompanyObj.Code && x.ServiceCode == op.CodeOperation)
                             .OrderByDescending(x => x.ID)
                             .FirstOrDefault();
            if (log != null)
            {
                output.Result = log.Comment;
                output.Error = log.IsError == 1;
            }

            return output;
        }

        /// <summary>
        /// Update the On-demand Flag with filter.Value
        /// over the operation with the Id filter.Id
        /// </summary>
        /// <param name="filter">
        /// Id the operation Id to be updated
        /// Value: the value of the update.
        /// </param>
        public void SetOnDemandFlags(WapiOnDemandFlagsInputModel filter)
        {
            var record = this.repository.Get<SyWapiSettings>(filter.Id);
            if (record == null)
            {
                throw new Exception("The operation to update does not exist.");
            }

            record.UpdateOnDemandFlag(filter.Value);
            this.repository.Update(record);
        }

        #endregion

        #region Executed Time

        /// <summary>
        /// Set the value DateTimeLastExecuted in Wapi config.
        /// </summary>
        /// <param name="filter"></param>
        public void SetLastExecutedTime(WapiSetDateTimeInputModel filter)
        {
            var record = this.repository.Get<SyWapiSettings>(filter.Id);
            if (record == null)
            {
                throw new Exception("The operation to update does not exist.");
            }

            record.UpdateLastExecutedDate(DateTime.Parse(filter.Value));
            this.repository.Update(record);
        }

        /// <summary>
        /// Return all operation last time executed if id = -1
        /// if not return the specific operation in Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<WapiGetDateTimeOutputModel> GetLastExecutedTime(int id)
        {
            // get module menu items
            var items = (id == -1) ? this.repository.Query<SyWapiSettings>() : this.repository.Query<SyWapiSettings>().Where(x => x.ID == id);
            var dt = from x in items
                     select new WapiGetDateTimeOutputModel { Value = x.DateLastExecution, Id = x.ID };
            return dt.ToList();
        }

        #endregion

        #region Logger Operations

        /// <summary>
        /// Get the records in the logger operation table
        /// if filter apply they are applied.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<WapiLoggerOutputModel> GetLoggerRecords(WapiLoggerInputModel filter)
        {
            var dateIni = new DateTime(filter.BeginDate.Year, filter.BeginDate.Month, filter.BeginDate.Day, 0, 0, 0);
            var dateEnd = new DateTime(filter.EndDate.Year, filter.EndDate.Month, filter.EndDate.Day, 23, 59, 59);

            // Apply first filter
            IList<SyWapiOperationLogger> list;
            if (filter.WapiOperationCode.ToUpper() == "ALL")
            {
                list =
                    this.repository.Query<SyWapiOperationLogger>()
                        .Where(x => x.DateExecution >= dateIni && x.DateExecution <= dateEnd)
                        .Take(500)
                        .ToList();
            }
            else
            {
                list =
                    this.repository.Query<SyWapiOperationLogger>()
                        .Where(
                            x =>
                                x.DateExecution >= dateIni && x.DateExecution <= dateEnd
                                && x.ServiceCode == filter.WapiOperationCode)
                        .Take(500)
                        .ToList();
            }

            switch (filter.LogType.ToUpperInvariant())
            {
                case "SUCCESS":
                    {
                        list = list.Where(x => x.IsError == 0).ToList();

                        // ltemp = ltemp.Where(x => x.IsError == 1).ToList();
                        break;
                    }

                case "ERROR":
                    {
                        list = list.Where(x => x.IsError == 1).ToList();

                        // ltemp = ltemp.Where(x => x.IsError == 0).ToList();
                        break;
                    }
            }

            var listresult = list.OrderByDescending(x => x.ID).ToList();
            var res = Mapper.Map<IEnumerable<WapiLoggerOutputModel>>(listresult);
            return res;
        }

        public void PostDeleteLoggerRecords()
        {
            var sess = this.repository.Session;

            var state = sess.CreateSQLQuery("TRUNCATE TABLE syWapiOperationLogger");
            state.ExecuteUpdate();

            // For<IStatelessSession>().Use(c => c.GetInstance <ISessionFactory>().OpenStatelessSession());
        }

        /// <summary>
        /// Insert a new Record in the logger operation table.
        /// </summary>
        /// <param name="logRecord"></param>
        public void PostLoggerRecord(WapiLoggerOutputModel logRecord)
        {
            var log = new SyWapiOperationLogger();
            log.ImportObject(logRecord);
            this.repository.SaveAndFlush(log);
        }

        #endregion

        #region Private helper methods

        private void ValidateAllowedOperation(WapiCatalogInputModel filter)
        {
            // Determine if it a insertion or updat operation
            if (filter.CatalogOperation == "UPDATE_ALLOWED_SERVICES")
            {
                foreach (WapiCatalogsOutputModel model in filter.Changes)
                {
                    if (string.IsNullOrEmpty(model.Code))
                    {
                        throw new ApplicationException(string.Format("Allowed operation need a name, can not be a empty string"));
                    }

                    // Get the actual name
                    var actual = this.repository.Get<SyWapiAllowedServices>(model.Id);
                    if (actual.Code != model.Code)
                    {
                        WapiCatalogsOutputModel model1 = model;
                        var count = this.repository.Query<SyWapiAllowedServices>().Where(x => x.Code == model1.Code);
                        if (count.Count() > 1)
                        {
                            // The name already exists, return exception
                            throw new ApplicationException(string.Format("The Allowed Operation Name {0} already is in use. You can not duplicate it", model.Code));
                        }
                    }
                }
            }

            if (filter.CatalogOperation == "INSERT_ALLOWED_SERVICES")
            {
                foreach (WapiCatalogsOutputModel model in filter.Changes)
                {
                    if (string.IsNullOrEmpty(model.Code))
                    {
                        throw new ApplicationException(string.Format("Allowed operation need a name, can not be a empty string"));
                    }

                    // Get the actual name
                    WapiCatalogsOutputModel model1 = model;
                    var count = this.repository.Query<SyWapiAllowedServices>().Where(x => x.Code == model1.Code);
                    if (count.Count() > 1)
                    {
                        // The name already exists, return exception
                        throw new ApplicationException(string.Format("The Allowed Operation Name {0} already is in use. You can not duplicate it", model.Code));
                    }
                }
            }
        }

        private string Validations(WapiOperationSettingOutputModel data)
        {
            var result = string.Empty;

            if (data.CodeOperation == string.Empty)
            {
                result = "The operation need a name - ";
            }

            var isany = this.repository.Query<SyWapiSettings>().Count(x => x.CodeOperation == data.CodeOperation);
            if (isany > 0)
            {
                result += string.Format("The code {0} is in use. You can not duplicate it! -- ", data.CodeOperation);
            }

            return result;
        }

        private int GentTenantId(string key)
        {
            const string SQL = "SELECT TOP 1 TenantId AS conn FROM [ApiAuthenticationKey] WHERE [Key] = @key";

            var tenconn = ConfigurationManager.ConnectionStrings["TenantAuthDBConnection"].ToString();

            var conn = new SqlConnection(tenconn);
            var command = new SqlCommand(SQL, conn);
            command.Parameters.AddWithValue("@key", key);
            conn.Open();
            try
            {
                var result = (int)command.ExecuteScalar();
                return result;
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion
    }
}
