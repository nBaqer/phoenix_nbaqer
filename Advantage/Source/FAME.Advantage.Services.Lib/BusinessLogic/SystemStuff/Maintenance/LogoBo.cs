﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Messages.SystemStuff.Maintenance;
using FAME.Advantage.Services.Lib.Formatters.MultipartDataMediaFormatter.Infrastructure;

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance
{
    public class LogoBo
    {

        // repository object that represents data store
        private readonly IRepositoryWithTypedID<int> repository;

        public LogoBo(IRepositoryWithTypedID<int> repository)
        {
            this.repository = repository;

        }

        public IEnumerable<SchoolLogoOutputModel> GetAllLogos()
        {
            // get module menu items
            var logoItems = this.repository.Query<SySchoolLogo>();

            // return module output object
            var result = Mapper.Map<IEnumerable<SchoolLogoOutputModel>>(logoItems.ToArray().OrderBy(x => x.ID));
            return result;
        }

        public IEnumerable<SchoolLogoOutputModel> GetLogosByImageCode(SchoolLogoInputModel filter)
        {
            // get module menu items
            var logoItems = this.repository.Query<SySchoolLogo>().Where(x => x.ImageCode == filter.ImageCode);

            // return module output object
            var result = Mapper.Map<IEnumerable<SchoolLogoOutputModel>>(logoItems.ToArray());
            return result;
        }

        public IEnumerable<SchoolLogoOutputModel> GetLogosByLogoId(SchoolLogoInputModel filter)
        {
            // get module menu items
            var logoItems = this.repository.Query<SySchoolLogo>().Where(x => x.ID == filter.ImageId);

            // return module output object
            var result = Mapper.Map<IEnumerable<SchoolLogoOutputModel>>(logoItems.ToArray());
            return result;
        }

        /// <summary>
        /// Store in database the logos files content in formData.
        /// </summary>
        /// <param name="formData">binary with the logo information</param>
        public void StoreLogoFile(FormData formData)
        {
            foreach (FormData.ValueFile valueFile in formData.Files)
            {
                HttpFile file;
                formData.TryGetValue(valueFile.Name, out file);
                var logo = new SySchoolLogo(
                               file.Buffer,
                               file.Buffer.Length,
                               file.MediaType,
                               false,
                               string.Empty,
                               string.Empty,
                               file.FileName);
                var maxLogoId = this.repository.Query<SySchoolLogo>().Count();
                if (maxLogoId != 0)
                {
                    maxLogoId = this.repository.Query<SySchoolLogo>().Max(x => x.ID);
                }

                logo.ID = maxLogoId + 1;
                this.repository.Save(logo);
            }
        }
    }
}
