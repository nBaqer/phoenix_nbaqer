﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiWindowsServiceBo.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Business Object for windows service operation.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Configuration;

    using FAME.Advantage.Messages.SystemStuff.Maintenance;

    /// <summary>
    /// Business Object for windows service operation.
    /// </summary>
    public class WapiWindowsServiceBo
    {
        /// <summary>
        /// Check if the service exists
        /// </summary>
        /// <param name="serviceName">Service Name</param>
        /// <returns>true if exists</returns>
        public bool CheckIfWindowsServiceExists(string serviceName)
        {
            ServiceController ctl = ServiceController.GetServices().FirstOrDefault(s => s.ServiceName == serviceName);
            if (ctl == null)
            {
                Console.WriteLine("Not installed");
                return false;
            }

            Console.WriteLine(ctl.Status);
            return true;
        }

        /// <summary>
        /// Return the status of the given windows service.
        /// </summary>
        /// <param name="serviceName">
        /// The name of the windows service
        /// </param>
        /// <param name="sc">
        /// The Service Controller.
        /// </param>
        /// <returns>
        /// Possible Values:
        /// Running, Stopped, Paused, Stopping, 
        /// Starting, Status Changing, Uninstalled
        /// </returns>
        public string GetStatusOfWindowsService(string serviceName, ServiceController sc)
        {
            if (this.CheckIfWindowsServiceExists(serviceName))
            {
                ////var sc = new ServiceController(serviceName);

                switch (sc.Status)
                {
                    case ServiceControllerStatus.Running:
                        return "Running";
                    case ServiceControllerStatus.Stopped:
                        return "Stopped";
                    case ServiceControllerStatus.Paused:
                        return "Paused";
                    case ServiceControllerStatus.StopPending:
                        return "Stopping";
                    case ServiceControllerStatus.StartPending:
                        return "Starting";
                    default:
                        return "Status Changing";
                }
            }

            return "Uninstalled";
        }

        /// <summary>
        /// Execute the different commands to manage the windows Service
        /// </summary>
        /// <param name="filter">
        /// ServiceName = name of service
        /// Command = initialize, start, stop
        /// </param>
        /// <exception cref="Exception">
        /// Exception("Command not allowed") if the command is not recognize
        /// Exception("Service Not Installed") the service is not installed.
        /// </exception>
        public void ExecuteWindowsServiceCommand(WapiWindowsServiceInputModel filter)
        {
            // Check if the windows service exists WapiWindowsService
            // Get impersonator account
            var username = ConfigurationManager.AppSettings["WapiServiceUsername"];
            var password = ConfigurationManager.AppSettings["WapiServicePassword"];

            var domain = ConfigurationManager.AppSettings["WapiServiceDomainName"];

            if (string.IsNullOrEmpty(domain))
            {
                domain = Environment.MachineName;
            }
            
            var context = new WrapperImpersonationContext(domain, username, password);
            context.Enter();


            var sc = new ServiceController("Advantage Wapi Service", System.Environment.MachineName);
            try
            {
                var status = this.GetStatusOfWindowsService(filter.ServiceName, sc);
                if (status == "Uninstalled")
                {
                    sc.Dispose();
                    throw new Exception("Service Not Installed");
                }

                switch (filter.Command)
                {
                    case "init":
                        {
                            // Init windows service is the web.config StartWapiWindowsService = 1
                            var windowsServiceInit = WebConfigurationManager.AppSettings["StartWapiWindowsService"];
                            if (windowsServiceInit == "1" & status == "Stopped")
                            {
                                //this.ExecuteAsAdmin(filter.ServiceName);
                                sc.Start();
                            }

                            break;
                        }

                    case "start":
                        {
                            // Start Windows Service
                            if (status == "Stopped")
                            {
                                //this.ExecuteAsAdmin(filter.ServiceName);
                                sc.Start();
                            }

                            break;
                        }

                    case "stop":
                        {
                            // Stop Windows Service
                            if (status == "Running")
                            {
                                sc.Stop();
                            }

                            break;
                        }

                    default:
                        {
                            throw new Exception("Command not allowed");
                        }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                context.Leave();
            }

        }

        public void ExecuteAsAdmin(string service)
        {
            Process proc = new Process();
            proc.StartInfo.FileName = "cmd.exe";
            proc.StartInfo.UseShellExecute = true;
            proc.StartInfo.ErrorDialog = true;
            proc.StartInfo.Verb = "runas";
            proc.StartInfo.Arguments = " net start " + service;
            proc.Start();
            proc.WaitForExit(5000);

        }
    }
}

