﻿using System;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.Widget;
using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff;

namespace FAME.Advantage.Services.Lib.BusinessLogic.Widgets
{
    public class VoyantWidgetBo
    {
        private readonly IRepositoryWithTypedID<int> repository;
        // ReSharper disable NotAccessedField.Local
        private IRepositoryWithTypedID<Guid> repositoryguid;
        // ReSharper restore NotAccessedField.Local

        public VoyantWidgetBo(IRepositoryWithTypedID<Guid> repositoryguid, IRepositoryWithTypedID<int> repository)
        {
            this.repository = repository;
            this.repositoryguid = repositoryguid;
        }

        /// <summary>
        /// Configure the necessary parameters to each case of the Widget.
        /// </summary>
        /// <param name="serviceStatus"></param>
        /// <param name="existsOperationSettings"></param>
        /// <param name="userIsInRolAuthorized"></param>
        /// <returns></returns>
        public VoyantWidgetOutputModel GetVoyantWidgetConfigParameters(string serviceStatus, bool existsOperationSettings, bool userIsInRolAuthorized)
        {
            var output = new VoyantWidgetOutputModel { ShowMinimizeCommand = true, ShowWidget = true };
            //var uriAdvantage = ConfigurationAppSettings.Setting("AdvantageSiteUri", repository);
            if (serviceStatus.ToLower() == "uninstalled" | existsOperationSettings == false |
                userIsInRolAuthorized == false)
            {
                // Show publicity screen.....
                output.ShowCustomCommand = false;
                output.ShowMaximizeCommand = false;
                //var uri = (uriAdvantage.EndsWith("/")) ? uriAdvantage + "VoyantDefault.html" : uriAdvantage + "/VoyantDefault.html";
                output.UrlWidget = "VoyantDefault.html";
                output.UrlWidGetMaximized = "VoyantDefault.html";
                return output;
            }

            var actualUrlConfigValue = ConfigurationAppSettings.Setting("WapiVoyantAddress", repository);
         
            if (string.IsNullOrWhiteSpace(actualUrlConfigValue))
            {
                output.UrlWidget = "VoyantFirstTime.html";
                output.UrlWidGetMaximized = "VoyantFirstTime.html";
                output.ShowCustomCommand = false;
                output.ShowMaximizeCommand = false;
                return output;
            }

            if (actualUrlConfigValue.Contains(","))
            {
                var sep = new[] { ',' };
                string[] urls = actualUrlConfigValue.Split(sep, 2, StringSplitOptions.RemoveEmptyEntries);
                output.UrlWidget = urls[1];
                output.UrlWidGetMaximized = urls[0];

            }
            else
            {
                output.UrlWidget = actualUrlConfigValue;
                output.UrlWidGetMaximized = actualUrlConfigValue;
            }

            // See if the windows service is running now.
            output.ShowCustomCommand = (serviceStatus.ToLower() == "running");
           // output.UrlWidGetMaximized = output.UrlWidget;
            output.ShowMaximizeCommand = true;
            return output;
        }
    }
}
