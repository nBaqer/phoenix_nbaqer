﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CatalogsController.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   This controller hold the operation with Advantage general catalog domain tables.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Controllers.Catalogs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    using Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.Common;
    using FAME.Advantage.Services.Lib.BusinessLogic.Catalogs;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    /// This controller hold the operation with Advantage general catalog domain tables.
    /// </summary>
    [RoutePrefix("api/Catalogs/Catalogs")]
    public class CatalogsController : ApiController
    {
        //// ReSharper disable NotAccessedField.Local

        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        //// ReSharper restore NotAccessedField.Local
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CatalogsController"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="repository">
        /// Domains with GUID as primary Key
        /// </param>
        /// <param name="repositoryWithInt">
        /// Domains with integer as primary key
        /// </param>
        public CatalogsController(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            this.repositoryWithInt = repositoryWithInt;
        }

        #endregion

        #region Http Verb controllers

        /// <summary>
        /// Response to Get
        /// </summary>
        /// <param name="filter">
        /// Refer to class <paramref />
        /// Acceptable Values....
        /// <code>CampusGrpId</code>
        /// <code>UserId</code>
        /// <code>GetAll</code> or no parameter get all.
        /// filter Command: null or 1: return the values of the catalog
        ///                 2 return the values. but with select added.
        /// </param>
        /// <returns>
        /// list of ID  - Description of Campus
        /// </returns>
        [HttpGet]
        [Route("Get")]
        public IEnumerable<DropDownOutputModel> Get([FromUri] CatalogsInputModel filter)
        {
            try
            {
                if (filter == null || filter.FldName == null)
                {
                    throw new Exception("Service was call with Incorrect Parameters");
                }
                filter.Command = filter.Command ?? 1;
                List<DropDownOutputModel> result;

                if (filter.DisableSort == false)
                {
                    result = CatalogStatic.FilteredDropDownOutputModels(filter, this.repository, this.repositoryWithInt)
                            .OrderBy(x => x.Description)
                            .ToList();
                }
                else
                {
                    result = CatalogStatic.FilteredDropDownOutputModels(filter, this.repository, this.repositoryWithInt).ToList();
                }

                switch (filter.Command)
                {
                    case 1:
                        {
                            return result;
                        }
                    case 2:
                        {
                            var select = new DropDownOutputModel() { Description = "Select", ID = Guid.Empty.ToString() };
                            result.Insert(0, select);
                            return result;
                        }
                    default:
                        {
                            throw new Exception("Command unknown");
                        }
                }
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        #endregion
    }
}
