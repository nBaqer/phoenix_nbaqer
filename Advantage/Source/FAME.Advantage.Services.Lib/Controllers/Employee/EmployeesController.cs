﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmployeesController.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the EmployeesController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.Employee
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Web;
    using System.Web.Http;

    using Domain.Infrastructure.Entities;
    using Messages.Employee;
    using Messages.MostRecentlyUsed;
    using Services.Lib.BusinessLogic.Employees;
    using Services.Lib.Infrastructure.Exceptions;
    using Services.Lib.Infrastructure.Extensions;

    /// <summary>
    ///  The employees controller.
    /// </summary>
    [RoutePrefix("api/EmployeesSearch")]
    public class EmployeesController : ApiController
    {
        /// <summary>
        ///  The _request. represents request
        /// </summary>
        private readonly HttpRequest request;

        /// <summary>
        ///  The _bo.
        /// </summary>
        private readonly EmployeeBo bo;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeesController"/> class.
        /// </summary>
        /// <param name="empRepository">
        ///  The employees repository.
        /// </param>
        /// <param name="repositoryWithInt">
        ///  The repository with integer.
        /// </param>
        public EmployeesController(IRepository empRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.request = HttpContext.Current.Request;
            this.bo = new EmployeeBo(empRepository, repositoryWithInt);
        }

        /// <summary>
        /// Get data for single student and filtered by items in filter object
        /// //GET: 
        /// <code>
        /// api/Students/{studentId}
        /// </code>
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The MRU employee
        /// </returns>
        [HttpGet]
        [Route("GetByMru")]
        public IEnumerable<EmployeeMruOutputModel> GetByMru([FromUri] GetMruInputModel filter)
        {
            if (filter.UserId.IsEmpty() || filter.CampusId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("CampusId and UserId is required");
            }

            return this.bo.GetEmployeeMru(filter.UserId, filter.CampusId);
        }

        /// <summary>
        /// Get data for single student and filtered by items in filter object
        /// <code>GET: api/Students/{studentId}</code>
        /// </summary>
        /// <returns>
        /// The employee output model
        /// </returns>
        [HttpGet]
        [Route("GetBySearchTerm")]
        public IEnumerable<EmployeeSearchOutputModel> GetBySearchTerm()
        {
            if (this.request["SearchTerm"] == null)
            {
                throw new HttpBadRequestResponseException("Search Term is required");
            }

            // get the search term from the request object
            var searchTermInit = this.request["SearchTerm"].ToString(CultureInfo.InvariantCulture).Split('|');

            return this.bo.GetemployeeSearch(searchTermInit);
        }
    }
}
