﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmployersController.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the EmployersController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.Employer
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Web;
    using System.Web.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.Employer;
    using FAME.Advantage.Messages.MostRecentlyUsed;
    using FAME.Advantage.Services.Lib.BusinessLogic.Employers;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
    using FAME.Advantage.Services.Lib.Infrastructure.Extensions;

    /// <summary>
    /// The employers controller.
    /// </summary>
    [RoutePrefix("api/EmployerSearch")]
    public class EmployersController : ApiController
    {
        /// <summary>
        ///  The _request.
        /// </summary>
        private readonly HttpRequest request;

        /// <summary>
        /// The bo.
        /// </summary>
        private EmployerBo bo;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployersController"/> class. 
        /// constructor. Set the repository and request objects
        /// </summary>
        /// <param name="empRepository">
        /// The repository
        /// </param>
        /// <param name="repositoryWithInt">
        /// Repository with integer
        /// </param>
        public EmployersController(IRepository empRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.request = HttpContext.Current.Request;
            this.bo = new EmployerBo(empRepository, repositoryWithInt);
        }

        /// <summary>
        /// Get data for single student and filtered by items in filter object
        /// //GET: api/Students/{studentId}
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("~/api/EmployersSearch/GetBySearchTerm")]
        public IEnumerable<EmployerSearchOutputModel> GetBySearchTerm()
        {
            // throw exception if no search term passed in
            if (this.request["SearchTerm"] == null)
            {
                throw new HttpBadRequestResponseException("Search Term is required");
            }

            // get the search term from the request object
            var searchTermInit = this.request["SearchTerm"].ToString(CultureInfo.InvariantCulture).Split('|');

            return this.bo.GetemployerSearch(searchTermInit);
        }

        /// <summary>
        /// Get data for single student and filtered by items in filter object
        /// //GET: api/Students/{studentId}
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// </returns>
        [HttpGet]
        [Route("GetByMru")]
        public IEnumerable<EmployerMruOutputModel> GetByMru([FromUri] GetMruInputModel filter)
        {
            if (filter.UserId.IsEmpty() || filter.CampusId.IsEmpty()) throw new HttpBadRequestResponseException("CampusId and UserId is required");

            return this.bo.GetEmployerMru(filter.UserId, filter.CampusId);
        }
    }
}
