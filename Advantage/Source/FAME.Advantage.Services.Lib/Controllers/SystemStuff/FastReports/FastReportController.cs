﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FastReportController.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the FastReportController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.FastReports
{
    using System;
    using System.Web.Http;
    using BusinessLogic.SystemStuff;

    using Domain.Infrastructure.Entities;

    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.FastReports;

    using Infrastructure.Exceptions;
    using Messages.SystemStuff.Reports;

    /// <summary>
    /// The fast report controller.
    /// </summary>
    [RoutePrefix("api/SystemStuff/FastReports/FastReport")]
    public class FastReportController : ApiController
    {
        /// <summary>
        /// The bo for Fast Report Controller.
        /// </summary>
        private readonly FastReportBo bo;

        /// <summary>
        /// The repository integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryInt;

        /// <summary>
        /// Initializes a new instance of the <see cref="FastReportController"/> class. 
        /// Constructor - initialize the repository
        /// </summary>
        /// <param name="repository">
        /// The repository with GUID
        /// </param>
        /// <param name="repositoryInt">
        /// The integer Repository
        /// </param>
        public FastReportController(IRepository repository, IRepositoryWithTypedID<int> repositoryInt)
        {
            this.bo = new FastReportBo(repository, repositoryInt);

            this.repositoryInt = repositoryInt;
        }

        /// <summary>
        /// Get the tree of directories of Report Server
        /// </summary>
        /// <param name="filter">
        /// Only Command is valid 1: for get the directory tree.
        /// </param>
        /// <returns>
        /// The directory tree 
        /// </returns>
        /// <remarks>
        /// Example connection string : <code>Server=GUIRADO\SQLDEV2012;database=ReportServer;uid=MtiTstUsr1;pwd=Fame.Fame4321;</code>
        /// Example Report server address: <code>http://guirado/ReportServer_SQLDEV2012</code>
        /// </remarks>
        [HttpGet]
        [Route("Get")]
        public IReportOutputModel Get([FromUri]ReportInputModel filter)
        {
            if (filter == null || filter.Command == 0)
            {
                throw new HttpBadRequestResponseException("Service Return: Filter parameters can not be null");
            }

            IReportOutputModel output = ReportOutputModel.Factory();
            try
            {
                switch (filter.Command)
                {
                    case 1:
                        {
                            var ssrsAddress = ConfigurationAppSettings.Setting("ReportServerConexion", this.repositoryInt);
                            var ssrsfolderbase = string.Empty;
                            if (string.IsNullOrWhiteSpace(filter.Directory))
                            {
                                ssrsfolderbase = ConfigurationAppSettings.Setting(
                                    "ReportServerFastDirectory",
                                    this.repositoryInt);
                            }
                            else
                            {
                                ssrsfolderbase = filter.Directory.Trim();
                            }

                            output = this.bo.ConstructNodeTree(output, ssrsAddress, ssrsfolderbase);
                            return output;
                        }

                    default:
                        {
                            throw new HttpBadRequestResponseException("Command unknown");
                        }
                }
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException(ex.Message);
                ////throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }
    }
}
