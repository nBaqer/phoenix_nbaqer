﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UdfController.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Defines the SdfController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.Maintenance
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using BusinessLogic.SystemStuff.Maintenance;
    using Domain.Infrastructure.Entities;
    using Infrastructure.Exceptions;
    using Messages.SystemStuff.Udf;

    /// <summary>
    /// The SDF controller.
    /// </summary>
    [RoutePrefix("api/SystemStuff/Maintenance/Udf")]
    public class UdfController : ApiController
    {
        ///// <summary>
        ///// The lead repository.
        ///// </summary>
        ////private readonly IRepository leadRepository;

        ///// <summary>
        ///// The repository with integer.
        ///// </summary>
        ////private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        /// The Business Object.
        /// </summary>
        private readonly UdfBo bo;

        #region constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UdfController"/> class. 
        /// constructor
        /// </summary>
        /// <param name="leadRepository">
        /// Repository with GUID for Lead Table
        /// </param>
        /// <param name="repositoryWithInt">
        /// The repository with integer
        /// </param>
        public UdfController(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            // this.leadRepository = leadRepository;
            // this.repositoryWithInt = repositoryWithInt;
            this.bo = new UdfBo(leadRepository, repositoryWithInt);
        }

        #endregion

        /// <summary>
        ///  The get.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <returns>
        /// The <see cref="IUdfOutputModel"/> filter.
        /// Command 1: Get entities values
        /// Command 2: Get Pages related to one entity that accept UDF
        /// Command 3: Get UDFs Lists of Assigned to page and unassigned to page
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// If something is bad.
        /// </exception>
        [HttpGet]
        [Route("Get")]
        public IUdfOutputModel Get([FromUri] UdfInputModel filter)
        {
            if (filter == null || filter.Command < 1)
            {
                throw new HttpBadRequestResponseException("Filter or command can not be null");
            }

            try
            {
                var output = UdfOutputModel.Factory();
                switch (filter.Command)
                {
                    case 1:
                        {
                            output.EntityList = this.bo.GetAdvantageEntityItems();
                            break;
                        }

                    case 2:
                        {
                            output.EntityList = this.bo.GetAdvantagePagesSupportUdfEntityItems(filter);
                            break;
                        }

                    case 3:
                    {
                            output = this.bo.GetUdfListsAssignedAndUnassigned(filter, output);
                            break;
                        }

                    default:
                        {
                            throw new HttpBadRequestResponseException("Unrecognized Command");
                        }
                }

                output.Filter = filter.Clone();
                return output;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Post the assigned UDF to a page.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="HttpResponseMessage"/>.
        /// return OK is correct
        /// </returns>
         /// <exception cref="HttpBadRequestResponseException">
         /// System Error
        /// </exception>
        /// <exception cref="HttpResponseException">
        /// Error possible to correct
        /// </exception>
        [HttpPost]
        [Route("Post")]
        public HttpResponseMessage Post([FromBody] UdfOutputModel filter)
        {
            if (filter == null || filter.Filter.Command < 1)
            {
                throw new HttpBadRequestResponseException("Filter or command can not be null");
            }

            try
            {
                switch (filter.Filter.Command)
                {
                    case 1:
                        {
                            this.bo.UpdateInsertSchoolDefinedFieldToPage(filter);
                            return new HttpResponseMessage(HttpStatusCode.OK);
                        }

                    default:
                        {
                            throw new HttpBadRequestResponseException("Unrecognized Command");
                        }
                }
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }
    }
}
