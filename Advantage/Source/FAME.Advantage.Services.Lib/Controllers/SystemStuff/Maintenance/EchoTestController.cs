﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EchoTestController.cs" company="Fame">
//   2014
// </copyright>
// <summary>
//   The echo test controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.Maintenance
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    /// <summary>
    /// The echo test controller.
    /// </summary>
    ////[RoutePrefix("api/SystemStuff/Resources/EchoTest")]
    [RoutePrefix("api/SystemStuff/Maintenance/EchoTest")]
    public class EchoTestController : ApiController
    {
        /// <summary>
        /// This controller return a ECHO operation to test the Advantage WAPI.
        /// </summary>
        /// <returns>May the force be with you.</returns>
        [HttpGet]
        [Route("Get"), Route("")]
        public IEnumerable<string> Get()
        {
            var echovalues = new List<string> { "May", "the", "Force", "be", "with", "you" };
            return echovalues;
        }

        /// <summary>
        /// This ECHO function is for testing purpose and return the parameters that
        /// receive. Used to test the ability of WAPI to end parameters to service. 
        /// </summary>
        /// <param name="param1">
        /// The parameter 1.
        /// </param>
        /// <param name="param2">
        /// The parameter 2.
        /// </param>
        /// <param name="param3">
        /// The parameter 3.
        /// </param>
        /// <returns>
        /// the 3 parameter concatenated
        /// </returns>
        [HttpGet]
        [Route("GetParam")]
        public string GetParam([FromUri]string param1, string param2, string param3)
        {
            return param1 + param2 + param3;
        }

        /// <summary>
        /// The post.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [HttpPost]
        [Route("Post")]
        public string Post([FromBody]string[] values)
        {
            const string Separator = ",";
            string data = string.Join(Separator, values.ToList());
            string result = $"Successfully uploaded: {data}";
            return result;
        }
    }
}
