﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.SystemStuff.Maintenance.Smtp;
using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff;
using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance;
using FAME.Advantage.Services.Lib.Formatters.MultipartDataMediaFormatter.Infrastructure;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.Maintenance
{
    /// <summary>
    /// this class is used to configure the SMTP parameters
    /// in Advantage
    /// </summary>
    [RoutePrefix("api/SystemStuff/Maintenance/Smtp")]
    public class SmtpController : ApiController
    {
        private readonly SmtpBo bo;
        private readonly IRepositoryWithTypedID<int> repositoryInt;

        /// <summary>
        /// Constructor - initialize the repository
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="repositoryInt"></param>
        public SmtpController(IRepository repository, IRepositoryWithTypedID<int> repositoryInt)
        {
            bo = new SmtpBo(repository, repositoryInt);
            this.repositoryInt = repositoryInt;
        }


        /// <summary>
        /// Command 1 Return the Actual SMTP Parameters
        /// </summary>
        /// <param name="filter">
        /// Command 1: Return the Actual SMTP Parameters
        /// </param>
        /// <returns></returns>
        [HttpGet]
        [Route("Get")]
        public ISmtpOutputModel Get([FromUri]SmtpInputModel filter)
        {
            if (filter == null || filter.Command == 0)
            {
                throw new HttpBadRequestResponseException("Service Return: Filter parameters can not be null");
            }

            try
            {
                switch (filter.Command)
                {
                    case 1:
                        {
                            var output = SmtpOutputModel.Factory();
                            output.Settings.Password = ConfigurationAppSettings.Setting("FameSMTPPassword", repositoryInt);
                            output.Settings.Username = ConfigurationAppSettings.Setting("FameSMTPUsername", repositoryInt);
                            output.Settings.Server = ConfigurationAppSettings.Setting("FameSMTPServer", repositoryInt);
                            output.Settings.From = ConfigurationAppSettings.Setting("FromEmailAddress", repositoryInt);
                            var port = ConfigurationAppSettings.Setting("FameSMTPPort", repositoryInt);
                            output.Settings.Port = (string.IsNullOrWhiteSpace(port) ? 80 : int.Parse(port));
                            var oauth = ConfigurationAppSettings.Setting("FameSMTPOAuth", repositoryInt);
                            output.Settings.IsOauth = (oauth.ToUpper() != "YES") ? 0 : 1;
                            var ssl = ConfigurationAppSettings.Setting("FameSMTPSSL", repositoryInt);
                            output.Settings.IsSsl = (ssl.ToUpper() != "YES") ? 0 : 1;
                            var secret = ConfigurationAppSettings.Setting("FameSMTPOAuthClientSecret", repositoryInt);
                            secret = (!string.IsNullOrWhiteSpace(secret) && secret.Length > 40)
                                ? secret.Substring(0, 40)
                                : string.Empty;
                            output.Settings.AutClientSecret = secret;
                            return output;
                        }
                    default:
                        {
                            throw new HttpBadRequestResponseException("Command unknown");
                        }
                }
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        /// <summary>
        /// Receive a file and process it
        /// </summary>
        /// <param name="formData"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("PostFile")]
        public HttpResponseMessage PostFile([FromBody]FormData formData)
        {
            if (formData == null)
            {
                throw new HttpBadRequestResponseException("Service Return: Filter parameters can not be null");
            }

            try
            {
                // Process file and store in Setting
                var content = bo.ProcessSecretFile(formData);
                ConfigurationAppSettings.StoreSettingString("FameSMTPOAuthClientSecret", repositoryInt, content);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }


        /// <summary>
        /// Configure the Smtp Options
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Post")]
        public HttpResponseMessage Post([FromBody]SmtpOutputModel filter)
        {
            if (filter == null || filter.Filter == null || filter.Settings == null)
            {
                throw new HttpBadRequestResponseException("Service Return: Filter parameters can not be null");
            }


            switch (filter.Filter.Command)
            {
                case 1:
                    {
                        try
                        {
                            var settings = filter.Settings;
                            ConfigurationAppSettings.StoreSettingString("FameSMTPPassword", repositoryInt, settings.Password);
                            ConfigurationAppSettings.StoreSettingString("FameSMTPUsername", repositoryInt, settings.Username);
                            ConfigurationAppSettings.StoreSettingString("FameSMTPServer", repositoryInt, settings.Server);
                            ConfigurationAppSettings.StoreSettingString("FameSMTPPort", repositoryInt, settings.Port.ToString());
                            ConfigurationAppSettings.StoreSettingString("FromEmailAddress", repositoryInt, settings.From);

                            var isOauth = (settings.IsOauth == 0) ? "No" : "Yes";
                            ConfigurationAppSettings.StoreSettingString("FameSMTPOAuth", repositoryInt, isOauth);
                            var isSsl = (settings.IsSsl == 0)? "No" : "Yes";
                            ConfigurationAppSettings.StoreSettingString("FameSMTPSSL", repositoryInt, isSsl);
                            return new HttpResponseMessage(HttpStatusCode.OK);
                        }
                        catch (Exception ex)
                        {
                            throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
                        }

                    }
                default:
                    {
                        throw new HttpBadRequestResponseException("Service Return: Command Unknown " );
                    }
            }
        }




    }
}
