﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiWindowsServiceController.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Service Layer... Control of WAPI Windows Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.Maintenance
{
    using System;
    using System.Collections.Generic;
    using System.ServiceProcess;
    using System.Web.Http;

    using FAME.Advantage.Messages.SystemStuff.Maintenance;
    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    /// Service Layer... Control of WAPI Windows Service.
    /// </summary>
    [RoutePrefix("api/SystemStuff/Maintenance/WapiWindowsService")]
    public class WapiWindowsServiceController : ApiController
    {
        /// <summary>
        /// GET the status of the service, included if it is not installed
        /// </summary>
        /// <param name="filter">
        /// ServiceName = The name of the service.
        /// </param>
        /// <returns>
        /// Running, Stopped, Paused, Stopping, 
        /// Starting, Status Changing, uninstalled
        /// </returns>
        /// <remarks>
        /// In filter set the command to string empty.
        /// Address: <code>../api/SystemStuff/Maintenance/WapiWindowsService/</code>
        /// Proxy address: <code>../proxy/api/SystemStuff/Maintenance/WapiWindowsService/</code>
        /// </remarks>
        [HttpGet]
        [Route("Get")]
        public string Get([FromUri] WapiWindowsServiceInputModel filter)
        {
            var bo = new WapiWindowsServiceBo();
            try
            {
                var sc = new ServiceController(filter.ServiceName);
                var result = bo.GetStatusOfWindowsService(filter.ServiceName, sc);
                sc.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Execute the different commands to manage the windows Service
        /// </summary>
        /// <param name="inputList">
        /// Input List
        /// </param>
        /// <returns>
        /// HttpResponseMessage OK or HttpBadRequestResponseException
        /// </returns>
        /// <remarks>
        /// Address: <code>../api/SystemStuff/Maintenance/WapiWindowsService/</code>
        /// Proxy address: <code>../proxy/api/SystemStuff/Maintenance/WapiWindowsService/</code>
        /// </remarks>
        [HttpPost]
        [Route("Post")]
        public string Post([FromBody] IDictionary<string, string> inputList)
        {
            if (inputList == null)
            {
                return "Input parameters can not be null";
            }

            var filter = new WapiWindowsServiceInputModel();
            string input;
            if (inputList.TryGetValue("ServiceName", out input) == false)
            {
                return "Incorrect parameter name";
            }

            filter.ServiceName = input;
            if (inputList.TryGetValue("Command", out input) == false)
            {
                return "Incorrect parameter name";
            }

            filter.Command = input;

            var bo = new WapiWindowsServiceBo();
            try
            {
                bo.ExecuteWindowsServiceCommand(filter);
                return "Command Executed, waiting for complexion...";
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("web.config"))
                {
                    throw new HttpBadRequestResponseException("Configuration File is read only! Please check it out ");
                }

                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }
    }
}
