﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Http;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Messages.Common;
using FAME.Advantage.Messages.SystemStuff.Maintenance;
using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.Maintenance
{
    [RoutePrefix("api/SystemStuff/Maintenance/WapiVendorsLead")]
    public class WapiVendorsLeadController : ApiController
    {
        #region Constructor and Fields

        //repository object that represents data store
        // ReSharper disable NotAccessedField.Local
        private readonly IRepositoryWithTypedID<int> repository;
        // ReSharper restore NotAccessedField.Local
        private readonly WapiVendorsLeadBo bo;

        /// <summary>
        /// Constructor - initialize the repository
        /// </summary>
        /// <param name="repository"></param>
        public WapiVendorsLeadController(IRepositoryWithTypedID<int> repository)
        {
            this.repository = repository;
            bo = new WapiVendorsLeadBo(repository);
        }
        #endregion

        #region Vendors
        /// <summary>
        /// Get the vendors list or a individual vendor. 
        /// </summary>
        /// <param name="filter">
        /// if filter is null all vendor are returned. 
        /// VendorId = specific vendor id.
        /// </param>
        [HttpGet]
        [Route("Get")]
        public IEnumerable<WapiVendorsLeadOutputModel> Get([FromUri] WapiVendorsLeadOutputInputModel filter)
        {
            try
            {
                var result = bo.GetVendorsLead(filter);
                return result;
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        [HttpPost]
        [Route("PostUpdateVendor")]
        public WapiVendorsLeadOutputModel PostUpdateVendor([FromBody]WapiVendorsLeadOutputModel data)
        {
            if (data == null)
            {
                throw new HttpBadRequestResponseException("Service Return: " + "Filter can not be null");
            }

            try
            {
                // Determine if it is a insert or update based in Campaign ID.
                if (data.ID > 0)
                {
                    var dat = bo.UpdateVendorsLead(data);
                    return dat;
                }

                throw new HttpBadRequestResponseException("False Vendor Id");

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        #endregion

        #region Campaigns
        /// <summary>
        /// GetCampaign
        /// </summary>
        /// <param name="filter">
        /// Depend of the filter values the result
        /// if filters are 0: return all campaigns in table
        /// if filter VendorId > 0 and CampaignId = 0 : return all Campaigns associated with the Vendor
        /// if filter VendorId > 0 and CampaignId > 0 : Return a specific CampaignId for the given VendorId.
        /// if filter VendorId = 0 and CampaignId > 0 : Return the specific campaignId.
        /// </param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCampaign")]
        public IEnumerable<WapiVendorsCampaignLeadOutputModel> GetCampaign([FromUri] WapiVendorsLeadOutputInputModel filter)
        {
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("Service Return: " + "Filter can not be null");
            }

            try
            {
                var result = bo.GetVendorsCampaigns(filter);
                return result;
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        /// <summary>
        /// This insert and update Vendor Campaign based in THe ID value.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("PostInsertCampaign")]
        public WapiVendorsCampaignLeadOutputModel PostInsertCampaign([FromBody]WapiVendorsCampaignLeadOutputModel data)
        {
            if (data == null)
            {
                throw new HttpBadRequestResponseException("Service Return: " + "Filter can not be null");
            }

            try
            {
                // Determine if it is a insert or update based in Campaign ID.
                if (data.ID > 0)
                {
                    var dat = bo.UpdateVendorsCampaignLead(data);
                    return dat;
                }
                var ndat = bo.InsertVendorsCampaignLead(data);
                return ndat;

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        [HttpPost]
        [Route("PostDeleteCampaign")]
        public WapiVendorsCampaignLeadOutputModel PostDeleteCampaign([FromBody]int id)
        {
            try
            {
                var rec = bo.DeleteCampaignLead(id);
                return rec;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        #endregion

        #region Pay For Methods

        [HttpGet]
        [Route("GetPayForList")]
        public IEnumerable<DropDownIntegerOutputModel> GetPayForList()
        {
            try
            {
                var result = bo.GetPayForLists();
                return result;
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        #endregion

    }
}
