﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using System.Web.Http;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.Maintenance
{
    [RoutePrefix("api/SystemStuff/Maintenance/Stuff")]
    public class StuffController : ApiController
    {

        /// <summary>
        /// Return the version of
        /// Service Layer, Wapi Services, Advantage as a CSV string
        /// </summary>
        /// <returns>
        /// first value: Service Layer Version
        /// second value: Wapi Services Version
        /// third value: Advantage Version
        /// </returns>
        /// <remarks>
        /// return Version not available if problem and Error in version if a exception is raised
        /// Added case insensitive substitute path.
        /// </remarks>
        [HttpGet]
        [Route("GetVersion")]
        public string GetVersion()
        {
            string response = "Version not available";
            try
            {
                var virtualpath = HostingEnvironment.ApplicationVirtualPath;
                if (virtualpath != null)
                {
                    // Service Layer Version
                    response = AssemblyName.GetAssemblyName(HostingEnvironment.MapPath(virtualpath) + "/bin/FAME.Advantage.Services.dll").Version.ToString();

                    // Wapi Service Version
                    virtualpath = CaseInsenstiveReplace(virtualpath, "Services", "WapiServices");
                    response += ", " + AssemblyName.GetAssemblyName(HostingEnvironment.MapPath(virtualpath) + "/bin/WapiServices.dll").Version;

                    // Advantage Version
                    virtualpath = CaseInsenstiveReplace(virtualpath, "WapiServices", "Site");
                    response += ", " + AssemblyName.GetAssemblyName(HostingEnvironment.MapPath(virtualpath) + "/bin/FAME.Advantage.Site.Lib.dll").Version;
                }

                return response;
            }
            catch (Exception ex)
            {
                throw new HttpNotFoundResponseException(string.Format("Error in get version: {0}", ex.Message));
            }

        }

        public static string CaseInsenstiveReplace(string originalString, string oldValue, string newValue)
        {
            var regEx = new Regex(oldValue,
               RegexOptions.IgnoreCase | RegexOptions.Multiline);
            return regEx.Replace(originalString, newValue);
        }

    }
}
