﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="T1098Controller.cs" company="FAME">
//   2015, 2016
// </copyright>
// <summary>
// JAGG -  Controller for 1098T Taxes from Advantage to FAME Server
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.Maintenance
{
    using System;
    using System.Web.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.SystemStuff.Maintenance;
    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    /// Controller for 1098T Taxes from Advantage to FAME Server
    /// </summary>
    [RoutePrefix("api/SystemStuff/Maintenance/T1098")]
    public class T1098Controller : ApiController
    {
        /// <summary>
        ///  The BO.
        /// </summary>
        private readonly T1098Bo bo;

        /// <summary>
        /// Initializes a new instance of the <see cref="T1098Controller"/> class. 
        /// Constructor - initialize the repository
        /// </summary>
        /// <param name="repository">
        /// Repository GUID
        /// </param>
        /// <param name="repositoryInt">
        /// Repository integer
        /// </param>
        public T1098Controller(IRepository repository, IRepositoryWithTypedID<int> repositoryInt) 
        {
            this.bo = new T1098Bo(repository, repositoryInt);
        }

        /// <summary>
        ///  The get.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <returns>
        /// The <see cref="T1098OutputModel"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// If something was wrong
        /// </exception>
        [HttpGet]
        [Route("Get")]
        public T1098OutputModel Get([FromUri]T1098InputModel filter)
        {
            if (filter == null || filter.CampusId == null || filter.Year == 0)
            {
                throw new HttpBadRequestResponseException("Service Return: Filter parameters can not be null");
            }
            
            try
            {
                T1098OutputModel model = this.bo.SendInfoToFame(filter);
                return model;
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }
    }
}