﻿using System;
using System.Web.Http;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.Common;
using FAME.Advantage.Messages.SystemStuff.Maintenance.Smtp;
using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff;
using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.Maintenance
{
    [RoutePrefix("api/SystemStuff/Maintenance/SmtpSendEmail")]
    public class SmtpSendEmailController : ApiController
    {

        private readonly SmtpSendEmailBo bo;
        private readonly IRepositoryWithTypedID<int> repositoryInt;

        /// <summary>
        /// Constructor - initialize the repository
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="repositoryInt"></param>
        public SmtpSendEmailController(IRepository repository, IRepositoryWithTypedID<int> repositoryInt)
        {
            bo = new SmtpSendEmailBo(repository, repositoryInt);
            this.repositoryInt = repositoryInt;
        }


        /// <summary>
        /// Configure the Smtp Options
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Post")]
        public IGenericOutput Post([FromBody]SmtpSendMessageInputModel filter)
        {
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("Service Return: Filter parameters can not be null");
            }

            // Get SMTP parameters
            var output = SmtpOutputModel.Factory();
            output.Settings.Password = ConfigurationAppSettings.Setting("FameSMTPPassword", repositoryInt);
            output.Settings.Username = ConfigurationAppSettings.Setting("FameSMTPUsername", repositoryInt);
            output.Settings.Server = ConfigurationAppSettings.Setting("FameSMTPServer", repositoryInt);
            output.Settings.From = ConfigurationAppSettings.Setting("FromEmailAddress", repositoryInt);
            var port = ConfigurationAppSettings.Setting("FameSMTPPort", repositoryInt);
            output.Settings.Port = (string.IsNullOrWhiteSpace(port) ? 80 : int.Parse(port));
            var oauth = ConfigurationAppSettings.Setting("FameSMTPOAuth", repositoryInt);
            output.Settings.IsOauth = (oauth.ToUpper() != "YES") ? 0 : 1;
            var ssl = ConfigurationAppSettings.Setting("FameSMTPSSL", repositoryInt);
            output.Settings.IsSsl = (ssl.ToUpper() != "YES") ? 0 : 1;
            output.Settings.AutClientSecret = ConfigurationAppSettings.Setting("FameSMTPOAuthClientSecret", repositoryInt);
            output.Settings.Token = ConfigurationAppSettings.Setting("FameSMTPOAuthToken", repositoryInt);

            // In the case of OAuth smtp, validate that the token is loaded in the database.
            if (output.Settings.IsOauth == 1)
            {
                // Verify that the correct token is loaded in the database
                var fileToken = bo.GetFileToken();
                if (!string.IsNullOrWhiteSpace(fileToken))
                {
                    if (fileToken != output.Settings.Token)
                    {
                        // Update the token in Db (this step change the token stored in database in order to update it)
                        output.Settings.Token = fileToken;
                        ConfigurationAppSettings.StoreSettingString("FameSMTPOAuthToken", repositoryInt,
                            output.Settings.Token);
                    }
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(output.Settings.Token))
                    {
                        throw new HttpUnauthorizedResponseException("OAuth GMail process is not completed. Please complete it before try to send email");

                    }

                    // Create file directory if not exists and create the file token
                    bo.SetfileToken(output.Settings.Token);

                }
            }

            IGenericOutput res;
            switch (filter.Command)
            {
                case 1: // Send emails without Attachments
                    {
                        try
                        {
                            res = output.Settings.IsOauth == 1 ?
                                bo.SendMailOauth(output.Settings, filter)
                                : bo.SendMailUsingSmtpProtocol(output.Settings, filter);
                            return res;
                        }
                        catch (Exception ex)
                        {
                            throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
                        }

                    }
                case 2: // Ask if OAuth is authorized or not
                    {
                        try
                        {
                            res = bo.TestIfOAuthMailIsAuthorized(filter);
                            return res;
                        }
                        catch (Exception ex)
                        {
                            throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
                        }

                    }

                default:
                    {
                        throw new HttpBadRequestResponseException("Service Return: Command Unknown ");
                    }
            }
        }


    }
}
