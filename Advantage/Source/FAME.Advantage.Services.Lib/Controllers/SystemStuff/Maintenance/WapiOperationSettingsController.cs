﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiOperationSettingsController.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the WapiOperationSettingsController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

// ReSharper disable InconsistentNaming
namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.Maintenance
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.SystemStuff.Maintenance;
    using FAME.Advantage.Messages.SystemStuff.Wapi;
    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    /// The WAPI operation settings controller.
    /// </summary>
    [RoutePrefix("api/SystemStuff/Maintenance/WapiOperationSettings")]
    public class WapiOperationSettingsController : ApiController
    {
        #region Fields and constructor

         /// <summary>
        /// The Advantage Key Container
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Reviewed. Suppression is OK here.")]
        // ReSharper disable once InconsistentNaming
         private const string X_CONTAINER_NAME = "AdvantageKeyContainer";

        /// <summary>
        /// The test key container
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Reviewed. Suppression is OK here.")]
        private const string X_CONTAINER_NAME_TEST = "TestKeyContainer";

       /// <summary>
        /// The repository. repository object that represents data store
        /// </summary>
       // ReSharper disable once NotAccessedField.Local
        private readonly IRepositoryWithTypedID<int> repository;

        /// <summary>
        /// The business object.
        /// </summary>
        private readonly WapiOperationSettingsBo bo;

        /// <summary>
        /// Initializes a new instance of the <see cref="WapiOperationSettingsController"/> class. 
        /// Constructor - initialize the repository
        /// </summary>
        /// <param name="repository">
        /// The repository
        /// </param>
        public WapiOperationSettingsController(IRepositoryWithTypedID<int> repository)
        {
            this.repository = repository;
            this.bo = new WapiOperationSettingsBo(repository);
        }

        #endregion

        #region WAPI Operation Settings Service

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable&lt;WapiOperationSettingOutputModel&gt;"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// if filter is invalid
        /// </exception>
        /// <exception cref="HttpResponseException">
        /// If something is wrong
        /// </exception>
        [HttpGet]

        [Route("Get"), Route("")]
        public IEnumerable<WapiOperationSettingOutputModel> Get([FromUri] WapiOperationSettingsInputModel filter)
        {
            try
            {
                if (filter == null)
                {
                    throw new HttpBadRequestResponseException("Filter can not be null");
                }

                var result = this.bo.GetOperationSettings(filter);
                return result;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// The post update operation.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="HttpResponseMessage"/>.
        /// </returns>
        /// <exception cref="HttpResponseException">
        /// if something go wrong
        /// </exception>
        [HttpPost]
        [Route("PostUpdateOperation")]
        public HttpResponseMessage PostUpdateOperation([FromBody] WapiOperationSettingOutputModel data)
        {
            try
            {
                this.bo.UpdateOperationSetting(data);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// The post insert operation.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The <see cref="HttpResponseMessage"/>.
        /// bad request operation string not valid
        /// </returns>
        /// <exception cref="HttpResponseException">
        /// something go wrong
        /// </exception>
        [HttpPost]
        [Route("PostInsertOperation")]
        public HttpResponseMessage PostInsertOperation([FromBody] WapiOperationSettingOutputModel data)
        {
            try
            {
                var error = this.bo.InsertOperationSetting(data);
                if (error != string.Empty)
                {
                    var res = new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = error };
                    return res;
                }

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        [HttpPost]
        [Route("PostDeleteOperation")]
        public HttpResponseMessage PostDeleteOperation([FromUri] int id)
        {
            try
            {
                this.bo.DeleteOperationSetting(id);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        #endregion

        #region Catalogs

        /// <summary>
        /// Get the different catalog from WapiSetting table and related
        /// </summary>
        /// <param name="filter">
        ///  Possible values:
        /// COMPANY = External companies codes,
        /// MODE = Type External Operation,
        /// WSERVICES = WAPI services available,
        /// OPERATION = operations settings declared to executed in windows service.
        /// integer: Get company codes and the relation with a specific WAPI Service
        /// </param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCatalog")]
        public IEnumerable<WapiCatalogsOutputModel> GetCatalog([FromUri] WapiCatalogInputModel filter)
        {
            try
            {
                var result = this.bo.GetWapiCatalogs(filter);
                return result;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        [HttpPost]
        [Route("PostUpdateCatalog")]
        public IEnumerable<WapiCatalogsOutputModel> PostUpdateCatalog([FromBody] WapiCatalogInputModel filter)
        {
            try
            {
                var key = this.Request.Headers.GetValues("AuthKey").First();
                var result = this.bo.PostWapiCatalogs(filter, key);
                return result;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        #endregion

        #region Security keys TAB services

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operation">
        /// Values from 1 to 3 are used in production
        /// 1: create key if not exists
        /// 2: delete key if exists
        /// 3: Retrieve public key if exists
        /// Values 41,42,43 are used to test
        /// 41: create key if not exists
        /// 42: delete key if exists
        /// 43: Retrieve public key if exists        /// </param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSecurityKeyService")]
        public string GetSecurityKeyService([FromUri] int operation)
        {
            try
            {
                string res;
                switch (operation)
                {
                    case 1:
                        {
                            res = this.bo.CreateSecurityKey(X_CONTAINER_NAME);
                            break;
                        }

                    case 2:
                        {
                            res = this.bo.DeleteSecurityKey(X_CONTAINER_NAME);
                            break;
                        }

                    case 3:
                        {
                            res = this.bo.GetSecurityKey(X_CONTAINER_NAME);
                            break;
                        }

                    case 41:
                        {
                            res = this.bo.CreateSecurityKey(X_CONTAINER_NAME_TEST);
                            break;
                        }

                    case 42:
                        {
                            res = this.bo.DeleteSecurityKey(X_CONTAINER_NAME_TEST);
                            break;
                        }

                    case 43:
                        {
                            res = this.bo.GetSecurityKey(X_CONTAINER_NAME_TEST);
                            break;
                        }

                    default:
                        {
                            throw new HttpBadRequestResponseException("Error: Parameter incorrect");
                        }
                }

                return res;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        #endregion

        #region On Demand Flags

        /// <summary>
        /// The get on demand flags.
        /// </summary>
        /// <param name="id">
        /// The ID.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable&lt;WapiOnDemandFlagsOutputModel&gt;"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// </exception>
        [HttpGet]
        [Route("GetOnDemandFlags")]
        public IEnumerable<WapiOnDemandFlagsOutputModel> GetOnDemandFlags([FromUri] int id = -1)
        {
            // Prevent for false id.
            if (id < 1)
            {
                id = -1;
            }

            try
            {
                var result = this.bo.GetOnDemandFlags(id);
                return result;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// This return a single demand flag and when go to 0 the reason
        /// Use this for quest if the operation on demand finished and how.
        /// </summary>
        /// <param name="id">The operation ID</param>
        /// <returns>The value of the demand flag</returns>
        [HttpGet]
        [Route("GetSingleOnDemandFlag")]
        public WapiOnDemandFlagsOutputModel GetSingleOnDemandFlag([FromUri] int id)
        {
            try
            {
                var result = this.bo.GetSingleOnDemandFlag(id);
                return result;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Set 1 or 0 the On Demand status flag.
        /// </summary>
        /// <param name="filter">
        /// Id The operation Id
        /// Value 1/0 the flag value. 0 reset the flag (no on demand) 1(on demand)
        /// </param>
        /// <returns>
        /// HttpResponse.Ok if the operation was OK.
        /// </returns>
        [HttpPost]
        [Route("PostOnDemandFlags")]
        public HttpResponseMessage PostOnDemandFlags([FromUri] WapiOnDemandFlagsInputModel filter)
        {
            // Prevent for null filter.
            if (filter == null || filter.Id < 1 || filter.Value > 1)
            {
                throw new HttpBadRequestResponseException("The filter values are incorrect");
            }

            try
            {
                this.bo.SetOnDemandFlags(filter);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        #endregion

        #region Last Execution Time values

        /// <summary>
        /// Receive the operation Id and the new value of time
        /// this value is the moment that the operation was executed 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("PostLastExecutedTime")]
        public HttpResponseMessage PostLastExecutedTime([FromUri] WapiSetDateTimeInputModel filter)
        {
            // Prevent for null filter.
            if (filter == null || filter.Id < 1 || DateTime.Parse(filter.Value) < DateTime.Now.AddYears(-4))
            {
                throw new HttpBadRequestResponseException("The filter values are incorrect");
            }

            try
            {
                this.bo.SetLastExecutedTime(filter);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Get the last executed time in a IEnumerable
        /// </summary>
        /// <param name="id">
        /// -1 return id and last executed time for all operations
        /// > 0 return the specific Id. If Id does not exists a exception is raised.
        /// </param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetLastExecutedTime")]
        public IEnumerable<WapiGetDateTimeOutputModel> GetLastExecutedTime([FromUri] int id = -1)
        {
            // Prevent for false id.
            id = (id < 1) ? -1 : id;

            try
            {
                var result = this.bo.GetLastExecutedTime(id);
                return result;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        #endregion

        #region Logger Operation

        /// <summary>
        /// Get the logger records
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetLoggerRecords")]
        public IEnumerable<WapiLoggerOutputModel> GetLoggerRecords([FromUri] WapiLoggerInputModel filter)
        {
            try
            {
                var result = this.bo.GetLoggerRecords(filter);
                return result;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Truncate the logger table and clean it.
        /// </summary>
        [HttpPost]
        [Route("PostLoggerRecord")]
        public HttpResponseMessage PostLoggerRecord([FromBody] WapiLoggerOutputModel logRecord)
        {
            try
            {
                this.bo.PostLoggerRecord(logRecord);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Truncate the logger table and clean it.
        /// </summary>
        [HttpPost]
        [Route("PostDeleteLoggerRecords")]
        public HttpResponseMessage PostDeleteLoggerRecords()
        {
            try
            {
                this.bo.PostDeleteLoggerRecords();
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        #endregion
    }
}
