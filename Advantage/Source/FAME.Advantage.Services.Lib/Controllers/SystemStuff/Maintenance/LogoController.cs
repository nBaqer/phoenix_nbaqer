﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogoController.cs" company="FAME">
//   2013
// </copyright>
// <summary>
//   Defines the LogoController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.Maintenance
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Messages.SystemStuff.Maintenance;
    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance;
    using FAME.Advantage.Services.Lib.Formatters.MultipartDataMediaFormatter.Infrastructure;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    /// The logo controller.
    /// </summary>
    [RoutePrefix("api/SystemStuff/Maintenance/Logo")]
    public class LogoController : ApiController
    {
        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repository;

        /// <summary>
        /// The bo.
        /// </summary>
        private readonly LogoBo bo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogoController"/> class. 
        /// Constructor - initialize the repository
        /// </summary>
        /// <param name="repository">
        /// the repository to be used
        /// </param>
        public LogoController(IRepositoryWithTypedID<int> repository) 
        {
            this.repository = repository;
            this.bo = new LogoBo(repository);
        }

        /// <summary>
        /// Get the logos or logo filtered by Id or Image Code.
        /// </summary>
        /// <param name="filter">
        /// admit <code>ImageCode, ImgId</code> 
        /// </param>
        /// <returns>A enumerable in all case.</returns>
        /// <remarks>ImageId and ImageCode always return 1 logo or none.</remarks>
        [HttpGet]
        [Route("Get")]
        public IEnumerable<SchoolLogoOutputModel> Get([FromUri] SchoolLogoInputModel filter = null)
        {
            try
            {
                // Constrains...............
                if (filter != null)
                {
                    if (filter.ImageId != null)
                    {
                        var result = this.bo.GetLogosByLogoId(filter);
                        return result;
                    }

                    if (!string.IsNullOrEmpty(filter.ImageCode))
                    {
                        var result = this.bo.GetLogosByImageCode(filter);
                        return result;
                    }
                }

                var resulta = this.bo.GetAllLogos();
                return resulta;
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
           }
        }

        /// <summary>
        /// Post this procedure received the logo or logos files
        /// </summary>
        /// <param name="formData">Logo files</param>
        /// <returns>200 OK if all is OK</returns>
        [HttpPost]
        [Route("PostLogoFile")]
        public HttpResponseMessage PostLogoFile(FormData formData)
        {
            try
            {
                this.bo.StoreLogoFile(formData);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
               throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        /// <summary>
        /// Delete the logo in database
        /// </summary>
        /// <param name="logodata">The ID is the only needed data</param>
        /// <returns>
        /// status code OK.
        /// </returns>
        [HttpDelete]
        [Route("Delete")]
        public HttpResponseMessage Delete([FromBody] SchoolLogoOutputModel logodata)
        {
            try
            {
                // Constrains...............
                if (logodata != null)
                {
                    var logo = this.repository.Get<SySchoolLogo>(logodata.ID);
                    if (logo == null)
                    {
                        throw new HttpBadRequestResponseException(
                            "The Image does not exists or was erased in server.");
                    }

                    this.repository.Delete(logo);
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }

                throw new HttpBadRequestResponseException("The required parameter does not exists.");
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        /// <summary>
        /// The logo code, observations and if it official use can be edited and updated.
        /// </summary>
        /// <param name="logodata">
        /// The information about the logo
        /// </param>
        /// <returns>
        /// OK if OK
        /// </returns>
        [HttpPost]
        [Route("PostUpdateLogo")]
        public HttpResponseMessage PostUpdateLogo([FromBody]SchoolLogoOutputModel logodata)
        {
            try
            {
                var logo = this.repository.Get<SySchoolLogo>(logodata.ID);
                if (logo == null)
                {
                    throw new HttpBadRequestResponseException("The Image does not exists or was erased in server.");
                }

                logo.UpdateLogo(logodata.Description, logodata.ImageCode, logodata.OfficialUse);

                this.repository.Update(logo);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }
    }
}
