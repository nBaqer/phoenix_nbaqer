﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppUiController.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The <code>klassApp</code> controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.Mobile
{
    using System;
    using System.Linq;
    using System.Web.Http;

    using BusinessLogic.SystemStuff.Mobile;
    using Domain.Infrastructure.Entities;

    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Messages.SystemStuff.Mobile.ApiKlass;
    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff;

    using Infrastructure.Exceptions;
    using Messages.SystemStuff.Mobile;

    /// <summary>
    ///  The <code>klassApp</code> controller.
    /// </summary>
    [RoutePrefix("api/SystemStuff/Mobile/KlassAppUi")]
    public class KlassAppUiController : ApiController
    {
        /// <summary>
        ///  The business Object.
        /// </summary>
        private readonly KlassAppBo bo;

        /// <summary>
        ///  The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        /// The repository.
        /// </summary>
        // ReSharper disable once NotAccessedField.Local
        private readonly IRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="KlassAppUiController"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="leadRepository">
        /// repository GUID
        /// </param>
        /// <param name="repositoryWithInt">
        /// repository integer
        /// </param>
        public KlassAppUiController(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = leadRepository;
            this.repositoryWithInt = repositoryWithInt;
            this.bo = new KlassAppBo(leadRepository, repositoryWithInt);
        }

        /// <summary>
        /// Configure the Configuration field in <code>Klass_App</code>
        /// </summary>
        /// <param name="filter">
        /// The filter parameters.
        /// CampusId = GUID,
        /// UserId = GUID
        /// Command = 0/1: Get All from configuration setting 
        /// Command = 2: Send Configuration setting to KLASS APP
        /// Command = 3: Initialize Configuration in Advantage Table 
        /// Command = 4: Return true if any configuration setting is pending 
        ///              if true the configuration settings are send besides. 
        /// Command = 10: Clear all in KLASS APP
        /// </param>
        /// <returns>
        /// The <code>IClassAppConfigurationSettingModel</code> output Model
        /// </returns>
        [HttpGet]
        [Route("Get")]
        public ClassAppUiConfigurationOutputModel Get([FromUri]ClassAppInputModel filter)
        {
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("Filter has not value");
            }

            try
            {
                var output = new ClassAppUiConfigurationOutputModel();

                // Get Klass_Application Setting Model List................................................................................
                if (filter.Command == 1 | filter.Command == 0)
                {
                    output.ConfigurationSetting = this.bo.GetAllConfigurationSettings(filter);
                }

                // Send Configuration options to KlassAPP
                if (filter.Command == 2 || filter.Command == 4)
                {
                    var operation = this.WapiOperation();

                    if (filter.Command == 2)
                    {
                        output.ConfigurationSetting = this.bo.SendConfigurationSettingsToMobile(filter, operation);
                    }
                    else
                    {
                        // Command = 4 Return true if any configuration setting is pending
                        output = (ClassAppUiConfigurationOutputModel)this.bo.GetActualServiceConfigurationStatus(filter, operation);
                    }
                }

                // Initialize Configuration in Advantage Table will be truncate a restore with initial values.
                if (filter.Command == 3)
                {
                    this.bo.ConfigureAdvantageKlassAppTable(filter);
                    output.ConfigurationSetting = this.bo.GetAllConfigurationSettings(filter);
                }

                if (filter.Command == 10)
                {
                    // This send the clear command to KlassApp
                    var operation = this.WapiOperation();
                    this.bo.SendClearOperationToKlassApp(filter, operation);
                }

                output.Filter = filter;

                return output;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        private IWapiOperation WapiOperation()
        {
            var operation = Messages.SystemStuff.Mobile.ApiKlass.WapiOperation.Factory();
            var wapit = this.repositoryWithInt.Query<SyWapiSettings>()
                .Where(x => x.ExternalOperationModeObj.Code == "KLASS_APP_SERVICE");
            if (!wapit.Any())
            {
                throw new ApplicationException("Please configure KLASS_APP_STUDENT");
            }

            var wap = wapit.First();
            operation.CodeOperation = wap.CodeOperation;
            operation.ConsumerKey = wap.ConsumerKey;
            operation.PrivateKey = wap.PrivateKey;
            operation.ExternalUrl = wap.ExternalUrl;

            // Read the Advantage configuration settings
            operation.KlassAppUrlConfigCustomFields = ConfigurationAppSettings.Setting(
                "KlassApp_UrlConfigCustomFields",
                this.repositoryWithInt);
            operation.KlassAppUrlConfigKeyValuesPairs = ConfigurationAppSettings.Setting(
                "KlassApp_UrlConfigKeyValuePairs",
                this.repositoryWithInt);
            operation.KlassAppUrlConfigLocations =
                ConfigurationAppSettings.Setting("KlassApp_UrlConfigLocations", this.repositoryWithInt);
            operation.KlassAppUrlConfigMajors =
                ConfigurationAppSettings.Setting("KlassApp_UrlConfigMajors", this.repositoryWithInt);
            return operation;
        }

        /// <summary>
        /// The post.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="ClassAppConfigurationOutputModel"/>.
        /// </returns>
        [HttpPost]
        [Route("Post")]
        public ClassAppConfigurationOutputModel Post([FromBody] ClassAppConfigurationOutputModel model)
        {
            if (model == null)
            {
                throw new HttpBadRequestResponseException("Model has not value");
            }

            try
            {
                ClassAppConfigurationOutputModel output = this.bo.UpdateConfigurationTable(model);
                return output;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }
    }
}
