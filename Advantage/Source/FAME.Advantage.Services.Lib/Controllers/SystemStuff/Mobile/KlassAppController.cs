﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppController.cs" company="FAME">
//   2016
// </copyright>
// <summary>
// JAGG - Defines the KlassAppController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.Mobile
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings;
    ////using FAME.Advantage.Services.Lib.BusinessLogic.Students.Academic;
    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff;
    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Mobile;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    ///  The <code>klass</code> application controller.
    /// </summary>
    [RoutePrefix("api/SystemStuff/Mobile/KlassApp")]
    public class KlassAppController : ApiController
    {
        // ReSharper disable NotAccessedField.Local

        /// <summary>
        ///  The repository.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryTyped;

        // ReSharper restore NotAccessedField.Local

        /// <summary>
        /// The business object.
        /// </summary>
        private readonly WapiKlassAppBo bo;

        /// <summary>
        /// Initializes a new instance of the <see cref="KlassAppController"/> class.
        /// </summary>
        /// <param name="repository">
        ///  The repository.
        /// </param>
        /// <param name="repositorytyped">
        ///  The repository Type.
        /// </param>
        public KlassAppController(IRepository repository, IRepositoryWithTypedID<int> repositorytyped)
        {
            this.bo = new WapiKlassAppBo(repository, repositorytyped);
            ////this.boa = new StudentAttendanceBo(repository, this.repositoryTyped);
            this.repositoryTyped = repositorytyped;
            ////this.request = HttpContext.Current.Request;
        }

        #region Production procedures

        /// <summary>
        ///  The get.
        /// </summary>
        /// <returns>
        /// The <see cref=" IList&lt;KlassAppOutputModel&gt;"/>.
        /// </returns>
        [HttpGet]
        [Route("Get")]
        public IList<KlassAppStudentOutputModel> Get()
        {
            string status = string.Empty;
            try
            {
                var klassObject = KlassStudentObject.Factory();

                ////klassObject.KlassAppDaysToConsiderer = int.Parse(ConfigurationAppSettings.Setting("KlassApp_DaysToConsider", this.repositoryTyped));

                // Get the authorization ID and Token
                var wapit =
                    this.repositoryTyped.Query<SyWapiSettings>()
                        .SingleOrDefault(x => x.ExternalOperationModeObj.Code == "KLASS_APP_SERVICE");
                if (wapit == null)
                {
                    throw new ApplicationException("Please configure KLASS_APP External Operation");
                }
                
                klassObject.Operation = wapit;
                    
                // Get the active campus to be consider
                var configValuesList = ConfigurationAppSettings.SettingByCampus("KlassApp_CampusToConsider", this.repositoryTyped);
                foreach (ConfigurationAppSettingValues settingValues in configValuesList)
                {
                    if (settingValues.Value == "Yes")
                    {
                        klassObject.CampusActiveGuidList.Add(settingValues.Campus == null ? Guid.Empty : settingValues.Campus.ID);
                    }
                }

                // Detect if they are not campus selected....
                if (klassObject.CampusActiveGuidList.Count == 0)
                {
                    throw new ApplicationException("None Campus was selected to be send to KlassApp. Please configure application setting: [KlassApp_CampusToConsider]");
                }
                ////this.bo.ValidateStudentOperation(klassObject);

                var settingList = this.bo.GetConfigurationFields();
                if (settingList.Count == 0)
                {
                    throw new ApplicationException("Klass-App Configuration settings are not present. Please go to WAPI configuration -> Mobility Application and set them");
                }

                // Test if the setting list was configured.
                var count = settingList.Count(x => x.KlassAppIdentification == 0);
                if (count > 0)
                {
                    throw new ApplicationException($"Please send configuration setting to Klass-App, there are {count} settings with Id value = 0");
                }

                var output = this.bo.GetStudentInfo(klassObject, settingList);
                var gpaCampusDictionary = this.GetGpaMethodDictionary();
                if (output.Count > 0)
                {
                    output = this.bo.ApplySettings(settingList, output, klassObject, gpaCampusDictionary);
                    output = this.bo.ApplyMajorStatusLocations(settingList, output);
                }

                return output;
                ////return new List<KlassAppStudentOutputModel>();
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }
        #endregion

        /// <summary>
        /// The get GPA method dictionary.
        /// </summary>
        /// <returns>
        /// A dictionary of campus GUID and the GPO Calculation method Value
        /// </returns>
        private Dictionary<Guid, string> GetGpaMethodDictionary()
        {
            // Get the GPAMethod to be consider
            var output = new Dictionary<Guid, string>();
            var configValuesList = ConfigurationAppSettings.SettingByCampus("GPAMethod", this.repositoryTyped);
            foreach (ConfigurationAppSettingValues settingValues in configValuesList)
            {
                if (settingValues.Campus == null)
                {
                    // Setting is for all campus
                    output.Clear();
                    output.Add(Guid.Empty, settingValues.Value);
                    return output;
                }

                output.Add(settingValues.Campus.ID, settingValues.Value);
            }

            return output;
        }
    }
}