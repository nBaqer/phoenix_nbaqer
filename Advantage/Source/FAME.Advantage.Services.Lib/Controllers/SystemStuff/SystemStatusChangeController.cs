﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemStatusChangeController.cs" company="FAME Inc.">
// FAME.Advantage.Services.Lib.BusinessLogic.SystemStatus  
// </copyright>
// <summary>
//   Defines the Dropped type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Web.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.Common;
    using FAME.Advantage.Messages.SystemStuff.SystemStatuses;
    using FAME.Advantage.Messages.Web;
    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff;
    using FAME.Advantage.Services.Lib.Infrastructure.ActionFilters;

    /// <summary>
    /// Web API Controller for CRUD Operations of System Status Change History
    /// </summary>
    [RoutePrefix("api/SystemStuff/SystemStatusChange")]
    public class SystemStatusChangeController : ApiController
    {
        /// <summary>
        /// The nHibernate Repository for Guids
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The nHibernate Repository for integers
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        ///  Initializes a new instance of the <see cref="SystemStatusChangeController"/> class. 
        /// </summary>
        /// <param name="repository">
        /// The nHibernate Repository for Guids
        /// </param>
        /// <param name="repositoryWithInt">
        /// The nHibernate Repository for integers
        /// </param>
        public SystemStatusChangeController(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            this.repositoryWithInt = repositoryWithInt;
        }

        /// <summary>
        /// Get a List of Status Change History based on Student Enrollment 
        /// </summary>
        /// <param name="input">The Input Model to filter the Status Change History.</param>
        /// <returns>
        /// An object of the Response&lt;<see cref="IEnumerable&lt;SystemStatusChangeModel&gt;"/>&gt; class. 
        /// </returns>
        [HttpPost]
        [Route("PostGetStatusHistory")]
        public Response<IEnumerable<SystemStatusChangeModel>> PostGetStatusHistory(SystemStatusChangeFilterModel input)
        {
            Response<IEnumerable<SystemStatusChangeModel>> response = new Response<IEnumerable<SystemStatusChangeModel>>();
            try
            {
                if (input != null)
                {
                    SystemStatusHistory statusChange = new SystemStatusHistory(this.repository, this.repositoryWithInt);
                    response = statusChange.GetHistory(input);
                }
                else
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Input is a required field!";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }


        /// <summary>
        /// Get a List of Status Codes
        /// </summary>
        /// <param name="input">The Input Model to filter the Status Codes.</param>
        /// <returns>
        /// An object of the Response&lt;<see cref="IEnumerable&lt;DropDownOutputModel&gt;"/>&gt; class. 
        /// </returns>
        [HttpGet]
        [Route("GetStatusCodesList")]
        [HttpGetBindJsonQueryString(typeof(SystemStatusChangeModel), "input")]
        public Response<IEnumerable<SystemStatusCodeModel>> GetStatusCodesList(SystemStatusChangeModel input)
        {
            Response<IEnumerable<SystemStatusCodeModel>> response = new Response<IEnumerable<SystemStatusCodeModel>>();
            try
            {
                if (input != null)
                {
                    SystemStatusHistory statusChange = new SystemStatusHistory(this.repository, this.repositoryWithInt);
                    response = statusChange.GetStatusCodesList(input);
                }
                else
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Input is a required field!";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Get a List of Active Users to be used in Requested by dropdown list
        /// </summary>
        /// <returns>
        /// An object of the Response&lt;<see cref="IEnumerable&lt;DropDownOutputModel&gt;"/>&gt; class. 
        /// </returns>
        [HttpGet]
        [Route("GetRequestedBy")]
        public Response<IEnumerable<DropDownOutputModel>> GetRequestedBy()
        {
            Response<IEnumerable<DropDownOutputModel>> response = new Response<IEnumerable<DropDownOutputModel>>();
            try
            {
                SystemStatusHistory statusChange = new SystemStatusHistory(this.repository, this.repositoryWithInt);
                response = statusChange.GetRequestedBy();
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }


        /// <summary>
        /// Get a list of Dropped Reasons
        /// </summary>
        /// <returns>
        /// An object of the Response&lt;<see cref="IEnumerable&lt;DropDownOutputModel&gt;"/>&gt; class. 
        /// </returns>
        [HttpGet]
        [Route("GetDroppedReasons")]
        public Response<IEnumerable<DropDownOutputModel>> GetDroppedReasons()
        {
            Response<IEnumerable<DropDownOutputModel>> response = new Response<IEnumerable<DropDownOutputModel>>();
            try
            {
                SystemStatusHistory statusChange = new SystemStatusHistory(this.repository, this.repositoryWithInt);
                response = statusChange.GetDroppedReasons();
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Verify if the Status Change is allowed and meets all the required field.
        /// </summary>
        /// <param name="input">The Input Model to validate the Status Change.</param>
        /// <returns>
        ///  An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class indicating if the Status Change is allowed. 
        /// </returns>
        [HttpGet]
        [Route("GetValidateStatusChange")]
        [HttpGetBindJsonQueryString(typeof(SystemStatusChangeModel), "input")]
        public Response<SystemStatusChangeModel> GetValidateStatusChange(SystemStatusChangeModel input)
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            try
            {
                if (input != null)
                {
                    SystemStatusHistory statusChange = new SystemStatusHistory(this.repository, this.repositoryWithInt);
                    response = statusChange.ValidateStatusChange(input);
                }
                else
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Input is a required field!";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Insert, Update or Delete a Status Change
        /// </summary>
        /// <param name="input">The Input Model for the Status Change CRUD operation </param>
        /// <returns>
        /// An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class indicating if the Status Change CRUD operation succeeded.
        /// </returns>
        [HttpPost]
        [Route("PostApplyStatusChange")]
        public Response<SystemStatusChangeModel> PostApplyStatusChange(SystemStatusChangeModel input)
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            try
            {
                if (input != null)
                {
                    SystemStatusHistory statusChange = new SystemStatusHistory(this.repository, this.repositoryWithInt);
                    response = statusChange.ApplySystemStatusChange(input);
                }
                else
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Input is a required field!";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Verify if the Status Change is allowed and meets all the required field.
        /// </summary>
        /// <param name="input">The Input Model to validate the Status Change.</param>
        /// <returns>
        ///  An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class indicating if the Status Change is allowed. 
        /// </returns>
        [HttpGet]
        [Route("GetValidateAttendanceAndGrades")]
        [HttpGetBindJsonQueryString(typeof(SystemStatusChangeModel), "input")]
        public Response<SystemStatusChangeModel> GetValidateAttendanceAndGrades(SystemStatusChangeModel input)
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            try
            {
                if (input != null)
                {
                    SystemStatusHistory statusChange = new SystemStatusHistory(this.repository, this.repositoryWithInt);
                    response = statusChange.ValidateAttendanceAndGrades(input);
                }
                else
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Input is a required field!";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Verify if the Delete Status Change is allowed and meets all the required field.
        /// </summary>
        /// <param name="input">The Status Change Id that will be deleted.</param>
        /// <returns>
        ///  An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class indicating if the Status Change is allowed. 
        /// </returns>
        [HttpGet]
        [Route("GetValidateDelete")]
        [HttpGetBindJsonQueryString(typeof(SystemStatusChangeModel), "input")]
        public Response<SystemStatusChangeModel> GetValidateDelete(SystemStatusChangeModel input)
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            try
            {
                if (input != null)
                {
                    SystemStatusHistory statusChange = new SystemStatusHistory(this.repository, this.repositoryWithInt);
                    response = statusChange.ValidateDelete(input);
                }
                else
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Input is a required field!";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Get a list of Delete Status Change Reasons
        /// </summary>
        /// <returns>
        /// An object of the Response&lt;<see cref="IEnumerable&lt;DropDownOutputModel&gt;"/>&gt; class. 
        /// </returns>
        [HttpGet]
        [Route("GetDeleteReasons")]
        public Response<IEnumerable<DropDownOutputModel>> GetDeleteReasons()
        {
            Response<IEnumerable<DropDownOutputModel>> response = new Response<IEnumerable<DropDownOutputModel>>();
            try
            {
                SystemStatusHistory statusChange = new SystemStatusHistory(this.repository, this.repositoryWithInt);
                response = statusChange.GetDeleteReasons();
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Delete a Status Change History
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        ///  An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class indicating if the Status Change is allowed. 
        /// </returns>
        [HttpPost]
        [Route("PostDelete")]
        public Response<SystemStatusChangeModel> PostDelete(SystemStatusChangeModel input)
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            try
            {
                if (input != null)
                {
                    SystemStatusHistory statusChange = new SystemStatusHistory(this.repository, this.repositoryWithInt);
                    response = statusChange.Delete(input);
                }
                else
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Input is a required field!";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Get a list of Leave of Absence Reasons
        /// </summary>
        /// <returns>
        /// An object of the Response&lt;<see cref="IEnumerable&lt;DropDownOutputModel&gt;"/>&gt; class. 
        /// </returns>
        [HttpGet]
        [Route("GetLoaReasons")]
        public Response<IEnumerable<DropDownOutputModel>> GetLoaReasons()
        {
            Response<IEnumerable<DropDownOutputModel>> response = new Response<IEnumerable<DropDownOutputModel>>();
            try
            {
                SystemStatusHistory statusChange = new SystemStatusHistory(this.repository, this.repositoryWithInt);
                response = statusChange.GetLoaReasons();
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Verify if the Delete Status Change is allowed and meets all the required field.
        /// </summary>
        /// <param name="input">The Status Change Id that will be deleted.</param>
        /// <returns>
        ///  An object of the <see cref="Response&lt;SystemStatusChangeModel&gt;"/> class indicating if the Status Change is allowed. 
        /// </returns>
        [HttpGet]
        [Route("GetValidateDeleteSequence")]
        [HttpGetBindJsonQueryString(typeof(SystemStatusChangeModel), "input")]
        public Response<SystemStatusChangeModel> GetValidateDeleteSequence(SystemStatusChangeModel input)
        {
            Response<SystemStatusChangeModel> response = new Response<SystemStatusChangeModel>();
            try
            {
                if (input != null)
                {
                    SystemStatusHistory statusChange = new SystemStatusHistory(this.repository, this.repositoryWithInt);
                    response = statusChange.ValidateDeleteSequence(input);
                }
                else
                {
                    response.ResponseCode = (int)HttpStatusCode.BadRequest;
                    response.ResponseMessage = "Input is a required field!";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)HttpStatusCode.InternalServerError;
                response.ResponseMessage = ex.Message;
            }

            return response;
        }
    }
}
