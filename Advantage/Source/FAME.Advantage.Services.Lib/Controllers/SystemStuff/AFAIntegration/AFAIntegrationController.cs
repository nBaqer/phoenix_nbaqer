﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AFAIntegrationController.cs" company="FAME inc">
//   2018
// </copyright>
// <summary>
//   Defines the AFAIntegrationController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.AFAIntegration
{
    using System.Web.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;

    /// <summary>
    /// The AFA integration controller. 
    /// This will contain all the required logic behind the AFA integration
    /// </summary>
    [RoutePrefix("api/SystemStuff/AFAIntegration/AFAIntegration")]
    public class AFAIntegrationController : ApiController
    {
        /// <summary>
        /// The repository type Id.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryTypeId;

        /// <summary>
        /// The get.
        /// </summary>
        [HttpGet]
        [Route("Get")]
        public void Get()
        {
        }
    }
}
