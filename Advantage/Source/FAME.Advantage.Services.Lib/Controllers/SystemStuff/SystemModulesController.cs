﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Domain.SystemStuff.Resources;
using FAME.Advantage.Messages.SystemStuff.SystemModules;

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff
{
    [RoutePrefix("api/SystemStuff/Modules/{moduleId}")]
    public class SystemModulesController : ApiController
    {
        private readonly IRepositoryWithTypedID<int> _repository;

        public SystemModulesController(IRepositoryWithTypedID<int> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<SystemModuleOutputModel> Get()
        {
            var systemModules = _repository.Query<SystemModule>().ToArray();
            return Mapper.Map<IEnumerable<SystemModuleOutputModel>>(systemModules);
        }
    }
}
