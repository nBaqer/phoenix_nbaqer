﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionController.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Controllers.SystemStuff.InstitutionController
// </copyright>
// <summary>
//   The institution controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff
{
    using System;
    using System.Collections.Generic;
    using System.Web.Http;

    using AdvantageV1.Common;

    using BusinessLogic.SystemStuff;

    using Domain.Infrastructure.Entities;

    using Messages.SystemStuff.Institution;
    using Messages.Web;

    /// <summary>
    /// The institution controller.
    /// </summary>
    [RoutePrefix("api/System/Institution")]
    public class InstitutionController : ApiController
    {
        #region Class Private members

        /// <summary>
        /// The NHibernate repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The repository with type id.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithTypeId;

        /// <summary>
        /// The institution business object.
        /// </summary>
        private readonly InstitutionBo institutionBo;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionController"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="repositoryWithTypeId">
        /// The repository with type id.
        /// </param>
        public InstitutionController(IRepository repository, IRepositoryWithTypedID<int> repositoryWithTypeId)
        {
            this.repository = repository;
            this.repositoryWithTypeId = repositoryWithTypeId;
            this.institutionBo = new InstitutionBo(repository, repositoryWithTypeId);
        }

        /// <summary>
        /// The get institution.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="InstitutionOutputModel"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        [HttpGet]
        [Route("Get")]
        public InstitutionOutputModel Get([FromUri] InstitutionFilterInputModel input)
        {
            try
            {
                return this.institutionBo.Get(input);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The find.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle exception
        /// </exception>
        [HttpGet]
        [Route("Find")]
        public List<InstitutionOutputModel> Find([FromUri] InstitutionFilterInputModel input)
        {
            try
            {
                return this.institutionBo.Find(input);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The find phone.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// An un-handle exception
        /// </exception>
        [HttpGet]
        [Route("FindPhone")]
        public List<InstitutionPhoneModel> FindPhone([FromUri] InstitutionPhoneModel input)
        {
            try
            {
                return this.institutionBo.FindPhone(input);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The find address.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// An un-handle exception
        /// </exception>
        [HttpGet]
        [Route("FindAddress")]
        public List<InstitutionAddressModel> FindAddress([FromUri] InstitutionAddressModel input)
        {
            try
            {
                return this.institutionBo.FindAddress(input);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The find contact.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// An un-handle exception
        /// </exception>
        [HttpGet]
        [Route("FindContact")]
        public List<InstitutionContactModel> FindContact([FromUri] InstitutionContactModel input)
        {
            try
            {
                return this.institutionBo.FindContact(input);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// The post institution phone.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        [HttpPost]
        [Route("PostPhone")]
        public Response<string> PostPhone([FromBody] InstitutionPhoneModel input)
        {
            return this.institutionBo.InsertPhone(input);
        }

        /// <summary>
        /// The put institution phone.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        [HttpPut]
        [Route("PutPhone")]
        public Response<string> PutPhone([FromBody] InstitutionPhoneModel input)
        {
            return this.institutionBo.UpdatePhone(input);
        }

        /// <summary>
        /// The delete institution phone.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        [HttpDelete]
        [Route("DeletePhone")]
        public Response<string> DeletePhone([FromUri] InstitutionPhoneModel input)
        {
            return this.institutionBo.DeletePhone(input);
        }


        /// <summary>
        /// The post institution address.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        [HttpPost]
        [Route("PostAddress")]
        public Response<string> PostAddress([FromBody] InstitutionAddressModel input)
        {
            return this.institutionBo.InsertAddress(input);
        }

        /// <summary>
        /// The put institution address.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        [HttpPut]
        [Route("PutAddress")]
        public Response<string> PutAddress([FromBody] InstitutionAddressModel input)
        {
            return this.institutionBo.UpdateAddress(input);
        }

        /// <summary>
        /// The delete institution address.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        [HttpDelete]
        [Route("DeleteAddress")]
        public Response<string> DeleteAddress([FromUri] InstitutionAddressModel input)
        {
            return this.institutionBo.DeleteAddress(input);
        }


        /// <summary>
        /// The post institution contact.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        [HttpPost]
        [Route("PostContact")]
        public Response<string> PostContact([FromBody] InstitutionContactModel input)
        {
            return this.institutionBo.InsertContact(input);
        }

        /// <summary>
        /// The put institution contact.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        [HttpPut]
        [Route("PutContact")]
        public Response<string> PutContact([FromBody] InstitutionContactModel input)
        {
            return this.institutionBo.UpdateContact(input);
        }

        /// <summary>
        /// The delete institution contact.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        [HttpDelete]
        [Route("DeleteContact")]
        public Response<string> DeleteContact([FromUri] InstitutionContactModel input)
        {
            return this.institutionBo.DeleteContact(input);
        }

        /// <summary>
        /// The get exist by import type (call this to get a flag whether they are institutions with the given TypeId passed in the input).
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        [HttpGet]
        [Route("GetExistByImportType")]
        public bool GetExistByImportType([FromUri] InstitutionFilterInputModel input)
        {
            return this.institutionBo.ExistByImportType(input);
        }

        /// <summary>
        /// The get exist by institution name (call this to get a flag whether they are institutions with the given name passed in the input).
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        [HttpGet]
        [Route("GetExistByName")]
        public bool GetExistByName([FromUri] InstitutionFilterInputModel input)
        {
            return this.institutionBo.GetExistByName(input);
        }
    }
}
