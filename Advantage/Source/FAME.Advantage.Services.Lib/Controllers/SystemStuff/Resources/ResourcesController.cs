﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.SystemStuff.Resources;
using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Resources;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.Resources
{
    /// <summary>
    /// Controller for resources
    /// </summary>
    public class ResourcesController: ApiController
    {
        private readonly IRepository repository;
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;
      
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="repositoryWithInt"></param>
        public ResourcesController(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            this.repositoryWithInt = repositoryWithInt;

        }

        ///// <summary>
        ///// Get a setting from application setting.
        ///// </summary>
        ///// <param name="filter">
        ///// PageResourceId: The page resource ID. The resource are organized by page.you need to put here the resource id of the page
        ///// Use 170 for Lead Info -- 206 also get the info.
        ///// CodeLang: The standard code id for country language. Actually only USA (EN-US) if empty the method assume English.
        ///// </param>
        ///// <returns>
        ///// A list of CaptionRequiredOutputModel
        /////  Caption: string, FldName: string , Required: boolean 
        ///// </returns>
        //[HttpGet]
        //public IList<CaptionRequiredOutputModel> Get([FromUri]CaptionRequiredInputModel filter)
        //{
        //    if (filter == null) throw new HttpBadRequestResponseException("The filter is required");
        //    if (filter.PageResourceId == 0) throw new HttpBadRequestResponseException("Page Resource Id is required");
        //    if (string.IsNullOrEmpty(filter.CodeLang)) filter.CodeLang = "EN-US"; 
        //    try
        //    {
        //        var bo = new ResourcesBo(repository, repositoryWithInt);
        //        var list = bo.GetCaptionAndRequiredList(filter);
        //        return list;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
        //    }
        //}
    }
}