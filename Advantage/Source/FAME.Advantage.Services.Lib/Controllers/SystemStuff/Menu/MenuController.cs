﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MenuController.cs" company="FAME">
//  2015
// </copyright>
// <summary>
//   Defines the MenuController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff.Menu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    using AutoMapper;

    using Domain.Infrastructure.Entities;
    using Domain.SystemStuff.Menu;

    using FAME.Advantage.Services.Lib.Utils;

    using Messages.SystemStuff.Menu;
    using Services.Lib.BusinessLogic.SystemStuff;
    using Services.Lib.Infrastructure.Exceptions;
    using Services.Lib.Infrastructure.Extensions;

    /// <summary>
    /// The menu controller.
    /// </summary>
    [RoutePrefix("api/SystemStuff/Menu")]
    public class MenuController : ApiController
    { 
        /// <summary>
        /// repository object that represents data store.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repository;

        /// <summary>
        /// The repository with GUID.
        /// </summary>
        private readonly IRepositoryWithTypedID<Guid> repositoryWithGuid;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuController"/> class. 
        /// Constructor - initialize the repository
        /// </summary>
        /// <param name="repository">
        /// Repository object that represents data store.
        /// </param>
        /// <param name="repositoryWithGuid">
        /// The repository with GUID.
        /// </param>
        public MenuController(IRepositoryWithTypedID<int> repository, IRepository repositoryWithGuid)
        {
            this.repository = repository;
            this.repositoryWithGuid = repositoryWithGuid;
        }

        /// <summary>
        /// Get the top level modules for the menu
        /// </summary>
        /// <param name="filter">
        /// The filter
        /// </param>
        /// <returns>
        /// The modules
        /// </returns>
        [HttpGet]
        [Route("GetModules")]
        public IEnumerable<MenuModulesOutputModel> GetModules([FromUri] GetMenuItemInputModel filter = null)
        {
           // get module menu items
            var menuItems = this.repository.Query<MenuItem>().Where(n => n.ParentId == null);
           
            // return module output object
            return Mapper.Map<IEnumerable<MenuModulesOutputModel>>(menuItems.ToArray().OrderBy(n => n.DisplayOrder));
        }

        /// <summary>
        /// Get the second level menu items 
        /// </summary>
        /// <param name="menuName">the module menu item selected, the parent menu item</param>
        /// <param name="filter">The filters</param>
        /// <returns>Return the Menu modules</returns>
        [HttpGet]
        [Route("~/api/SystemStuff/{menuName}/Menu")]
        public IEnumerable<MenuModulesOutputModel> GetSubLevelItems(string menuName = null, [FromUri] GetMenuItemInputModel filter = null)
        {
            // validate parameters and throw exception if not passed in
            if (menuName == null)
            {
                throw new HttpBadRequestResponseException("menu name is required");
            }

            if (filter == null || filter.CampusId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("campus Id is required");
            }

            // get the parent menu id for this menu item
            var bo = new MasterMenu(this.repository, this.repositoryWithGuid);
            var subLevelMenuItems = bo.GetSubLevelMenuItems(menuName, filter);
            
            // return menu module output object
            return Mapper.Map<IEnumerable<MenuModulesOutputModel>>(subLevelMenuItems.ToArray().OrderBy(n => n.DisplayOrder)); 
        } 

        /// <summary>
        /// Get the second level menu items when the second menu has links 
        /// </summary>
        /// <param name="filter">The filters</param>
        /// <returns>The menu items Output Model with the link</returns>
        [HttpGet]
        [Route("~/api/SystemStuff/GetSubLevelItemsWithLinks")]
        public IEnumerable<MenuItemsOutputModel> GetSubLevelItemsWithLinks([FromUri] GetMenuItemInputModel filter = null)
        {
            // validate parameters and throw exception if not passed in
            if (filter == null || filter.CampusId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("campus Id is required");
            }

            if (filter.MenuWithLinks == null)
            {
                throw new HttpBadRequestResponseException("menu name is required");
            }

            if (filter.UserId == null)
            {
                throw new HttpBadRequestResponseException("UserId is required");
            }

            // get the parent menu id for this menu item
            var bo = new MasterMenu(this.repository, this.repositoryWithGuid);
            var subLevelMenuItems = bo.GetSubLevelMenuItemsWithLinks(filter.MenuWithLinks, filter);
           
            // return menu module output object
            return Mapper.Map<IEnumerable<MenuItemsOutputModel>>(subLevelMenuItems.ToArray().OrderBy(n => n.DisplayOrder));
        }

        /// <summary>
        /// Get the Page Group and Pages level items
        /// </summary>
        /// <param name="moduleName">
        /// the top level menu item</param>
        /// <param name="subMenuName">
        /// the sub level menu item</param>
        /// <param name="filter">
        /// The filter</param>
        /// <returns>
        /// The menu items
        /// </returns>
        [HttpGet]
        [Route("~/api/SystemStuff/{moduleName}/Menu/{subMenuName}")]
        public IEnumerable<MenuItemsOutputModel> GetPageGroupAndPages(string moduleName = null, string subMenuName = null, [FromUri] GetMenuItemInputModel filter = null)
        {
            // Patch to avoid router conflict
            if (!string.IsNullOrEmpty(moduleName))
            {
                if (moduleName == "Maintenance1")
                {
                    moduleName = "Maintenance";
                }
            }

            // validate parameters and throw exception if not passed in
            if (string.IsNullOrEmpty(moduleName) || string.IsNullOrEmpty(subMenuName))
            {
                throw new HttpBadRequestResponseException("Menu Name is required");
            }

            if (filter == null || filter.CampusId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("Campus Id is required");
            }

            if (filter == null || filter.UserId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("User Id is required");
            }

            var bo = new MasterMenu(this.repository, this.repositoryWithGuid);
            var subLevelMenuItems = bo.SubLevelMenuItems(moduleName, subMenuName, filter);

            // TODO This code is provisional to hidden Skill from Menu. Later should be decided removed or repair the page
            if (moduleName == "Maintenance")
            {
                foreach (MenuItem item in subLevelMenuItems)
                {
                    if (item.ResourceId == 401)
                    {
                        MenuItem skill = null;
                        foreach (MenuItem menuItem in item.ChildrenForOutput)
                        {
                            if (menuItem.ResourceId == 28)
                            {
                                skill = menuItem;
                                break;
                            }
                        }

                        if (skill != null)
                        {
                            item.ChildMenuItems.Remove(skill);
                            item.ChildrenForOutput.Remove(skill);
                        }

                        break;
                    }
                }
            }
            //// TODO END Provisional code
            var menuItems = Mapper.Map<IEnumerable<MenuItemsOutputModel>>(subLevelMenuItems.ToArray().OrderBy(n => n.DisplayOrder));

            //// To Handle the TrackSapAttendance Key changes from configuration.
            if (moduleName == Constants.TrackSapAttendance.ModuleName && subMenuName == Constants.TrackSapAttendance.CommonTasks)
            {
                var trackSapAttendance = bo.GetConfigAppSettingValue(Constants.TrackSapAttendance.TrackSapAttendanceKeyName, filter.CampusId);
                if (trackSapAttendance == Constants.TrackSapAttendance.AttendnaceByDay)
                {
                    menuItems = menuItems.ForEach(
                        menu =>
                            {
                                menu.items = menu.items.Where(x => x.text != Constants.TrackSapAttendance.EnterClassSectionAttendance)
                                    .ToList();
                            });
                }
                else
                {
                    menuItems = menuItems.ForEach(
                        menu =>
                            {
                                menu.items = menu.items.Where(x => x.text != Constants.TrackSapAttendance.PostAttendance)
                                    .ToList();
                            });
                }
            } 

            // return menu item output object
            return menuItems;
        }

        /// <summary>
        /// To get is page enabled for user.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> page is enabled or not.
        /// </returns>
        [HttpGet]
        [Route("GetIsPageEnabledForUser")]
        public bool GetIsPageEnabledForUser([FromUri] GetMenuItemInputModel filter = null)
        {
            if (filter == null || filter.CampusId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("Campus Id is required");
            }

            if (filter == null || filter.UserId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("User Id is required");
            }

            if (filter == null || string.IsNullOrEmpty(filter.PageName))
            {
                throw new HttpBadRequestResponseException("Page Name is required");
            }

            var bo = new MasterMenu(this.repository, this.repositoryWithGuid);
            bool result = bo.IsPageEnabledForUser(filter);
            return result;
        }
 }
}