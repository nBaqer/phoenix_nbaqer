﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UniversalSearchController.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Controllers.SystemStuff.UniversalSearchController
// </copyright>
// <summary>
//   The universal search controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.Common;
    using FAME.Advantage.Messages.SystemStuff.UniversalSearch;
    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff;

    /// <summary>
    /// The universal search controller.
    /// </summary>
    /// 
    [RoutePrefix("api/SystemStuff/UniversalSearch")]
    public class UniversalSearchController : ApiController
    {

        #region Class Private members

        /// <summary>
        /// The NHibernate repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The repository with type id.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithTypeId;

        /// <summary>
        /// The institution business object.
        /// </summary>
        private readonly UniversalSearchBo universalSearchBo;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UniversalSearchController"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="repositoryWithTypeId">
        /// The repository with type id.
        /// </param>
        public UniversalSearchController(IRepository repository, IRepositoryWithTypedID<int> repositoryWithTypeId)
        {
            this.repository = repository;
            this.repositoryWithTypeId = repositoryWithTypeId;
            this.universalSearchBo = new UniversalSearchBo(repository, repositoryWithTypeId);
        }

        /// <summary>
        /// The get campuses.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        [HttpGet]
        [Route("GetCampuses")]
        public IEnumerable<DropDownOutputModel> GetCampuses([FromUri]FilterUniversalSearchInput input)
        {
            return this.universalSearchBo.GetCampuses(input);
        }

        /// <summary>
        /// The get lead status.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        [HttpGet]
        [Route("GetLeadStatus")]
        public IEnumerable<DropDownOutputModel> GetLeadStatus([FromUri]FilterUniversalSearchInput input)
        {
            return this.universalSearchBo.GetLeadStatus(input);
        }

        /// <summary>
        /// The get admin reps.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        [HttpGet]
        [Route("GetAdminReps")]
        public IEnumerable<DropDownOutputModel> GetAdminReps([FromUri]FilterUniversalSearchInput input)
        {
            return this.universalSearchBo.GetAdminReps(input);
        }
    }
}
