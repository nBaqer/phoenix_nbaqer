﻿using System.Linq;
using System.Web.Http;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings;
using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff
{
    [RoutePrefix("api/SystemStuff/ConfigurationAppSettings/{keyName}")]
    public class ConfigurationAppSettingsController : ApiController
    {
        private readonly IRepositoryWithTypedID<int> repository;

        public ConfigurationAppSettingsController(IRepositoryWithTypedID<int> repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get a setting from application setting.
        /// </summary>
        /// <param name="keyName"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public string Get(string keyName = null)
        {
            if (string.IsNullOrEmpty(keyName)) throw new HttpBadRequestResponseException("Key Name is required");

            var s = ConfigurationAppSettings.Setting(keyName, repository);
            if (s != null) return s;

            throw new HttpNotFoundResponseException("The Key Name does not exists in config settings");
        }
    }
}
