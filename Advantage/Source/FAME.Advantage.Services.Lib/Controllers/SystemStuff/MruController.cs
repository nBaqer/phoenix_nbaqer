﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.SystemStuff.Mru;
using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff
{
    /// <summary>
    /// Controller for operation with the MRU
    /// </summary>
    [RoutePrefix("api/SystemStuff/LeadStuffRouter/Mru")]
    public class MruController : ApiController
    {
        private readonly MruBo bo;
         // ReSharper disable NotAccessedField.Local
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;
        private readonly IRepository repository;
        // ReSharper restore NotAccessedField.Local


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="leadRepository">repository guid</param>
        /// <param name="repositoryWithInt">repository int</param>
        public MruController(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            repository = leadRepository;
            //request = HttpContext.Current.Request;
            this.repositoryWithInt = repositoryWithInt;
            bo = new MruBo(repositoryWithInt, leadRepository);
        }


        /// <summary>
        /// Post a Lead to MRU list.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// <exception cref="HttpBadRequestResponseException"></exception>
        [HttpPost]
        [Route("Post")]
        public HttpResponseMessage Post([FromUri] MruInputModel input)
        {
            if (input == null) throw new HttpBadRequestResponseException("Input can not be null");
            if (input.EntityId == null) throw new HttpBadRequestResponseException("No Entity Ids found");
            if (input.CampusId == null) throw new HttpBadRequestResponseException("No Campus Ids found");
            if (input.UserId == null) throw new HttpBadRequestResponseException("No User Ids found");
            try
            {
                // Insert the entity
                bo.InsertEntityInMru(input);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + Environment.NewLine + ex.Message);
            }
        }
    }
}
