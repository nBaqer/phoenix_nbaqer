﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http.Filters;
using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Messages.SystemStuff.UserImpersonationLog;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using System.Collections.Generic;
using System.Web.Http;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;
using FAME.Advantage.Services.Lib.Infrastructure.ResponseMessages;


namespace FAME.Advantage.Services.Lib.Controllers.SystemStuff
{
    [RoutePrefix("api/SystemStuff/UserImpersonationLogs")]
    public class UserImpersonationLogController : ApiController
    {
        private readonly IRepositoryWithTypedID<int> _repository;

        public UserImpersonationLogController(IRepositoryWithTypedID<int> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<UserImpersonationLogOutputModel> Get([FromUri] GetUserImpersonationLogInputModel filter)
        {
            if (filter == null) throw new HttpBadRequestResponseException("filter cannot be null");
            if (string.IsNullOrEmpty(filter.StartDate) || string.IsNullOrEmpty(filter.EndDate)) throw new HttpBadRequestResponseException("Start and/or end dates are invalid");

            DateTime lStart;
            DateTime lEnd;

            DateTime.TryParse(filter.StartDate, out lStart);
            DateTime.TryParse(filter.EndDate, out lEnd);

            lEnd =lEnd.AddDays(1);

            var impersonationLog = _repository.Query<UserImpersonationLog>().Where(n=>n.LogStart >= lStart && n.LogStart < lEnd);
            return Mapper.Map<IEnumerable<UserImpersonationLogOutputModel>>(impersonationLog.OrderByDescending(n=>n.LogStart));
        }


        [HttpPost]
        [Route("NewLog")]
        public HttpResponseMessage Post(  PutUserImpersonationLogInputModel input)
        {
            var impLog = new UserImpersonationLog(input.ImpersonatedUser, DateTime.Now, null);

            _repository.Save(impLog);

            var output = Mapper.Map<UserImpersonationLogOutputModel>(impLog);
            return new HttpCreatedResponseMessage<UserImpersonationLogOutputModel>(output, Request.RequestUri, impLog.ID.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Update an existing log entry
        /// PUT: api/SystemStuff/UserImpersonationLogs/{StudentDocumentId}
        /// </summary>
        /// <param name="logId"></param>
        /// <param name="inputModel"></param>
        [HttpPut]
        [Route("~/api/SystemStuff/{logId}/UserImpersonationLogs")]
        public void Put(string logId)
        {
            int id = 0;
            int.TryParse(logId, out id);

           

            var impLog = _repository.Get<UserImpersonationLog>(id);
               
            impLog.SetEndDate(DateTime.Now);
            _repository.Save(impLog);
        }
    }
}
