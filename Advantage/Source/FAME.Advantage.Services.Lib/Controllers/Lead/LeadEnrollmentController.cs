﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEnrollmentController.cs" company="FAMEinc">
//   2016
// </copyright>
// <summary>
//   Defines the LeadEnrollmentController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System.Linq.Expressions;
using Fame.Integration.Adv.Afa.Messages.WebApi.Entities;
using FAME.Advantage.Domain.Campuses;
using FAME.Advantage.Domain.Catalogs;
using FAME.Advantage.Domain.Student.Enrollments;

namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.Lead;
    using FAME.Advantage.Messages.Lead.Enroll;
    using FAME.Advantage.Messages.SystemStuff.Resources;
    using FAME.Advantage.Services.Lib.BusinessLogic.Lead;
    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Resources;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
    using static FAME.Advantage.Services.Lib.Utils.Constants;
    using System.Linq;
    using FAME.AFA.Api.Library.AcademicRecords;
    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings;
    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff;
    using FAME.Advantage.Domain.Lead;

    /// <summary>
    /// The lead enrollment controller.
    /// </summary>
    [RoutePrefix("api/Lead/LeadEnrollment")]
    public class LeadEnrollmentController : ApiController
    {
        #region Class Private members

        /// <summary>
        /// The lead repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        /// The Business Object.
        /// </summary>
        private readonly LeadEnrollmentBo bo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadEnrollmentController"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="repositoryWithInt">
        /// The repository with integer.
        /// </param>
        public LeadEnrollmentController(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            this.repositoryWithInt = repositoryWithInt;
            this.bo = new LeadEnrollmentBo(repository, repositoryWithInt);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// The get caption and required list.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The list of data source of the dropdowns 
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Displays the error message
        /// </exception>
        /// <exception cref="HttpResponseException">
        /// Displays the error message
        /// </exception>
        [HttpGet]
        [Route("GetCaptionAndRequiredList")]
        public IList<ResourcesRequiredAndDllOutputModel> GetCaptionAndRequiredList([FromUri]LeadInputModel filter)
        {
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("Filter can not be null");
            }

            if (filter.PageResourceId == 0)
            {
                throw new HttpBadRequestResponseException("The Page Resource Id can not be null");
            }

            if (filter.CampusId == null)
            {
                throw new HttpBadRequestResponseException("No Campus Ids found");
            }

            try
            {
                var resourceBo = new ResourcesBo(this.repository, this.repositoryWithInt);
                return resourceBo.GetCaptionAndRequiredList(filter);
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// The get required ipeds list.
        /// </summary>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        /// <exception cref="HttpResponseException">
        /// the exception
        /// </exception>
        [HttpGet]
        [Route("GetRequiredIpedsList")]
        public IList<ResourcesRequiredAndDllOutputModel> GetRequiredIpedsList()
        {
            try
            {
                var resourceBo = new ResourcesBo(this.repository, this.repositoryWithInt);
                return resourceBo.GetRequiredIpedsList();
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// The generate student id sequence.
        /// </summary>
        /// <param name="stuId">
        /// The stu id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="HttpResponseException">
        /// throw exception
        /// </exception>
        [HttpGet]
        [Route("GenerateStudentIdSeq")]
        public int GenerateStudentIdSeq([FromUri]int stuId)
        {
            try
            {
                return this.bo.GenerateStudentIdSeq(stuId);
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// The generate student id.
        /// </summary>
        /// <param name="firstName">
        /// The first Name.
        /// </param>
        /// <param name="lastName">
        /// The last Name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="HttpResponseException">
        ///  throw error
        /// </exception>
        [HttpGet]
        [Route("GenerateStudentId")]
        public string GenerateStudentId([FromUri] string firstName, string lastName)
        {
            try
            {
                return this.bo.GenerateStudentId(firstName, lastName);
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// The generate enroll number.
        /// </summary>
        /// <param name="firstName">
        /// The first name.
        /// </param>
        /// <param name="lastName">
        /// The last name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="HttpResponseException">
        /// throw error
        /// </exception>
        [HttpGet]
        [Route("GenerateEnrollNumber")]
        public string GenerateEnrollNumber([FromUri] string firstName, string lastName)
        {
            try
            {
                return this.bo.GenerateEnrollNumber(firstName, lastName);
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// The get lead data.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="EnrollOutputModel"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Displays the error message
        /// </exception>
        /// <exception cref="HttpResponseException">
        /// Displays the error message
        /// </exception>
        [HttpGet]
        [Route("GetLeadData")]
        public EnrollOutputModel GetLeadData([FromUri] LeadInputModel filter)
        {
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("Filter can not be null");
            }

            if (filter.CampusId == null)
            {
                throw new HttpBadRequestResponseException("No Campus Ids found");
            }

            if (filter.LeadId == null)
            {
                throw new HttpBadRequestResponseException("No Lead Ids found");
            }

            try
            {
                var result = this.bo.GetLeadData(filter);
                return result;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// The enroll lead.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="HttpResponseException">
        /// throw the exception
        /// </exception>
        [HttpPost]
        [Route("EnrollLead")]
        public string EnrollLead([FromBody] EnrollOutputModel input)
        {
            try
            {
                var result = this.bo.EnrollLead(input);

                if (result.ToLower().Equals("enrolled"))
                {
                    // Notify AFA of student enrollment
                    var wapit =
                        this.repositoryWithInt.Query<SyWapiSettings>()
                            .SingleOrDefault(x => x.CodeOperation == WapiCodes.AFA_INTEGRATION);
                    if (wapit == null)
                    {
                        throw new ApplicationException("Please configure " + WapiCodes.AFA_INTEGRATION + " External Operation");
                    }

                    // Get the enable afa integration value for campus
                    var configValuesList = ConfigurationAppSettings.SettingByCampus(ConfigurationAppSettingsKeys.EnableAFAIntegration, this.repositoryWithInt);
                    //get campus specific value
                    var afaIntegrationForCampus = configValuesList.FirstOrDefault(cv => cv.Campus != null && cv.Campus.ID.ToString() == input.LeadInfo.CampusId);
                    //if no campus specific value, get the value for all campuses
                    if (afaIntegrationForCampus == null)
                    {
                        afaIntegrationForCampus = configValuesList.FirstOrDefault(cv => cv.Campus == null);
                    }

                    if (afaIntegrationForCampus == null) return result;

                    if (afaIntegrationForCampus.Value.Equals("yes", StringComparison.InvariantCultureIgnoreCase))
                    {

                        /*IF Afa Is Enabled*/
                        EnrollmentRequest enrollmentRequest = new EnrollmentRequest(wapit.ConsumerKey, wapit.UserName, wapit.PrivateKey, wapit.ExternalUrl);
                        var leadId = Guid.Parse(input.LeadInfo.LeadId);
                        var campusId = Guid.Parse(input.LeadInfo.CampusId);
                        var prgVerId = Guid.Parse(input.LeadInfo.PrgVerId);
                        var adminCriteriaId = Guid.Parse(input.LeadInfo.AdminCriteriaId);
                        var date = DateTime.Now;
                        var lead = this.repository.Query<Domain.Lead.Lead>()
                                .FirstOrDefault(x => x.ID == leadId);
                        var idStudent = lead?.AfaStudentId;
                        var studentNumber = lead?.StudentNumber;

                        var cmsId = this.repository.Query<Campus>()
                            .FirstOrDefault(x => x.ID == campusId)?.CmsId;

                        var newEnrollment = this.repository.Query<Enrollment>()
                            .FirstOrDefault(enr =>
                                enr.ProgramVersion != null && enr.ProgramVersion.ID == prgVerId && enr.CampusId == campusId &&
                                enr.LeadObj != null && enr.LeadObj.ID == leadId);

                        var enrollmentId = newEnrollment?.ID;

                        var programName = this.repository.Query<ProgramVersion>()
                            .FirstOrDefault(pv =>
                                pv.ID == prgVerId)
                            ?.Description;
                        var adminCriteria = this.repository.Query<AdminCriteria>()
                           .FirstOrDefault(ac =>
                               ac.ID == adminCriteriaId)
                           ?.Code;

                        var schedHours = (from s in this.repository.Query<Schedules>()
                            join ps in this.repository.Query<ProgramSchedule>() on s.ScheduleId equals ps.ID
                            join psd in this.repository.Query<ProgramScheduleDetails>() on ps.ID
                                equals psd.ProgramScheduleObj.ID
                            where s.StuEnrollId == enrollmentId && ps.Active
                            select psd).Sum(x => x.Total);

                        if (enrollmentId.HasValue && !string.IsNullOrEmpty(idStudent) && !string.IsNullOrEmpty(cmsId) && !string.IsNullOrEmpty(programName) && long.TryParse(idStudent, out var longResultStudentId))
                        {
                            var enrollment = new AdvStagingEnrollmentV3()
                            {
                                StatusEffectiveDate = newEnrollment.EnrollmentDate,
                                EnrollmentStatus = newEnrollment.EnrollmentStatus.SystemStatus.ID.ToString(),
                                StartDate = input.StartDate,
                                GradDate = input.ContractedGradDate,
                                ProgramVersionID = prgVerId,
                                ProgramName = programName,
                                SISEnrollmentID = enrollmentId.Value,
                                IDStudent = longResultStudentId,
                                SSN = input.LeadInfo.SSN,
                                AdmissionsCriteria = adminCriteria,
                                LocationCMSID = cmsId,
                                DateCreated = date,
                                UserCreated = input.LeadInfo.ModUser,
                                UserUpdated = input.LeadInfo.ModUser,
                                DateUpdated = date,
                                AdvStudentNumber = studentNumber,
                                HoursPerWeek = schedHours,
                                PriorSchoolHours = newEnrollment.TransferHours,
                                InSchoolTransferHours = newEnrollment.TotalTransferHoursFromThisSchool
                            };
                            enrollmentRequest.SendEnrollmentToAFA(new List<AdvStagingEnrollmentV3>() { enrollment });
                            /*
                                 * Check with PO where are we going to add an action to retry enrolling an existing advantage enrollment into afa if this step failed.
                                 */
                        }
                    }


                }

                return result;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        [HttpPost]
        [Route("ServerSideValidation")]
        public string ServerSideValidation([FromBody] EnrollOutputModel input)
        {
            try
            {
                return this.bo.ServerSideValidation(input);
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }
        #endregion
    }
}

