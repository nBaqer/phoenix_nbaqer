﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadsController.cs" company="FAME">
//   2005,2016
// </copyright>
// <summary>
//   Lead Controller Class. Hold operation with student leads.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web;
    using System.Web.Http;
    using System.Web.UI;

    using BusinessLogic.Lead;
    using BusinessLogic.SystemStuff.Resources;

    using Domain.Infrastructure.Entities;
    using Domain.Users;

    using FAME.Advantage.Messages.Common;
    using FAME.Advantage.Services.Lib.BusinessLogic.Catalogs;

    using Infrastructure.Exceptions;
    using Infrastructure.Extensions;

    using Messages.Lead;
    using Messages.MostRecentlyUsed;
    using Messages.SystemStuff.SchoolDefinedFields;
    using Messages.Web;

    using NHibernate.Util;

    /// <summary>
    /// Lead Controller Class. Hold operation with student leads.
    /// </summary>
    [RoutePrefix("api/Lead")]
    public class LeadsController : ApiController
    {
        #region Class Private members

        /// <summary>
        ///  The request.
        /// </summary>
        private readonly HttpRequest request;

        /// <summary>
        /// The lead repository.
        /// </summary>
        private readonly IRepository leadRepository;

        /// <summary>
        /// The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        /// The Business Object.
        /// </summary>
        private readonly LeadBo bo;

        private readonly CatalogsBo catalogBo;

        #endregion

        #region constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadsController"/> class. 
        /// constructor
        /// </summary>
        /// <param name="leadRepository">
        /// Repository with GUID for Lead Table
        /// </param>
        /// <param name="repositoryWithInt">
        /// The repository with integer
        /// </param>
        public LeadsController(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.request = HttpContext.Current.Request;
            this.leadRepository = leadRepository;
            this.repositoryWithInt = repositoryWithInt;
            this.bo = new LeadBo(leadRepository, repositoryWithInt);
            this.catalogBo = new CatalogsBo(leadRepository,repositoryWithInt);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get the Lead Info to be show in Lead Info Bar.
        /// </summary>
        /// <param name="filter">Get Lead Input Filter</param>
        /// <returns>
        /// Lead InfoBar Output Model
        /// </returns>
        [HttpGet]
        [Route("~/api/Lead/Leads/GetLeadInfoBar")]
        public LeadInfoBarOutputModel GetLeadInfoBar([FromUri] GetLeadInputModel filter)
        {
            // Test the filter
            if (filter == null || filter.LeadGuid == null)
            {
                throw new HttpBadRequestResponseException("Filter has not value");
            }

            try
            {
                LeadInfoBarOutputModel lead = this.bo.GetLeadInfo(filter);
                return lead;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Get data for single lead and filtered by items in filter object
        /// //GET: 
        /// <code>
        /// API/Students/{studentId}
        /// </code>
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// Lead output Model
        /// </returns>
        [HttpGet]
        [Route("~/api/LeadSearch/GetById")]
        public LeadOutputModel GetById([FromUri] GetLeadInputModel filter)
        {
            // throw exception if no search term passed in
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("filter object required");
            }

            if (filter.LeadGuid.IsEmpty())
            {
                throw new HttpBadRequestResponseException("LeadGuid is required");
            }

            return this.bo.GetLead(filter.LeadGuid);
        }

        /// <summary>
        /// Get data for single lead and filtered by items in filter object
        /// //GET: <code>api/Students/{studentId}</code>
        /// </summary>
        /// <returns>
        /// The Search Output Model
        /// </returns>
        [HttpGet]
        [Route("~/api/LeadsSearch/GetBySearchTerm")]
        public IEnumerable<LeadSearchOutputModel> GetBySearchTerm()
        {
            // throw exception if no search term passed in
            if (this.request["SearchTerm"] == null)
            {
                throw new HttpBadRequestResponseException("Search Term is required");
            }

            // get the search term from the request object
            var searchTermInit = this.request["SearchTerm"].ToString(CultureInfo.InvariantCulture).Split('|');
            List<Guid> leadStatuses = null;

            if (this.request["LeadStatus"] != null)
            {
                leadStatuses = this.request["LeadStatus"].ToString(CultureInfo.InvariantCulture).Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(x => Guid.Parse(x)).ToList();
            }

            List<Guid> adminReps = null;

            if (this.request["AdmissionRep"] != null)
            {
                adminReps = this.request["AdmissionRep"].ToString(CultureInfo.InvariantCulture).Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(x => Guid.Parse(x)).ToList();
            }

            var output = this.bo.GetLeadSearch(searchTermInit, leadStatuses, adminReps);
            return output;
        }

        /// <summary>
        /// Get data for single student and filtered by items in filter object
        /// <code> GET: api/Students/{studentId}</code>
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The select
        /// </returns>
        [HttpGet]
        [Route("~/api/LeadSearch/GetByMru")]
        public IEnumerable<LeadMruOutputModel> GetByMru([FromUri] GetMruInputModel filter)
        {
            if (filter.UserId.IsEmpty() || filter.CampusId.IsEmpty())
            {
                throw new HttpBadRequestResponseException("CampusId and UserId is required");
            }

            return this.bo.GetLeadMru(filter.UserId, filter.CampusId);
        }

        /// <summary>
        /// Public API method to get leads that are unassigned
        /// </summary>
        /// <param name="filter">
        /// Campus and Campus ID
        /// </param>
        /// <returns>The Lead Output Model</returns>
        [HttpGet]
        [Route("GetByUnassigned")]
        public IEnumerable<LeadOutputModel> GetByUnassigned([FromUri] GetLeadInputModel filter)
        {
            try
            {
                return !filter.CampusId.IsEmpty()
                       ? this.bo.GetLeadsUnassignedToRep(filter.VendorId, filter.CampusId)
                       : this.bo.GetLeadsUnassignedToCampus(filter.VendorId, filter.AreaOfInterest);
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// public method to get leads that are currently already assigned but we want to re-assign
        /// </summary>
        /// <param name="filter">
        /// Last Modification Data, Campus ID, Representative ID
        /// </param>
        /// <returns>
        /// The lead output model
        /// </returns>
        [HttpGet]
        [Route("GetByReassign")]
        public IEnumerable<LeadOutputModel> GetByReassign([FromUri] GetLeadInputModel filter)
        {
            // return GetLeadsUnassignedToCampus(filter.VendorId, filter.CampusOfInterest);
            if (!filter.CampusId.IsEmpty() && filter.RepId.IsEmpty())
            {
                return this.bo.GetLeadsReAssignToCampus(filter.CampusId, filter.LastModDate);
            }

            if (!filter.CampusId.IsEmpty() && !filter.RepId.IsEmpty())
            {
                return this.bo.GetLeadsReAssignToRep(filter.CampusId, filter.RepId, filter.LastModDate);
            }

            throw new HttpBadRequestResponseException("CampusId and RepId combination invalid");
        }

        /// <summary>
        /// public API method to get a list of campuses of interest
        /// </summary>
        /// <returns>
        /// List of campus of interest
        /// </returns>
        [HttpGet]
        [Route("GetAreaOfInterest")]
        public IEnumerable<DropDownWithCampusOutputModel> GetAreaOfInterest()
        {
            return this.catalogBo.GetAllProgramGroupsItems(true).OrderBy(x => x.Description);
        }

        /// <summary>
        /// public method to get the lead counts for lead assignment page
        /// </summary>
        /// <returns>
        /// Leads Assigned Count Output Model
        /// </returns>
        [HttpGet]
        [Route("GetLeadCounts")]
        public LeadsAssignedCountOutputModel GetLeadCounts()
        {
            try
            {
                return this.bo.GetLeadAssignmentCount();
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// The GetLeadContactInfo
        /// </summary>
        /// <param name="filter">
        /// The Lead ID
        /// </param>
        /// <returns>
        /// The Lead Contact Output Model
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Input is required , lead ID is required
        /// </exception>
        [HttpGet]
        [Route("GetLeadContactInfo")]
        public LeadContactsOutputModel GetLeadContactInfo([FromUri] GetLeadInputModel filter)
        {
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("input is required");
            }

            if (filter.LeadGuid.IsEmpty())
            {
                throw new HttpBadRequestResponseException("lead id is required");
            }

            return this.bo.GetLeadContactInfo(filter.LeadGuid);
        }

        /// <summary>
        /// public method to update the assigned leads to a campus or admission rep
        /// </summary>
        /// <param name="input">The input filter</param>
        [HttpPost]
        [Route("~/api/Lead/Leads/UpdateAssignLeads")]
        public void UpdateAssignLeads([FromBody]GetLeadUpdateInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("input object is null");
            }

            if (input.LeadIds == null)
            {
                throw new HttpBadRequestResponseException("No Lead Ids found");
            }

            if (input.UserId == null)
            {
                throw new HttpBadRequestResponseException("No User Id found");
            }

            if (!input.CampusId.IsEmpty() && input.RepId.IsEmpty())
            {
                this.bo.AssignLeadsToCampus(input.LeadIds, input.CampusId, input.UserId);
            }
            else if (input.CampusId.IsEmpty() && !input.RepId.IsEmpty())
            {
                this.bo.AssignLeadsToRep(input.LeadIds, input.RepId, input.UserId);
            }
            else
            {
                throw new HttpBadRequestResponseException("Wrong combination of rep id and campus id");
            }
        }

        /// <summary>
        /// Get Lead Group info.
        /// The information return always with all groups.
        /// Additionally if a lead is selected return what groups the lead exists
        /// if a campus id is entered the groups are filtered by campus group.
        /// </summary>
        /// <param name="input">
        /// input.LeadId = '0' get only the information about the groups
        /// input.LeadId = 'valid lead id' add to the group (<code>IsInGrp</code> = true) indicated that the lead belong to the group.
        /// input.CampusId = '0' get all groups
        /// input.CampusId = 'valid campus id' get only the groups for the indicated campus
        /// !! ALL PARAMETERS ARE MANDATORY!!!!!!! 
        /// </param>
        /// <returns>
        ///  <code>GroupId</code> - The ID of the group 
        /// Description - A description of the group
        ///  <code>IsInGrp</code> - Always False if LeadId = '0' if LeadId is a valid lead return true or false depend if the lead belong to the group.
        /// </returns>
        [HttpGet]
        [Route("GetLeadGroupInformation")]
        public IList<LeadGroupInfoOutputModel> GetLeadGroupInformation([FromUri]LeadInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Filter can not be null");
            }

            if (input.LeadId == null)
            {
                throw new HttpBadRequestResponseException("No Lead Ids found");
            }

            if (input.CampusId == null)
            {
                throw new HttpBadRequestResponseException("No Campus Ids found");
            }

            try
            {
                var list = this.bo.GetLeadGroupInformation(input);
                return list;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Get the custom fields for the given LeadIds
        /// The fields and the value of the fields for the specific lead are returned together.
        /// </summary>
        /// <param name="input">
        /// Lead ID (mandatory) and Campus Id (optional) are the input parameters in filter
        /// </param>
        /// <returns cref="SdfOutputModel">The SDF Output Model</returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Possible Messages: 
        /// - Filter can not be null
        /// - No Lead Ids found
        /// - Service Return : specific cause of error
        /// </exception>
        [HttpGet]
        [Route("GetLeadSchoolCustomFields")]
        public IList<SdfOutputModel> GetLeadSchoolCustomFields([FromUri]LeadInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Filter can not be null");
            }

            if (input.LeadId == null)
            {
                throw new HttpBadRequestResponseException("No Lead Ids found");
            }

            // if (input.CampusId == null) throw new HttpBadRequestResponseException("No Campus Ids found");
            try
            {
                var list = this.bo.GetLeadSchoolCustomFields(input);
                return list;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Get the demographic Information from a Specific Lead.
        /// </summary>
        /// <param name="input">
        /// Lead ID (mandatory) and Campus Id (optional) are the input parameters in filter
        /// Allowed Commands: ALLOWED_STATUS: only the status change allowed is send back
        /// </param>
        /// <returns>
        /// The Lead information
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Possible Messages: 
        /// - Filter can not be null
        /// - No Lead Ids found
        /// - Service Return : specific cause of error
        /// </exception>
        [HttpGet]
        [Route("~/api/Lead/Leads/GetLeadInfoDemographicFields")]
        public LeadInfoPageOutputModel GetLeadInfoDemographicFields([FromUri]LeadInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Filter can not be null");
            }

            try
            {
                switch (input.CommandString)
                {
                    case "ALLOWED_STATUS":
                        {
                            if (input.CampusId == null)
                            {
                                throw new Exception("Filter: Campus can not be null");
                            }

                            if (input.LeadStatusId == null)
                            {
                                throw new Exception("Filter: Actual Lead Status can not be null");
                            }

                            var output = new LeadInfoPageOutputModel
                            {
                                StateChangeIdsList = this.bo.GetLegalStatusChangesIds(input)
                            };
                            return output;
                        }

                    default:
                        {
                            if (input.LeadId == null)
                            {
                                throw new Exception("Filter: No Lead Ids found");
                            }

                            if (input.UserId == null)
                            {
                                throw new Exception("Filter: No User Ids found");
                            }

                            // Get Lead information
                            var lead = this.bo.GetLeadInfoPageInformation(input);

                            input.CampusId = lead.CampusId;
                            input.LeadStatusId = lead.LeadStatusId;
                            lead.StateChangeIdsList = this.bo.GetLegalStatusChangesIds(input);
                            return lead;
                        }
                }
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Return true if the lead is erasable (you can delete it)
        /// </summary>
        /// <param name="input">filter with the LeadID</param>
        /// <returns>true if the lead can be deleted</returns>
        [HttpGet]
        [Route("~/api/Lead/Leads/IsLeadErasable")]
        public bool IsLeadErasable([FromUri] LeadInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Filter can not be null");
            }

            if (input.LeadId == null)
            {
                throw new HttpBadRequestResponseException("No Lead Ids found");
            }

            try
            {
                var itis = this.bo.IsLeadErasable(input);
                return itis;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Use this to get all resources of the page
        /// </summary>
        /// <param name="input">
        /// The lead input Model (Page resource and Campus ID
        /// </param>
        /// <returns>The info Page Resources Output Model</returns>
        [HttpGet]
        [Route("~/api/Lead/Leads/GetInfoPageResources")]
        public LeadInfoPageResourcesOutputModel GetInfoPageResources([FromUri] LeadInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Filter can not be null");
            }

            if (input.PageResourceId == 0)
            {
                throw new HttpBadRequestResponseException("The Page Resource Id can not be null");
            }

            if (input.CampusId == null)
            {
                throw new HttpBadRequestResponseException("No Campus Ids found");
            }

            try
            {
                var output = new LeadInfoPageResourcesOutputModel();
                var bores = new ResourcesBo(this.leadRepository, this.repositoryWithInt);
                var list = bores.GetCaptionAndRequiredList(input);
                output.RequiredList = list;

                // Get School Defined Fields (SDF)
                var listSdf = this.bo.GetLeadSchoolCustomFields(input);
                output.SdfList = listSdf;

                // Get Lead Groups
                var listGrp = this.bo.GetLeadGroupInformation(input);
                output.LeadGroupList = listGrp;

                // Return resources,
                return output;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Return the list of possible status change for a know lead actual status
        /// </summary>
        /// <param name="input">Lead status, Campus and User ID</param>
        /// <returns>The work-flow of status change for Lead Status given</returns>
        [HttpGet]
        [Route("LeadLeadStatusChangesWorkFlow")]
        public IList<string> LeadLeadStatusChangesWorkFlow([FromUri] LeadInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Filter can not be null");
            }

            if (input.LeadStatusId == null)
            {
                throw new HttpBadRequestResponseException("The actual Lead Status can not be null");
            }

            if (input.CampusId == null)
            {
                throw new HttpBadRequestResponseException("The actual Campus Id can not be null");
            }

            if (input.UserId == null)
            {
                throw new HttpBadRequestResponseException("The actual User Can not be null");
            }

            try
            {
                List<string> list = this.bo.GetLegalStatusChangesIds(input);
                return list;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Receive the posted lead info from lead info page.
        /// </summary>
        /// <param name="input">
        /// Lead Info Page Output Model
        /// </param>
        /// <returns>
        /// Update operation:
        /// Return if the lead change the status. The new valid status list to go from the new status
        /// If a validation fail a exception is returned
        /// Insert Operation:
        /// Return a complex object with the information about validation error, duplicate information
        /// The list of new status always return null.
        /// </returns>
        [HttpPost]
        [Route("~/api/Lead/Leads/PostInfoPageLeadFields")]
        public LeadInfoInsertOutputModel PostInfoPageLeadFields([FromBody] LeadInfoPageOutputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Input can not be null");
            }

            if (input.InputModel == null)
            {
                throw new HttpBadRequestResponseException("InputModel can not be null");
            }

            if (input.InputModel.LeadId == null)
            {
                throw new HttpBadRequestResponseException("No Lead Ids found");
            }

            if (input.InputModel.CampusId == null)
            {
                throw new HttpBadRequestResponseException("No Campus Ids found");
            }

            if (input.InputModel.UserId == null)
            {
                throw new HttpBadRequestResponseException("No User Ids found");
            }

            var user = this.leadRepository.Query<User>().SingleOrDefault(x => x.ID.ToString() == input.InputModel.UserId);

            if (user == null)
            {
                throw new HttpUnauthorizedResponseException("The user does not exists!");
            }

            // Complete the modification user fields
            input.ModUser = user.FullName;
            foreach (VehicleOutputModel model in input.Vehicles)
            {
                model.ModUser = user.FullName;
            }

            try
            {
                if (input.InputModel.LeadId == Guid.Empty.ToString())
                {
                    // Insert new Lead
                    var response = this.bo.InsertNewLead(input);
                    return response;
                }

                // Update a lead
                var result = this.bo.UpdateLeadInformation(input);
                var output = new LeadInfoInsertOutputModel { ValidStatusChangeList = result.StateChangeIdsList };
                return output;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Delete the lead withId is send as parameter
        /// </summary>
        /// <param name="leadId">The Lead ID</param>
        /// <returns>Response Message</returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// No Lead ID found, Lead ID incorrect format
        /// </exception>
        [HttpPost]
        [Route("~/api/Lead/Leads/PostLeadDelete")]
        public HttpResponseMessage PostLeadDelete([FromUri] string leadId)
        {
            if (leadId == null)
            {
                throw new HttpBadRequestResponseException("No Lead Ids found");
            }

            Guid gu;
            if (Guid.TryParse(leadId, out gu) == false)
            {
                throw new HttpBadRequestResponseException("Lead ID incorrect format");
            }

            try
            {
                // Delete Lead
                this.bo.DeleteLead(leadId);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Post a new/updated Lead Phone Contact
        /// </summary>
        /// <param name="input">The Lead Phone Contact to be created/updated</param>
        /// <returns>
        /// The Contact Lead Output Model
        /// </returns>
        [HttpPost]
        [Route("PostLeadPhoneContact")]
        public Response<LeadContactsOutputModel> PostLeadPhoneContact(LeadContactInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead Phone Input can't be null");
            }
            else
            {
                Response<LeadContactsOutputModel> response = this.bo.CreateOrUpdateLeadPhone(input);
                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    response.Data = this.bo.GetLeadContactInfo(Guid.Parse(input.LeadId));
                }

                return response;
            }
        }

        /// <summary>
        /// Delete Lead Phone
        /// </summary>
        /// <param name="input">The Lead Phone Contact to be created/updated</param>
        /// <returns>
        /// The Lead Contact Output Model
        /// </returns>
        [HttpPost]
        [Route("DeleteLeadPhoneContact")]
        public Response<LeadContactsOutputModel> DeleteLeadPhoneContact(LeadContactInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead Phone Input can't be null");
            }

            Response<LeadContactsOutputModel> response = this.bo.DeleteLeadPhone(input);

            if (response.ResponseCode == (int)HttpStatusCode.OK)
            {
                response.Data = this.bo.GetLeadContactInfo(Guid.Parse(input.LeadId));
            }

            return response;
        }

        /// <summary>
        /// Post a new/updated Lead Phone Contact
        /// </summary>
        /// <param name="input">The Lead Phone Contact to be created/updated</param>
        /// <returns></returns>
        [HttpPost]
        [Route("PostLeadEmailContact")]
        public Response<LeadContactsOutputModel> PostLeadEmailContact(LeadContactInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead Email Input can't be null");
            }
            else
            {
                Response<LeadContactsOutputModel> response = this.bo.CreateOrUpdateLeadEmail(input);
                if (response.ResponseCode == (int)HttpStatusCode.OK)
                    response.Data = this.bo.GetLeadContactInfo(Guid.Parse(input.LeadId));
                return response;
            }
        }

        /// <summary>
        /// Delete Lead Phone
        /// </summary>
        /// <param name="input">The Lead Phone Contact to be created/updated</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteLeadEmailContact")]
        public Response<LeadContactsOutputModel> DeleteLeadEmailContact(LeadContactInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead Email Input can't be null");
            }
            else
            {
                Response<LeadContactsOutputModel> response = this.bo.DeleteLeadEmail(input);

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                    response.Data = this.bo.GetLeadContactInfo(Guid.Parse(input.LeadId));

                return response;
            }
        }


        /// <summary>
        /// Post a new/updated Lead Address Contact
        /// </summary>
        /// <param name="input">The Lead Phone Contact to be created/updated</param>
        /// <returns></returns>
        [HttpPost]
        [Route("PostLeadAddressContact")]
        public Response<LeadContactsOutputModel> PostLeadAddressContact(LeadAddressInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead Address Input can't be null");
            }
            else
            {
                Response<LeadContactsOutputModel> response = this.bo.CreateOrUpdateLeadAddress(input);
                if (response.ResponseCode == (int)HttpStatusCode.OK)
                    response.Data = this.bo.GetLeadContactInfo(Guid.Parse(input.LeadId));
                return response;
            }
        }

        /// <summary>
        /// Delete Lead Address
        /// </summary>
        /// <param name="input">The Lead Phone Contact to be created/updated</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteLeadAddressContact")]
        public Response<LeadContactsOutputModel> DeleteLeadAddressContact(LeadContactInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead Address Input can't be null");
            }
            else
            {
                Response<LeadContactsOutputModel> response = this.bo.DeleteLeadAdddress(input);

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                    response.Data = this.bo.GetLeadContactInfo(Guid.Parse(input.LeadId));

                return response;
            }
        }

        /// <summary>
        /// Update Lead Comment
        /// </summary>
        /// <param name="input">The Lead Phone Contact to be created/updated</param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateLeadComment")]
        public Response<LeadContactsOutputModel> UpdateLeadComment(LeadContactInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead can't be null");
            }
            else
            {
                Response<LeadContactsOutputModel> response = this.bo.UpdateLeadComment(input);

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                    response.Data = this.bo.GetLeadContactInfo(Guid.Parse(input.LeadId));

                return response;
            }
        }

        #endregion
    }
}
