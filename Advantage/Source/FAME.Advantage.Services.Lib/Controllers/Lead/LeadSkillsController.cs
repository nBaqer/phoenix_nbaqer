﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadSkillsController.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Controller for Lead Skills
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Linq;
    using System.Web.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.Common;
    using FAME.Advantage.Messages.Lead.ExtraCurricular;
    using FAME.Advantage.Services.Lib.BusinessLogic.Catalogs;
    using FAME.Advantage.Services.Lib.BusinessLogic.Lead;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    /// Controller for Lead Skills
    /// </summary>
    [RoutePrefix("api/Lead/LeadSkills")]
    public class LeadSkillsController : ApiController
    {
        /// <summary>
        ///  The Business Object for Skill
        /// </summary>
        private readonly LeadSkillBo bo;
        
        // ReSharper disable NotAccessedField.Local
        
        /// <summary>
        ///  The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        ///  The repository.
        /// </summary>
        private readonly IRepository repository;
        // ReSharper restore NotAccessedField.Local

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadSkillsController"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="leadRepository">
        /// repository GUID
        /// </param>
        /// <param name="repositoryWithInt">
        /// repository integer
        /// </param>
        public LeadSkillsController(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = leadRepository;
            
            // request = HttpContext.Current.Request;
            this.repositoryWithInt = repositoryWithInt;
            this.bo = new LeadSkillBo(leadRepository, repositoryWithInt);
        }

        /// <summary>
        /// Get The Skills for the lead...
        /// </summary>
        /// <param name="filter">
        /// The filter parameters.
        /// CampusId = GUID,
        /// LeadId = GUID
        /// Commands =
        ///  1: Get the Skills values
        ///  2: Get the Levels
        ///  3: Get the Groups
        ///  4: Get Levels and Groups
        /// </param>
        /// <returns>
        /// The skills Output Model 
        /// </returns>
        [HttpGet]
        [Route("Get")]
        public IExtraInfoOutputModel Get([FromUri]ExtraInfoInputModel filter)
        {
            if (filter == null || filter.LeadId == null || filter.CampusId == null)
            {
                throw new HttpBadRequestResponseException("Filter has not value");
            }

            try
            {
                var output = ExtraInfoOutputModel.Factory();
                switch (filter.Command)
                {
                    case 1:
                        {
                            // Get the extracurricular values
                            output.ExtraInfoList = this.bo.GetSkillsValues(filter);
                            break;
                        }

                    case 2:
                        {
                            // Get the extracurricular Levels values
                            var catf = new CatalogsInputModel
                            {
                                AdditionalFilter = string.Empty,
                                CampusId = filter.CampusId,
                                FldName = "Levels"
                            };

                            output.LevelsItemsList =
                                CatalogStatic.FilteredDropDownOutputModels(catf, this.repository, this.repositoryWithInt).ToList();
                            break;
                        }

                    case 3:
                        {
                            // Get the extra skills Groups values
                            output.GroupItemsList = this.bo.GetGroupItemsSkillsValues(filter);
                            break;
                        }

                    case 4:
                        {
                            // Get the skills Level and Groups values
                            var catf = new CatalogsInputModel
                            {
                                AdditionalFilter = string.Empty,
                                CampusId = filter.CampusId,
                                FldName = "Levels"
                            };

                            output.LevelsItemsList =
                                CatalogStatic.FilteredDropDownOutputModels(catf, this.repository, this.repositoryWithInt).ToList();
                            output.GroupItemsList = this.bo.GetGroupItemsSkillsValues(filter);
                            break;
                        }

                    default:
                        {
                            throw new Exception("Bad Command");
                        }
                }

                return output;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Update/Delete/Insert Changes in Skills Model
        /// </summary>
        /// <param name="filter">
        /// Command:
        /// 1: Update Skills
        /// 2: Delete Skills
        /// 3: Insert Skills 
        /// </param>
        /// <returns>
        /// the new number
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">Bad Request</exception>
        [HttpPost]
        [Route("Post")]
        public IExtraInfoOutputModel Post([FromBody]ExtraInfoOutputModel filter)
        {
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("Input Parameter can not be null");
            }

            if (filter.Filter == null)
            {
                throw new HttpBadRequestResponseException("Filter can not be null");
            }

            if (string.IsNullOrWhiteSpace(filter.Filter.LeadId))
            {
                throw new HttpBadRequestResponseException("Filter parameter 1, can not be null");
            }

            if (string.IsNullOrWhiteSpace(filter.Filter.UserId))
            {
                throw new HttpBadRequestResponseException("Filter parameter 2, can not be null");
            }

            if (filter.Filter.Command < 1)
            {
                throw new HttpBadRequestResponseException("Filter parameter 3, can not be null");
            }

            try
            {
                var output = ExtraInfoOutputModel.Factory();
                switch (filter.Filter.Command)
                {
                    case 1:
                        {
                            this.bo.UpdateSkills(filter);
                            break;
                        }

                    case 2:
                        {
                            this.bo.DeleteSkills(filter);
                            break;
                        }

                    case 3:
                        {
                            output = this.bo.InsertSkills(filter);
                            break;
                        }

                    default:
                        {
                            break;
                        }
                }

                return output;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }
    }
}
