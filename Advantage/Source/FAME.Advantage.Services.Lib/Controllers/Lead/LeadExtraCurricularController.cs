﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadExtraCurricularController.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Control for Lead Extracurricular
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Linq;
    using System.Web.Http;
    using BusinessLogic.Catalogs;
    using BusinessLogic.Lead;
    using Domain.Infrastructure.Entities;
    using Infrastructure.Exceptions;
    using Messages.Common;
    using Messages.Lead.ExtraCurricular;

    /// <summary>
    /// Control for Lead Extracurricular
    /// </summary>
    [RoutePrefix("api/Lead/LeadExtraCurricular")]
    public class LeadExtraCurricularController : ApiController
    {
        /// <summary>
        ///  The business Object.
        /// </summary>
        private readonly LeadExtraCurricularBo bo;

        // ReSharper disable NotAccessedField.Local

        /// <summary>
        ///  The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepository repository;

        // ReSharper restore NotAccessedField.Local

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadExtraCurricularController"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="leadRepository">
        /// repository GUID
        /// </param>
        /// <param name="repositoryWithInt">
        /// repository INTEGER
        /// </param>
        public LeadExtraCurricularController(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = leadRepository;

            // request = HttpContext.Current.Request;
            this.repositoryWithInt = repositoryWithInt;
            this.bo = new LeadExtraCurricularBo(leadRepository, repositoryWithInt);
        }

        /// <summary>
        /// Get The Extracurricular for the lead...
        /// </summary>
        /// <param name="filter">
        /// The filter parameters.
        /// CampusId = guid,
        /// LeadId = guid
        /// Commands =
        ///  1: Get the Extracurricular values
        ///  2: Get the Levels
        ///  3: Get the Groups
        ///  4: Get Levels and Groups
        /// </param>
        /// <returns>
        /// The Extra Curricular Output Model 
        /// </returns>
        [HttpGet]
        [Route("Get")]
        public IExtraInfoOutputModel Get([FromUri] ExtraInfoInputModel filter)
        {
            if (filter == null || filter.LeadId == null || filter.CampusId == null)
            {
                throw new HttpBadRequestResponseException("Filter has not value");
            }

            try
            {
                var output = ExtraInfoOutputModel.Factory();
                switch (filter.Command)
                {
                    case 1:
                        {
                            // Get the extracurricular values
                            output.ExtraInfoList = this.bo.GetExtracurricularValues(filter);
                            break;
                        }

                    case 2:
                        {
                            // Get the extracurricular Levels values
                            var catf = new CatalogsInputModel
                                           {
                                               AdditionalFilter = string.Empty,
                                               CampusId = filter.CampusId,
                                               FldName = "Levels"
                                           };

                            output.LevelsItemsList =
                                CatalogStatic.FilteredDropDownOutputModels(
                                    catf,
                                    this.repository,
                                    this.repositoryWithInt).ToList();
                            break;
                        }

                    case 3:
                        {
                            // Get the extracurricular Groups values
                            output.GroupItemsList = this.bo.GetGroupItemsExtracurricularValues(filter);
                            break;
                        }

                    case 4:
                        {
                            // Get the extracurricular Level and Groups values
                            // Get the extracurricular Levels values
                            var catf = new CatalogsInputModel
                                           {
                                               AdditionalFilter = string.Empty,
                                               CampusId = filter.CampusId,
                                               FldName = "Levels"
                                           };

                            output.LevelsItemsList =
                                CatalogStatic.FilteredDropDownOutputModels(
                                    catf,
                                    this.repository,
                                    this.repositoryWithInt).ToList();
                            output.GroupItemsList = this.bo.GetGroupItemsExtracurricularValues(filter);
                            break;
                        }

                    default:
                        {
                            throw new Exception("Bad Command");
                        }
                }

                return output;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Update/Delete/Insert Changes in ExtraCurricular Model
        /// </summary>
        /// <param name="filter">
        /// Command:
        /// 1: Update ExtraCurricular
        /// 2: Delete Extracurricular
        /// 3: Insert ExtraCurricular 
        /// </param>
        /// <returns>
        /// the new number
        /// </returns>
        /// <exception cref="HttpResponseException">
        /// Can be informative or bad request
        /// </exception>
        [HttpPost]
        [Route("Post")]
        public IExtraInfoOutputModel Post([FromBody] ExtraInfoOutputModel filter)
        {
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("Input object can not be null");
            }

            if (filter.Filter == null)
            {
                throw new HttpBadRequestResponseException("Input Filter can not be null");
            }

            if (string.IsNullOrWhiteSpace(filter.Filter.LeadId))
            {
                throw new HttpBadRequestResponseException("Filter parameter 1, can not be null");
            }

            if (string.IsNullOrWhiteSpace(filter.Filter.UserId))
            {
                throw new HttpBadRequestResponseException("Filter parameter 2, can not be null");
            }

            if (filter.Filter.Command < 1)
            {
                throw new HttpBadRequestResponseException("Filter parameter 3, can not be null");
            }

            try
            {
                var output = ExtraInfoOutputModel.Factory();
                switch (filter.Filter.Command)
                {
                    case 1:
                        {
                            this.bo.UpdateExtraCurriculars(filter);
                            break;
                        }

                    case 2:
                        {
                            this.bo.DeleteExtraCurriculars(filter);
                            break;
                        }

                    case 3:
                        {
                            output = this.bo.InsertExtraCurriculars(filter);
                            break;
                        }

                    default:
                        {
                            break;
                        }
                }

                return output;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }
    }
}
