﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VehiclesController.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Controller for Vehicles
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    using Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.Lead;
    using FAME.Advantage.Services.Lib.BusinessLogic.Lead;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    /// Controller for Vehicles
    /// </summary>
    public class VehiclesController : ApiController
    {
        #region Class Private members

        /// <summary>
        /// Business Object
        /// </summary>
        private readonly VehiclesBo bo;

        #endregion

        #region constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VehiclesController"/> class. 
        /// constructor
        /// </summary>
        /// <param name="repository">
        /// Repository with GUID for Lead Table
        /// </param>
        /// <param name="repositoryWithInt">
        /// The integer repository
        /// </param>
        public VehiclesController(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.bo = new VehiclesBo(repository, repositoryWithInt);
        }

        /// <summary>
        /// Get Operations over table AdVehicles
        /// </summary>
        /// <param name="filter">
        /// <see cref="VehicleInputModel"/>
        /// The filter parameters: UserId and LeadId
        /// </param>
        /// <returns>
        /// A list of <see cref="VehicleOutputModel"/>
        /// </returns>
        [HttpGet]
        public IList<VehicleOutputModel> Get([FromUri] VehicleInputModel filter)
        {
            if (filter == null || filter.LeadId == null || filter.UserId == null)
            {
                throw new HttpBadRequestResponseException("Filter has not value");
            }

            try
            {
                IList<VehicleOutputModel> output;
                switch (filter.Command)
                {
                    case 1:
                        {
                            // Get the extracurricular values
                            output = this.bo.GetAllVehiclesForTheLead(filter);
                            break;
                        }

                    default:
                        {
                            throw new Exception("Bad Command");
                        }
                }

                return output;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Post the vehicles to database
        /// </summary>
        /// <param name="vehiclesList">The vehicle list</param>
        /// <returns>OK or ERROR</returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Exception send to client
        /// </exception>
        [HttpPost]
        public HttpResponseMessage Post([FromBody] List<VehicleOutputModel> vehiclesList)
        {
            if (vehiclesList == null || !vehiclesList.Any())
            {
                throw new HttpBadRequestResponseException("No Vehicles were found");
            }

            try
            {
                // Update or Insert Vehicles
                this.bo.UpdateInsertVehicle(vehiclesList);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        #endregion
    }
}
