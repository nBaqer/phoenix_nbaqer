﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadQuickController.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The lead quick controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Messages.Common;
    using FAME.Advantage.Messages.Lead;
    using FAME.Advantage.Messages.Lead.Quick;
    using FAME.Advantage.Messages.SystemStuff.SchoolDefinedFields;
    using FAME.Advantage.Messages.SystemStuff.SystemStates;
    using FAME.Advantage.Services.Lib.BusinessLogic.Lead;
    using FAME.Advantage.Services.Lib.Infrastructure.ActionFilters;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    /// The lead quick controller.
    /// </summary>
    [RoutePrefix("api/Lead/LeadQuick")]
    public class LeadQuickController : ApiController
    {
        // represents data store
        // private read-only IRepository repository;
        // private read-only IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        /// The bo.
        /// </summary>
        private readonly LeadQuickBo bo;

        /// <summary>
        /// The Lead business Object.
        /// </summary>
        private readonly LeadBo lbo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadQuickController"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="leadRepository">
        /// Repository to be passed
        /// </param>
        /// <param name="repositoryWithInt">
        /// This is Repository with integer
        /// </param>
        public LeadQuickController(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            // repository = leadRepository;
            // request = HttpContext.Current.Request;
            // this.repositoryWithInt = repositoryWithInt;
            this.bo = new LeadQuickBo(leadRepository, repositoryWithInt);
            this.lbo = new LeadBo(leadRepository, repositoryWithInt);
        }

        /// <summary>
        /// Get the list of required fields for the page
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// Return the list of Fields required to be dynamically generated
        /// </returns>
        [HttpGet]
        [Route("Get")]
        public IEnumerable<LeadQuick> Get([FromUri]LeadQuickFieldsInputModel filter)
        {
            // Test the filter
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("Filter is required");
            }

            try
            {
                switch (filter.Command)
                {
                    case 1:
                        {
                            return this.bo.GetQuickLeadFields(filter);
                        }

                    default:
                        {
                            throw new Exception("Default");
                        }
                }
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Get state data source as Code - Description.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// data source of the drop-down
        /// </returns>
        [HttpGet]
        [Route("GetQuickLeadStateValues")]
        public QuickLeadDropdownDataSource<SystemStatesOutputModel> GetQuickLeadStateValues([FromUri]LeadQuickFieldsInputModel filter)
        {
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("Filter is required");
            }

            if (filter.CtrlName == null)
            {
                throw new HttpBadRequestResponseException("Control name is required");
            }

            try
            {
                QuickLeadDropdownDataSource<SystemStatesOutputModel> outputModel = new QuickLeadDropdownDataSource<SystemStatesOutputModel>();
                outputModel.DataSource = this.bo.GetQuickLeadStateValues();
                outputModel.Name = filter.CtrlName;
                
                // outputModel.DataSource.OrderBy(x => x.Code);
                return outputModel;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Get quick lead drop down values.
        /// </summary>
        /// <param name="filter">
        /// It has <code>FldName, CampusId, AdditionalFilter</code>.
        /// </param>
        /// <returns>
        /// data source of the drop-down
        /// </returns>
        [HttpGet]
        [Route("GetQuickLeadDropDownValues")]
        [HttpGetBindJsonQueryString(typeof(LeadQuickFieldsInputModel), "filter")]
        public QuickLeadDropdownDataSource<DropDownOutputModel> GetQuickLeadDropDownValues(LeadQuickFieldsInputModel filter)
        {
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("Filter can not be null");
            }

            if (filter.CtrlName == null)
            {
                throw new HttpBadRequestResponseException("Control Name can not be null");
            }

            if (filter.Input == null)
            {
                throw new HttpBadRequestResponseException("Input can not be null");
            }

            if (filter.Input.FldName == null)
            {
                throw new HttpBadRequestResponseException("Field Name can not be null");
            }

            try
            {
                QuickLeadDropdownDataSource<DropDownOutputModel> outputModel = new QuickLeadDropdownDataSource<DropDownOutputModel>();
                outputModel.DataSource = this.bo.GetQuickLeadDropDownValues(filter.Input);
                outputModel.DataSource.OrderBy(x => x.Description);
                outputModel.Name = filter.CtrlName;
                return outputModel;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        ///// <summary>
        ///// Get quick lead drop down values.
        ///// </summary>
        ///// <param name="input">
        ///// It has FldName, CampusId, AdditionalFilter.
        ///// </param>
        ///// <returns>
        ///// data source of the dropdown
        ///// </returns>
        // [HttpGet]
        // public IEnumerable<DropDownOutputModel> GetQuickLeadDropDownValues([FromUri] CatalogsInputModel input)
        // {
        //    if (input == null)
        //    {
        //        throw new HttpBadRequestResponseException("Filter can not be null");
        //    }
        //    if (input.FldName == null)
        //    {
        //        throw new HttpBadRequestResponseException("Field Name can not be null");
        //    }
        //    try
        //    {
        //        return this.bo.GetQuickLeadDropDownValues(input);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
        //    }
        // }

        /// <summary>
        /// Get custom defined list.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// the control information of school/user defined fields
        /// </returns>
        [HttpGet]
        [Route("GetSdfList")]
        public IList<SdfOutputModel> GetSdfList([FromUri] LeadInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Filter can not be null");
            }

            if (input.PageResourceId == 0)
            {
                throw new HttpBadRequestResponseException("The Page Resource Id can not be null");
            }

            if (input.CampusId == null)
            {
                throw new HttpBadRequestResponseException("No Campus Ids found");
            }

            try
            {
                // Get School Defined Fields (SDF)
                var listSdf = this.lbo.GetLeadSchoolCustomFields(input);
                return listSdf;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Get the list group information associated with campus Id.
        /// </summary>
        /// <param name="input">Gets the campus id</param>
        /// <returns>returns the lead group checkbox list</returns>
        [HttpGet]
        [Route("GetLeadGroupInformation")]
        public IList<LeadGroupInfoOutputModel> GetLeadGroupInformation([FromUri] LeadInputModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Filter can not be null");
            }

            if (input.PageResourceId == 0)
            {
                throw new HttpBadRequestResponseException("The Page Resource Id can not be null");
            }

            if (input.CampusId == null)
            {
                throw new HttpBadRequestResponseException("No Campus Ids found");
            }

            try
            {
                var leadGrp = this.lbo.GetLeadGroupInformation(input);
                return leadGrp;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }
    }
}
