﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadToolBarController.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Controller for Lead Tool bar operation.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Web.Http;

    using BusinessLogic.Lead;
    using BusinessLogic.SystemStuff;
    using Domain.Infrastructure.Entities;
    using Infrastructure.Exceptions;
    using Messages.Lead.ToolBar;
    using Messages.SystemStuff.Mru;

    /// <summary>
    /// Controller for Lead Tool bar operation.
    /// </summary>
    [RoutePrefix("api/Lead/LeadToolBar")]
    public class LeadToolBarController : ApiController
    {
        #region Class Private members

        // Represents the request object

        /// <summary>
        ///  The bo.
        /// </summary>
        private readonly LeadToolBarBo bo;

        /// <summary>
        ///  The MRU bo.
        /// </summary>
        private readonly MruBo mbo;
        #endregion

        #region constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadToolBarController"/> class. 
        /// constructor
        /// </summary>
        /// <param name="leadRepository">
        /// Repository with GUID for Lead Table
        /// </param>
        /// <param name="repositoryWithInt">
        /// Repository with integer
        /// </param>
        public LeadToolBarController(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.bo = new LeadToolBarBo(leadRepository, repositoryWithInt);
            this.mbo = new MruBo(repositoryWithInt, leadRepository);
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Get the Lead Info to be show in Lead Info Bar.
        /// </summary>
        /// <param name="filter">
        /// Command in filter:
        /// 0: Get flags of alert
        /// 1: Up Queue
        /// 2: Down Queue
        /// </param>
        /// <returns>
        /// The lead Information for Tool Object
        /// </returns>
        [HttpGet]
        [Route("Get")]
        public LeadInfoToolOutputModel Get([FromUri] LeadInfoToolInputModel filter)
        {
            // Test the filter
            if (filter == null || filter.Command == null)
            {
                throw new HttpBadRequestResponseException("Filter has not value");
            }

            try
            {
                switch (filter.Command)
                {
                    case "0":
                        {
                            // GET QUEUE FLAGS
                        if (string.IsNullOrWhiteSpace(filter.UserId))
                        {
                            throw new Exception("Filter UserId has not value");
                        }

                        LeadInfoToolOutputModel flags = this.bo.GetAlertFlags(filter);
                        return flags;
                    }

                    case "2":
                        {
                            // NEXT
                        if (string.IsNullOrWhiteSpace(filter.UserId)
                            || string.IsNullOrEmpty(filter.CampusId)
                            || string.IsNullOrEmpty(filter.LeadId))
                        {
                            throw new Exception("A parameter is missing");
                        }

                        string leadId = this.bo.GetNextLeadInMru(filter);
                        var inp = new MruInputModel()
                        {
                            CampusId = filter.CampusId, 
                            UserId = filter.UserId, 
                            EntityId = leadId, 
                            TypeEntity = 4
                        };
                            this.mbo.InsertEntityInMru(inp);
                            var flags = new LeadInfoToolOutputModel { OkChangeLeadOperation = true };
                            return flags;
                        }

                    case "1":
                        {
                            // PREVIOUS
                        if (string.IsNullOrWhiteSpace(filter.UserId)
                            || string.IsNullOrEmpty(filter.CampusId)
                            || string.IsNullOrEmpty(filter.LeadId))
                        {
                            throw new HttpBadRequestResponseException("A parameter is missing");
                        }

                        string leadId = this.bo.GetPreviousLeadInMru(filter);
                        var inp = new MruInputModel()
                        {
                            CampusId = filter.CampusId, 
                            UserId = filter.UserId, 
                            EntityId = leadId, 
                            TypeEntity = 4
                        };
                            this.mbo.InsertEntityInMru(inp);
                            var flags = new LeadInfoToolOutputModel { OkChangeLeadOperation = true };
                            return flags;
                        }

                    default:
                    {
                        throw new Exception("Unrecognized command");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        #endregion
    }
}
