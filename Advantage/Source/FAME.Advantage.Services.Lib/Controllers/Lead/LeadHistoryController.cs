﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadHistoryController.cs" company="FAME Inc.">
//  FAME Inc. 2016
//  FAME.Advantage.Services.Lib.Controllers\Lead\LeadHistoryController.cs
// </copyright>
// <summary>
//   Controller for Lead History Page
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Linq;
    using System.Web.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.Lead.History;
    using FAME.Advantage.Services.Lib.BusinessLogic.Lead;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    /// Controller for Lead History Page
    /// </summary>
    [RoutePrefix("api/Lead/LeadHistory")]
    public class LeadHistoryController : ApiController
    {
        /// <summary>
        /// The repository. Represents data store
        /// </summary>
        private readonly IRepository repository;
 
        /// <summary>
        /// The bo. 
        /// </summary>
        private readonly LeadHistoryBo bo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadHistoryController"/> class. 
        /// The Constructor
        /// </summary>
        /// <param name="leadRepository">
        /// repository with GUID
        /// </param>
        /// <param name="repositoryWithInt">
        /// repository INTEGER
        /// </param>
        public LeadHistoryController(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = leadRepository;

            // request = HttpContext.Current.Request;
            //this.repositoryWithInt = repositoryWithInt;
            this.bo = new LeadHistoryBo(leadRepository, repositoryWithInt);
        }

        /// <summary>
        /// Get the list of History records
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// Return History associated with The Lead
        /// </returns>
        [HttpGet]
        [Route("Get")]
        public IHistoryOutputModel Get([FromUri] LeadHistoryInputFilter filter)
        {
            // Test the filter
            if (filter == null || filter.Command < 1 || string.IsNullOrWhiteSpace(filter.LeadId))
            {
                throw new HttpBadRequestResponseException("Filter has no value");
            }

            try
            {
                var result = HistoryOutputModel.Factory();
                switch (filter.Command)
                {
                    case 1:
                        {
                            if (string.IsNullOrWhiteSpace(filter.ModuleCode) || string.IsNullOrEmpty(filter.UserId)
                                || string.IsNullOrEmpty(filter.CampusId))
                            {
                                throw new HttpBadRequestResponseException(
                                    "Filter (Module Code or UserId or CampusId) has not value");
                            }

                            // Return the History    
                            result.HistoryList = this.bo.GetLeadHistoryList(filter);
                            return result;
                        }

                    case 2:
                        {
                            // Return the modules items Code/Description
                            result.ModulesList = this.bo.GetModulesItemList(filter);
                            return result;
                        }

                    default:
                        {
                            throw new Exception("Unrecognized Command");
                        }
                }
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }


    }
}
