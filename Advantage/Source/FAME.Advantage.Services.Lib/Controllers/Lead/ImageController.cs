﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageController.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Defines the ImageController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    using BusinessLogic.Lead;
    using Domain.Infrastructure.Entities;
    using Formatters.MultipartDataMediaFormatter.Infrastructure;
    using Infrastructure.Exceptions;
    using Messages.Lead.Image;

    /// <summary>
    /// The image controller.
    /// </summary>
    [RoutePrefix("api/Lead/Image")]
    public class ImageController : ApiController
    {
        /// <summary>
        /// The repository.
        /// </summary>
        ////private readonly IRepositoryWithTypedID<int> repository;

        /// <summary>
        /// The bo.
        /// </summary>
        private readonly ImageBo bo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageController"/> class. 
        /// Constructor - initialize the repository
        /// </summary>
        /// <param name="repository">
        /// the repository to be used
        /// </param>
        /// <param name="repositoryInteger">
        /// The repository Integer.
        /// </param>
        public ImageController(IRepository repository, IRepositoryWithTypedID<int> repositoryInteger)
        {
            ////this.repository = repositoryInteger;
            this.bo = new ImageBo(repository, repositoryInteger);
        }

        /// <summary>
        /// Get the image from table LeadImage.
        /// </summary>
        /// <param name="filter">
        /// admit <code>LeadId Command UserId</code> 
        /// </param>
        /// <returns>A enumerable in all case.</returns>
        [HttpGet]
        [Route("Get")]
        public ImageOutputModel Get([FromUri] ImageInputModel filter)
        {
            try
            {
                if (filter == null)
                {
                    throw new Exception("Filter can not be null");
                }

                switch (filter.Command)
                {
                    case 1:
                        {
                            if (filter.EntityId == null)
                            {
                                throw new Exception("Lead can not be null");
                            }

                            // Get the image by entity and module id
                            ImageOutputModel output = this.bo.GetImage(filter);
                            return output;
                        }

                    default:
                        {
                            throw new Exception("Command not recognized");
                        }
                }
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Post this procedure received the logo or logos files
        /// </summary>
        /// <param name="formData">
        /// The image
        /// </param>
        /// <param name="filter">Logo files</param>
        /// <returns>200 OK if all is OK</returns>
        [HttpPost]
        [Route("Post")]
        public HttpResponseMessage Post([FromBody] FormData formData, [FromUri] ImageInputModel filter)
        {
            try
            {
                if (filter == null)
                {
                    throw new Exception("Input filter can not be null");
                }

                if (filter.EntityId == null || filter.UserId == null)
                {
                    throw new Exception("Input parameter with null value");
                }

                this.bo.StoreImageFile(formData, filter);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// The logo code, observations and if it official use can be edited and updated.
        /// </summary>
        /// <param name="data">
        /// The information about the logo
        /// </param>
        /// <returns>
        /// OK if OK
        /// </returns>
        [HttpPost]
        [Route("PostImg")]
        public HttpResponseMessage PostImg([FromBody]ImageOutputModel data)
        {
            if (data == null || data.Filter == null)
            {
                throw new HttpBadRequestResponseException("Input can not be null");
            }

            try
            {
                switch (data.Filter.Command)
                {
                    case 1:
                        {
                            var filter = data.Filter;
                            if (string.IsNullOrWhiteSpace(filter.EntityId) || string.IsNullOrWhiteSpace(filter.UserId))
                            {
                                throw new Exception("Entity or user can not be null");
                            }

                            this.bo.StoreBase64Image(data);
                            return new HttpResponseMessage(HttpStatusCode.OK);
                        }
                    
                    default:
                        {
                            throw new Exception("Command not recognized");
                        }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }
    }

    /////// <summary>
    /////// Delete the logo in database
    /////// </summary>
    /////// <param name="logodata">The ID is the only needed data</param>
    /////// <returns>
    /////// status code OK.
    /////// </returns>
    ////[HttpDelete]
    ////public HttpResponseMessage Delete([FromBody] SchoolLogoOutputModel logodata)
    ////{
    ////    try
    ////    {
    ////        // Constrains...............
    ////        if (logodata != null)
    ////        {
    ////            var logo = this.repository.Get<SySchoolLogo>(logodata.ID);
    ////            if (logo == null)
    ////            {
    ////                throw new Exception("The Image does not exists or was erased in server.");
    ////            }

    ////            this.repository.Delete(logo);
    ////            return new HttpResponseMessage(HttpStatusCode.OK);
    ////        }

    ////        throw new Exception("The required parameter does not exists.");
    ////    }
    ////    catch (Exception ex)
    ////    {
    ////        throw new HttpBadRequestResponseException(ex.Message);
    ////    }
    ////}
}
