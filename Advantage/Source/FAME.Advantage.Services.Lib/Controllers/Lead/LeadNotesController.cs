﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadNotesController.cs" company="FAME">
//   2015, 2016
// </copyright>
// <summary>
//   Controller for Lead Notes Page
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Messages.Lead.Notes;
    using FAME.Advantage.Services.Lib.BusinessLogic.Lead;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    /// Controller for Lead Notes Page
    /// </summary>
    [RoutePrefix("api/Lead/LeadNotes")]
    public class LeadNotesController : ApiController
    {
        /// <summary>
        /// The repository. Represents data store
        /// </summary>
        private readonly IRepository repository;
 
        /// <summary>
        /// The bo. 
        /// </summary>
        private readonly LeadNotesBo bo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadNotesController"/> class. 
        /// The Constructor
        /// </summary>
        /// <param name="leadRepository">
        /// repository with GUID
        /// </param>
        /// <param name="repositoryWithInt">
        /// repository INTEGER
        /// </param>
        public LeadNotesController(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = leadRepository;

            // request = HttpContext.Current.Request;
            // this.repositoryWithInt = repositoryWithInt;
            this.bo = new LeadNotesBo(leadRepository, repositoryWithInt);
        }

        /// <summary>
        /// Get the list of Notes
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// Return Notes associates with The Lead
        /// </returns>
        [HttpGet]
        [Route("Get")]
        public INotesOutputModel Get([FromUri] LeadNotesInputFilter filter)
        {
            // Test the filter
            if (filter == null || filter.Command < 1 || string.IsNullOrWhiteSpace(filter.LeadId))
            {
                throw new HttpBadRequestResponseException("Filter has not value");
            }

            try
            {
                var result = NotesOutputModel.Factory();
                switch (filter.Command)
                {
                    case 1:
                        {
                            if (string.IsNullOrWhiteSpace(filter.ModuleCode) || string.IsNullOrEmpty(filter.UserId)
                                || string.IsNullOrEmpty(filter.CampusId))
                            {
                                throw new Exception("Filter (Module Code or UserId or CampusId) has not value");
                            }

                            // Analysis of user roles to get confidential or not
                            var bou = new BusinessLogic.Users.UserRolesBo(this.repository);
                            var isConfidential = bou.IsRolConfidential(filter.UserId, filter.CampusId);
                            var isSupport = bou.IsSupport(filter.UserId);

                            // Return the notes    
                            var notesList = this.bo.GetLeadNotesList(filter, isSupport || isConfidential);

                            var highId = (notesList.Count == 0) ? 0 : notesList.Max(x => x.IdNote) + 100;

                            // Get skills
                            var skillComments = this.bo.GetSkillNotes(filter);

                            foreach (var model in skillComments)
                            {
                                model.IdNote += highId;
                                notesList.Add(model);
                            }

                            // Get extra curricular
                            var extraCuComments = this.bo.GetExtraCurricularNotes(filter);
                            highId = (skillComments.Count == 0) ? highId : skillComments.Max(x => x.IdNote) + 100;

                            foreach (var model in extraCuComments)
                            {
                                model.IdNote += highId;
                                notesList.Add(model);
                            }

                            // Get Requirements
                            var requirementNotes = this.bo.GetRequirementsOverrideReasonsList(filter);
                            foreach (var model in requirementNotes)
                            {
                                // model.IdNote += highId;
                                notesList.Add(model);
                            }

                            ////// Get Task and Appointment
                            ////var taskappointment = this.bo.GetTaskAndAppointmentList(filter);
                            ////foreach (var model in taskappointment)
                            ////{
                            ////    // model.IdNote += highId;
                            ////    notesList.Add(model);
                            ////}

                            // Get Prior Work Comments
                            IList<NotesModel> priorWorkCommentsList = this.bo.GetPriorWorkCommentsList(filter);
                            foreach (var model in priorWorkCommentsList)
                            {
                                notesList.Add(model);
                            }

                            // Note This was eliminated. do not delete. can be added later. Get Messages (Email/Printer)
                            // var messages = this.bo.GetLeadMessagesList(filter);
                            // foreach (var model in messages)
                            // {
                            //    // model.IdNote += highId;
                            //    notesList.Add(model);
                            // }
                            result.NotesList = notesList.OrderByDescending(x => x.NoteDate).ToList();
                            return result;
                        }

                    case 2:
                        {
                            // Return the modules items Code/Description
                            result.ModulesList = this.bo.GetModulesItemList(filter);
                            return result;
                        }

                    // case 3:
                    // {
                    // result.TypeNotesList = bo.GetNotesTypeItemList(filter);
                    // return result;
                    // }
                    default:
                        {
                            throw new Exception("Unrecognized Command");
                        }
                }
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Post info to update  / Insert Notes.
        /// </summary>
        /// <param name="filter">The Filter</param>
        /// <returns>The Notes Output Model</returns>
        [HttpPost]
        [Route("Post")]
        public INotesOutputModel Post([FromBody] NotesOutputModel filter)
        {
            // Test the filter
            if (filter == null || filter.Filter == null || filter.Filter.Command < 1 || string.IsNullOrWhiteSpace(filter.Filter.UserId))
            {
                throw new HttpBadRequestResponseException("Filter has not value");
            }

            try
            {
                var output = NotesOutputModel.Factory();

                // var result = NotesOutputModel.Factory();
                switch (filter.Filter.Command)
                {
                    case 1:
                        {
                            // Update Notes
                            if (filter.NotesList == null || filter.NotesList.Count < 1)
                            {
                                throw new ApplicationException("Nothing to update");
                            }

                            // Update Notes   
                            this.bo.UpdateLeadNotesList(filter);
                            break;
                        }

                    case 2:
                        {
                            // Insert Notes
                            if (filter.NotesList == null || filter.NotesList.Count < 1)
                            {
                                throw new ApplicationException("Nothing to insert");
                            }

                            output = this.bo.InsertNotesList(filter);
                            break;
                        }

                    default:
                        {
                            throw new Exception("Unrecognized Command");
                        }
                }

                return output;
            }
             catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }
    }
}
