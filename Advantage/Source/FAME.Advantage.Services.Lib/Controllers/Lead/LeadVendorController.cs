﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadVendorController.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the LeadVendorController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Messages.Lead;
    using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

    /// <summary>
    /// The lead vendor controller.
    /// </summary>
    [RoutePrefix("api/Lead/LeadVendor")]
    public class LeadVendorController : ApiController
    {
        /// <summary>
        /// Repository with Integer
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadVendorController"/> class.
        /// </summary>
        /// <param name="repository">
        ///  The repository.
        /// </param>
        public LeadVendorController(IRepositoryWithTypedID<int> repository)
        {
            this.repository = repository;
        }

        /// <summary>
        ///  The get the vendors.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable&lt;LeadVendorOutputModel&gt;"/>.
        /// The list of vendors
        /// </returns>
        [HttpGet]
        [Route("Get")]
        public IEnumerable<LeadVendorOutputModel> Get()
        {
            try
            {
                var leadvendors = this.repository.Query<AdVendors>().Where(n => n.IsActive && n.IsDeleted == false).ToList();
                leadvendors = leadvendors.OrderBy(n => n.VendorName).ToList();

                var vendors =
                    leadvendors.Select(
                        v =>
                            new LeadVendorOutputModel
                            {
                                Id = v.ID,
                                Description = v.Description,
                                VendorCode = v.VendorCode,
                                VendorName = v.VendorName
                            }).ToList();

                vendors.Insert(
                    0,
                    new LeadVendorOutputModel()
                    {
                        Id = 0,
                        Description = "Advantage",
                        VendorCode = "ADV",
                        VendorName = "Advantage"
                    });
                vendors.Insert(0, new LeadVendorOutputModel() { Id = -1, Description = "All Vendors", VendorCode = "ALL", VendorName = "All Vendors" });

                return vendors;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }
    }
}
