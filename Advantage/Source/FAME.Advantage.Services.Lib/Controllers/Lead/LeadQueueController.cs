﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadQueueController.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Controller for Page Lead Queue
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    using BusinessLogic.Catalogs;
    using BusinessLogic.Lead;
    using BusinessLogic.SystemStuff;
    using BusinessLogic.Users;
    using Domain.Infrastructure.Entities;
    using Infrastructure.Exceptions;
    using Messages.Lead.Queue;

    /// <summary>
    /// Controller for Page Lead Queue
    /// </summary>
    [RoutePrefix("api/Lead/LeadQueue")]
    public class LeadQueueController : ApiController
    {
        /// <summary>
        ///  The bo.
        /// </summary>
        private readonly LeadQueueBo bo;

        /// <summary>
        ///  The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadQueueController"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="leadRepository">
        /// repository GUID
        /// </param>
        /// <param name="repositoryWithInt">
        /// repository integer
        /// </param>
        public LeadQueueController(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = leadRepository;
            this.repositoryWithInt = repositoryWithInt;
            this.bo = new LeadQueueBo(leadRepository, repositoryWithInt);
        }

        /// <summary>
        /// Get the Queue information for Lead Queue Page
        /// </summary>
        /// <param name="filter">
        /// The filter parameters.
        /// CampusId = GUID,
        /// UserId = GUID
        /// Command = 0:Get All 1: get only admission rep list. 2: get only LeadInfo
        /// 3: get only number of lead assigned to Admission rep and the time between job execution
        /// </param>
        /// <returns>
        /// The Queue output Model
        /// </returns>
        [HttpGet]
        [Route("Get")]
        public IQueueOutputModel Get([FromUri]QueueInputmodel filter)
        {
            if (filter == null || filter.UserId == null || filter.CampusId == null)
            {
                throw new HttpBadRequestResponseException("Filter has not value");
            }

            try
            {
                var output = QueueOutputModel.Factory();

                // Get Admission Rep List................................................................................
                if (filter.Command == 1 | filter.Command == 0)
                {
                    var bocat = new CatalogsBo(this.repository, this.repositoryWithInt);
                    output.AdmissionRepList = bocat.GetAdmissionRepItems(filter.CampusId).ToList();
                }

                // Get Lead Info Task and Demographic ...........................................................
                if (filter.Command == 0 | filter.Command == 2)
                {
                    // Get LeadInfo
                    output.LeadQueueInfoList = this.bo.GetQueueLeadInfo(filter);
                }

                // Get new lead assigned to Admission Rep and interval time to run the job
                if (filter.Command == 3)
                {
                    output.QueueInfoObj.NumberOfLeadPending = this.bo.GetInfoObject(filter);
                }

                // Get Queue Info
                output.QueueInfoObj.DeliveryTime = DateTime.Now;
                var refreshSec = ConfigurationAppSettings.Setting("RefreshQueueInterval", this.repositoryWithInt);
                output.QueueInfoObj.RefreshQueueInterval = Convert.ToInt32(refreshSec);

                // Get if the admission Rep combo-box must be visible or not
                // Get admission part
                var editOtherLeads =
                    ConfigurationAppSettings.Setting("EditOtherLeads", this.repositoryWithInt).ToUpperInvariant();
               
                // var showLeadstoUsers = ConfigurationAppSettings.Setting("ShowLeadstoUsers", this.repositoryWithInt);
                output.QueueInfoObj.ShowAdmissionRep = editOtherLeads == "YES"; 

                // Analysis if the user is Director Of Admission
                if (output.QueueInfoObj.ShowAdmissionRep == false)
                {
                    var rolBo = new UserRolesBo(this.repository);
                    var roles = new List<int>() { 8 }; // 8 director of admission role.
                    output.QueueInfoObj.ShowAdmissionRep = rolBo.IsUserAuthorized(roles, filter.UserId, filter.CampusId);

                    // Analysis if it is support
                    if (output.QueueInfoObj.ShowAdmissionRep == false)
                    {
                        output.QueueInfoObj.ShowAdmissionRep = rolBo.IsSupport(filter.UserId);
                    }
                }

                return output;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Modify a phone number for a Lead. 
        /// </summary>
        /// <param name="filter">
        /// Phone position can be: 1:primary 2:secondary 3: ternary phone
        /// </param>
        /// <returns>
        /// the new number
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Bad request exception
        /// </exception>
        [HttpPost]
        [Route("Post")]
        public QueuePhoneInputModel Post([FromBody]QueuePhoneInputModel filter)
        {
            if (filter == null || filter.LeadId == null || filter.Phone == null || filter.Position == 0)
            {
                throw new HttpBadRequestResponseException("Filter has not value");
            }

            try
            {
                var output = this.bo.SetUpdateLeadPhone(filter);
                return output;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }
    }
}
