﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadRequirementsController.cs" company="FAME Inc">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Controllers.Lead.LeadRequirementsBo
// </copyright>
// <summary>
//   Defines the LeadRequirementsController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using BusinessLogic.Lead;
    using Domain.Infrastructure.Entities;
    using Formatters.MultipartDataMediaFormatter.Infrastructure;
    using Infrastructure.Exceptions;
    using Infrastructure.Extensions;
    using Messages.Lead;
    using Messages.Requirements;

    /// <summary>
    /// The Lead Requirements Controller
    /// </summary>
    [RoutePrefix("api/Leads")]
    public class LeadRequirementsController : ApiController
    {
        /// <summary>
        /// The Lead Requirements Business Object.
        /// </summary>
        private readonly LeadRequirementsBo bo;

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadRequirementsController"/> class. 
        /// </summary>
        /// <param name="repository">
        /// The nHibernate repository for entities with Identity of type GUID
        /// </param>
        /// <param name="repositoryWithInt">
        /// The nHibernate repository for entities with Identity of type INT
        /// </param>
        public LeadRequirementsController(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.bo = new LeadRequirementsBo(repository, repositoryWithInt);
        }
        #endregion
        #region Public Methods
        /// <summary>
        /// Get requirements for one lead
        /// </summary>
        /// <param name="lead_Id">
        /// The lead Id as GUID
        /// </param>
        /// <param name="filter">
        /// The filter input value of type <see cref="GetLeadRequirementInputModel"/>. 
        /// </param>
        /// <returns>
        /// An object of type <see cref="GetLeadRequirementInputModel"/>. 
        /// </returns>
        [HttpGet]
        [Route("~/api/Leads/{lead_Id}/Requirements/GetRequirementsList")]
        public LeadRequirementsListOutputModel GetRequirementsList(Guid lead_Id, [FromUri] GetLeadRequirementInputModel filter)
        {
            var reqList = new LeadRequirementsListOutputModel
            {
                MandatoryRequirements = this.bo.GetRequirements(filter, lead_Id),
                OptionalRequirements = this.bo.GetLeadGroupOptionalRequirements(filter, lead_Id),
                RequirementGroups = this.bo.GetRequirementGroups(filter, lead_Id),
            };

            reqList.HaveMetRequirements = this.bo.HaveMetRequirements(
                lead_Id,
                filter.CampusId,
                reqList.MandatoryRequirements.ToList(),
                reqList.RequirementGroups.ToList());

            return reqList;
        }

        /// <summary>
        /// Get requirements for one lead
        /// </summary>
        /// <param name="lead_Id">
        /// The lead id as GUID
        /// </param>
        /// <param name="filter">
        /// The filter input value of type <see cref="GetLeadRequirementInputModel"/>. 
        /// </param>
        /// <returns>
        /// An IEnumerable of type <see cref="LeadReqEntityOutputModel"/>. 
        /// </returns>
        [HttpGet]
        [Route("~/api/Leads/{lead_Id}/Requirements/GetRequirementsByLeadId")]
        public IEnumerable<LeadReqEntityOutputModel> GetRequirementsByLeadId(Guid lead_Id, [FromUri] GetLeadRequirementInputModel filter)
        {
            return this.bo.GetRequirements(filter, lead_Id);
        }

        /// <summary>
        /// Get requirements for one lead
        /// </summary>
        /// <param name="lead_Id">
        /// The lead id as GUID
        /// </param>
        /// <param name="filter">
        /// The filter input value of type <see cref="GetLeadRequirementInputModel"/>. 
        /// </param>
        /// <returns>
        /// An IEnumerable of type <see cref="LeadReqEntityOutputModel"/>. 
        /// </returns>
        [HttpGet]
        [Route("~/api/Leads/{lead_Id}/Requirements/GetOptionalRequirementsByLeadId")]
        public IEnumerable<LeadReqEntityOutputModel> GetOptionalRequirementsByLeadId(Guid lead_Id, [FromUri] GetLeadRequirementInputModel filter)
        {
            return this.bo.GetLeadGroupOptionalRequirements(filter, lead_Id);
        }

        /// <summary>
        /// Get requirement groups for one lead
        /// </summary>
        /// <param name="lead_Id">
        /// The lead id as GUID
        /// </param>
        /// <param name="filter">
        /// The filter input value of type <see cref="GetLeadRequirementInputModel"/>. 
        /// </param>
        /// <returns>
        /// An IEnumerable of type <see cref="LeadReqEntityOutputModel"/>. 
        /// </returns>
        [HttpGet]
        [Route("~/api/Leads/{lead_Id}/Requirements/GetReqGroupsByLeadId")]
        public IEnumerable<LeadReqEntityOutputModel> GetReqGroupsByLeadId(Guid lead_Id, [FromUri] GetLeadRequirementInputModel filter)
        {
            return this.bo.GetRequirementGroups(filter, lead_Id);
        }

        /// <summary>
        /// Get requirements by group id for one lead
        /// </summary>
        /// <param name="grpId">
        /// The group id as GUID
        /// </param>
        /// <param name="filter">
        /// The filter input value of type <see cref="GetLeadRequirementInputModel"/>. 
        /// </param>
        /// <returns>
        /// An IEnumerable of type <see cref="LeadReqEntityOutputModel"/>. 
        /// </returns>
        [HttpGet]
        [Route("~/api/Leads/{grpId}/Requirements/GetRequirementsByGroupId")]
        public IEnumerable<LeadReqEntityOutputModel> GetRequirementsByGroupId(Guid grpId, [FromUri] GetLeadRequirementInputModel filter)
        {
            return this.bo.GetRequirementByGroupId(filter, grpId);
        }

        /// <summary>
        /// Get requirements by group id for one lead
        /// </summary>
        /// <param name="reqId">
        /// The requirement id as GUID
        /// </param>
        /// <param name="filter">
        /// The filter input value of type <see cref="GetLeadRequirementInputModel"/>. 
        /// </param>
        /// <returns>
        /// An object of type <see cref="LeadReqDetailsOutputModel"/>. 
        /// </returns>
        [HttpGet]
        [Route("~/api/Leads/{reqId}/Requirements/GetRequirementsDetailsByReqId")]
        public LeadReqDetailsOutputModel GetRequirementsDetailsByReqId(Guid reqId, [FromUri] GetLeadRequirementInputModel filter)
        {
            return this.bo.GetRequirementDetailsByReqId(filter, reqId);
        }

        /// <summary>
        /// Get requirements by group id for one lead
        /// </summary>
        /// <param name="filter">
        ///  The filter input value of type <see cref="GetLeadRequirementInputModel"/>. 
        /// </param>
        /// <returns>
        ///  An object of type <see cref="LeadTransactionOutput"/>. 
        /// </returns>
        [HttpGet]
        [Route("~/api/Leads/Requirements/GetLeadTransactionReceipt")]
        public LeadTransactionOutput GetLeadTransactionReceipt([FromUri] GetLeadRequirementInputModel filter)
        {
            return this.bo.GetLeadTransactionReceipt(filter);
        }

        /// <summary>
        /// Post Requirement
        /// </summary>
        /// <param name="filter">
        /// The filter input value of type <see cref="LeadRequirementInputModel"/>. 
        /// </param>
        /// <returns>
        /// An object of type <see cref="HttpResponseMessage"/>. 
        /// </returns>
        [HttpPost]
        [Route("~/api/Leads/Requirements/PostRequirement")]
        public HttpResponseMessage PostRequirement([FromBody] LeadRequirementInputModel filter)
        {
            try
            {
                if (!filter.DocReqId.IsEmpty())
                {
                    this.bo.UpdateExistingRequirement(filter, filter.DocReqId);
                }
                else
                {
                    this.bo.InsertNewRequirement(filter);
                }

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        /// <summary>
        /// Delete Requirement History Record
        /// </summary>
        /// <param name="filter">
        /// The filter input value of type <see cref="LeadReqDetailsInputModel"/>. 
        /// </param>
        /// <exception cref="HttpBadRequestResponseException">
        /// A HttpBadRequestResponseException with the reason. Mostly an exception with the reason of a Missing Required Field.
        /// </exception>
        /// <exception cref="NotImplementedException">
        /// A method not implementation exception
        /// </exception>
        /// <returns>
        /// A HttpResponseMessage
        /// </returns>
        [HttpPost]
        [Route("~/api/Leads/Requirements/DeleteRequirementHistoryRecord")]
        public HttpResponseMessage DeleteRequirementHistoryRecord([FromBody] LeadReqDetailsInputModel filter)
        {
            try
            {
                this.bo.DeleteRequirementHistoryRecord(filter);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        /// <summary>
        /// Post Document File
        /// </summary>
        /// <param name="requirement">
        /// The requirement of type String
        /// </param>
        /// <param name="formData">
        /// The form data of type <see cref="FormData"/>. 
        /// </param>
        /// <returns>
        /// An object of type <see cref="HttpResponseMessage"/>. 
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// A service exception for any un handle exceptions
        /// </exception>
        [HttpPost]
        [Route("~/api/Leads/Requirements/PostDocumentFile")]
        public HttpResponseMessage PostDocumentFile(string requirement, FormData formData)
        {
            try
            {
                this.bo.SaveDocumentFile(requirement, formData);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        /// <summary>
        /// Void Transaction
        /// </summary>
        /// <param name="filter">
        /// The filter input value of type <see cref="GetLeadRequirementInputModel"/>. 
        /// </param>
        /// <returns>
        /// An object of type <see cref="HttpResponseMessage"/>. 
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// A service exception for any un handle exceptions
        /// </exception>
        [HttpPost]
        [Route("~/api/Leads/Requirements/VoidTransaction")]
        public HttpResponseMessage VoidTransaction([FromBody] GetLeadRequirementInputModel filter)
        {
            try
            {
                this.bo.VoidTransaction(filter);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        /// <summary>
        /// The get requirement types met output model.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        [HttpGet]
        [Route("~/api/Leads/{leadId}/Requirements/GetRequirementTypesMet")]
        public List<LeadRequirementTypeOutputModel> GetRequirementTypesMet(Guid leadId, Guid campusId)
        {
            GetLeadRequirementInputModel filter = new GetLeadRequirementInputModel();
            filter.CampusId = campusId;
            filter.leadId = leadId;
            return this.bo.GetRequirementTypesMetOutputModel(leadId, filter);
        }

        /// <summary>
        /// The get have met requirement.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        [HttpGet]
        [Route("~/api/Leads/Requirements/GetHaveMetRequirement")]
        public string GetHaveMetRequirement(Guid leadId, Guid campusId)
        {
            return this.bo.HaveMetRequirements(leadId, campusId);
        }

        #endregion
    }
}
