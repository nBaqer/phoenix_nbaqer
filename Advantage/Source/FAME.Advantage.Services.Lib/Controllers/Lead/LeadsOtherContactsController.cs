﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadsOtherContactsController.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Controllers.Lead.LeadsOtherContactsController
// </copyright>
// <summary>
//   Controller that handles all the CRUD of Lead Other Contacts
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Web;
    using System.Web.Http;

    using AdvantageV1.Common;
    using BusinessLogic.Lead;
    using Domain.Infrastructure.Entities;
    using Infrastructure.Exceptions;
    using Messages.Lead;
    using Messages.Web;

    /// <summary>
    /// Controller that handles all the CRUD of Lead Other Contacts
    /// </summary>
    [RoutePrefix("api/Lead")]
    public class LeadsOtherContactsController : ApiController
    {
        #region Class Private members

        /// <summary>
        /// The represents the request object
        /// </summary>
        private readonly HttpRequest request;

        /// <summary>
        /// The lead repository.
        /// </summary>
        private readonly IRepository leadRepository;

        /// <summary>
        /// The repository with integer.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithInt;

        /// <summary>
        /// The bo.
        /// </summary>
        private readonly LeadBo bo;
        #endregion
        
        #region constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadsOtherContactsController"/> class.
        /// </summary>
        /// <param name="leadRepository">
        /// The lead repository.
        /// </param>
        /// <param name="repositoryWithInt">
        /// The repository with integer.
        /// </param>
        public LeadsOtherContactsController(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.request = HttpContext.Current.Request;
            this.leadRepository = leadRepository;
            this.repositoryWithInt = repositoryWithInt;
            this.bo = new LeadBo(leadRepository, repositoryWithInt);
        }
        #endregion

        /// <summary>
        /// The get lead other contacts.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        [HttpGet]
        [Route("GetLeadOtherContacts")]
        public Response<List<LeadOtherContactModel>> GetLeadOtherContacts([FromUri] GetLeadInputModel filter)
        {
            return this.bo.GetLeadOtherContacts(filter.LeadGuid);
        }

        /// <summary>
        /// The post lead other contacts.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Lead can't be null
        /// </exception>
        [HttpPost]
        [Route("PostLeadOtherContacts")]
        public Response<List<LeadOtherContactModel>> PostLeadOtherContacts(LeadOtherContactModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead can't be null");
            }
            else
            {
                Response<List<LeadOtherContactModel>> response = this.bo.CreateOrUpdateOtherContact(input);

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    response = this.bo.GetLeadOtherContacts(Guid.Parse(input.LeadId));
                }

                return response;
            }
        }

        /// <summary>
        /// The post lead other contacts phone.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Lead can't be null
        /// </exception>
        [HttpPost]
        [Route("PostLeadOtherContactsPhone")]
        public Response<List<LeadOtherContactModel>> PostLeadOtherContactsPhone(LeadOtherContactPhoneModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead can't be null");
            }
            else
            {
                Response<List<LeadOtherContactModel>> response = this.bo.CreateOrUpdateOtherContactPhone(input);

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    response = this.bo.GetLeadOtherContacts(Guid.Parse(input.LeadId));
                }

                return response;
            }
        }

        /// <summary>
        /// The post lead other contacts email.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Lead can't be null
        /// </exception>
        [HttpPost]
        [Route("PostLeadOtherContactsEmail")]
        public Response<List<LeadOtherContactModel>> PostLeadOtherContactsEmail(LeadOtherContactEmailModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead can't be null");
            }
            else
            {
                Response<List<LeadOtherContactModel>> response = this.bo.CreateOrUpdateOtherContactEmail(input);

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    response = this.bo.GetLeadOtherContacts(Guid.Parse(input.LeadId));
                }

                return response;
            }
        }

        /// <summary>
        /// The post lead other contacts address.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Lead can't be null
        /// </exception>
        [HttpPost]
        [Route("PostLeadOtherContactsAddress")]
        public Response<List<LeadOtherContactModel>> PostLeadOtherContactsAddress(LeadOtherContactAddressModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead can't be null");
            }
            else
            {
                Response<List<LeadOtherContactModel>> response = this.bo.CreateOrUpdateOtherContactAddress(input);

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    response = this.bo.GetLeadOtherContacts(Guid.Parse(input.LeadId));
                }

                return response;
            }
        }

        /// <summary>
        /// The delete lead other contacts.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Lead can't be null
        /// </exception>
        [HttpPost]
        [Route("DeleteLeadOtherContacts")]
        public Response<List<LeadOtherContactModel>> DeleteLeadOtherContacts(LeadOtherContactModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead can't be null");
            }
            else
            {
                Response<List<LeadOtherContactModel>> response = this.bo.DeleteLeadOtherContact(input);

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    response = this.bo.GetLeadOtherContacts(Guid.Parse(input.LeadId));
                }

                return response;
            }
        }

        /// <summary>
        /// The delete lead other contacts phone.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Lead can't be null
        /// </exception>
        [HttpPost]
        [Route("DeleteLeadOtherContactsPhone")]
        public Response<List<LeadOtherContactModel>> DeleteLeadOtherContactsPhone(LeadOtherContactPhoneModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead can't be null");
            }
            else
            {
                Response<List<LeadOtherContactModel>> response = this.bo.DeleteLeadOtherContactPhone(input);

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    response = this.bo.GetLeadOtherContacts(Guid.Parse(input.LeadId));
                }

                return response;
            }
        }

        /// <summary>
        /// The delete lead other contacts email.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Lead can't be null
        /// </exception>
        [HttpPost]
        [Route("DeleteLeadOtherContactsEmail")]
        public Response<List<LeadOtherContactModel>> DeleteLeadOtherContactsEmail(LeadOtherContactEmailModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead can't be null");
            }
            else
            {
                Response<List<LeadOtherContactModel>> response = this.bo.DeleteLeadOtherContactEmail(input);

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    response = this.bo.GetLeadOtherContacts(Guid.Parse(input.LeadId));
                }

                return response;
            }
        }

        /// <summary>
        /// The delete lead other contacts address.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Lead can't be null
        /// </exception>
        [HttpPost]
        [Route("DeleteLeadOtherContactsAddress")]
        public Response<List<LeadOtherContactModel>> DeleteLeadOtherContactsAddress(LeadOtherContactAddressModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead can't be null");
            }
            else
            {
                Response<List<LeadOtherContactModel>> response = this.bo.DeleteLeadOtherContactAddress(input);

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    response = this.bo.GetLeadOtherContacts(Guid.Parse(input.LeadId));
                }

                return response;
            }
        }

        /// <summary>
        /// The post lead other contacts comments.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Lead can't be null
        /// </exception>
        [HttpPost]
        [Route("PostLeadOtherContactsComments")]
        public Response<List<LeadOtherContactModel>> PostLeadOtherContactsComments(LeadOtherContactModel input)
        {
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Lead can't be null");
            }
            else
            {
                Response<List<LeadOtherContactModel>> response = this.bo.UpdateLeadOtherContactsComments(input);

                if (response.ResponseCode == (int)HttpStatusCode.OK)
                {
                    response = this.bo.GetLeadOtherContacts(Guid.Parse(input.LeadId));
                }

                return response;
            }
        }
    }
}
