﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadPriorWorkController.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the LeadPriorWorkController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Web.Http;
    using BusinessLogic.Catalogs;
    using BusinessLogic.Lead;
    using Domain.Infrastructure.Entities;

    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance;

    using Infrastructure.Exceptions;
    using Messages.Lead.PriorWork;

    /// <summary>
    /// The lead prior work controller.
    /// </summary>
    [RoutePrefix("api/Lead/LeadPriorWork")]
    public class LeadPriorWorkController : ApiController
    {
        /// <summary>
        /// The business object for Prior Work.
        /// </summary>
        private readonly LeadPriorWorkBo bo;

        /// <summary>
        /// The BO for Catalogs
        /// </summary>
        private readonly CatalogsBo cbo;

        private readonly UdfBo sdfBo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadPriorWorkController"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="leadRepository">
        /// repository GUID
        /// </param>
        /// <param name="repositoryWithInt">
        /// repository integer
        /// </param>
        public LeadPriorWorkController(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.cbo = new CatalogsBo(leadRepository, repositoryWithInt);
            this.bo = new LeadPriorWorkBo(leadRepository, repositoryWithInt);
            this.sdfBo = new UdfBo(leadRepository, repositoryWithInt);
        }

        /// <summary>
        ///  Get information about prior work from server.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// Command: 1 Get the prior work list.
        /// Command: 2 Get the list of AdTitles and JobStatus with campus Id.
        /// Command: 3 Get Drop Down content for plJobStatus (by campus and active)
        /// Command: 4 Get Drop Down the States of USA order by description
        /// Command: 5 Get the Drop Down Countries of USA
        /// Command: 6 Get the Prior Work Details and Contacts
        /// Command: 7 Get the Drop down list of Prefix filtered by Campus
        /// Command: 8 Get the custom feilds depending on the lead id
        /// Command 100: Poke test
        /// 100 Poke test
        /// </param>
        /// <returns>
        /// List of 
        /// The <see cref="IPriorWorkOutputModel"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Exception raised when something in the request is not valid
        /// Example filter is empty
        /// </exception>
        [HttpGet]
        [Route("Get")]
        public IPriorWorkOutputModel Get([FromUri]PriorWorkInputModel filter)
        {
            var output = new PriorWorkOutputModel { InputModel = filter };
            try
            {
                if (filter == null)
                {
                    throw new Exception("Filter can not be null");
                }

                switch (filter.Command)
                {
                    case 1: // Get the Prior Works with all information
                        {
                            if (filter.LeadId == null)
                            {
                                throw new ApplicationException("Please Select a Lead");
                            }

                            var priorList = this.bo.GetThePriorWorkItemList(filter);
                            output.PriorWorkItemList = priorList;
                            return output;
                        }

                    case 2: // Get Drop Down content for Ad Titles (by campus and active)
                        {
                            output.AdSchoolJobTitlesItemList = this.bo.GetSchoolJobtitlesList(filter);
                            return output;
                        }

                    case 3: // Get Drop Down content for plJobStatus (by campus and active)
                        {
                            output.JobStatusItemList = this.bo.GetJobStatusItemList(filter);
                            return output;
                        }

                    case 4:
                        {  // Get Drop Down the States of USA order by description

                            output.StatesList = this.cbo.GetAllStatesItems();
                            return output;
                        }

                    case 5:
                        {  // Get the Drop Down Countries of USA

                            output.CountriesList = this.cbo.GetCountriesByCampusId(filter);
                            return output;
                        }

                    case 6:
                        {  // Get the Prior Work Details and Contacts

                            output.PriorWorkDetailObject = this.bo.GetPriorWorkDetailsAndContacts(filter);
                            return output;
                        }

                    case 7:
                        { // Command: 7 Get the Drop down list of Prefix filtered by Campus
                            if (filter.CampusId == null)
                            {
                                throw new Exception("Campus can not be null");
                            }

                            output.PrefixList = this.cbo.GetPrefixByCampusId(filter.CampusId);
                            return output;
                        }

                    case 8:
                        {
                            if (filter.LeadId == null)
                            {
                                throw new Exception("Lead can not be null");
                            }

                            output.SdfList = this.sdfBo.GetSdfValues(filter.LeadId); 
                            return output;
                        }

                    case 100: // Poke Test
                        {
                            return new PriorWorkOutputModel();
                        }

                    default:
                        {
                            throw new Exception("Invalid Command");
                        }
                }
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// Example filter is empty
        /// <summary>
        /// Post operation.
        /// multiple operation of Insert, Update and Delete
        /// 1:  Insert a new prior Work
        /// 3:  Update The prior Work
        /// 4:  Update the Contact info
        /// 5:  Update the Prior Work Comment
        /// 6:  Update the job Responsibilities
        /// 7:  Delete the prior work
        /// 8:  Delete the Contact info
        /// 9:  Insert/Update custom fields
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The list of <see cref="IPriorWorkOutputModel"/>.
        /// </returns>
        /// <exception cref="HttpBadRequestResponseException">
        /// Exception raised when something in the request is not valid
        /// Example filter is empty
        /// </exception>
        [Route("Post")]
        public IPriorWorkOutputModel Post([FromBody] PriorWorkOutputModel filter)
        {
            this.ValidateFilter(filter);
            var output = new PriorWorkOutputModel
                             {
                                 InputModel = filter.InputModel,
                                 PriorWorkItemList = filter.PriorWorkItemList,
                                 PriorWorkDetailObject = filter.PriorWorkDetailObject
                             };
            try
            {
                switch (filter.InputModel.Command)
                {
                    case 1: // Insert a new prior Work
                        {
                            this.bo.InsertTheNewPriorWork(filter);
                            var priorList = this.bo.GetThePriorWorkItemList(filter.InputModel);
                            output.PriorWorkItemList = priorList;
                            break;
                        }

                    case 2: // Insert a new Contact Info
                        {
                            this.bo.InsertTheNewContactInfo(filter);
                            break;
                        }

                    case 3: // Update The prior Work
                        {
                            output.PriorWorkItemList = new List<PriorWorkItemOutputModel>();
                            var changed = this.bo.UpdatePriorWork(filter);
                            output.PriorWorkItemList.Add(changed);
                            break;
                        }

                    case 4: // Update the Contact info
                        {
                            this.bo.UpdateContactInfo(filter);
                            break;
                        }

                    case 5: // Update the Comment
                        {
                            this.bo.UpdateTheComment(filter);
                            break;
                        }

                    case 6: // Update the job Responsibilities
                        {
                            this.bo.UpdateJobResponsibilities(filter);
                            break;
                        }

                    case 7: // Delete the prior work
                        {
                            this.bo.DeletePriorWork(filter);
                            break;
                        }

                    case 8: // Delete the Contact info
                        {
                            this.bo.DeleteContactInfo(filter);
                            break;
                        }

                    case 9: // Insert/Update Sdf
                        {
                            this.sdfBo.SaveSdfValues(filter.InputModel.UserId, filter.InputModel.LeadId, filter.SdfList);
                            break;
                        }
                    ////case 9: // Delete the Comment
                    ////    {
                    ////        this.bo.DeleteTheComment(filter);
                    ////        break;
                    ////    }

                    ////case 10: // Delete the job Responsibilities
                    ////    {
                    ////        this.bo.DeleteJobResponsabilities(filter);
                    ////        break;
                    ////    }

                    default:
                        {
                            throw new Exception("Invalid Command");
                        }
                }

                return output;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// The filter to validate.
        /// </summary>
        /// <param name="filter">
        ///  The filter.
        /// </param>
        /// <exception cref="HttpBadRequestResponseException">
        /// If the filter is not valid
        /// </exception>
        private void ValidateFilter(IPriorWorkOutputModel filter)
        {
            string test = "Filter";
            if (filter != null)
            {
                test = "Input Model";
                if (filter.InputModel != null)
                {
                    test = "Lead ID";
                    if (filter.InputModel.LeadId != null)
                    {
                        test = "User ID";
                        if (filter.InputModel.UserId != null)
                        {
                              return;
                        }
                    }
                }
            }

            throw new HttpBadRequestResponseException(string.Format("The parameter {0} can not be null", test));
        }
    }
}
