﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEducationController.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Controllers.Lead.LeadEducationController
// </copyright>
// <summary>
//   The lead education controller, also known as lead prior education controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Web.Http;
    using AdvantageV1.Common;
    using BusinessLogic.Lead;
    using Domain.Infrastructure.Entities;
    using Messages.Lead.Education;
    using Messages.Web;

    /// <summary>
    /// The lead education controller, also known as lead prior education controller.
    /// </summary>
    [RoutePrefix("api/Lead/Education")]
    public class LeadEducationController : ApiController
    {
        #region Class Private members

        /// <summary>
        /// The NHibernate repository.
        /// </summary>
        private readonly IRepository repository;

        /// <summary>
        /// The repository with type id.
        /// </summary>
        private readonly IRepositoryWithTypedID<int> repositoryWithTypeId;

        /// <summary>
        /// The Business Object.
        /// </summary>
        private readonly LeadEducationBo leadEducationBo;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadEducationController"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="repositoryWithTypeId">
        /// The repository with type id.
        /// </param>
        public LeadEducationController(IRepository repository, IRepositoryWithTypedID<int> repositoryWithTypeId)
        {
            this.repository = repository;
            this.repositoryWithTypeId = repositoryWithTypeId;
            this.leadEducationBo = new LeadEducationBo(repository, repositoryWithTypeId);
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle Exception
        /// </exception>
        [HttpGet]
        [Route("Get")]
        public LeadEducationOutputModel Get([FromUri] LeadEducationFilterInputModel input)
        {
            try
            {
                return this.leadEducationBo.Get(input);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Un-handle Exception
        /// </exception>
        [HttpGet]
        [Route("Find")]
        public List<LeadEducationOutputModel> Find([FromUri] LeadEducationFilterInputModel input)
        {
            try
            {
                return this.leadEducationBo.Find(input);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The post.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        [HttpPost]
        [Route("Post")]
        public Response<string> Post([FromBody] LeadEducationInputModel input)
        {
            try
            {
                return this.leadEducationBo.Insert(input);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The put.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        [HttpPut]
        [Route("Put")]
        public Response<string> Put([FromBody] LeadEducationInputModel input)
        {
            try
            {
                return this.leadEducationBo.Update(input);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        [HttpDelete]
        [Route("Delete")]
        public void Delete([FromUri] LeadEducationFilterInputModel input)
        {
            try
            {
                this.leadEducationBo.Delete(input);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
