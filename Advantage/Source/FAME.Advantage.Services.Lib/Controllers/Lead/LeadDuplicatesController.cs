﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadDuplicatesController.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Controller Services for Lead Duplicated
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Controllers.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Web.Http;
    using BusinessLogic.Lead;
    using Domain.Infrastructure.Entities;
    using Infrastructure.Exceptions;
    using Messages.Lead;

    /// <summary>
    /// Controller Services for Lead Duplicated
    /// </summary>
    [RoutePrefix("api/Lead/LeadDuplicates")]
    public class LeadDuplicatesController : ApiController
    {
        /// <summary>
        ///  The bo.
        /// </summary>
        private readonly LeadDuplicatesBo bo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadDuplicatesController"/> class. 
        /// Constructor
        /// </summary>
        /// <param name="leadRepository">
        /// repository GUID
        /// </param>
        /// <param name="repositoryWithInt">
        /// repository integer
        /// </param>
        public LeadDuplicatesController(IRepository leadRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.bo = new LeadDuplicatesBo(leadRepository, repositoryWithInt);
        }

        /// <summary>
        /// Get the list of possible duplicates
        /// </summary>
        /// <returns>
        /// Return GUID new duplicate - GUID Possible Duplicate - Possible Duplicate full name
        /// </returns>
        [HttpGet]
        [Route("Get")]
        public IEnumerable<LeadNewDuplicatesOutputModel> Get()
        {
            // Test the filter
            try
            {
                IEnumerable<LeadNewDuplicatesOutputModel> list = this.bo.GetNewDuplicatesList();
                return list;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Return three Leads a title, lead A y Lead B
        /// </summary>
        /// <param name="filter">the GUID of lead A and Lead B</param>
        /// <returns>title Lead, Lead A Lead B</returns>
        [HttpGet]
        [Route("GetDuplicate")]
        public IEnumerable<LeadDuplicatesOutputModel> GetDuplicate([FromUri] LeadDuplicatesInputFilter filter)
        {
            // Test the filter
            if (filter == null || filter.LeadNewGuid == null || filter.LeadDuplicateGuid == null)
            {
                throw new HttpBadRequestResponseException("Filter has not value");
            }

            try
            {
                IEnumerable<LeadDuplicatesOutputModel> list = this.bo.GetDuplicate(filter);
                return list;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }

        /// <summary>
        /// Execute the Action over duplicate A B (depending of Command parameter)
        /// return the updated list of new duplicates
        /// </summary>
        /// <param name="filter">
        /// Command: Possible values:
        /// 1: Update Advantage with Lead A
        /// 2: Delete Lead A
        /// 3: Create new Lead with Lead A
        /// </param>
        /// <returns>The update list of new duplicates</returns>
        [HttpGet]
        [Route("ButtomCommandDuplicate")]
        public IEnumerable<LeadNewDuplicatesOutputModel> ButtomCommandDuplicate([FromUri] LeadDuplicatesInputFilter filter)
        {
            // Test the filter
            if (filter == null || filter.Command == 0 || filter.LeadNewGuid == null || filter.LeadDuplicateGuid == null || filter.UserId == null)
            {
                throw new HttpBadRequestResponseException("Filter value missing");
            }

            try
            {
                IEnumerable<LeadNewDuplicatesOutputModel> list = this.bo.ExecuteDuplicateCommand(filter);
                return list;
            }
            catch (Exception ex)
            {
                throw ManagerExceptions.ManageResponseExceptions(ex);
            }
        }
    }
}
