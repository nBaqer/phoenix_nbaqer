﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.Wapi.Voyant;
using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff;
using FAME.Advantage.Services.Lib.BusinessLogic.Voyant;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

namespace FAME.Advantage.Services.Lib.Controllers.Wapi.Voyant
{
    [RoutePrefix("api/Wapi/Voyant/Voyant")]
    public class VoyantController : ApiController
    {
        private readonly IRepositoryWithTypedID<int> repository;
        private IRepositoryWithTypedID<Guid> repositoryguid;
        private readonly VoyantBo bo;

        public VoyantController(IRepository repository, IRepositoryWithTypedID<int> repositorytyped)
        {
           this.repositoryguid = repository;
           bo = new VoyantBo(repositoryguid);
           this.repository = repositorytyped;
        }


        /// <summary>
        /// Get a Fake empty object for test purpose.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetFake")]
        public IList<VoyantStudentOutputModel> GetFake()
        {
            try
            {
                var list = new List<VoyantStudentOutputModel>();
                var student = VoyantStudentOutputModel.Factory();
                student.AcademicsDataList.Add(StudentAcademicOutputModel.Factory());
                student.EnrollmentDataList.Add(StudentEnrollmentOutputModel.Factory());
                student.FacultyDataList.Add(StudentFacultyOutputModel.Factory());
                list.Add(student);
                return list;
            }
            catch (Exception ex)
            {
                throw new HttpNotFoundResponseException(ex.Message);
                
            }
        }

        /// <summary>
        /// Get a Fake empty object for test purpose.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Get")]
        public IList<VoyantStudentOutputModel> Get([FromUri] VoyantStudentInputModel filter)
        {
            if (filter == null )
            {
                throw new HttpBadRequestResponseException("Please supply filter");
            }

            try
            {
                IList<VoyantStudentOutputModel> resultList = bo.GetVoyantStudentInfo(filter);
                return resultList;
            }
            catch (Exception ex)
            {
                throw new HttpNotFoundResponseException(ex.Message);

            }
        }

        [HttpGet]
        [Route("GetVoyantDash")]
        public string GetVoyantDash()
        {
            try
            {
                var result = ConfigurationAppSettings.Setting("WapiVoyantAddress", repository);
                return result;
            }
            catch (Exception ex)
            {
                throw new HttpNotFoundResponseException(ex.Message);

            }
        }



        [HttpPost]
        [Route("Post")]
        public string Post([FromBody] VoyantStudentInputModel filter)
        {
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("Please supply filter");
            }

            if (string.IsNullOrEmpty(filter.DashboardUrl))
            {
                throw new HttpBadRequestResponseException("Please Dashboard URL can not be empty");
            }

            try
            {
                var result = ConfigurationAppSettings.StoreSettingString("WapiVoyantAddress", repository, filter.DashboardUrl);
                return result;
            }
            catch (Exception ex)
            {
                throw new HttpNotFoundResponseException(ex.Message);
            }
        }

   
    }
}
