﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.Wapi.WapiLeads;
using FAME.Advantage.Services.Lib.BusinessLogic.Wapi;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

namespace FAME.Advantage.Services.Lib.Controllers.Wapi.Leads
{
    /// <summary>
    /// API Controller to WAPI Leads Services
    /// </summary>
    [RoutePrefix("api/Wapi/Leads/WapiLeads")]
    public class WapiLeadsController : ApiController
    {
        private const string X_PARAMETER_NULL = "The input parameter can not be null";
        // ReSharper disable NotAccessedField.Local
        private readonly IRepositoryWithTypedID<int> repository;
        // ReSharper restore NotAccessedField.Local
        private readonly WapiLeadsBo bo;

        public WapiLeadsController(IRepository repository, IRepositoryWithTypedID<int> repositorytyped)
        {
            bo = new WapiLeadsBo(repository);
            this.repository = repositorytyped;
        }

        /// <summary>
        /// Get the prospectID of the last lead received by the system
        /// </summary>
        /// <param name="vendorCode">
        /// The code of the vendor
        /// </param>
        /// <returns>
        /// Return the prospectId of the last lead received by the system
        /// if it is not found any return empty string
        /// </returns>
        [HttpGet]
        [Route("GetLastLeadImportedSandBox")]
        public string GetLastLeadImportedSandBox(string vendorCode)
        {
            try
            {
                var prospectid = bo.GetLastLeadSandBox(vendorCode);
                return prospectid;
            }
            catch (Exception ex)
            {
                throw new HttpNotFoundResponseException(ex.Message);
            }
        }

        /// <summary>
        /// Receive the lead for Vendors, process they and return the rejected leads
        /// </summary>
        /// <param name="leadsList">The imported Leads list.</param>
        /// <returns>A list of rejected Leads.</returns>
        [HttpPost]
        [Route("PostLeadsSandBox")]
        public IList<WapiLeadsOutputModel> PostLeadsSandBox(IList<WapiLeadsOutputModel> leadsList)
        {
            try
            {
                if (leadsList == null) throw new HttpRequestException(X_PARAMETER_NULL);

                IList<WapiLeadsOutputModel> rejecteds = bo.ProcessListOfLeadsSandBox(leadsList);
                return rejecteds;
            }
            catch (Exception ex)
            {
                throw new HttpNotFoundResponseException(ex.Message);
            }
        }


        [HttpGet]
        #region Production procedures

        [Route("Get")]
        public string Get()
        {
            //TODO Check VendorCode
            // TODO Validate Fields
            return string.Empty;
        }

        public IList<WapiLeadsOutputModel> Post(IList<WapiLeadsOutputModel> leadsList)
        {
            //TODO Check VendorCode
            //TODO Validate Fields
            return null;
        }

        #endregion

    }
}
