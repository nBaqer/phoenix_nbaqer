﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.SystemStuff.Maintenance;
using FAME.Advantage.Messages.Widget;
using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Maintenance;
using FAME.Advantage.Services.Lib.BusinessLogic.Users;
using FAME.Advantage.Services.Lib.BusinessLogic.Widgets;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

namespace FAME.Advantage.Services.Lib.Controllers.Widgets
{
    using System.ServiceProcess;

    [RoutePrefix("api/Widgets/VoyantWidget")]
    public class VoyantWidgetController : ApiController
    {
        private readonly VoyantWidgetBo bo;
        private readonly WapiWindowsServiceBo bows;
        private readonly WapiOperationSettingsBo boos;
        private readonly UserRolesBo borol;

        public VoyantWidgetController(IRepository repository, IRepositoryWithTypedID<int> repositorytyped)
        {
            IRepositoryWithTypedID<Guid> repositoryguid = repository;
            IRepositoryWithTypedID<int> repositoryInt = repositorytyped;
            this.bo = new VoyantWidgetBo(repositoryguid, repositoryInt);
            this.bows = new WapiWindowsServiceBo();
            this.boos = new WapiOperationSettingsBo(repositoryInt);
            this.borol = new UserRolesBo(repositoryguid);
        }

        [HttpGet]
        [Route("Get")]
        public VoyantWidgetOutputModel Get([FromUri]VoyantWidgetInputFilter filter)
        {
            if (filter == null)
            {
                throw new HttpBadRequestResponseException("Please supply filter");
            }

            try
            {
                var boosfilter = new WapiOperationSettingsInputModel
                {
                    IdOperation = 0,
                    OnlyActives = 1,
                    OperationMode = string.Empty
                };

                // TODO: Hard coded, can be in the future in the database
                // per US6516 the authorized roles are:
                // System Administrator (1)  Academic Advisors (4) Director of Academic (13) support has access by default
                var roles = new List<int> { 1, 4, 13 };
                var sc = new ServiceController(filter.WindowServiceName);
                var serviceStatus = this.bows.GetStatusOfWindowsService(filter.WindowServiceName, sc);
                var existsOperationSettings = this.boos.GetOperationSettings(boosfilter).Any();
                var userIsInRolAuthorized = this.borol.IsUserAuthorized(roles, filter.UserGuid, filter.CampusGuid);

                VoyantWidgetOutputModel result = this.bo.GetVoyantWidgetConfigParameters(serviceStatus, existsOperationSettings, userIsInRolAuthorized);
                return result;
            }
            catch (Exception ex)
            {
                throw new HttpNotFoundResponseException(ex.Message);
            }


        }


    }
}
