﻿using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Requirements.Documents;
using FAME.Advantage.Messages.DocumentStatuses;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using FAME.Advantage.Messages.Requirements.Document;

namespace FAME.Advantage.Services.Lib.Controllers.Students.Requirements.Documents
{
    [RoutePrefix("api/Students/EnrollmentsDocuments/Requirements/DocumentStatuses")]
    public class DocumentStatusesController : ApiController
    {
        //represents data store
        private readonly IRepository _repository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        public DocumentStatusesController(IRepository repository)
        {
            _repository = repository;
        }
        

        /// <summary>
        /// Get Document Statuses
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IEnumerable<DocumentStatusOutputModel> Get([FromUri] GetDocumentStatusInputModel filter)
        {
            var documentStatuses = _repository.Query<DocumentStatus>();

            if (filter.IsApproved.HasValue && filter.IsApproved == true)
                documentStatuses = documentStatuses.Where(n => n.SystemDocumentStatus.ID == 1);
            
            return Mapper.Map<IEnumerable<DocumentStatusOutputModel>>(documentStatuses.ToArray().OrderByDescending(n=>n.IsApproved));
        }
    }
}
