﻿using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student;
using FAME.Advantage.Domain.Student.Requirements;
using FAME.Advantage.Domain.Student.Requirements.Documents;
using FAME.Advantage.Domain.Users.UserRoles;
using FAME.Advantage.Messages.Student.Requirements;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;


namespace FAME.Advantage.Services.Lib.Controllers.Students.Requirements.Documents
{
    public class DocumentRequirementsController : ApiController
    {
        //data repository used to work with data store
        private readonly IRepository _repository;

        /// <summary>
        /// default constructor
        /// </summary>
        /// <param name="repository"></param>
        public DocumentRequirementsController(IRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Get requirements for all students
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<DocumentRequirementOutputModel> GetAll([FromUri] GetDocumentRequirementInputModel filter)
        {
            return Get(filter, Guid.Empty);
        }

        /// <summary>
        /// Get requirements for one student
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<DocumentRequirementOutputModel> GetByStudentId(Guid studentId, [FromUri] GetDocumentRequirementInputModel filter)
        {
            return Get(filter, studentId);
        }


        /// <summary>
        /// Main get method, if student id is empty get for all students.
        /// If student id has value get for specific student
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="studentId"></param>
        /// <returns></returns>
        private IEnumerable<DocumentRequirementOutputModel> Get(GetDocumentRequirementInputModel filter, Guid studentId)
        {   
         
            //---------------------------------------------------------
            //Validate required fields
            //---------------------------------------------------------
            if (filter == null)
                throw new HttpBadRequestResponseException("No search parameters passed.");

            if (filter.UserId.IsEmpty())
                throw new HttpBadRequestResponseException("UserId is required.");

            if (filter.CampusId.IsEmpty())
                throw new HttpBadRequestResponseException("CampusId is required.");

            if (studentId.IsEmpty() && String.IsNullOrEmpty(filter.StatusCode))
                throw new HttpBadRequestResponseException("Either StudentId or Status Code required.");
            //---------------------------------------------------------

            
            //Get requirements by calling methods for each type of document and joining them together
            var requirements = GetSchoolLevelRequirements(filter.StatusCode)
                                    .Union(GetRequirementGroupLevelRequirements(studentId, filter.StatusCode))
                                    .Union(StudentGroupLevelRequirements(studentId, filter.StatusCode))
                                    .Union(ProgramVersionLevelRequirements(studentId, filter.StatusCode))
                                        .Where(r => r.CampusGroup.Campuses.Any(c => c.ID == filter.CampusId));

            
            // Filter out financial aid docs if not a financial aid user.
            // We call a method for the system role domain to check if the domain
            // is either a Financial Aid Advisor or a Director of Financial aid
            bool showFinancialAid = ((Enumerable.Any(_repository.Query<UserRolesCampusGroupBridge>()
                                        .Where(n => n.CampusGroup.Campuses.Any(c => c.ID == filter.CampusId) && n.User.ID == filter.UserId)
                                            .Select(n => n.Role).Distinct(),
                                                r => r.SystemRole.IsFinancialAidEnabled())));
            
            
            SetFinancialAidVisibilityForEachRequirement(requirements, showFinancialAid);


            //Call method to set document object for each requirement
            SetDocumentForEachRequirementBasedOnStudentId(requirements, studentId);

            
            var output = requirements.Select(requirement => 
            {
                var outputModel = new DocumentRequirementOutputModel(studentId);
                return Mapper.Map(requirement, outputModel);
            });

            return output.ToArray().Distinct().OrderBy(n=>n.Description);
        }

        /// <summary>
        /// Return school level requirements
        /// </summary>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        private IEnumerable<DocumentRequirement> GetSchoolLevelRequirements(string statusCode)
        {
            var requirements = _repository.Query<DocumentRequirement>()
                .Where(DocumentRequirement.IsSchoolLevel)
                .ToArray()
                .ForEach(x => x.SetRequirementType(RequirementEnums.RequirementTypeLevel.School));       
            
            if (!String.IsNullOrEmpty(statusCode))
                requirements = requirements.Where(n => n.Status.StatusCode == statusCode);

            return requirements;
        }

        /// <summary>
        /// return requirements for requirement groups
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        private IEnumerable<DocumentRequirement> GetRequirementGroupLevelRequirements(Guid studentId, string statusCode)
        {
            var student = _repository.Get<Student>(studentId);

            var requirements = student.Enrollments
                                   .SelectMany(e => e.StudentGroupBridges)
                                       .Select(x => x.StudentGroups)
                                           .SelectMany(x => x.StudentGroupRequirementGroupBridge)
                                               .Select(x => x.RequirementGroup)
                                                   .SelectMany(x => x.RequirementGroupDefinitions)
                                                       .Select(x => x.Requirement).Distinct()
                                                        .Where(n => n.SetRequirementType(RequirementEnums.RequirementTypeLevel.RequirementGroup)).Distinct();

            if (!String.IsNullOrEmpty(statusCode))
                requirements = requirements.Where(n => n.Status.StatusCode == statusCode);

            return requirements;
        }


        /// <summary>
        /// return requirements for student groups
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        private IEnumerable<DocumentRequirement> StudentGroupLevelRequirements(Guid studentId, string statusCode)
        {
            var student = _repository.Get<Student>(studentId);           

            var requirements = student.Enrollments
                                    .SelectMany(e => e.StudentGroupBridges)
                                        .Select(x => x.StudentGroups)
                                            .SelectMany(x => x.RequirementLeadGroupBridge)
                                                .Select(x => x.EffectiveDates).Where(n => n != null)
                                                    .Select(x => x.Requirement)
                                                        .Where(n => n != null && n.SetRequirementType(RequirementEnums.RequirementTypeLevel.StudentGroup)).Distinct();

            if (!String.IsNullOrEmpty(statusCode))
                requirements = requirements.Where(n => n.Status.StatusCode == statusCode);

            return requirements;
        }


        /// <summary>
        /// return requirements for program versions
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        private IEnumerable<DocumentRequirement> ProgramVersionLevelRequirements(Guid studentId, string statusCode)
        {
            var student = _repository.Get<Student>(studentId);                       

            var requirements = student.Enrollments
                                .Select(x => x.ProgramVersion).ToArray()
                                    .SelectMany(x => x.RequirementProgramVersionBridge)
                                        .SelectMany(x => x.Requirements.Where(n => n != null)
                                            .Where(n => n.SetRequirementType(RequirementEnums.RequirementTypeLevel.Program))).Distinct();

            if (!String.IsNullOrEmpty(statusCode))
                requirements = requirements.Where(n => n.Status.StatusCode == statusCode);

            return requirements;
        }


        /// <summary>
        /// Add the document object for each requirement if it exists
        /// </summary>
        /// <param name="requirements"></param>
        /// <param name="studentId"></param>
        private void SetDocumentForEachRequirementBasedOnStudentId(IEnumerable<DocumentRequirement> requirements, Guid studentId)
        {
            foreach (var documentRequirement in requirements)
            {
                var documentForRequirement =
                    _repository.Query<Document>()
                        .Where(x => x.Student.ID == studentId)
                        .Where(x => x.DocumentRequirement.ID == documentRequirement.ID).SingleOrDefault();

                documentRequirement.SetDocument(documentForRequirement ?? Document.Empty());
            }
        }

        /// <summary>
        /// set visiblity for finanical aid requirements
        /// </summary>
        /// <param name="requirements"></param>
        /// <param name="show"></param>
        private void SetFinancialAidVisibilityForEachRequirement(IEnumerable<DocumentRequirement> requirements, bool show)
        {
            foreach (var documentRequirement in requirements)
                documentRequirement.ShowUpdateForFinancialAid(show);
        }
       
    }
}
