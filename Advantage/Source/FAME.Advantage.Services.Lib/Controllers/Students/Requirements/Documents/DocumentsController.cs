﻿using AutoMapper;
using FAME.Advantage.Domain.Campuses;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Domain.Student.Requirements;
using FAME.Advantage.Domain.Student.Requirements.Documents;
using FAME.Advantage.Domain.Users.UserRoles;
using FAME.Advantage.Messages;
using FAME.Advantage.Messages.Student.Requirements.Document;
using FAME.Advantage.Messages.Students.Requirements.Document;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;
using FAME.Advantage.Services.Lib.Infrastructure.ResponseMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Controllers.Students.Requirements.Documents
{
    public class DocumentsController : ApiController
    {
        //represents data store
        private readonly IRepository _repository;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="repository"></param>
        public DocumentsController(IRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Get documents for all students
        /// GET URL: api/Students/Enrollments/Requirements/Documents
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        public PagedOutputModel<IEnumerable<DocumentOutputModel>> GetAll([FromUri] GetDocumentInputModel filter)
        {
            return Get(filter);
        }

        /// <summary>
        /// Get documents for one student
        /// GET: api/Students/{studentId}/Enrollments/Requirements/Documents
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        public PagedOutputModel<IEnumerable<DocumentOutputModel>> GetByStudentId(Guid studentId, [FromUri] GetDocumentInputModel filter)
        {
            return Get(filter, studentId);
        }

        /// <summary>
        /// Insert a new document
        /// POST: api/Students/{studentId}/Enrollments/Requirements/{requirementId}/Documents 
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="requirementId"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Post(Guid studentId, Guid requirementId, PostDocumentInputModel input)
        {
            var student = _repository.Load<Student>(studentId);
            
            var docStatus = _repository.Get<DocumentStatus>(input.DocumentStatusId.Value);

            var documentRequirement = _repository.Load<DocumentRequirement>(requirementId);

            var document = new Document(student, docStatus, documentRequirement, input.DateRequested, input.DateReceived, DateTime.Now, this.GetUserFromHeader());

            _repository.Save(document);

            var output = Mapper.Map<PostDocumentOutputModel>(document);
            return new HttpCreatedResponseMessage<PostDocumentOutputModel>(output, Request.RequestUri, document.ID.ToString());
        }

        /// <summary>
        /// Update an existing document
        /// PUT: api/Students/Enrollments/Requirements/Documents/{StudentDocumentId}
        /// </summary>
        /// <param name="studentDocumentId"></param>
        /// <param name="inputModel"></param>
        [HttpPut]
        public void Put(Guid studentDocumentId, PutDocumentInputModel inputModel)
        {
            if (studentDocumentId.IsEmpty())
                throw new HttpBadRequestResponseException("No documentId to update.");

            if (inputModel == null)
                throw new HttpBadRequestResponseException("No data to update.");

            var studentDocument = _repository.Get<Document>(studentDocumentId);

            if (this.IsItInTheRequest<PutDocumentInputModel>(x => x.DocumentStatusId))
            {

                var documentStatus = _repository.Get<DocumentStatus>(inputModel.DocumentStatusId.Value);
                studentDocument.SetStatus(documentStatus);
            }

            if (this.IsItInTheRequest<PutDocumentInputModel>(x => x.DateRequested))
                studentDocument.SetDateRequested(inputModel.DateRequested);

            if (this.IsItInTheRequest<PutDocumentInputModel>(x => x.DateReceived))
                studentDocument.SetDateReceived(inputModel.DateReceived);

            studentDocument.SetModUser(this.GetUserFromHeader());

            _repository.Save(studentDocument);
        }

        
        /// <summary>
        /// Get method to retrieve documents, either with or without student id
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="studentId"></param>
        /// <returns></returns>
        private PagedOutputModel<IEnumerable<DocumentOutputModel>> Get(GetDocumentInputModel filter, Guid? studentId = null)
        {
            if (filter == null)
                throw new HttpBadRequestResponseException("No search parameters passed.");

            if (filter.CampusId.IsEmpty())
                throw new HttpBadRequestResponseException("CampusId is required.");

            if (filter.UserId.IsEmpty())
                throw new HttpBadRequestResponseException("UserId is required.");

            var query = _repository.Query<Document>().Where(n => n.Student.Enrollments.Any(r => r.CampusId == filter.CampusId));

            var campusGroupsIds = _repository.Query<Campus>().Where(n => n.ID == filter.CampusId).SelectMany(n => n.CampusGroup).Select(n => n.ID);

            //remove financial aids docs if not a financial aid user
            if (!(Enumerable.Any(_repository.Query<UserRolesCampusGroupBridge>()
                                .Where(n => n.CampusGroup.Campuses.Any(c => c.ID == filter.CampusId) && n.User.ID == filter.UserId)
                                    .Select(n => n.Role).Distinct(),
                                        r => r.SystemRole.IsFinancialAidEnabled())))
            {
                query = query.Where(n => n.DocumentRequirement.SystemModule.Code != "FA");
            }
                

            

            if (!studentId.IsEmpty())
                query = query.Where(doc => doc.Student.ID == studentId);

            if (filter.IsApproved.HasValue)
                query = query.Where(doc => doc.DocumentStatus.IsApproved == filter.IsApproved.Value);


            if (!filter.ProgramVersionId.IsEmpty())
                query = query.Where(doc => doc.Student.Enrollments.Any(r => r.ProgramVersion.ID == filter.ProgramVersionId));

            if (!filter.EnrollmentStatusId.IsEmpty())
                query = query.Where(doc => doc.Student.Enrollments.Any(r => r.EnrollmentStatus.ID == filter.EnrollmentStatusId));

            if (filter.StartDate != null)
                query = query.Where(n => n.Student.Enrollments.Any(r => r.StartDate == filter.StartDate));

            if (!filter.StudentGroupId.IsEmpty())
            {                
                var studentIds = _repository.Query<Enrollment>().Where(r => _repository.Query<StudentGroupBridge>()
                                    .Where(n => n.StudentGroupId == filter.StudentGroupId)
                                        .Select(n => n.StudentEnrollmentId)
                                            .Contains(r.ID))
                                                .Select(n => n.StudentId);

                query = query.Where(n => studentIds.Contains(n.Student.ID));
            }

            if (filter.ShowLastValue != 1000)
            {
                double showValue = 30d;
                if (filter.ShowLastValue.HasValue)
                    showValue = (double)filter.ShowLastValue;

                query = query.Where(n => n.DateModified > DateTime.Now.Date.AddDays(-showValue));
            }

            

            //only get documents where a file has been updated.
            if(filter.FileUploaded.HasValue && filter.FileUploaded == true)
                query = query.Where(n => n.Files.Count() > 0);

            var count = query.Count();

            var documents = query
                .Page(filter.Page, filter.PageSize)
                .ToArray();

            return new PagedOutputModel<IEnumerable<DocumentOutputModel>>(count, Mapper.Map<IEnumerable<DocumentOutputModel>>(documents
                                                                                                                                .OrderBy(n=>n.DateModified)
                                                                                                                                .ThenBy(n=>n.Student.LastName)
                                                                                                                                .ThenBy(n=>n.Student.FirstName)));
        }
    }
}
