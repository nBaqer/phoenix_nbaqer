﻿using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Requirements.Documents;
using FAME.Advantage.Domain.Requirements.Documents.File;
using FAME.Advantage.Messages.Requirements.Document.File;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using FAME.Advantage.Services.Lib.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Controllers.Students.Requirements.Documents.Files
{
    [RoutePrefix("api/Students/Enrollments/Requirements/Documents")]
    public class FilesController : ApiController
    {
        private readonly IRepository _repository;

        public FilesController(IRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        //GET: api/Students/Enrollments/Requirements/Documents/{documentId}/Files
        [Route("{documentId}/Files")]
        public IEnumerable<FileOutputModel> GetByDocumentId(Guid documentId)
        {
            var files = _repository.Query<File>().Where(file => file.Document.ID == documentId).OrderByDescending(file => file.DateCreated);

            return Mapper.Map<IEnumerable<FileOutputModel>>(files);
        }

        [HttpGet]
        //GET: api/Students/Enrollments/Requirements/Documents/Files/{fileId}/Contents
        [Route("Files/{fileId}/Contents")]
        public HttpResponseMessage GetContentsById(Guid fileId)
        {
            var file = _repository.Get<File>(fileId);

            return BuildResponseMessageForFileBinaries(file);
        }

        [HttpPost]
        //POST: api/Students/Enrollments/Requirements/Documents/{documentId}/Files/Contents
        [Route("{documentId}/Files/Contents")]
        public void Post(Guid documentId)
        {
            var modUser = HttpContext.Current.Request.Form["ModUser"];
            if (string.IsNullOrWhiteSpace(modUser)) throw new HttpBadRequestResponseException("modUser is required");

            if (!Request.Content.IsMimeMultipartContent()) throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            var uploadifyService = new UploadifyService(Request);

            var fileName = uploadifyService.GetFileName();
            var fileContent = uploadifyService.GetFileBytes();

            ValidateFile(fileName, fileContent);

            var document = _repository.Load<Document>(documentId);
            var file = new File(document, fileName, modUser, fileContent);

            _repository.Save(file);
        }

        [HttpDelete]
        [Route("File/{fileId}/Delete")]
        public void Delete(Guid fileId)
        {
            var file = _repository.Get<File>(fileId);
            _repository.Delete(file);
        }

        private static HttpResponseMessage BuildResponseMessageForFileBinaries(File file)
        {
            var streamContent = new ByteArrayContent(file.Content);
            streamContent.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            var contentDispositionHeader = new ContentDispositionHeaderValue("attachment") { FileName = file.FileName };
            streamContent.Headers.ContentDisposition = contentDispositionHeader;
            var responseMessage = new HttpResponseMessage { Content = streamContent };
            return responseMessage;
        }

        private static void ValidateFile(string fileName, byte[] fileContents)
        {
            try
            {
                var fileUploadValidator = new FileUploadValidationService(fileName, fileContents);
                fileUploadValidator.ValidateFileForUpload();
            }
            catch (FileUploadValidatorException)
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
        }
    }
}
