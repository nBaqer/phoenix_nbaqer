﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Requirements;
using FAME.Advantage.Messages.Requirements;

namespace FAME.Advantage.Services.Lib.Controllers.Students.Requirements.Test
{
    public class TestRequirementsController : ApiController
    {
        private readonly IRepository _repository;

        public TestRequirementsController(IRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<TestRequirementOutputModel> Get()
        {
            var requirements = _repository.Query<TestRequirement>().ToArray();

            return Mapper.Map<IEnumerable<TestRequirementOutputModel>>(requirements);
        }
    }
}
