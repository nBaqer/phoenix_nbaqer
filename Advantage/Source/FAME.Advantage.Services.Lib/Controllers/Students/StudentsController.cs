﻿using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages;
using FAME.Advantage.Messages.Student;
using FAME.Advantage.Messages.MostRecentlyUsed;
using FAME.Advantage.Messages.Students;
using FAME.Advantage.Services.Lib.BusinessLogic.Students;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Controllers.Students
{
    [RoutePrefix("api/Students")]
    public class StudentsController : ApiController
    {
        //represents data store
        private readonly IRepository _repository;

        //represents the rquest object
        private readonly HttpRequest _request = null;
        private readonly IRepositoryWithTypedID<int> _repositoryWithInt;

        private readonly StudentBo _bo = null;

        public StudentsController(IRepository studentRepository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            _repository = studentRepository;
            _request = HttpContext.Current.Request;

            this._repositoryWithInt = repositoryWithInt;

            _bo = new StudentBo(_repository, _repositoryWithInt);
        }


        /// <summary>
        /// Get all students, filtering by items in filter object
        /// //GET: api/Students
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public PagedOutputModel<IEnumerable<StudentOutputModel>> GetAll([FromUri] GetStudentInputModel filter)
        {
            return _bo.Get(filter);
        }


        /// <summary>
        /// Get data for single student and filtered by items in filter object
        /// //GET: api/Students/{studentId}
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("~/api/Students/{studentId}")]
        public PagedOutputModel<IEnumerable<StudentOutputModel>> GetByStudentId(Guid studentId, [FromUri] GetStudentInputModel filter)
        {
            return _bo.Get(filter, studentId);
        }


        /// <summary>
        /// Get data for single student and filtered by items in filter object
        /// //GET: api/Students/{studentId}
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("~/api/StudentSearch/GetBySearchTerm")]
        public IEnumerable<StudentSearchOutputModel> GetBySearchTerm()
        {
            // throw exception if no search term passed in
            if (_request["SearchTerm"] == null) 
            {
                throw new HttpBadRequestResponseException("Search Term is required");
            }

            // get the search term from the request object
            var searchTermInit = _request["SearchTerm"].ToString(CultureInfo.InvariantCulture).Split('|');

            return _bo.GetStudentSearch(searchTermInit);
        }

        /// <summary>
        /// Get data for single student and filtered by items in filter object
        /// //GET: api/Students/{studentId}
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("~/api/StudentSearch/GetByMru")]
        public IEnumerable<StudentMruOutputModel> GetByMru([FromUri] GetMruInputModel filter )
        {
            if (filter.UserId.IsEmpty() || filter.CampusId.IsEmpty()) throw new HttpBadRequestResponseException("CampusId and UserId is required");

            return _bo.GetStudentMru(filter.UserId, filter.CampusId);
        }
        

       
    }
}

   