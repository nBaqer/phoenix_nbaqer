﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Messages.Student.Enrollments;
using FAME.Advantage.Messages.Students.Enrollments;
using FAME.Advantage.Messages.SystemStuff.Menu;
using FAME.Advantage.Services.Lib.BusinessLogic.Students.Academic;
using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;

namespace FAME.Advantage.Services.Lib.Controllers.Students.Enrollments
{
    /// <summary>
    /// Service Controller for Student Attendance Page..........
    /// </summary>
    [RoutePrefix("api/Students/Enrollments/StudentAttendance")]
    public class StudentAttendanceController : ApiController
    {
        const string X_MODULENAME = "ATTENDANCE REPORTS";
        const string X_MENUNAME = "ACADEMICS";
        const string X_TOPLEVELMENU = "Reports";
        //const string XATTENDANCE_MINUTES = "MINUTES";
        //const string XATTENDANCE_CLOCKHOURS = "CLOCK HOURS";
        const string XATTENDANCE_PRESENT_ABSENT = "PRESENT ABSENT";

        private readonly StudentAttendanceBo bo;
        private readonly MasterMenu boMenu;

        #region Repository
        //represents data store
        private readonly IRepository repository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="repositorytyped"></param>
        public StudentAttendanceController(IRepository repository, IRepositoryWithTypedID<int> repositorytyped)
        {
            this.repository = repository;
            bo = new StudentAttendanceBo(repository, repositorytyped);
            boMenu = new MasterMenu(repositorytyped, repository);
        }

        #endregion

        #region Filters Services

        [HttpGet]
        [Route("GetCbItemsEnrollments")]
        public IEnumerable<EnrollmentAttendanceOutputModel> GetCbItemsEnrollments([FromUri] GetEnrollmentsInputModel filter = null)
        {
            try
            {
                // Constrains...............
                if (filter != null)
                {
                    if (filter.StudentId != null)
                    {
                        var result = bo.GetAttendanceEnrollmentsByStudentId(filter);
                        return result;
                    }
                }

                var resulta = bo.GetAllAttendanceEnrollment();
                return resulta;
            }
            catch (Exception ex)
            {
                //#if DEBUG
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
                //#else
                throw new HttpBadRequestResponseException("Error in processing Information.");
                //#endif
            }
        }    

        #endregion

        #region Servicios Present Absent

        /// <summary>
        /// Get Attendance for a specific Class
        /// //GET: api/Students/Enrollments
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>Client expected that this is organized by attendance days. If not does not working propertly</remarks>
        [HttpGet]
        [Route("GetAttendanceAbsentPresent")]
        public IEnumerable<AttendancePresentAbsentOutputModel> GetAttendanceAbsentPresent([FromUri] AttendanceInputModel filter)
        {
            // Validate filters......
            bo.CheckFilterNull(filter);

            // Filter has values
            if (filter.ClsSectionGuid == Guid.Empty)
            {
                throw new HttpBadRequestResponseException("You need to supply class filter");
            }

            try
            {
                // Get all class section for a course....
                var classSection = repository.Query<ArClassSections>().Select(x => x).FirstOrDefault(x => x.ID == filter.ClsSectionGuid);
                // Does not exists schedule time for this.........
                if (classSection == null)
                {
                    return new List<AttendancePresentAbsentOutputModel>();
                }

                // Determine if you want a total summary or only a period of time.....
                if (filter.IsSummary)
                {
                    filter.FilterIni = classSection.StartDate; // todo: revision of this maybe we need to get all from the pass up to current day
                    filter.FilterEnd = classSection.EndDate;
                }

                // Get the meat....
                var summary = bo.GetAllPeriodsforCourseAbsentPresent(filter);
                summary = summary.OrderBy(x => x.AttendanceDate);
                var result = Mapper.Map<IEnumerable<AttendancePresentAbsentOutputModel>>(summary);
                return result;

            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        /// <summary>
        /// Post attendance for Absent Present
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("PostAttendanceAbsentPresent")]
        public string[] PostAttendanceAbsentPresent([FromBody]PostAttendanceInputModel input)
        {
            //  Verify the input data
            bo.CkeckPostAttendanceInputModel(input);

            try
            {
                bo.PostAttendanceAbsentPresent(input);
                //  Return a message warning
                var response = new[] { "UPDATED!", "Please, click View Attendance button to update the grids" };
                return response;
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        #endregion

        #region Servicios Clock Hours & Minutes

        /// <summary>
        /// Get Attendance for a specific Class
        /// //GET: api/Students/Enrollments
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>Client expected that this is organized by attendance days. If not does not working propertly</remarks>
        [HttpGet]
        [Route("GetAttendanceMinutes")]
        public IEnumerable<AttendanceMinutesOutputModel> GetAttendanceMinutes([FromUri] AttendanceInputModel filter)
        {
            // Validate filters......
            bo.CheckFilterNull(filter);

            // Filter has values
            if (filter.ClsSectionGuid == Guid.Empty)
            {
                throw new HttpBadRequestResponseException("You need to supply class filter");
            }

            try
            {
                // Get all class section for a course....
                var classSection = repository.Query<ArClassSections>().Select(x => x).FirstOrDefault(x => x.ID == filter.ClsSectionGuid);
                // Does not exists schedule time for this.........
                if (classSection == null)
                {
                    return new List<AttendanceMinutesOutputModel>();
                }

                // Determine if you want a total summary or only a period of time.....
                if (filter.IsSummary)
                {
                    // preeliminary filter.... this can be used differnet for calendar day and attendance.
                    filter.FilterIni = classSection.StartDate; 
                    filter.FilterEnd = classSection.EndDate;
                }

                // Get the meat....
                var summary = bo.GetAllPeriodsforCourseMinutes(filter).ToList();
                //var summary = AttendanceMinutesOutputModelMockUpDatabase();
                //summary = new List<AttendanceMinutesOutputModel>(summary.OrderBy(x => x.AttendanceDate));
                
                var result = Mapper.Map<IEnumerable<AttendanceMinutesOutputModel>>(summary);
                return result;

            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }
       

        /// <summary>
        /// Post attendance for Clock Minutes
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("PostAttendanceMinutes")]
        public string[] PostAttendanceMinutes([FromBody]PostAttendanceInputModel input)
        {
            //  Verify the input data
            //bo.CkeckPostAttendanceInputModel(input);

            try
            {
                bo.PostAttendanceMinutes(input);
                //  Return a message warning
                var response = new[] { "UPDATED!", "Please, click Get/Set Attendance button to update the grids" };
                return response;
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        #endregion

        #region Enrollment Resume service
        [HttpGet]
        [Route("GetAttendanceEnrollmentSummary")]
        public AttendanceSummaryEnrollmentOutput GetAttendanceEnrollmentSummary([FromUri] AttendanceInputModel filter)
        {
            // Validate filters......
            bo.CheckFilterNull(filter);

            // Filter has values
            if (filter.StuEnrollmentId == Guid.Empty | filter.CampusId == Guid.Empty)
            {
                throw new HttpBadRequestResponseException("You need to supply class filter");
            }

            try
            {
                // get if it is a Absent Present Summary or not....
                // If it is AbsentPresent
                // Mockup........................................................
                //var re = MockUpDataBase();
                //return re;
                var output = bo.GetStudentAttendanceEnrollmentSummary(filter);
                return output;



            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        #endregion

        #region Make Up Hours Services
        /// <summary>
        /// Receive data for change attendance
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("PostMakeUp")]
        public string[] PostMakeUp([FromBody]PostAttendanceInputModel input)
        {
            //  Verify the input data
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Input should have a value");
            }

            try
            {
                // Get attendance type....
                var enrollment = repository.Query<Enrollment>().Select(x => x).FirstOrDefault(x => x.ID == input.EnrollmentGuid);
                // Get if program Version attendance is tracking.... (that is attendance different to none)
                if (enrollment == null) { throw new ArgumentException("Enrollment not found"); }
                // Get the attendance Type.....
                var attendancetype = enrollment.ProgramVersion.AttendanceType.Description.ToUpper(CultureInfo.InvariantCulture);
                if (attendancetype == XATTENDANCE_PRESENT_ABSENT)
                {
                     bo.PostMakeUpHours(input);
                }
                else
                {
                    bo.PostMinutesMakeUpHours(input);
                }
               
                //  Return a message warning
                var response = new[] { "OK", "Make Up was saved" };
                return response;
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        [HttpPost]
        [Route("DeleteMakeUp")]
        public string[] DeleteMakeUp([FromBody]PostAttendanceInputModel input)
        {
            //  Verify the input data
            if (input == null)
            {
                throw new HttpBadRequestResponseException("Input should have a value");
            }

            try
            {
                // Get attendance type....
                var enrollment = repository.Query<Enrollment>().Select(x => x).FirstOrDefault(x => x.ID == input.EnrollmentGuid);
                // Get if program Version attendance is tracking.... (that is attendance different to none)
                if (enrollment == null) { throw new ArgumentException("Enrollment not found"); }
                // Get the attendance Type.....
                var attendancetype = enrollment.ProgramVersion.AttendanceType.Description.ToUpper(CultureInfo.InvariantCulture);
                if (attendancetype == XATTENDANCE_PRESENT_ABSENT)
                {
                    bo.DeleteMakeUpHours(input);
                }
                else
                {
                    bo.DeleteMinutesMakeUpHours(input);
                }
                
               
                //  Return a message warning
                var response = new[] { "OK", "Make Up was deleted" };
                return response;
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }



        #endregion

        #region Get Attendance Report links

        /// <summary>
        /// Get the Page Group and Pages level items
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStudentAttendanceReportLink")]
        public IEnumerable<MenuItemsOutputModel> GetStudentAttendanceReportLink([FromUri] AttendanceInputModel filter = null)
        {
            // Validate filters......
            bo.CheckFilterNull(filter);
            // Validate filter content...
            //if (string.IsNullOrEmpty(moduleName) || string.IsNullOrEmpty(subMenuName)) throw new HttpBadRequestResponseException("Menu Name is required");
            // ReSharper disable PossibleNullReferenceException
            if (filter.CampusId.IsEmpty()) throw new HttpBadRequestResponseException("Campus Id is required");
            // ReSharper restore PossibleNullReferenceException
            if (filter.UserIdGuid.IsEmpty()) throw new HttpBadRequestResponseException("User Id is required");
            try
            {
                // Mock Up result
                // var mockup = MockUpReport();
                // return mockup;
                // Get the list of items
                var filterMenu = new GetMenuItemInputModel { UserId = filter.UserIdGuid, CampusId = filter.CampusId };
                var subLevelMenuItems = boMenu.SubLevelMenuItems(X_TOPLEVELMENU, X_MENUNAME, filterMenu);
                var attendanceReportList = boMenu.GetLastLevelItems(subLevelMenuItems, X_MODULENAME);
                var resultList = Mapper.Map<IEnumerable<MenuItemsOutputModel>>(attendanceReportList.OrderBy(n => n.DisplayOrder));
                return resultList;
            }
            catch (Exception ex)
            {
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
            }
        }

        #endregion

        #region Mock Up database

        private IList<AttendanceMinutesOutputModel> AttendanceMinutesOutputModelMockUpDatabase()
        {
            var output = new List<AttendanceMinutesOutputModel>();

            var att = new AttendanceMinutesOutputModel
            {
                AttendanceGuid = Guid.Empty,
                Absent = 25,
                Actual = 100,
                AttendanceDate = DateTime.Now.AddDays(-10),
                ClassSectionMeetingGuid = Guid.Empty,
                Excused = 0,
                MakeUp = 0,
                PeriodDescription = "Period Description",
                PeriodId = Guid.Empty,
                Schedule = 300,
                Tardies = 0
            };

            output.Add(att);

            att = new AttendanceMinutesOutputModel
            {
                AttendanceGuid = Guid.Empty,
                Absent = 25,
                Actual = 100,
                AttendanceDate = DateTime.Now.AddDays(-8),
                ClassSectionMeetingGuid = Guid.Empty,
                Excused = 0,
                MakeUp = 0,
                PeriodDescription = "Period Description",
                PeriodId = Guid.Empty,
                Schedule = 300,
                Tardies = 0
            };

            output.Add(att);

            att = new AttendanceMinutesOutputModel
            {
                AttendanceGuid = Guid.Empty,
                Absent = 25,
                Actual = 100,
                AttendanceDate = DateTime.Now.AddDays(-6),
                ClassSectionMeetingGuid = Guid.Empty,
                Excused = 0,
                MakeUp = 0,
                PeriodDescription = "Period Description",
                PeriodId = Guid.Empty,
                Schedule = 300,
                Tardies = 1
            };

            output.Add(att);
            att = new AttendanceMinutesOutputModel
            {
                AttendanceGuid = Guid.Empty,
                Absent = 25,
                Actual = 100,
                AttendanceDate = DateTime.Now.AddDays(-4),
                ClassSectionMeetingGuid = Guid.Empty,
                Excused = 0,
                MakeUp = 0,
                PeriodDescription = "Period Description",
                PeriodId = Guid.Empty,
                Schedule = 300,
                Tardies = 1
            };

            output.Add(att);

            return output;
        }

        //private AttendanceSummaryEnrollmentOutput MockUpDataBase()
        //{
        //    var data = new AttendanceSummaryEnrollmentOutput
        //    {
        //        PercentagePresent = new decimal(0.80),
        //        ScheduleHours = 200,
        //        SummaryEnrollmentLoaList = new List<SpecialAttendanceTable>()
        //    };
        //    var loa = new SpecialAttendanceTable
        //    {
        //        DateEnd = new DateTime(2014, 1, 1),
        //        DateIni = new DateTime(2014, 2, 24),
        //        DateRequested = new DateTime(2014, 2, 24),
        //        StatusReason = "Sport Games"
        //    };
        //    data.SummaryEnrollmentLoaList.Add(loa);

        //    data.SummaryEnrollmentTableList = new List<SummaryEnrollmentTable>();
        //    var ent = new SummaryEnrollmentTable
        //    {
        //        LabelField = "Actual",
        //        Absent = "20",
        //        Excused = "2",
        //        Makeup = "2",
        //        Present = "40",
        //        Tardy = "10"
        //    };
        //    data.SummaryEnrollmentTableList.Add(ent);

        //    ent = new SummaryEnrollmentTable
        //    {
        //        LabelField = "Adjusted",
        //        Absent = "25",
        //        Excused = "2",
        //        Makeup = "2",
        //        Present = "38",
        //        Tardy = "10"
        //    };

        //    data.SummaryEnrollmentTableList.Add(ent);
        //    data.SummaryEnrollmentStatusList = new List<SpecialAttendanceTable>();
        //    var entc = new SpecialAttendanceTable
        //    {
        //        DateEnd = new DateTime(2014, 1, 1),
        //        DateIni = new DateTime(2014, 1, 24),
        //        DateRequested = new DateTime(2014, 2, 24),
        //        StatusReason = "Dropped"
        //    };

        //    data.SummaryEnrollmentStatusList.Add(entc);
        //    entc = new SpecialAttendanceTable
        //    {
        //        DateEnd = null,
        //        DateIni = new DateTime(2014, 2, 24),
        //        DateRequested = new DateTime(2014, 2, 24),
        //        StatusReason = "Current Attendance"
        //    };

        //    data.SummaryEnrollmentStatusList.Add(entc);

        //    data.SummarySuspensionList = new List<SpecialAttendanceTable>();
        //    var sus = new SpecialAttendanceTable
        //    {
        //        DateEnd = new DateTime(2014, 1, 1),
        //        DateIni = new DateTime(2014, 2, 24),
        //        DateRequested = new DateTime(2014, 2, 24),
        //        StatusReason = "Unknow"
        //    };
        //    data.SummarySuspensionList.Add(sus);
        //    return data;
        //}

        //#endregion

        //#region Mockup Report List

        //private IList<MenuItemsOutputModel> MockUpReport()
        //{
        //    IList<MenuItemsOutputModel> mockuplist = new List<MenuItemsOutputModel>();
        //    var model = new MenuItemsOutputModel { Id = 1, IsEnabled = "true", items = null, text = "Report Student Attendance mockup", url = "http://www.google.com" };
        //    mockuplist.Add(model);
        //    model = new MenuItemsOutputModel { Id = 1, IsEnabled = "false", items = null, text = "Report Student Attendance mockup 2", url = "http://www.google.com" };
        //    mockuplist.Add(model);
        //    model = new MenuItemsOutputModel { Id = 1, IsEnabled = "true", items = null, text = "Report Student Attendance mockup 3", url = "http://www.google.com" };
        //    mockuplist.Add(model);
        //    return mockuplist;
        //}

        #endregion

    }
}
