﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Messages.Campuses.CampusGroups.Terms;
using FAME.Advantage.Messages.Common;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

namespace FAME.Advantage.Services.Lib.Controllers.Students.Enrollments
{
    /// <summary>
    /// Controller for Terms
    /// </summary>
    /// <remarks>
    /// This controler has a router by Action you dont need to create other.
    /// routeTemplate: "api/Students/Enrollments/{controller}/{action}/{param}",
    /// </remarks>
    [RoutePrefix("api/Students/Enrollments/Terms")]
    public class TermsController : ApiController
    {
        //represents data store
        private readonly IRepository repository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        public TermsController(IRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        [Route("GetDdTermItems")]
        public IEnumerable<DropDownOutputModel> GetDdTermItems([FromUri] GetTermsInputModel filter)
        {
            try
            {
                // Constrains...............
                if (filter != null)
                {
                    if (filter.StuEnrollmentId != Guid.Empty & String.IsNullOrEmpty(filter.StatusCode) == false)
                    {
                        var result = GetTermsDdByEnrollmentAndStatusCode(filter);
                        return result;
                    }

                    if (filter.StuEnrollmentId != Guid.Empty)
                    {
                        var result = GetTermsDdByEnrollment(filter);
                        return result;
                    }

                    if (String.IsNullOrEmpty(filter.StatusCode) == false)
                    {
                        var result = GetTermsDdByStatusCode(filter);
                        return result;
                    }
                }

                //If other values
                var resultAll = GetAllTerms();
                return resultAll;
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
#else
                throw new HttpBadRequestResponseException("Error in processing Information.");
#endif
            }
        }

        #region Private method (this can be move to a business object layer)

        private IEnumerable<DropDownOutputModel> GetAllTerms()
        {
            var enrollmentlist = repository.Query<Enrollment>();
            var termlist = (from enrollment in enrollmentlist from res in enrollment.ResultList select res.ClassSections.TermEntity).Distinct(); 
            return Mapper.Map<IEnumerable<DropDownOutputModel>>(termlist.ToArray());
        }

        private IEnumerable<DropDownOutputModel> GetTermsDdByStatusCode(GetTermsInputModel filter)
        {
            throw new NotImplementedException();
        }

        private IEnumerable<DropDownOutputModel> GetTermsDdByEnrollment(GetTermsInputModel filter)
        {
            var enrollmentlist = repository.Query<Enrollment>().Where(t => t.ID == filter.StuEnrollmentId);
            var termlist = (from enrollment in enrollmentlist from res in enrollment.ResultList select res.ClassSections.TermEntity).Distinct();
            return Mapper.Map<IEnumerable<DropDownOutputModel>>(termlist.ToList().OrderBy(x => x.StartDate));
        }

        private IEnumerable<DropDownOutputModel> GetTermsDdByEnrollmentAndStatusCode(GetTermsInputModel filter)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Commented Code, John if you dont use it, please delete
        ///// <summary>
        ///// Get All Terms applying any filters passed in
        ///// //api/Campuses/CampusGroups/Terms
        ///// </summary>
        ///// <param name="filter"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public IEnumerable<TermsOutputModel> GetAllTerms([FromUri] GetTermsInputModel filter)
        //{
        //    return Get(filter);
        //}

        ///// <summary>
        ///// Get tems by campus id
        ///// //api/Campuses/{campusId}/CampusGroups/Terms 
        ///// </summary>
        ///// <param name="campusId"></param>
        ///// <param name="filter"></param>
        ///// <returns></returns>
        //[HttpGet]
        
        //public IEnumerable<TermsOutputModel> GetTermsByCampusId(Guid campusId, [FromUri] GetTermsInputModel filter)
        //{
        //    return Get(filter, campusId);
        //}


        ///// <summary>
        ///// Get method called from public gets, apply campus id and/or filter
        ///// </summary>
        ///// <param name="filter"></param>
        ///// <param name="campusId"></param>
        ///// <returns></returns>
        //private IEnumerable<TermsOutputModel> Get(GetTermsInputModel filter, Guid? campusId = null)
        //{
        //    //initial data retrieval
        //    var terms = repository.Query<Term>();

        //    //filter on status code
        //    if ( ! string.IsNullOrEmpty(filter.StatusCode) )
        //        terms = terms.Where(n => n.Status.StatusCode == filter.StatusCode);

        //    //filter on campus id
        //    if (!campusId.IsEmpty())
        //        terms = terms.Where(n => n.CampusGroup.Campuses.Any(c => c.ID == campusId));                 

        //    //return data to client
        //    return Mapper.Map<IEnumerable<TermsOutputModel>>(terms.ToArray());
        //}
        #endregion
    }
}