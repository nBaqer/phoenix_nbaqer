﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Enrollments.Transactions;
using FAME.Advantage.Domain.Users;
using FAME.Advantage.Domain.Users.UserRoles;
using FAME.Advantage.Messages.Students.Enrollments;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;


namespace FAME.Advantage.Services.Lib.Controllers.Students.Enrollments
{
    [RoutePrefix("api/Students/Enrollments")]
    public class TransactionController : ApiController
    {
        //represents data store
        private readonly IRepository _repository;
        

       
        public TransactionController(IRepository trRepository)
        {
            _repository = trRepository;
           
        }


        /// <summary>
        /// Get data for transactions by payment period
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTransactionsByPaymentPeriodsSummary")]
        public IEnumerable<EnrollmentTransactionPayPeriodSummaryOutputModel> GetTransactionsByPaymentPeriodsSummary([FromUri] GetEnrollmentTransactionInputModel filter = null)
        {
            if (filter == null || string.IsNullOrEmpty(filter.SummaryType)) throw new HttpBadRequestResponseException("Summary Type is required");

            DateTime lStart;
            DateTime lEnd;

            lEnd = DateTime.Now.Date;

            if (filter.SummaryType.ToUpper() == "DAILY")
                lStart = DateTime.Now.Date.AddDays(-6);
            else if(filter.SummaryType.ToUpper() == "WEEKLY")
                lStart = DateTime.Now.Date.AddDays(-30);
            else if (filter.SummaryType.ToUpper() == "MONTHLY")
                lStart = DateTime.Now.Date.AddDays(-180);
            else
                throw new HttpBadRequestResponseException("Summary Type is invalid");
            

            return GetTransactionsSummary(lStart,lEnd);
        }


        /// <summary>
        /// Get data for transactions by payment period
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTransactionsByPaymentPeriods")]
        public IEnumerable<EnrollmentTransactionPayPeriodOutputModel> GetTransactionsByPaymentPeriods([FromUri] GetEnrollmentTransactionInputModel filter = null)
        {
            if (filter == null || string.IsNullOrEmpty(filter.FromDate) || string.IsNullOrEmpty(filter.ToDate)) throw new HttpBadRequestResponseException("From and to Date is required");

            DateTime lStart;
            DateTime lEnd;

            DateTime.TryParse(filter.FromDate, out lStart);
            DateTime.TryParse(filter.ToDate, out lEnd);

            return GetTransactions(lStart, lEnd);
        }

        /// <summary>
        ///IsPaymentPeriodWidgetEnabled
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("IsPaymentPeriodWidgetEnabled")]
        public bool IsPaymentPeriodWidgetEnabled([FromUri] GetEnrollmentTransactionInputModel filter = null)
        {
            if (filter == null || filter.UserId.IsEmpty() || filter.CampusId.IsEmpty()) throw new HttpBadRequestResponseException("UserId and CampusId is required");

            return IsWidgetEnabled(filter.UserId, filter.CampusId);
        }

        [HttpGet]
        [Route("GetProjectedPaymentPeriodCharges")]
        public IEnumerable<EnrollmentTransactionProjectedOutputModel> GetProjectedPaymentPeriodCharges([FromUri] GetEnrollmentTransactionProjectedInputModel filter = null)
        {
            if (filter == null) throw new HttpBadRequestResponseException("Parameters are required");
            if (filter.ThreshValue == null) throw new HttpBadRequestResponseException("Thresh Hold Value is required");
            if (filter.IncrementType == null) throw new HttpBadRequestResponseException("Increment Type Value is required");

            return GetProjectedPaymentPeriodCharges(filter.ThreshValue, filter.IncrementType);
        }


        [HttpGet]
        [Route("GetProjectedPaymentPeriodSummary")]
        public IEnumerable<EnrollmentTransactionProjectedSummaryOutputModel> GetProjectedPaymentPeriodSummary([FromUri] GetEnrollmentTransactionProjectedInputModel filter = null)
        {
            if (filter == null) throw new HttpBadRequestResponseException("Parameters are required");
            if (filter.ThreshValue == null) throw new HttpBadRequestResponseException("Thresh Hold Value is required");
            if (filter.IncrementType == null) throw new HttpBadRequestResponseException("Increment Type Value is required");

            return GetProjectedPaymentPeriodSummary(filter.ThreshValue, filter.IncrementType);
        }


        ///// <summary>
        ///// Get the Students based on the search term passed in from the universal search
        ///// </summary>
        ///// <returns></returns>
        private IEnumerable<EnrollmentTransactionPayPeriodSummaryOutputModel> GetTransactionsSummary(DateTime fromDate, DateTime toDate)
        {
            
            var transsummary = _repository.Query<Transactions>()
                .Where(n => n.TransDate >= fromDate && n.TransDate <= toDate.Date.AddDays(1) && n.PaymentPeriod != null)
                    .GroupBy(n => n.TransDate)
                        .ToArray();


            var trSum = new List<EnrollmentTransactionPayPeriodSummaryOutputModel>();

            foreach (var day in EachDay(fromDate, toDate))
            {
                var outputItem = new EnrollmentTransactionPayPeriodSummaryOutputModel
                {
                    TransDate =   day.Month.ToString(CultureInfo.InvariantCulture) + "/" + 
                                    day.Day.ToString(CultureInfo.InvariantCulture) + "/" + 
                                    day.Year.ToString(CultureInfo.InvariantCulture),
                    TDate = day.Date,
                    date = day.Date
                };
                
                var item = transsummary.FirstOrDefault(n => n.Key.Date == day.Date);

                //string sumString = "";
                if (item != null) outputItem.ChargeAmountSumString = item.Sum(res => res.Amount).ToString("c");
                if (item != null) outputItem.ChargeAmountSum = item.Sum(res => res.Amount);


                trSum.Add(outputItem);
            }
            

            return trSum.ToArray().OrderBy(n=>n.TDate);

        }

        /// <summary>
        /// Get the Students based on the search term passed in from the universal search
        /// </summary>
        /// <returns></returns>
        private IEnumerable<EnrollmentTransactionPayPeriodOutputModel> GetTransactions(DateTime fromDate, DateTime toDate)
        {
            var transactions = _repository.Query<Transactions>().Where(n => n.TransDate.Date >= fromDate && n.TransDate.Date <= toDate.Date && n.PaymentPeriod != null);

            

            //return the employee list
            return Mapper.Map<IEnumerable<EnrollmentTransactionPayPeriodOutputModel>>(transactions
                .OrderBy(n=>n.StudentEnrollment.Student.LastName)
                    .ThenBy(n=>n.StudentEnrollment.Student.FirstName)
                        .ThenBy(n=>n.PaymentPeriod.PeriodNumber))
                            .ToArray();
        }



        /// <summary>
        /// IsWidgetEnabled
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="campusId"></param>
        /// <returns></returns>
        private bool IsWidgetEnabled(Guid? userId, Guid? campusId)
        {
           // if (IsSupportUser(userId)) return true;
            
            // Get roles for this user
            var roles = _repository.Query<UserRolesCampusGroupBridge>()
                            .Where(n => n.CampusGroup.Campuses.Any(c => c.ID == campusId) && n.User.ID == userId)
                                .Select(n => n.Role).Distinct();

            //check if the user has any acceptable roles for this widget
            return Enumerable.Any(roles, r => r.SystemRole.Description.ToUpper() ==  "DIRECTOR OF BUSINESS OFFICE");
        }


        /// <summary>
        /// Get the resources enabled for this user
        /// </summary>  
        /// <param name="pThreshValue"></param>
        /// <param name="pIncrementType"></param>

        /// <returns>List of </returns>
        private IEnumerable<EnrollmentTransactionProjectedOutputModel> GetProjectedPaymentPeriodCharges(Decimal? pThreshValue, int? pIncrementType)
        {
            var projectedList = GetProjectedPaymentsList(pThreshValue,pIncrementType);

            //return resource levels object
            return projectedList.OrderBy(n=>n.FullName);
        }

        


        /// <summary>
        /// Get the resources enabled for this user
        /// </summary>  
        /// <param name="pThreshValue"></param>
        /// <param name="pIncrementType"></param>

        /// <returns>List of </returns>
        private IEnumerable<EnrollmentTransactionProjectedSummaryOutputModel> GetProjectedPaymentPeriodSummary(Decimal? pThreshValue, int? pIncrementType)
        {

            var projectedSummaryList = GetProjectedPaymentsList(pThreshValue,pIncrementType)
                    .GroupBy(n => n.CreditHoursLeft)
                        .OrderBy(n=>n.Key).ToArray();

            var outputList = new List<EnrollmentTransactionProjectedSummaryOutputModel>();
            var cumAmount = 0M;

            foreach (var item in projectedSummaryList)
            {
                var listItem = new EnrollmentTransactionProjectedSummaryOutputModel();
                

                cumAmount += item.Sum(res => res.ChargeAmount);
                listItem.CreditsHoursLeft = (decimal) item.Key;
                listItem.Amount = item.Sum(res => res.ChargeAmount);
                listItem.CumulativeAmount = cumAmount;
                listItem.CumulativeDifference = cumAmount - listItem.Amount;
                
                outputList.Add(listItem);
            }

            //return resource levels object
            return outputList.OrderBy(n=>n.CreditsHoursLeft);
        }


        /// <summary>
        /// get projected payment list
        /// </summary>        
        /// <param name="pThreshValue"></param>
        /// <param name="pIncrementType"></param>
        /// <returns></returns>
        private IEnumerable<EnrollmentTransactionProjectedOutputModel> GetProjectedPaymentsList(decimal? pThreshValue, int? pIncrementType)
        {

            if(pThreshValue == null) throw new HttpBadRequestResponseException("threshvalue is null");
            if (pIncrementType == null) throw new HttpBadRequestResponseException("IncrementType is null");

            var threshValue = (decimal)pThreshValue;
            var incrementType =(int) pIncrementType;

            var projectedPayments = _repository.Session.CreateSQLQuery(
               "exec GetProjectedPaymentPeriodCharges :ThreshValue, :IncrementType")
               .SetDecimal("ThreshValue", threshValue)
               .SetInt32("IncrementType", incrementType)
               .List<Object>();


            var list = new List<EnrollmentTransactionProjectedOutputModel>();
            foreach (object[] entity in projectedPayments)
            {
                var listItem = new EnrollmentTransactionProjectedOutputModel();

                listItem.PmtPeriodId = (Guid) entity[0];
                listItem.StudentId = (Guid) entity[1];
                listItem.StuEnrollId = (Guid) entity[2];
                listItem.LastName = entity[3].ToString();
                listItem.FirstName = entity[4].ToString();
                listItem.FullName = entity[3].ToString() + ", " + entity[4].ToString();
                listItem.ProgramVersionDescription = entity[5].ToString();
                listItem.ProgramVersionCode = entity[6].ToString();
                listItem.CampusDescription = entity[7].ToString();
                listItem.ChargeAmount = Convert.ToDecimal(entity[8]);
                listItem.CreditsHoursValue = Convert.ToDecimal(entity[9]);
                listItem.CumulativeValue = Convert.ToDecimal(entity[10]);
                listItem.CreditHoursLeft = Convert.ToDecimal(entity[11]);
                listItem.Threshhold = Convert.ToDecimal(entity[12]);
                listItem.IncrementType = Convert.ToInt32(entity[13]);
                listItem.InputIncrementType = Convert.ToInt32(entity[14]);

                list.Add(listItem);

            }
                
            return list;
        }


        /// <summary>
        /// Determine if the userid is the support user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private bool IsSupportUser(Guid? userId)
        {
            var user = _repository.Query<User>().FirstOrDefault(n => n.ID == userId);
            return user != null && user.UserName.ToUpper() == "SUPPORT";
        }



        public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }
    }
}
