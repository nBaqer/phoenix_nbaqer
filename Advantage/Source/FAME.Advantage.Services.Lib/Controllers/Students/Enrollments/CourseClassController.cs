﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;
using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Messages.Common;
using FAME.Advantage.Messages.Students.Enrollments;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using NHibernate.Criterion;

namespace FAME.Advantage.Services.Lib.Controllers.Students.Enrollments
{
    [RoutePrefix("api/Students/Enrollments/CourseClass")]
    public class CourseClassController : ApiController
    {
        //represents data store
        private readonly IRepository repository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        public CourseClassController(IRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        [Route("GetDdCourseClass")]
        public IEnumerable<DropDownOutputModel> GetDdCourseClass([FromUri] CourseClassInputModel filter = null)
        {
            try
            {
                // Constrains...............
                if (filter != null)
                {
                    if (filter.TermId != Guid.Empty)
                    {
                        var result = GetCourseClassByTermIdDd(filter);
                        return result;
                    }
                 }

                //If other values
                var resultAll = GetAllCourseClassDd();
                return resultAll;
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
#else
                throw new HttpBadRequestResponseException("Error in processing Information.");
#endif
            }
        }

        [HttpGet]
        [Route("GetCourseClass")]
        public IEnumerable<CourseClassOutputModel> GetCourseClass([FromUri] CourseClassInputModel filter = null)
        {
            try
            {
                // Constrains...............
                if (filter != null)
                {
                    if (filter.TermId != Guid.Empty)
                    {
                        var result = GetCourseClassByTermId(filter);
                        return result;
                    }
                    return new List<CourseClassOutputModel>();
                }

                throw new NotImplementedException(); 
                //If other values
                //var resultAll = GetAllCourseClass();
                
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
#else
                throw new HttpBadRequestResponseException("Error in processing Information.");
#endif
            }
        }

        //private IEnumerable<CourseClassOutputModel> GetAllCourseClass()
        //{
        //    throw new NotImplementedException();    
        //}

        /// <summary>
        /// Get Course class section id and Course assigned.
        /// </summary>
        /// <param name="filter">Term Guid</param>
        /// <returns></returns>
        private IEnumerable<CourseClassOutputModel> GetCourseClassByTermId(CourseClassInputModel filter)
        {
            var courses =

                repository.Query<ArResults>()
                    .Where(x => x.StuEnrollment.ID == filter.StuEnrollmentId && x.ClassSections.TermEntity.ID == filter.TermId)
                    .Select(c => c.ClassSections)
                    .Where(s => s.RequerimentCourse.UnitType.Description.ToLower() != "none");
           
            return Mapper.Map<IEnumerable<CourseClassOutputModel>>(courses.ToList().OrderBy(x => x.RequerimentCourse.Description));
        }
       

        #region Private methods

        private IEnumerable<DropDownOutputModel> GetAllCourseClassDd()
        {
            var courses = repository.Query<ReqsCourse>();
            return Mapper.Map<IEnumerable<DropDownOutputModel>>(courses.ToArray());
        }


        /// <summary>
        /// This return the arClassSections ID and the Description of the associated curse.
        /// </summary>
        /// <param name="filter">TermId: the Selected Term.</param>
        /// <returns>
        /// arClassSections ID and the Description of the associated curse.
        /// </returns>
        private IEnumerable<DropDownOutputModel> GetCourseClassByTermIdDd(CourseClassInputModel filter)
        {
            var courses =
                repository.Query<ArClassSections>()
                    .Where(c => c.TermEntity.ID == filter.TermId)
                    .Select(c => c).Where(r => r.RequerimentCourse.UnitType.Description.ToLower() != "none");
            return Mapper.Map<IEnumerable<DropDownOutputModel>>(courses.ToArray());
        }

        #endregion
    }
}
