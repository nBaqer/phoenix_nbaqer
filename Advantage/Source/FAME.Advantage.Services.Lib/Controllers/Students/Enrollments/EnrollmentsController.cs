﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Messages.Student.Enrollments;
using FAME.Advantage.Messages.Students.Enrollments;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;
using FAME.Advantage.Messages.Common;

namespace FAME.Advantage.Services.Lib.Controllers.Students.Enrollments
{
    [RoutePrefix("api/Students/Enrollments/Enrollments")]
    public class EnrollmentsController : ApiController
    {
        //represents data store
        private readonly IRepository repository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        public EnrollmentsController(IRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get All Enrollments but filtering using filter object
        /// //GET: api/Students/Enrollments
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public IEnumerable<EnrollmentOutputModel> GetAll([FromUri] GetEnrollmentsInputModel filter)
        {
            return Get(filter);
        }

        /// <summary>
        /// Get enrollments by student id
        /// //GET: api/Students/{studentId}/Enrollments/
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByStudentId")]
        public IEnumerable<EnrollmentOutputModel> GetByStudentId(Guid studentId, [FromUri]  GetEnrollmentsInputModel filter)
        {
            return Get(filter, studentId);
        }

        /// <summary>
        /// Get enrollments by enrollmentid and student id
        /// //GET: api/Students/{studentId}/Enrollments/{enrollmentId}
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="enrollmentId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetByEnrollmentIdAndStudentId")]
        public IEnumerable<EnrollmentOutputModel> GetByEnrollmentIdAndStudentId(Guid studentId, Guid enrollmentId, [FromUri]  GetEnrollmentsInputModel filter)
        {
            return Get(filter, studentId, enrollmentId);
        }

        /// <summary>
        /// Get method called by one of the other gets
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="studentId"></param>
        /// <param name="enrollmentId"></param>
        /// <returns></returns>
        [HttpGet]
        private IEnumerable<EnrollmentOutputModel> Get(GetEnrollmentsInputModel filter, Guid? studentId = null, Guid? enrollmentId = null)
        {
            //initial retrieval
            var enrollmentsQuery = repository.Query<Enrollment>();

            //student id passed in
            if (studentId.HasValue)
                enrollmentsQuery = enrollmentsQuery.Where(e => e.Student.ID == studentId);

            //enrollmentid passed in
            if (enrollmentId.HasValue)
                enrollmentsQuery = enrollmentsQuery.Where(e => e.ID == enrollmentId);

            //retrieve filter items and filter accordingly
            if (!filter.CampusId.IsEmpty())
                enrollmentsQuery = enrollmentsQuery.Where(e => e.CampusId.Equals(filter.CampusId));

            if (!filter.ProgramVersionId.IsEmpty())
                enrollmentsQuery = enrollmentsQuery.Where(e => e.ProgramVersion.ID == filter.ProgramVersionId);

            if (!filter.EnrollmentStatusId.IsEmpty())
                enrollmentsQuery = enrollmentsQuery.Where(e => e.EnrollmentStatus.ID == filter.EnrollmentStatusId);

            if (!filter.StudentGroupId.IsEmpty())
                enrollmentsQuery = enrollmentsQuery.Where(r => repository.Query<LeadGroupBridge>()
                                                        .Where(n => n.StudentGroupId == filter.StudentGroupId)
                                                            .Select(n => n.StudentEnrollmentId)
                                                            .Contains(r.ID));

            //page the data and return
            var enrollments = enrollmentsQuery.Page(filter.PageNumber, filter.PageSize).ToArray();

            return Mapper.Map<IEnumerable<EnrollmentOutputModel>>(enrollments);
        }


        /// <summary>
        /// Get the Enrollment by Student Id or all enrollment.
        /// This return only Id and Enrollment Description.
        /// </summary>
        /// <param name="filter">
        /// admit StudentId
        /// </param>
        /// <returns>A enumerable in all case.</returns>
        [HttpGet]
        [Route("GetCbItemsEnrollments")]
        public IEnumerable<DropDownOutputModel> GetCbItemsEnrollments([FromUri] GetEnrollmentsInputModel filter = null)
        {
            try
            {
                // Constrains...............
                if (filter != null)
                {
                    if (filter.StudentId != null)
                    {
                        var result = GetEnrollmentsByStudentId(filter);
                        return result;
                    }
                }

                var resulta = GetAllEnrollment();
                return resulta;
            }
            catch (Exception ex)
            {
                //#if DEBUG
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
                //#else
                throw new HttpBadRequestResponseException("Error in processing Information.");
                //#endif
            }
        }

        private IEnumerable<DropDownOutputModel> GetAllEnrollment()
        {
            var enrollmentList = repository.Query<Enrollment>().Distinct().Page(1,20);
            var enrollmentArray = Mapper.Map<IEnumerable<DropDownOutputModel>>(enrollmentList.ToArray());
            return enrollmentArray;
        }

        #region Private Refactoring

        /// <summary>
        /// Get the list of campus by Campus Group
        /// </summary>
        /// <param name="filter">campus group filter</param>
        /// <returns>a list of ID Description</returns>
        private IEnumerable<DropDownOutputModel> GetEnrollmentsByStudentId([FromUri] GetEnrollmentsInputModel filter)
        {
            //if campusgrpId pased in, get the campuses by campus group id
            if (filter.StudentId.IsEmpty()) { throw new HttpBadUsageOfControllerFunctionException("Controller processing has not a valid parameter"); }

            var enrollmentList = repository.Query<Enrollment>().Where(n => n.StudentId == filter.StudentId).Distinct();

            var enrollmentArray = Mapper.Map<IEnumerable<DropDownOutputModel>>(enrollmentList.ToList().OrderBy(x => x.EnrollmentDate));
            return enrollmentArray;

        }

        #endregion


    }
}


