﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Enrollments.Transactions;
using FAME.Advantage.Domain.Users;
using FAME.Advantage.Domain.Users.UserRoles;
using FAME.Advantage.Messages.Students.Enrollments;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;


namespace FAME.Advantage.Services.Lib.Controllers.Students.Enrollments
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("api/Students/Enrollments/GetTransactionsCodesBySystemCode")]
    public class TransactionCodeController : ApiController
    {
        //represents data store
        private readonly IRepository _repository;



        /// <summary>
        /// 
        /// </summary>
        /// <param name="trRepository"></param>
        public TransactionCodeController(IRepository trRepository)
        {
            _repository = trRepository;
           
        }


        /// <summary>
        /// Get data for transactions by payment period
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IEnumerable<TransactionCodeOutputModel> GetTransactionsCodesBySystemCode()
        {

            var transcodes = _repository.Query<TransactionCode>().Where(n => n.SysTransCodeId == 20);

            return Mapper.Map<IEnumerable<TransactionCodeOutputModel>>(transcodes.OrderBy(n=>n.Description).ToArray());
        }


       
    }
}
