﻿using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Requirements;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using FAME.Advantage.Messages.Requirements;

namespace FAME.Advantage.Services.Lib.Controllers
{
    [RoutePrefix("api/RequirementEffectiveDates")]
    public class RequirementEffectiveDatesController : ApiController
    {
        private readonly IRepository _repository;

        public RequirementEffectiveDatesController(IRepository repository)
        {
            _repository = repository;
        }

        [Route("")]
        public IEnumerable<RequirementEffectiveDatesOutputModel> Get()
        {
            
            var requirements = _repository.Query<RequirementEffectiveDates>().Where(n=>n.IsMandatory);

            return Mapper.Map<IEnumerable<RequirementEffectiveDatesOutputModel>>(requirements);
        }
    }
}
