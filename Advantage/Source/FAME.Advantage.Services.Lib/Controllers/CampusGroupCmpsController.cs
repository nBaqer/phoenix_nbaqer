﻿using AutoMapper;
using FAME.Advantage.Domain.CampusGroupBridges;
using FAME.Advantage.Domain.Infrastructure.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using FAME.Advantage.Messages.CampusGroupBridges;

namespace FAME.Advantage.Services.Lib.Controllers
{
    public class CampusGroupBridgeController : ApiController
    {
        private readonly IRepository _repository;

        public CampusGroupBridgeController(IRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<CampusGroupBridgeOutputModel> Get()
        {
            var campusGroupBridges = _repository.Query<CampusGroupBridge>().ToArray();

            return Mapper.Map<IEnumerable<CampusGroupBridgeOutputModel>>(campusGroupBridges);
        }
    }
}