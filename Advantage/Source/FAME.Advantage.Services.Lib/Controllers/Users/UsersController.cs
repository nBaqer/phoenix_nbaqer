﻿using AutoMapper;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Users;
using FAME.Advantage.Messages.Users;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Controllers.Users
{
    using FAME.Advantage.Domain.Users.UserRoles;

    [RoutePrefix("api/User/Users")]
    public class UsersController : ApiController
    {
        //represents data store
        private readonly IRepository _repository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        public UsersController(IRepository repository)
        {
            _repository = repository;
        }


        /// <summary>
        /// public get method.  All api calls will go through this one get
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IEnumerable<UserOutputModel> Get([FromUri] GetUsersInputModel filter)
        {

            if(filter == null)
                throw new HttpBadRequestResponseException("filter is required");

            IEnumerable<UserOutputModel> result; 
            // call the correct method based on parameters
            if (!filter.UserId.IsEmpty())
            {
                result =  GetById(filter.UserId);
            }
            else
            {
                if (!filter.CampusId.IsEmpty() && filter.ReturnReps != null)
                    result = GetAdmissionRepsByCampus(filter.CampusId, filter.Active);
                else if (!filter.CampusId.IsEmpty())
                    result = GetByCampus(filter.CampusId);
                else if (filter.ReturnSupport != null)
                    result = GetSupportUser(filter);
                else
                    result = GetAll(filter);

            }

            return result;
        }
        
        /// <summary>
        /// Get specific user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private IEnumerable<UserOutputModel> GetById(Guid userId)
        {

            
            if (userId.IsEmpty())
                throw new HttpBadRequestResponseException("User Id is required");
            //---------------------------------------------------------------------------------------

            // Get User and return the object
            var user = _repository.Query<User>().Where(n => n.ID == userId);
            return Mapper.Map<IEnumerable<UserOutputModel>>(user.ToArray());

        }

        /// <summary>
        /// Get users for a specific campus - NOT COMPLETED
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        private IEnumerable<UserOutputModel> GetByCampus( Guid campusId)
        {

            //////---------------------------------------------------------------------------------------
            ////// Validate required filter items 
            //////---------------------------------------------------------------------------------------
            //if (filter.CampusId.IsEmpty())
            //    throw new HttpBadRequestResponseException("Campus Id is required");

            //if (userId.IsEmpty())
            //    throw new HttpBadRequestResponseException("User Id is required");
            ////---------------------------------------------------------------------------------------


            //// Get users and return to client
            //var users = _repository.Query<User>();
            //users = users.Where(n=>n.CampusGroup

            ////return Mapper.Map<IEnumerable<RolesOutputModel>>(roles.ToArray());

            return null;
        }

        /// <summary>
        /// Get all active users
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        
        private IEnumerable<UserOutputModel> GetAll( [FromUri] GetUsersInputModel filter)
        {

            //---------------------------------------------------------------------------------------
            // Validate required filter items 
            //---------------------------------------------------------------------------------------
            if (filter == null)
                throw new HttpBadRequestResponseException("filter is required");
            //---------------------------------------------------------------------------------------


            // Get all users
            var users = _repository.Query<User>();

            if (filter.Active != null && filter.Active != false)
                users = users.Where(n => n.AccountActive == true);

            users = users.Where(n => n.UserName.ToUpper() != "SUPPORT");


            return Mapper.Map<IEnumerable<UserOutputModel>>(users.ToArray().OrderBy(n=>n.UserAndName));

        }

        private IEnumerable<UserOutputModel> GetSupportUser( [FromUri] GetUsersInputModel filter)
        {

            //---------------------------------------------------------------------------------------
            // Validate required filter items 
            //---------------------------------------------------------------------------------------
            if (filter == null)
                throw new HttpBadRequestResponseException("filter is required");
            //---------------------------------------------------------------------------------------

            // Get all users
            var users = _repository.Query<User>().Where(n=>n.UserName.ToUpper() == "SUPPORT");

            return Mapper.Map<IEnumerable<UserOutputModel>>(users.ToArray().OrderBy(n=>n.UserName));

        }



        private IEnumerable<UserOutputModel> GetAdmissionRepsByCampus(Guid campusId, bool? active)
        {

            var campusGroups = _repository.Query<CampusGroup>();
            
            var groupOutputIds = new List<Guid>();
            foreach (var cs in campusGroups)
            {
                groupOutputIds.AddRange(from c in cs.Campuses where c.ID == campusId select cs.ID);
            }

            var users = _repository.Query<User>().Where(u => u.RolesByCampusList.Any(r => r.Role.SystemRole.ID == (int)UserRolEnums.SystemRoles.AdmissionReps));
            if (active != null)
            {
                users = users.Where(n => n.AccountActive == true);
            }

            var userIds = new List<Guid>();
            foreach (var u in users)
            {
                userIds.AddRange(from g in u.CampusGroup where groupOutputIds.Contains(g.ID) select u.ID);
            }

            users = users.Where(n => userIds.Contains(n.ID) && !n.UserName.Contains("support"));

            var outputUsers = Mapper.Map<IEnumerable<UserOutputModel>>(users.ToArray().OrderBy(n => n.UserName));

            var userOutputModels = outputUsers as UserOutputModel[] ?? outputUsers.ToArray();

            foreach (var u in userOutputModels.ToArray())
            {
                UserOutputModel u1 = u;
                var leadsAssigned = _repository.Query<Domain.Lead.Lead>().Where(n => n.AdmissionRepUserObj.ID == u1.Id
                                       && n.LeadStatus.SystemStatus.StatusLevelId == 1
                                        && (n.LeadStatus.SystemStatus.ID == 1 ||
                                            n.LeadStatus.SystemStatusId == 2 ||
                                            n.LeadStatus.SystemStatusId == 4 ||
                                            n.LeadStatus.SystemStatusId == 5));
                u.ActiveLeadCount = leadsAssigned.Count();
            }
            

            return userOutputModels.OrderBy(x => x.FullName);


        }

    }
}