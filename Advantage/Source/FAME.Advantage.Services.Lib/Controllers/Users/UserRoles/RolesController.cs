﻿using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Users.UserRoles;
using FAME.Advantage.Messages.Users.Roles;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Controllers.Users.UserRoles
{
    public class RolesController : ApiController
    {
        //represents data store
        private readonly IRepository _repository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        public RolesController(IRepository repository)
        {
            _repository = repository;
        }

       
        /// <summary>
        /// Get Roles for a specific user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("~/api/User/{userId}/UserRoles")]
        public IEnumerable<RolesOutputModel> GetByUserId(Guid userId, [FromUri] GetRolesInputModel filter)  
        {
            
            //---------------------------------------------------------------------------------------
            // Validate required filter items 
            //---------------------------------------------------------------------------------------
            if(filter.CampusId.IsEmpty())
                throw new HttpBadRequestResponseException("Campus Id is required");

            if (userId.IsEmpty())
                throw new HttpBadRequestResponseException("User Id is required");
            //---------------------------------------------------------------------------------------

            
            // Get roles for this user and return to client
            var roles = _repository.Query<UserRolesCampusGroupBridge>()
                            .Where(n => n.CampusGroup.Campuses.Any(c => c.ID == filter.CampusId) && n.User.ID == userId)
                                .Select(n => n.Role).Distinct();

            return Mapper.Map<IEnumerable<RolesOutputModel>>(roles.ToArray());
           
            
        }
    }
}