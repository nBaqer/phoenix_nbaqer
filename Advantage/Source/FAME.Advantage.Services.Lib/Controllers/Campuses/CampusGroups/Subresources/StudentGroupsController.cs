﻿using System;
using AutoMapper;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;
using FAME.Advantage.Domain.Campuses;
using FAME.Advantage.Messages.StudentGroups;

using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Controllers.Campuses.CampusGroups.Subresources
{
    [RoutePrefix("api/Campuses/CampusGroups/StudentGroups")]
    public class StudentGroupsController : ApiController
    {
        //represents the data store
        private readonly IRepository _repository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        public StudentGroupsController(IRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Get all student groups, filtered by items in filter object
        /// //GET: api/Campuses/CampusGroups/ProgramVersions
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public IEnumerable<StudentGroupOutputModel> GetAll([FromUri] GetStudentGroupsInputModel filter)
        {
            return Get(filter);
        }


        /// <summary>
        /// Get student groups by campus Id
        /// //GET: api/Campuses/{campusId}/CampusGroups/ProgramVersions
        /// </summary>
        /// <param name="campusId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("~/api/Campuses/{campusId}/CampusGroups/StudentGroups")]
        public IEnumerable<StudentGroupOutputModel> GetByCampusId(Guid campusId, [FromUri] GetStudentGroupsInputModel filter)
        {
            return Get(filter, campusId);
        }

        /// <summary>
        /// get method called from public gets.
        /// Retrieve data, applying campusid and/or filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="campusId"></param>
        /// <returns></returns>
        public IEnumerable<StudentGroupOutputModel> Get([FromUri] GetStudentGroupsInputModel filter, Guid? campusId = null)
        {
            //get initial data
            var studentGroup = _repository.Query<StudentGroup>();

            //filter on status code
            if ( ! string.IsNullOrEmpty(filter.StatusCode) )
                studentGroup = studentGroup.Where(n => n.Status.StatusCode == filter.StatusCode);

            //filter on campus id
            if (!campusId.IsEmpty())
                studentGroup = studentGroup.Where(n => n.CampusGroup.Campuses.Any(c => c.ID == campusId));              
            
            //return data to client
            return Mapper.Map<IEnumerable<StudentGroupOutputModel>>(studentGroup.ToArray().OrderBy(n => n.Description));
        }
    }
}
