﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using FAME.Advantage.Domain.Campuses;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Messages.Campuses.CampusGroups.EnrollmentStatuses;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;

namespace FAME.Advantage.Services.Lib.Controllers.Campuses.CampusGroups.Subresources
{
    [RoutePrefix("api/Campuses/CampusGroups/EnrollmentStatuses")]
    public class EnrollmentStatusesController : ApiController
    {
        //represents data store
        private readonly IRepository _repository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        public EnrollmentStatusesController(IRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Get all enrollmentStatuses, using items in filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAll")]
        public IEnumerable<EnrollmentStatusOutputModel> GetAll([FromUri] GetEnrollmentStatusInputModel filter)
        {
            return Get(filter);
        }


        /// <summary>
        /// Get enrollment statuses using campus id, and filter
        /// </summary>
        /// <param name="campusId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("~/api/Campuses/{campusId}/CampusGroups/EnrollmentStatuses")]
        public IEnumerable<EnrollmentStatusOutputModel> GetByCampusId(Guid campusId, [FromUri] GetEnrollmentStatusInputModel filter)
        {
            return Get(filter, campusId);
        }


        /// <summary>
        /// Get method called by either get all or get by campus id.
        /// Apply campusid and/or filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="campusId"></param>
        /// <returns></returns>
        public IEnumerable<EnrollmentStatusOutputModel> Get(GetEnrollmentStatusInputModel filter, Guid? campusId = null)
        {
            //initial retrieval
            var enrollmentStatuses = _repository.Query<UserDefStatusCode>();

            //if passed in, filter by status code
            if (! string.IsNullOrEmpty(filter.StatusCode) )
                enrollmentStatuses = enrollmentStatuses.Where(n => n.Status.StatusCode == filter.StatusCode);

            //1 = lead, 2=StudentEnrollments, 3=Student
            if (filter.StatusLevelId.HasValue)
                enrollmentStatuses = enrollmentStatuses.Where(n => n.SystemStatus.StatusLevelId == filter.StatusLevelId);

            //if passed in, filter by campusId
            if (!campusId.IsEmpty())
                enrollmentStatuses = enrollmentStatuses.Where(n => n.CampusGroup.Campuses.Any(c => c.ID == campusId));              

            //return to client
            return Mapper.Map<IEnumerable<EnrollmentStatusOutputModel>>(enrollmentStatuses.ToArray().OrderBy(n=>n.Description));
        }
    }
}
