﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Messages.Campuses.CampusGroups.ProgramVersions;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;

namespace FAME.Advantage.Services.Lib.Controllers.Campuses.CampusGroups.Subresources
{
    [RoutePrefix("api/Campuses/CampusGroups/ProgramVersions")]
    public class ProgramVersionsController : ApiController
    {
        //represents data store
        private readonly IRepository _repository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        public ProgramVersionsController(IRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Get all program versions, filtering using filter option properties
        /// //GET: api/Campuses/CampusGroups/ProgramVersions
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("~/api/Campuses/CampusGroups/ProgramVersions")]
        public IEnumerable<ProgramVersionOutputModel> GetAll([FromUri] GetProgramVersionInputModel filter)
        {
            return Get(filter);
        }

        /// <summary>
        /// Get program versions for a specific campusId. Apply any filters passed in
        /// </summary>
        /// <param name="campusId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("~/api/Campuses/{campusId}/CampusGroups/ProgramVersions")]
        //GET: api/Campuses/{campusId}/CampusGroups/ProgramVersions
        public IEnumerable<ProgramVersionOutputModel> GetByCampusId(Guid campusId, [FromUri] GetProgramVersionInputModel filter)
        {
            return Get(filter, campusId);
        }


        /// <summary>
        /// Get method called from other public gets
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="campusId"></param>
        /// <returns></returns>
        private IEnumerable<ProgramVersionOutputModel> Get(GetProgramVersionInputModel filter, Guid? campusId = null)
        {
            //get initial data
            var programVersions = _repository.Query<ProgramVersion>();

            //filter on status code
            if (! string.IsNullOrEmpty(filter.StatusCode))
                programVersions = programVersions.Where(n => n.Status.StatusCode == filter.StatusCode);


            //filter on campus Id
            if (!campusId.IsEmpty())
                programVersions = programVersions.Where(n => n.CampusGroup.Campuses.Any(c=>c.ID == campusId));                  
            

            //return to client
            return Mapper.Map<IEnumerable<ProgramVersionOutputModel>>(programVersions.ToArray().OrderBy(n => n.Description));
        }
    }
}
