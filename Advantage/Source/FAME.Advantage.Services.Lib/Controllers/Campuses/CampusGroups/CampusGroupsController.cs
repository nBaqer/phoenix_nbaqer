﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.CampusGroups;

namespace FAME.Advantage.Services.Lib.Controllers.Campuses.CampusGroups
{
    [RoutePrefix("api/Campuses/CampusGroups")]
    public class CampusGroupsController : ApiController
    {
        private readonly IRepository _repository;

        public CampusGroupsController(IRepository repository)
        {
            _repository = repository;
        }

        [Route("")]
        public IEnumerable<CampusGroupOutputModel> Get()
        {
            var campusGroups = _repository.Query<CampusGroup>().ToArray();

            return Mapper.Map<IEnumerable<CampusGroupOutputModel>>(campusGroups);
        }
    }
}