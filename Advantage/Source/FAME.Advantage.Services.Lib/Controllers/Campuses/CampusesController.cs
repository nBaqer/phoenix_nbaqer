﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Messages.Campuses;
using FAME.Advantage.Messages.Common;
using FAME.Advantage.Services.Lib.BusinessLogic.Campuses;
using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;

namespace FAME.Advantage.Services.Lib.Controllers.Campuses
{
    /// <summary>
    /// Controller for Drop Down operation with Campus.
    /// </summary>
    [RoutePrefix("api/Campuses")]
    public class CampusesController : ApiController
    {
// ReSharper disable NotAccessedField.Local
        private readonly IRepository repository;
// ReSharper restore NotAccessedField.Local
        private readonly CampusesBo bo;

        #region Constructor
        public CampusesController(IRepository repository, IRepositoryWithTypedID<int> repositoryWithInt)
        {
            this.repository = repository;
            bo = new CampusesBo(repository, repositoryWithInt);
        }

        #endregion

        #region Http Verb controllers

        /// <summary>
        /// Response to Get
        /// </summary>
        /// <param name="filter">
        /// Refer to class <paramref />
        /// Acceptable Values....
        /// CampusGrpId
        /// UserId
        /// GetAll or no parameter get all.
        /// </param>
        /// <returns>
        /// list of ID  - Description of Campus
        /// </returns>
        [HttpGet]
        [Route("")]
        public IEnumerable<DropDownOutputModel> Get([FromUri] CampusesInputModel filter)
        {
            try
            {
                // Constrains...............
                if (filter != null)
                {
                    if (filter.UserId != Guid.Empty)
                    {
                        var result = bo.GetCampusItemsByUserId(filter);
                        return result;
                    }

                    if (filter.CampusGrpId != Guid.Empty)
                    {
                        var result = bo.GetCampusItemsByCampusGrpId(filter);
                        return result;
                    }

                    if (filter.IncludeAll)
                    {
                        var result = bo.GetAllCampusItems();
                        return result;
                    }
                }

                //If other values
                throw new HttpBadRequestResponseException("You need to supply a parameter.");
            }
            catch (Exception ex)
            {
#if DEBUG
                throw new HttpBadRequestResponseException("Service Return: " + ex.Message);
#else
                throw new HttpBadRequestResponseException("Error in processing Information.");
#endif
            }

        }

        #endregion
     }
}
