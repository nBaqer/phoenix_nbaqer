﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Users;
using FAME.Advantage.Messages.Campuses;

using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;


namespace FAME.Advantage.Services.Lib.Controllers.Campuses
{
    /// <summary>
    /// Controller for Drop Down operation with Campus.
    /// </summary>
    [RoutePrefix("api/CampusWithLead")]
    public class CampusWithLeadController : ApiController
    {
        private readonly IRepository repository;

       
        public CampusWithLeadController(IRepository repository)
        {
            this.repository = repository;
        }
        
        
        [HttpGet]
        [Route("GetWithActiveLeads")]
        public IEnumerable<CampusWithLeadsOutputModel> GetWithActiveLeads([FromUri] CampusesInputModel filter)
        {
            return GetCampusItemsWithLeadCount(filter);
        }

        /// <summary>
        /// Get the list of campus by User Id
        /// </summary>
        /// <param name="filter">user id filter</param>
        /// <returns>a list of ID Description</returns>
        private IEnumerable<CampusWithLeadsOutputModel> GetCampusItemsWithLeadCount([FromUri] CampusesInputModel filter)
        {
            if (filter.UserId == Guid.Empty) { throw new HttpBadRequestResponseException("User Id is required"); }

            var listItems = repository.Query<User>().Where(n => n.ID == filter.UserId).SelectMany(n => n.CampusGroup)
                            .SelectMany(y => y.Campuses).Where(y => y.Status.StatusCode == "A").Distinct(); //.SelectMany(y => y.Description).Distinct();

            var campusOutputList = new List<CampusWithLeadsOutputModel>();
            foreach (var l in listItems)
            {
                var campusOutput = new CampusWithLeadsOutputModel
                {
                    Id = l.ID,
                    Description = l.Description,
                    ActiveLeadCount = repository.Query<FAME.Advantage.Domain.Lead.Lead>().Count(n => n.Campus.ID == l.ID
                                        && n.LeadStatus.SystemStatus.StatusLevelId == 1
                                        && (n.LeadStatus.SystemStatus.ID == 1 || 
                                            n.LeadStatus.SystemStatusId == 2 || 
                                            n.LeadStatus.SystemStatusId == 4 || 
                                            n.LeadStatus.SystemStatusId == 5))
                };

                campusOutputList.Add(campusOutput);
            }


            return campusOutputList;
        }

      
    }
}
