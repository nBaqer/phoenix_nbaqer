﻿using FAME.Advantage.Messages.Student.Enrollments;
using FluentValidation;

namespace FAME.Advantage.Services.Lib.Validators.Student.Enrollments
{
    public class GetEnrollmentsInputModelValidator : AbstractValidator<GetEnrollmentsInputModel>
    {
        public GetEnrollmentsInputModelValidator()
        {
            RuleFor(x => x.PageNumber).GreaterThan(0);
            RuleFor(x => x.PageSize).GreaterThanOrEqualTo(10);
        }
    }
}
