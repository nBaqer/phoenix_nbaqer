﻿using FAME.Advantage.Messages.Requirements.Document;
using FluentValidation;

namespace FAME.Advantage.Services.Lib.Validators.Student.Requirements.Documents
{
    public class GetDocumentInputModelValidator : AbstractValidator<GetDocumentInputModel>
    {
        public GetDocumentInputModelValidator()
        {
            RuleFor(x => x.Page).GreaterThan(0);
            RuleFor(x => x.PageSize).GreaterThanOrEqualTo(10);
        }
    }
}
