﻿
using FAME.Advantage.Messages.Student;
using FluentValidation;

namespace FAME.Advantage.Services.Lib.Validators
{
    public class StudentValidator : AbstractValidator<StudentOutputModel>
    {
        public StudentValidator()
        {
            RuleFor(student => student.FirstName)
                .NotEmpty().WithMessage("First Name is required")
                .Length(0, 50);

            RuleFor(student => student.LastName)
                .NotEmpty().WithMessage("Last Name is required")
                .Length(0, 50);
        }
    }
}
