﻿using FAME.Advantage.Messages.Session;
using FluentValidation;

namespace FAME.Advantage.Services.Lib.Validators.Session
{
    public class CreateSessionInputModelValidation : AbstractValidator<CreateSessionInputModel>
    {
        public CreateSessionInputModelValidation()
        {
            RuleFor(s => s.AuthenticationKey).NotEmpty();
        }
    }
}
