﻿using FAME.Advantage.Messages.Student;
using FluentValidation;

namespace FAME.Advantage.Services.Lib.Validators
{
    public class StudentInputValidator : AbstractValidator<StudentUpdateModel>
    {
        public StudentInputValidator()
        {
            RuleFor(s => s.Id).NotEmpty();

            RuleFor(s => s.Balance)
                .NotEmpty()
                .GreaterThan(0);
        }
    }
}
