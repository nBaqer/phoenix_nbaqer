﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Constants.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Constants type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Utils
{
    /// <summary>
    /// The constants.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// The constants.
        /// </summary>
        public static class TrackSapAttendance
        {
            /// <summary>
            /// The track sap attendance key name.
            /// </summary>
            public const string TrackSapAttendanceKeyName = "TrackSapAttendance";

            /// <summary>
            /// The attendnace by class.
            /// </summary>
            public const string AttendnaceByClass = "byClass";

            /// <summary>
            /// The attendnace by day.
            /// </summary>
            public const string AttendnaceByDay = "byDay";

            /// <summary>
            /// The common tasks.
            /// </summary>
            public const string CommonTasks = "Common Tasks";

            /// <summary>
            /// The general options.
            /// </summary>
            public const string ModuleName = "Faculty";

            /// <summary>
            /// The post attendance.
            /// </summary>
            public const string PostAttendance = "Post Attendance";

            /// <summary>
            /// The enter class section attendance.
            /// </summary>
            public const string EnterClassSectionAttendance = "Enter Class Section Attendance";
        }

        internal static class WapiCodes
        {
            public const string AFA_INTEGRATION = "AFA_INTEGRATION";

        }

        /// <summary>
        /// The configuration app settings keys.
        /// </summary>
        internal static class ConfigurationAppSettingsKeys
        {

            /// <summary>
            /// The enable AFA Integration key.
            /// </summary>
            public const string EnableAFAIntegration = "EnableAFAIntegration";
        }
    }
}
