﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FormatValidator.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Utils.FormatValidator
// </copyright>
// <summary>
//   An util class to validate string formats like PhoneNumbers, Numbers, Zip Codes and Emails
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Utils
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;

    using NHibernate.Util;

    /// <summary>
    /// The format validator.
    /// An utility class to validate string formats like PhoneNumbers, Numbers, Zip Codes and Emails
    /// </summary>
    public class FormatValidator
    {
        /// <summary>
        /// The invalid.
        /// </summary>
        private static bool invalid = false;

        /// <summary>
        /// Validate the phone passed matches phone number characters only ( '0','1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '+', '*', '#', '/', ' ', '(', ')', '_' )
        /// </summary>
        /// <param name="phone">
        /// The phone.
        /// </param>
        /// <returns>
        /// Returns true if is valid otherwise returns false
        /// </returns>
        public static bool IsValidPhoneNumber(string phone)
        {
            char[] validPhoneFormat = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '+', '*', '#', '/', ' ', '(', ')', '_' };
            return (from i in phone
                    where validPhoneFormat.Contains(i) == false
                    select i).FirstOrNull() == null;
        }

        /// <summary>
        /// Validate the given text only matches numbers characters
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        /// <returns>
        /// Returns true if is valid otherwise returns false
        /// </returns>
        public static bool IsOnlyNumbers(string text)
        {
            char[] valid = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            return (from i in text
                    where valid.Contains(i) == false
                    select i).FirstOrNull() == null;
        }

        /// <summary>
        /// Validate the given text matches a limit of 5 digit for zip code and that only number are included
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        /// <param name="isInternational">
        /// The is international.
        /// </param>
        /// <returns>
        /// Returns true if is valid otherwise returns false
        /// </returns>
        public static bool IsValidZipCode(string text, bool isInternational)
        {
            if (!isInternational)
            {
                return IsOnlyNumbers(text) && text.Length <= 5;
            }
            return true;
        }

        /// <summary>
        /// Validate the given string is a valid email address
        /// </summary>
        /// <param name="value">
        /// The value to validate.
        /// </param>
        /// <returns>
        ///  Returns true if is valid otherwise returns false
        /// </returns>
        public static bool IsValidEmail(string value)
        {
            invalid = false;
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                value = Regex.Replace(value, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
            {
                return false;
            }

            // Return true if value is in valid e-mail format.
            try
            {
                return Regex.IsMatch(value, @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$", RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        /// <summary>
        /// The domain mapper.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        /// <returns>
        /// Returns true if is valid otherwise returns false
        /// </returns>
        private static string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }

            return match.Groups[1].Value + domainName;
        }
    }
}
