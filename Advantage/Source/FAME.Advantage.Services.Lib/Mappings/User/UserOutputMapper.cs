﻿using AutoMapper;
using FAME.Advantage.Messages.Users;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.User
{
    using AutoMapper.Configuration;

    public class UserOutputMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.Users.User, UserOutputModel>()
                .ForMember(dest => dest.FullName, option => option.MapFrom(source => source.FullName));
        }
    }
}
