﻿using AutoMapper;
using FAME.Advantage.Domain.Users.UserRoles;
using FAME.Advantage.Messages.Users.Roles;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.User.UserRoles
{
    using AutoMapper.Configuration;

    public class ResourceOutputMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Resources, ResourceOutputModel>();
        }
    }
}
