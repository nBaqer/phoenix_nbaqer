﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemStatusChangeHistoryModelMapper.cs" company="FAME Inc.">
// FAME.Advantage.Services.Lib.Mappings.SystemStuff  
// </copyright>
// <summary>
//   Defines the SystemStatusChangeHistoryModelMapper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff
{
    using System;

    using AutoMapper;
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.SystemStuff;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;
    using FAME.Advantage.Services.Lib.Models.SystemStatus;

    /// <summary>
    /// The system status change history model mapper.
    /// </summary>
    public class SystemStatusChangeHistoryModelMapper : IMapperDefinition
    {
        /// <summary>
        /// The create definition.
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<SystemStatusChangeHistory, SystemStatusChangeHistoryModel>();
        }
    }
}
