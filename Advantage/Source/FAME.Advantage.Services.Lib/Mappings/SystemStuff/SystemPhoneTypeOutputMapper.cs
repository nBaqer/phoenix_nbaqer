﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FAME.Advantage.Messages.Lead;
using FAME.Advantage.Messages.SystemStuff.SystemStatuses;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff
{
    using AutoMapper.Configuration;

    public class SystemPhoneTypeOutputMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            //Mapper.CreateMap<LeadEmail, LeadEmailOutputModel>();
            Mapper.CreateMap<Domain.SystemStuff.SyPhoneType, SystemPhoneTypeOuputModel>()
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.PhoneTypeDescrip))
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID.ToString()));
        }
    }
}
