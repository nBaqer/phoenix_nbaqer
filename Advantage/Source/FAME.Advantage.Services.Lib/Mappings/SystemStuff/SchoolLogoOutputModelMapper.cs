﻿using AutoMapper;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Messages.SystemStuff.Maintenance;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff
{
    using AutoMapper.Configuration;

    public class SchoolLogoOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<SySchoolLogo, SchoolLogoOutputModel>();
        }
    }
}
