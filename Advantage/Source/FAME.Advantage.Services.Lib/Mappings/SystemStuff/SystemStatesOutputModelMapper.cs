﻿using AutoMapper;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Messages.SystemStuff.SystemStates;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff
{
    using AutoMapper.Configuration;

    public class StudentStatesOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<SystemStates, SystemStatesOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description))
                .ForMember(dest => dest.Code, option => option.MapFrom(source => source.Code))
                .ForMember(dest => dest.FullDescription, option => option.MapFrom(source => source.Code + " - " + source.Description));
        }
    }
}