﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionOutputModelMapper.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Mappings.SystemStuff.Institution.InstitutionOutputModelMapper
// </copyright>
// <summary>
//   The institution output model mapper.
//   Maps Domain Entity <code>HighSchools</code> with message output model: <code>InstitutionOutputModel</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff.Institution
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using AutoMapper;
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.SystemStuff.Intitution;
    using FAME.Advantage.Messages.SystemStuff.Institution;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    /// The institution output model mapper.
    /// Maps Domain Entity <code>HighSchools</code> with message output model: <code>InstitutionOutputModel</code>
    /// </summary>
    public class InstitutionOutputModelMapper : IMapperDefinition
    {
        /// <summary>
        /// The create definition.
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            var map = Mapper.CreateMap<HighSchools, InstitutionOutputModel>();
            //map.ForAllMembers(opt => opt.Ignore());

            map.ForMember(dest => dest.Id, option => option.MapFrom(source => source.ID));
            map.ForMember(dest => dest.Name, option => option.MapFrom(source => source.HsDescrip));
            map.ForMember(dest => dest.LevelId, option => option.MapFrom(source => source.Level != null ? source.Level.ID : 0));
            map.ForMember(dest => dest.TypeId, option => option.MapFrom(source => source.ImportType != null ? source.ImportType.ID : 0));
            map.ForMember(dest => dest.Addresses, option => option.MapFrom(source => source.Addresses));
            map.ForMember(dest => dest.Contacts, option => option.MapFrom(source => source.Contacts));
            map.ForMember(dest => dest.Phones, option => option.MapFrom(source => source.Phones));
        }
    }
}
