﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionContactOutputModelMapper.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Mappings.SystemStuff.Institution.InstitutionContactOutputModelMapper
// </copyright>
// <summary>
//   The institution contact output model mapper.
//   Maps Domain Entity <code>InstitutionContact</code> with message output model: <code>InstitutionContactModel</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff.Institution
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using AutoMapper;
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.SystemStuff.Intitution;
    using FAME.Advantage.Messages.SystemStuff.Institution;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    /// The institution contact output model mapper.
    /// Maps Domain Entity <code>InstitutionContact</code> with message output model: <code>InstitutionContactModel</code>
    /// </summary>
    public class InstitutionContactOutputModelMapper : IMapperDefinition
    {
        /// <summary>
        /// The create definition.
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            var map = Mapper.CreateMap<InstitutionContact, InstitutionContactModel>();
            map.ForAllMembers(opt => opt.Ignore());

            map.ForMember(dest => dest.Id, option => option.MapFrom(source => source.ID));
            map.ForMember(dest => dest.Email, option => option.MapFrom(source => source.Email));
            map.ForMember(dest => dest.FirstName, option => option.MapFrom(source => source.FirstName));
            map.ForMember(dest => dest.LastName, option => option.MapFrom(source => source.LastName));
            map.ForMember(dest => dest.MiddleName, option => option.MapFrom(source => source.MiddleName));
            map.ForMember(dest => dest.Phone, option => option.MapFrom(source => source.Phone));
            map.ForMember(dest => dest.PrefixId, option => option.MapFrom(source => source.Prefix != null ? source.Prefix.ID : Guid.Empty));
            map.ForMember(dest => dest.Prefix, option => option.MapFrom(source => source.Prefix != null ? source.Prefix.Description : string.Empty));
            map.ForMember(dest => dest.PhoneExtension, option => option.MapFrom(source => source.PhoneExt));
            map.ForMember(dest => dest.SuffixId, option => option.MapFrom(source => source.Suffix != null ? source.Suffix.ID : Guid.Empty));
            map.ForMember(dest => dest.Suffix, option => option.MapFrom(source => source.Suffix != null ? source.Suffix.Description : string.Empty));
            map.ForMember(dest => dest.Title, option => option.MapFrom(source => source.Title));
            map.ForMember(dest => dest.InstitutionId, option => option.MapFrom(source => source.Institution != null ? source.Institution.ID : Guid.Empty));
            map.ForMember(dest => dest.IsDefault, option => option.MapFrom(source => source.IsDefault));
            map.ForMember(dest => dest.StatusId, option => option.MapFrom(source => source.Status != null ? source.Status.ID : Guid.Empty));
            map.ForMember(dest => dest.Status, option => option.MapFrom(source => source.Status != null ? source.Status.Status : string.Empty));
        }
    }
}

