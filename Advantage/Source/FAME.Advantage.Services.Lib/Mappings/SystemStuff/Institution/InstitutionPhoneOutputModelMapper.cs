﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionPhoneOutputModelMapper.cs" company="FAME Inc. 2016">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Mappings.SystemStuff.Institution.InstitutionPhoneOutputModelMapper
// </copyright>
// <summary>
//   The institution phone output model mapper.
//   Maps Domain Entity <code>InstitutionPhone</code> with message output model: <code>InstitutionPhoneModel</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff.Institution
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using AutoMapper;
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.SystemStuff.Intitution;
    using FAME.Advantage.Messages.SystemStuff.Institution;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    /// The institution phone output model mapper.
    /// /// Maps Domain Entity <code>InstitutionPhone</code> with message output model: <code>InstitutionPhoneModel</code>
    /// </summary>
    public class InstitutionPhoneOutputModelMapper : IMapperDefinition
    {
        /// <summary>
        /// The create definition.
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            var map = Mapper.CreateMap<InstitutionPhone, InstitutionPhoneModel>();
            map.ForAllMembers(opt => opt.Ignore());

            map.ForMember(dest => dest.Id, option => option.MapFrom(source => source.ID));
            map.ForMember(dest => dest.Phone, option => option.MapFrom(source => source.Phone));
            map.ForMember(dest => dest.IsForeignPhone, option => option.MapFrom(source => source.IsForeignPhone));

            map.ForMember(dest => dest.TypeId, option => option.MapFrom(source => source.Type != null ? source.Type.ID : Guid.Empty));
            map.ForMember(dest => dest.Type, option => option.MapFrom(source => source.Type != null ? source.Type.PhoneTypeDescrip : string.Empty));

            map.ForMember(dest => dest.InstitutionId, option => option.MapFrom(source => source.Institution != null ? source.Institution.ID : Guid.Empty));
            map.ForMember(dest => dest.StatusId, option => option.MapFrom(source => source.Status != null ? source.Status.ID : Guid.Empty));
        }
    }
}
