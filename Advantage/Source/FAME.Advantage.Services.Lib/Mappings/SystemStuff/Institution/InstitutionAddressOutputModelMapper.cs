﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionAddressOutputModelMapper.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Mappings.SystemStuff.Institution.InstitutionAddressOutputModelMapper
// </copyright>
// <summary>
//   The institution address output model mapper.
//   Maps Domain Entity <code>InstitutionAddress</code> with message output model: <code>InstitutionAddressModel</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff.Institution
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using AutoMapper;
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.SystemStuff.Intitution;
    using FAME.Advantage.Messages.SystemStuff.Institution;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    /// The institution address output model mapper.
    /// Maps Domain Entity <code>InstitutionAddress</code> with message output model: <code>InstitutionAddressModel</code>
    /// </summary>
    public class InstitutionAddressOutputModelMapper : IMapperDefinition
    {
        /// <summary>
        /// The create definition.
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            var map = Mapper.CreateMap<InstitutionAddress, InstitutionAddressModel>();
            map.ForAllMembers(opt => opt.Ignore());

            map.ForMember(dest => dest.Id, option => option.MapFrom(source => source.ID));
            map.ForMember(dest => dest.IsMailingAddress, option => option.MapFrom(source => source.IsMailingAddress));
            map.ForMember(dest => dest.Address1, option => option.MapFrom(source => source.Address1));
            map.ForMember(dest => dest.Address2, option => option.MapFrom(source => source.Address2));
            map.ForMember(dest => dest.AddressApto, option => option.MapFrom(source => source.AddressApto));
            map.ForMember(dest => dest.City, option => option.MapFrom(source => source.City));
            map.ForMember(dest => dest.CountryId, option => option.MapFrom(source => source.Country != null ? source.Country.ID : Guid.Empty));
            map.ForMember(dest => dest.CountyId, option => option.MapFrom(source => source.County != null ? source.County.ID : Guid.Empty));
            map.ForMember(dest => dest.Country, option => option.MapFrom(source => source.IsInternational ? source.ForeignCountry : source.Country != null ? source.Country.CountryCode : string.Empty));
            map.ForMember(dest => dest.County, option => option.MapFrom(source => source.IsInternational ? source.ForeignCounty : source.County != null ? source.County.CountyCode : string.Empty));
            map.ForMember(dest => dest.InstitutionId, option => option.MapFrom(source => source.Institution != null ? source.Institution.ID : Guid.Empty));
            map.ForMember(dest => dest.IsDefault, option => option.MapFrom(source => source.IsDefault));
            map.ForMember(dest => dest.IsInternational, option => option.MapFrom(source => source.IsInternational));
            map.ForMember(dest => dest.State, option => option.MapFrom(source => source.IsInternational ? source.OtherState : source.State != null ? source.State.Code : string.Empty));
            map.ForMember(dest => dest.StateId, option => option.MapFrom(source => source.State != null ? source.State.ID : Guid.Empty));
            map.ForMember(dest => dest.StatusId, option => option.MapFrom(source => source.Status != null ? source.Status.ID : Guid.Empty));
            map.ForMember(dest => dest.TypeId, option => option.MapFrom(source => source.AddressType != null ? source.AddressType.ID : Guid.Empty));
            map.ForMember(dest => dest.Type, option => option.MapFrom(source => source.AddressType != null ? source.AddressType.AddressDescrip : string.Empty));
            map.ForMember(dest => dest.ZipCode, option => option.MapFrom(source => source.ZipCode));
        }
    }
}
