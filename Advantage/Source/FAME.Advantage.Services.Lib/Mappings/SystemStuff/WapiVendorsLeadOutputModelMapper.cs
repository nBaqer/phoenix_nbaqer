﻿using AutoMapper;
using FAME.Advantage.Messages.SystemStuff.Maintenance;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff
{
    using AutoMapper.Configuration;

    /// <summary>
    /// Mapper for WapiVendorsLeadOutputModel
    /// </summary>
    public class WapiVendorsLeadOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.Lead.AdVendors, WapiVendorsLeadOutputModel>();

            Mapper.CreateMap<Domain.Lead.AdVendors, WapiVendorsLeadOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.DateOperationBegin,
                    option => option.MapFrom(source => source.DateOperationBegin))
                .ForMember(dest => dest.DateOperationEnd, option => option.MapFrom(source => source.DateOperationEnd))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description))
                .ForMember(dest => dest.IsActive, option => option.MapFrom(source => source.IsActive))
                .ForMember(dest => dest.VendorCode, option => option.MapFrom(source => source.VendorCode))
                .ForMember(dest => dest.VendorName, option => option.MapFrom(source => source.VendorName));

        }

    }
}
