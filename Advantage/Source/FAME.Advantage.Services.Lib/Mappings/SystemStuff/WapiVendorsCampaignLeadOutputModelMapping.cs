﻿using AutoMapper;
using FAME.Advantage.Messages.SystemStuff.Maintenance;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff
{
    using AutoMapper.Configuration;

    public class WapiVendorsCampaignLeadOutputModelMapping : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)

        {
            Mapper.CreateMap<Domain.Lead.AdVendorCampaign, WapiVendorsCampaignLeadOutputModel>();

            Mapper.CreateMap<Domain.Lead.AdVendorCampaign, WapiVendorsCampaignLeadOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.DateCampaignBegin, option => option.MapFrom(source => source.DateCampaignBegin))
                .ForMember(dest => dest.DateCampaignEnd, option => option.MapFrom(source => source.DateCampaignEnd))
                .ForMember(dest => dest.AccountId, option => option.MapFrom(source => source.AccountId))
                .ForMember(dest => dest.CampaignCode, option => option.MapFrom(source => source.CampaignCode))
                .ForMember(dest => dest.Cost, option => option.MapFrom(source => source.Cost))
                .ForMember(dest => dest.FilterRejectByCounty, option => option.MapFrom(source => source.FilterRejectByCounty))

               .ForMember(dest => dest.FilterRejectDuplicates, option => option.MapFrom(source => source.FilterRejectDuplicates))
               .ForMember(dest => dest.PayFor, option => option.MapFrom(source => source.PayForObj))
               .ForMember(dest => dest.IsActive, option => option.MapFrom(source => source.IsActive))
               .ForMember(dest => dest.VendorId, option => option.MapFrom(source => source.VendorObj.ID));
        }
    }
}
