﻿using AutoMapper;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Domain.SystemStuff.Resources;
using FAME.Advantage.Messages.SystemStuff.SystemModules;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff
{
    using AutoMapper.Configuration;

    public class SystemModuleOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<SystemModule, SystemModuleOutputModel>();
        }
    }
}
