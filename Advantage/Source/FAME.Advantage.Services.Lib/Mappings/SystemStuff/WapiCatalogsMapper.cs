﻿using AutoMapper;
using FAME.Advantage.Messages.SystemStuff.Wapi;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff
{
    using AutoMapper.Configuration;

    public class WapiCatalogsMapper: IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.SystemStuff.SyWapiExternalCompanies, WapiCatalogsOutputModel>()
               .ForMember(dest => dest.Url, action => action.Ignore())
                .ForMember(dest => dest.Authorized, action => action.Ignore());

            Mapper.CreateMap<Domain.SystemStuff.SyWapiExternalOperationMode, WapiCatalogsOutputModel>()
                .ForMember(dest => dest.IsActive, action => action.Ignore())
                .ForMember(dest => dest.Url, action => action.Ignore())
                 .ForMember(dest => dest.Authorized, action => action.Ignore());

            Mapper.CreateMap<Domain.SystemStuff.SyWapiAllowedServices, WapiCatalogsOutputModel>()
                 .ForMember(dest => dest.Authorized, action => action.Ignore());

            Mapper.CreateMap<Domain.SystemStuff.SyWapiSettings, WapiCatalogsOutputModel>()
                .ForMember(dest => dest.Code, option => option.MapFrom(source => source.CodeOperation))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.CodeOperation))
                .ForMember(dest => dest.Id, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.IsActive, option => option.MapFrom(source => source.IsActive))
                .ForMember(dest => dest.Url, action => action.Ignore())
                .ForMember(dest => dest.Authorized, action => action.Ignore());
        }
    }
}
