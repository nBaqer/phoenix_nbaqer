﻿using AutoMapper;
using FAME.Advantage.Domain.SystemStuff.Menu;
using FAME.Advantage.Messages.SystemStuff.Menu;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff.Menu
{
    using AutoMapper.Configuration;

    public class MenuItemTypeOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<MenuItemType, MenuItemTypeOutputModel>();
        }
    }
}
