﻿using AutoMapper;
using FAME.Advantage.Domain.SystemStuff.Menu;
using FAME.Advantage.Messages.SystemStuff.Menu;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff.Menu
{
    using AutoMapper.Configuration;

    public class MenuItemsOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<MenuItem, MenuItemsOutputModel>();

            Mapper.CreateMap<MenuItem, MenuItemsOutputModel>()
                .ForMember(dest => dest.text, option => option.MapFrom(source => source.MenuName))
                .ForMember(dest => dest.url, option => option.MapFrom(source => source.FullUrl))
                .ForMember(dest => dest.IsEnabled, option => option.MapFrom(source => source.IsEnabled))
                .ForMember(dest => dest.items, option => option.MapFrom(source => source.ChildrenForOutput));


        }
    }
}
