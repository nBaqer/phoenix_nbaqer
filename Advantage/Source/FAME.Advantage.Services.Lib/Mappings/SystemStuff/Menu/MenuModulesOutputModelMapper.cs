﻿using AutoMapper;
using FAME.Advantage.Domain.SystemStuff.Menu;
using FAME.Advantage.Messages.SystemStuff.Menu;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff.Menu
{
    using AutoMapper.Configuration;

    public class MenuModulesOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<MenuItem, MenuModulesOutputModel>();

            Mapper.CreateMap<MenuItem, MenuModulesOutputModel>()
                .ForMember(dest=>dest.Id, options => options.MapFrom(source=>source.ID))
                .ForMember(dest => dest.text, option => option.MapFrom(source => source.MenuName));



        }
    }
}
