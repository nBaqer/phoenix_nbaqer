﻿using AutoMapper;
using FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings;
using FAME.Advantage.Messages.SystemStuff.ConfigurationAppSettings;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff.ConfigurationAppSettings
{
    using AutoMapper.Configuration;

    public class ConfigurationAppSettingsOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<ConfigurationAppSetting, ConfigurationAppSettingOutputModel>();
        }
    }
}   