﻿using AutoMapper;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Messages.SystemStuff.SystemStatuses;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff
{
    using AutoMapper.Configuration;

    public class StudentStatusOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<SystemStatus, SystemStatusOutputModel>();

            Mapper.CreateMap<UserDefStatusCode, SystemStatusCodeModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description))
                .ForMember(dest => dest.StatusId, option => option.MapFrom(source => source.SystemStatus.ID));
        }
    }
}