﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FAME.Advantage.Messages.SystemStuff.SystemStatuses;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff
{
    using AutoMapper.Configuration;

    public class SystemEmailTypeOutputMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.SystemStuff.SyEmailType, SystemEmailTypeOutputModel>()
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.EMailTypeDescription))
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID.ToString()));
        }
    }
}
