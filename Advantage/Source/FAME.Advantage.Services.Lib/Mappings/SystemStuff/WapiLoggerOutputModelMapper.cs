﻿using AutoMapper;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Messages.SystemStuff.Maintenance;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff
{
    using AutoMapper.Configuration;

    public class WapiLoggerOutputModelMapper : IMapperDefinition
    {
        #region Implementation of IMapperDefinition

        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<SyWapiOperationLogger, WapiLoggerOutputModel>()
                .ForMember(dest => dest.Id, option => option.MapFrom(source => source.ID));
        }

        #endregion
    }
}
