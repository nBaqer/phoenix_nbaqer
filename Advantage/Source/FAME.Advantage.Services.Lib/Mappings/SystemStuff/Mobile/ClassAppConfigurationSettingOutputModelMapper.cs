﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassAppConfigurationSettingOutputModelMapper.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the ClassAppConfigurationSettingOutputModelMapper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff.Mobile
{
    using AutoMapper;
    using AutoMapper.Configuration;

    using Domain.SystemStuff.Mobile;
    using Messages.SystemStuff.Mobile;
    using Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    ///  The class application configuration setting output model mapper.
    /// </summary>
    public class ClassAppConfigurationSettingOutputModelMapper : IMapperDefinition
    {
        /// <summary>
        /// The create definition.
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            var map = Mapper.CreateMap<SyKlassAppConfigurationSetting, ClassAppConfigurationOutputModel>();
            map.ForMember(dest => dest.CodeEntity, option => option.MapFrom(source => source.KlassEntityObject.Code))
                .ForMember(
                    dest => dest.CodeOperation,
                    option => option.MapFrom(source => source.KlassOperationTypeObject.Code));
        }
    }
}
