﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassAppKeyValueMapper.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the ClassAppKeyValueMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff.Mobile
{
    using AutoMapper;
    using AutoMapper.Configuration;

    using Domain.Student.Enrollments;
    using Domain.SystemStuff;

    using FAME.Advantage.Domain.Campuses;
    using FAME.Advantage.Domain.SystemStuff.Mobile;

    using Infrastructure.AutoMapper;

    using Messages.SystemStuff.Mobile.ApiKlass;

    /// <summary>
    /// The class_app key value map.
    /// </summary>
    public class ClassAppKeyValueMapper : IMapperDefinition
    {
        /// <summary>
        /// The create definition.
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<ProgramVersion, Majors>()
            .ForMember(dest => dest.name, option => option.MapFrom(source => source.Description))
                .ForMember(dest => dest.updated_at, option => option.MapFrom(source => source.ModDate))
                .ForMember(dest => dest.id, option => option.Ignore())
                .ForMember(dest => dest.created_at, option => option.Ignore())
                .ForMember(dest => dest.type, option => option.Ignore())
                .ForMember(dest => dest.auto_disabled, option => option.Ignore())
                .ForMember(dest => dest.deleted, option => option.Ignore());
 
            Mapper.CreateMap<UserDefStatusCode, KeyValuePart>()
            .ForMember(dest => dest.name, option => option.MapFrom(source => source.Description))
                .ForMember(dest => dest.updated_at, option => option.MapFrom(source => source.ModifiedDate))
                .ForMember(dest => dest.created_at, option => option.Ignore())
                .ForMember(dest => dest.type, option => option.Ignore())
                .ForMember(dest => dest.deleted, option => option.Ignore())
                .ForMember(dest => dest.id, option => option.Ignore());

            Mapper.CreateMap<SyKlassAppConfigurationSetting, CustomField>()
               .ForMember(dest => dest.class_type, option => option.MapFrom(source => source.KlassEntityObject.Code))
               .ForMember(dest => dest.updated_at, option => option.MapFrom(source => source.ModDate))
               .ForMember(dest => dest.item_label, option => option.MapFrom(source => source.ItemLabel))
               .ForMember(dest => dest.field_type, option => option.MapFrom(source => source.ItemValueType))
               .ForMember(dest => dest.id, option => option.MapFrom(source => source.KlassAppIdentification))
               .ForMember(dest => dest.description, option => option.Ignore())
               .ForMember(dest => dest.created_at, option => option.Ignore())
               .ForMember(dest => dest.item_value, option => option.Ignore())
               .ForMember(dest => dest.deleted, option => option.Ignore())
               .ForMember(dest => dest.item_order, option => option.UseValue(-1));

            Mapper.CreateMap<Campus, Locations>()
                .ForMember(dest => dest.address, option => option.MapFrom(source => source.Address1))
                .ForMember(dest => dest.contact_email, option => option.MapFrom(source => source.Email))
                .ForMember(dest => dest.contact_phone, option => option.MapFrom(source => source.Phone1))
                .ForMember(dest => dest.name, option => option.MapFrom(source => source.Description))
                .ForMember(dest => dest.phone, option => option.MapFrom(source => source.Phone1))
                .ForMember(dest => dest.updated_at, option => option.MapFrom(source => source.ModDate))
                .ForMember(dest => dest.location_type, option => option.UseValue(string.Empty))
                .ForMember(dest => dest.contact_person, option => option.Ignore())
                .ForMember(dest => dest.contact_position, option => option.Ignore())
                .ForMember(dest => dest.created_at, option => option.Ignore())
                .ForMember(dest => dest.deleted, option => option.Ignore())
                .ForMember(dest => dest.id, option => option.Ignore())
                .ForMember(dest => dest.referrals_email_address, option => option.Ignore())
                .ForMember(dest => dest.payments_currency, option => option.Ignore())
                .ForMember(dest => dest.payments_on, option => option.Ignore())
                .ForMember(dest => dest.payments_worldpay_form_id, option => option.Ignore())
                .ForMember(dest => dest.payments_worldpay_gateway_id, option => option.Ignore())
                .ForMember(dest => dest.payments_worldpay_id, option => option.Ignore())
                .ForMember(dest => dest.payments_worldpay_session, option => option.Ignore())
                .ForMember(dest => dest.payments_currency, option => option.Ignore())
                .ForMember(dest => dest.payments_email_address, option => option.Ignore())
                .ForMember(dest => dest.payments_worldpay_key, option => option.Ignore());
        }
    }
}
