﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppStudentOutputModelLeadMapper.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the KlassAppStudentOutputModelLeadMapper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff.Mobile
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Domain.Student.Enrollments;
    using FAME.Advantage.Services.Lib.BusinessLogic.SystemStuff.Mobile;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    /// The <code>klass app</code> student output model lead mapper.
    /// </summary>
    public class KlassAppStudentOutputModelLeadMapper : IMapperDefinition
    {
        /// <summary>
        /// The active enrollment.
        /// </summary>
        private Enrollment activeEnrollment;

        /// <summary>
        /// The create definition.
        /// </summary>
        /// <param name="Mapper">
        /// The Mapper.
        /// </param>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            this.activeEnrollment = null;
            Mapper.CreateMap<Lead, KlassAppStudentOutputModel>()
                .ForMember(dest => dest.first_name, option => option.MapFrom(source => source.FirstName))
                .ForMember(dest => dest.last_name, option => option.MapFrom(source => source.LastName))
                .ForMember(dest => dest.college_id, option => option.MapFrom(source => source.StudentNumber))
                .ForMember(dest => dest.role, option => option.UseValue("student"))
                .ForMember(dest => dest.password, option => option.MapFrom(source => source.StudentNumber))
                .ForMember(
                    dest => dest.major,
                    option => option.MapFrom(source => this.GetProgramVersion(source.EnrollmentList)))
                .ForMember(dest => dest.lda, option => option.MapFrom(source => this.GetLda(source.EnrollmentList)))
                .ForMember(
                    dest => dest.status,
                    option => option.MapFrom(source => this.GetEnrollment(source.EnrollmentList)))
                .ForMember(
                    dest => dest.enrollmentId,
                    option => option.MapFrom(source => this.GetEnrollmentId(source.EnrollmentList)))
                .ForMember(dest => dest.date_of_birth, option => option.MapFrom(source => source.BirthDate))
                .ForMember(dest => dest.gender, option => option.MapFrom(source => source.GenderObj.Description))
                .ForMember(
                    dest => dest.phone,
                    option => option.MapFrom(
                        source => source.LeadPhoneList.Count > 0 ? source.LeadPhoneList.First().Phone : "-"))
                .ForMember(
                    dest => dest.email,
                    option => option.MapFrom(
                        source => source.LeadEmailList.Count > 0 ? source.LeadEmailList.First().Email : "-"))
                .ForMember(
                    dest => dest.phone_other,
                    option => option.MapFrom(
                        source => source.LeadPhoneList.Count > 1 ? source.LeadPhoneList.ElementAt(1).Phone : "-"))
                .ForMember(
                    dest => dest.address,
                    option => option.MapFrom(
                        source => source.AddressList.Count > 0 ? source.AddressList.First().Address1 : "-"))
                .ForMember(
                    dest => dest.address_city,
                    option => option.MapFrom(
                        source => source.AddressList.Count > 0 ? source.AddressList.First().City : "-"))
                .ForMember(
                    dest => dest.address_state,
                    option => option.MapFrom(
                        source => source.AddressList.Count > 0
                                      ? source.AddressList.First().IsInternational
                                            ? source.AddressList.First().StateInternational
                                            : source.AddressList.First().StateObj.Code
                                      : "-")).ForMember(
                    dest => dest.address_zip,
                    option => option.MapFrom(
                        source => source.AddressList.Count > 0 ? source.AddressList.First().ZipCode : string.Empty))
                .ForMember(
                    dest => dest.locations,
                    option => option.MapFrom(source => source.Campus.ID.ToString().ToUpper())).ForMember(
                    dest => dest.college_id,
                    option => option.MapFrom(source => source.StudentNumber))
                //// .ForMember(dest => dest.custom_fields, option => option.UseValue(new List<KeyValuePair<int, object>>()))
                .ForMember(dest => dest.cf_dictionary, option => option.UseValue(new Dictionary<int, object>()))
                .ForMember(
                    dest => dest.start_date,
                    option => option.MapFrom(source => this.GetStartDate(source.EnrollmentList))).ForMember(
                    dest => dest.graduation_date,
                    option => option.MapFrom(source => this.GetGraduationDate(source.EnrollmentList)));
                
            Mapper.CreateMap<IList<Lead>, IList<KlassAppStudentOutputModel>>();
        }

        /// <summary>
        /// The get program version.
        /// </summary>
        /// <param name="sourceEnrollmentList">
        /// The source enrollment list.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetProgramVersion(IList<Enrollment> sourceEnrollmentList)
        {
            var enrollment = this.GetEnrollmentActive(sourceEnrollmentList);
            if (enrollment != null)
            {
              return enrollment.ProgramVersion.ID.ToString().ToUpper();  
            }

            return string.Empty;
        }

        /// <summary>
        /// The get start date.
        /// </summary>
        /// <param name="sourceEnrollmentList">
        /// The source enrollment list.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetStartDate(IList<Enrollment> sourceEnrollmentList)
        {
            var enrollment = this.GetEnrollmentActive(sourceEnrollmentList);
            if (enrollment != null)
            {
                if (enrollment.StartDate.HasValue)
                {
                    return enrollment.StartDate.Value.Date.ToShortDateString();
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// The get graduation date.
        /// </summary>
        /// <param name="sourceEnrollmentList">
        /// The source enrollment list.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetGraduationDate(IList<Enrollment> sourceEnrollmentList)
        {
            var enrollment = this.GetEnrollmentActive(sourceEnrollmentList);
            if (enrollment != null)
            {
                if (enrollment.ExpectedGraduationDate.HasValue)
                {
                    return enrollment.ExpectedGraduationDate.Value.Date.ToShortDateString();
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// The get enrollment.
        /// </summary>
        /// <param name="sourceEnrollmentList">
        /// The source enrollment list.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetEnrollment(IList<Enrollment> sourceEnrollmentList)
        {
            var enrollment = this.GetEnrollmentActive(sourceEnrollmentList);
            if (enrollment != null)
            {
                 return enrollment.EnrollmentStatus.ID.ToString().ToUpper();
            }

            return string.Empty;
        }

        /// <summary>
        /// The get enrollment id.
        /// </summary>
        /// <param name="sourceEnrollmentList">
        /// The source enrollment list.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetEnrollmentId(IList<Enrollment> sourceEnrollmentList)
        {
            var enrollment = this.GetEnrollmentActive(sourceEnrollmentList);
            if (enrollment != null)
            {
                return enrollment.ID.ToString().ToUpper();
            }

            return string.Empty;
        }

        /// <summary>
        /// The get LDA.
        /// </summary>
        /// <param name="sourceEnrollmentList">
        /// The source enrollment list.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        private DateTime GetLda(IList<Enrollment> sourceEnrollmentList)
        {
            var enrollment = this.GetEnrollmentActive(sourceEnrollmentList);
            if (enrollment != null)
            {
                 return enrollment.Lda.GetValueOrDefault();
            }

            return DateTime.MinValue;
        }

        /// <summary>
        /// The get enrollment active.
        /// </summary>
        /// <param name="sourceEnrollmentList">
        /// The source enrollment list.
        /// </param>
        /// <returns>
        /// The <see cref="Enrollment"/>.
        /// </returns>
        private Enrollment GetEnrollmentActive(IList<Enrollment> sourceEnrollmentList)
        {
            if (this.activeEnrollment != null)
            {
                return this.activeEnrollment;
            }

            var listfiltered = sourceEnrollmentList.Where(x => x.EnrollmentStatus != null).ToList();
            if (listfiltered.Count > 0)
            {
                var mindate = listfiltered.Where(x => x.EnrollmentStatus.SystemStatus.IsInSchool == true)
                    .Min(x => x.EnrollmentDate);
                var minenr = listfiltered.First(
                    x => x.EnrollmentStatus.SystemStatus.IsInSchool == true && x.EnrollmentDate == mindate);
                return minenr;
            }

            return null;
        }
    }
}
