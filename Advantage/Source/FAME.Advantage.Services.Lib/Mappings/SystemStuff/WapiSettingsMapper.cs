﻿using AutoMapper;
using FAME.Advantage.Messages.SystemStuff.Maintenance;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.SystemStuff
{
    using AutoMapper.Configuration;

    public class WapiSettingsMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)

        {
            Mapper.CreateMap<Domain.SystemStuff.SyWapiSettings, WapiOperationSettingOutputModel>();

            Mapper.CreateMap<Domain.SystemStuff.SyWapiSettings, WapiOperationSettingOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.CodeExtCompany,
                    option => option.MapFrom(source => source.ExternalCompanyObj.Code))
                .ForMember(dest => dest.DescripExtCompany,
                    option => option.MapFrom(source => source.ExternalCompanyObj.Description))
                .ForMember(dest => dest.CodeExtOperationMode,
                    option => option.MapFrom(source => source.ExternalOperationModeObj.Code))
                .ForMember(dest => dest.DescripExtOperationMode,
                    option => option.MapFrom(source => source.ExternalOperationModeObj.Description))
                .ForMember(dest => dest.DateLastExecution, option => option.MapFrom(source => source.DateLastExecution))
                .ForMember(dest => dest.CodeWapiServ, option => option.MapFrom(source => source.AllowedServiceObj.Code))
                .ForMember(dest => dest.DescripWapiServ,
                    option => option.MapFrom(source => source.AllowedServiceObj.Description))
                .ForMember(dest => dest.SecondCodeWapiServ,
                    option =>
                        option.MapFrom(
                            source =>
                                source.SecondAllowedServiceObj != null ? source.SecondAllowedServiceObj.Code : "NONE"))
                .ForMember(dest => dest.SecondDescWapiServ,
                    option =>
                        option.MapFrom(
                            source =>
                                source.SecondAllowedServiceObj != null
                                    ? source.SecondAllowedServiceObj.Description
                                    : "None"))
             
                //.ForMember(dest => dest.SecondCodeWapiServ, option => option.MapFrom(source => (source.SecondAllowedServiceObj != null) ? source.SecondAllowedServiceObj.Code : "NONE"))
                //.ForMember(dest => dest.SecondDescWapiServ, option => option.MapFrom(source => (source.SecondAllowedServiceObj != null) ? source.SecondAllowedServiceObj.Description : "None"))
                .ForMember(dest => dest.CodeOperation, option => option.MapFrom(source => source.CodeOperation))
                .ForMember(dest => dest.ConsumerKey, option => option.MapFrom(source => source.ConsumerKey))
                .ForMember(dest => dest.ExternalUrl, option => option.MapFrom(source => source.ExternalUrl))
                .ForMember(dest => dest.FlagOnDemandOperation, option => option.MapFrom(source => source.FlagOnDemandOperation))
                .ForMember(dest => dest.FlagRefreshConfig, option => option.MapFrom(source => source.FlagRefreshConfiguration))
                .ForMember(dest => dest.IsActiveWapiServ, option => option.MapFrom(source => source.AllowedServiceObj.IsActive))
                .ForMember(dest => dest.OperationSecInterval, option => option.MapFrom(source => source.OperationSecondTimeInterval))
                .ForMember(dest => dest.PollSecOnDemandOperation, option => option.MapFrom(source => source.PollSecondForOnDemandOperation))
                .ForMember(dest => dest.PrivateKey, option => option.MapFrom(source => source.PrivateKey))
                .ForMember(dest => dest.UrlWapiServ, option => option.MapFrom(source => source.AllowedServiceObj.Url))
                .ForMember(dest => dest.IsActiveOperation, option => option.MapFrom(source => source.IsActive));

        }
    }
}
