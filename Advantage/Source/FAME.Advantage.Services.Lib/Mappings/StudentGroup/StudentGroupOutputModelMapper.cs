﻿using AutoMapper;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Messages.StudentGroups;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.StudentDocuments
{
    using AutoMapper.Configuration;

    public class StudentGroupOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<StudentGroup, StudentGroupOutputModel>();
        }
    }
}
