﻿using System.Linq;
using System.Runtime.InteropServices;
using AutoMapper;
using FAME.Advantage.Messages.Employee;

using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;


namespace FAME.Advantage.Services.Lib.Mappings.Employee
{
    using AutoMapper.Configuration;

    public class EmployeeContactOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {

            Mapper.CreateMap<Domain.Employee.EmployeeContact, EmployeeContactOutputModel>();
        }
    }
}



