﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmployeeSearchOutputModelMapper.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the EmployeeSearchOutputModelMapper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.Employee
{
    using System;
    using System.Linq;
    using AutoMapper;
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.Employee;
    using FAME.Advantage.Domain.Lead;

    using Messages.Employee;
    using Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    /// The employee search output model mapper.
    /// </summary>
    public class EmployeeSearchOutputModelMapper : IMapperDefinition
    {
        /// <summary>
        /// The create definition.
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.Employee.Employee, EmployeeSearchOutputModel>()
                .ForMember(dest => dest.FullName, option => option.MapFrom(source => string.Format("{0}, {1}", source.LastName, source.FirstName)))
                .ForMember(dest => dest.Field1, option => option.MapFrom(source => string.Format("Address: {0}", source.Address ?? string.Empty)))
                .ForMember(dest => dest.Field2, option => option.MapFrom(source => string.Empty))
                .ForMember(dest => dest.Field3, option => option.MapFrom(source => string.Format("City: {0}", source.City ?? string.Empty)))
                .ForMember(dest => dest.Field4, option => option.MapFrom(source => string.Format("State: {0}  ", (source.State == null ? string.Empty : source.State.Code ?? string.Empty)) + (source.Zip == null ? string.Empty : string.Format(", {0}", source.Zip.Substring(0, 5))) )  )
                .ForMember(dest => dest.Field5, option => option.MapFrom(source => !source.EmpContacts.Any() ? "Work Phone: " : string.IsNullOrEmpty(source.EmpContacts[0].WorkPhone) ? "Work Phone: " : string.Format("Phone: ({0}) {1}-{2} ", source.EmpContacts[0].WorkPhone.Substring(0, 3), source.EmpContacts[0].WorkPhone.Substring(3, 3), source.EmpContacts[0].WorkPhone.Substring(6))))
                .ForMember(dest => dest.Image64, option => option.MapFrom<string>(source => this.GetImageSrc(source))); 
        }

        /// <summary>
        /// The get image <code>src</code> .
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// get the image in base 64 ready to be inserted in the image <code>src</code> tag
        /// </returns>
        private string GetImageSrc(Employee source)
        {
                var output = "data:image/jpeg;base64,";
                output +=
                    @"iVBORw0KGgoAAAANSUhEUgAAAEsAAABLCAIAAAC3LO29AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAiESURBVHhe3Vv3V1RHFLa32GJijJrkaIrR5KScREVBRFFEooKFqLGXKL1JU0BEDKIICIoiAgIqKiogAoKgossiUpe6LM0FFlg6WP6AfGZPzJ5F3PfmzS5Gzh5+ujPvfnP7nTsDO5+9HPB+/wHh+/0boGp4HT1vPsGOnhctnc/wX9UMqBBhe8+Lgor6a2l5Men5d7JKc8qe5gnrHuSJkvklmYJqUV1LZOLjlOyy9BxhWW0TiFUEVVUIxc0daU+E+4Pil5ifXGoRsMYxeI/XJVPvK5tcw1c7BG/3iHQ+Faf9p6+xc8iGA6EOATeDYx/eflQsFDdTh0oZYcezl61dz7KKa0Nv8dcfCJ1i4DRgtonS3xANs0nLHJaaB/heSq9qaG3rfk5RnjQRwqia2rtzy8VG9mc/WrJv0BxTpdjkCQbNNf1U3zEsgV9Y2dDY1k3LRGkiBFup2eU6Jn4jtSwHMhBdb/yD5piMWmAFYd5Iz6+XdrZ3UzBOOghx3tmlT12DE37Y6AkWB7KUnjxUrP1A23raKhc9i0C/6HTg5KixFBC2db24eb9gq/uF6YauQ+eZs9LMvoiBc7S2tbFTCDxwPyOE64t9UGjsfO4TPQcq2OQ3+Xad+9ELd/4Jm+RpCScZAh78+ya3sI+X2lOHhw1Halkh0lTWt3AJIZwQSjt6whP4X692UwU82Z4zjQ8l8UsQgZjrqoLAyRHC/MpqmgysT43VsVEdwknLHO38r8NLM0eoQPkGhAyVvrS2yfpEzJiF1gPnKI/pxEcweqH1ErOTdc0d5AgZ4lH4gKS168a9gikGzmzDOluocM5QVCQ6xAkAoZaW1DR6hiYhC2HLMVt6fGLyciekgdLOHjIxEmopigNkz2zZJaMfv9g2OiW3TkqoqIQI72SVwTzIOGa7Cqboe/lVRk5Nhkw2unG/4PsNh9nySkaPJM4jNFFUJ2XCWG8aQjuMTs35fMV+Mo7ZrkIev/vIxeKqRhn3bF0jIcKrd/O+NFJhoJc/BbjTBbt9npQSJqiECBN5xZq7fNhKg4x+2HwLfctTeeVitWppyuOyperyNMM1LTfsDxWI6tWKELmizh5fMpmwXTVC03L7ociiyga1Ikx4VDRv5zG2vJLRj9Cy3OUJTyNRK8LEzGKt3WqyQ8hwm3uEoFK9CNH/1DXzJ5MJ21XD51ugtYXWq1plmJZTsdLuDFteyegRLZA/5ZSp15fyCqu3HLzApePEHC1kuM7pXIGcL2UV9AnjYXVDa1g8f+Y6dzRzmfNKQInic4yOjXdECnoZatVSdDIFogbTo9Eo8FVaAQ+bbz5jnfvDgsrmDnrVE8OjamjpissQ/LrVC3kjgXAYLhm3yHat4zkuXVNCLZWdAnr47iG3pxo4M2SXgGzqb86eYclNdPs0DGUIstau52jjz/rdg4B1hktmrD0Ye78AJsicK+WdKOZ7oXdSLWlD6B+iQafVrQB7hKbFYlP/2sa29v7qCL8SY+dzhA3cNDGUCSsyuLE1DsEc72c42aFM2ufjM3/Z4kW96TZBd5/WLp/DoUnMdeqNlBQQltY02gfcnG7kyko+byeGf15lFxQSy+MJqvofIThIzxUa7w8ZNJda9Id/9o9OJ+6Ryh8KBRliu+LqRrNj0bSu1iDen/74K/w2n6P0ZMvpIIzLKFxuFUh27/tGddU19UdPvR8Q9pXyImcMup7x8+YjVDK46YYuFj5XM/Ir+wFhX5/ECEb502aA1Njpzd3foJK4lpaPMNj/CBVEiss276hUtKiJIwfKMSQPx6NScS/CMQy+Ph1OdqiAEEnco8KqHzd6Ersc2U0TGnnElURvsXNC2Hu74moJRp7Iqg3YMCoJx8DYCtIGvqoivvy+RVUSdDfIEA7WMJtu5JZbXocrGCoWSDNavGZIUNmwzCoQ3TECfzNmoY2eZYCklesADc3aovdJoyOms9cPnRUChEjfN7uFU7RAFjJk3vnJF9bN3uo9jGhuaKKew16vS5jvoKii1HIaGU9II3FDNMv4EFl7CkM5Ow9HYvzy3UWICMYrrPrMwHkwUQo+dpHNCtvTaP+8uwhxSZuSVTZa24Ys4g+db/7deo9coZhLz4JdPGRufrJ9MRcccTuLwMe8XjJOx+ZIWBISQIYJDRMOaUb8rOKabYciuCBEhTlRz/5Y5F2MI9HSVWoIRfVSn4t3J+szGnt+yynAhjEhgKuY83GZQrEUOT1HqBQQQlXKxdKTV+7N3e49mNIMEWZo0PvZeTgqJI5Xgn5eF/nkN1eEbd0v7ueJ3IITAA/X0VxUVGEtpqE+1LXDtiZHL5+OeZCWUy6qbyFwQuQIca6Y4onPEOCkv1h5gMx/Kj0R1FPIkDBKu9ElFM3vyKTHSZklPEE1EmCM8zGRLQlCVEm1je2ZRdWnYjJwxhjsVsooFQLUVnjeoLnr+Ca3cNezCfDbeNKRJxRXiKWYJOyrbcUOoewpD84v6PrDRXv9yCI7FbSyTSbo2i3cc8L5dOytR0U1kjbkQ5i1xUCxvH9ihxAzz5ifX2zqN0HXHmMuFHkl2wqmATYw8z5luZPGjmPWvjFRydl5FXUSuecayhHiBQvsLYFX5HImfpllwFerXbGjem5/mcOGW0JROknf8Zu1B2E4hvvOHAiKT+SXII/vEyGyikJRA95leZxP3HwwXHvPCbTARi2wVJFHYQ5GKSVsBzKYZuiibxUIBVZECO8EhU7ml/pH34Ob1jU7iVcUZBWtUlZUTTBc0wJTzP8hhNBwO383u9zvcjp6LZjMVdGdmaqBKe4vy4lQWRdVSs7F8gysT6vN+6sJKuDhXUGmoAYznKiy1fRVondfhLwBYXRKzmIz//GLbPs9vhFiePt5IbWdt+M4+lzvWgCgh3br0f+pq2R6BO+v6P59ycP0JNTpG+h+671H+DcL5YizGcw8pwAAAABJRU5ErkJggg==";
                return output;
        }
    }
}



