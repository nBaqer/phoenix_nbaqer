﻿using AutoMapper;
using FAME.Advantage.Domain.Catalogs;
using FAME.Advantage.Domain.Lead;
using FAME.Advantage.Domain.Student;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Messages.Common;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Common
{
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.Requirements;
    using FAME.Advantage.Domain.Requirements.Documents;

    /// <summary>
    /// Mapper for Drop Down items
    /// </summary>
    public class DropDownOutputMappers : IMapperDefinition
    {
        /// <summary>
        /// Mapper definitions for DropDownOutput
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.Campuses.Campus, DropDownOutputModel>();

            Mapper.CreateMap<ReqsCourse, DropDownOutputModel>();

            Mapper.CreateMap<ArClassSections, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.RequerimentCourse.Description + " [ " + source.RequerimentCourse.Code + " ," + source.ClassSection + " ]"));

            Mapper.CreateMap<Enrollment, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.ProgramVersion == null ? string.Empty : source.ProgramVersion.Description + " [ " + source.EnrollmentStatus.Description + " ]"));

            Mapper.CreateMap<Term, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.TermDescription));

            Mapper.CreateMap<ArProgramGroup, DropDownOutputModel>()
               .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
               .ForMember(dest => dest.Description, option => option.MapFrom(source => source.PrgGrpDescrip));

            Mapper.CreateMap<SystemStates, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));

            Mapper.CreateMap<Prefixes, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.PrefixDescrip));

            Mapper.CreateMap<Suffixes, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.SuffixDescrip));

            Mapper.CreateMap<DependencyType, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Descrip));

            Mapper.CreateMap<FamilyIncoming, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.FamilyIncomeDescrip));

            Mapper.CreateMap<Gender, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));

            Mapper.CreateMap<HousingType, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Descrip));

            Mapper.CreateMap<MaritalStatus, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.MaritalStatDescrip));

            Mapper.CreateMap<Transportation, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.TransportationDescrip));

            Mapper.CreateMap<Citizenships, DropDownOutputModel>()
                 .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                 .ForMember(dest => dest.Description, option => option.MapFrom(source => source.CitizenshipsDescrip));

            Mapper.CreateMap<UserDefStatusCode, DropDownOutputModel>()
             .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
             .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));

            Mapper.CreateMap<ArPrograms, DropDownOutputModel>()
             .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
             .ForMember(dest => dest.Description, option => option.MapFrom(source => source.ProgDescrip));

            Mapper.CreateMap<ProgramVersion, DropDownOutputModel>()
              .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
              .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));

            Mapper.CreateMap<ArAttendTypes, DropDownOutputModel>()
              .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
              .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Descrip));

            Mapper.CreateMap<SourceCategory, DropDownOutputModel>()
            .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
            .ForMember(dest => dest.Description, option => option.MapFrom(source => source.SourceCatagoryDescrip));

            Mapper.CreateMap<SourceType, DropDownOutputModel>()
            .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
            .ForMember(dest => dest.Description, option => option.MapFrom(source => source.SourceTypeDescrip));

            Mapper.CreateMap<SourceAdvertisement, DropDownOutputModel>()
             .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
            .ForMember(dest => dest.Description, option => option.MapFrom(source => source.SourceAdvDescrip));

            Mapper.CreateMap<SyPhoneType, DropDownOutputModel>()
            .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
            .ForMember(dest => dest.Description, option => option.MapFrom(source => source.PhoneTypeDescrip));

            Mapper.CreateMap<EmailType, DropDownOutputModel>()
            .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
            .ForMember(dest => dest.Description, option => option.MapFrom(source => source.EmailTypeDescrip));

            Mapper.CreateMap<ProgramSchedule, DropDownOutputModel>()
            .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
            .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Descrip));

            Mapper.CreateMap<Term, DropDownOutputModel>()
                 .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
            .ForMember(dest => dest.Description, option => option.MapFrom(source => string.Format("[{0}] {1}", source.StartDate.ToShortDateString(), source.TermDescription)));
          
            Mapper.CreateMap<AdmissionRepByCampusView, DropDownOutputModel>()
            .ForMember(dest => dest.ID, option => option.MapFrom(source => source.UserObj.ID))
                 .ForMember(dest => dest.Description, option => option.MapFrom(source => source.FullName));

            Mapper.CreateMap<Country, DropDownOutputModel>()
            .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
            .ForMember(dest => dest.Description, option => option.MapFrom(source => source.CountryDescrip));

            Mapper.CreateMap<SystemStates, DropDownOutputModel>()
            .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
            .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));

            Mapper.CreateMap<SyStatuses, DropDownOutputModel>()
            .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
            .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Status));

            Mapper.CreateMap<County, DropDownOutputModel>()
            .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
            .ForMember(dest => dest.Description, option => option.MapFrom(source => source.CountyDescrip));

            Mapper.CreateMap<ContactType, DropDownOutputModel>()
            .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
            .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));

            Mapper.CreateMap<Suffix, DropDownOutputModel>()
            .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
            .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));

            Mapper.CreateMap<Prefix, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));

            Mapper.CreateMap<Relationship, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));

            Mapper.CreateMap<StatusChangeDeleteReasons, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));

            Mapper.CreateMap<DocumentStatus, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));

            Mapper.CreateMap<Domain.Users.User, DropDownOutputModel>()
                 .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                 .ForMember(dest => dest.Description, option => option.MapFrom(source => source.FullName));

            Mapper.CreateMap<DropReason, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));

            Mapper.CreateMap<LoaReasons, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));

            Mapper.CreateMap<StatusChangeDeleteReasons, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));
            Mapper.CreateMap<EducationLevel, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Description));
            Mapper.CreateMap<DegCertSeeking, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Descrip));
            Mapper.CreateMap<ProgramVersionType, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.Descrip));
        }
    }
}
