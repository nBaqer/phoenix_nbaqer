﻿using System;
using AutoMapper;
using FAME.Advantage.Messages.Common;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Common
{
    /// <summary>
    /// Mapping EnrollmentId => ID, Enrollment.ProgramVersion.Description => Description.
    /// </summary>
    public class DropDownEnrollmentOutputMapper: IMapperDefinition
    {
        public void CreateDefinition()
        {
            Mapper.CreateMap<Domain.Campuses.Campus, DropDownOutputModel>();
            
            Mapper.CreateMap<Domain.Student.Enrollments.Enrollment, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.ProgramVersion == null ? string.Empty : source.ProgramVersion.Description + " (" + source.EnrollmentStatus.Description + ")"));

            Mapper.CreateMap<Domain.Student.Enrollments.Term, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.TermDescription));

        }
    }
}
