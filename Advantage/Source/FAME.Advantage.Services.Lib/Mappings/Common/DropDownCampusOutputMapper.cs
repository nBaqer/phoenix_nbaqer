﻿using AutoMapper;
using FAME.Advantage.Messages.Common;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Common
{
    public class DropDownCampusOutputMapper : IMapperDefinition
    {
        /// <summary>
        /// Define the default mapping for this entity
        /// </summary>
        public void CreateDefinition()
        {
            Mapper.CreateMap<Domain.Campuses.Campus, DropDownOutputModel>();
        }
    }
}
