﻿using AutoMapper;
using FAME.Advantage.Domain.Lead;
using FAME.Advantage.Messages.Common;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Common
{
    using AutoMapper.Configuration;

    /// <summary>
    /// Vendor pay for mapper
    /// </summary>
    public class DropDownIntegerOutputMapper : IMapperDefinition
    {
        /// <summary>
        /// Create map definition
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<AdVendorPayFor, DropDownIntegerOutputModel>();
          
        }
    }
}
