﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadAddressOutpuModelMapper.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   The lead address output model mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using AutoMapper;
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Messages.Lead;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    /// The lead address output model mapper.
    /// </summary>
    public class LeadAddressOutputModelMapper : IMapperDefinition
    {
        /// <summary>
        /// The create definition.
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<LeadAddress, LeadAddressOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID.ToString()))
                .ForMember(dest => dest.Address1, option => option.MapFrom(source => source.Address1))
                .ForMember(dest => dest.Address2, option => option.MapFrom(source => source.Address2))
                .ForMember(dest => dest.AddressApto, option => option.MapFrom(source => source.AddressApto))
                .ForMember(dest => dest.AddressType, option => option.MapFrom(source => source.AddressType.AddessDescrip))
                .ForMember(dest => dest.AddressTypeId, option => option.MapFrom(source => source.AddressType.ID.ToString()))
                .ForMember(dest => dest.City, option => option.MapFrom(source => source.City))
                .ForMember(dest => dest.IsMailingAddress, option => option.MapFrom(source => source.IsMailingAddress ? "Yes" : "No"))
                .ForMember(dest => dest.Country, option => option.MapFrom(source => source.IsInternational ? source.CountryInternational : source.CountryObj.CountryDescrip))
                .ForMember(dest => dest.CountryId, option => option.MapFrom(source => source.CountryObj.ID.ToString()))
                .ForMember(dest => dest.IsShowOnLeadPage, option => option.MapFrom(source => source.IsShowOnLeadPage ? "Yes" : "No"))
                .ForMember(dest => dest.State, option => option.MapFrom(source => source.IsInternational ? source.StateInternational : source.StateObj.Code))
                .ForMember(dest => dest.StateId, option => option.MapFrom(source => source.StateObj.ID.ToString()))
                .ForMember(dest => dest.CountyId, option => option.MapFrom(source => source.CountyObj.ID.ToString()))
                .ForMember(dest => dest.County, option => option.MapFrom(source => source.IsInternational ? source.CountyInternational : source.CountyObj.CountyDescrip.ToString()))
                .ForMember(dest => dest.Status, option => option.MapFrom(source => source.StatusObj.Status))
                .ForMember(dest => dest.IsInternational, option => option.MapFrom(source => source.IsInternational))
                .ForMember(dest => dest.StatusId, option => option.MapFrom(source => source.StatusObj.ID.ToString()));
        }
    }
}
