﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadPriorWorkOutputModelMapper.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the LeadPriorWorkOutpuModelMapper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using System.Linq;

    using AutoMapper;
    using AutoMapper.Configuration;

    using Domain.Lead;

    using FAME.Advantage.Messages.Common;

    using Infrastructure.AutoMapper;
    using Messages.Lead.PriorWork;

    /// <summary>
    ///  The lead prior work output model mapper.
    /// </summary>
    public class LeadPriorWorkOutputModelMapper : IMapperDefinition
    {
        #region Implementation of IMapperDefinition

        /// <summary>
        /// The create definition.
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            // Mapper for PriorWorkAddressOutputModel
            Mapper.CreateMap<PlJobStatus, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.JobStatusDescrip));

            Mapper.CreateMap<AdTitles, DropDownOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.TitleDescrip));

            Mapper.CreateMap<PriorWorkAddress, PriorWorkAddressOutputModel>()
                .ForMember(dest => dest.Address, option => option.Ignore())
                .ForMember(dest => dest.Address2, option => option.MapFrom(source => source.Address2))
                .ForMember(dest => dest.Address1, option => option.MapFrom(source => source.Address1))
                .ForMember(dest => dest.AddressApto, option => option.MapFrom(source => source.Apartment))
                .ForMember(dest => dest.AddressId, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.City, option => option.MapFrom(source => source.City))
                .ForMember(dest => dest.IsInternational, option => option.MapFrom(source => source.IsInternational))
                .ForMember(
                    dest => dest.Country,
                    option =>
                        option.MapFrom(
                            source =>
                                source.CountryObj == null
                                    ? null
                                    : new DropDownOutputModel()
                                          {
                                              ID = source.CountryObj.ID.ToString(),
                                              Description = source.CountryObj.CountryDescrip
                                          }))
                .ForMember(
                    dest => dest.StateInternational,
                    option => option.MapFrom(source => source.StateInternational))
                .ForMember(dest => dest.ModDate, option => option.Ignore())
                .ForMember(dest => dest.Moduser, option => option.MapFrom(source => source.ModUser))
                .ForMember(
                    dest => dest.PriorWorkId,
                    option => option.MapFrom(source => source.StEmploymentObj.ID.ToString()))
                .ForMember(
                    dest => dest.State,
                    option =>
                        option.MapFrom(
                            source =>
                                source.IsInternational
                                    ? null
                                    : source.StateObj == null
                                        ? null
                                        : new DropDownOutputModel()
                                              {
                                                  ID = source.StateObj.ID.ToString(),
                                                  Description = source.StateObj.Description
                                              }))
                .ForMember(dest => dest.ZipCode, option => option.MapFrom(source => source.ZipCode));

            Mapper.CreateMap<PriorWorkContact, PriorWorkContactOutputModel>()
                .ForMember(dest => dest.ContactId, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Email, option => option.MapFrom(source => source.Email))
                .ForMember(dest => dest.IsActive, option => option.MapFrom(source => source.IsActive))
                .ForMember(
                    dest => dest.Name,
                    option =>
                        option.MapFrom(
                            source => string.Format("{0} {1} {2}", source.FirstName, source.MiddleName, source.LastName)))
                .ForMember(dest => dest.LastName, option => option.MapFrom(source => source.LastName))
                .ForMember(dest => dest.FirstName, option => option.MapFrom(source => source.FirstName))
                .ForMember(dest => dest.MiddleName, option => option.MapFrom(source => source.MiddleName))
                .ForMember(dest => dest.Phone, option => option.MapFrom(source => source.Phone))
                .ForMember(dest => dest.PriorWorkId, option => option.MapFrom(source => source.StEmploymentObj.ID))
                .ForMember(dest => dest.Title, option => option.MapFrom(source => source.JobTitle))
                .ForMember(
                    dest => dest.Prefix,
                    option =>
                        option.MapFrom(
                            source =>
                                source.PrefixObj != null
                                    ? new DropDownOutputModel()
                                          {
                                              ID = source.PrefixObj.ID.ToString(),
                                              Description = source.PrefixObj.PrefixDescrip
                                          }
                                    : null));

            Mapper.CreateMap<AdLeadEmployment, PriorWorkItemOutputModel>()
                .ForMember(dest => dest.Id, option => option.MapFrom(source => source.ID.ToString()))
                .ForMember(
                    dest => dest.AddressOutputModel,
                    option =>
                        option.MapFrom(
                            source =>
                                source.PriorWorkAddressesList == null || source.PriorWorkAddressesList.Count == 0
                                    ? null
                                    : source.PriorWorkAddressesList.FirstOrDefault()))
                .ForMember(dest => dest.Comments, option => option.MapFrom(source => source.Comments))
                .ForMember(dest => dest.Contacts, option => option.MapFrom(source => source.PriorWorkContactsList))
                .ForMember(dest => dest.Employer, option => option.MapFrom(source => source.EmployerName))
                .ForMember(dest => dest.EndDate, option => option.MapFrom(source => source.EndDate))
                .ForMember(
                    dest => dest.JobResponsabilities,
                    option => option.MapFrom(source => source.JobResponsibilities))
                .ForMember(dest => dest.JobStatus, option => option.MapFrom(source => source.JobStatusObj))
                .ForMember(dest => dest.JobTitle, option => option.MapFrom(source => source.EmployerJobTitle))
                .ForMember(dest => dest.SchoolJobTitle, option => option.MapFrom(source => source.JobTitleObj))
                .ForMember(dest => dest.StartDate, option => option.MapFrom(source => source.StartDate));
        }

        #endregion
    }
}
