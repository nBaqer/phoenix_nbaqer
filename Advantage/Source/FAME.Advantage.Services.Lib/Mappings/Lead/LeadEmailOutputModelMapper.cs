﻿using AutoMapper;
using FAME.Advantage.Domain.Lead;
using FAME.Advantage.Messages.Lead;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using AutoMapper.Configuration;

    public class LeadEmailOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            //Mapper.CreateMap<LeadEmail, LeadEmailOutputModel>();
            Mapper.CreateMap<Domain.Lead.LeadEmail, LeadEmailOutputModel>()
                .ForMember(dest => dest.IsPreferred, option => option.MapFrom(source => source.IsPreferred))
                .ForMember(dest => dest.IsShowOnLeadPage, option => option.MapFrom(source => source.IsShowOnLeadPage ? "Yes" : source.IsPreferred ? "Yes" : "No"))
                .ForMember(dest => dest.EmailType, option => option.MapFrom(source => source.EmailTypeObj.EmailTypeDescrip))
                .ForMember(dest => dest.EmailTypeId, option => option.MapFrom(source => source.EmailTypeObj.ID.ToString()))
                .ForMember(dest => dest.Email, option => option.MapFrom(source => source.Email.Length <= 49 ? source.Email : source.Email.Substring(0, 49) + "..."))
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID.ToString()))
                .ForMember(dest => dest.Email, option => option.MapFrom(source => source.Email))
                .ForMember(dest => dest.Status, option => option.MapFrom(source => source.Status.Status))
                .ForMember(dest => dest.StatusId, option => option.MapFrom(source => source.Status.ID.ToString()));

        }
    }
}
