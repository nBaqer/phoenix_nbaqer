﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadOutputModelMapper.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Lead output model mapping
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using System.Linq;

    using AutoMapper;
    using AutoMapper.Configuration;

    using FAME.Advantage.Messages.Lead;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    /// Lead output model mapping
    /// </summary>
    public class LeadOutputModelMapper : IMapperDefinition
    {
        /// <summary>
        /// Lead output model mapping
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.Lead.Lead, LeadOutputModel>()
                .ForMember(dest => dest.FullName, option => option.MapFrom(source => string.Format("{0}, {1}", source.LastName, source.FirstName)))
                //.ForMember(dest => dest.ProgramOfInterest, option => option.MapFrom(source => source.ProgramOfInterest))
                .ForMember(dest => dest.ProgramVersion, option => option.MapFrom(source => source.ProgramVersion.Description))
                .ForMember(dest => dest.AreaOfInterest, option => option.MapFrom(source => source.ProgramGroupObj.PrgGrpDescrip))
                .ForMember(dest => dest.Campus, option => option.MapFrom(source => source.Campus.Description))
                .ForMember(dest => dest.City, option => option.MapFrom(source => source.AddressList.Where(x => x.StatusObj.StatusCode == "A").OrderByDescending(x => x.IsMailingAddress).ThenByDescending(x => x.IsShowOnLeadPage).FirstOrDefault(x => x.IsMailingAddress || x.IsShowOnLeadPage || (!x.IsMailingAddress && !x.IsShowOnLeadPage)).City))
                .ForMember(dest => dest.State, option => option.MapFrom(source => source.StateObj.Code))
                .ForMember(dest => dest.Zip, option => option.MapFrom(source => source.AddressList.Where(x => x.StatusObj.StatusCode == "A").OrderByDescending(x => x.IsMailingAddress).ThenByDescending(x => x.IsShowOnLeadPage).FirstOrDefault(x => x.IsMailingAddress || x.IsShowOnLeadPage || (!x.IsMailingAddress && !x.IsShowOnLeadPage)).ZipCode))
                .ForMember(
                    dest => dest.FirstMiddleLastName,
                    option =>
                    option.MapFrom(
                        source =>
                        source.FirstName
                        + (!string.IsNullOrEmpty(source.MiddleName)
                               ? " " + source.MiddleName.Substring(0, 1) + ". "
                               : " ") + source.LastName));
        }
    }
}
