﻿using AutoMapper;
using FAME.Advantage.Messages.Lead;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    /// <summary>
    /// Mapper for infoBar 
    /// </summary>
    //public class LeadInfoBarOutputModelMapper : IMapperDefinition
    //{
    //    #region Implementation of IMapperDefinition

    //    /// <summary>
    //    /// MAP Definition Lead -> LeadInfoBarOutputModel
    //    /// </summary>
    //    public void CreateDefinition()
    //    {

    //        Mapper.CreateMap<Domain.Lead.Lead, LeadInfoBarOutputModel>()
    //            .ForMember(dest => dest.LeadFullName, option => option.MapFrom(source => string.Format("{0}, {1}", source.LastName, source.FirstName)))
    //            .ForMember(dest => dest.ExpectedStart, option => option.MapFrom(source => (source.ExpectedStart == null) ? string.Empty : source.ExpectedStart.ToString()))
    //            .ForMember(dest => dest.DateAssigned, option => option.MapFrom(source => (source.AssignedDate == null) ? string.Empty : source.AssignedDate.ToString()))
    //            .ForMember(dest => dest.LeadGuid, option => option.MapFrom(source => source.ID))
    //            .ForMember(dest => dest.CurrentStatus, option => option.MapFrom(source =>(source.LeadStatus == null) ? string.Empty: source.LeadStatus.Description))
    //            .ForMember(dest => dest.LeadImagePath, option => option.MapFrom(source => source.ImageUrl))
    //            .ForMember(dest => dest.ContentType, option => option.MapFrom(source => source.EncodeImage))
    //            .ForMember(dest => dest.ProgramVersionName, option => option.MapFrom(source => (source.PrgVersionObj == null) ? string.Empty : source.PrgVersionObj.Description))
    //            .ForMember(dest => dest.AdmissionRep, option => option.MapFrom(source => (source.AdmissionRepUserObj == null) ? string.Empty : source.AdmissionRepUserObj.FullName))
    //            .ForMember(dest => dest.CampusName, option => option.MapFrom(source => (source.Campus == null) ? string.Empty : source.Campus.Description));
               
    //   }

    //    #endregion
    //}

}
