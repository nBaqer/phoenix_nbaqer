﻿using System.Text.RegularExpressions;
using AutoMapper;
using FAME.Advantage.Domain.Lead;
using FAME.Advantage.Messages.Lead;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using AutoMapper.Configuration;

    public class LeadPhoneOutputModelMapper : IMapperDefinition
    {

        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.Lead.LeadPhone, LeadPhonesOutputModel>()
                // .ForMember(dest => dest.Best, option => option.MapFrom(source => source.IsBest ? "Yes" :"No"))
                .ForMember(
                    dest => dest.Best,
                    option =>
                    option.MapFrom(
                        source =>
                        source.Position == (int)LeadPhone.ELeadPhonePosition.IsBest
                            ? "Yes"
                            : source.IsBest ? "Yes" : "No"))
                .ForMember(
                    dest => dest.ShowOnLeadPage,
                    option =>
                    option.MapFrom(
                        source =>
                        source.IsShowOnLeadPage ? "Yes" : source.Position == (int)LeadPhone.ELeadPhonePosition.ShowOnLeadPage
                        || source.Position == (int)LeadPhone.ELeadPhonePosition.ShowOnLeadPageThird ? "Yes" : source.Position == (int)LeadPhone.ELeadPhonePosition.IsBest
                            ? "Yes"
                            : source.IsBest ? "Yes" : "No"))
                .ForMember(dest => dest.Extension, option => option.MapFrom(source => source.Extension))
                .ForMember(dest => dest.IsForeignPhone, option => option.MapFrom(source => source.IsForeignPhone))
                .ForMember(dest => dest.PhoneType, option => option.MapFrom(source => source.SyPhoneTypeObj.PhoneTypeDescrip))
                .ForMember(dest => dest.PhoneTypeId, option => option.MapFrom(source => source.SyPhoneTypeObj.ID.ToString()))
                .ForMember(dest => dest.Status, option => option.MapFrom(source => source.Status.Status))
                .ForMember(dest => dest.StatusId, option => option.MapFrom(source => source.Status.ID.ToString()))
                .ForMember(dest => dest.Phone,
                    option =>
                        option.MapFrom(
                            source =>
                                source.IsForeignPhone
                                    ? source.Phone
                                    : Regex.Replace(source.Phone, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3")));
            //.ForMember(dest => dest.ShowOnLeadPage, option => option.MapFrom(source => source.IsShowOnLeadPage ? "Yes" : "No"))


        }
    }
}
;