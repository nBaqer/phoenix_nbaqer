﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadRequirementGroupOutputModelMapper.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Mappings.Lead.LeadRequirementGroupOutputModelMapper
// </copyright>
// <summary>
//   The lead requirement group output model mapper.
//   Mapper between Domain Entity RequirementGroup and Message LeadReqEntityOutputModel
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Security.Cryptography;

    using AutoMapper;
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.Requirements;
    using FAME.Advantage.Messages.Lead;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    /// The lead requirement group output model mapper.
    /// Mapper between Domain Entity RequirementGroup and Message LeadREQEntityOutputModel
    /// </summary>
    public class LeadRequirementGroupOutputModelMapper : IMapperDefinition
    {
        /// <summary>
        /// The create definition.
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.Requirements.RequirementGroup, LeadReqEntityOutputModel>()
                .ConvertUsing<TypeConverter>();
        }

        /// <summary>
        /// The type converter.
        /// </summary>
        /// <summary>
        /// The type converter.
        /// </summary>
        private sealed class TypeConverter : ITypeConverter<Domain.Requirements.RequirementGroup, LeadReqEntityOutputModel>
        {
            /// <summary>
            /// The convert mapping
            /// </summary>
            /// <param name="source">The source for the mapping</param>
            /// <param name="destination">The destination for the mapping</param>
            /// <param name="context">The context for the converstion of the mapping</param>
            /// <returns>A converted object from the source type to the type <code>LeadReqEntityOutputModel</code></returns>
            public LeadReqEntityOutputModel Convert(RequirementGroup source, LeadReqEntityOutputModel destination, ResolutionContext context)
            {
                if (source == null)
                {
                    throw new ApplicationException("source is in invalid state");
                }
                if (destination == null)
                {
                    throw new ApplicationException("dest is in invalid state");
                }

                destination.Id = source.ID;
                destination.Code = source.Code;
                destination.Description = source.Description;
                destination.DisplayType = "Group";

                destination.ApprovalDate = string.Empty;
                destination.DateReceived = string.Empty;

                destination.IsMandatory = (bool)(source.IsMandatory ?? false);

                var requirements = source.GetRequirementsForEnrollments();

                var totalRequirements = requirements.Count();
                var reqRequired = source.GetMinimumRequirementRequired();

                var requirementsConverted = requirements.Select(r =>
                {
                    var outputModel = new LeadReqEntityOutputModel();
                    r.SetLeadGuid(source.LeadGuid);
                    return Mapper.Map(r, outputModel);
                }).ToList();

                destination.IsApproved = (requirementsConverted.Count(x => x.IsApproved) >= reqRequired) && (requirementsConverted.Count(x => x.IsApproved && x.IsMandatory) == requirementsConverted.Count(x => x.IsMandatory));

                destination.Description = source.Description + " (" + reqRequired.ToString(CultureInfo.InvariantCulture) + " of " +
                                   totalRequirements.ToString(CultureInfo.InvariantCulture) + ") ";

                return destination;
            }
        }
    }
}
