﻿using System.Text.RegularExpressions;
using AutoMapper;
using FAME.Advantage.Messages.Lead;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using AutoMapper.Configuration;

    /// <summary>
    ///  Mapper LeadOtherContactPhoneModel in Message with the Domain class LeadOtherContactPhone 
    /// </summary>
    public class LeadOtherContactPhoneModelMapper : IMapperDefinition
    {
        /// <summary>
        /// Mapper Definition
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.Lead.LeadOtherContactPhone, LeadOtherContactPhoneModel>()
                .ForMember(dest => dest.OtherContactsPhoneId, option => option.MapFrom(source => source.ID.ToString()))
                .ForMember(dest => dest.OtherContactId, option => option.MapFrom(source => source.LeadOtherContact.ID.ToString()))
                .ForMember(dest => dest.LeadId, option => option.MapFrom(source => source.Lead.ID.ToString()))
                .ForMember(dest => dest.Extension, option => option.MapFrom(source => source.Extension))
                .ForMember(dest => dest.IsForeignPhone, option => option.MapFrom(source => source.IsForeignPhone))
                .ForMember(dest => dest.PhoneType, option => option.MapFrom(source => source.PhoneType.PhoneTypeDescrip))
                .ForMember(dest => dest.PhoneTypeId, option => option.MapFrom(source => source.PhoneType.ID.ToString()))
                .ForMember(dest => dest.Status, option => option.MapFrom(source => source.Status.Status))
                .ForMember(dest => dest.StatusId, option => option.MapFrom(source => source.Status.ID.ToString()))
                .ForMember(dest => dest.Phone,
                    option =>
                        option.MapFrom(
                            source =>
                                source.IsForeignPhone
                                    ? source.Phone
                                    : Regex.Replace(source.Phone, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3")));
        }
    }
}
