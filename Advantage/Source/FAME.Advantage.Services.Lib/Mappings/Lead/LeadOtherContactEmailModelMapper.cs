﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FAME.Advantage.Messages.Lead;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using AutoMapper.Configuration;

    /// <summary>
    ///  Mapper for LeadOtherContactEmailModel in Message with the Domain class LeadOtherContactEmail 
    /// </summary>
    public class LeadOtherContactEmailModelMapper : IMapperDefinition
    {
        /// <summary>
        /// Mapper Definition
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
               Mapper.CreateMap<Domain.Lead.LeadOtherContactEmail, LeadOtherContactEmailModel>()
                .ForMember(dest => dest.EmailType, option => option.MapFrom(source => source.EmailType.EmailTypeDescrip))
                .ForMember(dest => dest.EmailTypeId, option => option.MapFrom(source => source.EmailType.ID.ToString()))
                .ForMember(dest => dest.Email, option => option.MapFrom(source => source.Email.Length <= 49 ? source.Email : source.Email.Substring(0, 49) + "..."))
                .ForMember(dest => dest.OtherContactsEmailId, option => option.MapFrom(source => source.ID.ToString()))
                .ForMember(dest => dest.OtherContactId, option => option.MapFrom(source => source.OtherContact.ID.ToString()))
                .ForMember(dest => dest.LeadId, option => option.MapFrom(source => source.Lead.ID.ToString()))
                .ForMember(dest => dest.Email, option => option.MapFrom(source => source.Email))
                .ForMember(dest => dest.Status, option => option.MapFrom(source => source.Status.Status))
                .ForMember(dest => dest.StatusId, option => option.MapFrom(source => source.Status.ID.ToString()));
        }
    }
}
