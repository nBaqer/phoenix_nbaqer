﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadQuickOutputModelMapper.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the LeadQuickOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using System.Web.UI.WebControls;

    using AutoMapper;
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Messages.Common;
    using FAME.Advantage.Messages.Lead.Quick;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    /// The lead quick output model.
    /// </summary>
    public class LeadQuickOutputModelMapper : IMapperDefinition
    {
        /// <summary>
        /// Lead output model mapping
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<LeadQuick, LeadQuickFieldsOutputModel>()
                .ForMember(dest => dest.FldName, option => option.MapFrom(source => source.FldName.ToString()))
                .ForMember(dest => dest.TblFldsId, option => option.MapFrom(source => source.TblFldsId))
                .ForMember(dest => dest.Caption, option => option.MapFrom(source => source.Caption.ToString()))
                .ForMember(dest => dest.ControlName, option => option.MapFrom(source => source.ControlName.ToString()))
                .ForMember(dest => dest.DdlId, option => option.MapFrom(source => source.DdlId))
                .ForMember(dest => dest.FldLen, option => option.MapFrom(source => source.FldLen.ToString()))
                .ForMember(dest => dest.FldId, option => option.MapFrom(source => source.FldId))
                .ForMember(dest => dest.FldType, option => option.MapFrom(source => source.FldType.ToString()))
                .ForMember(dest => dest.Required, option => option.MapFrom(source => source.Required))
                .ForMember(dest => dest.SectionId, option => option.MapFrom(source => source.SectionId))
                .ForMember(dest => dest.FldTypeId, option => option.MapFrom(source => source.FldTypeId))
                .ForMember(dest => dest.CtrlIdName, option => option.MapFrom(source => source.CtrlIdName))
                .ForMember(dest => dest.PropName, option => option.MapFrom(source => source.PropName))
                .ForMember(dest => dest.ParentCtrlId, option => option.MapFrom(source => source.ParentCtrlId));

            //Mapper.CreateMap<LeadQuick, DropDownOutputModel>()
            //   .ForMember(dest => dest.ID, option => option.Ignore())
            //   .ForMember(dest => dest.Description, option => option.Ignore());
        }
    }
}
