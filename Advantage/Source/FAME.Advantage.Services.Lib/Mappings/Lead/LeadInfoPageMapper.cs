﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadInfoPageMapper.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Mapper for class Lead to LeadInfoPageDemographicOutputModel
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using System.Linq;

    using AutoMapper;
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Domain.Users;
    using FAME.Advantage.Messages.Enumerations;
    using FAME.Advantage.Messages.Lead;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    /// Mapper for class Lead to LeadInfoPageDemographicOutputModel
    /// </summary>
    public class LeadInfoPageMapper : IMapperDefinition
    {
        /// <summary>
        /// Mapper Definition
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<LeadLastNameHistory, LeadLastNameHistoryOutputModel>()
                .ForMember(dest => dest.LastName, option => option.MapFrom(source => source.LastName))
                .ForMember(dest => dest.Id, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.LeadId, option => option.MapFrom(source => source.LeadObj.ID));

            Mapper.CreateMap<LeadPhone, LeadPhonesOutputModel>()
                .ForMember(dest => dest.PhoneTypeId, option => option.MapFrom(source => source.SyPhoneTypeObj.ID));

            Mapper.CreateMap<LeadEmail, LeadEmailOutputModel>()
                .ForMember(dest => dest.EmailType, option => option.MapFrom(source => source.EmailTypeObj.ID));
            ////Mapper.CreateMap<LeadAddress, LeadAddressOutputModel>()
            ////    .ForMember(dest => dest.Address1, option => option.MapFrom(source => source.Address1))
            ////    .ForMember(dest => dest.Address2, option => option.MapFrom(source => source.Address2))
            ////    .ForMember(
            ////        dest => dest.AddressType,
            ////        option => option.MapFrom(source => source.AddressType.AddessDescrip))
            ////    .ForMember(dest => dest.AddressTypeId, option => option.MapFrom(source => source.AddressType.ID))
            ////    .ForMember(dest => dest.City, option => option.MapFrom(source => source.City))
            ////    .ForMember(dest => dest.Country, option => option.MapFrom(source => source.Country.CountryDescrip))
            ////    .ForMember(dest => dest.CountryId, option => option.MapFrom(source => source.Country.ID))
            ////    .ForMember(dest => dest.ModDate, option => option.MapFrom(source => source.ModDate))
            ////    .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
            ////    .ForMember(dest => dest.County, option => option.MapFrom(source => source.County.CountyDescrip))
            ////    .ForMember(dest => dest.CountyId, option => option.MapFrom(source => source.County.ID))
            ////    .ForMember(dest => dest.IsInternational, option => option.MapFrom(source => source.IsInternational))
            ////    .ForMember(dest => dest.IsMailingAddress, option => option.MapFrom(source => source.IsMailingAddress))
            ////    .ForMember(dest => dest.IsShowOnLeadPage, option => option.MapFrom(source => source.IsShowOnLeadPage))
            ////    .ForMember(dest => dest.Moduser, option => option.MapFrom(source => source.ModUser))
            ////    .ForMember(dest => dest.State, option => option.MapFrom(source => source.State.Description))
            ////    .ForMember(dest => dest.StateId, option => option.MapFrom(source => source.State.ID))
            ////    .ForMember(dest => dest.StateInternational, option => option.MapFrom(source => source.StateInternational))
            ////    .ForMember(dest => dest.Status, option => option.MapFrom(source => source.Status.Status))
            ////    .ForMember(dest => dest.StatusId, option => option.MapFrom(source => source.Status.ID));

            Mapper.CreateMap<AdVehicles, VehicleOutputModel>()
                .ForMember(dest => dest.Id, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Color, option => option.MapFrom(source => source.Color))
                .ForMember(dest => dest.Make, option => option.MapFrom(source => source.Make))
                .ForMember(dest => dest.LeadId, option => option.MapFrom(source => source.LeadObj.ID.ToString()))
                .ForMember(dest => dest.Permit, option => option.MapFrom(source => source.Permit))
                .ForMember(dest => dest.Model, option => option.MapFrom(source => source.Model))
                .ForMember(dest => dest.Plate, option => option.MapFrom(source => source.Plate))
                .ForMember(dest => dest.Position, option => option.MapFrom(source => source.Position))
                .ForMember(dest => dest.ModDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(dest => dest.ModUser, option => option.MapFrom(source => source.ModUser));
            Mapper.CreateMap<Lead, LeadInfoPageOutputModel>()
                .ForMember(dest => dest.LeadId, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.FirstName, option => option.MapFrom(source => source.FirstName))
                .ForMember(dest => dest.LastName, option => option.MapFrom(source => source.LastName))
                .ForMember(dest => dest.MiddleName, option => option.MapFrom(source => source.MiddleName))
                .ForMember(dest => dest.Prefix, option => option.MapFrom(source => source.PrefixObj.ID))
                .ForMember(dest => dest.Suffix, option => option.MapFrom(source => source.SuffixObj.ID))
                .ForMember(dest => dest.Age, option => option.MapFrom(source => source.Age))
                .ForMember(dest => dest.AlienNumber, option => option.MapFrom(source => source.AlienNumber))
                .ForMember(dest => dest.Citizenship, option => option.MapFrom(source => source.CitizenshipsObj.ID))
                .ForMember(dest => dest.Dependants, option => option.MapFrom(source => source.Dependants))
                .ForMember(dest => dest.Transportation, option => option.MapFrom(source => source.TransportationObj.ID))
                .ForMember(dest => dest.Dependency, option => option.MapFrom(source => source.DependencyTypeObj.ID))
                .ForMember(dest => dest.Dob, option => option.MapFrom(source => source.BirthDate))
                .ForMember(dest => dest.MaritalStatus, option => option.MapFrom(source => source.MaritalStatusObj.ID))
                .ForMember(dest => dest.FamilyIncoming, option => option.MapFrom(source => source.FamilyIncomingObj.ID))
                .ForMember(dest => dest.HousingType, option => option.MapFrom(source => source.HousingTypeObj.ID))
                .ForMember(dest => dest.DrvLicStateCode, option => option.MapFrom(source => source.DriverLicenseStateCodeObj.ID))
                .ForMember(dest => dest.Gender, option => option.MapFrom(source => source.GenderObj.ID))
                .ForMember(dest => dest.NickName, option => option.MapFrom(source => source.NickName))
                .ForMember(dest => dest.DistanceToSchool, option => option.MapFrom(source => source.DistanceToSchool))
                .ForMember(dest => dest.IsDisabled, option => option.MapFrom(source => source.IsDisabled))
                .ForMember(dest => dest.LeadLastNameHistoryList, option => option.MapFrom(source => source.LeadLastNameList.OrderByDescending(x => x.ID)))
                .ForMember(dest => dest.RaceId, option => option.MapFrom(source => source.EthnicityObj.ID))
                .ForMember(dest => dest.Vehicles, option => option.MapFrom(source => source.VehiclesList))
                // Contact information 
                .ForMember(dest => dest.PreferredContactId, option => option.MapFrom(source => source.PreferredContactId))
                .ForMember(dest => dest.LeadAddress, option => option.MapFrom(source => source.AddressList.Where(x => x.StatusObj.StatusCode == "A").OrderByDescending(x => x.IsShowOnLeadPage).ThenByDescending(x => x.IsMailingAddress).FirstOrDefault(x => x.IsShowOnLeadPage || x.IsMailingAddress || (!x.IsShowOnLeadPage && !x.IsMailingAddress))))
                ////.ForMember(dest => dest.CountryId, option => option.MapFrom(source => source.AddressList.SingleOrDefault(x => x.IsShowOnLeadPage).Country.ID))
                ////.ForMember(dest => dest.CountyId, option => option.MapFrom(source => source.CountyObj.ID))
                ////.ForMember(dest => dest.AddressStateId, option => option.MapFrom(source => source.StateObj.ID))
                ////.ForMember(dest => dest.City, option => option.MapFrom(source => source.City))
                ////.ForMember(dest => dest.Zip, option => option.MapFrom(source => source.Zip))
                ////.ForMember(dest => dest.AddressTypeId, option => option.MapFrom(source => source.AddressTypeObj.ID))
                ////.ForMember(dest => dest.Address1, option => option.MapFrom(source => source.Address1))
                ////.ForMember(dest => dest.Address2, option => option.MapFrom(source => source.Address2))
                ////.ForMember(dest => dest.AddressApt, option => option.MapFrom(source => source.AddressApt))
                .ForMember(dest => dest.PhonesList, option => option.MapFrom(source => source.LeadPhoneList.Where(x => x.Status.StatusCode == "A").OrderByDescending(x => x.IsBest).ThenByDescending(x => x.IsShowOnLeadPage).ThenByDescending(x => x.Position)))
                .ForMember(dest => dest.EmailList, option => option.MapFrom(source => source.LeadEmailList.Where(x => x.Status.StatusCode == "A").OrderByDescending(x => x.IsPreferred).ThenByDescending(x => x.IsShowOnLeadPage)))
                .ForMember(dest => dest.BestTime, option => option.MapFrom(source => source.BestTime))
                .ForMember(dest => dest.NoneEmail, option => option.MapFrom(source => source.NoneEmail))
                ////.ForMember(dest => dest.OtherState, option => option.MapFrom(source => source.OtherStates))

                // Source Information
                .ForMember(dest => dest.SourceCategoryId, option => option.MapFrom(source => source.SourceCategoryObj.ID))
                .ForMember(dest => dest.SourceTypeId, option => option.MapFrom(source => source.SourceTypeObj.ID))
                .ForMember(dest => dest.AdvertisementId, option => option.MapFrom(source => source.SourceAdvertisementObj.ID))
                .ForMember(dest => dest.Note, option => option.MapFrom(source => this.GetNotesInfo(source)))
                .ForMember(dest => dest.SourceDateTime, option => option.MapFrom(source => source.SourceDate))

                .ForMember(dest => dest.VendorSource, option => option.MapFrom(source => source.Vendor != null ? (source.Vendor.UserType != null && source.Vendor.UserType.Code == UserType.VendorUserTypeCode ? source.Vendor.FullName : string.Empty) : string.Empty))

                // Other information
                .ForMember(dest => dest.AssignedDate, option => option.MapFrom(source => source.AssignedDate))
                .ForMember(dest => dest.DateApplied, option => option.MapFrom(source => source.DateApplied))
                .ForMember(dest => dest.AdmissionRepId, option => option.MapFrom(source => source.AdmissionRepUserObj.ID))
                .ForMember(dest => dest.AgencySponsorId, option => option.MapFrom(source => source.AgencySponsorsObj.ID))
                .ForMember(dest => dest.Comments, option => option.MapFrom(source => this.GetCommentInfo(source)))
                .ForMember(dest => dest.PreviousEducationId, option => option.MapFrom(source => source.PreviousEducationObj.ID))
                .ForMember(dest => dest.HighSchoolId, option => option.MapFrom(source => source.HighSchoolsObj.ID))
                .ForMember(dest => dest.HighSchoolName, option => option.MapFrom(source => source.HighSchoolsObj.HsDescrip))
                .ForMember(dest => dest.HighSchoolGradDate, option => option.MapFrom(source => source.HighSchoolGradDate))
                .ForMember(dest => dest.AttendingHs, option => option.MapFrom(source => source.AttendingHs))
                .ForMember(dest => dest.AdminCriteriaId, option => option.MapFrom(source => source.AdminCriteriaObj.ID))
                .ForMember(dest => dest.ReasonNotEnrolled, option => option.MapFrom(source => source.ReasonNotEnrolledObj.ID))

                // Academics
                .ForMember(dest => dest.CampusId, option => option.MapFrom(source => source.Campus.ID))
                .ForMember(dest => dest.AreaId, option => option.MapFrom(source => source.ProgramGroupObj.ID))
                .ForMember(dest => dest.PrgVerId, option => option.MapFrom(source => source.ProgramVersion.ID))
                .ForMember(dest => dest.ProgramId, option => option.MapFrom(source => source.ProgramObj.ID))
                .ForMember(dest => dest.AttendTypeId, option => option.MapFrom(source => source.AttendTypeObj.ID))

                .ForMember(dest => dest.ExpectedStart, option => option.MapFrom(source => source.ExpectedStart))
                .ForMember(dest => dest.LeadStatusId, option => option.MapFrom(source => source.LeadStatus.ID))
                .ForMember(dest => dest.ScheduleId, option => option.MapFrom(source => source.ProgramScheduleObj.ID))

                // School Defined Fields
                .ForMember(dest => dest.SdfList, option => option.Ignore())

                // Lead Groups
                .ForMember(dest => dest.GroupIdList, option => option.Ignore())

                // General field
                .ForMember(dest => dest.ModUser, option => option.Ignore())
                .ForMember(dest => dest.AvoidDuplicateAnalysis, option => option.Ignore())
                .ForMember(dest => dest.StateChangeIdsList, option => option.Ignore())
                .ForMember(dest => dest.IsEnrolled, option => option.Ignore())
                // Input Model
                .ForMember(dest => dest.InputModel, option => option.Ignore());
        }

        /// <summary>
        /// TODO The get comment info.
        /// </summary>
        /// <param name="source">
        /// TODO The source.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetCommentInfo(Lead source)
        {
            var noteobj = source.NotesList.FirstOrDefault(x => x.PageFieldsObj.ID == (int)Enumerations.EnumNotePageField.InfoComment); // Comments in Others 
            var output = (noteobj == null) ? string.Empty : noteobj.NoteText;
            return output;
        }

        /// <summary>
        /// TODO The get notes info.
        /// </summary>
        /// <param name="source">
        /// TODO The source.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetNotesInfo(Lead source)
        {
            var noteobj = source.NotesList.FirstOrDefault(x => x.PageFieldsObj.ID == (int)Enumerations.EnumNotePageField.InfoNote); // Advertisement notes map to InfopageNote -Notes
            var output = (noteobj == null) ? string.Empty : noteobj.NoteText;
            return output;
        }
    }
}
