﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEducationOutputModelMapper.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Mappings.Lead.LeadEducationOutputModelMapper
// </copyright>
// <summary>
//   The lead education output model mapper.
//   Maps Domain Entity <code>LeadEducation</code> with message output model: <code>LeadEducationOutputModel</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using System;

    using AutoMapper;
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Messages.Lead.Education;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    /// The lead education output model mapper.
    /// Maps Domain Entity <code>LeadEducation</code> with message output model: <code>LeadEducationOutputModel</code>
    /// </summary>
    public class LeadEducationOutputModelMapper : IMapperDefinition
    {
        /// <summary>
        /// The create definition.
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            var map = Mapper.CreateMap<LeadEducation, LeadEducationOutputModel>();
            //map.ForAllMembers(opt => opt.Ignore());

            map.ForMember(dest => dest.Id, option => option.MapFrom(source => source.ID));
            map.ForMember(dest => dest.CertificateOrDegree, option => option.MapFrom(source => source.Certificate));
            map.ForMember(dest => dest.Comments, option => option.MapFrom(source => source.Comments));
            map.ForMember(dest => dest.FinalGrade, option => option.MapFrom(source => source.FinalGrade));
            map.ForMember(dest => dest.Gpa, option => option.MapFrom(source => source.Gpa ?? 0));
            map.ForMember(dest => dest.Graduate, option => option.MapFrom(source => source.Graduate));
            map.ForMember(dest => dest.GraduationDate, option => option.MapFrom(source => source.GraduatedDate != null ? source.GraduatedDate.Value.ToString("MM/dd/yyyy") : string.Empty));
            map.ForMember(dest => dest.Institution, option => option.MapFrom(source => source.Institution));
            map.ForMember(dest => dest.LeadId, option => option.MapFrom(source => source.Lead.ID));
            map.ForMember(dest => dest.Level, option => option.MapFrom(source => source.Institution != null ? source.Institution.Level != null ? source.Institution.Level.Description : string.Empty : string.Empty));
            map.ForMember(dest => dest.LevelId, option => option.MapFrom(source => source.Institution != null ? source.Institution.Level != null ? source.Institution.Level.ID.ToString() : string.Empty : string.Empty));
            map.ForMember(dest => dest.Major, option => option.MapFrom(source => source.Major));
            map.ForMember(dest => dest.Percentile, option => option.MapFrom(source => source.Percentile));
            map.ForMember(dest => dest.Rank, option => option.MapFrom(source => source.Rank));
        }
    }
}
