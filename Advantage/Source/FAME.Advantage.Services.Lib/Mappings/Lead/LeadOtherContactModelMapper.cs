﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Messages.Lead;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using AutoMapper.Configuration;

    /// <summary>
    ///  Mapper for LeadOtherContactModel in Message with the Domain class LeadOtherContact 
    /// </summary>
    public class LeadOtherContactModelMapper : IMapperDefinition
    {
        /// <summary>
        /// Lead output model mapping
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
                
            Mapper.CreateMap<Domain.Lead.LeadOtherContact, LeadOtherContactModel>()
                .ForMember(dest => dest.FirstName, option => option.MapFrom(source => source.FirstName))
                .ForMember(dest => dest.LastName, option => option.MapFrom(source => source.LastName))
                .ForMember(dest => dest.Comments, option => option.MapFrom(source => source.Comments))
                .ForMember(dest => dest.MiddleName, option => option.MapFrom(source => source.MiddleName))
                .ForMember(dest => dest.ContactType, option => option.MapFrom(source => source.ContactType.Description))
                .ForMember(dest => dest.ContactTypeId, option => option.MapFrom(source => source.ContactType.ID.ToString()))
                .ForMember(dest => dest.OtherContactId, option => option.MapFrom(source => source.ID.ToString()))
                .ForMember(dest => dest.LeadId, option => option.MapFrom(source => source.Lead.ID.ToString()))
                .ForMember(dest => dest.Prefix, option => option.MapFrom(source => source.Prefix.Description))
                .ForMember(dest => dest.PrefixId, option => option.MapFrom(source => source.Prefix.ID.ToString()))
                .ForMember(dest => dest.SufixId, option => option.MapFrom(source => source.Sufix.ID.ToString()))
                .ForMember(dest => dest.Sufix, option => option.MapFrom(source => source.Sufix.Description.ToString()))
                .ForMember(dest => dest.RelationshipId, option => option.MapFrom(source => source.Relationship.ID.ToString()))
                .ForMember(dest => dest.Relationship, option => option.MapFrom(source => source.Relationship.Description.ToString()))
                .ForMember(dest => dest.FullName,
                    option => option.MapFrom(source => GetFullName(source.FirstName, source.LastName, source.Sufix, source.Prefix)))
                .ForMember(dest => dest.ModDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(dest => dest.ModUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(dest => dest.Status, option => option.MapFrom(source => source.Status.Status))
                .ForMember(dest => dest.StatusId, option => option.MapFrom(source => source.Status.ID.ToString()))
                .ForMember(dest => dest.Addresses, option => option.MapFrom(source => source.Addresses))
                .ForMember(dest => dest.Phones, option => option.MapFrom(source => source.Phones))
                .ForMember(dest => dest.Emails, option => option.MapFrom(source => source.Emails))
                ;
        }

        private string GetFullName( string FirstName, string Lastname, Suffix Sufix, Prefix Prefix)
        {
            string fullName = "";
            if (Prefix != null)
                fullName += Prefix.Description + " ";
            fullName += FirstName + " " + Lastname;
            if (Sufix != null)
                fullName +=  " " + Sufix.Description;
            return fullName;
        }
    }
}



