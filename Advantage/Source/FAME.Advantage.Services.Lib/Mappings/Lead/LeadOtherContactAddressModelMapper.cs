﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FAME.Advantage.Messages.Lead;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using AutoMapper.Configuration;

    /// <summary>
    ///  Mapper for LeadOtherContactAddressModel in Message with the Domain class LeadOtherContactAddress 
    /// </summary>
    public class LeadOtherContactAddressModelMapper : IMapperDefinition
    {
        /// <summary>
        /// Mapper Definition
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
              Mapper.CreateMap<Domain.Lead.LeadOtherContactAddress, LeadOtherContactAddressModel>()
                     .ForMember(dest => dest.OtherContactsAddresesId, option => option.MapFrom(source => source.ID.ToString()))
                .ForMember(dest => dest.OtherContactId, option => option.MapFrom(source => source.LeadOtherContact.ID.ToString()))
                .ForMember(dest => dest.LeadId, option => option.MapFrom(source => source.Lead.ID.ToString()))
                .ForMember(dest => dest.Address1, option => option.MapFrom(source => source.Address1))
                .ForMember(dest => dest.Address2, option => option.MapFrom(source => source.Address2))
                .ForMember(dest => dest.AddressType, option => option.MapFrom(source => source.AddressType.AddessDescrip))
                .ForMember(dest => dest.AddressTypeId, option => option.MapFrom(source => source.AddressType.ID.ToString()))
                .ForMember(dest => dest.City, option => option.MapFrom(source => source.City))
                .ForMember(dest => dest.IsMailingAddress, option => option.MapFrom(source => source.IsMailingAddress))
                .ForMember(dest => dest.Country, option => option.MapFrom(source => source.IsInternational ? source.CountryInternational : source.Country.CountryDescrip))
                .ForMember(dest => dest.CountryId, option => option.MapFrom(source => source.Country.ID.ToString()))
                .ForMember(dest => dest.State, option => option.MapFrom(source => source.IsInternational ? source.StateInternational : source.State.Code))
                .ForMember(dest => dest.StateId, option => option.MapFrom(source => source.State.ID.ToString()))
                .ForMember(dest => dest.CountyId, option => option.MapFrom(source => source.County.ID.ToString()))
                .ForMember(dest => dest.County, option => option.MapFrom(source => source.IsInternational ? source.CountyInternational : source.County.CountyDescrip.ToString()))
                .ForMember(dest => dest.Status, option => option.MapFrom(source => source.Status.Status))
                .ForMember(dest => dest.IsInternational, option => option.MapFrom(source => source.IsInternational))
                .ForMember(dest => dest.StatusId, option => option.MapFrom(source => source.Status.ID.ToString()));
        }
    }
}
