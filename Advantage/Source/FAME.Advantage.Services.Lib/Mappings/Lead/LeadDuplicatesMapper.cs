﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadDuplicatesMapper.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Mappers for Lead Duplicate page
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using System.Linq;

    using AutoMapper;
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Messages.Lead;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    /// Mappers for Lead Duplicate page
    /// </summary>
    public class LeadDuplicatesMapper : IMapperDefinition
    {
        #region Implementation of IMapperDefinition

        /// <summary>
        /// Mapper Definitions
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.Lead.Lead, LeadDuplicatesOutputModel>()
                .ForMember(
                    dest => dest.FullName,
                    option => option.MapFrom(source => string.Format("{0}, {1}", source.LastName, source.FirstName)))
                .ForMember(dest => dest.Header, option => option.Ignore())
                .ForMember(
                    dest => dest.From,
                    option =>
                    option.MapFrom(
                        source =>
                        source.LeadStatus.SystemStatus.Description.ToUpper() == "IMPORTED" ? "New" : "Advantage"))
                .ForMember(dest => dest.Campus, option => option.MapFrom(source => source.Campus.Description))
                .ForMember(
                    dest => dest.AcquiredDate,
                    option =>
                    option.MapFrom(source => source.SourceDate == null ? string.Empty : source.SourceDate.ToString()))
                .ForMember(
                    dest => dest.Phone,
                    option =>
                    option.MapFrom(
                        source =>
                        (source.LeadPhoneList == null || source.LeadPhoneList.Count < 1)
                            ? string.Empty
                            : source.LeadPhoneList.FirstOrDefault().Phone))
                .ForMember(dest => dest.Comments, option => option.Ignore())
                .ForMember(dest => dest.State, option => option.MapFrom(source => source.StateObj.Code))
                .ForMember(
                    dest => dest.Status,
                    option => option.MapFrom(source => source.LeadStatus.SystemStatus.Description))
                .ForMember(
                    dest => dest.AdmissionsRep,
                    option => option.MapFrom(source => source.AdmissionRepUserObj.FullName))

                .ForMember(
                    dest => dest.Email,
                    option =>
                    option.MapFrom(
                        source =>
                        (source.LeadEmailList == null || source.LeadEmailList.Count < 1)
                            ? string.Empty
                            : this.GetEmail(source))) 

                .ForMember(dest => dest.Address1, option => option.MapFrom(source => (source.AddressList == null || source.AddressList.Count < 1) ? string.Empty : source.AddressList.FirstOrDefault(x => x.IsShowOnLeadPage).Address1))
                .ForMember(dest => dest.Address2, option => option.MapFrom(source => (source.AddressList == null || source.AddressList.Count < 1) ? string.Empty : source.AddressList.FirstOrDefault(x => x.IsShowOnLeadPage).Address2))
                .ForMember(dest => dest.City, option => option.MapFrom(source => (source.AddressList == null || source.AddressList.Count < 1) ? string.Empty : source.AddressList.FirstOrDefault(x => x.IsShowOnLeadPage).City))
                .ForMember(dest => dest.Zip, option => option.MapFrom(source => (source.AddressList == null || source.AddressList.Count < 1) ? string.Empty : source.AddressList.FirstOrDefault(x => x.IsShowOnLeadPage).ZipCode))

                // .ForMember(dest => dest.Address1, option => option.MapFrom(source => source.AddressList.SingleOrDefault(x => x.IsShowOnLeadPage).Address1))
                // .ForMember(dest => dest.Address2, option => option.MapFrom(source => source.AddressList.SingleOrDefault(x => x.IsShowOnLeadPage).Address2))
                // .ForMember(dest => dest.City, option => option.MapFrom(source => source.AddressList.SingleOrDefault(x => x.IsShowOnLeadPage).City))
                // .ForMember(dest => dest.Zip, option => option.MapFrom(source => source.AddressList.SingleOrDefault(x => x.IsShowOnLeadPage).ZipCode))
                .ForMember(
                    dest => dest.SourceInfo,
                    option =>
                    option.MapFrom(
                        source =>
                        string.Format(
                            "{0}, {1}",
                            source.CampaignObj.VendorObj.VendorName,
                            source.CampaignObj.CampaignCode)));
        }

        #endregion

        private string GetEmail(Lead source)
        {
            if (source == null)
            {
                return string.Empty;
            }

            var mailObj = source.LeadEmailList.FirstOrDefault(x => x.IsShowOnLeadPage)
                          ?? source.LeadEmailList.FirstOrDefault();

            if (mailObj == null)
            {
                return string.Empty;
            }

            return mailObj.Email;
        }
    }
}
