﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadRequirementOutputModelMapper.cs" company="FAME Inc.">
//   FAME 2016
//   FAME.Advantage.Services.Lib.Mappings.Lead.LeadRequirementOutputModelMapper
// </copyright>
// <summary>
//   The lead requirement output model mapper.
//   Auto Mapper class for converting a Domain.Requirements.<see cref="Requirement" /> into a Message.<see cref="LeadRequirementOutputModelMapper" />.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Mappings.Lead
{
    using System;
    using System.Linq;
    using AutoMapper;
    using AutoMapper.Configuration;

    using Domain.Requirements;

    using FAME.Advantage.Domain.Requirements.Documents;

    using Infrastructure.AutoMapper;
    using Messages.Lead;

    /// <summary>
    /// The lead requirement output model mapper.
    /// Auto Mapper class for converting a Domain.Requirements.<see cref="Requirement"/> into a Message.<see cref="LeadRequirementOutputModelMapper"/>. 
    /// </summary>
    public class LeadRequirementOutputModelMapper : IMapperDefinition
    {
        /// <summary>
        /// The create definition.
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.Requirements.Requirement, LeadReqEntityOutputModel>()
                .ConvertUsing<TypeConverter>();
        }

        /// <summary>
        /// The type converter.
        /// </summary>
        private sealed class TypeConverter : ITypeConverter<Domain.Requirements.Requirement, LeadReqEntityOutputModel>
        {
            /// <summary>
            /// The convert mapping
            /// </summary>
            /// <param name="source">The source for the mapping</param>
            /// <param name="destination">The destination for the mapping</param>
            /// <param name="context">The context for the converstion of the mapping</param>
            /// <returns>A converted object from the source type to the type <code>LeadReqEntityOutputModel</code></returns>
            public LeadReqEntityOutputModel Convert(Requirement source, LeadReqEntityOutputModel destination, ResolutionContext context)
            {
                if (source == null)
                {
                    throw new ApplicationException("source is in invalid state");
                }

                if (destination == null)
                {
                    throw new ApplicationException("dest is in invalid state");
                }

                destination.Id = source.ID;
                destination.Code = source.Code;
                destination.Description = source.Description;
                destination.DisplayType = source.Type.Description;

                destination.ApprovalDate = string.Empty;
                destination.DateReceived = string.Empty;

                var effectiveDates = source.EffectiveDates.FirstOrDefault(n => n.StartDate <= DateTime.Now.Date);
                if (effectiveDates != null)
                {
                    destination.MinScore = effectiveDates.MinimumScore.ToString();
                }

                destination.IsMandatory = source.IsRequired;

                switch (source.Type.Description.ToUpper())
                {
                    case "DOCUMENT":
                        var doc = source.LeadDocuments.Where(n => n.Lead.ID == source.LeadGuid).OrderByDescending(n => n.ModDate).FirstOrDefault();
                        if (doc != null)
                        {
                            destination.DateReceived = doc.DateReceived != null ? doc.DateReceived.Value.ToShortDateString() : string.Empty;
                            destination.IsOverridden = doc.Override;
                            destination.IsApproved = doc.Override;
                            destination.OverReason = doc.OverrideReason;
                            if (doc.DocumentStatus != null)
                            {
                                destination.IsApproved = doc.DocumentStatus.IsApproved;
                                destination.DocumentStatusId = doc.DocumentStatus.ID;
                                if (doc.DocumentStatus.IsApproved)
                                {
                                    destination.ApprovalDate = doc.ApprovalDate.HasValue ? doc.ApprovalDate.Value.ToShortDateString() : string.Empty;
                                    if (string.IsNullOrEmpty(destination.ApprovalDate))
                                    {
                                        destination.ApprovalDate = doc.ModDate.ToShortDateString();
                                    }
                                }
                            }

                            destination.DocumentRequirementId = doc.ID;
                            if (source.UserList != null)
                            {
                                var userOutputModel = source.UserList.FirstOrDefault(n => n.UserName == doc.ModUser);
                                if (userOutputModel != null)
                                {
                                    destination.ApprovedBy = userOutputModel.FullName;
                                }
                            }
                        }

                        break;
                    case "TEST":

                        destination.IsApproved = false;
                        destination.IsOverridden = false;
                        var leadEntranceOutput =
                            source.LeadEntranceTests.Where(n => n.LeadObj != null && n.LeadObj.ID == source.LeadGuid)
                                .OrderByDescending(x => x.ModDate);

                        LeadEntranceTest appTest = null;
                        appTest = leadEntranceOutput.FirstOrDefault(x => x.Override != null && x.Override.Value == true);

                        if (appTest == null)
                        {
                            appTest = leadEntranceOutput.Where(x => x.Override == null).OrderByDescending(x => x.ModDate).ThenByDescending(x => x.CompletedDate).FirstOrDefault();
                        }

                        if (appTest == null)
                        {
                            appTest = leadEntranceOutput.OrderByDescending(x => x.ModDate).ThenByDescending(x => x.CompletedDate).FirstOrDefault();
                        }

                        if (appTest != null)
                        {
                            destination.IsApproved = appTest.Pass != null ? appTest.Pass.Value : false;
                            if (appTest.ModDate != null)
                            {
                                destination.ApprovalDate = appTest.ModDate.Value.ToShortDateString();
                            }

                            if (appTest.CompletedDate != null)
                            {
                                destination.DateReceived = appTest.CompletedDate.Value.ToShortDateString();
                            }

                            if (source.UserList != null)
                            {
                                var userOutputModel = source.UserList.FirstOrDefault(n => n.UserName == appTest.ModUser);
                                if (userOutputModel != null)
                                {
                                    destination.ApprovedBy = userOutputModel.FullName;
                                }
                            }

                            if (appTest.Override != null)
                            {
                                destination.IsOverridden = (bool)appTest.Override;
                                destination.OverReason = appTest.OverrideReason;
                                if (destination.IsOverridden)
                                {
                                    destination.IsApproved = true;
                                }
                            }
                            if (appTest.ActualScore != null)
                            {
                                destination.Score = appTest.ActualScore.ToString();
                            }

                            destination.DocumentRequirementId = appTest.ID;
                        }


                        break;
                    case "INTERVIEW":
                    case "EVENT":
                    case "TOUR":
                        {
                            destination.IsApproved = false;
                            destination.IsOverridden = false;
                            var leadReqOutput =
                                source.LeadRecsReceived.Where(n => n.Lead.ID == source.LeadGuid).OrderByDescending(x => x.ModDate);

                            AdLeadReqsReceived appReq = null;
                            appReq = leadReqOutput.Where(x => x.IsApproved == true && x.Override).OrderByDescending(x => x.ModDate).ThenBy(x => x.ApprovalDate).FirstOrDefault();

                            if (appReq == null)
                            {
                                appReq = leadReqOutput.OrderByDescending(x => x.ModDate).ThenBy(x => x.ApprovalDate).FirstOrDefault(x => x.Override == false);
                            }
                            if (appReq == null)
                            {
                                appReq = leadReqOutput.OrderByDescending(x => x.ModDate).ThenBy(n => n.ReceivedDate).FirstOrDefault();
                            }

                            if (appReq != null)
                            {
                                destination.IsApproved = appReq.IsApproved != null ? appReq.IsApproved.Value : false;
                                destination.OverReason = appReq.OverrideReason;
                                destination.IsOverridden = appReq.Override;

                                if (appReq.ApprovalDate != null)
                                {
                                    destination.ApprovalDate = appReq.ApprovalDate.Value.ToShortDateString();
                                }


                                if (appReq.ReceivedDate != null)
                                {
                                    destination.DateReceived = appReq.ReceivedDate != null
                                                            ? appReq.ReceivedDate.Value.ToShortDateString()
                                                            : string.Empty;
                                }

                                if (source.UserList != null)
                                {
                                    var userOutputModel = source.UserList.FirstOrDefault(n => n.UserName == appReq.ModUser);
                                    if (userOutputModel != null)
                                    {
                                        destination.ApprovedBy = userOutputModel.FullName;
                                    }
                                }
                            }

                            break;
                        }

                    case "FEE":
                        var tranReceived = source.LeadTranReceived.Where(n => n.Lead.ID == source.LeadGuid).ToList().OrderByDescending(n => n.ReceivedDate).FirstOrDefault();
                        if (tranReceived != null)
                        {
                            destination.DocumentRequirementId = tranReceived.ID;

                            destination.IsOverridden = tranReceived.Override;

                            bool showApprovedBy = false;

                            var totalVoidedTransactions =
                                tranReceived.Requirement.LeadTransactions.Count(
                                    n =>
                                    n.LeadObj.ID == source.LeadGuid
                                    && n.TransactionCode.SysTransCodeId == 20
                                    && n.TransactionType.Description.ToUpper() == "PAYMENT"
                                    && (n.Voided != null && n.Voided.Value));

                            var totalPaymentTransaction =
                                tranReceived.Requirement.LeadTransactions.Count(
                                    n =>
                                    n.LeadObj.ID == source.LeadGuid
                                    && n.TransactionCode.SysTransCodeId == 20
                                    && n.TransactionType.Description.ToUpper() == "PAYMENT");

                            if (totalPaymentTransaction > totalVoidedTransactions)
                            {
                                showApprovedBy = true;
                                destination.DateReceived = tranReceived.ReceivedDate.ToShortDateString();

                                if (source.LeadTransactions.Any())
                                {
                                    destination.IsApproved = tranReceived.IsApproved;
                                }

                                if (destination.IsApproved)
                                {
                                    destination.ApprovalDate = tranReceived.ApprovalDate != null ? tranReceived.ApprovalDate.Value.ToShortDateString() : string.Empty;
                                }
                            }
                            else
                            {
                                showApprovedBy = tranReceived.Override;
                                destination.IsApproved = tranReceived.Override;
                            }

                            if (showApprovedBy)
                            {
                                if (source.UserList != null)
                                {
                                    var userOutputModel = source.UserList.FirstOrDefault(
                                    n => n.UserName == tranReceived.ModUser);
                                    if (userOutputModel != null)
                                    {
                                        destination.ApprovedBy = userOutputModel.FullName;
                                    }
                                }
                            }

                            if (tranReceived.Override)
                            {
                                destination.OverReason = tranReceived.OverrideReason;
                            }
                        }

                        break;
                }

                return destination;
            }
        }
    }
}



