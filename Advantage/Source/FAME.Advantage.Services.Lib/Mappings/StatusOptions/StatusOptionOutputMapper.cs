﻿using AutoMapper;
using FAME.Advantage.Domain.StatusesOptions;
using FAME.Advantage.Messages.StatusOptions;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.ProgramVersions
{
    using AutoMapper.Configuration;

    public class StatusOptionOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<StatusOption, StatusOptionOutputModel>();
        }
    }
}
