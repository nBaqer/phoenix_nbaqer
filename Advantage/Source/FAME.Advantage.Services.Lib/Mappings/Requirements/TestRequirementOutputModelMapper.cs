﻿using AutoMapper;
using FAME.Advantage.Domain.Student.Requirements;
using FAME.Advantage.Messages.Requirements;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Requirements
{
    public class TestRequirementOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition()
        {
            Mapper.CreateMap<TestRequirement, TestRequirementOutputModel>();
        }
    }
}
