﻿using AutoMapper;
using FAME.Advantage.Domain.Requirements;
using FAME.Advantage.Messages.Requirements;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Requirements
{
    using AutoMapper.Configuration;

    public class RequirementEffectiveDatesOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<RequirementEffectiveDates, RequirementEffectiveDatesOutputModel>();
        }
    }
}
