﻿using System.Linq;
using System.Security.Cryptography;
using AutoMapper;
using FAME.Advantage.Domain.Requirements.Documents;
using FAME.Advantage.Messages.Requirements.Document;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Student.Requirements.Documents
{
    using AutoMapper.Configuration;

    public class DocumentOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Document, DocumentOutputModel>()
                .ForMember(dest => dest.FirstName, option => option.MapFrom(source => source.Student.FirstName))
                .ForMember(dest => dest.MiddleName, option => option.MapFrom(source => source.Student.MiddleName))
                .ForMember(dest => dest.LastName, option => option.MapFrom(source => source.Student.LastName))
                .ForMember(dest => dest.Student, option => option.MapFrom(source => source.Student))
                .ForMember(dest => dest.DocumentName, option => option.MapFrom(source => source.Requirement.Description))
                .ForMember(dest => dest.SystemModuleName, option => option.MapFrom(source => source.Requirement.SystemModule.Name))
                .ForMember(dest => dest.RequiredFor, option => option.MapFrom(source => source.Requirement.RequiredFor.ToString()))
                .ForMember(dest => dest.FullName, option => option.MapFrom(source => source.Student.FullName))
                .ForMember(dest => dest.IsApproved, option => option.MapFrom(source => source.DocumentStatus.IsApproved))
                .ForMember(dest => dest.Files, option => option.MapFrom(source => source.Files != null ? source.Files.OrderByDescending(n=>n.DateCreated) : null))
                .ForMember(dest=> dest.ShowUpdate, option => option.MapFrom(source=>source.showUpdate))
                .ForMember(dest => dest.Count, option => option.Ignore());
        }
    }
}




