﻿using AutoMapper;
using FAME.Advantage.Domain.Requirements.Documents;
using FAME.Advantage.Messages.Students.Requirements.Document;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Student.Requirements.Documents
{
    public class DocumentHistoryOuputModelMapper : IMapperDefinition
    {
        public void CreateDefinition()
        {
            Mapper.CreateMap<DocumentHistory, DocumentHistoryOutputModel>();
        }
    }
}
