﻿using AutoMapper;
using FAME.Advantage.Domain.Requirements.Documents;
using FAME.Advantage.Messages.Requirements.Document;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Student.Requirements.Documents
{
    public class PostDocumentOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition()
        {
            Mapper.CreateMap<Document, PostDocumentOutputModel>()
                .ForMember(dest => dest.IsApproved, option => option.MapFrom(source => source.DocumentStatus.IsApproved));
        }
    }
}
