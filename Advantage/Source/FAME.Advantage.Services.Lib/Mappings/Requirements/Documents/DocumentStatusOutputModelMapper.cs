﻿using System;
using AutoMapper;
using FAME.Advantage.Domain.Requirements.Documents;
using FAME.Advantage.Messages.DocumentStatuses;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Student.Requirements.Documents
{
    using AutoMapper.Configuration;

    public class SystemModuleOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<DocumentStatus, DocumentStatusOutputModel>()
                .ForMember(dest => dest.Description, option => option.MapFrom(source => String.Format("{0} ({1})",source.Description,source.Code)));
        }
    }
}
