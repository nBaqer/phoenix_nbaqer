﻿using AutoMapper;
using FAME.Advantage.Domain.Requirements.Documents.File;
using FAME.Advantage.Messages.Requirements.Document.File;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Requirements.Documents.Files
{
    using AutoMapper.Configuration;

    public class FileOuputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<File, FileOutputModel>();
        }
    }
}
