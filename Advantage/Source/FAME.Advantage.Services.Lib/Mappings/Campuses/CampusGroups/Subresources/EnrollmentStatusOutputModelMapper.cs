﻿using AutoMapper;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.SystemStuff;
using FAME.Advantage.Messages.Campuses.CampusGroups.EnrollmentStatuses;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Campuses.CampusGroups.Subresources
{
    using AutoMapper.Configuration;

    public class EnrollmentStatusOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<UserDefStatusCode, EnrollmentStatusOutputModel>();
        }
    }
}
