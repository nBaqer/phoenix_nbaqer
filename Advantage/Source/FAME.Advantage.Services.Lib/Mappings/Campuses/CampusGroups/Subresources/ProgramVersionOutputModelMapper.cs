﻿using AutoMapper;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Messages.Campuses.CampusGroups.ProgramVersions;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Campuses.CampusGroups.Subresources
{
    using AutoMapper.Configuration;

    public class ProgramVersionOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<ProgramVersion, ProgramVersionOutputModel>();
        }
    }
}
