﻿using AutoMapper;
using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Messages.Campuses.CampusGroups.Terms;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Campuses.CampusGroups.Subresources
{
    using AutoMapper.Configuration;

    public class TermOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Term, TermsOutputModel>();
        }
    }
}
