﻿using AutoMapper;
using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Messages.CampusGroups;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.CampusGroups
{
    using AutoMapper.Configuration;

    public class CampusGroupOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<CampusGroup, CampusGroupOutputModel>();
        }
    }
}
