﻿using AutoMapper;
using FAME.Advantage.Messages.Campuses;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Campuses
{
    using AutoMapper.Configuration;

    public class CampusOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.Campuses.Campus, CampusOutputModel>();
        }
    }
}
