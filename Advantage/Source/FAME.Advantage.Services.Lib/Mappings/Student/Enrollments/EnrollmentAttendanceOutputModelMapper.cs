﻿using AutoMapper;
using FAME.Advantage.Messages.Students.Enrollments;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Student.Enrollments
{
    using AutoMapper.Configuration;

    public class EnrollmentAttendanceOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.Student.Enrollments.Enrollment, EnrollmentAttendanceOutputModel>()
                .ForMember(dest => dest.ID, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.Description, option => option.MapFrom(source => source.ProgramVersion == null ? string.Empty : source.ProgramVersion.Description + " (" + source.EnrollmentStatus.Description + ")"))
                .ForMember(dest => dest.TardiesMakingAusence, option =>  option.MapFrom(source => source.ProgramVersion == null ? 1000 : source.ProgramVersion.TardiesMakingAbsence))
                .ForMember(dest => dest.TrackTardies, option =>  option.MapFrom(source => source.ProgramVersion == null ? 0 : source.ProgramVersion.TrackTardies))
                .ForMember(dest => dest.AttendanceType, option => option.MapFrom(source => source.ProgramVersion == null ? string.Empty : source.ProgramVersion.AttendanceType.Description));

        }
    }
}