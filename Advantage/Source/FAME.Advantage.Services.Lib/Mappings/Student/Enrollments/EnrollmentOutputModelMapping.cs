﻿using AutoMapper;
using FAME.Advantage.Domain.Student.Enrollments;
using FAME.Advantage.Messages.Student.Enrollments;
using FAME.Advantage.Messages.Students.Enrollments;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Student.Enrollments
{
    using AutoMapper.Configuration;

    using FAME.Advantage.Messages.Lead.Enroll;

    public class EnrollmentOutputModelMapping : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Enrollment, EnrollmentOutputModel>();
          
        }
    }
}
