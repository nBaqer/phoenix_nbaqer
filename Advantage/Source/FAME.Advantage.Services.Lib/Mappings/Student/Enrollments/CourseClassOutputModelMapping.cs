﻿using AutoMapper;
using FAME.Advantage.Messages.Students.Enrollments;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Student.Enrollments
{
    using AutoMapper.Configuration;

    public class CourseClassOutputModelMapping : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.Student.Enrollments.ArClassSections, CourseClassOutputModel>()
                .ForMember(dest => dest.ClassSectionGuid, option => option.MapFrom(source => source.ID))
                .ForMember(dest => dest.RequerimentDescription,
                    option =>
                        option.MapFrom(
                            source =>
                                source.RequerimentCourse.Description + " [ " + source.RequerimentCourse.Code + " ," +
                                source.ClassSection + " ]"))
                .ForMember(dest => dest.RequerimentGuid, option => option.MapFrom(source => source.RequerimentCourse.ID))
                .ForMember(dest => dest.UnitTypeDescription,
                    option => option.MapFrom(source => source.RequerimentCourse.UnitType.Description))
                .ForMember(dest => dest.UnitTypeGuid, option => option.MapFrom(source => source.RequerimentCourse.ID))
                .ForMember(dest => dest.IsTrackTardies, option => option.MapFrom(source => source.RequerimentCourse.IsTrackTardies))
                .ForMember(dest => dest.TardiesMakingAbsence, option => option.MapFrom(source => source.RequerimentCourse.TardiesMakingAbsence.GetValueOrDefault(0)));

        }
    }
}
