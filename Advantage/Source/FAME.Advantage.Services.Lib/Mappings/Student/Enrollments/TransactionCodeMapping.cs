﻿using AutoMapper;
using FAME.Advantage.Domain.Student.Enrollments.Transactions;
using FAME.Advantage.Messages.Students.Enrollments;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Student.Enrollments
{
    using AutoMapper.Configuration;

    public class TransactionCodeMapping : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<TransactionCode, TransactionCodeOutputModel>();
        }
    }
}
