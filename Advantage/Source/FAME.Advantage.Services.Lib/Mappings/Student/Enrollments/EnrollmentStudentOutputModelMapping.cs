﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnrollmentStudentOutputModelMapping.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   The enrollment student output model mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Mappings.Enrollments
{
    using AutoMapper;
    using AutoMapper.Configuration;

    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Messages.Student.Enrollments;
    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

    /// <summary>
    /// The enrollment student output model mapping.
    /// </summary>
    public class EnrollmentStudentOutputModelMapping : IMapperDefinition
    {
        /// <summary>
        /// The create definition.
        /// </summary>
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            // Mapper.CreateMap<Domain.Student.Student, EnrollmentStudentOutputModel>();
            Mapper.CreateMap<Lead, EnrollmentStudentOutputModel>()
                .ForMember(dest => dest.Id, option => option.MapFrom(source => source.StudentId))
                .ForMember(dest => dest.FirstName, option => option.MapFrom(source => source.FirstName))
                .ForMember(dest => dest.MiddleName, option => option.MapFrom(source => source.MiddleName))
                .ForMember(dest => dest.LastName, option => option.MapFrom(source => source.LastName))
                .ForMember(dest => dest.SSN, option => option.MapFrom(source => source.SSN))
                .ForMember(dest => dest.StudentNumber, option => option.MapFrom(source => source.StudentNumber))
                .ForMember(dest => dest.DOB, option => option.MapFrom(source => source.BirthDate))
                .ForMember(dest => dest.Gender, option => option.MapFrom(source => source.GenderObj.ID));

            // .ForMember(dest => dest.HomeEmail, option => option.MapFrom(source => source.LeadEmailList.First(e=> e.IsPreferred).Email))
            //.ForMember(dest => dest.HomeEmail, option => option.Ignore()) 
            //.ForMember(dest => dest.WorkEmail, option => option.Ignore()); 
        }
    }
}
