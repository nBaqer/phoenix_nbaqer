﻿using AutoMapper;
using FAME.Advantage.Messages.Students.Enrollments;
using FAME.Advantage.Services.Lib.BusinessLogic.Students.Dto;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Student.Enrollments
{
    using AutoMapper.Configuration;

    public class AttendanceMinutesOutputModelMapping : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<StudentAttendanceDto, AttendanceMinutesOutputModel>();
        }
    }
}