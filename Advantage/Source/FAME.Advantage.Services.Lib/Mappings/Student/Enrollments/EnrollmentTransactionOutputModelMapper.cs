﻿using AutoMapper;
using FAME.Advantage.Domain.Student.Enrollments.Transactions;
using FAME.Advantage.Messages.Students.Enrollments;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Student.Enrollments
{
    using AutoMapper.Configuration;

    public class EnrollmentTransactionOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {
            Mapper.CreateMap<Domain.Student.Enrollments.Transactions.Transactions, EnrollmentTransactionPayPeriodOutputModel>()
               .ForMember(dest => dest.LastName, option => option.MapFrom(source => source.StudentEnrollment.Student.LastName))
               .ForMember(dest => dest.FirstName, option => option.MapFrom(source => source.StudentEnrollment.Student.FullNameLastFirst))
               .ForMember(dest => dest.FullName, option => option.MapFrom(source => source.StudentEnrollment.Student.FullNameLastFirst))
               .ForMember(dest => dest.StudentNumber, option => option.MapFrom(source => source.StudentEnrollment.Student.StudentNumber))
               .ForMember(dest => dest.PeriodNumber, option => option.MapFrom(source =>  source.PaymentPeriod.PeriodNumber))
               .ForMember(dest => dest.ChargeAmount, option => option.MapFrom(source => source.Amount))
               .ForMember(dest => dest.TranDate, option => option.MapFrom(source => source.TransDate.ToShortDateString()));

        }
    }
}
