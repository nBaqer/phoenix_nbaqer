﻿using AutoMapper;
using FAME.Advantage.Messages.Student;
using FAME.Advantage.Messages.Students.Requirements.Document;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;

namespace FAME.Advantage.Services.Lib.Mappings.Student
{
    using AutoMapper.Configuration;

    public class StudentOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition(MapperConfigurationExpression Mapper)
        {

            Mapper.CreateMap<Domain.Student.Student, StudentOutputModel>()
                .ForMember(dest => dest.SSN, option => option.MapFrom(source => string.IsNullOrEmpty(source.SSN) ? "" : string.Format("***-**-{0}", source.SSN.GetLast(4))));
        }
    }
}



