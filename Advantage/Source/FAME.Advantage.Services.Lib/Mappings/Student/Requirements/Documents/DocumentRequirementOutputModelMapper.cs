﻿using AutoMapper;
using FAME.Advantage.Domain.Student.Requirements;
using FAME.Advantage.Domain.Student.Requirements.Documents;
using FAME.Advantage.Messages.Student.Requirements;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;
using System;
using System.Linq;

namespace FAME.Advantage.Services.Lib.Mappings.Student.Requirements.Documents
{
    public class DocumentRequirementOutputModelMapper : IMapperDefinition
    {
        public void CreateDefinition()
        {
            Mapper.CreateMap<DocumentRequirement, DocumentRequirementOutputModel>()
                .ConvertUsing<TypeConverter>();
        }

        private sealed class TypeConverter : ITypeConverter<DocumentRequirement, DocumentRequirementOutputModel>
        {
            public DocumentRequirementOutputModel Convert(ResolutionContext resolutionResult)
            {
                var source = resolutionResult.SourceValue as DocumentRequirement;
                if (source == null) throw new ApplicationException("source is in invalid state");

                var dest = resolutionResult.DestinationValue as DocumentRequirementOutputModel;
                if (dest == null) throw new ApplicationException("dest is in invalid state");

                dest.Id = source.ID;
                dest.Code = source.Code;
                dest.Description = source.Description;
                dest.Type = source.Type;
                dest.RequiredFor = source.RequiredFor.ToString();
                dest.RequirementType = source.RequirementType;
                dest.Status = source.Status.Status;
                dest.Module = source.SystemModule.Name;
                dest.StartDate = source.EffectiveDates.OrderByDescending(n => n.StartDate).FirstOrDefault().StartDate;
                dest.EndDate = source.EffectiveDates.OrderByDescending(n => n.StartDate).FirstOrDefault().EndDate;
                dest.DateRequested = source.Document.DateRequested;
                dest.DateReceived = source.Document.DateReceived;
                dest.IsApproved = source.Document.DocumentStatus.IsApproved;
                dest.DocumentRequirementId = !Document.IsEmpty(source.Document) ? source.Document.DocumentRequirement.ID : (Guid?)null;
                dest.StudentDocumentId = !Document.IsEmpty(source.Document) ? source.Document.ID : (Guid?)null;
                dest.DocumentStatusId = !Document.IsEmpty(source.Document) ? source.Document.DocumentStatus.ID : (Guid?)null;
                dest.DocumentStatusDescription = !Document.IsEmpty(source.Document) ? source.Document.DocumentStatus.Description : null;
                dest.ShowUpdate = source.showUpdate;

                return dest;
            }
        }
    }
}
