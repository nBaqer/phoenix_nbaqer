﻿using AutoMapper;
using FAME.Advantage.Domain.Student.Requirements.Documents.File;
using FAME.Advantage.Messages.Students.Requirements.Document.File;
using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;

namespace FAME.Advantage.Services.Lib.Mappings.Student.Requirements.Documents.Files
{
    public class FileOuputModelMapper : IMapperDefinition
    {
        public void CreateDefinition()
        {
            Mapper.CreateMap<File, FileOutputModel>();
        }
    }
}
