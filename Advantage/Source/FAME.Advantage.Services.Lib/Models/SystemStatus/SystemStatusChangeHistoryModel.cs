﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemStatusChangeHistoryModel.cs" company="FAME Inc">
//   FAME.Advantage.Services.Lib.Models.SystemStatus
// </copyright>
// <summary>
//   The system status change history model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Models.SystemStatus
{
    using System;

    using FAME.Advantage.Domain.Campuses;
    using FAME.Advantage.Domain.Student.Enrollments;
    using FAME.Advantage.Domain.SystemStuff;

    /// <summary>
    /// The system status change history model.
    /// </summary>
    public class SystemStatusChangeHistoryModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// Gets or sets the enrollment.
        /// </summary>
        public Enrollment Enrollment { get; set; }

        /// <summary>
        /// Gets or sets the original status.
        /// </summary>
        public UserDefStatusCode OriginalStatus { get; set; }

        /// <summary>
        /// Gets or sets the new status.
        /// </summary>
        public UserDefStatusCode NewStatus { get; set; }

        /// <summary>
        /// Gets or sets the campus.
        /// </summary>
        public Campus Campus { get; set; }

        /// <summary>
        /// Gets or sets the modified date. Also known of the date of change
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is reversal.
        /// </summary>
        public bool IsReversal { get; set; }

        /// <summary>
        /// Gets or sets the drop reason id.
        /// </summary>
        public Guid? DropReasonId { get; set; }

        /// <summary>
        /// Gets or sets the date of change.Also Known as Effective Date
        /// </summary>
        public DateTime? DateOfChange { get; set; }

        /// <summary>
        /// Gets or sets the last date of attendance (LDA). 
        /// </summary>
        public DateTime? Lda { get; set; }

        /// <summary>
        /// Gets or sets the case number.
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the requested by.
        /// </summary>
        public string RequestedBy { get; set; }

        /// <summary>
        /// The add millisecond to date of change.
        /// </summary>
        public virtual void AddMillisecondToDateOfChange()
        {
            if (this.DateOfChange != null)
            {
                this.DateOfChange = this.DateOfChange.Value.AddMilliseconds(1000);
            }
        }
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="campus">
        /// The campus.
        /// </param>
        /// <param name="originalStatus">
        /// The original status.
        /// </param>
        /// <param name="newStatus">
        /// The new status.
        /// </param>
        /// <param name="dateOfChange">
        /// The date of change.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="effectiveDate">
        /// The effective date.
        /// </param>
        /// <param name="caseNumber">
        /// The case number.
        /// </param>
        /// <param name="requestedBy">
        /// The requested by.
        /// </param>
        public void Create(Enrollment enrollment, Campus campus, UserDefStatusCode originalStatus, UserDefStatusCode newStatus, DateTime dateOfChange, string modifiedUser, DateTime effectiveDate, string caseNumber, string requestedBy)
        {
            this.Enrollment = enrollment;
            this.OriginalStatus = originalStatus;
            this.NewStatus = newStatus;
            this.ModifiedDate = dateOfChange;
            this.ModifiedUser = modifiedUser;
            this.DateOfChange = effectiveDate;
            this.CaseNumber = caseNumber;
            this.RequestedBy = requestedBy;
            this.Campus = campus;
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="campus">
        /// The campus.
        /// </param>
        /// <param name="originalStatus">
        /// The original status.
        /// </param>
        /// <param name="newStatus">
        /// The new status.
        /// </param>
        /// <param name="dateOfChange">
        /// The date of change.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="effectiveDate">
        /// The effective date.
        /// </param>
        /// <param name="caseNumber">
        /// The case number.
        /// </param>
        /// <param name="requestedBy">
        /// The requested by.
        /// </param>
        /// <param name="dropReasonId">
        /// The drop reason id.
        /// </param>
        /// <param name="lda">
        /// The LDA.
        /// </param>
        public void Create(Enrollment enrollment, Campus campus, UserDefStatusCode originalStatus, UserDefStatusCode newStatus, DateTime dateOfChange, string modifiedUser, DateTime effectiveDate, string caseNumber, string requestedBy, Guid? dropReasonId, DateTime? lda)
        {
            this.Enrollment = enrollment;
            this.OriginalStatus = originalStatus;
            this.NewStatus = newStatus;
            this.ModifiedDate = dateOfChange;
            this.ModifiedUser = modifiedUser;
            this.DateOfChange = effectiveDate;
            this.CaseNumber = caseNumber;
            this.RequestedBy = requestedBy;
            this.DropReasonId = dropReasonId;
            this.Lda = lda;
            this.Campus = campus;
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="campus">
        /// The campus.
        /// </param>
        /// <param name="originalStatus">
        /// The original status.
        /// </param>
        /// <param name="newStatus">
        /// The new status.
        /// </param>
        /// <param name="dateOfChange">
        /// The date of change.
        /// </param>
        /// <param name="modifiedUser">
        /// The modified user.
        /// </param>
        /// <param name="effectiveDate">
        /// The effective date.
        /// </param>
        /// <param name="caseNumber">
        /// The case number.
        /// </param>
        /// <param name="requestedBy">
        /// The requested by.
        /// </param>
        /// <param name="dropReasonId">
        /// The drop reason id.
        /// </param>
        /// <param name="lda">
        /// The LDA.
        /// </param>
        public virtual void Create(Guid id, Enrollment enrollment, Campus campus, UserDefStatusCode originalStatus, UserDefStatusCode newStatus, DateTime dateOfChange, string modifiedUser, DateTime effectiveDate, string caseNumber, string requestedBy, Guid? dropReasonId, DateTime? lda)
        {
            this.ID = id;
            this.Enrollment = enrollment;
            this.OriginalStatus = originalStatus;
            this.NewStatus = newStatus;
            this.ModifiedDate = dateOfChange;
            this.ModifiedUser = modifiedUser;
            this.DateOfChange = effectiveDate;
            this.CaseNumber = caseNumber;
            this.RequestedBy = requestedBy;
            this.DropReasonId = dropReasonId;
            this.Lda = lda;
            this.Campus = campus;
        }
    }
}
