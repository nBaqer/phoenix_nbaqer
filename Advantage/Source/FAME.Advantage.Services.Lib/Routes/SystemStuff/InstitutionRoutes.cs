﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionRoutes.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Routes.SystemStuff.InstitutionRoutes
// </copyright>
// <summary>
//   The institution routes.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Routes.SystemStuff
{
    using System.Web.Http;

    using Infrastructure.Routes;

    /// <summary>
    /// The institution routes.
    /// </summary>
    public class InstitutionRoutes : IRouteDefinition
    {
        /// <summary>
        /// The define routes.
        /// </summary>
        /// <param name="routes">
        /// The routes.
        /// </param>
        public void DefineRoutes(HttpRouteCollection routes)
        {
            //routes.MapHttpRoute(
            //    name: "System.Institution.Get",
            //    routeTemplate: "api/System/Institution/Get",
            //    defaults: new { controller = "Institution", action = "Get" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.Find",
            //    routeTemplate: "api/System/Institution/Find",
            //    defaults: new { controller = "Institution", action = "Find" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.FindPhone",
            //    routeTemplate: "api/System/Institution/FindPhone",
            //    defaults: new { controller = "Institution", action = "FindPhone" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.FindAddress",
            //    routeTemplate: "api/System/Institution/FindAddress",
            //    defaults: new { controller = "Institution", action = "FindAddress" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.FindContact",
            //    routeTemplate: "api/System/Institution/FindContact",
            //    defaults: new { controller = "Institution", action = "FindContact" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.PostPhone",
            //    routeTemplate: "api/System/Institution/PostPhone",
            //    defaults: new { controller = "Institution", action = "PostPhone" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.PutPhone",
            //    routeTemplate: "api/System/Institution/PutPhone",
            //    defaults: new { controller = "Institution", action = "PutPhone" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.DeletePhone",
            //    routeTemplate: "api/System/Institution/DeletePhone",
            //    defaults: new { controller = "Institution", action = "DeletePhone" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.PostAddress",
            //    routeTemplate: "api/System/Institution/PostAddress",
            //    defaults: new { controller = "Institution", action = "PostAddress" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.PutAddress",
            //    routeTemplate: "api/System/Institution/PutAddress",
            //    defaults: new { controller = "Institution", action = "PutAddress" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.DeleteAddress",
            //    routeTemplate: "api/System/Institution/DeleteAddress",
            //    defaults: new { controller = "Institution", action = "DeleteAddress" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.PostContact",
            //    routeTemplate: "api/System/Institution/PostContact",
            //    defaults: new { controller = "Institution", action = "PostContact" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.PutContact",
            //    routeTemplate: "api/System/Institution/PutContact",
            //    defaults: new { controller = "Institution", action = "PutContact" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.DeleteContact",
            //    routeTemplate: "api/System/Institution/DeleteContact",
            //    defaults: new { controller = "Institution", action = "DeleteContact" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.GetExistByImportType",
            //    routeTemplate: "api/System/Institution/GetExistByImportType",
            //    defaults: new { controller = "Institution", action = "GetExistByImportType" });
            //routes.MapHttpRoute(
            //    name: "System.Institution.GetExistByName",
            //    routeTemplate: "api/System/Institution/GetExistByName",
            //    defaults: new { controller = "Institution", action = "GetExistByName" });
        }
    }
}
