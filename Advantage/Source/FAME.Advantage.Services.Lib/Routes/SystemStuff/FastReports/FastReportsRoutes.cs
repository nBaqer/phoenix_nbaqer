﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FastReportsRoutes.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the FastReportsRoutes type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Routes.SystemStuff.FastReports
{
    using System.Web.Http;
    using Infrastructure.Routes;

    /// <summary>
    /// The fast reports routes.
    /// </summary>
    public class FastReportsRoutes : IRouteDefinition
    {
        /// <summary>
        /// <code>Router for directory api/SystemStuff/FastReports/</code>
        /// </summary>
        /// <param name="routes">
        /// Routes for controller in this directory
        /// </param>
        public void DefineRoutes(HttpRouteCollection routes)
        {
           //routes.MapHttpRoute(
           //     name: "ReportRouter",
           //     routeTemplate: "api/SystemStuff/FastReports/{controller}/{action}/{Id}",
           //     defaults: new { controller = "FastReport", Action = "Get", Id = RouteParameter.Optional });
        }
    }
}
