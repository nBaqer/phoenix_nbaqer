﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UniversalSearchRoutes.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Services.Lib.Routes.SystemStuff.UniversalSearchRoutes
// </copyright>
// <summary>
//   The universal search routes.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Routes.SystemStuff
{
    using System;
    using System.Web.Http;

    using Infrastructure.Routes;

    /// <summary>
    /// The universal search routes.
    /// </summary>
    public class UniversalSearchRoutes : IRouteDefinition
    {
        /// <summary>
        /// The define routes.
        /// </summary>
        /// <param name="routes">
        /// The routes.
        /// </param>
        public void DefineRoutes(HttpRouteCollection routes)
        {
            //routes.MapHttpRoute(
            //    name: "SystemModules.UniversalSearch.GetCampuses",
            //    routeTemplate: "api/SystemStuff/UniversalSearch/GetCampuses",
            //    defaults: new { controller = "UniversalSearch", action = "GetCampuses" });
            //routes.MapHttpRoute(
            //    name: "SystemModules.UniversalSearch.GetLeadStatus",
            //    routeTemplate: "api/SystemStuff/UniversalSearch/GetLeadStatus",
            //    defaults: new { controller = "UniversalSearch", action = "GetLeadStatus" });
            //routes.MapHttpRoute(
            //    name: "SystemModules.UniversalSearch.GetAdminReps",
            //    routeTemplate: "api/SystemStuff/UniversalSearch/GetAdminReps",
            //    defaults: new { controller = "UniversalSearch", action = "GetAdminReps" });
        }
    }
}
