﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MobileRoutes.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the MobileRoutes type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Routes.SystemStuff.Mobile
{
    using System.Diagnostics.CodeAnalysis;
    using System.Web.Http;
    using Infrastructure.Routes;

    /// <summary>
    /// The mobile routes.
    /// </summary>
    public class MobileRoutes : IRouteDefinition
    {
        /// <summary>
        /// Router General for path <code>api/SystemStuff/Mobile/</code>
        /// Please don't create another router for this directory 
        /// this should be adequate for all controller in the directory !!!
        /// </summary>
        /// <param name="routes">
        /// The routes collection.
        /// </param>
        [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1122:UseStringEmptyForEmptyStrings", Justification = "Reviewed. Suppression is OK here.")]
        public void DefineRoutes(HttpRouteCollection routes)
        {
            //routes.MapHttpRoute(
            //    name: "MobileRouter",
            //    routeTemplate: "api/SystemStuff/Mobile/{controller}/{action}/{Id}",
            //   defaults: new { controller = "", action = "Get", Id = RouteParameter.Optional });
        }
    }
}
