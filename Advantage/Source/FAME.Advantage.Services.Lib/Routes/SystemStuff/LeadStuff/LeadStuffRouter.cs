﻿using System.Web.Http;
using FAME.Advantage.Services.Lib.Infrastructure.Routes;

namespace FAME.Advantage.Services.Lib.Routes.SystemStuff.LeadStuff
{
    /// <summary>
    /// Router for related lead stuff in system for example MRU
    /// </summary>
    public class LeadStuffRouter : IRouteDefinition
    {
        /// <summary>
        /// Router General for path api/SystemStuff/LeadStuffRouter/
        /// Please don't create another router for this directory 
        /// this should be adequate for all controller in the directory !!!
        /// </summary>
        /// <param name="routes"></param>
        public void DefineRoutes(HttpRouteCollection routes)
        {

            //routes.MapHttpRoute(
            //  name: "LeadStuffRouter",
            //  routeTemplate: "api/SystemStuff/LeadStuffRouter/{controller}/{action}/{Id}",
            //  defaults: new { controller = "Mru", action = "Get", Id = RouteParameter.Optional });
        }

    }
}
