﻿using System;
using System.Web.Http;
using FAME.Advantage.Services.Lib.Infrastructure.Routes;

namespace FAME.Advantage.Services.Lib.Routes.SystemStuff
{
    public class SystemModulesRoutes : IRouteDefinition
    {
        public void DefineRoutes(HttpRouteCollection routes)
        {
            //routes.MapHttpRoute(
            //    name: "SystemModules",
            //    routeTemplate: "api/SystemStuff/Modules/{moduleId}",
            //    defaults: new {controller = "SystemModules", moduleId = RouteParameter.Optional});

            //routes.MapHttpRoute(
            //    name: "Maintenance",
            //    routeTemplate: "api/SystemStuff/Maintenance/{controller}/{action}/{Id}",
            //    defaults: new { Action = "Get", Id = RouteParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "SystemStatusChange.PostGetStatusHistory",
            //    routeTemplate: "api/SystemStuff/SystemStatusChange/PostGetStatusHistory",
            //    defaults: new { controller = "SystemStatusChange", Action = "PostGetStatusHistory", Id = RouteParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "SystemStatusChange.GetStatusCodesList",
            //    routeTemplate: "api/SystemStuff/SystemStatusChange/GetStatusCodesList",
            //    defaults: new { controller = "SystemStatusChange", Action = "GetStatusCodesList", Id = RouteParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "SystemStatusChange.GetRequestedBy",
            //    routeTemplate: "api/SystemStuff/SystemStatusChange/GetRequestedBy",
            //    defaults: new { controller = "SystemStatusChange", Action = "GetRequestedBy", Id = RouteParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "SystemStatusChange.GetLoaReasons",
            //    routeTemplate: "api/SystemStuff/SystemStatusChange/GetLoaReasons",
            //    defaults: new { controller = "SystemStatusChange", Action = "GetLoaReasons", Id = RouteParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "SystemStatusChange.GetDroppedReasons",
            //    routeTemplate: "api/SystemStuff/SystemStatusChange/GetDroppedReasons",
            //    defaults: new { controller = "SystemStatusChange", Action = "GetDroppedReasons", Id = RouteParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "SystemStatusChange.GetValidateStatusChange",
            //    routeTemplate: "api/SystemStuff/SystemStatusChange/GetValidateStatusChange",
            //    defaults: new { controller = "SystemStatusChange", Action = "GetValidateStatusChange", Id = RouteParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "SystemStatusChange.PostApplyStatusChange",
            //    routeTemplate: "api/SystemStuff/SystemStatusChange/PostApplyStatusChange",
            //    defaults: new { controller = "SystemStatusChange", Action = "PostApplyStatusChange", Id = RouteParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "SystemStatusChange.GetValidateAttendanceAndGrades",
            //    routeTemplate: "api/SystemStuff/SystemStatusChange/GetValidateAttendanceAndGrades",
            //    defaults: new { controller = "SystemStatusChange", Action = "GetValidateAttendanceAndGrades", Id = RouteParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "SystemStatusChange.GetValidateDelete",
            //    routeTemplate: "api/SystemStuff/SystemStatusChange/GetValidateDelete",
            //    defaults: new { controller = "SystemStatusChange", Action = "GetValidateDelete", Id = RouteParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "SystemStatusChange.PostDelete",
            //    routeTemplate: "api/SystemStuff/SystemStatusChange/PostDelete",
            //    defaults: new { controller = "SystemStatusChange", Action = "PostDelete", Id = RouteParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "SystemStatusChange.GetDeleteReasons",
            //    routeTemplate: "api/SystemStuff/SystemStatusChange/GetDeleteReasons",
            //    defaults: new { controller = "SystemStatusChange", Action = "GetDeleteReasons", Id = RouteParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "SystemStatusChange.GetValidateDeleteSequence",
            //    routeTemplate: "api/SystemStuff/SystemStatusChange/GetValidateDeleteSequence",
            //    defaults: new { controller = "SystemStatusChange", Action = "GetValidateDeleteSequence", Id = RouteParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "EchoTest",
            //    routeTemplate: "api/SystemStuff/Resources/{controller}/{action}/{Id}",
            //    defaults: new { controller = "EchoTest", action = "Get", Id = RouteParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "EchoTest",
            //    routeTemplate: "api/SystemStuff/{controller}/{action}/{Id}",
            //    defaults: new { controller = "EchoTest", action = "Get", Id = RouteParameter.Optional });
        }

    }
}
