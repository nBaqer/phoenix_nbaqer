﻿using System.Web.Http;
using FAME.Advantage.Services.Lib.Infrastructure.Routes;

namespace FAME.Advantage.Services.Lib.Routes.Widgets
{
    /// <summary>
    /// Route for all widgets Controllers.
    /// </summary>
    public class WidgetsRoutes : IRouteDefinition
    {
        public void DefineRoutes(HttpRouteCollection routes)
        {
            //routes.MapHttpRoute(
            //    name: "Widgets",
            //    routeTemplate: "api/Widgets/{controller}/{action}/{Id}",
            //    defaults: new { controller = "VoyantWidget", Action = "Get", Id = RouteParameter.Optional });
        }

    }

}
