﻿using System.Web.Http;
using FAME.Advantage.Services.Lib.Infrastructure.Routes;

namespace FAME.Advantage.Services.Lib.Routes.Leads
{
    public class LeadRequirementsRoute : IRouteDefinition
    {
        public void DefineRoutes(HttpRouteCollection routes)
        {
            
            ////routes.MapHttpRoute(
            ////    name: "LeadRequirements.GetRequirementsByLeadId",
            ////    routeTemplate: "api/Leads/{lead_Id}/Requirements/GetRequirementsByLeadId",
            ////    defaults: new { controller = "LeadRequirements", action = "GetRequirementsByLeadId" });


            ////routes.MapHttpRoute(
            ////    name: "LeadRequirements.GetOptionalRequirementsByLeadId",
            ////    routeTemplate: "api/Leads/{lead_Id}/Requirements/GetOptionalRequirementsByLeadId",
            ////    defaults: new { controller = "LeadRequirements", action = "GetOptionalRequirementsByLeadId" });

            //// routes.MapHttpRoute(
            ////    name: "LeadRequirements.GetReqGroupsByLeadId",
            ////    routeTemplate: "api/Leads/{lead_Id}/Requirements/GetReqGroupsByLeadId",
            ////    defaults: new { controller = "LeadRequirements", action = "GetReqGroupsByLeadId" });

             ////routes.MapHttpRoute(
             ////   name: "LeadRequirements.GetRequirementsByGroupId",
             ////   routeTemplate: "api/Leads/{grpId}/Requirements/GetRequirementsByGroupId",
             ////   defaults: new { controller = "LeadRequirements", action = "GetRequirementsByGroupId" });


             ////routes.MapHttpRoute(
             ////   name: "LeadRequirements.GetRequirementsDetailsByReqId",
             ////   routeTemplate: "api/Leads/{reqId}/Requirements/GetRequirementsDetailsByReqId",
             ////   defaults: new { controller = "LeadRequirements", action = "GetRequirementsDetailsByReqId" });

             ////routes.MapHttpRoute(
             ////    name: "LeadRequirements.GetRequirementsList",
             ////    routeTemplate: "api/Leads/{lead_Id}/Requirements/GetRequirementsList",
             ////    defaults: new { controller = "LeadRequirements", action = "GetRequirementsList" });

             ////routes.MapHttpRoute(
             ////    name: "LeadRequirements.PostRequirement",
             ////    routeTemplate: "api/Leads/Requirements/PostRequirement",
             ////    defaults: new { controller = "LeadRequirements", action = "PostRequirement" });


             ////routes.MapHttpRoute(
             ////    name: "LeadRequirements.PostDocumentFile",
             ////    routeTemplate: "api/Leads/Requirements/PostDocumentFile",
             ////    defaults: new { controller = "LeadRequirements", action = "PostDocumentFile" });

            ////routes.MapHttpRoute(
            ////     name: "LeadRequirements.DeleteRequirementHistoryRecord",
            ////     routeTemplate: "api/Leads/Requirements/DeleteRequirementHistoryRecord",
            ////     defaults: new { controller = "LeadRequirements", action = "DeleteRequirementHistoryRecord" });

            ////routes.MapHttpRoute(
            ////    name: "LeadRequirements.GetLeadTransactionReceipt",
            ////    routeTemplate: "api/Leads/Requirements/GetLeadTransactionReceipt",
            ////    defaults: new { controller = "LeadRequirements", action = "GetLeadTransactionReceipt" });

            //// routes.MapHttpRoute(
            ////    name: "LeadRequirements.VoidTransaction",
            ////    routeTemplate: "api/Leads/Requirements/VoidTransaction",
            ////    defaults: new { controller = "LeadRequirements", action = "VoidTransaction" });

            //// routes.MapHttpRoute(
            ////     name: "LeadRequirements.GetRequirementTypesMet",
            ////     routeTemplate: "api/Leads/{leadId}/Requirements/GetRequirementTypesMet",
            ////     defaults: new { controller = "LeadRequirements", action = "GetRequirementTypesMet" });

            ////routes.MapHttpRoute(
            ////    name: "LeadRequirements.GetHaveMetRequirement",
            ////    routeTemplate: "api/Leads/Requirements/GetHaveMetRequirement",
            ////    defaults: new { controller = "LeadRequirements", action = "GetHaveMetRequirement" });
        }
    }
}
