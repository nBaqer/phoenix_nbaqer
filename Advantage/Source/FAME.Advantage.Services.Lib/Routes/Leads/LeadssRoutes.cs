﻿using FAME.Advantage.Services.Lib.Infrastructure.Routes;
using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Routes.Leads
{
    public class LeadsRoutes : IRouteDefinition
    {
        public void DefineRoutes(HttpRouteCollection routes)
        {
            ////routes.MapHttpRoute(
            ////   name: "Leads.GetBySearchTerm",
            ////   routeTemplate: "api/LeadsSearch/GetBySearchTerm",
            ////   defaults: new { controller = "Leads", action = "GetBySearchTerm" });
            
            ////routes.MapHttpRoute(
            ////   name: "Leads.GetByUnassigned",
            ////   routeTemplate: "api/Lead/GetByUnassigned",
            ////   defaults: new { controller = "Leads", action = "GetByUnassigned" });

            ////routes.MapHttpRoute(
            ////   name: "Leads.GetCampusOfInterest",
            ////   routeTemplate: "api/Lead/GetCampusOfInterest",
            ////   defaults: new { controller = "Leads", action = "GetCampusOfInterest" });

            ////routes.MapHttpRoute(
            ////   name: "Leads.GetLeadCounts",
            ////   routeTemplate: "api/Lead/GetLeadCounts",
            ////   defaults: new { controller = "Leads", action = "GetLeadCounts" });

            //routes.MapHttpRoute(
            //   name: "Leads.UpdateAssignLeads",
            //   routeTemplate: "api/Lead/UpdateAssignLeads",
            //   defaults: new { controller = "Leads", action = "UpdateAssignLeads" });

            ////routes.MapHttpRoute(
            ////   name: "Leads.GetByReassign",
            ////   routeTemplate: "api/Lead/GetByReassign",
            ////   defaults: new { controller = "Leads", action = "GetByReassign" });

            ////routes.MapHttpRoute(
            ////   name: "Leads.GetByMru",
            ////   routeTemplate: "api/LeadSearch/GetByMru",
            ////   defaults: new { controller = "Leads", action = "GetByMru" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.GetById",
            ////  routeTemplate: "api/LeadSearch/GetById",
            ////  defaults: new { controller = "Leads", action = "GetById" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.GetLeadContactInfo",
            ////  routeTemplate: "api/Lead/GetLeadContactInfo",
            ////  defaults: new { controller = "Leads", action = "GetLeadContactInfo" });
           
            ////routes.MapHttpRoute(
            ////  name: "Leads.PostLeadPhoneContact",
            ////  routeTemplate: "api/Lead/PostLeadPhoneContact",
            ////  defaults: new { controller = "Leads", action = "PostLeadPhoneContact" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.DeleteLeadPhoneContact",
            ////  routeTemplate: "api/Lead/DeleteLeadPhoneContact",
            ////  defaults: new { controller = "Leads", action = "DeleteLeadPhoneContact" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.PostLeadEmailContact",
            ////  routeTemplate: "api/Lead/PostLeadEmailContact",
            ////  defaults: new { controller = "Leads", action = "PostLeadEmailContact" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.DeleteLeadEmailContact",
            ////  routeTemplate: "api/Lead/DeleteLeadEmailContact",
            ////  defaults: new { controller = "Leads", action = "DeleteLeadEmailContact" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.PostLeadAddressContact",
            ////  routeTemplate: "api/Lead/PostLeadAddressContact",
            ////  defaults: new { controller = "Leads", action = "PostLeadAddressContact" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.DeleteLeadAddressContact",
            ////  routeTemplate: "api/Lead/DeleteLeadAddressContact",
            ////  defaults: new { controller = "Leads", action = "DeleteLeadAddressContact" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.UpdateLeadComment",
            ////  routeTemplate: "api/Lead/UpdateLeadComment",
            ////  defaults: new { controller = "Leads", action = "UpdateLeadComment" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.GetLeadOtherContacts",
            ////  routeTemplate: "api/Lead/GetLeadOtherContacts",
            ////  defaults: new { controller = "LeadsOtherContacts", action = "GetLeadOtherContacts" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.PostLeadOtherContacts",
            ////  routeTemplate: "api/Lead/PostLeadOtherContacts",
            ////  defaults: new { controller = "LeadsOtherContacts", action = "PostLeadOtherContacts" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.PostLeadOtherContactsEmail",
            ////  routeTemplate: "api/Lead/PostLeadOtherContactsEmail",
            ////  defaults: new { controller = "LeadsOtherContacts", action = "PostLeadOtherContactsEmail" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.PostLeadOtherContactsPhone",
            ////  routeTemplate: "api/Lead/PostLeadOtherContactsPhone",
            ////  defaults: new { controller = "LeadsOtherContacts", action = "PostLeadOtherContactsPhone" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.PostLeadOtherContactsAddress",
            ////  routeTemplate: "api/Lead/PostLeadOtherContactsAddress",
            ////  defaults: new { controller = "LeadsOtherContacts", action = "PostLeadOtherContactsAddress" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.DeleteLeadOtherContacts",
            ////  routeTemplate: "api/Lead/DeleteLeadOtherContacts",
            ////  defaults: new { controller = "LeadsOtherContacts", action = "DeleteLeadOtherContacts" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.DeleteLeadOtherContactsEmail",
            ////  routeTemplate: "api/Lead/DeleteLeadOtherContactsEmail",
            ////  defaults: new { controller = "LeadsOtherContacts", action = "DeleteLeadOtherContactsEmail" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.DeleteLeadOtherContactsPhone",
            ////  routeTemplate: "api/Lead/DeleteLeadOtherContactsPhone",
            ////  defaults: new { controller = "LeadsOtherContacts", action = "DeleteLeadOtherContactsPhone" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.DeleteLeadOtherContactsAddress",
            ////  routeTemplate: "api/Lead/DeleteLeadOtherContactsAddress",
            ////  defaults: new { controller = "LeadsOtherContacts", action = "DeleteLeadOtherContactsAddress" });

            ////routes.MapHttpRoute(
            ////  name: "Leads.PostLeadOtherContactsComments",
            ////  routeTemplate: "api/Lead/PostLeadOtherContactsComments",
            ////  defaults: new { controller = "LeadsOtherContacts", action = "PostLeadOtherContactsComments" });

            ////routes.MapHttpRoute(
            ////    name: "Leads.LeadsEducation.Get", 
            ////    routeTemplate: "api/Lead/Education/Get", 
            ////    defaults: new { controller = "LeadEducation", action = "Get" });
            ////routes.MapHttpRoute(
            ////    name: "Leads.LeadsEducation.Find",
            ////    routeTemplate: "api/Lead/Education/Find",
            ////    defaults: new { controller = "LeadEducation", action = "Find" });
            ////routes.MapHttpRoute(
            ////    name: "Leads.LeadsEducation.Post",
            ////    routeTemplate: "api/Lead/Education/Post",
            ////    defaults: new { controller = "LeadEducation", action = "Post" });
            ////routes.MapHttpRoute(
            ////    name: "Leads.LeadsEducation.Put",
            ////    routeTemplate: "api/Lead/Education/Put",
            ////    defaults: new { controller = "LeadEducation", action = "Put" });
            ////routes.MapHttpRoute(
            ////    name: "Leads.LeadsEducation.Delete",
            ////    routeTemplate: "api/Lead/Education/Delete",
            ////    defaults: new { controller = "LeadEducation", action = "Delete" });
        }
    }
}
