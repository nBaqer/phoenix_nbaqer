﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadVendorRoutes.cs" company="FAME">
//   2014,2016
// </copyright>
// <summary>
//   Defines the WapiRoutes type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Routes.Leads
{
    using System.Web.Http;

    using FAME.Advantage.Services.Lib.Infrastructure.Routes;

    /// <summary>
    ///  The WAPI routes.
    /// </summary>
    public class LeadVendorRoutes : IRouteDefinition
    {
        /// <summary>
        ///  The define routes.
        /// </summary>
        /// <param name="routes">
        ///  The routes.
        /// </param>
        public void DefineRoutes(HttpRouteCollection routes)
        {
            //routes.MapHttpRoute(
            //    name: "LeadVendors",
            //    routeTemplate: "api/Lead/{controller}/{action}/{Id}",
            //    defaults: new { controller = "LeadVendor", Action = "Get", Id = RouteParameter.Optional });
        }
    }
}
