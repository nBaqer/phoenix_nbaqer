﻿using System.Web.Http;
using FAME.Advantage.Services.Lib.Infrastructure.Routes;

namespace FAME.Advantage.Services.Lib.Routes.User
{
    public class UsersRoutes : IRouteDefinition
    {
        public void DefineRoutes(HttpRouteCollection routes)
        {
            routes.MapHttpRoute(
                name: "Users.Get",
                routeTemplate: "api/User/Users",
                defaults: new { controller = "Users", action = "Get" });

        }
    }
}
