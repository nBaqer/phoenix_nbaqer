﻿using System.Web.Http;
using FAME.Advantage.Services.Lib.Infrastructure.Routes;

namespace FAME.Advantage.Services.Lib.Routes.Students.Enrollments
{
    public class EnrollmentTransactionRoutes : IRouteDefinition
    {
        public void DefineRoutes(HttpRouteCollection routes)
        {
            //routes.MapHttpRoute(
            // name: "Transactions.GetTransactionsSummary",
            // routeTemplate: "api/Students/Enrollments/GetTransactionsSummary",
            // defaults: new { controller = "Transaction", action = "GetTransactionsByPaymentPeriodsSummary" });
            
            //routes.MapHttpRoute(
            // name: "Transactions.GetTransactionsByPayPeriod",
            // routeTemplate: "api/Students/Enrollments/GetTransactionsByPayPeriod",
            // defaults: new { controller = "Transaction", action = "GetTransactionsByPaymentPeriods" });

            //routes.MapHttpRoute(
            //name: "Transactions.IsWidgetEnabled",
            //routeTemplate: "api/Students/Enrollments/IsPaymentPeriodWidgetEnabled",
            //defaults: new { controller = "Transaction", action = "IsPaymentPeriodWidgetEnabled" });

            //routes.MapHttpRoute(
            //name: "Transactions.GetProjectedPaymentPeriodCharges",
            //routeTemplate: "api/Students/Enrollments/GetProjectedPaymentPeriodCharges",
            //defaults: new { controller = "Transaction", action = "GetProjectedPaymentPeriodCharges" });

            //routes.MapHttpRoute(
            //name: "Transactions.GetProjectedPaymentPeriodSummary",
            //routeTemplate: "api/Students/Enrollments/GetProjectedPaymentPeriodSummary",
            //defaults: new { controller = "Transaction", action = "GetProjectedPaymentPeriodSummary" });
        }
    }
}
