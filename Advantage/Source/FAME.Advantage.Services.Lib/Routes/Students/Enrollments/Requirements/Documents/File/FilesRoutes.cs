﻿using FAME.Advantage.Services.Lib.Infrastructure.Routes;
using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Routes.Students.Enrollments.Requirements.Documents.File
{
    public class FilesRoutes : IRouteDefinition
    {
        public void DefineRoutes(HttpRouteCollection routes)
        {
            //routes.MapHttpRoute(
            //    name: "File.GetById",
            //    routeTemplate: "api/Students/Enrollments/Requirements/Documents/Files/{fileId}",
            //    defaults: new { controller = "Files", action = "GetAll" });

            //routes.MapHttpRoute(
            //    name: "File.GetContentsById",
            //    routeTemplate: "api/Students/Enrollments/Requirements/Documents/Files/{fileId}/Contents",
            //    defaults: new { controller = "Files", action = "GetContentsById" });

            //routes.MapHttpRoute(
            //    name: "File.GetByDocumentId",
            //    routeTemplate: "api/Students/Enrollments/Requirements/Documents/{documentId}/Files",
            //    defaults: new { controller = "Files", action = "GetByDocumentId" });

            //routes.MapHttpRoute(
            //    name: "File.Post",
            //    routeTemplate: "api/Students/Enrollments/Requirements/Documents/{documentId}/Files/Contents",
            //    defaults: new { controller = "Files", action = "Post" });

            //routes.MapHttpRoute(
            //    name: "File.Delete",
            //    routeTemplate: "api/Students/Enrollments/Requirements/Documents/File/{fileId}/Delete",
            //    defaults: new { controller = "Files", action = "Delete" });
        }
    }
}
