﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadDistinctComparator.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Give Lead equals if has the same First Name and LastName.
//   This comparer is for distinct
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Comparators
{
    using System.Collections.Generic;
    using System.Linq;

    using FAME.Advantage.Domain.Lead;

    /// <summary>
    /// Give Lead equals if has the same First Name and LastName.
    /// This comparer is for distinct
    /// </summary>
    public class LeadDistinctComparator : IEqualityComparer<Lead>
    {
        /// <summary>
        ///  The equals.
        /// </summary>
        /// <param name="x">
        ///  The x.
        /// </param>
        /// <param name="y">
        ///  The y.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Equals(Lead x, Lead y)
        {
            return (x.FirstName == y.FirstName) 
                && (x.LastName == y.LastName) 
                && (x.AddressList.SequenceEqual(y.AddressList) 
                && x.LeadPhoneList.SequenceEqual(y.LeadPhoneList) 
                && x.LeadEmailList.SequenceEqual(y.LeadEmailList));
        }

        /// <summary>
        /// The get hash code.
        /// </summary>
        /// <param name="x">
        ///  The x.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <remarks>
        /// If you won't create a valid GetHashCode based on values you compare on, LINQ won't work properly
        /// </remarks>
        public int GetHashCode(Lead x)
        {
            int hash = x.FirstName.GetHashCode() + x.LastName.GetHashCode() + x.GetHashCode()  
                + x.LeadPhoneList.GetHashCode() + x.LeadEmailList.GetHashCode();
            return hash;
        }
    }
}