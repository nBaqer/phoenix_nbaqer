﻿using System;
using System.Collections.Generic;
using FAME.Advantage.Domain.Lead;

namespace FAME.Advantage.Services.Lib.Comparators
{
    /// <summary>
    /// Compare if the entity has equal the phone number
    /// </summary>
    public class PhoneComparator : IEqualityComparer<LeadPhone>
    {

        public bool Equals(LeadPhone x, LeadPhone y)
        {
            return (x.Phone == y.Phone);
        }
        // If you won't create a valid GetHashCode based on values you compare on, Linq won't work properly
        public int GetHashCode(LeadPhone x)
        {
            int hash = x.Phone.GetHashCode();
            return hash;
        }
    }
}
