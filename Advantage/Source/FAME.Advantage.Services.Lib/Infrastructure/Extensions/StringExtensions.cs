﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using NHibernate.Hql.Ast.ANTLR;

namespace FAME.Advantage.Services.Lib.Infrastructure.Extensions
{
    internal static class StringExtensions
    {
        public static string RemoveEscapeCharacters(this string input)
        {
            return string.IsNullOrWhiteSpace(input) ? string.Empty : input.Replace("\"", string.Empty);
        }

        public static string PutQuotesAround(this string input)
        {
            if (string.IsNullOrWhiteSpace(input)) throw new ArgumentException("input cannot be null or empty");

            return @"""" + input + @"""";
        }

        public static string GetLast(this string input, int numToGet )
        {
            if (string.IsNullOrWhiteSpace(input)) throw new ArgumentException("input cannot be null or empty");

            return input.Substring(input.Length - numToGet);
        }

        public static string IsNullGetEmpty(this string input)
        {
            if (string.IsNullOrEmpty(input))
                return "";

            return input;
        }

        public static string FormatPhoneNumber(this String input)
        {
            if (input.Length < 10) return input;

            return "(" + input.Substring(0, 3) + ") " +
                                     input.Substring(3, 3) + "-" +
                                     input.Substring(6, 4);
            
        }
    }
}
