﻿using System.Linq;
using System.Web.Http.Controllers;

namespace FAME.Advantage.Services.Lib.Infrastructure.Extensions
{
    internal static class HttpActionContextExtensions
    {
        public static object GetTheInputModel(this HttpActionContext actionContext)
        {
            return actionContext.ActionArguments.Values.SingleOrDefault(obj => obj.GetType().IsClass);
        }
    }
}
