﻿using System.Net.Http;

namespace FAME.Advantage.Services.Lib.Infrastructure.Extensions
{
    internal static class HttpContentExtensions
    {
        internal static void CopyHeadersTo(this HttpContent source, HttpContent target)
        {
            foreach (var header in source.Headers)
            {
                target.Headers.Add(header.Key, header.Value);
            } 
        }
    }
}
