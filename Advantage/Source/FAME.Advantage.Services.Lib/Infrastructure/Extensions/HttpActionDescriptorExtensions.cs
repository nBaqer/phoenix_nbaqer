﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace FAME.Advantage.Services.Lib.Infrastructure.Extensions
{
    public static class HttpActionDescriptorExtensions
    {
        public static bool IsAnonymousAccessAllowed(this HttpActionDescriptor httpActionDescriptor)
        {
            return httpActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
        }
    }
}
