﻿using System;
using System.Linq;

namespace FAME.Advantage.Services.Lib.Infrastructure.Extensions
{
    public static class IQueryableExtensions
    {
        public static IQueryable<T> Page<T>(this IQueryable<T> query, int pageNumber, int pageSize)
        {
            if (pageNumber <= 0) throw new ArgumentException("Minimum page number must be 1");
            if (pageSize < 5) throw new ArgumentException("Minimum page size is 5");
            if (pageSize > 100) throw new ArgumentException("Maximum page size is 100");

            var zeroBasedPageNumber = pageNumber - 1;
            var numberOfRecordsToSkip = zeroBasedPageNumber * pageSize;
            return query.Skip(numberOfRecordsToSkip).Take(pageSize);
        }
    }
}
