﻿using System;

namespace FAME.Advantage.Services.Lib.Infrastructure.Extensions
{
    internal static class GuidExtensions
    {
        internal static bool IsEmpty(this Guid input)
        {
            return input.Equals(Guid.Empty);
        }

        internal static bool IsEmpty(this Guid? input)
        {
            return !input.HasValue || input.Value.IsEmpty();
        }
    }
}
