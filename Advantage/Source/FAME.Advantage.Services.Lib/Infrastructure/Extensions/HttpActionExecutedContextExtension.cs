﻿using System.Web.Http.Filters;

namespace FAME.Advantage.Services.Lib.Infrastructure.Extensions
{
    internal static class HttpActionExecutedContextExtension
    {
        public static bool ThereIsAnExceptionInTheContext(this HttpActionExecutedContext actionExecutedContext)
        {
            return actionExecutedContext.Exception != null;
        }
    }
}
