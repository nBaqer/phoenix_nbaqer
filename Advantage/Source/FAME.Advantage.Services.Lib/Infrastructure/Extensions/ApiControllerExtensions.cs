﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Infrastructure.Extensions
{
    internal static class ApiControllerExtensions
    {
        internal static string GetUserFromHeader(this ApiController controller)
        {
            var username = controller.Request.Headers.GetValues(Globals.UsernameHeaderName).SingleOrDefault();
            if (String.IsNullOrWhiteSpace(username)) throw new ApplicationException("Username must be present in the HTTP Request header but is not found");

            return username;
        }

        internal static bool IsItInTheRequest<T>(this ApiController controller, Expression<Func<T, object>> expression)
        {
            var inputModelPropertyName = GetPropertyNameIn(expression);
            var requestValues = GetRequestValues(controller.Request);

            return requestValues.ContainsKey(inputModelPropertyName);
        }

        private static string GetPropertyNameIn<T>(Expression<Func<T, object>> expression)
        {
            var body = expression.Body as MemberExpression;
            if (body != null) return body.Member.Name;

            var ubody = (UnaryExpression)expression.Body;
            body = ubody.Operand as MemberExpression;

            if (body == null) throw new NullReferenceException("body is null");

            return body.Member.Name;
        }

        private static Dictionary<string, string> GetRequestValues(HttpRequestMessage request)
        {
            if (HttpContext.Current.Items[Globals.RequestValues] == null)
            {
                var requestValues = request.GetRequestJsonAsDictionary();
                HttpContext.Current.Items.Add(Globals.RequestValues, requestValues);
            }

            return (Dictionary<string, string>)HttpContext.Current.Items[Globals.RequestValues];
        }
    }
}
