﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;

namespace FAME.Advantage.Services.Lib.Infrastructure.Extensions
{
    internal static class HttpRequestMessageExtensions
    {
        public static bool IsItAnEditRequest(this HttpRequestMessage request)
        {
            var allowedMethods = new[] {"POST", "PUT", "DELETE"};

            return allowedMethods.Contains(request.Method.Method);
        }

        public static Dictionary<string, string> GetRequestJsonAsDictionary(this HttpRequestMessage request)
        {
            var requestBody = request.Content.ReadAsStreamAsync().Result;
            requestBody.Position = 0;
            var jsonContent = new StreamReader(requestBody).ReadToEnd();
            return JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonContent);
        }
    }
}
