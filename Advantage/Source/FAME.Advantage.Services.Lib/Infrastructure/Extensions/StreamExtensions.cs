﻿using System.IO;

namespace FAME.Advantage.Services.Lib.Infrastructure.Extensions
{
    public static class StreamExtensions
    {
        public static Stream MakeCopy(this Stream stream)
        {
            var copyOfStream = new MemoryStream();
            stream.CopyTo(copyOfStream);
            return copyOfStream;
        }

        public static void WriteNewLineAtTheEnd(this Stream stream)
        {
            var writer = new StreamWriter(stream);

            stream.Seek(0, SeekOrigin.End);
            writer.WriteLine();
            writer.Flush();
            stream.Position = 0;
        }
    }
}
