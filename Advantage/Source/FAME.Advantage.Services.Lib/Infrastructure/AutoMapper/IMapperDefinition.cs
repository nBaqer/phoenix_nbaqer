﻿namespace FAME.Advantage.Services.Lib.Infrastructure.AutoMapper
{
    using global::AutoMapper.Configuration;

    public interface IMapperDefinition
    {
        void CreateDefinition(MapperConfigurationExpression Mapper);
    }
}
