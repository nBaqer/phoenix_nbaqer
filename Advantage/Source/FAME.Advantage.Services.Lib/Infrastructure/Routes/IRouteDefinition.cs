﻿using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Infrastructure.Routes
{
    public interface IRouteDefinition
    {
        void DefineRoutes(HttpRouteCollection routes);
    }
}
