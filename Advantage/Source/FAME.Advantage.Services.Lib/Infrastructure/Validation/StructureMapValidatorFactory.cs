﻿using System;
using FluentValidation;
using StructureMap;

namespace FAME.Advantage.Services.Lib.Infrastructure.Validation
{
    public class StructureMapValidatorFactory : IValidatorFactory
    {
        public IValidator<T> GetValidator<T>()
        {
            throw new NotSupportedException("Generic implementation is not supported");
        }

        public IValidator GetValidator(Type type)
        {
            if (type == null) return null;

            var abstractValidatorType = typeof(AbstractValidator<>);
            var validatorForType = abstractValidatorType.MakeGenericType(type);

            var validator = Bootstrapping.Bootstrapper.Container.TryGetInstance(validatorForType);

            return validator as IValidator;
        }
    }
}
