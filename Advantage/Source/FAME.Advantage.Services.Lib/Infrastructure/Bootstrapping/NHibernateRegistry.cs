﻿using System.Web;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.MultiTenant.Persistence;
using FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate;
using FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate.DynamicConnectionProvider;
using FAME.Advantage.Domain.Persistence.Infrastructure.UoW;
using FAME.Advantage.Services.Lib.Infrastructure.Services;
using Microsoft.Practices.ServiceLocation;
using NHibernate;
using StructureMap;
using StructureMap.Configuration.DSL;
using StructureMap.Web; 

namespace FAME.Advantage.Services.Lib.Infrastructure.Bootstrapping
{
    public class NHibernateRegistry : Registry
    {
        public NHibernateRegistry()
        {
            // Handle Setter Injection
            Policies.SetAllProperties(s => s.OfType<IUnitOfWork>());

            // Persistence Infrastructure
            For<IRepository>().Use<NHibernateRepository>();
            For<IRepositoryWithTypedID<int>>().Use<NHibernateRepository<int>>();

            For<IUnitOfWork>()
                .HybridHttpOrThreadLocalScoped()
                .Use<UnitOfWork>();

            ForSingletonOf<ISessionFactory>()
                .Use(c => new AdvantageSessionFactoryConfig().CreateSessionFactory());

            ForSingletonOf<IStatelessSession>()
                .Use(c => new MultiTenantSessionFactoryConfig().CreateSessionFactory().OpenStatelessSession());

            // ... added JAGG
            //ForSingletonOf<IStatelessSession>()
            //   .Use(c => new AdvantageSessionFactoryConfig().CreateSessionFactory().OpenStatelessSession());


            For<ConnectionStringResolver>()
                .Use(() => new ConnectionStringResolver(GetTheTenantConnectionFromHttpHeaders()));
        }

        private static string GetTheTenantConnectionFromHttpHeaders()
        {
            var apiAuthenticationKeyLookupService =
                ServiceLocator.Current.GetInstance<ApiAuthenticationKeyLookupService>();

            var apiAuthenticationKey =  HttpContext.Current.Request.Headers[Globals.AuthenticationKeyHeaderName];

            return apiAuthenticationKeyLookupService.GetTenantConnectionStringForKey(apiAuthenticationKey);
        }
    }
}