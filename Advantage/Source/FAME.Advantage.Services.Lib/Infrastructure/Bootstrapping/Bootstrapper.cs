﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Bootstrapper.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   The boots trapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Infrastructure.Bootstrapping
{
    using System.Web.Http;

    using FAME.Advantage.Services.Lib.Infrastructure.AutoMapper;
    using FAME.Advantage.Services.Lib.Infrastructure.DepencyResolver;
    using FAME.Advantage.Services.Lib.Infrastructure.Extensions;
    using FAME.Advantage.Services.Lib.Infrastructure.Routes;
    using FluentValidation;
    using global::AutoMapper;
    using global::AutoMapper.Configuration;

    using Microsoft.Practices.ServiceLocation;
    using StructureMap;
    using WebApiContrib.IoC.StructureMap;
    using Domain.Infrastructure.Extensions;

    /// <summary>
    /// The boots trapper.
    /// </summary>
    public static class Bootstrapper
    {
        /// <summary>
        /// The inversion of container (IoC).
        /// </summary>
        private static Container container;

        /// <summary>
        /// Gets the container.
        /// </summary>
        public static Container Container
        {
            get
            {
                return container;
            }
        }

        /// <summary>
        ///  The boots trap.
        /// </summary>
        public static void Bootstrap()
        {
            InitializeLogging();

            InitializeStructureMap();

            InitializeServiceLocator();

            InitializeAutoMapper();

            SetWebApiIocContainer();

            RegisterRoutes();
        }

        /// <summary>
        ///  The initialize logging.
        /// </summary>
        private static void InitializeLogging()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        /// <summary>
        ///  The initialize structure map.
        /// </summary>
        private static void InitializeStructureMap()
        {
            container = new Container(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.ConnectImplementationsToTypesClosing(typeof(AbstractValidator<>));
                    scan.AddAllTypesOf<IMapperDefinition>();
                    scan.AddAllTypesOf<IRouteDefinition>();
                });

                x.AddRegistry<NHibernateRegistry>();
                x.AddRegistry<SiteRegistry>();
            });
        }

        /// <summary>
        /// The initialize service locator's.
        /// </summary>
        private static void InitializeServiceLocator()
        {
            ServiceLocator.SetLocatorProvider(container.GetInstance<IServiceLocator>);
        }

        /// <summary>
        /// The initialize auto mapper.
        /// </summary>
        private static void InitializeAutoMapper()
        {
            var configuration = new MapperConfigurationExpression();
            var mappingDefinitions = container.GetAllInstances<IMapperDefinition>();
            mappingDefinitions.Each(mappingDefinition => mappingDefinition.CreateDefinition(configuration));

            Mapper.Initialize(configuration);

            Mapper.AssertConfigurationIsValid();
        }

        /// <summary>
        /// The set web API IOC container.
        /// </summary>
        private static void SetWebApiIocContainer()
        {
            // var container = container.Container;
            GlobalConfiguration.Configuration.DependencyResolver = new StructureMapDependencyResolver(container);
        }

        /// <summary>
        /// The register routes.
        /// </summary>
        private static void RegisterRoutes()
        {
            // var routes = GlobalConfiguration.Configuration.Routes;

            // var routeDefinitions = container.GetAllInstances<IRouteDefinition>();
            // routeDefinitions.ForEach(routeDefinition => routeDefinition.DefineRoutes(routes));
        }
    }
}
