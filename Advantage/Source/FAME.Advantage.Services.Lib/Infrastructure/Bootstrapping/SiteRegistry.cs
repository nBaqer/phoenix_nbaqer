﻿using FAME.Advantage.Domain.Infrastructure.ServiceLocation;
using FAME.Advantage.Services.Lib.Infrastructure.Services;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.ServiceLocation;
using StructureMap.Configuration.DSL;
using System.Web;
using StructureMap; 

namespace FAME.Advantage.Services.Lib.Infrastructure.Bootstrapping
{
    public class SiteRegistry : Registry
    {
        public SiteRegistry()
        {
            RegisterServiceLocator();
            
            RegisterHttpContext();
            
            RegisterLogger();

            RegisterCache();
        }

        private void RegisterHttpContext()
        {
            For<HttpContextBase>().Use(() => new HttpContextWrapper(HttpContext.Current));
        }

        private void RegisterServiceLocator()
        {
            For<IServiceLocator>().Use<StructureMapServiceLocator>();
        }

        private void RegisterLogger()
        {
            For<ILog>().Use(LogManager.GetLogger(Globals.GlobalLoggerName));
        }

        private void RegisterCache()
        {
            ForSingletonOf<ApiAuthenticationKeyLookupService>().Use<ApiAuthenticationKeyLookupService>();

            ForSingletonOf<CacheManager>()
                .Use(CacheFactory.GetCacheManager());
        }
    }
}
