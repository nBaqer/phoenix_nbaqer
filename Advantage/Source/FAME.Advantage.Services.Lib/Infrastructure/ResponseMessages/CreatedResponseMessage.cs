﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;

namespace FAME.Advantage.Services.Lib.Infrastructure.ResponseMessages
{
    internal sealed class HttpCreatedResponseMessage : HttpResponseMessage
    {
        public HttpCreatedResponseMessage(Uri location, string etag)
            : base(HttpStatusCode.Created)
        {
            if (location == null) throw new ArgumentNullException("location");
            if (string.IsNullOrWhiteSpace(etag)) throw new ArgumentException("etag cannot be null or empty");

            Headers.Location = location;

            var quotedEtag = etag.PutQuotesAround();
            Headers.ETag = new EntityTagHeaderValue(quotedEtag);
        }
    }

    internal sealed class HttpCreatedResponseMessage<T> : HttpResponseMessage 
    {
        public HttpCreatedResponseMessage(T response, Uri location, string etag)
            : base(HttpStatusCode.Created)
        {
            if (response.Equals(default(T))) throw new ArgumentNullException("response");
            if (location == null) throw new ArgumentNullException("location");
            if (string.IsNullOrWhiteSpace(etag)) throw new ArgumentException("etag cannot be null or empty");

            Headers.Location = location;

            var quotedEtag = etag.PutQuotesAround();
            Headers.ETag = new EntityTagHeaderValue(quotedEtag);

            Content = new ObjectContent(typeof(T), response, new JsonMediaTypeFormatter());
        }
    }
}
