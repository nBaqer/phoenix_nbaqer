﻿using System;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using FAME.Advantage.Domain.Persistence.Infrastructure.UoW;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;
using NHibernate.Context;
using StructureMap;

namespace FAME.Advantage.Services.Lib.Infrastructure.ActionFilters
{
    public sealed class UnitOfWorkActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var unitOfWork = Bootstrapping.Bootstrapper.Container.GetInstance<IUnitOfWork>();
            var transactionalScope = unitOfWork.CreateTransactionalScope();

            HttpContext.Current.Items[TRANSACTIONAL_SCOPE_HTTP_ITEMS_KEY] = transactionalScope;

            var session = unitOfWork.CurrentSession;
            CurrentSessionContext.Bind(session);
            session.BeginTransaction();
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            var transactionalScope = (TransactionalUnitOfWorkScope)HttpContext.Current.Items[TRANSACTIONAL_SCOPE_HTTP_ITEMS_KEY];

            if (!actionExecutedContext.ThereIsAnExceptionInTheContext() && !transactionalScope.WasRolledBack)
            {
                try
                {
                    transactionalScope.Complete();
                }
                catch (Exception)
                {
                    transactionalScope.Dispose();
                    throw;
                }
                
            }

            transactionalScope.Dispose();
        }

        private const string TRANSACTIONAL_SCOPE_HTTP_ITEMS_KEY = "TransactionalScope";
    }
}
