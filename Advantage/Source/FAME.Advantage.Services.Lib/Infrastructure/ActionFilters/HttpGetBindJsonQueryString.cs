﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpGetBindJsonQueryString.cs" company="FAME Inc.">
//    FAME.Advantage.Services.Lib.Infrastructure.ActionFilters
// </copyright>
// <summary>
//   Defines the HttpGetBindJsonQueryString type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Infrastructure.ActionFilters
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Runtime.Serialization.Json;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The http get bind JSon query string.
    /// Action Filter to Bind a JSon Object passed as query string in a HttpGet call
    /// </summary>
    public class HttpGetBindJsonQueryString : System.Web.Http.Filters.ActionFilterAttribute
    {
        /// <summary>
        /// The Type
        /// </summary>
        private readonly Type type;

        /// <summary>
        /// The Query String Key
        /// </summary>
        private readonly string queryStringKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpGetBindJsonQueryString"/> class. 
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="queryStringKey">
        /// The query String Key.
        /// </param>
        public HttpGetBindJsonQueryString(Type type, string queryStringKey)
        {
            this.type = type;
            this.queryStringKey = queryStringKey;
        }

        /// <summary>
        /// The on action executing.
        /// </summary>
        /// <param name="actionContext">
        /// The action context.
        /// </param>
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var json = actionContext.Request.RequestUri.ParseQueryString()[this.queryStringKey];
           
               DataContractJsonSerializer ser = new DataContractJsonSerializer(this.type);
                        MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            actionContext.ActionArguments[this.queryStringKey] = ser.ReadObject(ms);
        }
    }
}
