﻿using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;
using FAME.Advantage.Services.Lib.Infrastructure.Services;
using Microsoft.Practices.ServiceLocation;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace FAME.Advantage.Services.Lib.Infrastructure.ActionFilters
{
    public class AuthorizeAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ActionDescriptor.IsAnonymousAccessAllowed()) return;
            
            var requestHeaders = actionContext.Request.Headers;

            if (ApiAuthKeyHeaderIsNotFound(requestHeaders) || ApiAuthKeyIsNotValid(requestHeaders))
            {
                throw new HttpUnauthorizedResponseException("Access Denied");
            }
        }

        private static bool ApiAuthKeyHeaderIsNotFound(HttpHeaders requestHeaders)
        {
            return !requestHeaders.Contains(Globals.AuthenticationKeyHeaderName);
        }

        private static bool ApiAuthKeyIsNotValid(HttpHeaders requestHeaders)
        {
            var apiAuthenticationKey = GetWebApiAuthenticationKeyFromHeader(requestHeaders);
            var apiAuthenticationKeyLookupService =
                ServiceLocator.Current.GetInstance<ApiAuthenticationKeyLookupService>();
            return !apiAuthenticationKeyLookupService.ContainsKey(apiAuthenticationKey);
        }

        private static string GetWebApiAuthenticationKeyFromHeader(HttpHeaders requestHeaders)
        {
            var apiAuthenticationKey = requestHeaders.GetValues(Globals.AuthenticationKeyHeaderName).Single();
            return apiAuthenticationKey;
        }
    }
}
