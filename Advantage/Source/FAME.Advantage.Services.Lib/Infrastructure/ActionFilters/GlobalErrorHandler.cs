﻿using FAME.Advantage.Services.Lib.Infrastructure.Extensions;
using log4net;
using System;
using System.Web.Http.Filters;
using Microsoft.Practices.ServiceLocation;

namespace FAME.Advantage.Services.Lib.Infrastructure.ActionFilters
{
    public sealed class GlobalErrorHandler : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.ThereIsAnExceptionInTheContext())
            {
                LogTheException(actionExecutedContext.Exception);
            }
        }

        private static void LogTheException(Exception exception)
        {
            var logger = ServiceLocator.Current.GetInstance<ILog>();
            logger.Error("Unhandled Exception: " + exception);
        }
    }
}
