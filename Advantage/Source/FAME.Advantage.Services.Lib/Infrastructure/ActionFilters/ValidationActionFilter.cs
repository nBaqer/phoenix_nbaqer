﻿using FAME.Advantage.Services.Lib.Infrastructure.Exceptions;
using FAME.Advantage.Services.Lib.Infrastructure.Validation;
using FluentValidation;
using FluentValidation.Results;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace FAME.Advantage.Services.Lib.Infrastructure.ActionFilters
{
    public sealed class ValidationActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var actionArgument = GetTheActionArgument(actionContext);
            if (actionArgument == null) return;

            var validator = GetValidatorForActionArgument(actionArgument);
            if (validator == null) return;

            var validationResult = validator.Validate(actionArgument);

            if (validationResult.IsValid) return;

            CreateBadRequestResponse(validationResult);
        }

        private static IValidator GetValidatorForActionArgument(object actionArgument)
        {
            var validatorFactory = new StructureMapValidatorFactory();
            return validatorFactory.GetValidator(actionArgument.GetType());
        }

        private static object GetTheActionArgument(HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;

            return actionArguments.Count() == 1 ? actionArguments.Single().Value : null;
        }

        private static void CreateBadRequestResponse(ValidationResult validationResult)
        {
            var validationErrors =
               validationResult.Errors.Select(e => new KeyValuePair<string, string>(e.PropertyName, e.ErrorMessage));

            throw new HttpBadRequestResponseException(JsonConvert.SerializeObject(validationErrors));
        }
    }
}