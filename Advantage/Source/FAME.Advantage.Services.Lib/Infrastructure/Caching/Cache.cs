﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;

namespace FAME.Advantage.Services.Lib.Infrastructure.Caching
{
    public interface ICache
    {
        object Get(string key);

        void Add(string key, object value);

        void Add(string key, object value, CacheItemPriority priority, IEnumerable<CacheExpirationInfo> expirations);

        void Remove(string key);
    }

    public class Cache : ICache
    {
        private readonly CacheManager _cacheManager;

        public Cache(CacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        public object Get(string key)
        {
            return _cacheManager.GetData(key);
        }

        public void Add(string key, object value)
        {
            _cacheManager.Add(key, value);
        }

        public void Add(string key, object value, CacheItemPriority priority, IEnumerable<CacheExpirationInfo> expirations)
        {
            var msPriority = (Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority)priority;
            _cacheManager.Add(key, value, msPriority, null, CreateExpirations(expirations));
        }

        public void Remove(string key)
        {
            _cacheManager.Remove(key);
        }

        private ICacheItemExpiration[] CreateExpirations(IEnumerable<CacheExpirationInfo> expirationConfigs)
        {
            var expirations = new List<ICacheItemExpiration>();

            foreach (CacheExpirationInfo config in expirationConfigs) {
                switch (config.Type) {
                    case "never":
                        expirations.Add(new NeverExpired());
                        break;

                    case "file":
                        if (!string.IsNullOrEmpty(config.Param)) {
                            expirations.Add(new FileDependency(config.Param));
                        }
                        break;

                    case "absolute":
                        TimeSpan absoluteSpan;
                        if (TimeSpan.TryParse(config.Param, out absoluteSpan)) {
                            expirations.Add(new AbsoluteTime(DateTime.Now.Add(absoluteSpan)));
                        }
                        break;

                    case "sliding":
                        TimeSpan slidingSpan;
                        if (TimeSpan.TryParse(config.Param, out slidingSpan)) {
                            expirations.Add(new SlidingTime(slidingSpan));
                        }
                        break;

                    case "extended":
                        expirations.Add(new ExtendedFormatTime(config.Param));
                        break;
                }
            }

            return (expirations.Count > 0) ? expirations.ToArray() : null;
        }
    }
}