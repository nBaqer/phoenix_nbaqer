﻿using System;

namespace FAME.Advantage.Services.Lib.Infrastructure.Caching
{
    /// <summary>
    /// Summary description for CacheLoadingException.
    /// </summary>
    public class CachingException : ApplicationException
    {
        public CachingException() { }

        public CachingException(string s) : base(s) { }

        public CachingException(string s, Exception e) : base(s, e) { }
    }
}