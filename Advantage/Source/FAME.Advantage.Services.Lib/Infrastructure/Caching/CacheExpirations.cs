﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FAME.Advantage.Services.Lib.Infrastructure.Caching
{
    public class CacheExpirations<T>
    {
        private readonly IList<CacheExpirationInfo> _expirationConfigs;
        private readonly CacheVariable<T> _cacheVariable;

        public CacheExpirations(CacheVariable<T> cacheVariable)
        {
            _cacheVariable = cacheVariable;
            _expirationConfigs = new List<CacheExpirationInfo>();
        }

        internal IEnumerable<CacheExpirationInfo> AsEnumerable()
        {
            return _expirationConfigs.AsEnumerable();
        }

        public void AddAbsolute(TimeSpan timeFromNow)
        {
            AddExpiration("absolute", timeFromNow.ToString());
        }

        public void AddSliding(TimeSpan slidingTime)
        {
            AddExpiration("sliding", slidingTime.ToString());
        }

        public void AddExtended(string timeFormat)
        {
            AddExpiration("extended", timeFormat);
        }

        public void AddFileDependency(string filePath)
        {
            AddExpiration("file", filePath);
        }

        private void AddExpiration(string type, string param)
        {
            if (_cacheVariable.Priority != CacheItemPriority.NotRemovable) {
                _expirationConfigs.Add(new CacheExpirationInfo(type, param));
            }
        }
    }
}