﻿using System;

namespace FAME.Advantage.Services.Lib.Infrastructure.Caching
{
    public interface ICacheInterceptor
    {
        void OnSuccess(string key, object value);
        object OnError(string key, Exception e);
    }
}