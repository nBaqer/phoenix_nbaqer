﻿using System.Web.Security;
using FAME.Advantage.Messages.MultiTenant;

namespace FAME.Advantage.Services.Lib.Infrastructure.Services
{
    public interface IMembershipService
    {
        bool UserIsValid(CredentialsModel credentials);
        MembershipUser GetUser(string userName);
    }

    public class MembershipService : IMembershipService
    {
        public bool UserIsValid(CredentialsModel credentials)
        {
            return Membership.ValidateUser(credentials.UserName, credentials.Password);
        }

        public MembershipUser GetUser(string userName)
        {
            return Membership.GetUser(userName);
        }
    }
}
