﻿using System;
using System.IO;
using System.Linq;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;

namespace FAME.Advantage.Services.Lib.Infrastructure.Services
{
    internal class FileUploadValidationService
    {
        private readonly string[] _supportedExtensions = { ".gif", ".jpg", ".png", ".bmp", ".txt", ".doc", ".docx", ".csv", ".xls", ".xlsx", ".pdf" };
        private const int MaxFileSizeInBytes = 10000000;

        private readonly FileInfo _fileInfo;
        private readonly byte[] _fileBytes;

        internal FileUploadValidationService(string fileName, byte[] fileBytes)
        {
            if (string.IsNullOrEmpty(fileName)) throw new ArgumentException("fileName cannot be null or empty");
            if (fileBytes == null) throw new ArgumentNullException("fileBytes");

            _fileInfo = new FileInfo(fileName.RemoveEscapeCharacters());
            _fileBytes = fileBytes;
        }

        internal void ValidateFileForUpload()
        {
            if (IsFileEmpty()) throw new FileUploadValidatorException("File is empty");
            if (IsFileSizeExceedingMax()) throw new FileUploadValidatorException("File size exceeds the maximum size");
            if (IsFileOfUnupportedExtension()) throw new FileUploadValidatorException(string.Format("File type '{0}' is not supported", _fileInfo.Extension));
        }

        private bool IsFileEmpty()
        {
            return _fileBytes.Length == 0;
        }

        private bool IsFileSizeExceedingMax()
        {
            return _fileBytes.Length > MaxFileSizeInBytes;
        }

        private bool IsFileOfUnupportedExtension()
        {
            var fileExtension = _fileInfo.Extension;
            return !_supportedExtensions.Contains(fileExtension);
        }
    }

    internal class FileUploadValidatorException : Exception
    {
        public FileUploadValidatorException(string validationMessage) : base(validationMessage)
        {
        }
    }
}
