﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApiAuthenticationKeyLookupService.cs" company="FAME">
//   2013 - 2017
// </copyright>
// <summary>
//   Defines the ApiAuthenticationKeyLookupService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Infrastructure.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FAME.Advantage.Domain.MultiTenant;
    using FAME.Advantage.Services.Lib.Infrastructure.Caching;

    using NHibernate;

    /// <summary>
    /// The API authentication key lookup service.
    /// </summary>
    public class ApiAuthenticationKeyLookupService
    {
        /// <summary>
        /// The _stateless session.
        /// </summary>
        private readonly IStatelessSession statelessSession;

        /// <summary>
        /// The _cache factory.
        /// </summary>
        private readonly ICacheVariableFactory cacheFactory;

        /// <summary>
        /// The API authentication keys.
        /// </summary>
        internal readonly CacheVariable<IEnumerable<ApiAuthenticationKey>> _apiAuthenticationKeys;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiAuthenticationKeyLookupService"/> class.
        /// </summary>
        /// <param name="statelessSession">
        /// The stateless session.
        /// </param>
        /// <param name="cacheFactory">
        /// The cache factory.
        /// </param>
        public ApiAuthenticationKeyLookupService(IStatelessSession statelessSession, ICacheVariableFactory cacheFactory)
        {
            this.statelessSession = statelessSession;
            this.cacheFactory = cacheFactory;
            _apiAuthenticationKeys = this.GetCachedApiAuthenticationKeys();
        }

        /// <summary>
        /// The contains key.
        /// </summary>
        /// <param name="apiAuthenticationKey">
        /// The API authentication key.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public virtual bool ContainsKey(string apiAuthenticationKey)
        {
            var result = this._apiAuthenticationKeys.Value.Any(k => k.Key.Equals(apiAuthenticationKey, StringComparison.InvariantCultureIgnoreCase));
            return result;
        }

        /// <summary>
        /// The get tenant connection string for key.
        /// </summary>
        /// <param name="apiAuthenticationKey">
        /// The API authentication key.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetTenantConnectionStringForKey(string apiAuthenticationKey)
        {
            if (!this.ContainsKey(apiAuthenticationKey))
            {
                throw new ApplicationException("The requested key was not found");
            }

            return
                _apiAuthenticationKeys.Value.Single(x => x.Key.Equals(apiAuthenticationKey))
                    .Tenant.GetConnectionString();
        }

        /// <summary>
        /// The get cached API authentication keys.
        /// </summary>
        /// <returns>
        /// The <see cref="CacheVariable"/>.
        /// </returns>
        internal virtual CacheVariable<IEnumerable<ApiAuthenticationKey>> GetCachedApiAuthenticationKeys()
        {
            return this.cacheFactory.Build<IEnumerable<ApiAuthenticationKey>>()
                .WithKey(typeof(ApiAuthenticationKey).FullName)
                .WithPriority(CacheItemPriority.High)
                .InitializeWith(this.EagerLoadAuthenticationKeys)
                .AddSlidingExpiration(TimeSpan.FromHours(1))
                .EagerFetch()
                .Create();
        }

        /// <summary>
        /// The eager load authentication keys.
        /// Eager load because stateless sessions do not support lazy loading
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        internal virtual IEnumerable<ApiAuthenticationKey> EagerLoadAuthenticationKeys()
        {
            return this.statelessSession
                .QueryOver<ApiAuthenticationKey>()
                .Fetch(x => x.Tenant)
                .Eager
                .Fetch(x => x.Tenant.Keys)
                .Eager
                .List()
                .Distinct();
        }
    }
}
