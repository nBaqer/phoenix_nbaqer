﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using FAME.Advantage.Services.Lib.Infrastructure.Extensions;

namespace FAME.Advantage.Services.Lib.Infrastructure.Services
{
    internal class UploadifyService
    {
        private readonly HttpRequestMessage _request;
        private readonly HttpContent _filedataPart;

        internal UploadifyService(HttpRequestMessage request)
        {
            if (!request.Content.IsMimeMultipartContent()) throw new Exception("Multipart content was not found");

            _request = request;
            var multiparts = GetMultipartContentsFromRequestStream();
            _filedataPart = GetTheFiledataPart(multiparts);
        }

        internal string GetFileName()
        {
            var fileName = _filedataPart.Headers.ContentDisposition.FileName;
            return fileName;
        }

        internal byte[] GetFileBytes()
        {
            var fileContents = _filedataPart.ReadAsByteArrayAsync().Result;
            return fileContents;
        }

        private IEnumerable<HttpContent> GetMultipartContentsFromRequestStream()
        {
            var requestContent = _request.Content;

            var requestStream = requestContent.ReadAsStreamAsync().Result;
            var copyOfRequestStream = requestStream.MakeCopy();
            copyOfRequestStream.WriteNewLineAtTheEnd();

            var streamContent = new StreamContent(copyOfRequestStream);
            requestContent.CopyHeadersTo(streamContent);

            var contents = Task.Factory
                .StartNew(() => streamContent.ReadAsMultipartAsync(new MultipartMemoryStreamProvider()).Result.Contents,
                    CancellationToken.None,
                    TaskCreationOptions.LongRunning, // guarantees separate thread
                    TaskScheduler.Default).Result;

            return contents;
        }

        private static HttpContent GetTheFiledataPart(IEnumerable<HttpContent> contents)
        {
            var filedataPart = contents.SingleOrDefault(content => content.Headers.ContentDisposition.Name.Equals(@"""Filedata"""));

            if (filedataPart == null) throw new Exception("Multipart of name 'Filedata' was not found");

            return filedataPart;
        }
    }
}
