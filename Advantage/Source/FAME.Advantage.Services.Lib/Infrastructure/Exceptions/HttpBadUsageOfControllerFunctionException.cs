﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Infrastructure.Exceptions
{
    /// <summary>
    /// Use this in programming bad usage of functions.
    /// </summary>
    public class HttpBadUsageOfControllerFunctionException: HttpResponseException 
    {
        /// <summary>
        /// Basic Constructor
        /// </summary>
        public HttpBadUsageOfControllerFunctionException() : this(string.Empty)
        {
        }

        /// <summary>
        /// Return a message
        /// </summary>
        /// <param name="message">The message to be visualized</param>
        public HttpBadUsageOfControllerFunctionException(string message)
            : base(new HttpResponseMessage(HttpStatusCode.BadRequest) {Content = new StringContent(message)})
        {
        }
    }
}
