﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpInformativeException.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Use this to send a informative message to client
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Infrastructure.Exceptions
{
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    /// <summary>
    /// Use this to send a informative message to client
    /// </summary>
    public sealed class HttpInformativeException : HttpResponseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HttpInformativeException"/> class. 
        /// </summary>
        public HttpInformativeException()
            : this(string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpInformativeException"/> class. 
        /// </summary>
        /// <param name="message">
        /// The HTTP response to return to the client.
        /// </param>
        public HttpInformativeException(string message)
            : base(new HttpResponseMessage(HttpStatusCode.NotAcceptable)
            {
                Content = new StringContent(message)
            })
        {
        }
    }
}
