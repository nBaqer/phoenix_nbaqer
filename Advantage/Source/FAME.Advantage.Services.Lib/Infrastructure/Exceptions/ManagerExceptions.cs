﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ManagerExceptions.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Classified the exception to return to server
//   If ApplicationException happen a HttpInformativeException is returned to server
//   (in this case a warning message is show in client)
//   If Exception is raised a HttpBadRequestResponseException is returned to server
//   (in this case a error message is show in client)
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Infrastructure.Exceptions
{
    using System;
    using System.Web.Http;

    /// <summary>
    /// Classified the exception to return to server
    /// If ApplicationException happen a HttpInformativeException is returned to server
    /// (in this case a warning message is show in client)
    /// If Exception is raised a HttpBadRequestResponseException is returned to server
    /// (in this case a error message is show in client)
    /// </summary>
    public static class ManagerExceptions
    {
        /// <summary>
        /// Send informative or Bad Request exception to client
        /// based in the type of exception 
        /// </summary>
        /// <param name="ex">
        /// The ex.
        /// </param>
        /// <returns>
        /// The <see cref="HttpResponseException"/>.
        /// The exception selected
        /// </returns>
        public static HttpResponseException ManageResponseExceptions(Exception ex)
        {
            if (ex.GetType() == typeof(ApplicationException))
            {
                return new HttpInformativeException(ex.Message);
            }

            var responseError = string.Format("{0}<p> {1} </p>", Environment.NewLine, ex.Message);
            var exception = ex;
            while (exception.InnerException != null)
            {
                  responseError += string.Format("{0}<p> {1} </p>", Environment.NewLine, exception.InnerException.Message);
                  exception = exception.InnerException;
            }
           
            return new HttpBadRequestResponseException("Service Return: " + responseError);
        }
    }
}
