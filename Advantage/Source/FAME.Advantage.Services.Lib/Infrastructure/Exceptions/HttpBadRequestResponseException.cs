﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpBadRequestResponseException.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Use this when a error occurred in the service layer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Services.Lib.Infrastructure.Exceptions
{
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    /// <summary>
    /// Use this when a error occurred in the service layer
    /// </summary>
    public sealed class HttpBadRequestResponseException : HttpResponseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HttpBadRequestResponseException"/> class. 
        /// Basic constructor
        /// </summary>
        public HttpBadRequestResponseException()
            : this(string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpBadRequestResponseException"/> class. 
        /// Specific Constructor
        /// </summary>
        /// <param name="message">
        /// The message to be show
        /// </param>
        public HttpBadRequestResponseException(string message)
            : base(new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(message) })
        {
        }
    }
}
