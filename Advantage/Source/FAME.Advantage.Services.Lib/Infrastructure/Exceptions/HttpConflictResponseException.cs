﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Infrastructure.Exceptions
{
    public sealed class HttpConflictResponseException : HttpResponseException
    {
        public HttpConflictResponseException()
            : this(string.Empty)
        {
        }

        public HttpConflictResponseException(string message)
            : base(new HttpResponseMessage(HttpStatusCode.PreconditionFailed) {Content = new StringContent(message)})
        {
        }
    }
}
