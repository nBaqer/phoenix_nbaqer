﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Infrastructure.Exceptions
{
    public sealed class HttpNotFoundResponseException : HttpResponseException
    {
        public HttpNotFoundResponseException()
            : this(string.Empty)
        {
        }

        public HttpNotFoundResponseException(string message)
            : base(new HttpResponseMessage(HttpStatusCode.NotFound) {Content = new StringContent(message)})
        {
        }
    }
}
