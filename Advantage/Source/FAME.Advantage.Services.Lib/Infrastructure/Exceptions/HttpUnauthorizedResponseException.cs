﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FAME.Advantage.Services.Lib.Infrastructure.Exceptions
{
    public sealed class HttpUnauthorizedResponseException  : HttpResponseException {
           public HttpUnauthorizedResponseException() : this(string.Empty)
        {
        }

           public HttpUnauthorizedResponseException(string message)
            : base(new HttpResponseMessage(HttpStatusCode.Unauthorized) {Content = new StringContent(message)})
        {
        }
    }
}
