﻿
namespace FAME.Advantage.Services.Lib.Infrastructure
{
    internal  static class Globals
    {
        internal const string AuthenticationKeyHeaderName = "AuthKey";
        internal const string UsernameHeaderName = "Username";
        internal const string GlobalLoggerName = "FAMEAdvantageServices_Logger";
        internal const string RequestValues = "REQUEST_VALUES";
    }
}
