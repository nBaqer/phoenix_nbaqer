﻿using System.Configuration;
using System.Data.SqlClient;
using FAME.Advantage.Messages.SystemStuff.Wapi;

namespace FAME.Advantage.Services.Lib.DataLayer.Wapi
{
    /// <summary>
    /// Mini Db. You can not access Tenant Db from N-Hibernate, then we need 
    /// this ado.net class to access it.
    /// </summary>
    public class WapiCompanyNameDb
    {
        public SqlConnection InsertCompanyExternalNameInTenant(WapiCatalogsOutputModel model, int tenantId)
        {
            var tenconn = ConfigurationManager.ConnectionStrings["TenantAuthDBConnection"].ToString();
            const string SQL =
                "INSERT INTO dbo.WAPITenantCompanySecret (ExternalCompanyCode, TenantId) VALUES (@NewName, @TenantId)";
            var teconex = new SqlConnection(tenconn);

            try
            {
                var tencomm = new SqlCommand(SQL, teconex);
                tencomm.Parameters.AddWithValue("@NewName", model.Code);
                tencomm.Parameters.AddWithValue("@TenantId", tenantId);
                teconex.Open();

                tencomm.ExecuteNonQuery();
            }
            finally
            {
                teconex.Close();
            }
            return teconex;
        }

        /// <summary>
        /// The get exists company name tenant.
        /// </summary>
        /// <param name="companyName">
        /// The company name.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// return 0 if not exists the name in WAPI Tenant Company Secret
        /// </returns>
        public int GetExistsCompanyNameTenant(string companyName)
        {
            var conn = ConfigurationManager.ConnectionStrings["TenantAuthDBConnection"].ToString();
            var conex = new SqlConnection(conn);
            const string QUERY = "SELECT COUNT(*) FROM [WAPITenantCompanySecret] WHERE ExternalCompanyCode = @CompanyName";
            var comm = new SqlCommand(QUERY, conex);
            comm.Parameters.AddWithValue("@CompanyName", companyName);
            conex.Open();
            int res;
            try
            {
                res = (int)comm.ExecuteScalar();
            }
            finally
            {
                conex.Close();
            }
            return res;
        }
        public int GetExistsCompanyAndTenantRow(string companyName, int tenantId)
        {
            var conn = ConfigurationManager.ConnectionStrings["TenantAuthDBConnection"].ToString();
            var conex = new SqlConnection(conn);
            const string QUERY =
                "SELECT COUNT(*) FROM [WAPITenantCompanySecret] WHERE ExternalCompanyCode = @CompanyName AND TenantId = @TenantId";
            var comm = new SqlCommand(QUERY, conex);
            comm.Parameters.AddWithValue("@CompanyName", companyName);
            comm.Parameters.AddWithValue("@TenantId", tenantId);
            conex.Open();
            int res;
            try
            {
                res = (int)comm.ExecuteScalar();
            }
            finally
            {
                conex.Close();
            }
            return res;
        }

        public int DeleteCompanyTenantRow(string companyName, int tenantId)
        {
            var conn = ConfigurationManager.ConnectionStrings["TenantAuthDBConnection"].ToString();
            var conex = new SqlConnection(conn);
            const string QUERY =
                "DELETE FROM [WAPITenantCompanySecret] WHERE ExternalCompanyCode = @CompanyName AND TenantId = @TenantId";
            var comm = new SqlCommand(QUERY, conex);
            comm.Parameters.AddWithValue("@CompanyName", companyName);
            comm.Parameters.AddWithValue("@TenantId", tenantId);
            conex.Open();
            int res;
            try
            {
                res = comm.ExecuteNonQuery();
            }
            finally
            {
                conex.Close();
            }
            return res;
        }

    }
}
