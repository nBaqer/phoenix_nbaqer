﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FormData.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the FormData type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Services.Lib.Formatters.MultipartDataMediaFormatter.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// The form data.
    /// </summary>
    public class FormData
    {
        /// <summary>
        ///  The files.
        /// </summary>
        private List<ValueFile> files;

        /// <summary>
        /// The fields.
        /// </summary>
        private List<ValueString> fields;

        /// <summary>
        /// Gets or sets the files.
        /// </summary>
        public List<ValueFile> Files
        {
            get
            {
                if (this.files == null)
                {
                    this.files = new List<ValueFile>();
                }

                return this.files;
            }

            set
            {
                this.files = value;
            }
        }

        /// <summary>
        /// Gets or sets the fields
        /// </summary>
        public List<ValueString> Fields
        {
            get
            {
                if (this.fields == null)
                {
                    this.fields = new List<ValueString>();
                }

                return this.fields;
            }

            set
            {
                this.fields = value;
            }
        }

        /// <summary>
        /// The all keys.
        /// </summary>
        /// <returns>
        /// The keys
        /// </returns>
        public IEnumerable<string> AllKeys()
        {
            return this.Fields.Select(m => m.Name).Union(this.Files.Select(m => m.Name));
        }

        /// <summary>
        ///  The add.
        /// </summary>
        /// <param name="name">
        ///  The name.
        /// </param>
        /// <param name="value">
        ///  The value.
        /// </param>
        public void Add(string name, string value)
        {
            this.Fields.Add(new ValueString { Name = name, Value = value });
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public void Add(string name, HttpFile value)
        {
            this.Files.Add(new ValueFile { Name = name, Value = value });
        }

        /// <summary>
        ///  The try get value.
        /// </summary>
        /// <param name="name">
        ///  The name.
        /// </param>
        /// <param name="value">
        ///  The value.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// if the value is got
        /// </returns>
        public bool TryGetValue(string name, out string value)
        {
            var field = this.Fields.FirstOrDefault(m => string.Equals(m.Name, name, StringComparison.CurrentCultureIgnoreCase));
            if (field != null)
            {
                value = field.Value;
                return true;
            }

            value = null;
            return false;
        }

        /// <summary>
        ///  The try get value.
        /// </summary>
        /// <param name="name">
        ///  The name.
        /// </param>
        /// <param name="value">
        ///  The value.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// if the value is got
        /// </returns>
        public bool TryGetValue(string name, out HttpFile value)
        {
            var field = this.Files.FirstOrDefault(m => string.Equals(m.Name, name, StringComparison.CurrentCultureIgnoreCase));
            if (field != null)
            {
                value = field.Value;
                return true;
            }

            value = null;
            return false;
        }

        /// <summary>
        ///  The value string class.
        /// </summary>
        public class ValueString
        {
            /// <summary>
            /// Gets or sets the name.
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Gets or sets the value.
            /// </summary>
            public string Value { get; set; }
        }

        /// <summary>
        ///  The value file class.
        /// </summary>
        public class ValueFile
        {
            /// <summary>
            /// Gets or sets the name.
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Gets or sets the value.
            /// </summary>
            public HttpFile Value { get; set; }
        }
    }
}
