Imports System.Data
'Imports FAME.AdvantageV1.BusinessRules
Imports FAME.AdvantageV1.DataAccess
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.Advantage.Common
Imports System.Web

Public Class PortalRegisterStudentsBR
    Public Function ChkStudSchedConflict(ByVal StuEnrollId As String, ByVal ClsSectId As String) As String
        Dim db As New MyPortalRegisterDB
        Dim ds As  DataSet
        Dim ds2 As  DataSet
        Dim ds3 As  DataSet
        Dim row As DataRow
        Dim prevClsSectId As Guid
        Dim prevClsSection As String
        Dim db1 As New ClassSectionDB
        Dim clsSectArrayList As  ArrayList
        Dim clsSectArrayList2 As  ArrayList
        Dim clsSectionId As Guid
        'Dim ClsSection As String
        Dim clsSectMeeting As ClsSectMeetingInfo
        Dim clsSectMeeting2 As ClsSectMeetingInfo
        Dim errStr As String = ""
        Dim startDate As String
        Dim endDate As String
        Dim strDate As String


        ' strDate = db.GetClsSectDates(ClsSectId)
        strDate = db.GetClsSectDates_sp(ClsSectId)

        Dim arrRange() As String
        arrRange = strDate.Split(",")
        startDate = arrRange(0)
        endDate = arrRange(1)

        'Get all distinct ClsSections that Student is registered for term   
        'ds = db.GetStdClsSects(StuEnrollId, ClsSectId)
        ds = db.GetStdClsSects_SP(StuEnrollId, ClsSectId)

        If Not (ds.Tables(0).Rows.Count > 0) Then
            'there is not conflict
            Return "" 'Exit Function
        Else
            For Each row In ds.Tables(0).Rows
                prevClsSectId = row("TestId")
                prevClsSection = row("ClsSection")
                'Get ClsSection Meetings for this ClsSection
                ' ds2 = db1.GetClsSectMtgs(PrevClsSectId)

                ds2 = db1.GetClsSectMtgs_Sp(prevClsSectId)
                clsSectArrayList = ConvertClsSectMtgsToArrList(ds2)

                'Get ClsSection Meetings for the ClsSection that Stud is registering for.
                clsSectionId = XmlConvert.ToGuid(ClsSectId)
                'ds3 = db1.GetClsSectMtgs(ClsSectionId)
                ds3 = db1.GetClsSectMtgs_Sp(clsSectionId)
                clsSectArrayList2 = ConvertClsSectMtgsToArrList(ds3)

                'Check if start and end date range of class sections clash. If they is no conflict, then no need to go ahead
                'else check for other conflicts such as room, instructor, start & end times. 
                If ((row("StartDate") = startDate And row("EndDate") = endDate) _
                            Or (row("StartDate") >= startDate And row("endDate") >= endDate And row("StartDate") <= endDate) _
                            Or (row("StartDate") <= startDate And row("EndDate") >= startDate And row("EndDate") <= endDate) _
                            Or (row("StartDate") >= startDate And row("EndDate") <= endDate) _
                            Or (row("StartDate") < startDate And row("EndDate") > endDate)) Then

                Else
                    Exit For
                End If

                'If Not ((row("StartDate") = startDate And row("EndDate") = endDate) Or (row("StartDate") >= startDate And row("endDate") >= endDate) Or (row("StartDate") <= startDate And row("EndDate") <= endDate) Or (row("StartDate") >= startDate And row("EndDate") <= endDate) Or (row("StartDate") < startDate And row("EndDate") > endDate)) Then
                'End If


                'Check for any conflicts in the ClsSectionMeetings
                For Each clsSectMeeting In clsSectArrayList2
                    If (IsObjectNotEmpty(clsSectMeeting)) Then
                        For Each clsSectMeeting2 In clsSectArrayList


                            'Check if Day is the same. If it is..then no conflict
                            If Not (clsSectMeeting.Day.Equals(clsSectMeeting2.Day)) Or (clsSectMeeting.ClsSectMtgId = clsSectMeeting2.ClsSectMtgId) Then
                                'No conflict
                                Exit For
                            Else
                                '******************************************************************************
                                'The following block of code was commented out by BN(02/09/06).The new code below
                                'this will encompass all conditions of schedule conflicts. #4426
                                '********************************************************************************

                                ''Check if the starttime or endtime is the same.If it is then
                                ''conflict has occured
                                'If (ClsSectMeeting.StartTime.Equals(ClsSectMeeting2.StartTime)) Then
                                '    'Conflict.
                                '    ErrStr = "Conflicting with the the start time of class section " + PrevClsSection + " that Student was previously registered for. " + PrevClsSection + " meets from " + ClsSectMeeting2.STimeDescrip + " to " + ClsSectMeeting2.ETimeDescrip
                                'ElseIf (ClsSectMeeting.StartTime.Equals(ClsSectMeeting2.EndTime)) Then
                                '    'Conflict
                                '    ErrStr = "Conflicting with the the end time of class section " + PrevClsSection + " that Student was previously registered for " + PrevClsSection + " meets from " + ClsSectMeeting2.STimeDescrip + " to " + ClsSectMeeting2.ETimeDescrip
                                'ElseIf (ClsSectMeeting2.STimeDescrip >= ClsSectMeeting.STimeDescrip And ClsSectMeeting2.ETimeDescrip <= ClsSectMeeting.ETimeDescrip) Then
                                '    'Check if the starttime or endtime is within the timeframe of 
                                '    'the first one.If it is then conflict has occured
                                '    ErrStr = "Conflicting with the the timing of class section" + PrevClsSection + "that Student was previously registered for" + PrevClsSection + "meets from" + ClsSectMeeting2.STimeDescrip + "to" + ClsSectMeeting2.ETimeDescrip

                                'End If
                                'If ((ClsSectMeeting.STimeDescrip = ClsSectMeeting2.STimeDescrip And ClsSectMeeting.ETimeDescrip = ClsSectMeeting2.ETimeDescrip) _
                                '    Or (ClsSectMeeting.STimeDescrip >= ClsSectMeeting2.STimeDescrip And ClsSectMeeting.ETimeDescrip >= ClsSectMeeting2.ETimeDescrip And ClsSectMeeting.STimeDescrip <= ClsSectMeeting2.ETimeDescrip) _
                                '    Or (ClsSectMeeting.STimeDescrip <= ClsSectMeeting2.STimeDescrip And ClsSectMeeting.ETimeDescrip >= ClsSectMeeting2.STimeDescrip And ClsSectMeeting.ETimeDescrip <= ClsSectMeeting2.ETimeDescrip) _
                                '    Or (ClsSectMeeting.STimeDescrip >= ClsSectMeeting2.STimeDescrip And ClsSectMeeting.ETimeDescrip <= ClsSectMeeting2.ETimeDescrip) _
                                '    Or (ClsSectMeeting.STimeDescrip < ClsSectMeeting2.STimeDescrip And ClsSectMeeting.ETimeDescrip > ClsSectMeeting2.ETimeDescrip)) Then
                                If Not (Date.Parse(clsSectMeeting2.ETimeDescrip) <= Date.Parse(clsSectMeeting.STimeDescrip) Or Date.Parse(clsSectMeeting2.STimeDescrip) >= Date.Parse(clsSectMeeting.ETimeDescrip)) Then

                                    errStr = "Conflicting with class section " + prevClsSection + " that Student was previously registered for. " + prevClsSection + " meets from " + clsSectMeeting2.STimeDescrip + " to " + clsSectMeeting2.ETimeDescrip

                                End If

                            End If
                        Next

                    End If

                Next


            Next
        End If
        Return errStr
    End Function
    Private Function IsObjectNotEmpty(ByVal ClsSectMeeting As ClsSectMeetingInfo) As Boolean

        Dim obj As Boolean = False

        If Left(ClsSectMeeting.Day.ToString, 6) <> "000000" Then
            obj = True
        End If
        If Left(ClsSectMeeting.StartTime.ToString, 6) <> "000000" Then
            obj = True
        End If '
        If Left(ClsSectMeeting.EndTime.ToString, 6) <> "000000" Then
            obj = True
        End If '
        Return obj
    End Function
    Public Function ConvertClsSectMtgsToArrList(ByVal ds As DataSet) As ArrayList

        'Dim count As Integer
        'Dim strControl As String
        Dim ClsSectArrayList As New ArrayList
        Dim dt As DataTable
        Dim row As DataRow
        dt = ds.Tables("ClsSectMtgs")
        If ds.Tables.Count > 1 Then
            'Error - it returned more than one record
        Else
            'Loop thru the controls, find the appropriate dropdown(Day/Room/TimeInterval)
            'and set the object properties based on the ddl's that are set.
            For Each row In dt.Rows
                Dim ClsSectMtgObject As New ClsSectMeetingInfo
                If Not IsDBNull(row("WorkDaysId")) Then ClsSectMtgObject.Day = row("WorkDaysId")
                ClsSectMtgObject.Room = row("RoomId")
                If Not IsDBNull(row("TimeIntervalId")) Then ClsSectMtgObject.StartTime = row("TimeIntervalId")
                If Not IsDBNull(row("EndIntervalId")) Then ClsSectMtgObject.EndTime = row("EndIntervalId")
                ClsSectMtgObject.ClsSectMtgId = row("ClsSectMeetingId")
                If Not IsDBNull(row("StartTime")) Then ClsSectMtgObject.STimeDescrip = row("StartTime")
                If Not IsDBNull(row("EndTime")) Then ClsSectMtgObject.ETimeDescrip = row("EndTime")
                ClsSectMtgObject.StartDate = row("StartDate")
                ClsSectMtgObject.EndDate = row("EndDate")
                'Add the object to the array list
                ClsSectArrayList.Add(ClsSectMtgObject)
            Next
        End If

        Return ClsSectArrayList
    End Function

    ''Modified by Saraswathi Lakshmanan on August 19 2009
    ''mantis case 16592: prerequisite logic for galen to use in Portal
    ''if the student is registerred in the prereqs and it belongs to different term, then allow the student to take the course.
    ''based on the optional parameter ByPassIfRegistered, the function will check if the prereq is registered and belongs to different term, If so it will allow to register the student in the course.
    ''This was modified to use in Portal application.

    Public Function HasStdMetPrereqs(ByVal StuEnrollId As String, ByVal ClsSectId As String, ByVal PrgVerId As String, ByVal Campusid As String, ByVal ignorePrereqs As String, Optional ByVal ByPassIfRegistered As Boolean = False) As Boolean
        Dim db As New MyPortalRegisterDB
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim dt As DataTable
        Dim row As DataRow
        Dim dr As DataRow
        Dim PrereqId As String
        Dim Result As Boolean
        Dim IsPass As Boolean
        'Dim StdAvailable As Boolean
        Dim PreCoreqClsSect As String

        If ignorePrereqs.ToLower = "true" Then
            Return True
            Exit Function
        End If

        'ds = db.GetCoursePrereqs(ClsSectId, PrgVerId, Campusid)
        ds = db.GetCoursePrereqs_Sp(ClsSectId, PrgVerId, Campusid)
        dt = ds.Tables("CoursePrereqs")
        If (ds.Tables(0).Rows.Count > 0) Then

            For Each row In dt.Rows 'for each prereq, check if student attempted and passed it. 
                PrereqId = row("PreCoReqId").ToString
                'Get Class Sections for each Prereq and then check if any of student has grade
                'for any of these class sections. (results table only saves TestId and not ReqId).
                'ds2 = db.GetPrereqClsSects(PrereqId, StuEnrollId)
                ds2 = db.GetPrereqClsSects_Sp(PrereqId, StuEnrollId)

                For Each dr In ds2.Tables(0).Rows
                    PreCoreqClsSect = dr("ClsSectionId").ToString
                    Result = HasStdAttemptedPrereq(StuEnrollId, PrereqId, PreCoreqClsSect) 'attmpt this clssection?
                    If Result = True Then 'If Std attempted Prereq clssection
                        'Interested in Student
                        IsPass = DoesStdHavePassingGrd(StuEnrollId, PrereqId, PrgVerId, PreCoreqClsSect)
                        If IsPass = True Then
                            'Interested in Student, check if std passed next prereq
                            Exit For
                        End If
                    End If
                Next

                'If student has not attempted and passed any one of the prereqs, 
                'we are not interested in the student. (exit outer 'for')
                If IsPass = True Then
                    Result = True
                Else
                    Result = False
                    Exit For
                End If
            Next
        Else
            'No Prereqs
            Result = True
        End If
        ''if result is False and bypassiFregistered is true then Check for the prereq if it is registered and in previous term
        '' 
        'there is no conflict
        'if the student is registerred in the prereqs and it belongs to different term, then allow the student to take the course.
        ''based on the optional parameter ByPassIfRegistered, the function will check if the prereq is registered and belongs to different term, If so it will allow to register the student in the course.
        ''This was modified to use in Portal application.
        If Result = False And ByPassIfRegistered = True Then
            For Each row In dt.Rows 'for each prereq, check if student attempted and passed it. 
                PrereqId = row("PreCoReqId").ToString
                Result = IsStudentRegisteredandIsCourseInDifferentTerm(StuEnrollId, PrereqId, ClsSectId, Campusid)
                If Result = False Then
                    Exit For
                End If
            Next
            Return Result
        End If
        Return Result
    End Function

    ''Function added by Saraswathi lakshmanan on August 20 2009
    ''For Advantage Portal to check for prerequisites and allow them to register for the course if the prereqs in the previous terms are registerred.
    Public Function IsStudentRegisteredandIsCourseInDifferentTerm(ByVal StuEnrollId As String, ByVal ReqId As String, ByVal ClsSectionId As String, ByVal CampusId As String) As Boolean
        Dim result As Boolean
        Dim db As New MyPortalRegisterDB
        result = db.IsStudentRegisteredandIsCourseInDifferentTerm(StuEnrollId, ReqId, ClsSectionId, CampusId)

        Return result
    End Function


    Public Function HasStdAttemptedPrereq(ByVal StuEnrollId As String, ByVal PrereqId As String, ByVal ClsSectId As String) As Boolean
        Dim db As New MyPortalRegisterDB
        'Dim dt As DataTable
        'Dim row As DataRow
        Dim attempted As Integer
        Dim MetPrereq As Boolean

        'attempted = db.HasStdAttemptedReq(StuEnrollId, ClsSectId, PrereqId)
        attempted = db.HasStdAttemptedReq_Sp(StuEnrollId, ClsSectId, PrereqId)
        If attempted >= 1 Then
            MetPrereq = True
        Else
            MetPrereq = False
        End If

        Return MetPrereq
    End Function
    Public Function DoesStdHavePassingGrd(ByVal StuEnrollId As String, ByVal ReqId As String, ByVal PrgVerId As String, ByVal ClsSectId As String, Optional ByVal campusid As String = "") As Boolean
        Dim db As New MyPortalRegisterDB
        Dim count As String
        'Dim dt As DataTable
        'Dim row As DataRow
        Dim str As String
        Dim sGrdSysDetailId As String
        Dim sStdGrade As String
        Dim passingGrades As String
        ''  Dim GrdToSearch As String
        Dim isPassGrd As Boolean
        Dim IsPass As Integer

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        'Get the grade that student received for this course
        If myAdvAppSettings.AppSettings("GradesFormat", campusid).ToLower = "numeric" Then
            isPassGrd = DoesStdHavePassingGrdForNumeric_sp(StuEnrollId, ReqId, PrgVerId, ClsSectId)
        Else
            If ReqId <> "" Then
                sStdGrade = db.GetStdGrade_Sp(StuEnrollId, ClsSectId, ReqId)
            Else
                sStdGrade = db.GetStdGrade_Sp(StuEnrollId, ClsSectId)
            End If

            If sStdGrade = "" Then 'This means that the student is registered but has no grade yet.(ex:just registered)
                isPassGrd = False
                Return isPassGrd  'Note: Early Return
            End If

            str = db.DoesPrgVerHaveGrdOverride_SP(PrgVerId, ReqId)

            Dim arrRange() As String
            arrRange = str.Split(",")
            sGrdSysDetailId = arrRange(0)
            count = arrRange(1)


            If Not count = "" Then 'i.e PrgVer has a grade override attached to it
                passingGrades = db.GetHigherGrades_Sp(sGrdSysDetailId)
                Dim arrGrade() As String = passingGrades.Split(",")
                Dim i As Integer
                isPassGrd = False
                For i = 0 To arrGrade.Length - 1
                    If (sStdGrade.Trim() = arrGrade(i).Trim()) Then
                        isPassGrd = True
                        Exit For
                    End If
                Next
            Else
                IsPass = db.IsPassingGrade_SP(Trim(sStdGrade), PrgVerId)
                If IsPass = 1 Then
                    isPassGrd = True
                Else
                    isPassGrd = False
                End If
            End If
        End If

        Return isPassGrd

    End Function
    Public Function DoesStdHavePassingGrdForNumeric(ByVal StuEnrollId As String, ByVal ReqId As String, ByVal PrgVerId As String, ByVal ClsSectId As String) As Boolean
        Dim dtWorkUnit As DataTable = (New StuTranscriptDB).GetWorkUnitResults(StuEnrollId, ReqId)
        Dim avgScore As Decimal
        Dim minPassScore As Decimal = (New StuProgressReportDB).GetMinPassingScore

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        If dtWorkUnit.Rows.Count > 0 Then
            Return IsPass(dtWorkUnit, StuEnrollId)
        Else
            avgScore = (New SAPCheckDB).GetCourseAverage(StuEnrollId, ClsSectId)
            If myAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                avgScore = Math.Round(avgScore)
            End If
            If avgScore < minPassScore Then
                Return False
            End If
        End If
        Return True

    End Function
    Public Function DoesStdHavePassingGrdForNumeric_sp(ByVal stuEnrollId As String, ByVal reqId As String, ByVal prgVerId As String, ByVal clsSectId As String) As Boolean
        Dim dtWorkUnit As DataTable = (New StuTranscriptDB).GetWorkUnitResults_SP(stuEnrollId, reqId)
        Dim avgScore As Decimal
        Dim minPassScore As Decimal = (New StuProgressReportDB).GetMinPassingScore_Sp

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        If dtWorkUnit.Rows.Count > 0 Then
            Return IsPass(dtWorkUnit, stuEnrollId)
        Else
            avgScore = (New SAPCheckDB).GetCourseAverage_Sp(stuEnrollId, clsSectId)
            If myAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                avgScore = Math.Round(avgScore)
            End If
            If avgScore < minPassScore Then
                Return False
            End If
        End If
        Return True

    End Function

    Private Function IsPass(ByVal dtWU As DataTable, ByVal stuEnrollId As String) As Boolean
        Dim wuSatisfied As Boolean = True
        Dim i As Integer
        Dim db As New StuProgressReportDB
        Dim minPassScore As Decimal
        Dim avgScore As Decimal
        minPassScore = db.GetMinPassingScore
        Dim sapDb As New SAPCheckDB

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Check the work units that have been atempted.
        'arrWU = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "' AND Score >= 0")
        If dtWU.Rows.Count > 0 Then

            For i = 0 To dtWU.Rows.Count - 1
                'If dtWU.Rows(i)("SysComponentTypeId") <> "500" And dtWU.Rows(i)("SysComponentTypeId") <> "503" Then
                If IsDBNull(dtWU.Rows(i)("score")) Then
                    If dtWU.Rows(i)("Required") = True Or dtWU.Rows(i)("MustPass") = True Then
                        wuSatisfied = False
                        'strCourse(cCount) = "(" & dtWU.Rows(i)("CourseCode").ToString.Trim() & ")" & dtWU.Rows(i)("CourseDescrip")
                        'cCount = cCount + 1
                        Return False
                        Exit For
                    End If
                ElseIf dtWU.Rows(i)("score") >= 0 Then
                    If IsDBNull(dtWU.Rows(i)("score")) Then
                        If dtWU.Rows(i)("Required") = True Or dtWU.Rows(i)("MustPass") = True Then
                            wuSatisfied = False
                            'strCourse(cCount) = "(" & dtWU.Rows(i)("CourseCode").ToString.Trim() & ")" & dtWU.Rows(i)("CourseDescrip")
                            'cCount = cCount + 1
                            Return False
                            Exit For
                            'Else
                            '    wuSatisfied = True 
                        End If
                    ElseIf dtWU.Rows(i)("score") < dtWU.Rows(i)("MinResult") Then
                        'First check if it is required

                        If dtWU.Rows(i)("Required") = True And dtWU.Rows(i)("MustPass") = True Then
                            wuSatisfied = False
                            'strCourse(cCount) = "(" & dtWU.Rows(i)("CourseCode").ToString.Trim() & ")" & dtWU.Rows(i)("CourseDescrip")
                            'cCount = cCount + 1
                            Return False
                            Exit For
                        End If
                    Else
                        avgScore = sapDb.GetCourseAverage(stuEnrollId, dtWU.Rows(i)("TermId").ToString, dtWU.Rows(i)("ReqId").ToString)
                        If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                            avgScore = Math.Round(avgScore)
                        End If
                        If avgScore <> 0 And avgScore < minPassScore Then
                            wuSatisfied = False
                            'strCourse(cCount) = "(" & dtWU.Rows(i)("CourseCode").ToString.Trim() & ")" & dtWU.Rows(i)("CourseDescrip")
                            'cCount = cCount + 1
                            Return False
                            Exit For
                        End If
                    End If
                End If
                'End If
            Next
        End If

        Return wuSatisfied




    End Function

    Public Function IsStudentPassed(ByVal StuEnrollId As String, ByVal ReqId As String, ByVal overRideGrade As String, ByVal prgVerId As String) As Boolean
        Dim db As New MyPortalRegisterDB
        Dim count As String = "1"
        Dim sGrdSysDetailId As String
        Dim sStdGrade As String
        Dim PassingGrades As String
        Dim IsPassGrd As Boolean
        Dim IsPass As Integer

        'Get the grade that student received for this course

        sStdGrade = db.GetStudentGrade(StuEnrollId, ReqId)


        If sStdGrade = "" Then 'This means that the student is registered but has no grade yet.(ex:just registered)
            IsPassGrd = False
            Return IsPassGrd  'Note: Early Return
        End If
        sGrdSysDetailId = overRideGrade
        'Check if program version has a grade override attached to it.

        If Not count = "" Then 'i.e PrgVer has a grade override attached to it
            PassingGrades = db.GetHigherGrades(sGrdSysDetailId)
            Dim arrGrade() As String = PassingGrades.Split(",")
            Dim i As Integer
            IsPassGrd = False
            For i = 0 To arrGrade.Length - 1
                If (sStdGrade.Trim() = arrGrade(i).Trim()) Then
                    IsPassGrd = True
                    Exit For
                End If
            Next
        Else
            IsPass = db.IsPassingGrade(Trim(sStdGrade), prgVerId)
            If IsPass = 1 Then
                IsPassGrd = True
            Else
                IsPassGrd = False
            End If
        End If
        Return IsPassGrd

    End Function
    Public Function GrdOverRide(ByVal PrgVerId As String, ByVal Reqid As String, ByVal sStdGrade As String) As Boolean
        Dim db As New MyPortalRegisterDB
        Dim count As String
        Dim str As String
        Dim sGrdSysDetailId As String
        Dim passingGrades As String
        Dim isPassGrd As Boolean
        Dim isPass As Integer

        str = db.DoesPrgVerHaveGrdOverride(PrgVerId, Reqid)

        Dim arrRange() As String
        arrRange = str.Split(",")
        sGrdSysDetailId = arrRange(0)
        count = arrRange(1)


        If Not count = "" Then 'i.e PrgVer has a grade override attached to it
            passingGrades = db.GetHigherGrades(sGrdSysDetailId)
            Dim arrGrade() As String = passingGrades.Split(",")
            Dim i As Integer
            isPassGrd = False
            For i = 0 To arrGrade.Length - 1
                If (sStdGrade.Trim() = arrGrade(i).Trim()) Then
                    isPassGrd = True
                    Exit For
                End If
            Next
        Else
            isPass = db.IsPassingGrade(Trim(sStdGrade), PrgVerId)
            If isPass = 1 Then
                isPassGrd = True
            Else
                isPassGrd = False
            End If
        End If

        Return isPassGrd
    End Function

    Public Function GrdOverRide_SP(ByVal PrgVerId As String, ByVal Reqid As String, ByVal sStdGrade As String) As Boolean
        Dim db As New MyPortalRegisterDB
        Dim count As String
        Dim str As String
        Dim sGrdSysDetailId As String
        Dim PassingGrades As String
        Dim IsPassGrd As Boolean
        Dim IsPass As Integer

        str = db.DoesPrgVerHaveGrdOverride(PrgVerId, Reqid)

        Dim arrRange() As String
        arrRange = str.Split(",")
        sGrdSysDetailId = arrRange(0)
        count = arrRange(1)


        If Not count = "" Then 'i.e PrgVer has a grade override attached to it
            PassingGrades = db.GetHigherGrades(sGrdSysDetailId)
            Dim arrGrade() As String = PassingGrades.Split(",")
            Dim i As Integer
            IsPassGrd = False
            For i = 0 To arrGrade.Length - 1
                If (sStdGrade.Trim() = arrGrade(i).Trim()) Then
                    IsPassGrd = True
                    Exit For
                End If
            Next
        Else
            IsPass = db.IsPassingGrade(Trim(sStdGrade), PrgVerId)
            If IsPass = 1 Then
                IsPassGrd = True
            Else
                IsPassGrd = False
            End If
        End If

        Return IsPassGrd
    End Function
    Public Function GetStdGrade(ByVal StuEnrollId As String, ByVal ClsSectId As String, ByVal ReqId As String) As String
        Dim db As New MyPortalRegisterDB
        db.GetStdGrade(StuEnrollId, ClsSectId, ReqId)
    End Function
    Public Function ChkStudSchedConflictInAllEnrollments(ByVal StuEnrollId As String, ByVal ClsSectId As String) As String
        Dim db As New MyPortalRegisterDB
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim ds3 As New DataSet
        Dim row As DataRow
        Dim PrevClsSectId As Guid
        Dim PrevClsSection As String
        Dim PrevCourseId As Guid
        Dim CourseId As Guid
        Dim CourseName As String
        Dim db1 As New ClassSectionDB
        Dim ClsSectArrayList As New ArrayList
        Dim ClsSectArrayList2 As New ArrayList
        Dim ClsSectionId As Guid
        'Dim ClsSection As String
        Dim ClsSectMeeting As ClsSectMeetingInfo
        Dim ClsSectMeeting2 As ClsSectMeetingInfo
        Dim ErrStr As String = ""

        'Get all distinct ClsSections that Student is registered for term   
        ds = db.GetStdClsSectsFrAllEnrollments(StuEnrollId, ClsSectId)

        If Not (ds.Tables(0).Rows.Count > 0) Then
            'there is not conflict
            Return "" 'Exit Function
        Else
            For Each row In ds.Tables(0).Rows
                PrevClsSectId = row("TestId")
                PrevClsSection = row("ClsSection")
                PrevCourseId = row("ReqId")
                CourseId = row("CourseId")
                CourseName = row("CourseDescrip")
                If (PrevCourseId = CourseId) Then
                    'db1.DeleteStdToClsSect(PrevClsSectId.ToString, StuEnrollId)
                    ErrStr = PrevClsSection & "-" & CourseName & " Cannot be Registered"
                Else
                    'Get ClsSection Meetings for this ClsSection
                    ds2 = db1.GetClsSectMtgs(PrevClsSectId)
                    ClsSectArrayList = ConvertClsSectMtgsToArrList(ds2)

                    'Get ClsSection Meetings for the ClsSection that Stud is registering for.
                    ClsSectionId = XmlConvert.ToGuid(ClsSectId)
                    ds3 = db1.GetClsSectMtgs(ClsSectionId)
                    ClsSectArrayList2 = ConvertClsSectMtgsToArrList(ds3)

                    'Check for any conflicts in the ClsSectionMeetings
                    For Each ClsSectMeeting In ClsSectArrayList2
                        If (IsObjectNotEmpty(ClsSectMeeting)) Then
                            For Each ClsSectMeeting2 In ClsSectArrayList
                                If Not (CheckDates(ClsSectMeeting, ClsSectMeeting2)) Then

                                    'Check if Day is the same. If it is..then no conflict

                                    If Not (ClsSectMeeting.Day.Equals(ClsSectMeeting2.Day)) Then
                                        'No conflict
                                        Exit For
                                    Else
                                        'Check if the starttime or endtime is the same.If it is then
                                        'conflict has occured
                                        If (ClsSectMeeting.StartTime.Equals(ClsSectMeeting2.StartTime)) Then
                                            'Conflict.
                                            ErrStr = "Conflicting with the the start time of class section " + PrevClsSection + " that Student was previously registered for. " + PrevClsSection + " meets from " + ClsSectMeeting2.STimeDescrip + " to " + ClsSectMeeting2.ETimeDescrip
                                        ElseIf (ClsSectMeeting.StartTime.Equals(ClsSectMeeting2.StartTime)) Then
                                            'Conflict
                                            ErrStr = "Conflicting with the the end time of class section " + PrevClsSection + " that Student was previously registered for " + PrevClsSection + " meets from " + ClsSectMeeting2.STimeDescrip + " to " + ClsSectMeeting2.ETimeDescrip
                                        ElseIf (DateTime.Parse(ClsSectMeeting2.STimeDescrip) >= DateTime.Parse(ClsSectMeeting.STimeDescrip) And DateTime.Parse(ClsSectMeeting2.ETimeDescrip) <= DateTime.Parse(ClsSectMeeting.ETimeDescrip)) Then
                                            'Check if the starttime or endtime is within the timeframe of 
                                            'the first one.If it is then conflict has occured
                                            ErrStr = "Conflicting with the timing of class section " + PrevClsSection + ", that Student was previously registered for. " + PrevClsSection + " meets from " + ClsSectMeeting2.STimeDescrip + " to " + ClsSectMeeting2.ETimeDescrip

                                        End If

                                    End If
                                End If
                            Next

                        End If

                    Next

                End If
            Next
        End If
        Return ErrStr
    End Function

    Private Function CheckDates(ByVal ClsSectMeeting As ClsSectMeetingInfo, ByVal ClsSectMeeting2 As ClsSectMeetingInfo) As Boolean
        If (ClsSectMeeting.StartDate > ClsSectMeeting2.EndDate Or ClsSectMeeting2.StartDate > ClsSectMeeting.EndDate) Then
            Return True
        Else
            Return False
        End If
    End Function
    'Public Function IsPass(ByVal dtWU As DataTable, ByVal stuEnrollId As String) As Boolean
    '    Dim wuSatisfied As Boolean = True
    '    Dim i As Integer
    '    Dim strCourse(dtWU.Rows.Count) As String
    '    Dim db As New StuProgressReportDB
    '    Dim minPassScore As Decimal
    '    Dim avgScore As Decimal
    '    minPassScore = db.GetMinPassingScore
    '    Dim cCount As Integer = 0
    '    Dim sapDb As New SAPCheckDB

    '    'Check the work units that have been atempted.
    '    'arrWU = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "' AND Score >= 0")
    '    If dtWU.Rows.Count > 0 Then

    '        For i = 0 To dtWU.Rows.Count - 1
    '            'If dtWU.Rows(i)("SysComponentTypeId") <> "500" And dtWU.Rows(i)("SysComponentTypeId") <> "503" Then
    '            If IsDBNull(dtWU.Rows(i)("score")) Then
    '                If dtWU.Rows(i)("Required") = True Or dtWU.Rows(i)("MustPass") = True Then
    '                    wuSatisfied = False
    '                    strCourse(cCount) = "(" & dtWU.Rows(i)("CourseCode").ToString.Trim() & ")" & dtWU.Rows(i)("CourseDescrip")
    '                    cCount = cCount + 1
    '                    Return False
    '                    Exit For
    '                End If
    '            ElseIf dtWU.Rows(i)("score") >= 0 Then
    '                If IsDBNull(dtWU.Rows(i)("score")) Then
    '                    If dtWU.Rows(i)("Required") = True Or dtWU.Rows(i)("MustPass") = True Then
    '                        wuSatisfied = False
    '                        Return False
    '                        Exit For
    '                        'Else
    '                        '    wuSatisfied = True
    '                    End If
    '                ElseIf dtWU.Rows(i)("score") < dtWU.Rows(i)("PassingGrade") Then
    '                    'First check if it is required

    '                    If dtWU.Rows(i)("Required") = True And dtWU.Rows(i)("MustPass") = True Then
    '                        wuSatisfied = False

    '                        Return False
    '                        Exit For
    '                    End If
    '                Else
    '                    avgScore = sapDb.GetCourseAverage(stuEnrollId, dtWU.Rows(i)("TermId").ToString, dtWU.Rows(i)("ReqId").ToString)
    '                    If avgScore <> 0 And avgScore < minPassScore Then
    '                        wuSatisfied = False
    '                        Return False
    '                        Exit For
    '                    End If
    '                End If
    '            End If
    '            'End If
    '        Next
    '    End If

    '    Return wuSatisfied




    'End Function

    Public Function CCR_CheckIfStudentMetRequirements(ByVal StuEnrollid As String, _
                                                      ByVal ReqId As String) As Boolean
        Dim dtGetEffectiveDatesByCourse As DataTable = New DataTable
        Dim dtGetPreReqEffectiveDatesByCourse As DataTable = New DataTable
        Dim dtGetTopSpeedCategories As DataTable = New DataTable
        Dim dtGetMinCombination As DataTable = New DataTable
        Dim dtPreReqEffectiveDate As DateTime
        Dim boolStudentMetRequirements As Boolean = False
        Dim strpreReqId As String = ""
        Dim intMinPerCategory, intMaxPerCategory, intMinPerCombination, intMaxPerCombination As Integer
        '', intMentorProctored 
        Dim boolMentorProctored As Boolean = False
        Dim dtEffectiveDate As DateTime
        Dim boolRegister As Boolean = True
        Dim boolStudentMetMentorRequirements As Boolean = False

        Dim setDictationRules As New SetDictationTestRulesDB

        'This datatable dtGetEffectiveDatesByCourse gets the effective date, minimum/maximimum value per category,
        'minimum/maximum value per combination based on a course.This datatable will always contain one record.

        'Logic behind effective date : Rules (in Setup Rules Page) may be set up for any number of effective dates
        'and this datatable gets the effective date that is close to and earlier than the class start date.
        ' Example - Class A Starts on 01/09/2009 and rules have been effective since 9/10/2008, 01/03/2009, 01/10/2009
        ' which effective date will be considered? - 01/03/2009, as its close to class start date and is earlier than
        ' class start date.01/10/2009 is closer but later than the class start date.

        'The prerequisite validation should be performed based on the prerequisite course
        dtGetEffectiveDatesByCourse = setDictationRules.GetEffectivedatesbyCourse(ReqId).Tables(0)
        If Not dtGetEffectiveDatesByCourse Is Nothing AndAlso dtGetEffectiveDatesByCourse.Rows.Count = 1 Then
            strpreReqId = CType(dtGetEffectiveDatesByCourse.Rows(0)("PreReqId"), Guid).ToString
            dtEffectiveDate = dtGetEffectiveDatesByCourse.Rows(0)("EffectiveDate")
            dtGetPreReqEffectiveDatesByCourse = setDictationRules.GetEffectivedatesbyPrerequisiteCourse(strpreReqId).Tables(0)
            If Not dtGetPreReqEffectiveDatesByCourse Is Nothing AndAlso dtGetPreReqEffectiveDatesByCourse.Rows.Count = 1 Then
                dtPreReqEffectiveDate = dtGetPreReqEffectiveDatesByCourse.Rows(0)("EffectiveDate")
            End If

            Try
                'example: For a student to register in SH220, student should have met the requirements of SH200
                'Get the prerequisite condition defined for the coure (SH220)
                'For Each drGetEffectiveDatesByCourse As DataRow In dtGetEffectiveDatesByCourse.Rows 'Only one row - should return the effective date and min/max values
                If Not dtGetPreReqEffectiveDatesByCourse Is Nothing AndAlso dtGetPreReqEffectiveDatesByCourse.Rows.Count = 1 Then
                    'intMinPerCategory = drGetEffectiveDatesByCourse("MinPerCategory")
                    'intMaxPerCategory = drGetEffectiveDatesByCourse("MaxPerCategory")
                    'intMinPerCombination = drGetEffectiveDatesByCourse("MinPerCombination")
                    'intMaxPerCombination = drGetEffectiveDatesByCourse("MaxPerCombination")

                    'If prereq exist then the min/max/combo should read from prereq

                    dtPreReqEffectiveDate = dtGetPreReqEffectiveDatesByCourse.Rows(0)("EffectiveDate")
                    intMinPerCategory = dtGetPreReqEffectiveDatesByCourse.Rows(0)("MinPerCategory")
                    intMaxPerCategory = dtGetPreReqEffectiveDatesByCourse.Rows(0)("MaxPerCategory")
                    intMinPerCombination = dtGetPreReqEffectiveDatesByCourse.Rows(0)("MinPerCombination")
                    intMaxPerCombination = dtGetPreReqEffectiveDatesByCourse.Rows(0)("MaxPerCombination")
                    boolMentorProctored = dtGetPreReqEffectiveDatesByCourse.Rows(0)("MentorProctored")
                    'Try
                    '    strpreReqId = CType(dtGetPreReqEffectiveDatesByCourse.Rows(0)("PreReqId"), Guid).ToString
                    'Catch ex As System.Exception
                    'End Try
                Else 'If no prereqs then do not register
                    boolRegister = True
                End If




                'need to get the list of Top-Speed Testing category for the prerequisite course (SH200)
                Try
                    dtGetTopSpeedCategories = setDictationRules.GetTopSpeedTestCategory(dtPreReqEffectiveDate, strpreReqId).Tables(0)
                    dtGetMinCombination = setDictationRules.GetMinimumCombination(dtPreReqEffectiveDate, strpreReqId, StuEnrollid).Tables(0)
                Catch ex As System.Exception
                    boolRegister = True
                End Try


                'All these top-speed categories and minimum combination are for prerequisite courses
                If Not dtGetTopSpeedCategories Is Nothing Then
                    If dtGetTopSpeedCategories.Rows.Count >= 1 Then
                        For Each drGetTopSpeedCategories As DataRow In dtGetTopSpeedCategories.Rows ' Returns all components tied to course and effective date - JC, Literary, QA
                            Dim strGrdCompTypeId As String = CType(drGetTopSpeedCategories("GrdComponentTypeId"), Guid).ToString
                            If Not dtGetMinCombination Is Nothing AndAlso dtGetMinCombination.Rows.Count >= 1 Then
                                For Each drGetMinCombination As DataRow In dtGetMinCombination.Rows
                                    'If Student passed minimum required in each category
                                    If CType(drGetMinCombination("GrdComponentTypeId"), Guid).ToString = strGrdCompTypeId And _
                                        CType(drGetMinCombination("MinimumPerCombination"), Integer) >= intMinPerCategory And _
                                        dtGetMinCombination.Rows.Count >= intMaxPerCombination Then
                                        boolRegister = True
                                    Else
                                        boolRegister = False 'If Student Fails Even a Single componont exit Try
                                        Return False
                                        Exit Try
                                    End If
                                Next
                            Else
                                boolRegister = False 'If no score was posted for the student, its not possible to register
                                Return False
                                Exit Try
                            End If
                        Next
                    Else
                        'The course has a prereq but no min/max category or combination has been set.
                        Dim intPassedCourse As Integer = 0
                        intPassedCourse = (New SetDictationTestRulesDB).CCR_CheckIfCourseHasAPassingGrade(StuEnrollid, strpreReqId)
                        If intPassedCourse >= 1 Then
                            boolRegister = True
                        Else
                            boolRegister = False
                            Return False
                            Exit Try
                        End If
                    End If
                End If


                'This block of code checks to see if the SH Level Course has already been registered 4 times.
                'A student cannot repeat a course more than 4 times.
                Dim boolStudentMetCourseLimit As Boolean = True
                boolStudentMetCourseLimit = CheckIfStudentHasReachedTheSHLevelCourseRepeatLimit(StuEnrollid, ReqId)
                If boolStudentMetCourseLimit = False Then
                    Return False
                    Exit Try
                End If

                'This block of code gets the mentor proctored test requirement for the course and
                'also finds out how many test were mentor/proctored for the student
                'only if the course has mentor/proctored test as a prerequisite
                If boolMentorProctored = True Then
                    boolStudentMetMentorRequirements = CheckIfStudentPassedMentorProctoredTestRequirement(ReqId, _
                                                                                                          StuEnrollid, _
                                                                                                          dtEffectiveDate)
                    If boolStudentMetMentorRequirements = False Then
                        Return False
                        Exit Try
                    End If
                End If
                Return boolRegister
            Catch ex As System.Exception
                Return boolRegister
            End Try
        End If

        ' Old code
        'If Not dtGetEffectiveDatesByCourse Is Nothing AndAlso dtGetEffectiveDatesByCourse.Rows.Count = 1 Then
        '    Try
        '        For Each drGetEffectiveDatesByCourse As DataRow In dtGetEffectiveDatesByCourse.Rows 'Only one row - should return the effective date and min/max values
        '            intMinPerCategory = drGetEffectiveDatesByCourse("MinPerCategory")
        '            intMaxPerCategory = drGetEffectiveDatesByCourse("MaxPerCategory")
        '            intMinPerCombination = drGetEffectiveDatesByCourse("MinPerCombination")
        '            intMaxPerCombination = drGetEffectiveDatesByCourse("MaxPerCombination")
        '            dtEffectiveDate = drGetEffectiveDatesByCourse("EffectiveDate")
        '            boolMentorProctored = drGetEffectiveDatesByCourse("MentorProctored")
        '            strpreReqId = drGetEffectiveDatesByCourse("PreReqId")

        '            'need to get the list of Top-Speed Testing category for the prerequisite course.
        '            dtGetTopSpeedCategories = setDictationRules.GetTopSpeedTestCategory(dtEffectiveDate, strpreReqId).Tables(0)
        '            dtGetMinCombination = setDictationRules.GetMinimumCombination(dtEffectiveDate, strpreReqId, StuEnrollid).Tables(0)

        '            'This block of code checks to see if the SH Level Course has already been registered 4 times.
        '            'A student cannot repeat a course more than 4 times.

        '            'Reason:As per Jay, the Executive Director of CCR, "The global for all SH level will work; however, please make 
        '            'the limit four as opposed to six.  The reason is that the only students who can reach the limit of six are students 
        '            'who transfer into SH130 or higher.  If a student starts with SH100 with us, he or she cannot repeat SH courses six 
        '            'times due to the 11 semester limit; there are seven SH-level courses, so these seven plus six repeats puts the 
        '            'student into semester 13.
        '            Dim intNumberofTimesAStudentHasRepeatedSHLevelCourses As Integer = 0
        '            Dim intRepeatSHCoursesLimit As Integer = 0
        '            Try
        '                intRepeatSHCoursesLimit = SingletonAppSettings.AppSettings("CCR_RepeatSHCourses")
        '            Catch ex As System.Exception
        '                intRepeatSHCoursesLimit = 0
        '            End Try
        '            intNumberofTimesAStudentHasRepeatedSHLevelCourses = setDictationRules.GetNumberofTimesStudentRegisteredInSHCourses(StuEnrollid, ReqId, "SH")

        '            'If the max limit set by the course to retake a SH Course is 4 and if the student was already registered in the same course 4 times, then
        '            'student should not be allowed to register in the course again.
        '            If intRepeatSHCoursesLimit >= 1 AndAlso intNumberofTimesAStudentHasRepeatedSHLevelCourses = intRepeatSHCoursesLimit Then
        '                Return False
        '                Exit Try
        '            End If

        '            'Check if student met the prerequisites
        '            If Not dtGetTopSpeedCategories Is Nothing Then
        '                For Each drGetTopSpeedCategories As DataRow In dtGetTopSpeedCategories.Rows ' Returns all components tied to course and effective date - JC, Literary, QA
        '                    Dim strGrdCompTypeId As String = CType(drGetTopSpeedCategories("GrdComponentTypeId"), Guid).ToString
        '                    If Not dtGetMinCombination Is Nothing Then
        '                        For Each drGetMinCombination As DataRow In dtGetMinCombination.Rows
        '                            'If Student passed minimum required in each category
        '                            If CType(drGetMinCombination("GrdComponentTypeId"), Guid).ToString = strGrdCompTypeId And _
        '                                CType(drGetMinCombination("MinimumPerCombination"), Integer) >= intMinPerCategory And _
        '                                dtGetMinCombination.Rows.Count >= intMaxPerCombination Then
        '                                boolRegister = True
        '                            Else
        '                                boolRegister = False 'If Student Fails Even a Single componont exit Try
        '                                Return False
        '                                Exit Try
        '                            End If
        '                        Next
        '                    End If
        '                Next
        '            End If
        '        Next
        '    Catch ex As System.Exception
        '        Return boolRegister
        '    End Try
        'End If
        Return boolRegister
    End Function
    Private Function CheckIfStudentPassedMentorProctoredTestRequirement(ByVal reqId As String, _
                                                                        ByVal StuEnrollId As String, _
                                                                        ByVal dtEffectiveDate As DateTime) As Boolean

        Dim dtGetMentorRequirementsByStudent As DataTable = New DataTable()
        Dim dtGetMentorRules As DataTable = New DataTable()
        Dim intTestsMentoredForStudent As Integer = 0
        Dim intCountOfOrOperator As Integer = 0
        Dim intCountOfANDOperator As Integer = 0
        Dim intMentorRulesCount As Integer = 0
        Dim boolMatchFoundWithAndOperatorScore As Boolean = False

        dtGetMentorRequirementsByStudent = (New SetDictationTestRulesDB).GetMentorTestByStudent(StuEnrollId, dtEffectiveDate, reqId).Tables(0) '(New SetDictationTestRulesDB).GetMentorCombination(reqId, StuEnrollId).Tables(0)
        dtGetMentorRules = (New SetDictationTestRulesDB).GetMentorRules(dtEffectiveDate, reqId).Tables(0)
        If Not dtGetMentorRequirementsByStudent Is Nothing Then
            intTestsMentoredForStudent = dtGetMentorRequirementsByStudent.Rows.Count
        End If
        If Not dtGetMentorRules Is Nothing Then
            Dim dtMentorRulesOrOperator() As DataRow = dtGetMentorRules.Select("MentorOperator='Or'")
            Dim dtMentorRulesANDOperator() As DataRow = dtGetMentorRules.Select("MentorOperator='And'")

            ' The speed requirement for SAP can be one of this - Student needs to attain a speed level of 180L or 200J AND 225QA.
            ' To satisfy the above requirement, when student completes the course, student should have passed the test with a score
            'of 180 in Literature or 200 in Jury Charge AND 225 in QA.As AND operator is being used, student should take a score
            'of either 180 or 200 and should also have scored 225 in QA.If Student fails to score 225 or more than 225, student has not met SAP.

            'If student fails to meet the AND Operator requirement then boolMatchFoundWithAndOperatorScore will be False
            If dtMentorRulesANDOperator.Length >= 1 Then
                'Get the speed that is required 
                For Each drGetANDOperators As DataRow In dtGetMentorRules.Select("MentorOperator='And'")
                    For Each drGetScoresPosted As DataRow In dtGetMentorRequirementsByStudent.Rows
                        boolMatchFoundWithAndOperatorScore = False
                        'For example, as per Requirements, student needs to have a skill of 225 in QA
                        ' 1. check if student has passed any test in QA with a score of 225.
                        If drGetANDOperators("Speed") >= drGetScoresPosted("Speed") AndAlso _
                             drGetANDOperators("GrdComponentTypeId") = drGetScoresPosted("GrdComponentTypeId") Then
                            boolMatchFoundWithAndOperatorScore = True
                            Exit For
                        End If
                    Next
                Next
            End If

            If dtMentorRulesOrOperator.Length >= 1 Then
                intCountOfOrOperator = dtMentorRulesOrOperator.Length
                'Get the total count of "OR" operators and use the formula n - (n-1)
                'If there are 5 rows with Or Operator then the number of mentor proctored requirement will be 
                '5 - (5-1) = 5 - 4 = 1
                intCountOfOrOperator = intCountOfOrOperator - (intCountOfOrOperator - 1)
            End If
            If dtMentorRulesANDOperator.Length >= 1 Then
                'Get the total count of "AND" operators and use the formula n 
                intCountOfANDOperator = dtMentorRulesOrOperator.Length
            End If
            intMentorRulesCount = intCountOfOrOperator + intCountOfANDOperator
        End If

        'Validate if student has passed the required number of mentor proctored test
        If intTestsMentoredForStudent >= intMentorRulesCount Then
            Dim intSatisfied As Integer = 0
            For Each dr As DataRow In dtGetMentorRequirementsByStudent.Rows
                If dr("Speed") >= dr("MentorSpeed") Then
                    intSatisfied += 1
                End If
            Next
            If intSatisfied >= intMentorRulesCount Then 'If Student has score more than required in the mentor proctored test requirement
                If dtGetMentorRules.Select("Operator='And'").Length >= 1 Then
                    If boolMatchFoundWithAndOperatorScore = True Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    Return True
                End If
            Else
                Return False
            End If
        End If
    End Function
    Public Function CheckIfStudentPassedSpeedRequirementforSAP(ByVal SAPDetailId As String, _
                                                                                                                                    ByVal StuEnrollID As String) As Boolean

        Dim dtGetMentorRequirementsByStudent As DataTable = New DataTable()
        Dim dtGetMentorRules As DataTable = New DataTable()
        Dim intTestsMentoredForStudent As Integer = 0
        Dim intCountOfOrOperator As Integer = 0
        Dim intCountOfANDOperator As Integer = 0
        Dim intMentorRulesCount As Integer = 0
        Dim boolMatchFoundWithAndOperatorScore As Boolean = False

        dtGetMentorRequirementsByStudent = (New SetDictationTestRulesDB).GetSpeedPostedForSAP(SAPDetailId, StuEnrollID).Tables(0)
        dtGetMentorRules = (New SetDictationTestRulesDB).BindShortHandSkillRequirement(SAPDetailId).Tables(0)
        If Not dtGetMentorRequirementsByStudent Is Nothing Then
            intTestsMentoredForStudent = dtGetMentorRequirementsByStudent.Rows.Count
        End If
        If Not dtGetMentorRules Is Nothing Then
            Dim dtMentorRulesOrOperator() As DataRow = dtGetMentorRules.Select("Operator='Or'")
            Dim dtMentorRulesANDOperator() As DataRow = dtGetMentorRules.Select("Operator='And'")


            ' The speed requirement for SAP can be one of this - Student needs to attain a speed level of 180L or 200J AND 225QA.
            ' To satisfy the above requirement, when student completes the course, student should have passed the test with a score
            'of 180 in Literature or 200 in Jury Charge AND 225 in QA.As AND operator is being used, student should take a score
            'of either 180 or 200 and should also have scored 225 in QA.If Student fails to score 225 or more than 225, student has not met SAP.

            'If student fails to meet the AND Operator requirement then boolMatchFoundWithAndOperatorScore will be False
            If dtMentorRulesANDOperator.Length >= 1 Then
                'Get the speed that is required 
                For Each drGetANDOperators As DataRow In dtGetMentorRules.Select("Operator='And'")
                    For Each drGetScoresPosted As DataRow In dtGetMentorRequirementsByStudent.Rows
                        boolMatchFoundWithAndOperatorScore = False
                        'For example, as per SAP Requirements, student needs to have a skill of 225 in QA
                        ' 1. check if student has passed any test in QA with a score of 225.
                        If drGetANDOperators("Speed") >= drGetScoresPosted("Speed") AndAlso _
                             drGetANDOperators("GrdComponentTypeId") = drGetScoresPosted("GrdComponentTypeId") Then
                            boolMatchFoundWithAndOperatorScore = True
                            Exit For
                        End If
                    Next
                Next
            End If

            ' Requirement : Students are expected to pass the test with an accuracy limit.
            'Example: At the end of 12 months, student must pass short hand class test with an accuracy of 95%
            'We check if the accuracy posted for a student in Post Dictation Scores page exceeds the accuracy defined while creating SAP Policy (details).
            'If condition is not met, return false and a return value of false, indicates student has not met SAP.
            For Each drGetScoresPosted As DataRow In dtGetMentorRequirementsByStudent.Rows
                Dim decAccuracy As Decimal = 0
                Dim decRequiredAccuracy As Decimal = 0 'This is the accuracy set at SAP Policy Details
                If Not drGetScoresPosted("Accuracy") Is System.DBNull.Value Then decAccuracy = drGetScoresPosted("Accuracy")
                If Not drGetScoresPosted("RequiredAccuracy") Is System.DBNull.Value Then decRequiredAccuracy = drGetScoresPosted("RequiredAccuracy")
                If decAccuracy < decRequiredAccuracy Then
                    Return False
                End If
            Next


            If dtMentorRulesOrOperator.Length >= 1 Then
                intCountOfOrOperator = dtMentorRulesOrOperator.Length
                'Get the total count of "OR" operators and use the formula n - (n-1)
                'If there are 5 rows with Or Operator then the number of mentor proctored requirement will be 
                '5 - (5-1) = 5 - 4 = 1
                intCountOfOrOperator = intCountOfOrOperator - (intCountOfOrOperator - 1)
            End If
            If dtMentorRulesANDOperator.Length >= 1 Then
                'Get the total count of "AND" operators and use the formula n 
                intCountOfANDOperator = dtMentorRulesOrOperator.Length
            End If
            intMentorRulesCount = intCountOfOrOperator + intCountOfANDOperator
        End If

        'If no shorthand skill level has been defined in SAP Policy Details, consider that the student has passed speed requirement for SAP
        If intMentorRulesCount = 0 Then
            Return True
        End If

        'Check if any score has been posted against the skill levels
        'If the value is 0, indicates that no score was posted/student did not take or pass a single test, so student has not passed 
        'SAP speed requirements
        If intTestsMentoredForStudent = 0 Then
            Return False
        End If

        'Validate if student has passed the required number of mentor proctored test
        If intTestsMentoredForStudent >= intMentorRulesCount Then
            Dim intSatisfied As Integer = 0
            For Each dr As DataRow In dtGetMentorRequirementsByStudent.Rows
                If dr("Speed") >= dr("MentorSpeed") Then
                    intSatisfied += 1
                End If
            Next

            'If the value of boolMatchFoundWithAndOperatorScore is False, then it indicates the student has not scored the speed defined in SAP Policy Details with Operator "AND" 
            If intSatisfied >= intMentorRulesCount Then 'If Student has score more than required in the mentor proctored test requirement
                If dtGetMentorRules.Select("Operator='And'").Length >= 1 Then
                    If boolMatchFoundWithAndOperatorScore = True Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    Return True
                End If
            Else
                Return False
            End If
        End If
    End Function
    Private Function CheckIfStudentHasReachedTheSHLevelCourseRepeatLimit(ByVal StuEnrollId As String, ByVal ReqId As String) As Boolean
        'This block of code checks to see if the SH Level Course has already been registered 4 times.
        'A student cannot repeat a course more than 4 times.

        'Reason:As per Jay, the Executive Director of CCR, "The global for all SH level will work; however, please make 
        'the limit four as opposed to six.  The reason is that the only students who can reach the limit of six are students 
        'who transfer into SH130 or higher.  If a student starts with SH100 with us, he or she cannot repeat SH courses six 
        'times due to the 11 semester limit; there are seven SH-level courses, so these seven plus six repeats puts the 
        'student into semester 13.

        Dim intNumberofTimesAStudentHasRepeatedSHLevelCourses As Integer = 0
        Dim intRepeatSHCoursesLimit As Integer = 4
        Dim intRepeatSHSemesterLimit As Integer = 11

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            intRepeatSHCoursesLimit = MyAdvAppSettings.AppSettings("CCR_RepeatSHCourses")
        Catch ex As System.Exception
        End Try

        Try
            intRepeatSHSemesterLimit = MyAdvAppSettings.AppSettings("CCR_SHSemesterLimit")
        Catch ex As System.Exception
        End Try

        'intNumberofTimesAStudentHasRepeatedSHLevelCourses = (New SetDictationTestRulesDB).GetNumberofTimesStudentRegisteredInSHCourses(StuEnrollId, ReqId, "SH")
        ''If the max limit set by the course to retake a SH Course is 4 and if the student was already registered in the same course 4 times, then
        ''student should not be allowed to register in the course again.
        'If intRepeatSHCoursesLimit >= 1 AndAlso intNumberofTimesAStudentHasRepeatedSHLevelCourses = intRepeatSHCoursesLimit Then
        '    Return False 'Not Eligible for Registration
        'Else
        '    Return True 'Eligible for Registration
        'End If


        'In CCR the number of semesters is 11, and the number of SH Level Courses is 7, so only one of the SH Level courses can be repeated 4 times.
        'for example - Student is registered in SH1,SH2,SH3 and Student has retaken SH3 four times and now school tries to register the student in SH4
        'As the student has taken SH3 four times, student should not be allowed to register in any SH Level Courses.
        'Reason: Allowing the student to retake 

        Dim strValidForRegistration As String = ""
        Dim strOutput As String = ""
        Try
            strValidForRegistration = (New SetDictationTestRulesDB).RegistrationInSHCourses(StuEnrollId, "SH", intRepeatSHCoursesLimit, ReqId, strOutput, intRepeatSHSemesterLimit)
            If strValidForRegistration.ToString.ToLower.Trim = "true" Then
                Return True     ' Eligible for registration
            Else
                Return False   'Not Eligible for registration
            End If
        Catch ex As System.Exception
        End Try
    End Function
    Public Function IsCourseASHLevelCourse(ByVal ReqId As String, ByVal CourseCode As String) As Integer
        Return (New SetDictationTestRulesDB).IsCourseASHLevelCourse(ReqId, CourseCode)
    End Function
    Public Function GetCourseDescriptionByClass(ByVal ClsSectionId As String) As DataTable
        Return (New SetDictationTestRulesDB).GetCourseDescriptionByClass(ClsSectionId)
    End Function
End Class
