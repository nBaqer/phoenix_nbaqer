﻿Imports System
Imports System.Collections.Generic
Imports FAME.Advantage.Common.StudentRegistration.Lib
Imports FAME.AdvantageV1.DataAccess

Namespace StudentRegistration.Lib
    Public Interface IRegistration
        Sub RegisterStudent(students As IEnumerable(Of [Student]), user As [String])
    End Interface
    Public Class RegistrationService
        Implements IRegistration
        Public Sub RegisterStudent(students As IEnumerable(Of [Student]), user As [String]) Implements IRegistration.RegisterStudent
            For Each Student As [Student] In students
                For Each ClassSection As [Class] In Student.Classes
                    Dim db As New RegistrationProcessDB
                    db.RegisterStudentForCourse(ClassSection.newClassId, Student.StudentEnrollmentId, user)
                Next
            Next
        End Sub
    End Class

    Public Interface IArchive
        Sub ArchiveCourseAndComponents(students As IEnumerable(Of [Student]), user As [String])
    End Interface
    Public Class ArchiveService
        Implements IArchive
        Public Sub ArchiveCourseAndComponents(students As IEnumerable(Of [Student]), User As [String]) Implements IArchive.ArchiveCourseAndComponents
            ArchiveCompletedComponentsofIncompleteCourses(students, User)
            ArchiveInCompleteCourse(students, User)
        End Sub
        Private Sub ArchiveCompletedComponentsofIncompleteCourses(students As IEnumerable(Of [Student]), user As [String])
            For Each Student As [Student] In students
                For Each ClassSection As [Class] In Student.Classes
                    Dim db As New RegistrationProcessDB
                    db.ArchiveStudentCourseComponents(ClassSection.oldClassId, Student.StudentEnrollmentId, user)
                Next
            Next

        End Sub
        Private Sub ArchiveInCompleteCourse(students As IEnumerable(Of [Student]), user As [String])
            For Each Student As [Student] In students
                For Each ClassSection As [Class] In Student.Classes
                    Dim db As New RegistrationProcessDB
                    db.ArchiveStudentCourse(ClassSection.oldClassId, Student.StudentEnrollmentId, user)
                Next
            Next
        End Sub
    End Class

    Public Interface ITransfer
        Sub TransferCompletedComponents(students As IEnumerable(Of [Student]), user As [String])
    End Interface
    Public Class TransferService
        Implements ITransfer
        Public Sub TransferCompletedComponents(students As IEnumerable(Of [Student]), user As [String]) Implements ITransfer.TransferCompletedComponents
            For Each Student As [Student] In students
                For Each ClassSection As [Class] In Student.Classes
                    Dim db As New RegistrationProcessDB
                    db.TransferCompletedComponents(ClassSection.newClassId, ClassSection.oldClassId, Student.StudentEnrollmentId, user)
                Next
            Next
        End Sub
    End Class

    Public MustInherit Class registerClass
        Protected _students As IEnumerable(Of [Student])
        Protected _user As [String]
        Public Sub New(students As IEnumerable(Of [Student]), user As [String])
            _students = students
            _user = user
        End Sub
    End Class
    Public Class registerInCompleteClass
        Inherits registerClass
        Private ReadOnly _registrationService As IRegistration
        Private ReadOnly _transferService As ITransfer
        Private ReadOnly _archiveService As IArchive

        Public Sub New(students As IEnumerable(Of [Student]), registration As IRegistration, transfer As ITransfer, archive As IArchive, user As [String])
            MyBase.New(students, user)
            _registrationService = registration
            _transferService = transfer
            _archiveService = archive
        End Sub
        Public Sub RegisterStudent()
            _registrationService.RegisterStudent(_students, _user)
            _transferService.TransferCompletedComponents(_students, _user)
            _archiveService.ArchiveCourseAndComponents(_students, _user)
        End Sub
    End Class
End Namespace

