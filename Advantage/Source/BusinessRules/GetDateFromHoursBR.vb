﻿
Public Class GetDateFromHoursBR
    Public Function GetDateFromHours(ByVal StuEnrollId As String, ByRef Msg As String, ByRef UseTimeClock As Boolean, ByVal CampusId As String) As DateTime
        Dim dal As New DataAccess.GetDateFromHoursDB()

        '' New Code Added By Vijay Ramteke On June 15, 2010 For Manris Id 19167
        ''Return dal.GetDateFromHours(StuEnrollId, Msg, UseTimeClock)
        Return dal.GetDateFromHours(StuEnrollId, Msg, UseTimeClock, CampusId)
        '' New Code Added By Vijay Ramteke On June 15, 2010 For Manris Id 19167
    End Function
    Public Function GetDateFromHours(ByVal ProgramVersionId As String, ByVal StartDate As DateTime, ByVal ScheduleId As String, ByRef Msg As String, ByRef UseTimeClock As Boolean, ByVal CampusId As String) As DateTime
        Dim dal As New DataAccess.GetDateFromHoursDB()
        '' New Code Added By Vijay Ramteke On June 15, 2010 For Manris Id 19167
        ''Return dal.GetDateFromHours(ProgramVersionId, StartDate, ScheduleId, Msg, UseTimeClock)
        Return dal.GetDateFromHours(ProgramVersionId, StartDate, ScheduleId, Msg, UseTimeClock, CampusId)
        '' New Code Added By Vijay Ramteke On June 15, 2010 For Manris Id 19167
    End Function
End Class

