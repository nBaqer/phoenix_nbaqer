

Public Class PortalAttendancePercentageBR
    'Public Function GetPercentageAttendanceInfo(ByVal stuEnrollId As String, ByVal cutOffDate As Date, ByVal clsSectionId As String) As Common.AttendancePercentageInfo
    '    Dim dal As New DataAccess.PortalClsSectAttendanceDB()
    '    Dim dt As DataTable

    '    'check if attendance is tracked at program level
    '    Dim doesPrgVerTrackAttendance As Boolean = dal.DoesPrgVersionTrackAttendance(stuEnrollId, False)

    '    'get attendance type
    '    Dim attendanceType As String = ""
    '    Dim tardiesMakingOneAbsence As Integer
    '    If doesPrgVerTrackAttendance Then
    '        attendanceType = dal.GetPrgVersionAttendanceType(stuEnrollId, False)
    '        tardiesMakingOneAbsence = dal.GetTardiesMakingAbsenceForPrgVersion(stuEnrollId, False)
    '    Else
    '        attendanceType = dal.GetClassSectionAttendanceType(clsSectionId)
    '        tardiesMakingOneAbsence = dal.GetTardiesMakingAbsenceForCourse(clsSectionId, False)
    '    End If

    '    If attendanceType = "Minutes" Then
    '        dt =dal.GetAttendanceMinutes(stuEnrollId, clsSectionId)
    '    Else
    '        dt = dal.GetAttendancePercentageDT(stuEnrollId, cutOffDate, clsSectionId)
    '    End If

    '    Dim totalPresent As Integer = 0
    '    Dim totalAbsent As Integer = 0
    '    Dim totalTardies As Integer = 0
    '    Dim totalExcused As Integer = 0
    '    Dim totalScheduled As Decimal = 0.0
    '    Dim totalAttended As Decimal = 0.0
    '    For i As Integer = 0 To dt.Rows.Count - 1
    '        Dim row As DataRow = dt.Rows(i)
    '        Dim actual As Integer = CType(row("Actual"), Integer)

    '        'set tardy value
    '        Dim tardy As Boolean = CType(row("Tardy"), Boolean)

    '        Dim excused As Boolean = CType(row("Excused"), Boolean)
    '        If excused Then
    '            totalExcused += 1
    '        Else
    '            Select Case attendanceType
    '                Case "Present Absent"
    '                    totalScheduled += 1
    '                    Select Case tardy
    '                        Case True
    '                            totalTardies += 1
    '                            totalPresent += 1
    '                        Case False
    '                            If actual = 0 Then
    '                                totalPresent += 1
    '                            Else
    '                                totalAbsent += 1
    '                            End If
    '                    End Select
    '                Case "Minutes"
    '                    'calculate duration of the meeting
    '                    Dim meetingDuration As Integer = CType(row("EndTime"), DateTime).Subtract(CType(row("StartTime"), DateTime)).TotalMinutes
    '                    totalScheduled += meetingDuration
    '                    totalPresent += meetingDuration
    '                    If actual < meetingDuration Then
    '                        totalTardies += 1
    '                    End If
    '                Case "None"
    '            End Select
    '        End If
    '    Next

    '    'return attendance display object
    '    Dim totalPresentAdjusted As Decimal
    '    If Not tardiesMakingOneAbsence = 0 Then
    '        totalPresentAdjusted = totalPresent - (totalTardies / tardiesMakingOneAbsence)
    '    Else
    '        totalPresentAdjusted = totalPresent
    '    End If
    '    Dim totalAbsenceAdjusted As Decimal = totalAbsent
    '    Dim totalTardiesAdjusted As Decimal
    '    If Not tardiesMakingOneAbsence = 0 Then
    '        totalTardiesAdjusted = totalTardies Mod tardiesMakingOneAbsence
    '    Else
    '        totalTardiesAdjusted = totalTardies
    '    End If
    '    Dim totalExcusedAdjusted As Decimal = totalExcused
    '    Return New Common.AttendancePercentageInfo(totalPresent, totalAbsent, totalTardies, totalExcused, totalPresentAdjusted, totalAbsenceAdjusted, totalTardiesAdjusted, totalExcusedAdjusted, tardiesMakingOneAbsence)
    'End Function
    Public Function GetPercentageAttendanceInfo(ByVal stuEnrollId As String, ByVal cutOffDate As Date, ByVal clsSectionId As String) As Common.AttendancePercentageInfo

        Dim dal As New DataAccess.PortalClsSectAttendanceDB()
        Dim dt As DataTable

        'check if attendance is tracked at program level
        Dim doesPrgVerTrackAttendance As Boolean = dal.DoesPrgVersionTrackAttendance(stuEnrollId, False)
        Dim attendanceType As String = ""
        Dim tardiesMakingOneAbsence As Integer = 0
        'discussed with Jeff and he wants to go with program version level now
        tardiesMakingOneAbsence = dal.GetTardiesMakingAbsenceForPrgVersion(stuEnrollId, False)
        If doesPrgVerTrackAttendance Then
            attendanceType = dal.GetPrgVersionAttendanceType(stuEnrollId, False)
        Else
            '    attendanceType = dal.GetClassSectionAttendanceType(clsSectionId)
            '    tardiesMakingOneAbsence = dal.GetTardiesMakingAbsenceForCourse(clsSectionId, False)
            attendanceType = dal.GetStudentAttendanceType(stuEnrollId)
        End If

        If (attendanceType = "Minutes") Or (attendanceType = "Clock Hours") Then
            dt = dal.GetAttendanceMinutes(stuEnrollId, cutOffDate, clsSectionId)
        Else
            dt = dal.GetAttendancePercentageDTForReport(stuEnrollId, cutOffDate, clsSectionId)
        End If

        Dim totalPresent As Integer = 0
        Dim totalAbsent As Integer = 0
        Dim totalTardies As Integer = 0
        Dim totalExcused As Integer = 0
        Dim totalScheduled As Decimal = 0.0
        Dim totalAttended As Decimal = 0.0
        Dim totalPresentAdjusted As Decimal
        Dim totalAbsenceAdjusted As Decimal
        Dim totalTardiesAdjusted As Decimal
        Dim totalExcusedAdjusted As Decimal
        Dim ccounter As Integer
        Dim absences As Integer
        Dim minusPresent As Integer
        Dim minusTardies As Integer
        Dim totalmakeUp As Integer = 0
        'If doesPrgVerTrackAttendance Then

        'Attendance is tracked at the program version level

        For Each row As DataRow In dt.Rows
            Dim actual As Integer = CType(row("Actual"), Integer)
            Dim tardy As Boolean = CType(row("Tardy"), Boolean)
            Dim excused As Boolean = CType(row("Excused"), Boolean)
            Dim meetingDuration As Integer = 0
            Select Case attendanceType

                Case "Present Absent"
                    totalScheduled += 1

                    If actual = 0 And excused Then
                        totalExcused += 1

                    ElseIf actual = 0 And Not excused Then
                        totalAbsent += 1

                    ElseIf actual = 1 And tardy Then
                        totalTardies += 1

                    ElseIf actual = 1 And Not tardy Then
                        totalPresent += 1
                    End If

                Case "Minutes", "Clock Hours"
                    'calculate duration of the meeting


                    meetingDuration = CType(row("Scheduled"), Integer)
                    totalScheduled += meetingDuration
                    If meetingDuration > actual And Not tardy Then
                        totalAbsent += meetingDuration - actual
                        totalPresent += actual
                    Else
                        totalPresent += actual

                        If meetingDuration > actual Then

                            ccounter += 1
                            If ccounter = tardiesMakingOneAbsence Then
                                absences += meetingDuration
                                minusPresent += actual
                                minusTardies += meetingDuration - actual
                                ccounter = 0
                            Else
                                absences += meetingDuration - actual
                            End If
                            totalTardies += meetingDuration - actual
                        Else
                            totalmakeUp += actual - meetingDuration
                        End If
                    End If


                Case "None"
                    'do nothing
            End Select

        Next

        'End If


        totalPresentAdjusted = totalPresent
        totalExcusedAdjusted = totalExcused
        totalAbsenceAdjusted = totalAbsent + totalExcused
        totalTardiesAdjusted = totalTardies

        Select Case attendanceType

            Case "Present Absent"

                'Adjust absences and tardies based on the number of tardies making one absence
                If Not tardiesMakingOneAbsence = 0 Then
                    Dim tempAbsences As Integer = totalTardies \ tardiesMakingOneAbsence
                    totalAbsenceAdjusted += tempAbsences
                    If totalTardiesAdjusted >= tempAbsences Then
                        totalTardiesAdjusted -= tempAbsences
                    End If
                End If
                totalPresentAdjusted += totalTardiesAdjusted
            Case "Minutes", "Clock Hours"

                'Adjust present, absences and tardies based on the scheduled time that was missed.
                If totalPresentAdjusted >= minusPresent Then
                    totalPresentAdjusted -= minusPresent
                End If
                If totalTardiesAdjusted >= minusTardies Then
                    totalTardiesAdjusted -= minusTardies
                End If
                If totalPresentAdjusted > 0 And totalTardiesAdjusted > 0 Then
                    totalAbsenceAdjusted += absences
                End If

        End Select

        'Always adjusted present must include adjusted tardies


        'return attendance display object
        Return New Common.AttendancePercentageInfo(totalPresent, totalAbsent, totalTardies, totalExcused, totalPresentAdjusted, totalAbsenceAdjusted, totalTardiesAdjusted, totalExcusedAdjusted, tardiesMakingOneAbsence, attendanceType, totalScheduled, totalmakeUp)
    End Function
    Public Function GetPercentageAttendanceInfo(ByVal stuEnrollId As String, ByVal cutOffDate As Date) As Common.AttendancePercentageInfo
        Dim dal As New DataAccess.PortalClsSectAttendanceDB()
        Dim dt As DataTable

        'check if attendance is tracked at program level
        Dim doesPrgVerTrackAttendance As Boolean = dal.DoesPrgVersionTrackAttendance(stuEnrollId, False)
        Dim attendanceType As String = ""
        Dim tardiesMakingOneAbsence As Integer = 0
        'discussed with Jeff and he wants to go with program version level now
        tardiesMakingOneAbsence = dal.GetTardiesMakingAbsenceForPrgVersion(stuEnrollId, False)
        If doesPrgVerTrackAttendance Then
            attendanceType = dal.GetPrgVersionAttendanceType(stuEnrollId, False)
        Else
            '    attendanceType = dal.GetClassSectionAttendanceType(clsSectionId)
            '    tardiesMakingOneAbsence = dal.GetTardiesMakingAbsenceForCourse(clsSectionId, False)
            attendanceType = dal.GetStudentAttendanceType(stuEnrollId)
        End If

        If (attendanceType = "Minutes") Or (attendanceType = "Clock Hours") Then
            dt = dal.GetAttendanceMinutes(stuEnrollId, cutOffDate)
        Else
            dt = dal.GetAttendancePercentageDTForReport(stuEnrollId, cutOffDate)
        End If

        Dim totalPresent As Integer = 0
        Dim totalAbsent As Integer = 0
        Dim totalTardies As Integer = 0
        Dim totalExcused As Integer = 0
        Dim totalScheduled As Decimal = 0.0
        Dim totalAttended As Decimal = 0.0
        Dim totalPresentAdjusted As Decimal
        Dim totalAbsenceAdjusted As Decimal
        Dim totalTardiesAdjusted As Decimal
        Dim totalExcusedAdjusted As Decimal
        Dim ccounter As Integer
        Dim absences As Integer
        Dim minusPresent As Integer
        Dim minusTardies As Integer
        Dim totalmakeUp As Integer = 0


        'Attendance is tracked at the program version level

        For Each row As DataRow In dt.Rows
            Dim actual As Integer = CType(row("Actual"), Integer)
            Dim tardy As Boolean = CType(row("Tardy"), Boolean)
            Dim excused As Boolean = CType(row("Excused"), Boolean)
            Dim meetingDuration As Integer = 0
            Select Case attendanceType

                Case "Present Absent"
                    totalScheduled += 1
                    If actual = 0 And excused Then
                        totalExcused += 1

                    ElseIf actual = 0 And Not excused Then
                        totalAbsent += 1

                    ElseIf actual = 1 And tardy Then
                        totalTardies += 1

                    ElseIf actual = 1 And Not tardy Then
                        totalPresent += 1
                    End If

                Case "Minutes", "Clock Hours"
                    'calculate duration of the meeting


                    meetingDuration = CType(row("Scheduled"), Integer)
                    totalScheduled += meetingDuration
                    If meetingDuration > actual And Not tardy Then
                        totalAbsent += meetingDuration - actual
                        totalPresent += actual
                    Else
                        totalPresent += actual

                        If meetingDuration > actual Then

                            ccounter += 1
                            If ccounter = tardiesMakingOneAbsence Then
                                absences += meetingDuration
                                minusPresent += actual
                                minusTardies += meetingDuration - actual
                                ccounter = 0
                            Else
                                absences += meetingDuration - actual
                            End If
                            totalTardies += meetingDuration - actual
                        Else
                            totalmakeUp += actual - meetingDuration
                        End If
                    End If


                Case "None"
                    'do nothing
            End Select

        Next

        'End If


        totalPresentAdjusted = totalPresent
        totalExcusedAdjusted = totalExcused
        totalAbsenceAdjusted = totalAbsent + totalExcused
        totalTardiesAdjusted = totalTardies

        Select Case attendanceType

            Case "Present Absent"

                'Adjust absences and tardies based on the number of tardies making one absence
                If Not tardiesMakingOneAbsence = 0 Then
                    Dim tempAbsences As Integer = totalTardies \ tardiesMakingOneAbsence
                    totalAbsenceAdjusted += tempAbsences
                    If totalTardiesAdjusted >= tempAbsences Then
                        totalTardiesAdjusted -= tempAbsences
                    End If
                End If
                totalPresentAdjusted += totalTardiesAdjusted
            Case "Minutes", "Clock Hours"

                'Adjust present, absences and tardies based on the scheduled time that was missed.
                If totalPresentAdjusted >= minusPresent Then
                    totalPresentAdjusted -= minusPresent
                End If
                If totalTardiesAdjusted >= minusTardies Then
                    totalTardiesAdjusted -= minusTardies
                End If
                If totalPresentAdjusted > 0 And totalTardiesAdjusted > 0 Then
                    totalAbsenceAdjusted += absences
                End If

        End Select

        'Always adjusted present must include adjusted tardies


        'return attendance display object
        Return New Common.AttendancePercentageInfo(totalPresent, totalAbsent, totalTardies, totalExcused, totalPresentAdjusted, totalAbsenceAdjusted, totalTardiesAdjusted, totalExcusedAdjusted, tardiesMakingOneAbsence, attendanceType, totalScheduled, totalmakeUp)
    End Function
    Public Function GetPercentageAttendanceInfo_SP(ByVal stuEnrollId As String, ByVal cutOffDate As Date, ByVal attendanceType As String, ByVal tardiesMakingOneAbsence As Integer) As Common.AttendancePercentageInfo
        Dim dal As New DataAccess.PortalClsSectAttendanceDB()
        Dim dt As DataTable




        'check if attendance is tracked at program level



        'Dim tardiesMakingOneAbsence As Integer = 0


        'tardiesMakingOneAbsence = dal.GetTardiesMakingAbsenceForPrgVersion(stuEnrollId, False)


        If (attendanceType = "minutes" Or attendanceType = "clock hours") Then
            dt = dal.GetAttendanceMinutes_SP(stuEnrollId, cutOffDate)
        Else
            dt = dal.GetAttendancePercentageDT_SP(stuEnrollId, cutOffDate)
        End If

        Dim totalPresent As Integer = 0
        Dim totalAbsent As Integer = 0
        Dim totalTardies As Integer = 0
        Dim totalExcused As Integer = 0
        Dim totalScheduled As Decimal = 0.0
        Dim totalAttended As Decimal = 0.0
        Dim totalPresentAdjusted As Decimal
        Dim totalAbsenceAdjusted As Decimal
        Dim totalTardiesAdjusted As Decimal
        Dim totalExcusedAdjusted As Decimal
        Dim ccounter As Integer
        Dim absences As Integer
        Dim minusPresent As Integer
        Dim minusTardies As Integer
        Dim totalmakeUp As Integer = 0
        'If doesPrgVerTrackAttendance Then

        'Attendance is tracked at the program version level

        For Each row As DataRow In dt.Rows
            Dim actual As Integer = CType(row("Actual"), Integer)
            Dim tardy As Boolean = CType(row("Tardy"), Boolean)
            Dim excused As Boolean = CType(row("Excused"), Boolean)
            Dim meetingDuration As Integer = 0
            Select Case attendanceType

                Case "present absent"
                    totalScheduled += 1
                    If actual = 0 And excused Then
                        totalExcused += 1

                    ElseIf actual = 0 And Not excused Then
                        totalAbsent += 1

                    ElseIf actual = 1 And tardy Then
                        totalTardies += 1

                    ElseIf actual = 1 And Not tardy Then
                        totalPresent += 1
                    End If

                Case "minutes", "clock hours"
                    'calculate duration of the meeting


                    meetingDuration = CType(row("Scheduled"), Integer)
                    totalScheduled += meetingDuration
                    If meetingDuration > actual And Not tardy Then
                        totalAbsent += meetingDuration - actual
                        totalPresent += actual
                    Else
                        totalPresent += actual

                        If meetingDuration > actual Then

                            ccounter += 1
                            If ccounter = tardiesMakingOneAbsence Then
                                absences += meetingDuration
                                minusPresent += actual
                                minusTardies += meetingDuration - actual
                                ccounter = 0
                            Else
                                absences += meetingDuration - actual
                            End If
                            totalTardies += meetingDuration - actual
                        Else
                            totalmakeUp += actual - meetingDuration
                        End If
                    End If


                Case "None"
                    'do nothing
            End Select

        Next

        'End If


        totalPresentAdjusted = totalPresent
        totalExcusedAdjusted = totalExcused
        totalAbsenceAdjusted = totalAbsent + totalExcused
        totalTardiesAdjusted = totalTardies

        Select Case attendanceType

            Case "present absent"

                'Adjust absences and tardies based on the number of tardies making one absence
                If Not tardiesMakingOneAbsence = 0 Then
                    Dim tempAbsences As Integer = totalTardies \ tardiesMakingOneAbsence
                    totalAbsenceAdjusted += tempAbsences
                    If totalTardiesAdjusted >= tempAbsences Then
                        totalTardiesAdjusted -= tempAbsences
                    End If
                End If
                totalPresentAdjusted += totalTardiesAdjusted
            Case "minutes", "clock hours"

                'Adjust present, absences and tardies based on the scheduled time that was missed.
                If totalPresentAdjusted >= minusPresent Then
                    totalPresentAdjusted -= minusPresent
                End If
                If totalTardiesAdjusted >= minusTardies Then
                    totalTardiesAdjusted -= minusTardies
                End If
                If totalPresentAdjusted > 0 And totalTardiesAdjusted > 0 Then
                    totalAbsenceAdjusted += absences
                End If

        End Select

        'Always adjusted present must include adjusted tardies


        'return attendance display object
        Return New Common.AttendancePercentageInfo(totalPresent, totalAbsent, totalTardies, totalExcused, totalPresentAdjusted, totalAbsenceAdjusted, totalTardiesAdjusted, totalExcusedAdjusted, tardiesMakingOneAbsence, attendanceType, totalScheduled, totalmakeUp)
    End Function
    Public Function GetPercentageAttendanceInfoForReport(ByVal stuEnrollId As String, ByVal cutOffDate As Date) As Common.AttendancePercentageInfo
        Dim dal As New DataAccess.PortalClsSectAttendanceDB()
        Dim dt As DataTable

        'check if attendance is tracked at program level
        Dim doesPrgVerTrackAttendance As Boolean = dal.DoesPrgVersionTrackAttendance(stuEnrollId, False)
        Dim attendanceType As String = ""
        Dim tardiesMakingOneAbsence As Integer = 0
        'discussed with Jeff and he wants to go with program version level now
        tardiesMakingOneAbsence = dal.GetTardiesMakingAbsenceForPrgVersion(stuEnrollId, False)
        If doesPrgVerTrackAttendance Then
            attendanceType = dal.GetPrgVersionAttendanceType(stuEnrollId, False)
        Else
            attendanceType = dal.GetStudentAttendanceType(stuEnrollId)
        End If

        If (attendanceType = "Minutes") Or (attendanceType = "Clock Hours") Then
            dt = dal.GetAttendanceMinutes(stuEnrollId, cutOffDate)
        Else
            dt = dal.GetAttendancePercentageDTForReport(stuEnrollId, cutOffDate)
        End If



        Dim totalPresent As Integer = 0
        Dim totalAbsent As Integer = 0
        Dim totalTardies As Integer = 0
        Dim totalExcused As Integer = 0
        Dim totalScheduled As Decimal = 0.0
        Dim totalAttended As Decimal = 0.0
        Dim totalPresentAdjusted As Decimal
        Dim totalAbsenceAdjusted As Decimal
        Dim totalTardiesAdjusted As Decimal
        Dim totalExcusedAdjusted As Decimal
        Dim ccounter As Integer
        Dim absences As Integer
        Dim minusPresent As Integer
        Dim minusTardies As Integer
        Dim totalmakeUp As Integer = 0
        'If doesPrgVerTrackAttendance Then

        'Attendance is tracked at the program version level
        Dim totalScheduledMinutes As Integer = 0
        Dim totalExcusedMinutes As Integer = 0
        Dim totalAbsentMinutes As Integer = 0
        Dim totalTardiesMinutes As Integer = 0
        Dim totalPresentMinutes As Integer = 0
        For Each row As DataRow In dt.Rows
            Dim actual As Integer = CType(row("Actual"), Integer)
            Dim tardy As Boolean = CType(row("Tardy"), Boolean)
            Dim excused As Boolean = CType(row("Excused"), Boolean)
            Dim meetingDuration As Integer = 0


            Select Case attendanceType

                Case "Present Absent"
                    meetingDuration = CType(row("Duration"), Integer)
                    totalScheduled += 1
                    totalScheduledMinutes = totalScheduledMinutes + (meetingDuration * 1)
                    If actual = 0 And excused Then
                        totalExcused += 1
                        totalExcusedMinutes = totalExcusedMinutes + (meetingDuration * 1)

                    ElseIf actual = 0 And Not excused Then
                        totalAbsent += 1
                        totalAbsentMinutes = totalAbsentMinutes + (meetingDuration * 1)

                    ElseIf actual = 1 And tardy Then
                        totalTardies += 1
                        totalTardiesMinutes = totalTardiesMinutes + (meetingDuration * 1)

                    ElseIf actual = 1 And Not tardy Then
                        totalPresent += 1
                        totalPresentMinutes = totalPresentMinutes + (meetingDuration * 1)
                    End If

                Case "Minutes", "Clock Hours"
                    'calculate duration of the meeting


                    meetingDuration = CType(row("Scheduled"), Integer)
                    totalScheduled += meetingDuration
                    If meetingDuration > actual And Not tardy Then
                        totalAbsent += meetingDuration - actual
                        totalPresent += actual
                    Else
                        totalPresent += actual

                        If meetingDuration > actual Then

                            ccounter += 1
                            If ccounter = tardiesMakingOneAbsence Then
                                absences += meetingDuration
                                minusPresent += actual
                                minusTardies += meetingDuration - actual
                                ccounter = 0
                            Else
                                absences += meetingDuration - actual
                            End If
                            totalTardies += meetingDuration - actual
                        Else
                            totalmakeUp += actual - meetingDuration
                        End If
                    End If


                Case "None"
                    'do nothing
            End Select

        Next

        'End If


        totalPresentAdjusted = totalPresent
        totalExcusedAdjusted = totalExcused
        totalAbsenceAdjusted = totalAbsent + totalExcused
        totalTardiesAdjusted = totalTardies

        Select Case attendanceType

            Case "Present Absent"

                'Adjust absences and tardies based on the number of tardies making one absence
                If Not tardiesMakingOneAbsence = 0 Then
                    Dim tempAbsences As Integer = totalTardies \ tardiesMakingOneAbsence
                    totalAbsenceAdjusted += tempAbsences
                    If totalTardiesAdjusted >= tempAbsences Then
                        totalTardiesAdjusted -= tempAbsences
                    End If
                End If
                totalPresentAdjusted += totalTardiesAdjusted
            Case "Minutes", "Clock Hours"

                'Adjust present, absences and tardies based on the scheduled time that was missed.
                If totalPresentAdjusted >= minusPresent Then
                    totalPresentAdjusted -= minusPresent
                End If
                If totalTardiesAdjusted >= minusTardies Then
                    totalTardiesAdjusted -= minusTardies
                End If
                If totalPresentAdjusted > 0 And totalTardiesAdjusted > 0 Then
                    totalAbsenceAdjusted += absences
                End If

        End Select

        'Always adjusted present must include adjusted tardies


        'return attendance display object
        Return New Common.AttendancePercentageInfo(totalPresent, totalAbsent, totalTardies, totalExcused, totalPresentAdjusted, totalAbsenceAdjusted, totalTardiesAdjusted, totalExcusedAdjusted, tardiesMakingOneAbsence, attendanceType, totalScheduled, totalmakeUp)
    End Function

    Public Function GetPercentageAttendanceInfoForReportWithImportedAttendance(ByVal stuEnrollId As String, ByVal cutOffDate As Date) As Common.AttendancePercentageInfo
        Dim dal As New DataAccess.PortalClsSectAttendanceDB()
        Dim dt As DataTable
        Dim dtImported As DataTable
        'check if attendance is tracked at program level





        'dt = dal.GetAttendanceMinutes(stuEnrollId, cutOffDate)
        dt = dal.GetAttendanceMinutes_SP(stuEnrollId, cutOffDate)
        dtImported = dal.GetImportedAttendance_SP(stuEnrollId)

        Dim totalImportedScheduledMinutes As Integer = 0
        Dim totalImportedAbsentMinutes As Integer = 0
        Dim totalImportedPresentMinutes As Integer = 0

        If dtImported.Rows.Count > 0 Then
            totalImportedScheduledMinutes = dtImported.Rows(0)("Scheduled")
            totalImportedAbsentMinutes = dtImported.Rows(0)("Absent")
            totalImportedPresentMinutes = dtImported.Rows(0)("Actual")

        End If


        Dim totalPresent As Integer = 0
        Dim totalAbsent As Integer = 0
        Dim totalTardies As Integer = 0
        Dim totalExcused As Integer = 0
        Dim totalScheduled As Decimal = 0.0
        Dim totalAttended As Decimal = 0.0
        Dim totalPresentAdjusted As Decimal
        Dim totalAbsenceAdjusted As Decimal
        Dim totalTardiesAdjusted As Decimal
        Dim totalExcusedAdjusted As Decimal
        Dim ccounter As Integer
        Dim absences As Integer
        Dim minusPresent As Integer
        Dim minusTardies As Integer
        Dim totalmakeUp As Integer = 0
        'If doesPrgVerTrackAttendance Then

        'Attendance is tracked at the program version level
        Dim totalScheduledMinutes As Integer = 0
        Dim totalExcusedMinutes As Integer = 0
        Dim totalAbsentMinutes As Integer = 0
        Dim totalTardiesMinutes As Integer = 0
        Dim totalPresentMinutes As Integer = 0
        For Each row As DataRow In dt.Rows
            Dim actual As Integer = CType(row("Actual"), Integer)

            Dim meetingDuration As Integer = 0
            meetingDuration = CType(row("Scheduled"), Integer)
            totalScheduled += meetingDuration
            If meetingDuration > actual Then
                totalAbsent += meetingDuration - actual
            End If
            totalPresent += actual



        Next
        totalScheduled = totalScheduled + totalImportedScheduledMinutes
        totalPresent = totalPresent + totalImportedPresentMinutes
        totalAbsent = totalAbsent + totalImportedAbsentMinutes

        totalPresentAdjusted = totalPresent
        totalExcusedAdjusted = totalExcused
        totalAbsenceAdjusted = totalAbsent
        totalTardiesAdjusted = totalTardies

        Return New Common.AttendancePercentageInfo(totalPresent, totalAbsent, totalTardies, totalExcused, totalPresentAdjusted, totalAbsenceAdjusted, totalTardiesAdjusted, totalExcusedAdjusted, 0, "Clock Hours", totalScheduled, totalmakeUp)
    End Function

    Public Function GetImportedAttendanceBetweenDateRange(ByVal stuEnrollId As String, ByVal cutOffDate As Date, meetStartDate As Date) As Common.AttendancePercentageInfo
        Dim dal As New DataAccess.PortalClsSectAttendanceDB()
        Dim dt As DataTable
        Dim dtImported As DataTable
        'check if attendance is tracked at program level





        'dt = dal.GetAttendanceMinutes(stuEnrollId, cutOffDate)
        dt = dal.GetAttendanceMinutes(stuEnrollId, cutOffDate, , meetStartDate)
        dtImported = dal.GetImportedAttendanceBetweenDateRange_SP(stuEnrollId, cutOffDate, meetStartDate)

        Dim totalImportedScheduledMinutes As Integer = 0
        Dim totalImportedAbsentMinutes As Integer = 0
        Dim totalImportedPresentMinutes As Integer = 0

        If dtImported.Rows.Count > 0 Then
            totalImportedScheduledMinutes = dtImported.Rows(0)("Scheduled")
            totalImportedAbsentMinutes = dtImported.Rows(0)("Absent")
            totalImportedPresentMinutes = dtImported.Rows(0)("Actual")

        End If


        Dim totalPresent As Integer = 0
        Dim totalAbsent As Integer = 0
        Dim totalTardies As Integer = 0
        Dim totalExcused As Integer = 0
        Dim totalScheduled As Decimal = 0.0
        Dim totalAttended As Decimal = 0.0
        Dim totalPresentAdjusted As Decimal
        Dim totalAbsenceAdjusted As Decimal
        Dim totalTardiesAdjusted As Decimal
        Dim totalExcusedAdjusted As Decimal
        Dim ccounter As Integer
        Dim absences As Integer
        Dim minusPresent As Integer
        Dim minusTardies As Integer
        Dim totalmakeUp As Integer = 0
        'If doesPrgVerTrackAttendance Then

        'Attendance is tracked at the program version level
        Dim totalScheduledMinutes As Integer = 0
        Dim totalExcusedMinutes As Integer = 0
        Dim totalAbsentMinutes As Integer = 0
        Dim totalTardiesMinutes As Integer = 0
        Dim totalPresentMinutes As Integer = 0
        For Each row As DataRow In dt.Rows
            Dim actual As Integer = CType(row("Actual"), Integer)

            Dim meetingDuration As Integer = 0
            meetingDuration = CType(row("Scheduled"), Integer)
            totalScheduled += meetingDuration
            If meetingDuration > actual Then
                totalAbsent += meetingDuration - actual
            End If
            totalPresent += actual



        Next
        totalScheduled = totalScheduled + totalImportedScheduledMinutes
        totalPresent = totalPresent + totalImportedPresentMinutes
        totalAbsent = totalAbsent + totalImportedAbsentMinutes

        totalPresentAdjusted = totalPresent
        totalExcusedAdjusted = totalExcused
        totalAbsenceAdjusted = totalAbsent
        totalTardiesAdjusted = totalTardies

        Return New Common.AttendancePercentageInfo(totalPresent, totalAbsent, totalTardies, totalExcused, totalPresentAdjusted, totalAbsenceAdjusted, totalTardiesAdjusted, totalExcusedAdjusted, 0, "Clock Hours", totalScheduled, totalmakeUp)
    End Function


    Public Function GetPercentageAttendanceInfoForPresentAbsentHoursReport(ByVal stuEnrollId As String, ByVal cutOffDate As Date, Optional ByVal meetstartdate As Object = Nothing) As Common.AttendancePercentageInfo
        Dim dal As New DataAccess.PortalClsSectAttendanceDB()
        Dim dt As DataTable

        'check if attendance is tracked at program level
        Dim doesPrgVerTrackAttendance As Boolean = dal.DoesPrgVersionTrackAttendance(stuEnrollId, False)
        Dim attendanceType As String = ""
        Dim tardiesMakingOneAbsence As Integer = 0
        'discussed with Jeff and he wants to go with program version level now
        tardiesMakingOneAbsence = dal.GetTardiesMakingAbsenceForPrgVersion(stuEnrollId, False)
        If doesPrgVerTrackAttendance Then
            attendanceType = dal.GetPrgVersionAttendanceType(stuEnrollId, False)
        Else
            attendanceType = dal.GetStudentAttendanceType(stuEnrollId)
        End If

        If (attendanceType = "Minutes") Or (attendanceType = "Clock Hours") Then
            dt = dal.GetAttendanceMinutes(stuEnrollId, cutOffDate, , meetstartdate)
        Else
            dt = dal.GetAttendancePercentageDTForReport(stuEnrollId, cutOffDate, , meetstartdate)
        End If



        Dim totalPresent As Integer = 0
        Dim totalAbsent As Integer = 0
        Dim totalTardies As Integer = 0
        Dim totalExcused As Integer = 0
        Dim totalScheduled As Decimal = 0.0
        Dim totalAttended As Decimal = 0.0
        Dim totalPresentAdjusted As Decimal
        Dim totalAbsenceAdjusted As Decimal
        Dim totalTardiesAdjusted As Decimal
        Dim totalExcusedAdjusted As Decimal
        Dim ccounter As Integer
        Dim absences As Integer
        Dim minusPresent As Integer
        Dim minusTardies As Integer
        Dim totalmakeUp As Integer = 0
        'If doesPrgVerTrackAttendance Then

        'Attendance is tracked at the program version level
        Dim totalScheduledMinutes As Integer = 0
        Dim totalExcusedMinutes As Integer = 0
        Dim totalAbsentMinutes As Integer = 0
        Dim totalTardiesMinutes As Integer = 0
        Dim totalPresentMinutes As Integer = 0
        Try
            For Each row As DataRow In dt.Rows
                Dim actual As Integer = CType(row("Actual"), Integer)
                Dim tardy As Boolean = CType(row("Tardy"), Boolean)
                Dim excused As Boolean = CType(row("Excused"), Boolean)
                Dim meetingDuration As Integer = 0


                Select Case attendanceType

                    Case "Present Absent"
                        meetingDuration = CType(row("Duration"), Integer)
                        totalScheduled += 1
                        totalScheduledMinutes = totalScheduledMinutes + (meetingDuration * 1)
                        If actual = 0 And excused Then
                            totalExcused += 1
                            totalExcusedMinutes = totalExcusedMinutes + (meetingDuration * 1)

                        ElseIf actual = 0 And Not excused Then
                            totalAbsent += 1
                            totalAbsentMinutes = totalAbsentMinutes + (meetingDuration * 1)

                        ElseIf actual = 1 And tardy Then
                            totalTardies += 1
                            totalTardiesMinutes = totalTardiesMinutes + (meetingDuration * 1)

                            ccounter += 1
                            If ccounter = tardiesMakingOneAbsence Then
                                absences += meetingDuration
                                minusPresent += meetingDuration
                                minusTardies += meetingDuration
                                ccounter = 0
                            End If


                        ElseIf actual = 1 And Not tardy Then
                            totalPresent += 1
                            totalPresentMinutes = totalPresentMinutes + (meetingDuration * 1)
                        End If

                    Case "Minutes", "Clock Hours"
                        'calculate duration of the meeting


                        meetingDuration = CType(row("Scheduled"), Integer)
                        totalScheduled += meetingDuration
                        If meetingDuration > actual And Not tardy Then
                            totalAbsent += meetingDuration - actual
                            totalPresent += actual
                        Else
                            totalPresent += actual

                            If meetingDuration > actual Then
                                ccounter += 1
                                If ccounter = tardiesMakingOneAbsence Then
                                    absences += meetingDuration
                                    minusPresent += actual
                                    minusTardies += meetingDuration - actual
                                    ccounter = 0
                                Else
                                    absences += meetingDuration - actual
                                End If
                                totalTardies += meetingDuration - actual
                            Else
                                totalmakeUp += actual - meetingDuration
                            End If
                        End If


                    Case "None"
                        'do nothing
                End Select

            Next

            'End If

            Select Case attendanceType

                Case "Minutes", "Clock Hours"
                    totalPresentAdjusted = totalPresent
                    totalExcusedAdjusted = totalExcused
                    totalAbsenceAdjusted = totalAbsent + totalExcused
                    totalTardiesAdjusted = totalTardies
                Case Else
                    totalPresentAdjusted = totalPresentMinutes
                    totalExcusedAdjusted = totalExcusedMinutes
                    totalAbsenceAdjusted = totalAbsentMinutes + totalExcusedMinutes
                    totalTardiesAdjusted = totalTardiesMinutes

            End Select
            Select Case attendanceType

                Case "Present Absent"

                    'Adjust absences and tardies based on the number of tardies making one absence
                    'If Not tardiesMakingOneAbsence = 0 Then
                    '    Dim tempAbsences As Integer = totalTardies \ tardiesMakingOneAbsence
                    '    totalAbsenceAdjusted += tempAbsences
                    '    If totalTardiesAdjusted >= tempAbsences Then
                    '        totalTardiesAdjusted -= tempAbsences
                    '    End If
                    'End If
                    'totalPresentAdjusted += totalTardiesAdjusted
                    'If totalPresentMinutes >= minusPresent Then
                    '    totalPresentMinutes -= minusPresent
                    'End If
                    If totalTardiesMinutes >= minusTardies Then
                        totalTardiesMinutes -= minusTardies
                    End If
                    If totalPresentMinutes > 0 And totalTardiesMinutes > 0 Then
                        totalAbsentMinutes += absences
                    End If
                Case "Minutes", "Clock Hours"

                    'Adjust present, absences and tardies based on the scheduled time that was missed.
                    If totalPresentAdjusted >= minusPresent Then
                        totalPresentAdjusted -= minusPresent
                    End If
                    If totalTardiesAdjusted >= minusTardies Then
                        totalTardiesAdjusted -= minusTardies
                    End If
                    If totalPresentAdjusted > 0 And totalTardiesAdjusted > 0 Then
                        totalAbsenceAdjusted += absences
                    End If

            End Select

            'Always adjusted present must include adjusted tardies

            Select Case attendanceType

                Case "Minutes", "Clock Hours"
                    Return New Common.AttendancePercentageInfo(totalPresent, totalAbsent, totalTardies, totalExcused, totalPresentAdjusted, totalAbsenceAdjusted, totalTardiesAdjusted, totalExcusedAdjusted, tardiesMakingOneAbsence, attendanceType, totalScheduled, totalmakeUp)
                Case Else
                    Return New Common.AttendancePercentageInfo(totalPresentMinutes, totalAbsentMinutes, totalTardiesMinutes, totalExcusedMinutes, totalPresentAdjusted, totalAbsenceAdjusted, totalTardiesAdjusted, totalExcusedAdjusted, tardiesMakingOneAbsence, attendanceType, totalScheduled, totalmakeUp)
            End Select
            'return attendance display object
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            End If
        End Try
    End Function
    Public Function GetPercentageAttendanceInfoForReport_ByInsType(ByVal stuEnrollId As String, ByVal cutOffDate As Date, InsType As String) As Common.AttendancePercentageInfo
        Dim dal As New DataAccess.PortalClsSectAttendanceDB()
        Dim dt As DataTable
        Dim dRows As DataRow()
        'check if attendance is tracked at program level
        Dim doesPrgVerTrackAttendance As Boolean = dal.DoesPrgVersionTrackAttendance(stuEnrollId, False)
        Dim attendanceType As String = ""
        Dim tardiesMakingOneAbsence As Integer = 0
        'discussed with Jeff and he wants to go with program version level now
        tardiesMakingOneAbsence = dal.GetTardiesMakingAbsenceForPrgVersion(stuEnrollId, False)
        If doesPrgVerTrackAttendance Then
            attendanceType = dal.GetPrgVersionAttendanceType(stuEnrollId, False)
        Else
            attendanceType = dal.GetStudentAttendanceType(stuEnrollId)
        End If

        If (attendanceType = "Minutes") Or (attendanceType = "Clock Hours") Then
            dt = dal.GetAttendanceMinutes(stuEnrollId, cutOffDate)
        Else
            dt = dal.GetAttendancePercentageDTForReport(stuEnrollId, cutOffDate)
        End If
        dRows = dt.Select("InstructionTypeID='" & InsType & "'")


        Dim totalPresent As Integer = 0
        Dim totalAbsent As Integer = 0
        Dim totalTardies As Integer = 0
        Dim totalExcused As Integer = 0
        Dim totalScheduled As Decimal = 0.0
        Dim totalAttended As Decimal = 0.0
        Dim totalPresentAdjusted As Decimal
        Dim totalAbsenceAdjusted As Decimal
        Dim totalTardiesAdjusted As Decimal
        Dim totalExcusedAdjusted As Decimal
        Dim ccounter As Integer
        Dim absences As Integer
        Dim minusPresent As Integer
        Dim minusTardies As Integer
        Dim totalmakeUp As Integer = 0
        'If doesPrgVerTrackAttendance Then

        'Attendance is tracked at the program version level
        Dim totalScheduledMinutes As Integer = 0
        Dim totalExcusedMinutes As Integer = 0
        Dim totalAbsentMinutes As Integer = 0
        Dim totalTardiesMinutes As Integer = 0
        Dim totalPresentMinutes As Integer = 0
        For Each row As DataRow In dRows
            Dim actual As Integer = CType(row("Actual"), Integer)
            Dim tardy As Boolean = CType(row("Tardy"), Boolean)
            Dim excused As Boolean = CType(row("Excused"), Boolean)
            Dim meetingDuration As Integer = 0


            Select Case attendanceType

                Case "Present Absent"
                    meetingDuration = CType(row("Duration"), Integer)
                    totalScheduled += 1
                    totalScheduledMinutes = totalScheduledMinutes + (meetingDuration * 1)
                    If actual = 0 And excused Then
                        totalExcused += 1
                        totalExcusedMinutes = totalExcusedMinutes + (meetingDuration * 1)

                    ElseIf actual = 0 And Not excused Then
                        totalAbsent += 1
                        totalAbsentMinutes = totalAbsentMinutes + (meetingDuration * 1)

                    ElseIf actual = 1 And tardy Then
                        totalTardies += 1
                        totalTardiesMinutes = totalTardiesMinutes + (meetingDuration * 1)

                    ElseIf actual = 1 And Not tardy Then
                        totalPresent += 1
                        totalPresentMinutes = totalPresentMinutes + (meetingDuration * 1)
                    End If

                Case "Minutes", "Clock Hours"
                    'calculate duration of the meeting


                    meetingDuration = CType(row("Scheduled"), Integer)
                    totalScheduled += meetingDuration
                    If meetingDuration > actual And Not tardy Then
                        totalAbsent += meetingDuration - actual
                        totalPresent += actual
                    Else
                        totalPresent += actual

                        If meetingDuration > actual Then

                            ccounter += 1
                            If ccounter = tardiesMakingOneAbsence Then
                                absences += meetingDuration
                                minusPresent += actual
                                minusTardies += meetingDuration - actual
                                ccounter = 0
                            Else
                                absences += meetingDuration - actual
                            End If
                            totalTardies += meetingDuration - actual
                        Else
                            totalmakeUp += actual - meetingDuration
                        End If
                    End If


                Case "None"
                    'do nothing
            End Select

        Next

        'End If


        totalPresentAdjusted = totalPresent
        totalExcusedAdjusted = totalExcused
        totalAbsenceAdjusted = totalAbsent + totalExcused
        totalTardiesAdjusted = totalTardies

        Select Case attendanceType

            Case "Present Absent"

                'Adjust absences and tardies based on the number of tardies making one absence
                If Not tardiesMakingOneAbsence = 0 Then
                    Dim tempAbsences As Integer = totalTardies \ tardiesMakingOneAbsence
                    totalAbsenceAdjusted += tempAbsences
                    If totalTardiesAdjusted >= tempAbsences Then
                        totalTardiesAdjusted -= tempAbsences
                    End If
                End If
                totalPresentAdjusted += totalTardiesAdjusted
            Case "Minutes", "Clock Hours"

                'Adjust present, absences and tardies based on the scheduled time that was missed.
                If totalPresentAdjusted >= minusPresent Then
                    totalPresentAdjusted -= minusPresent
                End If
                If totalTardiesAdjusted >= minusTardies Then
                    totalTardiesAdjusted -= minusTardies
                End If
                If totalPresentAdjusted > 0 And totalTardiesAdjusted > 0 Then
                    totalAbsenceAdjusted += absences
                End If

        End Select

        'Always adjusted present must include adjusted tardies


        'return attendance display object
        Return New Common.AttendancePercentageInfo(totalPresent, totalAbsent, totalTardies, totalExcused, totalPresentAdjusted, totalAbsenceAdjusted, totalTardiesAdjusted, totalExcusedAdjusted, tardiesMakingOneAbsence, attendanceType, totalScheduled, totalmakeUp)
    End Function
    Public Function GetPercentageAttendanceInfoForReport_SP(ByVal stuEnrollId As String, ByVal cutOffDate As Date, ByVal attendanceType As String) As Common.AttendancePercentageInfo
        Dim dal As New DataAccess.PortalClsSectAttendanceDB()
        Dim dt As DataTable


        Dim tardiesMakingOneAbsence As Integer = 0
        tardiesMakingOneAbsence = dal.GetTardiesMakingAbsenceForPrgVersion(stuEnrollId, False)
        If (attendanceType = "minutes" Or attendanceType = "clock hours") Then
            dt = dal.GetAttendanceMinutes_SP(stuEnrollId, cutOffDate)
        Else
            dt = dal.GetAttendancePercentageDTForReport_SP(stuEnrollId, cutOffDate)
        End If



        Dim totalPresent As Integer = 0
        Dim totalAbsent As Integer = 0
        Dim totalTardies As Integer = 0
        Dim totalExcused As Integer = 0
        Dim totalScheduled As Decimal = 0.0
        Dim totalAttended As Decimal = 0.0
        Dim totalPresentAdjusted As Decimal
        Dim totalAbsenceAdjusted As Decimal
        Dim totalTardiesAdjusted As Decimal
        Dim totalExcusedAdjusted As Decimal
        Dim ccounter As Integer
        Dim absences As Integer
        Dim minusPresent As Integer
        Dim minusTardies As Integer
        Dim totalmakeUp As Integer = 0
        'If doesPrgVerTrackAttendance Then

        'Attendance is tracked at the program version level
        Dim totalScheduledMinutes As Integer = 0
        Dim totalExcusedMinutes As Integer = 0
        Dim totalAbsentMinutes As Integer = 0
        Dim totalTardiesMinutes As Integer = 0
        Dim totalPresentMinutes As Integer = 0
        For Each row As DataRow In dt.Rows
            Dim actual As Integer = CType(row("Actual"), Integer)
            Dim tardy As Boolean = CType(row("Tardy"), Boolean)
            Dim excused As Boolean = CType(row("Excused"), Boolean)
            Dim meetingDuration As Integer = 0


            Select Case attendanceType

                Case "present absent"
                    meetingDuration = CType(row("Duration"), Integer)
                    totalScheduled += 1
                    totalScheduledMinutes = totalScheduledMinutes + (meetingDuration * 1)
                    If actual = 0 And excused Then
                        totalExcused += 1
                        totalExcusedMinutes = totalExcusedMinutes + (meetingDuration * 1)

                    ElseIf actual = 0 And Not excused Then
                        totalAbsent += 1
                        totalAbsentMinutes = totalAbsentMinutes + (meetingDuration * 1)

                    ElseIf actual = 1 And tardy Then
                        totalTardies += 1
                        totalTardiesMinutes = totalTardiesMinutes + (meetingDuration * 1)

                    ElseIf actual = 1 And Not tardy Then
                        totalPresent += 1
                        totalPresentMinutes = totalPresentMinutes + (meetingDuration * 1)
                    End If

                Case "minutes", "clock hours"
                    'calculate duration of the meeting


                    meetingDuration = CType(row("Scheduled"), Integer)
                    totalScheduled += meetingDuration
                    If meetingDuration > actual And Not tardy Then
                        totalAbsent += meetingDuration - actual
                        totalPresent += actual
                    Else
                        totalPresent += actual

                        If meetingDuration > actual Then

                            ccounter += 1
                            If ccounter = tardiesMakingOneAbsence Then
                                absences += meetingDuration
                                minusPresent += actual
                                minusTardies += meetingDuration - actual
                                ccounter = 0
                            Else
                                absences += meetingDuration - actual
                            End If
                            totalTardies += meetingDuration - actual
                        Else
                            totalmakeUp += actual - meetingDuration
                        End If
                    End If


                Case "None"
                    'do nothing
            End Select

        Next

        'End If


        totalPresentAdjusted = totalPresent
        totalExcusedAdjusted = totalExcused
        totalAbsenceAdjusted = totalAbsent + totalExcused
        totalTardiesAdjusted = totalTardies

        Select Case attendanceType

            Case "present absent"

                'Adjust absences and tardies based on the number of tardies making one absence
                If Not tardiesMakingOneAbsence = 0 Then
                    Dim tempAbsences As Integer = totalTardies \ tardiesMakingOneAbsence
                    totalAbsenceAdjusted += tempAbsences
                    If totalTardiesAdjusted >= tempAbsences Then
                        totalTardiesAdjusted -= tempAbsences
                    End If
                End If
                totalPresentAdjusted += totalTardiesAdjusted
            Case "minutes", "clock hours"

                'Adjust present, absences and tardies based on the scheduled time that was missed.
                If totalPresentAdjusted >= minusPresent Then
                    totalPresentAdjusted -= minusPresent
                End If
                If totalTardiesAdjusted >= minusTardies Then
                    totalTardiesAdjusted -= minusTardies
                End If
                If totalPresentAdjusted > 0 And totalTardiesAdjusted > 0 Then
                    totalAbsenceAdjusted += absences
                End If

        End Select

        'Always adjusted present must include adjusted tardies


        'return attendance display object
        Return New Common.AttendancePercentageInfo(totalPresent, totalAbsent, totalTardies, totalExcused, totalPresentAdjusted, totalAbsenceAdjusted, totalTardiesAdjusted, totalExcusedAdjusted, tardiesMakingOneAbsence, attendanceType, totalScheduled, totalmakeUp)
    End Function

    Public Function GetOverallPercentageAttendanceInfo(ByVal stuEnrollId As String, campusId As String) As Common.AttendancePercentageInfo
        Dim dal As New DataAccess.PortalClsSectAttendanceDB()
        Dim dt As DataTable




        'check if attendance is tracked at program level
        Dim doesPrgVerTrackAttendance As Boolean = dal.DoesPrgVersionTrackAttendance(stuEnrollId, False)
        Dim attendanceType As String = ""
        Dim tardiesMakingOneAbsence As Integer = 0
        'discussed with Jeff and he wants to go with program version level now
        tardiesMakingOneAbsence = dal.GetTardiesMakingAbsenceForPrgVersion(stuEnrollId, False)
        If doesPrgVerTrackAttendance Then
            attendanceType = dal.GetPrgVersionAttendanceType(stuEnrollId, False)
        Else
            attendanceType = dal.GetStudentAttendanceType(stuEnrollId)
        End If



        If (attendanceType = "Minutes" Or attendanceType = "Clock Hours") Then
            dt = dal.GetAttendanceMinutes(stuEnrollId)
        Else
            dt = dal.GetAttendancePercentageDT(stuEnrollId)
        End If



        Dim totalPresent As Integer = 0
        Dim totalAbsent As Integer = 0
        Dim totalTardies As Integer = 0
        Dim totalExcused As Integer = 0
        Dim totalScheduled As Decimal = 0.0
        Dim totalAttended As Decimal = 0.0
        Dim totalPresentAdjusted As Decimal
        Dim totalAbsenceAdjusted As Decimal
        Dim totalTardiesAdjusted As Decimal
        Dim totalExcusedAdjusted As Decimal
        Dim ccounter As Integer
        Dim absences As Integer
        Dim minusPresent As Integer
        Dim minusTardies As Integer
        Dim totalMakeUp As Integer
        If doesPrgVerTrackAttendance Then

            'Attendance is tracked at the program version level

            For Each row As DataRow In dt.Rows
                Dim actual As Integer = CType(row("Actual"), Integer)
                Dim tardy As Boolean = CType(row("Tardy"), Boolean)
                Dim excused As Boolean = CType(row("Excused"), Boolean)
                Dim meetingDuration As Integer = 0
                Select Case attendanceType

                    Case "Present Absent"
                        totalScheduled += 1

                        If actual = 0 And excused Then
                            totalExcused += 1

                        ElseIf actual = 0 And Not excused Then
                            totalAbsent += 1

                        ElseIf actual = 1 And tardy Then
                            totalTardies += 1

                        ElseIf actual = 1 And Not tardy Then
                            totalPresent += 1
                        End If

                    Case "Minutes", "Clock Hours"
                        'calculate duration of the meeting
                        meetingDuration = CType(row("Scheduled"), Integer)
                        totalScheduled += meetingDuration
                        If meetingDuration > actual And Not tardy Then
                            totalAbsent += meetingDuration - actual
                            totalPresent += actual
                        Else
                            totalPresent += actual

                            If meetingDuration > actual Then

                                ccounter += 1
                                If ccounter = tardiesMakingOneAbsence Then
                                    absences += meetingDuration
                                    minusPresent += actual
                                    minusTardies += meetingDuration - actual
                                    ccounter = 0
                                Else
                                    absences += meetingDuration - actual
                                End If
                                totalTardies += meetingDuration - actual
                            Else
                                totalMakeUp += actual - meetingDuration
                            End If
                        End If

                    Case "None"
                        'do nothing
                End Select

            Next

        End If


        totalPresentAdjusted = totalPresent
        totalExcusedAdjusted = totalExcused
        'totalAbsenceAdjusted = totalAbsent + totalExcused
        totalAbsenceAdjusted = totalAbsent
        totalTardiesAdjusted = totalTardies

        Select Case attendanceType

            Case "Present Absent"

                'Adjust absences and tardies based on the number of tardies making one absence
                If Not tardiesMakingOneAbsence = 0 Then
                    Dim tempAbsences As Integer = totalTardies \ tardiesMakingOneAbsence
                    totalAbsenceAdjusted += tempAbsences
                    If totalTardiesAdjusted >= tempAbsences Then
                        totalTardiesAdjusted -= tempAbsences
                    End If
                End If
                'totalPresentAdjusted += totalTardiesAdjusted
            Case "Minutes", "Clock Hours"

                'Adjust present, absences and tardies based on the scheduled time that was missed.
                If totalPresentAdjusted >= minusPresent Then
                    totalPresentAdjusted -= minusPresent
                End If
                If totalTardiesAdjusted >= minusTardies Then
                    totalTardiesAdjusted -= minusTardies
                End If
                If totalPresentAdjusted > 0 And totalTardiesAdjusted > 0 Then
                    totalAbsenceAdjusted += absences
                End If

        End Select

        'Always adjusted present must include adjusted tardies


        'return attendance display object
        Return New Common.AttendancePercentageInfo(totalPresent, totalAbsent, totalTardies, totalExcused, totalPresentAdjusted, totalAbsenceAdjusted, totalTardiesAdjusted, totalExcusedAdjusted, tardiesMakingOneAbsence, attendanceType, totalScheduled, totalMakeUp)
    End Function
    Public Function GetScheduledMinutesForMeetDay(ByVal clsSectionId As String, ByVal sDay As String, ByVal termId As String, ByVal stuEnrollId As String, ClsSectMeetingId As String) As Integer
        Dim dbAttendance As New DataAccess.PortalClsSectAttendanceDB()
        Dim dtDays As New DataTable
        Dim dr As DataRow
        Dim startTime As DateTime
        Dim endTime As DateTime
        Dim schedLength As System.TimeSpan

        'Get the short day value
        'sDay = facAttendance.GetShortDayName(dtm)

        'Loop through the days that the class section meets and see which one this day matches one
        dtDays = dbAttendance.GetClsSectionMeetingDays(stuEnrollId, "'" + termId + "'", True)

        Dim arRow() As DataRow
        arRow = dtDays.Select("ClsSectionId='" & clsSectionId & "' AND WorkDaysDescrip='" & sDay & "' AND ClsSectMeetingId='" & ClsSectMeetingId & "'")

        If arRow.GetLength(0) > 0 Then
            dr = arRow(0)
            'For Each dr In arRow
            'If dr("WorkDaysDescrip").ToString() = sDay Then
            startTime = dr("TimeIntervalDescrip")
            endTime = dr("EndTime")
            'End If
            'Next
        End If

        schedLength = endTime.Subtract(startTime)

        Return CInt((schedLength.Hours * 60) + schedLength.Minutes)

    End Function
    'Private Function GetScheduledMinutesForMeetDay(ByVal dtm As DateTime, ByVal clsSectionId As String) As String
    '    Dim dbAttendance As New DataAccess.PortalPortalClsSectAttendanceDB()
    '    Dim sDay As String
    '    Dim dtDays As New DataTable
    '    Dim dr As DataRow
    '    Dim startTime As DateTime
    '    Dim endTime As DateTime
    '    Dim schedLength As System.TimeSpan

    '    'Get the short day value

    '    sDay = GetShortDayName(dtm)
    '    'Loop through the days that the class section meets and see which one this day matches one
    '    dtDays = dbAttendance.GetClsSectionMeetingDays(clsSectionId, True)

    '    Dim arRow() As DataRow
    '    arRow = dtDays.Select("ClsSectionId='" & clsSectionId & "' AND WorkDaysDescrip='" & sDay & "'")

    '    If arRow.GetLength(0) > 0 Then
    '        dr = arRow(0)
    '        'For Each dr In arRow
    '        'If dr("WorkDaysDescrip").ToString() = sDay Then
    '        startTime = dr("TimeIntervalDescrip")
    '        endTime = dr("EndTime")
    '        'End If
    '        'Next
    '    End If

    '    schedLength = endTime.Subtract(startTime)

    '    Return (schedLength.Hours * 60) + schedLength.Minutes

    'End Function
    Private Function GetShortDayName(ByVal dtm As DateTime) As String
        Select Case dtm.DayOfWeek
            Case DayOfWeek.Sunday
                Return "Sun"
            Case DayOfWeek.Monday
                Return "Mon"
            Case DayOfWeek.Tuesday
                Return "Tue"
            Case DayOfWeek.Wednesday
                Return "Wed"
            Case DayOfWeek.Thursday
                Return "Thu"
            Case DayOfWeek.Friday
                Return "Fri"
            Case DayOfWeek.Saturday
                Return "Sat"
        End Select
    End Function

    Private Function CampusId() As Object
        Throw New NotImplementedException
    End Function

End Class
