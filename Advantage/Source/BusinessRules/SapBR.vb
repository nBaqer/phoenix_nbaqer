Imports System.Data
Imports FAME.AdvantageV1.DataAccess
Imports FAME.AdvantageV1.Common
Imports System.Text
Imports System.Xml
Imports FAME.Advantage.Common
Imports System.Web
Imports AttendanceDB = FAME.AdvantageV1.DataAccess.AR.AttendanceDB

Namespace SAPCHK
    Public MustInherit Class SAP
        Public CredsCompleted As Decimal
        Public CredsAttempted As Decimal


        Public MustOverride Function IsMakingFASAP(ByVal StudentId As String, ByVal SAPInfo As SAPInfo, ByVal PrgVerId As String, ByVal user As String, ByVal gradeReps As String, Optional ByVal TermEDate As Date = #1/1/1001#, Optional campusId As String = "") As String
        Public MustOverride Function IsMakingSAP(ByVal StudentId As String, ByVal SAPInfo As SAPInfo, ByVal PrgVerId As String, ByVal user As String, ByVal gradeReps As String, Optional ByVal TermEDate As Date = #1/1/1001#, Optional campusId As String = "") As String
        Public Function ProcessQuantMinUnitTyp(ByVal QuantMinUnitTyp As String, ByVal StuEnrollId As String, ByVal PrgVerId As String, ByVal TrigUnit As String, ByVal TrigVal As Decimal, Optional ByVal TermEDate As Date = #1/1/1001#) As String
            Dim PercentCompltd As String = ""

            Select Case QuantMinUnitTyp
                Case "percent of credits attempted"
                    PercentCompltd = CalPercOfCredsCompltd(StuEnrollId, PrgVerId, TrigUnit, TrigVal, TermEDate).ToString
                Case "percent of courses attempted"
                    PercentCompltd = CalPercOfCoursesCompltd(StuEnrollId, PrgVerId, TrigUnit, TrigVal, TermEDate).ToString
                    ' Case "credits"
            End Select
            Return PercentCompltd
        End Function
        Public Function ProcessQualTyp(ByVal QualTyp As String, ByVal StuEnrollId As String, ByVal CredsCompleted As Decimal, ByVal PrgVerId As String, ByVal TrigUnit As String, ByVal TrigVal As Decimal, Optional ByVal TermEDate As Date = #1/1/1001#) As String
            Dim cgpa As String = ""

            Select Case QualTyp
                Case "an average of"

                Case "a CGPA of"
                    ' Calculate Cumulative GPA of student for all terms so far
                    cgpa = CalCumGPA(StuEnrollId, CredsCompleted, PrgVerId, TrigUnit, TrigVal, TermEDate).ToString
            End Select
            Return cgpa
        End Function
        Public Function CalPercOfCredsCompltd(ByVal StuEnrollId As String, ByVal ProgVerId As String, ByVal TrigUnit As String, ByVal TrigVal As Decimal, Optional ByVal TermEDate As Date = #1/1/1001#) As Decimal
            'Dim str As String
            Dim CredsCompltd As Decimal
            Dim CredsAttmptd As Decimal
            Dim PercentCompltd As Decimal
            Dim ds1 As New DataSet
            Dim ds2 As New DataSet
            Dim db As New SAPCheckDB
            Dim row1 As DataRow
            Dim row2 As DataRow

            ds1 = db.GetCredsAttempted(StuEnrollId, ProgVerId, TermEDate)
            ds2 = db.GetCredsCompltd(StuEnrollId, ProgVerId, TermEDate)

            If (TrigUnit = "Credits") Then
                For Each row1 In ds1.Tables(0).Rows
                    'row1("Credits") Changed to row1("CredsAttmptd") - modified 11/16/2004
                    If Not (row1.IsNull("CredsAttmptd")) Then
                        CredsAttmptd = row1("CredsAttmptd")
                        If (CredsAttmptd >= TrigVal) Then
                            Exit For
                        End If
                    End If

                Next

                'Get the total credits completed by this student.
                For Each row2 In ds2.Tables(0).Rows
                    'row1("Credits") Changed to row1("CredsCompltd") - modified 11/16/2004
                    If Not (row2.IsNull("CredsCompltd")) Then
                        CredsCompltd = row2("CredsCompltd")
                        If (CredsCompltd >= TrigVal) Then
                            Exit For
                        End If
                    End If

                Next

            Else 'for other trig unit types: Days/Weeks.
                'Get the total credits attempted by this student.
                For Each row1 In ds1.Tables(0).Rows
                    If Not (row1.IsNull("CredsAttmptd")) Then
                        CredsAttmptd = row1("CredsAttmptd")
                    End If
                Next
                ''Get the total credits completed by this student.
                For Each row2 In ds2.Tables(0).Rows
                    If Not (row2.IsNull("CredsCompltd")) Then
                        CredsCompltd = row2("CredsCompltd")
                    End If
                Next

            End If
            'Save the credscompltd for later use 
            CredsCompleted = CredsCompltd
            CredsAttempted = CredsAttmptd
            If CredsCompleted > 0 And CredsAttempted > 0 Then
                PercentCompltd = (CredsCompltd / CredsAttmptd) * 100
            End If
            Return PercentCompltd
        End Function
        Public Function CalPercOfCoursesCompltd(ByVal StuEnrollId As String, ByVal ProgVerId As String, ByVal TrigUnit As String, ByVal TrigVal As Decimal, Optional ByVal TermEDate As Date = #1/1/1001#) As Decimal
            'Dim str As String
            Dim CoursesAttmptd As Integer
            Dim CoursesCompltd As Integer
            Dim PercentCompltd As Decimal
            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim db As New SAPCheckDB
            'Dim row As DataRow

            ds = db.GetCoursesCompltd(StuEnrollId, ProgVerId, TermEDate)
            ds2 = db.GetCoursesAttempted(StuEnrollId, ProgVerId, TermEDate)
            If (TrigUnit = "Credits") Then
                'For Each row In ds.Tables(0).Rows
                CoursesCompltd += 1
                If (CoursesCompltd >= TrigVal) Then
                    'Exit For
                End If
                'Next
                'For Each row In ds2.Tables(0).Rows
                CoursesAttmptd = 1
                If (CoursesAttmptd >= TrigVal) Then
                    'Exit For
                End If
                'Next
            Else 'for other trig unit types: Days/Weeks.
                'For Each row In ds.Tables(0).Rows
                CoursesCompltd += 1
                'Next
                'For Each row In ds2.Tables(0).Rows
                CoursesAttmptd += 1
                'Next
            End If

            PercentCompltd = (CoursesCompltd / CoursesAttmptd) * 100
            Return PercentCompltd
        End Function
        Public Function CalCumGPA(ByVal StuEnrollId As String, ByVal CredsAttmptd As Decimal, ByVal ProgVerId As String, ByVal TrigUnit As String, ByVal TrigVal As Decimal, Optional ByVal TermEDate As Date = #1/1/1001#) As Decimal
            'Dim str As String
            Dim TotalCreds As Decimal
            'Dim PercentCompltd As Decimal
            Dim ds As New DataSet
            Dim db As New SAPCheckDB
            Dim row As DataRow
            Dim GrdWeight As Integer
            Dim CumGPA As Decimal
            Dim sumGPA As Decimal
            Dim numClasses As Integer

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim gpaMethod As String = MyAdvAppSettings.AppSettings("GPAMethod")

            ds = db.GetGPAInfo(StuEnrollId, ProgVerId, TrigUnit, TermEDate)


            '*************************************************************************************************
            'The following block of code was changed by BN (02/03/06) to accomodate the changes made to 
            'to the GPA calculation by Michelle in the Transcript & Term Progress.
            '***************************************************************************************************
            'GPA 

            If gpaMethod = "WeightedAVG" _
            And (New ProgVerDB()).CheckIfAPrgVersionTrackCredits(Guid.Parse(ProgVerId)) Then
                For Each row In ds.Tables(0).Rows
                    If Not row.IsNull("IsInGPA") Then
                        If row("IsInGPA") = 1 Or row("IsInGPA") = True Then
                            If Not row.IsNull("GPA") Then
                                TotalCreds += row("Credits")
                                GrdWeight += row("GPA") * row("Credits")

                            End If
                        End If
                    End If
                Next


                If GrdWeight > 0 And TotalCreds > 0 Then
                    CumGPA = Math.Round((GrdWeight / TotalCreds), 2).ToString("##.00")
                End If

            ElseIf gpaMethod = "SimpleAVG" Then
                For Each row In ds.Tables(0).Rows
                    If Not row.IsNull("IsInGPA") Then
                        If row("IsInGPA") = 1 Or row("IsInGPA") = True Then
                            If Not row.IsNull("GPA") Then
                                numClasses += 1
                                sumGPA += row("GPA")

                            End If
                        End If
                    End If
                Next

                If numClasses > 0 And sumGPA > 0 Then
                    CumGPA = Math.Round((sumGPA / numClasses), 2).ToString("##.00")

                End If


            End If



            '*************************************************************************************************
            'The following block of code was commented out by BN (02/03/06) to accomodate the changes made to 
            'to the GPA calculation by Michelle in the Transcript & Term Progress.
            '***************************************************************************************************
            'For Each row In ds.Tables(0).Rows
            '    If Not row.IsNull("IsInGPA") Then
            '        If row("IsInGPA") = 1 Or row("IsInGPA") = True Then
            '            If Not row.IsNull("GPA") Then
            '                numClasses += 1
            '                sumGPA += row("GPA")
            '            End If
            '        End If
            '    End If
            'Next
            'End If

            'If numClasses > 0 And sumGPA > 0 Then
            '    CumGPA = System.Math.Round((sumGPA / numClasses), 2).ToString("##.00")
            'End If
            '*************************************************************************************************

            Return CumGPA
        End Function
        Public Function GetAttendancePercentageByStudentEnrollment(ByVal StuEnrollId As String, ByVal cutOffDate As Date) As Decimal
            Dim br As New AttendancePercentageBR
            Dim AttendancePercentageObj As AttendancePercentageInfo

            AttendancePercentageObj = br.GetPercentageAttendanceInfo(StuEnrollId, cutOffDate)

            Return AttendancePercentageObj.PercentageAttendance
        End Function
        Public Function IsPass(ByVal dtWU As DataTable, ByVal stuEnrollId As String) As String()
            Dim wuSatisfied As Boolean = True
            Dim i As Integer
            Dim strCourse(dtWU.Rows.Count) As String
            Dim db As New StuProgressReportDB
            Dim minPassScore As Decimal
            Dim avgScore As Decimal
            minPassScore = db.GetMinPassingScore
            Dim cCount As Integer = 0
            Dim sapDb As New SAPCheckDB

            'Check the work units that have been atempted.
            'arrWU = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "' AND Score >= 0")
            If dtWU.Rows.Count > 0 Then

                For i = 0 To dtWU.Rows.Count - 1
                    'If dtWU.Rows(i)("SysComponentTypeId") <> "500" And dtWU.Rows(i)("SysComponentTypeId") <> "503" Then
                    If IsDBNull(dtWU.Rows(i)("score")) Then
                        If dtWU.Rows(i)("Required") = True Or dtWU.Rows(i)("MustPass") = True Then
                            wuSatisfied = False
                            strCourse(cCount) = "(" & dtWU.Rows(i)("CourseCode").ToString.Trim() & ")" & dtWU.Rows(i)("CourseDescrip")
                            cCount = cCount + 1
                            'Return False
                            'Exit For
                        End If
                    ElseIf dtWU.Rows(i)("score") >= 0 Then
                        If IsDBNull(dtWU.Rows(i)("score")) Then
                            If dtWU.Rows(i)("Required") = True Or dtWU.Rows(i)("MustPass") = True Then
                                wuSatisfied = False
                                strCourse(cCount) = "(" & dtWU.Rows(i)("CourseCode").ToString.Trim() & ")" & dtWU.Rows(i)("CourseDescrip")
                                cCount = cCount + 1
                                '    Return False
                                '    Exit For
                                'Else
                                '    wuSatisfied = True
                            End If
                        ElseIf dtWU.Rows(i)("score") < dtWU.Rows(i)("PassingGrade") Then
                            'First check if it is required

                            If dtWU.Rows(i)("Required") = True And dtWU.Rows(i)("MustPass") = True Then
                                wuSatisfied = False
                                strCourse(cCount) = "(" & dtWU.Rows(i)("CourseCode").ToString.Trim() & ")" & dtWU.Rows(i)("CourseDescrip")
                                cCount = cCount + 1
                                'Return False
                                'Exit For
                            End If
                        Else
                            avgScore = sapDb.GetCourseAverage(stuEnrollId, dtWU.Rows(i)("TermId").ToString, dtWU.Rows(i)("ReqId").ToString)
                            If avgScore <> 0 And avgScore < minPassScore Then
                                wuSatisfied = False
                                strCourse(cCount) = "(" & dtWU.Rows(i)("CourseCode").ToString.Trim() & ")" & dtWU.Rows(i)("CourseDescrip")
                                cCount = cCount + 1
                            End If
                        End If
                    End If
                    'End If
                Next
            End If
            ReDim Preserve strCourse(cCount + 1)
            If wuSatisfied = False Then
                Return RemoveDups(strCourse)
            Else
                Return Nothing
            End If
            'Return wuSatisfied




        End Function
        Public Function IsPass(ByVal dtWU As DataTable) As String()
            Dim wuSatisfied As Boolean = True
            Dim i As Integer


            Dim cCount As Integer = 0
            ' Dim dtWU.Rows() As DataRow
            'Check the work units that have been atempted.
            'dtWU.Rows = dtWU.Select("ReqId not in  (" & ReqId & ")")



            Dim strCourse(dtWU.Rows.Count) As String
            If dtWU.Rows.Count > 0 Then

                For i = 0 To dtWU.Rows.Count - 1
                    'If dtWU.Rows(i)("SysComponentTypeId") <> "500" And dtWU.Rows(i)("SysComponentTypeId") <> "503" Then
                    If IsDBNull(dtWU.Rows(i)("score")) Then
                        If dtWU.Rows(i)("Required") = True Or dtWU.Rows(i)("MustPass") = True Then
                            wuSatisfied = False
                            strCourse(cCount) = "(" & dtWU.Rows(i)("CourseCode").ToString.Trim() & ")" & dtWU.Rows(i)("CourseDescrip")
                            cCount = cCount + 1
                            'Return False
                            'Exit For
                        End If
                    ElseIf dtWU.Rows(i)("score") >= 0 Then
                        If IsDBNull(dtWU.Rows(i)("score")) Then
                            If dtWU.Rows(i)("Required") = True Or dtWU.Rows(i)("MustPass") = True Then
                                wuSatisfied = False
                                strCourse(cCount) = "(" & dtWU.Rows(i)("CourseCode").ToString.Trim() & ")" & dtWU.Rows(i)("CourseDescrip")
                                cCount = cCount + 1
                                '    Return False
                                '    Exit For
                                'Else
                                '    wuSatisfied = True
                            End If
                        ElseIf dtWU.Rows(i)("score") < dtWU.Rows(i)("PassingGrade") Then
                            'First check if it is required

                            If dtWU.Rows(i)("Required") = True And dtWU.Rows(i)("MustPass") = True Then
                                wuSatisfied = False
                                strCourse(cCount) = "(" & dtWU.Rows(i)("CourseCode").ToString.Trim() & ")" & dtWU.Rows(i)("CourseDescrip")
                                cCount = cCount + 1
                                'Return False
                                'Exit For
                            End If
                        End If
                    End If
                    'End If
                Next
            End If
            ReDim Preserve strCourse(cCount + 1)
            'ReqId.CopyTo(strCourse, ReqId.Length)
            If wuSatisfied = False Then
                Return RemoveDups(strCourse)
            Else
                Return Nothing
            End If
            'Return wuSatisfied




        End Function
        Public Function RemoveDups(ByVal items As String()) As String()
            Dim noDups As New ArrayList()
            For i As Integer = 0 To items.Length - 1
                If items(i) IsNot Nothing Then
                    If Not noDups.Contains(items(i).Trim()) Then
                        noDups.Add(items(i).Trim())
                    End If
                Else
                    Exit For
                End If
            Next
            'sorts list alphabetically 
            Dim uniqueItems As String() = New String(noDups.Count - 1) {}
            noDups.CopyTo(uniqueItems)
            Return uniqueItems
        End Function
    End Class
    Public Class SAPCredits
        Inherits SAP
        Public Overrides Function IsMakingSAP(ByVal StuEnrollId As String, ByVal SAPInfo As SAPInfo, ByVal PrgVerId As String, ByVal user As String, ByVal gradeReps As String, Optional ByVal TermEDate As Date = #1/1/1001#, Optional campusId As String = "") As String
            Dim db As New SAPCheckDB
            Dim TrigUnit As String
            Dim TrigOffsetTyp As String
            Dim QuantMinUnitTyp As String
            Dim QualTyp As String
            Dim QuantMinVal As Decimal
            Dim QualMinVal As Decimal
            Dim PercCompltd As Decimal
            Dim CumGPA As Decimal
            Dim currdate As Date
            Dim TrigOffSeq As Integer
            Dim Seq As Integer
            Dim resultString As New StringBuilder
            Dim passComments As New StringBuilder
            Dim SapChkInfo As New SAPCheckInfo
            Dim TrigVal As Decimal
            Dim SAPDetailId As String
            Dim AttendanceRequired As Decimal
            Dim AttendanceCompleted As Decimal

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            'Make certain to clear the ResultString variable
            resultString.Remove(0, resultString.Length)

            QuantMinVal = SAPInfo.QuantMinValue
            QualMinVal = SAPInfo.QualMinValue
            QualTyp = SAPInfo.QualType
            TrigOffsetTyp = SAPInfo.TrigOffsetTyp
            TrigOffSeq = SAPInfo.TrigOffsetSeq
            Seq = SAPInfo.Seq
            QuantMinUnitTyp = SAPInfo.QuantMinUnitType
            SAPDetailId = SAPInfo.SAPDetailId.ToString
            AttendanceRequired = SAPInfo.MinAttendanceValue

            If (TrigOffsetTyp = "start of term") Or (TrigOffsetTyp = "end of term") Then
                TermEDate = SAPInfo.TermEDate
            Else
                TermEDate = #1/1/1001#
            End If
            TrigUnit = SAPInfo.TrigUnit
            TrigVal = SAPInfo.TrigValue

            'Select Case Trim(TrigOffsetTyp)
            '    Case "Start Date"
            '        ''Get Student start date
            '        'ds3 = db.GetStuStartDate(StudentId)
            '        'CurrRow = ds3.Tables(0).Rows(0)
            '        ''Get the value of the StartDate Column in the table
            '        'StartDate = CurrRow("StartDate")

            '    Case "start of term"
            '        ''Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        'CurRow = ds4.Tables(0).Rows(TrigOffSeq)
            '        'TermId = CurRow("TermId")
            '        ''Get Results for this Term.
            '        ''ds5 = db.GetTermResults(TermId)

            '    Case "end of term"
            '        'If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
            '        '    resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
            '        'End If
            '        'Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        ''INSERT CODE TO LOOP THRU AND GET TERMIDs ONLY UPTO
            '        ''THE ROW(TRIGOFFSETSEQ)!!!!!

            '        'For Each row In ds4.Tables(0).Rows
            '        '    str &= row("TermId").ToString & ","
            '        '    int += 1
            '        '    If int = TrigOffSeq Then
            '        '        Exit For
            '        '    End If
            '        'Next
            '        'intpos = str.LastIndexOf(",")
            '        'str = str.Substring(0, intpos)
            '        '*************************************
            '        ' pos = InStrRev(str, ",")
            '        'str = Mid(str, 1, pos - 1)
            '        ' str = TermId
            'End Select

            'Check if Student is meeting Qualitative and Quantitative Reqs
            '**************************************************************************************
            'The following lines of code has been replaced to use the centralized component for 
            'computing the Percentage of Credits Earned (or Completed) instead of an independent function.
            'Modified By Michelle R. Rodriguez 05/15/06
            '**************************************************************************************
            'PercCompltd = CDec(ProcessQuantMinUnitTyp(QuantMinUnitTyp, StuEnrollId, PrgVerId, TrigUnit, TrigVal, TermEDate))
            If myAdvAppSettings.AppSettings("tracksapattendance", campusId).ToLower = "byday" Then
                Dim attUnitType As String = ""
                Dim dal As New ClsSectAttendanceDB()
                attUnitType = dal.GetPrgVersionAttendanceType(StuEnrollId, False)
                'If attUnitType.ToLower.IndexOf("present") > 0 Then
                If attUnitType <> Nothing Then
                    If attUnitType.ToString.Substring(0, 7).ToLower = "present" Then
                        attUnitType = "pa"
                    End If

                    'commented by Theresa G to get the attendance till the cutoffdate
                    'With (New FAME.AdvantageV1.DataAccess.AR.AttendanceDB).StudentSummaryForm(StuEnrollId, attUnitType)
                    With (New AttendanceDB).StudentSummaryFormForCutOffDate(StuEnrollId, attUnitType, SAPInfo.OffsetDate)
                        If .TotalHoursPresent <= 0 Or .TotalHoursSched <= 0 Then
                            PercCompltd = 0
                        Else
                            PercCompltd = Math.Round((.TotalHoursPresent / .TotalHoursSched) * 100, 2)
                        End If
                    End With
                End If
                AttendanceCompleted = PercCompltd
                PercCompltd = SAPInfo.PercCredsEarned
            Else
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    If SAPInfo.ActHoursAdjusted > 0 And SAPInfo.SchedHours > 0 Then
                        PercCompltd = SAPInfo.ActHoursAdjusted * 100 / SAPInfo.SchedHours
                    End If
                Else
                    PercCompltd = SAPInfo.PercCredsEarned
                End If
                AttendanceCompleted = Math.Round(GetAttendancePercentageByStudentEnrollment(StuEnrollId, SAPInfo.OffsetDate), 2)
            End If

            '**************************************************************************************
            'The following line of code has been replaced to use the centralized component for 
            'computing GPA instead of an independent function by BN 04/26/06
            '**************************************************************************************
            'CumGPA = CDec(ProcessQualTyp(QualTyp, StuEnrollId, CredsCompleted, PrgVerId, TrigUnit, TrigVal, TermEDate))
            CumGPA = SAPInfo.GPA
            CredsCompleted = SAPInfo.CredsEarned
            CredsAttempted = SAPInfo.CredsAttempted

            If PercCompltd >= QuantMinVal Then
                'Quantitative measure met
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                Else
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If

            ElseIf SAPInfo.QuantMinUnitTypId = "3" Then
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            Else
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            End If
            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                If SAPInfo.OverallAverage >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    'not met qual. meaasure
                    'Return msg
                    resultString.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                    'ErrMsgObj.ErrMsg = msg
                End If
                'If SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has  completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                End If
                'End If
            Else
                If CumGPA >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    resultString.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                End If

                'Check if student has met minimum credits required
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has  completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)








                End If
                If resultString.Length = 0 Then
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.MinTermGPA <> 0 Then
                        If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
                            resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        Else
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                Else
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.TermGPAOver <> 0 Then
                        If SAPInfo.TermGPA > SAPInfo.TermGPAOver Then
                            resultString.Remove(0, resultString.Length)
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                End If
            End If

            'Check if student has met minimum attendance required
            If AttendanceCompleted >= AttendanceRequired Then
                'Min Percent of Attendance Met
                passComments.Append("Student  has " + CType(AttendanceCompleted, String) + " % of attendance which is more than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            Else
                'not met Min Percent of Attendance
                'Return msg
                resultString.Append("Student only has " + CType(AttendanceCompleted, String) + " % of attendance which is less than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            End If

            If myAdvAppSettings.AppSettings("PassAllCoursesForSAPCheck").ToLower = "true" And myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then

                'If Not IsPass(db.GetWorkUnitResults(StuEnrollId, SAPInfo.OffsetDate)) Then
                '    resultString.Append("Student failed in any of the courses.")
                'End If
                Dim result() As String = IsPass(db.GetWorkUnitResults(StuEnrollId, SAPInfo.OffsetDate), StuEnrollId)
                Dim resultCount As Integer = 0
                If result IsNot Nothing Then
                    resultString.Append(" Student failed in the following Courses.")
                    For resultCount = 0 To result.Length - 1
                        If result(resultCount) IsNot Nothing Then
                            resultString.Append(result(resultCount) & ",")
                        Else
                            Exit For
                        End If
                    Next
                    resultString.Remove(resultString.Length - 1, 1)
                End If
            End If
            If myAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                SAPInfo.TrackIfStudentPassedSAPSpeedSkills = (New RegisterStudentsBR).CheckIfStudentPassedSpeedRequirementforSAP(SAPInfo.SAPDetailId.ToString, StuEnrollId.ToString)
                If SAPInfo.TrackIfStudentPassedSAPSpeedSkills = False Then
                    resultString.Append("Student has not satisfied the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                Else
                    passComments.Append("Student has satisfied the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                End If
            End If

            'Record the trigger as being performed.
            SapChkInfo.StudentId = XmlConvert.ToGuid(StuEnrollId)
            If (TrigOffsetTyp = "start of term") Then
                SapChkInfo.Period = TrigOffSeq - 1
            ElseIf (TrigOffsetTyp = "end of term") Then
                SapChkInfo.Period = TrigOffSeq
            ElseIf (TrigOffsetTyp = "start date") Then
                SapChkInfo.Period = Seq
            End If
            If resultString.Length <> 0 Then
                SapChkInfo.IsMakingSAP = False
            Else
                SapChkInfo.IsMakingSAP = True
            End If
            SapChkInfo.CredsEarned = CredsCompleted
            SapChkInfo.CredsAttempted = CredsAttempted
            'SapChkInfo.GPA = CumGPA
            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                SapChkInfo.GPA = SAPInfo.OverallAverage
            Else
                SapChkInfo.GPA = CumGPA
            End If
            SapChkInfo.DatePerf = DateTime.Now
            SapChkInfo.TermStartDate = SAPInfo.TermSDate
            'Added by Bhavana to capture reason 08/23/05
            If resultString.ToString.Length > 900 Then
                SapChkInfo.Comments = resultString.ToString.Substring(0, 900)
            Else
                SapChkInfo.Comments = resultString.ToString
            End If
            If SapChkInfo.IsMakingSAP Then SapChkInfo.Comments = passComments.ToString()
            'Added by Bhavana to store SAPDetailId in SAPChkResults table 11/02/05
            SapChkInfo.SAPDetailId = XmlConvert.ToGuid(SAPDetailId)
            SapChkInfo.PercAttendance = AttendanceCompleted
            SapChkInfo.FinCredsEarned = SAPInfo.FinCredsEarned
            SapChkInfo.PercCredsEarned = SAPInfo.PercCredsEarned
            db.AddTrigToDB(SapChkInfo, user)

            Return resultString.ToString
        End Function

        Public Overrides Function IsMakingFASAP(ByVal StuEnrollId As String, ByVal SAPInfo As SAPInfo, ByVal PrgVerId As String, ByVal user As String, ByVal gradeReps As String, Optional ByVal TermEDate As Date = #1/1/1001#, Optional campusId As String = "") As String
            Dim db As New SAPCheckDB
            Dim TrigUnit As String
            Dim TrigOffsetTyp As String
            Dim QuantMinUnitTyp As String
            Dim QualTyp As String
            Dim QuantMinVal As Decimal
            Dim QualMinVal As Decimal
            Dim PercCompltd As Decimal
            Dim CumGPA As Decimal
            'Dim currdate As Date
            Dim TrigOffSeq As Integer
            Dim Seq As Integer
            Dim resultString As New StringBuilder
            Dim passComments As New StringBuilder
            Dim SapChkInfo As New SAPCheckInfo
            Dim TrigVal As Decimal
            Dim SAPDetailId As String
            Dim AttendanceRequired As Decimal
            Dim AttendanceCompleted As Decimal

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            'Make certain to clear the ResultString variable
            resultString.Remove(0, resultString.Length)

            QuantMinVal = SAPInfo.QuantMinValue
            QualMinVal = SAPInfo.QualMinValue
            QualTyp = SAPInfo.QualType
            TrigOffsetTyp = SAPInfo.TrigOffsetTyp
            TrigOffSeq = SAPInfo.TrigOffsetSeq
            Seq = SAPInfo.Seq
            QuantMinUnitTyp = SAPInfo.QuantMinUnitType
            SAPDetailId = SAPInfo.SAPDetailId.ToString
            AttendanceRequired = SAPInfo.MinAttendanceValue

            If (TrigOffsetTyp = "start of term") Or (TrigOffsetTyp = "end of term") Then
                TermEDate = SAPInfo.TermEDate
            Else
                TermEDate = #1/1/1001#
            End If
            TrigUnit = SAPInfo.TrigUnit
            TrigVal = SAPInfo.TrigValue

            'Select Case Trim(TrigOffsetTyp)
            '    Case "Start Date"
            '        ''Get Student start date
            '        'ds3 = db.GetStuStartDate(StudentId)
            '        'CurrRow = ds3.Tables(0).Rows(0)
            '        ''Get the value of the StartDate Column in the table
            '        'StartDate = CurrRow("StartDate")

            '    Case "start of term"
            '        ''Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        'CurRow = ds4.Tables(0).Rows(TrigOffSeq)
            '        'TermId = CurRow("TermId")
            '        ''Get Results for this Term.
            '        ''ds5 = db.GetTermResults(TermId)

            '    Case "end of term"
            '        'If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
            '        '    resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
            '        'End If
            '        'Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        ''INSERT CODE TO LOOP THRU AND GET TERMIDs ONLY UPTO
            '        ''THE ROW(TRIGOFFSETSEQ)!!!!!

            '        'For Each row In ds4.Tables(0).Rows
            '        '    str &= row("TermId").ToString & ","
            '        '    int += 1
            '        '    If int = TrigOffSeq Then
            '        '        Exit For
            '        '    End If
            '        'Next
            '        'intpos = str.LastIndexOf(",")
            '        'str = str.Substring(0, intpos)
            '        '*************************************
            '        ' pos = InStrRev(str, ",")
            '        'str = Mid(str, 1, pos - 1)
            '        ' str = TermId
            'End Select

            'Check if Student is meeting Qualitative and Quantitative Reqs
            '**************************************************************************************
            'The following lines of code has been replaced to use the centralized component for 
            'computing the Percentage of Credits Earned (or Completed) instead of an independent function.
            'Modified By Michelle R. Rodriguez 05/15/06
            '**************************************************************************************
            'PercCompltd = CDec(ProcessQuantMinUnitTyp(QuantMinUnitTyp, StuEnrollId, PrgVerId, TrigUnit, TrigVal, TermEDate))
            If myAdvAppSettings.AppSettings("tracksapattendance", campusId).ToLower = "byday" Then
                Dim attUnitType As String = ""
                Dim dal As New ClsSectAttendanceDB()
                attUnitType = dal.GetPrgVersionAttendanceType(StuEnrollId, False)
                'If attUnitType.ToLower.IndexOf("present") > 0 Then
                If attUnitType <> Nothing Then
                    If attUnitType.ToString.Substring(0, 7).ToLower = "present" Then
                        attUnitType = "pa"
                    End If

                    'commented by Theresa G to get the attendance till the cutoffdate
                    'With (New FAME.AdvantageV1.DataAccess.AR.AttendanceDB).StudentSummaryForm(StuEnrollId, attUnitType)
                    With (New AttendanceDB).StudentSummaryFormForCutOffDate(StuEnrollId, attUnitType, SAPInfo.OffsetDate)
                        If .TotalHoursPresent <= 0 Or .TotalHoursSched <= 0 Then
                            PercCompltd = 0
                        Else
                            PercCompltd = Math.Round((.TotalHoursPresent / .TotalHoursSched) * 100, 2)
                        End If
                    End With
                End If
                AttendanceCompleted = PercCompltd
                PercCompltd = SAPInfo.PercCredsEarned
            Else
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    If SAPInfo.ActHoursAdjusted > 0 And SAPInfo.SchedHours > 0 Then
                        PercCompltd = SAPInfo.ActHoursAdjusted * 100 / SAPInfo.SchedHours
                    End If
                Else
                    PercCompltd = SAPInfo.PercCredsEarned
                End If
                AttendanceCompleted = Math.Round(GetAttendancePercentageByStudentEnrollment(StuEnrollId, SAPInfo.OffsetDate), 2)
            End If

            '**************************************************************************************
            'The following line of code has been replaced to use the centralized component for 
            'computing GPA instead of an independent function by BN 04/26/06
            '**************************************************************************************
            'CumGPA = CDec(ProcessQualTyp(QualTyp, StuEnrollId, CredsCompleted, PrgVerId, TrigUnit, TrigVal, TermEDate))
            CumGPA = SAPInfo.GPA
            CredsCompleted = SAPInfo.CredsEarned

            If PercCompltd >= QuantMinVal Then
                'Quantitative measure met
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                Else
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If

            ElseIf SAPInfo.QuantMinUnitTypId = "3" Then
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            Else
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            End If
            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                If SAPInfo.OverallAverage >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    'not met qual. meaasure
                    'Return msg
                    resultString.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                    'ErrMsgObj.ErrMsg = msg
                End If
                'If SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has  completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                End If
                'End If
            Else
                If CumGPA >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    resultString.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                End If

                'Check if student has met minimum credits required
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has  completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)


                End If
                If resultString.Length = 0 Then
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.MinTermGPA <> 0 Then
                        If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
                            resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        Else
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                Else
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.TermGPAOver <> 0 Then
                        If SAPInfo.TermGPA > SAPInfo.TermGPAOver Then
                            resultString.Remove(0, resultString.Length)
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                End If
            End If

            'Check if student has met minimum attendance required
            If AttendanceCompleted >= AttendanceRequired Then
                'Min Percent of Attendance Met
                passComments.Append("Student  has " + CType(AttendanceCompleted, String) + " % of attendance which is more than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            Else
                'not met Min Percent of Attendance
                'Return msg
                resultString.Append("Student only has " + CType(AttendanceCompleted, String) + " % of attendance which is less than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            End If

            If myAdvAppSettings.AppSettings("PassAllCoursesForSAPCheck").ToLower = "true" And myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then

                'If Not IsPass(db.GetWorkUnitResults(StuEnrollId, SAPInfo.OffsetDate)) Then
                '    resultString.Append("Student failed in any of the courses.")
                'End If
                Dim result() As String = IsPass(db.GetWorkUnitResults(StuEnrollId, SAPInfo.OffsetDate), StuEnrollId)
                Dim resultCount As Integer = 0
                If result IsNot Nothing Then
                    resultString.Append(" Student failed in the following Courses.")
                    For resultCount = 0 To result.Length - 1
                        If result(resultCount) IsNot Nothing Then
                            resultString.Append(result(resultCount) & ",")
                        Else
                            Exit For
                        End If
                    Next
                    resultString.Remove(resultString.Length - 1, 1)
                End If
            End If
            If myAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                SAPInfo.TrackIfStudentPassedSAPSpeedSkills = (New RegisterStudentsBR).CheckIfStudentPassedSpeedRequirementforSAP(SAPInfo.SAPDetailId.ToString, StuEnrollId.ToString)
                If SAPInfo.TrackIfStudentPassedSAPSpeedSkills = False Then
                    resultString.Append("Student has not satisified the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                Else
                    passComments.Append("Student has satisified the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                End If
            End If

            'Record the trigger as being performed.
            SapChkInfo.StudentId = XmlConvert.ToGuid(StuEnrollId)
            If (TrigOffsetTyp = "start of term") Then
                SapChkInfo.Period = TrigOffSeq - 1
            ElseIf (TrigOffsetTyp = "end of term") Then
                SapChkInfo.Period = TrigOffSeq
            ElseIf (TrigOffsetTyp = "start date") Then
                SapChkInfo.Period = Seq
            End If
            If resultString.Length <> 0 Then
                SapChkInfo.IsMakingSAP = False
            Else
                SapChkInfo.IsMakingSAP = True
            End If
            SapChkInfo.CredsEarned = CredsCompleted
            SapChkInfo.CredsAttempted = CredsAttempted
            'SapChkInfo.GPA = CumGPA
            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                SapChkInfo.GPA = SAPInfo.OverallAverage
            Else
                SapChkInfo.GPA = CumGPA
            End If
            SapChkInfo.DatePerf = DateTime.Now
            SapChkInfo.TermStartDate = SAPInfo.TermSDate
            'Added by Bhavana to capture reason 08/23/05
            If resultString.ToString.Length > 900 Then
                SapChkInfo.Comments = resultString.ToString.Substring(0, 900)
            Else
                SapChkInfo.Comments = resultString.ToString
            End If
            If SapChkInfo.IsMakingSAP Then SapChkInfo.Comments = passComments.ToString()
            'Added by Bhavana to store SAPDetailId in SAPChkResults table 11/02/05
            SapChkInfo.SAPDetailId = XmlConvert.ToGuid(SAPDetailId)
            SapChkInfo.PercAttendance = AttendanceCompleted
            SapChkInfo.FinCredsEarned = SAPInfo.FinCredsEarned
            SapChkInfo.PercCredsEarned = SAPInfo.PercCredsEarned
            db.AddTrigFASAPToDB(SapChkInfo, user)

            Return resultString.ToString
        End Function
    End Class

    Public Class SAPDays
        Inherits SAP
        Public Overrides Function IsMakingSAP(ByVal StudentId As String, ByVal SAPInfo As SAPInfo, ByVal PrgVerId As String, ByVal user As String, ByVal gradeReps As String, Optional ByVal TermEDate As Date = #1/1/1001#, Optional campusId As String = "") As String
            Dim db As New SAPCheckDB
            Dim TrigUnit As String
            Dim TrigOffsetTyp As String
            Dim QuantMinUnitTyp As String
            Dim QualTyp As String
            Dim QuantMinVal As Decimal
            Dim QualMinVal As Decimal
            Dim PercCompltd As Decimal
            Dim CumGPA As Decimal
            'Dim currdate As Date
            Dim TrigOffSeq As Integer
            Dim Seq As Integer
            Dim resultString As New StringBuilder
            Dim SapChkInfo As New SAPCheckInfo
            Dim TrigVal As Decimal
            Dim SAPDetailId As String
            Dim AttendanceRequired As Decimal
            Dim AttendanceCompleted As Decimal
            Dim passComments As New StringBuilder

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


            'Make certain to clear the ResultString variable
            resultString.Remove(0, resultString.Length)

            QuantMinVal = SAPInfo.QuantMinValue
            QualMinVal = SAPInfo.QualMinValue
            QualTyp = SAPInfo.QualType
            TrigOffsetTyp = SAPInfo.TrigOffsetTyp
            TrigOffSeq = SAPInfo.TrigOffsetSeq
            Seq = SAPInfo.Seq
            QuantMinUnitTyp = SAPInfo.QuantMinUnitType
            If (TrigOffsetTyp = "start of term") Or (TrigOffsetTyp = "end of term") Or (TrigOffsetTyp = "start date") Then
                TermEDate = SAPInfo.TermEDate
            Else
                TermEDate = #1/1/1001#
            End If
            TrigUnit = SAPInfo.TrigUnit
            TrigVal = SAPInfo.TrigValue
            SAPDetailId = SAPInfo.SAPDetailId.ToString
            AttendanceRequired = SAPInfo.MinAttendanceValue
            'Select Case Trim(TrigOffsetTyp)
            '    Case "Start Date"
            '        ''Get Student start date
            '        'ds3 = db.GetStuStartDate(StudentId)
            '        'CurrRow = ds3.Tables(0).Rows(0)
            '        ''Get the value of the StartDate Column in the table
            '        'StartDate = CurrRow("StartDate")

            '    Case "start of term"
            '        ''Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        'CurRow = ds4.Tables(0).Rows(TrigOffSeq)
            '        'TermId = CurRow("TermId")
            '        ''Get Results for this Term.
            '        ''ds5 = db.GetTermResults(TermId)

            '    Case "end of term"


            '        'Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        ''INSERT CODE TO LOOP THRU AND GET TERMIDs ONLY UPTO
            '        ''THE ROW(TRIGOFFSETSEQ)!!!!!

            '        'For Each row In ds4.Tables(0).Rows
            '        '    str &= row("TermId").ToString & ","
            '        '    int += 1
            '        '    If int = TrigOffSeq Then
            '        '        Exit For
            '        '    End If
            '        'Next
            '        'intpos = str.LastIndexOf(",")
            '        'str = str.Substring(0, intpos)
            '        '*************************************
            '        ' pos = InStrRev(str, ",")
            '        'str = Mid(str, 1, pos - 1)
            '        ' str = TermId
            'End Select

            'Check if Student is meeting Qualitative and Quantitative Reqs
            '**************************************************************************************
            'The following lines of code has been replaced to use the centralized component for 
            'computing the Percentage of Credits Earned (or Completed) instead of an independent function.
            'Modified by Michelle R. Rodriguez 05/15/06
            '**************************************************************************************
            'PercCompltd = CDec(ProcessQuantMinUnitTyp(QuantMinUnitTyp, StudentId, PrgVerId, TrigUnit, TrigVal, TermEDate))


            If myAdvAppSettings.AppSettings("tracksapattendance", campusId).ToLower = "byday" Then
                Dim attUnitType As String = ""
                Dim dal As New ClsSectAttendanceDB()
                Dim externScheduleHrs As Decimal = 0.0
                Dim externActualHrs As Decimal = 0.0
                'Dim stuEnrollId As String = (New SAPActualHours).getEnrollmentId(StudentId, PrgVerId)
                attUnitType = dal.GetPrgVersionAttendanceType(StudentId, False)
                If attUnitType <> Nothing Then
                    If attUnitType.ToString.Substring(0, 7).ToLower = "present" Then
                        attUnitType = "pa"
                    End If
                    If SAPInfo.TrackExternAttendance = True Then
                        Dim externAttendance As Decimal() = (New SapDB).GetHoursForExternships(StudentId, SAPInfo.OffsetDate)
                        externScheduleHrs = externAttendance(0)
                        externActualHrs = externAttendance(1)
                    End If
                    If SAPInfo.TrackExternAttendance = True Then

                        With (New AttendanceDB).StudentSummaryFormForCutOffDate(StudentId, attUnitType, SAPInfo.OffsetDate)
                            If (.TotalHoursPresent + externActualHrs) <= 0 Or (.TotalHoursSched + externScheduleHrs) <= 0 Then
                                PercCompltd = 0
                            Else
                                PercCompltd = Math.Round(((.TotalHoursPresent + externActualHrs) / (.TotalHoursSched + externScheduleHrs)) * 100, 2)
                            End If
                        End With
                    Else
                        With (New AttendanceDB).StudentSummaryFormForCutOffDate(StudentId, attUnitType, SAPInfo.OffsetDate)
                            If .TotalHoursPresent <= 0 Or .TotalHoursSched <= 0 Then
                                PercCompltd = 0
                            Else
                                PercCompltd = Math.Round((.TotalHoursPresent / .TotalHoursSched) * 100, 2)
                            End If
                        End With
                    End If

                End If
                AttendanceCompleted = PercCompltd
                PercCompltd = SAPInfo.PercCredsEarned
            Else
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    If SAPInfo.ActHoursAdjusted > 0 And SAPInfo.SchedHours > 0 Then
                        PercCompltd = SAPInfo.ActHoursAdjusted * 100 / SAPInfo.SchedHours
                    End If
                Else
                    PercCompltd = SAPInfo.PercCredsEarned
                End If
                AttendanceCompleted = Math.Round(GetAttendancePercentageByStudentEnrollment(StudentId, SAPInfo.OffsetDate), 2)
            End If
            CredsCompleted = SAPInfo.CredsEarned

            '**************************************************************************************
            'The following line of code has been replaced to use the centralized component for 
            'computing GPA instead of an independent function by BN 04/26/06
            '**************************************************************************************
            'CumGPA = CDec(ProcessQualTyp(QualTyp, StudentId, CredsCompleted, PrgVerId, TrigUnit, TrigVal, TermEDate))
            CumGPA = SAPInfo.GPA


            If PercCompltd >= QuantMinVal Then
                'Quantitative measure met
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                Else
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If

            ElseIf SAPInfo.QuantMinUnitTypId = "3" Then
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            Else
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            End If


            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                If SAPInfo.OverallAverage >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    'not met qual. meaasure
                    'Return msg
                    resultString.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                    'ErrMsgObj.ErrMsg = msg
                End If
                'If SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has  completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                End If
                'End If
            Else

                If CumGPA >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    resultString.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                End If

                'Check if student has met minimum credits required
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has  completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                End If

                If resultString.Length = 0 Then
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.MinTermGPA <> 0 Then
                        If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
                            resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        Else
                            'met
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                Else
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.TermGPAOver <> 0 Then
                        If SAPInfo.TermGPA > SAPInfo.TermGPAOver Then
                            resultString.Remove(0, resultString.Length)
                            'met
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                End If
            End If

            'Check if student has met minimum attendance required
            If AttendanceCompleted >= AttendanceRequired Then
                'Min Percent of Attendance Met
                passComments.Append("Student  has " + CType(AttendanceCompleted, String) + " % of attendance which is more than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            Else
                'not met Min Percent of Attendance
                'Return msg
                resultString.Append("Student only has " + CType(AttendanceCompleted, String) + " % of attendance which is less than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            End If

            If myAdvAppSettings.AppSettings("PassAllCoursesForSAPCheck").ToLower = "true" And myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then


                Dim result() As String = IsPass(db.GetWorkUnitResults(StudentId, SAPInfo.OffsetDate), StudentId)
                Dim resultCount As Integer = 0
                If result IsNot Nothing Then
                    resultString.Append(" Student failed in the following Courses.")
                    For resultCount = 0 To result.Length - 1
                        If result(resultCount) IsNot Nothing Then
                            resultString.Append(result(resultCount) & ",")
                        Else
                            Exit For
                        End If
                    Next
                    resultString.Remove(resultString.Length - 1, 1)
                End If


            End If
            '18874: QA: Need to expand the Short Hand SAP check to all the types of trigger points. 
            If myAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                SAPInfo.TrackIfStudentPassedSAPSpeedSkills = (New RegisterStudentsBR).CheckIfStudentPassedSpeedRequirementforSAP(SAPInfo.SAPDetailId.ToString, StudentId.ToString)
                If SAPInfo.TrackIfStudentPassedSAPSpeedSkills = False Then
                    resultString.Append("Student has not satisified the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                Else
                    passComments.Append("Student has  satisified the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                End If
            End If

            'Record the trigger as being performed.
            SapChkInfo.StudentId = XmlConvert.ToGuid(StudentId)

            SapChkInfo.Period = SAPInfo.Period
            If resultString.Length <> 0 Then
                SapChkInfo.IsMakingSAP = False
            Else
                SapChkInfo.IsMakingSAP = True
            End If
            SapChkInfo.CredsEarned = CredsCompleted
            SapChkInfo.CredsAttempted = CredsAttempted
            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                SapChkInfo.GPA = SAPInfo.OverallAverage
            Else
                SapChkInfo.GPA = CumGPA
            End If

            SapChkInfo.DatePerf = DateTime.Now
            SapChkInfo.CheckPointDate = SAPInfo.OffsetDate
            SapChkInfo.TermStartDate = SAPInfo.TermSDate
            'Added By Balaji on 11/17/2004 to capture reason
            'Modification starts here 
            If resultString.ToString.Length > 900 Then
                SapChkInfo.Comments = resultString.ToString.Substring(0, 900)
            Else
                SapChkInfo.Comments = resultString.ToString
            End If
            If SapChkInfo.IsMakingSAP Then SapChkInfo.Comments = passComments.ToString()
            'Modification ends here

            'Added by Bhavana to store SAPDetailId in SAPChkResults table 11/02/05
            SapChkInfo.SAPDetailId = XmlConvert.ToGuid(SAPDetailId)
            SapChkInfo.PercAttendance = AttendanceCompleted
            SapChkInfo.FinCredsEarned = SAPInfo.FinCredsEarned
            SapChkInfo.PercCredsEarned = SAPInfo.PercCredsEarned
            db.AddTrigToDB(SapChkInfo, user)



            Return resultString.ToString
        End Function

        Public Overrides Function IsMakingFASAP(ByVal StudentId As String, ByVal SAPInfo As SAPInfo, ByVal PrgVerId As String, ByVal user As String, ByVal gradeReps As String, Optional ByVal TermEDate As Date = #1/1/1001#, Optional campusId As String = "") As String
            Dim db As New SAPCheckDB
            Dim TrigUnit As String
            Dim TrigOffsetTyp As String
            Dim QuantMinUnitTyp As String
            Dim QualTyp As String
            Dim QuantMinVal As Decimal
            Dim QualMinVal As Decimal
            Dim PercCompltd As Decimal
            Dim CumGPA As Decimal
            'Dim currdate As Date
            Dim TrigOffSeq As Integer
            Dim Seq As Integer
            Dim resultString As New StringBuilder
            Dim SapChkInfo As New SAPCheckInfo
            Dim TrigVal As Decimal
            Dim SAPDetailId As String
            Dim AttendanceRequired As Decimal
            Dim AttendanceCompleted As Decimal
            Dim passComments As New StringBuilder

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            'Make certain to clear the ResultString variable
            resultString.Remove(0, resultString.Length)

            QuantMinVal = SAPInfo.QuantMinValue
            QualMinVal = SAPInfo.QualMinValue
            QualTyp = SAPInfo.QualType
            TrigOffsetTyp = SAPInfo.TrigOffsetTyp
            TrigOffSeq = SAPInfo.TrigOffsetSeq
            Seq = SAPInfo.Seq
            QuantMinUnitTyp = SAPInfo.QuantMinUnitType
            If (TrigOffsetTyp = "start of term") Or (TrigOffsetTyp = "end of term") Or (TrigOffsetTyp = "start date") Then
                TermEDate = SAPInfo.TermEDate
            Else
                TermEDate = #1/1/1001#
            End If
            TrigUnit = SAPInfo.TrigUnit
            TrigVal = SAPInfo.TrigValue
            SAPDetailId = SAPInfo.SAPDetailId.ToString
            AttendanceRequired = SAPInfo.MinAttendanceValue
            'Select Case Trim(TrigOffsetTyp)
            '    Case "Start Date"
            '        ''Get Student start date
            '        'ds3 = db.GetStuStartDate(StudentId)
            '        'CurrRow = ds3.Tables(0).Rows(0)
            '        ''Get the value of the StartDate Column in the table
            '        'StartDate = CurrRow("StartDate")

            '    Case "start of term"
            '        ''Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        'CurRow = ds4.Tables(0).Rows(TrigOffSeq)
            '        'TermId = CurRow("TermId")
            '        ''Get Results for this Term.
            '        ''ds5 = db.GetTermResults(TermId)

            '    Case "end of term"


            '        'Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        ''INSERT CODE TO LOOP THRU AND GET TERMIDs ONLY UPTO
            '        ''THE ROW(TRIGOFFSETSEQ)!!!!!

            '        'For Each row In ds4.Tables(0).Rows
            '        '    str &= row("TermId").ToString & ","
            '        '    int += 1
            '        '    If int = TrigOffSeq Then
            '        '        Exit For
            '        '    End If
            '        'Next
            '        'intpos = str.LastIndexOf(",")
            '        'str = str.Substring(0, intpos)
            '        '*************************************
            '        ' pos = InStrRev(str, ",")
            '        'str = Mid(str, 1, pos - 1)
            '        ' str = TermId
            'End Select

            'Check if Student is meeting Qualitative and Quantitative Reqs
            '**************************************************************************************
            'The following lines of code has been replaced to use the centralized component for 
            'computing the Percentage of Credits Earned (or Completed) instead of an independent function.
            'Modified by Michelle R. Rodriguez 05/15/06
            '**************************************************************************************
            'PercCompltd = CDec(ProcessQuantMinUnitTyp(QuantMinUnitTyp, StudentId, PrgVerId, TrigUnit, TrigVal, TermEDate))


            If myAdvAppSettings.AppSettings("tracksapattendance", campusId).ToLower = "byday" Then
                Dim attUnitType As String = ""
                Dim dal As New ClsSectAttendanceDB()
                Dim externScheduleHrs As Decimal = 0.0
                Dim externActualHrs As Decimal = 0.0
                'Dim stuEnrollId As String = (New SAPActualHours).getEnrollmentId(StudentId, PrgVerId)
                attUnitType = dal.GetPrgVersionAttendanceType(StudentId, False)
                If attUnitType <> Nothing Then
                    If attUnitType.ToString.Substring(0, 7).ToLower = "present" Then
                        attUnitType = "pa"
                    End If
                    If SAPInfo.TrackExternAttendance = True Then
                        Dim externAttendance As Decimal() = (New SapDB).GetHoursForExternships(StudentId, SAPInfo.OffsetDate)
                        externScheduleHrs = externAttendance(0)
                        externActualHrs = externAttendance(1)
                    End If
                    If SAPInfo.TrackExternAttendance = True Then

                        With (New AttendanceDB).StudentSummaryFormForCutOffDate(StudentId, attUnitType, SAPInfo.OffsetDate)
                            If (.TotalHoursPresent + externActualHrs) <= 0 Or (.TotalHoursSched + externScheduleHrs) <= 0 Then
                                PercCompltd = 0
                            Else
                                PercCompltd = Math.Round(((.TotalHoursPresent + externActualHrs) / (.TotalHoursSched + externScheduleHrs)) * 100, 2)
                            End If
                        End With
                    Else
                        With (New AttendanceDB).StudentSummaryFormForCutOffDate(StudentId, attUnitType, SAPInfo.OffsetDate)
                            If .TotalHoursPresent <= 0 Or .TotalHoursSched <= 0 Then
                                PercCompltd = 0
                            Else
                                PercCompltd = Math.Round((.TotalHoursPresent / .TotalHoursSched) * 100, 2)
                            End If
                        End With
                    End If

                End If
                AttendanceCompleted = PercCompltd
                PercCompltd = SAPInfo.PercCredsEarned
            Else
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    If SAPInfo.ActHoursAdjusted > 0 And SAPInfo.SchedHours > 0 Then
                        PercCompltd = SAPInfo.ActHoursAdjusted * 100 / SAPInfo.SchedHours
                    End If
                Else
                    PercCompltd = SAPInfo.PercCredsEarned
                End If
                AttendanceCompleted = Math.Round(GetAttendancePercentageByStudentEnrollment(StudentId, SAPInfo.OffsetDate), 2)
            End If
            CredsCompleted = SAPInfo.CredsEarned

            '**************************************************************************************
            'The following line of code has been replaced to use the centralized component for 
            'computing GPA instead of an independent function by BN 04/26/06
            '**************************************************************************************
            'CumGPA = CDec(ProcessQualTyp(QualTyp, StudentId, CredsCompleted, PrgVerId, TrigUnit, TrigVal, TermEDate))
            CumGPA = SAPInfo.GPA


            If PercCompltd >= QuantMinVal Then
                'Quantitative measure met
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                Else
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If

            ElseIf SAPInfo.QuantMinUnitTypId = "3" Then
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            Else
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            End If


            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                If SAPInfo.OverallAverage >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    'not met qual. meaasure
                    'Return msg
                    resultString.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                    'ErrMsgObj.ErrMsg = msg
                End If
                'If SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has  completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                End If
                'End If
            Else

                If CumGPA >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    resultString.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                End If

                'Check if student has met minimum credits required
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has  completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                End If

                If resultString.Length = 0 Then
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.MinTermGPA <> 0 Then
                        If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
                            resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        Else
                            'met
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                Else
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.TermGPAOver <> 0 Then
                        If SAPInfo.TermGPA > SAPInfo.TermGPAOver Then
                            resultString.Remove(0, resultString.Length)
                            'met
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                End If
            End If



            'Check if student has met minimum attendance required
            If AttendanceCompleted >= AttendanceRequired Then
                'Min Percent of Attendance Met
                passComments.Append("Student  has " + CType(AttendanceCompleted, String) + " % of attendance which is more than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            Else
                'not met Min Percent of Attendance
                'Return msg
                resultString.Append("Student only has " + CType(AttendanceCompleted, String) + " % of attendance which is less than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            End If

            If myAdvAppSettings.AppSettings("PassAllCoursesForSAPCheck").ToLower = "true" And myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then

                'If Not IsPass(db.GetWorkUnitResults(StudentId, SAPInfo.OffsetDate)) Then
                '    resultString.Append("Student failed in any of the courses.")
                'End If
                Dim result() As String = IsPass(db.GetWorkUnitResults(StudentId, SAPInfo.OffsetDate), StudentId)
                Dim resultCount As Integer = 0
                If result IsNot Nothing Then
                    resultString.Append(" Student failed in the following Courses.")
                    For resultCount = 0 To result.Length - 1
                        If result(resultCount) IsNot Nothing Then
                            resultString.Append(result(resultCount) & ",")
                        Else
                            Exit For
                        End If
                    Next
                    resultString.Remove(resultString.Length - 1, 1)
                End If


            End If
            '18874: QA: Need to expand the Short Hand SAP check to all the types of trigger points. 
            If myAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                SAPInfo.TrackIfStudentPassedSAPSpeedSkills = (New RegisterStudentsBR).CheckIfStudentPassedSpeedRequirementforSAP(SAPInfo.SAPDetailId.ToString, StudentId.ToString)
                If SAPInfo.TrackIfStudentPassedSAPSpeedSkills = False Then
                    resultString.Append("Student has not satisified the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                Else
                    passComments.Append("Student has  satisified the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                End If
            End If

            SapChkInfo.Period = SAPInfo.Period
            If resultString.Length <> 0 Then
                SapChkInfo.IsMakingSAP = False
            Else
                SapChkInfo.IsMakingSAP = True
            End If
            SapChkInfo.CredsEarned = CredsCompleted
            SapChkInfo.CredsAttempted = CredsAttempted
            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                SapChkInfo.GPA = SAPInfo.OverallAverage
            Else
                SapChkInfo.GPA = CumGPA
            End If

            SapChkInfo.DatePerf = DateTime.Now
            SapChkInfo.CheckPointDate = SAPInfo.OffsetDate
            SapChkInfo.TermStartDate = SAPInfo.TermSDate
            'Added By Balaji on 11/17/2004 to capture reason
            'Modification starts here 
            If resultString.ToString.Length > 900 Then
                SapChkInfo.Comments = resultString.ToString.Substring(0, 900)
            Else
                SapChkInfo.Comments = resultString.ToString
            End If
            If SapChkInfo.IsMakingSAP Then SapChkInfo.Comments = passComments.ToString()
            'Modification ends here

            'Added by Bhavana to store SAPDetailId in SAPChkResults table 11/02/05
            SapChkInfo.SAPDetailId = XmlConvert.ToGuid(SAPDetailId)
            SapChkInfo.PercAttendance = AttendanceCompleted
            SapChkInfo.FinCredsEarned = SAPInfo.FinCredsEarned
            SapChkInfo.PercCredsEarned = SAPInfo.PercCredsEarned
            db.AddTrigFASAPToDB(SapChkInfo, user)


            Return resultString.ToString
        End Function
    End Class
    Public NotInheritable Class SAPWeeks
        Inherits SAP
        Public Overrides Function IsMakingSAP(ByVal StudentId As String, ByVal SAPInfo As SAPInfo, ByVal PrgVerId As String, ByVal user As String, ByVal gradeReps As String, Optional ByVal TermEDate As Date = #1/1/1001#, Optional campusId As String = "") As String
            Dim db As New SAPCheckDB
            Dim TrigUnit As String
            Dim TrigOffsetTyp As String
            Dim QuantMinUnitTyp As String
            Dim QualTyp As String
            Dim QuantMinVal As Decimal
            Dim QualMinVal As Decimal
            Dim PercCompltd As Decimal
            Dim CumGPA As Decimal
            Dim TrigOffSeq As Integer
            Dim Seq As Integer
            Dim resultString As New StringBuilder
            Dim passComments As New StringBuilder
            Dim SapChkInfo As New SAPCheckInfo
            Dim TrigVal As Decimal
            Dim SAPDetailId As String
            Dim AttendanceRequired As Decimal
            Dim AttendanceCompleted As Decimal

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


            QuantMinVal = SAPInfo.QuantMinValue
            QualMinVal = SAPInfo.QualMinValue
            QualTyp = SAPInfo.QualType
            TrigOffsetTyp = SAPInfo.TrigOffsetTyp
            TrigOffSeq = SAPInfo.TrigOffsetSeq
            Seq = SAPInfo.Seq
            QuantMinUnitTyp = SAPInfo.QuantMinUnitType
            If (TrigOffsetTyp = "start of term") Or (TrigOffsetTyp = "end of term") Or (TrigOffsetTyp = "start date") Then
                TermEDate = SAPInfo.TermEDate

            Else
                TermEDate = #1/1/1001#
            End If
            TrigUnit = SAPInfo.TrigUnit
            TrigVal = SAPInfo.TrigValue
            SAPDetailId = SAPInfo.SAPDetailId.ToString
            AttendanceRequired = SAPInfo.MinAttendanceValue


            'Select Case Trim(TrigOffsetTyp)
            '    Case "Start Date"
            '        ''Get Student start date
            '        'ds3 = db.GetStuStartDate(StudentId)
            '        'CurrRow = ds3.Tables(0).Rows(0)
            '        ''Get the value of the StartDate Column in the table
            '        'StartDate = CurrRow("StartDate")

            '    Case "start of term"
            '        ''Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        'CurRow = ds4.Tables(0).Rows(TrigOffSeq)
            '        'TermId = CurRow("TermId")
            '        ''Get Results for this Term.
            '        ''ds5 = db.GetTermResults(TermId)

            '    Case "end of term"
            '        'If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
            '        '    resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
            '        'End If

            '        'Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        ''INSERT CODE TO LOOP THRU AND GET TERMIDs ONLY UPTO
            '        ''THE ROW(TRIGOFFSETSEQ)!!!!!

            '        'For Each row In ds4.Tables(0).Rows
            '        '    str &= row("TermId").ToString & ","
            '        '    int += 1
            '        '    If int = TrigOffSeq Then
            '        '        Exit For
            '        '    End If
            '        'Next
            '        'intpos = str.LastIndexOf(",")
            '        'str = str.Substring(0, intpos)
            '        '*************************************
            '        ' pos = InStrRev(str, ",")
            '        'str = Mid(str, 1, pos - 1)
            '        ' str = TermId
            'End Select

            'Check if Student is meeting Qualitative and Quantitative Reqs
            '**************************************************************************************
            'The following lines of code has been replaced to use the centralized component for 
            'computing the Percentage of Credits Earned (or Completed) instead of an independent function.
            'Modified By Michelle R. Rodriguez 05/15/06
            '**************************************************************************************
            'PercCompltd = CDec(ProcessQuantMinUnitTyp(QuantMinUnitTyp, StudentId, PrgVerId, TrigUnit, TrigVal, TermEDate))
            If myAdvAppSettings.AppSettings("tracksapattendance").ToLower = "byday" Then
                Dim attUnitType As String = ""
                Dim dal As New ClsSectAttendanceDB()
                Dim externScheduleHrs As Decimal = 0.0
                Dim externActualHrs As Decimal = 0.0
                'Dim stuEnrollId As String = (New SAPActualHours).getEnrollmentId(StudentId, PrgVerId)
                attUnitType = dal.GetPrgVersionAttendanceType(StudentId, False)
                If attUnitType <> Nothing Then
                    If attUnitType.ToString.Substring(0, 7).ToLower = "present" Then
                        attUnitType = "pa"
                    End If
                    If SAPInfo.TrackExternAttendance = True Then
                        Dim externAttendance As Decimal() = (New SapDB).GetHoursForExternships(StudentId, SAPInfo.OffsetDate)
                        externScheduleHrs = externAttendance(0)
                        externActualHrs = externAttendance(1)
                    End If
                    If SAPInfo.TrackExternAttendance = True Then

                        With (New AttendanceDB).StudentSummaryFormForCutOffDate(StudentId, attUnitType, SAPInfo.OffsetDate)
                            If (.TotalHoursPresent + externActualHrs) <= 0 Or (.TotalHoursSched + externScheduleHrs) <= 0 Then
                                PercCompltd = 0
                            Else
                                PercCompltd = Math.Round(((.TotalHoursPresent + externActualHrs) / (.TotalHoursSched + externScheduleHrs)) * 100, 2)
                            End If
                        End With
                    Else
                        With (New AttendanceDB).StudentSummaryFormForCutOffDate(StudentId, attUnitType, SAPInfo.OffsetDate)
                            If .TotalHoursPresent <= 0 Or .TotalHoursSched <= 0 Then
                                PercCompltd = 0
                            Else
                                PercCompltd = Math.Round((.TotalHoursPresent / .TotalHoursSched) * 100, 2)
                            End If
                        End With
                    End If

                End If
                AttendanceCompleted = PercCompltd
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    If SAPInfo.ActHoursAdjusted > 0 And SAPInfo.SchedHours > 0 Then
                        PercCompltd = SAPInfo.ActHoursAdjusted * 100 / SAPInfo.SchedHours
                    Else
                        PercCompltd = 0
                    End If
                Else
                    PercCompltd = SAPInfo.PercCredsEarned
                End If
            Else
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    If SAPInfo.ActHoursAdjusted > 0 And SAPInfo.SchedHours > 0 Then
                        PercCompltd = SAPInfo.ActHoursAdjusted * 100 / SAPInfo.SchedHours
                    Else
                        PercCompltd = 0
                    End If
                Else
                    PercCompltd = SAPInfo.PercCredsEarned
                End If
                AttendanceCompleted = Math.Round(GetAttendancePercentageByStudentEnrollment(StudentId, SAPInfo.OffsetDate), 2)
            End If

            '**************************************************************************************
            'The following line of code has been replaced to use the centralized component for 
            'computing GPA instead of an independent function by BN 04/26/06
            '**************************************************************************************
            'CumGPA = CDec(ProcessQualTyp(QualTyp, StudentId, CredsCompleted, PrgVerId, TrigUnit, TrigVal, TermEDate))
            CumGPA = SAPInfo.GPA
            CredsCompleted = SAPInfo.CredsEarned

            If myAdvAppSettings.AppSettings("GradesFormat", campusId) = "Numeric" Then
                If SAPInfo.OverallAverage >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    'not met qual. meaasure
                    'Return msg
                    resultString.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                    'ErrMsgObj.ErrMsg = msg
                End If
                ' If SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                End If
                'End If
            Else

                If CumGPA >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    'not met qual. meaasure
                    'Return msg
                    resultString.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                    'ErrMsgObj.ErrMsg = msg
                End If

                'Check if student has met minimum credits required
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has  completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                End If
                If resultString.Length = 0 Then
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.MinTermGPA <> 0 Then
                        If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
                            resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        Else
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                Else
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.TermGPAOver <> 0 Then
                        If SAPInfo.TermGPA > SAPInfo.TermGPAOver Then
                            resultString.Remove(0, resultString.Length)
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                End If
                'Check if student has met minimum attendance required

            End If
            If AttendanceCompleted >= AttendanceRequired Then
                'Min Percent of Attendance Met
                passComments.Append("Student  has " + CType(AttendanceCompleted, String) + " % of attendance which is more than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            Else
                'not met Min Percent of Attendance
                'Return msg
                resultString.Append("Student only has " + CType(AttendanceCompleted, String) + " % of attendance which is less than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            End If
            If PercCompltd >= QuantMinVal Then
                'Quantitative measure met
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                Else
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If
            ElseIf SAPInfo.QuantMinUnitTypId = "3" Then
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            Else
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            End If
            If myAdvAppSettings.AppSettings("PassAllCoursesForSAPCheck").ToLower = "true" And myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then

                'If Not IsPass(db.GetWorkUnitResults(StudentId, SAPInfo.OffsetDate)) Then
                '    resultString.Append("Student failed in any of the courses.")
                'End If
                Dim result() As String = IsPass(db.GetWorkUnitResults(StudentId, SAPInfo.OffsetDate), StudentId)
                Dim resultCount As Integer = 0
                If result IsNot Nothing Then
                    resultString.Append(" Student failed in the following Courses.")
                    For resultCount = 0 To result.Length - 1
                        If result(resultCount) IsNot Nothing Then
                            resultString.Append(result(resultCount) & ",")
                        Else
                            Exit For
                        End If
                    Next
                    resultString.Remove(resultString.Length - 1, 1)
                End If
            End If

            '18874: QA: Need to expand the Short Hand SAP check to all the types of trigger points. 
            If myAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                SAPInfo.TrackIfStudentPassedSAPSpeedSkills = (New RegisterStudentsBR).CheckIfStudentPassedSpeedRequirementforSAP(SAPInfo.SAPDetailId.ToString, StudentId.ToString)
                If SAPInfo.TrackIfStudentPassedSAPSpeedSkills = False Then
                    resultString.Append("Student has not satisfied the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                Else
                    passComments.Append("Student has satisfied the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                End If
            End If



            'Record the trigger as being performed.
            SapChkInfo.StudentId = XmlConvert.ToGuid(StudentId)

            SapChkInfo.Period = SAPInfo.Period
            If resultString.Length <> 0 Then
                SapChkInfo.IsMakingSAP = False
            Else
                SapChkInfo.IsMakingSAP = True
            End If
            SapChkInfo.CredsEarned = CredsCompleted
            SapChkInfo.CredsAttempted = CredsAttempted
            'SapChkInfo.GPA = CumGPA
            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                SapChkInfo.GPA = SAPInfo.OverallAverage
            Else
                SapChkInfo.GPA = CumGPA
            End If
            SapChkInfo.DatePerf = DateTime.Now
            SapChkInfo.CheckPointDate = SAPInfo.OffsetDate
            SapChkInfo.TermStartDate = SAPInfo.TermSDate

            'Added by Bhavana to capture reason 08/23/05
            If resultString.ToString.Length > 900 Then
                SapChkInfo.Comments = resultString.ToString.Substring(0, 900)
            Else
                SapChkInfo.Comments = resultString.ToString
            End If
            If SapChkInfo.IsMakingSAP Then SapChkInfo.Comments = passComments.ToString
            'Added by Bhavana to store SAPDetailId in SAPChkResults table 11/02/05
            SapChkInfo.SAPDetailId = XmlConvert.ToGuid(SAPDetailId)
            SapChkInfo.PercAttendance = AttendanceCompleted
            SapChkInfo.FinCredsEarned = SAPInfo.FinCredsEarned
            SapChkInfo.PercCredsEarned = SAPInfo.PercCredsEarned
            db.AddTrigToDB(SapChkInfo, user)
            Return resultString.ToString
        End Function

        Public Overrides Function IsMakingFASAP(ByVal StudentId As String, ByVal SAPInfo As SAPInfo, ByVal PrgVerId As String, ByVal user As String, ByVal gradeReps As String, Optional ByVal TermEDate As Date = #1/1/1001#, Optional campusId As String = "") As String
            Dim db As New SAPCheckDB
            Dim TrigUnit As String
            Dim TrigOffsetTyp As String
            Dim QuantMinUnitTyp As String
            Dim QualTyp As String
            Dim QuantMinVal As Decimal
            Dim QualMinVal As Decimal
            Dim PercCompltd As Decimal
            Dim CumGPA As Decimal
            'Dim currdate As Date
            Dim TrigOffSeq As Integer
            Dim Seq As Integer
            Dim resultString As New StringBuilder
            Dim passComments As New StringBuilder
            Dim SapChkInfo As New SAPCheckInfo
            Dim TrigVal As Decimal
            Dim SAPDetailId As String
            Dim AttendanceRequired As Decimal
            Dim AttendanceCompleted As Decimal

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


            QuantMinVal = SAPInfo.QuantMinValue
            QualMinVal = SAPInfo.QualMinValue
            QualTyp = SAPInfo.QualType
            TrigOffsetTyp = SAPInfo.TrigOffsetTyp
            TrigOffSeq = SAPInfo.TrigOffsetSeq
            Seq = SAPInfo.Seq
            QuantMinUnitTyp = SAPInfo.QuantMinUnitType
            If (TrigOffsetTyp = "start of term") Or (TrigOffsetTyp = "end of term") Or (TrigOffsetTyp = "start date") Then
                TermEDate = SAPInfo.TermEDate

            Else
                TermEDate = #1/1/1001#
            End If
            'currdate = #9/30/2005#
            TrigUnit = SAPInfo.TrigUnit
            TrigVal = SAPInfo.TrigValue
            SAPDetailId = SAPInfo.SAPDetailId.ToString
            AttendanceRequired = SAPInfo.MinAttendanceValue


            If myAdvAppSettings.AppSettings("tracksapattendance").ToLower = "byday" Then
                Dim attUnitType As String = ""
                Dim dal As New ClsSectAttendanceDB()
                Dim externScheduleHrs As Decimal = 0.0
                Dim externActualHrs As Decimal = 0.0
                'Dim stuEnrollId As String = (New SAPActualHours).getEnrollmentId(StudentId, PrgVerId)
                attUnitType = dal.GetPrgVersionAttendanceType(StudentId, False)
                If attUnitType <> Nothing Then
                    If attUnitType.ToString.Substring(0, 7).ToLower = "present" Then
                        attUnitType = "pa"
                    End If
                    If SAPInfo.TrackExternAttendance = True Then
                        Dim externAttendance As Decimal() = (New SapDB).GetHoursForExternships(StudentId, SAPInfo.OffsetDate)
                        externScheduleHrs = externAttendance(0)
                        externActualHrs = externAttendance(1)
                    End If
                    If SAPInfo.TrackExternAttendance = True Then

                        With (New AttendanceDB).StudentSummaryFormForCutOffDate(StudentId, attUnitType, SAPInfo.OffsetDate)
                            If (.TotalHoursPresent + externActualHrs) <= 0 Or (.TotalHoursSched + externScheduleHrs) <= 0 Then
                                PercCompltd = 0
                            Else
                                PercCompltd = Math.Round(((.TotalHoursPresent + externActualHrs) / (.TotalHoursSched + externScheduleHrs)) * 100, 2)
                            End If
                        End With
                    Else
                        With (New AttendanceDB).StudentSummaryFormForCutOffDate(StudentId, attUnitType, SAPInfo.OffsetDate)
                            If .TotalHoursPresent <= 0 Or .TotalHoursSched <= 0 Then
                                PercCompltd = 0
                            Else
                                PercCompltd = Math.Round((.TotalHoursPresent / .TotalHoursSched) * 100, 2)
                            End If
                        End With
                    End If

                End If
                AttendanceCompleted = PercCompltd
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    If SAPInfo.ActHoursAdjusted > 0 And SAPInfo.SchedHours > 0 Then
                        PercCompltd = SAPInfo.ActHoursAdjusted * 100 / SAPInfo.SchedHours
                    Else
                        PercCompltd = 0
                    End If
                Else
                    PercCompltd = SAPInfo.PercCredsEarned
                End If
            Else
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    If SAPInfo.ActHoursAdjusted > 0 And SAPInfo.SchedHours > 0 Then
                        PercCompltd = SAPInfo.ActHoursAdjusted * 100 / SAPInfo.SchedHours
                    Else
                        PercCompltd = 0
                    End If
                Else
                    PercCompltd = SAPInfo.PercCredsEarned
                End If
                AttendanceCompleted = Math.Round(GetAttendancePercentageByStudentEnrollment(StudentId, SAPInfo.OffsetDate), 2)
            End If

            CumGPA = SAPInfo.GPA
            CredsCompleted = SAPInfo.CredsEarned

            If myAdvAppSettings.AppSettings("GradesFormat", campusId) = "Numeric" Then
                If SAPInfo.OverallAverage >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else

                    'Return msg
                    resultString.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)

                End If

                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                End If
                'End If
            Else

                If CumGPA >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    'not met qual. meaasure
                    'Return msg
                    resultString.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                    'ErrMsgObj.ErrMsg = msg
                End If

                'Check if student has met minimum credits required
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has  completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                End If
                If resultString.Length = 0 Then
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.MinTermGPA <> 0 Then
                        If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
                            resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        Else
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                Else
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.TermGPAOver <> 0 Then
                        If SAPInfo.TermGPA > SAPInfo.TermGPAOver Then
                            resultString.Remove(0, resultString.Length)
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                End If
                'Check if student has met minimum attendance required

            End If
            If AttendanceCompleted >= AttendanceRequired Then
                'Min Percent of Attendance Met
                passComments.Append("Student  has " + CType(AttendanceCompleted, String) + " % of attendance which is more than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            Else
                'not met Min Percent of Attendance
                'Return msg
                resultString.Append("Student only has " + CType(AttendanceCompleted, String) + " % of attendance which is less than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            End If
            If PercCompltd >= QuantMinVal Then
                'Quantitative measure met
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                Else
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If
            ElseIf SAPInfo.QuantMinUnitTypId = "3" Then
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            Else
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            End If
            If myAdvAppSettings.AppSettings("PassAllCoursesForSAPCheck").ToLower = "true" And myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then

                'If Not IsPass(db.GetWorkUnitResults(StudentId, SAPInfo.OffsetDate)) Then
                '    resultString.Append("Student failed in any of the courses.")
                'End If
                Dim result() As String = IsPass(db.GetWorkUnitResults(StudentId, SAPInfo.OffsetDate), StudentId)
                Dim resultCount As Integer = 0
                If result IsNot Nothing Then
                    resultString.Append(" Student failed in the following Courses.")
                    For resultCount = 0 To result.Length - 1
                        If result(resultCount) IsNot Nothing Then
                            resultString.Append(result(resultCount) & ",")
                        Else
                            Exit For
                        End If
                    Next
                    resultString.Remove(resultString.Length - 1, 1)
                End If
            End If

            '18874: QA: Need to expand the Short Hand SAP check to all the types of trigger points. 
            If myAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                SAPInfo.TrackIfStudentPassedSAPSpeedSkills = (New RegisterStudentsBR).CheckIfStudentPassedSpeedRequirementforSAP(SAPInfo.SAPDetailId.ToString, StudentId.ToString)
                If SAPInfo.TrackIfStudentPassedSAPSpeedSkills = False Then
                    resultString.Append("Student has not satisified the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                Else
                    passComments.Append("Student has satisified the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                End If
            End If


            SapChkInfo.StudentId = XmlConvert.ToGuid(StudentId)

            SapChkInfo.Period = SAPInfo.Period

            SapChkInfo.Period = SAPInfo.Period
            If resultString.Length <> 0 Then
                SapChkInfo.IsMakingSAP = False
            Else
                SapChkInfo.IsMakingSAP = True
            End If

            SapChkInfo.CredsEarned = CredsCompleted
            SapChkInfo.CredsAttempted = CredsAttempted

            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                SapChkInfo.GPA = SAPInfo.OverallAverage
            Else
                SapChkInfo.GPA = CumGPA
            End If
            SapChkInfo.DatePerf = DateTime.Now
            SapChkInfo.CheckPointDate = SAPInfo.OffsetDate
            SapChkInfo.TermStartDate = SAPInfo.TermSDate

            If resultString.ToString.Length > 900 Then
                SapChkInfo.Comments = resultString.ToString.Substring(0, 900)
            Else
                SapChkInfo.Comments = resultString.ToString
            End If
            If SapChkInfo.IsMakingSAP Then SapChkInfo.Comments = passComments.ToString
            SapChkInfo.SAPDetailId = XmlConvert.ToGuid(SAPDetailId)
            SapChkInfo.PercAttendance = AttendanceCompleted
            SapChkInfo.FinCredsEarned = SAPInfo.FinCredsEarned
            SapChkInfo.PercCredsEarned = SAPInfo.PercCredsEarned
            db.AddTrigFASAPToDB(SapChkInfo, user)

            Return resultString.ToString
        End Function
    End Class
    Public Class SAPActualHours
        Inherits SAP
        Public Overrides Function IsMakingSAP(ByVal StudentId As String, ByVal SAPInfo As SAPInfo, ByVal PrgVerId As String, ByVal user As String, ByVal gradeReps As String, Optional ByVal TermEDate As Date = #1/1/1001#, Optional campusId As String = "") As String
            Dim db As New SAPCheckDB
            Dim TrigUnit As String
            Dim TrigOffsetTyp As String
            Dim QuantMinUnitTyp As String
            Dim QualTyp As String
            Dim QuantMinVal As Decimal
            Dim QualMinVal As Decimal
            Dim PercCompltd As Decimal
            Dim CumGPA As Decimal
            'Dim currdate As Date
            Dim TrigOffSeq As Integer
            Dim Seq As Integer
            Dim resultString As New StringBuilder
            Dim passComments As New StringBuilder
            Dim SapChkInfo As New SAPCheckInfo
            Dim TrigVal As Decimal
            Dim SAPDetailId As String
            Dim AttendanceRequired As Decimal
            Dim AttendanceCompleted As Decimal

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            'Make certain to clear the ResultString variable
            resultString.Remove(0, resultString.Length)

            QuantMinVal = SAPInfo.QuantMinValue
            QualMinVal = SAPInfo.QualMinValue
            QualTyp = SAPInfo.QualType
            TrigOffsetTyp = SAPInfo.TrigOffsetTyp
            TrigOffSeq = SAPInfo.TrigOffsetSeq
            Seq = SAPInfo.Seq
            QuantMinUnitTyp = SAPInfo.QuantMinUnitType
            If (TrigOffsetTyp = "start of term") Or (TrigOffsetTyp = "end of term") Or (TrigOffsetTyp = "start date") Then
                TermEDate = SAPInfo.TermEDate
            Else
                TermEDate = #1/1/1001#
            End If
            TrigUnit = SAPInfo.TrigUnit
            TrigVal = SAPInfo.TrigValue
            SAPDetailId = SAPInfo.SAPDetailId.ToString
            AttendanceRequired = SAPInfo.MinAttendanceValue
            'Select Case Trim(TrigOffsetTyp)
            '    Case "Start Date"
            '        ''Get Student start date
            '        'ds3 = db.GetStuStartDate(StudentId)
            '        'CurrRow = ds3.Tables(0).Rows(0)
            '        ''Get the value of the StartDate Column in the table
            '        'StartDate = CurrRow("StartDate")

            '    Case "start of term"
            '        ''Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        'CurRow = ds4.Tables(0).Rows(TrigOffSeq)
            '        'TermId = CurRow("TermId")
            '        ''Get Results for this Term.
            '        ''ds5 = db.GetTermResults(TermId)

            '    Case "end of term"
            '        'If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
            '        '    resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
            '        'End If

            '        'Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        ''INSERT CODE TO LOOP THRU AND GET TERMIDs ONLY UPTO
            '        ''THE ROW(TRIGOFFSETSEQ)!!!!!

            '        'For Each row In ds4.Tables(0).Rows
            '        '    str &= row("TermId").ToString & ","
            '        '    int += 1
            '        '    If int = TrigOffSeq Then
            '        '        Exit For
            '        '    End If
            '        'Next
            '        'intpos = str.LastIndexOf(",")
            '        'str = str.Substring(0, intpos)
            '        '*************************************
            '        ' pos = InStrRev(str, ",")
            '        'str = Mid(str, 1, pos - 1)
            '        ' str = TermId
            'End Select

            'Check if Student is meeting Qualitative and Quantitative Reqs
            '**************************************************************************************
            'The following lines of code has been replaced to use the centralized component for 
            'computing the Percentage of Credits Earned (or Completed) instead of an independent function.
            'Modified by Michelle R. Rodriguez 05/15/06
            '**************************************************************************************
            'PercCompltd = CDec(ProcessQuantMinUnitTyp(QuantMinUnitTyp, StudentId, PrgVerId, TrigUnit, TrigVal, TermEDate))
            'PercCompltd = SAPInfo.PercCredsEarned




            'CredsCompleted = SAPInfo.CredsEarned
            '**************************************************************************************
            'The following line of code has been replaced to use the centralized component for 
            'computing GPA instead of an independent function by BN 04/26/06
            '**************************************************************************************
            'CumGPA = CDec(ProcessQualTyp(QualTyp, StudentId, CredsCompleted, PrgVerId, TrigUnit, TrigVal, TermEDate))
            CumGPA = SAPInfo.GPA
            'AttendanceCompleted = Math.Round(GetAttendancePercentageByStudentEnrollment(StudentId, SAPInfo.OffsetDate), 2)
            If MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                If SAPInfo.OverallAverage >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    'not met qual. meaasure
                    'Return msg
                    resultString.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                    'ErrMsgObj.ErrMsg = msg
                End If
            Else

                If CumGPA >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    resultString.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                End If
                If resultString.Length = 0 Then
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.MinTermGPA <> 0 Then
                        If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
                            resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        Else
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                Else
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.TermGPAOver <> 0 Then
                        If SAPInfo.TermGPA > SAPInfo.TermGPAOver Then
                            resultString.Remove(0, resultString.Length)
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                End If

            End If
            If SAPInfo.attType = "CLOCK HOURS" And SAPInfo.OverallAttendance Then
                If SAPInfo.ActHours <= 0 Or SAPInfo.SchedHours <= 0 Then
                    PercCompltd = 0
                Else
                    PercCompltd = Math.Round((SAPInfo.ActHoursAdjusted / SAPInfo.SchedHours) * 100, 2)
                End If
                If PercCompltd >= QuantMinVal Then
                    'Quantitative measure met
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                Else
                    resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If
                SAPInfo.PercCredsEarned = PercCompltd
            ElseIf MyAdvAppSettings.AppSettings("tracksapattendance").ToLower = "byday" Then
                If SAPInfo.ActHours > 0 And SAPInfo.SchedHours > 0 Then
                    PercCompltd = Math.Round((SAPInfo.ActHours / SAPInfo.SchedHours) * 100, 2)
                End If
                If PercCompltd >= QuantMinVal Then
                    'Quantitative measure met
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                Else
                    resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If
            ElseIf Not SAPInfo.OverallAttendance Then

                For i As Integer = 0 To SAPInfo.QuantMinValueByInsTypes.Count - 1
                    If SAPInfo.QuantMinValueByInsTypes(i).attDetailsInfo.TotalPresentAdjusted <= 0 Or SAPInfo.QuantMinValueByInsTypes(i).attDetailsInfo.TotalScheduled <= 0 Then
                        PercCompltd = 0
                    Else
                        PercCompltd = (SAPInfo.QuantMinValueByInsTypes(i).attDetailsInfo.TotalPresentAdjusted / SAPInfo.QuantMinValueByInsTypes(i).attDetailsInfo.TotalScheduled) * 100
                    End If


                    If PercCompltd >= SAPInfo.QuantMinValueByInsTypes(i).QuantMinValue Then

                        passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of " + SAPInfo.QuantMinValueByInsTypes(i).InstructionTypeDescrip + " scheduled hours/days attempted which is more than the minimum of " + CType(SAPInfo.QuantMinValueByInsTypes(i).QuantMinValue, String) + "%. " + vbCr)

                    Else

                        resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of " + SAPInfo.QuantMinValueByInsTypes(i).InstructionTypeDescrip + " scheduled hours/days attempted which is less than the minimum of " + CType(SAPInfo.QuantMinValueByInsTypes(i).QuantMinValue, String) + "%. " + vbCr)

                    End If
                    SAPInfo.QuantMinValueByInsTypes(i).PercCompltd = PercCompltd
                Next
                PercCompltd = Math.Round((SAPInfo.ActHoursAdjusted / SAPInfo.SchedHours) * 100, 2)

            Else
                If SAPInfo.ActHoursAdjusted > 0 And SAPInfo.SchedHours > 0 Then
                    PercCompltd = SAPInfo.ActHoursAdjusted * 100 / SAPInfo.SchedHours
                End If
                If PercCompltd >= QuantMinVal Then
                    'Quantitative measure met
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                Else
                    resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If
            End If
            If MyAdvAppSettings.AppSettings("PassAllCoursesForSAPCheck").ToLower = "true" And MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then

                'If Not IsPass(db.GetWorkUnitResults(StudentId, SAPInfo.OffsetDate)) Then
                '    resultString.Append("Student failed in any of the courses.")
                'End If
                Dim result() As String = IsPass(db.GetWorkUnitResults(StudentId, SAPInfo.OffsetDate), StudentId)
                Dim resultCount As Integer = 0
                If result IsNot Nothing Then
                    resultString.Append(" Student failed in the following Courses.")
                    For resultCount = 0 To result.Length - 1
                        If result(resultCount) IsNot Nothing Then
                            resultString.Append(result(resultCount) & ",")
                        Else
                            Exit For
                        End If
                    Next
                    resultString.Remove(resultString.Length - 1, 1)
                End If
            End If
            If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                SAPInfo.TrackIfStudentPassedSAPSpeedSkills = (New RegisterStudentsBR).CheckIfStudentPassedSpeedRequirementforSAP(SAPInfo.SAPDetailId.ToString, StudentId.ToString)
                If SAPInfo.TrackIfStudentPassedSAPSpeedSkills = False Then
                    resultString.Append("Student has not satisfied the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                Else
                    resultString.Append("Student has satisfied the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                End If
            End If


            'Record the trigger as being performed.
            SapChkInfo.StudentId = XmlConvert.ToGuid(StudentId)

            SapChkInfo.Period = SAPInfo.Period
            If resultString.Length <> 0 Then
                SapChkInfo.IsMakingSAP = False
            Else
                SapChkInfo.IsMakingSAP = True
            End If
            SapChkInfo.CredsEarned = CredsCompleted
            SapChkInfo.CredsAttempted = CredsAttempted
            'SapChkInfo.GPA = CumGPA
            If MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                SapChkInfo.GPA = SAPInfo.OverallAverage
            Else
                SapChkInfo.GPA = CumGPA
            End If
            SapChkInfo.DatePerf = DateTime.Now
            SapChkInfo.TermStartDate = SAPInfo.TermSDate

            'Added By Balaji on 11/17/2004 to capture reason
            'Modification starts here 
            If resultString.ToString.Length > 900 Then
                SapChkInfo.Comments = resultString.ToString.Substring(0, 900)
            Else
                SapChkInfo.Comments = resultString.ToString
            End If
            If SapChkInfo.IsMakingSAP Then SapChkInfo.Comments = passComments.ToString
            'Modification ends here

            'Added by Bhavana to store SAPDetailId in SAPChkResults table 11/02/05
            SapChkInfo.SAPDetailId = XmlConvert.ToGuid(SAPDetailId)
            SapChkInfo.PercAttendance = AttendanceCompleted
            SapChkInfo.FinCredsEarned = SAPInfo.FinCredsEarned
            'SapChkInfo.PercCredsEarned = SAPInfo.PercCredsEarned
            SapChkInfo.PercCredsEarned = PercCompltd
            SapChkInfo.CheckPointDate = SAPInfo.OffsetDate
            SapChkInfo.HrsAttended = Math.Round(SAPInfo.TrigValue / CType(100, Decimal),2)
            SapChkInfo.HrsEarned = SAPInfo.ActHours
            If SAPInfo.OverallAttendance Then
                db.AddTrigToDB(SapChkInfo, user)
            Else
                db.AddTrigToDB(SapChkInfo, user, SAPInfo)
            End If




            Return resultString.ToString
        End Function

        Public Function getEnrollmentId(ByVal Studentid As String, ByVal PrgVerId As String) As String
            Dim db As New DataAccess.DataAccess
            Dim sb As New StringBuilder
            Dim strStuEnrollId As String = ""

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

            Try
                db.OpenConnection()
                With sb
                    .Append(" Select Distinct StuEnrollId from arStuEnrollments where ")
                    .Append(" StudentId=? and PrgVerId=? ")
                End With
                db.AddParameter("@StudentId", Studentid, DataAccess.DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@PrgVerId", PrgVerId, DataAccess.DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                strStuEnrollId = db.RunParamSQLScalar(sb.ToString)
                Return strStuEnrollId
            Catch ex As Exception
                Return strStuEnrollId
            Finally
                db.CloseConnection()
            End Try
        End Function

        Public Overrides Function IsMakingFASAP(ByVal StudentId As String, ByVal SAPInfo As SAPInfo, ByVal PrgVerId As String, ByVal user As String, ByVal gradeReps As String, Optional ByVal TermEDate As Date = #1/1/1001#, Optional campusId As String = "") As String
            Dim db As New SAPCheckDB
            Dim TrigUnit As String
            Dim TrigOffsetTyp As String
            Dim QuantMinUnitTyp As String
            Dim QualTyp As String
            Dim QuantMinVal As Decimal
            Dim QualMinVal As Decimal
            Dim PercCompltd As Decimal
            Dim CumGPA As Decimal
            'Dim currdate As Date
            Dim TrigOffSeq As Integer
            Dim Seq As Integer
            Dim resultString As New StringBuilder
            Dim passComments As New StringBuilder
            Dim SapChkInfo As New SAPCheckInfo
            Dim TrigVal As Decimal
            Dim SAPDetailId As String
            Dim AttendanceRequired As Decimal
            Dim AttendanceCompleted As Decimal

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


            'Make certain to clear the ResultString variable
            resultString.Remove(0, resultString.Length)

            QuantMinVal = SAPInfo.QuantMinValue
            QualMinVal = SAPInfo.QualMinValue
            QualTyp = SAPInfo.QualType
            TrigOffsetTyp = SAPInfo.TrigOffsetTyp
            TrigOffSeq = SAPInfo.TrigOffsetSeq
            Seq = SAPInfo.Seq
            QuantMinUnitTyp = SAPInfo.QuantMinUnitType
            If (TrigOffsetTyp = "start of term") Or (TrigOffsetTyp = "end of term") Or (TrigOffsetTyp = "start date") Then
                TermEDate = SAPInfo.TermEDate
            Else
                TermEDate = #1/1/1001#
            End If
            TrigUnit = SAPInfo.TrigUnit
            TrigVal = SAPInfo.TrigValue
            SAPDetailId = SAPInfo.SAPDetailId.ToString
            AttendanceRequired = SAPInfo.MinAttendanceValue
            'Select Case Trim(TrigOffsetTyp)
            '    Case "Start Date"
            '        ''Get Student start date
            '        'ds3 = db.GetStuStartDate(StudentId)
            '        'CurrRow = ds3.Tables(0).Rows(0)
            '        ''Get the value of the StartDate Column in the table
            '        'StartDate = CurrRow("StartDate")

            '    Case "start of term"
            '        ''Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        'CurRow = ds4.Tables(0).Rows(TrigOffSeq)
            '        'TermId = CurRow("TermId")
            '        ''Get Results for this Term.
            '        ''ds5 = db.GetTermResults(TermId)

            '    Case "end of term"
            '        'If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
            '        '    resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
            '        'End If

            '        'Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        ''INSERT CODE TO LOOP THRU AND GET TERMIDs ONLY UPTO
            '        ''THE ROW(TRIGOFFSETSEQ)!!!!!

            '        'For Each row In ds4.Tables(0).Rows
            '        '    str &= row("TermId").ToString & ","
            '        '    int += 1
            '        '    If int = TrigOffSeq Then
            '        '        Exit For
            '        '    End If
            '        'Next
            '        'intpos = str.LastIndexOf(",")
            '        'str = str.Substring(0, intpos)
            '        '*************************************
            '        ' pos = InStrRev(str, ",")
            '        'str = Mid(str, 1, pos - 1)
            '        ' str = TermId
            'End Select

            'Check if Student is meeting Qualitative and Quantitative Reqs
            '**************************************************************************************
            'The following lines of code has been replaced to use the centralized component for 
            'computing the Percentage of Credits Earned (or Completed) instead of an independent function.
            'Modified by Michelle R. Rodriguez 05/15/06
            '**************************************************************************************
            'PercCompltd = CDec(ProcessQuantMinUnitTyp(QuantMinUnitTyp, StudentId, PrgVerId, TrigUnit, TrigVal, TermEDate))
            'PercCompltd = SAPInfo.PercCredsEarned




            'CredsCompleted = SAPInfo.CredsEarned
            '**************************************************************************************
            'The following line of code has been replaced to use the centralized component for 
            'computing GPA instead of an independent function by BN 04/26/06
            '**************************************************************************************
            'CumGPA = CDec(ProcessQualTyp(QualTyp, StudentId, CredsCompleted, PrgVerId, TrigUnit, TrigVal, TermEDate))
            CumGPA = SAPInfo.GPA
            'AttendanceCompleted = Math.Round(GetAttendancePercentageByStudentEnrollment(StudentId, SAPInfo.OffsetDate), 2)
            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                If SAPInfo.OverallAverage >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    'not met qual. meaasure
                    'Return msg
                    resultString.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                    'ErrMsgObj.ErrMsg = msg
                End If
            Else

                If CumGPA >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    resultString.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                End If
                If resultString.Length = 0 Then
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.MinTermGPA <> 0 Then
                        If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
                            resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        Else
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                Else
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.TermGPAOver <> 0 Then
                        If SAPInfo.TermGPA > SAPInfo.TermGPAOver Then
                            resultString.Remove(0, resultString.Length)
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                End If

            End If
            If SAPInfo.attType = "CLOCK HOURS" And SAPInfo.OverallAttendance Then
                If SAPInfo.ActHours <= 0 Or SAPInfo.SchedHours <= 0 Then
                    PercCompltd = 0
                Else
                    PercCompltd = Math.Round((SAPInfo.ActHoursAdjusted / SAPInfo.SchedHours) * 100, 2)
                End If
                If PercCompltd >= QuantMinVal Then
                    'Quantitative measure met
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                Else
                    resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If
                SAPInfo.PercCredsEarned = PercCompltd
            ElseIf myAdvAppSettings.AppSettings("tracksapattendance").ToLower = "byday" Then
                If SAPInfo.ActHours > 0 And SAPInfo.SchedHours > 0 Then
                    PercCompltd = Math.Round((SAPInfo.ActHours / SAPInfo.SchedHours) * 100, 2)
                End If
                If PercCompltd >= QuantMinVal Then
                    'Quantitative measure met
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                Else
                    resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If
            ElseIf Not SAPInfo.OverallAttendance Then

                For i As Integer = 0 To SAPInfo.QuantMinValueByInsTypes.Count - 1
                    If SAPInfo.QuantMinValueByInsTypes(i).attDetailsInfo.TotalPresentAdjusted <= 0 Or SAPInfo.QuantMinValueByInsTypes(i).attDetailsInfo.TotalScheduled <= 0 Then
                        PercCompltd = 0
                    Else
                        PercCompltd = (SAPInfo.QuantMinValueByInsTypes(i).attDetailsInfo.TotalPresentAdjusted / SAPInfo.QuantMinValueByInsTypes(i).attDetailsInfo.TotalScheduled) * 100
                    End If


                    If PercCompltd >= SAPInfo.QuantMinValueByInsTypes(i).QuantMinValue Then

                        passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of " + SAPInfo.QuantMinValueByInsTypes(i).InstructionTypeDescrip + " scheduled hours/days attempted which is more than the minimum of " + CType(SAPInfo.QuantMinValueByInsTypes(i).QuantMinValue, String) + "%. " + vbCr)

                    Else

                        resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of " + SAPInfo.QuantMinValueByInsTypes(i).InstructionTypeDescrip + " scheduled hours/days attempted which is less than the minimum of " + CType(SAPInfo.QuantMinValueByInsTypes(i).QuantMinValue, String) + "%. " + vbCr)

                    End If
                    SAPInfo.QuantMinValueByInsTypes(i).PercCompltd = PercCompltd
                Next
                PercCompltd = Math.Round((SAPInfo.ActHoursAdjusted / SAPInfo.SchedHours) * 100, 2)

            Else
                If SAPInfo.ActHoursAdjusted > 0 And SAPInfo.SchedHours > 0 Then
                    PercCompltd = SAPInfo.ActHoursAdjusted * 100 / SAPInfo.SchedHours
                End If
                If PercCompltd >= QuantMinVal Then
                    'Quantitative measure met
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                Else
                    resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If
            End If
            If myAdvAppSettings.AppSettings("PassAllCoursesForSAPCheck").ToLower = "true" And myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then

                'If Not IsPass(db.GetWorkUnitResults(StudentId, SAPInfo.OffsetDate)) Then
                '    resultString.Append("Student failed in any of the courses.")
                'End If
                Dim result() As String = IsPass(db.GetWorkUnitResults(StudentId, SAPInfo.OffsetDate), StudentId)
                Dim resultCount As Integer = 0
                If result IsNot Nothing Then
                    resultString.Append(" Student failed in the following Courses.")
                    For resultCount = 0 To result.Length - 1
                        If result(resultCount) IsNot Nothing Then
                            resultString.Append(result(resultCount) & ",")
                        Else
                            Exit For
                        End If
                    Next
                    resultString.Remove(resultString.Length - 1, 1)
                End If
            End If
            If myAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                SAPInfo.TrackIfStudentPassedSAPSpeedSkills = (New RegisterStudentsBR).CheckIfStudentPassedSpeedRequirementforSAP(SAPInfo.SAPDetailId.ToString, StudentId.ToString)
                If SAPInfo.TrackIfStudentPassedSAPSpeedSkills = False Then
                    resultString.Append("Student has not satisified the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                Else
                    resultString.Append("Student has satisified the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                End If
            End If


            'Record the trigger as being performed.
            SapChkInfo.StudentId = XmlConvert.ToGuid(StudentId)
            SapChkInfo.Period = SAPInfo.Period
            ''FOR The FA SAP has to be always TRUE
            SapChkInfo.Period = SAPInfo.Period
            If resultString.Length <> 0 Then
                SapChkInfo.IsMakingSAP = False
            Else
                SapChkInfo.IsMakingSAP = True
            End If
            SapChkInfo.CredsEarned = CredsCompleted
            SapChkInfo.CredsAttempted = CredsAttempted
            'SapChkInfo.GPA = CumGPA
            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                SapChkInfo.GPA = SAPInfo.OverallAverage
            Else
                SapChkInfo.GPA = CumGPA
            End If
            SapChkInfo.DatePerf = DateTime.Now
            SapChkInfo.TermStartDate = SAPInfo.TermSDate

            'Added By Balaji on 11/17/2004 to capture reason
            'Modification starts here 
            If resultString.ToString.Length > 900 Then
                SapChkInfo.Comments = resultString.ToString.Substring(0, 900)
            Else
                SapChkInfo.Comments = resultString.ToString
            End If
            If SapChkInfo.IsMakingSAP Then SapChkInfo.Comments = passComments.ToString
            'Modification ends here

            'Added by Bhavana to store SAPDetailId in SAPChkResults table 11/02/05
            SapChkInfo.SAPDetailId = XmlConvert.ToGuid(SAPDetailId)
            SapChkInfo.PercAttendance = AttendanceCompleted
            SapChkInfo.FinCredsEarned = SAPInfo.FinCredsEarned
            'SapChkInfo.PercCredsEarned = SAPInfo.PercCredsEarned
            SapChkInfo.PercCredsEarned = PercCompltd
            'SapChkInfo.CheckPointDate = SAPInfo.OffsetDate
            If SAPInfo.OverallAttendance Then
                db.AddTrigFASAPToDB(SapChkInfo, user)
            Else
                db.AddTrigFASAPToDB(SapChkInfo, user, SAPInfo)
            End If




            Return resultString.ToString
        End Function
    End Class
    Public Class SAPMonths
        Inherits SAP
        Public Overrides Function IsMakingSAP(ByVal StudentId As String, ByVal SAPInfo As SAPInfo, ByVal PrgVerId As String, ByVal user As String, ByVal gradeReps As String, Optional ByVal TermEDate As Date = #1/1/1001#, Optional campusId As String = "") As String
            Dim db As New SAPCheckDB
            Dim TrigUnit As String
            Dim TrigOffsetTyp As String
            Dim QuantMinUnitTyp As String
            Dim QualTyp As String
            Dim QuantMinVal As Decimal
            Dim QualMinVal As Decimal
            Dim PercCompltd As Decimal
            Dim CumGPA As Decimal
            'Dim currdate As Date
            Dim TrigOffSeq As Integer
            Dim Seq As Integer
            Dim resultString As New StringBuilder
            Dim passComments As New StringBuilder
            Dim SapChkInfo As New SAPCheckInfo
            Dim TrigVal As Decimal
            Dim SAPDetailId As String
            Dim AttendanceRequired As Decimal
            Dim AttendanceCompleted As Decimal

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


            QuantMinVal = SAPInfo.QuantMinValue
            QualMinVal = SAPInfo.QualMinValue
            QualTyp = SAPInfo.QualType
            TrigOffsetTyp = SAPInfo.TrigOffsetTyp
            TrigOffSeq = SAPInfo.TrigOffsetSeq
            Seq = SAPInfo.Seq
            QuantMinUnitTyp = SAPInfo.QuantMinUnitType
            If (TrigOffsetTyp = "start of term") Or (TrigOffsetTyp = "end of term") Or (TrigOffsetTyp = "start date") Then
                TermEDate = SAPInfo.TermEDate

            Else
                TermEDate = #1/1/1001#
            End If
            'currdate = #9/30/2005#
            TrigUnit = SAPInfo.TrigUnit
            TrigVal = SAPInfo.TrigValue
            SAPDetailId = SAPInfo.SAPDetailId.ToString
            AttendanceRequired = SAPInfo.MinAttendanceValue


            'Select Case Trim(TrigOffsetTyp)
            '    Case "Start Date"
            '        ''Get Student start date
            '        'ds3 = db.GetStuStartDate(StudentId)
            '        'CurrRow = ds3.Tables(0).Rows(0)
            '        ''Get the value of the StartDate Column in the table
            '        'StartDate = CurrRow("StartDate")

            '    Case "start of term"
            '        ''Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        'CurRow = ds4.Tables(0).Rows(TrigOffSeq)
            '        'TermId = CurRow("TermId")
            '        ''Get Results for this Term.
            '        ''ds5 = db.GetTermResults(TermId)

            '    Case "end of term"
            '        'If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
            '        '    resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
            '        'End If

            '        'Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        ''INSERT CODE TO LOOP THRU AND GET TERMIDs ONLY UPTO
            '        ''THE ROW(TRIGOFFSETSEQ)!!!!!

            '        'For Each row In ds4.Tables(0).Rows
            '        '    str &= row("TermId").ToString & ","
            '        '    int += 1
            '        '    If int = TrigOffSeq Then
            '        '        Exit For
            '        '    End If
            '        'Next
            '        'intpos = str.LastIndexOf(",")
            '        'str = str.Substring(0, intpos)
            '        '*************************************
            '        ' pos = InStrRev(str, ",")
            '        'str = Mid(str, 1, pos - 1)
            '        ' str = TermId
            'End Select

            'Check if Student is meeting Qualitative and Quantitative Reqs
            '**************************************************************************************
            'The following lines of code has been replaced to use the centralized component for 
            'computing the Percentage of Credits Earned (or Completed) instead of an independent function.
            'Modified By Michelle R. Rodriguez 05/15/06
            '**************************************************************************************
            'PercCompltd = CDec(ProcessQuantMinUnitTyp(QuantMinUnitTyp, StudentId, PrgVerId, TrigUnit, TrigVal, TermEDate))
            If SAPInfo.attType = "CLOCK HOURS" Then
                If SAPInfo.ActHours <= 0 Or SAPInfo.SchedHours <= 0 Then
                    PercCompltd = 0
                Else
                    PercCompltd = Math.Round((SAPInfo.ActHoursAdjusted / SAPInfo.SchedHours) * 100, 2)
                End If
                If PercCompltd >= QuantMinVal Then
                    'Quantitative measure met
                Else
                    resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If
                AttendanceCompleted = PercCompltd
            ElseIf myAdvAppSettings.AppSettings("tracksapattendance").ToLower = "byday" Then
                Dim attUnitType As String = ""
                Dim dal As New ClsSectAttendanceDB()
                Dim externScheduleHrs As Decimal = 0.0
                Dim externActualHrs As Decimal = 0.0
                'Dim stuEnrollId As String = (New SAPActualHours).getEnrollmentId(StudentId, PrgVerId)
                attUnitType = dal.GetPrgVersionAttendanceType(StudentId, False)
                If attUnitType <> Nothing Then
                    If attUnitType.ToString.Substring(0, 7).ToLower = "present" Then
                        attUnitType = "pa"
                    End If
                    If SAPInfo.TrackExternAttendance = True Then
                        Dim externAttendance As Decimal() = (New SapDB).GetHoursForExternships(StudentId, SAPInfo.OffsetDate)
                        externScheduleHrs = externAttendance(0)
                        externActualHrs = externAttendance(1)
                    End If
                    If SAPInfo.TrackExternAttendance = True Then

                        With (New AttendanceDB).StudentSummaryFormForCutOffDate(StudentId, attUnitType, SAPInfo.OffsetDate)
                            If (.TotalHoursPresent + externActualHrs) <= 0 Or (.TotalHoursSched + externScheduleHrs) <= 0 Then
                                PercCompltd = 0
                            Else
                                PercCompltd = Math.Round(((.TotalHoursPresent + externActualHrs) / (.TotalHoursSched + externScheduleHrs)) * 100, 2)
                            End If
                        End With
                    Else
                        With (New AttendanceDB).StudentSummaryFormForCutOffDate(StudentId, attUnitType, SAPInfo.OffsetDate)
                            If .TotalHoursPresent <= 0 Or .TotalHoursSched <= 0 Then
                                PercCompltd = 0
                            Else
                                PercCompltd = Math.Round((.TotalHoursPresent / .TotalHoursSched) * 100, 2)
                            End If
                        End With
                    End If

                End If
                AttendanceCompleted = PercCompltd
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    If SAPInfo.ActHoursAdjusted > 0 And SAPInfo.SchedHours > 0 Then
                        PercCompltd = SAPInfo.ActHoursAdjusted * 100 / SAPInfo.SchedHours
                    Else
                        PercCompltd = 0
                    End If
                Else
                    PercCompltd = SAPInfo.PercCredsEarned
                End If
            Else
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    If SAPInfo.ActHoursAdjusted > 0 And SAPInfo.SchedHours > 0 Then
                        PercCompltd = SAPInfo.ActHoursAdjusted * 100 / SAPInfo.SchedHours
                    Else
                        PercCompltd = 0
                    End If
                Else
                    PercCompltd = SAPInfo.PercCredsEarned
                End If
                AttendanceCompleted = Math.Round(GetAttendancePercentageByStudentEnrollment(StudentId, SAPInfo.OffsetDate), 2)
            End If

            '**************************************************************************************
            'The following line of code has been replaced to use the centralized component for 
            'computing GPA instead of an independent function by BN 04/26/06
            '**************************************************************************************
            'CumGPA = CDec(ProcessQualTyp(QualTyp, StudentId, CredsCompleted, PrgVerId, TrigUnit, TrigVal, TermEDate))
            CumGPA = SAPInfo.GPA
            CredsCompleted = SAPInfo.CredsEarned

            If myAdvAppSettings.AppSettings("GradesFormat", campusId) = "Numeric" Then
                If SAPInfo.OverallAverage >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    'not met qual. meaasure
                    'Return msg
                    resultString.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                    'ErrMsgObj.ErrMsg = msg
                End If
                'If SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has  completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                End If
                'End If
            Else

                If CumGPA >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    'not met qual. meaasure
                    'Return msg
                    resultString.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                    'ErrMsgObj.ErrMsg = msg
                End If

                If myAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                    SAPInfo.TrackIfStudentPassedSAPSpeedSkills = (New RegisterStudentsBR).CheckIfStudentPassedSpeedRequirementforSAP(SAPInfo.SAPDetailId.ToString, StudentId.ToString)
                    If SAPInfo.TrackIfStudentPassedSAPSpeedSkills = False Then
                        resultString.Append("Student has not satisfied the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                    Else
                        passComments.Append("Student has satisfied the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                    End If
                End If

                'Check if student has met minimum credits required
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has  completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                End If
                If resultString.Length = 0 Then
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.MinTermGPA <> 0 Then
                        If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
                            resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        Else
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                Else
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.TermGPAOver <> 0 Then
                        If SAPInfo.TermGPA > SAPInfo.TermGPAOver Then
                            resultString.Remove(0, resultString.Length)
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                End If
                'Check if student has met minimum attendance required

            End If
            If AttendanceCompleted >= AttendanceRequired Then
                'Min Percent of Attendance Met
                passComments.Append("Student has " + CType(AttendanceCompleted, String) + " % of attendance which is more than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            Else
                'not met Min Percent of Attendance
                'Return msg
                resultString.Append("Student only has " + CType(AttendanceCompleted, String) + " % of attendance which is less than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            End If
            If PercCompltd >= QuantMinVal Then
                'Quantitative measure met
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    passComments.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                Else
                    passComments.Append("Student has completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If
            ElseIf SAPInfo.QuantMinUnitTypId = "3" Then
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            Else
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            End If
            If myAdvAppSettings.AppSettings("PassAllCoursesForSAPCheck").ToLower = "true" And myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then

                Dim result() As String = IsPass(db.GetWorkUnitResults(StudentId, SAPInfo.OffsetDate), StudentId)
                Dim resultCount As Integer = 0
                If result IsNot Nothing Then
                    resultString.Append(" Student failed in the following Courses.")
                    For resultCount = 0 To result.Length - 1
                        If result(resultCount) IsNot Nothing Then
                            resultString.Append(result(resultCount) & ",")
                        Else
                            Exit For
                        End If
                    Next
                    resultString.Remove(resultString.Length - 1, 1)
                End If
            End If


            SapChkInfo.Period = SAPInfo.Period
            If resultString.Length <> 0 Then
                SapChkInfo.IsMakingSAP = False
            Else
                SapChkInfo.IsMakingSAP = True
            End If
            SapChkInfo.CredsEarned = CredsCompleted
            SapChkInfo.CredsAttempted = CredsAttempted
            'SapChkInfo.GPA = CumGPA
            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                SapChkInfo.GPA = SAPInfo.OverallAverage
            Else
                SapChkInfo.GPA = CumGPA
            End If
            SapChkInfo.DatePerf = DateTime.Now
            SapChkInfo.CheckPointDate = SAPInfo.OffsetDate
            SapChkInfo.TermStartDate = SAPInfo.TermSDate

            'Added by Bhavana to capture reason 08/23/05
            If resultString.ToString.Length > 900 Then
                SapChkInfo.Comments = resultString.ToString.Substring(0, 900)
            Else
                SapChkInfo.Comments = resultString.ToString
            End If
            If SapChkInfo.IsMakingSAP Then SapChkInfo.Comments = passComments.ToString
            'Added by Bhavana to store SAPDetailId in SAPChkResults table 11/02/05
            SapChkInfo.SAPDetailId = XmlConvert.ToGuid(SAPDetailId)
            SapChkInfo.PercAttendance = AttendanceCompleted
            SapChkInfo.FinCredsEarned = SAPInfo.FinCredsEarned
            SapChkInfo.PercCredsEarned = SAPInfo.PercCredsEarned
            SapChkInfo.StudentId = New Guid(StudentId)

            db.AddTrigToDB(SapChkInfo, user)

            Return resultString.ToString
        End Function

        Public Overrides Function IsMakingFASAP(ByVal StudentId As String, ByVal SAPInfo As SAPInfo, ByVal PrgVerId As String, ByVal user As String, ByVal gradeReps As String, Optional ByVal TermEDate As Date = #1/1/1001#, Optional campusId As String = "") As String
            Dim db As New SAPCheckDB
            Dim TrigUnit As String
            Dim TrigOffsetTyp As String
            Dim QuantMinUnitTyp As String
            Dim QualTyp As String
            Dim QuantMinVal As Decimal
            Dim QualMinVal As Decimal
            Dim PercCompltd As Decimal
            Dim CumGPA As Decimal
            'Dim currdate As Date
            Dim TrigOffSeq As Integer
            Dim Seq As Integer
            Dim resultString As New StringBuilder
            Dim passComments As New StringBuilder
            Dim SapChkInfo As New SAPCheckInfo
            Dim TrigVal As Decimal
            Dim SAPDetailId As String
            Dim AttendanceRequired As Decimal
            Dim AttendanceCompleted As Decimal

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            QuantMinVal = SAPInfo.QuantMinValue
            QualMinVal = SAPInfo.QualMinValue
            QualTyp = SAPInfo.QualType
            TrigOffsetTyp = SAPInfo.TrigOffsetTyp
            TrigOffSeq = SAPInfo.TrigOffsetSeq
            Seq = SAPInfo.Seq
            QuantMinUnitTyp = SAPInfo.QuantMinUnitType
            If (TrigOffsetTyp = "start of term") Or (TrigOffsetTyp = "end of term") Or (TrigOffsetTyp = "start date") Then
                TermEDate = SAPInfo.TermEDate

            Else
                TermEDate = #1/1/1001#
            End If
            'currdate = #9/30/2005#
            TrigUnit = SAPInfo.TrigUnit
            TrigVal = SAPInfo.TrigValue
            SAPDetailId = SAPInfo.SAPDetailId.ToString
            AttendanceRequired = SAPInfo.MinAttendanceValue


            'Select Case Trim(TrigOffsetTyp)
            '    Case "Start Date"
            '        ''Get Student start date
            '        'ds3 = db.GetStuStartDate(StudentId)
            '        'CurrRow = ds3.Tables(0).Rows(0)
            '        ''Get the value of the StartDate Column in the table
            '        'StartDate = CurrRow("StartDate")

            '    Case "start of term"
            '        ''Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        'CurRow = ds4.Tables(0).Rows(TrigOffSeq)
            '        'TermId = CurRow("TermId")
            '        ''Get Results for this Term.
            '        ''ds5 = db.GetTermResults(TermId)

            '    Case "end of term"
            '        'If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
            '        '    resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
            '        'End If

            '        'Term that the student is currently enrolled in.
            '        'ds4 = db.GetStuTerms(StudentId)
            '        ''INSERT CODE TO LOOP THRU AND GET TERMIDs ONLY UPTO
            '        ''THE ROW(TRIGOFFSETSEQ)!!!!!

            '        'For Each row In ds4.Tables(0).Rows
            '        '    str &= row("TermId").ToString & ","
            '        '    int += 1
            '        '    If int = TrigOffSeq Then
            '        '        Exit For
            '        '    End If
            '        'Next
            '        'intpos = str.LastIndexOf(",")
            '        'str = str.Substring(0, intpos)
            '        '*************************************
            '        ' pos = InStrRev(str, ",")
            '        'str = Mid(str, 1, pos - 1)
            '        ' str = TermId
            'End Select

            'Check if Student is meeting Qualitative and Quantitative Reqs
            '**************************************************************************************
            'The following lines of code has been replaced to use the centralized component for 
            'computing the Percentage of Credits Earned (or Completed) instead of an independent function.
            'Modified By Michelle R. Rodriguez 05/15/06
            '**************************************************************************************
            'PercCompltd = CDec(ProcessQuantMinUnitTyp(QuantMinUnitTyp, StudentId, PrgVerId, TrigUnit, TrigVal, TermEDate))
            If SAPInfo.attType = "CLOCK HOURS" Then
                If SAPInfo.ActHours <= 0 Or SAPInfo.SchedHours <= 0 Then
                    PercCompltd = 0
                Else
                    PercCompltd = Math.Round((SAPInfo.ActHoursAdjusted / SAPInfo.SchedHours) * 100, 2)
                End If
                If PercCompltd >= QuantMinVal Then
                    'Quantitative measure met
                Else
                    resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If
                AttendanceCompleted = PercCompltd
            ElseIf myAdvAppSettings.AppSettings("tracksapattendance").ToLower = "byday" Then
                Dim attUnitType As String
                Dim dal As New ClsSectAttendanceDB()
                Dim externScheduleHrs As Decimal = 0.0
                Dim externActualHrs As Decimal = 0.0
                'Dim stuEnrollId As String = (New SAPActualHours).getEnrollmentId(StudentId, PrgVerId)
                attUnitType = dal.GetPrgVersionAttendanceType(StudentId, False)
                If attUnitType <> Nothing Then
                    If attUnitType.ToString.Substring(0, 7).ToLower = "present" Then
                        attUnitType = "pa"
                    End If
                    If SAPInfo.TrackExternAttendance = True Then
                        Dim externAttendance As Decimal() = (New SapDB).GetHoursForExternships(StudentId, SAPInfo.OffsetDate)
                        externScheduleHrs = externAttendance(0)
                        externActualHrs = externAttendance(1)
                    End If
                    If SAPInfo.TrackExternAttendance = True Then

                        With (New AttendanceDB).StudentSummaryFormForCutOffDate(StudentId, attUnitType, SAPInfo.OffsetDate)
                            If (.TotalHoursPresent + externActualHrs) <= 0 Or (.TotalHoursSched + externScheduleHrs) <= 0 Then
                                PercCompltd = 0
                            Else
                                PercCompltd = Math.Round(((.TotalHoursPresent + externActualHrs) / (.TotalHoursSched + externScheduleHrs)) * 100, 2)
                            End If
                        End With
                    Else
                        With (New AttendanceDB).StudentSummaryFormForCutOffDate(StudentId, attUnitType, SAPInfo.OffsetDate)
                            If .TotalHoursPresent <= 0 Or .TotalHoursSched <= 0 Then
                                PercCompltd = 0
                            Else
                                PercCompltd = Math.Round((.TotalHoursPresent / .TotalHoursSched) * 100, 2)
                            End If
                        End With
                    End If

                End If
                AttendanceCompleted = PercCompltd
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    If SAPInfo.ActHoursAdjusted > 0 And SAPInfo.SchedHours > 0 Then
                        PercCompltd = SAPInfo.ActHoursAdjusted * 100 / SAPInfo.SchedHours
                    Else
                        PercCompltd = 0
                    End If
                Else
                    PercCompltd = SAPInfo.PercCredsEarned
                End If
            Else
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    If SAPInfo.ActHoursAdjusted > 0 And SAPInfo.SchedHours > 0 Then
                        PercCompltd = SAPInfo.ActHoursAdjusted * 100 / SAPInfo.SchedHours
                    Else
                        PercCompltd = 0
                    End If
                Else
                    PercCompltd = SAPInfo.PercCredsEarned
                End If
                AttendanceCompleted = Math.Round(GetAttendancePercentageByStudentEnrollment(StudentId, SAPInfo.OffsetDate), 2)
            End If

            '**************************************************************************************
            'The following line of code has been replaced to use the centralized component for 
            'computing GPA instead of an independent function by BN 04/26/06
            '**************************************************************************************
            'CumGPA = CDec(ProcessQualTyp(QualTyp, StudentId, CredsCompleted, PrgVerId, TrigUnit, TrigVal, TermEDate))
            CumGPA = SAPInfo.GPA
            CredsCompleted = SAPInfo.CredsEarned

            If myAdvAppSettings.AppSettings("GradesFormat", campusId) = "Numeric" Then
                If SAPInfo.OverallAverage >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    'not met qual. meaasure
                    'Return msg
                    resultString.Append("Student has a Overall Average  of " + SAPInfo.OverallAverage.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                    'ErrMsgObj.ErrMsg = msg
                End If
                'If SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has  completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                End If
                'End If
            Else

                If CumGPA >= QualMinVal Then
                    'Qualitative measure met
                    passComments.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is more than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                Else
                    'not met qual. meaasure
                    'Return msg
                    resultString.Append("Student has a Cumulative GPA of " + CumGPA.ToString("#0.00") + " which is less than the minimum of " + CType(QualMinVal, String) + ". " + vbCr)
                    'ErrMsgObj.ErrMsg = msg
                End If

                If myAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                    SAPInfo.TrackIfStudentPassedSAPSpeedSkills = (New RegisterStudentsBR).CheckIfStudentPassedSpeedRequirementforSAP(SAPInfo.SAPDetailId.ToString, StudentId.ToString)
                    If SAPInfo.TrackIfStudentPassedSAPSpeedSkills = False Then
                        resultString.Append("Student has not satisified the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                    Else
                        passComments.Append("Student has satisified the shorthand skill level  or accuracy requirement defined in the SAP Policy. ")
                    End If
                End If

                'Check if student has met minimum credits required
                If CredsCompleted >= SAPInfo.MinCredsCompltd Then
                    'Met
                    passComments.Append("Student has  completed " + CType(CredsCompleted, String) + " credits which is more than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                Else
                    resultString.Append("Student has only completed " + CType(CredsCompleted, String) + " credits which is less than the minimum of " + CType(SAPInfo.MinCredsCompltd, String) + " credits." + vbCr)
                End If
                If resultString.Length = 0 Then
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.MinTermGPA <> 0 Then
                        If SAPInfo.TermGPA < SAPInfo.MinTermGPA Then
                            resultString.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is less than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        Else
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                Else
                    If Trim(TrigOffsetTyp).ToLower = "end of term" And SAPInfo.TermGPAOver <> 0 Then
                        If SAPInfo.TermGPA > SAPInfo.TermGPAOver Then
                            resultString.Remove(0, resultString.Length)
                            passComments.Append("Student has a Term GPA of " + SAPInfo.TermGPA.ToString("#0.00") + " which is more than the minimum of " + CType(SAPInfo.MinTermGPA, String) + ". " + vbCr)
                        End If
                    End If
                End If
                'Check if student has met minimum attendance required

            End If
            If AttendanceCompleted >= AttendanceRequired Then
                'Min Percent of Attendance Met
                passComments.Append("Student has " + CType(AttendanceCompleted, String) + " % of attendance which is more than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            Else
                'not met Min Percent of Attendance
                'Return msg
                resultString.Append("Student only has " + CType(AttendanceCompleted, String) + " % of attendance which is less than the minimum of " + CType(AttendanceRequired, String) + " % of attendance." + vbCr)
            End If
            If PercCompltd >= QuantMinVal Then
                'Quantitative measure met
                If SAPInfo.QuantMinUnitTypId = "3" Then
                    resultString.Append("Student has  completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                Else
                    resultString.Append("Student has completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is more than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
                End If
            ElseIf SAPInfo.QuantMinUnitTypId = "3" Then
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of scheduled hours/days attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            Else
                resultString.Append("Student has only completed " + PercCompltd.ToString("#0.00") + " % of credits attempted which is less than the minimum of " + CType(QuantMinVal, String) + "%. " + vbCr)
            End If
            If myAdvAppSettings.AppSettings("PassAllCoursesForSAPCheck").ToLower = "true" And myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then

                'If Not IsPass(db.GetWorkUnitResults(StudentId, SAPInfo.OffsetDate)) Then
                '    resultString.Append("Student failed in any of the courses.")
                'End If
                Dim result() As String = IsPass(db.GetWorkUnitResults(StudentId, SAPInfo.OffsetDate), StudentId)
                Dim resultCount As Integer
                If result IsNot Nothing Then
                    resultString.Append(" Student failed in the following Courses.")
                    For resultCount = 0 To result.Length - 1
                        If result(resultCount) IsNot Nothing Then
                            resultString.Append(result(resultCount) & ",")
                        Else
                            Exit For
                        End If
                    Next
                    resultString.Remove(resultString.Length - 1, 1)
                End If
            End If


            SapChkInfo.Period = SAPInfo.Period
            If resultString.Length <> 0 Then
                SapChkInfo.IsMakingSAP = False
            Else
                SapChkInfo.IsMakingSAP = True
            End If
            SapChkInfo.CredsEarned = CredsCompleted
            SapChkInfo.CredsAttempted = CredsAttempted
            'SapChkInfo.GPA = CumGPA
            If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                SapChkInfo.GPA = SAPInfo.OverallAverage
            Else
                SapChkInfo.GPA = CumGPA
            End If
            SapChkInfo.DatePerf = DateTime.Now
            SapChkInfo.CheckPointDate = SAPInfo.OffsetDate
            SapChkInfo.TermStartDate = SAPInfo.TermSDate

            'Added by Bhavana to capture reason 08/23/05
            If resultString.ToString.Length > 900 Then
                SapChkInfo.Comments = resultString.ToString.Substring(0, 900)
            Else
                SapChkInfo.Comments = resultString.ToString
            End If
            If SapChkInfo.IsMakingSAP Then SapChkInfo.Comments = passComments.ToString
            'Added by Bhavana to store SAPDetailId in SAPChkResults table 11/02/05
            SapChkInfo.SAPDetailId = XmlConvert.ToGuid(SAPDetailId)
            SapChkInfo.PercAttendance = AttendanceCompleted
            SapChkInfo.FinCredsEarned = SAPInfo.FinCredsEarned
            SapChkInfo.PercCredsEarned = SAPInfo.PercCredsEarned
            db.AddTrigFASAPToDB(SapChkInfo, user)

            Return resultString.ToString
        End Function
    End Class

End Namespace