﻿(function () {
    var ajaxSetup = function() {
        jQuery.ajaxSetup({
            timeout: 25000, //25 seconds,
            dataType: 'json',
            contentType: 'application/json'
        });
        
        jQuery(document)
          .ajaxStart(function () {
              vm.loading(true);
          }).ajaxComplete(function() {
              vm.loading(false);
          });
    };
    
    var viewModel = function () {
        var self = { };

        self.selectedTenant = ko.observable();
        self.tenants = ko.observableArray();
        self.loading = ko.observable(false);

        self.addKey = function() {
            jQuery.ajax({
                type: 'POST',
                url: 'api/ApiAuthenticationKey/',
                data: JSON.stringify({ TenantId: self.selectedTenant().Id }),
                dataType: 'text'
            })
                .done(function() {
                    window.models.tenant.get(self.selectedTenant().Id)
                        .done(function (data) {
                            var newTenant = window.models.tenant(data);
                            swapExistingTenantWithNewTenant(newTenant);
                        });
                })
                .fail(function() { alert('Your key could not be added. Try again.'); });
        };
        
        self.deleteKey = function (keyId) {
            jQuery.ajax({
                url: 'api/ApiAuthenticationKey/' + keyId,
                method: 'DELETE'
            })
                .done(function() {
                    window.models.tenant.get(self.selectedTenant().Id)
                        .done(function (data) {
                            var newTenant = window.models.tenant(data);
                            swapExistingTenantWithNewTenant(newTenant);
                        });
                })
                .fail(function() { alert('Your key could not be deleted. Try again.'); });
        };
        
        self.toggleKeyVisibility = function (key) {
            key.showLongKey(!key.showLongKey());
        };

        var swapExistingTenantWithNewTenant = function(newTenant) {
            var existingTenant = _.find(self.tenants(), function (tenantItem) { return tenantItem.Id === newTenant.Id; });
            var existingTenantIndex = self.tenants.indexOf(existingTenant);
            self.tenants()[existingTenantIndex] = newTenant;
            self.tenants.valueHasMutated();
            self.selectedTenant(newTenant);
        };

        self.seletedTenantKeys = function () {
            return self.isTenantSelected() ? self.selectedTenant().Keys : [];
        };

        self.headerTextForKeys = function() {
            return self.isTenantSelected() ? "Keys for '" + self.selectedTenant().Name + "'" : "Select a Tenant on the left";
        };

        self.isTenantSelected = function() {
            return !_.isUndefined(self.selectedTenant());
        };

        self.selectTenant = function (tenant) {
            self.selectedTenant(tenant);
        };

        return self;
    };

    var vm = viewModel();

    jQuery(function () {
        ajaxSetup();

        window.models.tenant.getAll().done(function (data) {
            var tenants = jQuery.map(data, function (tenantData) { return window.models.tenant(tenantData); });
            ko.utils.arrayPushAll(vm.tenants, tenants);
        });
        
        ko.applyBindings(vm);
    });
})();