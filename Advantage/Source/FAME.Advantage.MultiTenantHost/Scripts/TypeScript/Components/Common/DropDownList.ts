﻿/// <reference path="../../../../Kendo/typescript/kendo.all.d.ts" />
/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
module Api.Components {
    import IFilterParams = API.Models.IFilterParameters;
    import ISettings = API.Models.ISettings;
    import IDataProvider = API.Models.IDataProvider;
    export class DropDownList {
        private componentId: string;
        private selectedId: string;
        private filterParams: IFilterParams;
        private dataProvider: IDataProvider;
        private dropDownList: kendo.ui.DropDownList;

        constructor(componentId: string, dataProvider: IDataProvider, filterParams?: IFilterParams, settings?: ISettings, onchange?: (response) => any) {
            /**
             * Declaring the UserType API object. The UserType API encapsulate the User Type Controller.
             * We will use the UserType API object to get the kendo data source for the UserType dropdown.
             */
            let that = this;
            /**
             * In this step we are concatenating the # with the component id so we can find the HTML control on the using jquery and them with the
             * jquery reference to that DOM object the kendo UI control wil be created.
             */
            componentId = "#" + componentId;

            /**
             * storing the component id in the global variable scope so it can be referenced elsewhere
             */
            that.componentId = componentId;
            that.dataProvider = dataProvider;
            that.filterParams = filterParams;

            let defaults: Object = {
                dataSource: dataProvider.getDataSource(() => that.onRequestFilterData()),
                optionLabel: "Select",
                dataTextField: "text",
                dataValueField: "value"
            };

            if (settings) {
                $.extend(defaults, settings.settings);
            }
            /**
             * declaration of the kendo ui control.
             */
            $(componentId).kendoDropDownList(defaults).change((value) => {
                if (value)
                    that.selectedId = $(that.componentId).data("kendoDropDownList").value();

                onchange(value);
            });

            that.dropDownList = $(componentId).data("kendoDropDownList");


        }
        private onRequestFilterData(): any {
            let filter: Object = {};
            if (this.filterParams) {
                filter = $.extend(filter, this.filterParams.params);

            }
            return filter;
        }
        private setParameters(filterParams: IFilterParams) {
            this.filterParams = filterParams;
        }
        getSelected() {
            return this.selectedId;
        }
        reset() {
            this.dropDownList.value("");
        }
        reload(filterParams?: IFilterParams) {
            if (filterParams) {
                this.setParameters(filterParams);

            }
            this.dropDownList.setDataSource(this.dataProvider.getDataSource(() => this.onRequestFilterData()));
        }
        hasValue() {
            return this.dropDownList.value() !== "";
        }
        getValue() {
            return this.dropDownList.value();
        }
        setValue(newValue: string) {
            this.dropDownList.value(newValue);
        }
        getKendoDropDown() {
            return this.dropDownList;
        }
    }
}