﻿module API.Common 
{
    export var X_PLEASE_FILL_REQUIRED_FIELDS = "Please enter the required values.";
    export var X_ACCEPT_TERMS_AND_CONDITIONS = "Accept the terms and conditions."
    export var PASSWORD_UPDATE_SUCCESSFUL = "Password set updated successfully.";
    export var UPDATE_FAILED = "Failed updating record.";
}