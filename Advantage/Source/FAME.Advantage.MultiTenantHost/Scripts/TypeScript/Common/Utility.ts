﻿module API.Common {

    /**
        * Utility class will provide the common functionalities across the typescript project.        
       */
    export class Utility {

        //this method convert given number into currency format and return a string.
        public static formatNumberAsCurrency(value: number): string {
            return value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        //this method convert given number into percentage format and return a string.
        public static formatNumberAsPercenatgeResults(value: number): string {
            return value.toFixed(1).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        //this method convert given number into currency format and return a string.
        public static formatNumberAsCurrencyPercenatgeTab(value: number): string {
            return value.toFixed(3).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }

        public static getUrlParameter(sParam: any): string {
            var sPageUrl = decodeURIComponent(window.location.search.substring(1)),
                sUrlVariables = sPageUrl.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sUrlVariables.length; i++) {
                sParameterName = sUrlVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        // Method to convert date into MM/dd/yyyy format
        public static formatDate(inputDate: any): string {
            let date = new Date(inputDate);
            // Months use 0 index.
            return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
        }

    }
}