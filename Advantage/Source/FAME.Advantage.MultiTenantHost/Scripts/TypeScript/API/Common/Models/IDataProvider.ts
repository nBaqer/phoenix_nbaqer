﻿module API.Models {
    export interface IDataProvider {
        getDataSource: (onRequestFilterData: () => any) => kendo.data.DataSource;

    }
}