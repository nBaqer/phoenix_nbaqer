﻿/// <reference path="../../TypeScript/Common/AdvantageMessageBoxs.ts" />
module Api {
    export enum RequestParameterType {
        QueryString = 1,
        Body = 2
    };
    export enum RequestType {
        Get = 1,
        Post = 2,
        Put = 3,
        Delete = 4
    }
    export enum ReportOutput {
        /// <summary>
        /// The pdf.
        /// </summary>
        Pdf = 1
    }
    /**
     * The request class encapsulates the communication between the host site and the api.
     * This request is intended to be done without requiring authentication to access the api.
     */
    export class Request { 
        jsonWebToken: string;
        apiDomainName: string;
        shouldDoRequest: boolean;
        shouldShowIsLoading: boolean;

        constructor() {
          
            this.shouldDoRequest = true;
            this.shouldShowIsLoading = true;
            this.apiDomainName = sessionStorage.getItem("AdvantageApiUrl");
            this.jsonWebToken = sessionStorage.getItem("AdvantageApiToken");

            if (this.apiDomainName === null ||
                this.apiDomainName === "" 
                ) {
                this.shouldDoRequest = false;
                $.when(MasterPage
                    .SHOW_ERROR_WINDOW(
                        "An error has occured. Unable to access the advantage web api, this may be due to invalid configuration. Please try again later."));
            } else {
                if (this.apiDomainName.charAt(this.apiDomainName.length - 1) !== "/") {
                    this.apiDomainName = this.apiDomainName + "/";
                }
            }
        }

        public setShouldShowIsLoading(value: boolean) {
            this.shouldShowIsLoading = value;
        }

        /**
         * Send a custom ajax request to the API.
         * You can specify if the request is a POST or Get, the route to call the paramters to pass based on it's type (query string or body) and an event to be fired when the
         * request is ready.
         * This method encapculates error handling and token expiration exception.
         * @param requestType
         * This is an enum that identifies the type of HTTP request (GET, POST, PUT or DELETE)
         * @param route
         * The controller action to be called
         * @param requestData
         * The object to be pass on the request as a parameter based on it's type
         * @param typeOfParameter
         * This is a enum that identify how do you want to pass the requestData to the API. Wether you want to pass it as Query String or as Body.
         * @param onResponseCallback
         * An event that will be called when the response is ready.
         */
        public send(requestType: RequestType,
            route: string,
            requestData: any,
            typeOfParameter: RequestParameterType,
            onResponseCallback: (response: Response) => any): void {

            let that = this;

            if (that.shouldDoRequest) {
                let requestSettings: JQueryAjaxSettings;
                let callback = onResponseCallback;
                route = that.apiDomainName + route;

                let typeOfRequest = that.getTypeOfRequest(requestType);

                this.showIsPageLoading(this.shouldShowIsLoading);

                if (typeOfParameter === RequestParameterType.QueryString) {
                    requestSettings = {
                        type: typeOfRequest, //GET, POST, PUT
                        url: route + "?" + $.param(requestData), //the url to call
                        contentType: "application/json",
                        dataType: 'json',
                        global: false,
                        timeout: 20000,
                        beforeSend: xhr => {
                            //Include the bearer token in header
                            if (that.jsonWebToken !== null &&
                                that.jsonWebToken !== "" &&
                                that.jsonWebToken !== undefined) {
                                xhr.setRequestHeader("Authorization", 'Bearer ' + that.jsonWebToken);
                            }
                            
                        }
                    };
                } else {
                    requestSettings = {
                        type: typeOfRequest, //GET, POST, PUT
                        url: route, //the url to call
                        data: JSON.stringify(requestData), //Data sent to server
                        contentType: "application/json",
                        dataType: 'json',
                        global: false,
                        timeout: 20000,
                        beforeSend: xhr => {
                            //Include the bearer token in header
                            if (that.jsonWebToken !== null &&
                                that.jsonWebToken !== "" &&
                                that.jsonWebToken !== undefined) {
                                xhr.setRequestHeader("Authorization", 'Bearer ' + that.jsonWebToken);
                            }
                        }
                    };
                }

                $.ajax(requestSettings).done((ajaxResponse) => {
                    that.showIsPageLoading(false);
                    callback(ajaxResponse);
                }).fail((error) => {
                    that.showIsPageLoading(false);
                    that.apiExceptionHandler(error);
                });
            }
        }

        /**
        * Send a custom ajax request to the API.
        * You can specify if the request is a POST or Get, the route to call the paramters to pass based on it's type (query string or body) and an event to be fired when the
        * request is ready.
        * This method encapculates error handling and token expiration exception.
        * @param requestType
        * The type of HTTP request (GET, POST, PUT or DELETE)
        * @param route
        * The controller action to be called
        * @param requestData
        * The object to be pass on the request as a parameter based on it's type
        * @param output
        * This is a enum that identify what type output file do you want to pass the requestData to the API.
        * * @param fileName
        * This is a string identify what name of output file do you want at time of download.
        * @param typeOfParameter
        * This is a enum that identify how do you want to pass the requestData to the API. Wether you want to pass it as Query String or as Body.
        * @param onResponseCallback
        * An event that will be called when the response is ready.
        */
        public sendFile(requestType: RequestType,
            route: string,
            requestData: any,
            output: ReportOutput,
            fileName: string,
            typeOfParameter: RequestParameterType,
            onResponseCallback: (response: Response) => any): void {
            this.send(requestType, route, requestData, typeOfParameter, (response: Response) => {
                //write the byteArray to PDF
                if (!!response) {
                    this.generateFile(response, fileName, output);
                    onResponseCallback(response);
                }
            });
        }

        //This function will generate the pdf files based on the data provided
        public generateFile(response: any, fileName: string, output: ReportOutput) {
            var fileOutputType = "application/pdf";//default value  
            if (ReportOutput.Pdf === output) {
                fileOutputType = "application/pdf";
                fileName = `${fileName}.pdf`;
            }

            var bytes =this.convertBase64ToArrayBuffer(response);
            var blob = new Blob([bytes], { type: fileOutputType });
            var link = document.createElement("a");
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            link.click();
        }

        //This function will convert the base64 type into byte array
        convertBase64ToArrayBuffer(base64) {
            var binaryString = window.atob(base64);
            var binaryLen = binaryString.length;
            var bytes = new Uint8Array(binaryLen);
            for (var i = 0; i < binaryLen; i++) {
                var ascii = binaryString.charCodeAt(i);
                bytes[i] = ascii;
            }
            return bytes;
        } 

        /**
         * Prepare a Kendo Data Source that encapsulates the JWT Authorization process.
         * This Kendo data source is for a GET Operation that sends the paramters on the query string.
         * You can use this for loading dropdowns, auto completes, grid and other kendo controls that can be bound to a server side data set.
         * @param route
         * The controller action to be called
         * @param onRequestFilterData
         * The event to get the filter parameter that will be pass when the request is made.
         */
        public getRequestAsKendoDataSource(route: string, onRequestFilterData: () => any): kendo.data.DataSource {
            let that = this;
            route = that.apiDomainName + route;

            if (that.shouldDoRequest) {

                let kendoDataSource = new kendo.data.DataSource({
                    serverFiltering: true,
                    serverPaging: false,
                    transport: {
                        read: (options) => {
                            $.ajax({
                                type: "GET",
                                url: route,
                                contentType: "application/json",
                                dataType: "json",
                                timeout: 20000,
                                global: false,
                                data: onRequestFilterData(),
                                beforeSend: xhr => {
                                    //Include the bearer token in header
                                    if (that.jsonWebToken !== null &&
                                        that.jsonWebToken !== "" &&
                                        that.jsonWebToken !== undefined) {
                                        xhr.setRequestHeader("Authorization", 'Bearer ' + that.jsonWebToken);
                                    }
                                },
                                success: (result) => {
                                    // notify the data source that the request succeeded
                                    options.success(result);
                                },
                                error: (result) => {
                                    // notify the data source that the request failed
                                    options.error(result);
                                    that.apiExceptionHandler(result);
                                }
                            });
                        }
                    },
                   // pageSize: 20

                });
                return kendoDataSource;
            } else {
                return new kendo.data.DataSource({
                    serverFiltering: false,
                    serverPaging: false,
                    type: "GET"
                });
            }
        }

        private getTypeOfRequest(requestType: RequestType): string {
            let typeOfRequest: string = "GET";
            if (requestType === RequestType.Get) {
                typeOfRequest = "GET";
            } else if (requestType === RequestType.Post) {
                typeOfRequest = "POST";
            } else if (requestType === RequestType.Put) {
                typeOfRequest = "PUT";
            } else if (requestType === RequestType.Delete) {
                typeOfRequest = "DELETE";
            }
            return typeOfRequest;
        }

        private apiExceptionHandler(error) {
            if (error !== undefined) {
                try {
                    if (error.status === 400 && (error.responseText !== "" || error.statusText !== "")) {
                        let errorJson = { message: "" };
                        if (error.responseText !== "") {
                            errorJson.message += error.responseText + "\n";
                        }
                        if (error.statusText !== "") {
                            errorJson.message += error.statusText;
                        }

                        MasterPage.SHOW_WARNING_WINDOW(errorJson.message);
                    } else {

                        MasterPage
                            .SHOW_ERROR_WINDOW(
                                "Unable to access the Advantage web api, this may be due to invalid configuration. Please try again later.");
                    }
                } catch (e) {
                    MasterPage.SHOW_ERROR_WINDOW("An error has ocurred during your request.");
                }
            } else {
                MasterPage.SHOW_ERROR_WINDOW("An error has ocurred during your request.");
            }
        } 

        private showIsPageLoading(show: boolean) {
            var pageLoading = $(document).find(".page-loading");
            if (pageLoading.length === 0)
                $(document).find("body").append('<div class="page-loading"></div>');

            kendo.ui.progress($(".page-loading"), show);
        }
    }
}