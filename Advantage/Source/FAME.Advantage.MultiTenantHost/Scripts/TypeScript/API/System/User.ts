﻿module Api {
    /**
     * The API user class is the encapsulation to the user Controller.    
    */

    export class User {
        public updatePassword(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Put, "v1/SystemCatalog/User/UpdatePasswordSet", data, RequestParameterType.Body, onResponseCallback);
        }

        public updateTermsOfUser(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post, "v1/SystemCatalog/User/UpdateTermsOfUse", data, RequestParameterType.Body, onResponseCallback);
        }
    }
}