﻿module Api {
    import IDataProvider = API.Models.IDataProvider;

    /**
   * The API user type class is the encapsulation to the user question Controller.
     * 
   */
    export class UserQuestion implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getUserQuestions(onRequestFilterData);
        }
        public getUserQuestions(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/UserQuestion/GetUserQuestions", onRequestFilterData);
        }
    }
}