﻿module Api.ViewModels.System {

    /*
     * The terms of use vm
     */
    export class TermsOfUseVm {

        iAgreeButton: JQuery;
        cancelButton :JQuery;
        userId: JQuery;
        version: JQuery;


        constructor() {

        }

        public initialize(advantageRedirectUrl: string, cancelRedirectUrl: string) {
            var that = this;
            that.iAgreeButton = $("#acceptTermsAndConditionsBtn");
            that.cancelButton = $("#denyTermsAndConditionsBtn");
            that.userId = $("#content_userId");
            that.version = $("#content_advantageVersion");

            let user = new User();

            $("#acceptTermsAndConditionsBtn").on("click",
                () => {
                    let model = {
                        userId: this.userId.val(),
                        version: this.version.val()
                    };
                    //console.log(model)
                    user.updateTermsOfUser(model, (response: Object) => {
                        //console.log(response)
                        if (response["hasAcceptedLatestTermsOfUse"]) {
                            MasterPage.SHOW_INFO_WINDOW_PROMISE(
                                    "Thank you for using Advantage. You will be redirected to dashboard shortly...")
                                .done(() => {
                                    //var advantageUrl = sessionStorage.getItem("AdvantageUrl");
                                    //debugger;
                                    //advantageUrl = advantageUrl.replace("/current", "");
                                    //window.location.href = advantageUrl;
                                    $("#" + advantageRedirectUrl).click();
                                });
                        } else {
                            MasterPage.SHOW_WARNING_WINDOW(response["resultStatus"]);
                        }
                    });
                });

            $("#denyTermsAndConditionsBtn").on("click",
                () => {
                    $("#" + cancelRedirectUrl).click();
                });
        }
    }

    
}