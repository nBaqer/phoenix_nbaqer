﻿module Api.ViewModels.System {

    export class TermsOfUse {

        termsOfUseVm: TermsOfUseVm;
        constructor() {
            this.termsOfUseVm = new TermsOfUseVm();
        }

        public initialize(advantageRedirectUrl: string, cancelRedirectUrl: string) {
            this.termsOfUseVm.initialize(advantageRedirectUrl, cancelRedirectUrl);
        }
    }
}