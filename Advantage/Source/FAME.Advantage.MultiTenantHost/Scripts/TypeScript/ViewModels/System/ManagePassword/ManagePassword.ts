﻿module Api.ViewModels.System {

    export class ManagePassword {

        managePasswordVm: ManagePasswordVm;
        constructor() {
            this.managePasswordVm = new ManagePasswordVm();
        }

        public initialize() {
            this.managePasswordVm.initialize();
        }
    }
}