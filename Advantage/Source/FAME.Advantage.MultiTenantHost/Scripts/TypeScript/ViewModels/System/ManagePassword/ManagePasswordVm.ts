﻿/// <reference path="../../../../../Kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../../Components/Common/DropDownList.ts"/>
/// <reference path="../../../Common/Constants.ts"/>

module Api.ViewModels.System {
    import DropDownList = Api.Components.DropDownList;
    import ENTER_REQUIRED_FIELDS = API.Common.X_PLEASE_FILL_REQUIRED_FIELDS;
    import PASSWORD_UPDATE_SUCCESSFUL = API.Common.PASSWORD_UPDATE_SUCCESSFUL;
    import UPDATE_FAILED = API.Common.UPDATE_FAILED;
    declare var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL: string;
    declare var hasLatestVersion: boolean;
    export class ManagePasswordVm {
        userQuestionsDropDownList: DropDownList;
        setPasswordValidator: kendo.ui.Validator;
        emailInput: JQuery;
        newPasswordInput: JQuery;
        confirmNewPasswordInput: JQuery;
        securityAnswerInput: JQuery;

        constructor() {

        }

        public initialize() {
            var that = this;
            that.emailInput = $("#content_userEmail");
            that.newPasswordInput = $("#newPassword");
            that.confirmNewPasswordInput = $("#confirmNewPassword");
            that.securityAnswerInput = $("#securityAnswer");
            let userQuestion = new UserQuestion();
            let user = new User();
            that.userQuestionsDropDownList = new DropDownList("securityQuestions", userQuestion, undefined, undefined, () => { });
            that.setPasswordValidator = MasterPage.ADVANTAGE_VALIDATOR("setPasswordForm");

            that.newPasswordInput.focus(() => {
                that.setPasswordValidator.validateInput(that.newPasswordInput);
            });

            $("#setPasswordBtn").on("click",
                () => {

                    if (that.setPasswordValidator.validate()) {
                        //update password here
                        let model = {
                            email: this.emailInput.val(),
                            password: this.newPasswordInput.val(),
                            confirmNewPassword: this.confirmNewPasswordInput.val(),
                            securityQuestion: this.userQuestionsDropDownList.getKendoDropDown().text(),
                            securityAnswer: this.securityAnswerInput.val(),
                            tenantName: $("#content_userTenantName").val(),
                            hasLatestVersion: $("#content_advVersion").val()
                        };
                        user.updatePassword(model, (response: Object) => {
                            if (response["hasPassed"]) {
                                MasterPage.SHOW_INFO_WINDOW_PROMISE(
                                    "The user's password was successfully updated.")
                                    .done(() => {
                                        window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +"Default.aspx");
                                    });
                            } else {
                                MasterPage.SHOW_WARNING_WINDOW(response["resultStatus"]);
                            }

                        });

                    } else {
                        if (!hasLatestVersion) {
                            MasterPage.SHOW_WARNING_WINDOW(
                                "Please enter all required fields and accept the terms and conditions to continue.");

                        } else {
                            MasterPage.SHOW_WARNING_WINDOW("Please enter all required fields to continue.");

                        }
                    }

                });
        }
    }
}