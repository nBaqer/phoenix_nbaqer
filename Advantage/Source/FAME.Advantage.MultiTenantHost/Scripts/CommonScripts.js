/// <reference path="../../../Kendo/typescript/jquery.d.ts" />
/// <reference path="../../../Kendo/typescript/kendo.all.d.ts" />
var MasterPage;
(function (MasterPage) {
    /* Show error*/
    function SHOW_DATA_SOURCE_ERROR(e) {
        try {
            if (e.xhr != undefined) {
                if (showSessionFinished(e.xhr.statusText)) {
                    return "";
                }
                var display = "";
                // Status 406 Continue (Informative Error Not Acceptable)
                if (e.xhr.statusText !== "OK" && e.xhr.status !== 406) {
                    display = "Error: " + e.xhr.statusText + ", ";
                }
                display += (e.xhr.responseText == undefined) ? e.xhr.responseXML : e.xhr.responseText;
                if (e.xhr.status === 406) {
                    SHOW_WARNING_WINDOW(display);
                }
                else {
                    SHOW_ERROR_WINDOW(display);
                }
            }
            else {
                var display = "";
                // Status 406 Continue (Informative Error Not Acceptable)
                if (e.statusText !== "OK" && e.status !== 406) {
                    display = "Error: " + e.statusText + ", ";
                }
                display += (e.responseText == undefined) ? e.responseXML : e.responseText;
                if (e.status === 406) {
                    SHOW_WARNING_WINDOW(display);
                }
                else {
                    if ((e.readyState !== 0 && e.responseText !== "" && e.status !== 0 && e.statusText !== "error")) {
                        SHOW_ERROR_WINDOW(display);
                    }
                }
            }
        }
        catch (ex) {
            SHOW_ERROR_WINDOW("Server returned an undefined error");
            //alert("Server returned an undefined error");
        }
        return "";
    }
    MasterPage.SHOW_DATA_SOURCE_ERROR = SHOW_DATA_SOURCE_ERROR;
    ;
    /**
     * Create a Confirmation Dialog Message that way for user selection
     * You are able to put your code in the yes or not case.
     * Use The Promise syntax to use it.
     * @param message
     */
    function SHOW_CONFIRMATION_WINDOW_PROMISE(message) {
        return showWindow('#confirmationTemplate', message);
    }
    MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE = SHOW_CONFIRMATION_WINDOW_PROMISE;
    ;
    /**
     * Create a Warning Windows that way for the confirmation
     * Use the promise syntax to use it
     * @param message
     */
    function SHOW_WARNING_WINDOW_PROMISE(message) {
        return showErrorWarningInfoWindow('#warningDialogTemplate', message, 400);
    }
    MasterPage.SHOW_WARNING_WINDOW_PROMISE = SHOW_WARNING_WINDOW_PROMISE;
    ;
    /**
     * Create a Info Windows that way for the confirmation
     * Use the promise syntax to use it
     * You are able to put your code in the return of the function.
     * @param message
     */
    function SHOW_INFO_WINDOW_PROMISE(message) {
        return showErrorWarningInfoWindow('#infoDialogTemplate', message, 400);
    }
    MasterPage.SHOW_INFO_WINDOW_PROMISE = SHOW_INFO_WINDOW_PROMISE;
    ;
    /**
    * Create a Info Windows that way for the confirmation
    * Use the promise syntax to use it
    * You are able to put your code in the return of the function.
    * @param message
    */
    function SHOW_ERROR_WINDOW_PROMISE(message) {
        return showErrorWarningInfoWindow('#errorDialogTemplate', message, 400);
    }
    MasterPage.SHOW_ERROR_WINDOW_PROMISE = SHOW_ERROR_WINDOW_PROMISE;
    ;
    /**
     * Create a WARNING Windows that way for the confirmation
     * Use this if you do not need to put code after the dialog.
     * @param message
     */
    function SHOW_WARNING_WINDOW(message) {
        $.when(SHOW_WARNING_WINDOW_PROMISE(message))
            .then(function (confirmed) {
            return true;
        });
    }
    MasterPage.SHOW_WARNING_WINDOW = SHOW_WARNING_WINDOW;
    /**
      * Create a Error Windows that way for the confirmation
      * Use this if you do not need to put code after the dialog.
      * @param message
      */
    function SHOW_ERROR_WINDOW(message) {
        $.when(SHOW_ERROR_WINDOW_PROMISE(message))
            .then(function (confirmed) {
            return true;
        });
    }
    MasterPage.SHOW_ERROR_WINDOW = SHOW_ERROR_WINDOW;
    /**
     * Create a Info Windows that way for the confirmation
     * Use this if you do not need to put code after the dialog.
     * @param message
     */
    function SHOW_INFO_WINDOW(message) {
        $.when(SHOW_INFO_WINDOW_PROMISE(message))
            .then(function (confirmed) {
            return true;
        });
    }
    MasterPage.SHOW_INFO_WINDOW = SHOW_INFO_WINDOW;
    function showWindow(template, message) {
        var dfd = $.Deferred();
        var result = false;
        var win = $("<div id='popupWindow'></div>")
            .appendTo("body")
            .kendoWindow({
            width: 400,
            resizable: false,
            title: false,
            modal: true,
            visible: false,
            scrollable: false,
            close: function (e) {
                this.destroy();
                dfd.resolve(result);
            }
        })
            .data("kendoWindow");
        win.content($(template).html()).center().open();
        $('.popupMessage').html(message);
        $("#popupWindow .confirm_yes").val('Yes');
        $("#popupWindow .confirm_no").val('No');
        $("#popupWindow .confirm_no").click(function () {
            win.close();
        });
        $("#popupWindow .confirm_yes").click(function () {
            result = true;
            win.close();
        });
        return dfd.promise();
    }
    ;
    function showErrorWarningInfoWindow(template, message, wWidth) {
        var dfd = $.Deferred();
        var result = false;
        var win = $("<div id='popupWindow'></div>")
            .appendTo("body")
            .kendoWindow({
            width: wWidth,
            resizable: false,
            title: false,
            modal: true,
            visible: false,
            scrollable: false,
            close: function (e) {
                this.destroy();
                dfd.resolve(result);
            }
        })
            .data("kendoWindow");
        win.content($(template).html()).center().open();
        if (message.length > 200) {
            $("#wrapperMessage").css("overflow-y", "scroll");
        }
        $('.popupMessage').html(message);
        $("#popupWindow .confirm_yes").val('OK');
        $("#popupWindow .confirm_yes").click(function () {
            result = true;
            win.close();
        });
        return dfd.promise();
    }
    ;
    function showSessionFinished(statusText) {
        if (statusText === "OK") {
            alert("Session expired");
            return true;
        }
        return false;
    }
    function showPageLoading(show) {
        var pageLoading = $(document).find(".page-loading");
        if (pageLoading.length === 0)
            $(document).find("body").append('<div class="page-loading"></div>');
        kendo.ui.progress($(".page-loading"), show);
    }
    MasterPage.showPageLoading = showPageLoading;
})(MasterPage || (MasterPage = {}));
var MasterPage;
(function (MasterPage) {
    /*
     * To use this function you need to introduce in the master page the CSS
     * used by the function!!!!
     */
    // Messages on rules fail............................................
    var xValidatorMail = "Enter valid email";
    var xValidatorTel = "Enter valid phone number";
    var xValidatorInternaionalPhone = "Enter valid international phone number (min. 7 digit phone number)";
    MasterPage.xValidatorDate = "Date format is not valid";
    MasterPage.xValidatorDateWithFormat = "Date is invalid";
    var xValidatorDateTime = "Data time is not valid";
    var xValidatorTime = "Time entered is not valid";
    var xValidatorZip = "Enter Zip Code";
    var xValidatorSsn = "Enter a valid SSN";
    var xValidatorLetters = "Enter only letters";
    var xValidatorLettersNumbers = "Special symbol not allowed";
    var xValidatorNumbers = "Enter only numbers";
    var xValidatorRange = "must be between";
    var xValidatorDecimal = "Enter numbers or decimal";
    var xValidatorCheckbox = "Please check this box to proceed";
    var xValidatorPasswordsDoNotMatch = "The passwords do not match";
    var xValidatorWeakPassword = "The chosen password is not strong enough";
    var xValidatorPasswordMinSpecialChars = "The password should have at least {0} special characters";
    var xValidatorPasswordMinLength = "The password should have a minimum length of {0}";
    function ADVANTAGE_VALIDATOR(wrapperName, validateRequireOnBlur) {
        var validator = $("#" + wrapperName)
            .kendoValidator({
            validateOnBlur: true,
            errorTemplate: "",
            rules: {
                password: function (input) {
                    var passwordstrength = input.data('password');
                    var minlength = input.data('minlength');
                    var minspecialchar = input.data('minspecialchar');
                    if (passwordstrength && minlength && minspecialchar) {
                        var pass = $.trim(input.val());
                        // check if pass is strong enough
                        if (new RegExp(passwordstrength).test(pass)) {
                            destroyTooltip(input);
                            // pass is strong
                            return true;
                        }
                        else {
                            destroyTooltip(input);
                            var tooltipMsg = "";
                            if (!isPassMinLength(pass, minlength)) {
                                tooltipMsg += xValidatorPasswordMinLength.replace("{0}", minlength) + "\n";
                            }
                            if (!isPassMinSpecialChar(pass, minspecialchar)) {
                                tooltipMsg += xValidatorPasswordMinSpecialChars.replace("{0}", minspecialchar);
                            }
                            if (tooltipMsg == "") {
                                tooltipMsg =
                                    "The password that was provided is too simple.\nPlease enter a more complex password.";
                            }
                            createTooltip(tooltipMsg, input);
                            //validation fails
                            return false;
                        }
                    }
                    // don't perform password validation
                    return true;
                },
                required_checkbox: function (input) {
                    if (!input.is("[type=checkbox]") && !input.data('checkbox')) {
                        return true;
                    }
                    var checked = $(input).prop("checked");
                    if (checked) {
                        destroyTooltip(input);
                    }
                    else {
                        createTooltip(xValidatorCheckbox, input);
                    }
                    return checked;
                },
                required: function (input) {
                    var isRequired = validateRequireOnBlur !== undefined ? validateRequireOnBlur : true;
                    if (!input.is("[required]") || input.is("[type=checkbox]")) {
                        return true;
                    }
                    var html = input;
                    if (input.is('[data-role="numerictextbox"]')) {
                        if (isRequired) {
                            html = input.siblings("input").addClass("valueRequired");
                        }
                    }
                    if (input.is('[data-role="dropdownlist"]')) {
                        if (isRequired) {
                            var parentItem = input.siblings(".k-dropdown-wrap");
                            html = parentItem.children(".k-input");
                        }
                    }
                    if (input.is('[data-role="combobox"]')) {
                        if (isRequired) {
                            var parentItem = input.siblings(".k-dropdown-wrap");
                            html = parentItem.children("input");
                        }
                    }
                    var value = input.val();
                    if (value === undefined || value === null || value === "") {
                        if (isRequired) {
                            html.addClass("valueRequired");
                        }
                        return false;
                    }
                    else {
                        html.removeClass("valueRequired");
                        return true;
                    }
                },
                email: function (input) {
                    if (!input.is("[type=email]")) {
                        return true;
                    }
                    var email = isEmail($(input).val());
                    if (email) {
                        destroyTooltip(input);
                    }
                    else {
                        createTooltip(xValidatorMail, input);
                    }
                    return email;
                },
                matches: function (input) {
                    var matches = input.data('matches');
                    // if the `data-matches attribute was found`
                    if (matches) {
                        // get the input to match
                        var match = $(matches);
                        // trim the values and check them
                        if ($.trim(input.val()) === $.trim(match.val())) {
                            destroyTooltip(input);
                            // the fields match
                            return true;
                        }
                        else {
                            createTooltip(xValidatorPasswordsDoNotMatch, input);
                            // the fields don't match - validation fails
                            return false;
                        }
                    }
                    // don't perform any match validation on the input
                    return true;
                },
                minlength: function (input) {
                    var minlength = input.data('minlength');
                    var hasPasswordAttr = input.data('password');
                    if (minlength && !hasPasswordAttr) {
                        var pass = $.trim(input.val());
                        var passed = isPassMinLength(pass.length, minlength);
                        if (passed) {
                            destroyTooltip(input);
                            // the minimum number of characters needed is satisfied
                            return true;
                        }
                        else {
                            createTooltip(xValidatorPasswordMinLength.replace("{0}", minlength), input);
                            // validation fails
                            return false;
                        }
                    }
                    // don't perform any min length validation on the input
                    return true;
                },
                minspecialchar: function (input) {
                    var minspecialchar = input.data('minspecialchar');
                    var hasPasswordAttr = input.data('password');
                    if (minspecialchar && !hasPasswordAttr) {
                        var pass = $.trim(input.val());
                        var passed = isPassMinSpecialChar(pass, minspecialchar);
                        if (passed) {
                            destroyTooltip(input);
                            // the minimum number of special characters satisfied
                            return true;
                        }
                        else {
                            createTooltip(xValidatorPasswordMinSpecialChars.replace("{0}", minspecialchar), input);
                            //validation fails
                            return false;
                        }
                    }
                    // don't perform any non alpha numeric required on the input
                    return true;
                },
                tel: function (input) {
                    if (!input.is("[type=tel]")) {
                        return true;
                    }
                    if (input.is("[data-role=maskedtextbox]")) {
                        //  Analysis of masked
                        var maskedtextbox = input.data("kendoMaskedTextBox");
                        // Mask can be American or international. International no has dashed
                        if (maskedtextbox.value().indexOf("-") === -1) {
                            destroyTooltip(input);
                            return true; // no validated if other than American
                        }
                        var masked = maskedtextbox.value().indexOf(maskedtextbox.options.promptChar) === -1;
                        if (masked) {
                            destroyTooltip(input);
                        }
                        else {
                            createTooltip(xValidatorTel, input);
                        }
                        return masked;
                    }
                    if (input.is("[data-role=international]")) {
                        var isValidInternational = isInternationalPhone($(input).val().replace(/[\s()+\_\-\.]|ext/gi, ""));
                        if (isValidInternational) {
                            destroyTooltip(input);
                        }
                        else {
                            createTooltip(xValidatorInternaionalPhone, input);
                        }
                        return isValidInternational;
                    }
                    else {
                        // Fixed analysis to American format.
                        var tel = isTel($(input).val());
                        if (tel) {
                            destroyTooltip(input);
                        }
                        else {
                            createTooltip(xValidatorTel, input);
                        }
                        return tel;
                    }
                },
                date: function (input) {
                    if (input.is("[type=date]") || input.is("[data-validation=date]")) {
                        var date = isdate1($(input).val());
                        if (date) {
                            destroyTooltip(input);
                        }
                        else {
                            createTooltip(MasterPage.xValidatorDate, input);
                        }
                        return date;
                    }
                    return true;
                },
                dateWithFormat: function (input) {
                    if (input.is("[type=date]") || input.is("[data-validation=dateWithFormat]")) {
                        return cutomDateFormatValidation(input);
                    }
                    return true;
                },
                datetime: function (input) {
                    if (input.is("[type=datetime]") || input.is("[data-validation=datetime]")) {
                        var isTime = isDateTime($(input).val());
                        if (isTime) {
                            destroyTooltip(input);
                        }
                        else {
                            createTooltip(xValidatorDateTime, input);
                        }
                        return isTime;
                    }
                    return true;
                },
                time: function (input) {
                    if (input.is("[type=time]") || input.is("[data-validation=time]")) {
                        var isTime = isTime12Am($(input).val());
                        if (isTime) {
                            destroyTooltip(input);
                        }
                        else {
                            createTooltip(xValidatorTime, input);
                        }
                        return isTime;
                    }
                    return true;
                },
                // Custom validation for Advantage
                zip: function (input) {
                    if (!input.is("[data-validation=zip]")) {
                        return true;
                    }
                    if (input.is("[data-role=maskedtextbox]")) {
                        //  Analysis of masked
                        var maskedtextbox = input.data("kendoMaskedTextBox");
                        if (maskedtextbox.value().length > 5) {
                            destroyTooltip(input);
                            return true; // no validated if the ZIP is other than American
                        }
                        var masked = maskedtextbox.value().indexOf(maskedtextbox.options.promptChar) === -1;
                        if (masked) {
                            destroyTooltip(input);
                        }
                        else {
                            createTooltip(xValidatorZip, input);
                        }
                        return masked;
                    }
                    else {
                        // Fixed analysis to American format.
                        var zip = isZip($(input).val());
                        if (zip) {
                            destroyTooltip(input);
                        }
                        else {
                            createTooltip(xValidatorZip, input);
                        }
                        return zip;
                    }
                },
                ssn: function (input) {
                    if (!input.is("[data-validation=ssn]")) {
                        return true;
                    }
                    if (input.is("[data-role=maskedtextbox]")) {
                        //  Analysis of masked
                        var maskedtextbox = input.data("kendoMaskedTextBox");
                        var masked = maskedtextbox.value().indexOf(maskedtextbox.options.promptChar) === -1;
                        if (masked) {
                            destroyTooltip(input);
                        }
                        else {
                            createTooltip(xValidatorSsn, input);
                        }
                        return masked;
                    }
                    else {
                        // Fixed analysis to American format.
                        var ssn = isSsn($(input).val());
                        if (ssn) {
                            destroyTooltip(input);
                        }
                        else {
                            createTooltip(xValidatorSsn, input);
                        }
                        return ssn;
                    }
                },
                letters: function (input) {
                    if (!input.is("[data-validation=letters]")) {
                        return true;
                    }
                    // Fixed analysis to American format.
                    var letters = isLetters($(input).val());
                    if (letters) {
                        destroyTooltip(input);
                    }
                    else {
                        createTooltip(xValidatorLetters, input);
                    }
                    return letters;
                },
                lettersnumbers: function (input) {
                    if (!input.is("[data-validation=lettersnumbers]")) {
                        return true;
                    }
                    // Fixed analysis to American format.
                    var letters = isLettersNumber($(input).val());
                    if (letters) {
                        destroyTooltip(input);
                    }
                    else {
                        createTooltip(xValidatorLettersNumbers, input);
                    }
                    return letters;
                },
                dependRequired: function (input) {
                    var depcontrol;
                    if (!input.is("[data-validation=dependRequired]")) {
                        return true;
                    } // Check if the second attribute exists
                    if (input.is("[data-dependable]") === undefined) {
                        return true;
                    }
                    // Check if the dependable exists
                    depcontrol = $("#" + input.attr("data-dependable"));
                    if (depcontrol.length === 0) {
                        return true;
                    }
                    // Begin validation of the rule
                    var html = input;
                    if (input.is('[data-role="numerictextbox"]')) {
                        html = input.siblings("input").addClass("valueRequired");
                    }
                    if (input.is('[data-role="dropdownlist"]')) {
                        var parentItem = input.siblings(".k-dropdown-wrap");
                        html = parentItem.children(".k-input");
                    }
                    if (input.is('[data-role="combobox"]')) {
                        var parentItem = input.siblings(".k-dropdown-wrap");
                        html = parentItem.children("input");
                    }
                    var value = depcontrol.val();
                    if (value === undefined || value === null || value === "") {
                        // if the value of dependable is empty or null rule is not apply
                        html.removeClass("valueRequired");
                        return true;
                    }
                    // Exists a value in the dependable control. check if the control is empty
                    var emptyVal = input.attr("data-empty");
                    if (emptyVal === html.text()) {
                        // MArk the control that require a value
                        html.addClass("valueRequired");
                        return false;
                    }
                    else {
                        html.removeClass("valueRequired");
                        return true;
                    }
                },
                oneChecked: function (input) {
                    var cb = $("#" + input.prop("id"));
                    var cb0 = cb[0]; // The checkbox element...
                    if (cb0 === undefined || cb0 === null) {
                        return true;
                    }
                    if (cb0.type !== "checkbox") {
                        return true;
                    }
                    //// Get all values....
                    var wrapper = $("#" + cb0.id).closest("div[data-validation=oneChecked]");
                    if (wrapper[0] === undefined || wrapper[0] === null) {
                        return true;
                    }
                    var listCheckbox = $("#" + wrapper[0].id).find("input");
                    if (listCheckbox === undefined || listCheckbox === null) {
                        return true;
                    }
                    for (var i = 0; i < listCheckbox.length; i++) {
                        if (listCheckbox[i].checked) {
                            wrapper.removeClass("valueRequired");
                            return true;
                        }
                    }
                    wrapper.addClass("valueRequired");
                    return false;
                },
                number: function (input) {
                    if (input.is("[data-validation=number]")) {
                        var num = isNumber($(input).val());
                        if (num) {
                            destroyTooltip(input);
                        }
                        else {
                            createTooltip(xValidatorNumbers, input);
                        }
                        return num;
                    }
                    return true;
                },
                rangeNumber: function (input) {
                    if (input.is("[data-validation=rangeNumber]")) {
                        destroyTooltip(input);
                        var isRange = isRangeNum($(input).val(), ($(input).attr("data-min")), ($(input).attr("data-max")));
                        if (isRange) {
                            destroyTooltip(input);
                        }
                        else {
                            createTooltip($(input).attr("data-name") +
                                " " +
                                xValidatorRange +
                                " " +
                                $(input).attr("data-min") +
                                " and " +
                                $(input).attr("data-max"), input);
                        }
                        return isRange;
                    }
                    return true;
                },
                rangeDate: function (input) {
                    if (input.is("[data-validation=rangeDate]")) {
                        var num = isdate1($(input).val());
                        if (!num) {
                            createTooltip(MasterPage.xValidatorDate, input);
                        }
                        else {
                            destroyTooltip(input);
                            var isRange = isRangeDate($(input).val(), ($(input).attr("data-min")), ($(input).attr("data-max")));
                            if (isRange) {
                                destroyTooltip(input);
                            }
                            else {
                                createTooltip($(input).attr("data-name") +
                                    " " +
                                    xValidatorRange +
                                    " " +
                                    $(input).attr("data-min") +
                                    " and " +
                                    $(input).attr("data-max"), input);
                            }
                            return isRange;
                        }
                    }
                    return true;
                },
                decimal: function (input) {
                    if (input.is("[data-validation=decimal]")) {
                        var num = isAllowDecimal($(input).val());
                        if (num) {
                            destroyTooltip(input);
                        }
                        else {
                            createTooltip(xValidatorDecimal, input);
                        }
                        return num;
                    }
                    return true;
                }
            },
            messages: {
                date: "",
                mail: "",
                required: ""
            }
        })
            .data("kendoValidator");
        return validator;
    }
    MasterPage.ADVANTAGE_VALIDATOR = ADVANTAGE_VALIDATOR;
    function ADVANTAGE_VALIDATOR_HIDDEN_VALIDATION(wrapperComponent) {
        var childs = $("#" + wrapperComponent + " .advantageTooltipClass");
        childs.remove();
    }
    MasterPage.ADVANTAGE_VALIDATOR_HIDDEN_VALIDATION = ADVANTAGE_VALIDATOR_HIDDEN_VALIDATION;
    /**
     * Reset the form input fields to it's default state. Removing any tooltip and the required class
     * @param formName
     */
    function advantageValitorReset(formName) {
        $("#" + formName + " .valueRequired").removeClass('valueRequired');
        ADVANTAGE_VALIDATOR_HIDDEN_VALIDATION(formName);
    }
    MasterPage.advantageValitorReset = advantageValitorReset;
    function createTooltip(message, input) {
        var nam = input.prop("name");
        var position = input.position();
        var topO = position.top;
        var topN = topO + input.outerHeight() + 1;
        var ident = nam + "tt";
        var test = $("#" + ident).length;
        if (test === 0) {
            var element = document.createElement("div");
            var html = $(element);
            html.prop("id", ident);
            if ($(input).attr("valitor-posistion") !== "disabled") {
                html.css({ top: topN });
                html.css({ left: position.left });
            }
            //html.width(input.width());
            html.addClass("advantageTooltipClass");
            //replace new lines with breaks
            message = message.replace(/(?:\r\n|\r|\n)/g, '<br />');
            html.append(message);
            var lastElem = input.last();
            lastElem.after(html);
        }
    }
    MasterPage.createTooltip = createTooltip;
    function destroyTooltip(input) {
        var tt = input.prop("name") + "tt";
        var tool = $("#" + tt);
        if (tool.length > 0) {
            tool.remove();
        }
    }
    MasterPage.destroyTooltip = destroyTooltip;
    function cutomDateFormatValidation(input) {
        var message = "";
        var isValid = false;
        if (isdate1($(input).val()) === false) {
            message = MasterPage.xValidatorDate;
        }
        else if (message.length === 0) {
            if (isValidDate($(input).val()) === false) {
                message = MasterPage.xValidatorDateWithFormat;
                isValid = false;
            }
            else {
                isValid = true;
                $(input).val(formatDate($(input).val()));
            }
        }
        if (message.length === 0) {
            destroyTooltip(input);
            isValid = true;
        }
        else {
            createTooltip(message, input);
            isValid = false;
        }
        return isValid;
    }
    MasterPage.cutomDateFormatValidation = cutomDateFormatValidation;
    function formatDate(dateString) {
        if (dateString !== "") {
            var currentDate = dateString.split("/");
            if (currentDate.length === 3) {
                var mm = currentDate[0].length === 1 ? "0" + currentDate[0] : currentDate[0];
                var dd = currentDate[1].length === 1 ? "0" + currentDate[1] : currentDate[1];
                var yyyy = currentDate[2];
                return mm + "/" + dd + "/" + yyyy;
            }
        }
        return dateString;
    }
    MasterPage.formatDate = formatDate;
    function isValidDate(dateText) {
        // Match the date format through regular expression  
        if (dateText !== "" || dateText !== undefined || dateText !== null) {
            //Test which seperator is used '/' or '-'  
            var opera1 = dateText.split("/");
            var pdate = [];
            // Extract the string into month, date and year  
            if (opera1.length > 1) {
                pdate = dateText.split("/");
            }
            if (pdate.length >= 3 && pdate[2] !== undefined && pdate[2] !== null && pdate[2].length !== 4) {
                return false;
            }
            var mm = parseInt(pdate[0]);
            var dd = parseInt(pdate[1]);
            var yy = parseInt(pdate[2]);
            if (mm === 0 || dd === 0 || yy === 0) {
                return false;
            }
            // Create list of days of a month [assume there is no leap year by default]  
            var listofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            if (mm > 12)
                return false;
            if (mm === 1 || mm > 2) {
                if (dd > listofDays[mm - 1]) {
                    return false;
                }
            }
            if (mm === 2) {
                var lyear = false;
                if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
                    lyear = true;
                }
                if ((lyear === false) && (dd >= 29)) {
                    return false;
                }
                if ((lyear) && (dd > 29)) {
                    return false;
                }
            }
        }
        return true;
    }
    MasterPage.isValidDate = isValidDate;
    function isdate1(dateString) {
        if (dateString === "") {
            return true;
        }
        var reg = new RegExp("\\d{1,2}/\\d{1,2}/\\d{4,4}");
        return reg.test(dateString);
    }
    MasterPage.isdate1 = isdate1;
    function isNumber(numberString) {
        if (numberString === "") {
            return true;
        }
        var reg = new RegExp("^[0-9]*$");
        return reg.test(numberString);
    }
    function isAllowDecimal(numberString) {
        if (numberString === "") {
            return true;
        }
        var reg = new RegExp("\\d+\\.?\\d*$");
        return reg.test(numberString);
    }
    function isPassMinLength(pass, minlength) {
        if (pass.length >= minlength) {
            // the minimum number of characters needed is satisfied
            return true;
        }
        else {
            // validation fails
            return false;
        }
    }
    function isPassMinSpecialChar(pass, minspecialchar) {
        //replace all alphanumeric with blank and take the length
        var pattern = "[a-zA-Z0-9]*";
        if (pass.replace(new RegExp(pattern, "g"), "").length >= minspecialchar) {
            return true;
        }
        else {
            return false;
        }
    }
    function isTime12Am(val) {
        if (val === "") {
            return true;
        }
        var reg = new RegExp("(1[012]|[1-9]):[0-5][0-9](\\s)?(AM|PM|am|pm)");
        return reg.test(val);
    }
    function isDateTime(val) {
        if (val === "") {
            return true;
        }
        var reg = new RegExp("\\d{1,2}/\\d{1,2}/\\d{4,4}\\s(1[012]|[1-9]):[0-5][0-9](\\s)?(AM|PM|am|pm)");
        return reg.test(val);
    }
    function isEmail(val) {
        if (val === "") {
            return true;
        }
        var reg = new RegExp("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$");
        return reg.test(val);
    }
    function isTel(val) {
        if (val === "") {
            return true;
        }
        /* Admit the followings formats
         * 123-456-7890
         * (123) 456-7890
         * 123 456 7890
         * 123.456.7890
         * +91 (123) 456-7890
         */
        var reg = new RegExp("^(\\+\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}$");
        return reg.test(val);
    }
    function isInternationalPhone(value) {
        if (value === "") {
            return true;
        }
        return (/^\d{7,}$/).test(value) || isTel(value);
    }
    function isZip(val) {
        if (val === "") {
            return true;
        }
        var reg = new RegExp("^\\d{5}$");
        return reg.test(val);
    }
    function isSsn(val) {
        if (val === "") {
            return true;
        }
        var reg = new RegExp("^\\d{3}-?\\d{2}-?\\d{4}$");
        return reg.test(val);
    }
    function isLetters(val) {
        if (val === "") {
            return true;
        }
        var reg = new RegExp("^[a-zA-Z]+$");
        return reg.test(val);
    }
    function isLettersNumber(val) {
        if (val === "") {
            return true;
        }
        var reg = new RegExp("^[-_ a-zA-Z0-9]+$");
        return reg.test(val);
    }
    function isRangeNum(val, minVal, maxVal) {
        if (val === "") {
            return true;
        }
        var valN = Number(val);
        var minValN = Number(minVal);
        var maxValN = Number(maxVal);
        if (valN !== NaN && minValN !== NaN && maxValN !== NaN) {
            return valN >= minValN && valN <= maxValN;
        }
        else {
            return false;
        }
    }
    function isRangeDate(val, minVal, maxVal) {
        if (val === "") {
            return true;
        }
        var valN = new Date(val);
        var minValN = new Date(minVal);
        var maxValN = new Date(maxVal);
        var c = new Date(maxValN.getTime());
        return valN >= minValN && valN < c;
    }
})(MasterPage || (MasterPage = {}));
var API;
(function (API) {
    var Common;
    (function (Common) {
        Common.X_PLEASE_FILL_REQUIRED_FIELDS = "Please enter the required values.";
        Common.X_ACCEPT_TERMS_AND_CONDITIONS = "Accept the terms and conditions.";
        Common.PASSWORD_UPDATE_SUCCESSFUL = "Password set updated successfully.";
        Common.UPDATE_FAILED = "Failed updating record.";
    })(Common = API.Common || (API.Common = {}));
})(API || (API = {}));
var API;
(function (API) {
    var Common;
    (function (Common) {
        /**
            * Utility class will provide the common functionalities across the typescript project.
           */
        var Utility = (function () {
            function Utility() {
            }
            //this method convert given number into currency format and return a string.
            Utility.formatNumberAsCurrency = function (value) {
                return value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            };
            //this method convert given number into percentage format and return a string.
            Utility.formatNumberAsPercenatgeResults = function (value) {
                return value.toFixed(1).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            };
            //this method convert given number into currency format and return a string.
            Utility.formatNumberAsCurrencyPercenatgeTab = function (value) {
                return value.toFixed(3).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            };
            Utility.getUrlParameter = function (sParam) {
                var sPageUrl = decodeURIComponent(window.location.search.substring(1)), sUrlVariables = sPageUrl.split('&'), sParameterName, i;
                for (i = 0; i < sUrlVariables.length; i++) {
                    sParameterName = sUrlVariables[i].split('=');
                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };
            ;
            // Method to convert date into MM/dd/yyyy format
            Utility.formatDate = function (inputDate) {
                var date = new Date(inputDate);
                // Months use 0 index.
                return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
            };
            return Utility;
        }());
        Common.Utility = Utility;
    })(Common = API.Common || (API.Common = {}));
})(API || (API = {}));
/// <reference path="../../../../Kendo/typescript/kendo.all.d.ts" />
/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
var Api;
(function (Api) {
    var Components;
    (function (Components) {
        var DropDownList = (function () {
            function DropDownList(componentId, dataProvider, filterParams, settings, onchange) {
                /**
                 * Declaring the UserType API object. The UserType API encapsulate the User Type Controller.
                 * We will use the UserType API object to get the kendo data source for the UserType dropdown.
                 */
                var that = this;
                /**
                 * In this step we are concatenating the # with the component id so we can find the HTML control on the using jquery and them with the
                 * jquery reference to that DOM object the kendo UI control wil be created.
                 */
                componentId = "#" + componentId;
                /**
                 * storing the component id in the global variable scope so it can be referenced elsewhere
                 */
                that.componentId = componentId;
                that.dataProvider = dataProvider;
                that.filterParams = filterParams;
                var defaults = {
                    dataSource: dataProvider.getDataSource(function () { return that.onRequestFilterData(); }),
                    optionLabel: "Select",
                    dataTextField: "text",
                    dataValueField: "value"
                };
                if (settings) {
                    $.extend(defaults, settings.settings);
                }
                /**
                 * declaration of the kendo ui control.
                 */
                $(componentId).kendoDropDownList(defaults).change(function (value) {
                    if (value)
                        that.selectedId = $(that.componentId).data("kendoDropDownList").value();
                    onchange(value);
                });
                that.dropDownList = $(componentId).data("kendoDropDownList");
            }
            DropDownList.prototype.onRequestFilterData = function () {
                var filter = {};
                if (this.filterParams) {
                    filter = $.extend(filter, this.filterParams.params);
                }
                return filter;
            };
            DropDownList.prototype.setParameters = function (filterParams) {
                this.filterParams = filterParams;
            };
            DropDownList.prototype.getSelected = function () {
                return this.selectedId;
            };
            DropDownList.prototype.reset = function () {
                this.dropDownList.value("");
            };
            DropDownList.prototype.reload = function (filterParams) {
                var _this = this;
                if (filterParams) {
                    this.setParameters(filterParams);
                }
                this.dropDownList.setDataSource(this.dataProvider.getDataSource(function () { return _this.onRequestFilterData(); }));
            };
            DropDownList.prototype.hasValue = function () {
                return this.dropDownList.value() !== "";
            };
            DropDownList.prototype.getValue = function () {
                return this.dropDownList.value();
            };
            DropDownList.prototype.setValue = function (newValue) {
                this.dropDownList.value(newValue);
            };
            DropDownList.prototype.getKendoDropDown = function () {
                return this.dropDownList;
            };
            return DropDownList;
        }());
        Components.DropDownList = DropDownList;
    })(Components = Api.Components || (Api.Components = {}));
})(Api || (Api = {}));
//# sourceMappingURL=CommonScripts.js.map