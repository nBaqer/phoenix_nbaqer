﻿(function () {
    window.key = function (data) {
        var self = {};
        data = data || {};

        self.Id = data.Id;
        self.Key = data.Key;

        self.showLongKey = ko.observable(false);

        self.displayShortKey = function (key) {
            return key.substring(0, 10) + ' ... ';
        };
        self.displayKey = ko.computed(function () {
            return self.showLongKey() ? self.Key : self.displayShortKey(self.Key);
        });

        return self;
    };
    
    window.models = window.models || {};
    window.models.key = key;
})();