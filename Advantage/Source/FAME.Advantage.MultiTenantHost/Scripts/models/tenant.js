﻿(function () {
    var tenant = function (data) {
        var self = {};
        data = data || {};

        self.Id = data.Id;
        self.Name = data.Name;
        self.Keys = jQuery.map(data.Keys || [], function (key) { return window.models.key(key); });

        return self;
    };

    tenant.get = function (id) {
        return jQuery.get('api/Tenant/' + id)
            .fail(function () { alert('Tenant failed to load... Try again'); });
    };

    tenant.getAll = function () {
        return jQuery.get('api/Tenant/')
            .fail(function () { alert('Tenants failed to load... Try again'); });
    };

    window.models = window.models || {};
    window.models.tenant = tenant;
})();