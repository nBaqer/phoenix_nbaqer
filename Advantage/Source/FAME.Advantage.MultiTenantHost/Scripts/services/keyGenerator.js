﻿(function() {
    window.keyGenerator = function() {
        var self = {};

        self.GenerateKey = function() {
            var keyLength = getKeyLength();
            var key = [];
            
            for (var i = 0; i < keyLength; i++) {
                var randomKeyValue = Math.floor((Math.random() * 62) + 1);;
                var keyAsciiValue = self.mapToAscii(randomKeyValue);
                var keyChar = String.fromCharCode(keyAsciiValue);
                key.push(keyChar);
            }

            return key.join("");
        };

        self.mapToAscii = function (input) {
            var firstRangeUpper = 10;
            var secondRangeUpper = 36;
            var thirdRangeUpper = 62;
            
            if (input <= 0 || input > thirdRangeUpper) throw new Error('Input must be integer of the set of integers {1...62}');
            
            var firstRangeOffset = 47;
            var secondRangeOffset = 54;
            var thirdRangeOffset = 60;

            if (input <= firstRangeUpper) return input + firstRangeOffset;
            if (input > firstRangeUpper && input <= secondRangeUpper) return input + secondRangeOffset;
            if (input > secondRangeUpper && input <= thirdRangeUpper) return input + thirdRangeOffset;

            throw 'Input was out of bounds';
        };

        var getKeyLength = function () {
            var minKeyLength = 80;

            return Math.floor((Math.random() * 20) + 1) + minKeyLength;
        };

        return self;
    };
})();