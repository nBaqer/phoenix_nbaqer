﻿
Imports FAME.Advantage.MultiTenantHost.Lib

Namespace FAME.Advantage.MultiTenantHost

    Partial Class NavigationPane
        Inherits UserControl
        public IsSupportUser As Boolean
        Protected Sub lnkManageHosting_Click(sender As Object, e As EventArgs) Handles lnkManageHosting.Click
            Response.Redirect("ManageHosting.aspx")
        End Sub

        Protected Sub lnkLogOut_Click(sender As Object, e As EventArgs) Handles lnkLogOut.Click
            SecurityHelper.LogOut()
        End Sub

        Protected Sub lnkTenantPicker_Click(sender As Object, e As EventArgs) Handles lnkTenantPicker.Click
            Response.Redirect("TenantPicker.aspx")
        End Sub

        Protected Sub lnkManageApiAuthenticationKeys_Click(sender As Object, e As EventArgs) Handles lnkManageApiAuthenticationKeys.Click
            Response.Redirect("ServiceAuthenticationKeys.aspx")
        End Sub

        Protected Sub lnkWapiProxyLog_Click(sender As Object, e As EventArgs) Handles lnkWapiProxyLog.Click
            Response.Redirect("WapiProxyLogger.aspx")
        End Sub
    End Class
End Namespace