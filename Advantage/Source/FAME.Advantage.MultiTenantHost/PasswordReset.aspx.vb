﻿Imports FAME.Advantage.MultiTenantHost.Lib.Presenters
Imports FAME.Advantage.MultiTenantHost.Lib
Imports log4net

Partial Class PasswordReset
    Inherits Page

    Private ReadOnly _presenter As PasswordResetPresenter
    Private ReadOnly _logger As ILog

    Public Sub New()
        _presenter = New PasswordResetPresenter()
        _logger = LogManager.GetLogger(Me.GetType())
    End Sub

    Protected Sub RedirectToLogIn(sender As Object, ByVal e As EventArgs)
        SecurityHelper.RedirectToLoginPage()
    End Sub

    Protected Sub RecoverPwd_SendingMail(sender As Object, e As MailMessageEventArgs)
        Dim userName = CType(Page.FindControl("RecoverPwd").Controls(0).Controls(3), TextBox).Text
        Dim passwordAnswer = CType(Page.FindControl("RecoverPwd").Controls(1).Controls(11), TextBox).Text.ToLower()

        Dim newPassword = _presenter.ResetPassword(userName, passwordAnswer)
        e.Message.Body = _presenter.GetPasswordResetEmail(userName, newPassword, e.Message.Body)
    End Sub

    'DE9525 5/1/2013 Janet Robinson do not allow inactive user to reset password
    Protected Sub RecoverPwd_VerifyingUser(sender As Object, e As LoginCancelEventArgs)
        Dim providerUserKey = CType(Page.FindControl("RecoverPwd").Controls(0).Controls(3), TextBox).Text
        Dim literalControl = CType(Page.FindControl("RecoverPwd").Controls(0).Controls(7), Literal)

        Try
            _presenter.AssertValidUser(providerUserKey)
        Catch ex As PresenterException
            'can't find no user
            literalControl.Visible = True
            literalControl.Text = "Invalid username. Please try again."
            e.Cancel = True
            _logger.Error(ex)
        End Try
    End Sub

    Protected Sub RecoverPwd_SendMailError(sender As Object, e As SendMailErrorEventArgs)
        RecoverPwd.SuccessTemplate = Nothing
        RecoverPwd.SuccessText = String.Empty
        e.Handled = True
        tblFailure.Visible = True
    End Sub
End Class
