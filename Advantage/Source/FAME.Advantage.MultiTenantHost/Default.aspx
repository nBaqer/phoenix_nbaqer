﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI, Version=2018.3.910.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Register Src="~/usercontrols/Advantage_Logo.ascx" TagPrefix="FAME" TagName="AdvantageLogo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Welcome to Advantage</title>
    <link rel="shortcut icon" href="images/favicon.ico"/>

    <link rel="Stylesheet" type="text/css" href="Login.css" />

</head>
<body>
    <form runat="server">
        <div class="clwyLoginContent" align="center">
            <div class="headerBanner">
                <FAME:AdvantageLogo runat="server" ID="Advantage_Logo" />

            </div>
            <asp:Login ID="Login1" runat="server">
                <LayoutTemplate>
                    <table class="clwyLoginBox" align="center">
                        <tr>
                            <td class="clwyLoginHeader" width="100%">
                                <asp:Label ID="lblLoginTitle" runat="server" Text="Enter your login information" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="clwyLoginBody" align="center">
                                    <tr class="clwyLoginFooter" valign="top">
                                        <td colspan="2">&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clwyLoginLables">
                                            <asp:Label ID="lblUserNameTitle" runat="server" Text="Email:" />
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="UserName" runat="server" Width="190px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" Text="email is required" Enabled="false"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="clwyLoginLables">
                                            <asp:Label ID="lblPasswordTitle" runat="server" Text="Password:" />
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="Password" runat="server" TextMode="Password" Width="190px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" Text="password is required" Enabled="false"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="clwyLoginFooter" valign="top">
                            <td>
                                <asp:LinkButton ID="lnkForgotPassword" CssClass="primaryLink" runat="server" Text="Forgot Password?" CommandName="ForgotPassword" OnClick="ForgotPassword_Click"
                                    CausesValidation="false"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr class="clwyLoginFooter" valign="top">
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr class="clwyLoginFooter" valign="top">
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="Login" runat="server"
                                    CssClass="primaryButton"
                                    CommandName="Login" Text="Login"
                                    ValidationGroup="Login" Font-Size="12px"
                                    OnClick="Login_Click" />
                            </td>
                        </tr>
                        <tr class="clwyLoginFooter" valign="top">
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <span class="error">
                                    <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                                </span>
                            </td>
                        </tr>

                    </table>
                </LayoutTemplate>
            </asp:Login>
        </div>
        <div class="clwyLoginPageFooterLabel">
            <%-- This site requires Microsoft Internet Explorer Version 8.0 --%>
            <br />
            <br />
            <table height="40" width="100%">
                <tr>
                    <td>
                        <asp:Label runat="server" Style="width: 250px; margin: auto;">
                    Copyright &copy; 2005 - <%=Year(DateTime.Now).ToString%> FAME. All Rights Reserved.
                        </asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
