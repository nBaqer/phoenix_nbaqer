﻿Option Strict On
Option Explicit On

Imports FAME.Advantage.MultiTenantHost.Lib.Presenters
Imports System.Threading

Partial Class _Default
    Inherits Page

    ReadOnly presenter As LoginPresenter

    Protected Sub New()
        presenter = New LoginPresenter()
    End Sub

    Protected Sub Login_Click(sender As Object, e As EventArgs)
        Dim loginControl = Page.FindControl("Login1")
        Dim userName = CType(loginControl.FindControl("UserName"), TextBox).Text.Trim()
        Dim passWord = CType(loginControl.FindControl("Password"), TextBox).Text.Trim()

        Try
            presenter.Login(userName, passWord)
        Catch ex As ThreadAbortException
            'Debug.WriteLine("Normal Abort Thread exception because Application redirect to other page")
        Catch ex As Exception
            Login1.FailureText = ex.Message
        End Try
    End Sub

    Protected Sub ForgotPassword_Click(sender As Object, e As EventArgs)
        Response.Redirect("~/PasswordReset.aspx")
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim smsg As String = Request.QueryString("errmsg")

        Dim userName = CType(Page.FindControl("Login1").FindControl("UserName"), TextBox)

        If Not IsNothing(smsg) Then
            CType(Page.FindControl("Login1").FindControl("FailureText"), Literal).Text = smsg

            Dim usr = Request.QueryString("usr")
            If Not IsNothing(usr) Then
                userName.Text = usr
            End If
        End If

        userName.Focus()
    End Sub
End Class
