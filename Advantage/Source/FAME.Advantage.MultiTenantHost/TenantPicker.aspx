﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TenantPicker.aspx.vb" Inherits="TenantPicker" MasterPageFile="MasterPage.master" %>

<%@ Register Src="~/NavigationPane.ascx" TagPrefix="fame" TagName="NavigationPane" %>

<asp:Content runat="server" ContentPlaceHolderID="content">
    <style>
        .word-wrap {
            word-wrap: break-word;
        }

        .tenant-header {
            text-align: center;
            border-width: 1px;
            border-style: solid;
            font-size:16px;
        }
    </style>
    <div>

        <asp:ScriptManager ID="scriptmanager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblerrmsg" runat="server" ForeColor="red" Visible="false"></asp:Label>
        <table style="float:left; margin-top:15px;">
            <tr>
                <td>
                    <asp:DropDownList ID="ddltenant" runat="server" Visible="false"></asp:DropDownList></td>
                <td>
                    <div class="RadGrid_Material tenant-header">Select Tenant</div>
                    <telerik:RadGrid runat="server" ID="radTenantPicker" AllowPaging="false" AllowSorting="True" Visible="true"
                        AutoGenerateColumns="False" Width="1024px" ShowStatusBar="True" GridLines="None" AllowFilteringByColumn="True"
                        PageSize="5"
                        OnNeedDataSource="RadGrid1_NeedDataSource"
                        OnItemCommand="RadGrid1_ItemCommand"
                        >
                        <GroupingSettings CaseSensitive="False"></GroupingSettings>
                        <PagerStyle Mode="NumericPages" AlwaysVisible="true" />
                        <MasterTableView TableLayout="Fixed" Width="100%" DataKeyNames="Id" InsertItemPageIndexAction="ShowItemOnFirstPage">
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="Tenant" DataField="Name" Visible="True"
                                    UniqueName="TenantName" FilterControlWidth="130px" GroupByExpression="Name" SortExpression="Name" AutoPostBackOnFilter="True" AllowFiltering="True" CurrentFilterFunction="Contains">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="Linkbutton1" runat="server" Text='<%# Eval("Name")%>' CausesValidation="False" CommandArgument='<%# Eval("Id") %>'>
                                        </asp:LinkButton>

                                    </ItemTemplate>
                                    <HeaderStyle Width="180px" />

                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="VersionNumber" Visible="True" SortExpression="VersionNumber" AutoPostBackOnFilter="True" AllowFiltering="True" CurrentFilterFunction="Contains" HeaderText="VersionNumber" EditFormHeaderTextFormat=""
                                    UniqueName="Upload" FilterControlWidth="80px">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblName" Text='<%# Eval("VersionNumber")%>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="120px" />

                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="Url" Visible="True" SortExpression="Url" AutoPostBackOnFilter="True" AllowFiltering="True" CurrentFilterFunction="Contains" HeaderText="URL" EditFormHeaderTextFormat=""
                                    UniqueName="url" FilterControlWidth="200px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="urlLinkButton" runat="server" Text='<%# Eval("Url")%>' CausesValidation="False" CommandArgument='<%# Eval("Id") %>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle Width="250px" />
                                    <ItemStyle Width="240" CssClass="word-wrap" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="Api" Visible="True" SortExpression="Api" AutoPostBackOnFilter="True" AllowFiltering="True" CurrentFilterFunction="Contains" HeaderText="API" EditFormHeaderTextFormat=""
                                    UniqueName="api" FilterControlWidth="200px">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="apiLabel" Text='<%# Eval("ApiUrl")%>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="250px" />
                                    <ItemStyle Width="240" CssClass="word-wrap" />
                                </telerik:GridTemplateColumn>
                            </Columns>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <ClientSettings>
                            <Scrolling AllowScroll="True" ScrollHeight="475px" UseStaticHeaders="true" SaveScrollPosition="True"></Scrolling>
                        </ClientSettings>
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </div>
    <div class="RadGrid_Material" style="float: left; width:300px; margin-left:25px; margin-top:15px;">
        <fame:NavigationPane runat="server" ID="navigationPane" />
    </div>
</asp:Content>
