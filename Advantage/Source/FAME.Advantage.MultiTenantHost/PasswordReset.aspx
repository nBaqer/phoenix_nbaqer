﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="PasswordReset.aspx.vb" Inherits="PasswordReset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Password Reset</title>
    <link rel="Stylesheet" type="text/css" href="Login.css" />
    <style type="text/css">

        #RecoverPwd{
            margin:auto;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <div class="clwyLoginContent" align="center">
            <div class="headerBanner">
                <img src="images/AdvantageLogo.png" />
            </div>
            <div class="passwordContent" style="margin:auto;">
                <table runat="server" id="tblFailure" border="0" visible="false">
                    <tr>
                        <td align="center">Forgot Your Password?</td>
                    </tr>

                    <tr>
                        <td align="center">Your email settings are not configured. Please contact your system administrator.</td>
                    </tr>
                    <tr>
                        <td style="padding-top: .8em; padding-bottom: .5em; text-align: left;">
                            <asp:LinkButton ID="lnkLogin" runat="server" Text="Login" OnClick="RedirectToLogIn"></asp:LinkButton></td>
                    </tr>
                </table>

                <asp:PasswordRecovery ID="RecoverPwd" runat="server" HelpPageUrl="r" OnSendingMail="RecoverPwd_SendingMail" EnableViewState="false" OnSendMailError="RecoverPwd_SendMailError"
                    QuestionFailureText="Your answer does not match. Please try again." OnVerifyingUser="RecoverPwd_VerifyingUser" Width="300px"
                    SuccessText="Your password reset link has been sent to your email address." GeneralFailureText="Your attempt to retrieve your password was not successful. <br> Please contact your system administrator."
                    UserNameFailureText="We were unable to access your information. Possible reasons are either your email address is incorrect or your account is locked. Please try again or contact your system administrator.">
                    <MailDefinition BodyFileName="~/EmailTemplate/CustomMail.txt" IsBodyHtml="True" Subject="Advantage Password Reset">
                    </MailDefinition>
                    <UserNameTemplate>
                        <table cellpadding="1" cellspacing="0" style="border-collapse: collapse;">
                            <tr>
                                <td>
                                    <table cellpadding="0" width="458px">
                                        <tr>
                                            <td align="center" class="clwyLoginHeader">Forgot Your Password?</td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <p>
                                                    Enter your email address to receive your password. A new password reset link will be sent to your email address.
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="padding-top: 1.2em; padding-bottom: .5em;">
                                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Email:</asp:Label>
                                                <asp:TextBox ID="UserName" runat="server" Width="350px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" CssClass="error"
                                                    ErrorMessage="User Name is required." ToolTip="User Name is required."
                                                    ValidationGroup="RecoverPwd">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td align="left" style="padding-top: .5em; padding-bottom: .5em; padding-left: 0.8em; color: red;">
                                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False" Visible="false"></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%">

                                        <tr>
                                            <td align="left">
                                                <asp:Button ID="SubmitButton" runat="server" CommandName="Submit"
                                                    Text="Submit" ValidationGroup="RecoverPwd" Font-Size="Small" CssClass="primaryButton" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" Font-Size="Small" OnClick="RedirectToLogIn" CssClass="secondaryButton" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </UserNameTemplate>
                    <QuestionTemplate>
                        <table width="400px" cellpadding="1" cellspacing="0" style="border-collapse: collapse;">
                            <tr>
                                <td>
                                    <table cellpadding="0">
                                        <tr>
                                            <td align="center" class="clwyLoginHeader">Forgot Your Password?</td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <p>
                                                    Answer the following security question and a password reset link
                                                    <br />
                                                    will be sent to your email address.
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="430px">
                                        <tr>
                                            <td align="left" width="10px" style="padding-top: 1.2em; padding-bottom: .5em;">
                                                <asp:Label ID="UserNameQuestionLabel" runat="server" AssociatedControlID="UserName" Font-Bold="true">Email:</asp:Label>
                                            </td>
                                            <td align="left" width="140px" style="padding-top: 1.2em; padding-bottom: .5em;">
                                                <asp:Literal ID="UserName" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" nowrap="nowrap" width="10px" style="padding-top: 1.2em; padding-bottom: .5em;">
                                                <asp:Label ID="lblQuestion" runat="server" AssociatedControlID="Question" Font-Bold="true" Width="230px">Question:</asp:Label>
                                            </td>
                                            <td align="left" nowrap="nowrap" width="430px" style="padding-top: 1.2em; padding-bottom: .5em;">
                                                <asp:Literal ID="Question" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="10px" style="padding-top: 1.2em; padding-bottom: .5em;">
                                                <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer" Font-Bold="true">Answer:</asp:Label>
                                            </td>
                                            <td align="left" width="140px" style="padding-top: 1.2em; padding-bottom: .5em;">
                                                <asp:TextBox ID="Answer" runat="server" Width="230px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" ControlToValidate="Answer"
                                                    ErrorMessage="Answer is required." ToolTip="Answer is required." ValidationGroup="PasswordRecovery1" CssClass="error">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="10px" style="padding-top: 1.2em; padding-bottom: .5em;"></td>
                                            <td nowrap="nowrap" align="left" style="padding-top: .5em; padding-bottom: .5em; color: red;">
                                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="90%">
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="SubmitButton" runat="server" CommandName="Submit" Text="Submit" ValidationGroup="PasswordRecovery1" CssClass="primaryButton" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" Font-Size="Small" OnClick="RedirectToLogIn" CssClass="secondaryButton" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </QuestionTemplate>
                    <SuccessTemplate runat="server">
                        <table border="0">
                            <tr>
                                <td align="left" class="sucecssMessage">The password reset link has been sent to your email address.</td>
                            </tr>
                            <tr>
                                <td style="padding-top: .8em; padding-bottom: .5em; text-align: left;">
                                    <asp:LinkButton ID="lnkLogin" runat="server" Text="Login" OnClick="RedirectToLogIn" CssClass="primaryLink"></asp:LinkButton></td>
                            </tr>
                        </table>
                    </SuccessTemplate>
                </asp:PasswordRecovery>
            </div>
        </div>
        <div class="clwyLoginPageFooterLabel">
            <%-- This site requires Microsoft Internet Explorer Version 8.0 --%>
            <br />
            <br />
            <table height="40" width="100%">
                <tr>
                    <td>
                        <asp:Label ID="lblfooter" runat="server" Style="width: 250px; margin: auto;">
                    Copyright &copy; 2005 - <%=Year(DateTime.Now).ToString%> FAME. All Rights Reserved.
                        </asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
