﻿<%@ Page Language="VB"  AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/MasterPage.master" CodeFile="LinkNotValid.aspx.vb" Inherits="LinkNotValid" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <div style="text-align:center;">
        <p>The link that you clicked on is no longer valid </p>
        <p>Please click <a href="Default.aspx">here</a> to go to the login screen</p>
    </div> 
</asp:Content>
