﻿Imports System.Data
Imports System.Data.SqlClient

Namespace FAME.Advantage.MultiTenantHost

    Public Class SQLHelper

        Public Shared Function GetConnection() As SqlConnection
            Dim connectionString = ConfigurationManager.ConnectionStrings("TenantAuthDBConnection").ToString()
            Return New SqlConnection(connectionString)
        End Function

        Public Shared Function GetCommand(commandText As String, connection As SqlConnection) As SqlCommand
            Dim command = New SqlCommand(commandText, connection)
            command.CommandType = CommandType.StoredProcedure

            Return command
        End Function
    End Class
End Namespace