﻿Imports Microsoft.ApplicationInsights.Channel
Imports Microsoft.ApplicationInsights.DataContracts
Imports Microsoft.ApplicationInsights.Extensibility

Namespace FAME.Advantage.MultiTenantHost
    Public Class AppInsightsTelemetryInitializer
        Implements ITelemetryInitializer

        Public Sub Initialize(telemetry As ITelemetry) Implements ITelemetryInitializer.Initialize
            If (Not telemetry.Context.Properties.ContainsKey("Advantage_App")) Then
                telemetry.Context.Properties.Add("Advantage_App", "Host")
            Else
                telemetry.Context.Properties("Advantage_App") = "Host"
            End If
        End Sub
    End Class

End Namespace