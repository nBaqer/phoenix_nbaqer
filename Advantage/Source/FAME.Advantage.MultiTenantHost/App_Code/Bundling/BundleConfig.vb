﻿Imports System.Web.Optimization
Imports Microsoft.VisualBasic

    Public Class BundleConfig
        Public Shared Sub RegisterBundles(bundles As BundleCollection)
            bundles.Add(New StyleBundle("~/bundles/masterpagestyle").Include(
                        "~/css/colors.css",
                        "~/Kendo/styles/kendo.common.min.css",
                        "~/Kendo/styles/kendo.material.min.css",
                        "~/css/localhost_lowercase.css"))

   

            bundles.Add(New ScriptBundle("~/bundles/masterpagescripts").Include(
                             "~/kendo/js/jquery.min.js",
                             "~/kendo/js/kendo.all.min.js",
                             "~/Scripts/KendoExtensions/kendo.web.ext.js"
                            ))

            'Set the optimization key by web.config 
            BundleTable.EnableOptimizations = CType(ConfigurationManager.AppSettings("BundleEnableOptimizations"), Boolean)
        End Sub
    End Class

