Option Strict On
Option Explicit On

Namespace FAME.Advantage.MultiTenantHost

    Public Interface IManageHosting

        Sub ResetPasswordForMigratedUsers(ByVal databaseName As String, ByVal tenantId As Integer)
        Sub DeleteTenant(tenantId As Integer)
    End Interface
End Namespace