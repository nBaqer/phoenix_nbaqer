Option Strict On
Option Explicit On

Imports System.Data.SqlClient
Imports System.Data

Namespace FAME.Advantage.MultiTenantHost

    Public Class ManageUsers

        Public Sub UpdateCreationandPasswordChangeDate(ByVal userId As String)
            Dim connTenant = SQLHelper.GetConnection()
            Dim cmdTenant = SQLHelper.GetCommand("USP_PasswordDate_Update", connTenant)

            cmdTenant.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier, 50).Value = New Guid(userId)
            connTenant.Open()

            Try
                cmdTenant.ExecuteNonQuery()
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                connTenant.Close()
            End Try
        End Sub

        Public Function GetSecurityQuestions() As DataSet
            Dim connTenant = SQLHelper.GetConnection()
            Dim cmdTenant = SQLHelper.GetCommand("USP_SecurityQuestions", connTenant)

            connTenant.Open()

            Try
                Dim ds As New DataSet
                Dim cmdAdapter As SqlDataAdapter = New SqlDataAdapter()
                cmdAdapter.SelectCommand = cmdTenant
                cmdAdapter.Fill(ds)
                Return ds
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                connTenant.Close()
            End Try
        End Function
    End Class
End Namespace