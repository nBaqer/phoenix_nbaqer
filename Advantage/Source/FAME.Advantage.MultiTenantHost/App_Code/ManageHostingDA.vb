﻿Option Strict On
Option Explicit On

Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration

Namespace FAME.Advantage.MultiTenantHost

    Public Class ManageHosting
        Implements IManageHosting

        Private Const DEFAULT_PASSWORD_FOR_MIGRATION As String = "adv4525AD*"

        Public Sub ResetPasswordForMigratedUsers(ByVal databaseName As String, _
                                                 ByVal tenantId As Integer) Implements IManageHosting.ResetPasswordForMigratedUsers
            ' 
            databaseName = "[" + databaseName + "]"

            'Step 1: Migrate Existing users
            MigrateExistingUsers(databaseName, tenantId)

            'Step 2: Get the migrated users and reset their passsword
            Dim dsMigratedUsers = GetMigratedUsers(databaseName)

            'Step 3: Clear tblUsers table
            DisposeMigratedUsersList(databaseName)

            'Step 4: Update Support UserId in the target database
            'UpdateExistingSupportUserAccount(databaseName)

            'Step 5: Map Support Users (commented by Balaji on May 6 2013)
            'Code creates time out issue. This functionality not needed in Version 3.3
            'MapSupportUsers(Databasename)

            'Step 6: Add and System Administrator
            'MigrateSystemAdministrator(Databasename, TenantId)

            'Dim saUserId = MigrateSystemAdministrator(databaseName, tenantId)
            'If Not String.IsNullOrEmpty(saUserId) Then
            '    UpdateSystemAdministrator(databaseName, saUserId)
            'End If

            Dim membershipProvider = Membership.Providers.Item("AspNetSqlMembershipProviderNoSecurityAnswer")

            For Each dr As DataRow In dsMigratedUsers.Tables(0).Rows
                'Try
                Dim userName As String = dr("Email").ToString
                Dim userId As String = dr("UserId").ToString
                Dim myUser As MembershipUser = membershipProvider.GetUser(userName, False)
                If (Not (myUser Is Nothing)) Then

                    Dim oldPassword As String = myUser.ResetPassword()

                    'Change the user's old password to new password
                    myUser.ChangePassword(oldPassword, DEFAULT_PASSWORD_FOR_MIGRATION)

                    'Update user Creation date and Last Password change date 
                    'This step is needed to force the user to change password after password reset
                    Dim clsManageUsers As New ManageUsers
                    clsManageUsers.UpdateCreationandPasswordChangeDate(userId)
                End If
                'Catch ex As Exception
                '    'Suppress for now
                'End Try
            Next

            'Update Password for saUser
            'Try
            '    If Not String.IsNullOrEmpty(saUserId) Then
            '        Dim userKey As Guid = New Guid(saUserId)
            '        Dim mySAUser As MembershipUser = membershipProvider.GetUser(userKey, False)
            '        Dim oldPassword As String = mySAUser.ResetPassword()

            '        'Change the user's old password to new password
            '        mySAUser.ChangePassword(oldPassword, DEFAULT_PASSWORD_FOR_MIGRATION)

            '        'Update user Creation date and Last Password change date 
            '        'This step is needed to force the user to change password after password reset
            '        Dim clsManageUsers1 As New ManageUsers
            '        clsManageUsers1.UpdateCreationandPasswordChangeDate(saUserId)
            '    End If
            'Catch ex As Exception
            'End Try
        End Sub

        ''' <summary>
        ''' Delete the tenant selected
        ''' The info is wipe out for asp-net membership tables and also in tenant tables
        ''' included the Api-key
        ''' </summary>
        ''' <param name="tenantId">The tenant Id</param>
        ''' <remarks></remarks>
        Public Sub DeleteTenant(tenantId As Integer) Implements IManageHosting.DeleteTenant
            ' Get the connection string
            Dim connectionString = ConfigurationManager.ConnectionStrings.Item("TenantAuthDBConnection").ToString()

            ' Call the stored procedure
            Dim conn = New SqlConnection(connectionString)
            Dim command = New SqlCommand("RemoveTenant", conn)
            command.CommandType = CommandType.StoredProcedure
            command.Parameters.AddWithValue("@tenantId", tenantId)

            'Execute the command
            conn.Open()
            Try
                command.ExecuteNonQuery()
            Finally
                conn.Close()
            End Try

            ' End
        End Sub


        Private Sub MigrateExistingUsers(ByVal databaseName As String, ByVal tenantId As Integer)
            Dim connTenant = SQLHelper.GetConnection()
            Dim cmdTenant = SQLHelper.GetCommand("USP_MigrateUsers", connTenant)

            cmdTenant.Parameters.Add("@DatabaseName", SqlDbType.VarChar, 50).Value = databaseName
            cmdTenant.Parameters.Add("@TenantId", SqlDbType.Int).Value = tenantId
            connTenant.Open()

            Try
                cmdTenant.ExecuteNonQuery()
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                connTenant.Close()
            End Try
        End Sub

        Private Sub UpdateSystemAdministrator(ByVal databaseName As String, ByVal userId As String)
            'Dim connTenant = SQLHelper.GetConnection()
            'Dim cmdTenant = SQLHelper.GetCommand("UpdateExistingSystemAdministrator", connTenant)

            'cmdTenant.Parameters.Add("@DatabaseName", SqlDbType.VarChar, 50).Value = databaseName
            'cmdTenant.Parameters.Add("@UserId", SqlDbType.VarChar, 50).Value = userId
            'connTenant.Open()

            'Try
            '    cmdTenant.ExecuteNonQuery()
            'Catch ex As Exception
            '    Throw New Exception(ex.Message)
            'Finally
            '    connTenant.Close()
            'End Try
        End Sub

        Private Function MigrateSystemAdministrator(ByVal databaseName As String, ByVal tenantId As Integer) As String
            Dim connTenant = SQLHelper.GetConnection()
            Dim cmdTenant = SQLHelper.GetCommand("USP_MigrateUsers_systemadministrator", connTenant)

            cmdTenant.CommandTimeout = 300
            cmdTenant.Parameters.Add("@DatabaseName", SqlDbType.VarChar, 50).Value = databaseName
            cmdTenant.Parameters.Add("@TenantId", SqlDbType.Int).Value = tenantId
            connTenant.Open()

            Try
                Return cmdTenant.ExecuteScalar().ToString
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                connTenant.Close()
            End Try
        End Function

        Private Function GetMigratedUsers(ByVal databaseName As String) As DataSet
            Dim connTenant = SQLHelper.GetConnection()
            Dim cmdTenant = SQLHelper.GetCommand("USP_GetMigratedUsers", connTenant)

            cmdTenant.Parameters.Add("@DatabaseName", SqlDbType.VarChar, 50).Value = databaseName
            connTenant.Open()

            Try
                Dim ds As New DataSet
                Dim cmdAdapter = New SqlDataAdapter()

                cmdAdapter.SelectCommand = cmdTenant
                cmdAdapter.Fill(ds)
                Return ds
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                connTenant.Close()
            End Try
        End Function

        Private Sub DisposeMigratedUsersList(ByVal databaseName As String)
            Dim connTenant = SQLHelper.GetConnection()
            Dim cmdTenant = SQLHelper.GetCommand("USP_MigrationUsers_Clean", connTenant)

            cmdTenant.Parameters.Add("@DatabaseName", SqlDbType.VarChar, 50).Value = databaseName
            connTenant.Open()

            Try
                cmdTenant.ExecuteNonQuery()
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                connTenant.Close()
            End Try
        End Sub

        Private Sub UpdateExistingSupportUserAccount(ByVal databaseName As String)
            'Dim connTenant = SQLHelper.GetConnection()
            '' Dim cmdTenant = SQLHelper.GetCommand("UpdateExistingSupportIdentifier", connTenant)
            'Dim cmdTenant = SQLHelper.GetCommand("UpdateAdvantageSupportUser", connTenant)

            'cmdTenant.Parameters.Add("@DatabaseName", SqlDbType.VarChar, 50).Value = databaseName
            'connTenant.Open()

            'Try
            '    cmdTenant.ExecuteNonQuery()
            'Catch ex As Exception
            '    Throw New Exception(ex.Message)
            'Finally
            '    connTenant.Close()
            'End Try
        End Sub
    End Class
End Namespace
