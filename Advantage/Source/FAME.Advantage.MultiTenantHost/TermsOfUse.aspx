﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TermsOfUse.aspx.vb" Inherits="TermsOfUse" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <title>Change Password</title>
    <script src="Scripts/MultiTenantHost.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var advUrl = "<%=TermsOfUse.GetAdvantageUrl()%>";
            sessionStorage.setItem("AdvantageUrl", advUrl);

            var advRedirect = "<%=advantageRedirect.ClientID%>";
            var cancelRedirect = "<%=cancelRedirect.ClientID%>";

            var termsOfUse = new Api.ViewModels.System.TermsOfUseVm();
            termsOfUse.initialize(advRedirect, cancelRedirect);
        });
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <div>
        <div class="k-block k-shadow Absolute-Center" style="margin:-250px 0 0 -600px; width: 1200px; background-color: white; position: absolute; top: 50%; left: 50%; ">
            <div class="k-header" style="font-size: 1.3em; text-align: center">
                Terms and Conditions Updated
            </div>
            <div style="text-align: center; padding: 0 20px 20px 20px;">
                <div class="formRow">
                    <p>The Terms and Conditions for using Advantage have been updated.</p>
                    <p>Click on the <a href="https://support.fameinc.com/hc/en-us/articles/115001146071" target="_blank">Advantage Software and Services Agreement</a> to read the full details of the Terms and Conditions, and then click "I Agree" button if you agree with the terms of use and wish to proceed to use Advantage. Otherwise, click on the "Cancel" button to go back to the login page.</p>
                </div>
                <br/>
                <div class="k-content">
                    <button id="acceptTermsAndConditionsBtn" type="button" class="k-button" style="font-size: 12px; display: inline; margin: auto;">I Agree</button>
                    <button id="denyTermsAndConditionsBtn" type="button" class="k-button" style="font-size: 12px; display: inline; margin: auto;">Cancel</button>
                    <button style="display: none; visibility: hidden" runat="server" id="advantageRedirect" name="advantageRedirect"></button>
                    <button style="display: none; visibility: hidden" runat="server" id="cancelRedirect" name="cancelRedirect"></button>
                </div>
            </div>
            
        </div>
    </div>
    
    <asp:HiddenField ID="userId" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="advantageVersion" runat="server"></asp:HiddenField>
</asp:Content>
