﻿Imports FAME.Advantage.MultiTenantHost
Imports FAME.Advantage.MultiTenantHost.Lib
Imports log4net

Partial Class PasswordResetDefault
    Inherits Page

    Private ReadOnly _logger As ILog

    Public Sub New()
        _logger = LogManager.GetLogger(Me.GetType())
    End Sub

    Protected Sub ChangePasswordPushButton_Click(sender As Object, e As EventArgs)
        Try
            If Not Session("TenantUser") Is Nothing Then
                Dim userName As MembershipUser = Membership.GetUser(Session("TenantUser").ToString())
                Dim currentPassword As String

                If CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("CurrentPassword"), TextBox).Visible = False Then
                    currentPassword = tmppass.Value
                Else
                    currentPassword = CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("CurrentPassword"), TextBox).Text
                End If

                Dim newPassword As String = CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("NewPassword"), TextBox).Text
                Dim minnonalphanumeric = Membership.Provider.MinRequiredNonAlphanumericCharacters
                Dim minlength = Membership.Provider.MinRequiredPasswordLength
                Dim passRegex As New Regex(Membership.Provider.PasswordStrengthRegularExpression)
                Dim newPass As String = CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("NewPassword"), TextBox).Text

                If Not passRegex.IsMatch(newPass) Then
                    If CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("CurrentPassword"), TextBox).Visible Then
                        ChangePassword1.ChangePasswordFailureText = "Password incorrect or New Password invalid. New Password length minimum: " & minlength & ". Special characters required: " & minnonalphanumeric & ". Numeric characters required: 1."
                    Else
                        ChangePassword1.ChangePasswordFailureText = "New Password invalid. New Password length minimum: " & minlength & ". Special characters required: " & minnonalphanumeric & ". Numeric characters required: 1."
                    End If

                    Exit Sub
                End If

                Dim boolSuccess As Boolean = userName.ChangePassword(currentPassword, newPassword)
                If boolSuccess Then
                    Try
                        'Store the security question and answer
                        Dim newPasswordQuestion As String = CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("ddlSecurityQuestion"), DropDownList).SelectedItem.Text
                        Dim newPasswordAnswer As String = CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("txtSecurityAnswer"), TextBox).Text
                        userName.ChangePasswordQuestionAndAnswer(newPassword, newPasswordQuestion, newPasswordAnswer.ToLower)
                        SecurityHelper.RedirectToLoginPage()
                    Catch ex As Threading.ThreadAbortException
                    Catch ex As Exception
                        SecurityHelper.RedirectToLoginPage()
                    End Try
                Else
                    If userName.IsApproved = False Then
                        ChangePassword1.ChangePasswordFailureText = "The user account is currently marked as Inactive. Please contact your system administrator."
                    ElseIf userName.IsLockedOut = True Then
                        ChangePassword1.ChangePasswordFailureText = "The user account is currently locked. Please contact your system administrator."
                    Else
                        ChangePassword1.ChangePasswordFailureText = "Password incorrect or New Password invalid. New Password length minimum: " & minlength & ". Special characters required: " & minnonalphanumeric & ". Numeric characters required: 1."
                    End If
                End If
            End If
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            _logger.Error(ex)
            SecurityHelper.RedirectToLoginPage()
        End Try
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If String.IsNullOrWhiteSpace(Request.QueryString("email")) = False And String.IsNullOrWhiteSpace(Request.QueryString("tpass")) = False Then
                CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("CurrentPasswordLabel"), Label).Visible = False
                CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("CurrentPassword"), TextBox).Visible = False
                CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("CurrentPasswordRequired"), RequiredFieldValidator).Visible = False

                CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("UserName"), Label).Text = Server.UrlDecode(Request.QueryString("email"))
                tmppass.Value = Server.UrlDecode(Request.QueryString("tpass"))
                Session("TenantUser") = Server.UrlDecode(Request.QueryString("email"))
                BuildSecurityQuestion()
            Else
                Try
                    CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("UserName"), Label).Text = Session("TenantUser").ToString()
                    BuildSecurityQuestion()
                Catch ex As Exception
                    _logger.Error(ex)
                    SecurityHelper.RedirectToLoginPage()
                End Try
            End If
        End If
    End Sub
    Private Sub BuildSecurityQuestion()
        Dim ddlSQ = CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("ddlSecurityQuestion"), DropDownList)
        Dim objDBUser As New ManageUsers
        With ddlSQ
            .DataSource = objDBUser.GetSecurityQuestions()
            .DataTextField = "Description"
            .DataValueField = "Id"
            .DataBind()
        End With
    End Sub
End Class
