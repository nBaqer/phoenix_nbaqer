﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="WapiProxyLogger.aspx.vb"
    Inherits="FAME.Advantage.MultiTenantHost.WapiProxyLogger" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">
    <div style="float: left; margin-left: 10px; max-width: 870px; max-height: 600px; ">

        <p style="font-size: 12px">
            <asp:Label ID="Label1" runat="server" Text="WAPI Proxy Log Information:"></asp:Label>
        </p>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="width: 100%; max-height: 580px; overflow: auto; border: darkblue solid 1px">
                    <asp:GridView ID="GridProxyLog" runat="server" AutoGenerateColumns="False"
                        CellPadding="4"
                        EmptyDataText="No record to show" Font-Size="X-Small" ForeColor="#333333"
                        GridLines="None" Width="99%" HorizontalAlign="Left">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="Id" HeaderText="ID" ReadOnly="True" HeaderStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" >
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <div style="min-width:80px; text-align: left">
                                        Tenant Name:
                                  <asp:DropDownList ID="ddlTenant" runat="server" OnSelectedIndexChanged="TenantChanged" AutoPostBack="true" AppendDataBoundItems="true" Font-Size="X-Small">
                                      <asp:ListItem Text="ALL" Value="ALL"></asp:ListItem>
                                  </asp:DropDownList>

                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div style="min-width:80px">
                                        <%# Eval("TenantName")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="DateMod" DataFormatString="{0:g}" HeaderText="Date" ReadOnly="True" HeaderStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px" >
                          
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="110px" />
                            </asp:BoundField>
                          
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Top">
                                <HeaderTemplate>
                                    Company Key:
                                  <asp:DropDownList ID="ddlCompany" runat="server" OnSelectedIndexChanged="CompanyChanged" AutoPostBack="true" AppendDataBoundItems="true" Font-Size="X-Small">
                                      <asp:ListItem Text="ALL" Value="ALL"></asp:ListItem>
                                  </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("Company")%>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Top">
                                <HeaderTemplate>
                                    Called Service:
                                  <asp:DropDownList ID="ddlService" runat="server" OnSelectedIndexChanged="ServiceChanged" AutoPostBack="true" AppendDataBoundItems="true" Font-Size="X-Small">
                                      <asp:ListItem Text="ALL" Value="ALL"></asp:ListItem>
                                  </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("CalledService")%>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    OK:
                                  <asp:DropDownList ID="ddlIsOk" runat="server" OnSelectedIndexChanged="IsOkChanged" AutoPostBack="true" AppendDataBoundItems="true" Font-Size="X-Small">
                                      <asp:ListItem Text="ALL" Value="2"></asp:ListItem>
                                      <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                      <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                  </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("IsOk")%>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>

                            <asp:BoundField DataField="ServiceDescrip" HeaderText="Operation" HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Top" >
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Comment" HeaderText="Comment" HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Top" >
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:BoundField>
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" Height="22px" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" Height="20px" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" Height="21px" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <br />
                </div>
                <p>
                    <asp:Button ID="btnRefresh" runat="server" Text="Refresh" />
                    <asp:Button ID="btnClear" runat="server" Text="Clear" />
                </p>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Button ID="btnExcel" runat="server" Text="Export To Excel" />
    </div>
</asp:Content>

