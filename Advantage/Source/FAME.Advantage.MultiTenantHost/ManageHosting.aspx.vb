﻿Imports FAME.Advantage.MultiTenantHost
Imports FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Extensions
Imports FAME.Advantage.MultiTenantHost.Lib.Presenters
Imports FAME.Advantage.MultiTenantHost.Lib
Imports log4net

Partial Class ManageHosting
    Inherits Page

    Private ReadOnly _presenter As ManageHostingPresenter
    Private ReadOnly _logger As ILog

    Public Sub New()
        _presenter = New ManageHostingPresenter()
        _logger = LogManager.GetLogger(Me.GetType())
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                BuildDropDownLists()
            End If
        Catch ex As Exception
            lblMainMessage.Visible = True
            lblMainMessage.Text = ex.Message.ToString
            _logger.Error(ex)
        End Try
    End Sub

    Private Sub BuildDropDownLists()
        BuildTenantDropDownLists()
        BuildEnvironmentDropDownLists()
    End Sub

    Private Sub BuildEnvironmentDropDownLists()
        Dim environments = _presenter.GetEnvironments()
        ddlTenantEnvironment.LookupList(environments)
        ddlClientEnvironment.LookupList(environments)
    End Sub

    Private Sub BuildTenantDropDownLists()
        Dim tenants = _presenter.GetTenants()
        ddlTenant.LookupList(tenants)
        ddlAccessTenant.LookupList(tenants)
        ddDeleteTenant.LookupList(tenants)
    End Sub

    Protected Sub btnAddTenant_Click(sender As Object, e As EventArgs) Handles btnAddTenant.Click
        Try
            Dim args = New AddTenantArgs() With { _
                .DatabaseName = txtDatabaseName.Text, _
                .EnvironmentId = CInt(ddlTenantEnvironment.SelectedValue), _
                .Name = txtTenantName.Text, _
                .DatabasePassword = txtPassword.Text, _
                .DatabaseServerName = txtServerName.Text, _
                .DatabaseUserName = txtUserName.Text _
            }

            _presenter.AddTenant(args)

            lblMessage.Visible = True
            lblMessage.Text = String.Format("Tenant {0} was successfully created.", args.Name)

            ClearTenantControls()

            BuildTenantDropDownLists()
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message.ToString
            _logger.Error(ex)
        End Try
    End Sub

    Private Sub ClearTenantControls()
        txtDatabaseName.Text = ""
        ddlTenantEnvironment.SelectedIndex = 0
        txtTenantName.Text = ""
        txtPassword.Text = ""
        txtServerName.Text = ""
        txtUserName.Text = ""
    End Sub

    Protected Sub btnSetAccess_Click(sender As Object, e As EventArgs) Handles btnSetAccess.Click
        If ddlAccessTenant.SelectedValue = "" OrElse ddlClientEnvironment.SelectedValue = "" Then
            lblAccessMessage.Visible = True
            lblAccessMessage.Text = "Please select a tenant and environment"
            Return
        End If

        Try
            Dim tenantId = CInt(ddlAccessTenant.SelectedValue)
            Dim environmentId = CInt(ddlClientEnvironment.SelectedValue)

            _presenter.MigrateTenantToNewEnvironment(tenantId, environmentId)

            lblAccessMessage.Visible = True
            lblAccessMessage.Text = "The tenant was switched successfully to the selected environment"
        Catch ex As Exception
            lblAccessMessage.Visible = True
            lblAccessMessage.Text = "An error occurred migrating the tenant to the selected environment"
            _logger.Error(ex)
        End Try
    End Sub

    Protected Sub lnkMigrateUser_Click(sender As Object, e As EventArgs) Handles lnkMigrateUser.Click
        Try
            Dim objDBUser As IManageHosting = New FAME.Advantage.MultiTenantHost.ManageHosting
            objDBUser.ResetPasswordForMigratedUsers(txtDatabase.Text, CInt(ddlTenant.SelectedValue))

            lblMigrationMessage.Visible = True
            lblMigrationMessage.Text = "Tenant's existing users were migrated successfully."
            ddlTenant.SelectedIndex = 0
            txtDatabase.Text = ""
        Catch ex As Exception
            lblMigrationMessage.Visible = True
            lblMigrationMessage.Text = ex.Message.ToString
            _logger.Error(ex)
        End Try
    End Sub

    Protected Sub RadPageView1_Load(sender As Object, e As EventArgs) Handles RadPageView1.Load
        lblMessage.Visible = False
    End Sub

    Protected Sub lnkLogin_Click(sender As Object, e As EventArgs) Handles lnkLogin.Click
        SecurityHelper.RedirectToLoginPage()
    End Sub

    Protected Sub RadTabStrip1_TabClick(sender As Object, e As Telerik.Web.UI.RadTabStripEventArgs) Handles RadTabStrip1.TabClick
        BuildTenantDropDownLists()
        lblMigrationMessage.Text = String.Empty
        lblMessage.Text = String.Empty
        lblDeleteMessage.Text = String.Empty

    End Sub


    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim tenantName = ddDeleteTenant.SelectedItem.Text
        If tenantName = "Select" Then
            lblDeleteMessage.Text = "Please Select a Tenant"
            Return
        End If

        'Begin delete operation
        Try
            Dim tenantId = CInt(ddDeleteTenant.SelectedValue)
            Dim manageHosting As IManageHosting = New FAME.Advantage.MultiTenantHost.ManageHosting()
            manageHosting.DeleteTenant(tenantId)
            lblDeleteMessage.Text = String.Format("The Tenant {0} was Deleted!", tenantName)

            ' Refresh the tenant list
            BuildTenantDropDownLists()
        Catch ex As Exception
            lblDeleteMessage.Text = String.Format("The following exception was raised: {0}{1}", Environment.NewLine, ex.Message)
        End Try


    End Sub
End Class