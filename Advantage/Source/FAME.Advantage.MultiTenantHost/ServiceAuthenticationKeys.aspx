﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ServiceAuthenticationKeys.aspx.vb" Inherits="ServiceAuthenticationKeys" MasterPageFile="MasterPage.master" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/jquery-2.0.3.min.js"></script>
    <script src="Scripts/knockout-2.3.0.js"></script>
    <script src="Scripts/underscore-1.5.1.js"></script>
    <script src="Scripts/models/key.js"></script>
    <script src="Scripts/services/keyGenerator.js"></script>
    <script src="Scripts/models/tenant.js"></script>
    <script src="Scripts/viewModels/serviceAuthenticationKeys.js"></script>    
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">
    <div style="margin-left: 25px">
        <div style="min-height: 25px;"><img src="Content/Images/loading.gif" alt="loading" style="display: none;" data-bind="visible: loading"/></div>
        <div style="float: left; width: 200px;">
            <b>Tenants</b>
            <ul data-bind="foreach: tenants">
                <li><a style="cursor: pointer; text-decoration: underline;" data-bind="text: Name, click: function () { $root.selectTenant($data); }"></a></li>
            </ul>
        </div>
        <div style="border-left: thin solid darkblue; display: inline-block; padding-left: 10px; min-height: 400px; float: left; min-width: 400px;">
            <b data-bind="text: headerTextForKeys()"></b>
            <div>
                <button style="display: none;" data-bind="click: addKey, visible: selectedTenant">Create Key</button>    
            </div>
            <table>
                <tbody data-bind="foreach: seletedTenantKeys()">
                    <tr>
                        <td><button data-bind="click: function () { $root.deleteKey($data.Id); }">Delete</button></td>
                        <td><button data-bind="click: function () { $root.toggleKeyVisibility($data); }, text: $data.showLongKey() ? 'Hide' : 'Show'"></button></td>
                        <td data-bind="text: $data.displayKey"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>