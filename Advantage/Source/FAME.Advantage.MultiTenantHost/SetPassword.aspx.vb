﻿
Imports System.Web.Services
Imports FAME.Advantage.Domain.Infrastructure.Entities
Imports FAME.Advantage.Domain.MultiTenant
Imports FAME.Advantage.MultiTenantHost.Lib.Presenters

Partial Class SetPassword
    Inherits Page
#Region " Web Form Designer Generated Code "

    Shared _email As String
    Shared _tenantName As String
    Shared _timeStamp As String
    Private _hasLatestVersion As String
    Private _tempPass As String
    Protected IsLatestVersion As Boolean
    Protected IsReset As Boolean
    Protected MinPassLength As Integer
    Protected PassRegex As Regex
    Protected MinNonAlphaNumeric As Integer
    Protected Sub New()
    End Sub
    'This call is required by the Web Form Designer.
    <Diagnostics.DebuggerStepThrough> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        Page.Title = "Set Password"
        MinNonAlphaNumeric = Membership.Provider.MinRequiredNonAlphanumericCharacters
        MinPassLength = Membership.Provider.MinRequiredPasswordLength
        PassRegex = New Regex(Membership.Provider.PasswordStrengthRegularExpression)
    End Sub
    Private Sub Page_Init(sender As System.Object, e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region
    Protected campusId As String
    Private Sub Page_Load(sender As System.Object, e As EventArgs) Handles MyBase.Load
        If (String.IsNullOrWhiteSpace(Request.QueryString("email")) Or String.IsNullOrWhiteSpace(Request.QueryString("tpass")) Or String.IsNullOrWhiteSpace(Request.QueryString("tenant")) Or String.IsNullOrWhiteSpace(Request.QueryString("timestamp"))) Then
            Response.Redirect("~/LinkNotValid.aspx")
        End If
        Try
            _email = Encoding.ASCII.GetString(HttpServerUtility.UrlTokenDecode(Request.QueryString("email")))
            _tempPass = Encoding.ASCII.GetString(HttpServerUtility.UrlTokenDecode(Request.QueryString("tpass")))
            _tenantName = Encoding.ASCII.GetString(HttpServerUtility.UrlTokenDecode(Request.QueryString("tenant")))
            _hasLatestVersion = Encoding.ASCII.GetString(HttpServerUtility.UrlTokenDecode(Request.QueryString("hasAcceptedLatest")))
            _timeStamp = Encoding.ASCII.GetString(HttpServerUtility.UrlTokenDecode(Request.QueryString("timestamp")))

            If Not (Membership.ValidateUser(_email, _tempPass)) Then
                Response.Redirect("~/LinkNotValid.aspx")
            End If

            Dim timeStampEpoch As Long = Long.Parse(_timeStamp)
            If ((DateTime.Now - UnixTimeStampToDateTime(timeStampEpoch)).TotalHours >= 6) Then
                Response.Redirect("~/LinkNotValid.aspx")
            End If

            userEmail.Value = _email
            userTenantName.Value = _tenantName
            IsLatestVersion = _hasLatestVersion.Equals("true", StringComparison.InvariantCultureIgnoreCase)
            advVersion.Value = _hasLatestVersion
            Dim reset = False
            IsReset = Boolean.TryParse(Request.QueryString("reset"), reset) AndAlso reset
            If (IsReset) Then
                Page.Title = "Reset Password"
            End If
        Catch
            Response.Redirect("~/LinkNotValid.aspx")
        End Try

    End Sub

    Public Shared Function GetAdvantageApiUrl() As String
        If Not String.IsNullOrWhiteSpace(_email) Then
            Return New LoginPresenter().GetAdvantageApiUrl(_email)
        End If
        Return ""
    End Function
    Public Shared Function GetAdvantageApiUrlWithTenant() As String
        If Not String.IsNullOrWhiteSpace(_email) Then
            Return New LoginPresenter().GetAdvantageApiUrl(_email, _tenantName)
        End If
        Return ""
    End Function
    Public Shared Function UnixTimeStampToDateTime(unixTimeStamp As Long) As DateTime
        'Unix timestamp is seconds past epoch'
        Dim dtDateTime As DateTime = New DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)
        dtDateTime = dtDateTime.AddSeconds(unixTimeStamp)
        Return dtDateTime
    End Function
End Class

