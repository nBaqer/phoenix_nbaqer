﻿
Imports FAME.Advantage.Api.Library.Models

Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Private Sub Page_Load(sender As System.Object, e As EventArgs) Handles MyBase.Load

        Dim token As TokenResponse

        token = Ctype(Session("AdvantageApiToken"), TokenResponse)

        if (not token is nothing )
            hdnApiToken.Value = token.Token
            hdnApiUrl.Value = token.ApiUrl
            hdnApiResponseStatusCode.Value = token.ResponseStatusCode.ToString()
        End If


        Page.Header.DataBind()
    End Sub
End Class

