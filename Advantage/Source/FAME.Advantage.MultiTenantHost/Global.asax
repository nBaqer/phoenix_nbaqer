﻿<%@ Application Language="VB" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="System.Web.Http" %>
<%@ Import Namespace="FAME.Advantage.MultiTenantHost.Lib.Infrastructure.ActionFilters" %>
<%@ Import Namespace="FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Bootstrapping" %>
<%@ Import Namespace="log4net" %>
<%@ Import Namespace="System.Security" %>
<%@ Import Namespace="System.Web.Configuration" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="FAME.Advantage.MultiTenantHost" %>
<%@ Import Namespace="FAME.Advantage.MultiTenantHost.Lib.Infrastructure.DepencyResolver" %>
<%@ Import Namespace="StructureMap" %>
<%@ Import Namespace="FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Validation.FluentValidation.WebApi" %>
<%@ Import Namespace="Microsoft.ApplicationInsights.Extensibility" %>
<%@ Import Namespace="WebApiContrib.IoC.StructureMap" %>
<%@ Import Namespace="StackExchange.Profiling.MiniProfiler" %>

<script RunAt="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        Bootstrapper.Bootstrap()

        RegisterWebApiRoutes()

        RegisterWebApiActionFilters()

        SetWebApiIocContainer()

        EnableFluentValidation()

        BundleConfig.RegisterBundles(BundleTable.Bundles) 'Register asp.net bundling class

        Try
            if (Not WebConfigurationManager.AppSettings("InstrumentationKey") is nothing) then
                TelemetryConfiguration.Active.InstrumentationKey = WebConfigurationManager.AppSettings("InstrumentationKey").ToString()
                Dim appInsightsTelemetryInitializer as new AppInsightsTelemetryInitializer()
                TelemetryConfiguration.Active.TelemetryInitializers.Add(appInsightsTelemetryInitializer)
            else
                TelemetryConfiguration.Active.DisableTelemetry = true
            end If
        Catch ex As Exception
            '_log.Info("Could not initiliaze app insights")
        End Try

    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        Dim logger = LogManager.GetLogger(Me.GetType())
        Dim lastError = Server.GetLastError()

        logger.Error(lastError)
    End Sub

    sub Application_BeginRequest()
        Try
            If (Request.IsLocal) Then
                StackExchange.Profiling.MiniProfiler.Start()
            End If

        Catch ex As Exception

        End Try
    End sub
    sub Application_EndRequest()
        Try
            StackExchange.Profiling.MiniProfiler.Stop()

        Catch ex As Exception

        End Try
    End sub
    <SecurityCritical>
    Private Shared Sub RegisterWebApiRoutes()
        ' Dim routes As RouteCollection = RouteTable.Routes
        ' ''System.Web.Http.RouteCollectionExtensions.MapHttpRoute(("DefaultApi","api/{controller}/{id}")
        '    routes.MapHttpRoute(
        '           name:="DefaultApi",
        '      routeTemplate:="api/{controller}/{id}",
        '      defaults:=New With {.id = System.Web.Http.RouteParameter.Optional}
        '  )
        RouteTable.Routes.MapHttpRoute(
              name:="DefaultApi",
              routeTemplate:="api/{controller}/{id}",
              defaults:=New With {.id = System.Web.Http.RouteParameter.Optional}
          )
    End Sub

    Private Shared Sub RegisterWebApiActionFilters()
        GlobalConfiguration.Configuration.Filters.Add(New UnitOfWorkActionFilter())
        GlobalConfiguration.Configuration.Filters.Add(New AuthenticationActionFilter())
        GlobalConfiguration.Configuration.Filters.Add(New ValidationActionFilter())
        GlobalConfiguration.Configuration.Filters.Add(New GlobalErrorHandler())
    End Sub

    Private Shared Sub SetWebApiIocContainer()
        GlobalConfiguration.Configuration.DependencyResolver = New StructureMapDependencyResolver(Bootstrapper.Container)
    End Sub

    Private Shared Sub EnableFluentValidation()
        GlobalConfiguration.Configuration.Services.Add(GetType(Validation.ModelValidatorProvider), New WebApiFluentValidationModelValidatorProvider())
    End Sub

</script>
