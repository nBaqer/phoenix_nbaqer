﻿
Imports FAME.Advantage.MultiTenantHost.Lib.Infrastructure.API
Imports FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Extensions
Imports FAME.Advantage.MultiTenantHost.Lib.Presenters
Imports Newtonsoft.Json.Linq
Imports Telerik.Windows.Documents.Spreadsheet.Expressions.Functions

Partial Class TermsOfUse
    Inherits System.Web.UI.Page
    Shared userName As String
    Private Shared _presenter As LoginPresenter
    Private _tenantId As Integer?
    Private _userInfo As UserInformation
    Protected Sub New()
        _presenter = New LoginPresenter()
    End Sub
    'This call is required by the Web Form Designer.
    <Diagnostics.DebuggerStepThrough> Private Sub InitializeComponent()
    End Sub

    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        Page.Title = "Terms and Condition Updated"

    End Sub

    Private Sub Page_Init(sender As System.Object, e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

    Private Sub Page_Load(sender As System.Object, e As EventArgs) Handles MyBase.Load
        Dim tempInt As Integer = 0
        If (Integer.TryParse(Request.QueryString("tenantId"), tempInt) AndAlso (tempInt <> 0)) Then
            _tenantId = tempInt
        Else
            _tenantId = Nothing
        End If
        userName = User.Identity.Name
        _userInfo = _presenter.GetUserInformation(userName, _tenantId)

        'Version is latest advantage version retrieved from appsetings via API
        advantageVersion.Value = _userInfo.TermsOfUse.Version

        Dim userIdInCode = User.Identity.Name
        userId.Value = userIdInCode

        If Not Page.IsPostBack Then
            If ((Not _userInfo.TermsOfUse Is Nothing) AndAlso _userInfo.TermsOfUse.HasAcceptedLatestTermsOfUse) Then
                If _userInfo.HasMultipleTenants = True Then
                    Response.Redirect("~/TenantPicker.aspx")
                Else
                    Response.Redirect("~/Logout.aspx")
                End If
            End If
        End If

    End Sub

    Protected Sub AdvantageRedirect_Click(sender As System.Object, e As EventArgs) Handles advantageRedirect.ServerClick
        Response.Redirect(_userInfo.Url)
    End Sub

    Protected Sub CancelRedirect_Click(sender As System.Object, e As EventArgs) Handles cancelRedirect.ServerClick
        If _userInfo.HasMultipleTenants = True Then
            Response.Redirect("~/TenantPicker.aspx")
        Else
            Response.Redirect("~/Logout.aspx")
        End If


    End Sub

    Public Shared Function GetAdvantageUrl() As String
        Return _presenter.GetAdvantageUrl(userName)
    End Function
End Class
