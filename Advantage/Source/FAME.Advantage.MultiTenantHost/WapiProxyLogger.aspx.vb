﻿

Imports FAME.Advantage.MultiTenantHost.Lib.Presenters
Imports System.IO
Imports FAME.Advantage.MultiTenantHost.Lib.Models


Namespace FAME.Advantage.MultiTenantHost
    Partial Class WapiProxyLogger
        Inherits Page

        ReadOnly presenter As WapiProxyLoggerPresenter

        Public Sub New()
            presenter = New WapiProxyLoggerPresenter()
        End Sub

        Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
            If Not Page.IsPostBack Then
                RefreshGridContent()
            End If
        End Sub

#Region "Button Click Events"
        Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
            RefreshGridContent()
        End Sub

        Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
            presenter.ClearLogRecords()
            Dim filter As IWapiProxyLoggerInputModel = CType(ViewState("Filter"), IWapiProxyLoggerInputModel)
            GridProxyLog.DataSource = presenter.GetWapiProxyLogRecords(filter)
            GridProxyLog.DataBind()

        End Sub

        Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click

            Dim form1 As New HtmlForm()
            Response.Clear()
            Response.Buffer = True
            Dim timeStamp As String = DateTime.Now.ToString("{0:yyyy_MM_dd_HH.mm.ss}")
            Dim filename As String = "WapiProxyLog_" & timeStamp & ".xls"


            Response.AddHeader("content-disposition", "attachment;filename=" & filename)
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            'Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            GridProxyLog.AllowPaging = False

            'Eliminate the controls 
            DirectCast(GridProxyLog.HeaderRow.FindControl("ddlTenant"), DropDownList).Visible = False
            DirectCast(GridProxyLog.HeaderRow.FindControl("ddlService"), DropDownList).Visible = False
            DirectCast(GridProxyLog.HeaderRow.FindControl("ddlCompany"), DropDownList).Visible = False
            DirectCast(GridProxyLog.HeaderRow.FindControl("ddlIsOk"), DropDownList).Visible = False

            'GridProxyLog.DataBind()
            form1.Controls.Add(GridProxyLog)
            Controls.Add(form1)
            form1.RenderControl(hw)

            'style to format numbers to string
            Const style As String = "<style> .textmode { mso-number-format:\@; } </style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.[End]()

        End Sub

#End Region

#Region "Filters DropDown Load list information"
        Private Sub BindTenantList(ByVal ddlTenant As DropDownList)
            ddlTenant.DataSource = presenter.GetTenantNameInLoggedRecords()
            ddlTenant.DataBind()
            Dim filter As IWapiProxyLoggerInputModel = CType(ViewState("Filter"), IWapiProxyLoggerInputModel)
            ddlTenant.Items.FindByValue(filter.TenantName).Selected = True
        End Sub

        Private Sub BindCompanyList(ByVal ddlCompany As DropDownList)
            ddlCompany.DataSource = presenter.GetCalledCompaniesInLoggedRecords()
            ddlCompany.DataBind()
            Dim filter As IWapiProxyLoggerInputModel = CType(ViewState("Filter"), IWapiProxyLoggerInputModel)
            ddlCompany.Items.FindByValue(filter.Company).Selected = True
        End Sub

        Private Sub BindServiceList(ByVal ddlService As DropDownList)

            ddlService.DataSource = presenter.GetCalledOperationInLoggedRecords()
            ddlService.DataBind()
            Dim filter As IWapiProxyLoggerInputModel = CType(ViewState("Filter"), IWapiProxyLoggerInputModel)
            ddlService.Items.FindByValue(filter.CalledService).Selected = True
        End Sub

#End Region

#Region "Filter Dropdown Change Events"

        Protected Sub ServiceChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim ddlService As DropDownList = DirectCast(sender, DropDownList)
            Dim filter As IWapiProxyLoggerInputModel = CType(ViewState("Filter"), IWapiProxyLoggerInputModel)
            filter.CalledService = ddlService.SelectedValue
            ViewState("Filter") = filter
            BindGrid()
        End Sub

        Protected Sub CompanyChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim ddlCompany As DropDownList = DirectCast(sender, DropDownList)
            Dim filter As IWapiProxyLoggerInputModel = CType(ViewState("Filter"), IWapiProxyLoggerInputModel)
            filter.Company = ddlCompany.SelectedValue
            ViewState("Filter") = filter
            BindGrid()
        End Sub


        Protected Sub IsOkChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim ddlIsOk As DropDownList = DirectCast(sender, DropDownList)
            Dim filter As IWapiProxyLoggerInputModel = CType(ViewState("Filter"), IWapiProxyLoggerInputModel)
            filter.IsOk = CType(ddlIsOk.SelectedValue, Integer)
            ViewState("Filter") = filter
            BindGrid()
        End Sub

        Protected Sub TenantChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim ddlTenant As DropDownList = DirectCast(sender, DropDownList)
            Dim filter As IWapiProxyLoggerInputModel = CType(ViewState("Filter"), IWapiProxyLoggerInputModel)
            filter.TenantName = ddlTenant.SelectedValue
            ViewState("Filter") = filter
            BindGrid()
        End Sub
#End Region

#Region "Helpers"
        Private Sub RefreshGridContent()
            Dim filter As IWapiProxyLoggerInputModel = WapiProxyLoggerInputModel.Factory()
            ViewState("Filter") = filter
            BindGrid()

        End Sub

        Private Sub BindGrid()
            Dim filter As IWapiProxyLoggerInputModel = CType(ViewState("Filter"), IWapiProxyLoggerInputModel)
            Dim records As IList(Of WapiProxyLoggerOutputModel) = CType(presenter.GetWapiProxyLogRecords(filter), IList(Of WapiProxyLoggerOutputModel))
            GridProxyLog.DataSource = records

            GridProxyLog.DataBind()

            If records.Count <> 0 Then

                Dim ddlTenant As DropDownList = DirectCast(GridProxyLog.HeaderRow.FindControl("ddlTenant"), DropDownList)
                BindTenantList(ddlTenant)

                Dim ddlService As DropDownList = DirectCast(GridProxyLog.HeaderRow.FindControl("ddlService"), DropDownList)
                BindServiceList(ddlService)
                Dim ddlCompany As DropDownList = DirectCast(GridProxyLog.HeaderRow.FindControl("ddlCompany"), DropDownList)
                BindCompanyList(ddlCompany)

                Dim ddlIsOk As DropDownList = DirectCast(GridProxyLog.HeaderRow.FindControl("ddlIsOk"), DropDownList)
                ddlIsOk.Items.FindByValue(CType(filter.IsOk, String)).Selected = True
            End If

        End Sub

#End Region

    End Class
End Namespace