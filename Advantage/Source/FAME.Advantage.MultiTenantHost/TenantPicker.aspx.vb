﻿Imports FAME.Advantage.MultiTenantHost.Lib.Presenters
Imports log4net
Imports Telerik.Web.UI

Partial Class TenantPicker
    Inherits Page

    Private ReadOnly _presenter As TenantPickerPresenter
    Private ReadOnly _logger As ILog
    Protected IsSupportUser As Boolean

    Public Sub New()
        Dim password = ""
        _presenter = New TenantPickerPresenter(User.Identity.Name)
        _logger = LogManager.GetLogger(Me.GetType())
        IsSupportUser = _presenter.IsSupportUser()
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        navigationPane.IsSupportUser = IsSupportUser

        If Not Page.IsPostBack Then
            lblerrmsg.Visible = False
        End If
    End Sub

    Protected Sub RadGrid1_ItemCommand(ByVal source As Object, ByVal e As GridCommandEventArgs)
        Try
            lblerrmsg.Visible = False
            If (TypeOf e.Item Is GridDataItem) Then
                Dim intTenantId As Integer = CType(e.CommandArgument, Integer)

                _presenter.PickTenant(intTenantId)
            End If
        Catch ex As Exception
            lblerrmsg.Text = ex.Message.Replace(vbCrLf, "")
            lblerrmsg.Visible = True
            _logger.Error(ex)
        End Try
    End Sub

    Protected Sub RadGrid1_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        Try
            radTenantPicker.DataSource = _presenter.GetTenantsWithUrls()
        Catch ex As Exception
            lblerrmsg.Text = ex.Message.Replace(vbCrLf, "")
            lblerrmsg.Visible = True
            _logger.Error(ex)
        End Try
    End Sub
    Protected Sub RadGrid1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles radTenantPicker.PreRender
        If (Not IsSupportUser) Then
            For Each column As GridColumn In radTenantPicker.Columns
                If (column.UniqueName = "Upload") Then
                    CType(column, GridTemplateColumn).Visible = False
                    Exit For
                End If
            Next
            radTenantPicker.Rebind()
        End If

    End Sub
End Class
