﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DialogTemplates.ascx.cs" Inherits="usercontrols_DialogTemplates" %>
<!--Kendo Templates for Advantage Custom Dialogs and information Center -->
<script id="errorTemplate" type="text/x-kendo-template">
        <div class="errorMessageTemplate">
            <img width="32px" height="32px" src=#= src #  />
            <h3>#= title #</h3>
            <p>#= message #</p>
        </div>
</script>

<script id="infoTemplate" type="text/x-kendo-template">
        <div class="infoMessageTemplate">
            <img width="32px" height="32px" src=#= src #   />
            <h3>#= title #</h3>
            <p>#= message #</p>
        </div>
</script>
<script id="confirmationTemplate" type="text/x-kendo-template">
    <div style="width:388px;margin:0">
         <div style="width: 390px; text-align:center; display: table; border-spacing: 10px; position: relative; max-height: 50px;">
            <div style="display: table-cell; vertical-align: middle; width:90px">
                    <img src="../Host/images/HelpQuestionMark100.png" />
            </div>
            <div style="display: table-cell; vertical-align: middle; text-align: left; max-height: 50px;">
                <div class="popupMessage" >
             </div>
            </div>
        </div>
        <div class="dialog_buttons" style="background-color: whitesmoke; text-align: center;">
            <input type="button" class="confirm_yes k-button" value="Yes" style="width: 70px; margin:15px" />
            &nbsp;
            <input type="button" class="confirm_no k-button" value="No" style="width: 70px; margin:15px" />
        </div>
    </div>
</script>

<script id="warningDialogTemplate" type="text/x-kendo-template">
        <div style="width:390px;margin:0">
          <div style="width:390px; margin: 0; ">
        <div style="width: 390px; text-align:center; display: table; border-spacing: 10px; position: relative; max-height: 50px;">
            <div style="display: table-cell; vertical-align: middle; width:70px">
                    <img src="../Host/images/HelpWarningTriangle48.png" />
            </div>
            <div style="display: table-cell; vertical-align: middle; text-align: left; max-height: 50px;">
                <div class="popupMessage" >
             </div>
            </div>
        </div>

        <div class="dialog_buttons" style="background-color: whitesmoke; text-align: center;">
            <input type="button" class="confirm_yes k-button" value="OK" style="width: 70px; margin:15px" />
        </div>
    </div>
</script>

<script id="errorDialogTemplate" type="text/x-kendo-template">
          <div style="width:395px; margin: 0; ">
            <div style="width: 395px; text-align:center; display: table;
            border-spacing: 10px; position: relative; height: 150px;
            overflow: scroll;">
                <div style="display: table-cell; vertical-align: middle; width: 70px;">
                    <img src="../Host/images/HelpError50.png" />
                </div>
                <div style="display: table-cell; vertical-align: middle; text-align: left; max-height: 350px; ">
                    <div class="popupMessage" style="width: 290px; max-height: 350px; overflow: auto;">
                    </div>
                </div>
            </div>
            <div class="dialog_buttons" style="background-color: whitesmoke; text-align: center;">
                <input type="button" class="confirm_yes k-button" value="OK" style="width: 70px; margin:15px" />
            </div>
        </div>
</script>

<script id="infoDialogTemplate" type="text/x-kendo-template">
        <div style="width:390px; margin: 0; ">
        <div style="width: 390px; text-align:center; display: table; border-spacing: 10px; position: relative; max-height: 50px;">
            <div style="display: table-cell; vertical-align: middle; width:70px">
                    <img src="../Host/images/HelpInformationMark50.png" />
            </div>
            <div style="display: table-cell; vertical-align: middle; text-align: left; max-height: 50px;">
                <div class="popupMessage" >
             </div>
            </div>
        </div>

        <div class="dialog_buttons" style="background-color: whitesmoke; text-align: center;">
            <input type="button" class="confirm_yes k-button" value="OK" style="width: 70px; margin:15px" />
        </div>
    </div>
</script>

<script id="notificationCenterTemplate" type="text/x-kendo-template">
    <div style="border-spacing: 5px; display: table; position: relative; width:100%; min-height: 45px; margin: 0; background-color: #DDFFD0;  ">
        <div style="margin: 10px; width: 30px; display: table-cell; vertical-align: middle; ">
            <img src="../Host/images/HelpInformationMark50.png" width="25" height="25" />
        </div>
        <div id="popupMessage" style="display: table-cell; vertical-align: middle; color:green ">
        </div>
        <div style="float: right; display:table-cell">
            <div class="popupMessage" style="text-align: center; margin: 10px">
                <img id="closeNotificationCenter" src="HelpClose20.png" style="cursor:pointer" />
            </div>
        </div>
    </div>        
    
</script>
