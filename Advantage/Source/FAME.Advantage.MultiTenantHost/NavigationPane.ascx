﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NavigationPane.ascx.vb" Inherits="FAME.Advantage.MultiTenantHost.NavigationPane" %>

<div>
    <div class="RadGrid_Material tenant-header">Navigate To</div>
    <style type="text/css">
        #navigateTo li{ padding-bottom: 0.9em;}
    </style>
    <ul style="list-style:none;" id="navigateTo">
        <%If (IsSupportUser) %>
        <li>
            <asp:LinkButton ID="lnkTenantPicker" runat="server" Text="Tenant Picker" Font-Size="12px" Font-Underline="false"></asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="lnkManageHosting" runat="server" Text="Manage Hosting" Font-Size="12px" Font-Underline="false"></asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="lnkManageApiAuthenticationKeys" runat="server" Text="Manage Api Authentication Keys" Font-Size="12px" Font-Underline="false"></asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="lnkWapiProxyLog" runat="server" Font-Size="12px" Font-Underline="false">Wapi Proxy Log</asp:LinkButton>
        </li>
            <li>
                <a ID="lnkAdvantageDataFixer" href="../AdvantageDataFixer" target="_blank" style="text-decoration: none; font-size: 12px;">Advantage Data Fixer</a>
            </li>
        <%End If %>
        <li>
            <asp:LinkButton ID="lnkLogOut" runat="server" Text="Log Out" Font-Names="Arial" Font-Size="12px" Font-Underline="false"></asp:LinkButton>
        </li>
    </ul>
</div>
