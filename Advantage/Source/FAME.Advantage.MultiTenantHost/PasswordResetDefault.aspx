﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PasswordResetDefault.aspx.vb" Inherits="PasswordResetDefault" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Change Password</title>
    <link rel="Stylesheet" type="text/css" href="Login.css" />
</head>
<body>
    <form id="form1" runat="server">
     <div class="clwyLoginPageFooter">
     <div class="clwyPageHeader"> 
             <div class="clwyToolbarHeaderInfo">           
            <div class="clwyToolbarHeaderVersionInfo">
                <asp:Label ID="lblApplicationHeaderVersion" runat="server" />
            </div>
            <div class="clwyToolbarHeaderActionsInfo">&nbsp;</div>
        </div>
    <div class="clwyChangePasswordContent" align="center">
        <asp:ChangePassword
        id="ChangePassword1"
        InstructionText="Complete this form to create a new password."
        DisplayUserName="false"
        ContinueDestinationPageUrl="~/Default.aspx"
        CancelDestinationPageUrl="~/Default.aspx"
        CssClass="changePassword"
        TitleTextStyle-CssClass="changePassword_title"
        InstructionTextStyle-CssClass="changePassword_instructions"
        ChangePasswordButtonStyle-CssClass="changePassword_button"
        CancelButtonStyle-CssClass="changePassword_button"
        ContinueButtonStyle-CssClass="changePassword_button"
        Runat="server"
        PasswordHintText = "Please enter a password at least 8 characters long, containing a number and one special character." 
            ChangePasswordFailureText ="New Password invalid. New Password length minimum: 8. Non-alphanumeric characters required: 1" >
       <%-- NewPasswordRegularExpression = '@\"(?=.{8,})(?=(.*\d){1,})(?=(.*\W){1,})' 
        NewPasswordRegularExpressionErrorMessage = "Your password must be at least 8 characters long, and contain at least one number and one special character.">--%>
<CancelButtonStyle CssClass="changePassword_button"></CancelButtonStyle>

<ChangePasswordButtonStyle CssClass="changePassword_button"></ChangePasswordButtonStyle>
                <ChangePasswordTemplate>
                   <table cellpadding="1" cellspacing="0" style="border-collapse:collapse;">
                    <tr>
                    <td>
                        <table cellpadding="0" border="0">
                            <tr>
                                <td align="center" colspan="2" class="changePassword_title">
                                    Change your password and update your security question/answer</td>
                            </tr>
                            <tr>
                                 <td align="left" style="padding-top: .5em;padding-bottom: .6em;">
                                    <asp:Label ID="lblUserName" runat="server" 
                                        AssociatedControlID="UserName">Email</asp:Label>
                                </td>
                                  <td align="left" style="width: 230px;padding-top: .5em;padding-bottom: .5em;padding-left: .2em;">
                                    <asp:label ID="UserName" runat="server" Style="text-align:left;padding-left:0;"></asp:label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="padding-top: .5em;padding-bottom: .5em;">
                                    <asp:Label ID="CurrentPasswordLabel" runat="server" 
                                        AssociatedControlID="CurrentPassword">Password</asp:Label>
                                </td>
                                <td align="left" style="width: 230px;padding-top: .5em;padding-bottom: .5em;padding-left: .2em;">
                                    <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" Width="230px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" 
                                        ControlToValidate="CurrentPassword" ErrorMessage="Password is required." 
                                        ToolTip="Password is required." ValidationGroup="ChangePassword1" Visible="true">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="padding-top: .5em;padding-bottom: .5em;">
                                    <asp:Label ID="NewPasswordLabel" runat="server" 
                                        AssociatedControlID="NewPassword">New Password</asp:Label>
                                </td>
                                 <td align="left" style="width: 230px;padding-top: .5em;padding-bottom: .5em;padding-left: .2em;">
                                    <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" Width="230px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" 
                                        ControlToValidate="NewPassword" ErrorMessage="New Password is required." 
                                        ToolTip="New Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap align="left" style="padding-top: .5em;padding-bottom: .5em;">
                                    <asp:Label ID="ConfirmNewPasswordLabel" runat="server" 
                                        AssociatedControlID="ConfirmNewPassword">Confirm New Password</asp:Label>
                                </td>
                                 <td align="left" style="width: 230px;padding-top: .5em;padding-bottom: .5em;padding-left: .2em;">
                                    <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" Width="230px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" 
                                        ControlToValidate="ConfirmNewPassword" 
                                        ErrorMessage="Confirm New Password is required." 
                                        ToolTip="Confirm New Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"  style="width:100px;padding-top: .5em;padding-bottom: .5em;">
                                    <asp:Label ID="lblSecurityQuestion" runat="server" 
                                        AssociatedControlID="ddlSecurityQuestion">Security question</asp:Label>
                                </td>
                                 <td align="left" style="width: 230px;padding-top: .5em;padding-bottom: .5em;padding-left: .2em;">
                                    <asp:dropdownlist ID="ddlSecurityQuestion" runat="server"></asp:dropdownlist>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                        ControlToValidate="ddlSecurityQuestion" 
                                        ErrorMessage="Security question is required." 
                                        ToolTip="Security question is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" nowrap style="padding-top: .5em;padding-bottom: .5em;" nowrap>
                                    <asp:Label ID="lblSecurityAnswer" runat="server" 
                                        AssociatedControlID="txtSecurityAnswer">Security answer</asp:Label>
                                </td>
                                 <td align="left" style="width: 230px;padding-top: .5em;padding-bottom: .5em;padding-left: .2em;">
                                    <asp:textbox ID="txtSecurityAnswer" runat="server" Width="230px"></asp:textbox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                        ControlToValidate="txtSecurityAnswer" 
                                        ErrorMessage="Security answer is required." 
                                        ToolTip="Security answer is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:CompareValidator ID="NewPasswordCompare" runat="server" 
                                        ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" 
                                        Display="Dynamic" 
                                        ErrorMessage="The new password and the confirm new password does not match. Please try again." 
                                        ValidationGroup="ChangePassword1"></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="color:black;padding-top: .5em;padding-bottom: .5em;">
                                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="ChangePasswordPushButton" runat="server" 
                                        CommandName="ChangePassword" Text="Change Password" 
                                        ValidationGroup="ChangePassword1" Font-Size="12px" OnClick="ChangePasswordPushButton_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

                </ChangePasswordTemplate> 
          
<ContinueButtonStyle CssClass="changePassword_button"></ContinueButtonStyle>

<InstructionTextStyle CssClass="changePassword_instructions"></InstructionTextStyle>

<TitleTextStyle CssClass="changePassword_title"></TitleTextStyle>
            </asp:ChangePassword> 

    </div>
    <div class="clwyLoginPageFooterLabel">
       <%-- This site requires Microsoft Internet Explorer Version 8.0 --%>
        <br />
        <br />
         <table Height="40" width="100%" style="background-color:#3C62A0; padding-bottom:2px;">
        <tr>
            <td>
                <asp:Label ID="lblfooter" runat="server" style="width:250px;margin:auto;color:white;font-size:x-small;">
                    Copyright &copy; 2005 - <%=Year(DateTime.Now).ToString%> FAME. All Rights Reserved.
                </asp:Label>
            </td>
        </tr>
     </table>
        </div>
         </div>
         </div>
  <asp:HiddenField ID="tmppass" runat="server"></asp:HiddenField>
    </form>
</body>
</html>
