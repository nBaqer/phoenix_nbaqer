﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ManageHosting.aspx.vb" Inherits="ManageHosting" MasterPageFile="MasterPage.master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="content">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>
    <style type="text/css">
        .fontLabel {
            font-family: Verdana, Sans Serif;
            font-size: 12px;
        }

        .radpanel {
            padding: 5px;
            height: 400px;
        }
    </style>

    <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />

    <div class="exampleWrapper" style="width: 600px; float: left; margin-left: 10px;">
        <telerik:RadWindowManager RenderMode="Lightweight" ID="RadWindowManager1" runat="server" EnableShadow="true">
        </telerik:RadWindowManager>
        <asp:LinkButton ID="lnkLogin" runat="server" Text="Login" Visible="false"></asp:LinkButton>
        <br />
        <br />
        <asp:Label ID="lblMainMessage" runat="server" Visible="false" ForeColor="Red"></asp:Label>
        <div style="float: left; border: gainsboro solid 1px">
        <telerik:RadTabStrip ID="RadTabStrip1" runat="server" MultiPageID="RadMultiPage1" Width="600px"
                SelectedIndex="0" CssClass="tabStrip" Style="padding-top: 2em;" >
            <Tabs>
                    <telerik:RadTab Text="Add Tenant" Selected="True">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Delete Tenant">
                </telerik:RadTab>
                    <telerik:RadTab Text="Migrate Existing Users">
                </telerik:RadTab>
                <telerik:RadTab Text="Switch Tenants to Live/Beta">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>

            <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" CssClass="multiPage" Width="600px">
            <telerik:RadPageView ID="RadPageView1" runat="server" BackColor="AliceBlue">
                    <table cellpadding="1" cellspacing="0" style="border-collapse: collapse; overflow: scroll;">
                    <tr>
                        <td>
                            <table cellpadding="0">
                                <tr>
                                    <td align="left" style="padding-top: .5em; padding-bottom: .6em;">
                                        <asp:Label ID="lblTenantName" runat="server" ForeColor="Black" Font-Size="10pt">Tenant Name:</asp:Label>
                                    </td>
                                    <td style="padding-top: .5em; padding-bottom: .6em;">
                                        <asp:TextBox ID="txtTenantName" runat="server" Style="text-align: left; padding-left: 0;" Width="230px" Font-Size="10pt"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="padding-top: .5em; padding-bottom: .5em;">
                                        <asp:Label ID="lblServerName" runat="server" ForeColor="Black" Font-Size="10pt">Server Name:</asp:Label>
                                    </td>
                                    <td style="width: 235px; padding-top: .5em; padding-bottom: .5em;">
                                        <asp:TextBox ID="txtServerName" runat="server" Width="230px" Font-Size="10pt"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding-top: .5em; padding-bottom: .5em;">
                                        <asp:Label ID="lblDatabaseName" runat="server" ForeColor="Black" Font-Size="10pt">Database Name:</asp:Label>
                                    </td>
                                    <td nowrap style="width: 235px; padding-top: .5em; padding-bottom: .5em;">
                                        <asp:TextBox ID="txtDatabaseName" runat="server" Width="230px" Font-Size="10pt"></asp:TextBox>
                                            <label class="fontLabel">As appear in SQL Server</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="padding-top: .5em; padding-bottom: .5em;">
                                        <asp:Label ID="lblUserName" runat="server" ForeColor="Black" Font-Size="10pt">User Name:</asp:Label>
                                    </td>
                                    <td style="width: 235px; padding-top: .5em; padding-bottom: .5em;">
                                        <asp:TextBox ID="txtUserName" runat="server" Width="230px" Font-Size="10pt"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="padding-top: .5em; padding-bottom: .5em;">
                                        <asp:Label ID="lblPassword" runat="server" ForeColor="Black" Font-Size="10pt">Password:</asp:Label>
                                    </td>
                                    <td style="width: 235px; padding-top: .5em; padding-bottom: .5em;">
                                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="230px" Font-Size="10pt"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="padding-top: .5em; padding-bottom: .5em;">
                                        <asp:Label ID="lblTenantEnvironment" runat="server" ForeColor="Black" Font-Size="10pt">Environment:</asp:Label>
                                    </td>
                                    <td style="width: 235px; padding-top: .5em; padding-bottom: .5em;">
                                        <asp:DropDownList ID="ddlTenantEnvironment" runat="server" Width="170px"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" style="color: Red; padding-top: .5em; padding-bottom: .5em;">
                                        <asp:Label ID="lblFailureText" runat="server" EnableViewState="False" Font-Size="10pt"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnAddTenant" runat="server" Text="Add Tenant" Font-Size="10pt" />
                                    </td>
                                </tr>
                                <tr colspan="2">
                                        <td>
                                            <asp:Label ID="lblMessage" runat="server" Visible="false" ForeColor="blue" Font-Size="10pt"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageViewDeleteTenant" runat="server" CssClass="radpanel" BackColor="AliceBlue">
                    <div>
                        <label class="fontLabel">
                            Use This to delete a tenant. Delete operation is irrevocable!
                        <br />
                            You got the idea?
                        <br />
                        </label>
                        <br />
                        <br />
                        <label class="fontLabel">Select Tenant to Delete:</label>
                        <asp:DropDownList ID="ddDeleteTenant" runat="server" Height="25px" Width="210px">
                        </asp:DropDownList>
                       
                            <asp:Button ID="btnDelete" runat="server" Text="Delete Tenant" OnClientClick="confirmAspButton(this); return false;" />
                      
                        <br />
                        <br />
                        <asp:Label ID="lblDeleteMessage" runat="server" CssClass="fontLabel"></asp:Label>
                    </div>
                </telerik:RadPageView>

            <telerik:RadPageView ID="RadPageView2" runat="server" CssClass="MigrateUsers" BackColor="AliceBlue">
                <table style="padding-top: 5em; padding-left: 8em;">
                    <tr>
                        <td>
                            <asp:Label ID="lblTenant" runat="server" Text="Tenant" ForeColor="black" Font-Size="10pt"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTenant" runat="server" Width="230px" Font-Size="10pt"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblTenantDatabase" runat="server" ForeColor="Black" Font-Size="10pt">Source Database Name </asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDatabase" runat="server" Width="230px" Font-Size="10pt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Button ID="lnkMigrateUser" runat="server" Text="Migrate Advantage Users" Font-Size="10pt" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                            <td>
                                <asp:Label ID="lblMigrationMessage" runat="server" Visible="false" ForeColor="blue"></asp:Label></td>
                    </tr>
                </table>
            </telerik:RadPageView>

                <telerik:RadPageView ID="RadPageView3" runat="server" Width="600px" Height="362px" BackColor="AliceBlue">
                <table style="padding-top: 2em; padding-left: 2em;">
                    <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" ForeColor="Black" Font-Size="11pt">Select Tenant</asp:Label></td>
                            <td>
                                <asp:DropDownList ID="ddlAccessTenant" runat="server" Width="250px"></asp:DropDownList></td>
                    </tr>
                    <tr style="padding-top: 0.6em;">
                            <td>
                                <asp:Label ID="lblclientenv" runat="server" ForeColor="Black" Font-Size="11pt">Select Default Environment </asp:Label></td>
                            <td>
                                <asp:DropDownList ID="ddlClientEnvironment" runat="server" Width="170px"></asp:DropDownList></td>
                    </tr>
                    <tr style="padding-top: 0.6em;">
                        <td>&nbsp;</td>
                            <td>
                                <asp:Button ID="btnSetAccess" runat="server" Text="Switch Tenant" Font-Size="11pt" /></td>
                    </tr>
                    <tr style="padding-top: 0.6em;">
                        <td>&nbsp;</td>
                            <td>
                                <asp:Label ID="lblAccessMessage" runat="server" Visible="false" ForeColor="blue" Font-Size="11pt"></asp:Label></td>
                    </tr>
                </table>
            </telerik:RadPageView>
        </telerik:RadMultiPage>

        <telerik:RadFormDecorator runat="server" ID="RadFormDecorator1" DecoratedControls="Textarea"></telerik:RadFormDecorator>
    </div>
        <script type="text/javascript">

            function confirmAspButton(button) {
                function aspButtonCallbackFn(arg) {
                    if (arg) {
                        __doPostBack(button.name, "");
                    }
                }
                radconfirm("All Tenant Info will be delete. Are you sure?", aspButtonCallbackFn, 330, 180, null, "Confirm");
            }

        </script>
    </div>
</asp:Content>