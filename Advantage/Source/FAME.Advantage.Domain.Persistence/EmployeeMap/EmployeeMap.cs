﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Employee
{
    public class EmployeeMap : ClassMap<Domain.Employee.Employee>
    {
        public EmployeeMap()
        {
            Table("hrEmployees");

            Id(e => e.ID).Column("EmpId").GeneratedBy.Guid();

            Map(e => e.FirstName).Column("FirstName").Length(50).Not.Nullable();
            Map(e => e.LastName).Column("LastName").Length(50).Not.Nullable();
            Map(e => e.Address).Column("Address1").Length(80).Nullable();
            Map(e => e.City).Column("City").Length(80).Nullable();
            Map(e => e.ModUser).Column("ModUser").Length(50).Nullable();
            Map(e => e.ModDate).Column("ModDate").Nullable();
            References(x => x.State).Column("StateId").Not.LazyLoad();
            Map(e => e.Zip).Column("Zip").Length(80).Nullable();
            HasMany(e => e.EmpContacts).KeyColumn("Empid");
            References(e => e.Campus).Column("CampusId");
            References(e => e.Status).Column("StatusId").Not.LazyLoad();
            HasMany(e => e.EmployeeInfo).KeyColumn("EmpId");
        }
    }
}
