﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.EmployeeMap
{
    public class PositionMap : ClassMap<Domain.Employee.Position>
    {
        public PositionMap()
        {
            Table("syPositions");

            Id(e => e.ID).Column("PositionId").GeneratedBy.Guid();

            Map(e => e.Code).Column("PositionCode").Length(12).Not.Nullable();
            Map(e => e.Description).Column("PositionDescrip").Length(50).Not.Nullable();

        }
    }
}
