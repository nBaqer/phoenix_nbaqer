﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.EmployeeMap
{
    public class EmployeeContactMap : ClassMap<Domain.Employee.EmployeeContact>
    {
        public EmployeeContactMap()
        {
            Table("hrEmpContactInfo");

            Id(e => e.ID).Column("EmpContactInfoId").GeneratedBy.Guid();

            Map(e => e.WorkPhone).Column("WorkPhone").Length(50).Not.Nullable();

            References(e => e.Employee).Column("EmpId").Not.LazyLoad();

        }
    }
}
