﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.EmployeeMap
{
    public class DepartmenttMap : ClassMap<Domain.Employee.Department>
    {
        public DepartmenttMap()
        {
            Table("syHRDepartments");

            Id(e => e.ID).Column("HRDepartmentId").GeneratedBy.Guid();

            Map(e => e.Code).Column("HRDepartmentCode").Length(12).Not.Nullable();
            Map(e => e.Description).Column("HRDepartmentDescrip").Length(50).Not.Nullable();

        }
    }
}
