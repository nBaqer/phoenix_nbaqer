﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.EmployeeMap
{
    public class EmployeeInfoMap : ClassMap<Domain.Employee.EmployeeInfo>
    {
        public EmployeeInfoMap()
        {
            Table("hrEmpHRInfo");

            Id(e => e.ID).Column("EmpHRInfoId").GeneratedBy.Guid();
            
            References(e => e.Department).Column("DepartmentId").Not.LazyLoad();
            References(e => e.Position).Column("PositionId").Not.LazyLoad();
            Map(e => e.HireDate).Column("HireDate").Nullable();

        }
    }
}
