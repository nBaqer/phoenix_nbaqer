﻿using FAME.Advantage.Domain.MostRecentlyUsed;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.MostRecentlyUsed
{
    public class MruMap : ClassMap<Mru>
    {
        public MruMap()
        {
            Table("syMRUS");

            Id(x => x.ID).Column("MRUId").Not.Nullable().GeneratedBy.Guid();
            Map(x => x.Counter).Column("Counter").Not.Nullable();
            Map(x => x.EntityId).Column("ChildId").Not.Nullable();
            Map(x => x.SortOrder).Column("SortOrder").Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable();
            Map(x => x.ModDate).Column("ModDate").Nullable();

            References(x => x.CampusObj).Column("CampusId").Nullable();
            References(x => x.UserObj).Column("UserId").Not.Nullable();
            References(x => x.MruTypeObj).Column("MRUTypeId").Not.Nullable();

        }
    }
}
