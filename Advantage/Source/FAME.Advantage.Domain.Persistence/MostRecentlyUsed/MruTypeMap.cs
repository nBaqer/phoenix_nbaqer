﻿using FAME.Advantage.Domain.MostRecentlyUsed;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.MostRecentlyUsed
{
    /// <summary>
    /// Map for Table syMRUTypes
    /// </summary>
    public class MruTypeMap : ClassMap<MruType>
    {
        /// <summary>
        /// Map for Table syMRUTypes
        /// </summary>
        public MruTypeMap()
        {
            Table("syMRUTypes");

            Id(x => x.ID).Column("MRUTypeId").Not.Nullable().GeneratedBy.Assigned();

            Map(x => x.MruTypeDescription).Column("MRUType").Nullable().Length(50);
            
        }
    }
}
