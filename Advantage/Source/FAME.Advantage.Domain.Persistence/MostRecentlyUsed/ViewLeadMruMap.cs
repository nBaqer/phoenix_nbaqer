﻿using FAME.Advantage.Domain.MostRecentlyUsed;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.MostRecentlyUsed
{
    /// <summary>
    /// Map for ViewLeadMru
    /// </summary>
    public class ViewLeadMruMap : ClassMap<ViewLeadMru>
    {
        /// <summary>
        /// Map for ViewLeadMru
        /// </summary>
        public ViewLeadMruMap()
        {
            Table("VIEW_LeadMru");

            Id(x => x.ID).Column("MRUId").Not.Nullable().GeneratedBy.Guid();
            

            Map(x => x.Counter).Column("Counter").Not.Nullable().Generated.Insert();
            Map(x => x.ModDate).Column("ModDate").Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable();

            References(x => x.LeadObj).Column("LeadId").Not.Nullable().LazyLoad();
            References(x => x.CampusObj).Column("CampusId").Nullable().LazyLoad();
            References(x => x.UserObj).Column("UserId").Not.Nullable().LazyLoad();
            References(x => x.MruTypeObj).Column("MRUTypeId").Not.Nullable().LazyLoad();
        }
    }
}
