﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student
{
    /// <summary>
    /// Map of the table ArProgram
    /// </summary>
    public class ArProgramsMap : ClassMap<ArPrograms>
    {
        /// <summary>
        /// Map of the table ArProgram
        /// </summary>
        public ArProgramsMap()
        {
            Table("arPrograms");

            Id(x => x.ID).Column("ProgId").GeneratedBy.Guid();

            Map(x => x.ProgCode).Column("ProgCode").Not.Nullable().Length(12);
            Map(x => x.ProgDescrip).Column("ProgDescrip").Not.Nullable().Length(100);

            References(x => x.StatusesObj).Column("StatusId").Not.Nullable().LazyLoad();
            References(x => x.CampusGroupObj).Column("CampGrpId").Nullable().LazyLoad();
            
            HasMany(x => x.LeadList).KeyColumn("ProgId").Inverse().Cascade.None().LazyLoad();
            HasMany(x => x.ProgramVersionList).KeyColumn("ProgId").Cascade.DeleteOrphan().LazyLoad();
            this.References(x => x.Calendartype).Column("ACId").LazyLoad();
        }
    }
}
