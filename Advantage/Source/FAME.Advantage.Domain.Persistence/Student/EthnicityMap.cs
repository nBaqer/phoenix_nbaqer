﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student
{
    /// <summary>
    /// Map for table Ethnicity
    /// </summary>
    public class EthnicityMap : ClassMap<Domain.Student.Ethnicity>
    {
        /// <summary>
        /// Map for table Ethnicity
        /// </summary>
        public EthnicityMap()
        {
            Table("adEthCodes");
            ReadOnly();

            Id(x => x.ID).Column("EthCodeId").GeneratedBy.Guid();

            Map(x => x.Code).Column("EthCode").Length(12);
            Map(x => x.Description).Column("EthCodeDescrip").Length(50);

            References(x => x.CampusGroupObj).Column("CampGrpId").Not.Nullable();
            References(x => x.SyStatusesObj).Column("StatusId").Not.Nullable();
        }
    }
}
