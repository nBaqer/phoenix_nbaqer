﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student
{
    public class StudentPhoneMap : ClassMap<Domain.Student.StudentPhone>
    {
        public StudentPhoneMap()
        {
            Table("arStudentPhone");

            Id(s => s.ID).Column("StudentPhoneId").GeneratedBy.Guid();

            Map(s => s.Phone).Column("Phone").Length(12).Nullable();
            Map(s => s.Default1).Column("Default1").Not.Nullable();
            Map(s => s.BestTime).Column("BestTime").Nullable();
            References(s => s.PhoneType).Column("PhoneTypeId");

        }
    }
}
