﻿using FAME.Advantage.Domain.Student.Requirements;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Requirements
{
    public class StudentGroupRequirementGroupBridgeMap : ClassMap<StudentGroupRequirementGroupBridge>
    {
        public StudentGroupRequirementGroupBridgeMap()
        {
            Table("adLeadGrpReqGroups");

            Id(x => x.ID).Column("LeadGrpReqGrpId").GeneratedBy.Guid();

            References(x => x.StudentGroup).Column("LeadGrpId");
            References(x => x.RequirementGroup).Column("ReqGrpId");
        }
    }
}
