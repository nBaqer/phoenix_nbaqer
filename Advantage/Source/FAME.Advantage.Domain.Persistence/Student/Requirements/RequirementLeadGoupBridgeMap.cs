﻿using FAME.Advantage.Domain.Student.Requirements;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Requirements
{
    public class RequirementLeadGoupBridgeMap : ClassMap<RequirementLeadGoupBridge>
    {
        public RequirementLeadGoupBridgeMap()
        {
            Table("adReqLeadGroups");

            Id(x => x.ID).Column("ReqLeadGrpId").GeneratedBy.Guid();

            Map(x => x.IsRequired).Column("IsRequired").Nullable();

            References(x => x.StudentGroup).Column("LeadGrpId");
            References(x => x.EffectiveDates).Column("adReqEffectiveDateId").NotFound.Ignore();
        }
    }
}