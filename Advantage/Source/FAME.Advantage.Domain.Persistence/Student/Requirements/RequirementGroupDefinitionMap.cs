﻿using FAME.Advantage.Domain.Student.Requirements;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Requirements
{
    public class RequirementGroupDefinitionMap : ClassMap<RequirementGroupDefinition>
    {
        public RequirementGroupDefinitionMap()
        {
            Table("adReqGrpDef");

            Id(x => x.ID).Column("ReqGrpDefId").GeneratedBy.Guid();

            Map(x => x.Sequence).Column("Sequence").Nullable();
            Map(x => x.IsRequired).Column("IsRequired").Length(50).Nullable();

            References(x => x.Requirement).Column("adReqId");
            References(x => x.RequirementGroup).Column("ReqGrpId");
        }
    }
}



