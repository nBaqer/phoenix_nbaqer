﻿using FAME.Advantage.Domain.Student.Requirements;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Requirements
{
    public class EducationLeveltMap : ClassMap<EducationLevel>
    {
        public EducationLeveltMap()
        {
            Table("adEdLvls");

            Id(x => x.ID).Column("EdLvlId").GeneratedBy.Guid();

            Map(x => x.Code).Column("EdLvlCode").Length(12).Not.Nullable();
            Map(x => x.Description).Column("EdLvlDescrip").Length(50).Not.Nullable();            

            References(x => x.Status).Column("StatusId").Not.LazyLoad();
            References(x => x.CampusGroup).Column("CampGrpId").Not.LazyLoad();           
        }
    }
}



