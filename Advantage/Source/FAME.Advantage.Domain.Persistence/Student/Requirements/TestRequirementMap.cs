﻿using FAME.Advantage.Domain.Student.Requirements;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Requirements
{
    public class TestRequirementMap : SubclassMap<TestRequirement>
    {
        private const int TestRequirementId = 1;

        public TestRequirementMap()
        {
            DiscriminatorValue(TestRequirementId);
        }
    }
}
