﻿using FAME.Advantage.Domain.Student.Requirements;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Requirements
{
    public class RequirementProgramVersionBridgeMap : ClassMap<RequirementProgramVersionBridge>
    {
        public RequirementProgramVersionBridgeMap()
        {
            Table("adPrgVerTestDetails");

            Id(x => x.ID).Column("PrgVerTestDetailId");

            References(x => x.ProgramVersion).Column("PrgVerId");
            References(x => x.RequirementGroup).Column("ReqGrpId");
            References(x => x.Requirement).Column("adReqId");
        }
    }
}
