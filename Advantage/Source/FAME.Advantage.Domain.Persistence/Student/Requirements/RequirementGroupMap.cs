﻿using FAME.Advantage.Domain.Student.Requirements;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Requirements
{
    public class RequirementGroupMap : ClassMap<RequirementGroup>
    {
        public RequirementGroupMap()
        {
            Table("adReqGroups");

            Id(x => x.ID).Column("ReqGrpId").GeneratedBy.Guid();

            Map(x => x.Code).Column("Code").Nullable();
            Map(x => x.Description).Column("Descrip").Length(50).Nullable();            
            Map(x => x.IsMandatory).Column("IsMandatoryReqGrp").Nullable();
            
            References(x => x.Status).Column("StatusId").Nullable();
            References(x => x.CampusGroup).Column("CampGrpId").Nullable();

            HasMany(x => x.StudentGroupRequirementGroupBridge).KeyColumn("ReqGrpId");
            HasMany(x => x.RequirementGroupDefinitions).KeyColumn("ReqGrpId");
            HasMany(x => x.RequirementProgramVersionBridge).KeyColumn("ReqGrpId");
        }
    }
}