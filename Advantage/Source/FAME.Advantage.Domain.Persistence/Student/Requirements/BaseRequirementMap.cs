﻿using FAME.Advantage.Domain.Student.Requirements;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Requirements
{
    public class BaseRequirementMap : ClassMap<BaseRequirement>
    {
        public BaseRequirementMap()
        {
            Table("adReqs");

            Id(x => x.ID).Column("adReqId").GeneratedBy.Guid();

            Map(x => x.Code).Column("Code").Length(50).Nullable();
            Map(x => x.Description).Column("Descrip").Length(50).Nullable();
            Map(x => x.RequiredForEnrollment).Column("ReqforEnrollment").Nullable();
            Map(x => x.RequiredForFinancialAid).Column("ReqforFinancialAid").Nullable();
            Map(x => x.RequiredForGraduation).Column("ReqforGraduation").Nullable();
            Map(x => x.CampGrpId).Column("CampGrpId").Nullable();

            References(x => x.Status).Column("StatusId").Not.LazyLoad();
            References(x => x.CampusGroup).Column("CampGrpId").Not.LazyLoad();
            References(x => x.SystemModule).Column("ModuleId").Not.LazyLoad();

            HasMany(x => x.EffectiveDates).KeyColumn("adReqId");
            HasMany(x => x.RequirementProgramVersionBridge).KeyColumn("adReqId");
            

           
                        

            DiscriminateSubClassesOnColumn("AdReqTypeId");
        }
    }   
}
