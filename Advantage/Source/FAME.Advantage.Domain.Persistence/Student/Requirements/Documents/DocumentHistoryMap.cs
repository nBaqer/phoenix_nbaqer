﻿using FAME.Advantage.Domain.Student.Requirements.Documents;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Requirements.Documents
{
    public class DocumentHistoryMap : ClassMap<DocumentHistory>
    {
        public DocumentHistoryMap()
        {
            Table("syDocumentHistory");
            
            ReadOnly();

            Id(x => x.ID).Column("FileId").GeneratedBy.Guid();

            Map(x => x.FileName).Column("FileName").Nullable();
            Map(x => x.FileExtension).Column("FileExtension").Nullable();
            Map(x => x.DocumentType).Column("DocumentType").Nullable();
            
        }
    }
}
