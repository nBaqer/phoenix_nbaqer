﻿using FAME.Advantage.Domain.Student.Requirements.Documents;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Requirements.Documents
{
    public class DocumentStatusMap : ClassMap<DocumentStatus>
    {
        public DocumentStatusMap()
        {
            ReadOnly();

            Table("syDocStatuses");

            Id(x => x.ID).Column("DocStatusId").GeneratedBy.Guid();

            Map(x => x.Code).Column("DocStatusCode").Not.Nullable();
            Map(x => x.Description).Column("DocStatusDescrip").Not.Nullable();
            Map(x => x.IsApproved).Column("SysDocStatusId").CustomType<DocumentStatusToBoolean>();
            Map(x => x.SystemDocStatusId).Column("SysDocStatusId").Not.Nullable();
        }
    }
}
