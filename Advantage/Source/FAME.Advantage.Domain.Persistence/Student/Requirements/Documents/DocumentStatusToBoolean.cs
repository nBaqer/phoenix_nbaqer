﻿using System;
using System.Data;
using NHibernate;

namespace FAME.Advantage.Domain.Persistence.Student.Requirements.Documents
{
    internal sealed class DocumentStatusToBoolean : BaseUserType
    {
        private const int Approved = 1;
        private const int NotApproved = 2;

        public override object NullSafeGet(IDataReader rs, string[] names, object owner)
        {
            var obj = NHibernateUtil.Int32.NullSafeGet(rs, names[0]);

            if (obj == null) return null;

            int documentStatusId = 0;
            Int32.TryParse(obj.ToString(), out documentStatusId);            

            if (documentStatusId == 1)
                return true;
            else if (documentStatusId == 2)            
                return false;
                            

            throw new ApplicationException(string.Format("DocumentStatusId '{0}' is not a valid Approved/NotApproved value", documentStatusId));
        }

        public override void NullSafeSet(IDbCommand cmd, object value, int index)
        {
            if (value == null) throw new ArgumentNullException("value");

            var boolValue = (bool)value;

            var documentStatusId = boolValue ? Approved : NotApproved;

            NHibernateUtil.Guid.NullSafeSet(cmd, documentStatusId, index);
        }
    }
}
