﻿using FAME.Advantage.Domain.Student.Requirements;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Requirements
{
    public class DocumentRequirementMap : SubclassMap<DocumentRequirement>
    {
        private const int DocumentRequirementId = 3;

        public DocumentRequirementMap()
        {
            DiscriminatorValue(DocumentRequirementId);
        }
    }
}
