﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student
{
    public class StudentMap : ClassMap<Domain.Student.Student>
    {
        public StudentMap()
        {
            Table("arStudent");

            Id(s => s.ID).Column("StudentId").GeneratedBy.Guid();

            Map(s => s.FirstName).Column("FirstName").Length(50).Not.Nullable();
            Map(s => s.MiddleName).Column("MiddleName").Length(50).Nullable();
            Map(s => s.LastName).Column("LastName").Length(50).Not.Nullable();
            Map(s => s.DOB).Column("DOB").Nullable();
            Map(s => s.HomeEmail).Column("HomeEmail").Length(50).Nullable();
            Map(s => s.WorkEmail).Column("WorkEmail").Length(50).Nullable();
            Map(s => s.SSN).Column("SSN").Length(50).Nullable();
            Map(s => s.StudentNumber).Column("StudentNumber").Length(50).Nullable();
            Map(s => s.ModUser).Column("ModUser").Length(50).Nullable();
            Map(s => s.ModDate).Column("ModDate").Nullable();
            References(x => x.Status).Column("StudentStatus").LazyLoad();
            References(x => x.Gender).Column("Gender").Not.LazyLoad();
            References(x => x.Ethnicity).Column("Race");
            References(x => x.CitizenshipsObj).Column("Citizen").Nullable();

            HasMany(x => x.Enrollments).KeyColumn("StudentId");
            //HasMany(x => x.DocumentHistory).KeyColumn("StudentId");
            HasMany(x => x.StudentPhones).KeyColumn("StudentId");

        }
    }
}
