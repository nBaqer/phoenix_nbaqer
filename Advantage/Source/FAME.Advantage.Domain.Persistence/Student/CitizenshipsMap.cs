﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student
{
    /// <summary>
    ///  Mapping for table adCitizenships
    /// </summary>
    public class CitizenshipsMap: ClassMap<Domain.Student.Citizenships>
    {
        /// <summary>
        /// Mapping for table adCitizenships
        /// </summary>
        public CitizenshipsMap()
        {
            Table("adCitizenships");
 
            Id(x => x.ID).Column("CitizenshipId").GeneratedBy.Guid();

            Map(x => x.CitizenshipCode).Column("CitizenshipCode").Length(12).Not.Nullable();
            Map(x => x.CitizenshipsDescrip).Column("CitizenshipDescrip").Length(50).Not.Nullable();

            HasMany(x => x.StudentList).KeyColumn("CitizenshipId");
            HasMany(x => x.LeadList).KeyColumn("CitizenshipId");
        }
    }
}
