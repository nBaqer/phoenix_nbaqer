﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student
{
    public class StudentPhoneTypeMap : ClassMap<Domain.Student.StudentPhoneType>
    {
        public StudentPhoneTypeMap()
        {
            Table("syPhoneType");

            Id(s => s.ID).Column("PhoneTypeId").GeneratedBy.Guid();

            Map(s => s.Code).Column("PhoneTypeCode").Length(12).Nullable();
            Map(s => s.Description).Column("PhoneTypeDescription").Length(50).Not.Nullable();
            
        }
    }
}
