﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student
{
    public class GenderMap : ClassMap<Domain.Student.Gender>
    {
        public GenderMap()
        {
            Table("adGenders");

            Id(s => s.ID).Column("GenderId").GeneratedBy.Guid();
            Map(x => x.Code).Column("GenderCode").Length(12).Not.Nullable();
            Map(x => x.Description).Column("GenderDescrip").Length(50).Not.Nullable();

            References(x => x.Status).Column("StatusId").Not.LazyLoad();


        }
    }
}
