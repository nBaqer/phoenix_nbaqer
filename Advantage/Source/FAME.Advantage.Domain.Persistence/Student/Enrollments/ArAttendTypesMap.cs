﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map for table AttendTypeId
    /// </summary>
    public class ArAttendTypesMap: ClassMap<ArAttendTypes>
    {
        /// <summary>
        /// Map
        /// </summary>
        public ArAttendTypesMap()
        {
            Table("arAttendTypes");

            Id(x => x.ID).Column("AttendTypeId").GeneratedBy.Guid();

            Map(x => x.Code).Column("Code").Nullable().Length(50);
            Map(x => x.Descrip).Column("Descrip").Nullable().Length(50);
 
            References(x => x.CampusGrpObj).Column("CampGrpId").Not.Nullable();
            References(x => x.StatusObj).Column("StatusId").Not.Nullable();

            HasMany(x => x.LeadList).KeyColumn("AttendTypeId").Inverse().Cascade.None().LazyLoad();

        }
    }
}
