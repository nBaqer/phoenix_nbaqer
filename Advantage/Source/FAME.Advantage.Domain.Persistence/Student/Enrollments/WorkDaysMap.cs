﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map plWorkDays with WorkDays domain.
    /// </summary>
    public class WorkDaysMap : ClassMap<WorkDays>
    {
        public WorkDaysMap()
        {
            Table("plWorkDays");

            Id(x => x.ID).Column("WorkDaysId").GeneratedBy.Guid();
            Map(x => x.WorkDaysDescrip).Column("WorkDaysDescrip").Not.Nullable();
            Map(x => x.ViewOrder).Column("ViewOrder").Not.Nullable();

            HasManyToMany(x => x.PeriodList).Table("syPeriodsWorkDays").ParentKeyColumn("WorkDayId").ChildKeyColumn("PeriodId");
        }
    }
}

