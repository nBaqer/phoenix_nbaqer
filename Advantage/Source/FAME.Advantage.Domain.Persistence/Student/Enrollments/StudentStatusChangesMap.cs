﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map for syStudentStatusChanges. this is a Log Table.
    /// </summary>
    public class StudentStatusChangesMap: ClassMap<StudentStatusChanges>
    {
        public StudentStatusChangesMap()
        {
            Table("syStudentStatusChanges");

            Id(x => x.ID).Column("StudentStatusChangeId").GeneratedBy.Guid();
            Map(x => x.DateOfChange).Column("DateOfChange").Nullable();
            References(x => x.CampusObj).Column("CampusId").Not.Nullable();
            References(x => x.DropReasonObj).Column("DropReasonId").Nullable();
            References(x => x.EnrollmentObj).Column("StuEnrollId").Not.Nullable();
            References(x => x.NewEnrollmentStatusObj).Column("NewStatusId").Nullable();
            References(x => x.OriginalEnrollmentStatusObj).Column("OrigStatusId").Nullable();
        }
    }
}
