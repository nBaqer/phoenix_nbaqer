﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map table arClsSectMeetings to ClassSectionMeeting
    /// </summary>
    public class ClassSectionMeetingMap: ClassMap<ClassSectionMeeting>
    {
        public ClassSectionMeetingMap()
        {
            Table("arClsSectMeetings");

            Id(x => x.ID).Column("ClsSectMeetingId").GeneratedBy.Guid();
            Map(x => x.BreakDuration).Column("BreakDuration").Nullable();
            Map(x => x.EndDate).Column("EndDate").Nullable();
            Map(x => x.StartDate).Column("StartDate").Nullable();

            References(x => x.ClassSectionsObj).Column("ClsSectionId").Not.Nullable();
            References(x => x.InstructionTypeObj).Column("InstructionTypeId").Not.Nullable();
            References(x => x.PeriodObj).Column("PeriodId").Nullable();

            HasMany(x => x.ClassSectionAttendanceList).KeyColumn("ClsSectMeetingId").KeyNullable();
            //References(x => x.WorkDayObj).Column("WorkDaysId").Nullable(); // delete this is not used
        }
    }
}
