﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    public class DropReasonMap: ClassMap<DropReason>
    {
        public DropReasonMap()
        {
            Table("arDropReasons");

            Id(x => x.ID).Column("DropReasonId").GeneratedBy.Guid();
            Map(x => x.Description).Column("Descrip").Not.Nullable().Length(50);
        }
    }
}
