﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map table arStuSuspensions to StudentSuspensions domain.
    /// </summary>
    public class StudentSuspensionsMap: ClassMap<StudentSuspensions>
    {

        public StudentSuspensionsMap()
        {
            Table("arStdSuspensions");

            Id(x => x.ID).Column("StuSuspensionId").GeneratedBy.Guid();

            Map(x => x.StartDate).Column("StartDate").Not.Nullable();
            Map(x => x.EndDate).Column("EndDate").Not.Nullable();
            Map(x => x.Reason).Column("Reason").Length(50).Nullable();
            Map(x => x.StudentStatusChangeId).Column("StudentStatusChangeId").Nullable();

            Map(x => x.ModDate).Column("ModDate").Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable();

            References(x => x.StuEnrollment).Column("StuEnrollId").Not.Nullable();
        }

    }
}
