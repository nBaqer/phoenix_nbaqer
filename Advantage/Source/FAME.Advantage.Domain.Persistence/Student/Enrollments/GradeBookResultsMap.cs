﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    public class GradeBookResultsMap : ClassMap<GradeBookResult>
    {
        public GradeBookResultsMap()
        {
            Table("arGrdBkResults");
            Id(x => x.ID).Column("GrdBkResultId").GeneratedBy.Guid();

            Map(x => x.InstrGrdBkWgtDetailId).Column("InstrGrdBkWgtDetailId").Not.Nullable();
            Map(x => x.Score).Column("Score").Nullable();
            Map(x => x.Comments).Column("Comments").Nullable().Length(50);
            Map(x => x.ModifiedUser).Column("ModUser").Not.Nullable();
            Map(x => x.ModifiedDate).Column("ModDate").Not.Nullable();

            Map(x => x.ResNum).Column("ResNum").Not.Nullable();
            Map(x => x.PostDate).Column("PostDate").Nullable();
            Map(x => x.IsCompGraded).Column("IsCompGraded").Nullable();
            Map(x => x.IsCourseCredited).Column("isCourseCredited").Nullable();

            References(x => x.ClassSection).Column("ClsSectionId").Not.Nullable().LazyLoad();
            References(x => x.Enrollment).Column("StuEnrollId").Nullable().LazyLoad();
        }
    }
}

