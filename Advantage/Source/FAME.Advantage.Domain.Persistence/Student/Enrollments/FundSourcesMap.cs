﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Mapping Table saFundSources
    /// </summary>
    public class FundSourcesMap : ClassMap<FundSources>
    {
        public FundSourcesMap()
        {
            Table("saFundSources");

            Id(x => x.ID).Column("FundSourceId").GeneratedBy.Guid();

            Map(x => x.FundSourceCode).Column("FundSourceCode").Not.Nullable().Length(12);
            HasMany(x => x.StudentAwardList).KeyColumn("FundSourceId").KeyNullable();

        }
    }
}
