﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map cmTimeInternal to TimeInternal domain.
    /// </summary>
    public class TimeIntervalMap : ClassMap<TimeInterval>
    {
        /// <summary>
        /// Map cmTimeInternal to TimeInternal domain.
        /// </summary>
        public TimeIntervalMap()
        {
            Table("cmTimeInterval");

            Id(x => x.ID).Column("TimeIntervalId").GeneratedBy.Guid();
            Map(x => x.TimeIntervalDescrip).Column("TimeIntervalDescrip").Nullable();

            References(x => x.SyStatusesObj).Column("StatusId").Not.Nullable().LazyLoad();

            References(x => x.CampusGroupObj).Column("CampGrpId").Nullable().LazyLoad();

            HasMany(x => x.LeadsList).KeyColumn("TimeIntervalId").Inverse().Cascade.None().LazyLoad();
        }
    }
}
