﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map of table faStudentAwards
    /// </summary>
    public class StudentAwardsMap: ClassMap<StudentAwards>
    {
        public StudentAwardsMap()
        {
            Table("faStudentAwards");

            Id(x => x.ID).Column("StudentAwardId").GeneratedBy.Guid();

            Map(x => x.GrossAmount).Column("GrossAmount").Not.Nullable();
            References(x => x.AwardTypeObj).Column("AwardTypeId").Not.Nullable();
            References(x => x.EnrollmentObj).Column("StuEnrollId").Not.Nullable();
        }
    }
}