﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map arClassSections to arClassSection domain.
    /// </summary>
    public class ArClassSectionsMap : ClassMap<ArClassSections>
    {
        public ArClassSectionsMap()
        {
            Table("arClassSections");

            Id(x => x.ID).Column("ClsSectionId").GeneratedBy.Guid();

            Map(x => x.StartDate).Column("StartDate").Not.Nullable();
            Map(x => x.EndDate).Column("EndDate").Not.Nullable();
            Map(x => x.ClassSection).Column("ClsSection").Not.Nullable().Length(12);

            // This field has a relation many to one with UserID in syUsers
            // The current se in VOYANTdoes not require to mapping the relation.
            Map(x => x.InstructorId).Column("InstructorId").Nullable();
          
            References(x => x.TermEntity).Column("TermId").Not.Nullable();
            References(x => x.RequerimentCourse).Column("ReqId").Not.Nullable();
            
            HasMany(x => x.ResultList).KeyColumn("TestId").KeyNullable();
            HasMany(x => x.UnScheduleClosuresList).KeyColumn("ClsSectionId");
            HasMany(x => x.ClassSectionMeetingList).KeyColumn("ClsSectionId");
            HasMany(x => x.ClassSectionAttendanceList).KeyColumn("ClsSectionId");
        }
    }
}
