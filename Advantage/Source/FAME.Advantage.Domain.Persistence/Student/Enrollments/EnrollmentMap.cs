﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnrollmentMap.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the EnrollmentMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    using FAME.Advantage.Domain.Student.Enrollments;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The enrollment map.
    /// </summary>
    public class EnrollmentMap : ClassMap<Enrollment>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnrollmentMap"/> class.
        /// </summary>
        public EnrollmentMap()
        {
            this.Table("arStuEnrollments");

            this.Id(x => x.ID).Column("StuEnrollId").GeneratedBy.Guid();

            this.Map(x => x.CampusId).Column("CampusId").Not.Nullable().ReadOnly();
            this.Map(x => x.EnrollmentDate).Column("EnrollDate").Not.Nullable();
            this.Map(x => x.StudentId).Column("StudentId").Not.Nullable().ReadOnly();
            this.Map(x => x.EnrollmentId).Column("EnrollmentId").Nullable().Length(50);
            this.Map(x => x.StartDate).Column("StartDate").Nullable();
            this.Map(x => x.DateDetermined).Column("DateDetermined").Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Nullable();
            this.Map(x => x.DropReasonId).Column("DropReasonId").Nullable();
            this.Map(x => x.EntranceInterviewDate).Column("EntranceInterviewDate").Nullable();
            this.Map(x => x.IsFirstTimeInSchool).Column("IsFirstTimeInSchool").Not.Nullable();
            this.Map(x => x.IsFirstTimePostSecSchool).Column("IsFirstTimePostSecSchool").Not.Nullable();
            this.Map(l => l.DisableAutoCharge).Column("DisableAutoCharge").Not.Nullable();
            this.Map(l => l.ThirdPartyContract).Column("ThirdPartyContract").Not.Nullable();
            this.Map(x => x.ExpectedStartDate).Column("ExpStartDate").Nullable();
            this.Map(x => x.MidPointDate).Column("MidPtDate").Nullable();
            this.Map(x => x.ContractedGradDate).Column("ContractedGradDate").Nullable();
            this.Map(x => x.TransferDate).Column("TransferDate").Nullable();
            this.Map(x => x.BadgeNumber).Column("BadgeNumber").Length(50).Nullable();
            this.Map(x => x.TransferHours).Column("TransferHours").Nullable();
            this.Map(x => x.TotalTransferHoursFromThisSchool).Column("TotalTransferHoursFromThisSchool").Nullable();

            this.References(x => x.ProgramVersion).Column("PrgVerId").Not.Nullable();
            this.References(x => x.EnrollmentStatus).Column("StatusCodeId").Nullable();
            this.References(x => x.Student).Column("StudentId").Not.Nullable();
            this.References(x => x.LeadObj).Column("LeadId").Nullable();
            this.References(x => x.CampusObj).Column("CampusId").Not.Nullable();
            this.References(x => x.Shift).Column("ShiftId").Nullable();
            this.References(x => x.ChargingMethod).Column("BillingMethodId").Nullable();
            this.References(x => x.ProgramVersionType).Column("PrgVersionTypeId").Not.Nullable();
            this.References(x => x.TuitionCategory).Column("TuitionCategoryId").Nullable();
            this.References(x => x.FinAidAdvisor).Column("FAAdvisorId").LazyLoad();
            this.References(x => x.AcademicAdvisor).Column("AcademicAdvisor").LazyLoad();
            this.References(x => x.EducationLevel).Column("EdLvlId").LazyLoad();
            this.HasMany(x => x.StudentGroupBridges).KeyColumn("StuEnrollId").LazyLoad();
            this.HasMany(x => x.ResultList).KeyColumn("StuEnrollId").LazyLoad();
            this.HasMany(x => x.StudentLoaList).KeyColumn("StuEnrollId").Not.KeyNullable().Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.StudentSuspensionsList).KeyColumn("StuEnrollId").Not.KeyNullable().Cascade.AllDeleteOrphan().Inverse().LazyLoad();

            this.HasMany(x => x.ExternalShipAttendanceList).KeyColumn("StuEnrollId").Not.KeyNullable().LazyLoad();
            this.HasMany(x => x.AttendanceList).KeyColumn("EnrollId").Not.KeyNullable().LazyLoad();
            this.HasMany(x => x.ClassSectionAttendanceList).KeyColumn("StuEnrollId").KeyNullable().LazyLoad();
            this.HasMany(x => x.StudentClockAttendanceList).KeyColumn("StuEnrollId").Not.KeyNullable().LazyLoad();
            this.HasMany(x => x.ConversionAttendanceList).KeyColumn("StuEnrollId").Not.KeyNullable().LazyLoad();
            this.HasMany(x => x.StudentAttendanceSummaryList).KeyColumn("StuEnrollId").KeyNullable().LazyLoad();
            this.HasMany(x => x.StudentAwardList).KeyColumn("StuEnrollId").Not.KeyNullable().LazyLoad();
            this.HasMany(x => x.GradeBookResults).KeyColumn("StuEnrollId").Not.KeyNullable().LazyLoad();

            this.Map(x => x.Lda).Column("LDA").Nullable();
            this.Map(x => x.ExpectedGraduationDate).Column("ExpGradDate").Nullable();
            this.Map(x => x.ReEnrollmentDate).Column("ReEnrollmentDate").Nullable();
        }
    }
}
