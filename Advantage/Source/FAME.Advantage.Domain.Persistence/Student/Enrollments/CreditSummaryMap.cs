﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditSummaryMap.cs" company="FAME Inc">
//   FAME.Advantage.Domain.Persistence.Student.Enrollments
// </copyright>
// <summary>
//   Defines the CreditSummaryMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    using System;

    using FAME.Advantage.Domain.Student.Enrollments;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The credit summary map.
    /// </summary>
    public class CreditSummaryMap : ClassMap<CreditSummary>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreditSummaryMap"/> class.
        /// </summary>
        public CreditSummaryMap()
        {
            this.Table("syCreditSummary");

            //this.Id(x => x.ID).Column("StuEnrollId").Nullable().GeneratedBy.Guid();

            this.CompositeId()
                .KeyProperty(x => x.StuEnrollId, set => { set.ColumnName("StuEnrollId"); set.Type(typeof(Guid)).Access.Property(); })
                .KeyProperty(x => x.ClassSectionId, set => { set.ColumnName("ClsSectionId"); set.Type(typeof(string)).Access.Property(); })
                .KeyProperty(x => x.TermId, set => { set.ColumnName("TermId"); set.Type(typeof(Guid)).Access.Property(); })
                .KeyProperty(x => x.RequirementId, set => { set.ColumnName("ReqId"); set.Type(typeof(Guid)).Access.Property(); });

            this.Map(x => x.StuEnrollId).Column("StuEnrollId").Nullable();
            this.Map(x => x.TermId).Column("TermId").Nullable();
            this.Map(x => x.TermDescription).Column("TermDescrip").Nullable();
            this.Map(x => x.RequirementId).Column("ReqId").Nullable();
            this.Map(x => x.RequirementDescription).Column("ReqDescrip").Nullable();
            this.Map(x => x.ClassSectionId).Column("ClsSectionId").Nullable();
            this.Map(x => x.CreditsEarned).Column("CreditsEarned").Nullable();
            this.Map(x => x.CreditsAttempted).Column("CreditsAttempted").Nullable();
            this.Map(x => x.CurrentScore).Column("CurrentScore").Nullable();
            this.Map(x => x.CurrentGrade).Column("CurrentGrade").Nullable();
            this.Map(x => x.FinalScore).Column("FinalScore").Nullable();
            this.Map(x => x.FinalGrade).Column("FinalGrade").Nullable();
            this.Map(x => x.Completed).Column("Completed").Nullable();
            this.Map(x => x.FinalGpa).Column("FinalGPA").Nullable();
            this.Map(x => x.ProductWeightedAverageCreditsGpa).Column("Product_WeightedAverage_Credits_GPA").Nullable();
            this.Map(x => x.CountWeightedAverageCredits).Column("Count_WeightedAverage_Credits").Nullable();
            this.Map(x => x.ProductSimpleAverageCreditsGpa).Column("Product_SimpleAverage_Credits_GPA").Nullable();
            this.Map(x => x.CountSimpleAverageCredits).Column("Count_SimpleAverage_Credits").Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Nullable();
            this.Map(x => x.TermGpaSimple).Column("TermGPA_Simple").Nullable();
            this.Map(x => x.TermGpaWeighted).Column("TermGPA_Weighted").Nullable();
            this.Map(x => x.CourseCredits).Column("coursecredits").Nullable();
            this.Map(x => x.CumulativeGpa).Column("CumulativeGPA").Nullable();
            this.Map(x => x.CumulativeGpaSimple).Column("CumulativeGPA_Simple").Nullable();
            this.Map(x => x.FinancialAidCreditsEarned).Column("FACreditsEarned").Nullable();
            this.Map(x => x.Average).Column("Average").Nullable();
            this.Map(x => x.CumAverage).Column("CumAverage").Nullable();
            this.Map(x => x.TermStartDate).Column("TermStartDate").Nullable();
        }
    }
}
