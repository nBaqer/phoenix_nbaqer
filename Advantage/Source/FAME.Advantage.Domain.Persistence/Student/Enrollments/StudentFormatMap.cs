﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentFormatMap.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the StudentFormatMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    using FAME.Advantage.Domain.Student.Enrollments;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The student format map.
    /// </summary>
    public class StudentFormatMap : ClassMap<StudentFormat>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentFormatMap"/> class.
        /// </summary>
        public StudentFormatMap()
        {
            this.Table("syStudentFormat");

            this.Id(l => l.ID).Column("Id").GeneratedBy.Guid();

            this.Map(l => l.FormatType).Column("FormatType").Length(50).Nullable().LazyLoad();
            this.Map(l => l.YearNumber).Column("YearNumber").Nullable().LazyLoad();
            this.Map(l => l.MonthNumber).Column("MonthNumber").Nullable().LazyLoad();
            this.Map(l => l.DateNumber).Column("DateNumber").Nullable().LazyLoad();
            this.Map(l => l.LNameNumber).Column("LNameNumber").Nullable().LazyLoad();
            this.Map(l => l.FNameNumber).Column("FNameNumber").Nullable().LazyLoad();
            this.Map(l => l.SeqNumber).Column("SeqNumber").Nullable().LazyLoad();
            this.Map(l => l.SeqStartingNumber).Column("SeqStartingNumber").Nullable().LazyLoad();

            this.Map(l => l.ModUser).Column("ModUser").Nullable().LazyLoad();
            this.Map(l => l.ModDate).Column("ModDate").Nullable().LazyLoad();
        }
    }
}
