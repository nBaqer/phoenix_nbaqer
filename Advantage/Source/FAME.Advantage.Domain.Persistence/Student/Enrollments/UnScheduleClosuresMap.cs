﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map for arUnschedClosures table to domain UnScheduleClosures
    /// </summary>
    public class UnScheduleClosuresMap: ClassMap<UnScheduleClosures>
    {

        public UnScheduleClosuresMap()
        {
            Table("arUnschedClosures");

            Id(x => x.ID).Column("UnschedClosureId").GeneratedBy.Guid();

            Map(x => x.StartDate).Column("StartDate").Not.Nullable();
            Map(x => x.EndDate).Column("EndDate").Not.Nullable();
            References(x => x.ClassSection).Column("ClsSectionId").Not.Nullable();
        }
    }
}
