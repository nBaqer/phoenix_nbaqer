﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassSectionAttendanceMap.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the ClassSectionAttendanceMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    using FAME.Advantage.Domain.Student.Enrollments;

    using FluentNHibernate.Conventions;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// The class section attendance map.
    /// </summary>
    public class ClassSectionAttendanceMap : ClassMap<ClassSectionAttendance>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClassSectionAttendanceMap"/> class.
        /// </summary>
        public ClassSectionAttendanceMap()
        {
            this.Table("atClsSectAttendance");
            this.LazyLoad();

            this.Id(x => x.ID).Column("ClsSectAttId").GeneratedBy.Guid();
            this.Map(x => x.Actual).Column("Actual").Not.Nullable();
            this.Map(x => x.Comments).Column("Comments").Nullable();
            this.Map(x => x.Excused).Column("Excused").Not.Nullable();
            this.Map(x => x.MeetDate).Column("MeetDate").Not.Nullable();
            this.Map(x => x.Scheduled).Column("Scheduled").Nullable();
            this.Map(x => x.Tardy).Column("Tardy").Not.Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Length(50).Not.Nullable();

            this.References(x => x.ClassSectionMeetingObj).Column("ClsSectMeetingId").Nullable();
            this.References(x => x.ClassSectionObj).Column("ClsSectionId").Not.Nullable();
            this.References(x => x.EnrollmentObj).Column("StuEnrollId").Nullable();
        }
    }
}
