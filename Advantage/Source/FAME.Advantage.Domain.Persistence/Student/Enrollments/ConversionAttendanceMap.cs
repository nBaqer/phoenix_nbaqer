﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConversionAttendanceMap.cs" company="FAME Inc.">
//   FAME.Advantage.Domain.Persistence.Student.Enrollments
// </copyright>
// <summary>
//   Defines the ConversionAttendanceMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    using FAME.Advantage.Domain.Student.Enrollments;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The conversion attendance map.
    /// </summary>
    public class ConversionAttendanceMap : ClassMap<ConversionAttendance>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConversionAttendanceMap"/> class.
        /// </summary>
        public ConversionAttendanceMap()
        {
            this.Table("atConversionAttendance");

            this.Id(x => x.ID).Column("AttendanceId").GeneratedBy.Guid();
            this.Map(x => x.StuEnrollId).Column("StuEnrollId").Not.Nullable();
            this.Map(x => x.MeetDate).Column("MeetDate").Not.Nullable();
            this.Map(x => x.Actual).Column("Actual").Not.Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Length(50).Not.Nullable();

            this.References(x => x.EnrollmentObj).Column("StuEnrollId").Nullable();
        }
    }
}
