﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SchedulesMap.cs" company="FAME Inc.">
//   FAME.Advantage.Domain.Persistence.Student.Enrollments
// </copyright>
// <summary>
//   Defines the SchedulesMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    using FAME.Advantage.Domain.Student.Enrollments;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// Map ARStudentSchedules to Schedules Domain
    /// </summary>
    public class SchedulesMap : ClassMap<Schedules>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SchedulesMap"/> class.
        /// </summary>
        public SchedulesMap()
        {
            this.Table("arStudentSchedules");

            this.Id(x => x.ID).Column("StuScheduleId").GeneratedBy.Guid();

            this.Map(x => x.StuEnrollId).Column("StuEnrollId").Not.Nullable();
            this.Map(x => x.ScheduleId).Column("ScheduleId").Not.Nullable();
            this.Map(x => x.StartDate).Column("StartDate").Nullable();
            this.Map(x => x.EndDate).Column("EndDate").Nullable();
            this.Map(x => x.Active).Column("Active").Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Nullable();
            this.Map(x => x.Source).Column("Source").Nullable();
        }
    }
}
