﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersionMap.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   map for arPrgVersions
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    using FAME.Advantage.Domain.Student.Enrollments;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Map for <code>arPrgVersions</code>
    /// </summary>
    public class ProgramVersionMap : ClassMap<ProgramVersion>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramVersionMap"/> class. 
        /// </summary>
        public ProgramVersionMap()
        {
            this.Table("arPrgVersions");

            this.ReadOnly();

            this.Id(x => x.ID).Column("PrgVerId").Not.Nullable().GeneratedBy.Guid();

            this.Map(x => x.Code).Column("PrgVerCode").Not.Nullable().Length(12);
            this.Map(x => x.Description).Column("PrgVerDescrip").Not.Nullable().Length(50);
            this.Map(x => x.CampGrpId).Column("CampGrpId").Nullable();
            this.Map(x => x.TrackTardies).Column("TrackTardies").Not.Nullable();
            this.Map(x => x.TardiesMakingAbsence).Column("TardiesMakingAbsence").Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Nullable();
            this.Map(x => x.UseTimeClock).Column("UseTimeClock").Nullable();

            this.References(x => x.Status).Column("StatusId").LazyLoad();
            this.References(x => x.CampusGroup).Column("CampGrpId").LazyLoad();
            this.References(x => x.ProgramObj).Column("ProgId").Nullable().LazyLoad();
            this.References(x => x.ProgramGroupObj).Column("PrgGrpId").Nullable().LazyLoad();

            this.References(x => x.AttendanceType).Column("UnitTypeId").Not.Nullable().LazyLoad();

            this.HasMany(x => x.RequirementProgramVersionBridge).KeyColumn("PrgVerId");
         }
    }
}
