﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map for atAttendance Table. 
    /// </summary>
    /// <remarks> this table is empty maybe is not used and can be eliminated</remarks>
    public class AttendanceMap: ClassMap<Attendance>
    {
        public AttendanceMap()
        {
            Table("atAttendance");

            Id(x => x.ID).Column("AttendanceId").GeneratedBy.Guid();

            Map(x => x.Actual).Column("Actual").Nullable();
            Map(x => x.AttendanceDate).Column("AttendanceDate").Nullable();
          
            References(x => x.EnrollmentObj).Column("EnrollId").Nullable();
        }
    }
}
