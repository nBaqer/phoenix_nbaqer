﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// arAttUnitType Table Map to AttendanceUnitType
    /// </summary>
    public class AttendanceUnitTypeMap: ClassMap<AttendanceUnitType>
    {
        public AttendanceUnitTypeMap()
        {
            Table("arAttUnitType");
            
            Id(x => x.ID).Column("UnitTypeId").Not.Nullable().GeneratedBy.Guid();
            Map(x => x.Description).Column("UnitTypeDescrip").Not.Nullable().Length(20);
        }
    }
}
