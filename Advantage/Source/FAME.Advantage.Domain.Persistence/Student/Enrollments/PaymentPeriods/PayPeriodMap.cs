﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments.PaymentPeriods
{
    public class PayPeriodMap : ClassMap<Domain.Student.Enrollments.PaymentPeriods.PayPeriods>
    {
        public PayPeriodMap()
        {
            Table("saPmtPeriods");
            

            Id(x => x.ID).Column("PmtPeriodId").Not.Nullable().GeneratedBy.Guid();
            Map(x => x.PeriodNumber).Column("PeriodNumber").Not.Nullable();
            Map(x => x.ChargeAmount).Column("ChargeAmount").Not.Nullable();
            Map(x => x.CumulativeValue).Column("CumulativeValue").Not.Nullable();
            Map(x => x.IncrementValue).Column("IncrementValue").Not.Nullable();
            Map(x => x.IsCharged).Column("IsCharged").Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable().Length(50);
            Map(x => x.ModDate).Column("ModDate").Nullable();
            

            References(x => x.Increment).Column("IncrementId").Not.Nullable();
            
            

        }
    }
}
