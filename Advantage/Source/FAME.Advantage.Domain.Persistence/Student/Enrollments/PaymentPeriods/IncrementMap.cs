﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments.PaymentPeriods
{
    public class IncrementMap : ClassMap<Domain.Student.Enrollments.PaymentPeriods.Increments>
    {
        public IncrementMap()
        {
            Table("saIncrements");


            Id(x => x.ID).Column("IncrementId").Not.Nullable().GeneratedBy.Guid();

            Map(x => x.EffectiveDate).Column("EffectiveDate").Not.Nullable();
            Map(x => x.ExcessiveAdbsencePercentage).Column("ExcAbscenesPercent").Not.Nullable();
            Map(x => x.IncrementName).Column("IncrementName").Not.Nullable().Length(50);
            Map(x => x.IncrementType).Column("IncrementType").Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable().Length(50);
            Map(x => x.ModDate).Column("ModDate").Nullable();

        }
    }
}
