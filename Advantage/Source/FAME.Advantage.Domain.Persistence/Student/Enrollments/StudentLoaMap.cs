﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    ///  Map table arStudentLOAs to entity StudentLoa
    /// </summary>
    public class StudentLoaMap : ClassMap<StudentLoa>
    {

        public StudentLoaMap()
        {
            Table("arStudentLOAs");

            Id(x => x.ID).Column("StudentLOAId").GeneratedBy.Guid();

            Map(x => x.StartDate).Column("StartDate").Not.Nullable();
            Map(x => x.EndDate).Column("EndDate").Not.Nullable();
            Map(x => x.LOAReturnDate).Column("LOAReturnDate").Nullable();
            Map(x => x.StudentStatusChangeId).Column("StudentStatusChangeId").Nullable();

            Map(x => x.ModDate).Column("ModDate").Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable();
            Map(x => x.StatusCodeId).Column("StatusCodeId").Nullable();
            Map(x => x.LoaRequestDate).Column("LOARequestDate").Nullable();
            
            References(x => x.StuEnrollment).Column("StuEnrollId").Not.Nullable();
            References(x => x.LoaReasonsObj).Column("LOAReasonId").Nullable();
        }

    }
}
