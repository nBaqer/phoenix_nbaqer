﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map for arExternshipAttendance
    /// </summary>
    public class ExternalShipAttendanceMap: ClassMap<ExternalShipAttendance>
    {
        public ExternalShipAttendanceMap()
        {
            Table("arExternshipAttendance");

            Id(x => x.ID).Column("ExternshipAttendanceId").GeneratedBy.Guid();
            Map(x => x.AttendedDate).Column("AttendedDate").Not.Nullable();
            Map(x => x.ModDate).Column("ModDate").Nullable();
            References(x => x.StuEnrollmentObj).Column("StuEnrollId").Not.Nullable();
        }
    }
}
