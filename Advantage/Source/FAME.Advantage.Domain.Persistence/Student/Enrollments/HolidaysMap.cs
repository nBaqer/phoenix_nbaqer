﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map for table syHolidays...................
    /// </summary>
    public class HolidaysMap: ClassMap<Holidays>
    {
        public HolidaysMap()
        {
            Table("syHolidays");

            Id(x => x.ID).Column("HolidayId").GeneratedBy.Guid();

            Map(x => x.StartDate).Column("HolidayStartDate").Not.Nullable();
            Map(x => x.EndDate).Column("HolidayEndDate").Nullable();
            Map(x => x.AllDay).Column("AllDay").Not.Nullable();
            Map(x => x.Description).Column("HolidayDescrip").Not.Nullable();
            References(x => x.CampusGroupObj).Column("CampGrpId").Not.Nullable();
            References(x => x.StatusOptionObj).Column("StatusId").Not.Nullable();
            References(x => x.StartTimeIntervalObj).Column("StartTimeId").Nullable();
            References(x => x.EndTimeIntervalObj).Column("EndTimeId").Nullable();
        }
    }
}
