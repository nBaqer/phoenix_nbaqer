﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentClockAttendanceMap.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Map for Table arStudentClockAttendance
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    using System;

    using FAME.Advantage.Domain.Student.Enrollments;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// Map for Table arStudentClockAttendance
    /// </summary>
    /// <remarks>
    /// This have a composite primary key....
    /// </remarks>
    public class StudentClockAttendanceMap: ClassMap<StudentClockAttendance>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentClockAttendanceMap"/> class.
        /// </summary>
        public StudentClockAttendanceMap()
        {
            this.Table("arStudentClockAttendance");

            this.CompositeId()
                .KeyProperty(x => x.ID, set => { set.ColumnName("ScheduleId"); set.Type(typeof(Guid)).Access.Property();})
                .KeyProperty(x => x.StuEnrollmentObj.ID, set => { set.ColumnName("StuEnrollId"); set.Type(typeof(Guid)).Access.Property(); })
                .KeyProperty(x => x.RecordDate, set => {set.ColumnName("RecordDate"); set.Type(typeof(DateTime)).Access.Property();});

            this.Map(x => x.RecordDate).Column("RecordDate").Not.Nullable();
            this.Map(x => x.ActualHours).Column("ActualHours").Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Nullable();

            this.References(x => x.StuEnrollmentObj).Column("StuEnrollId").Not.Nullable();
        }
    }
}
