﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map syPeriods to Periods Domain.
    /// </summary>
    public class PeriodsMap: ClassMap<Periods>
    {
        public PeriodsMap()
        {
            Table("syPeriods");

            Id(x => x.ID).Column("PeriodId").GeneratedBy.Guid();
            Map(x => x.PeriodDescrip).Column("PeriodDescrip").Not.Nullable();
           
            References(x => x.StartTimeIntervalObj).Column("StartTimeId").Not.Nullable();
            References(x => x.EndTimeIntervalObj).Column("EndTimeId").Not.Nullable();
            
            HasManyToMany(x => x.WorkDaysList).Table("syPeriodsWorkDays").ParentKeyColumn("PeriodId").ChildKeyColumn("WorkDayId");
        }
    }
}

