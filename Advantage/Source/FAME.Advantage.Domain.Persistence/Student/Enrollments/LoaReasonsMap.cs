﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    public class LoaReasonsMap : ClassMap<LoaReasons>
    {
        public LoaReasonsMap()
        {
            Table("arLOAReasons");

            Id(x => x.ID).Column("LOAReasonId").GeneratedBy.Guid();

            Map(x => x.Code).Column("Code").Nullable();
            Map(x => x.Description).Column("Descrip").Nullable();
        }
    }
}
