﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Mapping table ArReqs
    /// </summary>
    public class ReqsCourseMap : ClassMap<ReqsCourse>
    {
        public ReqsCourseMap()
        {
            Table("arReqs");

            Id(x => x.ID).Column("ReqId").Not.Nullable().GeneratedBy.Guid();

            Map(x => x.Description).Column("Descrip").Not.Nullable().Length(100);
            Map(x => x.Code).Column("Code").Not.Nullable().Length(12);
            Map(x => x.IsTrackTardies).Column("TrackTardies").Not.Nullable();
            Map(x => x.TardiesMakingAbsence).Column("TardiesMakingAbsence").Nullable();
           
            References(x => x.UnitType).Column("UnitTypeId").Nullable();
        }
    }
}
