﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SapCheckResultsMap.cs" company="FAME Inc.">
//   FAME.Advantage.Domain.Persistence.Student.Enrollments.SAP
// </copyright>
// <summary>
//   Defines the SapCheckResultsMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments.SAP
{
    using FAME.Advantage.Domain.Student.Enrollments.SAP;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The sap check results map.
    /// </summary>
    public class SapCheckResultsMap : ClassMap<SapCheckResults>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SapCheckResultsMap"/> class.
        /// </summary>
        public SapCheckResultsMap()
        {
            this.Table("arSapChkResults");

            this.Id(x => x.ID).Column("StdRecKey").GeneratedBy.Guid();
            this.Map(x => x.StuEnrollId).Column("StuEnrollId").Not.Nullable();
            this.Map(x => x.DatePerformed).Column("DatePerformed").Not.Nullable();
            this.Map(x => x.Period).Column("Period").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Nullable();
        }
    }
}
