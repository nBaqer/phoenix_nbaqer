﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentAttendanceSummaryMap.cs" company="FAME Inc.">
//  FAME.Advantage.Domain.Persistence.Student.Enrollments 
// </copyright>
// <summary>
//   Defines the AttendanceSummaryMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    using FAME.Advantage.Domain.Student.Enrollments;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The attendance summary map.
    /// </summary>
    public class StudentAttendanceSummaryMap : ClassMap<StudentAttendanceSummary>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentAttendanceSummaryMap"/> class.
        /// </summary>
        public StudentAttendanceSummaryMap()
        {
            this.Table("syStudentAttendanceSummary");

            this.Id(x => x.ID).Column("StudentAttendanceSummaryId").Unique().GeneratedBy.Identity();

            //// this.CompositeId().KeyProperty(x => x.Enrollment.ID).KeyProperty(x => x.ClassSections.ID);

            this.Map(x => x.StudentAttendedDate).Column("StudentAttendedDate").Nullable();
            this.Map(x => x.ScheduledDays).Column("ScheduledDays").Nullable();
            this.Map(x => x.ActualDays).Column("ActualDays").Nullable();
            this.Map(x => x.ActualRunningScheduledDays).Column("ActualRunningScheduledDays").Nullable();
            this.Map(x => x.ActualRunningPresentDays).Column("ActualRunningPresentDays").Nullable();
            this.Map(x => x.ActualRunningAbsentDays).Column("ActualRunningAbsentDays").Nullable();
            this.Map(x => x.ActualRunningMakeupDays).Column("ActualRunningMakeupDays").Nullable();
            this.Map(x => x.ActualRunningTardyDays).Column("ActualRunningTardyDays").Nullable();
            this.Map(x => x.AdjustedPresentDays).Column("AdjustedPresentDays").Nullable();
            this.Map(x => x.AdjustedAbsentDays).Column("AdjustedAbsentDays").Nullable();
            this.Map(x => x.AttendanceTrackType).Column("AttendanceTrackType").Nullable();
            this.Map(x => x.ModifiedUser).Column("ModUser").Nullable();
            this.Map(x => x.ModifiedDate).Column("ModDate").Nullable();
            this.Map(x => x.ActualPresentDaysConvertToHoursCalc).Column("ActualPresentDays_ConvertTo_Hours_Calc").Nullable();
            this.Map(x => x.ScheduledHours).Column("ScheduledHours").Nullable();
            this.Map(x => x.ActualAbsentDaysConvertToHoursCalc).Column("ActualAbsentDays_ConvertTo_Hours_Calc").Nullable();
            this.Map(x => x.TermStartDate).Column("TermStartDate").Nullable();
            this.Map(x => x.ActualPresentDaysConvertToHours).Column("ActualPresentDays_ConvertTo_Hours").Nullable();
            this.Map(x => x.ActualAbsentDaysConvertToHours).Column("ActualAbsentDays_ConvertTo_Hours").Nullable();
            this.Map(x => x.ActualTardyDaysConvertToHours).Column("ActualTardyDays_ConvertTo_Hours").Nullable();
            this.Map(x => x.TermDescrip).Column("TermDescrip").Nullable();
            this.Map(x => x.TardiesMakingAbsence).Column("tardiesmakingabsence").Nullable();
            this.Map(x => x.IsExcused).Column("IsExcused").Nullable();
            this.Map(x => x.ActualRunningExcusedDays).Column("ActualRunningExcusedDays").Nullable();
            this.Map(x => x.IsTardy).Column("IsTardy").Nullable();

            this.References(x => x.EnrollmentObj).Column("StuEnrollId").Nullable().LazyLoad();

            this.References(x => x.ClassSectionsObj).Column("ClsSectionId").Nullable().LazyLoad();

            this.References(x => x.TermObj).Column("TermId").Nullable().LazyLoad();
        }
    }
}
