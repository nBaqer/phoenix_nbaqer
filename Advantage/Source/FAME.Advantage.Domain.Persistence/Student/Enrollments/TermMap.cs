﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map for Table arTerm
    /// </summary>
    public class TermMap : ClassMap<Term>
    {
        /// <summary>
        /// Map for Table arTerm
        /// </summary>
        public TermMap()
        {
            Table("arTerm");

            ReadOnly();

            Id(x => x.ID).Column("TermId").Not.Nullable().GeneratedBy.Guid();

            Map(x => x.TermCode).Column("TermCode").Not.Nullable().Length(12);
            Map(x => x.TermDescription).Column("TermDescrip").Not.Nullable().Length(50);
            Map(x => x.IsModule).Column("IsModule").Nullable();
            Map(x => x.CampGrpId).Column("CampGrpId").Nullable();
            Map(x => x.StartDate).Column("StartDate").Nullable();
            Map(x => x.EndDate).Column("EndDate").Nullable();
            
            References(x => x.Status).Column("StatusId").Not.Nullable().LazyLoad();
            References(x => x.CampusGroup).Column("CampGrpId").Nullable().LazyLoad();
            References(x => x.ProgramObj).Column("ProgId").Nullable().LazyLoad();

            HasMany(x => x.ClassSectionsList).KeyColumn("TermId").LazyLoad();

        }
    }
}
