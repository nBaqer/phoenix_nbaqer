﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    /// <summary>
    /// Map Table arGradeSystemDetails
    /// </summary>
    public class GradeSystemDetailsMap: ClassMap<GradeSystemDetails>
    {
        public GradeSystemDetailsMap()
        {
            Table("arGradeSystemDetails");

            Id(x => x.ID).Column("GrdSysDetailId").GeneratedBy.Guid();

            Map(x => x.Gpa).Column("GPA").Nullable();
            Map(x => x.Grade).Column("Grade").Nullable();

            HasMany(x => x.ArResultsList).KeyColumn("GrdSysDetailId");
        }
    }
}
