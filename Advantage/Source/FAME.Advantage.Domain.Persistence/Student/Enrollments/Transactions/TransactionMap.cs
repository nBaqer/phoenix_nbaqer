﻿using FAME.Advantage.Domain.Student.Enrollments.Transactions;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments.Transactions
{
    public class TransactionMap : ClassMap<Domain.Student.Enrollments.Transactions.Transactions>
    {
        public TransactionMap()
        {
            Table("saTransactions");
            

            Id(x => x.ID).Column("TransactionId").Not.Nullable().GeneratedBy.Guid();

            Map(x => x.Description).Column("TransDescrip").Nullable().Length(50);
            Map(x => x.CreatedDate).Column("CreateDate").Nullable();
            Map(x => x.Amount).Column("TransAmount").Not.Nullable();
            Map(x => x.IsPosted).Column("IsPosted").Not.Nullable();
            Map(x => x.Reference).Column("TransReference").Not.Nullable();
            Map(x => x.TransDate).Column("TransDate").Not.Nullable();
            

            References(x => x.PaymentPeriod).Column("PmtPeriodId").Nullable();
            References(x => x.StudentEnrollment).Column("StuEnrollId").Not.Nullable();
            References(x => x.Term).Column("TermId").Nullable();

            References(x => x.TransactionCode).Column("TransCodeId").Nullable();
            

        }
    }
}
