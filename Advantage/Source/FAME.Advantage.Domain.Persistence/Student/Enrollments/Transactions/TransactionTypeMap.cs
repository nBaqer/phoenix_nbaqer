﻿using FAME.Advantage.Domain.Student.Enrollments.Transactions;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments.Transactions
{
    public class TransactionTypesMap : ClassMap<Domain.Student.Enrollments.Transactions.TransactionType>
    {
        public TransactionTypesMap()
        {
            Table("saTransTypes");


            Id(x => x.ID).Column("TransTypeId").Not.Nullable().GeneratedBy.Identity();
            Map(x => x.Description).Column("Description").Nullable().Length(50);
            
            



        }
    }
}
