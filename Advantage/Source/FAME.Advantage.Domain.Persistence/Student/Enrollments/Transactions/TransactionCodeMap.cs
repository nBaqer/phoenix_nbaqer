﻿using FAME.Advantage.Domain.Student.Enrollments.Transactions;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments.Transactions
{
    public class TransactionCodeMap : ClassMap<Domain.Student.Enrollments.Transactions.TransactionCode>
    {
        public TransactionCodeMap()
        {
            Table("saTransCodes");


            Id(x => x.ID).Column("TransCodeId").Not.Nullable().GeneratedBy.Guid();
            Map(x => x.SysTransCodeId).Column("SysTransCodeId").Nullable();
            Map(x => x.Description).Column("TransCodeDescrip").Nullable().Length(50);
            References(x => x.Status).Column("StatusId").Nullable();
            



        }
    }
}
