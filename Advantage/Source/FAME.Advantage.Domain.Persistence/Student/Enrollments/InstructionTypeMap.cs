﻿using FAME.Advantage.Domain.Student.Enrollments;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    public class InstructionTypeMap: ClassMap<InstructionType>
    {
        public InstructionTypeMap()
        {
            Table("arInstructionType");

            Id(x => x.ID).Column("InstructionTypeId").GeneratedBy.Guid();
            Map(x => x.InstructionTypeDescrip).Column("InstructionTypeDescrip").Not.Nullable();
        }
    }
}
