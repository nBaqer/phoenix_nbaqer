﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArResultMap.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Map fro ArResult
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Student.Enrollments
{
    using FAME.Advantage.Domain.Student.Enrollments;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// Map from <code>ArResult</code>
    /// </summary>
    /// <remarks>
    /// This table is a full table, do not map as bridge!!! 
    /// </remarks>
    public class ArResultMap : ClassMap<ArResults>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArResultMap"/> class.
        /// </summary>
        public ArResultMap()
        {
            this.Table("ArResults");

            this.Id(x => x.ID).Column("ResultId").GeneratedBy.Guid();

            this.Map(x => x.Score).Column("Score").Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Nullable();

            this.References(x => x.StuEnrollment).Column("StuEnrollId").Not.Nullable().LazyLoad();
            this.References(x => x.ClassSections).Column("TestId").Nullable().LazyLoad();
            this.References(x => x.GradeSystemDetailsObj).Column("GrdSysDetailId").Nullable().LazyLoad();
        }
    }
}
