﻿using System.Collections.Generic;

namespace FAME.Advantage.Domain.Persistence.Infrastructure.Extensions
{
    public static class KeyValuePairExtensions
    {
        public static bool IsEmpty<K, V>(this KeyValuePair<K, V> keyValuePair) where K : class where V : class
        {
            return keyValuePair.Key == null && keyValuePair.Value == null;
        }
    }
}
