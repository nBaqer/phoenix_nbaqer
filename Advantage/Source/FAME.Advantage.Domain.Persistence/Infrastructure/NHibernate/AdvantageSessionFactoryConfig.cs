﻿
using FluentNHibernate.Cfg.Db;

namespace FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate
{
    public sealed class AdvantageSessionFactoryConfig : BaseSessionFactoryConfig<AdvantageSessionFactoryConfig>
    {
        private const string CONNECTION_STRING_NAME = "TenantAuthDBConnection";

        public AdvantageSessionFactoryConfig() : base(CONNECTION_STRING_NAME, "web")
        {
        }

        protected override MsSqlConfiguration GetMsSqlConfiguration()
        {
            return  base.GetMsSqlConfiguration().Provider<DynamicConnectionProvider.DynamicConnectionProvider>();
        }
    }
}
