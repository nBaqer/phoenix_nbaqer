﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseSessionFactoryConfig.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Defines the BaseSessionFactoryConfig type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate
{
    using System;

    using FluentNHibernate.Cfg;
    using FluentNHibernate.Cfg.Db;

    using global::NHibernate;

    using Server.Domain.Infrastructure.NHibernate;

    using Environment = global::NHibernate.Cfg.Environment;

    public abstract class BaseSessionFactoryConfig<T>
    {
        /// <summary>
        /// The connection string.
        /// </summary>
        private readonly string connectionString;

        /// <summary>
        /// TODO The _session context.
        /// </summary>
        private readonly string sessionContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseSessionFactoryConfig{T}"/> class.
        /// </summary>
        /// <param name="connectionString">
        ///  The connection string.
        /// </param>
        /// <param name="sessionContext">
        ///  The session context.
        /// </param>
        /// <exception cref="ArgumentException">
        /// If session context or connection string are null or empty
        /// </exception>
        protected BaseSessionFactoryConfig(string connectionString, string sessionContext)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("connectionString cannot be null or empty");
            }

            if (string.IsNullOrEmpty(sessionContext))
            {
                throw new ArgumentException("sessionContext cannot be null or empty");
            }

            this.connectionString = connectionString;
            this.sessionContext = sessionContext;
        }

        protected virtual MsSqlConfiguration GetMsSqlConfiguration()
        {
            return MsSqlConfiguration.MsSql2005
                .ConnectionString(b => b.FromConnectionStringWithKey(this.connectionString));
        }

        public ISessionFactory CreateSessionFactory()
        {
            try
            {
                var configuration = Fluently.Configure()
                               .Database(this.GetMsSqlConfiguration())
                               .ExposeConfiguration(config =>
                                config.SetProperty(
                                        Environment.SqlExceptionConverter,
                                        typeof(SqlExceptionConverter).AssemblyQualifiedName))
                                .CurrentSessionContext(this.sessionContext)
                                .Mappings(m => m
                                   .FluentMappings.AddFromAssemblyOf<T>().Conventions.Add<TreatStringAsSqlAnsiStringConvention>())
                                .BuildSessionFactory();
                return configuration;
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                {
                    throw e.InnerException;
                }

                throw;
            }
        }
    }
}
