﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate
{
    public sealed class TreatStringAsSqlAnsiStringConvention : IPropertyConvention
    {
        public void Apply(IPropertyInstance instance)
        {
            if (instance.Property.PropertyType == typeof(string))
                instance.CustomType("AnsiString");
        }
    }
}