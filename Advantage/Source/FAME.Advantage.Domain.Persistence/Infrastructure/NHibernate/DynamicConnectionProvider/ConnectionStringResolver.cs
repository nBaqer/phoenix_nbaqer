﻿using System;

namespace FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate.DynamicConnectionProvider
{
    public class ConnectionStringResolver
    {
        private readonly string _connectionString;

        public ConnectionStringResolver(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString)) throw new ArgumentException("connectionString cannot be null or empty");
            _connectionString = connectionString;
        }

        internal string GetConnectionString()
        {
            return _connectionString;
        }
    }
}
