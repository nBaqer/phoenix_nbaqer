﻿using Microsoft.Practices.ServiceLocation;
using NHibernate.Connection;

namespace FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate.DynamicConnectionProvider
{
    internal class DynamicConnectionProvider : DriverConnectionProvider
    {
        protected override string ConnectionString
        {
            get
            {
                var connectionStringResolver = ServiceLocator.Current.GetInstance<ConnectionStringResolver>();
                var connectionString = connectionStringResolver.GetConnectionString();
                return connectionString;
            }
        }
    }
}
