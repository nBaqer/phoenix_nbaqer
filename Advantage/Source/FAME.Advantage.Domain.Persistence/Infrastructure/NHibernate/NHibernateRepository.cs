﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NHibernateRepository.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the NHibernateRepository type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FAME.Advantage.Domain.Infrastructure.Entities;
    using FAME.Advantage.Domain.Persistence.Infrastructure.UoW;

    using FluentNHibernate.Utils;

    using global::NHibernate;

    using global::NHibernate.Linq;

    /// <summary>
    ///  The n hibernate repository.
    /// </summary>
    public class NHibernateRepository : NHibernateRepository<Guid>, IRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NHibernateRepository"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        ///  The unit of work.
        /// </param>
        public NHibernateRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }
    }

    /// <summary>
    /// The n-hibernate repository of GUID.
    /// </summary>
    public class NHibernateRepositoryOfGuid : NHibernateRepository<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NHibernateRepositoryOfGuid"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        public NHibernateRepositoryOfGuid(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }
    }

    /// <summary>
    /// The n hibernate repository.
    /// </summary>
    /// <typeparam name="TId">
    /// </typeparam>
    public class NHibernateRepository<TId> : IRepositoryWithTypedID<TId>
    {
        private readonly IUnitOfWork _unitOfWork;

        public NHibernateRepository(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Gets the session.
        /// </summary>
        public ISession Session
        {
            get
            {
                return this._unitOfWork.CurrentSession;
            }
        }

        /// <summary>
        ///  The save all.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        /// <typeparam name="TEntity">
        /// The type parameter to be created
        /// </typeparam>
        public void SaveAll<TEntity>(IEnumerable<DomainEntityWithTypedID<TId>> entities)
            where TEntity : DomainEntityWithTypedID<TId>
        {
            entities.Each(this.Save);
        }

        /// <summary>
        ///  The update all without flush.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        public void UpdateAll(IEnumerable<DomainEntityWithTypedID<TId>> entities)
        {
            entities.Each(this.Update);
        }

        /// <summary>
        /// Create to save only (insert) JAGG
        /// </summary>
        /// <typeparam name="TEntity">type parameter to be used</typeparam>
        /// <param name="entity">the entity to insert</param>
        public void SaveOnly<TEntity>(TEntity entity) where TEntity : DomainEntityWithTypedID<TId>
        {
            this.Session.Save(entity);
        }

        public void Save<TEntity>(TEntity entity) where TEntity : DomainEntityWithTypedID<TId>
        {
            this.Session.SaveOrUpdate(entity);
        }

        public void SaveAndFlush<TEntity>(TEntity entity) where TEntity : DomainEntityWithTypedID<TId>
        {
            this.Session.SaveOrUpdate(entity);
            this.Session.Flush();
        }

        public void Merge<TEntity>(TEntity entity) where TEntity : DomainEntityWithTypedID<TId>
        {
            this.Session.Merge(entity);
        }

        public void MergeAndFlush<TEntity>(TEntity entity) where TEntity : DomainEntityWithTypedID<TId>
        {
            this.Session.Merge(entity);
            this.Session.Flush();
        }

        public void Evict<TEntity>(TEntity entity) where TEntity : DomainEntityWithTypedID<TId>
        {
            this.Session.Evict(entity);
        }

        public TEntity Get<TEntity>(TId id) where TEntity : DomainEntityWithTypedID<TId>
        {
            return this.Session.Get<TEntity>(id);
        }

        public TEntity Load<TEntity>(TId id) where TEntity : DomainEntityWithTypedID<TId>
        {
            return this.Session.Load<TEntity>(id);
        }

        public void Delete<TEntity>(TEntity entity) where TEntity : DomainEntityWithTypedID<TId>
        {
            this.Session.Delete(entity);
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : DomainEntityWithTypedID<TId>
        {
            return this.Session.Query<TEntity>();
        }

        public IQueryable<TEntity> Query<TEntity>(IDomainQueryWithTypedID<TEntity, TId> whereQuery)
            where TEntity : DomainEntityWithTypedID<TId>
        {
            return this.Session.Query<TEntity>().Where(whereQuery.Expression);
        }

        public IQueryable<TEntity> Evict<TEntity>(IQueryable<TEntity> queryable)
            where TEntity : DomainEntityWithTypedID<TId>
        {
            queryable.Each(i => this.Session.Evict(i));
            return queryable;
        }

        /// <summary>
        /// Update a TEntity
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        public void Update<TEntity>(TEntity entity) where TEntity : DomainEntityWithTypedID<TId>
        {
            this.Session.Update(entity);
        }

        /// <summary>
        /// Update a TEntity and also flush the session
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        public void UpdateAndFlush<TEntity>(TEntity entity) where TEntity : DomainEntityWithTypedID<TId>
        {
            this.Session.Update(entity);
            this.Session.Flush();
        }
    }
}
