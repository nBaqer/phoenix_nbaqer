﻿using System;
using System.Data;

namespace FAME.Advantage.Domain.Persistence.Infrastructure.UoW
{
    public interface IUnitOfWorkScope : IDisposable
    {
        void Complete();
    }

    public class UnitOfWorkScope : IUnitOfWorkScope
    {
        private readonly IUnitOfWork _unitOfWork;

        public UnitOfWorkScope(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _unitOfWork.Start();
        }

        public void Complete()
        {
            _unitOfWork.Flush();
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }

    public class TransactionalUnitOfWorkScope : IUnitOfWorkScope
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUnitOfWorkTransaction _transaction;

        public TransactionalUnitOfWorkScope(IUnitOfWork unitOfWork)
            : this(unitOfWork, new UnitOfWorkTransactionOptions { IsolationLevel = IsolationLevel.Serializable }) { }

        public TransactionalUnitOfWorkScope(
            IUnitOfWork unitOfWork,
            UnitOfWorkTransactionOptions transactionOptions)
        {
            _unitOfWork = unitOfWork;
            _unitOfWork.Start();
            _transaction = new UnitOfWorkTransaction(_unitOfWork, transactionOptions);
        }

        public bool WasRolledBack
        {
            get { return _transaction.WasRolledBack; }
        }

        public void Complete()
        {
            _unitOfWork.Flush();
            _transaction.Commit();
        }

        public void Dispose()
        {
            _transaction.Dispose();
            _unitOfWork.Dispose();
        }
    }
}
