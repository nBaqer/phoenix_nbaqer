﻿using System;
using NHibernate;

namespace FAME.Advantage.Domain.Persistence.Infrastructure.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        void Start();

        void Flush();

        void Clear();

        ISession CurrentSession { get; }

        /// <summary>
        /// Creates a non transactional scope for an uninitialized IUnitOfWork
        /// </summary>
        /// <returns></returns>
        IUnitOfWorkScope CreateScope();

        /// <summary>
        /// Creates a transactional scope for an uninitialized IUnitOfWork
        /// </summary>
        /// <returns></returns>
        IUnitOfWorkScope CreateTransactionalScope();

        /// <summary>
        /// Creates a transactional scope for an uninitialized IUnitOfWork
        /// </summary>
        /// <returns></returns>
        IUnitOfWorkScope CreateTransactionalScope(UnitOfWorkTransactionOptions options);
    }

    [Serializable]
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ISessionFactory _factory;

        public ISession CurrentSession { get; private set; }

        public IUnitOfWorkScope CreateScope()
        {
            return new UnitOfWorkScope(this);
        }

        public IUnitOfWorkScope CreateTransactionalScope()
        {
            return CreateTransactionalScope(UnitOfWorkTransactionOptions.Default());
        }

        public IUnitOfWorkScope CreateTransactionalScope(UnitOfWorkTransactionOptions options)
        {
            return new TransactionalUnitOfWorkScope(this, options);
        }

        public UnitOfWork(ISessionFactory factory)
        {
            _factory = factory;
        }

        public void Start()
        {
            AssertInValidState();

            CurrentSession = _factory.OpenSession();
        }

        private void AssertInValidState()
        {
            if (CurrentSession != null) {
                Dispose();
                throw new ObjectDisposedException("Previous session was not properly disposed.");
            }
        }

        public void Flush()
        {
            CurrentSession.Flush();
        }

        public void Clear()
        {
            CurrentSession.Clear();
        }

        public void Dispose()
        {
            if (CurrentSession != null) {
                CurrentSession.Dispose();
                CurrentSession = null;
            }
        }
    }
}
