﻿using System;
using NHibernate;

namespace FAME.Advantage.Domain.Persistence.Infrastructure.UoW
{
    public interface IUnitOfWorkTransaction : IDisposable
    {
        void Begin();

        void Commit();

        void Rollback();

        bool WasRolledBack { get; }
    }

    public class UnitOfWorkTransaction : IUnitOfWorkTransaction
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UnitOfWorkTransactionOptions _options;
        private ITransaction _transaction;

        public UnitOfWorkTransaction(IUnitOfWork unitOfWork, UnitOfWorkTransactionOptions options)
        {
            _unitOfWork = unitOfWork;
            _options = options;
            Begin();
        }

        public void Begin()
        {
            _transaction = _unitOfWork.CurrentSession.BeginTransaction(_options.IsolationLevel);
        }

        public void Commit()
        {
            if (!_transaction.WasCommitted) {
                _transaction.Commit();
            }
        }

        public void Rollback()
        {
            if (!_transaction.WasRolledBack) {
                _transaction.Rollback();
            }
        }

        public void Dispose()
        {
            _transaction.Dispose();
        }

        public bool WasRolledBack
        {
            get { return _transaction.WasRolledBack; }
        }
    }
}
