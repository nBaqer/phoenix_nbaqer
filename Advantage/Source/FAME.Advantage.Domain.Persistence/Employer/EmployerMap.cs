﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Employer
{
    public class EmployerMap : ClassMap<Domain.Employer.Employer>
    {
        public EmployerMap()
        {
            Table("plEmployers");

            Id(e => e.ID).Column("EmployerId").GeneratedBy.Guid();

            Map(e => e.EmployerName).Column("EmployerDescrip").Length(50).Not.Nullable();
            Map(e => e.Address).Column("Address1").Length(80).Nullable();
            Map(e => e.City).Column("City").Length(80).Not.Nullable();
            References(x => x.State).Column("StateId").Not.LazyLoad();
            Map(e => e.Zip).Column("Zip").Length(50).Not.Nullable();
            Map(e => e.Phone).Column("Phone").Length(50).Not.Nullable();
            Map(e => e.ModDate).Column("ModDate").Nullable();
            Map(e => e.ModUser).Column("modUser").Length(50).Nullable();

            References(x => x.CampusGroup).Column("CampGrpId").Not.LazyLoad();

            References(x => x.Industry).Column("IndustryId").Not.LazyLoad();
            
        }
    }
}
