﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Employer
{
    public class IndustryMap : ClassMap<Domain.Employer.Industry>
    {
        public IndustryMap()
        {
            Table("plIndustries");

            Id(e => e.ID).Column("IndustryId").GeneratedBy.Guid();

            Map(e => e.Code).Column("IndustryCode").Length(12).Not.Nullable();
            Map(e => e.Description).Column("IndustryDescrip").Length(50).Not.Nullable();
            References(e=>e.CampusGroup).Column("CampGrpId").LazyLoad();

        }
    }
}
