﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
// </copyright>
// <summary>
//   The campus map for table <code>syCampuses</code> with domain entity Campus.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Campuses
{
    using FAME.Advantage.Domain.Campuses;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The campus map for table <code>syCampuses</code> with domain entity Campus.
    /// </summary>
    public class CampusMap : ClassMap<Campus>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CampusMap"/> class.
        /// </summary>
        public CampusMap()
        {
            this.Table("syCampuses");
            this.ReadOnly();

            this.Id(x => x.ID).Column("CampusId").Not.Nullable().GeneratedBy.Guid();

            this.Map(x => x.Code).Column("CampCode").Not.Nullable().Length(10);
            this.Map(x => x.Description).Column("CampDescrip").Not.Nullable().Length(100);
            this.Map(x => x.Address1).Column("Address1").Nullable().Length(80);
            this.Map(x => x.Address2).Column("Address2").Nullable().Length(50);
            this.Map(x => x.City).Column("City").Nullable().Length(80);
            this.Map(x => x.StateId).Column("StateId").Nullable();
            this.Map(x => x.StatusId).Column("StatusId").Nullable();
            this.Map(x => x.Zip).Column("Zip").Nullable();
            this.Map(x => x.Token1098TService).Column("Token1098TService").Nullable();
            this.Map(x => x.SchoolCodeKissSchoolId).Column("SchoolCodeKissSchoolId").Nullable();
            this.Map(x => x.Phone1).Column("Phone1").Nullable();
            this.Map(x => x.Email).Column("Email").Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Nullable();
            this.Map(x => x.CmsId).Column("CmsId").Nullable();

            this.References(x => x.State).Column("StateId").Nullable().LazyLoad();
            this.References(x => x.Country).Column("CountryId").Nullable().LazyLoad();
            this.References(x => x.Status).Column("StatusId").Cascade.None();
  

            this.HasMany(x => x.EnrollmentsList).KeyColumn("CampusId");
            this.HasMany(x => x.MruList).KeyColumn("CampusId").Inverse().LazyLoad();

            this.HasManyToMany(x => x.CampusGroup).Cascade.All().Table("syCmpGrpCmps").ParentKeyColumn("CampusId").ChildKeyColumn("CampGrpId"); 
        }
    }
}
