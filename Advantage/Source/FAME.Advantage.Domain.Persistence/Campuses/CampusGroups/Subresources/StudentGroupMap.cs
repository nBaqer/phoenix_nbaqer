﻿using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Campuses.CampusGroups.Subresources
{
    public class StudentGroupMap : ClassMap<StudentGroup>
    {
        public StudentGroupMap()
        {
            Table("adLeadGroups");

            ReadOnly();

            Id(x => x.ID).Column("LeadGrpId").Not.Nullable().GeneratedBy.Guid();

            Map(x => x.Description).Column("Descrip").Not.Nullable().Length(50);
            Map(x => x.CampGrpId).Column("CampGrpId").Nullable();
            Map(x => x.Code).Column("Code").Nullable().Length(50);
            Map(x => x.UseForScheduling).Column("UseForScheduling").Nullable();

            References(x => x.Status).Column("StatusId");
            References(x => x.CampusGroup).Column("CampGrpId");

            HasMany(x => x.StudentGroupBridges).KeyColumn("LeadGrpId");
            HasMany(x => x.StudentGroupRequirementGroupBridge).KeyColumn("LeadGrpId");
            HasMany(x => x.RequirementLeadGroupBridge).KeyColumn("LeadGrpId");
        }
    }
}
