﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadGroupBridgeMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.Campuses.CampusGroups.Subresources.LeadGroupBridgeMap
// </copyright>
// <summary>
//   The lead group bridge map.
//   Mapping for Domain Entity LeadGroupBridge with table adLeadByLeadGroups
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Campuses.CampusGroups.Subresources
{
    using FAME.Advantage.Domain.Campuses.CampusGroups.Subresources;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The lead group bridge map.
    /// Mapping for Domain Entity LeadGroupBridge with table adLeadByLeadGroups
    /// </summary>
    public class LeadGroupBridgeMap : ClassMap<LeadGroupBridge>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadGroupBridgeMap"/> class.
        /// </summary>
        public LeadGroupBridgeMap()
        {
            this.Table("adLeadByLeadGroups");

            this.ReadOnly();

            this.Id(x => x.ID).Column("LeadGrpLeadId").Not.Nullable().GeneratedBy.Guid();

            this.Map(x => x.StudentGroupId).Column("LeadGrpId").Not.Nullable();
            this.Map(x => x.StudentEnrollmentId).Column("StuEnrollId").Not.Nullable();
            this.Map(x => x.LeadId).Column("LeadId").Not.Nullable();

            this.References(x => x.Lead).Not.Nullable().Cascade.SaveUpdate().Column("LeadId");
            this.References(x => x.LeadGroups).Not.Nullable().Cascade.SaveUpdate().Column("LeadGrpId");
            this.References(x => x.Enrollments).Not.Nullable().Cascade.SaveUpdate().Column("StuEnrollId");
        }
    }
}
