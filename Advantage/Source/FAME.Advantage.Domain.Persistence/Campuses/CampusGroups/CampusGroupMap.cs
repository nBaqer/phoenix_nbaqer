﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusGroupMap.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   The campus group map.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Campuses.CampusGroups
{
    using FAME.Advantage.Domain.Campuses.CampusGroups;

    using FluentNHibernate.Mapping;

    /// <summary>
    ///  The campus group map.
    /// </summary>
    public class CampusGroupMap : ClassMap<CampusGroup>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CampusGroupMap"/> class.
        /// </summary>
        public CampusGroupMap()
        {
            this.Table("syCampGrps");

            this.ReadOnly();

            this.Id(x => x.ID).Column("CampGrpId").Not.Nullable().GeneratedBy.Guid();

            this.Map(x => x.CampusGrpCode).Column("CampGrpCode").Not.Nullable().Length(12);
            this.Map(x => x.CampusGrpDescription).Column("CampGrpDescrip").Not.Nullable().Length(50);
            this.Map(x => x.StatusId).Column("StatusId").Not.Nullable();
            this.Map(x => x.IsAllCampusGrp).Column("IsAllCampusGrp").Nullable();
            this.Map(x => x.CampusId).Column("CampusId").Nullable();

            this.HasMany(x => x.ProgramVersions).KeyColumn("CampGrpId").LazyLoad();
            this.HasMany(x => x.EnrollmentStatuses).KeyColumn("CampGrpId").LazyLoad();
            this.HasMany(x => x.StudentGroups).KeyColumn("CampGrpId").LazyLoad();
            this.HasMany(x => x.HolidaysList).KeyColumn("CampGrpId").LazyLoad();
            this.HasMany(x => x.LeadGroupsList).KeyColumn("CampGrpId").LazyLoad();
            this.HasMany(x => x.AttendTypesList).KeyColumn("CampGrpId").Inverse().Cascade.None().LazyLoad();
            this.HasMany(x => x.ReasonNotEnrolledList).KeyColumn("CampGrpId").Cascade.DeleteOrphan().LazyLoad();

            this.HasManyToMany(x => x.Campuses).Cascade.All().Table("syCmpGrpCmps").ParentKeyColumn("CampGrpId").ChildKeyColumn("CampusId");
            this.HasManyToMany(x => x.ListUsers).Cascade.None().Inverse().Table("syUsersRolesCampGrps").ParentKeyColumn("CampGrpId").ChildKeyColumn("UserId");
        }
    }
}
