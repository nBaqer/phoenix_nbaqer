﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// Map for table adCountries
    /// </summary>
    public class CountryMap : ClassMap<Country>
    {
        /// <summary>
        /// MAP for Table adCountries
        ///  </summary>
        public CountryMap()
        {
            Table("adCountries");


            Id(l => l.ID).Column("CountryId").GeneratedBy.Guid();

            Map(l => l.CountryCode).Column("CountryCode").Length(12).Not.Nullable();
            Map(l => l.CountryDescrip).Column("CountryDescrip").Length(50).Not.Nullable();

            References(l => l.CampusGroupObj).Column("CampGrpId").Not.Nullable().LazyLoad();
            References(x => x.SyStatusesObj).Column("StatusId").Not.Nullable().LazyLoad();

            HasMany(x => x.LeadList).KeyColumn("CountryId").Inverse().Cascade.None().LazyLoad();
        }
    }
}
