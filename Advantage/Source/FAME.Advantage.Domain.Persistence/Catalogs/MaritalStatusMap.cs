﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// Table Map for adMaritalStatus
    /// </summary>
    public class MaritalStatusMap : ClassMap<MaritalStatus>
    {
        /// <summary>
        /// Marital Status Type Map of table adMaritalStatus
        /// </summary>
        public MaritalStatusMap()
        {
            Table("adMaritalStatus");

            Id(s => s.ID).Column("MaritalStatId").GeneratedBy.Guid();
            Map(x => x.MaritalStatCode).Column("MaritalStatCode").Length(12).Not.Nullable();
            Map(x => x.MaritalStatDescrip).Column("MaritalStatDescrip").Length(50).Nullable();

            References(x => x.SyStatusesObj).Column("StatusId").LazyLoad();
            HasMany(x => x.LeadList).KeyColumn("MaritalStatId").Inverse().Cascade.None().LazyLoad();
        }
    }
}
