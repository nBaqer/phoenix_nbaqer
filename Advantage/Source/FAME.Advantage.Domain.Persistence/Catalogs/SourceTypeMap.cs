﻿using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// Mapping for adSourceType
    /// </summary>
    public class SourceTypeMap: ClassMap<Domain.Catalogs.SourceType>
    {
        /// <summary>
        /// Source Category Type Table map adSourceType
        /// </summary>
        public SourceTypeMap()
        {
            Table("adSourceType");

            Id(l => l.ID).Column("SourceTypeId").GeneratedBy.Guid();

            Map(l => l.SourceTypeCode).Column("SourceTypeCode").Length(50).Not.Nullable();
            Map(l => l.SourceTypeDescrip).Column("SourceTypeDescrip").Length(50).Not.Nullable();
            
            References(x => x.SyStatusesObj).Column("StatusID").LazyLoad();
            References(x => x.CampusGroupObj).Column("CampGrpId").LazyLoad();
            References(x => x.SourceCategoryObj).Column("SourceCatagoryId").LazyLoad();
            
            HasMany(x => x.LeadList).Inverse().Cascade.None().LazyLoad();
        }
    }
}
