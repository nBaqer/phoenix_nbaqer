﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    public class SuffixesMap : ClassMap<Suffixes>
    {
        public SuffixesMap()
        {
            Table("sySuffixes");

            Id(l => l.ID).Column("SuffixId").GeneratedBy.Guid();

            Map(l => l.SuffixCode).Column("SuffixCode").Length(12).Nullable();
            Map(l => l.SuffixDescrip).Column("SuffixDescrip").Length(50).Not.Nullable();
            
            References(x => x.SyStatusesObj).Column("StatusId").LazyLoad();

            HasMany(x => x.LeadList).Inverse().Cascade.None().LazyLoad();
        }
    }
}
