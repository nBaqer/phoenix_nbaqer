﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyAcademicCalendarsMap.cs" company="FAME Inc">
//   2016
// </copyright>
// <summary>
//   Defines the SyAcademicCalendarsMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    using FAME.Advantage.Domain.Catalogs;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The academic calendars map.
    /// </summary>
    public class SyAcademicCalendarsMap : ClassMap<SyAcademicCalendars>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyAcademicCalendarsMap"/> class.
        /// </summary>
        public SyAcademicCalendarsMap()
        {
           this.Table("syAcademicCalendars");

            this.Id(x => x.ID).Column("ACId").Not.Nullable();
            this.Map(x => x.Descrip).Column("ACDescrip").Nullable().Length(50);
        }
    }
}
