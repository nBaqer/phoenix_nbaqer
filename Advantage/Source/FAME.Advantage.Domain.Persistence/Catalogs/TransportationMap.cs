﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// Transportation Map of table plTransportation
    /// </summary>
    public class TransportationMap : ClassMap<Transportation>
    {
        /// <summary>
        /// Transportation Map of table plTransportation
        /// </summary>
        public TransportationMap()
        {
            Table("plTransportation");

            Id(s => s.ID).Column("TransportationId").GeneratedBy.Guid();
            Map(x => x.TransportationCode).Column("TransportationCode").Length(12).Not.Nullable();
            Map(x => x.TransportationDescrip).Column("TransportationDescrip").Length(50).Not.Nullable();

            References(x => x.SyStatusesObj).Column("StatusId").LazyLoad();
            References(x => x.CampusGroupObj).Column("CampGrpId").LazyLoad();

            HasMany(x => x.LeadList).KeyColumn("TransportationId").Inverse().Cascade.None().LazyLoad();

        }
    }
}
