﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    ///  MAP for Table arPrgGrp
    /// </summary>
    public class ArProgramGroupMap : ClassMap<ArProgramGroup>
    {
        /// <summary>
        /// MAP for Table arPrgGrp
        /// </summary>
        public ArProgramGroupMap()
        {
            Table("ArPrgGrp");

            Id(l => l.ID).Column("PrgGrpId").GeneratedBy.Guid();

            Map(l => l.PrgGrpCode).Column("PrgGrpCode").Length(12).Not.Nullable();
            Map(l => l.PrgGrpDescrip).Column("PrgGrpDescrip").Length(50).Not.Nullable();
            
            References(x => x.SyStatusesObj).Column("StatusId").LazyLoad();
            References(x => x.CampusGroupObj).Column("CampGrpId").Nullable().LazyLoad();

            HasMany(x => x.LeadList).KeyColumn("PrgGrpId").Cascade.None().LazyLoad();
            HasMany(x => x.ProgramVersionList).KeyColumn("PrgGrpId").Cascade.None().LazyLoad();
        }
    }
}
