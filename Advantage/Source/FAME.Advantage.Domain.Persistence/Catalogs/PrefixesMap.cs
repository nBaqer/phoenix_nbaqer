﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PrefixesMap.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   MAP to syPrefixes
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    using FAME.Advantage.Domain.Catalogs;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// MAP to <code>syPrefixes</code>
    /// </summary>
    public class PrefixesMap : ClassMap<Prefixes>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrefixesMap"/> class. 
        /// MAP to <code>syPrefixes</code>
        /// </summary>
        public PrefixesMap()
        {
            this.Table("syPrefixes");

            this.Id(l => l.ID).Column("PrefixId").GeneratedBy.Guid();

            this.Map(l => l.PrefixCode).Column("PrefixCode").Length(12).Nullable();
            this.Map(l => l.PrefixDescrip).Column("PrefixDescrip").Length(50).Not.Nullable();

            this.References(x => x.CampusGroupObject).Column("CampGrpId").Not.Nullable().LazyLoad();
            this.References(x => x.SyStatusesObj).Column("StatusId").LazyLoad();
            this.HasMany(x => x.LeadList).Inverse().Cascade.None().LazyLoad();
        }
    }
}
