﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// The map for table AdDependencyTypes
    /// </summary>
    public class DependencyTypeMap : ClassMap<DependencyType>
    {
        /// <summary>
        /// Map Definitions
        /// </summary>
        public DependencyTypeMap()
        {
            Table("AdDependencyTypes");

            Id(l => l.ID).Column("DependencyTypeId").GeneratedBy.Guid();

            Map(l => l.Code).Column("Code").Length(50).Nullable();
            Map(l => l.Descrip).Column("Descrip").Length(50).Nullable();
            
            References(l => l.SyStatusesObj).Column("StatusId").Not.Nullable().LazyLoad();
            HasMany(l => l.LeadList).KeyColumn("DependencyTypeId").LazyLoad();
        }
    }
}
