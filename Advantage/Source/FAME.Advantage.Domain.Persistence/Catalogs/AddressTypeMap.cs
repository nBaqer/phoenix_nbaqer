﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// MAP for table plAddressTypes and Domain AddressType
    /// </summary>
    public class AddressTypeMap : ClassMap<AddressType>
    {
        /// <summary>
        /// MAP for Table plAddressTypes
        ///  </summary>
        public AddressTypeMap()
        {
            Table("plAddressTypes");


            Id(l => l.ID).Column("AddressTypeId").GeneratedBy.Guid();

            Map(l => l.AddressCode).Column("AddressCode").Length(12).Nullable();
            Map(l => l.AddessDescrip).Column("AddressDescrip").Length(50).Nullable();

            References(l => l.CampusGroupObj).Column("CampGrpId").Nullable().LazyLoad();
            References(x => x.SyStatusesObj).Column("StatusId").Nullable().LazyLoad();

            HasMany(x => x.LeadList).KeyColumn("AddressTypeId").Inverse().Cascade.None().LazyLoad();
        }
    }
}
