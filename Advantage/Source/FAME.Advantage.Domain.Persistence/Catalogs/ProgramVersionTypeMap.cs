﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersionTypeMap.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the ProgramVersionTypeMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    using FAME.Advantage.Domain.Catalogs;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The program version type map.
    /// </summary>
    public class ProgramVersionTypeMap : ClassMap<ProgramVersionType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramVersionTypeMap"/> class. 
        /// </summary>
        public ProgramVersionTypeMap()
        {
            this.Table("arProgramVersionType");
            this.Id(p => p.ID).Column("ProgramVersionTypeId").Not.Nullable();
            this.Map(p => p.Descrip).Column("DescriptionProgramVersionType").Not.Nullable();
            this.HasMany(x => x.EnrollList).KeyColumn("ProgramVersionTypeId").Inverse().Cascade.None().LazyLoad();
        }
    }
}
