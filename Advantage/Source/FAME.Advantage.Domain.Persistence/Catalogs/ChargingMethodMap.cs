﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ChargingMethodMap.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the ChargingMethodMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    using FAME.Advantage.Domain.Catalogs;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The charging method map.
    /// </summary>
    public class ChargingMethodMap : ClassMap<ChargingMethod>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChargingMethodMap"/> class.
        /// </summary>
        public ChargingMethodMap()
        {
            this.Table("saBillingMethods");

            this.Id(l => l.ID).Column("BillingMethodId").GeneratedBy.Guid();

            this.Map(l => l.Descrip).Column("BillingMethodDescrip").Length(50).Not.Nullable();
            this.Map(l => l.BillingMethod).Column("BillingMethod").Not.Nullable();
            this.References(l => l.CampusGroupObj).Column("CampGrpId").Not.Nullable().LazyLoad();
            this.References(x => x.SyStatusesObj).Column("StatusId").Not.Nullable().LazyLoad();

            this.HasMany(x => x.LeadList).KeyColumn("BillingMethodId").Inverse().Cascade.None().LazyLoad();
        }
    }
}
