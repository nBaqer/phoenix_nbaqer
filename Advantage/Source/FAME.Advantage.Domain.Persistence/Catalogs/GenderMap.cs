﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// Genders
    /// </summary>
    public class GenderMap : ClassMap<Gender>
    {
        /// <summary>
        /// Genders Map of table adGenders
        /// </summary>
        public GenderMap()
        {
            Table("adGenders");

            Id(s => s.ID).Column("GenderId").GeneratedBy.Guid();
            Map(x => x.Code).Column("GenderCode").Length(12).Not.Nullable();
            Map(x => x.Description).Column("GenderDescrip").Length(50).Not.Nullable();
            References(x => x.CampusGrp).Column("CampGrpId").Nullable().LazyLoad();
            References(x => x.SyStatusesObj).Column("StatusId").LazyLoad();
            HasMany(x => x.LeadList).KeyColumn("GenderId").Inverse().Cascade.None().LazyLoad();

        }
    }
}
