﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// Mapping Table arHousing
    /// </summary>
    public class HousingTypeMap : ClassMap<HousingType>
    {
        /// <summary>
        /// Housing Type Map of table arHousing
        /// </summary>
        public HousingTypeMap()
        {
            Table("arHousing");

            Id(s => s.ID).Column("HousingId").GeneratedBy.Guid();
            Map(x => x.Code).Column("Code").Length(50).Nullable();
            Map(x => x.Descrip).Column("Descrip").Length(50).Nullable();

            References(x => x.SyStatusesObj).Column("StatusId").LazyLoad();
            HasMany(x => x.LeadList).KeyColumn("HousingId").Inverse().Cascade.None().LazyLoad();
        }
    }
}
