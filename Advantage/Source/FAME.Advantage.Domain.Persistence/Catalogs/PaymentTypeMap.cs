﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PaymentTypeMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.Catalogs.PaymentTypeMap
// </copyright>
// <summary>
//   The payment type.
//   Domain Entity for Table <code>saPaymentTypes</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    using Domain.Catalogs;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// The payment type.
    /// Domain Entity for Table <code>saPaymentTypes</code>
    /// </summary>
    public class PaymentTypeMap : ClassMap<PaymentType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentTypeMap"/> class.
        /// </summary>
        public PaymentTypeMap()
        {
            this.Table("saPaymentTypes");
            this.Id(l => l.ID).Column("PaymentTypeId").GeneratedBy.Identity();
            this.Map(l => l.Description).Column("Description").Length(50).Not.Nullable();
        }
    }
}
