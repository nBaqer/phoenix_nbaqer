﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NationalitiesMap.cs" company="FMAE INC">
//   2016
// </copyright>
// <summary>
//   Defines the NationalitiesMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    using FAME.Advantage.Domain.Catalogs;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The mapping of nationality table.
    /// </summary>
    public class NationalitiesMap : ClassMap<Nationalities>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NationalitiesMap"/> class. 
        /// map for all the columns in this table
        /// </summary>
        public NationalitiesMap()
        {
            this.Table("adNationalities");

            this.Id(l => l.ID).Column("NationalityId").GeneratedBy.Guid();

            //this.Map(l => l.NationalityCode).Column("NationalityCode").Length(12).Not.Nullable();
            this.Map(l => l.Descrip).Column("NationalityDescrip").Length(50).Not.Nullable();

            this.References(l => l.CampusGroupObj).Column("CampGrpId").Not.Nullable().LazyLoad();
            this.References(x => x.SyStatusesObj).Column("StatusId").Not.Nullable().LazyLoad();

            this.HasMany(x => x.LeadList).KeyColumn("NationalityId").Inverse().Cascade.None().LazyLoad();
        }
    }
}
