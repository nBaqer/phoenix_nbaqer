﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ShiftsMap.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the ShiftsMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    using FAME.Advantage.Domain.Catalogs;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The shifts map.
    /// </summary>
    public class ShiftsMap : ClassMap<Shifts>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShiftsMap"/> class.
        /// </summary>
        public ShiftsMap()
        {
            this.Table("arShifts");

            this.Id(l => l.ID).Column("ShiftId").GeneratedBy.Guid();

            //this.Map(l => l.ShiftCode).Column("ShiftCode").Length(12).Not.Nullable();
            this.Map(l => l.Descrip).Column("ShiftDescrip").Length(50).Not.Nullable();

            this.References(l => l.CampusGroupObj).Column("CampGrpId").Not.Nullable().LazyLoad();
            this.References(x => x.SyStatusesObj).Column("StatusId").Not.Nullable().LazyLoad();

            this.HasMany(x => x.LeadList).KeyColumn("ShiftId").Inverse().Cascade.None().LazyLoad();
        }
    }
}
