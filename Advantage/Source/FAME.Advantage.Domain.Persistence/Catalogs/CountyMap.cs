﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// Map for Table adCounties
    /// </summary>
    public class CountyMap : ClassMap<County>
    {
        /// <summary>
        /// MAP for Table adCounties
        ///  </summary>
        public CountyMap()
        {
            Table("adCounties");


            Id(l => l.ID).Column("CountyId").GeneratedBy.Guid();

            Map(l => l.CountyCode).Column("CountyCode").Length(12).Not.Nullable();
            Map(l => l.CountyDescrip).Column("CountyDescrip").Length(50).Not.Nullable();

            References(l => l.CampusGroupObj).Column("CampGrpId").Not.Nullable().LazyLoad();
            References(x => x.SyStatusesObj).Column("StatusId").Not.Nullable().LazyLoad();

            HasMany(x => x.LeadList).KeyColumn("CountyId").Inverse().Cascade.None().LazyLoad();
        }
    }
}
