﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// MAP for Table arProgScheduleDetails
    /// </summary>
    public class ProgramScheduleDetailsMap : ClassMap<ProgramScheduleDetails>
    {
        /// <summary>
        /// MAP for Table arProgScheduleDetails
        /// </summary>
        public ProgramScheduleDetailsMap()
        {
            Table("arProgScheduleDetails");

            Id(l => l.ID).Column("ScheduleDetailId").GeneratedBy.Guid();

            Map(l => l.Dw).Column("dw").Nullable();
            Map(l => l.Total).Column("total").Nullable();
            Map(l => l.TimeIn).Column("timein").Nullable();
            Map(l => l.LunchIn).Column("lunchin").Nullable();
            Map(l => l.LunchOut).Column("lunchout").Nullable();
            Map(l => l.TimeOut).Column("timeout").Nullable();
            Map(l => l.MaxNoLunch).Column("maxnolunch").Nullable();
            Map(l => l.AllowEarlyIn).Column("allow_earlyin").Nullable();
            Map(l => l.AllowLateOut).Column("allow_lateout").Nullable();
            Map(l => l.AllowExtraHours).Column("allow_extrahours").Nullable();
            Map(l => l.CheckTardyIn).Column("check_tardyin").Nullable();
            Map(l => l.MaxBeforeTardy).Column("max_beforetardy").Nullable();
            Map(l => l.TardyInTime).Column("tardy_intime").Nullable();
            Map(l => l.MinimumHoursToBePresent).Column("minimumHourstobepresent").Nullable();

            References(x => x.ProgramScheduleObj).Column("ScheduleId").LazyLoad();;
        }
    }
}
