﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NotesPageFieldsMap.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   MAp for Table syNotesPageFields
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    using Domain.Catalogs;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Map for Table <code>syNotesPageFields</code>
    /// </summary>
    public class NotesPageFieldsMap : ClassMap<NotesPageFields>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotesPageFieldsMap"/> class. 
        /// MAP for Table <code>syNotesPageFields</code>
        /// </summary>
        public NotesPageFieldsMap()
        {
            this.Table("syNotesPageFields");

            this.ReadOnly();

            this.Id(l => l.ID).Column("PageFieldId").GeneratedBy.Identity();

            this.Map(l => l.PageName).Column("PageName").Length(50).Not.Nullable();
            this.Map(l => l.FieldCaption).Column("FieldCaption").Length(50).Not.Nullable();

            this.HasMany(x => x.AllNotesList).KeyColumn("PageFieldId").Inverse().Cascade.None().LazyLoad();
        }
    }
}
