﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// MAP for Table arProgSchedules
    /// </summary>
    public class ProgramScheduleMap : ClassMap<ProgramSchedule>
    {
        /// <summary>
        /// MAP for Table arProgSchedules
        /// </summary>
        public ProgramScheduleMap()
        {
            Table("arProgSchedules");

            Id(l => l.ID).Column("ScheduleId").GeneratedBy.Guid();

            Map(l => l.Code).Column("Code").Length(12).Nullable();
            Map(l => l.Descrip).Column("Descrip").Length(50).Nullable();
            Map(l => l.Active).Column("Active").Nullable();
            
            References(x => x.ProgramVersionObj).Column("PrgVerID").LazyLoad();

            HasMany(x => x.LeadList).KeyColumn("ScheduleId").Cascade.None().LazyLoad();
        }
    }
}
