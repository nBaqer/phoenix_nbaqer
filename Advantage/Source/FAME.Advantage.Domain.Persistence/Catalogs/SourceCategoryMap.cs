﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// Map for adSourceCatagory
    /// </summary>
    public class SourceCategoryMap : ClassMap<SourceCategory>
    {
        /// <summary>
        /// Source Category Table map adSourceCatagory
        /// </summary>
        public SourceCategoryMap()
        {
            Table("adSourceCatagory");

            Id(l => l.ID).Column("SourceCatagoryId").GeneratedBy.Guid();

            Map(l => l.SourceCatagoryCode).Column("SourceCatagoryCode").Length(50).Not.Nullable();
            Map(l => l.SourceCatagoryDescrip).Column("SourceCatagoryDescrip").Length(50).Not.Nullable();
            
            References(x => x.SyStatusesObj).Column("StatusID").LazyLoad();
            References(x => x.CampusGroupObj).Column("CampGrpId").LazyLoad();
            
            HasMany(x => x.LeadList).Inverse().Cascade.None().LazyLoad();
        }
    }
}
