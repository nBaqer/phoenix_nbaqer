﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GeographicTypesMap.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the GeographicTypesMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    using FAME.Advantage.Domain.Catalogs;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The geographic types map.
    /// </summary>
    public class GeographicTypesMap : ClassMap<GeographicTypes>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GeographicTypesMap"/> class.
        /// </summary>
        public GeographicTypesMap()
        {
            this.Table("adGeographicTypes");

            this.Id(l => l.ID).Column("GeographicTypeId").GeneratedBy.Guid();

            //this.Map(l => l.Code).Column("Code").Length(12).Not.Nullable();
            this.Map(l => l.Descrip).Column("Descrip").Length(50).Not.Nullable();

            this.References(l => l.CampusGroupObj).Column("CampGrpId").Not.Nullable().LazyLoad();
            this.References(x => x.SyStatusesObj).Column("StatusId").Not.Nullable().LazyLoad();

            this.HasMany(x => x.LeadList).KeyColumn("GeographicTypeId").Inverse().Cascade.None().LazyLoad();
        }
    }
}
