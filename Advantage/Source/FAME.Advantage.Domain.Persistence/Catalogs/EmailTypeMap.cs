﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// MAP to syEmailType
    /// </summary>
    public class EmailTypeMap: ClassMap<EmailType>
    {
        /// <summary>
        /// MAP to syEmailType
        /// </summary>
        public EmailTypeMap()
        {
            Table("syEmailType");

            Id(l => l.ID).Column("EmailTypeId").GeneratedBy.Guid();

            Map(l => l.EmailTypeCode).Column("EmailTypeCode").Length(50).Not.Nullable();
            Map(l => l.EmailTypeDescrip).Column("EmailTypeDescription").Length(100).Not.Nullable();
         
            
            HasMany(x => x.LeadMailsList).Inverse().Cascade.None().LazyLoad();
        }
    }
}
