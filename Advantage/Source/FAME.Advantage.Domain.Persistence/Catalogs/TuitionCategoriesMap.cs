﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TuitionCategoriesMap.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the TuitionCategoriesMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    using FAME.Advantage.Domain.Catalogs;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The tuition categories map.
    /// </summary>
    public class TuitionCategoriesMap : ClassMap<TuitionCategories>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TuitionCategoriesMap"/> class.
        /// </summary>
        public TuitionCategoriesMap()
        {
            this.Table("saTuitionCategories");

            this.Id(l => l.ID).Column("TuitionCategoryId").GeneratedBy.Guid();

            this.Map(l => l.Descrip).Column("TuitionCategoryDescrip").Length(50).Not.Nullable();

            this.References(l => l.CampusGroupObj).Column("CampGrpId").Not.Nullable().LazyLoad();
            this.References(x => x.SyStatusesObj).Column("StatusId").Not.Nullable().LazyLoad();

            this.HasMany(x => x.LeadList).KeyColumn("TuitionCategoryId").Inverse().Cascade.None().LazyLoad();
        }
    }
}
