﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// Source Category Table Map adSourceCategory
    /// </summary>
    public class SourceAdvertisementMap : ClassMap<SourceAdvertisement>
    {
        /// <summary>
        /// Source Category Table map adSourceCatagory
        /// </summary>
        public SourceAdvertisementMap()
        {
            Table("adSourceAdvertisement");

            Id(l => l.ID).Column("SourceAdvId").GeneratedBy.Guid();

            Map(l => l.SourceAdvCode).Column("SourceAdvCode").Length(50).Not.Nullable();
            Map(l => l.SourceAdvDescrip).Column("SourceAdvDescrip").Length(50).Not.Nullable();
            
            References(x => x.SyStatusesObj).Column("StatusID").Not.Nullable().LazyLoad();
            References(x => x.CampusGroupObj).Column("CampGrpId").Nullable().LazyLoad();
            References(x => x.SourceTypeObj).Column("SourceTypeId").Not.Nullable().LazyLoad();
            
            HasMany(x => x.LeadList).Inverse().Cascade.None().LazyLoad();
        }
    }
}
