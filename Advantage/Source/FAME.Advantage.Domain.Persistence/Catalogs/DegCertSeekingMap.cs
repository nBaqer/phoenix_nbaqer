﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DegCertSeekingMap.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   Defines the DegCertSeekingMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    using FAME.Advantage.Domain.Catalogs;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The degree certificate seeking .
    /// </summary>
    public class DegCertSeekingMap : ClassMap<DegCertSeeking>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DegCertSeekingMap"/> class.
        /// </summary>
        public DegCertSeekingMap()
        {
            this.Table("adDegCertSeeking");

            this.Id(l => l.ID).Column("DegCertSeekingId").GeneratedBy.Guid();

            //this.Map(l => l.Code).Column("Code").Length(12).Not.Nullable();
            this.Map(l => l.Descrip).Column("Descrip").Length(50).Not.Nullable();

            this.References(l => l.CampusGroupObj).Column("CampGrpId").Not.Nullable().LazyLoad();
            this.References(x => x.SyStatusesObj).Column("StatusId").Not.Nullable().LazyLoad();

            this.HasMany(x => x.LeadList).KeyColumn("DegCertSeekingId").Inverse().Cascade.None().LazyLoad();
        }
    }
}
