﻿using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    ///// <summary>
    ///// Map for table syNotes Type
    ///// </summary>
    //public class NotesTypeMap: ClassMap<NotesType>
    //{
    //    /// <summary>
    //    /// MAP for Table plAddressTypes
    //    ///  </summary>
    //    public NotesTypeMap()
    //    {
    //        Table("syNotesType");


    //        Id(l => l.ID).Column("NoteTypeId").GeneratedBy.Identity();

    //        Map(l => l.Code).Column("Code").Length(2).Not.Nullable();
    //        Map(l => l.Description).Column("Description").Length(20).Not.Nullable();
    //        Map(l => l.ModUser).Column("ModUser").Length(50).Not.Nullable();
    //        Map(l => l.ModDate).Column("ModDate").Not.Nullable();


    //        HasMany(x => x.AllNotesList).KeyColumn("NoteTypeId").Inverse().Cascade.None().LazyLoad();
    //    }
    //}
}
