﻿using FAME.Advantage.Domain.Campuses.CampusGroups;
using FAME.Advantage.Domain.Catalogs;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Catalogs
{
    /// <summary>
    /// Map of syFamilyIncome
    /// </summary>
    public class FamilyIncomingMap: ClassMap<FamilyIncoming>
    {
        /// <summary>
        /// Map of syFamilyIncome
        /// </summary>
        public FamilyIncomingMap()
        {
            Table("syFamilyIncome");

            Id(l => l.ID).Column("FamilyIncomeID").GeneratedBy.Guid();

            Map(l => l.FamilyIncomeCode).Column("FamilyIncomeCode").Length(50).Nullable();
            Map(l => l.FamilyIncomeDescrip).Column("FamilyIncomeDescrip").Length(50).Nullable();
            Map(l => l.ViewOrder).Column("ViewOrder").Not.Nullable();
            References(x => x.CampusGrp).Column("CampGrpId").Nullable().LazyLoad();
            References(x => x.SyStatusesObj).Column("StatusId").Nullable().LazyLoad();
            
            HasMany(x => x.LeadList).Inverse().Cascade.None().LazyLoad();
        }
    }
}
