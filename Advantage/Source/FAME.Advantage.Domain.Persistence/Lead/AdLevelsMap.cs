﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Map for Table AdExtraCurricularLevel
    /// </summary>
    public class AdLevelsMap: ClassMap<AdLevels>
    {
        /// <summary>
        /// Constructor of the class.
        /// </summary>
        public AdLevelsMap()
        {
            Table("AdLevel");

            Id(l => l.ID).Column("LevelId").Unique().GeneratedBy.Assigned();
            Map(x => x.Code).Column("Code").Not.Nullable();
            Map(x => x.Description).Column("Description").Not.Nullable();
            Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Not.Nullable();
           

            References(x => x.StatusObj).Column("StatusId").Nullable().LazyLoad();
            HasMany(x => x.ExtraCurricularList).KeyColumn("LevelId").Cascade.None().LazyLoad();
            
        }
    }
}