﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Table map syLeadStatusesChanges
    /// </summary>
    public class LeadStatusesChangesMap: ClassMap<LeadStatusesChanges>
    {
        /// <summary>
        /// Table map syLeadStatusesChanges
        /// </summary>
        public LeadStatusesChangesMap()
        {
            Table("syLeadStatusesChanges");

            Id(l => l.ID).Column("StatusChangeId").GeneratedBy.Guid();

            Map(x => x.DateOfChange).Column("DateOfChange").Nullable();
            Map(x => x.ModDate).Column("ModDate").Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable();
           
            References(x => x.LeadObj).Column("LeadId").Not.Nullable().Cascade.None().LazyLoad();
            References(x => x.NewStatusObj).Column("NewStatusId").Cascade.None().LazyLoad();
            References(x => x.OrigStatusObj).Column("OrigStatusId").Cascade.None().LazyLoad();

        }
    }
}
