﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Map for VIEW VIEW_AdmissionRepByCampus
    /// </summary>
    public class AdmissionRepByCampusViewMap : ClassMap<AdmissionRepByCampusView>
    {
        /// <summary>
        /// Constructor of the class.
        /// </summary>
        public AdmissionRepByCampusViewMap()
        {
            Table("VIEW_AdmissionRepByCampus");

            Id(l => l.ID).Column("ID").Unique().GeneratedBy.Assigned();

            Map(l => l.FullName).Column("FullName").Nullable().Length(100);

            References(l => l.UserObj).Column("UserId").Not.Nullable().LazyLoad();
            References(l => l.CampusObj).Column("CampusID").Not.Nullable().LazyLoad();
        }
    }
}

