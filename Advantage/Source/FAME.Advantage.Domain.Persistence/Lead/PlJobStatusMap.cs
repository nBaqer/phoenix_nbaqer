﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlJobStatusMap.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Mapping for Table PlJobStatus
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.Lead
{
    using Domain.Lead;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Mapping for Table PlJobStatus
    /// </summary>
    public class PlJobStatusMap : ClassMap<PlJobStatus>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlJobStatusMap"/> class.
        ///  Mapping for Table PlJobStatus
        /// </summary>
        public PlJobStatusMap()
        {
            this.Table("PlJobStatus");

            this.Id(l => l.ID).Column("JobStatusId").Unique().GeneratedBy.Guid();

            this.Map(x => x.JobStatusDescrip).Column("JobStatusDescrip").Not.Nullable();
            this.References(x => x.StatusObj).Column("StatusId").Nullable().LazyLoad();
            this.Map(x => x.JobCode).Column("JobCode").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Not.Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            this.References(x => x.CampGrpObj).Column("CampGrpId").Nullable().LazyLoad();
        }
    }
}