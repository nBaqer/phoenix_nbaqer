﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Mapping for adLastNameHistory
    /// </summary>
    public class LeadLastNameHistoryMap: ClassMap<LeadLastNameHistory>
    {
        /// <summary>
        /// Constructor of the class.
        /// </summary>
        public LeadLastNameHistoryMap()
        {
            Table("adLastNameHistory");

            Id(l => l.ID).Column("Id").GeneratedBy.Native().Unique();
            Map(x => x.LastName).Column("LastName").Not.Nullable().Length(50);
            Map(x => x.ModUser).Column("ModUser").Not.Nullable();
            Map(x => x.ModDate).Column("ModDate").Not.Nullable();

            References(x => x.LeadObj).Column("LeadId").Not.Nullable().LazyLoad();
           
        }
    }
}

