﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Map for table adExtraCurrGrp
    /// </summary>
    public class ExtraCurricularGroupMap: ClassMap<ExtraCurricularGroup>
    {
        /// <summary>
        /// Constructor of the class.
        /// </summary>
        public ExtraCurricularGroupMap()
        {
            Table("adExtraCurrGrp");

            Id(l => l.ID).Column("ExtraCurrGrpId").Unique().GeneratedBy.Guid();
            Map(x => x.ExtraCurrGrpCode).Column("ExtraCurrGrpCode").Nullable();
            Map(x => x.ExtraCurrGrpDescrip).Column("ExtraCurrGrpDescrip").Nullable();
            Map(x => x.ModDate).Column("ModDate").Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable();
           

            References(x => x.StatusObj).Column("StatusId").Nullable().LazyLoad();
            References(x => x.CampusGroupObj).Column("CampGrpId").Nullable().LazyLoad();

            HasMany(x => x.ExtraCurricularList).KeyColumn("ExtraCurrGrpId").Cascade.None().LazyLoad();
        }
    }
}