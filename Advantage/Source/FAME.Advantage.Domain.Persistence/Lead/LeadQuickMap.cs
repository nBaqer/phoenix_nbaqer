﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadQuickMap.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   MAP for Table VIEW_GetQuickLeadFields
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Lead
{
    using FAME.Advantage.Domain.Lead;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// MAP for Table VIEW_GetQuickLeadFields
    /// </summary>
    public class LeadQuickMap : ClassMap<LeadQuick>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadQuickMap"/> class. 
        ///  MAP for Table VIEW_GetQuickLeadFields
        /// Gets the list of all the fields and related data to add the fields dynamically in the Quick Lead page
        /// </summary>
        public LeadQuickMap()
        {
            this.Table("VIEW_GetQuickLeadFields");

            this.Id(x => x.ID).Column("ResDefId").Not.Nullable();
            this.Map(x => x.FldName).Column("FldName").Not.Nullable();
            this.Map(x => x.Required).Column("Required").Not.Nullable();
            this.Map(x => x.FldLen).Column("FldLen").Not.Nullable();
            this.Map(x => x.FldType).Column("FldType").Not.Nullable();
            this.Map(x => x.Caption).Column("Caption").Not.Nullable();
            this.Map(x => x.DdlId).Column("DDLId").Nullable();
            this.Map(x => x.TblFldsId).Column("TblFldsId").Not.Nullable();
            this.Map(x => x.FldId).Column("FldId").Not.Nullable();
            this.Map(x => x.SectionId).Column("SectionId").Not.Nullable();
            this.Map(x => x.ControlName).Column("ControlName").Nullable();
            this.Map(x => x.FldTypeId).Column("FldTypeId").Not.Nullable();
            this.Map(x => x.CtrlIdName).Column("ctrlIdName").Not.Nullable();
            this.Map(x => x.PropName).Column("propName").Not.Nullable();
            this.Map(x => x.ParentCtrlId).Column("ParentCtrlId").Nullable();
            this.Map(x => x.Sequence).Column("sequence").Nullable();
        }
    }
}