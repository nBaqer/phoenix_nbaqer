﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ViewTaskNotesMap.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Map for View View_TaskNotes
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.Lead
{
    using Domain.Lead;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Map for View View_TaskNotes
    /// </summary>
    public class ViewTaskNotesMap : ClassMap<ViewTaskNotes>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewTaskNotesMap"/> class. 
        /// Constructor of the class.
        /// </summary>
        public ViewTaskNotesMap()
        {
            this.Table("View_TaskNotes");

            this.Id(l => l.ID).Column("UserTaskId").Unique().GeneratedBy.Guid();
            this.Map(x => x.Description).Column("Description").Nullable();
            this.Map(x => x.Message).Column("Message").Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Nullable();
            
            this.References(x => x.LeadObj).Column("LeadId").Not.Nullable().LazyLoad();
            this.References(x => x.UserObj).Column("AssignedById").Nullable().LazyLoad();
        }
    }
}