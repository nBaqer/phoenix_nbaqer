﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PriorWorkContactMap.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Mapping for Table PriorWorkContact
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.Lead
{
    using Domain.Lead;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Mapping for Table PriorWorkContact
    /// </summary>
    public class PriorWorkContactMap : ClassMap<PriorWorkContact>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PriorWorkContactMap"/> class.
        /// Mapping for Table PriorWorkContact
        /// </summary>
        public PriorWorkContactMap()
        {
            this.Table("PriorWorkContact");

            this.Id(l => l.ID).Column("Id").Unique().GeneratedBy.Identity();
             this.Map(x => x.JobTitle).Column("JobTitle").Not.Nullable();
            this.Map(x => x.FirstName).Column("FirstName").Not.Nullable();
            this.Map(x => x.MiddleName).Column("MiddleName").Not.Nullable();
            this.Map(x => x.LastName).Column("LastName").Not.Nullable();
            this.Map(x => x.Phone).Column("Phone").Not.Nullable();
            this.Map(x => x.IsPhoneInternational).Column("IsPhoneInternational").Not.Nullable();
            this.Map(x => x.Email).Column("Email").Not.Nullable();
            this.Map(x => x.IsActive).Column("IsActive").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Not.Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            this.Map(x => x.IsDefault).Column("IsDefault").Not.Nullable();

            this.References(x => x.StEmploymentObj).Column("StEmploymentId").Not.Nullable().LazyLoad();
            this.References(x => x.PrefixObj).Column("PrefixId").Nullable().LazyLoad();
         }
    }
}