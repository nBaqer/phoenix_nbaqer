﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AllHistoryMap.cs" company="FAME Inc.">
//  FAME Inc. 2016
// </copyright>
// <summary>
//   The History map.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.Domain.Persistence.Lead
{

using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;
 
    /// <summary>
    /// Map for Table AllHistory
    /// </summary>
    public class AllHistoryMap : ClassMap<AllHistory>
    {
        /// <summary>
        /// Constructor of the class.
        /// </summary>

        public AllHistoryMap()
        {
            Table("VIEW_GetHistory");
            this.ReadOnly();
            this.Id(x => x.ID).Column("HistoryId").Not.Nullable().GeneratedBy.Guid();
            Map(l => l.ModuleCode).Column("ModuleCode").Length(2).Not.Nullable().Default(string.Empty);
            Map(l => l.ModDate).Column("Date").Nullable();
            Map(l => l.HistoryModule).Column("Module").Length(70).Not.Nullable().Default(string.Empty);
            Map(l => l.HistoryType).Column("Type").Length(50).Not.Nullable().Default(string.Empty);
            Map(l => l.Description).Column("Description").Length(2000).Not.Nullable().Default(string.Empty);
            Map(l => l.ModUserFullName).Column("ModUser").Length(50).Nullable().Default(string.Empty);
            Map(l => l.LeadId).Column("LeadId").Not.Nullable();
            Map(l => l.AdditionalContent).Column("AdditionalContent").Length(2000).Not.Nullable().Default(string.Empty);

        }
    }
}
