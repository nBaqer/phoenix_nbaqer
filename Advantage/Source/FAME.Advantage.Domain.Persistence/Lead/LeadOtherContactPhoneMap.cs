﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Domain Mapping for adLeadOtherContactsPhone
    /// </summary>
    public class LeadOtherContactPhoneMap: ClassMap<LeadOtherContactPhone>
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public LeadOtherContactPhoneMap()
        {
            Table("adLeadOtherContactsPhone");
            Id(x => x.ID).Column("OtherContactsPhoneId").Not.Nullable().GeneratedBy.Guid();

            //Map(x => x.OtherContactId).Column("OtherContactId").Not.Nullable();
            //Map(x => x.StatusId).Column("StatusId").Nullable();
            Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Not.Nullable().Length(50);

            //Map(x => x.LeadId).Column("LeadId").Not.Nullable();
            //Map(x => x.PhoneTypeId).Column("PhoneTypeId").Not.Nullable();
            Map(x => x.Phone).Column("Phone").Not.Nullable().Length(15);
            Map(x => x.Extension).Column("Extension").Not.Nullable().Length(10);
            Map(x => x.IsForeignPhone).Column("IsForeignPhone").Not.Nullable();
            
            References(x => x.Lead).Column("LeadId").Not.Nullable().LazyLoad();
            References(x => x.Status).Column("StatusId").Not.Nullable().LazyLoad();
            References(x => x.PhoneType).Column("PhoneTypeId").Not.Nullable().LazyLoad();
            References(x => x.LeadOtherContact).Column("OtherContactId").Not.Nullable().LazyLoad();
        }
    }
}
