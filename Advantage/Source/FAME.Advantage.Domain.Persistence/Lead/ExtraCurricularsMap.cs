﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Map for table adExtraCurr
    /// </summary>
    public class ExtraCurricularsMap : ClassMap<ExtraCurriculars>
    {
        /// <summary>
        /// Source Category Table map adAdminCriteria
        /// </summary>
        public ExtraCurricularsMap()
        {
            Table("adExtraCurricular");

            Id(l => l.ID).Column("IdExtraCurricular").GeneratedBy.Identity();

            Map(l => l.ExtraCurrDescription).Column("ExtraCurrDescription").Length(50).Not.Nullable();
            Map(l => l.ExtraCurrComments).Column("ExtraCurrComment").Length(100).Not.Nullable().Default(string.Empty);
            Map(l => l.ModDate).Column("ModDate").Not.Nullable();
            Map(l => l.ModUser).Column("ModUser").Not.Nullable();

            References(l => l.ExtraCurricularGroupObj).Column("ExtraCurrGrpId").Not.Nullable().LazyLoad();
            References(l => l.LevelObj).Column("LevelId").Not.Nullable().LazyLoad();
            References(l => l.LeadObj).Column("LeadId").Not.Nullable().LazyLoad();


            //HasManyToMany(l => l.LeadList)
            //    .Table("adLeadExtraCurriculars")
            //    .ParentKeyColumn("ExtraCurricularID")
            //    .ChildKeyColumn("LeadId")
            //    .Cascade.SaveUpdate()
            //    .LazyLoad();
        }
    }
}
