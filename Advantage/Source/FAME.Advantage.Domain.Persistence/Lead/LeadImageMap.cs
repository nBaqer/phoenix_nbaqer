﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadImageMap.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Mapping for Table LeadImage
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.Lead
{
    using Domain.Lead;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Mapping for Table LeadImage
    /// </summary>
    public class LeadImageMap : ClassMap<LeadImage>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadImageMap"/> class. 
        ///  Mapping for Table LeadImage
        /// </summary>
        public LeadImageMap()
        {
            this.Schema("doc");
            this.Table("LeadImage");

            this.Id(l => l.ID).Column("ImageId").Unique().GeneratedBy.Native();
            this.Map(x => x.Image).Column("Image").Not.Nullable().Length(100000);
            this.Map(x => x.ImgFile).Column("ImgFile").Not.Nullable();
            this.Map(x => x.ImgSize).Column("ImgSize").Not.Nullable();
            this.Map(x => x.MediaType).Column("MediaType").Not.Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Not.Nullable();
            this.Map(x => x.Type).Column("TypeId").Not.Nullable();
            this.References(x => x.LeadObj).Column("LeadId").Not.Nullable().LazyLoad();
        }
    }
}
