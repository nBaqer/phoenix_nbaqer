﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// plSkillGroups Map
    /// </summary>
    public class SkillGroupsMap : ClassMap<SkillGroups>
    {
        /// <summary>
        /// Constructor of the class.
        /// </summary>
        public SkillGroupsMap()
        {
            Table("plSkillGroups");

            Id(l => l.ID).Column("SkillGrpId").Unique().GeneratedBy.Guid();
            Map(x => x.SkillGrpCode).Column("SkillGrpCode").Nullable();
            Map(x => x.SkillGrpName).Column("SkillGrpName").Nullable();
            Map(x => x.ModDate).Column("ModDate").Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable();
           

            References(x => x.StatusObj).Column("StatusId").Nullable().LazyLoad();
            References(x => x.CampusGroupObj).Column("CampGrpId").Nullable().LazyLoad();

            HasMany(x => x.SkillsList).KeyColumn("SkillGrpId").Cascade.None().LazyLoad();
        }
    }
}