﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadTransactionsMap.cs" company="FAME Inc">
//  2016
// FAME.Advantage.Domain.Persistence.Lead.LeadTransactionsMap   
// </copyright>
// <summary>
//   MAP Table adLeadTransactions
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Lead
{
    using Domain.Lead;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// MAP Table adLeadTransactions
    /// </summary>
    public class LeadTransactionsMap : ClassMap<LeadTransactions>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadTransactionsMap"/> class. 
        /// MAP Table adLeadTransactions
        /// </summary>
        public LeadTransactionsMap()
        {
            this.Table("adLeadTransactions");

            this.Id(l => l.ID).Column("TransactionId").Unique().GeneratedBy.Guid();

            this.Map(l => l.CreatedDate).Column("CreatedDate").Not.Nullable();
            this.Map(l => l.DisplaySequence).Column("DisplaySequence").Nullable();
            this.Map(l => l.IsEnrolled).Column("IsEnrolled").Nullable();
            this.Map(l => l.MapTransactionId).Column("MapTransactionId").Nullable();
            this.Map(l => l.ModDate).Column("ModDate").Nullable();
            this.Map(l => l.ModUser).Column("ModUser").Nullable();
            this.Map(l => l.ReversalReason).Column("ReversalReason").Nullable();
            this.Map(l => l.SecondDisplaySequence).Column("SecondDisplaySequence").Nullable();
            this.Map(l => l.TransAmount).Column("TransAmount").Nullable();
            this.Map(l => l.TransDate).Column("TransDate").Nullable();
            this.Map(l => l.TransDescription).Column("TransDescrip").Nullable();
            this.Map(l => l.TransReference).Column("TransReference").Nullable();

            this.Map(l => l.Voided).Column("Voided").Nullable();

            this.References(l => l.LeadObj).Column("LeadId").Not.Nullable().LazyLoad();
            this.References(l => l.TransactionCode).Column("TransCodeId").Not.Nullable().LazyLoad();
            this.References(l => l.TransactionType).Column("TransTypeId").Not.Nullable().LazyLoad();
            this.References(l => l.Requirement).Column("LeadRequirementId").Nullable().LazyLoad();
            this.References(l => l.Payment).Column("TransactionId").Nullable().LazyLoad();
            this.References(l => l.Campus).Column("CampusId").Nullable().LazyLoad();
        }
    }
}   
