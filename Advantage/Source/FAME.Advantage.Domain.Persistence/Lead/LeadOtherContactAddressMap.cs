﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Domain Mapping for adLeadOtherContactsAddreses
    /// </summary>
    public class LeadOtherContactAddressMap : ClassMap<LeadOtherContactAddress>
    { /// <summary>
        /// Default Constructor
        /// </summary>
        public LeadOtherContactAddressMap()
        {
            Table("adLeadOtherContactsAddreses");
            Id(x => x.ID).Column("OtherContactsAddresesId").Not.Nullable().GeneratedBy.Guid();

            //Map(x => x.OtherContactId).Column("OtherContactId").Not.Nullable();
            //Map(x => x.StatusId).Column("StatusId").Not.Nullable();
            //Map(x => x.StateId).Column("StateId").Nullable();
            //Map(x => x.CountryId).Column("CountryId").Nullable();
            //Map(x => x.CountyId).Column("CountyId").Nullable();
            //Map(x => x.AddressTypeId).Column("AddressTypeId").Nullable();
            Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Not.Nullable().Length(50);

            //Map(x => x.LeadId).Column("LeadId").Not.Nullable();
            Map(x => x.Address1).Column("Address1").Not.Nullable().Length(250);
            Map(x => x.Address2).Column("Address2").Nullable().Length(250);
            Map(x => x.City).Column("City").Nullable().Length(250);
            Map(x => x.ZipCode).Column("ZipCode").Nullable().Length(10);
            Map(x => x.IsMailingAddress).Column("IsMailingAddress").Not.Nullable();
            Map(x => x.IsInternational).Column("IsInternational").Nullable();
            Map(x => x.StateInternational).Column("State").Nullable().Length(100);
            Map(x => x.CountyInternational).Column("County").Nullable().Length(100);
            Map(x => x.CountryInternational).Column("Country").Nullable().Length(100);
            
            References(x => x.Lead).Column("LeadId").Not.Nullable().LazyLoad();
            References(x => x.Status).Column("StatusId").Not.Nullable().LazyLoad();
            References(x => x.AddressType).Column("AddressTypeId").Not.Nullable().LazyLoad();

            References(x => x.State).Column("StateId").Nullable().LazyLoad();
            References(x => x.Country).Column("CountryId").Nullable().LazyLoad();
            References(x => x.County).Column("CountyId").Nullable().LazyLoad();
            References(x => x.LeadOtherContact).Column("OtherContactId").Cascade.None().Not.Nullable().LazyLoad();
        }
    }
}
