﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Map Table AdVehicles
    /// </summary>
    public class AdVehiclesMap: ClassMap<AdVehicles>
    {
        /// <summary>
        /// Constructor of the class.
        /// </summary>
        public AdVehiclesMap()
        {
            Table("adVehicles");

            Id(l => l.ID).Column("VehicleId").Unique().GeneratedBy.Identity();
            Map(x => x.Permit).Column("Permit").Not.Nullable();
            Map(x => x.Color).Column("Color").Not.Nullable();
            Map(x => x.Make).Column("Make").Not.Nullable();
            Map(x => x.Model).Column("Model").Not.Nullable();
            Map(x => x.Plate).Column("Plate").Not.Nullable();
            Map(x => x.Position).Column("Position").Not.Nullable();
            Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Not.Nullable();

            References(x => x.LeadObj).Column("LeadId").Not.Nullable().LazyLoad();
        }
    }
}