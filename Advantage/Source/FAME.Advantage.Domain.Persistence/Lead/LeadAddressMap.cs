﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadAddressMap.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   MAP for table adLeadPhone
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Lead
{
    using FluentNHibernate.Mapping;

    /// <summary>
    /// MAP for table adLeadPhone
    /// </summary>
    public class LeadAddressMap : ClassMap<Domain.Lead.LeadAddress>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadAddressMap"/> class. 
        /// Mapping for table adLeadPhone
        /// </summary>
        public LeadAddressMap()
        {
            this.Table("adLeadAddresses");
 
            this.Id(x => x.ID).Column("adLeadAddressId").GeneratedBy.Guid();
            
            this.Map(x => x.Address1).Column("Address1").Length(250).Not.Nullable();
            this.Map(x => x.AddressApto).Column("AddressApto").Length(20).Not.Nullable();
            this.Map(x => x.Address2).Column("Address2").Length(250).Nullable();
            this.Map(x => x.City).Column("City").Length(250).Nullable();
            this.Map(x => x.ZipCode).Column("ZipCode").Length(10).Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Length(50).Not.Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            this.Map(x => x.IsMailingAddress).Column("IsMailingAddress").Not.Nullable();
            this.Map(x => x.IsShowOnLeadPage).Column("IsShowOnLeadPage").Not.Nullable();
            this.Map(x => x.IsInternational).Column("IsInternational").Nullable();
            this.Map(x => x.StateInternational).Column("State").Nullable();
            this.Map(x => x.CountryInternational).Column("ForeignCountryStr").Nullable();
            this.Map(x => x.CountyInternational).Column("ForeignCountyStr").Nullable();
            
            this.References(x => x.AddressType).Column("AddressTypeId").Not.Nullable().LazyLoad();
            this.References(x => x.CountryObj).Column("CountryId").Nullable().LazyLoad();
            this.References(x => x.StateObj).Column("StateId").Nullable().LazyLoad();
            this.References(x => x.StatusObj).Column("StatusId").Not.Nullable().LazyLoad();
            this.References(x => x.LeadObj).Column("LeadId").Not.Nullable().LazyLoad();
            this.References(x => x.CountyObj).Column("CountyId").Nullable().LazyLoad();
        }
    }
}
