﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// MAP Table adVendors
    /// </summary>
    public class AdVendorsMap: ClassMap<AdVendors>
    {
        public AdVendorsMap()
        {
            Table("adVendors");

            Id(l => l.ID).Column("VendorId").Unique().GeneratedBy.Native();

            Map(l => l.VendorName).Column("VendorName").Length(50).Not.Nullable();
            Map(l => l.VendorCode).Column("VendorCode").Length(50).Not.Nullable();
            Map(l => l.Description).Column("Description").Length(100).Nullable();
            Map(l => l.DateOperationBegin).Column("DateOperationBegin").Not.Nullable();
            Map(l => l.DateOperationEnd).Column("DateOperationEnd").Nullable();
            Map(l => l.IsDeleted).Column("IsDeleted").Not.Nullable();
            Map(l => l.IsActive).Column("IsActive").Not.Nullable();

           
            //HasMany(x => x.VendorCampaignsList).KeyColumn("VendorId");
        }
    }
}
