﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadTaskViewMap.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Mapping for VIEW_LeadUserTask
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.Lead
{
    using FAME.Advantage.Domain.Lead;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// Mapping for VIEW_LeadUserTask
    /// </summary>
    public class LeadTaskViewMap : ClassMap<LeadTaskView>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadTaskViewMap"/> class. 
        /// MAP View VIEW_LeadUserTask
        /// </summary>
        public LeadTaskViewMap()
        {
            this.Table("VIEW_LeadUserTask");

            this.Id(l => l.ID).Column("UserTaskId").Unique().GeneratedBy.Guid();

            this.Map(l => l.Activity).Column("Activity").Nullable();
            this.Map(l => l.Code).Column("ActivityCode").Nullable();
            this.Map(l => l.Status).Column("Status").Nullable();
            this.Map(l => l.EndDate).Column("EndDate").Nullable();
            this.Map(l => l.StartDate).Column("StartDate").Nullable();

            this.References(l => l.LeadsObj).Column("LeadId").Nullable().LazyLoad();
            this.References(l => l.CampusGrpObj).Column("CampGroupId").Nullable().LazyLoad();
        }
    }
}
