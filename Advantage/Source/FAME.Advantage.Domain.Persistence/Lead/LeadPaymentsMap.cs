﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadPaymentsMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.Lead.LeadPaymentsMap
// </copyright>
// <summary>
//   Domain Mapping of table adLeadPayments with Domain entity LeadPayments
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Lead
{
    using FAME.Advantage.Domain.Lead;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// Domain Mapping of table adLeadPayments with Domain entity LeadPayments
    /// </summary>
    public class LeadPaymentsMap : ClassMap<LeadPayments>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadPaymentsMap"/> class.
        /// </summary>
        public LeadPaymentsMap()
        {
            this.Table("adLeadPayments");

            this.Id(l => l.ID).Column("TransactionId").Unique().GeneratedBy.Guid();
            this.Map(l => l.CheckNumber).Column("CheckNumber").Nullable();
            this.Map(l => l.ModUser).Column("ModUser").Nullable();
            this.Map(l => l.ModDate).Column("ModDate").Nullable();
            this.Map(l => l.IsDeposited).Column("IsDeposited").Not.Nullable();
            this.References(l => l.PaymentType).Column("PaymentTypeId").Not.Nullable().LazyLoad();
        }
    }
}
