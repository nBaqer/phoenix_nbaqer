﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// MAP for table adLeadPhone
    /// </summary>
    public class LeadPhoneMap: ClassMap<LeadPhone>
    {
        /// <summary>
        /// Mapping for table adLeadPhone
        /// </summary>
        public LeadPhoneMap()
        {
            Table("adLeadPhone");
 
            Id(x => x.ID).Column("LeadPhoneId").GeneratedBy.Guid();

            Map(x => x.Phone).Column("Phone").Length(50).Not.Nullable();
            Map(x => x.Extension).Column("Extension").Length(12).Nullable();
            Map(x => x.IsForeignPhone).Column("IsForeignPhone").Length(50).Not.Nullable();
            Map(x => x.ModDate).Column("ModDate").Length(50).Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Length(50).Not.Nullable();
            Map(x => x.Position).Column("Position").Not.Nullable();
            Map(x => x.IsBest).Column("IsBest").Nullable();
            Map(x => x.IsShowOnLeadPage).Column("IsShowOnLeadPage").Nullable();

            References(x => x.NewLeadObj).Column("LeadId").Cascade.None().Not.Nullable().LazyLoad();
            References(x => x.SyPhoneTypeObj).Column("PhoneTypeId").Not.Nullable().LazyLoad();
            References(x => x.Status).Column("StatusId").Nullable().LazyLoad();
        }
    }
}
