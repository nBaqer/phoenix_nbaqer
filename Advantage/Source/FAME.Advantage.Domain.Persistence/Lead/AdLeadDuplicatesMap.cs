﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdLeadDuplicatesMap.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Map adLeadDuplicates Table
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.Lead
{
    using FAME.Advantage.Domain.Lead;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Map adLeadDuplicates Table
    /// </summary>
    public class AdLeadDuplicatesMap : ClassMap<AdLeadDuplicates>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AdLeadDuplicatesMap"/> class. 
        /// Constructor of the class.
        /// </summary>
        public AdLeadDuplicatesMap()
        {
            this.Table("adLeadDuplicates");

            this.Id(l => l.ID).Column("IdDuplicates").Unique().GeneratedBy.Native();

            this.References(x => x.NewLeadObj).Column("NewLeadGuid").Not.Nullable().LazyLoad();
            this.References(x => x.PosibleLeadDuplicateObj).Column("PosibleDuplicateGuid").Not.Nullable();
        }
    }
}
