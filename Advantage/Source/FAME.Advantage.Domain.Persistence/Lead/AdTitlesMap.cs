﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdTitlesMap.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Mapping for Table AdTitles
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.Lead
{
    using Domain.Lead;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Mapping for Table AdTitles
    /// </summary>
    public class AdTitlesMap : ClassMap<AdTitles>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AdTitlesMap"/> class.
        ///  Mapping for Table AdTitles
        /// </summary>
        public AdTitlesMap()
        {
            this.Table("AdTitles");

            this.Id(l => l.ID).Column("TitleId").Unique().GeneratedBy.Guid();

            this.Map(x => x.TitleCode).Column("TitleCode").Not.Nullable();
            this.Map(x => x.TitleDescrip).Column("TitleDescrip").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Not.Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Not.Nullable();

            this.References(x => x.StatusObj).Column("StatusId").Nullable().LazyLoad();
            this.References(x => x.JobCatObj).Column("JobCatId").Nullable().LazyLoad();
            this.References(x => x.CampGrpObj).Column("CampGrpId").Nullable().LazyLoad();
        }
    }
}