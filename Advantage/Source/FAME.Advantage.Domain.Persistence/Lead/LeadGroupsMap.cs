﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadGroupsMap.cs" company="FAME Inc.">
//   FAME Inc. 2016 
//   FAME.Advantage.Domain.Persistence.Lead.LeadGroupsMap
// </copyright>
// <summary>
//   Map between the Domain LeadGroups with Table adLeadGroups
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Lead
{
    using FAME.Advantage.Domain.Lead;
    using FAME.Advantage.Domain.Requirements;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Map between the Domain LeadGroups with Table adLeadGroups
    /// </summary>
    public class LeadGroupsMap : ClassMap<LeadGroups>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadGroupsMap"/> class. 
        /// Mapping between the Domain LeadGroups with Table adLeadGroups
        /// </summary>
        public LeadGroupsMap()
        {
            this.Table("AdLeadGroups");

            this.Id(l => l.ID).Column("LeadGrpId").GeneratedBy.Guid();

            this.Map(l => l.ModifiedDate).Column("ModDate").Not.Nullable();
            this.Map(l => l.ModifiedUser).Column("ModUser").Length(50).Not.Nullable();
            this.Map(l => l.Code).Column("code").Nullable();
            this.Map(l => l.UseForScheduling).Column("UseForScheduling").Nullable();
            this.Map(l => l.UseForStudentGroupTracking).Column("UseForStudentGroupTracking").Nullable();
            this.Map(l => l.Description).Column("Descrip").Not.Nullable();

            this.References(l => l.Status).Column("StatusId");
            this.References(l => l.CampusGroup).Column("CampGrpId");

            this.HasMany(l => l.RequirementLeadGroup).KeyColumn("LeadGrpId").LazyLoad();

            this.HasManyToMany(l => l.LeadsList)
                .Cascade.SaveUpdate()
                .Table("adLeadByLeadGroups")
                .ParentKeyColumn("LeadGrpId")
                .ChildKeyColumn("LeadId").LazyLoad();

            this.HasManyToMany(l => l.RequirementGroups)
                .Cascade.SaveUpdate()
                .Table("adLeadGrpReqGroups")
                .ParentKeyColumn("LeadGrpId")
                .ChildKeyColumn("ReqGrpId").LazyLoad();
        }
    }
}