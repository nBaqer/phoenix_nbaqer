﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AllNotesMap.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Map for Table AllNotes
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Map for Table AllNotes
    /// </summary>
    public class AllNotesMap: ClassMap<AllNotes>
    {
        /// <summary>
        /// Constructor of the class.
        /// </summary>
        public AllNotesMap()
        {
            Table("AllNotes");

            Id(l => l.ID).Column("NotesId").Unique().GeneratedBy.Identity();
            Map(l => l.NoteText).Column("NoteText").Length(2000).Not.Nullable();
            Map(l => l.ModUser).Column("ModUser").Length(50).Not.Nullable();
            Map(l => l.ModDate).Column("ModDate").Not.Nullable();
            Map(l => l.NoteType).Column("NoteType").Length(50).Not.Nullable().Default(string.Empty);
            //Map(l => l.IsConfidential).Column("IsConfidential").Not.Nullable();

            References(x => x.UserObj).Column("UserId").Not.Nullable().Cascade.None().LazyLoad();
            References(x => x.ModuleObj).Column("ModuleCode").Not.Nullable().PropertyRef(x => x.Code).Cascade.None().LazyLoad();
            //References(x => x.NoteTypeObj).Column("NoteTypeId").Not.Nullable().Cascade.None().LazyLoad();
            References(x => x.PageFieldsObj).Column("PageFieldId").Not.Nullable().Cascade.None().LazyLoad();

            //HasMany(l => l.LeadNotesList).KeyColumn("NotesId").Cascade.DeleteOrphan().Inverse().LazyLoad();
            HasManyToMany(x => x.LeadList)
              .Table("adLead_Notes")
              .ParentKeyColumn("NotesId")
              .ChildKeyColumn("LeadId")
              .Cascade.None()
              .Inverse()
              .LazyLoad();
        }
    }
}

