﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PriorWorkAddressMap.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Mapping for Table PriorWorkAddress
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Lead
{
    using Domain.Lead;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Mapping for Table PriorWorkAddress
    /// </summary>
    public class PriorWorkAddressMap : ClassMap<PriorWorkAddress>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PriorWorkAddressMap"/> class. 
        ///  Mapping for Table PriorWorkAddressMap
        /// </summary>
        public PriorWorkAddressMap()
        {
            this.Table("PriorWorkAddress");

            this.Id(l => l.ID).Column("Id").Unique().GeneratedBy.Identity();
            this.Map(x => x.Address1).Column("Address1").Not.Nullable();
            this.Map(x => x.Apartment).Column("Apartment").Not.Nullable().Default(string.Empty);
            this.Map(x => x.Address2).Column("Address2").Not.Nullable().Default(string.Empty);
            this.Map(x => x.City).Column("City").Not.Nullable();
            this.Map(x => x.IsInternational).Column("IsInternational").Not.Nullable();
            this.Map(x => x.StateInternational).Column("StateInternational").Not.Nullable().Default(string.Empty);
            this.Map(x => x.ZipCode).Column("ZipCode").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Not.Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Not.Nullable();

            this.References(x => x.StateObj).Column("StateId").Nullable().LazyLoad().Cascade.None();
            this.References(x => x.CountryObj).Column("CountryId").Nullable().LazyLoad().Cascade.None();
            this.References(x => x.StEmploymentObj).Column("StEmploymentId").Not.Nullable().LazyLoad();
       }
    }
}