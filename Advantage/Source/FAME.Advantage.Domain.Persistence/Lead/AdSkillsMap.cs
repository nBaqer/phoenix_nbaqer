﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Source Category Table map plSkills
    /// </summary>
    public class AdSkillsMap : ClassMap<AdSkills>
    {
        /// <summary>
        /// Source Category Table map plSkills
        /// </summary>
        public AdSkillsMap()
        {
            Table("adSkills");

            Id(l => l.ID).Column("SkillId").GeneratedBy.Identity();

            Map(l => l.Description).Column("Description").Length(50).Not.Nullable();
            Map(l => l.Comment).Column("Comment").Length(100).Not.Nullable();
            Map(l => l.ModDate).Column("ModDate").Not.Nullable();
            Map(l => l.ModUser).Column("ModUser").Length(50).Not.Nullable();

            References(x => x.LevelObj).Column("LevelId").Not.Nullable().LazyLoad();
            References(x => x.SkillGroupObj).Column("SkillGrpId").LazyLoad();
            References(x => x.LeadObj).Column("LeadId").Not.Nullable().LazyLoad();
        }
    }
}
