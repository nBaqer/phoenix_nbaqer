﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Domain Mapping for adLeadOtherContacts
    /// </summary>
    public class LeadOtherContactMap: ClassMap<LeadOtherContact>
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public LeadOtherContactMap()
        {
            Table("adLeadOtherContacts");
            Id(x => x.ID).Column("OtherContactId").Not.Nullable().GeneratedBy.Guid();

            //Map(x => x.StatusId).Column("StatusId").Not.Nullable();
            Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Not.Nullable().Length(50);

            //Map(x => x.SufixId).Column("SufixId").Nullable();
            //Map(x => x.LeadId).Column("LeadId").Not.Nullable();
            //Map(x => x.PrefixId).Column("PrefixId").Nullable();
           // Map(x => x.ContactTypeId).Column("ContactTypeId").Not.Nullable();
            //Map(x => x.RelationshipId).Column("RelationshipId").Not.Nullable();

            Map(x => x.Comments).Column("Comments").Nullable().Length(500);
            Map(x => x.LastName).Column("LastName").Not.Nullable().Length(50);
            Map(x => x.MiddleName).Column("MiddleName").Nullable().Length(50);
            Map(x => x.FirstName).Column("FirstName").Not.Nullable().Length(50);

            References(x => x.Lead).Column("LeadId").Not.Nullable().LazyLoad();
            References(x => x.Sufix).Column("SufixId").Nullable().LazyLoad();
            References(x => x.Status).Column("StatusId").Not.Nullable().LazyLoad();
            References(x => x.Prefix).Column("PrefixId").Nullable().LazyLoad();
            References(x => x.ContactType).Column("ContactTypeId").Not.Nullable().LazyLoad();
            References(x => x.Relationship).Column("RelationshipId").Not.Nullable().LazyLoad();

            HasMany(x => x.Phones).KeyColumn("OtherContactId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            HasMany(x => x.Emails).KeyColumn("OtherContactId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            HasMany(x => x.Addresses).KeyColumn("OtherContactId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
        }
    }
}
