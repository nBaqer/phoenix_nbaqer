﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadMap.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Lead Domain for AdLeads Table
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Lead
{
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Lead Domain for AdLeads Table
    /// </summary>
    public class LeadMap : ClassMap<Domain.Lead.Lead>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadMap"/> class. 
        /// Map for AdLeads
        /// </summary>
        public LeadMap()
        {
            this.Table("adLeads");

            this.Id(l => l.ID).Column("LeadId").GeneratedBy.GuidNative();

            // Demographic information ..................................................................................
            this.Map(l => l.FirstName).Column("FirstName").Length(50).Not.Nullable();
            this.Map(l => l.MiddleName).Column("MiddleName").Length(50).Nullable();
            this.Map(l => l.LastName).Column("LastName").Length(50).Not.Nullable();
            this.Map(l => l.SSN).Column("SSN").Length(50).Nullable();
            this.Map(l => l.BirthDate).Column("BirthDate").Nullable();
            this.Map(l => l.DriverLicenseNumber).Column("DrivLicNumber").Length(50).Nullable();
            this.Map(l => l.AlienNumber).Column("AlienNumber").Length(50).Nullable();
            this.Map(l => l.Age).Column("Age").Length(3).Nullable();
            this.Map(l => l.Dependants).Column("Children").Nullable();
            this.Map(l => l.NickName).Column("NickName").Not.Nullable();
            this.Map(l => l.DistanceToSchool).Column("DistanceToSchool").Nullable();
            this.Map(l => l.IsDisabled).Column("IsDisabled").Nullable();
            this.Map(l => l.EntranceInterviewDate).Column("EntranceInterviewDate").Nullable();
            this.Map(l => l.IsFirstTimeInSchool).Column("IsFirstTimeInSchool").Not.Nullable();
            this.Map(l => l.IsFirstTimePostSecSchool).Column("IsFirstTimePostSecSchool").Not.Nullable();
            this.Map(l => l.StudentNumber).Column("StudentNumber").Not.Nullable();
            this.Map(l => l.StudentId).Column("StudentId").Not.Nullable();
            this.Map(l => l.AfaStudentId).Column("AfaStudentId").Nullable();
            this.References(l => l.StudentStatus).Column("StudentStatusId").Not.Nullable();
            this.References(l => l.FamilyIncomingObj).Column("FamilyIncome").Nullable().LazyLoad();
            this.References(l => l.DriverLicenseStateCodeObj).Column("DrivLicStateID").Nullable().LazyLoad();
            this.References(x => x.PrefixObj).Column("Prefix").Nullable().LazyLoad();
            this.References(x => x.SuffixObj).Column("Suffix").Nullable().LazyLoad();
            this.References(x => x.CitizenshipsObj).Column("Citizen").Nullable().LazyLoad();
            this.References(x => x.GenderObj).Column("Gender").Nullable().LazyLoad();
            this.References(x => x.HousingTypeObj).Column("HousingId").Nullable().LazyLoad();
            this.References(x => x.MaritalStatusObj).Column("MaritalStatus").Nullable().LazyLoad();
            this.References(x => x.TransportationObj).Column("TransportationId").Nullable().LazyLoad();
            this.References(x => x.DependencyTypeObj).Column("DependencyTypeId").Nullable().LazyLoad();
            this.References(x => x.EthnicityObj).Column("Race").Nullable().LazyLoad();
            this.References(x => x.Nationality).Column("Nationality").Nullable().LazyLoad();
            this.References(x => x.ShiftId).Column("ShiftID").Nullable().LazyLoad();
            this.References(x => x.DegCertSeekingId).Column("DegCertSeekingId").Nullable().LazyLoad();
            this.References(x => x.GeographicTypeId).Column("GeographicTypeId").Nullable().LazyLoad();
            this.HasMany(x => x.LeadLastNameList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.VehiclesList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();

            // Academic Information .......................................................................................
            this.Map(l => l.ExpectedStart).Column("ExpectedStart").Nullable();
            this.References(x => x.LeadStatus).Column("LeadStatus").Not.Nullable().LazyLoad();
            this.References(x => x.Campus).Column("CampusId").Nullable().LazyLoad();
            this.References(x => x.ProgramGroupObj).Column("AreaID").Nullable().LazyLoad();
            this.References(x => x.ProgramVersion).Column("PrgVerId").Nullable().LazyLoad();
            this.References(x => x.AttendTypeObj).Column("AttendTypeId").Nullable().LazyLoad();
            this.References(x => x.ProgramObj).Column("ProgramID").Nullable().LazyLoad();
            this.References(x => x.ProgramScheduleObj).Column("ProgramScheduleId").Nullable().LazyLoad();
            
            // Contact information .........................................................................................
            this.Map(l => l.HomeEmail).Column("HomeEmail").Nullable();
            this.Map(l => l.PreferredContactId).Column("PreferredContactId").Not.Nullable();
            this.Map(l => l.ProgramOfInterest).Column("ProgramOfInterest");
            this.Map(l => l.CampusOfInterest).Column("CampusOfInterest");

            this.Map(l => l.BestTime).Column("BestTime").Nullable().Length(20);
            this.Map(l => l.NoneEmail).Column("NoneEmail").Not.Nullable();

            this.References(x => x.StateObj).Column("StateId").LazyLoad();
            this.References(x => x.EnrollState).Column("EnrollStateId").LazyLoad();

            this.HasMany(x => x.LeadPhoneList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.LeadEmailList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.StatusesChangesList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.OtherContacts).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.AddressList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();

            // Source information ......................................................................................
            ////Map(l => l.AdvertisementNote).Column("AdvertisementNote").Nullable().Length(50);
            this.Map(l => l.SourceDate).Column("SourceDate").Nullable();
            this.References(l => l.SourceCategoryObj).Column("SourceCategoryID").Nullable().LazyLoad();
            this.References(l => l.SourceTypeObj).Column("SourceTypeID").Nullable().LazyLoad();
            this.References(l => l.SourceAdvertisementObj).Column("SourceAdvertisement").Nullable().LazyLoad();

            // Others Information.......................................................................................
            this.Map(l => l.AssignedDate).Column("AssignedDate").Nullable();
            this.Map(l => l.DateApplied).Column("DateApplied").Nullable();
            ////Map(l => l.Comments).Column("Comments").Length(100).Nullable();
            this.Map(l => l.HighSchoolGradDate).Column("HighSchoolGradDate").Nullable();
            this.Map(l => l.AttendingHs).Column("AttendingHs").Nullable();
            this.References(l => l.AgencySponsorsObj).Column("Sponsor").Nullable().LazyLoad();
            this.References(l => l.ReasonNotEnrolledObj).Column("ReasonNotEnrolledId").Nullable().LazyLoad();

            this.References(l => l.PreviousEducationObj).Column("PreviousEducation").Nullable().LazyLoad();
            this.References(l => l.HighSchoolsObj).Column("HighSchoolId").Nullable().LazyLoad();
            this.References(l => l.AdminCriteriaObj).Column("admincriteriaid").Nullable().LazyLoad();
            
            // audit Information.......................................................................................
            this.Map(l => l.ModDate).Column("ModDate");
            this.Map(l => l.ModUser).Column("ModUser").Length(50).Nullable();
            this.References(x => x.CampaignObj).Column("CampaignId").LazyLoad();
            this.References(x => x.AdmissionRepUserObj).Column("AdmissionsRep").LazyLoad();
            this.References(x => x.Vendor).Column("VendorId").LazyLoad();

            ////References(x => x.LeadGroupBridges).Column("LeadId").Nullable().Not.LazyLoad();

            this.HasMany(x => x.DocumentHistory).KeyColumn("LeadId");
            this.HasMany(x => x.LeadDocumentsList).KeyColumn("LeadId");
            this.HasMany(x => x.NewLeadsWithDuplicatesList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.DuplicateLeadsList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.SdfModuleValueList).KeyColumn("LeadId").Cascade.SaveUpdate().Inverse().LazyLoad();
            this.HasMany(x => x.TransactionsList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.LeadEmploymentsList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().LazyLoad();
            ////HasMany(x => x.NotesList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.MruList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.EducationList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.TasksList).KeyColumn("LeadId").Cascade.None().Inverse().LazyLoad();
            this.HasMany(x => x.ExtraCurricularList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.SkillList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
 
            // LeadPhoto table relation
            this.HasMany(x => x.ImageList64).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();

            // Relation with enrollment table
           this.HasMany(x => x.EnrollmentList).KeyColumn("LeadId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();

            this.HasManyToMany(x => x.LeadGroupsList)
                .Table("adLeadByLeadGroups")
                .ParentKeyColumn("LeadId")
                .ChildKeyColumn("LeadGrpId")
                .Cascade.SaveUpdate()
             ////   .Inverse()
                .LazyLoad();

            this.HasManyToMany(x => x.NotesList)
                .Table("adLead_Notes")
                .ParentKeyColumn("LeadId")
                .ChildKeyColumn("NotesId")
                .Cascade.AllDeleteOrphan()
                .LazyLoad();
        }
    }
}
