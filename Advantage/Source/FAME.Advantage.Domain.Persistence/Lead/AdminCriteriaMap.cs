﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Map for table adAdminCriteria
    /// </summary>
    public class AdminCriteriaMap : ClassMap<AdminCriteria>
    {
        /// <summary>
        /// Source Category Table map adAdminCriteria
        /// </summary>
        public AdminCriteriaMap()
        {
            Table("adAdminCriteria");

            Id(l => l.ID).Column("admincriteriaid").GeneratedBy.Guid();

            Map(l => l.Code).Column("Code").Length(50).Not.Nullable();
            Map(l => l.Description).Column("Descrip").Length(50).Not.Nullable();
            
            References(x => x.SyStatusesObj).Column("StatusID").LazyLoad();
            References(x => x.CampusGroupObj).Column("CampGrpId").LazyLoad();
            
            HasMany(x => x.LeadList).Inverse().Cascade.None().LazyLoad();
        }
    }
}
