﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Map for adVendorCampaign
    /// </summary>
    public class AdVendorCampaignMap: ClassMap<AdVendorCampaign>
    {
        /// <summary>
        /// Constructor of the class.
        /// </summary>
        public AdVendorCampaignMap()
        {
            Table("adVendorCampaign");

            Id(l => l.ID).Column("CampaignId").Unique().GeneratedBy.Native();

            Map(l => l.AccountId).Column("AccountId").Length(50).Nullable();
            Map(l => l.CampaignCode).Column("CampaignCode").Length(50).Not.Nullable();
            Map(l => l.IsActive).Column("IsActive").Not.Nullable();
            Map(l => l.IsDeleted).Column("IsDeleted").Not.Nullable();
            Map(l => l.DateCampaignBegin).Column("DateCampaignBegin").Not.Nullable();
            Map(l => l.DateCampaignEnd).Column("DateCampaignEnd").Not.Nullable();
            Map(l => l.Cost).Column("Cost").Not.Nullable();
            Map(l => l.FilterRejectByCounty).Column("FilterRejectByCounty").Not.Nullable();
            Map(l => l.FilterRejectDuplicates).Column("FilterRejectDuplicates").Not.Nullable();

           
            References(x => x.PayForObj).Column("PayForId").LazyLoad();
            References(x => x.VendorObj).Column("VendorId").Not.Nullable().LazyLoad();

            HasMany(x => x.LeadsList).KeyColumn("CampaignId").LazyLoad();
        }
    }
}

