﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdLeadEmploymentMap.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   MAP for Table adLeadEmployment
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Lead
{
    using Domain.Lead;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// MAP for Table adLeadEmployment
    /// </summary>
    public class AdLeadEmploymentMap : ClassMap<AdLeadEmployment>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AdLeadEmploymentMap"/> class. 
        ///  MAP for Table adLeadEmployment
        /// </summary>
        public AdLeadEmploymentMap()
        {
            this.Table("adLeadEmployment");

            this.Id(l => l.ID).Column("StEmploymentId").Unique().GeneratedBy.Guid();

            this.Map(x => x.StartDate).Column("StartDate").Nullable();
            this.Map(x => x.EndDate).Column("EndDate").Nullable();
            this.Map(x => x.Comments).Column("Comments").Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Not.Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            this.Map(x => x.JobResponsibilities).Column("JobResponsibilities").Nullable();
            this.Map(x => x.EmployerName).Column("EmployerName").Nullable();
            this.Map(x => x.EmployerJobTitle).Column("EmployerJobTitle").Nullable();

            this.References(x => x.LeadObj).Column("LeadId").Not.Nullable().LazyLoad();
            this.References(x => x.JobTitleObj).Column("JobTitleId").Nullable().LazyLoad();
            this.References(x => x.JobStatusObj).Column("JobStatusId").Nullable().LazyLoad();

            this.HasMany(x => x.PriorWorkContactsList).KeyColumn("StEmploymentId").Cascade.All().Inverse().LazyLoad();
            this.HasMany(x => x.PriorWorkAddressesList).KeyColumn("StEmploymentId").Cascade.All().Inverse().LazyLoad();
        }
    }
}
