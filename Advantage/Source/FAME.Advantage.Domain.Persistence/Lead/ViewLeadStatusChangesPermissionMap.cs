﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Mapping for the view View_LeadStatusChangesPerUser
    /// </summary>
    public class ViewLeadStatusChangesPermissionMap : ClassMap<ViewLeadStatusChangesPermission>
    {
        /// <summary>
        /// Constructor of the class.
        /// </summary>
        public ViewLeadStatusChangesPermissionMap()
        {
            Table("View_LeadStatusChangesPerUser");

            Id(l => l.ID).Column("Id").Unique().GeneratedBy.Assigned();

            References(x => x.NewStatusObj).Column("NewStatusId").LazyLoad();
            References(x => x.OrigStatusObj).Column("OrigStatusId").LazyLoad();
            References(x => x.CampusObj).Column("CampusID").LazyLoad();
            References(x => x.UserObj).Column("UserId").LazyLoad();
        }
    }
}

