﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{

    /// <summary>
    /// Domain Mapping for adLeadOtherContactsEmail
    /// </summary>
    public class LeadOtherContactEmailMap: ClassMap<LeadOtherContactEmail>
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public LeadOtherContactEmailMap()
        {
            Table("adLeadOtherContactsEmail");
            Id(x => x.ID).Column("OtherContactsEmailId").Not.Nullable().GeneratedBy.Guid();

            //Map(x => x.OtherContactId).Column("OtherContactId").Not.Nullable();
            //Map(x => x.StatusId).Column("StatusId").Nullable();
            //Map(x => x.EmailTypeId).Column("EMailTypeId").Nullable();
            Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Not.Nullable().Length(50);

            //Map(x => x.LeadId).Column("LeadId").Not.Nullable();
            Map(x => x.Email).Column("EMail").Not.Nullable().Length(100);

            References(x => x.OtherContact).Column("OtherContactId").Not.Nullable().LazyLoad();
            References(x => x.Lead).Column("LeadId").Not.Nullable().LazyLoad();
            References(x => x.Status).Column("StatusId").Not.Nullable().LazyLoad();
            References(x => x.EmailType).Column("EMailTypeId").Not.Nullable().LazyLoad();
        }
    }
}
