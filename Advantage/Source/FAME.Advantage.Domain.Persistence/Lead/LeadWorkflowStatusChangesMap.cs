﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// MAP for syLeadStatusChanges Table
    /// </summary>
    public class LeadWorkflowStatusChangesMap : ClassMap<LeadWorkflowStatusChanges>
    {
        /// <summary>
        /// Source Category Table map syLeadStatusChanges
        /// </summary>
        public LeadWorkflowStatusChangesMap()
        {
            Table("syLeadStatusChanges");

            Id(l => l.ID).Column("LeadStatusChangeId").GeneratedBy.Guid();

            Map(l => l.ModDate).Column("ModDate").Nullable();
            Map(l => l.ModUser).Column("ModUser").Length(50).Nullable();
            
            References(x => x.NewStatusObj).Column("NewStatusId").LazyLoad();
            References(x => x.OrigStatusObj).Column("OrigStatusId").LazyLoad();
            References(x => x.CampusGroupObj).Column("CampGrpId").LazyLoad();
            
            HasMany(x => x.StatusChangesPermissionsList).Cascade.None().LazyLoad();
        }
    }
}