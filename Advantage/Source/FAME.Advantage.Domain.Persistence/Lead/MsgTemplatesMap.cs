using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{

    /// <summary>
    /// MAP for Table msgTemplates
    /// </summary>
    public class MsgTemplatesMap : ClassMap<MsgTemplates>
    {
        /// <summary>
        ///  MAP for Table msgTemplates
        /// </summary>
        public MsgTemplatesMap()
        {
            Table("msgTemplates");

            Id(l => l.ID).Column("TemplateId").Unique().GeneratedBy.Guid();

            Map(x => x.Code).Column("Code").Not.Nullable();
            Map(x => x.Descrip).Column("Descrip").Not.Nullable();
            Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Not.Nullable();
            Map(x => x.Active).Column("Active").Not.Nullable();

            HasMany(x => x.MessagesList).KeyColumn("TemplateId").Cascade.DeleteOrphan().LazyLoad();

            //Map(x => x.Data).Column("Data").Not.Nullable();
            //References(x => x.GroupObj).Column("GroupId").Nullable().LazyLoad();
            //References(x => x.ModuleEntityObj).Column("ModuleEntityId").Nullable().LazyLoad();
            //References(x => x.CampGroupObj).Column("CampGroupId").Nullable().LazyLoad();

        }
    }
}

