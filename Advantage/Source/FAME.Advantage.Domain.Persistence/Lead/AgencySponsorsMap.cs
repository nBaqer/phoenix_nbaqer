﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Map for adAgencySponsors Table
    /// </summary>
    public class AgencySponsorsMap: ClassMap<AgencySponsors>
    {
        /// <summary>
        /// Map for adAgencySponsors Table
        /// </summary>
        public AgencySponsorsMap()
        {
            Table("adAgencySponsors");

            Id(l => l.ID).Column("AgencySpId").GeneratedBy.Guid();

            Map(l => l.AgencySpCode).Column("AgencySpCode").Length(12).Not.Nullable();
            Map(l => l.AgencySpDescrip).Column("AgencySpDescrip").Length(50).Not.Nullable();
            
            References(x => x.SyStatusesObj).Column("StatusID").LazyLoad();
            References(x => x.CampusGroupObj).Column("CampGrpId").LazyLoad();
            
            HasMany(x => x.LeadList).Inverse().Cascade.None().LazyLoad();
        }
    }
}
