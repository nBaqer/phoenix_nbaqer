﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyReasonNotEnrolledMap.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Mapping for Table syReasonNotEnrolled
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.Lead
{
    using Domain.Lead;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Mapping for Table <code>syReasonNotEnrolled</code>
    /// </summary>
    public class SyReasonNotEnrolledMap : ClassMap<SyReasonNotEnrolled>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyReasonNotEnrolledMap"/> class. 
        ///  Mapping for Table <code>syReasonNotEnrolled</code>
        /// </summary>
        public SyReasonNotEnrolledMap()
        {
            this.Table("syReasonNotEnrolled");

            this.Id(l => l.ID).Column("ReasonNotEnrolledId").Unique().GeneratedBy.Guid();

            this.Map(x => x.Code).Column("Code").Not.Nullable();
            this.Map(x => x.Description).Column("Description").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Not.Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            this.References(x => x.StatusObj).Column("StatusId").Nullable().LazyLoad();
            this.References(x => x.CampGrpObj).Column("CampGrpId").Nullable().LazyLoad();
            this.HasMany(x => x.LeadList).KeyColumn("ReasonNotEnrolledId").LazyLoad();
        }
    }
}
