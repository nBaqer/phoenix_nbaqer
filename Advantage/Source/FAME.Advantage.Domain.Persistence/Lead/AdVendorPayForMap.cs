﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Map for adVendorPayFor
    /// </summary>
    public class AdVendorPayForMap: ClassMap<AdVendorPayFor>
    {
        public AdVendorPayForMap()
        {
            Table("adVendorPayFor");

            Id(l => l.ID).Column("IdPayFor").Unique().GeneratedBy.Native();

            Map(l => l.PayForCode).Column("PayForCode").Length(50).Not.Nullable();
            Map(l => l.Description).Column("Description").Length(100).Nullable();
           
            HasMany(x => x.VendorCampaignsList).KeyColumn("PayForId").LazyLoad();
           
        }
    }
}
