﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Mapping for table adLeadEmail
    /// </summary>
    public class LeadEmailMap: ClassMap<LeadEmail>
    {
        /// <summary>
        /// Mapping for table adLeadEmail
        /// </summary>
        public LeadEmailMap()
        {
            Table("adLeadEmail");
 
            Id(x => x.ID).Column("LeadEMailId").GeneratedBy.Guid();

            Map(x => x.Email).Column("EMail").Length(100).Not.Nullable();
            Map(x => x.IsPreferred).Column("IsPreferred").Not.Nullable();
            Map(x => x.IsShowOnLeadPage).Column("IsShowOnLeadPage").Nullable();
            Map(x => x.IsPortalUserName).Column("IsPortalUserName").Not.Nullable();
            Map(x => x.ModDate).Column("ModDate").Length(50).Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Length(50).Not.Nullable();
 
            References(x => x.LeadObj).Column("LeadId").Cascade.None().Not.Nullable().LazyLoad();
            References(x => x.EmailTypeObj).Column("EMailTypeId").Not.Nullable().LazyLoad();
            References(x => x.Status).Column("StatusId").Nullable().LazyLoad();
        }
    }
}
