﻿using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// Map for syLeadStatusChangePermissions Table
    /// </summary>
    public class LeadWorkflowStatusChangesPermissionsMap : ClassMap<LeadWorkflowStatusChangesPermissions>
    {
        /// <summary>
        /// Table map syLeadStatusChangePermissions
        /// </summary>
        public LeadWorkflowStatusChangesPermissionsMap()
        {
            Table("syLeadStatusChangePermissions");

            Id(l => l.ID).Column("LeadStatusChangePermissionID").GeneratedBy.Guid();

            Map(l => l.ModDate).Column("ModDate").Nullable();
            Map(l => l.ModUser).Column("ModUser").Length(50).Nullable();
            
            References(l => l.LeadWorkflowStatusChangesObj).Column("LeadStatusChangeId").LazyLoad();
            References(x => x.RoleObj).Column("RoleId").LazyLoad();
            References(x => x.StatusObj).Column("StatusId").LazyLoad();
        }
    }
}