using FAME.Advantage.Domain.Lead;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Lead
{
    /// <summary>
    /// MAP for Table View_Messages
    /// </summary>
    public class ViewMessagesMap : ClassMap<ViewMessages>
    {
        /// <summary>
        ///  MAP for Table View_Messages
        /// </summary>
        public ViewMessagesMap()
        {
            Table("View_Messages");

            Id(l => l.ID).Column("MessageId").Unique().GeneratedBy.Guid();

            Map(x => x.DeliveryType).Column("DeliveryType").Not.Nullable().Default("Email");
            Map(x => x.DateDelivered).Column("DateDelivered").Not.Nullable();
            References(x => x.FromObj).Column("FromId").Nullable().LazyLoad();
            References(x => x.RecipientObj).Column("RecipientId").Nullable().LazyLoad();
            References(x => x.TemplateObj).Column("TemplateId").Nullable().LazyLoad();
        }
    }
}

