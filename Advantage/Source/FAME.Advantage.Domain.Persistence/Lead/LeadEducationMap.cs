﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEducationMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.Lead.LeadEducationMap
// </copyright>
// <summary>
//   The lead education map.
//   Domain Mapping for table <code>adLeadEducation</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Lead
{
    using FAME.Advantage.Domain.Lead;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The lead education map.
    /// Domain Mapping for table <code>adLeadEducation</code>
    /// </summary>
    public class LeadEducationMap : ClassMap<LeadEducation>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadEducationMap"/> class.
        /// </summary>
        public LeadEducationMap()
        {
            this.Table("adLeadEducation");

            this.Id(l => l.ID).Column("LeadEducationId").GeneratedBy.Guid();

            this.Map(l => l.EducationInstitutionType).Column("EducationInstType").Length(50).Not.Nullable();
            this.Map(l => l.GraduatedDate).Column("GraduatedDate").Nullable();
            this.Map(l => l.FinalGrade).Column("FinalGrade").Length(50).Nullable();
            this.Map(l => l.CertificateId).Column("CertificateId").Nullable();
            this.Map(l => l.Comments).Column("Comments").Length(300).Nullable();
            this.Map(l => l.ModifiedUser).Column("ModUser").Length(50).Nullable();
            this.Map(l => l.ModifiedDate).Column("ModDate").Nullable();
            this.Map(l => l.Major).Column("Major").Length(50).Nullable();
            this.Map(l => l.Graduate).Column("Graduate").Not.Nullable();
            this.Map(l => l.Gpa).Column("GPA").Nullable();
            this.Map(l => l.Rank).Column("Rank").Nullable();
            this.Map(l => l.Percentile).Column("Percentile").Nullable();
            this.Map(l => l.Certificate).Column("Certificate").Length(50).Nullable();

            this.References(x => x.Lead).Column("LeadId").Cascade.None().LazyLoad();
            this.References(x => x.Institution).Column("EducationInstId").Cascade.None().LazyLoad();
            this.References(x => x.Status).Column("StatusId").Not.Nullable().LazyLoad();
        }
    }
}
