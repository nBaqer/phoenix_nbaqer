﻿using FAME.Advantage.Domain.Requirements.Documents;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Requirements.Documents
{
    /// <summary>
    /// Map for adEntrTestOverRide table
    /// </summary>
    public class AdEntrTestOverRideMap : ClassMap<AdEntrTestOverRide>
    {
        /// <summary>
        /// Mapping
        /// </summary>
        public AdEntrTestOverRideMap()
        {
            Table("adEntrTestOverRide");
            Id(x => x.ID).Column("EntrTestOverRideId").GeneratedBy.Guid();

            Map(x => x.ModUser).Column("ModUser").Length(50).Nullable();
            Map(x => x.ModDate).Column("ModDate").Nullable();
            Map(x => x.OverRide).Column("OverRide").Nullable();

            References(x => x.Student).Column("StudentId").LazyLoad();
            References(x => x.LeadObj).Column("LeadId").LazyLoad();
            References(x => x.Requirement).Column("EntrTestId").LazyLoad();
        }
    }
}
