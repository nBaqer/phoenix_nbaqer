﻿using FAME.Advantage.Domain.Requirements.Documents;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Requirements.Documents
{
    /// <summary>
    /// Map for table AdLeadReqsReceived
    /// </summary>
    public class AdLeadReqsReceivedMap : ClassMap<AdLeadReqsReceived>
    {
        /// <summary>
        ///  Map for table AdLeadReqsReceived
        /// </summary>
        public AdLeadReqsReceivedMap()
        {
            Table("AdLeadReqsReceived");

            Id(x => x.ID).Column("LeadReqId").GeneratedBy.Guid();

            Map(x => x.ReceivedDate).Column("ReceivedDate").Nullable();
            Map(x => x.IsApproved).Column("IsApproved").Nullable();
            Map(x => x.ApprovalDate).Column("ApprovalDate").Nullable();
            Map(x => x.ModDate).Column("ModDate").Nullable();
            Map(x => x.ModUser).Column("ModUser").Length(50).Nullable();
            Map(x => x.Override).Column("Override").Not.Nullable();
            Map(x => x.OverrideReason).Column("OverrideReason").Length(1000).Nullable();
            References(x => x.Requirement).Column("DocumentId").LazyLoad().NotFound.Ignore();

            References(x => x.Lead).Column("LeadId").LazyLoad();
        }
    }
}



