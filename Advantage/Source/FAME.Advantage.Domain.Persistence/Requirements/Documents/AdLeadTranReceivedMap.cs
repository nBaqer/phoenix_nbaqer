﻿using FAME.Advantage.Domain.Requirements.Documents;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Requirements.Documents
{
    /// <summary>
    /// MAP AdLeadTranreceived
    /// </summary>
    public class AdLeadTranReceivedMap : ClassMap<AdLeadTranReceived>
    {
        /// <summary>
        /// MAP
        /// </summary>
        public AdLeadTranReceivedMap()
        {
            Table("AdLeadTranReceived");

            Id(x => x.ID).Column("adLeadTranreceivedId").GeneratedBy.Guid();

            Map(x => x.ReceivedDate).Column("ReceivedDate").Not.Nullable();
            Map(x => x.IsApproved).Column("IsApproved").Not.Nullable();
            Map(x => x.ApprovalDate).Column("ApprovalDate").Nullable();
            Map(x => x.ModDate).Column("ModDate").Nullable();
            Map(x => x.ModUser).Column("ModUser").Length(50).Nullable();
            Map(x => x.Override).Column("Override").Not.Nullable();
            Map(x => x.OverrideReason).Column("OverrideReason").Length(50).Nullable();

            References(x => x.Requirement).Column("DocumentId").LazyLoad().NotFound.Ignore();
            References(x => x.Lead).Column("LeadId").LazyLoad();
        }
    }
}



