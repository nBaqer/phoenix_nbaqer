﻿using FAME.Advantage.Domain.Requirements.Documents;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Requirements.Documents
{
    /// <summary>
    /// Mapping for adLeadDocsReceived
    /// </summary>
    public class LeadDocumentMap : ClassMap<LeadDocument>
    {
        /// <summary>
        /// Map
        /// </summary>
        public LeadDocumentMap()
        {
            Table("adLeadDocsReceived");

            Id(x => x.ID).Column("LeadDocId").GeneratedBy.Guid();
            
            Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Not.Nullable();
            Map(x => x.DateReceived).Column("ReceiveDate").Nullable();
            Map(x => x.Override).Column("Override").Not.Nullable();
            Map(x => x.OverrideReason).Column("OverrideReason").Length(1000).Nullable();
            Map(x => x.ApprovalDate).Column("ApprovalDate").Nullable();
            

            References(x => x.DocumentStatus)
                .Column("DocStatusId")
                .LazyLoad().Cascade.All()
                .Nullable();

            References(x => x.Requirement)
                .Column("DocumentId")
                .LazyLoad().                
                Not.Nullable();

            References(x => x.Lead)
                .Column("LeadId")
                .LazyLoad()
                .Nullable();
         
        }
    }
}
