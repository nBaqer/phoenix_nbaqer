﻿using FAME.Advantage.Domain.Requirements.Documents;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Requirements.Documents
{
    public class DocumentMap : ClassMap<Document>
    {
        public DocumentMap()
        {
            Table("plStudentDocs");

            Id(x => x.ID).Column("StudentDocId").GeneratedBy.Guid();
            
            Map(x => x.DateModified).Column("ModDate").Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable();
            Map(x => x.DateRequested).Column("RequestDate").Nullable();
            Map(x => x.DateReceived).Column("ReceiveDate").Nullable();

            References(x => x.DocumentStatus)
                .Column("DocStatusId")
                .LazyLoad().Cascade.All()
                .Not.Nullable();

            References(x => x.Requirement)
                .Column("DocumentId")
                .LazyLoad().                
                Not.Nullable();

            References(x => x.Student)
                .Column("StudentId")
                .LazyLoad()
                .Nullable();

            HasMany(x => x.Files).KeyColumn("StudentDocId").LazyLoad();
        }
    }
}
