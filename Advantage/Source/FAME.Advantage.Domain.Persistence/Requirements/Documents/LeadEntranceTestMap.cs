﻿using FAME.Advantage.Domain.Requirements.Documents;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Requirements.Documents
{
    /// <summary>
    /// MAP for Table adLeadEntranceTest
    /// </summary>
    public class LeadEntranceTestMap : ClassMap<LeadEntranceTest>
    {
        /// <summary>
        /// Mapping
        /// </summary>
        public LeadEntranceTestMap()
        {
            Table("adLeadEntranceTest");

            Id(x => x.ID).Column("LeadEntrTestId").GeneratedBy.Guid();

            Map(x => x.ModDate).Column("ModDate").Nullable();
            Map(x => x.CompletedDate).Column("CompletedDate").Nullable();
            Map(x => x.ModUser).Column("ModUser").Length(50).Nullable();
            Map(x => x.Override).Column("Override").Nullable();
            Map(x => x.Pass).Column("Pass").Nullable();
            Map(x => x.TestTaken).Column("TestTaken").Length(50).Nullable();
            Map(x => x.ViewOrder).Column("ViewOrder").Nullable();
            Map(x => x.ActualScore).Column("ActualScore").Nullable();
            Map(x => x.OverrideReason).Column("OverrideReason").Length(1000).Nullable();

            References(x => x.Student).Column("StudentId").LazyLoad();
            References(x => x.LeadObj).Column("LeadId").LazyLoad();
            References(x => x.Requirement).Column("EntrTestId").LazyLoad();
        }
    }
}



