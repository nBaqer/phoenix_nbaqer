﻿using System;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Requirements.Documents.File
{
    public class FileMap : ClassMap<Domain.Requirements.Documents.File.File>
    {
        public FileMap()
        {
            Schema("doc");
            Table("DocumentFile");

            Id(x => x.ID).Column("Id").GeneratedBy.Guid();

            Map(x => x.Content).Column("Content").Length(Int32.MaxValue).Not.Nullable().LazyLoad();
            Map(x => x.FileName).Column("FileName").Length(250).Not.Nullable();
            Map(x => x.DateCreated).Column("DateCreated").Not.Nullable().ReadOnly();
            Map(x => x.ModUser).Column("ModUser").Length(60).Not.Nullable();

            References(x => x.Document).Column("StudentDocId");
        }
    }
}
