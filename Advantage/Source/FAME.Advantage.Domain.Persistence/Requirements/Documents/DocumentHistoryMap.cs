﻿using FAME.Advantage.Domain.Requirements.Documents;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Requirements.Documents
{
    public class DocumentHistoryMap : ClassMap<DocumentHistory>
    {
        public DocumentHistoryMap()
        {
            Table("syDocumentHistory");
            
          

            Id(x => x.ID).Column("FileId").GeneratedBy.Guid();

            Map(x => x.FileName).Column("FileName").Nullable();
            Map(x => x.FileExtension).Column("FileExtension").Nullable();
            Map(x => x.DocumentType).Column("DocumentType").Nullable();
            Map(x => x.DisplayName).Column("DisplayName").Nullable();

            Map(x => x.DocumentId).Column("DocumentId").Nullable();
            Map(x => x.LeadId).Column("LeadId").Nullable();

            Map(x => x.UploadDate).Column("ModDate").Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable();
            Map(x => x.ModuleId).Column("ModuleId").Nullable();
            


            
        }
    }
}
    