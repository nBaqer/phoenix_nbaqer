﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequirementProgramVersionBridgeMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.Requirements
// </copyright>
// <summary>
//   The requirement program version bridge map.
//   Mapping for Domain Entity RequirementProgramVersionBridge with table adPRGVERTestDetails
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Requirements
{
    using FAME.Advantage.Domain.Requirements;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The requirement program version bridge map.
    /// Mapping for Domain Entity RequirementProgramVersionBridge with table adPRGVERTestDetails
    /// </summary>
    public class RequirementProgramVersionBridgeMap : ClassMap<RequirementProgramVersionBridge>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequirementProgramVersionBridgeMap"/> class.
        /// </summary>
        public RequirementProgramVersionBridgeMap()
        {
            this.Table("adPrgVerTestDetails");

            this.Id(x => x.ID).Column("PrgVerTestDetailId");

            this.References(x => x.ProgramVersion).Column("PrgVerId");

            this.References(x => x.RequirementGroup).Column("ReqGrpId");

            this.References(x => x.Requirement).Column("adReqId").NotFound.Ignore().LazyLoad();

            this.Map(x => x.MinScore).Column("MinScore").Nullable();
        }
    }
}