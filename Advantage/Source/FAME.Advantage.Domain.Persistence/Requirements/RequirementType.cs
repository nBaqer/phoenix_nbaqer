﻿using FAME.Advantage.Domain.Requirements;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Requirements
{
    public class RequirementTypeMap : ClassMap<RequirementType>
    {
        public RequirementTypeMap()
        {
            Table("adReqTypes");

            Id(x => x.ID).Column("adReqTypeId").GeneratedBy.Identity();
            Map(x => x.Description).Column("Descrip").Length(50).Nullable();
           
        }
    }   
}
