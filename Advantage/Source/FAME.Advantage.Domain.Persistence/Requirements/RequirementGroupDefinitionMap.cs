﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequirementGroupDefinitionMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.Requirements.RequirementGroupDefinitionMap
// </copyright>
// <summary>
//   The requirement group definition map.
//   Mapping for Domain Entity with Table adREQGRPDEF
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Requirements
{
    using FAME.Advantage.Domain.Requirements;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The requirement group definition map.
    /// Mapping for Domain Entity with Table adREQGRPDEF
    /// </summary>
    public class RequirementGroupDefinitionMap : ClassMap<RequirementGroupDefinition>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequirementGroupDefinitionMap"/> class.
        /// </summary>
        public RequirementGroupDefinitionMap()
        {
            this.Table("adReqGrpDef");

            this.Id(x => x.ID).Column("ReqGrpDefId").GeneratedBy.Guid();

            this.Map(x => x.Sequence).Column("Sequence").Nullable();
            this.Map(x => x.IsRequired).Column("IsRequired").Length(50).Nullable();

            this.References(x => x.Requirement).Column("adReqId").LazyLoad();

            this.References(x => x.LeadGroup).Column("LeadGrpId").LazyLoad();

            this.References(x => x.RequirementGroup).Column("ReqGrpId").LazyLoad();
        }
    }
}



