﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequirementGroupMap.cs" company="FAME Inc.">
//   FAME 2016
//   FAME.Advantage.Domain.Persistence.Requirements.RequirementGroupMap
// </copyright>
// <summary>
//   The requirement group map.
//   Mapping for Domain Entity RequirementGroup with table ADREQSGroups.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Requirements
{
    using FAME.Advantage.Domain.Requirements;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The requirement group map.
    /// Mapping for Domain Entity RequirementGroup with table ADREQGroups.
    /// </summary>
    public class RequirementGroupMap : ClassMap<RequirementGroup>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequirementGroupMap"/> class.
        /// </summary>
        public RequirementGroupMap()
        {
            this.Table("adReqGroups");

            this.Id(x => x.ID).Column("ReqGrpId").GeneratedBy.Guid();

            this.Map(x => x.Code).Column("Code").Nullable();
            this.Map(x => x.Description).Column("Descrip").Length(50).Nullable();
            this.Map(x => x.IsMandatory).Column("IsMandatoryReqGrp").Nullable();

            this.References(x => x.Status).Column("StatusId").Nullable().LazyLoad();
            this.References(x => x.CampusGroup).Column("CampGrpId").Nullable().LazyLoad();

            this.HasMany(x => x.LeadGroupRequirementGroupBridge).KeyColumn("ReqGrpId").LazyLoad();
            this.HasMany(x => x.RequirementGroupDefinitions).KeyColumn("ReqGrpId").LazyLoad();
            this.HasMany(x => x.RequirementProgramVersionBridge).KeyColumn("ReqGrpId").LazyLoad();
        }
    }
}