﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequirementLeadGroupBridgeMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.Requirements.RequirementLeadGroupBridgeMap
// </copyright>
// <summary>
//   Defines the RequirementLeadGroupBridgeMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Requirements
{
    using FAME.Advantage.Domain.Requirements;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The requirement lead group bridge map for Domain Entity RequirementLeadGroupBridge.
    /// Domain Mapping between Domain Entity and Table adREQLeadGroups
    /// Table adREQLeadGroups holds the association between a Lead Group and a Requirement Effective Date (table adREQSEffectiveDates).
    /// Requirement Effective Date bridge table holds the association with requirement.
    /// </summary>
    public class RequirementLeadGroupBridgeMap : ClassMap<RequirementLeadGroupBridge>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequirementLeadGroupBridgeMap"/> class.
        /// </summary>
        public RequirementLeadGroupBridgeMap()
        {
            this.Table("adReqLeadGroups");

            this.Id(x => x.ID).Column("ReqLeadGrpId").GeneratedBy.Guid();

            this.Map(x => x.IsRequired).Column("IsRequired").Nullable();

            this.References(x => x.LeadGroup).Column("LeadGrpId").LazyLoad();

            this.References(x => x.RequirementEffectiveDates).Column("adReqEffectiveDateId").NotFound.Ignore().LazyLoad();
        }
    }
}