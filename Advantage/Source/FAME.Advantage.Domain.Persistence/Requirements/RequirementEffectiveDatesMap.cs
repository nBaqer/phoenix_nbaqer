﻿using FAME.Advantage.Domain.Requirements;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Requirements
{
    public class RequirementEffectiveDatestMap : ClassMap<RequirementEffectiveDates>
    {
        public RequirementEffectiveDatestMap()
        {
            Table("adReqsEffectiveDates");

            Id(x => x.ID).Column("adReqEffectiveDateId").GeneratedBy.Guid();

            Map(x => x.StartDate).Column("StartDate").Nullable();
            Map(x => x.EndDate).Column("EndDate").Length(50).Nullable();
            Map(x => x.MinimumScore).Column("MinScore").Nullable();
            Map(x => x.IsMandatory).Column("MandatoryRequirement").Nullable();
            Map(x => x.ValidDays).Column("ValidDays").Nullable();

            References(x => x.Requirement).Column("adReqId").NotFound.Ignore().LazyLoad();
      

            HasMany(x => x.RequirementLeadGroupBridge).KeyColumn("adReqEffectiveDateId").LazyLoad();
        }
    }
}



