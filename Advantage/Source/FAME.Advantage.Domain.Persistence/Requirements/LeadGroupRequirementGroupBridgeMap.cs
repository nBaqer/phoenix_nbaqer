﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadGroupRequirementGroupBridgeMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.Requirements.LeadGroupRequirementGroupBridgeMap
// </copyright>
// <summary>
//   The lead group requirement group bridge map.
//   Mapping for Domain Entity LeadGroupRequirementGroupBridge with table adLeadGRPREQGroups
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Requirements
{
    using FAME.Advantage.Domain.Requirements;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The lead group requirement group bridge map.
    /// Mapping for Domain Entity LeadGroupRequirementGroupBridge with table adLeadGRPREQGroups
    /// </summary>
    public class LeadGroupRequirementGroupBridgeMap : ClassMap<LeadGroupRequirementGroupBridge>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadGroupRequirementGroupBridgeMap"/> class.
        /// </summary>
        public LeadGroupRequirementGroupBridgeMap()
        {
            this.Table("adLeadGrpReqGroups");

            this.Id(x => x.ID).Column("LeadGrpReqGrpId").GeneratedBy.Guid();
            this.Map(x => x.MinimumNumberRequirements).Column("NumReqs").Nullable();

            this.References(l => l.Status).Column("StatusId").LazyLoad();
            this.References(x => x.LeadGroup).Column("LeadGrpId").LazyLoad();
            this.References(x => x.RequirementGroup).Column("ReqGrpId").LazyLoad();
        }
    }
}
