﻿using FAME.Advantage.Domain.Requirements;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Requirements
{
    /// <summary>
    ///  Map table adEdLvls. this is know also as Previous Education.
    /// </summary>
    public class EducationLeveltMap : ClassMap<EducationLevel>
    {
        /// <summary>
        ///  Map table adEdLvls
        /// </summary>
        public EducationLeveltMap()
        {
            Table("adEdLvls");

            Id(x => x.ID).Column("EdLvlId").GeneratedBy.Guid();

            Map(x => x.Code).Column("EdLvlCode").Length(12).Not.Nullable();
            Map(x => x.Description).Column("EdLvlDescrip").Length(50).Not.Nullable();            

            References(x => x.Status).Column("StatusId").LazyLoad();
            References(x => x.CampusGroup).Column("CampGrpId").LazyLoad();

            HasMany(x => x.LeadList).Inverse().Cascade.None().LazyLoad();
        }
    }
}



