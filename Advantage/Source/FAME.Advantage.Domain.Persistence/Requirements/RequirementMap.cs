﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequirementMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.Requirements.RequirementMap
// </copyright>
// <summary>
//   Defines the RequirementMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Requirements
{
    using FAME.Advantage.Domain.Requirements;

    using FluentNHibernate.Mapping;
    using FluentNHibernate.Utils;

    /// <summary>
    /// The requirement map.
    /// Mapping for Domain Entity Requirement with table adREQS
    /// </summary>
    public class RequirementMap : ClassMap<Requirement>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequirementMap"/> class.
        /// </summary>
        public RequirementMap()
        {
            this.Table("adReqs");

            this.Id(x => x.ID).Column("adReqId").GeneratedBy.Guid();

            this.Map(x => x.Code).Column("Code").Length(50).Nullable();
            this.Map(x => x.Description).Column("Descrip").Length(50).Nullable();
            this.Map(x => x.RequiredForEnrollment).Column("ReqforEnrollment").Nullable();
            this.Map(x => x.RequiredForFinancialAid).Column("ReqforFinancialAid").Nullable();
            this.Map(x => x.RequiredForGraduation).Column("ReqforGraduation").Nullable();
            this.Map(x => x.CampusGroupId).Column("CampGrpId").Nullable();

            this.References(x => x.Status).Column("StatusId").Not.LazyLoad(); 
            this.References(x => x.CampusGroup).Column("CampGrpId").LazyLoad();
            this.References(x => x.Type).Column("adReqTypeId").Not.LazyLoad();
            this.References(x => x.SystemModule).Column("ModuleId").Not.LazyLoad();

            this.HasMany(x => x.EffectiveDates).KeyColumn("adReqId").Not.LazyLoad();
            this.HasMany(x => x.RequirementProgramVersionBridge).KeyColumn("adReqId").LazyLoad();

            this.HasMany(x => x.LeadDocuments).KeyColumns.Add("DocumentId").NotFound.Ignore().LazyLoad();
            this.HasMany(x => x.LeadEntranceTests).KeyColumns.Add("EntrTestId").NotFound.Ignore().LazyLoad();
            this.HasMany(x => x.EntranceOverRideTests).KeyColumns.Add("EntrTestId").NotFound.Ignore().LazyLoad();
            this.HasMany(x => x.LeadRecsReceived).KeyColumns.Add("DocumentId").NotFound.Ignore().LazyLoad();
            this.HasMany(x => x.LeadTranReceived).KeyColumns.Add("DocumentId").NotFound.Ignore().LazyLoad();
            this.HasMany(x => x.LeadTransactions).KeyColumns.Add("LeadRequirementId").NotFound.Ignore().LazyLoad();
        }
    }
}
