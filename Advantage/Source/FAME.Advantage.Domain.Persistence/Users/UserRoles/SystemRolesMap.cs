﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemRolesMap.cs" company="FAME">
//   2015, 2016
// </copyright>
// <summary>
//   Defines the SystemRolesMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.Users.UserRoles
{
    using FAME.Advantage.Domain.Users.UserRoles;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The system roles map.
    /// </summary>
    public class SystemRolesMap : ClassMap<SystemRoles>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemRolesMap"/> class. 
        /// </summary>
        public SystemRolesMap()
        {
            this.Table("sySysRoles");

            this.Id(x => x.ID).Column("SysRoleId").GeneratedBy.Identity();

            this.Map(x => x.Description).Column("Descrip").Not.Nullable().Length(80);
            this.References(x => x.Status).Column("StatusId").LazyLoad();
            this.Map(x => x.ModDate).Column("ModDate").Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Nullable();
            this.HasMany(x => x.Roles).KeyColumn("SysRoleId").LazyLoad();
        }
    }
}
