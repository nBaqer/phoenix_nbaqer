﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RolesMap.cs" company="FAME">
//   2014, 2016
// </copyright>
// <summary>
//   MAP for Table <code>syRoles</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Users.UserRoles
{
    using FAME.Advantage.Domain.Users.UserRoles;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// MAP for Table <code>syRoles</code>
    /// </summary>
    public class RolesMap : ClassMap<Roles>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RolesMap"/> class. 
        /// MAP for Table <code>syRoles</code>
        /// </summary>
        public RolesMap()
        {
            this.Table("syRoles");

            this.Id(x => x.ID).Column("RoleId").Not.Nullable().GeneratedBy.Guid();

            this.Map(x => x.Role).Column("Role").Not.Nullable().Length(60);
            this.Map(x => x.Code).Column("Code").Not.Nullable().Length(12);
            this.Map(x => x.ModDate).Column("ModDate").Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Nullable();

            this.References(x => x.Status).Column("StatusId").LazyLoad();
            this.References(x => x.SystemRole).Column("SysRoleId").LazyLoad();

            this.HasMany(x => x.ResourceLevels).KeyColumn("RoleId").LazyLoad();
            this.HasMany(x => x.UserByCampusGroupList).KeyColumn("RoleId").LazyLoad();
            this.HasMany(x => x.LeadStatusChangePermissionList).KeyColumn("RoleId").LazyLoad();

            this.HasMany(x => x.RoleModulesBridge).KeyColumn("RoleId").LazyLoad();
        }
    }
}
