﻿using FAME.Advantage.Domain.Users.UserRoles;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Users.UserRoles
{
    public class ResourceLevelsMap : ClassMap<ResourceLevels>
    {
        public ResourceLevelsMap()
        {
            Table("syRlsResLvls");

            Id(x => x.ID).Column("RRLId").Not.Nullable().GeneratedBy.Guid();


            Map(x => x.ResourceId).Column("ResourceId").Not.Nullable();
            Map(x => x.RoleId).Column("RoleId").Not.Nullable();
            Map(x => x.AccessLevel).Column("AccessLevel").Nullable().Length(100);
            Map(x => x.ModDate).Column("ModDate").Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable();
            Map(x => x.ParentId).Column("ParentId").Nullable();

            References(x => x.Resource).Column("ResourceId").LazyLoad();
            

        }
    }
}
    