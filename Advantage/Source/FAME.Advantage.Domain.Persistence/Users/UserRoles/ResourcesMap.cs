﻿using FAME.Advantage.Domain.Users.UserRoles;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Users.UserRoles
{
    public class ResourcesMap : ClassMap<Resources>
    {
        public ResourcesMap()
        {
            Table("syResources");

            Id(x => x.ID).Column("ResourceID").Not.Nullable().GeneratedBy.Identity();

            Map(x => x.Resource).Column("Resource").Nullable().Length(200);
            Map(x => x.ResourceUrl).Column("ResourceURL").Nullable().Length(100);
            Map(x => x.DisplayName).Column("DisplayName").Nullable().Length(100);
            Map(x => x.AllowSchlReqFields).Column("AllowSchlReqFlds").Nullable();
            Map(x => x.ModDate).Column("ModDate").Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable();

            References(x => x.ResourceTypeObj).Column("ResourceTypeId");
            HasMany(x => x.ResourceLevels).KeyColumn("ResourceId");

            HasMany(x => x.ResourceTableFieldsList).KeyColumn("ResourceId").LazyLoad();

            HasMany(x => x.ResourceUdfList).KeyColumn("ResourceId").LazyLoad();
            HasMany(x => x.ResourceRelatedUdfList).KeyColumn("RelatedResourceId").LazyLoad();
        }
    }
}
