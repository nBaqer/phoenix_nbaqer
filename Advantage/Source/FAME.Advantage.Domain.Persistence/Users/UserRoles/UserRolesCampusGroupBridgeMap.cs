﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserRolesCampusGroupBridgeMap.cs" company="FAME">
//   2014,2016
// </copyright>
// <summary>
//   MAP for table syUsersRolesCampGrps
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Users.UserRoles
{
    using FAME.Advantage.Domain.Users.UserRoles;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// MAP for table <code>syUsersRolesCampGrps</code>
    /// </summary>
    public class UserRolesCampusGroupBridgeMap : ClassMap<UserRolesCampusGroupBridge>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserRolesCampusGroupBridgeMap"/> class. 
        /// MAP for table <code>syUsersRolesCampGrps</code>
        /// </summary>
        public UserRolesCampusGroupBridgeMap()
        {
            this.Table("syUsersRolesCampGrps");

            this.Id(x => x.ID).Column("UserRoleCampGrpId").Not.Nullable().GeneratedBy.Guid();
            this.References(x => x.User).Column("UserId").LazyLoad();
            this.References(x => x.Role).Column("RoleId").LazyLoad();
            this.References(x => x.CampusGroup).Column("CampGrpId").LazyLoad();

            this.Map(x => x.ModUser).Column("ModUser").Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Nullable();
            
        }
    }
}