﻿using FAME.Advantage.Domain.Users;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.Users
{
    public class AdmissionRepMap : ClassMap<AdmissionRep>
    {
        public AdmissionRepMap()
        {
            Table("rptAdmissionsRep");

            Id(x => x.ID).Column("rptAdmissionsRepId").Not.Nullable().GeneratedBy.Guid();

            Map(x => x.Description).Column("AdmissionsRepDescrip").Nullable().Length(100);
            Map(x => x.AdmissionsRepId).Column("AdmissionsRepId").Not.Nullable();
            References(x => x.Status).Column("StatusId").LazyLoad();
           
        }
    }
}
