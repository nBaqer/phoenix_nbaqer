﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserTypeMap.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UserTypeMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Users
{
    using FAME.Advantage.Domain.Users;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The user type map.
    /// </summary>
    public class UserTypeMap : ClassMap<UserType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserTypeMap"/> class.
        /// </summary>
        public UserTypeMap()
        {
            this.Table("syUserType");
            this.Id(x => x.Id).Column("UserTypeId").Not.Nullable().GeneratedBy.Identity();
            this.Map(x => x.Code).Column("Code").Not.Nullable().Length(100);
            this.Map(x => x.Description).Column("Description").Nullable().Length(500);
            this.Map(x => x.ModifiedDate).Column("ModDate").Not.Nullable();
            this.Map(x => x.ModifiedUser).Column("ModUser").Not.Nullable().Length(100);
        }
    }
}
