﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UsersMap.cs" company="FAME Inc.">
//   FAME Inc.
// </copyright>
// <summary>
//   MAP for Table syUsers
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.Users
{
    using FAME.Advantage.Domain.Users;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// MAP for Table syUsers
    /// </summary>
    public class UsersMap : ClassMap<User>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UsersMap"/> class.
        /// </summary>
        public UsersMap()
        {
            this.Table("syUsers");

            this.Id(x => x.ID).Column("UserId").Not.Nullable().GeneratedBy.Guid();

            this.Map(x => x.Email).Column("EMail").Nullable().Length(100);
            this.Map(x => x.UserName).Column("UserName").Not.Nullable().Length(50);
            this.Map(x => x.AccountActive).Column("AccountActive").Nullable();
            this.Map(x => x.FullName).Column("FullName").Nullable().Length(100);

            this.HasMany(x => x.AdmissionRepList).KeyColumn("UserId").LazyLoad();
            this.HasMany(x => x.RolesByCampusList).KeyColumn("UserId").LazyLoad();
            this.HasMany(x => x.LeadMruList).KeyColumn("UserId").Inverse().LazyLoad();

            this.Map(x => x.IsAdvantageSuperUser).Column("IsAdvantageSuperUser").Not.Nullable();
            this.HasManyToMany(x => x.CampusGroup).Cascade.None().Table("syUsersRolesCampGrps").ParentKeyColumn("UserId").ChildKeyColumn("CampGrpId");

            this.References(x => x.UserType).Column("UserTypeId").LazyLoad();
        }
    }
}
