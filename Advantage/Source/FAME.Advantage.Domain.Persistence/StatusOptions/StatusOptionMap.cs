﻿using FAME.Advantage.Domain.StatusesOptions;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.StatusOptions
{
    /// <summary>
    /// Map syStatuses to StatusOption
    /// </summary>
    public class StatusOptionMap : ClassMap<StatusOption>
    {
        public StatusOptionMap()
        {
            Table("syStatuses");
            ReadOnly();
            Id(x => x.ID).Column("StatusId").Not.Nullable().GeneratedBy.Guid();
            Map(x => x.Status).Column("Status").Not.Nullable().Length(15);          
            Map(x => x.StatusCode).Column("StatusCode").Not.Nullable().Length(3);
            HasMany(x => x.Campuses).Inverse().Cascade.None().LazyLoad();
            HasMany(x => x.HolidaysList).Inverse().Cascade.None().LazyLoad();
        }
    }
}
