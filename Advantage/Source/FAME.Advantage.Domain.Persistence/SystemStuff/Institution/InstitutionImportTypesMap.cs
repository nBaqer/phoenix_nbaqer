﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionImportTypesMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.SystemStuff.Institution.InstitutionImportTypesMap
// </copyright>
// <summary>
//   The institution import types map.
//   Domain Mapping for table <code>syInstitutionImportTypes</code>.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Institution
{
    using Domain.SystemStuff.Intitution;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The institution import types map.
    /// Domain Mapping for table <code>syInstitutionImportTypes</code>.
    /// </summary>
    public class InstitutionImportTypesMap : ClassMap<InstitutionImportType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionImportTypesMap"/> class.
        /// </summary>
        public InstitutionImportTypesMap()
        {
            this.Table("syInstitutionImportTypes");

            this.Id(x => x.ID).Column("ImportTypeID").GeneratedBy.Identity();

            this.Map(x => x.Description).Column("Description").Nullable().Length(50);
        }
    }
}
