﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionPhoneMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.SystemStuff.Institution.InstitutionPhoneMap
// </copyright>
// <summary>
//   The institution phone map.
//   Domain Mapping for table <code>syInstitutionPhone</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Institution
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Domain.SystemStuff.Intitution;

    using FluentNHibernate.Mapping;
    using FluentNHibernate.Utils;

    /// <summary>
    /// The institution phone map.
    /// Domain Mapping for table <code>syInstitutionPhone</code>
    /// </summary>
    public class InstitutionPhoneMap : ClassMap<InstitutionPhone>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionPhoneMap"/> class.
        /// </summary>
        public InstitutionPhoneMap()
        {
            this.Table("syInstitutionPhone");

            this.Id(x => x.ID).Column("InstitutionPhoneId").GeneratedBy.Guid();

            this.Map(x => x.Phone).Column("Phone").Nullable().Length(50);
            this.Map(x => x.ModifiedDate).Column("ModDate").Not.Nullable();
            this.Map(x => x.ModifiedUser).Column("ModUser").Not.Nullable().Length(50);
            this.Map(x => x.IsForeignPhone).Column("IsForeignPhone").Nullable();
            this.Map(x => x.IsDefault).Column("IsDefault").Nullable();

            this.References(x => x.Institution).Column("InstitutionId").Not.Nullable().LazyLoad();
            this.References(x => x.Type).Column("PhoneTypeId").Nullable().LazyLoad();
            this.References(x => x.Status).Column("StatusId").Not.Nullable().LazyLoad();
        }
    }
}
