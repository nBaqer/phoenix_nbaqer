﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionTypesMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.SystemStuff.Institution.syInstitutionTypes
// </copyright>
// <summary>
//   The institution types map.
//   Domain Mapping for table <code>syInstitutionTypes</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Institution
{
    using Domain.SystemStuff.Intitution;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The institution types map.
    /// Domain Mapping for table <code>syInstitutionTypes</code>
    /// </summary>
    public class InstitutionTypesMap : ClassMap<InstitutionType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionTypesMap"/> class.
        /// </summary>
        public InstitutionTypesMap()
        {
            this.Table("syInstitutionTypes");

            this.Id(x => x.ID).Column("TypeID").GeneratedBy.Identity();
            this.Map(x => x.Description).Column("Description").Nullable().Length(50);
            this.HasMany(x => x.HighSchoolList).KeyColumn("TypeId").Cascade.None().LazyLoad(); 
        }
    }
}
