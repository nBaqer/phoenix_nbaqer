﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HighSchoolsMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.SystemStuff.Institution.HighSchoolsMap
// </copyright>
// <summary>
//   The high schools map.
//   Domain Mapping for Table <code>syInstitutions</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Institution
{
    using FAME.Advantage.Domain.SystemStuff.Intitution;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The high schools map.
    /// Domain Mapping for Table <code>syInstitutions</code>
    /// </summary>
    public class HighSchoolsMap : ClassMap<HighSchools>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HighSchoolsMap"/> class.
        /// </summary>
        public HighSchoolsMap()
        {
            this.Table("syInstitutions");

            this.Id(l => l.ID).Column("HSId").GeneratedBy.Guid();

            this.Map(l => l.HsCode).Column("HsCode").Length(12).Not.Nullable();
            this.Map(l => l.HsDescrip).Column("HSName").Length(50).Nullable();
            this.Map(l => l.ModifiedDate).Column("ModDate").Not.Nullable();
            this.Map(l => l.ModifiedUser).Column("ModUser").Nullable();
            
            this.References(x => x.SyStatusesObj).Column("StatusId").LazyLoad();
            this.References(x => x.CampusGroupObj).Column("CampGrpId").LazyLoad();
            this.References(x => x.Level).Column("LevelID").Nullable().LazyLoad();
            this.References(x => x.Type).Column("TypeId").Nullable();
            this.References(x => x.ImportType).Column("ImportTypeId").Nullable().LazyLoad();

            this.HasMany(x => x.LeadList).KeyColumn("PrgGrpId").Cascade.None().LazyLoad();
            this.HasMany(x => x.Phones).KeyColumn("InstitutionId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.Addresses).KeyColumn("InstitutionId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
            this.HasMany(x => x.Contacts).KeyColumn("InstitutionId").Cascade.AllDeleteOrphan().Inverse().LazyLoad();
        }
    }
}
