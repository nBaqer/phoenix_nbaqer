﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionAddressMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.SystemStuff.Institution.InstitutionAddressMap
// </copyright>
// <summary>
//   The institution address map.
//   Domain Mapping for table <code>syInstitutionAddresses</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Institution
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Domain.SystemStuff.Intitution;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The institution address map.
    /// Domain Mapping for table <code>syInstitutionAddresses</code>
    /// </summary>
    public class InstitutionAddressMap : ClassMap<InstitutionAddress>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionAddressMap"/> class.
        /// </summary>
        public InstitutionAddressMap()
        {
            this.Table("syInstitutionAddresses");

            this.Id(x => x.ID).Column("InstitutionAddressId").GeneratedBy.Guid();
            this.Map(x => x.Address1).Column("Address1").Length(250).Nullable();
            this.Map(x => x.Address2).Column("Address2").Length(250).Nullable();
            this.Map(x => x.City).Column("City").Length(250).Nullable();
            this.Map(x => x.ZipCode).Column("ZipCode").Length(10).Nullable();
            this.Map(x => x.IsMailingAddress).Column("IsMailingAddress").Not.Nullable();
            this.Map(x => x.ModifiedUser).Column("ModUser").Length(50).Nullable();
            this.Map(x => x.ModifiedDate).Column("ModDate").Length(50).Nullable();
            this.Map(x => x.IsInternational).Column("IsInternational").Not.Nullable();
            this.Map(x => x.ForeignCountry).Column("ForeignCountry").Length(100).Nullable();
            this.Map(x => x.ForeignCounty).Column("ForeignCountyStr").Length(100).Nullable();
            this.Map(x => x.AddressApto).Column("AddressApto").Length(20).Nullable();
            this.Map(x => x.OtherState).Column("OtherState").Length(50).Nullable();
            this.Map(x => x.IsDefault).Column("IsDefault").Not.Nullable();

            this.References(x => x.Institution).Column("InstitutionId").Not.Nullable().LazyLoad();
            this.References(x => x.AddressType).Column("AddressTypeId").Nullable().LazyLoad();
            this.References(x => x.State).Column("StateId").Nullable().LazyLoad();
            this.References(x => x.Status).Column("StatusId").Not.Nullable().LazyLoad();
            this.References(x => x.Country).Column("CountryId").Nullable().LazyLoad();
            this.References(x => x.County).Column("CountyId").Nullable().LazyLoad();
        }
    }
}
