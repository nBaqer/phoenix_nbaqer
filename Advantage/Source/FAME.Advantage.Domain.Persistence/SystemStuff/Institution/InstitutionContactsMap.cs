﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionContactsMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.SystemStuff.Institution.syInstitutionContacts
// </copyright>
// <summary>
//   The institution contacts map.
//   Domain Mapping for table <code>syInstitutionContacts</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Institution
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Domain.SystemStuff.Intitution;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The institution contacts map.
    /// Domain Mapping for table <code>syInstitutionContacts</code>
    /// </summary>
    public class InstitutionContactsMap : ClassMap<InstitutionContact>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionContactsMap"/> class.
        /// </summary>
        public InstitutionContactsMap()
        {
            this.Table("syInstitutionContacts");

            this.Id(x => x.ID).Column("InstitutionContactId").GeneratedBy.Guid();

            this.Map(x => x.FirstName).Column("FirstName").Length(50).Not.Nullable();
            this.Map(x => x.LastName).Column("LastName").Length(50).Not.Nullable();
            this.Map(x => x.MiddleName).Column("MiddleName").Length(50).Nullable();
            this.Map(x => x.Title).Column("Title").Length(100).Nullable();
            this.Map(x => x.Phone).Column("Phone").Length(50).Nullable();
            this.Map(x => x.PhoneExt).Column("PhoneExt").Length(50).Nullable();
            this.Map(x => x.IsForeignPhone).Column("ForeignPhone").Nullable();
            this.Map(x => x.Email).Column("Email").Length(50).Nullable();
            this.Map(x => x.ModifiedUser).Column("ModUser").Length(50).Nullable();
            this.Map(x => x.ModifiedDate).Column("ModDate").Nullable();
            this.Map(x => x.IsDefault).Column("IsDefault").Nullable();

            this.References(x => x.Institution).Column("InstitutionId").Not.Nullable().LazyLoad();
            this.References(x => x.Status).Column("StatusId").Nullable().LazyLoad();
            this.References(x => x.Prefix).Column("PrefixId").Nullable().LazyLoad();
            this.References(x => x.Suffix).Column("SuffixId").Nullable().LazyLoad();
        }
    }
}
