﻿using FAME.Advantage.Domain.SystemStuff;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    public class SystemStatusChangeHistoryMap: ClassMap<SystemStatusChangeHistory>
    {
        public SystemStatusChangeHistoryMap()
        {
            Table("syStudentStatusChanges");

            Id(x => x.ID).Column("StudentStatusChangeId").Not.Nullable();

            Map(x => x.CaseNumber).Column("CaseNumber").Nullable().Length(50);
            Map(x => x.IsReversal).Column("IsReversal").Not.Nullable();
            Map(x => x.DateOfChange).Column("DateOfChange").Nullable();
            Map(x => x.Lda).Column("Lda").Nullable();
            Map(x => x.ModifiedDate).Column("ModDate").Nullable();
            Map(x => x.ModifiedUser).Column("ModUser").Nullable();
            Map(x => x.RequestedBy).Column("RequestedBy").Nullable().Length(100);
            Map(x => x.DropReasonId).Column("DropReasonId").Nullable();
            Map(x => x.HaveBackup).Column("HaveBackup").Nullable();
            Map(x => x.HaveClientConfirmation).Column("HaveClientConfirmation").Nullable();

            References(x => x.Campus).Column("CampusId").LazyLoad();
            
            References(x => x.Enrollment).Column("StuEnrollId").LazyLoad().Not.Nullable();
            References(x => x.NewStatus).Column("NewStatusId").LazyLoad().Nullable();
            References(x => x.OriginalStatus).Column("OrigStatusId").LazyLoad().Nullable();

        }
    }
}


    