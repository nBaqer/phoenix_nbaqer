﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyWapiOperationLoggerMap.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the SyWapiOperationLoggerMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    using FAME.Advantage.Domain.SystemStuff;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The SYWAPI operation logger map.
    /// </summary>
    public class SyWapiOperationLoggerMap : ClassMap<SyWapiOperationLogger>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyWapiOperationLoggerMap"/> class.
        /// </summary>
        public SyWapiOperationLoggerMap()
        {
            this.Table("SyWapiOperationLogger");

            this.Id(x => x.ID).Column("Id").Unique().GeneratedBy.Native();
            this.Map(x => x.Comment).Column("Comment").Nullable();
            this.Map(x => x.CompanyCode).Column("CompanyCode").Not.Nullable();
            this.Map(x => x.DateExecution).Column("DateExecution").Not.Nullable();
            this.Map(x => x.IsError).Column("IsError").Not.Nullable();
            this.Map(x => x.NextPlanningDate).Column("NextPlanningDate").Nullable();
            this.Map(x => x.ServiceCode).Column("ServiceCode").Not.Nullable();
       }
    }
}