﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemStatesMap.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the SystemStatesMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    using FAME.Advantage.Domain.SystemStuff;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The system states map.
    /// USA States
    /// </summary>
    public class SystemStatesMap : ClassMap<SystemStates>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemStatesMap"/> class.
        /// </summary>
        public SystemStatesMap()
        {
            this.Table("syStates");

            this.ReadOnly();

            this.Id(x => x.ID).Column("StateId").Not.Nullable();

            this.Map(x => x.Description).Column("StateDescrip").Not.Nullable().Length(80);
            this.Map(x => x.Code).Column("StateCode").Not.Nullable().Length(12);

            this.References(x => x.StatusObj).Column("StatusId").Not.Nullable().LazyLoad();
        }
    }
}