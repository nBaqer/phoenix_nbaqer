﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyWapiExternalOperationModeMap.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the SyWapiExternalOperationModeMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    using FAME.Advantage.Domain.SystemStuff;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The SYWAPI external operation mode map.
    /// </summary>
    public class SyWapiExternalOperationModeMap: ClassMap<SyWapiExternalOperationMode>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyWapiExternalOperationModeMap"/> class.
        /// </summary>
        public SyWapiExternalOperationModeMap()
        {
            this.Table("SyWapiExternalOperationMode");

            this.Id(x => x.ID).Column("Id").Unique().GeneratedBy.Native();
            this.Map(x => x.Code).Column("Code").Not.Nullable().Unique().Length(30);
            this.Map(x => x.Description).Column("Description").Nullable().Length(100);
            this.HasMany(x => x.WapiSettingsList).KeyColumn("IdExtOperation");
        }
    }
}
