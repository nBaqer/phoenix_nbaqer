﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyWapiAllowedServicesMap.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the SyWapiAllowedServicesMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    using FAME.Advantage.Domain.SystemStuff;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The SYWAPI allowed services map.
    /// </summary>
    public class SyWapiAllowedServicesMap : ClassMap<SyWapiAllowedServices>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyWapiAllowedServicesMap"/> class.
        /// </summary>
        public SyWapiAllowedServicesMap()
        {
            this.Table("SyWapiAllowedServices");

            this.Id(x => x.ID).Column("Id").Unique().GeneratedBy.Native();
            this.Map(x => x.Code).Column("Code").Not.Nullable().Unique().Length(30);
            this.Map(x => x.Description).Column("Description").Not.Nullable().Length(100);
            this.Map(x => x.Url).Column("Url").Not.Nullable().Length(2048);
            this.Map(x => x.IsActive).Column("IsActive").Not.Nullable();
            this.HasMany(x => x.WapiSettingsList).KeyColumn("Id");
            this.HasMany(x => x.SecondWapingSettingList).KeyColumn("Id");

            this.HasManyToMany(x => x.WapiExternalCompaniesList)
                .Cascade.All()
                .Inverse()
                .Table("syWapiBridgeExternalCompanyAllowedServices")
            .ParentKeyColumn("IdAllowedServices")
            .ChildKeyColumn("IdExternalCompanies");
        }
    }
}
