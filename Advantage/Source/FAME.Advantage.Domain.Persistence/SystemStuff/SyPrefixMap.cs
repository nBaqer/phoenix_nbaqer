﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyPrefixMap.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Domain Mapping for syPrefixes
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    using FluentNHibernate.Mapping;
    using Prefix = Domain.SystemStuff.Prefix;

    /// <summary>
    /// Domain Mapping for <code>syPrefixes</code>
    /// </summary>
    public class SyPrefixMap : ClassMap<Prefix>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyPrefixMap"/> class. 
        /// Default Constructor
        /// </summary>
        public SyPrefixMap()
        {
            this.Table("syPrefixes");
            this.Id(x => x.ID).Column("PrefixId").Not.Nullable().GeneratedBy.Guid();
            this.Map(x => x.Code).Column("PrefixCode").Nullable().Length(12);
            this.Map(x => x.Description).Column("PrefixDescrip").Not.Nullable().Length(50);
            this.Map(x => x.StatusId).Column("StatusId").Not.Nullable();
            this.Map(x => x.CampusGroupId).Column("CampGrpId").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Not.Nullable().Length(50);
            this.Map(x => x.ModDate).Column("ModDate").Not.Nullable();

            this.References(x => x.Status).Column("StatusId").LazyLoad();
        }
    }
}
