﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusChangeDeleteReasonsMap.cs" company="FAME Inc.">
//   FAME.Advantage.Domain.Persistence.SystemStuff
// </copyright>
// <summary>
//   Defines the StatusChangeDeleteReasonsMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    using FAME.Advantage.Domain.SystemStuff;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The status change delete reasons map.
    /// </summary>
    public class StatusChangeDeleteReasonsMap : ClassMap<StatusChangeDeleteReasons>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StatusChangeDeleteReasonsMap"/> class.
        /// </summary>
        public StatusChangeDeleteReasonsMap()
        {
            this.Table("syStatusChangeDeleteReasons");

            this.Id(x => x.ID).Column("Id").Not.Nullable();

            this.Map(x => x.Description).Column("Description").Not.Nullable().Length(50);
            this.Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Not.Nullable().Length(50);
        }
    }
}
