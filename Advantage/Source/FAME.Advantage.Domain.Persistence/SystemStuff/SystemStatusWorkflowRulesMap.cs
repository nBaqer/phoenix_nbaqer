﻿using FAME.Advantage.Domain.SystemStuff;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    class SystemStatusWorkflowRulesMap : ClassMap<SystemStatusWorkflowRules>
    {
        public SystemStatusWorkflowRulesMap()
        {
            Table("sySystemStatusWorkflowRules");

            ReadOnly();

            Id(x => x.ID).Column("Id").Not.Nullable();

            Map(x => x.AllowDelete).Column("AllowDelete").Not.Nullable();
            Map(x => x.AllowEdit).Column("AllowEdit").Not.Nullable();
            Map(x => x.AllowEditSameStatusType).Column("AllowEditSameStatusType").Not.Nullable();
            Map(x => x.AllowInsert).Column("AllowInsert").Not.Nullable();
            Map(x => x.AllowInsertSameStatusType).Column("AllowInsertSameStatusType").Not.Nullable();
            Map(x => x.AllowedInBatch).Column("AllowedInBatch").Not.Nullable();
            Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Not.Nullable().Length(50);
            Map(x => x.UniqueId).Column("UniqueId").Not.Nullable();

            References(x => x.StatusFrom).Column("StatusIdFrom").LazyLoad().Nullable();
            References(x => x.StatusTo).Column("StatusIdTo").LazyLoad().Not.Nullable();
        }
    }

}