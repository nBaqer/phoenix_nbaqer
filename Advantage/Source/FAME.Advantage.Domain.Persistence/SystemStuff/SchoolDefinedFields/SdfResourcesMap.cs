﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SdfResourcesMap.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Map for table syResourceSdf
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.SchoolDefinedFields
{
    using Domain.SystemStuff.SchoolDefinedFields;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Map for table <code>syResourceSdf</code>
    /// </summary>
    public class SdfResourcesMap : ClassMap<SdfResources>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SdfResourcesMap"/> class. 
        /// Constructor Mapping
        /// </summary>
        public SdfResourcesMap()
        {
            this.Table("syResourceSdf");
            this.Id(x => x.ID).Column("PKId").Not.Nullable().GeneratedBy.GuidNative();
            this.Map(x => x.SdfVisibility).Column("SDFVisibilty").Nullable();
            this.Map(x => x.Position).Column("Position").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Nullable();

            this.References(x => x.SdfObj).Column("sdfId");
            this.References(x => x.ResourceObj).Column("ResourceId");
            this.References(x => x.EntityObj).Column("EntityId");
        }
    }
}