﻿using FAME.Advantage.Domain.SystemStuff.SchoolDefinedFields;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff.SchoolDefinedFields
{
    /// <summary>
    /// This mapping a view VIEW_SySDFModuleValue_Lead
    /// </summary>
    public class SdfModuleValueLeadMap : ClassMap<SdfModuleValueLead>
    {
        /// <summary>
        /// Constructor Mapping
        /// </summary>
        public SdfModuleValueLeadMap()
        {
            Table("VIEW_SySDFModuleValue_Lead");
            Id(x => x.ID).Column("SDFPKID").Not.Nullable().GeneratedBy.Guid();
            Map(x => x.SdfValue).Column("SdfValue").Nullable().Length(50);
            Map(x => x.ModUser).Column("ModUser").Nullable();
            Map(x => x.ModDate).Column("ModDate").Nullable();

            References(x => x.SdfObj).Column("sdfId");
            References(x => x.LeadObj).Column("LeadId");
        }
    }
}