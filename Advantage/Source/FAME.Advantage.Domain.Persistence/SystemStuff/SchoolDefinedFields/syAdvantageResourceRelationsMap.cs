﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyAdvantageResourceRelationsMap.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Mapping for Table syAdvantageResourceRelations
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.SchoolDefinedFields
{
    using Domain.SystemStuff.SchoolDefinedFields;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Mapping for Table syAdvantageResourceRelations
    /// </summary>
    public class SyAdvantageResourceRelationsMap : ClassMap<SyAdvantageResourceRelations>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyAdvantageResourceRelationsMap()"/> class.
        ///  Mapping for Table syAdvantageResourceRelations
        /// </summary>
        public SyAdvantageResourceRelationsMap()
        {
            Table("syAdvantageResourceRelations");

            Id(l => l.ID).Column("ResRelId").Unique().GeneratedBy.Identity();
            References(x => x.ResourceObj).Column("ResourceId").Nullable().LazyLoad();
            References(x => x.RelatedResourceObj).Column("ResourceId").Nullable().LazyLoad();
        }
    }
}

