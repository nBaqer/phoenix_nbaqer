﻿using FAME.Advantage.Domain.SystemStuff.SchoolDefinedFields;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff.SchoolDefinedFields
{
    public class SdfValListMap : ClassMap<SdfValList>
    {
        /// <summary>
        /// Constructor Mapping
        /// </summary>
        public SdfValListMap()
        {
            Table("sySdfValList");
            Id(x => x.ID).Column("SdfListId").Not.Nullable().GeneratedBy.Guid();
            Map(x => x.ValList).Column("ValList").Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable();
            Map(x => x.ModDate).Column("ModDate").Nullable();

            References(x => x.SdfObj).Column("SDFId");
        }
    }
}