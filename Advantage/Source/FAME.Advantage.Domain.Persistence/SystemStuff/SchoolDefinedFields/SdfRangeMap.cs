﻿using FAME.Advantage.Domain.SystemStuff.SchoolDefinedFields;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff.SchoolDefinedFields
{
    /// <summary>
    /// Mapping of table sySdfRange
    /// </summary>
    public class SdfRangeMap : ClassMap<SdfRange>
    {
        /// <summary>
        /// Constructor Mapping
        /// </summary>
        public SdfRangeMap()
        {
            Table("sySdfRange");
            Id(x => x.ID).Column("SdfRangeId").Not.Nullable().GeneratedBy.Guid();
            Map(x => x.MaxVal).Column("MaxVal").Nullable();
            Map(x => x.MinVal).Column("MinVal").Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable();
            Map(x => x.ModDate).Column("ModDate").Nullable();

            References(x => x.SdfObj).Column("sdfId");
        }
    }
}