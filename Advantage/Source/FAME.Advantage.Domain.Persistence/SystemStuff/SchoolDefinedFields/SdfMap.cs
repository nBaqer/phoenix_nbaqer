﻿using FAME.Advantage.Domain.SystemStuff.SchoolDefinedFields;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff.SchoolDefinedFields
{
    /// <summary>
    /// MAP for sySDF (UDF)
    /// </summary>
    public class SdfMap: ClassMap<Sdf>
    {
        /// <summary>
        /// Constructor Mapping
        /// </summary>
        public SdfMap()
        {
            this.Table("sySDF");
            this.Id(x => x.ID).Column("SDFId").Not.Nullable().GeneratedBy.Guid();
            this.Map(x => x.SdfDescrip).Column("SDFDescrip").Not.Nullable();
            this.Map(x => x.DtypeId).Column("DTypeId").Not.Nullable();
            this.Map(x => x.HelpText).Column("HelpText").Nullable();
            this.Map(x => x.Len).Column("Len").Nullable();
            this.Map(x => x.Decimals).Column("Decimals").Nullable();
            this.Map(x => x.ValTypeId).Column("ValTypeId").Nullable();
            this.Map(x => x.IsRequired).Column("IsRequired").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Nullable();
           
            this.References(x => x.StatusObj).Column("StatusId");

            this.HasMany(x => x.ValuesList).KeyColumn("SDFId");
            this.HasMany(x => x.RangeList).KeyColumn("SDFId");
            this.HasMany(x => x.ResourcesList).KeyColumn("sdfID");
            this.HasMany(x => x.SdfModuleValuesList).KeyColumn("SDFID");
        }
    }
}