﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RptAgenciesMap.cs" company="FAME INC">
//   2016
// </copyright>
// <summary>
//   The rpt agencies map.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Resources
{
    using FAME.Advantage.Domain.SystemStuff.Resources;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The rpt agencies map.
    /// </summary>
    public class RptAgenciesMap : ClassMap<RptAgencies>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RptAgenciesMap"/> class.
        /// </summary>
        public RptAgenciesMap()
        {
            this.Table("syRptAgencies");
            this.Id(l => l.ID).Column("RptAgencyId").Not.Nullable();
            this.Map(l => l.Descrip).Column("Descrip").Nullable().LazyLoad();
        }
    }
}
