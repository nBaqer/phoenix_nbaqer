﻿using FAME.Advantage.Domain.SystemStuff.Resources;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Resources
{
    /// <summary>
    /// Mapping for Table syFields
    /// </summary>
    public class FieldsMap : ClassMap<Fields>
    {
        public FieldsMap()
        {
           
            Table("syFields");

            Id(x => x.ID).Column("FldId").GeneratedBy.Assigned().Unique();
            Map(x => x.FieldLen).Column("FldLen").Not.Nullable();
            Map(x => x.FldName).Column("FldName").Nullable().Length(200);
      
            HasMany(x => x.FieldsCaptionsList).KeyColumn("FldId");
            HasMany(x => x.TableFieldsList).KeyColumn("FldId");
        }
    }
}
