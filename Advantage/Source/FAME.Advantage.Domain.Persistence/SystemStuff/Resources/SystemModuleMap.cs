﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemModuleMap.cs" company="FAME Inc.">
//   FAME Inc. 2013
//   FAME.Advantage.Domain.Persistence.SystemStuff.Resources.SystemModuleMap
// </copyright>
// <summary>
//   Map for <code>SyModules</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Resources
{
    using Domain.SystemStuff.Resources;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Map for <code>SyModules</code>
    /// </summary>
    public class SystemModuleMap : ClassMap<SystemModule>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemModuleMap"/> class. 
        /// Mapping for <code> SyModules </code>
        /// </summary>
        public SystemModuleMap()
        {
            this.ReadOnly();

            this.Table("syModules");

            this.Id(x => x.ID).Column("ModuleId").GeneratedBy.Identity();

            this.Map(x => x.Name).Column("ModuleName").Not.Nullable();
            this.Map(x => x.Code).Column("ModuleCode").Not.Nullable();

            this.HasMany(x => x.AllNotesList).KeyColumn("ModuleCode").PropertyRef("Code").LazyLoad();

            // this.HasMany(x => x.UniversalSearchModules).KeyColumn("ModuleId").LazyLoad();
        }
    }
}
