﻿using FAME.Advantage.Domain.SystemStuff.Resources;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Resources
{
    /// <summary>
    /// Mapping Table syTblFlds
    /// </summary>
    public class TableFieldsMap : ClassMap<TableFields>
    {
        public TableFieldsMap()
        {
           
            Table("syTblFlds");

            Id(x => x.ID).Column("TblFldsId").GeneratedBy.Assigned().Unique();
            References(x => x.FieldObj).Column("FldId").Nullable();
            HasMany(x => x.ResourceTableFieldsList).KeyColumn("TblFldsId");
        }
    }
}
