﻿using FAME.Advantage.Domain.SystemStuff.Resources;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Resources
{
    public class ResourceTypesMap: ClassMap<ResourceTypes>
    {
        public ResourceTypesMap()
        {
           
            Table("syResourceTypes");

            Id(x => x.ID).Column("ResourceTypeID").GeneratedBy.Assigned().Unique();
            Map(x => x.ResourceType).Column("ResourceType").Nullable().Length(50);

            HasMany(x => x.ResourcesList).KeyColumn("ResourceTypeID");
        }
    }
}
