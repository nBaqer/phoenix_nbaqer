﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Resources
{
    using FAME.Advantage.Domain.SystemStuff.Resources;

    using FluentNHibernate.Mapping;

    public class RptFieldsMap : ClassMap<RptFields>
    {
        public RptFieldsMap()
        {
            this.Table("syRptFields");
            this.Id(l => l.ID).Column("RptFldId").Not.Nullable();
            this.Map(l => l.Descrip).Column("Descrip").Length(50).Nullable().LazyLoad();
            this.References(l => l.Field).Column("FldId").Nullable();
        }
    }
}
