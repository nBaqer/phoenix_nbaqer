﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Resources
{
    using FAME.Advantage.Domain.SystemStuff.Resources;

    using FluentNHibernate.Mapping;

    public class RptAgencyFieldsMap : ClassMap<RptAgencyFields>
    {
        public RptAgencyFieldsMap()
        {
            this.Table("syRptAgencyFields");
            this.Id(i => i.ID).Column("RptAgencyFldId").Not.Nullable();
            this.References(i => i.Fields).Column("RptFldId");
            this.References(i => i.RptAgencyId).Column("RptAgencyId");
        }
    }
}
