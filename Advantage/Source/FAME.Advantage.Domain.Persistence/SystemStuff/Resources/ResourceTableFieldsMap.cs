﻿using FAME.Advantage.Domain.SystemStuff.Resources;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Resources
{
    public class ResourceTableFieldsMap : ClassMap<ResourceTableFields>
    {
        public ResourceTableFieldsMap()
        {
           
            Table("syResTblFlds");

            Id(x => x.ID).Column("ResDefId").GeneratedBy.Assigned().Unique();
            Map(x => x.IsRequired).Column("Required").Not.Nullable();
            Map(x => x.SchlReq).Column("SchlReq").Not.Nullable();

            References(x => x.ResourceObj).Column("ResourceId").Not.Nullable();
            References(x => x.TableFieldsObj).Column("TblFldsId").Not.Nullable();
     
        }
    }
}
