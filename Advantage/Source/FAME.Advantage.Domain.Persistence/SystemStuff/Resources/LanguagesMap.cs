﻿using FAME.Advantage.Domain.SystemStuff.Resources;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Resources
{
    /// <summary>
    /// Mapping table syLangs
    /// </summary>
    public class LanguagesMap: ClassMap<Languages>
    {
        public LanguagesMap()
        {
           
            Table("syLangs");

            Id(x => x.ID).Column("LangId").GeneratedBy.Assigned().Unique();
            Map(x => x.LangName).Column("LangName").Not.Nullable().Length(5);

            HasMany(x => x.FieldsCaptionsList).KeyColumn("LangId");
        }
    }
}
