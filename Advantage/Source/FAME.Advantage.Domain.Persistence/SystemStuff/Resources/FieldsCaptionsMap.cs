﻿using FAME.Advantage.Domain.SystemStuff.Resources;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Resources
{
    /// <summary>
    /// MApping for Table syFldCaptions
    /// </summary>
    public class FieldsCaptionsMap: ClassMap<FieldsCaptions>
    {
        public FieldsCaptionsMap()
        {
           
            Table("syFldCaptions");

            Id(x => x.ID).Column("FldCapId").GeneratedBy.Assigned().Unique();
            Map(x => x.FieldDescription).Column("FldDescrip").Nullable().Length(150);
            Map(x => x.Caption).Column("Caption").Nullable().Length(100);

            References(x => x.LenguagesObj).Column("LangId").Not.Nullable();
            References(x => x.FieldsObj).Column("FldId").Not.Nullable();
        }
    }
}
