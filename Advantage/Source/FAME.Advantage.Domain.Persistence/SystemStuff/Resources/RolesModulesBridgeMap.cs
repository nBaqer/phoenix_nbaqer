﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RolesModulesBridgeMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.SystemStuff.Resources.RolesModulesBridgeMap
// </copyright>
// <summary>
//   The roles modules bridge map.
//   Mapping of domain entity RolesModulesBridge for table <code>syRolesModules</code>
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Resources
{
    using FAME.Advantage.Domain.SystemStuff.Resources;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The roles modules bridge map.
    /// Mapping of domain entity RolesModulesBridge for table <code>syRolesModules</code>
    /// </summary>
    public class RolesModulesBridgeMap : ClassMap<RolesModulesBridge>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RolesModulesBridgeMap"/> class.
        /// </summary>
        public RolesModulesBridgeMap()
        {
            this.Table("syRolesModules");
            this.ReadOnly();

            this.Id(x => x.ID).Column("RoleModuleId").GeneratedBy.Guid();
            this.References(x => x.Role).Column("RoleId").LazyLoad();
            this.References(x => x.Resource).Column("ModuleId").LazyLoad();
        }
    }
}
