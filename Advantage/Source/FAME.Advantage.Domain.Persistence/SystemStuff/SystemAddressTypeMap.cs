﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.SystemStuff;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    public class SystemAddressTypeMap: ClassMap<PlAddressTypes>
    {
        public SystemAddressTypeMap()
        {
            Table("plAddressTypes");

            Id(s => s.ID).Column("AddressTypeId").Not.Nullable().GeneratedBy.Guid();

            Map(s => s.AddressDescrip).Column("AddressDescrip").Length(50).Not.Nullable();
            Map(s => s.AddressCode).Column("AddressCode").Length(12).Not.Nullable();

            References(s => s.SyStatusesObj).Column("StatusId").Not.Nullable();
        }
    }
}
