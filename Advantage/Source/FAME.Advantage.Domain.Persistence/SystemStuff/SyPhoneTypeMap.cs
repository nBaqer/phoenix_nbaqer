﻿using FAME.Advantage.Domain.SystemStuff;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    /// <summary>
    /// Map for table syPhoneType
    /// </summary>
    public class SyPhoneTypeMap : ClassMap<SyPhoneType>
    {
        /// <summary>
        /// Map for table syPhoneType
        /// </summary>
        public SyPhoneTypeMap()
        {
            Table("syPhoneType");

            Id(s => s.ID).Column("PhoneTypeId").Not.Nullable().GeneratedBy.Guid();

            Map(s => s.PhoneTypeCode).Column("PhoneTypeCode").Length(12).Nullable();
            Map(s => s.PhoneTypeDescrip).Column("PhoneTypeDescrip").Length(50).Not.Nullable();
            
            References(s => s.SyStatusesObj).Column("StatusId").Nullable();

            HasMany(x => x.LeadPhoneList).KeyColumn("PhoneType").Inverse().Cascade.None();
            
        }
    }
}
