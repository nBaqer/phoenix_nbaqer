﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyStatusesMap.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   MAP table syStatuses
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    using FAME.Advantage.Domain.SystemStuff;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// MAP table <code>syStatuses</code>
    /// </summary>
    public class SyStatusesMap : ClassMap<SyStatuses>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyStatusesMap"/> class. 
        /// Constructor Mapping
        /// </summary>
        public SyStatusesMap()
        {
            this.Table("syStatuses");
            this.ReadOnly();
            this.Id(x => x.ID).Column("StatusId").Not.Nullable().GeneratedBy.Guid();
            this.Map(x => x.Status).Column("Status").Not.Nullable().Length(15);
            this.Map(x => x.StatusCode).Column("StatusCode").Nullable().Length(3);
            this.HasMany(x => x.Campuses).Inverse().Cascade.None().LazyLoad();
            this.HasMany(x => x.HolidaysList).Inverse().Cascade.None().LazyLoad();
            this.HasMany(x => x.ProgramGroupList).Inverse().Cascade.None().LazyLoad();
            this.HasMany(x => x.PrefixesList).Inverse().Cascade.None().LazyLoad();
            this.HasMany(x => x.SuffixesList).Inverse().Cascade.None().LazyLoad();
            this.HasMany(x => x.ProgramList).Inverse().Cascade.None().LazyLoad();
            this.HasMany(x => x.AttendTypesList).Inverse().Cascade.None().LazyLoad();
            this.HasMany(x => x.ReasonNotEnrolledList).Inverse().Cascade.None().LazyLoad();
        }
    }
}