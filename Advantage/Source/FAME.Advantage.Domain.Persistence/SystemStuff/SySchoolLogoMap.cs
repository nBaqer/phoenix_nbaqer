﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SySchoolLogoMap.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Mapping sySchoolLogo Table to domain.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    using FAME.Advantage.Domain.SystemStuff;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Mapping <code>sySchoolLogo</code> Table to domain.
    /// </summary>
    public class SySchoolLogoMap : ClassMap<SySchoolLogo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SySchoolLogoMap"/> class.
        /// </summary>
        public SySchoolLogoMap()
        {
            this.Table("SySchoolLogo");

            this.Id(x => x.ID).Column("ImgId").GeneratedBy.Assigned();
            this.Map(x => x.ContentType).Column("ContentType").Nullable();
            this.Map(x => x.ImagenBytes).Column("Image").Not.Nullable().Length(50000);
            this.Map(x => x.ImgLenth).Column("ImgLen").Nullable();
            this.Map(x => x.ImageFile).Column("imgFile").Nullable();
            this.Map(x => x.OfficialUse).Column("OfficialUse").Nullable();
            this.Map(x => x.ImageCode).Column("ImageCode").Nullable();
            this.Map(x => x.Description).Column("Description").Nullable();
        }
    }
}
