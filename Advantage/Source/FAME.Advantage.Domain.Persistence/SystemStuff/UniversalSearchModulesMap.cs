﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UniversalSearchModulesMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.SystemStuff.UniversalSearchModulesMap
// </copyright>
// <summary>
//   The universal search modules map.
//   Mapping for Domain Entity <code>UniversalSearchModules</code> with table <code>syUniversalSearchModules</code> which hold the list of items that are searchable from the Universal Search drop down.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.Domain.SystemStuff;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The universal search modules map.
    /// Mapping for Domain Entity <code>UniversalSearchModules</code> with table <code>syUniversalSearchModules</code> which hold the list of items that are searchable from the Universal Search drop down.
    /// </summary>
    public class UniversalSearchModulesMap : ClassMap<UniversalSearchModule>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UniversalSearchModulesMap"/> class.
        /// </summary>
        public UniversalSearchModulesMap()
        {
            this.Table("syUniversalSearchModules");
            this.ReadOnly();

            this.Id(x => x.ID).Column("UniversalSearchId").GeneratedBy.Identity();
            this.Map(x => x.Code).Column("Code").Not.Nullable().Length(70);
            this.Map(x => x.Description).Column("Description").Not.Nullable().Length(70);
            this.Map(x => x.ModifiedDate).Column("ModifiedDate").Not.Nullable();
            this.Map(x => x.ModifiedUser).Column("ModifiedUser").Not.Nullable().Length(50);
            this.References(x => x.Resource).Column("ResourceId").Not.Nullable().LazyLoad();
        }
    }
}
