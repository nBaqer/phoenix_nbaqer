﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.SystemStuff;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    public class SystemEmailTypeMap: ClassMap<SyEmailType>
    {
        public SystemEmailTypeMap()
        {
            Table("syEmailType");

            Id(s => s.ID).Column("EMailTypeId").Not.Nullable().GeneratedBy.Guid();

            Map(s => s.EMailTypeDescription).Column("EMailTypeDescription").Length(100).Not.Nullable();
            Map(s => s.EMailTypeCode).Column("EMailTypeCode").Length(50).Not.Nullable();

        }
    }
}
