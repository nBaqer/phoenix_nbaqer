﻿using FAME.Advantage.Domain.SystemStuff;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    public class SyWapiSettingsMap : ClassMap<SyWapiSettings>
    {
        public SyWapiSettingsMap()
        {
           // ReadOnly();

            Table("SyWapiSettings");

            Id(x => x.ID).Column("Id").GeneratedBy.Native().Unique();
            Map(x => x.CodeOperation).Column("CodeOperation").Not.Nullable().Length(30);
            Map(x => x.ExternalUrl).Column("ExternalUrl").Not.Nullable().Length(2048);
            Map(x => x.ConsumerKey).Column("ConsumerKey").Nullable().Length(100);
            Map(x => x.PrivateKey).Column("PrivateKey").Nullable().Length(100);
            Map(x => x.OperationSecondTimeInterval).Column("OperationSecondTimeInterval").Not.Nullable();
            Map(x => x.PollSecondForOnDemandOperation).Column("PollSecondForOnDemandOperation").Nullable();
            Map(x => x.FlagOnDemandOperation).Column("FlagOnDemandOperation").Nullable();
            Map(x => x.FlagRefreshConfiguration).Column("FlagRefreshConfiguration").Not.Nullable();
            Map(x => x.DateLastExecution).Column("DateLastExecution").Not.Nullable();
            Map(x => x.IsActive).Column("IsActive").Not.Nullable();
            Map(x => x.UserName).Column("UserName").Nullable();
            Map(x => x.FirstAllowedServiceQueryString).Column("FirstAllowedServiceQueryString").Nullable();
            References(x => x.ExternalCompanyObj).Column("IdExtCompany").Not.Nullable();
            References(x => x.ExternalOperationModeObj).Column("IdExtOperation").Not.Nullable();
            References(x => x.AllowedServiceObj).Column("IdAllowedService").Not.Nullable();
            References(x => x.SecondAllowedServiceObj).Column("IdSecondAllowedService").Nullable();
        }
    }
}
