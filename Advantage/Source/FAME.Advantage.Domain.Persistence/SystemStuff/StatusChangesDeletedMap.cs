﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusChangesDeletedMap.cs" company="FAME Inc.">
//   FAME.Advantage.Domain.Persistence.SystemStuff
// </copyright>
// <summary>
//   Defines the StatusChangesDeletedMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    using FAME.Advantage.Domain.SystemStuff;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The status changes deleted map.
    /// </summary>
    public class StatusChangesDeletedMap : ClassMap<StatusChangesDeleted>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StatusChangesDeletedMap"/> class. 
        /// </summary>
        public StatusChangesDeletedMap()
        {
            this.Table("syStatusChangesDeleted");

            this.Id(x => x.ID).Column("Id").Not.Nullable();

            this.Map(x => x.CaseNumber).Column("CaseNumber").Nullable().Length(50);
            this.Map(x => x.IsReversal).Column("IsReversal").Not.Nullable();
            this.Map(x => x.DateOfChange).Column("DateOfChange").Nullable();
            this.Map(x => x.Lda).Column("Lda").Nullable();
            this.Map(x => x.ModifiedDate).Column("ModDate").Nullable();
            this.Map(x => x.ModifiedUser).Column("ModUser").Nullable();
            this.Map(x => x.RequestedBy).Column("RequestedBy").Nullable().Length(100);
            this.Map(x => x.DropReasonId).Column("DropReasonId").Nullable();
            this.Map(x => x.HaveBackup).Column("HaveBackup").Nullable();
            this.Map(x => x.HaveClientConfirmation).Column("HaveClientConfirmation").Nullable();
            this.Map(x => x.DeleteDate).Column("DeleteDate").Not.Nullable();
            this.Map(x => x.UserDeleted).Column("UserDeleted").Not.Nullable();
            this.Map(x => x.StudentStatusChangeId).Column("StudentStatusChangeId").Not.Nullable();

            this.References(x => x.Campus).Column("CampusId").LazyLoad();
            this.References(x => x.NewStatus).Column("NewStatusId").LazyLoad().Nullable();
            this.References(x => x.Enrollment).Column("StuEnrollId").LazyLoad().Not.Nullable();
            this.References(x => x.OriginalStatus).Column("OrigStatusId").LazyLoad().Nullable();
            this.References(x => x.DeleteReason).Column("DeleteReasonsId").LazyLoad().Nullable();
        }
    }
}
