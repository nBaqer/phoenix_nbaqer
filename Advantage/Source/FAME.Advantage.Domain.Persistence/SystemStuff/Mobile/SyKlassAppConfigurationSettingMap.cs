﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyKlassAppConfigurationSettingMap.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Mapping for Table syKlassAppConfigurationSetting
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Mobile
{
    using Domain.SystemStuff.Mobile;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Mapping for Table <code>syKlassAppConfigurationSetting</code>
    /// </summary>
    public class SyKlassAppConfigurationSettingMap : ClassMap<SyKlassAppConfigurationSetting>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyKlassAppConfigurationSettingMap"/> class.
        ///  Mapping for Table <code>syKlassAppConfigurationSetting</code>
        /// </summary>
        public SyKlassAppConfigurationSettingMap()
        {
            this.Table("syKlassAppConfigurationSetting");

            this.Id(l => l.ID).Column("Id").Unique().GeneratedBy.Identity();
            this.Map(x => x.AdvantageIdentification).Column("AdvantageId").Not.Nullable();
            this.Map(x => x.KlassAppIdentification).Column("KlassAppId").Not.Nullable();
            this.References(x => x.KlassEntityObject).Column("KlassEntityId").Nullable().LazyLoad();
            this.References(x => x.KlassOperationTypeObject).Column("KlassOperationTypeId").Nullable().LazyLoad();
            this.Map(x => x.IsCustomField).Column("IsCustomField").Not.Nullable();
            this.Map(x => x.IsActive).Column("IsActive").Not.Nullable();
            this.Map(x => x.ItemStatus).Column("ItemStatus").Not.Nullable();
            this.Map(x => x.ItemValue).Column("ItemValue").Not.Nullable();
            this.Map(x => x.ItemLabel).Column("ItemLabel").Not.Nullable();
            this.Map(x => x.ItemValueType).Column("ItemValueType").Not.Nullable();
            this.Map(x => x.CreationDate).Column("CreationDate").Not.Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Not.Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Not.Nullable();
            this.Map(x => x.LastOperationLog).Column("LastOperationLog").Not.Nullable();
        }
    }
}