﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyKlassOperationTypeMap.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Mapping for Table syKlassOperationType
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Mobile
{
    using Domain.SystemStuff.Mobile;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Mapping for Table <code>syKlassOperationType</code>
    /// </summary>
    public class SyKlassOperationTypeMap : ClassMap<SyKlassOperationType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyKlassOperationTypeMap"/> class.
        ///  Mapping for Table <code>syKlassOperationType</code>
        /// </summary>
        public SyKlassOperationTypeMap()
        {
            this.Table("syKlassOperationType");

            this.Id(l => l.ID).Column("KlassOperationTypeId").Unique().GeneratedBy.Assigned();
            this.Map(x => x.Code).Column("Code").Not.Nullable();
            this.Map(x => x.Description).Column("Description").Not.Nullable();
            this.HasMany(x => x.ConfigurationSettingList).KeyColumn("Id").Cascade.AllDeleteOrphan().LazyLoad();
        }
    }
}