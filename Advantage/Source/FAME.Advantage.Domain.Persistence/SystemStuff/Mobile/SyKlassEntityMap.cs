﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyKlassEntityMap.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Mapping for Table syKlassEntity
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Mobile
{
    using Domain.SystemStuff.Mobile;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Mapping for Table <code>syKlassEntity</code>
    /// </summary>
    public class SyKlassEntityMap : ClassMap<SyKlassEntity>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyKlassEntityMap"/> class.
        ///  Mapping for Table <code>syKlassEntity</code>
        /// </summary>
        public SyKlassEntityMap()
        {
            this.Table("syKlassEntity");

            this.Id(l => l.ID).Column("KlassEntityId").Unique().GeneratedBy.Assigned();
            this.Map(x => x.Code).Column("Code").Not.Nullable();
            this.Map(x => x.Description).Column("Description").Not.Nullable();
            this.HasMany(x => x.ConfigurationSettingList).KeyColumn("Id").Cascade.AllDeleteOrphan().LazyLoad();
        }
    }
}
