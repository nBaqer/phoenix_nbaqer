﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserDefStatusCodeMap.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   MAP for Table syStatusCode
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    using FAME.Advantage.Domain.SystemStuff;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// MAP for Table <code>syStatusCode</code>
    /// </summary>
    public class UserDefStatusCodeMap : ClassMap<UserDefStatusCode>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserDefStatusCodeMap"/> class. 
        /// MAP For <code> syStatusCode </code>
        /// </summary>
        public UserDefStatusCodeMap()
        {
            this.Table("syStatusCodes");

            this.ReadOnly();

            this.Id(x => x.ID).Column("StatusCodeId").Not.Nullable().GeneratedBy.Guid();

            this.Map(x => x.StatusCode).Column("StatusCode").Not.Nullable().Length(20);
            this.Map(x => x.Description).Column("StatusCodeDescrip").Not.Nullable().Length(80);
            this.Map(x => x.CampGrpId).Column("CampGrpId").Nullable();
            this.Map(x => x.SystemStatusId).Column("SysStatusId").Not.Nullable();
            this.Map(x => x.IsAcademicProbation).Column("AcadProbation").Nullable();
            this.Map(x => x.DiscloseProbation).Column("DiscProbation").Nullable();
            this.Map(x => x.IsDefaultLeadStatus).Column("IsDefaultLeadStatus").Nullable();

            this.Map(x => x.ModifiedDate).Column("ModDate").Nullable();

            this.References(x => x.Status).Column("StatusId").Not.LazyLoad();
            this.References(x => x.CampusGroup).Column("CampGrpId").LazyLoad();
            this.References(x => x.SystemStatus).Column("SysStatusId").Not.LazyLoad();

            // To Lead list of status changes syLeadStatusesChanges
            this.HasMany(x => x.LeadStatusesOriginalChangesList).KeyColumn("OrigStatusId").LazyLoad();
            this.HasMany(x => x.LeadStatusesNewChangesList).KeyColumn("NewStatusId").LazyLoad();

            // To work-flows entries in syLeadStatusChanges
            this.HasMany(x => x.LeadWorkflowNewChangeList).KeyColumn("NewStatusId").Cascade.None().LazyLoad();
            this.HasMany(x => x.LeadWorkflowOriginalChangeList).KeyColumn("OrigStatusId").Cascade.None().LazyLoad();
        }
    }
}
