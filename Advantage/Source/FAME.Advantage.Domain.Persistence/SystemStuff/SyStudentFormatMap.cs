﻿

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    // --------------------------------------------------------------------------------------------------------------------
    // <copyright file="StudentFormatMap.cs" company="FAME INC">
    //   2018
    // </copyright>
    // <summary>
    //   Defines the StudentFormatMap type.
    // </summary>
    // --------------------------------------------------------------------------------------------------------------------


    using FAME.Advantage.Domain.SystemStuff;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The student format map.
    /// </summary>
    public class SyStudentFormatMap : ClassMap<SyStudentFormat>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyStudentFormatMap"/> class.
        /// </summary>
        public SyStudentFormatMap()
        {
            this.Table("syStudentFormat");
            this.Id(l => l.StudentFormatId).Column("StudentFormatId").Unique();
            this.Map(l => l.FormatType).Column("FormatType").Length(50);
            this.Map(l => l.YearNumber).Column("YearNumber").Nullable().LazyLoad();
            this.Map(l => l.MonthNumber).Column("MonthNumber").Nullable().LazyLoad();
            this.Map(l => l.DateNumber).Column("DateNumber").Nullable().LazyLoad();
            this.Map(l => l.LNameNumber).Column("LNameNumber").Nullable().LazyLoad();
            this.Map(l => l.FNameNumber).Column("FNameNumber").Nullable().LazyLoad();
            this.Map(l => l.SeqNumber).Column("SeqNumber").Nullable().LazyLoad();
            this.Map(l => l.SeqStartingNumber).Column("SeqStartingNumber").Nullable().LazyLoad();

            this.Map(l => l.ModUser).Column("ModUser").Nullable().LazyLoad();
            this.Map(l => l.ModDate).Column("ModDate").Nullable().LazyLoad();
        }
    }
}


