﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemStatusMap.cs" company="FAME">
//   2015, 2016
// </copyright>
// <summary>
//   Defines the SystemStatusMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    using FAME.Advantage.Domain.SystemStuff;
    using FluentNHibernate.Mapping;

    /// <summary>
    ///  The system status map.
    /// </summary>
    public class SystemStatusMap : ClassMap<SystemStatus>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemStatusMap"/> class.
        /// </summary>
        public SystemStatusMap()
        {
            this.Table("sySysStatus");

            this.ReadOnly();

            this.Id(x => x.ID).Column("SysStatusId").Not.Nullable();

            this.Map(x => x.Description).Column("SysStatusDescrip").Not.Nullable().Length(80);
            this.Map(x => x.StatusLevelId).Column("StatusLevelId").Not.Nullable();
            this.Map(x => x.IsInSchool).Column("InSchool").Nullable();
            this.Map(x => x.GEProgramStatus).Column("GEProgramStatus").Not.Nullable();

            this.References(x => x.Status).Column("StatusId").LazyLoad();
        }
    }
} 