﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyWapiExternalCompaniesMap.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the SyWapiExternalCompaniesMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    using FAME.Advantage.Domain.SystemStuff;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The SYWAPI external companies map.
    /// </summary>
    public class SyWapiExternalCompaniesMap : ClassMap<SyWapiExternalCompanies>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyWapiExternalCompaniesMap"/> class.
        /// </summary>
        public SyWapiExternalCompaniesMap()
        {
            this.Table("SyWapiExternalCompanies");

            this.Id(x => x.ID).Column("Id").Unique().GeneratedBy.Native();
            this.Map(x => x.Code).Column("Code").Not.Nullable().Unique().Length(30);
            this.Map(x => x.Description).Column("Description").Not.Nullable().Length(100);
            this.Map(x => x.IsActive).Column("IsActive").Not.Nullable();

            // You need to check in your logic if the company is used in ant setting operation
            // to avoid a orphan record there!
            this.HasMany(x => x.WapiSettingsList).KeyColumn("IdExtCompany").Cascade.None();

            this.HasManyToMany(x => x.WapiAllowedServicesList)
                .Cascade.All()
                .Table("syWapiBridgeExternalCompanyAllowedServices")
            .ParentKeyColumn("IdExternalCompanies")
            .ChildKeyColumn("IdAllowedServices");
        }
    }
}