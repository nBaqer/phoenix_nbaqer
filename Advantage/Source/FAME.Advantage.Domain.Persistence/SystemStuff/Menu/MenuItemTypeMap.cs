﻿using FAME.Advantage.Domain.SystemStuff.Menu;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff.Menu
{
    public class MenuItemTypeMap : ClassMap<MenuItemType>
    {
        public MenuItemTypeMap()
        {
            ReadOnly();

            Table("syMenuItemType");

            Id(x => x.ID).Column("MenuItemTypeId").GeneratedBy.Identity();
            Map(x => x.ItemType).Column("MenuItemType").Not.Nullable();
        }
    }
}
