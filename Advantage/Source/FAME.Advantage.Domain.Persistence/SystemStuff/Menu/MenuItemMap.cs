﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MenuItemMap.cs" company="FAME">
//   2014, 2016
// </copyright>
// <summary>
//   Defines the MenuItemMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.Domain.Persistence.SystemStuff.Menu
{
    using FAME.Advantage.Domain.SystemStuff.Menu;
    using FluentNHibernate.Mapping;

    /// <summary>
    ///  The menu item map.
    /// </summary>
    public class MenuItemMap : ClassMap<MenuItem>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuItemMap"/> class.
        /// </summary>
        public MenuItemMap()
        {
            this.ReadOnly();

            this.Table("syMenuItems");

            this.Id(x => x.ID).Column("MenuItemId").GeneratedBy.Identity();

            this.Map(x => x.MenuName).Column("MenuName").Length(250).Not.Nullable();
            this.Map(x => x.DisplayName).Column("DisplayName").Length(250).Not.Nullable();
            this.Map(x => x.ResourceUrl).Column("Url").Length(250).Nullable();
            this.Map(x => x.ModuleCode).Column("ModuleCode").Length(250).Nullable();
            this.Map(x => x.DisplayOrder).Column("DisplayOrder").Nullable();
            this.Map(x => x.IsPopup).Column("IsPopup").Not.Nullable();
            this.Map(x => x.ModDate).Column("ModDate").Nullable();
            this.Map(x => x.IsActive).Column("IsActive").Not.Nullable();
            this.Map(x => x.ResourceId).Column("ResourceId").Nullable();
            this.Map(x => x.HierarchyId).Column("HierarchyId").Nullable();
            this.Map(x => x.ParentId).Column("ParentId").Nullable();

            this.References(x => x.ItemType).Column("MenuItemTypeId").LazyLoad();

            this.HasMany(x => x.ChildMenuItems).KeyColumn("ParentId").LazyLoad();
        }
    }
}
