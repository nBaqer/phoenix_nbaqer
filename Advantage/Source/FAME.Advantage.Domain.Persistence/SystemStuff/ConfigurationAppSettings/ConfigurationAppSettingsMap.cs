﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationAppSettingsMap.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the ConfigurationAppSettingsMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.ConfigurationAppSettings
{
    using FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The configuration application settings map.
    /// </summary>
    public class ConfigurationAppSettingsMap : ClassMap<ConfigurationAppSetting>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationAppSettingsMap"/> class.
        /// </summary>
        public ConfigurationAppSettingsMap()
        {
            this.Table("syConfigAppSettings");

            this.Id(x => x.ID).Column("SettingId").GeneratedBy.Identity();

            this.Map(x => x.KeyName).Column("KeyName").Length(200).Not.Nullable();
            this.Map(x => x.Description).Column("Description").Length(200).Nullable();
            this.Map(x => x.CampusSpecific).Column("CampusSpecific").Not.Nullable();
            this.Map(x => x.ExtraConfirmation).Column("ExtraConfirmation").Not.Nullable();

            this.HasMany(x => x.ConfigurationValues).KeyColumn("SettingId").Not.LazyLoad();
        }
    }
}
