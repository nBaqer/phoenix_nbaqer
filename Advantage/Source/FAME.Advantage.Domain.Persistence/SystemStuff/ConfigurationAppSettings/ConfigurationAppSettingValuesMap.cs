﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationAppSettingValuesMap.cs" company="FAME Inc.">
//   FAME Inc. 2016
//   FAME.Advantage.Domain.Persistence.SystemStuff.ConfigurationAppSettings.ConfigurationAppSettingValuesMap
// </copyright>
// <summary>
//   The configuration app setting values map.
//   Domain Mapping for table <code>syConfigAppSetValues</code> with Domain Entity ConfigurationAppSettingValues
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff.ConfigurationAppSettings
{
    using FAME.Advantage.Domain.SystemStuff.ConfigurationAppSettings;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The configuration app setting values map.
    /// Domain Mapping for table <code>syConfigAppSetValues</code> with Domain Entity ConfigurationAppSettingValues
    /// </summary>
    public class ConfigurationAppSettingValuesMap : ClassMap<ConfigurationAppSettingValues>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationAppSettingValuesMap"/> class.
        /// </summary>
        public ConfigurationAppSettingValuesMap()
        {

            this.Table("syConfigAppSetValues");

            this.Id(x => x.ID).Column("ValueId").GeneratedBy.Guid();

            this.Map(x => x.Value).Column("Value").Length(1000).Nullable();
            this.Map(x => x.Active).Column("Active").Not.Nullable();

            this.References(x => x.Campus).Column("CampusId").LazyLoad();
            this.References(x => x.ConfigurationAppSetting).Column("SettingId").Nullable().LazyLoad();

            this.HasManyToMany(x => x.MenuItemsToExclude).Cascade.None().Table("syMenuConfigExclusions").ParentKeyColumn("ValueId").ChildKeyColumn("MenuItemId"); 
        }
    }
}
