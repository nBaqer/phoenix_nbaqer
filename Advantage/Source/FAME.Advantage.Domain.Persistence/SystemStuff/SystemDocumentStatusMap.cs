﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemDocumentStatusMap.cs" company="FAME Inc">
//   2016
//   FAME.Advantage.Domain.Persistence.SystemStuff.SystemDocumentStatusMap
// </copyright>
// <summary>
//   Defines the System Document Status Map type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    using FAME.Advantage.Domain.SystemStuff;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The system document status map.
    /// </summary>
    public class SystemDocumentStatusMap : ClassMap<SystemDocumentStatus>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemDocumentStatusMap"/> class.
        /// </summary>
        public SystemDocumentStatusMap()
        {
            this.Table("sySysDocStatuses");
            this.Id(x => x.ID).Column("SysDocStatusId").Not.Nullable().GeneratedBy.Identity();
            this.Map(x => x.Description).Column("DocStatusDescrip").Not.Nullable();
        }
    }
}
