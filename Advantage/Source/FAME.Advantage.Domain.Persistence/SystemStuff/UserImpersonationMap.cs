﻿using FAME.Advantage.Domain.SystemStuff;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    public class UserImpersonationLogMap : ClassMap<UserImpersonationLog>
    {
        public UserImpersonationLogMap()
        {
            Table("syUserImpersonationLog");

            

            Id(x => x.ID).Column("UserImpersonationLogId").Not.Nullable();

            Map(x => x.ImpersonatedUser).Column("ImpersonatedUser").Not.Nullable().Length(50);
            Map(x => x.LogStart).Column("LogStart").Nullable();
            Map(x => x.LogEnd).Column("LogEnd").Nullable();
            Map(x => x.ModUser).Column("ModUser").Nullable();

        }
    }
}


    