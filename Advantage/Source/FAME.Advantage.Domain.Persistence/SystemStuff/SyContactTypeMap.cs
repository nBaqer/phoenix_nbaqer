﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.Domain.SystemStuff;
using FluentNHibernate.Mapping;

namespace FAME.Advantage.Domain.Persistence.SystemStuff
{
    /// <summary>
    /// Domain Mapping for syContactTypes
    /// </summary>
    public class SyContactTypeMap: ClassMap<ContactType>
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public SyContactTypeMap()
        {
            Table("syContactTypes");
            Id(x => x.ID).Column("ContactTypeId").Not.Nullable().GeneratedBy.Guid();
            Map(x => x.Code).Column("ContactTypeCode").Nullable().Length(12);
            Map(x => x.Description).Column("ContactTypeDescrip").Not.Nullable().Length(50);
            Map(x => x.StatusId).Column("StatusId").Not.Nullable();
            Map(x => x.CampusGroupId).Column("CampGrpId").Not.Nullable();
            Map(x => x.ModUser).Column("ModUser").Not.Nullable().Length(50);
            Map(x => x.ModDate).Column("ModDate").Not.Nullable();

            References(x => x.Status).Column("StatusId").LazyLoad();
        }
    }
}
