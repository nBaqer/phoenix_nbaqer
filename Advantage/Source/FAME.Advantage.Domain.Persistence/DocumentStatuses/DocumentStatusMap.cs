﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentStatusMap.cs" company="FAME Inc.">
//   2016
//   FAME.Advantage.Domain.Persistence.DocumentStatuses.DocumentStatusMap
// </copyright>
// <summary>
//   Defines the School Defined Document Status Map type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.Domain.Persistence.DocumentStatuses
{
    using FAME.Advantage.Domain.Requirements.Documents;

    using FluentNHibernate.Mapping;

    /// <summary>
    /// The document status map.
    /// </summary>
    public class DocumentStatusMap : ClassMap<DocumentStatus>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentStatusMap"/> class.
        /// </summary>
        public DocumentStatusMap()
        {
            this.ReadOnly();
            this.Table("syDocStatuses");
            this.Id(x => x.ID).Column("DocStatusId").GeneratedBy.Guid();
            this.Map(x => x.Code).Column("DocStatusCode").Not.Nullable();
            this.Map(x => x.Description).Column("DocStatusDescrip").Not.Nullable();
            this.Map(x => x.IsApproved).Column("SysDocStatusId").CustomType<DocumentStatusToBoolean>();
            this.Map(x => x.ModDate).Column("ModDate").Nullable();
            this.Map(x => x.ModUser).Column("ModUser").Nullable();
            this.References(x => x.SystemDocumentStatus).Column("SysDocStatusId").Not.Nullable().LazyLoad();
        }
    }
}
