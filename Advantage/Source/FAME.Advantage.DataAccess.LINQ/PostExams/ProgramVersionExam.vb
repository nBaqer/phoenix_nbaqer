﻿'Option Strict On
<Serializable()>
Public Class ProgramVersionExam
    Public Property Name As String

    Public Property GradeBookDetailId As Guid

    Public Property SequenceNumber As Integer

    Public Property SubjectDescription As String

    Public Property SubjectId As Guid

    Public property Position As Integer
End Class
