﻿Imports System.Text.RegularExpressions
Imports FAME.Advantage.Common.LINQ.Entities

Namespace PostExams
    Public Class PostExamsDA
        Inherits LinqDataAccess
        Public Sub New(ByVal connection As String)
            MyBase.New(connection)
        End Sub


        Public Function GetPostExamsData(ByVal stuEnrollId As String, ByVal type As Integer, ByVal Optional termId As String = "") As List(Of PostExamsInfo)

            Dim isProgramRegistrationType = (From e In db.arStuEnrollments
                                             Where e.StuEnrollId.ToString() = stuEnrollId
                                             Select e.arPrgVersion.ProgramRegistrationType).FirstOrDefault()

            Dim enrollmentRecord = (From e In db.arStuEnrollments
                    Where  e.StuEnrollId.ToString() = stuEnrollId).FirstOrDefault() 

            Dim enrollmentStartDate= If( enrollmentRecord Isnot Nothing, enrollmentRecord.StartDate, New DateTime(1953,1,1))


            Dim examsWithResults = From gbr In db.arGrdBkResults
                                   Join gbwd In db.arGrdBkWgtDetails On gbr.InstrGrdBkWgtDetailId Equals gbwd.InstrGrdBkWgtDetailId
                                    Join gbw In db.arGrdBkWeights On gbwd.InstrGrdBkWgtId Equals gbw.InstrGrdBkWgtId
                                   Join cs In db.arClassSections On gbr.ClsSectionId Equals cs.ClsSectionId
                                   Join gct In db.arGrdComponentTypes On gbwd.GrdComponentTypeId Equals gct.GrdComponentTypeId
                                   Join term In db.arTerms On cs.TermId Equals term.TermId
                                   Join reqs In db.arReqs On cs.ReqId Equals reqs.ReqId
                                   Where gbr.StuEnrollId.ToString() = stuEnrollId AndAlso gct.SysComponentTypeId.Value = type
                                   Select New PostExamsInfo With {.Seq = gbwd.Seq, .StuEnrollId = gbr.StuEnrollId, .ClsSectionId = gbr.ClsSectionId, .InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId,
                                                       .TermId = cs.TermId, .GrpDescrip = term.TermDescrip, .StartDate = term.StartDate, .EndDate = term.EndDate,
                                                       .Subject = reqs.Descrip, .GRDComponentTypeId = gct.GrdComponentTypeId, .Code = gbwd.Code,
                                                       .Name = IIf(isProgramRegistrationType = 0, gct.Descrip.Trim(), String.Concat(gct.Descrip.Trim(), "[ ", cs.ClsSection, " ]")), .Type = 1, .DBId = gbr.GrdBkResultId, .Score = gbr.Score,
                                                       .MinScore = ((From csect In db.arClassSections
                                                                     Join gsd In db.arGradeScaleDetails On csect.GrdScaleId Equals gsd.GrdScaleId
                                                                     Where csect.ClsSectionId = gbr.ClsSectionId
                                                                     Select gsd.MinVal).Min()),
                                                       .MaxScore = ((From csect2 In db.arClassSections
                                                                     Join gsd2 In db.arGradeScaleDetails On csect2.GrdScaleId Equals gsd2.GrdScaleId
                                                                     Where csect2.ClsSectionId = gbr.ClsSectionId
                                                                     Select gsd2.MaxVal).Max()),
                                                       .PostDate = gbr.PostDate, .Number = gbwd.Number,
                                                       .DBIdName = "GrdBkResultId",
                    .ReqId = reqs.ReqId,
                    .BookEffectiveDate = gbw.EffectiveDate,
                    .GradeBookId = gbw.InstrGrdBkWgtId
                                       }



            examsWithResults = examsWithResults.Concat(
                    From gbcr In db.arGrdBkConversionResults
                    Join gct In db.arGrdComponentTypes On gbcr.GrdComponentTypeId Equals gct.GrdComponentTypeId
                    Join gbwd In db.arGrdBkWgtDetails On gct.GrdComponentTypeId Equals gbwd.GrdComponentTypeId
                                                          Join gbw In db.arGrdBkWeights On gbwd.InstrGrdBkWgtId Equals gbw.InstrGrdBkWgtId
                    Join term In db.arTerms On gbcr.TermId Equals term.TermId
                    Join reqs In db.arReqs On gbcr.ReqId Equals reqs.ReqId
                    Join cs In db.arClassSections On reqs.ReqId Equals cs.ReqId
                    Where gbcr.StuEnrollId.ToString() = stuEnrollId AndAlso gct.SysComponentTypeId.Value = type
                    Select New PostExamsInfo With {.Seq = gbwd.Seq, .StuEnrollId = gbcr.StuEnrollId, .ClsSectionId = Guid.Empty, .InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId,
                                        .TermId = gbcr.TermId.Value, .GrpDescrip = term.TermDescrip, .StartDate = term.StartDate, .EndDate = term.EndDate,
                                        .Subject = reqs.Descrip, .GRDComponentTypeId = gct.GrdComponentTypeId, .Code = gbwd.Code,
                                        .Name = IIf(isProgramRegistrationType = 0, gct.Descrip.Trim(), String.Concat(gct.Descrip.Trim(), "[ ", cs.ClsSection, " ]")), .Type = 0, .DBId = gbcr.ConversionResultId, .Score = gbcr.Score,
                                        .MinScore = CType(0, Short),
                                        .MaxScore = CType(100, Short),
                                        .PostDate = gbcr.PostDate, .Number = gbwd.Number,
                                        .DBIdName = "ConversionResultId",
                                                          .ReqId = reqs.ReqId,
                                                          .BookEffectiveDate = gbw.EffectiveDate,   .GradeBookId = gbw.InstrGrdBkWgtId
                        }
                            )


            Dim examWithResultToList = examsWithResults.OrderBy(Function(ae As PostExamsInfo) ae.Name).ToList()


            Dim allExams = New List(Of PostExamsInfo)

            If (isProgramRegistrationType Is Nothing Or isProgramRegistrationType = 0) Then
                allExams = (From ar In db.arResults
                            Join cs In db.arClassSections On ar.TestId Equals cs.ClsSectionId
                            Join term In db.arTerms On term.TermId Equals cs.TermId
                            Join gbwd In db.arGrdBkWgtDetails On cs.InstrGrdBkWgtId Equals gbwd.InstrGrdBkWgtId
                            Join gbw In db.arGrdBkWeights On gbwd.InstrGrdBkWgtId Equals gbw.InstrGrdBkWgtId
                            Join gct In db.arGrdComponentTypes On gbwd.GrdComponentTypeId Equals gct.GrdComponentTypeId
                            Join reqs In db.arReqs On cs.ReqId Equals reqs.ReqId
                            Where ar.StuEnrollId.ToString() = stuEnrollId AndAlso gct.SysComponentTypeId.Value = type
                            Select New PostExamsInfo With {.Seq = gbwd.Seq, .StuEnrollId = ar.StuEnrollId, .ClsSectionId = ar.TestId.Value, .InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId,
                                                .TermId = cs.TermId, .GrpDescrip = term.TermDescrip, .StartDate = term.StartDate, .EndDate = term.EndDate,
                                                .Subject = reqs.Descrip, .GRDComponentTypeId = gct.GrdComponentTypeId, .Code = gbwd.Code,
                                                .Name = IIf(isProgramRegistrationType = 0, gct.Descrip.Trim(), String.Concat(gct.Descrip.Trim(), "[ ", cs.ClsSection, " ]")), .Type = 1, .DBId = ar.ResultId, .Score = CType(0, Decimal?),
                                                .MinScore = ((From csect In db.arClassSections
                                                              Join gsd In db.arGradeScaleDetails On csect.GrdScaleId Equals gsd.GrdScaleId
                                                              Where csect.ClsSectionId = ar.TestId
                                                              Select gsd.MinVal).Min()),
                                                .MaxScore = ((From csect2 In db.arClassSections
                                                              Join gsd2 In db.arGradeScaleDetails On csect2.GrdScaleId Equals gsd2.GrdScaleId
                                                              Where csect2.ClsSectionId = ar.TestId
                                                              Select gsd2.MaxVal).Max()),
                                                .PostDate = Nothing, .Number = gbwd.Number,
                                                .DBIdName = "ResultId",
                                .ReqId = reqs.ReqId,
                                .BookEffectiveDate = gbw.EffectiveDate,   .GradeBookId = gbw.InstrGrdBkWgtId
                                }).Distinct().OrderBy(Function(ae As PostExamsInfo) ae.Name).ToList()

            Else
                allExams = (From ar In db.arResults
                            Join cs In db.arClassSections On ar.TestId Equals cs.ClsSectionId
                            Join pv In db.arProgVerDefs On pv.ProgVerDefId Equals cs.ProgramVersionDefinitionId
                            Join reqs In db.arReqs On pv.ReqId Equals reqs.ReqId
                            Join gbw In db.arGrdBkWeights On gbw.ReqId Equals reqs.ReqId
                            Join term In db.arTerms On term.TermId Equals cs.TermId
                            Join gbwd In db.arGrdBkWgtDetails On gbw.InstrGrdBkWgtId Equals gbwd.InstrGrdBkWgtId
                            Join gct In db.arGrdComponentTypes On gbwd.GrdComponentTypeId Equals gct.GrdComponentTypeId
                            Where ar.StuEnrollId.ToString() = stuEnrollId AndAlso gct.SysComponentTypeId.Value = type
                            Select New PostExamsInfo With {.Seq = gbwd.Seq, .StuEnrollId = ar.StuEnrollId, .ClsSectionId = ar.TestId.Value, .InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId,
                                                .TermId = cs.TermId, .GrpDescrip = term.TermDescrip, .StartDate = term.StartDate, .EndDate = term.EndDate,
                                                .Subject = reqs.Descrip, .GRDComponentTypeId = gct.GrdComponentTypeId, .Code = gbwd.Code,
                                                .Name = IIf(isProgramRegistrationType = 0, gct.Descrip.Trim(), String.Concat(gct.Descrip.Trim(), "[ ", cs.ClsSection, " ]")), .Type = 1, .DBId = ar.ResultId, .Score = CType(0, Decimal?),
                                                .MinScore = ((From csect In db.arClassSections
                                                              Join gsd In db.arGradeScaleDetails On csect.GrdScaleId Equals gsd.GrdScaleId
                                                              Where csect.ClsSectionId = ar.TestId
                                                              Select gsd.MinVal).Min()),
                                                .MaxScore = ((From csect2 In db.arClassSections
                                                              Join gsd2 In db.arGradeScaleDetails On csect2.GrdScaleId Equals gsd2.GrdScaleId
                                                              Where csect2.ClsSectionId = ar.TestId
                                                              Select gsd2.MaxVal).Max()),
                                                .PostDate = Nothing, .Number = gbwd.Number,
                                                .DBIdName = "ResultId",
                                .ReqId = reqs.ReqId,
                                .BookEffectiveDate = gbw.EffectiveDate,   .GradeBookId = gbw.InstrGrdBkWgtId
                                }).Distinct().OrderBy(Function(ae As PostExamsInfo) ae.Name).ToList()
            End If

            dim courseBookCount = allExams.GroupBy(Function(e as PostExamsInfo) e.ReqId) _
            .ToDictionary(function(p)p.Key, _
                          function(c) if(
                              (c.select(function (ex As PostExamsInfo) ex.GradeBookId).distinct().count() = 1), _
                              (c.FirstOrDefault().BookEffectiveDate), 
                              if(
                                  (c.Where(function (ex As PostExamsInfo) ex.BookEffectiveDate < enrollmentStartDate).Max(function (ex As PostExamsInfo) ex.BookEffectiveDate) )IsNot Nothing, 
                                  (c.Where(function (ex As PostExamsInfo) ex.BookEffectiveDate < enrollmentStartDate).Max(function (ex As PostExamsInfo) ex.BookEffectiveDate)), _
                                  (c.Where(function (ex As PostExamsInfo) ex.BookEffectiveDate >= enrollmentStartDate).Min(function (ex As PostExamsInfo) ex.BookEffectiveDate) )
                                  )
                              )
                          )

   

            Dim leftOuterJoin = (From res In allExams
                                 Group Join grades In examWithResultToList
                                On res.ClsSectionId Equals grades.ClsSectionId And res.InstrGrdBkWgtDetailId Equals grades.InstrGrdBkWgtDetailId Into Group
                                 From grades In Group.DefaultIfEmpty()).AsEnumerable()

            Dim result = leftOuterJoin.Select(Function(pei, index) New PostExamsInfo With {
                                                .Seq = pei.res.Seq,
                                                .StuEnrollId = pei.res.StuEnrollId,
                                                .ClsSectionId = pei.res.ClsSectionId,
                                                .InstrGrdBkWgtDetailId = pei.res.InstrGrdBkWgtDetailId,
                                                .TermId = pei.res.TermId,
                                                .GrpDescrip = pei.res.GrpDescrip,
                                                .StartDate = pei.res.StartDate,
                                                .EndDate = pei.res.EndDate,
                                                .Subject = pei.res.Subject,
                                                .GRDComponentTypeId = pei.res.GRDComponentTypeId,
                                                .Code = pei.res.Code,
                                                .Name = pei.res.Name,
                                                .Type = pei.res.Type,
                                                .BookEffectiveDate = pei.res.BookEffectiveDate,
                                                .GradeBookId = pei.res.GradeBookId,
                                                .DBId = If(pei.grades IsNot Nothing, pei.grades.DBId, pei.res.DBId),
                                                .DBIdName = If(pei.grades IsNot Nothing, "GrdBkResultId", "ResultId"),
                                                .Score = If(pei.grades IsNot Nothing, pei.grades.Score, pei.res.Score),
                                                .MinScore = If(pei.grades IsNot Nothing, pei.grades.MinScore, pei.res.MinScore),
                                                .MaxScore = If(pei.grades IsNot Nothing, pei.grades.MaxScore, pei.res.MaxScore),
                                                .PostDate = If(pei.grades IsNot Nothing, pei.grades.PostDate, pei.res.PostDate),
                                                .Number = If(pei.grades IsNot Nothing, pei.grades.Number, pei.res.Number),
                                                .Row = index,
                                                 .ValidForStudent = courseBookCount(pei.res.ReqId) = pei.res.BookEffectiveDate
                            }).OrderBy(Function(pei As PostExamsInfo) pei.Subject).ThenBy(Function(pei As PostExamsInfo) pei.Seq).ThenBy(Function(pei As PostExamsInfo) pei.GrpDescrip).ThenBy(Function(pei As PostExamsInfo) pei.Name).ThenBy(Function(pei As PostExamsInfo) pei.GRDComponentTypeId).ThenBy(Function(pei As PostExamsInfo) pei.Type).ThenBy(Function(pei As PostExamsInfo) pei.PostDate)
        
            Dim finalResult = result.where(Function(e As PostExamsInfo) e.ValidForStudent = true).ToList()
            Return finalResult

        End Function


        Public Function GetProgramVersionExams(ByVal programVersionId As String, ByVal requirementId As String) As List(Of ProgramVersionExam)
            Dim programVersion = (From pv In db.arPrgVersions
                                  Where pv.PrgVerId.ToString = programVersionId
                                  Select pv).FirstOrDefault()


            Dim results = New List(Of ProgramVersionExam)

            If (programVersion.ProgramRegistrationType.HasValue AndAlso programVersion.ProgramRegistrationType.Value = 1) Then
                Dim reqs = (From r In db.arReqs
                            Where r.ReqId.ToString() = requirementId AndAlso r.arProgVerDefs.Any(Function(pvd As arProgVerDef) pvd.arClassSections.Any(Function(cs As arClassSection) cs.arProgVerDef.PrgVerId.ToString = programVersionId))
                            Select r)

                Dim requirements = (From r In reqs
                                    Select r.ReqId).ToList()

                
                results = (From d In db.arGrdBkWgtDetails
                           Group Join r In reqs On d.arGrdBkWeight.ReqId Equals r.ReqId into Group
                           from r In Group.DefaultIfEmpty()
                           Where d.arGrdBkWeight.ReqId.HasValue AndAlso requirements.Contains(d.arGrdBkWeight.ReqId.Value) _
                            Select New ProgramVersionExam With { .Name = d.Descrip,
                            .GradeBookDetailId = d.InstrGrdBkWgtDetailId,
                            .SequenceNumber = d.Seq, 
                            .SubjectDescription = If(r IsNot nothing, r.Descrip , ""),
                            .SubjectId = d.arGrdBkWeight.ReqId} ).ToList()


            Else
            End If

            results = results.OrderBy(Function(exam As ProgramVersionExam)exam.SubjectDescription).ThenBy(Function(exam As ProgramVersionExam)exam.SequenceNumber).ToList()

            Dim position = 1
            For Each e As ProgramVersionExam In results
                
                e.Position = position
                position = position + 1
            Next

            Return results
        End Function

        Public Function UpdateSequence(exams As List(Of ProgramVersionExam)) as Boolean
            Dim detailIds = (From e In exams _
                            Select e.GradeBookDetailId).ToList()

            Dim resultsToUpdate = (From d In db.arGrdBkWgtDetails
                    Where detailIds.Contains(d.InstrGrdBkWgtDetailId)).ToList()

            For Each r As arGrdBkWgtDetail In resultsToUpdate
                Dim position = (From e In exams 
                        where e.GradeBookDetailId = r.InstrGrdBkWgtDetailId).FirstOrDefault()

                If ( position IsNot Nothing)
                    r.Seq = position.Position
                End If
            Next
            db.SubmitChanges()
            return true
        End Function
    End Class
End Namespace