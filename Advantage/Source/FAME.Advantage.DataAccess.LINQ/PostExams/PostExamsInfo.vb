﻿Namespace PostExams
    Public Class PostExamsInfo
        Public Property DBId As Guid?
        Public Property Seq As Integer
        Public Property StuEnrollId As Guid
        Public Property ClsSectionId As Guid
        Public Property InstrGrdBkWgtDetailId As Guid
        Public Property TermId As Guid
        Public Property GrpDescrip As String
        Public Property StartDate As DateTime?
        Public Property EndDate As DateTime?
        Public Property Subject As String
        Public Property GRDComponentTypeId As Guid
        Public Property Code As String
        Public Property Name As String
        Public Property Type As Integer
        Public Property Score As Decimal?
        Public Property MinScore As Short?
        Public Property MaxScore As Short?
        Public Property PostDate As DateTime?
        Public Property Number As Integer?
        Public Property DBIdName As String

        Public Property BookEffectiveDate As String
        Public Property ReqId As Guid
        Public Property GradeBookId As Guid

        Public Row As Integer
        public Property ValidForStudent As Boolean
    End Class
End Namespace
