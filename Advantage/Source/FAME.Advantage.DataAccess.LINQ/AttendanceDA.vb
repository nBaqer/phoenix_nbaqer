﻿Public Class AttendanceDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub

    Public Sub ResetScheduleHours(ByVal enrollmentId As String, ByVal startDate As DateTime, ByVal endDate As DateTime)
        Dim results = (From a In db.arStudentClockAttendances
                       Where a.StuEnrollId.ToString() = enrollmentId And a.RecordDate >= startDate AndAlso a.RecordDate <= endDate).ToList()


        For Each r In results
           db.arStudentClockAttendances.DeleteOnSubmit(r)
        Next
        db.SubmitChanges()
    End Sub

End Class
