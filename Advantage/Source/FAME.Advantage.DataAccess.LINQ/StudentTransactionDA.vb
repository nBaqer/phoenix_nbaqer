﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities

Public Class StudentTransactionDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function SearchTransactions() As IQueryable(Of StudentTransaction)

        Try
            Dim query As IQueryable(Of StudentTransaction) = (From st In db.StudentTransactions)
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
