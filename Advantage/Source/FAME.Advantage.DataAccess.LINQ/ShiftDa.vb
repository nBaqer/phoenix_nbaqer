﻿'Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Public Class ShiftDa
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function USP_getShift_byCampusDate_andTermCourseInstructor(ByVal Campus As Guid, ByVal WeekEndDate As Date,
                                                               ByVal TermId As String, ByVal CourseId As String,
                                                               ByVal InstructorID As String, ByVal ShowInactive As Boolean) As IEnumerable(Of USP_getShift_byCampusDate_andTermCourseInstructorResult)
        Try
            Dim query As IList(Of USP_getShift_byCampusDate_andTermCourseInstructorResult) = db.USP_getShift_byCampusDate_andTermCourseInstructor(Campus, WeekEndDate, TermId, CourseId, InstructorID, ShowInactive).ToList()
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
