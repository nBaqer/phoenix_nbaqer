﻿Imports System.Text.RegularExpressions
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ.Common
Public Class ClassSectionTermsDA
    Inherits LinqDataAccess

    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub

    Public Function GetClsSectsbyUserandRoles(ByVal Term As String, ByVal StudentGroup As String, ByVal userId As String, ByVal CampusId As String, ByVal ShiftId As String, ByVal MinimumClassStartDate As Date, ByVal UserName As String, ByVal isAcademicAdvisor As Boolean) As List(Of ClassSectionTerm)
        Dim emptyGuid = Guid.Empty.ToString()
        Dim now = Date.Now
        Dim instructors = New List(Of String)
        Dim queryResult As IQueryable(Of ClassSectionTerm)

        Dim result = New List(Of ClassSectionTerm)

        If UserName <> "sa" And UserName.ToLower() <> "support" And isAcademicAdvisor = False Then
            instructors = From I In db.arInstructorsSupervisors
                          Where I.InstructorId.ToString() = userId OrElse I.SupervisorId.ToString() = userId
                          Select I.InstructorId.ToString.Distinct().ToList()
        End If

        If (Term <> emptyGuid And (StudentGroup = emptyGuid Or String.IsNullOrEmpty(StudentGroup))) Then


            queryResult = From cst In db.arClassSectionTerms
                          Join cs In db.arClassSections On cst.ClsSectionId Equals cs.ClsSectionId
                          Join r In db.arReqs On r.ReqId Equals cs.ReqId
                          Where cst.TermId.ToString() = Term _
                                AndAlso (ShiftId.ToString() = emptyGuid OrElse cs.ShiftId.ToString() = ShiftId) _
                                AndAlso (MinimumClassStartDate = Nothing OrElse cs.StartDate > MinimumClassStartDate) _
                                AndAlso cs.CampusId.ToString() = CampusId _
                                AndAlso cs.StartDate <= now _
                                AndAlso (Not instructors.Count > 0 OrElse instructors.Contains(cs.InstructorId.ToString()))
                          Select New ClassSectionTerm With {
                              .Id = cs.ClsSectionId.ToString(),
                              .Description = "(" + r.Code + ") " + r.Descrip,
                              .InstructorGradeBookWeightId = cs.InstrGrdBkWgtId.ToString(),
                              .RequirementId = r.ReqId.ToString()
                              }

            result = queryResult.ToList()
        ElseIf ((Term = emptyGuid Or String.IsNullOrEmpty(Term)) And StudentGroup <> emptyGuid) Then

            queryResult = From lgr In db.adLeadByLeadGroups
                          Join e In db.arStuEnrollments On e.LeadId Equals lgr.LeadId
                          Join r In db.arResults On r.StuEnrollId Equals e.StuEnrollId
                          Join cs In db.arClassSections On cs.ClsSectionId Equals r.TestId
                          Join req In db.arReqs On req.ReqId Equals cs.ReqId
                          Where lgr.LeadGrpId.ToString() = StudentGroup _
                                AndAlso (ShiftId.ToString() = emptyGuid OrElse cs.ShiftId.ToString() = ShiftId) _
                                AndAlso (MinimumClassStartDate = Nothing OrElse cs.StartDate > MinimumClassStartDate) _
                                AndAlso cs.CampusId.ToString() = CampusId _
                                AndAlso cs.StartDate <= now _
                                AndAlso (Not instructors.Count > 0 OrElse instructors.Contains(cs.InstructorId.ToString()))
                          Select New ClassSectionTerm With {
                              .Id = req.ReqId.ToString(),
                              .Description = "(" + req.Code + ") " + req.Descrip,
                              .InstructorGradeBookWeightId = String.Empty,
                              .RequirementId = req.ReqId.ToString()
                              } _
                           

            result = queryResult.ToList()
        End If



        'Dim primeResult = primeQuery.ToList()

        'If (primeResult.Count > 0) Then
        '    Dim dtTransferGrade As New TransferGradeDB
        '    For Each row In primeResult
        '        If (dtTransferGrade.IsCourseALab(row.RequirementId, row.RequirementId) = True) Then
        '            primeResult.Remove(row)
        '        End If
        '    Next
        'End If

        'Dim result = From pr In primeResult
        '             Select New GenericListItem With {
        '                 .Id = pr.Id,
        '        .Description = pr.Description
        '                                      }D
        result = result.Distinct().OrderBy(Function (x As ClassSectionTerm)  x.description).ToList()
        Return result
    End Function
End Class
