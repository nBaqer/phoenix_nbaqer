﻿'Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Imports LinqKit
Imports System.Linq.Expressions
Imports FAME.Parameters.Collections
Imports System.Data.SqlClient

Public Class CoursesDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function GetCoursesByCampusId(ByVal campusId As Guid, ByVal statusTypes As List(Of String)) As List(Of arReq)
        Try
            Dim i As Integer = 0
            Dim stats(statusTypes.Count - 1) As String
            For Each item As String In statusTypes
                stats(i) = item
                i = i + 1
            Next
            Dim query As IQueryable(Of arReq) = (From p In db.arReqs _
                                                     Join cg In db.syCampGrps _
                                                     On p.CampGrpId Equals cg.CampGrpId _
                                                     Join cgc In db.syCmpGrpCmps _
                                                     On cg.CampGrpId Equals cgc.CampGrpId _
                                                     Join c In db.syCampus _
                                                     On cgc.CampusId Equals c.CampusId _
                                                     Join s In db.syStatus _
                                                     On p.StatusId Equals s.StatusId _
                                                     Where c.CampusId.Equals(campusId) _
                                                     AndAlso stats.Contains(s.Status)
                                                     Select p Distinct _
                                                     Order By p.Descrip)
            Return query.ToList


        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function GetCoursesByCampusId(ByVal campusId As Guid, ByVal statusTypes As List(Of String), ByVal SelectedCourses As List(Of String)) As List(Of arReq)
        Try
            Dim i As Integer = 0
            Dim stats(statusTypes.Count - 1) As String
            For Each item As String In statusTypes
                stats(i) = item
                i = i + 1
            Next
            i = 0
            Dim selected(SelectedCourses.Count - 1) As String
            For Each item As String In SelectedCourses
                selected(i) = item
                i = i + 1
            Next
            Dim query As IQueryable(Of arReq) = (From p In db.arReqs _
                                                     Join cg In db.syCampGrps _
                                                     On p.CampGrpId Equals cg.CampGrpId _
                                                     Join cgc In db.syCmpGrpCmps _
                                                     On cg.CampGrpId Equals cgc.CampGrpId _
                                                     Join c In db.syCampus _
                                                     On cgc.CampusId Equals c.CampusId _
                                                     Join s In db.syStatus _
                                                     On p.StatusId Equals s.StatusId _
                                                     Where c.CampusId.Equals(campusId) _
                                                     AndAlso stats.Contains(s.Status) _
                                                     AndAlso Not selected.Contains(p.ReqId.ToString) _
                                                     Select p Distinct _
                                                     Order By p.Descrip)
            Return query.ToList


        Catch ex As Exception
            Throw
        End Try
    End Function
    'Public Function GetCoursesByCampusAndProgramVersion(ByVal campusId As Guid, ByVal statusTypes As List(Of String), _
    '                                                    ByVal progVerId As Guid) As List(Of arReq)
    '    Try
    '        Dim i As Integer = 0
    '        Dim stats(statusTypes.Count - 1) As String
    '        For Each item As String In statusTypes
    '            stats(i) = item
    '            i = i + 1
    '        Next
    '        Dim query As IQueryable(Of arReq) = (From p In db.arReqs _
    '                                                 Join cg In db.syCampGrps _
    '                                                 On p.CampGrpId Equals cg.CampGrpId _
    '                                                 Join cgc In db.syCmpGrpCmps _
    '                                                 On cg.CampGrpId Equals cgc.CampGrpId _
    '                                                 Join c In db.syCampus _
    '                                                 On cgc.CampusId Equals c.CampusId _
    '                                                 Join s In db.syStatus _
    '                                                 On p.StatusId Equals s.StatusId _
    '                                                 Join pvd In db.arProgVerDefs On pvd.ReqId Equals p.ReqId
    '                                                 Where c.CampusId.Equals(campusId) _
    '                                                 AndAlso stats.Contains(s.Status) _
    '                                                 AndAlso pvd.PrgVerId.Equals(progVerId) _
    '                                                 AndAlso p.ReqTypeId.Equals(1)
    '                                                 Select p Distinct Order By p.Descrip).Union(
    '                                                 From p In db.arReqs _
    '                                                 Join cg In db.syCampGrps _
    '                                                 On p.CampGrpId Equals cg.CampGrpId _
    '                                                 Join cgc In db.syCmpGrpCmps _
    '                                                 On cg.CampGrpId Equals cgc.CampGrpId _
    '                                                 Join c In db.syCampus _
    '                                                 On cgc.CampusId Equals c.CampusId _
    '                                                 Join s In db.syStatus _
    '                                                 On p.StatusId Equals s.StatusId _
    '                                                 Join pvd In db.arProgVerDefs On pvd.ReqId Equals p.ReqId
    '                                                 Join rgd In db.arReqGrpDefs On p.ReqId Equals rgd.GrpId
    '                                                 Join p1 In db.arReqs On rgd.ReqId Equals p1.ReqId
    '                                                 Where c.CampusId.Equals(campusId) _
    '                                                 AndAlso stats.Contains(s.Status) _
    '                                                 AndAlso pvd.PrgVerId.Equals(progVerId) _
    '                                                 AndAlso p.ReqTypeId.Equals(2)
    '                                                 Select p1 Distinct Order By p1.Descrip)
    '        Return query.ToList


    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function
    'Public Function GetCoursesByCampusAndProgramVersion(ByVal campusId As Guid, ByVal statusTypes As List(Of String), _
    '                                                    ByVal SelectedCourses As List(Of String), _
    '                                                    ByVal progVerId As Guid) As List(Of arReq)
    '    Try
    '        Dim i As Integer = 0
    '        Dim stats(statusTypes.Count - 1) As String
    '        For Each item As String In statusTypes
    '            stats(i) = item
    '            i = i + 1
    '        Next
    '        i = 0
    '        Dim selected(SelectedCourses.Count - 1) As String
    '        For Each item As String In SelectedCourses
    '            selected(i) = item
    '            i = i + 1
    '        Next
    '        Dim query As IQueryable(Of arReq) = (From p In db.arReqs _
    '                                                 Join cg In db.syCampGrps _
    '                                                 On p.CampGrpId Equals cg.CampGrpId _
    '                                                 Join cgc In db.syCmpGrpCmps _
    '                                                 On cg.CampGrpId Equals cgc.CampGrpId _
    '                                                 Join c In db.syCampus _
    '                                                 On cgc.CampusId Equals c.CampusId _
    '                                                 Join s In db.syStatus _
    '                                                 On p.StatusId Equals s.StatusId _
    '                                                 Join pvd In db.arProgVerDefs On pvd.ReqId Equals p.ReqId
    '                                                 Where c.CampusId.Equals(campusId) _
    '                                                 AndAlso stats.Contains(s.Status) _
    '                                                 AndAlso pvd.PrgVerId.Equals(progVerId) _
    '                                                 AndAlso p.ReqTypeId.Equals(1) _
    '                                                 AndAlso Not selected.Contains(p.ReqId.ToString) _
    '                                                 Select p Distinct Order By p.Descrip).Union(
    '                                                 From p In db.arReqs _
    '                                                 Join cg In db.syCampGrps _
    '                                                 On p.CampGrpId Equals cg.CampGrpId _
    '                                                 Join cgc In db.syCmpGrpCmps _
    '                                                 On cg.CampGrpId Equals cgc.CampGrpId _
    '                                                 Join c In db.syCampus _
    '                                                 On cgc.CampusId Equals c.CampusId _
    '                                                 Join s In db.syStatus _
    '                                                 On p.StatusId Equals s.StatusId _
    '                                                 Join pvd In db.arProgVerDefs On pvd.ReqId Equals p.ReqId
    '                                                 Join rgd In db.arReqGrpDefs On p.ReqId Equals rgd.GrpId
    '                                                 Join p1 In db.arReqs On rgd.ReqId Equals p1.ReqId
    '                                                 Where c.CampusId.Equals(campusId) _
    '                                                 AndAlso stats.Contains(s.Status) _
    '                                                 AndAlso pvd.PrgVerId.Equals(progVerId) _
    '                                                 AndAlso Not selected.Contains(p.ReqId.ToString) _
    '                                                 AndAlso p.ReqTypeId.Equals(2)
    '                                                 Select p1 Distinct Order By p1.Descrip)
    '        Return query.ToList


    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function
    Public Function GetCoursesByCampusId(ByVal campusId As Guid, ByVal statusTypes As List(Of String), ByVal selectedCourses As List(Of String), _
                                         ByVal selectedTerms As List(Of String)) As List(Of arReq)
        Try
            Dim i As Integer = 0
            Dim stats(statusTypes.Count - 1) As String
            For Each item As String In statusTypes
                stats(i) = item
                i = i + 1
            Next
            'reset count
            i = 0
            Dim selected(selectedTerms.Count - 1) As String
            For Each item As String In selectedTerms
                selected(i) = item
                i = i + 1
            Next

            i = 0
            Dim selectedCourseList(selectedCourses.Count - 1) As String
            For Each item As String In selectedCourses
                selectedCourseList(i) = item
                i = i + 1
            Next

            Dim query As IQueryable(Of arReq) = (From p In db.arReqs _
                                                 Join cl In db.arClassSections _
                                                 On p.ReqId Equals cl.ReqId _
                                                     Join cg In db.syCampGrps _
                                                     On p.CampGrpId Equals cg.CampGrpId _
                                                     Join cgc In db.syCmpGrpCmps _
                                                     On cg.CampGrpId Equals cgc.CampGrpId _
                                                     Join c In db.syCampus _
                                                     On cgc.CampusId Equals c.CampusId _
                                                     Join s In db.syStatus _
                                                     On p.StatusId Equals s.StatusId _
                                                     Where c.CampusId.Equals(campusId) _
                                                     AndAlso stats.Contains(s.Status) _
                                                     AndAlso selected.Contains(cl.TermId.ToString) _
                                                     Select p Distinct _
                                                     Order By p.Descrip)
            Return query.ToList

        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function GetCoursesByTerms(ByVal campuses As List(Of String), ByVal selectedTerms As List(Of String) _
                                     , ByVal selectedCourses As List(Of String) _
                                     , ByVal statusTypes As List(Of String)
                                     ) As List(Of arReq)
        ' DA.GetCoursesByTerms(SelectedCampus, selectedTerms, SelectedCourses, SelectedStatuses)
        Try
            Dim i As Integer = 0
            Dim statusArray(statusTypes.Count - 1) As String
            For Each item As String In statusTypes
                statusArray(i) = item
                i = i + 1
            Next
            'reset count
            i = 0
            Dim termsArray(selectedTerms.Count - 1) As String
            For Each item As String In selectedTerms
                termsArray(i) = item
                i = i + 1
            Next

            i = 0
            Dim courseArray(selectedCourses.Count - 1) As String
            For Each item As String In selectedCourses
                CourseArray(i) = item
                i = i + 1
            Next
            Dim query As IQueryable(Of arReq) = (From p In db.arReqs
                                                 Join cl In db.arClassSections
                                                 On p.ReqId Equals cl.ReqId
                                                 Join cg In db.syCampGrps
                                                 On p.CampGrpId Equals cg.CampGrpId
                                                 Join cgc In db.syCmpGrpCmps
                                                 On cg.CampGrpId Equals cgc.CampGrpId
                                                 Join c In db.syCampus
                                                 On cgc.CampusId Equals c.CampusId
                                                 Join s In db.syStatus
                                                 On p.StatusId Equals s.StatusId
                                                 Where campuses.Contains(c.CampusId.ToString) _
                                                 AndAlso statusArray.Contains(s.Status) _
                                                 AndAlso termsArray.Contains(cl.TermId.ToString) _
                                                 AndAlso Not courseArray.Contains(p.ReqId.ToString)
                                                 Select p Distinct
                                                 Order By p.Descrip)
            Return query.ToList
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function USP_getCourses_byCampusDate_andTerm(ByVal Campus As Guid, ByVal WeekEndDate As Date, ByVal TermId As String, ByVal ShowInactive As Boolean) As IEnumerable(Of USP_getCourses_byCampusDate_andTermResult)
        Try
            Dim query As IList(Of USP_getCourses_byCampusDate_andTermResult) = db.USP_getCourses_byCampusDate_andTerm(Campus, WeekEndDate, TermId, ShowInactive).ToList()
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
