﻿Public Class Refund
    Public Property StudentEnrollmentId As Guid
    Public Property StudentAwardId As Guid
    Public Property SSN As String
    Public Property AwardDisbursementId As Guid
    Public Property AFAStudentId As Long?
    Public Property FinancialAidId As String
    Public Property DisbursementNumber As Integer
    Public Property AdjustmentAmount As Decimal
    Public Property AdjustmentDate As DateTime
    Public Property CampusId As Guid
    Public Property CmsId As String
    Public Property CreatedBy As String
    Public Property CreatedDate As DateTime
    Public Property UpdatedBy As String
    Public Property UpdatedDate As DateTime
End Class
