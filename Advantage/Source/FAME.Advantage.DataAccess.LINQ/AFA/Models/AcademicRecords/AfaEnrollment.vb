﻿Public Class AfaEnrollment
    Public Property StudentEnrollmentId As Guid
    Public Property StudentId As Guid
    Public Property AFAStudentId As Long?
    Public Property ExpectedGradDate As DateTime?
    Public Property StartDate As DateTime?
    Public Property ProgramVersionId As Guid
    Public Property ProgramName As String
    Public Property CampusId As Guid
    Public Property CmsId As String
    Public Property LeaveOfAbsenceDate As DateTime?
    Public Property DroppedDate As DateTime?
    Public Property LastDateAttendance As DateTime?
    Public Property StatusEffectiveDate As DateTime?
    Public Property AdmissionsCriteria As String
    Public Property EnrollmentStatus As String
    Public Property SSN As String
    Public Property ReturnFromLOADate As DateTime?
    Public Property CreatedBy As String
    Public Property CreatedDate As DateTime
    Public Property UpdatedBy As String
    Public Property UpdatedDate As DateTime
    Public Property AdvStudentNumber As String
    Public Property HoursPerWeek As Decimal?
    Public Property PriorSchoolHrs As Decimal?
    Public Property InSchoolTransferHrs As Decimal?
End Class
