﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities

Public Class StatesDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub

    Public Function GetStatesByCampus(campusId As Guid) As IQueryable(Of syState)
        Try
            Dim query As IQueryable(Of syState) =
                    (From y In db.syStates
                     Join x In db.syCampus On y.StateId Equals x.StateId
                     Where x.CampusId = campusId
                     Select y)
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetStateBoardReportStates() As IQueryable(Of syState)
        Try
            Dim query As IQueryable(Of syState) =
                    (From y In db.syStates
                    Where y.IsStateBoard
                    Select y)
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function


End Class





