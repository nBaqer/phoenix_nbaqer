﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Imports LinqKit
Imports System.Linq.Expressions
Imports FAME.Parameters.Collections

Public Class ProgramVersionDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Private Function GetUserName(ByVal userid As Guid) As String
        Try
            Dim query As syUser = (From u In db.syUsers
                                   Select u Where u.UserId.Equals(userid)).SingleOrDefault
            Return query.UserName
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function GetUserName(ByVal filtervalues As MasterDictionary) As String
        Try
            Dim UserCollection As New ParamValueList(Of Guid)
            If filtervalues.Contains("ProgramVersion") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("ProgramVersion"), DetailDictionary)
                If objPsFilter.Contains("UserId") Then
                    Dim UserValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("UserId"), ParamValueDictionary)
                    For Each di As DictionaryEntry In UserValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(UserValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)

                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                UserCollection = DirectCast(FModDictionary.Item("UserId"), ParamValueList(Of Guid))
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & UserValueDictionary.Name)
                        End Select
                    Next
                Else
                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                    Throw New Exception(currentmethod & " has UserId as a required filter.")
                End If
            End If
            Dim query As syUser = (From u In db.syUsers
                                   Select u Where u.UserId.Equals(UserCollection(0))).SingleOrDefault
            Return query.UserName
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function CreateCampGrpIdListByUserId(ByVal filtervalues As MasterDictionary) As String()
        Try
            If filtervalues.Contains("ProgramVersion") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("ProgramVersion"), DetailDictionary)
                If objPsFilter.Contains("UserId") Then
                    Dim UserCollection As New ParamValueList(Of Guid)
                    Dim UserValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("UserId"), ParamValueDictionary)
                    For Each di As DictionaryEntry In UserValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(UserValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)

                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                UserCollection = DirectCast(FModDictionary.Item("UserId"), ParamValueList(Of Guid))
                                Dim users(UserCollection.Count - 1) As String
                                Dim i As Integer = 0
                                For Each user As Guid In UserCollection
                                    users(i) = user.ToString
                                    i = i + 1
                                Next

                                Dim userName As String = GetUserName(New Guid(users(0)))
                                Dim query As IQueryable(Of syCampGrp)
                                If userName.ToLower = "sa" Or userName.ToLower = "support" Or userName.ToLower = "super" Then
                                    query = (From cg In db.syCampGrps)
                                Else
                                    Dim sub1 = (From cg In db.syCampGrps
                                                Join urcg In db.syUsersRolesCampGrps
                                                On cg.CampGrpId Equals urcg.CampGrpId
                                                Where users.Contains(urcg.UserId.ToString)
                                                Select cg.CampGrpId Distinct)
                                    Dim IdList1 As List(Of Guid) = sub1.ToList

                                    Dim sub2 = (From cgc In db.syCmpGrpCmps Where IdList1.Contains(cgc.CampGrpId) Select cgc.CampusId Distinct)
                                    Dim IdList2 As List(Of Guid) = sub2.ToList
                                    query = (From cg In db.syCampGrps
                                             Join cgc In db.syCmpGrpCmps
                                             On cg.CampGrpId Equals cgc.CampGrpId
                                             Where IdList2.Contains(cgc.CampusId)
                                             Select cg Distinct)
                                    'And cg.IsAllCampusGrp = False
                                End If

                                Dim results As List(Of syCampGrp) = query.ToList
                                'Look for All Campus Group ID
                                ' Dim query2 As IQueryable(Of syCampGrp) = (From q In query Where q.CampGrpId.Equals(GetAllCampusGroup()))
                                ' Dim myids As New List(Of syCampGrp)
                                ' myids = query2.ToList
                                'Add the All Campus Group if it doesn't already exist
                                'If myids.Count = 0 Then
                                '    Dim CampGrpIds(results.Count) As String
                                '    CampGrpIds(0) = GetAllCampusGroup.CampGrpId.ToString
                                '    'restart counter at one since we added the All Campus Group
                                '    i = 1
                                '    For Each item As syCampGrp In results
                                '        CampGrpIds(i) = item.CampGrpId.ToString
                                '        i = i + 1
                                '    Next
                                '    Return CampGrpIds
                                'Else
                                Dim CampGrpIds(results.Count - 1) As String
                                'restart counter
                                i = 0
                                For Each item As syCampGrp In results
                                    CampGrpIds(i) = item.CampGrpId.ToString
                                    i = i + 1
                                Next
                                Return CampGrpIds
                                'End If
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & UserValueDictionary.Name)
                        End Select
                    Next
                Else
                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                    Throw New Exception(currentmethod & " has UserId as a required filter.")
                End If
            Else
                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                Throw New Exception(currentmethod & " missing required filter.")
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    Private Function CreateCampGrpIdListByUserId(ByVal userid As String) As String()
        Try
            Dim i As Integer = 0
            Dim userName As String = GetUserName(New Guid(userid))
            Dim query As IQueryable(Of syCampGrp)
            If userName.ToLower = "sa" Or userName.ToLower = "support" Or userName.ToLower = "super" Then
                query = (From cg In db.syCampGrps)
            Else
                query = (From cg In db.syCampGrps
                         Join urcg In db.syUsersRolesCampGrps On cg.CampGrpId Equals urcg.CampGrpId
                         Where urcg.UserId.ToString.Equals(userid)
                         Select cg Distinct)
            End If

            Dim results As List(Of syCampGrp) = query.ToList

            'Look for All Campus Group ID
            Dim query2 As IQueryable(Of syCampGrp) = (From q In query Where q.CampGrpId.Equals(GetAllCampusGroup()))
            Dim myids As New List(Of syCampGrp)
            myids = query2.ToList
            'Add the All Campus Group if it doesn't already exist
            If myids.Count = 0 Then
                Dim CampGrpIds(results.Count) As String
                CampGrpIds(0) = GetAllCampusGroup.CampGrpId.ToString
                'restart counter at one since we added the All Campus Group
                i = 1
                For Each item As syCampGrp In results
                    CampGrpIds(i) = item.CampGrpId.ToString
                    i = i + 1
                Next
                Return CampGrpIds
            Else
                Dim CampGrpIds(results.Count - 1) As String
                'restart counter
                i = 0
                For Each item As syCampGrp In results
                    CampGrpIds(i) = item.CampGrpId.ToString
                    i = i + 1
                Next
                Return CampGrpIds
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    Private Function CreateCampGrpIdListFromSelectedCampGrps(ByVal selected As ParamValueList(Of Guid)) As List(Of Guid)
        Try
            Dim CampGrpList As New List(Of Guid)
            For Each campgrp As Guid In selected
                'If Not campgrp = GetAllCampusGroup.CampGrpId Then
                CampGrpList.Add(campgrp)
                'End If
            Next
            Dim sub1 = (From cgc In db.syCmpGrpCmps Where CampGrpList.Contains(cgc.CampGrpId) Select cgc.CampusId Distinct)
            Dim idlist1 As List(Of Guid) = sub1.ToList
            Dim query = (From cg In db.syCampGrps
                         Join cgc In db.syCmpGrpCmps
                         On cg.CampGrpId Equals cgc.CampGrpId
                         Where idlist1.Contains(cgc.CampusId)
                         Select cg.CampGrpId Distinct)
            Return query.ToList
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DoesProgramVersionUseTimeClock(ByVal programVersionId As String) As Boolean?
        Dim doesUseTimeClock = (From pv In db.arPrgVersions
                Where pv.PrgVerId.ToString() = programVersionId
                Select pv.UseTimeClock).FirstOrDefault()

        Return doesUseTimeClock
    End Function

    Public Function GetProgramVersions(ByVal filtervalues As MasterDictionary) As IQueryable(Of arPrgVersion)
        Try
            Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(filtervalues)
            Dim AllCampusGroup As syCampGrp = GetAllCampusGroup()
            Dim userName As String = GetUserName(filtervalues)
            Dim query As IQueryable(Of arPrgVersion)

            query = (From pv In db.arPrgVersions
                     Join cg In db.syCampGrps
                     On pv.CampGrpId Equals cg.CampGrpId
                     Where CampGrpIds.Contains(pv.CampGrpId.ToString)
                     Select pv
                     Order By pv.PrgVerDescrip)


            Dim basePredicate As Expression(Of Func(Of arPrgVersion, Boolean)) = PredicateBuilder.True(Of arPrgVersion)()
            basePredicate = basePredicate.And(Function(pv) True)
            'Dim predicate As Expression(Of Func(Of arPrgVersion, Boolean)) = PredicateBuilder.False(Of arPrgVersion)()
            If filtervalues.Contains("ProgramVersion") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("ProgramVersion"), DetailDictionary)
                If objPsFilter.Contains("CampGrpId") Then
                    query = (From pv In db.arPrgVersions
                             Order By pv.PrgVerDescrip)
                    Dim CampGrpIdValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CampGrpId"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of arPrgVersion, Boolean)) = PredicateBuilder.False(Of arPrgVersion)()
                    For Each di As DictionaryEntry In CampGrpIdValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CampGrpIdValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                    Dim selectedlist As List(Of Guid) = CreateCampGrpIdListFromSelectedCampGrps(mylist)
                                    'Add the "All" CampGrpId - We shouldn't need to do this but we do...
                                    'Dim InList As Boolean = False
                                    ''Dim AllItem As Guid = New Guid("2AC97FC1-BB9B-450A-AA84-7C503C90A1EB")
                                    'Dim AllItem As Guid = AllCampusGroup.CampGrpId
                                    'For Each item As Guid In mylist
                                    '    If item = AllItem Then
                                    '        InList = True
                                    '    End If
                                    'Next
                                    'If InList = False Then
                                    '    mylist.Add(AllItem)
                                    'End If
                                    For Each campgrpid As Guid In selectedlist
                                        Dim temp As String = campgrpid.ToString
                                        predicate = predicate.Or(Function(pv As arPrgVersion) pv.CampGrpId.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CampGrpIdValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
                If objPsFilter.Contains("Status") Then
                    Dim StatusValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("Status"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of arPrgVersion, Boolean)) = PredicateBuilder.False(Of arPrgVersion)()
                    For Each di As DictionaryEntry In StatusValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(StatusValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                ' Dim predicate As Expression(Of Func(Of arPrgVersion, Boolean)) = PredicateBuilder.False(Of arPrgVersion)()
                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))
                                    For Each status As String In mylist
                                        Dim temp As String = status
                                        predicate = predicate.Or(Function(pv As arPrgVersion) pv.syStatus.Status.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & StatusValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
                If objPsFilter.Contains("CurrentlySelected") Then
                    Dim CurrentlySelectedValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CurrentlySelected"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of arPrgVersion, Boolean)) = PredicateBuilder.True(Of arPrgVersion)()
                    For Each di As DictionaryEntry In CurrentlySelectedValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CurrentlySelectedValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary
                                    'Dim predicate As Expression(Of Func(Of arPrgVersion, Boolean)) = PredicateBuilder.False(Of arPrgVersion)()
                                    Dim mylist As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))
                                    For Each selected As String In mylist
                                        Dim temp As String = selected
                                        predicate = predicate.And(Function(pv As arPrgVersion) Not pv.PrgVerId.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CurrentlySelectedValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
            End If

            Dim result As IQueryable(Of arPrgVersion) = query.Where(basePredicate)


            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetProgramVersionsByUser(ByVal userid As String) As IQueryable(Of arPrgVersion)
        Try
            Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(userid)
            Dim userName As String = GetUserName(New Guid(userid))
            Dim query As IQueryable(Of arPrgVersion)

            query = (From pv In db.arPrgVersions
                     Join cg In db.syCampGrps
                     On pv.CampGrpId Equals cg.CampGrpId
                     Select pv Distinct
                     Where CampGrpIds.Contains(pv.CampGrpId.ToString)
                     Order By pv.PrgVerDescrip)



            Dim result As IQueryable(Of arPrgVersion) = query


            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function GetAllCampusGroup() As syCampGrp
        Try
            Dim query As syCampGrp = (From cg In db.syCampGrps
                                      Select cg Where cg.IsAllCampusGrp.Equals(True)).SingleOrDefault
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPrgVersionByCmpGrpId(ByVal UserId As Guid, ByVal CmpGrpID As List(Of String), ByVal StatusTypes As List(Of String)) As List(Of arPrgVersion)
        Dim query As IQueryable(Of arPrgVersion)

        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next

            If CmpGrpID Is Nothing Then
                Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(UserId)
                ReDim Preserve CampGrpIds(CampGrpIds.Length)
                CampGrpIds(CampGrpIds.Length - 1) = GetAllCampusGroup.CampGrpId.ToString


                query = (From pv In db.arPrgVersions
                         Join s In db.syStatus
                          On pv.StatusId Equals s.StatusId
                         Where CampGrpIds.Contains(pv.CampGrpId.ToString) _
                       AndAlso stats.Contains(s.Status)
                         Select pv Distinct
                         Order By pv.PrgVerDescrip)


            ElseIf CmpGrpID.Contains(GetAllCampusGroup.CampGrpId.ToString) Then
                query = (From pv In db.arPrgVersions
                         Join s In db.syStatus
                          On pv.StatusId Equals s.StatusId
                         Where stats.Contains(s.Status)
                         Select pv Distinct
                         Order By pv.PrgVerDescrip)

            Else
                Dim j As Integer
                Dim CmpGrpIDs(CmpGrpID.Count) As String
                For Each item As String In CmpGrpID
                    CmpGrpIDs(j) = item
                    j = j + 1
                Next
                CmpGrpIDs(j) = GetAllCampusGroup.CampGrpId.ToString

                query = (From pv In db.arPrgVersions
                         Join s In db.syStatus
                          On pv.StatusId Equals s.StatusId
                         Where CmpGrpIDs.Contains(pv.CampGrpId.ToString) _
                         AndAlso stats.Contains(s.Status)
                         Select pv Distinct
                         Order By pv.PrgVerDescrip)

            End If
            Return query.ToList
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPrgVersionByCmpGrpId(ByVal UserId As Guid, ByVal CmpGrpID As List(Of String), ByVal StatusTypes As List(Of String), ByVal SelectedPrgVers As List(Of String)) As List(Of arPrgVersion)
        Dim query As IQueryable(Of arPrgVersion)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            'reset count
            i = 0
            Dim selected(SelectedPrgVers.Count - 1) As String
            For Each item As String In SelectedPrgVers
                selected(i) = item
                i = i + 1
            Next

            If CmpGrpID Is Nothing Then

                Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(UserId)
                ReDim Preserve CampGrpIds(CampGrpIds.Length)
                CampGrpIds(CampGrpIds.Length - 1) = GetAllCampusGroup.CampGrpId.ToString

                'Dim j As Integer
                'Dim CmpGrpIDs(CmpGrpID.Count - 1) As String
                'For Each item As String In CmpGrpID
                '    CmpGrpIDs(j) = item
                '    j = j + 1
                'Next

                query = (From pv In db.arPrgVersions
                         Join s In db.syStatus
                          On pv.StatusId Equals s.StatusId
                         Where CampGrpIds.Contains(pv.CampGrpId.ToString) _
                        AndAlso stats.Contains(s.Status) _
                         AndAlso Not selected.Contains(pv.PrgVerId.ToString)
                         Select pv Distinct
                         Order By pv.PrgVerDescrip)

                'Dim query As IQueryable(Of arProgram) = (From p In db.arPrograms _
                '                                         Join cg In db.syCampGrps _
                '                                         On p.CampGrpId Equals cg.CampGrpId _
                '                                         Join cgc In db.syCmpGrpCmps _
                '                                         On cg.CampGrpId Equals cgc.CampGrpId _
                '                                         Join c In db.syCampus _
                '                                         On cgc.CampusId Equals c.CampusId _
                '                                         Join s In db.syStatus _
                '                                         On p.StatusId Equals s.StatusId _
                '                                         Where c.CampusId.Equals(CampusId) _
                '                                         AndAlso stats.Contains(s.Status) _
                '                                         AndAlso Not selected.Contains(p.ProgId.ToString) _
                '                                         Select p Distinct _
                '                                         Order By p.ProgDescrip)
            ElseIf CmpGrpID.Contains(GetAllCampusGroup.CampGrpId.ToString) Then
                query = (From pv In db.arPrgVersions
                         Join s In db.syStatus
                          On pv.StatusId Equals s.StatusId
                         Where stats.Contains(s.Status) _
                           AndAlso Not selected.Contains(pv.PrgVerId.ToString)
                         Select pv Distinct
                         Order By pv.PrgVerDescrip)
            Else


                Dim j As Integer
                Dim CmpGrpIDs(CmpGrpID.Count) As String
                For Each item As String In CmpGrpID
                    CmpGrpIDs(j) = item
                    j = j + 1
                Next
                CmpGrpIDs(j) = GetAllCampusGroup.CampGrpId.ToString


                query = (From pv In db.arPrgVersions
                         Join s In db.syStatus
                          On pv.StatusId Equals s.StatusId
                         Where CmpGrpIDs.Contains(pv.CampGrpId.ToString) _
                     AndAlso stats.Contains(s.Status) _
                     AndAlso Not selected.Contains(pv.PrgVerId.ToString)
                         Select pv Distinct
                         Order By pv.PrgVerDescrip)

            End If
            Return query.ToList
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function CreateCampGrpIdListByUserId(ByVal userid As Guid) As String()
        Try
            Dim i As Integer = 0
            Dim query As IQueryable(Of syUsersRolesCampGrp) = (From urcg In db.syUsersRolesCampGrps Where urcg.UserId.Equals(userid))
            Dim results As List(Of syUsersRolesCampGrp) = query.ToList
            Dim userName As String = GetUserName(userid)

            If userName.ToLower = "sa" Or userName.ToLower = "super" Or userName.ToLower = "support" Then
                Dim SuperUserQuery As IQueryable(Of syCampGrp) = (From cg In db.syCampGrps)
                Dim SuperUserCampGrpIds(SuperUserQuery.Count) As String
                i = 0
                For Each item As syCampGrp In SuperUserQuery
                    SuperUserCampGrpIds(i) = item.CampGrpId.ToString
                    i = i + 1
                Next
                Return SuperUserCampGrpIds
            Else

                Dim CampGrpIds(results.Count - 1) As String
                'restart counter
                i = 0
                For Each item As syUsersRolesCampGrp In results
                    CampGrpIds(i) = item.CampGrpId.ToString
                    i = i + 1
                Next
                Return CampGrpIds
            End If

        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    Public Function GetProgramVersionsByCampusId(ByVal campusId As Guid, ByVal StatusTypes As List(Of String)) As List(Of arPrgVersion)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            Dim query As IQueryable(Of arPrgVersion) = (From p In db.arPrgVersions
                                                        Join cg In db.syCampGrps
                                                        On p.CampGrpId Equals cg.CampGrpId
                                                        Join cgc In db.syCmpGrpCmps
                                                        On cg.CampGrpId Equals cgc.CampGrpId
                                                        Join c In db.syCampus
                                                        On cgc.CampusId Equals c.CampusId
                                                        Join s In db.syStatus
                                                        On p.StatusId Equals s.StatusId
                                                        Where c.CampusId.Equals(campusId) _
                                                              AndAlso stats.Contains(s.Status)
                                                        Select p Distinct
                                                        Order By p.PrgVerDescrip)
            Return query.ToList


        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetProgramVersionsByCampusId(ByVal campuses As List(Of String), ByVal StatusTypes As List(Of String)) As List(Of arPrgVersion)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            Dim query As IQueryable(Of arPrgVersion) = (From p In db.arPrgVersions
                                                        Join cg In db.syCampGrps
                                                        On p.CampGrpId Equals cg.CampGrpId
                                                        Join cgc In db.syCmpGrpCmps
                                                        On cg.CampGrpId Equals cgc.CampGrpId
                                                        Join c In db.syCampus
                                                        On cgc.CampusId Equals c.CampusId
                                                        Join s In db.syStatus
                                                        On p.StatusId Equals s.StatusId
                                                        Where campuses.Contains(c.CampusId.ToString) _
                                                        AndAlso stats.Contains(s.Status)
                                                        Select p Distinct
                                                        Order By p.PrgVerDescrip)
            Return query.ToList


        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPrgVersionByCmpGrpIdProgIdListPrgVersionIdList(ByVal campusId As Guid _
                                                                      , ByVal progIdList As List(Of Guid) _
                                                                      , ByVal prgVersionIdList As List(Of Guid) _
                                                                      , ByVal statusValuesList As List(Of String)
                                                                      ) As List(Of arPrgVersion)
        Try

            Dim query As IQueryable(Of arPrgVersion) = (From p In db.arPrgVersions
                                                        Join cg In db.syCampGrps
                                                        On p.CampGrpId Equals cg.CampGrpId
                                                        Join cgc In db.syCmpGrpCmps
                                                        On cg.CampGrpId Equals cgc.CampGrpId
                                                        Join c In db.syCampus
                                                        On cgc.CampusId Equals c.CampusId
                                                        Join prog In db.arPrograms
                                                        On p.ProgId Equals prog.ProgId
                                                        Join s In db.syStatus
                                                        On p.StatusId Equals s.StatusId
                                                        Where c.CampusId.Equals(campusId) _
                                                        AndAlso (progIdList.Count.Equals(0) Or progIdList.Contains(prog.ProgId)) _
                                                        AndAlso Not prgVersionIdList.Contains(p.PrgVerId) _
                                                        AndAlso statusValuesList.Contains(s.Status)
                                                        Select p Distinct
                                                        Order By p.PrgVerDescrip)
            Return query.ToList


        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetProgramVersionsByCampusId(ByVal campusId As Guid, ByVal StatusTypes As List(Of String), ByVal SelectedProgramVersions As List(Of String)) As List(Of arPrgVersion)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            'reset count
            i = 0
            Dim selected(SelectedProgramVersions.Count - 1) As String
            For Each item As String In SelectedProgramVersions
                selected(i) = item
                i = i + 1
            Next

            Dim query As IQueryable(Of arPrgVersion) = (From p In db.arPrgVersions
                                                        Join cg In db.syCampGrps
                                                        On p.CampGrpId Equals cg.CampGrpId
                                                        Join cgc In db.syCmpGrpCmps
                                                        On cg.CampGrpId Equals cgc.CampGrpId
                                                        Join c In db.syCampus
                                                        On cgc.CampusId Equals c.CampusId
                                                        Join s In db.syStatus
                                                        On p.StatusId Equals s.StatusId
                                                        Where c.CampusId.Equals(campusId) _
                                                              AndAlso stats.Contains(s.Status) _
                                                        AndAlso Not selected.Contains(p.PrgVerId.ToString)
                                                        Select p Distinct
                                                        Order By p.PrgVerDescrip)
            Return query.ToList

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function GetProgramsHavingApprovedNaccasProgramVersionByCampusId(ByVal campusId As Guid, ByVal StatusTypes As List(Of String), Optional ByVal SelectedPrograms As List(Of String) = Nothing) As List(Of NaccasProgram)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            'reset count
            i = 0
            Dim selected(SelectedPrograms.Count - 1) As String
            For Each item As String In SelectedPrograms
                selected(i) = item
                i = i + 1
            Next

            Dim query As IQueryable(Of NaccasProgram) = (From s In db.syNaccasSettings
                                                         Join apv In db.syApprovedNACCASProgramVersions On s.NaccasSettingId Equals apv.NaccasSettingId
                                                         Where s.CampusId = campusId AndAlso stats.Contains(apv.arPrgVersion.syStatus.Status) AndAlso
                                                                (selected.Length < 1 OrElse Not selected.Contains(apv.arPrgVersion.arProgram.ProgId.ToString))
                                                         Select New NaccasProgram With {.ProgramId = apv.arPrgVersion.arProgram.ProgId,
                                                                 .ProgramDescription = apv.arPrgVersion.arProgram.ProgDescrip} Distinct
                                                     ).OrderBy(Function(obj) obj.ProgramDescription)

            Return query.ToList
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetProgramVersionsByCampusId(ByVal campuses As List(Of String), ByVal StatusTypes As List(Of String), ByVal SelectedProgramVersions As List(Of String)) As List(Of arPrgVersion)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            'reset count
            i = 0
            Dim selected(SelectedProgramVersions.Count - 1) As String
            For Each item As String In SelectedProgramVersions
                selected(i) = item
                i = i + 1
            Next

            Dim query As IQueryable(Of arPrgVersion) = (From p In db.arPrgVersions
                                                        Join cg In db.syCampGrps
                                                        On p.CampGrpId Equals cg.CampGrpId
                                                        Join cgc In db.syCmpGrpCmps
                                                        On cg.CampGrpId Equals cgc.CampGrpId
                                                        Join c In db.syCampus
                                                        On cgc.CampusId Equals c.CampusId
                                                        Join s In db.syStatus
                                                        On p.StatusId Equals s.StatusId
                                                        Where campuses.Contains(c.CampusId.ToString) _
                                                              AndAlso stats.Contains(s.Status) _
                                                        AndAlso Not selected.Contains(p.PrgVerId.ToString)
                                                        Select p Distinct
                                                        Order By p.PrgVerDescrip)
            Return query.ToList

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetProgramVersionsByCampusIdandProgId(ByVal CampusId As Guid, ByVal StatusTypes As List(Of String),
                                                          ByVal ProgId As Guid) As List(Of arPrgVersion)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            Dim query As IQueryable(Of arPrgVersion) = (From p In db.arPrgVersions
                                                        Join cg In db.syCampGrps
                                                        On p.CampGrpId Equals cg.CampGrpId
                                                        Join cgc In db.syCmpGrpCmps
                                                        On cg.CampGrpId Equals cgc.CampGrpId
                                                        Join c In db.syCampus
                                                        On cgc.CampusId Equals c.CampusId
                                                        Join s In db.syStatus
                                                        On p.StatusId Equals s.StatusId
                                                        Where c.CampusId.Equals(CampusId) _
                                                        AndAlso p.ProgId.Equals(ProgId) _
                                                        AndAlso stats.Contains(s.Status)
                                                        Select p Distinct
                                                        Order By p.PrgVerDescrip)



            Return query.ToList


        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetProgramVersionsByCampusIdandProgramIdCollection(ByVal CampusId As Guid, ByVal StatusTypes As List(Of String),
                                                 ByVal SelectedProgramVersions As List(Of String),
                                                 ByVal ProgId As Guid) As List(Of arPrgVersion)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            'reset count
            i = 0
            Dim selected(SelectedProgramVersions.Count - 1) As String
            For Each item As String In SelectedProgramVersions
                selected(i) = item
                i = i + 1
            Next

            Dim query As IQueryable(Of arPrgVersion) = (From p In db.arPrgVersions
                                                        Join cg In db.syCampGrps
                                                        On p.CampGrpId Equals cg.CampGrpId
                                                        Join cgc In db.syCmpGrpCmps
                                                        On cg.CampGrpId Equals cgc.CampGrpId
                                                        Join c In db.syCampus
                                                        On cgc.CampusId Equals c.CampusId
                                                        Join s In db.syStatus
                                                        On p.StatusId Equals s.StatusId
                                                        Where c.CampusId.Equals(CampusId) _
                                                        AndAlso p.ProgId.Equals(ProgId) _
                                                        AndAlso stats.Contains(s.Status) _
                                                        AndAlso Not selected.Contains(p.PrgVerId.ToString)
                                                        Select p Distinct
                                                        Order By p.PrgVerDescrip)
            Return query.ToList

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetProgramVersionsOnlyClockHoursByCampusId(ByVal CampusId As Guid, ByVal StatusTypes As List(Of String)) As List(Of arPrgVersion)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            Dim query As IQueryable(Of arPrgVersion) = (From p In db.arPrgVersions
                                                        Join cg In db.syCampGrps
                                                        On p.CampGrpId Equals cg.CampGrpId
                                                        Join cgc In db.syCmpGrpCmps
                                                        On cg.CampGrpId Equals cgc.CampGrpId
                                                        Join c In db.syCampus
                                                        On cgc.CampusId Equals c.CampusId
                                                        Join s In db.syStatus
                                                        On p.StatusId Equals s.StatusId
                                                        Join a In db.arAttUnitTypes
                                                        On p.UnitTypeId Equals a.UnitTypeId
                                                        Where c.CampusId.Equals(CampusId) _
                                                        AndAlso stats.Contains(s.Status) _
                                                        AndAlso a.UnitTypeDescrip = "Clock Hours"
                                                        Select p Distinct
                                                        Order By p.PrgVerDescrip)
            Return query.ToList


        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetProgramVersionsOnlyClockHoursByCampusIdandProgId(ByVal CampusId As Guid, ByVal StatusTypes As List(Of String),
                                                              ByVal ProgId As Guid) As List(Of arPrgVersion)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            Dim query As IQueryable(Of arPrgVersion) = (From p In db.arPrgVersions
                                                        Join cg In db.syCampGrps
                                                        On p.CampGrpId Equals cg.CampGrpId
                                                        Join cgc In db.syCmpGrpCmps
                                                        On cg.CampGrpId Equals cgc.CampGrpId
                                                        Join c In db.syCampus
                                                        On cgc.CampusId Equals c.CampusId
                                                        Join s In db.syStatus
                                                        On p.StatusId Equals s.StatusId
                                                        Join a In db.arAttUnitTypes
                                                        On p.UnitTypeId Equals a.UnitTypeId
                                                        Where c.CampusId.Equals(CampusId) _
                                                        AndAlso p.ProgId.Equals(ProgId) _
                                                        AndAlso stats.Contains(s.Status) _
                                                        AndAlso a.UnitTypeDescrip = "Clock Hours"
                                                        Select p Distinct
                                                        Order By p.PrgVerDescrip)
            Return query.ToList


        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetProgramVersionsOnlyClockHoursByCampusId(ByVal CampusId As Guid, ByVal StatusTypes As List(Of String), ByVal SelectedProgramVersions As List(Of String)) As List(Of arPrgVersion)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            'reset count
            i = 0
            Dim selected(SelectedProgramVersions.Count - 1) As String
            For Each item As String In SelectedProgramVersions
                selected(i) = item
                i = i + 1
            Next

            Dim query As IQueryable(Of arPrgVersion) = (From p In db.arPrgVersions
                                                        Join cg In db.syCampGrps
                                                        On p.CampGrpId Equals cg.CampGrpId
                                                        Join cgc In db.syCmpGrpCmps
                                                        On cg.CampGrpId Equals cgc.CampGrpId
                                                        Join c In db.syCampus
                                                        On cgc.CampusId Equals c.CampusId
                                                        Join s In db.syStatus
                                                        On p.StatusId Equals s.StatusId
                                                        Join a In db.arAttUnitTypes
                                                        On p.UnitTypeId Equals a.UnitTypeId
                                                        Where c.CampusId.Equals(CampusId) _
                                                        AndAlso stats.Contains(s.Status) _
                                                        AndAlso Not selected.Contains(p.PrgVerId.ToString) _
                                                        AndAlso a.UnitTypeDescrip = "Clock Hours"
                                                        Select p Distinct
                                                        Order By p.PrgVerDescrip)
            Return query.ToList

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetProgramVersionsOnlyClockHoursByCampusIdandProgramIdCollection(ByVal CampusId As Guid, ByVal StatusTypes As List(Of String),
                                                 ByVal SelectedProgramVersions As List(Of String),
                                                 ByVal ProgId As Guid) As List(Of arPrgVersion)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            'reset count
            i = 0
            Dim selected(SelectedProgramVersions.Count - 1) As String
            For Each item As String In SelectedProgramVersions
                selected(i) = item
                i = i + 1
            Next

            Dim query As IQueryable(Of arPrgVersion) = (From p In db.arPrgVersions
                                                        Join cg In db.syCampGrps
                                                        On p.CampGrpId Equals cg.CampGrpId
                                                        Join cgc In db.syCmpGrpCmps
                                                        On cg.CampGrpId Equals cgc.CampGrpId
                                                        Join c In db.syCampus
                                                        On cgc.CampusId Equals c.CampusId
                                                        Join s In db.syStatus
                                                        On p.StatusId Equals s.StatusId
                                                        Join a In db.arAttUnitTypes
                                                        On p.UnitTypeId Equals a.UnitTypeId
                                                        Where c.CampusId.Equals(CampusId) _
                                                        AndAlso p.ProgId.Equals(ProgId) _
                                                        AndAlso stats.Contains(s.Status) _
                                                        AndAlso Not selected.Contains(p.PrgVerId.ToString) _
                                                        AndAlso a.UnitTypeDescrip = "Clock Hours"
                                                        Select p Distinct
                                                        Order By p.PrgVerDescrip)
            Return query.ToList

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetProgramVersionByProgram(ByVal programs As List(Of String), ByVal StatusTypes As List(Of String), Optional ByVal SelectedProgramVersions As List(Of String) = Nothing) As List(Of arPrgVersion)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            Dim query As IQueryable(Of arPrgVersion) = From p In db.arPrgVersions
                                                       Join s In db.syStatus On p.StatusId Equals s.StatusId
                                                       Where programs.Contains(p.ProgId.ToString) _
                                                             AndAlso stats.Contains(s.Status) _
                                                             AndAlso (SelectedProgramVersions Is Nothing OrElse (SelectedProgramVersions IsNot Nothing AndAlso Not SelectedProgramVersions.Contains(p.PrgVerId.ToString)))
                                                       Select p Distinct


            Return query.ToList


        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetProgramVersionForProgram(programs As List(Of String)) As List(Of arPrgVersion)
        Try
            Dim query As IQueryable(Of arPrgVersion) = From pv In db.arPrgVersions
                                                       Where programs.Contains(pv.ProgId.ToString)
                                                       Order By pv.PrgVerDescrip
                                                       Select pv Distinct

            Return query.ToList
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetRequirementsForProgramVersion(ByVal programVersionId As String) As List(Of arReq)
        Dim programVersion = (From pv In db.arPrgVersions
                              Where pv.PrgVerId.ToString = programVersionId
                              Select pv).FirstOrDefault()

        Dim results = New List(Of arReq)
        If (programVersion IsNot Nothing) Then
            results = (From r In db.arReqs
                       Where r.arProgVerDefs.Any(Function(pvd As arProgVerDef) pvd.arClassSections.Any(Function(cs As arClassSection) cs.arProgVerDef.PrgVerId.ToString = programVersionId))
                       Select r).OrderBy(Function(req As arReq) req.Descrip).ToList()

        End If

        Return results
    End Function

    public Function IsProgramRegistrationType(ByVal programVersionId As String) As Boolean
        Dim programVersion = (From pv In db.arPrgVersions
                Where pv.PrgVerId.ToString = programVersionId
                Select pv).FirstOrDefault()
        Dim result = False
        If (programVersion IsNot Nothing) Then
            result = programVersion.ProgramRegistrationType.HasValue AndAlso  programVersion.ProgramRegistrationType.Value = 1
        End If
        Return result
    End Function

    Public Function GetProgramVersionProgramId(programVersionId As String, studentId As String) As Guid
        Try
            Dim oldEnollId = (From e In db.arStuEnrollments Join sc In db.syStatusCodes On e.StatusCodeId Equals sc.StatusCodeId Join ss In db.syStatus On sc.StatusId Equals ss.StatusId
                    Where e.StudentId = guid.Parse(studentId) And e.PrgVerId = guid.Parse(programVersionId) And ss.Status = "Active" Select e).FirstOrDefault()
            If(oldEnollId IsNot Nothing) Then
                Return oldEnollId.StuEnrollId
            Else 
                    Return Guid.Empty
            End If
            
               
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
