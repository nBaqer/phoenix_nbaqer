﻿Public Class FundSourcesDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub

    Public Function IsFundSourceTitleIV(fundSourceId As Guid) As Boolean
        Try
            Dim query As IQueryable(Of Nullable(Of Boolean)) = (From fs In db.saFundSources
                                                                Where fs.FundSourceId = fundSourceId
                                                                Select fs.TitleIV)
            Dim result = query.FirstOrDefault()
            Return If(result, False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function IsFundSourceDirectLoan(fundSourceId As String) As Boolean
        Try
            Dim advantageFundSourceListForDirectLoans = New List(Of Byte?)

            advantageFundSourceListForDirectLoans.Add(7) 'Subs
            advantageFundSourceListForDirectLoans.Add(8) 'Unsub
            advantageFundSourceListForDirectLoans.Add(6) 'PLUS
            Dim query = (From fs In db.saFundSources
                         Where fs.FundSourceId.ToString = fundSourceId AndAlso advantageFundSourceListForDirectLoans.Contains(fs.AdvFundSourceId)
                         Select fs)
            Dim result = query.FirstOrDefault()
            Return If(result IsNot Nothing, True, False)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
