﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Imports LinqKit
Imports System.Linq.Expressions
Imports FAME.Parameters.Collections

Public Class StudentDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function GetStudents() As List(Of arStudent)
        Try
            Dim query As IQueryable(Of arStudent) = (From s In db.arStudents _
                                                     Order By s.LastName.Trim, s.FirstName.Trim)

            Dim result As List(Of arStudent) = query.ToList()
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetMRUStudents(ByVal user As Guid, ByVal campus As Guid) As List(Of arStudent)
        Try
            Dim query As IQueryable(Of arStudent) = (From s In db.arStudents _
                                                     Join m In db.syMRUS _
                                                     On s.StudentId Equals m.ChildId _
                                                     Where m.MRUTypeId = 1 _
                                                     And m.UserId = user _
                                                     And m.CampusId = campus _
                                                     Select s _
                                                     Order By s.LastName.Trim, s.FirstName.Trim)

            Dim result As List(Of arStudent) = query.ToList()
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetStudents2() As IQueryable(Of arStudent)
        Try
            Dim query As IQueryable(Of arStudent) = (From s In db.arStudents _
                                                     Order By s.LastName.Trim, s.FirstName.Trim)

            Dim result As IQueryable(Of arStudent) = query
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetStudents3(ByVal filtervalues As MasterDictionary) As List(Of arStudent)
        Try
            Dim query As IQueryable(Of arStudent) = (From s In db.arStudents _
                                                     Order By s.LastName.Trim, s.FirstName.Trim)
            Dim basePredicate As Expression(Of Func(Of arStudent, Boolean)) = PredicateBuilder.True(Of arStudent)()
            basePredicate = basePredicate.And(Function(s) True)

            If filtervalues.Contains("StudentSearch") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("StudentSearch"), DetailDictionary)
                If objPsFilter.Contains("Status") Then
                    Dim StatusValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("Status"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of arStudent, Boolean)) = PredicateBuilder.False(Of arStudent)()
                    For Each di As DictionaryEntry In StatusValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(StatusValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList

                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))
                                    For Each status As String In mylist
                                        Dim temp As String = status
                                        predicate = predicate.Or(Function(s As arStudent) s.syStatus.Status.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & StatusValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
                If objPsFilter.Contains("CurrentlySelected") Then
                    Dim CurrentlySelectedValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CurrentlySelected"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of arStudent, Boolean)) = PredicateBuilder.True(Of arStudent)()
                    For Each di As DictionaryEntry In CurrentlySelectedValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CurrentlySelectedValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))
                                    For Each selected As String In mylist
                                        Dim temp As String = selected
                                        predicate = predicate.And(Function(s As arStudent) Not s.StudentId.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CurrentlySelectedValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
            End If
            Dim result As List(Of arStudent) = query.ToList
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function GetAllCampusGroup() As syCampGrp
        Try
            Dim query As syCampGrp = (From cg In db.syCampGrps _
                                      Select cg Where cg.IsAllCampusGrp.Equals(True)).SingleOrDefault
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
