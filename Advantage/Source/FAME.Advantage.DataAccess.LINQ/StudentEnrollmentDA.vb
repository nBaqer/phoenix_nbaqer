﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Imports LinqKit
Imports System.Linq.Expressions
Imports FAME.Advantage.DataAccess.LINQ.Common
Imports FAME.Parameters.Collections
Imports FAME.Advantage.DataAccess.LINQ.EnumsDA

Public Class StudentEnrollmentDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Private Function CreateCampGrpIdListByUserId(ByVal filtervalues As MasterDictionary) As String()
        Try
            If filtervalues.Contains("ProgramVersion") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("ProgramVersion"), DetailDictionary)
                If objPsFilter.Contains("UserId") Then
                    Dim UserCollection As New ParamValueList(Of Guid)
                    Dim UserValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("UserId"), ParamValueDictionary)
                    For Each di As DictionaryEntry In UserValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(UserValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)

                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                UserCollection = DirectCast(FModDictionary.Item("UserId"), ParamValueList(Of Guid))
                                Dim users(UserCollection.Count - 1) As String
                                Dim i As Integer = 0
                                For Each user As Guid In UserCollection
                                    users(i) = user.ToString
                                    i = i + 1
                                Next
                                Dim query As IQueryable(Of syUsersRolesCampGrp) = (From urcg In db.syUsersRolesCampGrps Where users.Contains(urcg.UserId.ToString))
                                Dim results As List(Of syUsersRolesCampGrp) = query.ToList
                                'Look for All Campus Group ID
                                Dim query2 As IQueryable(Of syUsersRolesCampGrp) = (From q In query Where q.CampGrpId.Equals(GetAllCampusGroup()))
                                Dim myids As New List(Of syUsersRolesCampGrp)
                                myids = query2.ToList
                                'Add the All Campus Group if it doesn't already exist
                                If myids.Count = 0 Then
                                    Dim CampGrpIds(results.Count) As String
                                    CampGrpIds(0) = GetAllCampusGroup.CampGrpId.ToString
                                    'restart counter at one since we added the All Campus Group
                                    i = 1
                                    For Each item As syUsersRolesCampGrp In results
                                        CampGrpIds(i) = item.CampGrpId.ToString
                                        i = i + 1
                                    Next
                                    Return CampGrpIds
                                Else
                                    Dim CampGrpIds(results.Count - 1) As String
                                    'restart counter
                                    i = 0
                                    For Each item As syUsersRolesCampGrp In results
                                        CampGrpIds(i) = item.CampGrpId.ToString
                                        i = i + 1
                                    Next
                                    Return CampGrpIds
                                End If
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & UserValueDictionary.Name)
                        End Select
                    Next
                Else
                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                    Throw New Exception(currentmethod & " has UserId as a required filter.")
                End If
            Else
                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                Throw New Exception(currentmethod & " missing required filter.")
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    Private Function CreateCampGrpIdListByUserId(ByVal userids As String()) As String()
        Try
            Dim i As Integer = 0
            Dim query As IQueryable(Of syUsersRolesCampGrp) = (From urcg In db.syUsersRolesCampGrps Where userids.Contains(urcg.UserId.ToString))
            Dim results As List(Of syUsersRolesCampGrp) = query.ToList

            'Look for All Campus Group ID
            Dim query2 As IQueryable(Of syUsersRolesCampGrp) = (From q In query Where q.CampGrpId.Equals(GetAllCampusGroup().CampGrpId))
            Dim myids As New List(Of syUsersRolesCampGrp)
            myids = query2.ToList
            'Add the All Campus Group if it doesn't already exist
            If myids.Count = 0 Then
                Dim CampGrpIds(results.Count) As String
                CampGrpIds(0) = GetAllCampusGroup.CampGrpId.ToString
                'restart counter at one since we added the All Campus Group
                i = 1
                For Each item As syUsersRolesCampGrp In results
                    CampGrpIds(i) = item.CampGrpId.ToString
                    i = i + 1
                Next
                Return CampGrpIds
            Else
                Dim CampGrpIds(results.Count - 1) As String
                'restart counter
                i = 0
                For Each item As syUsersRolesCampGrp In results
                    CampGrpIds(i) = item.CampGrpId.ToString
                    i = i + 1
                Next
                Return CampGrpIds
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    Private Function CreateCampIdListFromCampGrpIds(ByVal CampGrpIds As String(), ByVal UserName As String) As String()
        Try
            Dim i As Integer = 0
            Dim query As IQueryable(Of syCmpGrpCmp)
            If UserName.ToLower = "sa" Or UserName.ToLower = "super" Or UserName.ToLower = "support" Then
                query = (From c In db.syCmpGrpCmps Where CampGrpIds.Contains(c.CampGrpId.ToString))
            Else
                query = (From c In db.syCmpGrpCmps Where CampGrpIds.Contains(c.CampGrpId.ToString)) ' AndAlso Not c.CampGrpId.Equals(GetAllCampusGroup.CampGrpId))
            End If
            Dim results As List(Of syCmpGrpCmp) = query.ToList
            Dim CampusIds(results.Count - 1) As String
            For Each item As syCmpGrpCmp In results
                CampusIds(i) = item.CampusId.ToString
                i = i + 1
            Next
            Return CampusIds
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    Private Function CreateUserIdList(ByVal filtervalues As MasterDictionary) As String()
        Try
            If filtervalues.Contains("StudentEnrollSearch") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("StudentEnrollSearch"), DetailDictionary)
                If objPsFilter.Contains("UserId") Then
                    Dim UserCollection As New ParamValueList(Of Guid)
                    Dim UserValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("UserId"), ParamValueDictionary)
                    For Each di As DictionaryEntry In UserValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(UserValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)

                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                UserCollection = DirectCast(FModDictionary.Item("UserId"), ParamValueList(Of Guid))
                                Dim users(UserCollection.Count - 1) As String
                                Dim i As Integer = 0
                                For Each user As Guid In UserCollection
                                    users(i) = user.ToString
                                    i = i + 1
                                Next
                                Return users
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & UserValueDictionary.Name)
                        End Select
                    Next
                Else
                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                    Throw New Exception(currentmethod & " has UserId as a required filter.")
                End If
            Else
                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                Throw New Exception(currentmethod & " missing required filter.")
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    Private Function GetUserName(ByVal userid As Guid) As String
        Try
            Dim query As syUser = (From u In db.syUsers
                                   Select u Where u.UserId.Equals(userid)).SingleOrDefault
            Return query.UserName
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SearchStudents(ByVal isSuper As Boolean) As IQueryable(Of NewStudentSearch)

        Try
            Dim query As IQueryable(Of NewStudentSearch) = (From nss In db.NewStudentSearches)
            Return query
        Catch ex As Exception
            Throw ex
        End Try

        'Try
        '    Dim CurrentEnrollments As Integer = 0
        '    Dim query As IQueryable(Of NewStudentSearch)
        '    Dim querycount As IQueryable(Of NewStudentSearch)
        '    If isSuper = True Then
        '        query = (From nss In db.NewStudentSearches)
        '    Else
        '        Dim StatusList As Integer() = {7, 9, 10, 11, 20, 21, 22}
        '        querycount = (From nss In db.NewStudentSearches _
        '                              Where StatusList.Contains(nss.SysStatusId) _
        '                              Select nss)
        '        CurrentEnrollments = querycount.Count

        '        If CurrentEnrollments > 0 Then
        '            query = (From nss In db.NewStudentSearches Where StatusList.Contains(nss.SysStatusId))
        '        Else
        '            query = (From nss In db.NewStudentSearches)
        '        End If

        '    End If

        '    Return query
        'Catch ex As Exception
        '    Throw ex
        'End Try
    End Function
    Public Function GetStudentEnrollments(ByVal Users As String(), Optional ByVal filtervalues As MasterDictionary = Nothing) As IQueryable(Of StudentEnrollmentSearch)
        Try
            Dim userName As String = GetUserName(New Guid(Users(0)))
            Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(Users)
            Dim CampusIds As String() = CreateCampIdListFromCampGrpIds(CampGrpIds, userName)
            Dim query As IQueryable(Of StudentEnrollmentSearch) = (From se In db.StudentEnrollmentSearches
                                                                   Where CampusIds.Contains(se.CampusId.ToString)
                                                                   Select se Distinct
                                                                   Order By se.LastName.Trim, se.FirstName.Trim,
                                                                   se.PrgVerDescrip, se.SysStatusId,
                                                                   se.StatusCodeDescrip, se.ShiftId)

            Dim basePredicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.True(Of StudentEnrollmentSearch)()
            basePredicate = basePredicate.And(Function(se) True)

            If Not filtervalues Is Nothing Then



                If filtervalues.Contains("StudentEnrollSearch") Then
                    Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("StudentEnrollSearch"), DetailDictionary)
                    'If objPsFilter.Contains("Status") Then
                    '    Dim StatusValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("Status"), ParamValueDictionary)
                    '    Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.False(Of StudentEnrollmentSearch)()
                    '    For Each di As DictionaryEntry In StatusValueDictionary
                    '        Dim FModDictionary As ModDictionary = DirectCast(StatusValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                    '        Select Case CType(di.Key, ModDictionary.Modifier)
                    '            Case ModDictionary.Modifier.InList

                    '                For Each di2 As DictionaryEntry In FModDictionary

                    '                    Dim mylist As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))
                    '                    For Each status As String In mylist
                    '                        Dim temp As String = status
                    '                        predicate = predicate.Or(Function(se As StudentEnrollmentSearch) se.StatusCodedescrip.Equals(temp))
                    '                        'predicate = predicate.Or(Function(s As StudentEnrollmentSearch) s.syStatus.Status.Equals(temp))
                    '                    Next
                    '                Next
                    '            Case Else
                    '                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                    '                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & StatusValueDictionary.Name)
                    '        End Select
                    '    Next
                    '    basePredicate = basePredicate.And(predicate)
                    'End If

                    If objPsFilter.Contains("EnrollmentStatus") Then
                        Dim EnrollmentStatusValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("EnrollmentStatus"), ParamValueDictionary)
                        Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.False(Of StudentEnrollmentSearch)()
                        For Each di As DictionaryEntry In EnrollmentStatusValueDictionary
                            Dim FModDictionary As ModDictionary = DirectCast(EnrollmentStatusValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                            Select Case CType(di.Key, ModDictionary.Modifier)
                                Case ModDictionary.Modifier.InList

                                    For Each di2 As DictionaryEntry In FModDictionary

                                        Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                        For Each enrollmentstatus As Guid In mylist
                                            Dim temp As Guid = enrollmentstatus
                                            predicate = predicate.Or(Function(se As StudentEnrollmentSearch) se.StatusCodeId.Equals(temp))
                                        Next
                                    Next
                                Case Else
                                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                    Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & EnrollmentStatusValueDictionary.Name)
                            End Select
                        Next
                        basePredicate = basePredicate.And(predicate)
                    End If

                    If objPsFilter.Contains("CampusGroup") Then
                        Dim CampGrpIdValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CampusGroup"), ParamValueDictionary)
                        Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.False(Of StudentEnrollmentSearch)()
                        For Each di As DictionaryEntry In CampGrpIdValueDictionary
                            Dim FModDictionary As ModDictionary = DirectCast(CampGrpIdValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                            Select Case CType(di.Key, ModDictionary.Modifier)
                                Case ModDictionary.Modifier.InList
                                    For Each di2 As DictionaryEntry In FModDictionary
                                        Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                        'Add the "All" CampGrpId - We shouldn't need to do this but we do...
                                        'Dim InList As Boolean = False
                                        'For Each item As Guid In mylist
                                        Dim AllCampusGroup As syCampGrp = GetAllCampusGroup()
                                        ''Dim AllItem As Guid = New Guid("2AC97FC1-BB9B-450A-AA84-7C503C90A1EB")
                                        ' Dim AllItem As Guid = AllCampusGroup.CampGrpId
                                        '    If item = AllItem Then
                                        '        InList = True
                                        '    End If
                                        'Next
                                        'If InList = False Then
                                        '    mylist.Add(New Guid("2AC97FC1-BB9B-450A-AA84-7C503C90A1EB"))
                                        'End If
                                        For Each campgrpid As Guid In mylist
                                            Dim temp As String = campgrpid.ToString
                                            predicate = predicate.Or(Function(se As StudentEnrollmentSearch) (se.CampGrpId).Equals(temp))
                                        Next
                                    Next
                                Case Else
                                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                    Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CampGrpIdValueDictionary.Name)
                            End Select
                        Next
                        basePredicate = basePredicate.And(predicate)
                    End If

                    If objPsFilter.Contains("ProgramVersion") Then
                        Dim ProgramVersionValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("ProgramVersion"), ParamValueDictionary)
                        Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.False(Of StudentEnrollmentSearch)()
                        For Each di As DictionaryEntry In ProgramVersionValueDictionary
                            Dim FModDictionary As ModDictionary = DirectCast(ProgramVersionValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                            Select Case CType(di.Key, ModDictionary.Modifier)
                                Case ModDictionary.Modifier.InList
                                    For Each di2 As DictionaryEntry In FModDictionary

                                        Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                        For Each prgverid As Guid In mylist
                                            Dim temp As String = prgverid.ToString
                                            predicate = predicate.Or(Function(se As StudentEnrollmentSearch) (se.PrgVerId).Equals(temp))
                                        Next
                                    Next
                                Case Else
                                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                    Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & ProgramVersionValueDictionary.Name)
                            End Select
                        Next
                        basePredicate = basePredicate.And(predicate)
                    End If

                    If objPsFilter.Contains("StudentGroup") Then
                        Dim StudentGroupValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("StudentGroup"), ParamValueDictionary)
                        Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.False(Of StudentEnrollmentSearch)()
                        For Each di As DictionaryEntry In StudentGroupValueDictionary
                            Dim FModDictionary As ModDictionary = DirectCast(StudentGroupValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                            Select Case CType(di.Key, ModDictionary.Modifier)
                                Case ModDictionary.Modifier.InList
                                    For Each di2 As DictionaryEntry In FModDictionary
                                        Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                        For Each prgverid As Guid In mylist
                                            Dim temp As String = prgverid.ToString
                                            predicate = predicate.Or(Function(se As StudentEnrollmentSearch) (se.LeadGrpId).Equals(temp))
                                        Next
                                    Next
                                Case Else
                                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                    Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & StudentGroupValueDictionary.Name)
                            End Select
                        Next
                        basePredicate = basePredicate.And(predicate)
                    End If

                    If objPsFilter.Contains("CurrentlySelected") Then
                        Dim CurrentlySelectedValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CurrentlySelected"), ParamValueDictionary)
                        Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.True(Of StudentEnrollmentSearch)()
                        For Each di As DictionaryEntry In CurrentlySelectedValueDictionary
                            Dim FModDictionary As ModDictionary = DirectCast(CurrentlySelectedValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                            Select Case CType(di.Key, ModDictionary.Modifier)
                                Case ModDictionary.Modifier.InList
                                    For Each di2 As DictionaryEntry In FModDictionary

                                        Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                        For Each selected As Guid In mylist
                                            Dim temp As String = selected.ToString
                                            predicate = predicate.And(Function(se As StudentEnrollmentSearch) Not se.StuEnrollId.Equals(temp))
                                        Next
                                    Next
                                Case Else
                                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                    Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CurrentlySelectedValueDictionary.Name)
                            End Select
                        Next
                        basePredicate = basePredicate.And(predicate)
                    End If

                    If objPsFilter.Contains("CurrentlySelected") Then
                        Dim CurrentlySelectedValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CurrentlySelected"), ParamValueDictionary)
                        Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.True(Of StudentEnrollmentSearch)()
                        For Each di As DictionaryEntry In CurrentlySelectedValueDictionary
                            Dim FModDictionary As ModDictionary = DirectCast(CurrentlySelectedValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                            Select Case CType(di.Key, ModDictionary.Modifier)
                                Case ModDictionary.Modifier.InList
                                    For Each di2 As DictionaryEntry In FModDictionary

                                        Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                        For Each selected As Guid In mylist
                                            Dim temp As String = selected.ToString
                                            predicate = predicate.And(Function(se As StudentEnrollmentSearch) Not se.StuEnrollId.Equals(temp))
                                        Next
                                    Next
                                Case Else
                                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                    Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CurrentlySelectedValueDictionary.Name)
                            End Select
                        Next
                        basePredicate = basePredicate.And(predicate)
                    End If

                    If objPsFilter.Contains("CampusId") Then
                        Dim CurrentlySelectedValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CampusId"), ParamValueDictionary)
                        Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.True(Of StudentEnrollmentSearch)()
                        For Each di As DictionaryEntry In CurrentlySelectedValueDictionary
                            Dim FModDictionary As ModDictionary = DirectCast(CurrentlySelectedValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                            Select Case CType(di.Key, ModDictionary.Modifier)
                                Case ModDictionary.Modifier.InList
                                    For Each di2 As DictionaryEntry In FModDictionary

                                        Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                        For Each selected As Guid In mylist
                                            Dim temp As String = selected.ToString
                                            predicate = predicate.And(Function(se As StudentEnrollmentSearch) se.CampusId.Equals(temp))
                                        Next
                                    Next
                                Case Else
                                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                    Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CurrentlySelectedValueDictionary.Name)
                            End Select
                        Next
                        basePredicate = basePredicate.And(predicate)
                    End If

                End If


            End If

            Dim result As IQueryable(Of StudentEnrollmentSearch) = query.Where(basePredicate)
            'Dim test As List(Of StudentEnrollmentSearch) = result.ToList
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetMRUStudentEnrollments(ByVal user As String(), ByVal campus As Guid, ByVal filtervalues As MasterDictionary) As IQueryable(Of StudentEnrollmentSearch)
        Try
            Dim users As String() = CreateUserIdList(filtervalues)
            Dim query As IQueryable(Of StudentEnrollmentSearch) = (From se In db.StudentEnrollmentSearches
                                                                   Join m In db.syMRUS
                                                                   On se.StudentId Equals m.ChildId
                                                                   Where m.MRUTypeId = 1 _
                                                                   And users.Contains(m.UserId.ToString) _
                                                                   And m.CampusId = campus
                                                                   Select se
                                                                   Order By se.LastName.Trim, se.FirstName.Trim, se.PrgVerDescrip, se.SysStatusId,
                                                                               se.StatusCodeDescrip, se.ShiftId)

            Dim basePredicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.True(Of StudentEnrollmentSearch)()
            basePredicate = basePredicate.And(Function(se) True)

            If filtervalues.Contains("StudentEnrollSearch") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("StudentEnrollSearch"), DetailDictionary)
                'If objPsFilter.Contains("Status") Then
                '    Dim StatusValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("Status"), ParamValueDictionary)
                '    Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.False(Of StudentEnrollmentSearch)()
                '    For Each di As DictionaryEntry In StatusValueDictionary
                '        Dim FModDictionary As ModDictionary = DirectCast(StatusValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                '        Select Case CType(di.Key, ModDictionary.Modifier)
                '            Case ModDictionary.Modifier.InList

                '                For Each di2 As DictionaryEntry In FModDictionary

                '                    Dim mylist As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))
                '                    For Each status As String In mylist
                '                        Dim temp As String = status
                '                        predicate = predicate.Or(Function(se As StudentEnrollmentSearch) se.StatusCodedescrip.Equals(temp))
                '                        'predicate = predicate.Or(Function(s As StudentEnrollmentSearch) s.syStatus.Status.Equals(temp))
                '                    Next
                '                Next
                '            Case Else
                '                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                '                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & StatusValueDictionary.Name)
                '        End Select
                '    Next
                '    basePredicate = basePredicate.And(predicate)
                'End If

                If objPsFilter.Contains("EnrollmentStatus") Then
                    Dim EnrollmentStatusValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("EnrollmentStatus"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.False(Of StudentEnrollmentSearch)()
                    For Each di As DictionaryEntry In EnrollmentStatusValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(EnrollmentStatusValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList

                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                    For Each enrollmentstatus As Guid In mylist
                                        Dim temp As String = enrollmentstatus.ToString()
                                        predicate = predicate.Or(Function(se As StudentEnrollmentSearch) se.StatusCodeId.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & EnrollmentStatusValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If

                If objPsFilter.Contains("CampusGroup") Then
                    Dim CampGrpIdValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CampusGroup"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.False(Of StudentEnrollmentSearch)()
                    For Each di As DictionaryEntry In CampGrpIdValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CampGrpIdValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                    'Add the "All" CampGrpId - We shouldn't need to do this but we do...
                                    'Dim InList As Boolean = False
                                    'For Each item As Guid In mylist
                                    '    Dim AllItem As Guid = New Guid("2AC97FC1-BB9B-450A-AA84-7C503C90A1EB")
                                    '    If item = AllItem Then
                                    '        InList = True
                                    '    End If
                                    'Next
                                    'If InList = False Then
                                    '    mylist.Add(New Guid("2AC97FC1-BB9B-450A-AA84-7C503C90A1EB"))
                                    'End If
                                    For Each campgrpid As Guid In mylist
                                        Dim temp As String = campgrpid.ToString
                                        predicate = predicate.Or(Function(se As StudentEnrollmentSearch) (se.CampGrpId).Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CampGrpIdValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If

                If objPsFilter.Contains("ProgramVersion") Then
                    Dim ProgramVersionValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("ProgramVersion"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.False(Of StudentEnrollmentSearch)()
                    For Each di As DictionaryEntry In ProgramVersionValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(ProgramVersionValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                    For Each prgverid As Guid In mylist
                                        Dim temp As String = prgverid.ToString
                                        predicate = predicate.Or(Function(se As StudentEnrollmentSearch) (se.PrgVerId).Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & ProgramVersionValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If

                If objPsFilter.Contains("StudentGroup") Then
                    Dim StudentGroupValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("StudentGroup"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.False(Of StudentEnrollmentSearch)()
                    For Each di As DictionaryEntry In StudentGroupValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(StudentGroupValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary
                                    Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                    For Each prgverid As Guid In mylist
                                        Dim temp As String = prgverid.ToString
                                        predicate = predicate.Or(Function(se As StudentEnrollmentSearch) (se.LeadGrpId).Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & StudentGroupValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If

                If objPsFilter.Contains("CurrentlySelected") Then
                    Dim CurrentlySelectedValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CurrentlySelected"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.True(Of StudentEnrollmentSearch)()
                    For Each di As DictionaryEntry In CurrentlySelectedValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CurrentlySelectedValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                    For Each selected As Guid In mylist
                                        Dim temp As String = selected.ToString
                                        predicate = predicate.And(Function(se As StudentEnrollmentSearch) Not se.StuEnrollId.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CurrentlySelectedValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If

                If objPsFilter.Contains("CampusId") Then
                    Dim CurrentlySelectedValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CampusId"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of StudentEnrollmentSearch, Boolean)) = PredicateBuilder.True(Of StudentEnrollmentSearch)()
                    For Each di As DictionaryEntry In CurrentlySelectedValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CurrentlySelectedValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                    For Each selected As Guid In mylist
                                        Dim temp As String = selected.ToString
                                        predicate = predicate.And(Function(se As StudentEnrollmentSearch) se.CampusId.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CurrentlySelectedValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If

            End If
            Dim result As IQueryable(Of StudentEnrollmentSearch) = query.Where(basePredicate)
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetStuEnrollmentsByStuEnrollId(ByVal Users As String(), ByVal stuenrolids As String()) As IQueryable(Of StudentEnrollmentSearch)
        Try
            Dim userName As String = GetUserName(New Guid(Users(0)))
            Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(Users)
            Dim CampusIds As String() = CreateCampIdListFromCampGrpIds(CampGrpIds, userName)
            Dim query As IQueryable(Of StudentEnrollmentSearch) = (From se In db.StudentEnrollmentSearches
                                                                   Where CampusIds.Contains(se.CampusId.ToString) _
                                                                   And stuenrolids.Contains(se.StuEnrollId.ToString)
                                                                   Select se Distinct
                                                                   Order By se.LastName.Trim, se.FirstName.Trim,
                                                                   se.PrgVerDescrip, se.SysStatusId,
                                                                   se.StatusCodeDescrip, se.ShiftId)


            Dim result As IQueryable(Of StudentEnrollmentSearch) = query
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function GetAllCampusGroup() As syCampGrp
        Try
            Dim query As syCampGrp = (From cg In db.syCampGrps
                                      Select cg Where cg.IsAllCampusGrp.Equals(True)).SingleOrDefault
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function USP_getEnrollment_byCampus(ByVal Campus As Guid, ByVal ShowInactive As Boolean) As IEnumerable(Of USP_getEnrollment_byCampusResult)
        Try
            Dim query As IList(Of USP_getEnrollment_byCampusResult) = db.USP_getEnrollment_byCampus(Campus, ShowInactive).ToList()
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetEnrollemnts(selectedCampuses As List(Of String), selectedPrograms As List(Of String), selectedTitleIVStatuses As List(Of String), getStatusFilters As List(Of String), Optional selectedEnrollments As List(Of String) = Nothing) As List(Of GenericListItem)

        Dim result As IQueryable(Of GenericListItem) = (From e In db.arStuEnrollments
                                                        Join pv In db.arPrgVersions On pv.PrgVerId Equals e.PrgVerId
                                                        Join p In db.arPrograms On pv.ProgId Equals p.ProgId
                                                        Join c In db.syCampus On e.CampusId Equals c.CampusId
                                                        Join s In db.arStudents On e.StudentId Equals s.StudentId
                                                        Join r In db.arFASAPChkResults On r.StuEnrollId Equals e.StuEnrollId
                                                        Where Not selectedEnrollments.Contains(e.StuEnrollId.ToString()) _
                                                          AndAlso selectedCampuses.Contains(c.CampusId.ToString()) _
                                                          AndAlso selectedPrograms.Contains(p.ProgId.ToString()) _
                                                          AndAlso selectedTitleIVStatuses.Contains(r.TitleIVStatusId.ToString())
                                                        Order By s.LastName
                                                        Select New GenericListItem With {.Id = e.StuEnrollId.ToString(),
                                                                          .Description = s.LastName + ", " + s.FirstName + "(" + p.ProgDescrip + ")"})



        Return result.Distinct().ToList()
    End Function

    Public Function GetEnrollmentsForAFA(enrollments As IEnumerable(Of String)) As List(Of AfaEnrollment)

        Dim result = (From e In db.arStuEnrollments
                      Join l In db.adLeads On e.StudentId Equals l.StudentId
                      Join c In db.syCampus On c.CampusId Equals e.CampusId
                      Where enrollments.Contains(e.StuEnrollId.ToString())
                      Select New AfaEnrollment With {.StudentEnrollmentId = e.StuEnrollId, .AFAStudentId = l.AfaStudentId,
                      .ExpectedGradDate = e.ExpGradDate, .StartDate = e.StartDate, .ProgramVersionId = e.PrgVerId, .ProgramName = e.arPrgVersion.PrgVerDescrip, .CmsId = c.CmsId, .CampusId = e.CampusId})

        Return result.ToList()
    End Function
    Public Function IsEnrollmentByProgram(enrollmentId As Guid) As Boolean

        Dim result As IQueryable(Of Boolean?) = (From e In db.arStuEnrollments
                                                 Join pv In db.arPrgVersions On e.PrgVerId Equals pv.PrgVerId
                                                 Where e.StuEnrollId = enrollmentId
                                                 Select pv.ProgramRegistrationType = EnumsDA.ProgramRegistrationType.ByProgram)


        Return If(result.FirstOrDefault, False)
    End Function

    Public Function IsGPAForEnrollmentWeighted(ByVal enrollmentId As String) As Boolean
        Dim result = (From e In db.arStuEnrollments
                      Where e.StuEnrollId.ToString() = enrollmentId
                      Select e.arPrgVersion.DoCourseWeightOverallGPA).FirstOrDefault()

        Return result
    End Function

    Public Function GetTransferProgramsForEnrollment(ByVal enrollmentId As String) As List(Of EnrollmentProgram)
        Dim results = New List(Of EnrollmentProgram)
        Dim leadIdentifier = (From e In db.arStuEnrollments
                              Where e.StuEnrollId.ToString() = enrollmentId
                              Select e.LeadId).FirstOrDefault()

        Dim sySysstatusIds = New List(Of Integer)({12, 16, 19})

        If (leadIdentifier <> Guid.Empty) Then
            results = (From e In db.arStuEnrollments
                       Where e.LeadId = leadIdentifier AndAlso e.StuEnrollId.ToString() <> enrollmentId AndAlso sySysstatusIds.Contains(e.syStatusCode.SysStatusId)
                       Select New EnrollmentProgram With {
                           .EnrollmentId = e.StuEnrollId,
                           .ProgramDescription = e.arPrgVersion.arProgram.ProgDescrip,
                           .ProgramId = e.arPrgVersion.ProgId
                        }).ToList()
        End If



        Return results
    End Function

    Public Function GetTransferProgramsForLead(ByVal leadId As String, Optional enrollmentId As String = Nothing) As List(Of EnrollmentProgram)


        Dim sySysstatusIds = New List(Of Integer)({12, 16, 19})
        Dim leadIdentifier As Guid
        Dim results = New List(Of EnrollmentProgram)
        If (Guid.TryParse(leadId, leadIdentifier) AndAlso leadIdentifier <> Guid.Empty) Then
            results = (From e In db.arStuEnrollments
                           Join sc In db.syStatusCodes On e.StatusCodeId Equals sc.StatusCodeId
                       Where e.LeadId = leadIdentifier AndAlso ( enrollmentId Is Nothing OrElse e.StuEnrollId.ToString() <> enrollmentId) andalso sySysstatusIds.Contains(e.syStatusCode.SysStatusId)
                       Select New EnrollmentProgram With {
                           .EnrollmentId = e.StuEnrollId,
                           .ProgramDescription = e.arPrgVersion.PrgVerDescrip + " ("+ sc.StatusCodeDescrip + ")" ,
                           .ProgramId = e.arPrgVersion.ProgId
                        }).ToList()
        End If



        Return results
    End Function

    Public Sub UpdateTransferHours(studentEnrollmentId As String, transferHoursValue As Decimal, transferHoursFromThisSchoolValue As Decimal, transferHoursFromProgramId As String)
        Dim enrollment = (From e In db.arStuEnrollments
                          Where e.StuEnrollId.ToString() = studentEnrollmentId
                          Select e).FirstOrDefault()

        If (enrollment IsNot Nothing) Then
            enrollment.TransferHours = transferHoursValue
            enrollment.TotalTransferHoursFromThisSchool = transferHoursFromThisSchoolValue
            Dim transferHours As Guid
            If (Guid.TryParse(transferHoursFromProgramId, transferHours) And transferHours <> Guid.Empty) Then
                enrollment.TransferHoursFromThisSchoolEnrollmentId = transferHours
            End If
            db.SubmitChanges()
        End If
    End Sub

    Public Function GetNewlyTransferEnrollmentId(studentId As String, campusId As String, programVersionId As String) As Guid
        Dim enrollmentId = (From e In db.arStuEnrollments
                            Where e.StudentId.ToString() = studentId AndAlso e.CampusId.ToString() = campusId AndAlso e.PrgVerId.ToString() = programVersionId
                            Select e.StuEnrollId).FirstOrDefault()

        Return enrollmentId
    End Function

    Public Function IsStudentProgramClockHour(ByVal enrollmentId As String) As Boolean
        Dim result = (From e In db.arStuEnrollments
                      Where e.StuEnrollId.ToString() = enrollmentId And e.arPrgVersion.arProgram.ACId = AdvantageSystemAcademicCalendars.ClockHour
                      Select e.arPrgVersion.PrgVerId).Any()
        Return result
    End Function
End Class
