﻿'Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Imports LinqKit
Imports System.Linq.Expressions
Imports FAME.Parameters.Collections

Public Class TermDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Private Function GetUserName(ByVal userid As Guid) As String
        Try
            Dim query As syUser = (From u In db.syUsers _
                                   Select u Where u.UserId.Equals(userid)).SingleOrDefault
            Return query.UserName
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function CreateCampGrpIdListByUserId(ByVal filtervalues As MasterDictionary) As String()
        Try
            If filtervalues.Contains("Term") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("Term"), DetailDictionary)
                If objPsFilter.Contains("UserId") Then
                    Dim UserCollection As New ParamValueList(Of Guid)
                    Dim UserValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("UserId"), ParamValueDictionary)
                    For Each di As DictionaryEntry In UserValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(UserValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                UserCollection = DirectCast(FModDictionary.Item("UserId"), ParamValueList(Of Guid))
                                Dim users(UserCollection.Count - 1) As String
                                Dim i As Integer = 0
                                For Each user As Guid In UserCollection
                                    users(i) = user.ToString
                                    i = i + 1
                                Next

                                Dim userName As String = GetUserName(New Guid(users(0)))
                                Dim query As IQueryable(Of syCampGrp)
                                If userName.ToLower = "sa" Or userName.ToLower = "support" Or userName.ToLower = "super" Then
                                    query = (From cg In db.syCampGrps)
                                Else
                                    Dim sub1 = (From cg In db.syCampGrps _
                                            Join urcg In db.syUsersRolesCampGrps _
                                            On cg.CampGrpId Equals urcg.CampGrpId _
                                            Where users.Contains(urcg.UserId.ToString) _
                                            Select cg.CampGrpId Distinct)
                                    Dim IdList1 As List(Of Guid) = sub1.ToList

                                    Dim sub2 = (From cgc In db.syCmpGrpCmps Where IdList1.Contains(cgc.CampGrpId) Select cgc.CampusId Distinct)
                                    Dim IdList2 As List(Of Guid) = sub2.ToList
                                    query = (From cg In db.syCampGrps _
                                            Join cgc In db.syCmpGrpCmps _
                                            On cg.CampGrpId Equals cgc.CampGrpId _
                                            Where IdList2.Contains(cgc.CampusId) _
                                            Select cg Distinct)
                                    'And cg.IsAllCampusGrp = False
                                End If

                                Dim results As List(Of syCampGrp) = query.ToList
                                Dim CampGrpIds(results.Count - 1) As String
                                'restart counter
                                i = 0
                                For Each item As syCampGrp In results
                                    CampGrpIds(i) = item.CampGrpId.ToString
                                    i = i + 1
                                Next
                                Return CampGrpIds
                                'End If
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & UserValueDictionary.Name)
                        End Select
                    Next
                Else
                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                    Throw New Exception(currentmethod & " has UserId as a required filter.")
                End If
            Else
                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                Throw New Exception(currentmethod & " missing required filter.")
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    
    Public Function DeleteTermByProgramVersion (ByVal programVersionId As String) As String
        Try
            Dim enrollments = (From e in db.arStuEnrollments
                    Where e.PrgVerId.ToString() = programVersionId).ToList()

            if enrollments.Count > 0 Then
                Return "Cannot delete program version because it has enrollments."
            End If

            Dim term As arTerm = (From t In db.arTerms _
                    Where t.ProgramVersionId.HasValue AndAlso  t.ProgramVersionId.Value.ToString() = programVersionId 
                    Select t ).FirstOrDefault()
            If Not term Is Nothing Then
                db.arTerms.DeleteOnSubmit(term)
                db.SubmitChanges()
            End If
           
            Return String.Empty
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CreateCampGrpIdListByCampusId(ByVal campusid As Guid) As String()
        Try
            Dim i As Integer = 0
            Dim query As IQueryable(Of syCmpGrpCmp) = (From cgc In db.syCmpGrpCmps Where cgc.CampusId.Equals(campusid))
            Dim results As List(Of syCmpGrpCmp) = query.ToList


            Dim CampGrpIds(results.Count - 1) As String
            'restart  i = 0
            For Each item As syCmpGrpCmp In results
                CampGrpIds(i) = item.CampGrpId.ToString
                i = i + 1
            Next
            Return CampGrpIds


        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    Public Function GetTermsByCampusId(ByVal campusid As Guid) As IQueryable(Of arTerm)
        Try
            Dim CampGrpIds As String() = CreateCampGrpIdListByCampusId(campusid)
            Dim query As IQueryable(Of arTerm) = (From t In db.arTerms _
                                              Join st In db.syStatus _
                                              On t.StatusId Equals st.StatusId _
                                              Join cg In db.syCampGrps _
                                              On t.CampGrpId Equals cg.CampGrpId _
                                              Where t.StartDate <= Date.Now _
                                              And CampGrpIds.Contains(t.CampGrpId.ToString) _
                                              And st.Status = "Active"
                                              Select t Distinct _
                                              Order By t.StartDate Descending, t.TermDescrip)
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetAllTermsByCampusId(ByVal campusId As Guid, ByVal statusTypes As List(Of String)) As List(Of arTerm)
        Try
            Dim i As Integer = 0
            Dim stats(statusTypes.Count - 1) As String
            For Each item As String In statusTypes
                stats(i) = item
                i = i + 1
            Next
            Dim query As IQueryable(Of arTerm) = (From p In db.arTerms
                                                  Join cg In db.syCampGrps
                                                     On p.CampGrpId Equals cg.CampGrpId
                                                  Join cgc In db.syCmpGrpCmps
                                                     On cg.CampGrpId Equals cgc.CampGrpId
                                                  Join c In db.syCampus
                                                     On cgc.CampusId Equals c.CampusId
                                                  Join s In db.syStatus
                                                     On p.StatusId Equals s.StatusId
                                                  Where c.CampusId.Equals(campusId) _
                                                     AndAlso stats.Contains(s.Status)
                                                  Select p Distinct
                                                  Order By p.TermDescrip)
            Return query.ToList


        Catch ex As Exception
            Throw
        End Try
    End Function
    
    Public Function GetAllTermsForCampuses(ByVal campuses As List(Of String), ByVal statusTypes As List(Of String)) As List(Of arTerm)
        Try
            Dim i As Integer = 0
            Dim stats(statusTypes.Count - 1) As String
            For Each item As String In statusTypes
                stats(i) = item
                i = i + 1
            Next
            Dim query As IQueryable(Of arTerm) = (From p In db.arTerms
                    Join cg In db.syCampGrps
                    On p.CampGrpId Equals cg.CampGrpId
                    Join cgc In db.syCmpGrpCmps
                    On cg.CampGrpId Equals cgc.CampGrpId
                    Join c In db.syCampus
                    On cgc.CampusId Equals c.CampusId
                    Join s In db.syStatus
                    On p.StatusId Equals s.StatusId
                    Where campuses.Contains(c.CampusId.ToString) _
                          AndAlso stats.Contains(s.Status)
                    Select p Distinct
                    Order By p.TermDescrip)
            Return query.ToList


        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Function USP_getTerms_byCampusDate(ByVal Campus As Guid, ByVal WeekEndDate As Date, ByVal ShowInactive As Boolean) As IEnumerable(Of USP_getTerms_byCampusDateResult)
        Try
            Dim query As IList(Of USP_getTerms_byCampusDateResult) = db.USP_getTerms_byCampusDate(Campus, WeekEndDate, ShowInactive).ToList()
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetTermByCmpGrpIdProgIdListTermIdList(ByVal campusId As Guid _
                                                                  , ByVal progIdList As List(Of Guid) _
                                                                  , ByVal termIdList As List(Of Guid) _
                                                                  , ByVal statusValuesList As List(Of String) _
                                                                  ) As List(Of arTerm)
        Try
            Dim query As IQueryable(Of arTerm) = (From t In db.arTerms
                                                  Join cg In db.syCampGrps
                                                     On t.CampGrpId Equals cg.CampGrpId
                                                  Join cgc In db.syCmpGrpCmps
                                                     On cg.CampGrpId Equals cgc.CampGrpId
                                                  Join c In db.syCampus
                                                     On cgc.CampusId Equals c.CampusId
                                                  Group Join prog In db.arPrograms
                                                     On t.ProgId Equals prog.ProgId
                                                      Into progTerms = Group
                                                  From prog In progTerms.DefaultIfEmpty
                                                  Join s In db.syStatus
                                                     On t.StatusId Equals s.StatusId
                                                  Where c.CampusId.Equals(campusId) _
                                                     AndAlso (progIdList.Count.Equals(0) OrElse (t.ProgId.Equals(Nothing) OrElse progIdList.Contains(prog.ProgId))) _
                                                     AndAlso Not termIdList.Contains(t.TermId) _
                                                     AndAlso statusValuesList.Contains(s.Status)
                                                  Select t Distinct
                                                  Order By t.TermDescrip)
            Return query.ToList


        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetTerms(ByVal filtervalues As MasterDictionary) As IQueryable(Of arTerm)
        Try
            Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(filtervalues)
            Dim query As IQueryable(Of arTerm) = (From t In db.arTerms _
                                                  Join cg In db.syCampGrps _
                                                  On t.CampGrpId Equals cg.CampGrpId _
                                                  Where t.StartDate <= Date.Now _
                                                  And CampGrpIds.Contains(t.CampGrpId.ToString)
                                                  Select t Distinct _
                                                  Order By t.StartDate Descending, t.TermDescrip)
            Dim basePredicate As Expression(Of Func(Of arTerm, Boolean)) = PredicateBuilder.True(Of arTerm)()
            basePredicate = basePredicate.And(Function(pv) True)
            If filtervalues.Contains("Term") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("Term"), DetailDictionary)
                If objPsFilter.Contains("CampGrpId") Then
                    Dim CampGrpIdValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CampGrpId"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of arTerm, Boolean)) = PredicateBuilder.False(Of arTerm)()
                    For Each di As DictionaryEntry In CampGrpIdValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CampGrpIdValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                    'Add the "All" CampGrpId - We shouldn't need to do this but we do...
                                    Dim InList As Boolean = False
                                    Dim AllCampusGroup As syCampGrp = GetAllCampusGroup()
                                    'Dim AllItem As Guid = New Guid("2AC97FC1-BB9B-450A-AA84-7C503C90A1EB")
                                    Dim AllItem As Guid = AllCampusGroup.CampGrpId
                                    For Each item As Guid In mylist
                                        If item = AllItem Then
                                            InList = True
                                        End If
                                    Next
                                    If InList = False Then
                                        mylist.Add(AllItem)
                                    End If
                                    For Each campgrpid As Guid In mylist
                                        Dim temp As String = campgrpid.ToString
                                        predicate = predicate.Or(Function(pv As arTerm) pv.CampGrpId.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CampGrpIdValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
                If objPsFilter.Contains("StartDate") Then
                    Dim CurrentlySelectedValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("StartDate"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of arTerm, Boolean)) = PredicateBuilder.True(Of arTerm)()
                    For Each di As DictionaryEntry In CurrentlySelectedValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CurrentlySelectedValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.GreaterThanOrEqualTo
                                For Each di2 As DictionaryEntry In FModDictionary
                                    Dim mylist As ParamValueList(Of Date) = CType(di2.Value, ParamValueList(Of Date))
                                    For Each startdate As Date In mylist
                                        Dim temp As Date = startdate
                                        predicate = predicate.And(Function(t As arTerm) t.StartDate >= temp)
                                    Next
                                Next
                            Case ModDictionary.Modifier.GreaterThan
                                For Each di2 As DictionaryEntry In FModDictionary
                                    Dim mylist As ParamValueList(Of Date) = CType(di2.Value, ParamValueList(Of Date))
                                    For Each startdate As Date In mylist
                                        Dim temp As Date = startdate
                                        predicate = predicate.And(Function(t As arTerm) t.StartDate > temp)
                                    Next
                                Next
                            Case ModDictionary.Modifier.LessThanOrEqualTo
                                For Each di2 As DictionaryEntry In FModDictionary
                                    Dim mylist As ParamValueList(Of Date) = CType(di2.Value, ParamValueList(Of Date))
                                    For Each startdate As Date In mylist
                                        Dim temp As Date = startdate
                                        predicate = predicate.And(Function(t As arTerm) t.StartDate <= temp)
                                    Next
                                Next
                            Case ModDictionary.Modifier.LessThan
                                For Each di2 As DictionaryEntry In FModDictionary
                                    Dim mylist As ParamValueList(Of Date) = CType(di2.Value, ParamValueList(Of Date))
                                    For Each startdate As Date In mylist
                                        Dim temp As Date = startdate
                                        predicate = predicate.And(Function(t As arTerm) t.StartDate < temp)
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CurrentlySelectedValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
                If objPsFilter.Contains("EndDate") Then
                    Dim CurrentlySelectedValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("EndDate"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of arTerm, Boolean)) = PredicateBuilder.True(Of arTerm)()
                    For Each di As DictionaryEntry In CurrentlySelectedValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CurrentlySelectedValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Dim temp1 As Date = DateTime.Now
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.GreaterThanOrEqualTo
                                For Each di2 As DictionaryEntry In FModDictionary
                                    Dim mylist As ParamValueList(Of Date) = CType(di2.Value, ParamValueList(Of Date))
                                    For Each startdate As Date In mylist
                                        Dim temp As Date = startdate
                                        predicate = predicate.And(Function(t As arTerm) t.EndDate >= temp)
                                    Next
                                Next
                            Case ModDictionary.Modifier.GreaterThan
                                For Each di2 As DictionaryEntry In FModDictionary
                                    Dim mylist As ParamValueList(Of Date) = CType(di2.Value, ParamValueList(Of Date))
                                    For Each startdate As Date In mylist
                                        Dim temp As Date = startdate
                                        predicate = predicate.And(Function(t As arTerm) t.EndDate > temp)
                                    Next
                                Next
                            Case ModDictionary.Modifier.LessThanOrEqualTo
                                For Each di2 As DictionaryEntry In FModDictionary
                                    Dim mylist As ParamValueList(Of Date) = CType(di2.Value, ParamValueList(Of Date))
                                    For Each startdate As Date In mylist
                                        Dim temp As Date = startdate
                                        predicate = predicate.And(Function(t As arTerm) t.EndDate <= temp)
                                    Next
                                Next
                            Case ModDictionary.Modifier.LessThan
                                For Each di2 As DictionaryEntry In FModDictionary
                                    Dim mylist As ParamValueList(Of Date) = CType(di2.Value, ParamValueList(Of Date))
                                    For Each startdate As Date In mylist
                                        Dim temp As Date = startdate
                                        predicate = predicate.And(Function(t As arTerm) t.EndDate < temp)
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CurrentlySelectedValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
                If objPsFilter.Contains("Status") Then
                    Dim StatusValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("Status"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of arTerm, Boolean)) = PredicateBuilder.False(Of arTerm)()
                    For Each di As DictionaryEntry In StatusValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(StatusValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))
                                    For Each status As String In mylist
                                        Dim temp As String = status
                                        predicate = predicate.Or(Function(pv As arTerm) pv.syStatus.Status.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & StatusValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
                If objPsFilter.Contains("CurrentlySelected") Then
                    Dim CurrentlySelectedValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CurrentlySelected"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of arTerm, Boolean)) = PredicateBuilder.True(Of arTerm)()
                    For Each di As DictionaryEntry In CurrentlySelectedValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CurrentlySelectedValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary
                                    Dim mylist As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))
                                    For Each selected As String In mylist
                                        Dim temp As String = selected
                                        predicate = predicate.And(Function(pv As arTerm) Not pv.TermId.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CurrentlySelectedValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
            End If

            Dim result As IQueryable(Of arTerm) = query.Where(basePredicate)


            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function GetAllCampusGroup() As syCampGrp
        Try
            Dim query As syCampGrp = (From cg In db.syCampGrps _
                                      Select cg Where cg.IsAllCampusGrp.Equals(True)).SingleOrDefault
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetTermsForStudentEnrollments_WithTerms(ByVal stuenrollid As Guid, ByVal campusid As Guid, ByVal startdate As Date) As IEnumerable(Of USP_SA_GetStudentTerms_WithTermsResult)
        Try
            Dim query As IEnumerable(Of USP_SA_GetStudentTerms_WithTermsResult) = (From t In db.USP_SA_GetStudentTerms_WithTerms(stuenrollid, campusid, startdate))

            Dim result As IEnumerable(Of USP_SA_GetStudentTerms_WithTermsResult) = query

            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetTermsForStudentEnrollments_WithoutTerms(ByVal stuenrollid As Guid, ByVal studentid As Guid, ByVal campusid As Guid, ByVal startdate As Date, ByVal IsSuper As Boolean) As IEnumerable(Of USP_SA_GetStudentTerms_WithoutTermsResult)



        Try

            Dim query As IEnumerable(Of USP_SA_GetStudentTerms_WithoutTermsResult) = (From t In db.USP_SA_GetStudentTerms_WithoutTerms(stuenrollid, studentid, campusid, startdate, IsSuper))

            Dim result As IEnumerable(Of USP_SA_GetStudentTerms_WithoutTermsResult) = query

            Return result
        Catch ex As Exception
            Throw ex
        End Try


    End Function

End Class
