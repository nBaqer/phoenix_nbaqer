﻿'Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Public Class ClassSectionDa
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function USP_getClasses_byCampusDate_andTermCourseInstructorShift(ByVal Campus As Guid, ByVal WeekEndDate As Date,
                                                                 ByVal TermId As String, ByVal CourseId As String,
                                                                 ByVal InstructorID As String, ByVal ShiftId As String) As IEnumerable(Of USP_getClasses_byCampusDate_andTermCourseInstructorShiftResult)
        Try
            Dim query As IList(Of USP_getClasses_byCampusDate_andTermCourseInstructorShiftResult) = db.USP_getClasses_byCampusDate_andTermCourseInstructorShift(Campus, WeekEndDate, TermId, CourseId, InstructorID, ShiftId).ToList()
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
