﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities

Public Class PaymentPeriodsDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub

    Public Function GetPaymentPeriods(ByVal studentid As Guid, ByVal stuenrollid As Guid) As IEnumerable(Of USP_AR_StuEnrollPayPeriodsResult)
        Try
            'Dim users As String() = CreateUserIdList(filtervalues)
            Dim query As IEnumerable(Of USP_AR_StuEnrollPayPeriodsResult) = (From pp In db.USP_AR_StuEnrollPayPeriods(stuenrollid))

            Dim result As IEnumerable(Of USP_AR_StuEnrollPayPeriodsResult) = query
            'Return Nothing
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
