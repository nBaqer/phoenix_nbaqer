﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Imports LinqKit
Imports System.Linq.Expressions
Imports FAME.Parameters.Collections

Public Class StudentAwardDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function GetFinancialAidId(studentAwardId As Guid) As String
        Try
            Dim query As IQueryable(Of String) = (From sa In db.faStudentAwards
                                                  Where sa.StudentAwardId = studentAwardId
                                                  Select sa.FA_Id)

            Dim result = query.FirstOrDefault()
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Disbursement number in Advantage is not the same as the Disbursement number received from AFA
    'Disbursement number in advantage = total number of disbursements for each award
    'Disburment number from AFA will be used as an identifier and will be stored in reference column
    Public Function GetAFADisbursementNumber(disbursementId As Guid) As Integer
        Try
            Dim query As IQueryable(Of Nullable(Of Byte)) = (From sa In db.faStudentAwardSchedules
                                                             Where sa.AwardScheduleId = disbursementId
                                                             Select sa.SequenceNumber)

            Dim result = CType(query.FirstOrDefault(), Integer)
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
