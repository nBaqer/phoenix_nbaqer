﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities

Public Class ProgramDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function GetProgramsByCampusId(ByVal CampusId As Guid _
                                          , ByVal StatusTypes As List(Of String) _
                                          , Optional ByVal FilteredByGE As Boolean = False
                                          ) As List(Of arProgram)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            Dim query As IQueryable(Of arProgram) = (From p In db.arPrograms
                                                     Join cg In db.syCampGrps
                                                     On p.CampGrpId Equals cg.CampGrpId
                                                     Join cgc In db.syCmpGrpCmps
                                                     On cg.CampGrpId Equals cgc.CampGrpId
                                                     Join c In db.syCampus
                                                     On cgc.CampusId Equals c.CampusId
                                                     Join s In db.syStatus
                                                     On p.StatusId Equals s.StatusId
                                                     Where c.CampusId.Equals(CampusId) _
                                                         AndAlso stats.Contains(s.Status) _
                                                         AndAlso ((FilteredByGE = True And p.IsGEProgram = True) _
                                                                  Or (FilteredByGE = False))
                                                     Select p Distinct
                                                     Order By p.ProgDescrip)
            Return query.ToList
            'AndAlso FilteredByGE Equal vbTrue And p.IsGEProgram Equals True Or FilteredByGE Equal vbTrue _
            'AndAlso ( (FilteredByGE = True And p.IsGEProgram Equals True) _
            '                                                       Or (FilteredByGe = False))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetProgramsByCampusId(ByVal CampusId As Guid _
                                          , ByVal StatusTypes As List(Of String) _
                                          , ByVal SelectedPrograms As List(Of String) _
                                          , Optional ByVal FilteredByGE As Boolean = False
                                          ) As List(Of arProgram)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            'reset count
            i = 0
            Dim selected(SelectedPrograms.Count - 1) As String
            For Each item As String In SelectedPrograms
                selected(i) = item
                i = i + 1
            Next

            Dim query As IQueryable(Of arProgram) = (From p In db.arPrograms
                                                     Join cg In db.syCampGrps
                                                     On p.CampGrpId Equals cg.CampGrpId
                                                     Join cgc In db.syCmpGrpCmps
                                                     On cg.CampGrpId Equals cgc.CampGrpId
                                                     Join c In db.syCampus
                                                     On cgc.CampusId Equals c.CampusId
                                                     Join s In db.syStatus
                                                     On p.StatusId Equals s.StatusId
                                                     Where c.CampusId.Equals(CampusId) _
                                                         AndAlso stats.Contains(s.Status) _
                                                         AndAlso Not selected.Contains(p.ProgId.ToString) _
                                                         AndAlso ((FilteredByGE = True And p.IsGEProgram = True) _
                                                                  Or (FilteredByGE = False))
                                                     Select p Distinct
                                                     Order By p.ProgDescrip)
            Return query.ToList

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetProgramsByCampusGroupsId(ByVal campusGroups As List(Of String), ByVal StatusTypes As List(Of String), Optional ByVal SelectedPrograms As List(Of String) = Nothing) As List(Of arProgram)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            Dim query As IQueryable(Of arProgram) = From p In db.arPrograms
                                                     Join cg In db.syCampGrps On p.CampGrpId Equals cg.CampGrpId
                                                     Join cgc In db.syCmpGrpCmps On cg.CampGrpId Equals cgc.CampGrpId
                                                     Join c In db.syCampus On cgc.CampusId Equals c.CampusId
                                                     Join s In db.syStatus On p.StatusId Equals s.StatusId
                                                     Where (campusGroups.Contains(p.CampGrpId.ToString) OrElse p.syCampGrp.IsAllCampusGrp) _
                                                           AndAlso stats.Contains(s.Status) _
                                                           AndAlso (SelectedPrograms Is Nothing OrElse (SelectedPrograms IsNot Nothing AndAlso Not SelectedPrograms.Contains(p.ProgId.ToString)))
                                                     Select p Distinct
                                                     
                                                    
            Return query.ToList


        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
