﻿Imports FAME.Advantage.Common.LINQ.Entities

Public Class TranscodeDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub

    Public Function GetTranCodesByCampusId(ByVal UserId As Guid, ByVal CampusID As Guid, ByVal StatusTypes As List(Of String)) As List(Of saTransCode)

        Dim query As IQueryable(Of saTransCode)

        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next

            Dim CampGrpIds As String() = CreateCampGrpIdListByCampusId(CampusID)
            ReDim Preserve CampGrpIds(CampGrpIds.Length - 1)



            query = (From tc In db.saTransCodes _
                                                   Join s In db.syStatus _
                                                    On tc.StatusId Equals s.StatusId _
                                                      Where CampGrpIds.Contains(tc.CampGrpId.ToString) _
                                                    AndAlso stats.Contains(s.Status) _
                                                    Select tc Distinct _
                                                    Order By tc.TransCodeDescrip)




            Return query.ToList
        Catch ex As Exception
            Throw ex
        End Try



    End Function

    Public Function GetTranCodesByCampusId(ByVal UserId As Guid, ByVal CampusID As Guid, ByVal StatusTypes As List(Of String), ByVal SelectedTranCodes As List(Of String)) As List(Of saTransCode)
        Dim query As IQueryable(Of saTransCode)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            'reset count
            i = 0
            Dim selected(SelectedTranCodes.Count - 1) As String
            For Each item As String In SelectedTranCodes
                selected(i) = item
                i = i + 1
            Next

            Dim CampGrpIds As String() = CreateCampGrpIdListByCampusId(CampusID)
            ReDim Preserve CampGrpIds(CampGrpIds.Length - 1)



            query = (From tc In db.saTransCodes _
                                                   Join s In db.syStatus _
                                                    On tc.StatusId Equals s.StatusId _
                                                      Where CampGrpIds.Contains(tc.CampGrpId.ToString) _
                                                    AndAlso stats.Contains(s.Status) _
                                                     AndAlso Not selected.Contains(tc.TransCodeId.ToString) _
                                                    Select tc Distinct _
                                                    Order By tc.TransCodeDescrip)





            Return query.ToList
        Catch ex As Exception
            Throw ex
        End Try
    End Function
  
    Private Function CreateCampGrpIdListByCampusId(ByVal campusid As Guid) As String()
        Try
            Dim i As Integer = 0
            Dim query As IQueryable(Of syCmpGrpCmp) = (From cgc In db.syCmpGrpCmps Where cgc.CampusId.Equals(campusid))
            Dim results As List(Of syCmpGrpCmp) = query.ToList

          
                Dim CampGrpIds(results.Count - 1) As String
                'restart  i = 0
            For Each item As syCmpGrpCmp In results
                CampGrpIds(i) = item.CampGrpId.ToString
                i = i + 1
            Next
                Return CampGrpIds


        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
  
End Class
