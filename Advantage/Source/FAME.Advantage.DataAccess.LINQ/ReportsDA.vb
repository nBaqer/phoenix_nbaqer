﻿Imports FAME.Advantage.Common.LINQ.Entities

Public Class ReportsDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    public Function GetRDL(ByVal resourseId As Integer) As String
        Try
            Dim rdl =
                    (From r In db.syReports
                    Where r.ResourceId = resourseId
                    Select r.RDLName).FirstOrDefault()
            Return rdl
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetNACCASReports() As List(Of syReport)
        Try
            Dim resourceIds As Integer() = New Integer() {850, 852, 849, 851}
            Dim reports =
                    (From r In db.syReports
                     Where resourceIds.Contains( r.ResourceId)
                     Select r).ToList()
            Return reports
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
