﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities

Public Class LeadDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function GetLeads() As IQueryable(Of adLead)
        Try

            Dim query As IQueryable(Of adLead) = (From l In db.adLeads
                                                  Where Not db.arStuEnrollments.Any(Function(se) se.LeadId.ToString = l.LeadId.ToString)
                                                  Order By l.LastName, l.FirstName
                                                  Select l)

            Dim result As IQueryable(Of adLead) = query
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetLeadStudentNumber(ByVal stuEnrollId As String, ByVal isStudentID As String) As String

        Try
            Dim studentIdentifier As String = (From l In db.adLeads
                                               Join se In db.arStuEnrollments On se.LeadId Equals l.LeadId
                                               Where se.StuEnrollId.ToString() = stuEnrollId
                                               Select If(isStudentID = "studentid", l.StudentNumber, If(isStudentID = "ssn", "XX" + l.SSN.Substring(l.SSN.Length - 4), se.EnrollmentId))).FirstOrDefault()


            Return studentIdentifier
        Catch ex As Exception
            Return String.Empty
        End Try

    End Function

    Public Function GetLeadById(ByVal leadid As Guid) As adLead
        Try

            Dim query As IQueryable(Of adLead) = (From l In db.adLeads
                                                  Where l.LeadId = leadid
                                                  Select l)

            Dim result As adLead = query.FirstOrDefault()
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetAfaStudentId(studentId As Guid) As Long?
        Try

            Dim query As IQueryable(Of Nullable(Of Long)) = (From l In db.adLeads
                                                             Where l.StudentId = studentId
                                                             Select l.AfaStudentId)
            Dim result = query.FirstOrDefault()
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetAfaStudentIdFromEnrollment(studentEnrollmentId As Guid) As Long?
        Try
            Dim query As IQueryable(Of Nullable(Of Long)) = (From l In db.adLeads
                                                             Join se In db.arStuEnrollments On se.LeadId Equals l.LeadId
                                                             Where se.StuEnrollId = studentEnrollmentId
                                                             Select l.AfaStudentId)
            Dim result = query.FirstOrDefault()
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetSSN(studentEnrollmentId As Guid) As String
        Try
            Dim query As IQueryable(Of String) = (From l In db.adLeads
                                                  Join se In db.arStuEnrollments On se.LeadId Equals l.LeadId
                                                  Where se.StuEnrollId = studentEnrollmentId
                                                  Select l.SSN)
            Dim result = query.FirstOrDefault()
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
