﻿
Imports FAME.Advantage.Common.LINQ.Entities
'Imports FAME.Advantage.Reporting.Info
Public Class TranscriptReportDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function GetTranscriptReportRecordCount(ByVal strStuEnrollIdList As String, _
                                                   ByVal boolShowMultipleEnrollments As Boolean, _
                                                   ByVal strStatusCodeIdList As String) As Integer
        Try

            Dim stuEnrollIdList As String() = strStuEnrollIdList.Split(New [Char]() {","c})
            Dim StatusCodeIdList As String() = strStatusCodeIdList.Split(New [Char]() {","c})
            Dim AllCG As syCampGrp = GetAllCampusGroup()
            Dim strAllCampGrpId As String = AllCG.CampGrpId.ToString
            Dim ShowMultipleEnrollments As Boolean = boolShowMultipleEnrollments

            Dim q = (From SE In db.arStuEnrollments _
                     Join SC In db.syStatusCodes On SC.StatusCodeId Equals SE.StatusCodeId _
                     Where (String.IsNullOrEmpty(strStuEnrollIdList) Or stuEnrollIdList.Contains(SE.StuEnrollId.ToString())) _
                       And (ShowMultipleEnrollments = False _
                            Or (ShowMultipleEnrollments = True And String.IsNullOrEmpty(strStatusCodeIdList)) _
                            Or (ShowMultipleEnrollments = True And StatusCodeIdList.Contains(SC.StatusCodeId.ToString()))
                           ) _
                      Select SE.StuEnrollId Distinct
                    ).Count

            Return q
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function GetAllCampusGroup() As syCampGrp
        Try
            Dim query As syCampGrp = (From cg In db.syCampGrps _
                                      Select cg Where cg.IsAllCampusGrp.Equals(True)).SingleOrDefault
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'Public Function GetTermStartDate(ByVal TermId As String) As Date
    '    Dim dtStartDate As Date = Nothing
    '    Try
    '        If Not TermId.ToString.Trim = "" Then
    '            Dim query As IQueryable(Of arTerm) = (From T In db.arTerms Where T.TermId.ToString.Equals(TermId))
    '            Dim results As List(Of arTerm) = query.ToList
    '            For Each item As arTerm In results
    '                'dtStartDate = item.StartDate.ToString
    '                If item.EndDate.ToString Is System.DBNull.Value Then
    '                    dtStartDate = item.StartDate.ToString
    '                Else
    '                    dtStartDate = item.EndDate.ToString
    '                End If
    '                Exit For
    '            Next
    '        End If
    '        Return dtStartDate
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    'Public Function GetProgressReportRecordCount(ByVal getSelectedFilters As ReportSelectionValues) As Integer
    '    Try

    '        Dim strCampGrpId As String = ""
    '        Dim strPrgVerId As String = ""
    '        Dim strTermId As String = ""
    '        Dim strStatusCodeId As String = ""
    '        Dim strStuEnrollId As String = ""
    '        Dim strStudentGrpId As String = ""
    '        Dim dtTermDate As Date = Nothing

    '        With getSelectedFilters
    '            strStuEnrollId = .StuEnrollId
    '            strCampGrpId = .CampGrpId
    '            strPrgVerId = .PrgVerId
    '            strStatusCodeId = .StatusCodeId
    '            strStuEnrollId = .StuEnrollId
    '            strTermId = .TermId
    '            strStudentGrpId = .StudentGrpId
    '        End With


    '        Dim studentEnrollments As String() = strStuEnrollId.Split(New [Char]() {","c})
    '        Dim CampusGroups As String() = strCampGrpId.Split(New [Char]() {","c})
    '        Dim ProgramVersions As String() = strPrgVerId.Split(New [Char]() {","c})
    '        Dim StatusCodes As String() = strStatusCodeId.Split(New [Char]() {","c})
    '        Dim Terms As String() = strTermId.Split(New [Char]() {","c})
    '        Dim StudentGroups As String() = strStudentGrpId.Split(New [Char]() {","c})

    '        Dim q = (From SE In db.arStuEnrollments _
    '                                        Join PV In db.arPrgVersions On SE.PrgVerId Equals PV.PrgVerId _
    '                                        Join CGC In db.syCmpGrpCmps On SE.CampusId Equals CGC.CampusId _
    '                                        Join SC In db.syStatusCodes On SC.StatusCodeId Equals SE.StatusCodeId _
    '                                        Join R In db.arResults On SE.StuEnrollId Equals R.StuEnrollId _
    '                                        Join CS In db.arClassSections On R.TestId Equals CS.ClsSectionId _
    '                                        Join T In db.arTerms On T.TermId Equals CS.TermId _
    '                                        Group Join LLG In db.adLeadByLeadGroups On SE.StuEnrollId Equals LLG.StuEnrollId Into StoreResults = Group _
    '                                        From LLG In StoreResults.DefaultIfEmpty _
    '                        Where _
    '                                        (String.IsNullOrEmpty(strStuEnrollId) Or studentEnrollments.Contains(SE.StuEnrollId.ToString)) _
    '                                                And _
    '                                        (String.IsNullOrEmpty(strCampGrpId) Or CampusGroups.Contains(CGC.CampGrpId.ToString)) _
    '                                                And _
    '                                        (String.IsNullOrEmpty(strStatusCodeId) Or StatusCodes.Contains(SC.StatusCodeId.ToString)) _
    '                                                And _
    '                                        (String.IsNullOrEmpty(strTermId) Or Terms.Contains(T.TermId.ToString)) _
    '                                                And _
    '                                        (String.IsNullOrEmpty(strStudentGrpId) Or StudentGroups.Contains(LLG.LeadGrpId.ToString)) _
    '                           Select SE.StuEnrollId Distinct).Count
    '        Return q
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
End Class
