﻿Imports System.Data.SqlClient
Imports FAME.Advantage.Common

Public Class CreditSummaryDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub

    Public Function GetOverallGPA(ByVal enrollmentId As String) As Double

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connString As String = myAdvAppSettings.AppSettings("ConnectionString")
        Dim conn As New SqlConnection(connString)

        Try
            Dim cmd1 As SqlCommand = New SqlCommand("USP_GPACalculator", conn)
            cmd1.CommandType = CommandType.StoredProcedure
            cmd1.Parameters.Add("@EnrollmentId", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = enrollmentId
            cmd1.Parameters.Add(New SqlParameter("@StudentGPA", SqlDbType.Decimal) With {.Precision = 10, .Scale = 2})
            cmd1.Parameters("@StudentGPA").Direction = ParameterDirection.Output
            conn.Open()
            cmd1.ExecuteNonQuery()
            Return Convert.ToDouble(cmd1.Parameters("@StudentGPA").Value)

        Catch ex As Exception
            Throw ex
        Finally
            conn.Close()
        End Try
    End Function
End Class
