﻿Public NotInheritable Class EnumsDA
    Public Enum ProgramRegistrationType
        ByClass = 0
        ByProgram = 1
    End Enum

    ''' <summary>
    ''' Enums for Academic Calendars
    ''' </summary>
    Public Enum AdvantageSystemAcademicCalendars
        NonStandardTerm = 1
        Quarter = 2
        Semester = 3
        Trimester = 4
        ClockHour = 5
        NonTerm = 6
    End Enum

    Private Sub New()
    End Sub
End Class
