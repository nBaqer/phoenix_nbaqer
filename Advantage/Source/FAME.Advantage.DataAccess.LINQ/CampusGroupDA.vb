﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Parameters.Collections
Imports LinqKit

Public Class CampusGroupDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function GetAllCampusGroup() As syCampGrp
        Try
            Dim query As syCampGrp = (From cg In db.syCampGrps _
                                      Select cg Where cg.IsAllCampusGrp.Equals(True)).SingleOrDefault
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function GetUserName(ByVal userid As Guid) As String
        Try
            Dim query As syUser = (From u In db.syUsers _
                                   Select u Where u.UserId.Equals(userid)).SingleOrDefault
            Return query.UserName
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetCampusGroups(ByVal filtervalues As MasterDictionary) As IQueryable(Of syCampGrp)
        Try
            Dim query As IQueryable(Of syCampGrp) = (From cg In db.syCampGrps _
                                                     Join st In db.syStatus _
                                                     On cg.StatusId Equals st.StatusId _
                                                     Select cg Distinct _
                                                     Order By cg.IsAllCampusGrp Descending, cg.CampGrpDescrip)
            Dim UserCollection As New ParamValueList(Of Guid)
            Dim StatusCollection As New ParamValueList(Of String)
            Dim CurrentlySelectedCollection As New ParamValueList(Of String)
            If filtervalues.Contains("CampusGroup") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("CampusGroup"), DetailDictionary)
                If objPsFilter.Contains("UserId") Then
                    Dim UserValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("UserId"), ParamValueDictionary)
                    For Each di As DictionaryEntry In UserValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(UserValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)

                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                UserCollection = DirectCast(FModDictionary.Item("UserId"), ParamValueList(Of Guid))
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & UserValueDictionary.Name)
                        End Select
                    Next
                Else
                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                    Throw New Exception(currentmethod & " has UserId as a required filter.")
                End If
                If objPsFilter.Contains("Status") Then
                    Dim StatusValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("Status"), ParamValueDictionary)
                    For Each di As DictionaryEntry In StatusValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(StatusValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                StatusCollection = DirectCast(FModDictionary.Item("Status"), ParamValueList(Of String))
                                Dim users(UserCollection.Count - 1) As String
                                Dim i As Integer = 0
                                Dim IsTheAdminUser As Boolean = False
                                For Each user As Guid In UserCollection
                                    Dim username As String = GetUserName(user)
                                    If username.ToLower = "sa" Or username.ToLower = "super" Or username.ToLower = "support" Then
                                        IsTheAdminUser = True
                                    End If
                                    users(i) = user.ToString
                                    i = i + 1
                                Next
                                i = 0
                                Dim stats(StatusCollection.Count - 1) As String
                                For Each stat As String In StatusCollection
                                    stats(i) = stat
                                    i = i + 1
                                Next
                                If IsTheAdminUser = False Then
                                    query = (From cg In db.syCampGrps _
                                             Join s In db.syStatus _
                                             On cg.StatusId Equals s.StatusId _
                                             Join urcg In db.syUsersRolesCampGrps _
                                             On cg.CampGrpId Equals urcg.CampGrpId _
                                             Where users.Contains(urcg.UserId.ToString) AndAlso stats.Contains(s.Status) _
                                             Select cg Distinct _
                                             Order By cg.IsAllCampusGrp Descending, cg.CampGrpDescrip)
                                Else
                                    query = (From cg In db.syCampGrps _
                                             Join s In db.syStatus _
                                             On cg.StatusId Equals s.StatusId _
                                             Where stats.Contains(s.Status) _
                                             Select cg Distinct _
                                             Order By cg.IsAllCampusGrp Descending, cg.CampGrpDescrip)
                                End If

                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & StatusValueDictionary.Name)
                        End Select
                    Next
                Else
                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                    Throw New Exception(currentmethod & " has Status as a required filter.")
                End If
                'For Each di As DictionaryEntry In objPsFilter
                '    Dim x As String = di.Key.ToString
                'Next
                If objPsFilter.Contains("CurrentlySelected") Then
                    Dim CurrentlySelectedDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CurrentlySelected"), ParamValueDictionary)
                    For Each di As DictionaryEntry In CurrentlySelectedDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CurrentlySelectedDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                CurrentlySelectedCollection = DirectCast(FModDictionary.Item("CurrentlySelected"), ParamValueList(Of String))
                                Dim users(UserCollection.Count - 1) As String
                                Dim i As Integer = 0
                                Dim IsTheAdminUser As Boolean = False
                                For Each user As Guid In UserCollection
                                    Dim username As String = GetUserName(user)
                                    If username.ToLower = "sa" Or username.ToLower = "super" Or username.ToLower = "support" Then
                                        IsTheAdminUser = True
                                    End If
                                    users(i) = user.ToString
                                    i = i + 1
                                Next
                                i = 0
                                Dim stats(StatusCollection.Count - 1) As String
                                For Each stat As String In StatusCollection
                                    stats(i) = stat
                                    i = i + 1
                                Next
                                i = 0
                                Dim selected(CurrentlySelectedCollection.Count - 1) As String
                                For Each item As String In CurrentlySelectedCollection
                                    selected(i) = item
                                    i = i + 1
                                Next
                                If IsTheAdminUser = False Then
                                    query = (From cg In db.syCampGrps _
                                             Join s In db.syStatus _
                                             On cg.StatusId Equals s.StatusId _
                                             Join urcg In db.syUsersRolesCampGrps _
                                             On cg.CampGrpId Equals urcg.CampGrpId _
                                             Where users.Contains(urcg.UserId.ToString) AndAlso stats.Contains(s.Status) _
                                             AndAlso Not selected.Contains(cg.CampGrpId.ToString) _
                                             Select cg Distinct _
                                             Order By cg.IsAllCampusGrp Descending, cg.CampGrpDescrip)
                                Else
                                    query = (From cg In db.syCampGrps _
                                             Join s In db.syStatus _
                                             On cg.StatusId Equals s.StatusId _
                                             Where stats.Contains(s.Status) _
                                             AndAlso Not selected.Contains(cg.CampGrpId.ToString) _
                                             Select cg Distinct _
                                             Order By cg.IsAllCampusGrp Descending, cg.CampGrpDescrip)
                                End If

                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CurrentlySelectedDictionary.Name)
                        End Select
                    Next
                End If
            Else
                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                Throw New Exception(currentmethod & " is missing on of its required filters")
            End If

            Dim result As IQueryable(Of syCampGrp) = query

            Return result
        Catch ex As Exception
            Throw ex
        End Try

    End Function


    Public Function GetCampusGrpsByUserId(ByVal UserId As Guid, ByVal StatusTypes As List(Of String)) As List(Of syCampGrp)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next

            Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(UserId)
            Dim query As IQueryable(Of syCampGrp) = (From c In db.syCampGrps _
                                                       Join s In db.syStatus _
                                                         On c.StatusId Equals s.StatusId _
                                                        Where CampGrpIds.Contains(c.CampGrpId.ToString) _
                                                           AndAlso stats.Contains(s.Status) _
                                                            Select c Distinct _
                                                            Order By c.CampGrpDescrip)
            Return query.ToList

        Catch ex As Exception
            Throw ex
        End Try


    End Function


    Public Function GetCampusGrpsByUserId(ByVal UserId As Guid, ByVal StatusTypes As List(Of String), ByVal SelectedCmpGrps As List(Of String)) As List(Of syCampGrp)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            i = 0
            Dim selected(SelectedCmpGrps.Count - 1) As String
            For Each item As String In SelectedCmpGrps
                selected(i) = item
                i = i + 1
            Next
            Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(UserId)
            Dim query As IQueryable(Of syCampGrp) = (From c In db.syCampGrps _
                                                       Join s In db.syStatus _
                                                         On c.StatusId Equals s.StatusId _
                                                        Where CampGrpIds.Contains(c.CampGrpId.ToString) _
                                                           AndAlso stats.Contains(s.Status) _
                                                                 AndAlso Not selected.Contains(c.CampGrpId.ToString) _
                                                            Select c Distinct _
                                                            Order By c.CampGrpDescrip)
            Return query.ToList

        Catch ex As Exception
            Throw ex
        End Try


    End Function

    Private Function CreateCampGrpIdListByUserId(ByVal userid As Guid) As String()
        Try
            Dim i As Integer = 0
            Dim query As IQueryable(Of syUsersRolesCampGrp) = (From urcg In db.syUsersRolesCampGrps Where urcg.UserId.Equals(userid))
            Dim hasAllCampusGroups = query.Any(function(grp as syUsersRolesCampGrp) grp.syCampGrp.IsAllCampusGrp.HasValue  AndAlso grp.syCampGrp.IsAllCampusGrp.Value = true)
            Dim results As List(Of syUsersRolesCampGrp) = query.ToList
            Dim userName As String = GetUserName(userid)

            If userName.ToLower = "sa" Or userName.ToLower = "super" Or userName.ToLower = "support" Or hasAllCampusGroups Then
                Dim SuperUserQuery As IQueryable(Of syCampGrp) = (From cg In db.syCampGrps)
                Dim SuperUserCampGrpIds(SuperUserQuery.Count) As String
                i = 0
                For Each item As syCampGrp In SuperUserQuery
                    SuperUserCampGrpIds(i) = item.CampGrpId.ToString
                    i = i + 1
                Next
                Return SuperUserCampGrpIds
            Else

                Dim CampGrpIds(results.Count - 1) As String
                'restart counter
                i = 0
                For Each item As syUsersRolesCampGrp In results
                    CampGrpIds(i) = item.CampGrpId.ToString
                    i = i + 1
                Next
                Return CampGrpIds
            End If

        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
End Class
