﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities

Public Class ResourceDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub

    Public Function GetResourceByResourceID(ByVal resourceID As Integer) As syResource
            Dim query As syResource = (From r In db.syResources _
                                        Select r Where r.resourceID.Equals(ResourceID)).SingleOrDefault
            Return query
    End Function
End Class
