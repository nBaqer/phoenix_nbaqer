﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities

Public Class AcademicYearsDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub

    Public Function GetAcademicYears() As IQueryable(Of saAcademicYear)
        Try
            Dim query As IQueryable(Of saAcademicYear) =
                (From y In db.saAcademicYears
                 Select y)
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function

     Public Function GetAcademicYearsOrdered() As List(Of saAcademicYear)
        Try
            Dim query As IQueryable(Of saAcademicYear) = GetAcademicYears()
            Dim academicYearCodeParsed As Integer
            Return query.OrderByDescending(Function(year) year.AcademicYearCode).ToList()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetYearsByRange(currentYear As Integer, minus As Integer, plus As Integer) As List(Of saAcademicYear)
        Try
            Dim max = currentYear + plus
            Dim min = currentYear - minus
            Dim query As IQueryable(Of saAcademicYear) =
                    (From y In db.saAcademicYears
                     Where Convert.ToInt32(y.AcademicYearCode) >= min AndAlso Convert.ToInt32(y.AcademicYearCode) <= max AndAlso y.syStatus.Status.ToString() = "active"
                     Select y)
            Return query.OrderByDescending(Function(year) Convert.ToInt32(year.AcademicYearCode)).ToList()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class





