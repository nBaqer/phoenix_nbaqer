﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Imports LinqKit
Imports System.Linq.Expressions
Imports FAME.Parameters.Collections
Public Class AppSettingsDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function GetAppSettingValueByCampus(ByVal keyName As String, ByVal CampusIds As IEnumerable(Of Guid)) As List(Of CampusValuePair)
        Try
            Dim i As Integer = 0
            Dim query = (From appsett In db.syConfigAppSettings Join appsettval In db.syConfigAppSetValues On appsett.SettingId Equals appsettval.SettingId
                         Where appsett.KeyName = keyName)

            Dim allCampusVal = query.Where(Function(q) q.appsettval.Active AndAlso Not( q.appsettval.CampusId.HasValue)).Select(Function(x) x.appsettval.Value).FirstOrDefault()

            Dim campusSpecificVals = query.Where(Function(q) q.appsettval.Active AndAlso q.appsettval.CampusId.HasValue AndAlso CampusIds.Contains(q.appsettval.CampusId.Value)).Select(Function(x) New CampusValuePair With {.CampusId = x.appsettval.CampusId.Value, .Value = x.appsettval.Value}).ToList()

            Dim noCampusSpecific = CampusIds.Where(Function(x) Not(campusSpecificVals.Select(Function(csv) csv.CampusId).Contains(x))).Select(Function(x) New CampusValuePair With {.CampusId = x, .Value = allCampusVal}).ToList()

            campusSpecificVals.AddRange(noCampusSpecific)

            Return campusSpecificVals
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
End Class
