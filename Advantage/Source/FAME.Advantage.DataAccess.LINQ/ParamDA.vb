﻿
Imports FAME.Parameters.Info

Public Class ParamDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function GetParamSetBySetName(ByVal SetName As String) As ParameterSetInfo
        Try
            Dim q = (From ParamSet In db.ParamSets _
                     Where ParamSet.SetName.Equals(SetName) _
                    Select New ParameterSetInfo With {.SetId = ParamSet.SetId, _
                    .SetName = ParamSet.SetName, _
                    .SetDisplayName = ParamSet.SetDisplayName, _
                    .SetType = ParamSet.SetType, _
                    .SetDescription = ParamSet.SetDescription, _
                    .ParameterSectionCollection = _
                    (From ParamSection In db.ParamSections _
                    Where ParamSet.SetId = ParamSection.SetId _
                    Select New ParameterSectionInfo With {.SectionId = ParamSection.SectionId, _
                    .SectionName = ParamSection.SectionName, _
                    .SectionDescription = ParamSection.SectionDescription, _
                    .SectionCaption = ParamSection.SectionCaption, _
                    .SectionSeq = ParamSection.SectionSeq, _
                    .ParameterDetailItemCollection = (From ParamDetail In db.ParamDetails _
                    From ParamItem In db.ParamItems _
                    Where ParamSection.SectionId = ParamDetail.SectionId _
                    And ParamDetail.ItemId = ParamItem.ItemId _
                    Order By ParamDetail.ItemSeq _
                    Select New ParameterDetailItemInfo With {.ItemId = ParamDetail.ItemId, _
                    .ItemName = ParamItem.Caption, _
                    .ReturnValueName = ParamItem.ReturnValueName, _
                    .ControllerClass = ParamItem.ControllerClass, _
                    .DetailId = ParamDetail.DetailId, _
                    .SetId = ParamSet.SetId, _
                    .SetName = ParamSet.SetName, _
                    .SectionType = ParamSection.SectionType, _
                    .PageId = ParamSection.SectionId, _
                    .PageName = ParamSection.SectionName, _
                    .CaptionOverride = ParamDetail.CaptionOverride, _
                    .ValueProp = ParamItem.valueprop, _
                    .ItemSeq = ParamDetail.ItemSeq, _
                    .ParameterDetailPropertyCollection = ( _
                    From ParamDetailProp In db.ParamDetailProps _
                    Where ParamDetail.DetailId = ParamDetailProp.detailid _
                    Select New ParameterDetailPropertyInfo With { _
                    .DetailPropertyId = ParamDetailProp.detailpropertyid, _
                    .DetailId = ParamDetailProp.detailid, _
                    .ChildControl = ParamDetailProp.childcontrol, _
                    .PropName = ParamDetailProp.propname, _
                    .Value = ParamDetailProp.value, _
                    .ValueType = ParamDetailProp.valuetype}), _
                    .ParameterItemPropertyCollection = ( _
                    From ParamItemProp In db.ParamItemProps _
                    Where ParamItem.ItemId = ParamItemProp.itemid _
                    Select New ParameterItemPropertyInfo With {.ItemPropertyId = ParamItemProp.itempropertyid, _
                    .ItemId = ParamItemProp.itemid, _
                    .ChildControl = ParamItemProp.childcontrol, _
                    .PropName = ParamItemProp.propname, _
                    .Value = ParamItemProp.value, _
                    .ValueType = ParamItemProp.valuetype})})})}).Single

            Return q
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetParamSetBySetId(ByVal SetId As Integer) As ParameterSetInfo
        Try
            Dim q = (From ParamSet In db.ParamSets _
                     Where ParamSet.SetId.Equals(SetId) _
                    Select New ParameterSetInfo With {.SetId = ParamSet.SetId, _
                    .SetName = ParamSet.SetName, _
                    .SetDisplayName = ParamSet.SetDisplayName, _
                    .SetType = ParamSet.SetType, _
                    .SetDescription = ParamSet.SetDescription, _
                    .ParameterSectionCollection = _
                    (From ParamSection In db.ParamSections _
                    Where ParamSet.SetId = ParamSection.SetId Order By ParamSection.SectionSeq _
                    Select New ParameterSectionInfo With {.SectionId = ParamSection.SectionId, _
                    .SectionName = ParamSection.SectionName, _
                    .SectionDescription = ParamSection.SectionDescription, _
                    .SectionType = ParamSection.SectionType, _
                    .SectionCaption = ParamSection.SectionCaption, _
                    .SectionSeq = ParamSection.SectionSeq, _
                    .ParameterDetailItemCollection = (From ParamDetail In db.ParamDetails _
                    From ParamItem In db.ParamItems _
                    Where ParamSection.SectionId = ParamDetail.SectionId _
                    And ParamDetail.ItemId = ParamItem.ItemId _
                    Order By ParamDetail.ItemSeq _
                    Select New ParameterDetailItemInfo With {.ItemId = ParamDetail.ItemId, _
                    .ItemName = ParamItem.Caption, _
                    .ReturnValueName = ParamItem.ReturnValueName, _
                    .ControllerClass = ParamItem.ControllerClass, _
                    .DetailId = ParamDetail.DetailId, _
                    .SetId = ParamSet.SetId, _
                    .SetName = ParamSet.SetName, _
                    .SectionType = ParamSection.SectionType, _
                    .PageId = ParamSection.SectionId, _
                    .PageName = ParamSection.SectionName, _
                    .CaptionOverride = ParamDetail.CaptionOverride, _
                    .ValueProp = ParamItem.valueprop, _
                    .ItemSeq = ParamDetail.ItemSeq, _
                    .ParameterDetailPropertyCollection = ( _
                    From ParamDetailProp In db.ParamDetailProps _
                    Where ParamDetail.DetailId = ParamDetailProp.detailid _
                    Select New ParameterDetailPropertyInfo With { _
                    .DetailPropertyId = ParamDetailProp.detailpropertyid, _
                    .DetailId = ParamDetailProp.detailid, _
                    .ChildControl = ParamDetailProp.childcontrol, _
                    .PropName = ParamDetailProp.propname, _
                    .Value = ParamDetailProp.value, _
                    .ValueType = ParamDetailProp.valuetype}), _
                    .ParameterItemPropertyCollection = ( _
                    From ParamItemProp In db.ParamItemProps _
                    Where ParamItem.ItemId = ParamItemProp.itemid _
                    Select New ParameterItemPropertyInfo With {.ItemPropertyId = ParamItemProp.itempropertyid, _
                    .ItemId = ParamItemProp.itemid, _
                    .ChildControl = ParamItemProp.childcontrol, _
                    .PropName = ParamItemProp.propname, _
                    .Value = ParamItemProp.value, _
                    .ValueType = ParamItemProp.valuetype})})})}).Single

            Return q
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
