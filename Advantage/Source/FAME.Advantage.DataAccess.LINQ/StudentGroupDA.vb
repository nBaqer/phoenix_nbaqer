﻿Imports System.Net.Sockets
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.Advantage.DataAccess.LINQ.Common

Public Class StudentGroupDA
    Inherits LinqDataAccess

    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub

    Public Function GetStudentGroupsByCampusId(ByVal CampusId As String) As List(Of GenericListItem)
        Dim query as syStatus = (From  ss In db.syStatus Select ss Where ss.Status.Equals("Active")).SingleOrDefault()
        Dim result As IQueryable(Of GenericListItem) = From sg In db.adLeadGroups
                                                       Join cgc In db.syCmpGrpCmps On sg.CampGrpId Equals cgc.CampGrpId
                                                       Where ( sg.StatusId = query.StatusId) And  (cgc.CampusId.ToString() = CampusId OrElse sg.syCampGrp.IsAllCampusGrp )
                                                       Select New GenericListItem  With {
                                                                           .Id = sg.LeadGrpId.ToString(),
                                                                           .Description = sg.Descrip}

        Dim resultList = result.Distinct().OrderBy(Function(x) x.Description).ToList()
        
        Return resultList
    End Function

End Class
