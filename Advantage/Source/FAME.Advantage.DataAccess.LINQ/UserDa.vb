﻿'Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Public Class UserDa
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function USP_getInstructor_byCampusDate_andTermCourse(ByVal Campus As Guid, ByVal WeekEndDate As Date,
                                                                 ByVal TermId As String, ByVal CourseId As String, ByVal ShowInactive As Boolean) As IEnumerable(Of USP_getInstructor_byCampusDate_andTermCourseResult)
        Try
            Dim query As IList(Of USP_getInstructor_byCampusDate_andTermCourseResult) = db.USP_getInstructor_byCampusDate_andTermCourse(Campus, WeekEndDate, TermId, CourseId, ShowInactive).ToList()
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
