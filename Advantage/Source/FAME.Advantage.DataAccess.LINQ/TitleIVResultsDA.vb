﻿Public Class TitleIVResultsDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub

    Public Sub UpdatePrintedFlagForEnrollment(ByVal enrollments As String)

        if string.IsNullOrEmpty(enrollments) =  False
            'split enrollments being passed in UI to a list
            Dim enrollmentsList = enrollments.Split(New Char() {","c})

            Dim results = (From r In db.arFASAPChkResults
                    Where enrollmentsList.Contains(r.StuEnrollId.ToString())
                    Order By r.DatePerformed Descending
                    Group r By r.StuEnrollId Into Group
                    Select Group.FirstOrDefault()).ToList()

            For Each r In results
                r.Printed = True
            Next
            db.SubmitChanges()
        End If
        

    End Sub
End Class
