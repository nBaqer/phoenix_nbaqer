﻿Imports FAME.Advantage.Common.LINQ.Entities

Public Class StateReportsDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub

    Public Function GetStateReportsForState(stateId As Guid) As IQueryable(Of syReport)
        Try
            Dim query As IQueryable(Of syReport) =
                    (From y In db.syStateReports
                     Where y.StateId = stateId
                     Select y.syReport)
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
