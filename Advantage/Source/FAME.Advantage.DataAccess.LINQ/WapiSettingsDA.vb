﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Imports LinqKit
Imports System.Linq.Expressions
Imports FAME.Parameters.Collections
Public Class WapiSettingsDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function GetWapiSettingsByCode(ByVal codeName As String) As WapiSetting
        Try
            Dim i As Integer = 0
            Dim query = (From wSett In db.syWapiSettings
                    Where wSett.CodeOperation = codeName AndAlso wSett.IsActive).Select(Function(s) new WapiSetting With {.ExternalUrl = s.ExternalUrl, .ClientKey = s.ConsumerKey, .Password = s.PrivateKey, .UserName = s.UserName}).FirstOrDefault()
            Return query
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
End Class
