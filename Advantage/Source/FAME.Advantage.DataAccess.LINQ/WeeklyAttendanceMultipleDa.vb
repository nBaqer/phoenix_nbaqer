﻿'Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Public Class WeeklyAttendanceMultipleDa
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function USP_WeeklyAttendance(ByVal weekEndingDate As Date, ByVal campus As Guid, ByVal termId As String, ByVal courseId As String, ByVal instructorId As String, ByVal shiftId As String, ByVal classId As String, ByVal enrollmentId As String) As Integer
        Try
            Dim weekstartDate As Date
            Dim weekEndDate As Date
            weekEndDate = DateAdd("d", 7 - (DatePart(DateInterval.Weekday, weekEndingDate, FirstDayOfWeek.Sunday)), weekEndingDate)
            weekstartDate = DateAdd("d", -6, weekEndDate)
            Dim query As IList(Of USP_WeeklyAttendanceResult) = db.USP_WeeklyAttendance(weekstartDate, weekEndDate, campus, termId, courseId, instructorId, shiftId, classId, enrollmentId).ToList()
            Return query.Count
        Catch ex As Exception
        Catch ex As Exception
        End Try
    End Function
End Class
