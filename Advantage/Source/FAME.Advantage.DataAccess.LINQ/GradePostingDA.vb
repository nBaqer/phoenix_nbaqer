﻿Imports System.Data.Common
Imports FAME.Advantage.DataAccess.LINQ.Common
Imports FAME.AdvantageV1.Common

Public Class GradePostingDA
    Inherits LinqDataAccess

    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub

    Public Function GetEnrollmentStartDate(ByVal enrollment As Guid)
        Dim enrollmentDate = From e In db.arStuEnrollments
                             Where e.StuEnrollId = enrollment
                             Select e.StartDate
        Return enrollmentDate.ToList()
    End Function

    Public Function GetClassSectionData(ByVal term As String, ByVal studentGroup As String, ByVal classOrClassSection As String, ByVal grdCriteria As String, Optional ByVal itemToGradeDescription As String = "", Optional ByVal resNum As Integer = 0) As List(Of ClassSectionStudent)
        Dim result = New ClassSection
        Dim finalResult = New List(Of ClassSectionStudent)
        Dim isClassSection = Not String.IsNullOrEmpty(term) And term <> Guid.Empty.ToString() And (String.IsNullOrEmpty(studentGroup) Or studentGroup = Guid.Empty.ToString())

        Dim hasStudentGroup = (Not String.IsNullOrEmpty(studentGroup) And Not studentGroup = Guid.Empty.ToString())

        Dim possibleEnrollments = New List(Of Guid)

        If hasStudentGroup Then
            possibleEnrollments = (From e In db.arStuEnrollments
                                   Join ld In db.adLeadByLeadGroups On ld.LeadId Equals e.LeadId
                                   Where ld.LeadGrpId.ToString() = studentGroup
                                   Select e.StuEnrollId).ToList()

        End If

        'Join g In db.arGradeSystemDetails On g.GrdSysDetailId Equals r.GrdSysDetailId
        Dim classSectionStudentGradesQuery = From r In db.arResults
                                             Join e In db.arStuEnrollments On r.StuEnrollId Equals e.StuEnrollId
                                             Join s In db.arStudents On e.StudentId Equals s.StudentId
                                             Join sc In db.syStatusCodes On e.StatusCodeId Equals sc.StatusCodeId
                                             Join ss In db.sySysStatus On ss.SysStatusId Equals sc.SysStatusId
                                             Join c In db.arClassSections On c.ClsSectionId Equals r.TestId
                                             Join w In db.arGrdBkWeights On w.ReqId Equals c.ReqId
                                             Join d In db.arGrdBkWgtDetails On d.InstrGrdBkWgtId Equals w.InstrGrdBkWgtId
                                             Where (ss.PostAcademics OrElse ss.SysStatusId = 7) _
                                                    And (r.TestId.ToString = classOrClassSection) _
                                                    And (d.InstrGrdBkWgtDetailId.ToString() = grdCriteria) _
                                                    AndAlso If(hasStudentGroup, possibleEnrollments.Contains(e.StuEnrollId), True)
                                             Select New ClassSectionStudentGrades With {
                                             .LastName = s.LastName,
                                             .FirstName = s.FirstName,
                                             .Ssn = If(s.SSN.Length = 9, "*******" + s.SSN.Substring(6, 4), ""),
                                            .Grade = r.arGradeSystemDetail.Grade,
                                            .GradeSystemDetailId = r.GrdSysDetailId.ToString(),
                                            .IsCompGraded = False,
                                            .StudentEnrollmentId = e.StuEnrollId.ToString(),
                                            .StudentNumber = s.StudentNumber,
                                            .StudentId = s.StudentId.ToString(),
                                                        .Sequence = d.Seq,
                                            .InstructorGradeBookWeightDetailId = d.InstrGrdBkWgtDetailId.ToString(),
                .ClassSectionId = c.ClsSectionId.ToString(),
                .itemToGradeDescription = d.Descrip
                                                                 }

        result.ClassSectionStudentGrades = classSectionStudentGradesQuery.ToList()

        Dim classSectionGradesQuery = From gbr In db.arGrdBkResults
                                      Join c In db.arClassSections On c.ClsSectionId Equals gbr.ClsSectionId
                                      Where If(isClassSection, gbr.ClsSectionId.ToString = classOrClassSection, c.ReqId.ToString = classOrClassSection) _
                                      AndAlso If(itemToGradeDescription = "InstrGrdBkWgtDetailId", gbr.InstrGrdBkWgtDetailId.ToString() = grdCriteria, gbr.arGrdBkWgtDetail.GrdComponentTypeId.ToString = grdCriteria) _
                                      AndAlso If(resNum > 0, gbr.ResNum = resNum, c.ClsSectionId.ToString() = classOrClassSection)
                                      Select New ClassSectionGrades With {
                                                            .StudentEnrollmentId = gbr.StuEnrollId.ToString(),
                                                            .Comments = gbr.Comments,
                                                            .DateCompleted = gbr.DateCompleted,
                                                            .GradebookResultId = gbr.GrdBkResultId.ToString(),
                                                            .IsCompGraded = ((gbr.IsCompGraded IsNot Nothing) AndAlso gbr.IsCompGraded.Value),
                                                            .Score = gbr.Score,
                                                            .resNum = gbr.ResNum
                                          }
        result.ClassSectionGrades = classSectionGradesQuery.ToList()

        finalResult = (From s In result.ClassSectionStudentGrades
                       Group Join g In result.ClassSectionGrades On g.StudentEnrollmentId Equals s.StudentEnrollmentId Into Group
                       From g In Group.DefaultIfEmpty()
                       Select New ClassSectionStudent With {
                           .LastName = s.LastName,
                           .FirstName = s.FirstName,
                           .Ssn = s.Ssn,
                           .Grade = s.Grade,
                           .GradeSystemDetailId = If(s.GradeSystemDetailId IsNot Nothing, s.GradeSystemDetailId.ToString(), ""),
                           .IsCompGraded = ((g IsNot Nothing) AndAlso g.IsCompGraded),
                           .StudentEnrollmentId = s.StudentEnrollmentId.ToString(),
                           .StudentNumber = s.StudentNumber,
                           .StudentId = s.StudentId.ToString(),
                           .Comments = If(g IsNot Nothing, g.Comments, ""),
                           .DateCompleted = If(g IsNot Nothing, g.DateCompleted, Nothing),
                           .GradebookResultId = If(g IsNot Nothing, g.GradebookResultId, ""),
                           .InstructorGradeBookWeightDetailId = s.InstructorGradeBookWeightDetailId,
                           .ClassSectionId = s.ClassSectionId,
                           .Score = If(g IsNot Nothing, g.Score, ""),
                           .itemToGradeDescription = s.ItemToGradeDescription,
                           .Sequence = If(s.GradeSystemDetailId IsNot Nothing, s.Sequence, 0),
                           .resNum = If(g IsNot Nothing, g.ResNum, 0)
                                                     }).ToList()

        finalResult = finalResult.OrderBy(Function(r As ClassSectionStudent) r.LastName).ThenBy(Function(r As ClassSectionStudent) r.StudentEnrollmentId).ThenBy(Function(r As ClassSectionStudent) r.Sequence).ToList()
        Return finalResult
    End Function


    Public Function GetClassSectionDataInstructor(ByVal term As String, ByVal studentGroup As String, ByVal classOrClassSection As String, ByVal grdCriteria As String, Optional ByVal itemToGradeDescription As String = "InstrGrdBkWgtDetailId") As List(Of ClassSectionStudent)
        Dim result = New ClassSection
        Dim finalResult = New List(Of ClassSectionStudent)
        Dim isClassSection = Not String.IsNullOrEmpty(term) And term <> Guid.Empty.ToString() And (String.IsNullOrEmpty(studentGroup) Or studentGroup = Guid.Empty.ToString())

        Dim hasStudentGroup = (Not String.IsNullOrEmpty(studentGroup) And Not studentGroup = Guid.Empty.ToString())

        Dim possibleEnrollments = New List(Of Guid)

        If hasStudentGroup Then
            possibleEnrollments = (From e In db.arStuEnrollments
                                   Join ld In db.adLeadByLeadGroups On ld.LeadId Equals e.LeadId
                                   Where ld.LeadGrpId.ToString() = studentGroup
                                   Select e.StuEnrollId).ToList()

        End If


        'Join g In db.arGradeSystemDetails On g.GrdSysDetailId Equals r.GrdSysDetailId
        Dim classSectionStudentGradesQuery = From r In db.arResults
                                             Join e In db.arStuEnrollments On r.StuEnrollId Equals e.StuEnrollId
                                             Join s In db.arStudents On e.StudentId Equals s.StudentId
                                             Join sc In db.syStatusCodes On e.StatusCodeId Equals sc.StatusCodeId
                                             Join ss In db.sySysStatus On ss.SysStatusId Equals sc.SysStatusId
                                             Join c In db.arClassSections On c.ClsSectionId Equals r.TestId
                                             Group Join d In db.arGrdBkWgtDetails On d.InstrGrdBkWgtId Equals c.InstrGrdBkWgtId Into co = Group From o In co.DefaultIfEmpty()
                                             Where (ss.PostAcademics OrElse ss.SysStatusId = 7) _
                                                   AndAlso If(hasStudentGroup, possibleEnrollments.Contains(e.StuEnrollId), True) _
                                                    AndAlso If(isClassSection, r.TestId.ToString = classOrClassSection, c.ReqId.ToString = classOrClassSection) _
                                                    AndAlso If(isClassSection, o.InstrGrdBkWgtDetailId.ToString() = grdCriteria, True)
                                             Select New ClassSectionStudentGrades With {
                                             .LastName = s.LastName,
                                             .FirstName = s.FirstName,
                                             .Ssn = If(s.SSN.Length = 9, "*******" + s.SSN.Substring(6, 4), ""),
                                            .Grade = r.arGradeSystemDetail.Grade,
                                            .GradeSystemDetailId = r.GrdSysDetailId.ToString(),
                                            .IsCompGraded = False,
                                            .StudentEnrollmentId = e.StuEnrollId.ToString(),
                                            .StudentNumber = s.StudentNumber,
                                            .StudentId = s.StudentId.ToString(),
                                            .Sequence = If(o.Seq.HasValue, o.Seq.Value, 0),
                                            .InstructorGradeBookWeightDetailId = o.InstrGrdBkWgtDetailId.ToString(),
                                            .ClassSectionId = c.ClsSectionId.ToString(),
                                            .itemToGradeDescription = o.Descrip
                                                                 }

        result.ClassSectionStudentGrades = classSectionStudentGradesQuery.ToList()

        Dim classSectionGradesQuery = From gbr In db.arGrdBkResults
                                      Join c In db.arClassSections On c.ClsSectionId Equals gbr.ClsSectionId
                                      Join gbwd In db.arGrdBkWgtDetails On gbwd.InstrGrdBkWgtDetailId Equals gbr.InstrGrdBkWgtDetailId
                                      Where If(isClassSection, gbr.ClsSectionId.ToString = classOrClassSection, c.ReqId.ToString = classOrClassSection) _
                                      AndAlso If(itemToGradeDescription = "InstrGrdBkWgtDetailId", gbr.InstrGrdBkWgtDetailId.ToString() = grdCriteria, gbwd.GrdComponentTypeId.ToString = grdCriteria)
                                      Select New ClassSectionGrades With {
                                                            .StudentEnrollmentId = gbr.StuEnrollId.ToString(),
                                                            .Comments = gbr.Comments,
                                                            .DateCompleted = gbr.DateCompleted,
                                                            .GradebookResultId = gbr.GrdBkResultId.ToString(),
                                                            .IsCompGraded = If(gbr.IsCompGraded Is Nothing, 0, gbr.IsCompGraded),
                                                            .Score = gbr.Score,
                                                            .ResNum = gbr.ResNum
                                          }
        result.ClassSectionGrades = classSectionGradesQuery.ToList()

        finalResult = (From s In result.ClassSectionStudentGrades
                       Group Join g In result.ClassSectionGrades On g.StudentEnrollmentId Equals s.StudentEnrollmentId Into Group
                       From g In Group.DefaultIfEmpty()
                       Select New ClassSectionStudent With {
                           .LastName = s.LastName,
                           .FirstName = s.FirstName,
                           .Ssn = s.Ssn,
                           .Grade = s.Grade,
                           .GradeSystemDetailId = If(s.GradeSystemDetailId IsNot Nothing, s.GradeSystemDetailId.ToString(), ""),
                           .IsCompGraded = ((g IsNot Nothing) AndAlso g.IsCompGraded),
                           .StudentEnrollmentId = s.StudentEnrollmentId.ToString(),
                           .StudentNumber = s.StudentNumber,
                           .StudentId = s.StudentId.ToString(),
                           .Comments = If(g IsNot Nothing, g.Comments, ""),
                           .DateCompleted = If(g IsNot Nothing, g.DateCompleted, Nothing),
                           .GradebookResultId = If(g IsNot Nothing, g.GradebookResultId, ""),
                            .InstructorGradeBookWeightDetailId = s.InstructorGradeBookWeightDetailId,
                                  .ClassSectionId = s.ClassSectionId,
                           .Score = If(g IsNot Nothing, g.Score, ""),
                           .itemToGradeDescription = s.ItemToGradeDescription,
                           .Sequence = If(s.GradeSystemDetailId IsNot Nothing, s.Sequence, 0),
                           .ResNum = If(g IsNot Nothing, g.ResNum, 0)
                                                     }).ToList()
        finalResult = finalResult.OrderBy(Function(r As ClassSectionStudent) r.LastName).ThenBy(Function(r As ClassSectionStudent) r.StudentEnrollmentId).ThenBy(Function(r As ClassSectionStudent) r.Sequence).ToList()
        Return finalResult

        Return finalResult
    End Function

    Public Function GetGradingCriteria(ByVal ClassSectOrClass As String, ByVal Instr As String, ByVal username As String, Optional ByVal isAcademicAdvisor As Boolean = False, Optional ByVal isBaseOnClassSection As Boolean = True) As List(Of GenericListItem)
        Dim instructors = New List(Of String)
        Dim queryResult As IQueryable(Of GenericListItem)

        Dim result = New List(Of GenericListItem)
        Dim components = New List(Of Integer)
        components.Add(500)
        components.Add(503)
        components.Add(544)

        If username <> "sa" And username.ToLower() <> "support" And isAcademicAdvisor = False Then
            instructors = From I In db.arInstructorsSupervisors
                          Where I.SupervisorId.ToString() = Instr
                          Select I.InstructorId.ToString.Distinct().ToList()

            If Not instructors.Contains(Instr) Then
                instructors.Add(Instr)
            End If
        End If

        If isBaseOnClassSection Then


            queryResult = (From gbwd In db.arGrdBkWgtDetails
                           Join cs In db.arClassSections On gbwd.InstrGrdBkWgtId Equals cs.InstrGrdBkWgtId
                           Join gbw In db.arGrdBkWeights On cs.InstructorId Equals gbw.InstructorId
                           Join gct In db.arGrdComponentTypes On gbwd.GrdComponentTypeId Equals gct.GrdComponentTypeId
                           Where cs.ClsSectionId.ToString() = ClassSectOrClass _
                                 AndAlso Not components.Contains(gct.SysComponentTypeId.ToString())
                           Select New GenericListItem With {
                               .Id = gbwd.InstrGrdBkWgtDetailId.ToString(),
                               .Description = gbwd.Descrip})

            result = queryResult.Distinct().ToList()
        Else
            queryResult = (From gbwd In db.arGrdBkWgtDetails
                           Join cs In db.arClassSections On gbwd.InstrGrdBkWgtId Equals cs.InstrGrdBkWgtId
                           Join gbw In db.arGrdBkWeights On cs.InstructorId Equals gbw.InstructorId
                           Join gct In db.arGrdComponentTypes On gbwd.GrdComponentTypeId Equals gct.GrdComponentTypeId
                           Where cs.ReqId.ToString() = ClassSectOrClass _
                                 AndAlso Not components.Contains(gct.SysComponentTypeId.ToString())
                           Select New GenericListItem With {
                               .Id = gbwd.GrdComponentTypeId.ToString(),
                               .Description = gbwd.arGrdComponentType.Descrip})

            result = queryResult.Distinct().ToList()
        End If

        Return result.OrderBy(Function(r As GenericListItem) r.Description).ToList()
    End Function

    Public Function GetCourseLevelGrdCriteria(ByVal ClassSectOrClass As String, Optional ByVal isBaseOnClassSection As Boolean = True) As List(Of GenericListItem)
        Dim classes = New List(Of String)
        Dim result As List(Of GenericListItem)
        Dim queryResult As IQueryable(Of GenericListItem)
        Dim maxDate = DateTime.Now '(New FA.GrdPostingsDB).GetRecentDate(ClassSectOrClass) 'this function will need to be created 

        Dim components = New List(Of Integer)
        components.Add(500)
        components.Add(503)
        components.Add(544)

        If isBaseOnClassSection Then
            classes = (From r In db.arClassSections
                       Where r.ClsSectionId.ToString() = ClassSectOrClass
                       Select (r.InstructorId.ToString())).Distinct().ToList()

            queryResult = (From gbwd In db.arGrdBkWgtDetails
                           Join gbw In db.arGrdBkWeights On gbwd.InstrGrdBkWgtId Equals gbw.InstrGrdBkWgtId
                           Join cs In db.arClassSections On gbw.ReqId Equals cs.ReqId
                           Join gct In db.arGrdComponentTypes On gbwd.GrdComponentTypeId Equals gct.GrdComponentTypeId
                           Where classes.Contains(gbw.ReqId.ToString()) _
                                 AndAlso gbw.EffectiveDate <= cs.StartDate _
                                 AndAlso gbw.EffectiveDate = maxDate _
                                 AndAlso Not components.Contains(gct.SysComponentTypeId.ToString())
                           Select New GenericListItem With {
                               .Id = gbwd.InstrGrdBkWgtDetailId.ToString(),
                               .Description = gbwd.Descrip})

            result = queryResult.Distinct().ToList()
        Else
            queryResult = (From gbwd In db.arGrdBkWgtDetails
                           Join gbw In db.arGrdBkWeights On gbwd.InstrGrdBkWgtId Equals gbw.InstrGrdBkWgtId
                           Join cs In db.arClassSections On gbw.ReqId Equals cs.ReqId
                           Join gct In db.arGrdComponentTypes On gbwd.GrdComponentTypeId Equals gct.GrdComponentTypeId
                           Where gbw.ReqId.ToString() = ClassSectOrClass _
                                 AndAlso gbw.EffectiveDate <= cs.StartDate _
                                 AndAlso Not components.Contains(gct.SysComponentTypeId.ToString())
                           Select New GenericListItem With {
                               .Id = gbwd.InstrGrdBkWgtDetailId.ToString(),
                               .Description = gbwd.arGrdComponentType.Descrip})
            'AndAlso gbw.EffectiveDate = maxDate _

            result = queryResult.Distinct().ToList()
        End If


        Return result.OrderBy(Function(r As GenericListItem) r.Description).ToList()
    End Function
End Class
