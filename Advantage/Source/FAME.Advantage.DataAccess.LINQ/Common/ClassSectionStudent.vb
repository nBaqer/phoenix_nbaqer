﻿Public Class ClassSectionStudent
#Region " Private Variables and Objects"
    Private _studentEnrollmentId As String
    Private _lastName As String
    Private _ssn As String
    Private _firstName As String
    Private _studentNumber As String
    Private _gradeSystemDetailId As String
    Private _studentId As String
    Private _grade As String
    Private _isCompGraded As Boolean
    Private _score As String
    Private _dateCompleted As DateTime?
    Private _comments As String
    Private _gradebookResultId As String
    private _instructorGradeBookWeightDetailId As String
    private _classSectionId As String
    Private _itemToGradeDescription As String
    Private _sequence As Integer
    Private _resNum As Integer
#End Region


#Region " Public Constructors "
    Public Sub New ()
        _studentEnrollmentId = string.Empty
        _lastName = string.Empty
        _ssn = string.Empty
        _firstName = string.Empty
        _studentNumber = string.Empty
        _gradeSystemDetailId = string.Empty
        _studentId = string.Empty
        _grade = string.Empty
        _isCompGraded = False
        _score = string.Empty
        _comments = string.Empty
        _gradebookResultId = string.Empty
        _instructorGradeBookWeightDetailId = string.Empty
        _classSectionId= string.Empty
        _itemToGradeDescription= string.Empty
        _sequence = 0
        _resNum = 0
    End Sub
#End Region

#Region " Public Properties"
    Public Property StudentEnrollmentId() As String
        Get
            Return _studentEnrollmentId
        End Get
        Set(ByVal Value As String)
            _studentEnrollmentId = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return _lastName
        End Get
        Set(ByVal Value As String)
            _lastName = Value
        End Set
    End Property
    Public Property Ssn() As String
        Get
            Return _ssn
        End Get
        Set(ByVal Value As String)
            _ssn = Value
        End Set
    End Property
    Public Property FirstName() As String
        Get
            Return _firstName
        End Get
        Set(ByVal Value As String)
            _firstName = Value
        End Set
    End Property
    Public Property StudentNumber() As String
        Get
            Return _studentNumber
        End Get
        Set(ByVal Value As String)
            _studentNumber = Value
        End Set
    End Property
    Public Property GradeSystemDetailId() As String
        Get
            Return _gradeSystemDetailId
        End Get
        Set(ByVal Value As String)
            _gradeSystemDetailId = Value
        End Set
    End Property
    Public Property StudentId() As String
        Get
            Return _studentId
        End Get
        Set(ByVal Value As String)
            _studentId = Value
        End Set
    End Property
    Public Property Grade() As String
        Get
            Return _grade
        End Get
        Set(ByVal Value As String)
            _grade = Value
        End Set
    End Property
    Public Property IsCompGraded() As Boolean
        Get
            Return _isCompGraded
        End Get
        Set(ByVal Value As Boolean)
            _isCompGraded = Value
        End Set
    End Property
    Public Property Score() As String
        Get
            Return _score
        End Get
        Set(ByVal Value As String)
            _score = Value
        End Set
    End Property
    Public Property DateCompleted() As DateTime?
        Get
            Return _dateCompleted
        End Get
        Set(ByVal Value As DateTime?)
            _dateCompleted = Value
        End Set
    End Property
    Public Property Comments() As String
        Get
            Return _comments
        End Get
        Set(ByVal Value As String)
            _comments = Value
        End Set
    End Property
    Public Property InstructorGradeBookWeightDetailId() As String
        Get
            Return _instructorGradeBookWeightDetailId
        End Get
        Set(ByVal Value As String)
            _instructorGradeBookWeightDetailId = Value
        End Set
    End Property

    Public Property GradebookResultId() As String
        Get
            Return _gradebookResultId
        End Get
        Set(ByVal Value As String)
            _gradebookResultId = Value
        End Set
    End Property
    Public Property ClassSectionId() As String
        Get
            Return _classSectionId
        End Get
        Set(ByVal Value As String)
            _classSectionId = Value
        End Set
    End Property
    Public Property ItemToGradeDescription() As String
        Get
            Return _itemToGradeDescription
        End Get
        Set(ByVal Value As String)
            _itemToGradeDescription = Value
        End Set
    End Property

    Public Property Sequence() As Integer
        Get
            Return _sequence
        End Get
        Set(ByVal Value As Integer)
            _sequence = Value
        End Set
    End Property

    Public Property ResNum() As Integer
        Get
            Return _resNum
        End Get
        Set(ByVal Value As Integer)
            _resNum = Value
        End Set
    End Property
#End Region
End Class
