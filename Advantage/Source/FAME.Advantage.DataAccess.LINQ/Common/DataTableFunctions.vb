﻿Imports System.Reflection
Namespace Common
    Public Class DataTableFunctions
        Public Shared Function ConvertToDataTable(Of T)(ByVal list As IList(Of T)) As DataTable
            Dim table As New DataTable()
            Dim fields() As FieldInfo = GetType(T).GetFields(BindingFlags.Instance Or BindingFlags.Static Or BindingFlags.NonPublic Or BindingFlags.Public)
            For Each field As FieldInfo In fields
                table.Columns.Add(field.Name.Replace("_", ""), If(Nullable.GetUnderlyingType(field.FieldType), field.FieldType))
            Next
            For Each item As T In list
                Dim row As DataRow = table.NewRow()
                For Each field As FieldInfo In fields
                    row(field.Name.Replace("_", "")) = If(field.GetValue(item), CObj(DBNull.Value))
                Next
                table.Rows.Add(row)
            Next
            Return table
        End Function
    End Class
End Namespace
