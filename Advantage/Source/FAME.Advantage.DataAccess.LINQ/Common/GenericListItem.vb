﻿Namespace Common
    Public Class GenericListItem
        Implements IEquatable(Of GenericListItem)
#Region " Private Variables and Objects"
        Private _id As String
        Private _description As String
        Private _status As String
#End Region

#Region " Public Constructors "
        Public Sub New()
            _id = String.Empty
            _description = String.Empty
            _status = String.Empty
        End Sub
#End Region

#Region " Public Properties"
        Public Property Id() As String
            Get
                Return _id
            End Get
            Set(ByVal Value As String)
                _id = Value
            End Set
        End Property
        Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal Value As String)
                _description = Value
            End Set
        End Property
        Public Property Status() As String
            Get
                Return _status
            End Get
            Set(ByVal Value As String)
                _status = Value
            End Set
        End Property
#End Region
        Public Function Equals(other As GenericListItem) As Boolean Implements IEquatable(Of GenericListItem).Equals

            Return Me.Description = other.Description And Me.Id = other.Id
        End Function

        Public Overrides Function GetHashCode() As Integer
            Return (Me.Id + Me.Description).GetHashCode()
        End Function
    End Class

    
End Namespace