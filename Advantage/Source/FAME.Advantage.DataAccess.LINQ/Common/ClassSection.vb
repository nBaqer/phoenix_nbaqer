﻿Public Class ClassSection
#Region " Private Variables and Objects"
    Private _classSectionGrades As List(Of ClassSectionGrades)
    Private _classSectionStudentGrades As List(Of ClassSectionStudentGrades)
#End Region

#Region " Public Constructors "
    Public Sub New()
        _classSectionGrades = New List(Of ClassSectionGrades)()
        _classSectionStudentGrades = New List(Of ClassSectionStudentGrades)
    End Sub
#End Region

#Region " Public Properties"
    Public Property ClassSectionGrades() As List(Of ClassSectionGrades)
        Get
            Return _classSectionGrades
        End Get
        Set(ByVal Value As List(Of ClassSectionGrades))
            _classSectionGrades = Value
        End Set
    End Property

    Public Property ClassSectionStudentGrades() As List(Of ClassSectionStudentGrades)
        Get
            Return _classSectionStudentGrades
        End Get
        Set(ByVal Value As List(Of ClassSectionStudentGrades))
            _classSectionStudentGrades = Value
        End Set
    End Property
#End Region
End Class
