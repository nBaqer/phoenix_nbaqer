﻿Imports FAME.Advantage.DataAccess.LINQ.Common

Public Class ClassSectionTerm
    Inherits GenericListItem
    Implements IEquatable(Of ClassSectionTerm)

#Region " Private Variables and Objects"
    Private _requirementId As String
    Private _instructorGradeBookWeightId As String
#End Region

#Region " Public Constructors "
    Public Sub New()
        _requirementId = String.Empty
        _instructorGradeBookWeightId = String.Empty
    End Sub
#End Region

#Region " Public Properties"
    Public Property RequirementId() As String
        Get
            Return _requirementId
        End Get
        Set(ByVal Value As String)
            _requirementId = Value
        End Set
    End Property
    Public Property InstructorGradeBookWeightId() As String
        Get
            Return _instructorGradeBookWeightId
        End Get
        Set(ByVal Value As String)
            _instructorGradeBookWeightId = Value
        End Set
    End Property
#End Region

    Public Function Equals(other As ClassSectionTerm) As Boolean Implements IEquatable(Of ClassSectionTerm).Equals

        Return Me.RequirementId = other.RequirementId And Me.InstructorGradeBookWeightId = other.InstructorGradeBookWeightId And Me.Description = other.Description And Me.Id = other.Id 
    End Function

    Public Overrides Function GetHashCode() As Integer
        Return (Me.RequirementId + Me.Id + Me.Description + Me.InstructorGradeBookWeightId).GetHashCode()
    End Function
End Class


