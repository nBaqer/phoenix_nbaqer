﻿Public Class ClassSectionGrades
#Region " Private Variables and Objects"
    Private _studentEnrollmentId As String
    Private _score As String
    Private _dateCompleted As DateTime?
    Private _comments As String
    Private _gradebookResultId As String
    Private _isCompGraded As Boolean
    Private _resNum As Integer
#End Region

#Region " Public Constructors "
    Public Sub New ()
        _studentEnrollmentId = string.Empty
        _score = string.Empty
        _comments = string.Empty
        _gradebookResultId = string.Empty
        _isCompGraded = False
        _resNum = 0
    End Sub
#End Region
#Region " Public Properties"
    Public Property StudentEnrollmentId() As String
        Get
            Return _studentEnrollmentId
        End Get
        Set(ByVal Value As String)
            _studentEnrollmentId = Value
        End Set
    End Property
    Public Property Score() As String
        Get
            Return _score
        End Get
        Set(ByVal Value As String)
            _score = Value
        End Set
    End Property
    Public Property DateCompleted() As  DateTime?
        Get
            Return _dateCompleted
        End Get
        Set(ByVal Value As  DateTime?)
            _dateCompleted = Value
        End Set
    End Property
    Public Property Comments() As String
        Get
            Return _comments
        End Get
        Set(ByVal Value As String)
            _comments = Value
        End Set
    End Property
    
    Public Property GradebookResultId() As String
        Get
            Return _gradebookResultId
        End Get
        Set(ByVal Value As String)
            _gradebookResultId = Value
        End Set
    End Property
    Public Property IsCompGraded() As Boolean
        Get
            Return _isCompGraded
        End Get
        Set(ByVal Value As Boolean)
            _isCompGraded = Value
        End Set
    End Property

    Public Property ResNum() As Integer
        Get
            Return _resNum
        End Get
        Set(ByVal Value As Integer)
            _resNum = Value
        End Set
    End Property
#End Region
End Class
