﻿Public Class ClassSectionStudentGrades
#Region " Private Variables and Objects"
    Private _studentEnrollmentId As String
    Private _lastName As String
    Private _ssn As String
    Private _firstName As String
    Private _studentNumber As String
    Private _gradeSystemDetailId As String
    Private _studentId As String
    Private _grade As String
    Private _isCompGraded As Boolean
    Private _instructorGradeBookWeightDetailId As String
    private _classSectionId As String
    Private _itemToGradeDescription As String
    Private _sequence As Integer
#End Region

#Region " Public Constructors "
    Public Sub New()
        _studentEnrollmentId = String.Empty
        _lastName = String.Empty
        _ssn = String.Empty
        _firstName = String.Empty
        _studentNumber = String.Empty
        _gradeSystemDetailId = String.Empty
        _studentId = String.Empty
        _grade = String.Empty
        _isCompGraded = False
        _instructorGradeBookWeightDetailId = String.Empty
        _classSectionId = String.Empty
        _itemToGradeDescription = String.Empty
        _sequence = 0
    End Sub
#End Region

#Region " Public Properties"
    Public Property StudentEnrollmentId() As String
        Get
            Return _studentEnrollmentId
        End Get
        Set(ByVal Value As String)
            _studentEnrollmentId = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return _lastName
        End Get
        Set(ByVal Value As String)
            _lastName = Value
        End Set
    End Property
    Public Property Ssn() As String
        Get
            Return _ssn
        End Get
        Set(ByVal Value As String)
            _ssn = Value
        End Set
    End Property
    Public Property FirstName() As String
        Get
            Return _firstName
        End Get
        Set(ByVal Value As String)
            _firstName = Value
        End Set
    End Property
    Public Property StudentNumber() As String
        Get
            Return _studentNumber
        End Get
        Set(ByVal Value As String)
            _studentNumber = Value
        End Set
    End Property
    Public Property GradeSystemDetailId() As String
        Get
            Return _gradeSystemDetailId
        End Get
        Set(ByVal Value As String)
            _gradeSystemDetailId = Value
        End Set
    End Property
    Public Property StudentId() As String
        Get
            Return _studentId
        End Get
        Set(ByVal Value As String)
            _studentId = Value
        End Set
    End Property
    Public Property Grade() As String
        Get
            Return _grade
        End Get
        Set(ByVal Value As String)
            _grade = Value
        End Set
    End Property
    Public Property IsCompGraded() As Boolean
        Get
            Return _isCompGraded
        End Get
        Set(ByVal Value As Boolean)
            _isCompGraded = Value
        End Set
    End Property
    Public Property InstructorGradeBookWeightDetailId() As String
        Get
            Return _instructorGradeBookWeightDetailId
        End Get
        Set(ByVal Value As String)
            _instructorGradeBookWeightDetailId = Value
        End Set
    End Property
    Public Property ItemToGradeDescription() As String
        Get
            Return _itemToGradeDescription
        End Get
        Set(ByVal Value As String)
            _itemToGradeDescription = Value
        End Set
    End Property
    Public Property ClassSectionId() As String
        Get
            Return _classSectionId
        End Get
        Set(ByVal Value As String)
            _classSectionId = Value
        End Set
    End Property
    Public Property Sequence() As Integer
        Get
            Return _sequence
        End Get
        Set(ByVal Value As Integer)
            _sequence = Value
        End Set
    End Property
#End Region
End Class
