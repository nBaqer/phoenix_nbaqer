﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Imports LinqKit
Imports System.Linq.Expressions
Imports FAME.Parameters.Collections
Public Class CampusDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Private Function GetAllCampusGroup() As syCampGrp
        Try
            Dim query As syCampGrp = (From cg In db.syCampGrps
                                      Select cg Where cg.IsAllCampusGrp.Equals(True)).SingleOrDefault
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function CreateCampGrpIdListByUserId(ByVal userid As Guid) As String()
        Try
            Dim i As Integer = 0
            Dim query As IQueryable(Of syUsersRolesCampGrp) = (From urcg In db.syUsersRolesCampGrps Where urcg.UserId.Equals(userid))
            Dim results As List(Of syUsersRolesCampGrp) = query.ToList
            Dim userName As String = String.Empty
            If userName.ToLower = "sa" Or userName.ToLower = "super" Or userName.ToLower = "support" Then
                Dim SuperUserQuery As IQueryable(Of syCampGrp) = (From cg In db.syCampGrps)
                Dim SuperUserCampGrpIds(SuperUserQuery.Count) As String
                i = 0
                For Each item As syCampGrp In SuperUserQuery
                    SuperUserCampGrpIds(i) = item.CampGrpId.ToString
                    i = i + 1
                Next
                Return SuperUserCampGrpIds
            Else

                Dim CampGrpIds(results.Count - 1) As String
                'restart counter
                i = 0
                For Each item As syUsersRolesCampGrp In results
                    CampGrpIds(i) = item.CampGrpId.ToString
                    i = i + 1
                Next
                Return CampGrpIds
            End If

        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    Private Function GetUserName(ByVal userid As Guid) As String
        Try
            Dim query As syUser = (From u In db.syUsers
                                   Select u Where u.UserId.Equals(userid)).SingleOrDefault
            Return query.UserName
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function CreateCampIdListFromCampGrpIds(ByVal CampGrpIds As String()) As String()
        Try
            Dim i As Integer = 0
            Dim query As IQueryable(Of syCmpGrpCmp) = (From c In db.syCmpGrpCmps Where CampGrpIds.Contains(c.CampGrpId.ToString))
            Dim results As List(Of syCmpGrpCmp) = query.ToList
            Dim CampusIds(results.Count - 1) As String
            For Each item As syCmpGrpCmp In results
                CampusIds(i) = item.CampusId.ToString
                i = i + 1
            Next
            Return CampusIds
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    Private Function CreateCampIdListFromACampusGroup(ByVal CampGrpId As String) As List(Of String)
        Dim CampusIds As New List(Of String)
        Try
            Dim i As Integer = 0
            Dim query As IQueryable(Of syCmpGrpCmp) = (From c In db.syCmpGrpCmps Where CampGrpId.Equals(c.CampGrpId))
            Dim results As List(Of syCmpGrpCmp) = query.ToList

            For Each item As syCmpGrpCmp In results
                CampusIds.Add(item.CampusId.ToString)
                i = i + 1
            Next

        Catch ex As Exception
            Throw ex
        End Try
        Return CampusIds
    End Function
    Public Function GetCampusesByUserId(ByVal UserId As Guid) As List(Of syCampus)
        Try
            Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(UserId)
            Dim CampusIds As String() = CreateCampIdListFromCampGrpIds(CampGrpIds)
            Dim query As IQueryable(Of syCampus) = (From c In db.syCampus
                                                    Where CampusIds.Contains(c.CampusId.ToString)
                                                    Select c Distinct
                                                    Order By c.CampDescrip)
            Return query.ToList
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetParentCampusesByUserId(ByVal UserId As Guid) As List(Of syCampus)
        Try
            Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(UserId)
            Dim CampusIds As String() = CreateCampIdListFromCampGrpIds(CampGrpIds)
            Dim query As IQueryable(Of syCampus) = (From c In db.syCampus
                                                    Where CampusIds.Contains(c.CampusId.ToString) AndAlso c.syStatus.Status = "Active"  AndAlso Not c.IsBranch
                                                    Select c Distinct
                                                    Order By c.CampDescrip)
            Return query.ToList
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetCampusesById(ByVal CampusIds As IEnumerable(Of Guid)) As IQueryable(Of syCampus)
        Try
            Dim query As IQueryable(Of syCampus) = (From c In db.syCampus
                                                    Where CampusIds.Contains(c.CampusId)
                                                    Select c Distinct
                                                    Order By c.CampDescrip)
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetCampusById(ByVal CampusId As Guid) As syCampus
        Try
            Dim query = (From c In db.syCampus
                         Where c.CampusId = CampusId
                         Select c).FirstOrDefault()
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetCampusesByUserId(ByVal UserId As Guid, Optional ByVal StatusTypes As List(Of String) = Nothing) As List(Of syCampus)
        Try
            Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(UserId)
            Dim CampusIds As String() = CreateCampIdListFromCampGrpIds(CampGrpIds)
            Dim query As IQueryable(Of syCampus) = (From c In db.syCampus
                                                    Where CampusIds.Contains(c.CampusId.ToString) _
                                                          AndAlso If(StatusTypes IsNot Nothing, StatusTypes.Contains(c.syStatus.Status), True)
                                                    Select c Distinct
                                                    Order By c.CampDescrip)
            Return query.ToList
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetCampusesByUserId(ByVal UserId As Guid, Optional ByVal StatusTypes As List(Of String) = Nothing, Optional ByVal selectedCampus As List(Of String) = Nothing) As List(Of syCampus)
        Try
            Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(UserId)
            Dim CampusIds As String() = CreateCampIdListFromCampGrpIds(CampGrpIds)
            Dim query As IQueryable(Of syCampus) = (From c In db.syCampus
                                                    Where CampusIds.Contains(c.CampusId.ToString) _
                                                     AndAlso If(StatusTypes IsNot Nothing, StatusTypes.Contains(c.syStatus.Status), True) _
                                                    AndAlso If(selectedCampus IsNot Nothing, selectedCampus.Contains(c.CampusId.ToString) = False, True)
                                                    Select c Distinct
                                                    Order By c.CampDescrip)
            Return query.ToList
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetCampusesForCampusGroups(ByVal campusGroups As List(Of String)) As List(Of syCampus)
        Try
            Dim query As IQueryable(Of syCampus) = From cgc In db.syCmpGrpCmps
                                                   Where campusGroups.Contains(cgc.CampGrpId.ToString)
                                                   Order By cgc.syCampus.CampDescrip
                                                   Select cgc.syCampus Distinct


            'Dim query As IQueryable(Of syCampus) = (From c In db.syCampus _
            '        Where CampusIds.Contains(c.CampusId.ToString) _
            '        Select c Distinct _
            '        Order By c.CampDescrip)
            Return query.ToList
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetCampuses(ByVal filtervalues As MasterDictionary) As IQueryable(Of syCampus)
        Dim query As IQueryable(Of syCampus) = (From c In db.syCampus
                                                Join cgc In db.syCmpGrpCmps
                                                On c.CampusId Equals cgc.CampusId
                                                Join r In db.syUsersRolesCampGrps
                                                On cgc.CampGrpId Equals r.CampGrpId
                                                Select c Distinct
                                                Order By c.CampDescrip)
        Dim basePredicate As Expression(Of Func(Of syCampus, Boolean)) = PredicateBuilder.True(Of syCampus)()
        basePredicate = basePredicate.And(Function(c) True)
        Dim UserCollection As New ParamValueList(Of Guid)
        Try
            If filtervalues.Contains("Campus") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("Campus"), DetailDictionary)
                If objPsFilter.Contains("UserId") Then
                    Dim UserValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("UserId"), ParamValueDictionary)
                    For Each di As DictionaryEntry In UserValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(UserValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)

                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                UserCollection = DirectCast(FModDictionary.Item("UserId"), ParamValueList(Of Guid))
                                Dim IsTheAdminUser As Boolean = False
                                If UserCollection.Count > 0 Then
                                    Dim username As String = GetUserName(UserCollection.Item(0))
                                    If Not username.ToLower = "sa" Or username.ToLower = "super" Or username.ToLower = "support" Then
                                        query = (From c In db.syCampus
                                                 Join cgc In db.syCmpGrpCmps
                                                 On c.CampusId Equals cgc.CampusId
                                                 Join r In db.syUsersRolesCampGrps
                                                 On cgc.CampGrpId Equals r.CampGrpId
                                                 Where r.UserId.Equals(UserCollection.Item(0))
                                                 Select c Distinct
                                                 Order By c.CampDescrip)
                                    End If
                                Else
                                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                    Throw New Exception(currentmethod & " has UserId as a required filter.")
                                End If
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & UserValueDictionary.Name)
                        End Select
                    Next
                Else
                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                    Throw New Exception(currentmethod & " has UserId as a required filter.")
                End If
                If objPsFilter.Contains("CampGrpId") Then
                    Dim CampGrpIdValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CampGrpId"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of syCampus, Boolean)) = PredicateBuilder.False(Of syCampus)()
                    For Each di As DictionaryEntry In CampGrpIdValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CampGrpIdValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary
                                    Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                    For Each campgrpid As Guid In mylist
                                        If Not campgrpid = GetAllCampusGroup.CampGrpId Then
                                            Dim CampusIds As New List(Of String)
                                            CampusIds = CreateCampIdListFromACampusGroup(campgrpid.ToString)
                                            For Each campusId As String In CampusIds
                                                Dim tempid As String = campusId
                                                predicate = predicate.Or(Function(c As syCampus) c.CampusId.Equals(tempid))
                                            Next
                                        End If
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CampGrpIdValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
                If objPsFilter.Contains("Status") Then
                    Dim StatusValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("Status"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of syCampus, Boolean)) = PredicateBuilder.False(Of syCampus)()

                    For Each di As DictionaryEntry In StatusValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(StatusValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary
                                    Dim mylist As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))
                                    For Each status As String In mylist
                                        Dim temp As String = status
                                        predicate = predicate.Or(Function(c As syCampus) c.syStatus.Status.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & StatusValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
                If objPsFilter.Contains("CurrentlySelected") Then
                    Dim CurrentlySelectedValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CurrentlySelected"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of syCampus, Boolean)) = PredicateBuilder.True(Of syCampus)()
                    For Each di As DictionaryEntry In CurrentlySelectedValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CurrentlySelectedValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary
                                    Dim mylist As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))
                                    For Each selected As String In mylist
                                        Dim temp As String = selected
                                        predicate = predicate.And(Function(c As syCampus) Not c.CampusId.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CurrentlySelectedValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
            End If

            Dim result As IQueryable(Of syCampus) = query.Where(basePredicate)


            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
