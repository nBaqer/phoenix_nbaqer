﻿Imports FAME.Advantage.Common.LINQ.Entities

Public Class TitleIvSapStatusDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub


    Public Function GetTitleIvStatuses( ByVal StatusTypes As List(Of String),Optional ByVal SelectedStatuses As List(Of String) = Nothing) As List(Of syTitleIVSapStatus)
        Dim query As IQueryable(Of syTitleIVSapStatus) =  From st In db.syTitleIVSapStatus
                Join s In db.syStatus On st.StatusId Equals s.StatusId
                where  Not SelectedStatuses.Contains(st.Id) AndAlso StatusTypes.Contains(s.Status) _
                Select st Distinct
                Order By st.Description

        Return query.ToList
    End Function
End Class
