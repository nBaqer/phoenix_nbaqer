﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Imports LinqKit
Imports System.Linq.Expressions
Imports FAME.Advantage.DataAccess.LINQ.Common
Imports FAME.Parameters.Collections

Public Class LeadGroupDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
      
    Private Function CreateCampGrpIdListByUserId(ByVal filtervalues As MasterDictionary) As List(Of String)
        Try
            If filtervalues.Contains("ProgramVersion") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("ProgramVersion"), DetailDictionary)
                If objPsFilter.Contains("UserId") Then
                    Dim UserCollection As New ParamValueList(Of Guid)
                    Dim UserValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("UserId"), ParamValueDictionary)
                    For Each di As DictionaryEntry In UserValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(UserValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)

                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                UserCollection = DirectCast(FModDictionary.Item("UserId"), ParamValueList(Of Guid))
                                Dim users(UserCollection.Count - 1) As String
                                Dim i As Integer = 0
                                For Each user As Guid In UserCollection
                                    users(i) = user.ToString
                                    i = i + 1
                                Next
                                Dim query As IQueryable(Of syUsersRolesCampGrp) = (From urcg In db.syUsersRolesCampGrps Where users.Contains(urcg.UserId.ToString))
                                Dim results As List(Of syUsersRolesCampGrp) = query.ToList
                                'Look for All Campus Group ID
                                Dim query2 As IQueryable(Of syUsersRolesCampGrp) = (From q In query Where q.CampGrpId.Equals(GetAllCampusGroup()))
                                Dim myids As New List(Of syUsersRolesCampGrp)
                                myids = query2.ToList
                                'Add the All Campus Group if it doesn't already exist

                                Dim campusGroupList = New List(Of string)
                                dim mainCampusIdSelect = DirectCast(objPsFilter.Item("MainCampusIdSelect") , ParamValueDictionary)
                                Dim campusId As String = ""
                                For Each cs As DictionaryEntry In mainCampusIdSelect
                                    Dim csModDictionary As ModDictionary = mainCampusIdSelect.Item(CType(cs.Key, ModDictionary.Modifier))
                                    If CType(cs.Key, ModDictionary.Modifier) = ModDictionary.Modifier.InList
                                        Dim mainCampusSelectCollection As New ParamValueList(Of String)
                                        mainCampusSelectCollection = DirectCast(csModDictionary.Item("MainCampusSelectId"), ParamValueList(Of String))
                                        For Each campus As String In mainCampusSelectCollection
                                            campusId = campus.ToString
                                        Next
                                    End If
                                Next
                                Dim campusgroupsQuery = (From cgc In db.syCmpGrpCmps
                                        Where cgc.CampusId.ToString() = campusId
                                        Select (cgc.CampGrpId.ToString())).ToList()

                                For Each item As string In campusgroupsQuery
                                    campusGroupList.Add(item)
                                Next


                                For Each item As syUsersRolesCampGrp In results
                                    campusGroupList.Add(item.CampGrpId.ToString)
                                Next

                                If myids.Count = 0 Then
                                    campusGroupList.Add(GetAllCampusGroup.CampGrpId.ToString)
                                End If
                                 Return campusGroupList
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & UserValueDictionary.Name)
                        End Select
                    Next
                Else
                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                    Throw New Exception(currentmethod & " has UserId as a required filter.")
                End If
            Else
                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                Throw New Exception(currentmethod & " missing required filter.")
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    Public Function GetLeadGroups(ByVal filtervalues As MasterDictionary) As IQueryable(Of adLeadGroup)
        Try
            Dim CampGrpIds As List(Of string) = CreateCampGrpIdListByUserId(filtervalues)
            Dim query As IQueryable(Of adLeadGroup) = (From lg In db.adLeadGroups
                                                       Join cg In db.syCampGrps
                                                       On lg.CampGrpId Equals cg.CampGrpId
                                                       Where CampGrpIds.Contains(cg.CampGrpId.ToString)
                                                       Select lg Distinct
                                                       Order By lg.Descrip)
            Dim basePredicate As Expression(Of Func(Of adLeadGroup, Boolean)) = PredicateBuilder.True(Of adLeadGroup)()
            basePredicate = basePredicate.And(Function(lg) True)

            If filtervalues.Contains("LeadGroup") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("LeadGroup"), DetailDictionary)
                If objPsFilter.Contains("CampGrpId") Then
                    Dim CampGrpIdValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CampGrpId"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of adLeadGroup, Boolean)) = PredicateBuilder.False(Of adLeadGroup)()
                    For Each di As DictionaryEntry In CampGrpIdValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CampGrpIdValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                    'Add the "All" CampGrpId - We shouldn't need to do this but we do...
                                    Dim InList As Boolean = False
                                    Dim AllCampusGroup As syCampGrp = GetAllCampusGroup()
                                    'Dim AllItem As Guid = New Guid("2AC97FC1-BB9B-450A-AA84-7C503C90A1EB")
                                    Dim AllItem As Guid = AllCampusGroup.CampGrpId
                                    For Each item As Guid In mylist
                                        If item = AllItem Then
                                            InList = True
                                        End If
                                    Next
                                    If InList = False Then
                                        mylist.Add(AllItem)
                                    End If
                                    For Each campgrpid As Guid In mylist
                                        Dim temp As String = campgrpid.ToString
                                        predicate = predicate.Or(Function(lg As adLeadGroup) lg.CampGrpId.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CampGrpIdValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
                If objPsFilter.Contains("Status") Then
                    Dim StatusValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("Status"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of adLeadGroup, Boolean)) = PredicateBuilder.False(Of adLeadGroup)()
                    For Each di As DictionaryEntry In StatusValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(StatusValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList

                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))
                                    For Each status As String In mylist
                                        Dim temp As String = status
                                        predicate = predicate.Or(Function(lg As adLeadGroup) lg.syStatus.Status.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & StatusValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
                If objPsFilter.Contains("CurrentlySelected") Then
                    Dim CurrentlySelectedValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CurrentlySelected"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of adLeadGroup, Boolean)) = PredicateBuilder.True(Of adLeadGroup)()
                    For Each di As DictionaryEntry In CurrentlySelectedValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CurrentlySelectedValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))
                                    For Each selected As String In mylist
                                        Dim temp As String = selected
                                        predicate = predicate.And(Function(lg As adLeadGroup) Not lg.LeadGrpId.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CurrentlySelectedValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
            End If

            Dim result As IQueryable(Of adLeadGroup) = query.Where(basePredicate)


            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function GetAllCampusGroup() As syCampGrp
        Try
            Dim query As syCampGrp = (From cg In db.syCampGrps
                                      Select cg Where cg.IsAllCampusGrp.Equals(True)).SingleOrDefault
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetStudentGroups(ByVal campusId As Guid) As List(Of GenericListItem)
        Dim query = (From lg In db.adLeadGroups
                     Join cg In db.syCmpGrpCmps
                     On cg.CampGrpId Equals lg.CampGrpId
                     Join stat In db.syStatus
                     On lg.StatusId Equals stat.StatusId
                     Where cg.CampusId = campusId And lg.UseForStudentGroupTracking.HasValue AndAlso lg.UseForStudentGroupTracking.Value AndAlso stat.Status = "Active"
                     Order By lg.Descrip
                     Select New GenericListItem With {.Id = lg.LeadGrpId.ToString(), .Description = lg.Descrip})

        Return query.Distinct().ToList()
    End Function
    Public Function GetLeadGroupsByCampus(ByVal campusId As Guid, Optional ByVal StatusTypes As List(Of String) = Nothing) As List(Of GenericListItem)
        If StatusTypes Is Nothing
            StatusTypes = New List(Of String)
            StatusTypes.Add("Active")
        End If
        Dim query = (From lg In db.adLeadGroups
                     Join cg In db.syCmpGrpCmps
                     On cg.CampGrpId Equals lg.CampGrpId
                     Join stat In db.syStatus
                     On lg.StatusId Equals stat.StatusId
                     Where cg.CampusId = campusId AndAlso StatusTypes.Contains(stat.Status)
                     Order By lg.Descrip
                     Select New GenericListItem With {.Id = lg.LeadGrpId.ToString(), .Description = lg.Descrip, .Status = lg.syStatus.Status})

        Return query.Distinct().ToList()
    End Function


    Public Function GetStudentGroupsByCampusId(ByVal campusId As Guid, ByVal StatusTypes As List(Of String), Optional ByVal  SelectedCampusGroups As List(Of String) = Nothing) As List(Of adLeadGroup)
        Try
            Dim i As Integer = 0
            Dim stats(StatusTypes.Count - 1) As String
            For Each item As String In StatusTypes
                stats(i) = item
                i = i + 1
            Next
            'reset count
            i = 0
            Dim selected(SelectedCampusGroups.Count - 1) As String
            For Each item As String In SelectedCampusGroups
                selected(i) = item
                i = i + 1
            Next

            Dim query As IQueryable(Of adLeadGroup) = (From lg In db.adLeadGroups
                    Join cg In db.syCmpGrpCmps
                    On cg.CampGrpId Equals lg.CampGrpId
                    Join stat In db.syStatus
                    On lg.StatusId Equals stat.StatusId _
                    Join s In db.syStatus _
                    On lg.StatusId Equals s.StatusId _
                    Where cg.CampusId.Equals(CampusId) _
                          AndAlso stats.Contains(s.Status) _
                          AndAlso Not selected.Contains(lg.LeadGrpId.ToString) _
                    Select lg Distinct _
                    Order By lg.Descrip)
            Return query.ToList

        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
