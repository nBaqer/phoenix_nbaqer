﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities
Imports LinqKit
Imports System.Linq.Expressions
Imports FAME.Parameters.Collections

Public Class SystemStatusCodeDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Private Function CreateCampGrpIdListByUserId(ByVal filtervalues As MasterDictionary) As String()
        Try
            If filtervalues.Contains("ProgramVersion") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("ProgramVersion"), DetailDictionary)
                If objPsFilter.Contains("UserId") Then
                    Dim UserCollection As New ParamValueList(Of Guid)
                    Dim UserValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("UserId"), ParamValueDictionary)
                    For Each di As DictionaryEntry In UserValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(UserValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)

                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                UserCollection = DirectCast(FModDictionary.Item("UserId"), ParamValueList(Of Guid))
                                Dim users(UserCollection.Count - 1) As String
                                Dim i As Integer = 0
                                For Each user As Guid In UserCollection
                                    users(i) = user.ToString
                                    i = i + 1
                                Next
                                Dim query As IQueryable(Of syUsersRolesCampGrp) = (From urcg In db.syUsersRolesCampGrps Where users.Contains(urcg.UserId.ToString))
                                Dim results As List(Of syUsersRolesCampGrp) = query.ToList
                                'Look for All Campus Group ID
                                Dim query2 As IQueryable(Of syUsersRolesCampGrp) = (From q In query Where q.CampGrpId.Equals(GetAllCampusGroup()))
                                Dim myids As New List(Of syUsersRolesCampGrp)
                                myids = query2.ToList
                                'Add the All Campus Group if it doesn't already exist
                                If myids.Count = 0 Then
                                    Dim CampGrpIds(results.Count) As String
                                    CampGrpIds(0) = GetAllCampusGroup.CampGrpId.ToString
                                    'restart counter at one since we added the All Campus Group
                                    i = 1
                                    For Each item As syUsersRolesCampGrp In results
                                        CampGrpIds(i) = item.CampGrpId.ToString
                                        i = i + 1
                                    Next
                                    Return CampGrpIds
                                Else
                                    Dim CampGrpIds(results.Count - 1) As String
                                    'restart counter
                                    i = 0
                                    For Each item As syUsersRolesCampGrp In results
                                        CampGrpIds(i) = item.CampGrpId.ToString
                                        i = i + 1
                                    Next
                                    Return CampGrpIds
                                End If
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & UserValueDictionary.Name)
                        End Select
                    Next
                Else
                    Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                    Throw New Exception(currentmethod & " has UserId as a required filter.")
                End If
            Else
                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                Throw New Exception(currentmethod & " missing required filter.")
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    Private Function CreateCampGrpIdListByUserId(ByVal userid As String) As String()
        Try
            Dim i As Integer = 0
            Dim query As IQueryable(Of syUsersRolesCampGrp) = (From urcg In db.syUsersRolesCampGrps Where urcg.UserId.ToString.Equals(userid))
            Dim results As List(Of syUsersRolesCampGrp) = query.ToList

            'Look for All Campus Group ID
            Dim query2 As IQueryable(Of syUsersRolesCampGrp) = (From q In query Where q.CampGrpId.Equals(GetAllCampusGroup()))
            Dim myids As New List(Of syUsersRolesCampGrp)
            myids = query2.ToList
            'Add the All Campus Group if it doesn't already exist
            If myids.Count = 0 Then
                Dim CampGrpIds(results.Count) As String
                CampGrpIds(0) = GetAllCampusGroup.CampGrpId.ToString
                'restart counter at one since we added the All Campus Group
                i = 1
                For Each item As syUsersRolesCampGrp In results
                    CampGrpIds(i) = item.CampGrpId.ToString
                    i = i + 1
                Next
                Return CampGrpIds
            Else
                Dim CampGrpIds(results.Count - 1) As String
                'restart counter
                i = 0
                For Each item As syUsersRolesCampGrp In results
                    CampGrpIds(i) = item.CampGrpId.ToString
                    i = i + 1
                Next
                Return CampGrpIds
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function
    'Private Function CreateUserIdList(ByVal filtervalues As MasterDictionary) As String()
    '    Try
    '        If filtervalues.Contains("SystemStatusCode") Then
    '            Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("SystemStatusCode"), DetailDictionary)
    '            If objPsFilter.Contains("UserId") Then
    '                Dim UserCollection As New ParamValueList(Of Guid)
    '                Dim UserValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("UserId"), ParamValueDictionary)
    '                For Each di As DictionaryEntry In UserValueDictionary
    '                    Dim FModDictionary As ModDictionary = DirectCast(UserValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)

    '                    Select Case CType(di.Key, ModDictionary.Modifier)
    '                        Case ModDictionary.Modifier.InList
    '                            UserCollection = DirectCast(FModDictionary.Item("UserId"), ParamValueList(Of Guid))
    '                            Dim users(UserCollection.Count - 1) As String
    '                            Dim i As Integer = 0
    '                            For Each user As Guid In UserCollection
    '                                users(i) = user.ToString
    '                                i = i + 1
    '                            Next
    '                            Return users
    '                        Case Else
    '                            Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
    '                            Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & UserValueDictionary.Name)
    '                    End Select
    '                Next
    '            Else
    '                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
    '                Throw New Exception(currentmethod & " has UserId as a required filter.")
    '            End If
    '        Else
    '            Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
    '            Throw New Exception(currentmethod & " missing required filter.")
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    '    Return Nothing
    'End Function
    Public Function GetEnrollmentSystemStatusCodes(ByVal filtervalues As MasterDictionary) As IQueryable(Of syStatusCode)
        Try
            Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(filtervalues)
            Dim query As IQueryable(Of syStatusCode) = (From c In db.syStatusCodes _
                                                        Join sy In db.sySysStatus _
                                                        On c.SysStatusId Equals sy.SysStatusId _
                                                        Join cg In db.syCampGrps _
                                                        On c.CampGrpId Equals cg.CampGrpId _
                                                        Where sy.StatusLevelId = 2 _
                                                        And CampGrpIds.Contains(c.CampGrpId.ToString)
                                                        Select c Distinct _
                                                        Order By c.SysStatusId, c.StatusCodeDescrip)
            Dim basePredicate As Expression(Of Func(Of syStatusCode, Boolean)) = PredicateBuilder.True(Of syStatusCode)()
            basePredicate = basePredicate.And(Function(c) True)

            If filtervalues.Contains("SystemStatusCode") Then
                Dim objPsFilter As DetailDictionary = DirectCast(filtervalues.Item("SystemStatusCode"), DetailDictionary)
                If objPsFilter.Contains("CampGrpId") Then
                    Dim CampGrpIdValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CampGrpId"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of syStatusCode, Boolean)) = PredicateBuilder.False(Of syStatusCode)()
                    For Each di As DictionaryEntry In CampGrpIdValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CampGrpIdValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of Guid) = CType(di2.Value, ParamValueList(Of Guid))
                                    'Add the "All" CampGrpId - We shouldn't need to do this but we do...
                                    Dim InList As Boolean = False
                                    Dim AllCampusGroup As syCampGrp = GetAllCampusGroup()
                                    'Dim AllItem As Guid = New Guid("2AC97FC1-BB9B-450A-AA84-7C503C90A1EB")
                                    Dim AllItem As Guid = AllCampusGroup.CampGrpId
                                    For Each item As Guid In mylist
                                        If item = AllItem Then
                                            InList = True
                                        End If
                                    Next
                                    If InList = False Then
                                        mylist.Add(AllItem)
                                    End If
                                    For Each campgrpid As Guid In mylist
                                        Dim temp As String = campgrpid.ToString
                                        predicate = predicate.Or(Function(c As syStatusCode) c.CampGrpId.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CampGrpIdValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
                If objPsFilter.Contains("Status") Then
                    Dim StatusValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("Status"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of syStatusCode, Boolean)) = PredicateBuilder.False(Of syStatusCode)()
                    For Each di As DictionaryEntry In StatusValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(StatusValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList

                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))
                                    For Each status As String In mylist
                                        Dim temp As String = status
                                        predicate = predicate.Or(Function(c As syStatusCode) c.syStatus.Status.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & StatusValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
                'If objPsFilter.Contains("StatusLevelId") Then
                '    Dim StatusValueDictionary As ParameterValueDictionary = DirectCast(objPsFilter.Item("StatusLevelId"), ParameterValueDictionary)
                '    Dim predicate As Expression(Of Func(Of syStatusCode, Boolean)) = PredicateBuilder.False(Of syStatusCode)()
                '    For Each di As DictionaryEntry In StatusValueDictionary
                '        Dim FModDictionary As FilterModDictionary = DirectCast(StatusValueDictionary.Item(CType(di.Key, FilterModDictionary.Modifier)), FilterModDictionary)
                '        Select Case CType(di.Key, FilterModDictionary.Modifier)
                '            Case FilterModDictionary.Modifier.InList

                '                For Each di2 As DictionaryEntry In FModDictionary

                '                    Dim mylist As ParameterValueList(Of String) = CType(di2.Value, ParameterValueList(Of String))
                '                    For Each status As String In mylist
                '                        Dim temp As String = status
                '                        predicate = predicate.Or(Function(c As syStatusCode) c.sySysStatus.SysStatusId.Equals(temp))
                '                    Next
                '                Next
                '            Case Else
                '                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                '                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & StatusValueDictionary.Name)
                '        End Select
                '    Next
                '    basePredicate = basePredicate.And(predicate)
                'End If
                If objPsFilter.Contains("CurrentlySelected") Then
                    Dim CurrentlySelectedValueDictionary As ParamValueDictionary = DirectCast(objPsFilter.Item("CurrentlySelected"), ParamValueDictionary)
                    Dim predicate As Expression(Of Func(Of syStatusCode, Boolean)) = PredicateBuilder.True(Of syStatusCode)()
                    For Each di As DictionaryEntry In CurrentlySelectedValueDictionary
                        Dim FModDictionary As ModDictionary = DirectCast(CurrentlySelectedValueDictionary.Item(CType(di.Key, ModDictionary.Modifier)), ModDictionary)
                        Select Case CType(di.Key, ModDictionary.Modifier)
                            Case ModDictionary.Modifier.InList
                                For Each di2 As DictionaryEntry In FModDictionary

                                    Dim mylist As ParamValueList(Of String) = CType(di2.Value, ParamValueList(Of String))
                                    For Each selected As String In mylist
                                        Dim temp As String = selected
                                        predicate = predicate.And(Function(c As syStatusCode) Not c.StatusCodeId.Equals(temp))
                                    Next
                                Next
                            Case Else
                                Dim currentmethod As String = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
                                Throw New Exception(currentmethod & " does not accept the " & di.Key.ToString & " modifier for " & CurrentlySelectedValueDictionary.Name)
                        End Select
                    Next
                    basePredicate = basePredicate.And(predicate)
                End If
            End If

            Dim result As IQueryable(Of syStatusCode) = query.Where(basePredicate)


            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetEnrollmentSystemStatusCodesByUser(ByVal userid As String) As IQueryable(Of syStatusCode)
        Try
            Dim CampGrpIds As String() = CreateCampGrpIdListByUserId(userid)
            Dim query As IQueryable(Of syStatusCode) = (From c In db.syStatusCodes _
                                                        Join sy In db.sySysStatus _
                                                        On c.SysStatusId Equals sy.SysStatusId _
                                                        Join cg In db.syCampGrps _
                                                        On c.CampGrpId Equals cg.CampGrpId _
                                                        Where sy.StatusLevelId = 2 _
                                                        And CampGrpIds.Contains(c.CampGrpId.ToString)
                                                        Select c Distinct _
                                                        Order By c.SysStatusId, c.StatusCodeDescrip)

            Dim result As IQueryable(Of syStatusCode) = query

            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetEnrollmentSystemStatusCodesByUser(ByVal userid As Guid, ByVal StatusTypes As List(Of String)) As List(Of syStatusCode)
        Try
           
            Dim enrollmentStatusesIds  = GetEnrollmentSystemStatusCodesByUser(userid.ToString())

            Dim results = enrollmentStatusesIds.Where(function(sc As syStatusCode) StatusTypes.Contains(sc.syStatus.Status)).ToList()

            Return results
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetEnrollmentSystemStatusCodesByUser(ByVal UserId As Guid, ByVal StatusTypes As List(Of String), ByVal SelectedEnrollmentStatuses As List(Of String)) As List(Of syStatusCode)
        Try
           
            Dim enrollmentStatusesIds  = GetEnrollmentSystemStatusCodesByUser(userid.ToString())

            Dim results = enrollmentStatusesIds.Where(function(sc As syStatusCode) StatusTypes.Contains(sc.syStatus.Status) AndAlso Not SelectedEnrollmentStatuses.Contains(sc.StatusCodeId.ToString())).ToList()

            Return results
        Catch ex As Exception
            Throw ex
        End Try


    End Function

    Private Function GetAllCampusGroup() As syCampGrp
        Try
            Dim query As syCampGrp = (From cg In db.syCampGrps _
                                      Select cg Where cg.IsAllCampusGrp.Equals(True)).SingleOrDefault
            Return query
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
