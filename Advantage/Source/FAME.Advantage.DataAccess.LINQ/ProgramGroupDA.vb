﻿Option Strict On
Imports FAME.Advantage.Common.LINQ.Entities

Public Class ProgramGroupDA
    Inherits LinqDataAccess
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub
    Public Function GetProgramGroups(ByVal StatusTypes As List(Of String)) As List(Of arPrgGrp)
        Dim i As Integer = 0
        Dim stats(StatusTypes.Count - 1) As String
        For Each item As String In StatusTypes
            stats(i) = item
            i = i + 1
        Next
        Try

            Dim query As IQueryable(Of arPrgGrp) = (From pg In db.arPrgGrps _
                                                     Join s In db.syStatus _
                                                     On pg.StatusId Equals s.StatusId _
                                                     Where stats.Contains(s.Status) _
                                                     Select pg Distinct _
                                                     Order By pg.PrgGrpDescrip)
            Return query.ToList


        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetProgramGroups(ByVal StatusTypes As List(Of String), ByVal SelectedAreasOfInterest As List(Of String)) As List(Of arPrgGrp)
        Dim i As Integer = 0
        Dim stats(StatusTypes.Count - 1) As String
        For Each item As String In StatusTypes
            stats(i) = item
            i = i + 1
        Next
        'reset count
        i = 0
        Dim selected(SelectedAreasOfInterest.Count - 1) As String
        For Each item As String In SelectedAreasOfInterest
            selected(i) = item
            i = i + 1
        Next
        Try

            Dim query As IQueryable(Of arPrgGrp) = (From pg In db.arPrgGrps _
                                                     Join s In db.syStatus _
                                                     On pg.StatusId Equals s.StatusId _
                                                     Where stats.Contains(s.Status) _
                                                     AndAlso Not selected.Contains(pg.PrgGrpId.ToString) _
                                                     Select pg Distinct _
                                                     Order By pg.PrgGrpDescrip)
            Return query.ToList


        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
