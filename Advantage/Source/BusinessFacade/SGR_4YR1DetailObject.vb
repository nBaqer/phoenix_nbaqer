Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.AdvantageV1.BusinessFacade.SGR_CommonObject

Public Class SGR_4YR1DetailObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SGR_4YR1Detail"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet
	Private RptParamInfo As ReportParamInfoIPEDS

	Private DateFilter As String
	Private dvStuEnrollInfo As DataView

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As Common.ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SGR_4YR1DetailObjectDB.GetReportDatasetRaw(RptParamInfo)

		' save passed-in report parameter info 
		Me.RptParamInfo = RptParamInfo

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		Dim dtGenderInfo As DataTable = IPEDSFacade.GetIPEDSGenderInfo
		Dim drGenderInfo As DataRow
		Dim dtEthnicInfo As DataTable = IPEDSFacade.GetIPEDSEthnicInfo
		Dim drEthnicInfo As DataRow

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add(RptStuIdentifierColName, GetType(String))
			.Add("Name", GetType(String))
			.Add("GenderDescrip", GetType(String))
			.Add("EthCodeDescrip", GetType(String))
			.Add("Ind_RevCohort", GetType(String))
			.Add("Ind_Bachelors", GetType(String))
		End With

		' create student Enrollments DataView to be used when processing records
		dvStuEnrollInfo = New DataView(IPEDSFacade.GetProcessedEnrollInfo(dsRaw))

		' set up date filter to be used when processing records
		DateFilter = EnrollDateFilter(RptParamInfo)

		' loop thru genders, and ethnic groups for each gender, and process matching records in raw dataset
		For Each drGenderInfo In dtGenderInfo.Rows
			For Each drEthnicInfo In dtEthnicInfo.Rows
				ProcessRows(drGenderInfo("GenderDescrip"), drEthnicInfo("EthCodeDescrip"))
			Next
		Next

		' return the Report DataSet
		Return dsRpt
	End Function

	Private Sub ProcessRows(ByVal GenderDescrip As String, ByVal EthCodeDescrip As String)
		Dim i As Integer
		Dim drRpt As DataRow
		Dim dvStudents As DataView

		' create dataview from students in raw dataset, filtered according to passed in gender and ethnic ids
		dvStudents = New DataView(dsRaw.Tables(TblNameStudents), _
								  "GenderDescrip LIKE '" & GenderDescrip & "' AND EthCodeDescrip LIKE '" & EthCodeDescrip & "'", _
								  "", DataViewRowState.CurrentRows)

		If dvStudents.Count > 0 Then
			For i = 0 To dvStudents.Count - 1
				drRpt = dsRpt.Tables(MainTableName).NewRow

				' set student identifier and name
				IPEDSFacade.SetStudentInfo(dvStudents(i), drRpt)

				' set student Gender and Ethnic Code descriptions
				drRpt("GenderDescrip") = GenderDescrip
				drRpt("EthCodeDescrip") = EthCodeDescrip

				' indicate student is in Revised Cohort 
				drRpt("Ind_RevCohort") = "X"

				' indicate if student was Bachelor's or equivalent
				drRpt("Ind_Bachelors") = DBNull.Value
				dvStuEnrollInfo.RowFilter = "StudentId = '" & dvStudents(i)("StudentId").ToString & "'"
				If (dvStuEnrollInfo(0)("DegCertSeekingDescrip").ToString = DegCertSeekingOtherDegree Or _
					dvStuEnrollInfo(0)("DegCertSeekingDescrip").ToString.StartsWith(DegCertSeekingFirstTime)) And _
				   dvStuEnrollInfo(0)("ProgTypeDescrip") = ProgTypeUndergraduate Then
					drRpt("Ind_Bachelors") = "X"
				End If

				' add new row to report
				dsRpt.Tables(MainTableName).Rows.Add(drRpt)
			Next

		Else
			' if no matching records, add blank row to report, with Gender and Ethnic Codes, 
			'	NULL data otherwise
			drRpt = dsRpt.Tables(MainTableName).NewRow

			drRpt(RptStuIdentifierColName) = DBNull.Value
			drRpt("Name") = DBNull.Value
			drRpt("GenderDescrip") = GenderDescrip
			drRpt("EthCodeDescrip") = EthCodeDescrip
			drRpt("Ind_RevCohort") = DBNull.Value
			drRpt("Ind_Bachelors") = DBNull.Value

			' add empty report row to report 
			dsRpt.Tables(MainTableName).Rows.Add(drRpt)
		End If

	End Sub

End Class