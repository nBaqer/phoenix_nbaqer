Public Class LeadAdDetailObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim leadList As New LeadMasterDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(leadList.GetLeadAdvertisementDetail(rptParamInfo))
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    ' Copy data from dataset to datatable.
    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        Try
            If ds.Tables.Count = 1 Then
                Dim strName As String = ""
                Dim dtLeads As DataTable = ds.Tables(0)

                For Each dr As DataRow In dtLeads.Rows
                    dr("RecCount") = dtLeads.Rows.Count
                    '
                    If dr.IsNull("SourceCategoryId") Then
                        dr("Category") = " None"
                    End If
                    If dr.IsNull("SourceTypeId") Then
                        dr("Source") = " None"
                    End If
                    If dr.IsNull("SourceAdvertisement") Then
                        dr("Advertisement") = " None"
                    End If
                    '
                    strName = ""
                    '
                    'Only Lead's MiddleName allows NULL.
                    If Not (dr.IsNull("MiddleName")) Then
                        If dr("MiddleName") <> "" Then
                            dr("LeadName") = dr("LastName") & ", " & dr("FirstName") & _
                                                " " & dr("MiddleName") & "."
                        Else
                            dr("LeadName") = dr("LastName") & ", " & dr("FirstName")
                        End If
                    Else
                        dr("LeadName") = dr("LastName") & ", " & dr("FirstName")
                    End If
                    '
                    Dim inqDt As Date
                    Dim expStartDt As Date
                    '
                    'Compute Elapsed Time only if BOTH Inquiry Date and Expected Start Date are not NULL
                    If Not (dr.IsNull("InquiryDate")) And Not (dr.IsNull("ExpectedStart")) Then
                        inqDt = Date.Parse(dr("InquiryDate"))
                        expStartDt = Date.Parse(dr("ExpectedStart"))
                        dr("ElapsedTime") = expStartDt.Subtract(inqDt).Days
                    End If
                Next
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
