﻿Imports System
Imports System.IO
Imports System.Net
Imports System.Data
Imports System.Text
Imports System.Data.OleDb
Imports FAME.AdvantageV1.DataAccess
Public Class EDFacade_New
    Dim dtLogMessage, dtPATSData, dtPATSDisb, dtDirectLoan, dtDLSchDisb, dtDLActualDisb As New DataTable()
    Public Function ProcessEDFile(ByRef clsflFile As EDFileInfo_New, ByRef myMsgCollection As EDCollectionMessageInfos_New, Optional ByVal SourceFolderLocation As String = "", Optional ByVal AcademicYearId As String = "", Optional ByVal AwardStartDate As String = "", Optional ByVal AwardEndDate As String = "", Optional ByVal IsOverrideAdvDateWithEDExpDate As Boolean = False, Optional ByVal IsOverrideAdvAmtWithEDExpAmt As Boolean = False, Optional ByVal IsPostPayment As Boolean = False, Optional ByVal IsPayOutOfSchoolStudent As Boolean = False, Optional ByVal IsUseExpDate As Boolean = False, Optional ByVal SpecifiedDate As String = "") As Boolean
        Dim strRecord As String = ""
        Dim intLoopCount As Integer = 0
        Dim intBytesRead As Integer = 0
        Dim Request As WebRequest
        Dim ResponseStream As Stream
        Dim Response1 As WebResponse
        Dim myCredentials As NetworkCredential
        ProcessEDFile = False
        Try
            If clsflFile.datafromremotecomputer = True Then
                myCredentials = New NetworkCredential(clsflFile.networkuser, clsflFile.networkPassword, "")
            End If
            Request = System.Net.FileWebRequest.Create(clsflFile.strFileName) 'WebRequest.Create(strFileName) 'System.Net.FileWebRequest.Create("\\qaw2k\ESPFileIn\CLFAHEAD.000") 
            Request.Headers.Add("Translate: f")
            Request.Method = "GET"
            If clsflFile.datafromremotecomputer = True Then
                Request.Credentials = myCredentials
            Else
                Request.Credentials = CredentialCache.DefaultCredentials
            End If

            Response1 = CType(Request.GetResponse, WebResponse)
            ResponseStream = Response1.GetResponseStream
            Dim objIn As StreamReader
            objIn = New StreamReader(ResponseStream, System.Text.Encoding.Default)
            Dim contents As String
            'Dim intLoop As Integer = 1
            Do
                Try
                    contents = objIn.ReadLine().Trim()
                Catch ex As Exception
                    contents = Nothing
                    Exit Do
                End Try
                If Not contents + "" = "" Then
                    strRecord = contents
                    myMsgCollection.Add(clsflFile.strMsgType, strRecord, clsflFile.strFileName, AcademicYearId, AwardStartDate, AwardEndDate, IsOverrideAdvDateWithEDExpDate, IsOverrideAdvAmtWithEDExpAmt, IsPostPayment, IsPayOutOfSchoolStudent, IsUseExpDate, SpecifiedDate)
                End If
            Loop Until contents Is Nothing
            objIn.Close()
            'End If

            ''ResponseStream.Flush()
            ResponseStream.Close()
            Response1.Close()
            ProcessEDFile = True
        Catch e As ArgumentNullException
            ProcessEDFile = False
            ''Throw
        Catch e As ArgumentOutOfRangeException
            ProcessEDFile = False
            ''Throw
        Catch e As System.Exception
            ProcessEDFile = False
            ''Throw
        End Try

    End Function

    Public Function ProcessExceptionEDFile(ByRef clsflFile As EDFileInfo_New, ByRef myMsgCollection As EDCollectionMessageInfos_New, ByVal FileName As String, ByRef AcademicYearId As String, ByRef AwardStartDate As String, ByRef AwardEndDate As String, Optional ByVal SourceFolderLocation As String = "") As Boolean
        Dim strRecord As String = ""
        Dim intLoopCount As Integer = 0
        Dim intBytesRead As Integer = 0

        Dim db As New DataAccess.DataAccess
        Dim sbParent As New StringBuilder
        Dim sbChild As New StringBuilder
        Dim dsParent As DataSet
        Dim dsChild As DataSet

        ProcessExceptionEDFile = False
        Try
            If clsflFile.strMsgType.ToUpper = "PELL" Then '' For Pell, ACG, SMART and Teach
                With sbParent
                    .Append(" Select ExceptionReportId, DbIn, Filter, AwardAmount, AwardId, GrantType, AddDate, AddTime, OriginalSSN, UpdateDate, UpdateTime, OriginationStatus, AcademicYearId, AwardYearStartDate, AwardYearEndDate, ErrorMsg ")
                    .Append(" From syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable Where FileName='" & FileName & "'")
                End With
                Try
                    dsParent = db.RunSQLDataSet(sbParent.ToString)
                    sbParent = sbParent.Remove(0, sbParent.Length)
                    If dsParent.Tables(0).Rows.Count > 0 Then
                        For Each drP As DataRow In dsParent.Tables(0).Rows
                            If drP("DbIn").ToString = "T" Then
                                strRecord = drP("DbIn").ToString & "," & drP("Filter").ToString & "," & drP("AwardAmount").ToString & "," & drP("AwardId").ToString & "," & drP("GrantType").ToString & "," & drP("OriginationStatus").ToString & "," & drP("AddDate").ToString & "," & drP("AddTime").ToString & "," & drP("OriginalSSN").ToString & "," & drP("UpdateDate").ToString & "," & drP("UpdateTime").ToString
                            Else
                                strRecord = drP("DbIn").ToString & "," & drP("Filter").ToString & "," & drP("AwardAmount").ToString & "," & drP("AwardId").ToString & "," & drP("OriginationStatus").ToString & "," & drP("AddDate").ToString & "," & drP("AddTime").ToString & "," & drP("OriginalSSN").ToString & "," & drP("UpdateDate").ToString & "," & drP("UpdateTime").ToString
                            End If

                            AcademicYearId = drP("AcademicYearId").ToString
                            AwardStartDate = drP("AwardYearStartDate").ToString
                            AwardEndDate = drP("AwardYearEndDate").ToString
                            If Not strRecord + "" = "" Then
                                myMsgCollection.Add(clsflFile.strMsgType, strRecord, clsflFile.strFileName)
                                myMsgCollection.Items(myMsgCollection.Count - 1).strError = drP("ErrorMsg").ToString
                                myMsgCollection.Items(myMsgCollection.Count - 1).strAwardYearStartDate = drP("AwardYearStartDate").ToString
                                myMsgCollection.Items(myMsgCollection.Count - 1).strAwardYearEndDate = drP("AwardYearEndDate").ToString
                                myMsgCollection.Items(myMsgCollection.Count - 1).strAcademicYearId = drP("AcademicYearId").ToString
                            End If

                            With sbChild
                                .Append(" Select DbIn, Filter, AccDisbAmount, DisbDate, DisbNum, DisbRelIndi, DusbSeqNum, SimittedDisbAmount, ActionStatusDisb, ErrorMsg ")
                                .Append(" From syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable Where FileName='" & FileName & "' and ExceptionReportId='" & drP("ExceptionReportId").ToString & "' order by DisbNum")
                            End With
                            dsChild = db.RunSQLDataSet(sbChild.ToString)
                            For Each drC As DataRow In dsChild.Tables(0).Rows
                                If drP("DbIn").ToString = "T" Then
                                    strRecord = drC("DbIn").ToString & "," & drC("Filter").ToString & "," & drC("AccDisbAmount").ToString & "," & drC("ActionStatusDisb").ToString & "," & drC("DisbDate").ToString & "," & drC("DisbNum").ToString & "," & drC("DisbRelIndi").ToString & "," & drC("DusbSeqNum").ToString & "," & drC("SimittedDisbAmount").ToString
                                Else
                                    strRecord = drC("DbIn").ToString & "," & drC("Filter").ToString & "," & drC("AccDisbAmount").ToString & "," & drC("DisbDate").ToString & "," & drC("DisbNum").ToString & "," & drC("DisbRelIndi").ToString & "," & drC("DusbSeqNum").ToString & "," & drC("SimittedDisbAmount").ToString & "," & drC("ActionStatusDisb").ToString
                                End If

                                If Not strRecord + "" = "" Then
                                    myMsgCollection.Add(clsflFile.strMsgType, strRecord, clsflFile.strFileName)
                                    myMsgCollection.Items(myMsgCollection.Count - 1).strError = drC("ErrorMsg").ToString
                                    myMsgCollection.Items(myMsgCollection.Count - 1).strAwardYearStartDate = drP("AwardYearStartDate").ToString
                                    myMsgCollection.Items(myMsgCollection.Count - 1).strAwardYearEndDate = drP("AwardYearEndDate").ToString
                                    myMsgCollection.Items(myMsgCollection.Count - 1).strAcademicYearId = drP("AcademicYearId").ToString
                                End If
                            Next
                            sbChild = sbChild.Remove(0, sbChild.Length)
                        Next
                    Else
                        ProcessEDFile(clsflFile, myMsgCollection, SourceFolderLocation)
                    End If
                    ProcessExceptionEDFile = True
                Catch ex As Exception

                End Try
            Else  '' For Direct Loan
                With sbParent
                    .Append(" Select ExceptionReportId, DbIn, Filter, AddDate, AddTime, LoanAmtApproved, LoanFeePer, LoanId, LoanPeriodEndDate, LoanPeriodStartDate, LoanStatus, LoanType, MPNStatus, OriginalSSN, UpdateDate, UpdateTime, AcademicYearId, ErrorMsg ")
                    .Append(" From syEDExpressExceptionReportDirectLoan_StudentTable Where FileName='" & FileName & "'")
                End With
                Try
                    dsParent = db.RunSQLDataSet(sbParent.ToString)
                    sbParent = sbParent.Remove(0, sbParent.Length)
                    If dsParent.Tables(0).Rows.Count > 0 Then
                        For Each drP As DataRow In dsParent.Tables(0).Rows
                            strRecord = drP("DbIn").ToString & "," & drP("Filter").ToString & "," & drP("AddDate").ToString & "," & drP("AddTime").ToString & "," & drP("LoanAmtApproved").ToString & "," & drP("LoanFeePer").ToString & "," & drP("LoanId").ToString & "," & drP("LoanPeriodEndDate").ToString & "," & drP("LoanPeriodStartDate").ToString & "," & drP("LoanStatus").ToString & "," & drP("LoanType").ToString & "," & drP("MPNStatus").ToString & "," & drP("OriginalSSN").ToString & "," & drP("UpdateDate").ToString & "," & drP("UpdateTime").ToString
                            AcademicYearId = drP("AcademicYearId").ToString
                            If Not strRecord + "" = "" Then
                                myMsgCollection.Add(clsflFile.strMsgType, strRecord, clsflFile.strFileName)
                                myMsgCollection.Items(myMsgCollection.Count - 1).strError = drP("ErrorMsg").ToString
                            End If
                            With sbChild
                                .Append(" Select DbIn, Filter, DisbDate, DisbGrossAmount, DisbIntRebAmount, DisbLoanFeeAmount, DisbNetAdjAmount, DisbNetAmount, DisbNum, DisbSeqNum, DisbStatus_RelInd, DisbType, LoanId, ErrorMsg  ")
                                .Append(" From syEDExpressExceptionReportDirectLoan_DisbursementTable Where FileName='" & FileName & "' and ExceptionReportId='" & drP("ExceptionReportId").ToString & "' order by DisbNum")
                            End With
                            dsChild = db.RunSQLDataSet(sbChild.ToString)
                            For Each drC As DataRow In dsChild.Tables(0).Rows
                                If drC("DbIn").ToString = "M" Then
                                    strRecord = drC("DbIn").ToString & "," & drC("Filter").ToString & "," & drC("DisbDate").ToString & "," & drC("DisbGrossAmount").ToString & "," & drC("DisbIntRebAmount").ToString & "," & drC("DisbLoanFeeAmount").ToString & "," & drC("DisbNetAdjAmount").ToString & "," & drC("DisbNetAmount").ToString & "," & drC("DisbNum").ToString & "," & drC("DisbSeqNum").ToString & "," & drC("DisbStatus_RelInd").ToString & "," & drC("DisbType").ToString & "," & drC("LoanId").ToString
                                Else
                                    strRecord = drC("DbIn").ToString & "," & drC("Filter").ToString & "," & drC("DisbDate").ToString & "," & drC("DisbLoanFeeAmount").ToString & "," & drC("DisbGrossAmount").ToString & "," & drC("DisbIntRebAmount").ToString & "," & drC("LoanId").ToString & "," & drC("DisbNetAmount").ToString & "," & drC("DisbNum").ToString & "," & drC("DisbStatus_RelInd").ToString
                                End If

                                If Not strRecord + "" = "" Then
                                    myMsgCollection.Add(clsflFile.strMsgType, strRecord, clsflFile.strFileName)
                                    myMsgCollection.Items(myMsgCollection.Count - 1).strError = drC("ErrorMsg").ToString
                                End If
                            Next
                            sbChild = sbChild.Remove(0, sbChild.Length)
                        Next
                    Else
                        ProcessEDFile(clsflFile, myMsgCollection, SourceFolderLocation)
                    End If
                    ProcessExceptionEDFile = True
                Catch ex As Exception

                End Try
            End If

        Catch e As ArgumentNullException
            Throw
        Catch e As ArgumentOutOfRangeException
            Throw
        Catch e As System.Exception
            Throw
        End Try

    End Function
    Public Function ProcessExceptionEDFileNew(ByRef clsflFile As EDFileInfo_New, ByVal FileName As String, ByRef AcademicYearId As String, ByRef AwardStartDate As String, ByRef AwardEndDate As String, ByRef dtParent As DataTable, ByRef dtChild_PD_SD As DataTable, Optional ByRef dtChild_AD As DataTable = Nothing, Optional ByVal SourceFolderLocation As String = "") As Boolean
        Dim strRecord As String = ""
        Dim intLoopCount As Integer = 0
        Dim intBytesRead As Integer = 0
        Dim EDDX As New EDDataXfer_New


        Dim db As New DataAccess.DataAccess
        Dim sbParent As New StringBuilder
        Dim sbChild As New StringBuilder
        Dim dsParent As DataSet
        Dim dsChild As DataSet
        Dim intParentIndex As Integer = 0
        Dim intIndex As Integer = 0


        ProcessExceptionEDFileNew = False
        Try
            If clsflFile.strMsgType.ToUpper = "PELL" Then '' For Pell, ACG, SMART and Teach
                With sbParent
                    .Append(" Select ExceptionReportId, DbIn, Filter, AwardAmount, AwardId, GrantType, AddDate, AddTime, OriginalSSN, UpdateDate, UpdateTime, OriginationStatus, AcademicYearId, AwardYearStartDate, AwardYearEndDate, ErrorMsg ")
                    .Append(" From syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable Where FileName='" & FileName & "'")
                End With
                Try
                    dsParent = db.RunSQLDataSet(sbParent.ToString)
                    sbParent = sbParent.Remove(0, sbParent.Length)
                    If dsParent.Tables(0).Rows.Count > 0 Then
                        For Each drP As DataRow In dsParent.Tables(0).Rows
                            If AcademicYearId = "" Or AwardStartDate = "" Or AwardEndDate = "" Then
                                AcademicYearId = drP("AcademicYearId").ToString
                                AwardStartDate = drP("AwardYearStartDate").ToString
                                AwardEndDate = drP("AwardYearEndDate").ToString
                            End If
                            'Dim SSNOriginal As String
                            'Dim AwardId As String
                            Dim StuEnrollmentId As String
                            Dim CampusId As String
                            Dim CampusName As String
                            Dim FirstName As String
                            Dim LastName As String
                            Dim IsInSchool As Boolean

                            Dim strGrantTypeDesc As String = ""
                            Dim strGrantType As String = ""
                            strGrantType = drP("GrantType").ToString
                            If strGrantType = "" Or strGrantType = "P" Then
                                strGrantTypeDesc = "PELL"
                            ElseIf strGrantType = "A" Then
                                strGrantTypeDesc = "ACG"
                            ElseIf strGrantType = "S" Or strGrantType = "T" Then
                                strGrantTypeDesc = "SMART"
                            End If

                            '' Code changes by Kamalesh ahuja on 5th December 2010 to Pass Loan/AwardId to GetStudentInfo Function to check if the Award/ Loan id allready exists in the database
                            '' then return the corresponding enrollment id. This is to avoid conflict when the student has 2 enrollments with the same status and start date
                            ''EDDX.GetStudentInfo(db, drP("OriginalSSN").ToString, FirstName, LastName, StuEnrollmentId, CampusId, CampusName, IsInSchool)
                            EDDX.GetStudentInfo(db, drP("OriginalSSN").ToString, FirstName, LastName, StuEnrollmentId, CampusId, CampusName, IsInSchool, drP("AwardId").ToString)

                            Dim drPP As DataRow
                            Dim dbAwardAmt As Decimal
                            Try
                                dbAwardAmt = Convert.ToDecimal(drP("AwardAmount").ToString)
                            Catch ex As Exception
                                dbAwardAmt = 0.0
                            End Try

                            drPP = dtParent.NewRow()
                            drPP("IndexID") = intIndex
                            drPP("LastName") = LastName
                            drPP("FirstName") = FirstName
                            drPP("SSN") = IIf(drP("OriginalSSN").ToString.Trim().Length = 9, FormatSSN(drP("OriginalSSN").ToString.Trim()), drP("OriginalSSN").ToString.Trim())
                            drPP("Campus") = CampusName
                            drPP("CampusId") = CampusId
                            drPP("StuEnrollmentId") = StuEnrollmentId
                            drPP("GrantType") = strGrantType
                            drPP("GrantTypeDesc") = strGrantTypeDesc
                            drPP("AwardId") = drP("AwardId").ToString
                            drPP("AddDate") = drP("AddDate").ToString
                            drPP("AddTime") = drP("AddTime").ToString
                            drPP("AwardAmount") = dbAwardAmt.ToString("0.00")
                            drPP("AcademicYearId") = AcademicYearId
                            drPP("AwardStartDate") = AwardStartDate
                            drPP("AwardEndDate") = AwardEndDate
                            drPP("ErrorType") = 0
                            drPP("dbIndicator") = drP("DbIn").ToString
                            drPP("dbFilter") = drP("Filter").ToString
                            drPP("OriginationStatus") = drP("OriginationStatus").ToString
                            drPP("SSNOriginal") = drP("OriginalSSN").ToString
                            drPP("UpdatedDate") = drP("UpdateDate").ToString
                            drPP("UpdatedTime") = drP("UpdateTime").ToString
                            drPP("IsParsed") = 1
                            drPP("IsInSchool") = IsInSchool
                            intParentIndex = intIndex
                            intIndex += 1
                            dtParent.Rows.Add(drPP)
                            With sbChild
                                .Append(" Select DbIn, Filter, AccDisbAmount, DisbDate, DisbNum, DisbRelIndi, DusbSeqNum, SimittedDisbAmount, ActionStatusDisb, ErrorMsg ")
                                .Append(" From syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable Where FileName='" & FileName & "' and ExceptionReportId='" & drP("ExceptionReportId").ToString & "' order by DisbNum")
                            End With
                            dsChild = db.RunSQLDataSet(sbChild.ToString)
                            For Each drC As DataRow In dsChild.Tables(0).Rows
                                Dim drPC As DataRow
                                drPC = dtChild_PD_SD.NewRow()

                                Dim dbSubDisbAmt As Decimal
                                Try
                                    dbSubDisbAmt = Convert.ToDecimal(drC("SimittedDisbAmount").ToString)
                                Catch ex As Exception
                                    dbSubDisbAmt = 0.0
                                End Try
                                Dim dbAccDisbAmt As Decimal
                                Try
                                    dbAccDisbAmt = Convert.ToDecimal(drC("AccDisbAmount").ToString)
                                Catch ex As Exception
                                    dbAccDisbAmt = 0.0
                                End Try

                                drPC("IndexID") = intIndex
                                drPC("ParentIndexID") = intParentIndex
                                drPC("LastName") = LastName
                                drPC("FirstName") = FirstName
                                drPC("SSN") = IIf(drP("OriginalSSN").ToString.Trim().Length = 9, FormatSSN(drP("OriginalSSN").ToString.Trim()), drP("OriginalSSN").ToString.Trim())
                                drPC("SSNOriginal") = drP("OriginalSSN").ToString
                                drPC("StuEnrollmentId") = StuEnrollmentId
                                drPC("CampusId") = CampusId
                                drPC("Campus") = CampusName
                                drPC("AwardId") = drP("AwardId").ToString
                                drPC("DisbDate") = drC("DisbDate").ToString
                                drPC("DisbRelInd") = drC("DisbRelIndi").ToString
                                drPC("ActionStatus") = drC("ActionStatusDisb").ToString
                                drPC("DisbNum") = drC("DisbNum").ToString
                                drPC("SeqNum") = drC("DusbSeqNum").ToString
                                drPC("SubDisbAmt") = dbSubDisbAmt.ToString("0.00")
                                drPC("AccDisbAmt") = dbAccDisbAmt.ToString("0.00")
                                drPC("dbIndicator") = drC("DbIn").ToString
                                drPC("AcademicYrId") = AcademicYearId
                                drPC("AwardYrStartDate") = AwardStartDate
                                drPC("AwardYrEndDate") = AwardEndDate
                                drPC("ErrorType") = 0
                                drPC("Show") = 0
                                drPC("IsInSchool") = IsInSchool
                                drPC("IsParsed") = 1
                                drPC("dbFilter") = drC("Filter").ToString

                                dtChild_PD_SD.Rows.Add(drPC)
                                intIndex += 1

                            Next
                            sbChild = sbChild.Remove(0, sbChild.Length)
                        Next
                    End If
                    ProcessExceptionEDFileNew = True
                Catch ex As Exception

                End Try
            Else  '' For Direct Loan
                ''With sbParent
                ''    .Append(" Select ExceptionReportId, DbIn, Filter, AddDate, AddTime, LoanAmtApproved, LoanFeePer, LoanId, LoanPeriodEndDate, LoanPeriodStartDate, LoanStatus, LoanType, MPNStatus, OriginalSSN, UpdateDate, UpdateTime, AcademicYearId, ErrorMsg ")
                ''    .Append(" From syEDExpressExceptionReportDirectLoan_StudentTable Where FileName='" & FileName & "'")
                ''End With
                ''Try
                ''    dsParent = db.RunSQLDataSet(sbParent.ToString)
                ''    sbParent = sbParent.Remove(0, sbParent.Length)
                ''    If dsParent.Tables(0).Rows.Count > 0 Then
                ''        For Each drP As DataRow In dsParent.Tables(0).Rows
                ''            strRecord = drP("DbIn").ToString & "," & drP("Filter").ToString & "," & drP("AddDate").ToString & "," & drP("AddTime").ToString & "," & drP("LoanAmtApproved").ToString & "," & drP("LoanFeePer").ToString & "," & drP("LoanId").ToString & "," & drP("LoanPeriodEndDate").ToString & "," & drP("LoanPeriodStartDate").ToString & "," & drP("LoanStatus").ToString & "," & drP("LoanType").ToString & "," & drP("MPNStatus").ToString & "," & drP("OriginalSSN").ToString & "," & drP("UpdateDate").ToString & "," & drP("UpdateTime").ToString
                ''            AcademicYearId = drP("AcademicYearId").ToString
                ''            If Not strRecord + "" = "" Then
                ''                myMsgCollection.Add(clsflFile.strMsgType, strRecord, clsflFile.strFileName)
                ''                myMsgCollection.Items(myMsgCollection.Count - 1).strError = drP("ErrorMsg").ToString
                ''            End If
                ''            With sbChild
                ''                .Append(" Select DbIn, Filter, DisbDate, DisbGrossAmount, DisbIntRebAmount, DisbLoanFeeAmount, DisbNetAdjAmount, DisbNetAmount, DisbNum, DisbSeqNum, DisbStatus_RelInd, DisbType, LoanId, ErrorMsg  ")
                ''                .Append(" From syEDExpressExceptionReportDirectLoan_DisbursementTable Where FileName='" & FileName & "' and ExceptionReportId='" & drP("ExceptionReportId").ToString & "' order by DisbNum")
                ''            End With
                ''            dsChild = db.RunSQLDataSet(sbChild.ToString)
                ''            For Each drC As DataRow In dsChild.Tables(0).Rows
                ''                If drC("DbIn").ToString = "M" Then
                ''                    strRecord = drC("DbIn").ToString & "," & drC("Filter").ToString & "," & drC("DisbDate").ToString & "," & drC("DisbGrossAmount").ToString & "," & drC("DisbIntRebAmount").ToString & "," & drC("DisbLoanFeeAmount").ToString & "," & drC("DisbNetAdjAmount").ToString & "," & drC("DisbNetAmount").ToString & "," & drC("DisbNum").ToString & "," & drC("DisbSeqNum").ToString & "," & drC("DisbStatus_RelInd").ToString & "," & drC("DisbType").ToString & "," & drC("LoanId").ToString
                ''                Else
                ''                    strRecord = drC("DbIn").ToString & "," & drC("Filter").ToString & "," & drC("DisbDate").ToString & "," & drC("DisbLoanFeeAmount").ToString & "," & drC("DisbGrossAmount").ToString & "," & drC("DisbIntRebAmount").ToString & "," & drC("LoanId").ToString & "," & drC("DisbNetAmount").ToString & "," & drC("DisbNum").ToString & "," & drC("DisbStatus_RelInd").ToString
                ''                End If

                ''                If Not strRecord + "" = "" Then
                ''                    myMsgCollection.Add(clsflFile.strMsgType, strRecord, clsflFile.strFileName)
                ''                    myMsgCollection.Items(myMsgCollection.Count - 1).strError = drC("ErrorMsg").ToString
                ''                End If
                ''            Next
                ''            sbChild = sbChild.Remove(0, sbChild.Length)
                ''        Next
                ''    Else
                ''        ProcessEDFile(clsflFile, myMsgCollection, SourceFolderLocation)
                ''    End If
                ''    ProcessExceptionEDFileNew = True
                ''Catch ex As Exception

                ''End Try
            End If

        Catch e As ArgumentNullException
            Throw
        Catch e As ArgumentOutOfRangeException
            Throw
        Catch e As System.Exception
            Throw
        End Try
        ProcessExceptionEDFileNew = True
    End Function

    Public Function ProcessEDMsgCollection(ByVal CxnString As String, ByRef myMsgCollection As EDCollectionMessageInfos_New, ByVal ProcessType As String, ByRef dtParent_Pell_DirectLoan As DataTable, ByVal dtChild_PellDisb_DLSchDisb As DataTable, Optional ByVal dtChild_DLActDisb As DataTable = Nothing, Optional ByVal SourcePath As String = "", Optional ByVal TargetPath As String = "", Optional ByVal FileName As String = "", Optional ByVal ExceptionPath As String = "", Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal Override As String = "", Optional ByVal Datafromremotecomputer As String = "no") As String
        'Dim rowIndex As Integer
        Dim EDDX As New EDDataXfer_New
        Dim EDDA As New DataAccess.DataAccess
        Dim blResult As String
        'Dim intStartIndex As Integer
        'Dim strByPass As String
        'Dim strOriginalSSN As String
        Dim strAwardId As String = ""
        ''  Dim intDataExistsInException As Integer
        Dim strType As String = ""
        Dim strDBIndicator As String = ""
        Dim strOrigSSN As String = ""
        Dim strMPNStatus As String = ""
        Dim strFileMessageType As String = ""
        Dim intID As Integer = 1
        'ProcessEDMsgCollection = False
        EDDA.OpenConnection()
        strType = ProcessType
        Dim groupTrans As OleDb.OleDbTransaction = EDDA.StartTransaction()
        'EDDA.ConnectionString = CxnString

        Try
            If strType.ToUpper = "PELL" Then
                blResult = Process_PELLMessage(EDDX, EDDA, groupTrans, myMsgCollection, dtParent_Pell_DirectLoan, dtChild_PellDisb_DLSchDisb, ExceptionGUID, SourceToTarget, Override)
            Else
                blResult = Process_DLMessage(EDDX, EDDA, groupTrans, myMsgCollection, dtParent_Pell_DirectLoan, dtChild_PellDisb_DLSchDisb, dtChild_DLActDisb, ExceptionGUID, SourceToTarget, Override)
            End If
            If Not blResult.ToUpper.Contains("EXCEPTION") And Trim(SourceToTarget) = "sourcetotarget" Then
                MoveFileToEDArchiveFolder(SourcePath, TargetPath, FileName, Datafromremotecomputer)
                EDDX.DeleteFromExceptionReport(EDDA, FileName, strType)
                Return blResult
                Exit Function
            ElseIf Not blResult.ToUpper.Contains("EXCEPTION") And Trim(SourceToTarget) = "exceptiontotarget" Then
                MoveFileToEDArchiveFolder(SourcePath, TargetPath, FileName, Datafromremotecomputer)
                EDDX.DeleteFromExceptionReport(EDDA, FileName, strType)
                Return blResult
                Exit Function
            ElseIf blResult.ToUpper.Contains("EXCEPTION") And Trim(SourceToTarget) = "sourcetotarget" Then
                MoveFileToEDExceptionFolder(SourcePath, ExceptionPath, FileName, Datafromremotecomputer)
                Return blResult
                Exit Function
            Else
                Return blResult
                Exit Function
            End If

        Catch e As System.Exception
            'groupTrans.Rollback()
            Return blResult
            'Throw
        Finally
            EDDA.CloseConnection()
        End Try
    End Function
    Public Function ProcessEDMsgCollectionNew(ByVal CxnString As String, ByVal ProcessType As String, ByRef dtParent_Pell_DirectLoan As DataTable, ByVal dtChild_PellDisb_DLSchDisb As DataTable, Optional ByVal dtChild_DLActDisb As DataTable = Nothing, Optional ByVal IsOverrideAdvDateWithExpDate As Boolean = False, Optional ByVal IsOverrideAdvAmtWithExpAmt As Boolean = False, Optional ByVal IsPostPayment As Boolean = False, Optional ByVal IsPayOutOfSchool As Boolean = False, Optional ByVal IsUsedExpDate As Boolean = False, Optional ByVal SpecifiedDate As String = "", Optional ByVal Recid As String = "", Optional ByVal PaymentType As String = "", Optional ByVal SourcePath As String = "", Optional ByVal TargetPath As String = "", Optional ByVal FileName As String = "", Optional ByVal ExceptionPath As String = "", Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal Override As String = "", Optional ByVal Datafromremotecomputer As String = "no", Optional ByVal NotPostTargetPath As String = "") As String
        '  Dim rowIndex As Integer
        Dim EDDX As New EDDataXfer_New
        Dim EDDA As New DataAccess.DataAccess
        Dim blResult As String
        'Dim intStartIndex As Integer
        'Dim strByPass As String
        'Dim strOriginalSSN As String
        Dim strAwardId As String = ""
        '  Dim intDataExistsInException As Integer
        Dim strType As String = ""
        Dim strDBIndicator As String = ""
        Dim strOrigSSN As String = ""
        Dim strMPNStatus As String = ""
        Dim strFileMessageType As String = ""
        Dim intID As Integer = 1
        'ProcessEDMsgCollection = False
        EDDA.OpenConnection()
        strType = ProcessType
        Dim groupTrans As OleDb.OleDbTransaction = EDDA.StartTransaction()
        'EDDA.ConnectionString = CxnString

        Try
            If strType.ToUpper = "PELL" Then
                'blResult = Process_PELLMessage(EDDX, EDDA, groupTrans, myMsgCollection, dtParent_Pell_DirectLoan, dtChild_PellDisb_DLSchDisb, ExceptionGUID, SourceToTarget, Override)
                blResult = Process_PELLMessageNew(EDDX, EDDA, groupTrans, dtParent_Pell_DirectLoan, dtChild_PellDisb_DLSchDisb, FileName, IsOverrideAdvDateWithExpDate, IsOverrideAdvAmtWithExpAmt, IsPostPayment, IsPayOutOfSchool, IsUsedExpDate, SpecifiedDate, Recid, PaymentType, ExceptionGUID, SourceToTarget, Override)
            Else
                'blResult = Process_DLMessage(EDDX, EDDA, groupTrans, myMsgCollection, dtParent_Pell_DirectLoan, dtChild_PellDisb_DLSchDisb, dtChild_DLActDisb, ExceptionGUID, SourceToTarget, Override)
                blResult = Process_DLMessageNew(EDDX, EDDA, groupTrans, dtParent_Pell_DirectLoan, dtChild_PellDisb_DLSchDisb, dtChild_DLActDisb, FileName, IsOverrideAdvDateWithExpDate, IsOverrideAdvAmtWithExpAmt, IsPostPayment, IsPayOutOfSchool, IsUsedExpDate, SpecifiedDate, Recid, PaymentType, ExceptionGUID, SourceToTarget, Override)
            End If
            If Not blResult.ToUpper.Contains("EXCEPTION") And Trim(SourceToTarget) = "sourcetotarget" Then
                ''If IsPostPayment = False Then
                ''    MoveFileToEDNotPostFolder(SourcePath, NotPostTargetPath, FileName, Datafromremotecomputer)
                ''End If
                MoveFileToEDArchiveFolder(SourcePath, TargetPath, FileName, Datafromremotecomputer)
                EDDX.DeleteFromExceptionReport(EDDA, FileName, strType)
                If IsPostPayment = False Then
                End If
                Return blResult
                Exit Function
            ElseIf Not blResult.ToUpper.Contains("EXCEPTION") And Trim(SourceToTarget) = "exceptiontotarget" Then
                ''If IsPostPayment = False Then
                ''    MoveFileToEDNotPostFolder(SourcePath, NotPostTargetPath, FileName, Datafromremotecomputer)
                ''End If
                MoveFileToEDArchiveFolder(SourcePath, TargetPath, FileName, Datafromremotecomputer)
                EDDX.DeleteFromExceptionReport(EDDA, FileName, strType)
                Return blResult
                Exit Function
            ElseIf blResult.ToUpper.Contains("EXCEPTION") And Trim(SourceToTarget) = "sourcetotarget" Then
                ''If IsPostPayment = False Then
                ''    MoveFileToEDNotPostFolder(SourcePath, NotPostTargetPath, FileName, Datafromremotecomputer)
                ''End If
                MoveFileToEDExceptionFolder(SourcePath, ExceptionPath, FileName, Datafromremotecomputer)
                Return blResult
                Exit Function
            Else
                Return blResult
                Exit Function
            End If

        Catch e As System.Exception
            'groupTrans.Rollback()
            Return blResult
            'Throw
        Finally
            EDDA.CloseConnection()
        End Try
    End Function
    Public Function ProcessEDPendingPaymentNew(ByVal ProcessType As String, ByRef dtTable As DataTable, ByVal PayOutOfSchool As Boolean, ByVal IsUseExpDate As Boolean, ByVal SpecifiedDate As String, ByVal Recid As String, ByVal PaymentType As String, Optional ByVal SourcePath As String = "", Optional ByVal TargetPath As String = "", Optional ByVal FileName As String = "", Optional ByVal ExceptionPath As String = "", Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal Override As String = "", Optional ByVal Datafromremotecomputer As String = "no") As String
        Dim EDDX As New EDDataXfer_New
        Dim EDDA As New DataAccess.DataAccess
        Dim blResult As String
        'Dim intStartIndex As Integer
        'Dim strByPass As String
        'Dim strOriginalSSN As String
        Dim strAwardId As String = ""
        '  Dim intDataExistsInException As Integer
        Dim strType As String = ""
        Dim strDBIndicator As String = ""
        Dim strOrigSSN As String = ""
        Dim strMPNStatus As String = ""
        Dim strFileMessageType As String = ""
        Dim intID As Integer = 1
        Dim ds As New DataSet
        Dim sb As New System.Text.StringBuilder
        'ProcessEDMsgCollection = False
        EDDA.OpenConnection()
        strType = ProcessType
        Dim groupTrans As OleDb.OleDbTransaction = EDDA.StartTransaction()
        'EDDA.ConnectionString = CxnString

        Try
            If strType.ToUpper = "PELL" Then
                blResult = Process_PELLPendingPaymentsNew(EDDX, EDDA, groupTrans, dtTable, PayOutOfSchool, IsUseExpDate, SpecifiedDate, Recid, PaymentType)
            Else
            End If
            'With sb
            '    .Append(" Select count(*) from syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable Where FileName=?")
            'End With
            'EDDA.ClearParameters()
            'EDDA.AddParameter("@FileName", FileName, DataAccess.DataAccess.OleDbDataType.OleDbString, 200, ParameterDirection.Input)
            'ds = EDDA.RunParamSQLDataSet(sb.ToString)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    If CType(ds.Tables(0).Rows(0)(0).ToString, Integer) = 0 Then
            '        MoveFileToEDArchiveFolder(SourcePath, TargetPath, FileName, Datafromremotecomputer)
            '    End If
            'End If
            Return blResult
        Catch e As System.Exception
            'groupTrans.Rollback()
            Return blResult
            'Throw
        Finally
            EDDA.CloseConnection()
        End Try
    End Function
    Public Function ValidateEDMsgCollection(ByVal CxnString As String, ByRef myMsgCollection As EDCollectionMessageInfos_New, ByVal ProcessType As String, ByRef dtParent_Pell_DirectLoan As DataTable, ByVal dtChild_PellDisb_DLSchDisb As DataTable, Optional ByVal dtChild_DLActDisb As DataTable = Nothing) As String
        Dim EDDX As New EDDataXfer_New
        Dim EDDA As New DataAccess.DataAccess
        Dim blResult As String
        Dim strType As String = ""
        EDDA.OpenConnection()
        Dim groupTrans As OleDb.OleDbTransaction = EDDA.StartTransaction()
        Try
            strType = ProcessType
            If strType.ToUpper = "PELL" Then
                blResult = Validate_PELLMessage(EDDX, EDDA, groupTrans, myMsgCollection, dtParent_Pell_DirectLoan, dtChild_PellDisb_DLSchDisb)
            Else
                blResult = Validate_DLMessage(EDDX, EDDA, groupTrans, myMsgCollection, dtParent_Pell_DirectLoan, dtChild_PellDisb_DLSchDisb, dtChild_DLActDisb)
            End If
        Catch e As System.Exception
            Return blResult
        Finally
            EDDA.CloseConnection()
        End Try
    End Function
    Public Function ValidateEDMsgCollectionNew(ByVal CxnString As String, ByVal ProcessType As String, ByRef dtParent_Pell_DirectLoan As DataTable, ByVal dtChild_PellDisb_DLSchDisb As DataTable, Optional ByVal dtChild_DLActDisb As DataTable = Nothing) As String
        Dim EDDX As New EDDataXfer_New
        Dim EDDA As New DataAccess.DataAccess
        Dim blResult As String
        Dim strType As String = ""
        EDDA.OpenConnection()
        Dim groupTrans As OleDb.OleDbTransaction = EDDA.StartTransaction()
        Try
            strType = ProcessType
            If strType.ToUpper = "PELL" Then
                blResult = Validate_PELLMessageNew(EDDX, EDDA, groupTrans, dtParent_Pell_DirectLoan, dtChild_PellDisb_DLSchDisb)
            Else
                blResult = Validate_DLMessageNew(EDDX, EDDA, groupTrans, dtParent_Pell_DirectLoan, dtChild_PellDisb_DLSchDisb, dtChild_DLActDisb)
            End If
        Catch e As System.Exception
            Return blResult
        Finally
            EDDA.CloseConnection()
        End Try
    End Function

    Private Sub RenameExistingFile(ByVal strTargetPathWithFile As String)
        Dim strRandomNumber As New Random
        Dim strTargetPathWithNewFile As String = strTargetPathWithFile + strRandomNumber.Next.ToString
        If File.Exists(strTargetPathWithFile) Then
            Dim fs As FileStream = New FileStream(strTargetPathWithFile, FileMode.Open)
            fs.Close()
            File.Move(strTargetPathWithFile, strTargetPathWithNewFile)
        End If
    End Sub
    Private Sub MoveFileToEDArchiveFolder(ByVal SourcePath As String, ByVal TargetPath As String, ByVal FileName As String, Optional ByVal Datafromremotecomputer As String = "no")
        Dim uriSource As New Uri(SourcePath.ToString.ToLower)
        Dim uriTarget As New Uri(TargetPath.ToString.ToLower)
        Dim strSourceFile As String
        Dim strTargetFile As String
        Dim strMessage As String = ""
        If Datafromremotecomputer = "yes" Then
            strSourceFile = SourcePath + "\" + FileName
            strTargetFile = TargetPath + "\" + FileName
        Else
            strSourceFile = uriSource.AbsolutePath + FileName
            strTargetFile = uriTarget.AbsolutePath + FileName
        End If
        'modified by balaji on 10/11/2007
        'based on input from patrick - files from Fame ESP will have an extension that is sequencial
        'in other words we won't have a case where there will be two HEAD Files (or any other file types)
        'with same extension, the extension will be .000,.001,.002
        'so when that is the case there is no need to rename the file.
        Try
            RenameExistingFile(strTargetFile)
            'Directory.Move(strSourceFile, strTargetFile)
            File.Move(strSourceFile, strTargetFile)
        Catch e As System.Exception
        End Try
    End Sub
    Private Sub MoveFileToEDExceptionFolder(ByVal SourcePath As String, ByVal TargetPath As String, ByVal FileName As String, Optional ByVal DataFromRemoteComputer As String = "no")
        Dim uriSource As New Uri(SourcePath.ToString.ToLower)
        Dim uriTarget As New Uri(TargetPath.ToString.ToLower)
        Dim strSourceFile As String
        Dim strTargetFile As String
        Dim strMessage As String = ""
        If DataFromRemoteComputer = "yes" Then
            strSourceFile = SourcePath + "\" + FileName
            strTargetFile = TargetPath + "\" + FileName
        Else
            strSourceFile = uriSource.AbsolutePath + FileName
            strTargetFile = uriTarget.AbsolutePath + FileName
        End If

        'modified by balaji on 10/11/2007
        'based on input from patrick - files from Fame ESP will have an extension that is sequencial
        'in other words we won't have a case where there will be two HEAD Files (or any other file types)
        'with same extension, the extension will be .000,.001,.002
        'so when that is the case there is no need to rename the file.
        Try
            ''RenameExistingFile(strTargetFile)
            'Directory.Move(strSourceFile, strTargetFile)
            File.Move(strSourceFile, strTargetFile)
        Catch e As System.Exception
        End Try
    End Sub
    Private Sub MoveFileToEDNotPostFolder(ByVal SourcePath As String, ByVal TargetPath As String, ByVal FileName As String, Optional ByVal DataFromRemoteComputer As String = "no")
        Dim uriSource As New Uri(SourcePath.ToString.ToLower)
        Dim uriTarget As New Uri(TargetPath.ToString.ToLower)
        Dim strSourceFile As String
        Dim strTargetFile As String
        Dim strMessage As String = ""
        If DataFromRemoteComputer = "yes" Then
            strSourceFile = SourcePath + "\" + FileName
            strTargetFile = TargetPath + "\" + FileName
        Else
            strSourceFile = uriSource.AbsolutePath + FileName
            strTargetFile = uriTarget.AbsolutePath + FileName
        End If

        'modified by balaji on 10/11/2007
        'based on input from patrick - files from Fame ESP will have an extension that is sequencial
        'in other words we won't have a case where there will be two HEAD Files (or any other file types)
        'with same extension, the extension will be .000,.001,.002
        'so when that is the case there is no need to rename the file.
        Try
            RenameExistingFile(strTargetFile)
            'Directory.Move(strSourceFile, strTargetFile)
            File.Copy(strSourceFile, strTargetFile, True)
        Catch e As System.Exception
        End Try
    End Sub
    Public Function Process_PELLMessage(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDbTransaction, ByRef myMsgCollection As EDCollectionMessageInfos_New, ByVal dtParent As DataTable, ByVal dtChild As DataTable, Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal override As String = "") As String
        'Dim strStudentName As String = ""
        'Dim strStudentEnrollmentID As String = ""
        'Dim strEnrollId As String = ""
        'Dim strCampusId As String = ""
        'Dim strStudentAwardID As String = ""
        'Dim msgEntryParent As EDMessageInfo_New
        'Dim msgEntryChild As EDMessageInfo_New
        'Dim strExceptionMessage, strFileNameOnly, sMessage As String
        'Dim strGetAward As String = ""
        'Dim strFinalMessage As String = ""
        'Dim strErrorMessage As String = ""
        'Dim intPostedRecords As Integer = 0
        'Dim intPostedDisbRecords As Integer = 0
        'Dim intNotPostedRecords As Integer = 0
        'Dim intNotPostedDisbRecords As Integer = 0
        'Dim drTempRows() As DataRow
        'Dim strAwardScheduleId As String = ""
        'Dim strFundSourceDesc As String = ""
        'Dim strFundSourdeId As String = ""
        'Dim strTransactionId As String = ""
        'Dim strPmtDisbId As String = ""
        'Dim IsTransactionPosted As Boolean = False
        'Dim IsOutOfSchool As Boolean = False
        'Dim IsInSchool As Boolean = False
        'Dim strDBI As String = ""
        'Dim blHasException As Boolean = False
        'Dim blValidate As Boolean = False
        'Dim blOverrideAdvDateWithEDExpDate = False
        'Dim blOverrideAdvAmtWithEDExpAmt = False
        'Dim blIsPostPayment = False
        'Dim blIsPayOutOfSchool = False
        'Dim blIsUseExpDate = False
        'Dim strSpecificDate As String


        'For Each drP As DataRow In dtParent.Rows
        '    EDDX.GetStudentInfo(db, drP("SSNOriginal").ToString, strStudentName, strEnrollId, strCampusId, IsInSchool)
        '    msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drP("IndexID").ToString))
        '    strDBI = msgEntryParent.strDatabaseIndicator
        '    blOverrideAdvDateWithEDExpDate = msgEntryParent.blnIsOverrideAdvDateWithEDExpDate
        '    blOverrideAdvAmtWithEDExpAmt = msgEntryParent.blnIsOverrideAdvAmtWithEDExpAmt
        '    blIsPostPayment = msgEntryParent.blnIsPostPayment
        '    blIsPayOutOfSchool = msgEntryParent.blnIsPayOutOfSchoolStudent
        '    blIsUseExpDate = msgEntryParent.blnIsUseExpDate
        '    strSpecificDate = msgEntryParent.strSpecifiedDate
        '    Dim intStartIndex As Integer = InStrRev(msgEntryParent.strFileNameSource, "/")
        '    strFileNameOnly = msgEntryParent.strFileNameSource.Substring(intStartIndex)
        '    drTempRows = dtChild.Select("SSNOriginal = '" + drP("SSNOriginal").ToString.Trim + "' AND ParentIndexID= " + drP("IndexID").ToString)
        '    blValidate = ValidateAwardId(drP, drTempRows, myMsgCollection)
        '    If blValidate = True Then
        '        If Trim(strStudentName) = "" Then
        '            blHasException = True
        '            sMessage = "Unable to find student with SSN:" & msgEntryParent.strOriginalSSN
        '            EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
        '            EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
        '        Else
        '            Process_PELLMessage = False
        '            Try
        '                If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
        '                    sMessage = "Unable to find student with SSN:" & msgEntryParent.strOriginalSSN
        '                    blHasException = True
        '                    EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
        '                    EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
        '                Else
        '                    If Not Trim(strEnrollId) = "" Then
        '                        Try
        '                            Dim strFSDtl = EDDX.GetFundSourceDetails(db, strFundSourceDesc, strFundSourdeId, msgEntryParent.strGrandType, msgEntryParent.strMsgType)
        '                            If strFSDtl = "" And Not strFSDtl.Contains("Unable") Then
        '                                strGetAward = EDDX.Add_faStudentAwardsFromPell(groupTrans, db, strStudentAwardID, strEnrollId, msgEntryParent, drTempRows.Length.ToString, strStudentName, SourceToTarget, blOverrideAdvDateWithEDExpDate, blOverrideAdvAmtWithEDExpAmt)
        '                                EDDX.Check_StudentSchedulePell(db, groupTrans, strGetAward, myMsgCollection, drTempRows)
        '                                If Not (strGetAward = "") And Not strGetAward.Contains("Unable") Then
        '                                    For Each drC As DataRow In drTempRows
        '                                        strAwardScheduleId = ""
        '                                        strTransactionId = ""
        '                                        msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drC("IndexId").ToString))
        '                                        EDDX.Add_faAwardScheduleForPell(db, groupTrans, strAwardScheduleId, strTransactionId, strStudentAwardID, msgEntryChild, strStudentName, SourceToTarget)
        '                                        If blIsPostPayment = True And IsInSchool = True Then
        '                                            'Post Payment for Student In Schol
        '                                            If msgEntryChild.strDisbursementRealeaseIndicator.ToUpper = "Y" Then
        '                                                IsTransactionPosted = EDDX.Check_TransactionPosted(db, groupTrans, strAwardScheduleId, strTransactionId)
        '                                                If IsTransactionPosted = False Then
        '                                                    EDDX.Add_saTransaction(db, groupTrans, strTransactionId, strEnrollId, strCampusId, msgEntryChild, strFundSourceDesc, strStudentName, , blIsUseExpDate, strSpecificDate)
        '                                                    EDDX.Update_faAwardScheduleForPell(db, groupTrans, strAwardScheduleId, strTransactionId)
        '                                                    EDDX.Add_saPmtDisbRel(db, groupTrans, strPmtDisbId, strTransactionId, strGetAward, strAwardScheduleId, msgEntryChild)
        '                                                    EDDX.Add_saPayments(db, groupTrans, strTransactionId, msgEntryChild, strStudentName)
        '                                                Else
        '                                                    EDDX.Update_faAwardScheduleForPell(db, groupTrans, strAwardScheduleId, strTransactionId)
        '                                                End If
        '                                                IsTransactionPosted = False
        '                                            End If
        '                                        ElseIf blIsPostPayment = True And IsInSchool = False Then
        '                                            If blIsPayOutOfSchool = True Then
        '                                                'Post Payments for Student Out Of School
        '                                                If msgEntryChild.strDisbursementRealeaseIndicator.ToUpper = "Y" Then
        '                                                    IsTransactionPosted = EDDX.Check_TransactionPosted(db, groupTrans, strAwardScheduleId, strTransactionId)
        '                                                    If IsTransactionPosted = False Then
        '                                                        EDDX.Add_saTransaction(db, groupTrans, strTransactionId, strEnrollId, strCampusId, msgEntryChild, strFundSourceDesc, strStudentName, , blIsUseExpDate, strSpecificDate)
        '                                                        EDDX.Update_faAwardScheduleForPell(db, groupTrans, strAwardScheduleId, strTransactionId)
        '                                                        EDDX.Add_saPmtDisbRel(db, groupTrans, strPmtDisbId, strTransactionId, strGetAward, strAwardScheduleId, msgEntryChild)
        '                                                        EDDX.Add_saPayments(db, groupTrans, strTransactionId, msgEntryChild, strStudentName)
        '                                                    Else
        '                                                        EDDX.Update_faAwardScheduleForPell(db, groupTrans, strAwardScheduleId, strTransactionId)
        '                                                    End If
        '                                                    IsTransactionPosted = False
        '                                                End If
        '                                            Else
        '                                                'Save in the Database
        '                                                EDDX.SaveNotPostedRecords(db, strFileNameOnly, myMsgCollection, msgEntryParent.strMsgType, drP, drTempRows)
        '                                            End If
        '                                        Else
        '                                            'Save in the Database
        '                                            EDDX.SaveNotPostedRecords(db, strFileNameOnly, myMsgCollection, msgEntryParent.strMsgType, drP, drTempRows)
        '                                        End If
        '                                        intPostedDisbRecords += 1
        '                                    Next
        '                                Else
        '                                    sMessage = strFSDtl
        '                                    blHasException = True
        '                                    EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
        '                                    EDDX.BuildExceptionReport(db, strGetAward, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
        '                                End If
        '                                intPostedRecords += 1
        '                            Else
        '                                blHasException = True
        '                                EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
        '                                EDDX.BuildExceptionReport(db, strFSDtl, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
        '                            End If
        '                        Catch ex As System.Exception
        '                            sMessage = ex.Message.ToString
        '                            EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
        '                            EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
        '                        End Try
        '                    Else
        '                        sMessage = "Unable to match with student " & strStudentName & " enrollment records "
        '                        blHasException = True
        '                        EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
        '                        EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
        '                    End If
        '                End If
        '            Catch e As System.Exception
        '                sMessage = e.Message.ToString
        '                blHasException = True
        '                EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
        '                EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
        '            End Try
        '        End If
        '    Else
        '        blHasException = True
        '        sMessage = "Award Id in file is not same for student with SSN:" & msgEntryParent.strOriginalSSN
        '        EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
        '        EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
        '    End If
        'Next
        'If blHasException = True Then
        '    strFinalMessage = "Exception Total number of records :  " & (intPostedRecords + intPostedDisbRecords + intNotPostedRecords + intNotPostedDisbRecords).ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " awards posted : " & intPostedRecords.ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " awards not posted : " & intNotPostedRecords.ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " Disbursements posted : " & intPostedDisbRecords.ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " Disbursements not posted : " & intNotPostedDisbRecords.ToString
        'Else
        '    strFinalMessage = "Total number of records :  " & (intPostedRecords + intPostedDisbRecords + intNotPostedRecords + intNotPostedDisbRecords).ToString & vbLf & "Number of Pell posted : " & intPostedRecords.ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " awards not posted : " & intNotPostedRecords.ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " Disbursements posted : " & intPostedDisbRecords.ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " Disbursements not posted : " & intNotPostedDisbRecords.ToString
        'End If
        'Return strFinalMessage
    End Function
    Public Function Process_PELLMessageNew(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDbTransaction, ByVal dtParent As DataTable, ByVal dtChild As DataTable, ByVal FileName As String, Optional ByVal IsOverrideAdvDateWithExpDate As Boolean = False, Optional ByVal IsOverrideAdvAmtWithExpAmt As Boolean = False, Optional ByVal IsPostPayment As Boolean = False, Optional ByVal IsPayOutOfSchool As Boolean = False, Optional ByVal IsUsedExpDate As Boolean = False, Optional ByVal SpecifiedDate As String = "", Optional ByVal Recid As String = "", Optional ByVal PaymentType As String = "", Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal override As String = "") As String
        Dim strStudentName As String = ""
        Dim strEnrollId As String = ""
        Dim strCampusId As String = ""
        Dim strStudentAwardID As String = ""
        '' strExceptionMessage, 
        Dim strFileNameOnly, sMessage As String
        Dim strFinalMessage As String = ""
        Dim strErrorMessage As String = ""
        Dim intPostedRecords As Integer = 0
        Dim intPostedDisbRecords As Integer = 0
        Dim intNotPostedRecords As Integer = 0
        Dim intNotPostedDisbRecords As Integer = 0
        Dim drTempRows() As DataRow
        ''  Dim drDistDisnNum() As DataRow
        Dim strAwardScheduleId As String = ""
        Dim strDBI As String = ""
        Dim blHasException As Boolean = False
        Dim blValidate As Boolean = False
        Dim dbAwardAmountPosted As Decimal = 0.0
        Dim dbDisbAmountPosted As Decimal = 0.0
        Dim dbAwardAmountNotPosted As Decimal = 0.0
        Dim dbDisbAmountNotPosted As Decimal = 0.0

        'EDDX.DeleteFromExceptionReportNew(db, FileName, "PELL", ExceptionGUID)
        '' To Delete from Not Post Table
        'EDDX.DeleteFromNotPostRecordsNew(db, FileName, "PELL")

        For Each drP As DataRow In dtParent.Rows
            strStudentName = drP("FirstName").ToString + " " + drP("LastName").ToString
            strEnrollId = drP("StuEnrollmentId").ToString
            strCampusId = drP("CampusId").ToString
            strDBI = drP("dbIndicator").ToString
            strFileNameOnly = FileName
            drTempRows = dtChild.Select("SSNOriginal = '" + drP("SSNOriginal").ToString.Trim + "' AND ParentIndexID= " + drP("IndexID").ToString, "DisbNum , SeqNum ASC")
            blValidate = ValidateAwardIdNew(drP, drTempRows)
            If blValidate = True Then
                If Trim(strStudentName) = "" Then
                    blHasException = True
                    sMessage = "Unable to find student with SSN:" & drP("SSNOriginal").ToString.Trim
                    EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "PELL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRows, , ExceptionGUID)
                Else
                    Process_PELLMessageNew = False
                    Try
                        If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
                            sMessage = "Unable to find student with SSN:" & drP("SSNOriginal").ToString.Trim
                            blHasException = True
                            EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "PELL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRows, , ExceptionGUID)
                        Else
                            If Not Trim(strEnrollId) = "" Then
                                Try
                                    If CType(drP("ErrorType").ToString, Integer) <> 0 Then
                                        Dim ParentID As String = Guid.NewGuid().ToString
                                        strStudentAwardID = EDDX.Add_faStudentAwardsFromPellNew(groupTrans, db, drP, drTempRows.Length, ParentID, strFileNameOnly, IsOverrideAdvDateWithExpDate, IsOverrideAdvAmtWithExpAmt, IsPostPayment, IsPayOutOfSchool)
                                        If Not (strStudentAwardID = "") And Not strStudentAwardID.Contains("Unable") Then
                                            Try
                                                dbAwardAmountPosted += Convert.ToDecimal(drP("AwardAmount").ToString)
                                            Catch ex As Exception
                                                dbAwardAmountPosted += 0.0
                                            End Try
                                            Dim DisbNums As String = EDDX.GetDisbursementNumber(drTempRows)
                                            For Each drC As DataRow In drTempRows
                                                If CType(drC("ErrorType").ToString, Integer) <> 0 And CType(drC("Show").ToString, Integer) = 1 Then
                                                    EDDX.Add_faAwardScheduleForPellNew(db, groupTrans, strStudentAwardID, drP("GrantType").ToString, drC, DisbNums, ParentID, strFileNameOnly, Recid, IsPostPayment, IsPayOutOfSchool, IsUsedExpDate, SpecifiedDate, PaymentType, IsOverrideAdvDateWithExpDate, IsOverrideAdvAmtWithExpAmt)
                                                    Try
                                                        dbDisbAmountPosted += Convert.ToDecimal(drC("AccDisbAmt").ToString)
                                                    Catch ex As Exception
                                                        dbDisbAmountPosted += 0.0
                                                    End Try
                                                    intPostedDisbRecords += 1
                                                End If
                                            Next
                                            intPostedRecords += 1
                                        Else
                                            blHasException = True
                                            sMessage = "Unable to find award type for with SSN:" & drP("SSNOriginal").ToString.Trim
                                            EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "PELL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRows, , ExceptionGUID)
                                        End If
                                    End If
                                Catch ex As System.Exception
                                    sMessage = ex.Message.ToString
                                    EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "PELL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRows, , ExceptionGUID)
                                End Try
                            Else
                                sMessage = "Unable to match with student " & strStudentName & " enrollment records "
                                blHasException = True
                                EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "PELL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRows, , ExceptionGUID)
                            End If
                        End If
                    Catch e As System.Exception
                        sMessage = e.Message.ToString
                        blHasException = True
                        EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "PELL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRows, , ExceptionGUID)
                    End Try
                End If
            Else
                blHasException = True
                sMessage = "Award Id in file is not same for student with SSN:" & drP("SSNOriginal").ToString.Trim
                EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "PELL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRows, , ExceptionGUID)
            End If
        Next
        If blHasException = True Then
            strFinalMessage = "Exception Total number of records :  " & (intPostedRecords + intPostedDisbRecords + intNotPostedRecords + intNotPostedDisbRecords).ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " awards posted : " & intPostedRecords.ToString & vbLf & "Total of awards posted : " & String.Format("{0:c}", dbAwardAmountPosted) & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " awards not posted : " & intNotPostedRecords.ToString & vbLf & "Total of awards not posted : " & String.Format("{0:c}", dbAwardAmountNotPosted) & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " Disbursements posted : " & intPostedDisbRecords.ToString & vbLf & "Total of disbursements posted : " & String.Format("{0:c}", dbDisbAmountPosted) & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " Disbursements not posted : " & intNotPostedDisbRecords.ToString & vbLf & "Total of disbursements not posted : " & String.Format("{0:c}", dbDisbAmountNotPosted)
        Else
            strFinalMessage = "Total number of records :  " & (intPostedRecords + intPostedDisbRecords + intNotPostedRecords + intNotPostedDisbRecords).ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " awards posted : " & intPostedRecords.ToString & vbLf & "Total of awards posted : " & String.Format("{0:c}", dbAwardAmountPosted) & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " awards not posted : " & intNotPostedRecords.ToString & vbLf & "Total of awards not posted : " & String.Format("{0:c}", dbAwardAmountNotPosted) & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " Disbursements posted : " & intPostedDisbRecords.ToString & vbLf & "Total of disbursements posted : " & String.Format("{0:c}", dbDisbAmountPosted) & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " Disbursements not posted : " & intNotPostedDisbRecords.ToString & vbLf & "Total of disbursements not posted : " & String.Format("{0:c}", dbDisbAmountNotPosted)
        End If
        Return strFinalMessage
    End Function
    Public Function Process_DLMessageNew(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDbTransaction, ByVal dtParent As DataTable, ByVal dtSubDisb As DataTable, ByVal dtActDisb As DataTable, ByVal FileName As String, Optional ByVal IsOverrideAdvDateWithExpDate As Boolean = False, Optional ByVal IsOverrideAdvAmtWithExpAmt As Boolean = False, Optional ByVal IsPostPayment As Boolean = False, Optional ByVal IsPayOutOfSchool As Boolean = False, Optional ByVal IsUsedExpDate As Boolean = False, Optional ByVal SpecifiedDate As String = "", Optional ByVal Recid As String = "", Optional ByVal PaymentType As String = "", Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal override As String = "") As String
        Dim strStudentName As String = ""
        Dim strEnrollId As String = ""
        Dim strCampusId As String = ""
        Dim strStudentAwardID As String = ""
        ''strExceptionMessage,
        Dim strFileNameOnly, sMessage As String
        Dim strFinalMessage As String = ""
        Dim strErrorMessage As String = ""
        Dim intPostedRecords As Integer = 0
        Dim intPostedDisbRecords As Integer = 0
        Dim intNotPostedRecords As Integer = 0
        Dim intNotPostedDisbRecords As Integer = 0
        Dim intActualPostedRecords As Integer = 0
        Dim intNotActualPostedRecords As Integer = 0
        Dim drTempRowsSD() As DataRow
        Dim drTempRowsAD() As DataRow
        '' Dim drDistDisnNum() As DataRow
        Dim strAwardScheduleId As String = ""
        Dim strDBI As String = ""
        Dim blHasException As Boolean = False
        Dim blValidate As Boolean = False
        Dim dbAwardAmountPosted As Decimal = 0.0
        Dim dbDisbAmountPosted As Decimal = 0.0
        Dim dbActAmountPosted As Decimal = 0.0
        Dim dbAwardAmountNotPosted As Decimal = 0.0
        Dim dbDisbAmountNotPosted As Decimal = 0.0
        Dim dbActAmountNotPosted As Decimal = 0.0

        EDDX.DeleteFromExceptionReportNew(db, FileName, "DL", ExceptionGUID)
        '' To Delete from Not Post Table
        EDDX.DeleteFromNotPostRecordsNew(db, FileName, "DL")

        For Each drP As DataRow In dtParent.Rows
            If drP("IsParsed").ToString = "1" And Not drP("AcademicYearId").ToString = Guid.Empty.ToString Then
                strStudentName = drP("FirstName").ToString + " " + drP("LastName").ToString
                strEnrollId = drP("StuEnrollmentId").ToString
                strCampusId = drP("CampusId").ToString
                strDBI = drP("dbIndicator").ToString
                strFileNameOnly = FileName
                drTempRowsSD = dtSubDisb.Select("SSNOriginal = '" + drP("SSNOriginal").ToString.Trim + "' AND ParentIndexID= " + drP("IndexID").ToString, "DisbNum , SeqNum ASC")
                drTempRowsAD = dtActDisb.Select("SSNOriginal = '" + drP("SSNOriginal").ToString.Trim + "' AND ParentIndexID= " + drP("IndexID").ToString, "DisbNum , SeqNum ASC")
                blValidate = ValidateLoanIdNew(drP, drTempRowsSD, drTempRowsAD)
                If blValidate = True Then
                    If Trim(strStudentName) = "" Then
                        blHasException = True
                        sMessage = "Unable to find student with SSN:" & drP("SSNOriginal").ToString.Trim
                        EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "DL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRowsSD, drTempRowsAD, ExceptionGUID, dbActAmountNotPosted)
                    Else
                        Process_DLMessageNew = False
                        Try
                            If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
                                sMessage = "Unable to find student with SSN:" & drP("SSNOriginal").ToString.Trim
                                blHasException = True
                                EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "DL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRowsSD, drTempRowsAD, ExceptionGUID, dbActAmountNotPosted)
                            Else
                                If Not Trim(strEnrollId) = "" Then
                                    Try
                                        If CType(drP("ErrorType").ToString, Integer) <> 0 Then
                                            Dim ParentID As String = Guid.NewGuid().ToString
                                            strStudentAwardID = EDDX.Add_faStudentAwardsFromDLNew(groupTrans, db, drP, drTempRowsAD.Length, ParentID, strFileNameOnly, IsOverrideAdvDateWithExpDate, IsOverrideAdvAmtWithExpAmt, IsPostPayment, IsPayOutOfSchool)
                                            If Not (strStudentAwardID = "") And Not strStudentAwardID.Contains("Unable") Then
                                                Try
                                                    dbAwardAmountPosted += Convert.ToDecimal(drP("GrossAmount").ToString)
                                                Catch ex As Exception
                                                    dbAwardAmountPosted += 0.0
                                                End Try
                                                Dim DisbNums As String = "0"
                                                For Each drC As DataRow In drTempRowsSD
                                                    If CType(drC("ErrorType").ToString, Integer) <> 0 And CType(drC("Show").ToString, Integer) = 1 Then
                                                        EDDX.Add_faAwardScheduleForDLNew(db, groupTrans, strStudentAwardID, drP("LoanType").ToString, drC, DisbNums, ParentID, strFileNameOnly, Recid, IsPostPayment, IsPayOutOfSchool, IsUsedExpDate, SpecifiedDate, PaymentType, "SD", IsOverrideAdvDateWithExpDate, IsOverrideAdvAmtWithExpAmt)
                                                        Try
                                                            dbDisbAmountPosted += Convert.ToDecimal(drC("GrossAmt").ToString)
                                                        Catch ex As Exception
                                                            dbDisbAmountPosted += 0.0
                                                        End Try
                                                        intPostedDisbRecords += 1
                                                    End If
                                                Next
                                                For Each drC As DataRow In drTempRowsAD
                                                    If CType(drC("ErrorType").ToString, Integer) <> 0 And CType(drC("Show").ToString, Integer) = 1 Then
                                                        EDDX.Add_faAwardScheduleForDLNew(db, groupTrans, strStudentAwardID, drP("LoanType").ToString, drC, DisbNums, ParentID, strFileNameOnly, Recid, IsPostPayment, IsPayOutOfSchool, IsUsedExpDate, SpecifiedDate, PaymentType, "AD")
                                                        Try
                                                            dbActAmountPosted += Convert.ToDecimal(drC("GrossAmt").ToString)
                                                        Catch ex As Exception
                                                            dbActAmountPosted += 0.0
                                                        End Try
                                                        intActualPostedRecords += 1
                                                    End If
                                                Next
                                                intPostedRecords += 1
                                            Else
                                                blHasException = True
                                                sMessage = "Unable to find award type for with SSN:" & drP("SSNOriginal").ToString.Trim
                                                EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "DL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRowsSD, drTempRowsAD, ExceptionGUID, dbActAmountNotPosted)
                                            End If
                                        End If
                                    Catch ex As System.Exception
                                        sMessage = ex.Message.ToString
                                        EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "DL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRowsSD, drTempRowsAD, ExceptionGUID, dbActAmountNotPosted)
                                    End Try
                                Else
                                    sMessage = "Unable to match with student " & strStudentName & " enrollment records "
                                    blHasException = True
                                    EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "DL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRowsSD, drTempRowsAD, ExceptionGUID, dbActAmountNotPosted)
                                End If
                            End If
                        Catch e As System.Exception
                            sMessage = e.Message.ToString
                            blHasException = True
                            EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "DL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRowsSD, drTempRowsAD, ExceptionGUID, dbActAmountNotPosted)
                        End Try
                    End If
                Else
                    blHasException = True
                    sMessage = "Award Id in file is not same for student with SSN:" & drP("SSNOriginal").ToString.Trim
                    EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "DL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRowsSD, drTempRowsAD, ExceptionGUID, dbActAmountNotPosted)
                End If
            Else
                blHasException = True
                strStudentName = drP("FirstName").ToString + " " + drP("LastName").ToString
                strEnrollId = drP("StuEnrollmentId").ToString
                strCampusId = drP("CampusId").ToString
                strDBI = drP("dbIndicator").ToString
                strFileNameOnly = FileName
                drTempRowsSD = dtSubDisb.Select("SSNOriginal = '" + drP("SSNOriginal").ToString.Trim + "' AND ParentIndexID= " + drP("IndexID").ToString, "DisbNum , SeqNum ASC")
                drTempRowsAD = dtActDisb.Select("SSNOriginal = '" + drP("SSNOriginal").ToString.Trim + "' AND ParentIndexID= " + drP("IndexID").ToString, "DisbNum , SeqNum ASC")
                sMessage = "Award information missing in file for student with SSN:" & drP("SSNOriginal").ToString.Trim
                EDDX.BuildExceptionReportNew2(db, sMessage, strFileNameOnly, "DL", intNotPostedRecords, intNotPostedDisbRecords, dbAwardAmountNotPosted, dbDisbAmountNotPosted, drP, drTempRowsSD, drTempRowsAD, ExceptionGUID, dbActAmountNotPosted)
            End If
        Next

        If blHasException = True Then
            strFinalMessage = "Exception Total number of records :  " & (intPostedRecords + intPostedDisbRecords + intNotPostedRecords + intNotPostedDisbRecords + intActualPostedRecords + intNotActualPostedRecords).ToString & vbLf & "Number of Direct Loan posted : " & intPostedRecords.ToString & vbLf & "Total of Direct Loan posted : " & String.Format("{0:c}", dbAwardAmountPosted) & vbLf & "Number of Direct Loan not posted : " & intNotPostedRecords.ToString & vbLf & "Total of Direct Loan not posted : " & String.Format("{0:c}", dbAwardAmountNotPosted) & vbLf & "Number of Direct Loan Disbursements posted : " & intPostedDisbRecords.ToString & vbLf & "Total of Direct Loan Disbirsements posted : " & String.Format("{0:c}", dbDisbAmountPosted) & vbLf & "Number of Direct Loan Disbursements not posted : " & intNotPostedDisbRecords.ToString & vbLf & "Total of Direct Loan Disbursement not posted : " & String.Format("{0:c}", dbDisbAmountNotPosted) & vbLf & "Number of Direct Loan Actual Disbursements posted : " & intActualPostedRecords.ToString & vbLf & "Total of Direct Loan Actual Disbursements posted : " & String.Format("{0:c}", dbActAmountPosted) & vbLf & "Number of Direct Loan Actual Disbursements not posted : " & intNotActualPostedRecords.ToString & vbLf & "Total of Direct Loan Actual Disbursements not posted : " & String.Format("{0:c}", dbActAmountNotPosted)
        Else
            strFinalMessage = "Total number of records :  " & (intPostedRecords + intPostedDisbRecords + intNotPostedRecords + intNotPostedDisbRecords + intActualPostedRecords + intNotActualPostedRecords).ToString & vbLf & "Number of Direct Loan posted : " & intPostedRecords.ToString & vbLf & "Total of Direct Loan posted : " & String.Format("{0:c}", dbAwardAmountPosted) & vbLf & "Number of Direct Loan not posted : " & intNotPostedRecords.ToString & vbLf & "Total of Direct Loan not posted : " & String.Format("{0:c}", dbAwardAmountNotPosted) & vbLf & "Number of Direct Loan Disbursements posted : " & intPostedDisbRecords.ToString & vbLf & "Total of Direct Loan Disbirsements posted : " & String.Format("{0:c}", dbDisbAmountPosted) & vbLf & "Number of Direct Loan Disbursements not posted : " & intNotPostedDisbRecords.ToString & vbLf & "Total of Direct Loan Disbursement not posted : " & String.Format("{0:c}", dbDisbAmountNotPosted) & vbLf & "Number of Direct Loan Actual Disbursements posted : " & intActualPostedRecords.ToString & vbLf & "Total of Direct Loan Actual Disbursements posted : " & String.Format("{0:c}", dbActAmountPosted) & vbLf & "Number of Direct Loan Actual Disbursements not posted : " & intNotActualPostedRecords.ToString & vbLf & "Total of Direct Loan Actual Disbursements not posted : " & String.Format("{0:c}", dbActAmountNotPosted)
        End If
        Return strFinalMessage
    End Function
    Public Function Process_PELLPendingPaymentsNew(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDbTransaction, ByVal dtTable As DataTable, ByVal PayOutOfSchool As Boolean, ByVal IsUseExpDate As Boolean, ByVal SpecifiedDate As String, ByVal Recid As String, ByVal PaymentType As String) As String
        Dim strFinalMessage As String = ""
        Dim intPosted As Integer = 0
        Dim intRemove As Integer = 0
        Dim dbDisbAmountPosted As Decimal = 0.0
        Dim dbDisbAmountRemoved As Decimal = 0.0
        For Each dr As DataRow In dtTable.Rows

            If dr("Pay").ToString = "True" Or (PayOutOfSchool = True And dr("IsInSchool").ToString = "False") Then
                EDDX.PostPendingDisbursement(db, groupTrans, IIf(dr("Pay").ToString = "True", True, PayOutOfSchool), IsUseExpDate, dr("DisbDate").ToString, Recid, PaymentType, dr)
                Try
                    dbDisbAmountPosted += Convert.ToDecimal(dr("PaymentAmount").ToString)
                Catch ex As Exception
                    dbDisbAmountPosted += 0.0
                End Try
                intPosted = intPosted + 1
            Else
                If dr("RemoveFromList").ToString = "True" Then
                    EDDX.RemoveDisbursementFromNotPostTable(db, groupTrans, dr("ParentId").ToString, dr("DetailId").ToString)
                    Try
                        dbDisbAmountRemoved += Convert.ToDecimal(dr("PaymentAmount").ToString)
                    Catch ex As Exception
                        dbDisbAmountRemoved += 0.0
                    End Try
                    intRemove = intRemove + 1
                End If
            End If
        Next
        strFinalMessage = "Total number of records :  " & (dtTable.Rows.Count).ToString & vbLf & "Number of disbursement posted : " & intPosted.ToString & vbLf & "Total of disbursements to be posted: " & String.Format("{0:c}", dbDisbAmountPosted) & vbLf & "Number of records remove from list : " + (intRemove.ToString) & vbLf & "Total of disbursements removed from list: " & String.Format("{0:c}", dbDisbAmountRemoved)
        Return strFinalMessage
    End Function
    Public Function GetPendingPayments(ByVal FileName As String, ByVal FilterCampusIds As String, ByVal FilterEnrollmentStatusIds As String, ByVal FilterFundSourceIds As String, ByVal FilterLenderIds As String, ByVal ExpDateFrom As String, ByVal ExpDateTo As String) As DataSet
        Dim ds As New DataSet
        Dim EDDX As New EDDataXfer_New
        Dim EDDA As New DataAccess.DataAccess
        ds = EDDX.GetPendingPayments(EDDA, FileName, FilterCampusIds, FilterEnrollmentStatusIds, FilterFundSourceIds, FilterLenderIds, ExpDateFrom, ExpDateTo)
        Return ds
    End Function
    Public Function Process_DLMessage(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDbTransaction, ByRef myMsgCollection As EDCollectionMessageInfos_New, ByVal dtDL As DataTable, ByVal dtDLScheduleDisp As DataTable, ByVal dtDLActuleDesp As DataTable, Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal override As String = "") As String
        'Dim strStudentName As String = ""
        'Dim strStudentEnrollmentID As String = ""
        'Dim strStudentAwardID As String = ""
        'Dim strFinalMessage As String = ""
        'Dim strErrorMessage As String = ""
        'Dim intPostedRecords As Integer = 0
        'Dim intPostedDisbRecords As Integer = 0
        'Dim intNotPostedRecords As Integer = 0
        'Dim intNotPostedDisbRecords As Integer = 0
        'Dim intActualPostedRecords As Integer = 0
        'Dim intNotActualPostedRecords As Integer = 0
        'Dim msgEntryDL As EDMessageInfo_New
        'Dim msgEntrySD As EDMessageInfo_New
        'Dim msgEntryAD As EDMessageInfo_New
        'Dim strExceptionMessage, strFileNameOnly, sMessage As String
        'Dim strGetAward As String = ""
        'Dim drTempRowsSD() As DataRow
        'Dim drTempRowsAD() As DataRow
        'Dim strAwardScheduleId As String = ""
        'Dim IsTransactionPosted As Boolean = False
        'Dim strFundSourceDesc As String = ""
        'Dim strFundSourdeId As String = ""
        'Dim strTransactionId As String = ""
        'Dim strCampusId As String = ""
        'Dim strPmtDisbId As String = ""
        'Dim blHasException As Boolean = False
        'Dim blValidate As Boolean = False
        'For Each drDL As DataRow In dtDL.Rows
        '    strStudentName = getStudentNameAndSSN(drDL("SSN").ToString)
        '    msgEntryDL = myMsgCollection.Items(Convert.ToInt32(drDL("MCID").ToString))
        '    Dim intStartIndex As Integer = InStrRev(msgEntryDL.strFileNameSource, "/")
        '    strFileNameOnly = msgEntryDL.strFileNameSource.Substring(intStartIndex)
        '    drTempRowsSD = dtDLScheduleDisp.Select("SSN = " + drDL("SSN").ToString + " AND RMCID= " + drDL("RMCID").ToString)
        '    drTempRowsAD = dtDLActualDisb.Select("SSN = " + drDL("SSN").ToString + " AND RMCID= " + drDL("RMCID").ToString)
        '    blValidate = ValidateLoanId(drDL, drTempRowsSD, drTempRowsAD, myMsgCollection)
        '    If blValidate = True Then
        '        If Trim(strStudentName) = "" Then
        '            sMessage = "Unable to find student with SSN:" & msgEntryDL.strOriginalSSN
        '            EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
        '            EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, drTempRowsSD, drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
        '            blHasException = True
        '        Else
        '            Process_DLMessage = False
        '            Try
        '                Dim strEnrollId As String = ""
        '                strEnrollId = EDDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntryDL.strOriginalSSN)
        '                strCampusId = EDDX.GetStuCampusIDbySSN(db, strCampusId, msgEntryDL.strOriginalSSN)
        '                If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
        '                    sMessage = "Unable to find student with SSN:" & msgEntryDL.strOriginalSSN
        '                    EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
        '                    EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, drTempRowsSD, drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
        '                    blHasException = True
        '                End If
        '                If Not Trim(strEnrollId) = "" Then
        '                    Try
        '                        strGetAward = EDDX.Add_faStudentAwardsFromDL(groupTrans, db, strStudentAwardID, strEnrollId, msgEntryDL, IIf(drTempRowsSD.Length > 0, drTempRowsSD.Length.ToString, 0), strStudentName, SourceToTarget)
        '                        EDDX.Check_StudentScheduleDL(db, groupTrans, strGetAward, myMsgCollection, drTempRowsSD)
        '                        If Not (strGetAward = "") And Not strGetAward.Contains("Unable") Then
        '                            For Each drSD As DataRow In drTempRowsSD
        '                                msgEntrySD = myMsgCollection.Items(Convert.ToInt32(drSD("MCID").ToString))
        '                                If msgEntrySD.blnIsParsed Then
        '                                    EDDX.Add_faAwardScheduleForDL(db, groupTrans, strAwardScheduleId, strTransactionId, strStudentAwardID, msgEntrySD, strStudentName, SourceToTarget)
        '                                    intPostedDisbRecords += 1
        '                                End If
        '                            Next
        '                            For Each drAD As DataRow In drTempRowsAD
        '                                msgEntryAD = myMsgCollection.Items(Convert.ToInt32(drAD("MCID").ToString))
        '                                If msgEntryAD.blnIsParsed Then
        '                                    strAwardScheduleId = ""
        '                                    strTransactionId = ""
        '                                    EDDX.Add_faAwardScheduleForDL(db, groupTrans, strAwardScheduleId, strTransactionId, strStudentAwardID, msgEntryAD, strStudentName, SourceToTarget)
        '                                    Dim strFSDtl = EDDX.GetFundSourceDetails(db, strFundSourceDesc, strFundSourdeId, msgEntryAD.strActualDisbursementType, msgEntryAD.strMsgType)
        '                                    If strFSDtl = "" And Not strFSDtl.Contains("Unable") Then
        '                                        'If msgEntryAD.strActualDisbursementStatus.ToUpper = "R" Or msgEntryAD.strActualDisbursementStatus.ToUpper = "Y" Then
        '                                        IsTransactionPosted = EDDX.Check_TransactionPosted(db, groupTrans, strAwardScheduleId, strTransactionId)
        '                                        If IsTransactionPosted = False Then
        '                                            EDDX.Add_saTransaction(db, groupTrans, strTransactionId, strEnrollId, strCampusId, msgEntryAD, strFundSourceDesc, strStudentName)
        '                                            EDDX.Update_faAwardScheduleForDL(db, groupTrans, strAwardScheduleId, strTransactionId)
        '                                            EDDX.Add_saPmtDisbRel(db, groupTrans, strPmtDisbId, strTransactionId, strGetAward, strAwardScheduleId, msgEntryAD)
        '                                            EDDX.Add_saPayments(db, groupTrans, strTransactionId, msgEntryAD, strStudentName)
        '                                            'End If
        '                                            'intPostedDisbRecords += 1
        '                                        Else
        '                                            EDDX.Update_faAwardScheduleForDL(db, groupTrans, strAwardScheduleId, strTransactionId)
        '                                        End If


        '                                        IsTransactionPosted = False
        '                                        intActualPostedRecords += 1
        '                                    Else
        '                                        blHasException = True
        '                                        sMessage = strFSDtl
        '                                        EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
        '                                        EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, , drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
        '                                        Exit For
        '                                    End If
        '                                End If
        '                            Next
        '                            intPostedRecords += 1
        '                        Else
        '                            EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
        '                            EDDX.BuildExceptionReport(db, strGetAward, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, drTempRowsSD, drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
        '                            blHasException = True
        '                        End If
        '                    Catch ex As System.Exception
        '                        sMessage = ex.Message.ToString
        '                        EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
        '                        EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, drTempRowsSD, drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
        '                    End Try
        '                Else
        '                    sMessage = "Unable to match data with student " & strStudentName & " enrollment records "
        '                    EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
        '                    EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, drTempRowsSD, drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
        '                    blHasException = True
        '                End If
        '            Catch e As System.Exception
        '                sMessage = e.Message.ToString
        '                EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
        '                EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, drTempRowsSD, drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
        '                blHasException = True
        '            End Try
        '        End If
        '    Else
        '        sMessage = "Loan Id in the file is not same for student with SSN:" & msgEntryDL.strOriginalSSN
        '        EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
        '        EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, drTempRowsSD, drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
        '        blHasException = True
        '    End If
        'Next
        'If blHasException = True Then
        '    strFinalMessage = "Exception Total number of records :  " & (intPostedRecords + intPostedDisbRecords + intNotPostedRecords + intNotPostedDisbRecords + intActualPostedRecords + intNotActualPostedRecords).ToString & vbLf & "Number of Direct Loan posted : " & intPostedRecords.ToString & vbLf & "Number of Direct Loan not posted : " & intNotPostedRecords.ToString & vbLf & "Number of Direct Loan Disbursements posted : " & intPostedDisbRecords.ToString & vbLf & "Number of Direct Loan Disbursements not posted : " & intNotPostedDisbRecords.ToString & vbLf & "Number of Direct Loan Actual Disbursements posted : " & intActualPostedRecords.ToString & vbLf & "Number of Direct Loan Actual Disbursements not posted : " & intNotActualPostedRecords.ToString
        'Else
        '    strFinalMessage = "Total number of records :  " & (intPostedRecords + intPostedDisbRecords + intNotPostedRecords + intNotPostedDisbRecords + intActualPostedRecords + intNotActualPostedRecords).ToString & vbLf & "Number of Direct Loan posted : " & intPostedRecords.ToString & vbLf & "Number of Direct Loan not posted : " & intNotPostedRecords.ToString & vbLf & "Number of Direct Loan Disbursements posted : " & intPostedDisbRecords.ToString & vbLf & "Number of Direct Loan Disbursements not posted : " & intNotPostedDisbRecords.ToString & vbLf & "Number of Direct Loan Actual Disbursements posted : " & intActualPostedRecords.ToString & vbLf & "Number of Direct Loan Actual Disbursements not posted : " & intNotActualPostedRecords.ToString
        'End If
        'Return strFinalMessage
    End Function
    Public Function getStudentNameAndSSN(ByVal SSN As String) As String
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        'Dim zipMask As String
        'Dim strMaskSSN As String
        'Dim dr As DataRow
        Dim strStudentName As String = (New EDDataXfer_New).GetStudentInfo(SSN)
        If InStr(strStudentName, "Student not found") >= 1 Then
            Return ""
            Exit Function
        End If
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        If SSN.ToString.Length >= 1 Then
            Return strStudentName & " (" + SSN & ")"
        Else
            Return ""
        End If
    End Function
    Private Sub BuildDtDirectLoan()
        Try
            Dim dcSSN As New DataColumn("SSN", GetType(String))
            Dim dcMCID As New DataColumn("MCID", GetType(Integer))
            Dim dcRMCID As New DataColumn("RMCID", GetType(Integer))

            dtDirectLoan.Columns.Add(dcSSN)
            dtDirectLoan.Columns.Add(dcMCID)
            dtDirectLoan.Columns.Add(dcRMCID)
        Catch ex As System.Exception
        End Try
    End Sub
    Private Sub BuildDirectLoan(ByVal strSSN As String, ByVal intMCID As String, ByVal intRMCID As String)
        If Not strSSN = "" Then
            Dim drDirectLoan As DataRow
            drDirectLoan = dtDirectLoan.NewRow()
            drDirectLoan("SSN") = strSSN
            drDirectLoan("MCID") = intMCID
            drDirectLoan("RMCID") = intRMCID
            dtDirectLoan.Rows.Add(drDirectLoan)
        End If
    End Sub
    Private Sub BuilddtPATSData()
        Try
            Dim dcSSN As New DataColumn("SSN", GetType(String))
            Dim dcMCID As New DataColumn("MCID", GetType(Integer))
            Dim dcRMCID As New DataColumn("RMCID", GetType(Integer))

            dtPATSData.Columns.Add(dcSSN)
            dtPATSData.Columns.Add(dcMCID)
            dtPATSData.Columns.Add(dcRMCID)
        Catch ex As System.Exception
        End Try
    End Sub
    Private Sub BuildPATSData(ByVal strSSN As String, ByVal intMCID As String, ByVal intRMCID As String)
        If Not strSSN = "" Then
            Dim drPATS As DataRow
            drPATS = dtPATSData.NewRow()
            drPATS("SSN") = strSSN
            drPATS("MCID") = intMCID
            drPATS("RMCID") = intRMCID
            dtPATSData.Rows.Add(drPATS)
        End If
    End Sub
    Private Sub BuilddtPATSDisb()
        Try
            Dim dcSSN As New DataColumn("SSN", GetType(String))
            Dim dcMCID As New DataColumn("MCID", GetType(Integer))
            Dim dcRMCID As New DataColumn("RMCID", GetType(Integer))

            dtPATSDisb.Columns.Add(dcSSN)
            dtPATSDisb.Columns.Add(dcMCID)
            dtPATSDisb.Columns.Add(dcRMCID)
        Catch ex As System.Exception
        End Try
    End Sub
    Private Sub BuildPATSDisb(ByVal strSSN As String, ByVal intMCID As String, ByVal intRMCID As String)
        If Not strSSN = "" Then
            Dim drPATSDisb As DataRow
            drPATSDisb = dtPATSDisb.NewRow()
            drPATSDisb("SSN") = strSSN
            drPATSDisb("MCID") = intMCID
            drPATSDisb("RMCID") = intRMCID
            dtPATSDisb.Rows.Add(drPATSDisb)
        End If
    End Sub
    Private Sub BuilddtDLSchDisb()
        Try
            Dim dcSSN As New DataColumn("SSN", GetType(String))
            Dim dcMCID As New DataColumn("MCID", GetType(Integer))
            Dim dcRMCID As New DataColumn("RMCID", GetType(Integer))

            dtDLSchDisb.Columns.Add(dcSSN)
            dtDLSchDisb.Columns.Add(dcMCID)
            dtDLSchDisb.Columns.Add(dcRMCID)
        Catch ex As System.Exception
        End Try
    End Sub
    Private Sub BuildDLSchDisb(ByVal strSSN As String, ByVal intMCID As String, ByVal intRMCID As String)
        If Not strSSN = "" Then
            Dim drDLSchDisb As DataRow
            drDLSchDisb = dtDLSchDisb.NewRow()
            drDLSchDisb("SSN") = strSSN
            drDLSchDisb("MCID") = intMCID
            drDLSchDisb("RMCID") = intRMCID
            dtDLSchDisb.Rows.Add(drDLSchDisb)
        End If
    End Sub
    Private Sub BuilddtDLActualDisb()
        Try
            Dim dcSSN As New DataColumn("SSN", GetType(String))
            Dim dcMCID As New DataColumn("MCID", GetType(Integer))
            Dim dcRMCID As New DataColumn("RMCID", GetType(Integer))

            dtDLActualDisb.Columns.Add(dcSSN)
            dtDLActualDisb.Columns.Add(dcMCID)
            dtDLActualDisb.Columns.Add(dcRMCID)
        Catch ex As System.Exception
        End Try
    End Sub
    Private Sub BuildDLActualDisb(ByVal strSSN As String, ByVal intMCID As String, ByVal intRMCID As String)
        If Not strSSN = "" Then
            Dim drDLActualDisb As DataRow
            drDLActualDisb = dtDLActualDisb.NewRow()
            drDLActualDisb("SSN") = strSSN
            drDLActualDisb("MCID") = intMCID
            drDLActualDisb("RMCID") = intRMCID
            dtDLActualDisb.Rows.Add(drDLActualDisb)
        End If
    End Sub
    Private Function GetStudentData(ByVal dr As DataRow, ByVal OriginalSSN As String) As Boolean
        Dim db As New DataAccess.DataAccess
        Dim strSQL As String = "SELECT S.LastName, S.FirstName, S.SSN FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId Where S.SSN='" + OriginalSSN + "' AND SC.SysStatusId=9 "
        Dim dsTemp As DataSet = db.RunSQLDataSet(strSQL, "StudentInfo")
        If dsTemp.Tables("StudentInfo").Rows.Count > 0 Then
            dr("LastName") = dsTemp.Tables("StudentInfo").Rows(0)("LastName").ToString
            dr("FirstName") = dsTemp.Tables("StudentInfo").Rows(0)("FirstName").ToString
            If OriginalSSN.Length = 9 Then
                dr("SSN") = FormatSSN(OriginalSSN)
            Else
                dr("SSN") = OriginalSSN
            End If
            Return True
        Else
            dr("LastName") = ""
            dr("FirstName") = ""
            If OriginalSSN.Length = 9 Then
                dr("SSN") = FormatSSN(OriginalSSN)
            Else
                dr("SSN") = OriginalSSN
            End If

            Return False
        End If
    End Function
    Public Function getExceptionReport(ByVal ExceptionGUID As String, ByVal MsgType As String) As DataSet
        Dim ds As New DataSet
        ds = (New EDDataXfer_New).getExceptionReport(ExceptionGUID, MsgType)
        Return ds
    End Function
    Public Function GetFileNames() As DataSet
        Dim ds As New DataSet
        ds = (New EDDataXfer_New).GetFileNames()
        Return ds
    End Function
    Public Function ValidateAwardId(ByVal drTempParent As DataRow, ByVal drTempChild() As DataRow, ByVal myMsgCollection As EDCollectionMessageInfos_New) As Boolean
        Dim msgEntryParent As EDMessageInfo_New
        Dim msgEntryChild As EDMessageInfo_New
        msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drTempParent("IndexID").ToString))
        For Each dr As DataRow In drTempChild
            msgEntryChild = myMsgCollection.Items(Convert.ToInt32(dr("IndexID").ToString))
            If Not msgEntryParent.strAwardId = msgEntryChild.strAwardId Then
                Return False
            End If
        Next
        Return True
    End Function
    Public Function ValidateAwardIdNew(ByVal drTempParent As DataRow, ByVal drTempChild() As DataRow) As Boolean
        Dim strParentAwardId As String
        Dim strChildAwardId As String
        strParentAwardId = drTempParent("AwardId").ToString
        For Each dr As DataRow In drTempChild
            strChildAwardId = dr("AwardId").ToString
            If Not strParentAwardId = strChildAwardId Then
                Return False
            End If
        Next
        Return True
    End Function
    Public Function ValidateLoanId(ByVal drTempParent As DataRow, ByVal drTempSD() As DataRow, ByVal drTempAD() As DataRow, ByVal myMsgCollection As EDCollectionMessageInfos_New) As Boolean
        Dim msgEntryParent As EDMessageInfo_New
        Dim msgEntrySD As EDMessageInfo_New
        Dim msgEntryAD As EDMessageInfo_New
        msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drTempParent("IndexID").ToString))
        For Each drSD As DataRow In drTempSD
            msgEntrySD = myMsgCollection.Items(Convert.ToInt32(drSD("IndexID").ToString))
            If msgEntrySD.blnIsParsed Then
                If Not msgEntryParent.strLoanId = msgEntrySD.strLoanId Then
                    Return False
                End If
            End If
        Next
        For Each drAD As DataRow In drTempSD
            msgEntryAD = myMsgCollection.Items(Convert.ToInt32(drAD("IndexID").ToString))
            If msgEntryAD.blnIsParsed Then
                If Not msgEntryParent.strLoanId = msgEntryAD.strLoanId Then
                    Return False
                End If
            End If
        Next
        Return True
    End Function
    Public Function ValidateLoanIdNew(ByVal drTempParent As DataRow, ByVal drTempSD() As DataRow, ByVal drTempAD() As DataRow) As Boolean
        For Each drSD As DataRow In drTempSD
            If Not drSD("LoanId").ToString = drTempParent("LoanId").ToString Then
                Return False
            End If
        Next
        For Each drAD As DataRow In drTempAD
            If Not drAD("LoanId").ToString = drTempParent("LoanId").ToString Then
                Return False
            End If
        Next
        Return True
    End Function
    Public Function Validate_PELLMessage(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDbTransaction, ByRef myMsgCollection As EDCollectionMessageInfos_New, ByRef dtParent As DataTable, ByVal dtChild As DataTable) As String
        Dim strStudentName As String = ""
        Dim strStudentEnrollmentID As String = ""
        Dim strStudentAwardID As String = ""
        Dim msgEntryParent As EDMessageInfo_New
        ''   Dim msgEntryChild As EDMessageInfo_New  strExceptionMessage, 
        Dim strFileNameOnly, sMessage As String
        Dim strGetAward As String = ""
        Dim strFinalMessage As String = ""
        Dim strErrorMessage As String = ""
        Dim intPostedRecords As Integer = 0
        Dim intPostedDisbRecords As Integer = 0
        Dim intNotPostedRecords As Integer = 0
        Dim intNotPostedDisbRecords As Integer = 0
        Dim drTempRows() As DataRow
        Dim strAwardScheduleId As String = ""
        Dim strFundSourceDesc As String = ""
        Dim strFundSourdeId As String = ""
        Dim strTransactionId As String = ""
        Dim strCampusId As String = ""
        Dim strPmtDisbId As String = ""
        Dim IsTransactionPosted As Boolean = False
        Dim strDBI As String = ""
        Dim blHasException As Boolean = False
        Dim blValidate As Boolean = False
        For Each drP As DataRow In dtParent.Rows
            msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drP("IndexID").ToString))
            strStudentName = getStudentNameAndSSN(msgEntryParent.strOriginalSSN.ToString)
            strDBI = msgEntryParent.strDatabaseIndicator
            Dim intStartIndex As Integer = InStrRev(msgEntryParent.strFileNameSource, "/")
            strFileNameOnly = msgEntryParent.strFileNameSource.Substring(intStartIndex)
            drTempRows = dtChild.Select("SSNOriginal = '" + msgEntryParent.strOriginalSSN + "' AND ParentIndexID= " + drP("IndexID").ToString)
            blValidate = ValidateAwardId(drP, drTempRows, myMsgCollection)
            If blValidate = True Then
                If Trim(strStudentName) = "" Then
                    blHasException = True
                    sMessage = "Unable to find student with SSN:" & msgEntryParent.strOriginalSSN
                    msgEntryParent.strErrorType = 1
                    drP("ErrorType") = 1
                Else
                    Validate_PELLMessage = False
                    Try
                        Dim strEnrollId As String = ""
                        strEnrollId = EDDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntryParent.strOriginalSSN)
                        strCampusId = EDDX.GetStuCampusIDbySSN(db, strCampusId, msgEntryParent.strOriginalSSN)
                        If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
                            sMessage = "Unable to find student with SSN:" & msgEntryParent.strOriginalSSN
                            blHasException = True
                            drP("ErrorType") = 1
                            msgEntryParent.strErrorType = 1
                        Else
                            If Not Trim(strEnrollId) = "" Then
                                Try
                                    EDDX.Check_StudentAwardAndSchedulePell(db, groupTrans, myMsgCollection, strEnrollId, drP, drTempRows)
                                Catch ex As System.Exception
                                    msgEntryParent.strErrorType = 2
                                    drP("ErrorType") = 2
                                End Try
                            Else
                                msgEntryParent.strErrorType = 1
                                drP("ErrorType") = 1
                            End If
                        End If
                    Catch e As System.Exception
                        sMessage = e.Message.ToString
                        blHasException = True
                        msgEntryParent.strErrorType = 2
                        drP("ErrorType") = 2
                    End Try
                End If
            Else
                msgEntryParent.strErrorType = 3
                drP("ErrorType") = 3
            End If
        Next
        Return strFinalMessage
    End Function
    Public Function Validate_PELLMessageNew(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDbTransaction, ByRef dtParent As DataTable, ByRef dtChild As DataTable) As String
        Dim strResult As String = ""
        Dim strStudentName As String = ""
        Dim strFinalMessage As String = ""
        Dim drTempRows() As DataRow
        Dim strCampusId As String = ""
        Dim blValidate As Boolean = False
        Dim strEnrollId As String = ""
        Dim intIndex As Integer = 0
        Dim intDisbNum As Integer = 0
        Dim intSeqNum As Integer = 0

        For Each drP As DataRow In dtParent.Rows
            intIndex = 0
            strStudentName = drP("FirstName").ToString + " " + drP("LastName").ToString
            strEnrollId = drP("StuEnrollmentId").ToString
            strCampusId = drP("CampusId").ToString
            drTempRows = dtChild.Select("SSNOriginal = '" + drP("SSNOriginal").ToString + "' AND ParentIndexID= " + drP("IndexID").ToString, "DisbNum , SeqNum ASC")
            blValidate = ValidateAwardIdNew(drP, drTempRows)
            If blValidate = True Then
                If Trim(strStudentName) = "" Then
                    drP("ErrorType") = 1
                    intIndex = 0
                    For Each drC As DataRow In drTempRows
                        intSeqNum = CType(drC("SeqNum").ToString, Integer)
                        drC("ErrorType") = 2
                        drC("Show") = 1
                        If intSeqNum > 1 Then
                            If intIndex > 0 Then
                                drTempRows(intIndex - 1)("Show") = 0
                            End If
                        End If
                        intIndex = intIndex + 1
                    Next
                Else
                    Validate_PELLMessageNew = False
                    Try
                        If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
                            drP("ErrorType") = 1
                            intIndex = 0
                            For Each drC As DataRow In drTempRows
                                intSeqNum = CType(drC("SeqNum").ToString, Integer)
                                drC("ErrorType") = 2
                                drC("Show") = 1
                                If intSeqNum > 1 Then
                                    If intIndex > 0 Then
                                        drTempRows(intIndex - 1)("Show") = 0
                                    End If
                                End If
                                intIndex = intIndex + 1
                            Next
                        Else
                            If Not Trim(strEnrollId) = "" Then
                                Try
                                    If drP("SSNOriginal").ToString = "345709377" Or drP("SSNOriginal").ToString = "339849927" Or drP("SSNOriginal").ToString = "337847024" Or drP("SSNOriginal").ToString = "356743997" Or drP("SSNOriginal").ToString = "353489171" Then
                                        Dim strBR = ""
                                    End If
                                    strResult = EDDX.Check_StudentAwardAndSchedulePellNew(db, groupTrans, drP, drTempRows)
                                    If strResult = "" Then
                                        drP("ErrorType") = 0
                                        ''drP.Delete()
                                    Else
                                        drP("ErrorType") = 2
                                        intIndex = 0
                                        For Each drC As DataRow In drTempRows
                                            intSeqNum = CType(drC("SeqNum").ToString, Integer)
                                            drC("ErrorType") = 2
                                            drC("Show") = 1
                                            If intSeqNum > 1 Then
                                                If intIndex > 0 Then
                                                    drTempRows(intIndex - 1)("Show") = 0
                                                End If
                                            End If
                                            intIndex = intIndex + 1
                                        Next
                                    End If
                                Catch ex As System.Exception
                                    drP("ErrorType") = 2
                                    intIndex = 0
                                    For Each drC As DataRow In drTempRows
                                        intSeqNum = CType(drC("SeqNum").ToString, Integer)
                                        drC("ErrorType") = 2
                                        drC("Show") = 1
                                        If intSeqNum > 1 Then
                                            If intIndex > 0 Then
                                                drTempRows(intIndex - 1)("Show") = 0
                                            End If
                                        End If
                                        intIndex = intIndex + 1
                                    Next
                                End Try
                            Else
                                drP("ErrorType") = 1
                                intIndex = 0
                                For Each drC As DataRow In drTempRows
                                    intSeqNum = CType(drC("SeqNum").ToString, Integer)
                                    drC("ErrorType") = 2
                                    drC("Show") = 1
                                    If intSeqNum > 1 Then
                                        If intIndex > 0 Then
                                            drTempRows(intIndex - 1)("Show") = 0
                                        End If
                                    End If
                                    intIndex = intIndex + 1
                                Next
                            End If
                        End If
                    Catch e As System.Exception
                        drP("ErrorType") = 2
                        intIndex = 0
                        For Each drC As DataRow In drTempRows
                            intSeqNum = CType(drC("SeqNum").ToString, Integer)
                            drC("ErrorType") = 2
                            drC("Show") = 1
                            If intSeqNum > 1 Then
                                If intIndex > 0 Then
                                    drTempRows(intIndex - 1)("Show") = 0
                                End If
                            End If
                            intIndex = intIndex + 1
                        Next
                    End Try
                End If
            Else
                drP("ErrorType") = 3
                intIndex = 0
                For Each drC As DataRow In drTempRows
                    intSeqNum = CType(drC("SeqNum").ToString, Integer)
                    drC("ErrorType") = 2
                    drC("Show") = 1
                    If intSeqNum > 1 Then
                        If intIndex > 0 Then
                            drTempRows(intIndex - 1)("Show") = 0
                        End If
                    End If
                    intIndex = intIndex + 1
                Next
            End If
        Next
        dtParent.AcceptChanges()
        dtChild.AcceptChanges()
        Return strFinalMessage
    End Function
    Public Function Validate_DLMessageNew(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDbTransaction, ByRef dtParent As DataTable, ByRef dtChildSD As DataTable, ByRef dtChildAD As DataTable) As String
        Dim strResult As String = ""
        Dim strStudentName As String = ""
        Dim strFinalMessage As String = ""
        Dim drTempRowsSD() As DataRow
        Dim drTempRowsAD() As DataRow
        Dim strCampusId As String = ""
        Dim blValidate As Boolean = False
        Dim strEnrollId As String = ""
        Dim intIndex As Integer = 0
        Dim intDisbNum As Integer = 0
        Dim intSeqNum As Integer = 0

        For Each drP As DataRow In dtParent.Rows
            intIndex = 0
            strStudentName = drP("FirstName").ToString + " " + drP("LastName").ToString
            strEnrollId = drP("StuEnrollmentId").ToString
            strCampusId = drP("CampusId").ToString
            drTempRowsSD = dtChildSD.Select("SSNOriginal = '" + drP("SSNOriginal").ToString + "' AND ParentIndexID= " + drP("IndexID").ToString, "DisbNum , SeqNum ASC")
            drTempRowsAD = dtChildAD.Select("SSNOriginal = '" + drP("SSNOriginal").ToString + "' AND ParentIndexID= " + drP("IndexID").ToString, "DisbNum , SeqNum ASC")
            blValidate = ValidateLoanIdNew(drP, drTempRowsSD, drTempRowsAD)
            If blValidate = True Then
                If Trim(strStudentName) = "" Then
                    drP("ErrorType") = 2
                    intIndex = 0
                    For Each drSD As DataRow In drTempRowsSD
                        intSeqNum = CType(drSD("SeqNum").ToString, Integer)
                        drSD("ErrorType") = 2
                        drSD("Show") = 1
                        If intSeqNum > 1 Then
                            If intIndex > 0 Then
                                drTempRowsSD(intIndex - 1)("Show") = 0
                            End If
                        End If
                        intIndex = intIndex + 1
                    Next
                    intIndex = 0
                    For Each drAD As DataRow In drTempRowsAD
                        intSeqNum = CType(drAD("SeqNum").ToString, Integer)
                        drAD("ErrorType") = 2
                        drAD("Show") = 1
                        If intSeqNum > 1 Then
                            If intIndex > 0 Then
                                drTempRowsAD(intIndex - 1)("Show") = 0
                            End If
                        End If
                        intIndex = intIndex + 1
                    Next
                Else
                    Validate_DLMessageNew = False
                    Try
                        If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
                            drP("ErrorType") = 2
                            intIndex = 0
                            For Each drSD As DataRow In drTempRowsSD
                                intSeqNum = CType(drSD("SeqNum").ToString, Integer)
                                drSD("ErrorType") = 2
                                drSD("Show") = 1
                                If intSeqNum > 1 Then
                                    If intIndex > 0 Then
                                        drTempRowsSD(intIndex - 1)("Show") = 0
                                    End If
                                End If
                                intIndex = intIndex + 1
                            Next
                            intIndex = 0
                            For Each drAD As DataRow In drTempRowsAD
                                intSeqNum = CType(drAD("SeqNum").ToString, Integer)
                                drAD("ErrorType") = 2
                                drAD("Show") = 1
                                If intSeqNum > 1 Then
                                    If intIndex > 0 Then
                                        drTempRowsAD(intIndex - 1)("Show") = 0
                                    End If
                                End If
                                intIndex = intIndex + 1
                            Next
                        Else
                            If Not Trim(strEnrollId) = "" Then
                                Try
                                    strResult = EDDX.Check_StudentAwardAndScheduleDLNew(db, groupTrans, drP, drTempRowsSD, drTempRowsAD)
                                    If strResult = "" Then
                                        drP("ErrorType") = 0
                                    End If
                                Catch ex As System.Exception
                                    drP("ErrorType") = 2
                                    intIndex = 0
                                    For Each drSD As DataRow In drTempRowsSD
                                        intSeqNum = CType(drSD("SeqNum").ToString, Integer)
                                        drSD("ErrorType") = 2
                                        drSD("Show") = 1
                                        If intSeqNum > 1 Then
                                            If intIndex > 0 Then
                                                drTempRowsSD(intIndex - 1)("Show") = 0
                                            End If
                                        End If
                                        intIndex = intIndex + 1
                                    Next
                                    intIndex = 0
                                    For Each drAD As DataRow In drTempRowsAD
                                        intSeqNum = CType(drAD("SeqNum").ToString, Integer)
                                        drAD("ErrorType") = 2
                                        drAD("Show") = 1
                                        If intSeqNum > 1 Then
                                            If intIndex > 0 Then
                                                drTempRowsAD(intIndex - 1)("Show") = 0
                                            End If
                                        End If
                                        intIndex = intIndex + 1
                                    Next
                                End Try
                            Else
                                drP("ErrorType") = 2
                                intIndex = 0
                                For Each drSD As DataRow In drTempRowsSD
                                    intSeqNum = CType(drSD("SeqNum").ToString, Integer)
                                    drSD("ErrorType") = 2
                                    drSD("Show") = 1
                                    If intSeqNum > 1 Then
                                        If intIndex > 0 Then
                                            drTempRowsSD(intIndex - 1)("Show") = 0
                                        End If
                                    End If
                                    intIndex = intIndex + 1
                                Next
                                intIndex = 0
                                For Each drAD As DataRow In drTempRowsAD
                                    intSeqNum = CType(drAD("SeqNum").ToString, Integer)
                                    drAD("ErrorType") = 2
                                    drAD("Show") = 1
                                    If intSeqNum > 1 Then
                                        If intIndex > 0 Then
                                            drTempRowsAD(intIndex - 1)("Show") = 0
                                        End If
                                    End If
                                    intIndex = intIndex + 1
                                Next
                            End If
                        End If
                    Catch e As System.Exception
                        drP("ErrorType") = 2
                        intIndex = 0
                        For Each drSD As DataRow In drTempRowsSD
                            intSeqNum = CType(drSD("SeqNum").ToString, Integer)
                            drSD("ErrorType") = 2
                            drSD("Show") = 1
                            If intSeqNum > 1 Then
                                If intIndex > 0 Then
                                    drTempRowsSD(intIndex - 1)("Show") = 0
                                End If
                            End If
                            intIndex = intIndex + 1
                        Next
                        intIndex = 0
                        For Each drAD As DataRow In drTempRowsAD
                            intSeqNum = CType(drAD("SeqNum").ToString, Integer)
                            drAD("ErrorType") = 2
                            drAD("Show") = 1
                            If intSeqNum > 1 Then
                                If intIndex > 0 Then
                                    drTempRowsAD(intIndex - 1)("Show") = 0
                                End If
                            End If
                            intIndex = intIndex + 1
                        Next
                    End Try
                End If
            Else
                drP("ErrorType") = 2
                intIndex = 0
                For Each drSD As DataRow In drTempRowsSD
                    intSeqNum = CType(drSD("SeqNum").ToString, Integer)
                    drSD("ErrorType") = 2
                    drSD("Show") = 1
                    If intSeqNum > 1 Then
                        If intIndex > 0 Then
                            drTempRowsSD(intIndex - 1)("Show") = 0
                        End If
                    End If
                    intIndex = intIndex + 1
                Next
                intIndex = 0
                For Each drAD As DataRow In drTempRowsAD
                    intSeqNum = CType(drAD("SeqNum").ToString, Integer)
                    drAD("ErrorType") = 2
                    drAD("Show") = 1
                    If intSeqNum > 1 Then
                        If intIndex > 0 Then
                            drTempRowsAD(intIndex - 1)("Show") = 0
                        End If
                    End If
                    intIndex = intIndex + 1
                Next
            End If
        Next
        dtParent.AcceptChanges()
        dtChildSD.AcceptChanges()
        dtChildAD.AcceptChanges()
        Return strFinalMessage
    End Function
    Public Sub DeleteErrorType(ByRef dtParent As DataTable, ByRef dtChild As DataTable)

    End Sub

    Public Function Validate_DLMessage(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDbTransaction, ByRef myMsgCollection As EDCollectionMessageInfos_New, ByRef dtDirectLoan As DataTable, ByVal dtDLScheduleDisp As DataTable, ByVal dtDLActuleDesp As DataTable) As String
        Dim strStudentName As String = ""
        Dim strStudentEnrollmentID As String = ""
        Dim strStudentAwardID As String = ""
        Dim strFinalMessage As String = ""
        Dim strErrorMessage As String = ""
        Dim intPostedRecords As Integer = 0
        Dim intPostedDisbRecords As Integer = 0
        Dim intNotPostedRecords As Integer = 0
        Dim intNotPostedDisbRecords As Integer = 0
        Dim intActualPostedRecords As Integer = 0
        Dim intNotActualPostedRecords As Integer = 0
        Dim msgEntryDL As EDMessageInfo_New
        'Dim msgEntrySD As EDMessageInfo_New
        'Dim msgEntryAD As EDMessageInfo_New strExceptionMessage, 
        Dim strFileNameOnly, sMessage As String
        Dim strGetAward As String = ""
        Dim drTempRowsSD() As DataRow
        Dim drTempRowsAD() As DataRow
        Dim strAwardScheduleId As String = ""
        Dim IsTransactionPosted As Boolean = False
        Dim strFundSourceDesc As String = ""
        Dim strFundSourdeId As String = ""
        Dim strTransactionId As String = ""
        Dim strCampusId As String = ""
        Dim strPmtDisbId As String = ""
        Dim blHasException As Boolean = False
        Dim blValidate As Boolean = False
        For Each drDL As DataRow In dtDirectLoan.Rows
            msgEntryDL = myMsgCollection.Items(Convert.ToInt32(drDL("IndexID").ToString))
            strStudentName = getStudentNameAndSSN(msgEntryDL.strOriginalSSN.ToString)
            Dim intStartIndex As Integer = InStrRev(msgEntryDL.strFileNameSource, "/")
            strFileNameOnly = msgEntryDL.strFileNameSource.Substring(intStartIndex)
            drTempRowsSD = dtDLScheduleDisp.Select("SSNOriginal = '" + msgEntryDL.strOriginalSSN + "' AND ParentIndexID= " + drDL("IndexID").ToString)
            drTempRowsAD = dtDLActualDisb.Select("SSNOriginal = '" + msgEntryDL.strOriginalSSN + "' AND ParentIndexID= " + drDL("IndexID").ToString)
            blValidate = ValidateLoanId(drDL, drTempRowsSD, drTempRowsAD, myMsgCollection)
            If blValidate = True Then
                If Trim(strStudentName) = "" Then
                    msgEntryDL.strErrorType = 1
                    drDL("ErrorType") = 1
                    sMessage = "Unable to find student with SSN:" & msgEntryDL.strOriginalSSN
                    blHasException = True
                Else
                    Validate_DLMessage = False
                    Try
                        Dim strEnrollId As String = ""
                        strEnrollId = EDDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntryDL.strOriginalSSN)
                        strCampusId = EDDX.GetStuCampusIDbySSN(db, strCampusId, msgEntryDL.strOriginalSSN)
                        If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
                            sMessage = "Unable to find student with SSN:" & msgEntryDL.strOriginalSSN
                            blHasException = True
                            drDL("ErrorType") = 1
                            msgEntryDL.strErrorType = 1
                        End If
                        If Not Trim(strEnrollId) = "" Then
                            Try
                                EDDX.Check_StudentAwardAndScheduleDL(db, groupTrans, myMsgCollection, strEnrollId, drDL, drTempRowsSD, drTempRowsAD)

                            Catch ex As System.Exception
                                msgEntryDL.strErrorType = 2
                                drDL("ErrorType") = 2
                            End Try
                        Else
                            blHasException = True
                            msgEntryDL.strErrorType = 2
                            drDL("ErrorType") = 2
                        End If
                    Catch e As System.Exception
                        blHasException = True
                        msgEntryDL.strErrorType = 2
                        drDL("ErrorType") = 2
                    End Try
                End If
            Else
                msgEntryDL.strErrorType = 3
                drDL("ErrorType") = 3
            End If
        Next
        Return ""
    End Function
    '' New Code By Vijay Ramteke on March 12, 2010
    Private Function ParseTMessage(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByVal IndexID As Integer, ByRef SSN As String, ByRef StuEnrollmentID As String, ByRef FirstName As String, ByRef LastName As String, ByRef CampusId As String, ByRef CampusName As String, ByRef IsInSchool As Boolean, ByRef AwardId As String, ByVal strArrRecord() As String, ByRef dr As DataRow, Optional ByVal AcademicYearId As String = "", Optional ByVal AwardStartDate As String = "", Optional ByVal AwardEndDate As String = "") As Boolean
        ParseTMessage = False
        'If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided for FAID message parse")
        Try
            ''strError = ""
            If strArrRecord.Length = 1 Then
                dr("dbIndicator") = strArrRecord(0).Trim()
                dr("IsParsed") = 0
                ParseTMessage = False
                Exit Function
            Else

                Dim strFirstName As String = ""
                Dim strLastName As String = ""
                Dim strSSN As String = ""
                Dim strCampusName As String = ""
                Dim strCampusId As String = ""
                Dim StrEnrollmentId As String = ""
                Dim InSchool As Boolean = False

                '' Code changes by Kamalesh ahuja on 9th December 2010 to Pass Loan/AwardId to GetStudentInfo Function to check if the Award/ Loan id allready exists in the database
                '' then return the corresponding enrollment id. This is to avoid conflict when the student has 2 enrollments with the same status and start date
                ''EDDX.GetStudentInfo(db, strArrRecord(8).Trim(), strFirstName, strLastName, StrEnrollmentId, strCampusId, strCampusName, InSchool)
                EDDX.GetStudentInfo(db, strArrRecord(8).Trim(), strFirstName, strLastName, StrEnrollmentId, strCampusId, strCampusName, InSchool, strArrRecord(3).Trim())
                '''''

                AwardId = strArrRecord(3).Trim()
                FirstName = strFirstName
                LastName = strLastName
                StuEnrollmentID = StrEnrollmentId
                CampusId = strCampusId
                CampusName = strCampusName
                IsInSchool = InSchool
                SSN = strArrRecord(8).Trim()

                Dim strGrantTypeDesc As String = ""
                Dim strGrantType As String = ""
                strGrantType = strArrRecord(4).Trim()
                If strGrantType = "" Or strGrantType = "P" Then
                    strGrantTypeDesc = "PELL"
                ElseIf strGrantType = "A" Then
                    strGrantTypeDesc = "ACG"
                ElseIf strGrantType = "S" Or strGrantType = "T" Then
                    strGrantTypeDesc = "SMART"
                End If

                Dim dbAwardAmount As Decimal = 0.0
                Try
                    dbAwardAmount = Convert.ToDecimal(strArrRecord(2).Trim())
                Catch ex As Exception
                    dbAwardAmount = 0.0
                End Try

                dr("IndexID") = IndexID
                dr("FirstName") = strFirstName
                dr("LastName") = strLastName
                dr("SSN") = IIf(strArrRecord(8).Trim().Length = 9, FormatSSN(strArrRecord(8).Trim()), strArrRecord(8).Trim())
                dr("Campus") = strCampusName
                dr("StuEnrollmentId") = StrEnrollmentId
                dr("CampusId") = strCampusId
                dr("dbIndicator") = strArrRecord(0).Trim()
                dr("dbFilter") = strArrRecord(1).Trim()
                dr("AwardAmount") = dbAwardAmount
                dr("AwardId") = strArrRecord(3).Trim()
                dr("GrantType") = strArrRecord(4).Trim()
                dr("GrantTypeDesc") = strGrantTypeDesc
                dr("OriginationStatus") = strArrRecord(5).Trim()
                dr("AddDate") = FormatDate(strArrRecord(6).Trim())
                dr("AddTime") = FormatTime(strArrRecord(7).Trim())
                dr("SSNOriginal") = strArrRecord(8).Trim()
                dr("UpdatedDate") = strArrRecord(9).Trim()
                dr("UpdatedTime") = strArrRecord(10).Trim()
                dr("AcademicYearId") = AcademicYearId
                dr("AwardStartDate") = AwardStartDate
                dr("AwardEndDate") = AwardEndDate
                'dr("IsOverrideAdvDateWithEDExpDate") = IsOverrideAdvDateWithEDExpDate
                'dr("IsOverrideAdvAmtWithEDExpAm") = IsOverrideAdvAmtWithEDExpAmt
                'dr("IsPostPayment") = IsPostPayment
                'dr("IsPayOutOfSchoolStudent") = IsPayOutOfSchoolStudent
                'dr("IsUseExpDate") = IsUseExpDate
                'dr("SpecifiedDate") = SpecifiedDate
                dr("IsInSchool") = InSchool
                dr("IsParsed") = 1
            End If
            ParseTMessage = True
        Catch e As ArgumentNullException
            ''strError = e.Message
        Catch e As ArgumentOutOfRangeException
            ''strError = e.Message
        Catch e As System.Exception
            ''strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseSMessage(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByVal IndexID As Integer, ByVal ParentIndexID As Integer, ByVal SSN As String, ByVal FirstName As String, ByVal LastName As String, ByVal StuEnrollmentid As String, ByVal CampusId As String, ByVal CampusName As String, ByVal IsInSchool As Boolean, ByVal AwardId As String, ByVal strArrRecord() As String, ByRef dr As DataRow, Optional ByVal AcademicYearId As String = "", Optional ByVal AwardStartDate As String = "", Optional ByVal AwardEndDate As String = "") As Boolean

        ParseSMessage = False
        Try
            ''strError = ""
            If strArrRecord.Length = 1 Then
                ''strDatabaseIndicator = strArrRecord(0).Trim()
                dr("dbIndicator") = strArrRecord(0).Trim()
                dr("IsParsed") = 0
                ParseSMessage = False
                Exit Function
            Else
                Dim dbAccDisbAmt As Decimal = 0.0
                Try
                    dbAccDisbAmt = Convert.ToDecimal(strArrRecord(2).Trim())
                Catch ex As Exception
                    dbAccDisbAmt = 0.0
                End Try
                Dim dbSubDisbAmt As Decimal = 0.0
                Try
                    dbSubDisbAmt = Convert.ToDecimal(strArrRecord(8).Trim())
                Catch ex As Exception
                    dbSubDisbAmt = 0.0
                End Try

                dr("IndexID") = IndexID
                dr("ParentIndexID") = ParentIndexID
                dr("FirstName") = FirstName
                dr("LastName") = LastName
                dr("SSN") = IIf(SSN.Trim().Length = 9, FormatSSN(SSN.Trim()), SSN.Trim())
                dr("SSNOriginal") = SSN
                dr("AwardId") = AwardId
                dr("Campus") = CampusName
                dr("StuEnrollmentId") = StuEnrollmentid
                dr("CampusId") = CampusId
                dr("dbIndicator") = strArrRecord(0).Trim()
                dr("dbFilter") = strArrRecord(1).Trim()
                dr("AccDisbAmt") = dbAccDisbAmt
                dr("ActionStatus") = strArrRecord(3).Trim()
                dr("DisbDate") = FormatDate(strArrRecord(4).Trim())
                dr("DisbNum") = strArrRecord(5).Trim()
                dr("DisbRelInd") = strArrRecord(6).Trim()
                dr("SeqNum") = strArrRecord(7).Trim()
                dr("SubDisbAmt") = dbSubDisbAmt
                dr("AcademicYrId") = AcademicYearId
                dr("AwardYrStartDate") = AwardStartDate
                dr("AwardYrEndDate") = AwardEndDate
                'dr("IsOverrideAdvDateWithEDExpDate") = IsOverrideAdvDateWithEDExpDate
                'dr("IsOverrideAdvAmtWithEDExpAmt") = IsOverrideAdvAmtWithEDExpAmt
                'dr("IsPostPayment") = IsPostPayment
                'dr("IsPayOutOfSchoolStudent") = IsPayOutOfSchoolStudent
                'dr("IsUseExpDate") = IsUseExpDate
                'dr("SpecifiedDate") = SpecifiedDate
                dr("IsInSchool") = IsInSchool
                dr("IsParsed") = 1
            End If
            ParseSMessage = True
        Catch e As ArgumentNullException
            ''strError = e.Message
        Catch e As ArgumentOutOfRangeException
            ''strError = e.Message
        Catch e As System.Exception
            ''strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseCMessage(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByVal IndexID As Integer, ByVal ParentIndexID As Integer, ByVal SSN As String, ByVal FirstName As String, ByVal LastName As String, ByVal StuEnrollmentid As String, ByVal CampusId As String, ByVal CampusName As String, ByVal IsInSchool As Boolean, ByVal AwardId As String, ByVal strArrRecord() As String, ByRef dr As DataRow, Optional ByVal AcademicYearId As String = "", Optional ByVal AwardStartDate As String = "", Optional ByVal AwardEndDate As String = "") As Boolean
        ParseCMessage = False
        Try
            ''strError = ""
            If strArrRecord.Length = 1 Then
                ''strDatabaseIndicator = strArrRecord(0).Trim()
                dr("dbIndicator") = strArrRecord(0).Trim()
                dr("IsParsed") = 0
                ParseCMessage = False
                Exit Function
            Else
                Dim dbAccDisbAmt As Decimal = 0.0
                Try
                    dbAccDisbAmt = Convert.ToDecimal(strArrRecord(2).Trim())
                Catch ex As Exception
                    dbAccDisbAmt = 0.0
                End Try
                Dim dbSubDisbAmt As Decimal = 0.0
                Try
                    dbSubDisbAmt = Convert.ToDecimal(strArrRecord(8).Trim())
                Catch ex As Exception
                    dbSubDisbAmt = 0.0
                End Try

                dr("IndexID") = IndexID
                dr("ParentIndexID") = ParentIndexID
                dr("FirstName") = FirstName
                dr("LastName") = LastName
                dr("SSN") = IIf(SSN.Trim().Length = 9, FormatSSN(SSN.Trim()), SSN.Trim())
                dr("SSNOriginal") = SSN
                dr("AwardId") = AwardId
                dr("Campus") = CampusName
                dr("StuEnrollmentId") = StuEnrollmentid
                dr("CampusId") = CampusId
                dr("dbIndicator") = strArrRecord(0).Trim()
                dr("dbFilter") = strArrRecord(1)
                dr("AccDisbAmt") = dbAccDisbAmt
                dr("DisbDate") = FormatDate(strArrRecord(3).Trim())
                dr("DisbNum") = strArrRecord(4).Trim()
                dr("DisbRelInd") = strArrRecord(5).Trim()
                dr("SeqNum") = strArrRecord(6).Trim()
                dr("ActionStatus") = strArrRecord(7).Trim()
                dr("SubDisbAmt") = dbSubDisbAmt
                dr("AcademicYrId") = AcademicYearId
                dr("AwardYrStartDate") = AwardStartDate
                dr("AwardYrEndDate") = AwardEndDate
                'dr("IsOverrideAdvDateWithEDExpDate") = IsOverrideAdvDateWithEDExpDate
                'dr("IsOverrideAdvAmtWithEDExpAmt") = IsOverrideAdvAmtWithEDExpAmt
                'dr("IsPostPayment") = IsPostPayment
                'dr("IsPayOutOfSchoolStudent") = IsPayOutOfSchoolStudent
                'dr("IsUseExpDate") = IsUseExpDate
                'dr("SpecifiedDate") = SpecifiedDate
                dr("IsInSchool") = IsInSchool
                dr("IsParsed") = 1
            End If
            ParseCMessage = True
        Catch e As ArgumentNullException
            ''strError = e.Message
        Catch e As ArgumentOutOfRangeException
            ''strError = e.Message
        Catch e As System.Exception
            ''strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseHMessage(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByVal IndexID As Integer, ByRef SSN As String, ByRef StuEnrollmentID As String, ByRef FirstName As String, ByRef LastName As String, ByRef CampusId As String, ByRef CampusName As String, ByRef IsInSchool As Boolean, ByRef AwardId As String, ByVal strArrRecord() As String, ByRef dr As DataRow, Optional ByVal AcademicYearId As String = "", Optional ByVal AwardStartDate As String = "", Optional ByVal AwardEndDate As String = "") As Boolean

        ParseHMessage = False
        Try
            ''_strError = ""
            If strArrRecord.Length = 1 Then
                ''strDatabaseIndicator = strArrRecord(0).Trim()
                dr("dbIndicator") = strArrRecord(0).Trim()
                dr("IsParsed") = 0
                ParseHMessage = False
                Exit Function

            Else
                Dim strFirstName As String = ""
                Dim strLastName As String = ""
                Dim strSSN As String = ""
                Dim strCampusName As String = ""
                Dim strCampusId As String = ""
                Dim StrEnrollmentId As String = ""
                Dim InSchool As Boolean = False

                '' Code changes by Kamalesh ahuja on 9th December 2010 to Pass Loan/AwardId to GetStudentInfo Function to check if the Award/ Loan id allready exists in the database
                '' then return the corresponding enrollment id. This is to avoid conflict when the student has 2 enrollments with the same status and start date
                ''EDDX.GetStudentInfo(db, strArrRecord(8).Trim(), strFirstName, strLastName, StrEnrollmentId, strCampusId, strCampusName, InSchool)
                EDDX.GetStudentInfo(db, strArrRecord(8).Trim(), strFirstName, strLastName, StrEnrollmentId, strCampusId, strCampusName, InSchool, strArrRecord(3).Trim())

                FirstName = strFirstName
                LastName = strLastName
                StuEnrollmentID = StrEnrollmentId
                CampusId = strCampusId
                CampusName = strCampusName
                IsInSchool = InSchool
                SSN = strArrRecord(8).Trim()
                AwardId = strArrRecord(3).Trim()

                Dim dbAwardAmount As Decimal = 0.0
                Try
                    dbAwardAmount = Convert.ToDecimal(strArrRecord(2).Trim())
                Catch ex As Exception
                    dbAwardAmount = 0.0
                End Try

                dr("IndexID") = IndexID
                dr("FirstName") = strFirstName
                dr("LastName") = strLastName
                dr("SSN") = IIf(strArrRecord(7).Trim().Length = 9, FormatSSN(strArrRecord(7).Trim()), strArrRecord(7).Trim())
                dr("Campus") = strCampusName
                dr("StuEnrollmentId") = StrEnrollmentId
                dr("CampusId") = strCampusId
                dr("dbIndicator") = strArrRecord(0).Trim()
                dr("dbFilter") = strArrRecord(1).Trim()
                dr("AwardAmount") = dbAwardAmount
                dr("AwardId") = strArrRecord(3).Trim()
                dr("OriginationStatus") = strArrRecord(4).Trim()
                dr("AddDate") = FormatDate(strArrRecord(5).Trim())
                dr("AddTime") = FormatTime(strArrRecord(6).Trim())
                dr("SSNOriginal") = strArrRecord(7).Trim()
                dr("UpdatedDate") = strArrRecord(8).Trim()
                dr("UpdatedTime") = strArrRecord(9).Trim()
                dr("GrantType") = "TT"
                dr("GrantTypeDesc") = "TEACH"
                dr("AcademicYearId") = AcademicYearId
                dr("AwardStartDate") = AwardStartDate
                dr("AwardEndDate") = AwardEndDate
                'dr("IsOverrideAdvDateWithEDExpDate") = IsOverrideAdvDateWithEDExpDate
                'dr("IsOverrideAdvAmtWithEDExpAm") = IsOverrideAdvAmtWithEDExpAmt
                'dr("IsPostPayment") = IsPostPayment
                'dr("IsPayOutOfSchoolStudent") = IsPayOutOfSchoolStudent
                'dr("IsUseExpDate") = IsUseExpDate
                'dr("SpecifiedDate") = SpecifiedDate
                dr("IsInSchool") = InSchool
                dr("IsParsed") = 1
            End If
            ParseHMessage = True
        Catch e As ArgumentNullException
            ''strError = e.Message
        Catch e As ArgumentOutOfRangeException
            ''strError = e.Message
        Catch e As System.Exception
            ''strError = e.Message
            Throw
        End Try
    End Function
    Public Function ProcessEDFileNew(ByRef clsflFile As EDFileInfo_New, ByRef dtParent As DataTable, ByRef dtChild As DataTable, Optional ByRef dtChild2 As DataTable = Nothing, Optional ByVal SourceFolderLocation As String = "", Optional ByRef AcademicYearId As String = "", Optional ByVal AwardStartDate As String = "", Optional ByVal AwardEndDate As String = "") As Boolean
        Dim strRecord As String = ""
        Dim intLoopCount As Integer = 0
        Dim intBytesRead As Integer = 0
        Dim Request As WebRequest
        Dim ResponseStream As Stream
        Dim Response1 As WebResponse
        Dim myCredentials As NetworkCredential
        Dim strProcessRecord As String
        Dim EDDX As New EDDataXfer_New
        Dim EDDA As New DataAccess.DataAccess
        Dim RowIndexID As Integer = 0
        Dim IndexID As Integer = 0
        Dim ParentIndexID As Integer = 0

        ProcessEDFileNew = False
        EDDA.CommandTimeout = 10800

        Try
            If clsflFile.datafromremotecomputer = True Then
                myCredentials = New NetworkCredential(clsflFile.networkuser, clsflFile.networkPassword, "")
            End If
            Request = System.Net.FileWebRequest.Create(clsflFile.strFileName) 'WebRequest.Create(strFileName) 'System.Net.FileWebRequest.Create("\\qaw2k\ESPFileIn\CLFAHEAD.000") 
            Request.Headers.Add("Translate: f")
            Request.Method = "GET"
            If clsflFile.datafromremotecomputer = True Then
                Request.Credentials = myCredentials
            Else
                Request.Credentials = CredentialCache.DefaultCredentials
            End If

            Response1 = CType(Request.GetResponse, WebResponse)
            ResponseStream = Response1.GetResponseStream
            Dim objIn As StreamReader
            objIn = New StreamReader(ResponseStream, System.Text.Encoding.Default)
            Dim contents As String
            'Dim intLoop As Integer = 1
            Do
                Try
                    contents = objIn.ReadLine().Trim()
                Catch ex As Exception
                    contents = Nothing
                    Exit Do
                End Try
                If Not contents + "" = "" Then
                    strRecord = contents
                    Dim SSNOriginal As String
                    Dim AwardId As String
                    Dim StuEnrollmentId As String
                    Dim CampusId As String
                    Dim CampusName As String
                    Dim FirstName As String
                    Dim LastName As String
                    Dim IsInSchool As Boolean
                    Dim ArrRecord() As String
                    Dim splitChar As Char = ","
                    Dim dbRebateAmount As Decimal = 0.0
                    Dim AddDate As String
                    Dim AddTime As String
                    If strRecord.Length > 1 Then
                        ArrRecord = strRecord.Split(splitChar)
                    Else
                        ArrRecord = strRecord.Split(" ")
                    End If

                    If clsflFile.strMsgType = "PELL" Then
                        If ArrRecord.Length > 0 Then
                            strProcessRecord = ArrRecord(0)
                            Select Case strProcessRecord.Trim()
                                Case "T"
                                    Dim drP As DataRow
                                    drP = dtParent.NewRow()
                                    If ParseTMessage(EDDX, EDDA, IndexID, SSNOriginal, StuEnrollmentId, FirstName, LastName, CampusId, CampusName, IsInSchool, AwardId, ArrRecord, drP, AcademicYearId, AwardStartDate, AwardEndDate) = True Then
                                        dtParent.Rows.Add(drP)
                                    Else

                                    End If
                                    ParentIndexID = IndexID
                                    IndexID += 1
                                Case "H"
                                    Dim drP As DataRow
                                    drP = dtParent.NewRow()
                                    If ParseHMessage(EDDX, EDDA, IndexID, SSNOriginal, StuEnrollmentId, FirstName, LastName, CampusId, CampusName, IsInSchool, AwardId, ArrRecord, drP, AcademicYearId, AwardStartDate, AwardEndDate) = True Then
                                        dtParent.Rows.Add(drP)
                                    End If
                                    ParentIndexID = IndexID
                                    IndexID += 1
                                Case "S"
                                    Dim drC As DataRow
                                    drC = dtChild.NewRow()
                                    If ParseSMessage(EDDX, EDDA, IndexID, ParentIndexID, SSNOriginal, FirstName, LastName, StuEnrollmentId, CampusId, CampusName, IsInSchool, AwardId, ArrRecord, drC, AcademicYearId, AwardStartDate, AwardEndDate) = True Then
                                        dtChild.Rows.Add(drC)
                                    End If
                                    IndexID += 1
                                Case "C"
                                    Dim drC As DataRow
                                    drC = dtChild.NewRow()
                                    If ParseCMessage(EDDX, EDDA, IndexID, ParentIndexID, SSNOriginal, FirstName, LastName, StuEnrollmentId, CampusId, CampusName, IsInSchool, AwardId, ArrRecord, drC, AcademicYearId, AwardStartDate, AwardEndDate) = True Then
                                        dtChild.Rows.Add(drC)
                                    End If
                                    IndexID += 1
                            End Select
                        End If
                    Else
                        '12-March-2010 For DL
                        If ArrRecord.Length > 0 Then
                            strProcessRecord = ArrRecord(0)
                            Select Case strProcessRecord.Trim()
                                Case "A"
                                    ParseAMessage(AddDate, AddTime, ArrRecord)
                                Case "D"
                                    Dim drP As DataRow
                                    drP = dtParent.NewRow()
                                    If ParseDMessage(EDDX, EDDA, IndexID, SSNOriginal, StuEnrollmentId, FirstName, LastName, CampusId, CampusName, IsInSchool, AwardId, ArrRecord, drP, AcademicYearId, AwardStartDate, AwardEndDate, AddDate, AddTime) = True Then
                                        If AcademicYearId = "" Then
                                            GetAwardYear(EDDX, drP("LoanId").ToString, AcademicYearId)
                                            drP("AcademicYearId") = AcademicYearId
                                        Else
                                            drP("AcademicYearId") = AcademicYearId
                                        End If
                                    End If
                                    dtParent.Rows.Add(drP)
                                    RowIndexID += 1
                                    ParentIndexID = IndexID
                                    IndexID += 1
                                Case "M"
                                    If ArrRecord.Length = 13 Then
                                        If dtParent.Rows(RowIndexID - 1)("IsParsed").ToString = "0" Then
                                            ParseDMMessage(EDDX, EDDA, RowIndexID - 1, ParentIndexID, SSNOriginal, StuEnrollmentId, FirstName, LastName, CampusId, CampusName, IsInSchool, AwardId, ArrRecord(12).ToString, dtParent, AcademicYearId, ArrRecord(11))
                                        End If
                                    End If
                                    Dim drC As DataRow
                                    drC = dtChild2.NewRow()
                                    If ParseMMessage(EDDX, EDDA, IndexID, ParentIndexID, SSNOriginal, FirstName, LastName, StuEnrollmentId, CampusId, CampusName, IsInSchool, AwardId, ArrRecord, drC, AcademicYearId, AwardStartDate, AwardEndDate) = True Then
                                        dtChild2.Rows.Add(drC)
                                        IndexID += 1
                                    End If
                                Case "N"
                                    If ArrRecord.Length = 10 Then
                                        If dtParent.Rows(RowIndexID - 1)("IsParsed").ToString = "0" Then
                                            ParseDNMessage(EDDX, EDDA, RowIndexID - 1, ParentIndexID, SSNOriginal, StuEnrollmentId, FirstName, LastName, CampusId, CampusName, IsInSchool, AwardId, ArrRecord(6).ToString, dtParent, AcademicYearId, "")
                                        End If
                                    End If
                                    Dim drC As DataRow
                                    drC = dtChild.NewRow()
                                    If ParseNMessage(EDDX, EDDA, IndexID, ParentIndexID, RowIndexID, SSNOriginal, FirstName, LastName, StuEnrollmentId, CampusId, CampusName, IsInSchool, AwardId, ArrRecord, drC, AcademicYearId, , , dtParent) = True Then
                                        dtChild.Rows.Add(drC)
                                        IndexID += 1
                                    End If
                            End Select
                        End If
                        '12-March-2010 For DL
                    End If

                    ''myMsgCollection.Add(clsflFile.strMsgType, strRecord, clsflFile.strFileName, AcademicYearId, AwardStartDate, AwardEndDate, IsOverrideAdvDateWithEDExpDate, IsOverrideAdvAmtWithEDExpAmt, IsPostPayment, IsPayOutOfSchoolStudent, IsUseExpDate, SpecifiedDate)
                End If
            Loop Until contents Is Nothing
            objIn.Close()
            'End If

            ''ResponseStream.Flush()
            ResponseStream.Close()
            Response1.Close()
            ProcessEDFileNew = True
        Catch e As ArgumentNullException
            ProcessEDFileNew = False
            ''Throw
        Catch e As ArgumentOutOfRangeException
            ProcessEDFileNew = False
            ''Throw
        Catch e As System.Exception
            ProcessEDFileNew = False
            ''Throw
        End Try

    End Function
    Public Function FormatSSN(ByRef SSNToFormat As String) As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        Return facInputMasks.ApplyMask(strSSNMask, "*****" & SSNToFormat.Substring(5))
    End Function
    Public Function FormatDate(ByRef DateToFormat As String) As String
        Dim strDD As String
        Dim strMM As String
        Dim strYYYY As String
        Try
            strYYYY = DateToFormat.Substring(0, 4)
            strMM = DateToFormat.Substring(4, 2)
            strDD = DateToFormat.Substring(6, 2)
            Return strMM + "/" + strDD + "/" + strYYYY
        Catch ex As Exception
            Return DateToFormat
        End Try

    End Function
    Public Function FormatTime(ByRef TimeToFormat As String) As String
        Dim strHH As String
        Dim strMM As String
        Dim strSS As String
        Try
            strHH = TimeToFormat.Substring(0, 2)
            strMM = TimeToFormat.Substring(2, 2)
            strSS = TimeToFormat.Substring(4, 2)
            Return strHH + ":" + strMM + ":" + strSS
        Catch ex As Exception
            Return TimeToFormat
        End Try
    End Function
    '' New Code By Vijay Ramteke on March 12, 2010
    '' New Code By Vijay Ramteke on April 05, 2010
    Private Function ParseAMessage(ByRef AddDate As String, ByRef AddTime As String, ByVal strArrRecord() As String) As Boolean
        ParseAMessage = False
        'If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided for FAID message parse")
        Try
            ''strError = ""
            If strArrRecord.Length = 1 Then
                AddDate = ""
                AddTime = ""
                ParseAMessage = False
                Exit Function
            Else
                AddDate = FormatDate(strArrRecord(2))
                AddTime = FormatTime(strArrRecord(3))
            End If
            ParseAMessage = True
        Catch e As ArgumentNullException
            ''strError = e.Message
        Catch e As ArgumentOutOfRangeException
            ''strError = e.Message
        Catch e As System.Exception
            ''strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseDMessage(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByVal IndexID As Integer, ByRef SSN As String, ByRef StuEnrollmentID As String, ByRef FirstName As String, ByRef LastName As String, ByRef CampusId As String, ByRef CampusName As String, ByRef IsInSchool As Boolean, ByRef LoanId As String, ByVal strArrRecord() As String, ByRef dr As DataRow, Optional ByVal AcademicYearId As String = "", Optional ByVal AwardStartDate As String = "", Optional ByVal AwardEndDate As String = "", Optional ByVal AddDate As String = "", Optional ByVal AddTime As String = "") As Boolean
        ParseDMessage = False
        'If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided for FAID message parse")
        Try
            ''strError = ""
            If strArrRecord.Length = 1 Or strArrRecord.Length = 2 Or strArrRecord.Length = 3 Then
                dr("dbIndicator") = strArrRecord(0).Trim()
                dr("IsParsed") = 0
                ParseDMessage = False
                Exit Function
            Else

                Dim strFirstName As String = ""
                Dim strLastName As String = ""
                Dim strSSN As String = ""
                Dim strCampusName As String = ""
                Dim strCampusId As String = ""
                Dim StrEnrollmentId As String = ""
                Dim InSchool As Boolean = False

                '' Code changes by Kamalesh ahuja on 5th December 2010 to Pass Loan/AwardId to GetStudentInfo Function to check if the Award/ Loan id allready exists in the database
                '' then return the corresponding enrollment id. This is to avoid conflict when the student has 2 enrollments with the same status and start date
                ''EDDX.GetStudentInfo(db, strArrRecord(10).Trim(), strFirstName, strLastName, StrEnrollmentId, strCampusId, strCampusName, InSchool)
                EDDX.GetStudentInfo(db, strArrRecord(10).Trim(), strFirstName, strLastName, StrEnrollmentId, strCampusId, strCampusName, InSchool, strArrRecord(4).Trim())

                LoanId = strArrRecord(4).Trim()
                FirstName = strFirstName
                LastName = strLastName
                StuEnrollmentID = StrEnrollmentId
                CampusId = strCampusId
                CampusName = strCampusName
                IsInSchool = InSchool
                SSN = strArrRecord(10).Trim()
                Dim strLoanTypeDesc As String = ""
                Dim strLoanType As String = ""
                strLoanType = strArrRecord(8).Trim()
                If strLoanType.ToUpper = "P" Then
                    strLoanTypeDesc = "DL-PLUS"
                ElseIf strLoanType.ToUpper = "S" Then
                    strLoanTypeDesc = "DL-SUB"
                ElseIf strLoanType.ToUpper = "U" Then
                    strLoanTypeDesc = "DL-UNSUB"
                Else
                    strLoanTypeDesc = ""
                End If
                dr("IndexID") = IndexID
                dr("FirstName") = strFirstName
                dr("LastName") = strLastName
                dr("SSN") = IIf(strArrRecord(10).Trim().Length = 9, FormatSSN(strArrRecord(10).Trim()), strArrRecord(10).Trim())
                dr("Campus") = strCampusName
                dr("StuEnrollmentId") = StrEnrollmentId
                dr("CampusId") = strCampusId
                dr("dbFilter") = strArrRecord(1).Trim()
                dr("LoanType") = strArrRecord(8).Trim()
                dr("LoanTypeDesc") = strLoanTypeDesc
                dr("LoanId") = strArrRecord(4).Trim()
                dr("AddDate") = AddDate
                dr("AddTime") = AddTime
                Dim dbGrossAmount As Double
                Dim dbFeesPer As Double
                Try
                    dbGrossAmount = Convert.ToDouble(strArrRecord(2).Trim())
                Catch ex As Exception
                    dbGrossAmount = 0.0
                End Try
                Try
                    dbFeesPer = Convert.ToDouble(strArrRecord(3).Trim())
                Catch ex As Exception
                    dbFeesPer = 0.0
                End Try
                Dim dbFeesAmount As Double = (dbFeesPer * dbGrossAmount) / 100
                dr("GrossAmount") = dbGrossAmount.ToString("0.00")
                dr("Fees") = (Math.Round(dbFeesAmount)).ToString("0.00")
                dr("NetAmount") = (dbGrossAmount - Math.Round(dbFeesAmount)).ToString("0.00")
                dr("AcademicYearId") = ""
                dr("LoanStartDate") = FormatDate(strArrRecord(6).Trim())
                dr("LoanEndDate") = FormatDate(strArrRecord(5).Trim())
                dr("ErrorType") = 0
                dr("dbIndicator") = strArrRecord(0).Trim()
                dr("SSNOriginal") = strArrRecord(10).Trim()
                dr("UpdatedDate") = strArrRecord(11).Trim()
                dr("UpdatedTime") = strArrRecord(12).Trim()
                dr("IsInSchool") = InSchool
                dr("IsParsed") = 1
            End If
            ParseDMessage = True
        Catch e As ArgumentNullException
            ''strError = e.Message
        Catch e As ArgumentOutOfRangeException
            ''strError = e.Message
        Catch e As System.Exception
            ''strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseMMessage(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByVal IndexID As Integer, ByVal ParentIndexID As Integer, ByVal SSN As String, ByVal FirstName As String, ByVal LastName As String, ByVal StuEnrollmentid As String, ByVal CampusId As String, ByVal CampusName As String, ByVal IsInSchool As Boolean, ByVal AwardId As String, ByVal strArrRecord() As String, ByRef dr As DataRow, Optional ByVal AcademicYearId As String = "", Optional ByVal AwardStartDate As String = "", Optional ByVal AwardEndDate As String = "") As Boolean
        ParseMMessage = False
        Try
            ''strError = ""
            If strArrRecord.Length = 1 Or strArrRecord.Length = 2 Or strArrRecord.Length = 3 Then
                ''strDatabaseIndicator = strArrRecord(0).Trim()
                dr("dbIndicator") = strArrRecord(0).Trim()
                dr("IsParsed") = 0
                ParseMMessage = False
                Exit Function
            Else
                Dim dbGrossAmt As Decimal = 0.0
                Try
                    dbGrossAmt = Convert.ToDecimal(strArrRecord(3).Trim())
                Catch ex As Exception
                    dbGrossAmt = 0.0
                End Try
                Dim dbFeesAmt As Decimal = 0.0
                Try
                    dbFeesAmt = Convert.ToDecimal(strArrRecord(5).Trim())
                Catch ex As Exception
                    dbFeesAmt = 0.0
                End Try
                Dim dbRebAmt As Decimal = 0.0
                Try
                    dbRebAmt = Convert.ToDecimal(strArrRecord(4).Trim())
                Catch ex As Exception
                    dbRebAmt = 0.0
                End Try
                Dim dbNetAmt As Decimal = 0.0
                Try
                    dbNetAmt = Convert.ToDecimal(strArrRecord(7).Trim())
                Catch ex As Exception
                    dbNetAmt = 0.0
                End Try

                dr("IndexID") = IndexID
                dr("ParentIndexID") = ParentIndexID
                dr("FirstName") = FirstName
                dr("LastName") = LastName
                dr("SSN") = IIf(SSN.Trim().Length = 9, FormatSSN(SSN.Trim()), SSN.Trim())
                dr("SSNOriginal") = SSN
                dr("StuEnrollmentId") = StuEnrollmentid
                dr("CampusId") = CampusId
                dr("Campus") = CampusName
                dr("LoanId") = strArrRecord(12)
                dr("DisbDate") = FormatDate(strArrRecord(2).Trim())
                dr("DisbRelInd") = strArrRecord(10).Trim()
                dr("DisbNum") = strArrRecord(8).Trim()
                dr("SeqNum") = strArrRecord(9).Trim()
                dr("GrossAmt") = dbGrossAmt.ToString("0.00")
                dr("Fees") = (dbFeesAmt - dbRebAmt).ToString("0.00")
                dr("NetAmt") = dbNetAmt.ToString("0.00")
                dr("dbIndicator") = strArrRecord(0).Trim()
                dr("dbFilter") = strArrRecord(1)
                dr("AcademicYrId") = AcademicYearId
                dr("ErrorType") = 0
                dr("Show") = 0
                dr("IsInSchool") = IsInSchool
                dr("IsParsed") = 1
            End If
            ParseMMessage = True
        Catch e As ArgumentNullException
            ''strError = e.Message
        Catch e As ArgumentOutOfRangeException
            ''strError = e.Message
        Catch e As System.Exception
            ''strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseNMessage(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByVal IndexID As Integer, ByVal ParentIndexID As Integer, ByVal RowIndexID As Integer, ByVal SSN As String, ByVal FirstName As String, ByVal LastName As String, ByVal StuEnrollmentid As String, ByVal CampusId As String, ByVal CampusName As String, ByVal IsInSchool As Boolean, ByVal LoanId As String, ByVal strArrRecord() As String, ByRef dr As DataRow, Optional ByVal AcademicYearId As String = "", Optional ByVal AwardStartDate As String = "", Optional ByVal AwardEndDate As String = "", Optional ByRef dtParent As DataTable = Nothing) As Boolean
        ParseNMessage = False
        Try
            ''strError = ""
            If strArrRecord.Length = 1 Or strArrRecord.Length = 2 Or strArrRecord.Length = 3 Then
                ''strDatabaseIndicator = strArrRecord(0).Trim()
                dr("dbIndicator") = strArrRecord(0).Trim()
                dr("IsParsed") = 0
                ParseNMessage = False
                Exit Function
            Else

                dr("IndexID") = IndexID
                dr("ParentIndexID") = ParentIndexID
                dr("FirstName") = FirstName
                dr("LastName") = LastName
                dr("SSN") = IIf(SSN.Trim().Length = 9, FormatSSN(SSN.Trim()), SSN.Trim())
                dr("SSNOriginal") = SSN
                dr("StuEnrollmentId") = StuEnrollmentid
                dr("CampusId") = CampusId
                dr("Campus") = CampusName
                dr("LoanId") = LoanId
                dr("DisbDate") = FormatDate(strArrRecord(2).Trim())
                dr("DisbRelInd") = strArrRecord(9).Trim()
                dr("DisbNum") = strArrRecord(8).Trim()
                dr("SeqNum") = "1"
                Dim dbGrossAmount As Double
                Dim dbFees As Double
                Dim dbRebate As Double
                Dim dbNetAmtParent As Double
                Dim dbFeeAmtParent As Double
                Try
                    dbGrossAmount = Convert.ToDouble(strArrRecord(4).Trim())
                Catch ex As Exception
                    dbGrossAmount = 0.0
                End Try
                Try
                    dbFees = Convert.ToDouble(strArrRecord(3).Trim())
                Catch ex As Exception
                    dbFees = 0.0
                End Try
                Try
                    dbRebate = Convert.ToDouble(strArrRecord(5).Trim())
                Catch ex As Exception
                    dbRebate = 0.0
                End Try
                dr("GrossAmt") = (dbGrossAmount).ToString("0.00")
                dr("Fees") = (dbFees - dbRebate).ToString("0.00")
                dr("NetAmt") = (dbGrossAmount - dbFees + dbRebate).ToString("0.00")
                Try
                    dbNetAmtParent = Convert.ToDouble(dtParent.Rows(RowIndexID - 1)("NetAmount").ToString) + dbRebate
                    dtParent.Rows(RowIndexID - 1)("NetAmount") = dbNetAmtParent.ToString("0.00")
                Catch ex As Exception
                    dbNetAmtParent = 0.0
                    dtParent.Rows(RowIndexID - 1)("NetAmount") = (dbNetAmtParent + dbRebate).ToString("0.00")
                End Try
                Try
                    dbFeeAmtParent = Convert.ToDouble(dtParent.Rows(RowIndexID - 1)("Fees").ToString) - dbRebate
                    dtParent.Rows(RowIndexID - 1)("Fees") = dbFeeAmtParent.ToString("0.00")
                Catch ex As Exception
                    dbFeeAmtParent = 0.0
                    dtParent.Rows(RowIndexID - 1)("Fees") = (dbFeeAmtParent - dbRebate).ToString("0.00")
                End Try
                dr("dbIndicator") = strArrRecord(0).Trim()
                dr("dbFilter") = strArrRecord(1)
                dr("AcademicYrId") = AcademicYearId
                dr("IsInSchool") = IsInSchool
                dr("IsParsed") = 1
            End If
            ParseNMessage = True
        Catch e As ArgumentNullException
            ''strError = e.Message
        Catch e As ArgumentOutOfRangeException
            ''strError = e.Message
        Catch e As System.Exception
            ''strError = e.Message
            Throw
        End Try
    End Function
    Public Sub GetAwardYear(ByRef EDDX As EDDataXfer_New, ByVal strLoanId As String, ByRef AwardYearValue As String)
        Dim ds As New DataSet
        Dim strAYV As String = ""
        Dim strAYNum As String = ""
        Dim strAYLNum
        strAYNum = strLoanId.Substring(10, 2)

        ds = EDDX.GetAwardYearForDL()
        For Each dr As DataRow In ds.Tables("AwardYears").Rows
            strAYLNum = dr("AcademicYearDescrip").ToString.Substring(dr("AcademicYearDescrip").ToString.Length - 2, 2)
            If strAYNum = strAYLNum Then
                strAYV = dr("AcademicYearId").ToString
                Exit For
            End If
        Next
        If strAYV = "" Then
            strAYNum = strLoanId.Substring(strLoanId.IndexOf("S") + 1, 2)
            For Each dr As DataRow In ds.Tables("AwardYears").Rows
                strAYLNum = dr("AcademicYearDescrip").ToString.Substring(dr("AcademicYearDescrip").ToString.Length - 2, 2)
                If strAYNum = strAYLNum Then
                    strAYV = dr("AcademicYearId").ToString
                    Exit For
                End If
            Next
        End If
        If strAYV = "" Then
            AwardYearValue = Guid.Empty.ToString
        Else
            AwardYearValue = strAYV
        End If

    End Sub
    '' New Code By Vijay Ramteke on April 05, 2010
    '' New Code By Vijay Ramteke on April 08, 2010
    Public Function GetPaymentTypes() As DataSet
        Dim ds As New DataSet
        Dim EDDX As New EDDataXfer_New
        ds = EDDX.GetPaymentsTypes()
        Return ds
    End Function
    '' New Code By Vijay Ramteke on April 08, 2010
    '' New Code By Vijay Ramteke on April 09, 2010
    Private Function ParseDMMessage(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByVal RowIndexID As Integer, ByVal IndexID As Integer, ByRef SSN As String, ByRef StuEnrollmentID As String, ByRef FirstName As String, ByRef LastName As String, ByRef CampusId As String, ByRef CampusName As String, ByRef IsInSchool As Boolean, ByRef AwardId As String, ByRef LoanId As String, ByRef dtParent As DataTable, ByRef AcademicYearId As String, ByVal LoanType As String) As Boolean
        ParseDMMessage = False
        Try
            Dim ds As New DataSet
            Dim InSchool As Boolean = False
            AwardId = LoanId
            GetStudentSSNAndLoanType(LoanId, SSN, LoanType)

            '' Code changes by Kamalesh ahuja on 5th December 2010 to Pass Loan/AwardId to GetStudentInfo Function to check if the Award/ Loan id allready exists in the database
            '' then return the corresponding enrollment id. This is to avoid conflict when the student has 2 enrollments with the same status and start date
            ''EDDX.GetStudentInfo(db, SSN, FirstName, LastName, StuEnrollmentID, CampusId, CampusName, InSchool)
            EDDX.GetStudentInfo(db, SSN, FirstName, LastName, StuEnrollmentID, CampusId, CampusName, InSchool, LoanId)


            ds = EDDX.GetAwardInfoForDL(db, LoanId)
            ''FirstName = ds.Tables("AwardInfo").Rows(0)("FirstName").ToString
            ''LastName = ds.Tables("AwardInfo").Rows(0)("LastName").ToString
            ''StuEnrollmentID = ds.Tables("AwardInfo").Rows(0)("StuEnrollId").ToString
            ''CampusId = ds.Tables("AwardInfo").Rows(0)("CampusId").ToString
            ''CampusName = ds.Tables("AwardInfo").Rows(0)("CampDescrip").ToString
            ''IsInSchool = ds.Tables("AwardInfo").Rows(0)("InSchool").ToString
            ''SSN = ds.Tables("AwardInfo").Rows(0)("SSN").ToString

            If ds.Tables("AwardInfo").Rows.Count > 0 Then
                If Not StuEnrollmentID = ds.Tables("AwardInfo").Rows(0)("StuEnrollId").ToString Then
                    StuEnrollmentID = ds.Tables("AwardInfo").Rows(0)("StuEnrollId").ToString
                End If
            End If
            Dim strLoanTypeDesc As String = ""
            Dim strLoanType As String = ""
            strLoanType = LoanType
            If strLoanType.ToUpper = "P" Then
                strLoanTypeDesc = "DL-PLUS"
            ElseIf strLoanType.ToUpper = "S" Then
                strLoanTypeDesc = "DL-SUB"
            ElseIf strLoanType.ToUpper = "U" Then
                strLoanTypeDesc = "DL-UNSUB"
            Else
                strLoanTypeDesc = ""
            End If
            dtParent.Rows(IndexID)("RowIndexID") = IndexID
            dtParent.Rows(RowIndexID)("FirstName") = FirstName
            dtParent.Rows(RowIndexID)("LastName") = LastName
            dtParent.Rows(RowIndexID)("SSN") = IIf(SSN.Length = 9, FormatSSN(SSN), SSN)
            dtParent.Rows(RowIndexID)("Campus") = CampusName
            dtParent.Rows(RowIndexID)("StuEnrollmentId") = StuEnrollmentID
            dtParent.Rows(RowIndexID)("CampusId") = CampusId
            dtParent.Rows(RowIndexID)("dbFilter") = ""
            dtParent.Rows(RowIndexID)("LoanType") = LoanType
            dtParent.Rows(RowIndexID)("LoanTypeDesc") = strLoanTypeDesc
            dtParent.Rows(RowIndexID)("LoanId") = LoanId
            dtParent.Rows(RowIndexID)("AddDate") = ""
            dtParent.Rows(RowIndexID)("AddTime") = ""
            Dim dbGrossAmount As Double
            ''   Dim dbFeesPer As Double
            Try
                dbGrossAmount = Convert.ToDouble(ds.Tables("AwardInfo").Rows(0)("GrossAmount").ToString)
            Catch ex As Exception
                dbGrossAmount = 0.0
            End Try
            Dim dbFeesAmount As Double = 0.0
            Try
                dbFeesAmount = Convert.ToDouble(ds.Tables("AwardInfo").Rows(0)("LoanFees").ToString)
            Catch ex As Exception
                dbFeesAmount = 0.0
            End Try
            dtParent.Rows(RowIndexID)("GrossAmount") = dbGrossAmount.ToString("0.00")
            dtParent.Rows(RowIndexID)("Fees") = (Math.Round(dbFeesAmount)).ToString("0.00")
            dtParent.Rows(RowIndexID)("NetAmount") = (dbGrossAmount - Math.Round(dbFeesAmount)).ToString("0.00")
            Try
                dtParent.Rows(RowIndexID)("AcademicYearId") = ds.Tables("AwardInfo").Rows(0)("AcademicYearId").ToString
            Catch ex As Exception
                dtParent.Rows(RowIndexID)("AcademicYearId") = Guid.Empty.ToString
            End Try

            Try
                dtParent.Rows(RowIndexID)("LoanStartDate") = ds.Tables("AwardInfo").Rows(0)("AwardStartDate").ToString
            Catch ex As Exception
                dtParent.Rows(RowIndexID)("LoanStartDate") = ""
            End Try
            Try
                dtParent.Rows(RowIndexID)("LoanEndDate") = ds.Tables("AwardInfo").Rows(0)("AwardEndDate").ToString
            Catch ex As Exception
                dtParent.Rows(RowIndexID)("LoanEndDate") = ""
            End Try
            dtParent.Rows(RowIndexID)("ErrorType") = 0
            dtParent.Rows(RowIndexID)("dbIndicator") = "D"
            dtParent.Rows(RowIndexID)("SSNOriginal") = SSN
            dtParent.Rows(RowIndexID)("UpdatedDate") = ""
            dtParent.Rows(RowIndexID)("UpdatedTime") = ""
            dtParent.Rows(RowIndexID)("IsInSchool") = InSchool
            dtParent.Rows(RowIndexID)("IsParsed") = 1
            ParseDMMessage = True
        Catch e As System.Exception
        End Try
    End Function
    Private Function ParseDNMessage(ByRef EDDX As EDDataXfer_New, ByRef db As DataAccess.DataAccess, ByVal RowIndexID As Integer, ByVal IndexID As Integer, ByRef SSN As String, ByRef StuEnrollmentID As String, ByRef FirstName As String, ByRef LastName As String, ByRef CampusId As String, ByRef CampusName As String, ByRef IsInSchool As Boolean, ByRef AwardId As String, ByRef LoanId As String, ByRef dtParent As DataTable, ByRef AcademicYearId As String, ByVal LoanType As String) As Boolean
        ParseDNMessage = False
        Try
            Dim ds As New DataSet
            Dim InSchool As Boolean = False
            AwardId = LoanId
            GetStudentSSNAndLoanType(LoanId, SSN, LoanType)

            '' Code changes by Kamalesh ahuja on 5th December 2010 to Pass Loan/AwardId to GetStudentInfo Function to check if the Award/ Loan id allready exists in the database
            '' then return the corresponding enrollment id. This is to avoid conflict when the student has 2 enrollments with the same status and start date
            ''EDDX.GetStudentInfo(db, SSN, FirstName, LastName, StuEnrollmentID, CampusId, CampusName, InSchool)
            EDDX.GetStudentInfo(db, SSN, FirstName, LastName, StuEnrollmentID, CampusId, CampusName, InSchool, LoanId)

            ds = EDDX.GetAwardInfoForDL(db, LoanId)
            ''FirstName = ds.Tables("AwardInfo").Rows(0)("FirstName").ToString
            ''LastName = ds.Tables("AwardInfo").Rows(0)("LastName").ToString
            ''StuEnrollmentID = ds.Tables("AwardInfo").Rows(0)("StuEnrollId").ToString
            ''CampusId = ds.Tables("AwardInfo").Rows(0)("CampusId").ToString
            ''CampusName = ds.Tables("AwardInfo").Rows(0)("CampDescrip").ToString
            ''IsInSchool = ds.Tables("AwardInfo").Rows(0)("InSchool").ToString
            ''SSN = ds.Tables("AwardInfo").Rows(0)("SSN").ToString

            If ds.Tables("AwardInfo").Rows.Count > 0 Then
                If Not StuEnrollmentID = ds.Tables("AwardInfo").Rows(0)("StuEnrollId").ToString Then
                    StuEnrollmentID = ds.Tables("AwardInfo").Rows(0)("StuEnrollId").ToString
                End If
            End If
            Dim strLoanTypeDesc As String = ""
            Dim strLoanType As String = ""
            strLoanType = LoanType
            If strLoanType.ToUpper = "P" Then
                strLoanTypeDesc = "DL-PLUS"
            ElseIf strLoanType.ToUpper = "S" Then
                strLoanTypeDesc = "DL-SUB"
            ElseIf strLoanType.ToUpper = "U" Then
                strLoanTypeDesc = "DL-UNSUB"
            Else
                strLoanTypeDesc = ""
            End If
            dtParent.Rows(RowIndexID)("IndexID") = IndexID
            dtParent.Rows(RowIndexID)("FirstName") = FirstName
            dtParent.Rows(RowIndexID)("LastName") = LastName
            dtParent.Rows(RowIndexID)("SSN") = IIf(SSN.Length = 9, FormatSSN(SSN), SSN)
            dtParent.Rows(RowIndexID)("Campus") = CampusName
            dtParent.Rows(RowIndexID)("StuEnrollmentId") = StuEnrollmentID
            dtParent.Rows(RowIndexID)("CampusId") = CampusId
            dtParent.Rows(RowIndexID)("dbFilter") = ""
            dtParent.Rows(RowIndexID)("LoanType") = LoanType
            dtParent.Rows(RowIndexID)("LoanTypeDesc") = strLoanTypeDesc
            dtParent.Rows(RowIndexID)("LoanId") = LoanId
            dtParent.Rows(RowIndexID)("AddDate") = ""
            dtParent.Rows(RowIndexID)("AddTime") = ""
            Dim dbGrossAmount As Double
            ''   Dim dbFeesPer As Double
            Try
                dbGrossAmount = Convert.ToDouble(ds.Tables("AwardInfo").Rows(0)("GrossAmount").ToString)
            Catch ex As Exception
                dbGrossAmount = 0.0
            End Try
            Dim dbFeesAmount As Double = 0.0
            Try
                dbFeesAmount = Convert.ToDouble(ds.Tables("AwardInfo").Rows(0)("LoanFees").ToString)
            Catch ex As Exception
                dbFeesAmount = 0.0
            End Try
            dtParent.Rows(RowIndexID)("GrossAmount") = dbGrossAmount.ToString("0.00")
            dtParent.Rows(RowIndexID)("Fees") = (Math.Round(dbFeesAmount)).ToString("0.00")
            dtParent.Rows(RowIndexID)("NetAmount") = (dbGrossAmount - Math.Round(dbFeesAmount)).ToString("0.00")
            Try
                dtParent.Rows(RowIndexID)("AcademicYearId") = ds.Tables("AwardInfo").Rows(0)("AcademicYearId").ToString
            Catch ex As Exception
                dtParent.Rows(RowIndexID)("AcademicYearId") = Guid.Empty.ToString
            End Try
            Try
                dtParent.Rows(RowIndexID)("LoanStartDate") = ds.Tables("AwardInfo").Rows(0)("AwardStartDate").ToString
            Catch ex As Exception
                dtParent.Rows(RowIndexID)("LoanStartDate") = ""
            End Try
            Try
                dtParent.Rows(RowIndexID)("LoanEndDate") = ds.Tables("AwardInfo").Rows(0)("AwardEndDate").ToString
            Catch ex As Exception
                dtParent.Rows(RowIndexID)("LoanEndDate") = ""
            End Try
            dtParent.Rows(RowIndexID)("ErrorType") = 0
            dtParent.Rows(RowIndexID)("dbIndicator") = "D"
            dtParent.Rows(RowIndexID)("SSNOriginal") = SSN
            dtParent.Rows(RowIndexID)("UpdatedDate") = ""
            dtParent.Rows(RowIndexID)("UpdatedTime") = ""
            dtParent.Rows(RowIndexID)("IsInSchool") = InSchool
            dtParent.Rows(RowIndexID)("IsParsed") = 1
            ParseDNMessage = True
        Catch e As System.Exception
        End Try
    End Function
    '' New Code By Vijay Ramteke on April 09, 2010
    Private Sub GetStudentSSNAndLoanType(ByVal LoanId As String, ByRef SSN As String, ByRef LoanType As String)
        SSN = LoanId.Substring(0, 9)
        LoanType = LoanId.Substring(9, 1)
    End Sub

    Public Function DeleteNotPostedParentRecordsWithNoChildRecords() As String
        Dim db As New EDDataXfer_New

        Return db.DeleteNotPostedParentRecordsWithNoChildRecords

    End Function
End Class
