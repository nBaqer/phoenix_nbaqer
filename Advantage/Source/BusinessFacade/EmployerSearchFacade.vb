
Public Class EmployerSearchFacade
    Public Function GetEmployerID(ByVal Code As String) As String
        With New EmployerSearchDB
            Return .GetEmployerID(Code)
        End With
    End Function
    Public Function GetEmployerCount(ByVal Code As String) As String
        With New EmployerSearchDB
            Return .GetEmployerCount(Code)
        End With
    End Function
    Public Function GetStudentID(ByVal SSN As String) As String
        With New EmployerSearchDB
            Return .GetStudentID(SSN)
        End With
    End Function
    Public Function GetStudentCount(ByVal SSN As String) As String
        With New EmployerSearchDB
            Return .GetStudentCount(SSN)
        End With
    End Function
    Public Function StudentSearchResultsBySSN(ByVal SSN As String) As DataSet
        Dim ds As New DataSet
        With New EmployerSearchDB
            ds = StudentSearchResultsBySSN(SSN)
        End With

        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        'Dim zipMask As String
        Dim dr As DataRow

        'Get the mask for phone numbers and zip
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds
    End Function
    Public Function GetEmployerName(ByVal SSN As String) As String
        With New EmployerSearchDB
            Return .GetStudentName(SSN)
        End With
    End Function
    Public Function GetStudentIdBySSN(ByVal SSN As String) As String
        Dim strStudentId As String
        Dim StudSearch As New EmployerSearchDB
        strStudentId = StudSearch.GetStudentIdBySSN(SSN)
        Return strStudentId
    End Function
    Public Function GetEmployerNameByID(ByVal EmployerID As String) As String
        With New EmployerSearchDB
            Return .GetEmployerNameByID(EmployerID)
        End With
    End Function
    Public Function GetAllCountriesUsedByEmployers() As DataSet

        '   Instantiate DAL component
        With New EmployerSearchDB

            '   return Dataset
            Return .GetAllCountriesUsedByEmployers()

        End With

    End Function
End Class
