Imports FAME.AdvantageV1.BusinessRules
Imports FAME.Advantage.Common

Public Class TranscriptFacade
    Public Function GetTranscriptByEnrollment(ByVal strEnrollId As String, ByVal prgVerId As String, Optional ByVal excludeNullGrades As Boolean = False) As DataSet
        With New TranscriptDB
            Return .GetTranscriptByEnrollment(strEnrollId, prgVerId, excludeNullGrades)
        End With
    End Function
    Public Function GetTranscriptGradeByEnrollment(ByVal strEnrollId As String, ByVal prgVerId As String, ByVal studentId As String) As DataSet
        With New TranscriptDB
            Return .GetTranscriptGradeByEnrollment(strEnrollId, prgVerId, studentId)
        End With
    End Function

    ''' <summary>
    ''' Get the lsit of enrollemnt for a determinate student and campus.
    ''' </summary>
    ''' <param name="studentId">Student ID (internal Guid)</param>
    ''' <param name="campusId">Campus ID  (internal Guid)</param>
    ''' <returns>List(Of EnrollmentItemsDto)</returns>
    Public Shared Function GetEnrollmentList(ByVal studentId As String, Optional ByVal campusId As String = Nothing) As List(Of EnrollmentItemsDto)
        Dim db As TranscriptDB = New TranscriptDB()
        Dim ds As DataSet = db.GetEnrollment(studentId, campusId)
        Dim dt As DataTable = ds.Tables(0)
        Dim list As New List(Of EnrollmentItemsDto)
        For Each row As DataRow In dt.Rows
            Dim dto As New EnrollmentItemsDto()
            dto.PrgVerDescrip = row("PrgVerDescrip").ToString()
            dto.PrgVerId = row("PrgVerId").ToString()
            dto.StuEnrollId = row("StuEnrollId").ToString()
            list.Add(dto)
        Next

        Return list
    End Function

    Public Function GetEnrollment(ByVal studentId As String, Optional ByVal campusId As String = Nothing) As DataSet
        With New TranscriptDB
            Return .GetEnrollment(studentId, campusId)
        End With
    End Function

    Public Function IsEnrollmentInSchool(ByVal stuEnrollId As String) As Boolean
        With New TranscriptDB
            Return .IsEnrollmentInSchool(stuEnrollId)
        End With
    End Function
    Public Function GetEnrollmentCanChangeGrade(ByVal stuEnrollId As String, ByVal userId As String) As Boolean
        With New TranscriptDB
            Return .GetEnrollmentCanChangeGrade(stuEnrollId, userId)
        End With

    End Function
    Public Function GetSummary(ByVal stuEnrollId As String, ByVal prgVerId As String, Optional ByVal excludeNullGrades As Boolean = False) As TranscriptInfo
        With New TranscriptDB
            Return .GetSummary(stuEnrollId, prgVerId, excludeNullGrades)
        End With
    End Function
    Public Function GetSummaryFromSP(ByVal StuEnrollIdList As String) As TranscriptInfo
        With New TranscriptDB
            Return .GetSummaryFromSP(StuEnrollIdList)
        End With
    End Function
    Public Function GetGraduateAuditByEnrollment(ByVal stuEnrollId As String, ByVal prgVerId As String) As DataSet
        With New TranscriptDB
            Return .GetGraduateAuditByEnrollment(stuEnrollId, prgVerId)
        End With
    End Function
    Public Function GetGraduateAuditByEnrollment(ByVal stuEnrollId As String) As DataSet
        Dim ds As DataSet
        Dim tranDb As New TranscriptDB

        ds = tranDb.GetGraduateAuditByEnrollment(stuEnrollId)


        Return ds

    End Function
    Public Function GetDefaultGradeByGradeSystem(ByVal grdSystemId As String) As String
        With New TranscriptDB
            Return .GetDefaultGradeByGradeSystem(grdSystemId)
        End With
    End Function
    Public Function GetCoursesByEnrollment(ByVal PrgVerId As String, ByVal DescCollection As String) As DataSet
        With New TranscriptDB
            Return .GetCoursesByEnrollment(PrgVerId, DescCollection)
        End With
    End Function
    Public Function GetCumGPA(ByVal StuEnrollmentId As String) As String

        Dim db As New TermProgressDB
        Dim ds As DataSet
        Dim tbl1 As DataTable
        Dim tbl2 As DataTable
        Dim tbl3 As DataTable
        Dim tbl4 As DataTable
        Dim row As DataRow
        Dim grdDetail As DataRow
        Dim dr As DataRow
        Dim arr As DataRow
        Dim arr1 As DataRow
        Dim Req As String
        Dim Grade As String
        Dim BelongToPrgVer As Integer
        Dim StudentProgress As New StudentProgressInfo
        Dim TermCredsAttmptd As Integer
        Dim TermCredsEarned As Integer
        Dim Term As String
        Dim TotalCreds As Double
        Dim GrdWeight As Double
        Dim TotalCreds2 As Double
        Dim GrdWeight2 As Double

        ds = db.GetStudentProgress(StuEnrollmentId)

        tbl1 = ds.Tables("StdProgress")
        tbl2 = ds.Tables("StuResults")
        tbl3 = ds.Tables("CredsAttmptd")

        'With tbl1
        '    .PrimaryKey = New DataColumn() {.Columns("TermId")}
        'End With

        'With tbl2
        '    .PrimaryKey = New DataColumn() {.Columns("ReqId")}
        'End With

        Dim col1 As DataColumn = tbl1.Columns.Add("CredsAttmptd", GetType(String))
        Dim col2 As DataColumn = tbl1.Columns.Add("CredsEarned", GetType(String))
        Dim col3 As DataColumn = tbl1.Columns.Add("CurrGPA", GetType(String))
        Dim col4 As DataColumn = tbl1.Columns.Add("CumGPA", GetType(String))


        'Fill up the Student Progress table with the processed data.
        If tbl1.Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            For Each dr In tbl1.Rows
                Term = dr("TermId").ToString
                Dim TermSDate As DateTime = dr("StartDate")

                'Get all the Results belonging to this Term in the Student Results table.
                Dim aRows As DataRow()
                Dim aRows1 As DataRow()

                TermCredsAttmptd = 0
                TermCredsEarned = 0
                TotalCreds = 0
                GrdWeight = 0
                TotalCreds2 = 0
                GrdWeight2 = 0

                'Find this Term in the results table and retrieve corresponding info.
                aRows = tbl2.Select("TermId ='" & dr("TermId").ToString & "'")

                For Each arr In aRows
                    If (arr("IsCreditsAttempted")) Then
                        TermCredsAttmptd += arr("Credits")
                    End If

                    If (arr("IsCreditsEarned")) Then
                        TermCredsEarned += arr("Credits")
                    End If

                    If (arr("IsInGPA")) Then
                        TotalCreds += arr("Credits")
                        GrdWeight += arr("GPA") * arr("Credits")
                    End If
                Next

                dr("CredsAttmptd") = TermCredsAttmptd
                dr("CredsEarned") = TermCredsEarned

                If GrdWeight > 0 And TotalCreds > 0 Then
                    dr("CurrGPA") = System.Math.Round((GrdWeight / TotalCreds), 2).ToString("##.00")
                End If

                'To calculate the Cumulative GPA
                Dim a As String = "StartDate <= " & "#" & TermSDate.ToShortDateString() & "#"
                aRows1 = tbl2.Select(a)


                For Each arr1 In aRows1
                    Dim reqstr As String = arr1("ReqId").ToString
                    'Dim isingpab As Boolean = arr1("IsInGPA")


                    If Not arr1.IsNull("IsInGPA") Then 'And (arr1("IsInGPA") = True) Then
                        TotalCreds2 += arr1("Credits")
                        If Not arr1.IsNull("GPA") Then
                            GrdWeight2 += arr1("GPA") * arr1("Credits")
                        End If
                    End If
                Next
                If GrdWeight2 > 0 And TotalCreds2 > 0 Then
                    dr("CumGPA") = System.Math.Round((GrdWeight2 / TotalCreds2), 2).ToString("##.00")
                End If
            Next
            Return dr("CumGPA").ToString
        Else
            Return ""
        End If



    End Function
    Public Function GetGraduateAuditByEnrollmentCourseGroup(ByVal StuEnrollId As String) As DataSet
        With New TranscriptDB
            Return .GetGraduateAuditByEnrollmentCourseGroup(StuEnrollId)
        End With
    End Function
    Public Function GetGraduateAuditGradeByEnrollmentCourseGroup(ByVal StuEnrollId As String, ByVal STudentId As String) As DataSet
        With New TranscriptDB
            Return .GetGraduateAuditGradeByEnrollmentCourseGroup(StuEnrollId, STudentId)
        End With
    End Function
    Public Function GetParentChildLevel() As Integer
        With New TranscriptDB
            Return .GetParentChildLevel()
        End With
    End Function
    Public Function GetStudentResults(ByVal stuEnrollmentId As String, Optional ByVal bGetNumGrade As Boolean = False, Optional ByVal bNumGradeRnd As Boolean = False) As DataTable
        Dim db As New TermProgressDB

        Return db.GetStudentResultsForClassesInEnrollment(stuEnrollmentId, bGetNumGrade, bNumGradeRnd)

    End Function
    Public Function GetCoursesForStudentEnrollment(ByVal stuEnrollmentId As String) As DataTable
        Dim db As New TermProgressDB

        Return db.GetCoursesForStudentEnrollment(stuEnrollmentId)
    End Function
    Public Function GetCoursesForProgramVersion(ByVal prgVerId As String) As DataTable
        Dim db As New TermProgressDB

        Return db.GetCoursesForProgramVersion(prgVerId)

    End Function
    Public Function GetClassSectionsForStudentEnrollmentWhenEnrolling(ByVal stuEnrollment As String) As DataTable
        Dim db As New TermProgressDB

        Return db.GetClassSectionsForStudentEnrollmentWhenEnrolling(stuEnrollment)

    End Function
    Public Function GetClassSectionsForStudentWhenEnrolling(ByVal prgVerId As String, ByVal expStartDate As String, ByVal campusId As String, ByVal shiftId As String) As DataTable
        Dim db As New TermProgressDB

        Return db.GetClassSectionsForStudentWhenEnrolling(prgVerId, expStartDate, campusId, shiftId)

    End Function
    Public Function RegisterStudentForClassesWhenEnrollingUsingRotationalSchedule(ByVal stuEnrollmentId As String, ByVal user As String, ByVal campusId As String) As DataTable
        Dim dt1 As DataTable
        Dim dt2 As DataTable
        Dim dt3 As New DataTable
        Dim dr As DataRow
        Dim numCoursesReq As Integer
        Dim row As DataRow
        Dim fac As New RegFacade

        dt1 = GetCoursesForStudentEnrollment(stuEnrollmentId).Copy
        dt2 = GetClassSectionsForStudentEnrollmentWhenEnrolling(stuEnrollmentId).Copy

        'Add the relevant columns to dt3
        Dim col1 As DataColumn = dt3.Columns.Add("ReqId", GetType(String))
        Dim col2 As DataColumn = dt3.Columns.Add("ClsSectionId", GetType(String))
        Dim col3 As DataColumn = dt3.Columns.Add("Description", GetType(String))
        Dim col4 As DataColumn = dt3.Columns.Add("Start", GetType(Date))
        Dim col5 As DataColumn = dt3.Columns.Add("End", GetType(Date))

        dt3.PrimaryKey = New DataColumn() {dt3.Columns("ReqId")}

        '************************************************************************************************
        'We need to loop through dt2 and see if the student already has the associated course in dt3
        '       If the course is not there then we need to add it (if any prereq has already been added)
        '           If the number of class sections in dt3 = number of courses in dt1
        '               exit the loop since the student is now registered for the necessary courses
        '           End if
        '       Else move on to the next class section in the list
        '       
        '       End if
        '
        'End loop
        '************************************************************************************************
        For Each dr In dt2.Rows
            row = dt3.Rows.Find(dr("ReqId"))
            If row Is Nothing Then
                Dim newRow As DataRow = dt3.NewRow

                newRow("ReqId") = dr("ReqId").ToString
                newRow("ClsSectionId") = dr("ClsSectionId").ToString
                newRow("Description") = dr("Descrip").ToString
                newRow("Start") = dr("StartDate").ToString
                newRow("End") = dr("EndDate").ToString
                dt3.Rows.Add(newRow)

                If dt3.Rows.Count = dt1.Rows.Count Then
                    Exit For
                End If
            End If

        Next

        'We want to register the student for each class section in dt3
        For Each dr In dt3.Rows
            fac.AddStdToClsSect(dr("ClsSectionId"), stuEnrollmentId, user, campusId)
        Next
        Return dt3
    End Function
    Public Function RegisterStudentForClassesWithSameStartDate(ByVal stuEnrollmentId As String, ByVal StartDate As String, ByVal user As String, Optional ByVal ShiftId As String = "") As String
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable
        Dim dt3 As New DataTable
        Dim dr As DataRow
        Dim numCoursesReq As Integer
        Dim row As DataRow
        Dim fac As New RegFacade
        Dim db As New LeadEnrollmentDB
        Return db.RegisterStudentForClassesWithSameStartDate(stuEnrollmentId, StartDate, user, ShiftId)
    End Function
    Public Function RegisterLeadForInitialCourses(ByVal prgVerId As String, ByVal stuEnrollmentId As String, ByVal StartDate As String, ByVal user As String, ByVal ShiftId As String) As String
        Return (New LeadEnrollmentDB).RegisterLeadForInitialCourses(prgVerId, stuEnrollmentId, StartDate, user, ShiftId)
    End Function
    Public Function AutoRegisterForRegularTraditionalSchools(ByVal ExpStartDate As Date,
                                                              ByVal PrgVerId As String,
                                                              ByVal StuEnrollId As String,
                                                              ByVal user As String) As String
        Return (New LeadEnrollmentDB).AutoRegisterForRegularTraditionalSchools(ExpStartDate, PrgVerId, StuEnrollId, user)
    End Function
    Public Function RegisterLeadForCoursesGrdBkComponents(ByVal prgVerId As String, ByVal stuEnrollmentId As String, ByVal StartDate As String, ByVal user As String, ByVal ShiftId As String) As String
        Dim rightNow As Date = Date.Now
        Return (New LeadEnrollmentDB).RegisterLeadForGradeBookComponents(stuEnrollmentId, user, rightNow)
    End Function
    Public Function GetCoursesWithoutPassingGradeForStudentEnrollment(ByVal stuEnrollmentId As String, Optional ByVal excludeNullGrades As Boolean = False) As DataTable
        Dim db As New TermProgressDB
        Dim dbTranscript As New TranscriptDB
        Dim ds As New DataSet
        If dbTranscript.IsContinuingEdPrgVersion(stuEnrollmentId) Then
            Return db.GetCoursesWithoutPassingGradeForCEEnrollment(stuEnrollmentId)
        Else
            Return db.GetCoursesWithoutPassingGradeForStudentEnrollment(stuEnrollmentId, excludeNullGrades)
        End If
    End Function
    Public Function GetReqsForCourseGroup(ByVal courseGrpId As String) As DataTable
        Dim db As New TranscriptDB

        Return db.GetReqsForCourseGroup(courseGrpId)
    End Function
    Public Function DoesReqExistsInCourseGroup(ByVal reqId As String, ByVal courseGrpId As String) As Boolean
        Dim dt As DataTable
        Dim dr As DataRow

        dt = GetReqsForCourseGroup(courseGrpId)

        For Each dr In dt.Rows
            If dr("ReqId").ToString() = reqId Then
                Return True
            End If
        Next
        Return False

    End Function
    Public Function GetFirstCommonRequirement(ByVal courseGrpId As String, ByVal reqsCollection As ArrayList) As String
        Dim dt1 As DataTable
        Dim dt2 As DataTable
        Dim dr As DataRow
        Dim dr2 As DataRow
        Dim rInfo As New ReqInfo

        'Get the reqs for the course grp that is passed in
        dt1 = GetReqsForCourseGroup(courseGrpId)

        If dt1.Rows.Count = 0 Or reqsCollection.Count = 0 Then
            Return ""
        Else
            'For each item in the collection. If the item is a course then we will check the dt1 datatable
            'to see if it exists there. If the item is a course group then we will populate dt2 with the
            'reqs for it. We will then loop through and see if there are any common courses.
            For Each rInfo In reqsCollection
                If rInfo.ReqTypeId = 1 Then
                    For Each dr In dt1.Rows
                        If dr("ReqId").ToString() = rInfo.ReqId Then
                            Return rInfo.ReqDescription
                        End If
                    Next
                    Return ""
                ElseIf rInfo.ReqTypeId = 2 Then
                    dt2 = GetReqsForCourseGroup(rInfo.ReqId.ToString)
                    For Each dr In dt1.Rows
                        For Each dr2 In dt2.Rows
                            If dr("ReqId").ToString() = dr2("ReqId").ToString() Then
                                Return dr("Descrip")
                            End If
                        Next
                    Next
                    Return ""

                End If
            Next
        End If


    End Function

    Public Function GetScheduledCourses(ByVal stuEnrollId As String, ByVal campusId As String) As DataSet
        Dim ds As DataSet = (New TranscriptDB).GetScheduledCourses(stuEnrollId, campusId)

        If ds.Tables.Count > 1 Then
            Dim dtSchedCourses As DataTable = ds.Tables(0)
            Dim dtMeetings As DataTable = ds.Tables(1)
            Dim arrRows() As DataRow

            For Each dr As DataRow In dtSchedCourses.Rows
                arrRows = dtMeetings.Select("TestId='" & dr("TestId").ToString & "'")
                If arrRows.GetLength(0) > 0 Then
                    Dim sb As New System.Text.StringBuilder
                    For Each row As DataRow In arrRows
                        If sb.Length > 0 Then sb.Append("<br>")
                        If Not row.IsNull("WorkDaysDescrip") Then
                            sb.Append(row("WorkDaysDescrip"))
                            sb.Append(vbCrLf)
                        End If
                        If Not row.IsNull("TimeIn") And Not row.IsNull("TimeOut") Then
                            sb.Append(" " & Convert.ToDateTime(row("TimeIn")).ToShortTimeString)
                            sb.Append(" - " & Convert.ToDateTime(row("TimeOut")).ToShortTimeString)
                        ElseIf Not row.IsNull("TimeIn") Then
                            sb.Append(" " & Convert.ToDateTime(row("TimeIn")).ToShortTimeString)
                        ElseIf Not row.IsNull("TimeOut") Then
                            sb.Append(" - " & Convert.ToDateTime(row("TimeOut")).ToShortTimeString)
                        End If
                        If Not row.IsNull("RoomDescrip") Then
                            sb.Append(" (" & row("RoomDescrip") & ")")
                        End If
                        sb.Append(vbCrLf)
                    Next
                    dr("TimeAndRoom") = sb.ToString
                End If
            Next
        End If
        Return ds
    End Function

    Public Function GetRemainingCourses(ByVal stuEnrollId As String, ByVal campusId As String) As DataSet
        'Return (New TranscriptDB).GetRemainingCourses(stuEnrollId, campusId)
        Dim br As New RegisterStudentsBR
        Dim myDataSet As DataSet = (New TranscriptDB).GetRemainingCourses(stuEnrollId)
        Dim myRow As DataRow
        For Each myRow In myDataSet.Tables(0).Rows
            If myRow.Item("OverRide").ToString() <> "00000000-0000-0000-0000-000000000000" Then
                If (br.IsStudentPassed(myRow.Item("StuEnrollId").ToString, myRow.Item("ReqId").ToString, myRow.Item("OverRide").ToString, myRow.Item("PrgVerId").ToString)) Then
                    'myDataSet.Tables(0).Rows(0).Delete()
                    myRow.Delete()
                End If
            End If
        Next
        Return myDataSet

    End Function
    'US4112 6/10/2013 Janet Robinson re factored to stored proc
    Public Function GetGradedAndScheduledClassesForEnrollmentSP(ByVal stuEnrollId As String, campusid As String) As DataTable
        Dim db As New TermProgressDB
        Dim dt As DataTable
        Dim dr As DataRow

        dt = db.GetGradedAndScheduledClassesForEnrollmentSP(stuEnrollId, campusid)

        'Add a new column call DisplayedCol that will concatenate the course description,
        'class section start date and any grade earned for the class
        dt.Columns.Add("ShortStartDate", GetType(String))
        dt.Columns.Add("ShortTermStartDate", GetType(String))
        dt.Columns.Add("ClassSection", GetType(String))

        For Each dr In dt.Rows
            If Not IsDBNull(dr("StartDate")) Then
                dr("ShortStartDate") = dr("StartDate").ToShortDateString
            End If
            dr("ClassSection") = dr("ClsSection").ToString()

            If Not IsDBNull(dr("TermStart")) Then
                dr("ShortTermStartDate") = dr("TermStart").ToShortDateString
            End If

        Next

        dt.Columns.Add("DisplayedCol", GetType(String), "'(' + Code + ')' + Descrip + ' (' + ShortStartDate + ')' + ' (' + ClassSection + ')' + ' (' + IsNull(Grade,'Not Graded') + ')' ")

        Return dt
    End Function
    Public Function GetGradedAndScheduledClassesForEnrollment(ByVal stuEnrollId As String, campusid As String) As DataTable
        Dim db As New TermProgressDB
        Dim dt As DataTable
        Dim dr As DataRow

        dt = db.GetGradedAndScheduledClassesForEnrollment(stuEnrollId, campusid)

        'Add a new column call DisplayedCol that will concatenate the course description,
        'class section start date and any grade earned for the class
        dt.Columns.Add("ShortStartDate", GetType(String))
        dt.Columns.Add("ShortTermStartDate", GetType(String))
        dt.Columns.Add("ClassSection", GetType(String))

        For Each dr In dt.Rows
            dr("ShortStartDate") = dr("StartDate").ToShortDateString
            dr("ClassSection") = dr("ClsSection").ToString()
            dr("ShortTermStartDate") = dr("TermStart").ToShortDateString
        Next

        dt.Columns.Add("DisplayedCol", GetType(String), "'(' + Code + ')' + Descrip + ' (' + ShortStartDate + ')' + ' (' + ClassSection + ')' + ' (' + IsNull(Grade,'Not Graded') + ')' ")

        Return dt
    End Function

    Public Function GetAvailableClassesForEnrollmentUsingRotationalSchedule(ByVal stuEnrollId As String) As DataTable
        Dim db As New TermProgressDB
        Dim dt As DataTable
        Dim dr As DataRow

        dt = db.GetAvailableClassesForEnrollmentUsingRotationalSchedule(stuEnrollId)

        dt.Columns.Add("ShortStartDate", GetType(String))
        dt.Columns.Add("TermStartShortDate", GetType(String))

        For Each dr In dt.Rows
            dr("ShortStartDate") = dr("StartDate").ToShortDateString
            dr("TermStartShortDate") = dr("TermStart").Toshortdatestring
        Next

        dt.Columns.Add("DisplayedCol", GetType(String), "Descrip + ' (' + TermStartShortDate + ')' + ' (' + ShortStartDate + ')' ")

        Return dt

    End Function
    ' US4112 6/10/2013 Janet Robinson re-factored to stored proc
    ' US4172 7/02/2013 Janet Robinson clear visual indicator of when a course is completed on the schedules page
    Public Function GetAvailableClassesForEnrollmentUsingStartDateSP(ByVal stuEnrollId As String) As DataTable
        Dim db As New TermProgressDB
        Dim dt As DataTable
        Dim dr As DataRow
        Dim clsRegStd As New RegStdManuallyFacade
        Dim bReturn As Boolean

        dt = db.GetAvailableClassesForEnrollmentUsingStartDateSP(stuEnrollId)

        dt.Columns.Add("ShortStartDate", GetType(String))
        dt.Columns.Add("ShortTermStartDate", GetType(String))
        dt.Columns.Add("ClassSection", GetType(String))
        dt.Columns.Add("IsCourseCompleted", GetType(String))

        For Each dr In dt.Rows
            If Not IsDBNull(dr("StartDate")) Then
                dr("ShortStartDate") = dr("StartDate").ToShortDateString
            End If
            If Not IsDBNull(dr("TermStart")) Then
                dr("ShortTermStartDate") = dr("TermStart").ToShortDateString
            End If
            dr("ClassSection") = dr("ClsSection").ToString()
            dr("ClsSectionId") = dr("ClsSectionId").ToString()
            bReturn = clsRegStd.IsCourseCompleted(stuEnrollId, dr("ClsSectionId").ToString())
            dr("IsCourseCompleted") = bReturn
        Next

        dt.Columns.Add("DisplayedCol", GetType(String), " ' (' + Code + ')' + Descrip + ' (' + ShortTermStartDate + ')' + ' (' + ShortStartDate + ')' + ' (' + ClassSection + ')' ")
        Return dt

    End Function
    Public Function GetAvailableClassesForEnrollmentUsingStartDate(ByVal stuEnrollId As String) As DataTable
        Dim db As New TermProgressDB
        Dim dt As DataTable
        Dim dr As DataRow

        dt = db.GetAvailableClassesForEnrollmentUsingStartDate(stuEnrollId)

        Dim col1 As DataColumn = dt.Columns.Add("ShortStartDate", GetType(String))
        Dim col2 As DataColumn = dt.Columns.Add("ShortTermStartDate", GetType(String))
        Dim col3 As DataColumn = dt.Columns.Add("ClassSection", GetType(String))

        For Each dr In dt.Rows
            dr("ShortStartDate") = dr("StartDate").ToShortDateString
            dr("ShortTermStartDate") = dr("TermStart").ToShortDateString
            dr("ClassSection") = dr("ClsSection").ToString()
        Next

        'Dim col As DataColumn = dt.Columns.Add("DisplayedCol", GetType(String), " ' (' + Code + ')' + Descrip + ' (' + ShortTermStartDate + ')' + ' (' + ShortStartDate + ')' ")
        Dim col As DataColumn = dt.Columns.Add("DisplayedCol", GetType(String), " ' (' + Code + ')' + Descrip + ' (' + ShortTermStartDate + ')' + ' (' + ShortStartDate + ')' + ' (' + ClassSection + ')' ")


        Return dt

    End Function
    Public Function GetConflictingClassSectionsUsingRotationalSchedule(ByVal ReqId As String, ByVal startDate As Date,
        ByVal endDate As Date, ByVal campusId As String, ByVal shiftId As String, ByVal clsSectionId As String, Optional ByVal IsModule As Boolean = True) As DataTable
        Dim db As New TermProgressDB
        Dim dt As DataTable
        Dim dr As DataRow

        dt = db.GetConflictingClassSectionsUsingRotationalSchedule(ReqId, startDate, endDate, campusId, shiftId, clsSectionId, IsModule)

        'Add a new column call DisplayedCol that will concatenate the course description,
        'class section start date and end dates
        Dim col1 As DataColumn = dt.Columns.Add("ShortStartDate", GetType(String))
        Dim col2 As DataColumn = dt.Columns.Add("ShortEndDate", GetType(String))

        For Each dr In dt.Rows
            dr("ShortStartDate") = dr("StartDate").ToShortDateString
            dr("ShortEndDate") = dr("EndDate").ToShortDateString
        Next

        Dim col3 As DataColumn = dt.Columns.Add("ClassDescrip", GetType(String), "'(' + Code + ') ' + Descrip + ' (' + ShortStartDate +  ' - ' + ShortEndDate + ')' ")

        Return dt

    End Function


    Public Function GetOtherCoursesInPrgVersion(ByVal reqId As String) As DataTable
        Dim db As New TermProgressDB

        Return db.GetOtherCoursesInPrgVersion(reqId)

    End Function

    Public Function GetExpGradDateUsingRotationalSchedule(ByVal prgVerId As String, ByVal expStartDate As String, ByVal campusId As String, ByVal shiftId As String) As String
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable
        Dim dt3 As New DataTable
        Dim dr As DataRow
        Dim numCoursesReq As Integer
        Dim row As DataRow
        Dim fac As New RegFacade
        Dim db As New TermProgressDB
        Dim maxDate As DateTime

        'If the prgVerId, expStartDate or campusId is empty then we should return an empty string
        If prgVerId = "" Or expStartDate = "" Or campusId = "" Then
            Return ""
        End If

        dt1 = db.GetCoursesForProgramVersion(prgVerId)
        dt2 = GetClassSectionsForStudentWhenEnrolling(prgVerId, expStartDate, campusId, shiftId).Copy

        'Add the relevant columns to dt3
        Dim col1 As DataColumn = dt3.Columns.Add("ReqId", GetType(String))
        Dim col2 As DataColumn = dt3.Columns.Add("ClsSectionId", GetType(String))
        Dim col3 As DataColumn = dt3.Columns.Add("Description", GetType(String))
        Dim col4 As DataColumn = dt3.Columns.Add("Start", GetType(Date))
        Dim col5 As DataColumn = dt3.Columns.Add("End", GetType(Date))

        dt3.PrimaryKey = New DataColumn() {dt3.Columns("ReqId")}

        '************************************************************************************************
        'We need to loop through dt2 and see if the student already has the associated course in dt3
        '       If the course is not there then we need to add it (if any prereq has already been added)
        '           If the number of class sections in dt3 = number of courses in dt1
        '               exit the loop since the student is now registered for the necessary courses
        '           End if
        '       Else move on to the next class section in the list
        '       
        '       End if
        '
        'End loop
        '************************************************************************************************
        For Each dr In dt2.Rows
            row = dt3.Rows.Find(dr("ReqId"))
            If row Is Nothing Then
                Dim newRow As DataRow = dt3.NewRow

                newRow("ReqId") = dr("ReqId").ToString
                newRow("ClsSectionId") = dr("ClsSectionId").ToString
                newRow("Description") = dr("Descrip").ToString
                newRow("Start") = dr("StartDate").ToString
                newRow("End") = dr("EndDate").ToString
                dt3.Rows.Add(newRow)

                If dt3.Rows.Count = dt1.Rows.Count Then
                    Exit For
                End If
            End If

        Next

        'If dt3 has rows we can return the max end as the expected grad date
        If dt3.Rows.Count = 0 Then
            Return ""
        Else
            row = dt3.Rows(dt3.Rows.Count - 1)
            maxDate = row("End")
            Return maxDate.ToShortDateString
        End If

    End Function

    Public Sub UpdateExpectedGradDateUsingRotationalSchedule(ByVal stuEnrollId As String)
        Dim db As New TermProgressDB

        db.UpdateExpectedGradDateUsingRotationalSchedule(stuEnrollId)

    End Sub

    Public Function CalculateClassEndDate(ByVal startDate As String, ByVal hours As String, ByVal arrMDT As ArrayList) As String
        Dim dtm As DateTime
        Dim counter As Integer
        Dim mMDT As MeetDateTimeInfo
        Dim iHrs As Decimal
        Dim clsSectAttFac As New ClsSectAttendanceFacade


        'If any of the params are empty we need to return an empty string
        If startDate = "" Or hours = "" Or arrMDT.Count = 0 Then
            Return ""
        End If

        'if any of the meetdatetimeinfo object has an empty day, starttime or endtime then return an empty string
        For Each mMDT In arrMDT
            If mMDT.MeetDay = "" Or mMDT.StartTime = "" Or mMDT.EndTime = "" Then
                Return ""
            End If
        Next

        'We are going to check each day, starting with the start day, to see if it is a meet day.
        'If it is and it is not a holiday then we will increment the iHrs variable.
        'We will continue doing this until we get to the day where the iHrs variable >= hours of the class
        'We need a loop  so we will use 365 as the upper bound. However, as soon as we find the end date
        'we will return it and so exit the loop without wasting time going all the way through it.
        dtm = Convert.ToDateTime(startDate)

        For counter = 1 To 365
            'Check if the date is a meet day
            For Each mMDT In arrMDT
                If mMDT.MeetDay = clsSectAttFac.GetShortDayName(dtm) Then
                    'Skip if it is a holiday
                    If Not clsSectAttFac.IsDateHoliday(dtm) Then
                        iHrs += mMDT.Hours
                        If iHrs >= hours Then
                            Return dtm.ToShortDateString
                        End If
                        Exit For
                    End If

                End If
            Next
            dtm = dtm.AddDays(1)

        Next

    End Function
    Public Function UpdateSingleGrade(ByVal resultId As String, ByVal testId As String, ByVal grdSysDetailId As String, ByVal user As String) As String

        Return (New TranscriptDB).UpdateSingleGrade(resultId, testId, grdSysDetailId, user)

    End Function
    Public Function UpdateSingleGradeTermAllEnrollments(ByVal resultId As String, ByRef clsSectionOrTermId As String, ByVal reqId As String, ByVal testId As String, ByVal grdSysDetailId As String, ByVal user As String, ByVal reqDescr As String, ByVal bTermChanged As Boolean, ByVal stuEnrollId As String) As String

        Return (New TranscriptDB).UpdateSingleGradeTermAllEnrollments(resultId, clsSectionOrTermId, reqId, testId, grdSysDetailId, user, reqDescr, bTermChanged, stuEnrollId)

    End Function
    Public Function DeleteSingleGrade(ByVal resultId As String, ByVal testId As String, ByVal user As String, ByVal stuEnrollId As String) As String

        Return (New TranscriptDB).DeleteSingleGrade(resultId, testId, user, stuEnrollId)
    End Function

    Public Shared Function DeleteSingleGradeAllEnrollments(resultId As String, testId As String, user As String, stuEnrollId As String) As String

        Return (New TranscriptDB).DeleteSingleGradeAllEnrollments(resultId, testId, user, stuEnrollId)
    End Function

    Public Function UpdateSingleClassSectionId(ByVal resultId As String, ByVal testId As String, ByVal clsSectionId As String, ByVal user As String, Optional ByVal reqId As String = "", Optional ByVal reqDesc As String = "") As String

        Return (New TranscriptDB).UpdateSingleClassSectionId(resultId, testId, clsSectionId, user, reqId, reqDesc)

    End Function
    Public Function GetAllTermsInWhichAReqHasBeenTaught(ByVal resultId As String) As DataSet

        Return (New TranscriptDB).GetAllTermsInWhichAReqHasBeenTaught(resultId)
    End Function
    Public Function GetStudentName(ByVal StuEnrollID As String) As String
        Dim ds As New DataSet
        With New TranscriptDB
            'get the dataset with all SkillGroups
            Return .GetStudentName(StuEnrollID)
        End With
    End Function
    Public Function GetClassesWithStudentStartDate(ByVal StuEnrollId As String)
        With New LeadEnrollmentDB
            Return .GetClassesWithStudentStartDate(StuEnrollId)
        End With
    End Function
    Public Function GetCoursesForProgram(ByVal programId As String, Optional ByVal campusId As String = "", Optional ByVal activeOnly As Boolean = False) As DataTable
        Dim db As New TermProgressDB
        Return db.GetCoursesForProgram(programId, campusId, activeOnly)
    End Function
    Public Function GetCoursesForTerm(ByVal programId As String, ByVal termId As String, ByVal campusid As String, Optional ByVal activeOnly As Boolean = False) As DataTable
        Dim db As New TermProgressDB
        Return db.GetCoursesForTerm(programId, termId, campusid, activeOnly)
    End Function

    Public Function GetPrograms(ByVal campusId As String) As DataTable
        Dim db As New TranscriptDB

        Return db.GetPrograms(campusId)

    End Function

    Public Function GetTerms(ByVal campusId As String, ByVal progId As String) As DataTable
        Dim db As New TranscriptDB

        Return db.GetTerms(campusId, progId)

    End Function

    'Public Function RegisterStudentsInClassesRestrictedByStudentStart(ByVal campusId As String, ByVal progId As String, ByVal termId As String, ByVal user As String) As DataTable
    '    Dim db As New TranscriptDB
    '    Dim dt As New DataTable
    '    Dim msg As String
    '    Dim dr As DataRow
    '    Dim drRows As DataRow()
    '    Dim dtProcessed As New DataTable
    '    Dim dtAvailableStudents As New DataTable
    '    Dim dtStudentClasses As New DataTable
    '    Dim br As New RegisterStudentsBR
    '    Dim dbReg As New RegisterDB
    '    dtProcessed.Columns.Add("ClsSectionId", Type.GetType("System.String"))

    '    dtStudentClasses.Columns.Add("StuEnrollId", Type.GetType("System.String"))
    '    dtStudentClasses.Columns.Add("ClsSectionId", Type.GetType("System.String"))
    '    dtStudentClasses.Columns.Add("Program", Type.GetType("System.String"))
    '    dtStudentClasses.Columns.Add("Course", Type.GetType("System.String"))
    '    dtStudentClasses.Columns.Add("Code", Type.GetType("System.String"))
    '    dtStudentClasses.Columns.Add("Section", Type.GetType("System.String"))
    '    dtStudentClasses.Columns.Add("StartDate", Type.GetType("System.DateTime"))
    '    dtStudentClasses.Columns.Add("EndDate", Type.GetType("System.DateTime"))
    '    dtStudentClasses.Columns.Add("LastName", Type.GetType("System.String"))
    '    dtStudentClasses.Columns.Add("FirstName", Type.GetType("System.String"))

    '    dt = db.GetAvailableClassesRestrictedByStudentStart(campusId, progId, termId)
    '    If dt.Rows.Count = 0 Then
    '        msg += "There were no available classes found." + vbCr
    '    Else
    '        'There are available classes. We want to loop through each class in the dt.
    '        'If the class is not already in the dtProcessed dt then we will get all classes
    '        'that has the same course (reqid).
    '        For Each dr In dt.Rows
    '            'Check if the class section associated with the row has been processed already
    '            If Not HasClassBeenProcessed(dr("ClsSectionId").ToString, dtProcessed) Then
    '                'We want to get all the classes that are based on the course of this class
    '                'There will always be at least one class but you might have more than one
    '                'when the school has to create different classes for the same course because
    '                'of the number of the students.
    '                drRows = GetClassesBasedOnCourse(dr("ReqId").ToString, dr("ShiftId").ToString, dr("TermId").ToString, dt)
    '                'Get the list of available enrollments for the course that the class is based on.
    '                'We are assuming that even if there are more than one class based on the same
    '                'course that the student start date restriction will be the same for all of them.
    '                dtAvailableStudents = db.GetAvailableStudentsForClassRestrictedByStartDate(dr("ClsSectionId").ToString)
    '                If dtAvailableStudents.Rows.Count > 0 Then
    '                    'Add the students to the classes in the array dtRows.
    '                    'Do each class until it is full and then move on to the next one.
    '                    For Each drAS As DataRow In dtAvailableStudents.Rows
    '                        If Not (drRows Is Nothing) Then
    '                            For Each drClass As DataRow In drRows
    '                                If drClass("Available") > 0 Then
    '                                    'Create an entry for the student and class in dtStudentClasses
    '                                    'if the student shift matches that of the class
    '                                    If drAS("ShiftId").ToString <> "" And drClass("ShiftId").ToString <> "" And drAS("ShiftId").ToString = drClass("ShiftId").ToString Then

    '                                        'If (br.HasStdMetPrereqs(drAS("StuEnrollId").ToString, drClass("ClsSectionId").ToString, progId)) Then
    '                                        'If (br.HasStdMetPrereqs(drAS("StuEnrollId").ToString, drClass("ClsSectionId").ToString, progId)) Then
    '                                        If (br.HasStdMetPrereqs(drAS("StuEnrollId").ToString, drClass("ClsSectionId").ToString, drAS("PrgVerId").ToString)) Then
    '                                            'If (dbReg.GetStdGrade(drAS("StuEnrollId").ToString, drClass("ClsSectionId").ToString, dr("ReqId").ToString) = "") Then
    '                                            If Not (br.DoesStdHavePassingGrd(drAS("StuEnrollId").ToString, dr("ReqId").ToString, drAS("PrgVerId").ToString, drClass("ClsSectionId").ToString)) Then
    '                                                'if (br.DoesStdHavePassingGrd(drAS("StuEnrollId").ToString, ByVal ReqId As String, ByVal PrgVerId As String, ByVal ClsSectId As String) ) then
    '                                                If ((progId = "") Or (progId = drAS("ProgId").ToString)) Then
    '                                                    If db.GetScheduleCount(drClass("ClsSectionId").ToString, drAS("StuEnrollId").ToString) = 0 Then
    '                                                        Dim drSC As DataRow = dtStudentClasses.NewRow
    '                                                        drSC("StuEnrollId") = drAS("StuEnrollId").ToString
    '                                                        drSC("Program") = drAS("ProgDescrip").ToString
    '                                                        drSC("FirstName") = drAS("FirstName").ToString
    '                                                        drSC("LastName") = drAS("LastName").ToString
    '                                                        drSC("Course") = drClass("Descrip").ToString
    '                                                        drSC("Code") = drClass("Code").ToString
    '                                                        drSC("StartDate") = drClass("StartDate").ToShortDateString
    '                                                        drSC("EndDate") = drClass("EndDate").ToShortDateString
    '                                                        drSC("Section") = drClass("ClsSection")
    '                                                        drSC("ClsSectionId") = drClass("ClsSectionId").ToString
    '                                                        dtStudentClasses.Rows.Add(drSC)
    '                                                    End If
    '                                                End If
    '                                            End If
    '                                        End If
    '                                        'Decrement the available slots
    '                                        drClass("Available") = drClass("Available") - 1
    '                                        'Exit as we only want to add the student to the first available class
    '                                        Exit For

    '                                    End If

    '                                End If
    '                            Next
    '                        End If
    '                    Next


    '                End If

    '                'Add the class(es) to the list of processed class. We cannot simply add the class
    '                'that we are processing now. We have to use the array containing the classes based
    '                'on the course of the current class.
    '                If Not (drRows Is Nothing) Then
    '                    For Each drCBC As DataRow In drRows
    '                        Dim drCS As DataRow = dtProcessed.NewRow
    '                        drCS("ClsSectionId") = drCBC("ClsSectionId")
    '                        dtProcessed.Rows.Add(drCS)
    '                    Next
    '                End If


    '            End If
    '        Next

    '        'At this point if there are any rows in the dtStudentClasses dt then we should send it
    '        'off to be processed by the DB layer
    '        If dtStudentClasses.Rows.Count > 0 Then
    '            'msg = db.RegisterStudentsForClasses(dtStudentClasses, user)

    '        Else
    '            'msg = "There were no available students found"
    '        End If

    '    End If


    '    Return dtStudentClasses

    'End Function

    Public Function StudentsInClassesRestrictedByStudentStart(ByVal campusId As String, ByVal progId As String, ByVal termId As String, ByVal user As String, ByVal leadGrpid As String) As DataTable
        Dim db As New TranscriptDB
        Dim dt As New DataTable
        Dim msg As String
        Dim dr As DataRow
        Dim drRows As DataRow()
        Dim dtProcessed As New DataTable
        Dim dtProcessed_New As New DataTable
        Dim dtAvailableStudents As New DataTable
        Dim dtAvailableClassStudents As New DataTable
        Dim dtStudentClasses As New DataTable
        Dim br As New RegisterStudentsBR
        Dim dbReg As New RegisterDB
        Dim cnt As Integer = 0

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dtProcessed.Columns.Add("ClsSectionId", Type.GetType("System.String"))
        dtProcessed_New.Columns.Add("ClsSectionId", Type.GetType("System.String"))

        dtStudentClasses.Columns.Add("StuEnrollId", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("ClsSectionId", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("Program", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("Course", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("Code", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("Section", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("StartDate", Type.GetType("System.DateTime"))
        dtStudentClasses.Columns.Add("EndDate", Type.GetType("System.DateTime"))
        dtStudentClasses.Columns.Add("LastName", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("FirstName", Type.GetType("System.String"))
        dtStudentClasses.Columns.Add("TermId", Type.GetType("System.String"))


        'If we are dealing with a school that schedules using CohortStartDate then
        'we need to unregister students from any classes that belong to a CohortStartDate
        'where they fail one or more courses. The CohortStartDate on the enrollment needs
        'to be updated to the next available one.
        'db.UnRegisterCohortStartDateClasses(campusId, progId)


        dt = db.GetAvailableClassesRestrictedByStudentStart_SP(campusId, progId, termId)
        If dt.Rows.Count = 0 Then
            msg += "There were no available classes found." + vbCr
        Else
            'There are available classes. We want to loop through each class in the dt.
            'If the class is not already in the dtProcessed dt then we will get all classes
            'that has the same course (reqid).
            For Each dr In dt.Rows
                'Check if the class section associated with the row has been processed already
                'If Not HasClassBeenProcessed(dr("ClsSectionId").ToString, dtProcessed) Then
                'We want to get all the classes that are based on the course of this class
                'There will always be at least one class but you might have more than one
                'when the school has to create different classes for the same course because
                'of the number of the students.
                drRows = GetClassesBasedOnCourse(dr("ReqId").ToString, dr("ShiftId").ToString, dr("TermId").ToString, dt)
                'Get the list of available enrollments for the course that the class is based on.
                'We are assuming that even if there are more than one class based on the same
                'course that the student start date restriction will be the same for all of them.
                '*---------------------------------------------------------------------------------------------------*'
                'replaced this function with stored procedure by Theresa G on 10/22/09
                'when fixing issue 15154: ENH: Run schedule by student group 
                '*---------------------------------------------------------------------------------------------------*'
                'dtAvailableStudents = db.GetAvailableStudentsForClassRestrictedByStartDate(dr("ClsSectionId").ToString)
                dtAvailableStudents = db.GetAvailableStudentsForClassRestrictedByStartDate_SP(dr("ClsSectionId").ToString, leadGrpid, campusId)
                If Not dtAvailableStudents Is Nothing AndAlso dtAvailableStudents.Rows.Count > 0 Then
                    'Add the students to the classes in the array dtRows.
                    'Do each class until it is full and then move on to the next one.

                    For Each drAS As DataRow In dtAvailableStudents.Rows
                        If Not (drRows Is Nothing) Then
                            For Each drClass As DataRow In drRows
                                If drClass("Available") > 0 Then
                                    If drClass("ClsSectionId").ToString <> dr("ClsSectionId").ToString Then
                                        'dtAvailableClassStudents = db.GetAvailableStudentsForClassRestrictedByStartDate(drClass("ClsSectionId").ToString)
                                        dtAvailableClassStudents = db.GetAvailableStudentsForStudentGroups(drClass("ClsSectionId").ToString, dr("ClsSectionId").ToString)
                                        If Not HasClassBeenProcessed(drClass("ClsSectionId").ToString, dtProcessed_New) Then
                                            ''CampusId added by Saraswathi lakshmanan on August 05 2009
                                            ''For mantis case 16753

                                            cnt = HasStudentMetReqs(dtAvailableClassStudents, drClass("ClsSectionId").ToString, dr("ClsSectionId").ToString, progId, dr("ReqId").ToString, campusId)
                                            drClass("Available") = drClass("Available") - cnt
                                            If cnt > 0 Then
                                                Dim drSC As DataRow = dtStudentClasses.NewRow
                                                drSC("StuEnrollId") = dtAvailableClassStudents.Rows(0)("StuEnrollId").ToString
                                                'drSC("Program") = dtAvailableClassStudents.Rows(0)("ShiftDescrip").ToString
                                                drSC("Program") = dtAvailableClassStudents.Rows(0)("ProgDescrip").ToString
                                                drSC("FirstName") = dtAvailableClassStudents.Rows(0)("FirstName").ToString
                                                drSC("LastName") = dtAvailableClassStudents.Rows(0)("LastName").ToString
                                                drSC("Course") = drClass("Descrip").ToString
                                                drSC("Code") = drClass("Code").ToString
                                                drSC("StartDate") = drClass("StartDate").ToShortDateString
                                                drSC("EndDate") = drClass("EndDate").ToShortDateString
                                                drSC("Section") = drClass("ClsSection")
                                                drSC("ClsSectionId") = drClass("ClsSectionId").ToString
                                                drSC("TermId") = drClass("TermId")
                                                dtStudentClasses.Rows.Add(drSC)
                                                Dim drCS As DataRow = dtProcessed_New.NewRow
                                                drCS("ClsSectionId") = drClass("ClsSectionId")

                                                dtProcessed_New.Rows.Add(drCS)
                                                cnt = 0
                                                Exit For
                                            End If

                                        End If

                                    End If
                                    'Create an entry for the student and class in dtStudentClasses
                                    'if the student shift matches that of the class
                                    If drAS("ShiftId").ToString <> "" And drClass("ShiftId").ToString <> "" And drAS("ShiftId").ToString = drClass("ShiftId").ToString Then

                                        ''added by Saraswathi lakshmanan to fix the  lead group issue for Hair Dynamics. The student was in a different lead group and he was assigned to a class section in another lead group
                                        ''Modified on Sept 02 2010

                                        If (drAS("LeadGrpID").ToString = "" Or drClass("LeadGrpID").ToString = "" Or drAS("LeadGrpID").ToString = drClass("LeadGrpID").ToString) Then


                                            If (drClass("StudentStartDate").ToString = "" Or drAS("ExpStartDate").ToString = drClass("StudentStartDate").ToString) Then

                                                'If (br.HasStdMetPrereqs(drAS("StuEnrollId").ToString, drClass("ClsSectionId").ToString, progId)) Then
                                                'If (br.HasStdMetPrereqs(drAS("StuEnrollId").ToString, drClass("ClsSectionId").ToString, progId)) Then

                                                ''CampusId added by Sartaswathi lakshmanan on August 05 2009
                                                ''For mantis case 16753

                                                If (br.HasStdMetPrereqs(drAS("StuEnrollId").ToString, drClass("ClsSectionId").ToString, drAS("PrgVerId").ToString, campusId, MyAdvAppSettings.AppSettings("IgnorePrereqsForRunSchedules"))) Then
                                                    'If (dbReg.GetStdGrade(drAS("StuEnrollId").ToString, drClass("ClsSectionId").ToString, dr("ReqId").ToString) = "") Then
                                                    If Not (br.DoesStdHavePassingGrd(drAS("StuEnrollId").ToString, dr("ReqId").ToString, drAS("PrgVerId").ToString, drClass("ClsSectionId").ToString, campusId)) Then
                                                        'if (br.DoesStdHavePassingGrd(drAS("StuEnrollId").ToString, ByVal ReqId As String, ByVal PrgVerId As String, ByVal ClsSectId As String) ) then
                                                        If ((progId = "") Or (progId = drAS("ProgId").ToString)) Then
                                                            If db.GetScheduleCount(drClass("ClsSectionId").ToString, drAS("StuEnrollId").ToString) = 0 Then
                                                                If Not HasClassBeenProcessed(drAS("StuEnrollId").ToString, drClass("Code").ToString, dtStudentClasses) Then
                                                                    Dim drSC As DataRow = dtStudentClasses.NewRow
                                                                    drSC("StuEnrollId") = drAS("StuEnrollId").ToString
                                                                    drSC("Program") = drAS("ProgDescrip").ToString
                                                                    drSC("FirstName") = drAS("FirstName").ToString
                                                                    drSC("LastName") = drAS("LastName").ToString
                                                                    drSC("Course") = drClass("Descrip").ToString
                                                                    drSC("Code") = drClass("Code").ToString
                                                                    drSC("StartDate") = drClass("StartDate").ToShortDateString
                                                                    drSC("EndDate") = drClass("EndDate").ToShortDateString
                                                                    drSC("Section") = drClass("ClsSection")
                                                                    drSC("ClsSectionId") = drClass("ClsSectionId").ToString
                                                                    drSC("TermId") = drClass("TermId")
                                                                    dtStudentClasses.Rows.Add(drSC)
                                                                    drClass("Available") = drClass("Available") - 1

                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If


                                                'Decrement the available slots
                                                'drClass("Available") = drClass("Available") - 1
                                                'Exit as we only want to add the student to the first available class
                                                Exit For
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    Next


                End If

                'Add the class(es) to the list of processed class. We cannot simply add the class
                'that we are processing now. We have to use the array containing the classes based
                'on the course of the current class.
                If Not (drRows Is Nothing) Then
                    For Each drCBC As DataRow In drRows
                        Dim drCS As DataRow = dtProcessed.NewRow
                        drCS("ClsSectionId") = drCBC("ClsSectionId")
                        dtProcessed.Rows.Add(drCS)
                    Next
                End If


                ' End If
            Next

            'At this point if there are any rows in the dtStudentClasses dt then we should send it
            'off to be processed by the DB layer


        End If


        Return dtStudentClasses

    End Function
    ''Campusid added by Saraswathi lakshamnan on August 05 2009
    ''For mantis case 16753

    Private Function HasStudentMetReqs(ByVal dtAvailableClassStudents As DataTable, ByVal clsSection As String, ByVal parentClsSection As String, ByVal progId As String, ByVal reqid As String, ByVal CampusId As String) As Integer

        Dim db As New TranscriptDB
        Dim br As New RegisterStudentsBR

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim resultCount As Integer = 0
        For Each drClass As DataRow In dtAvailableClassStudents.Rows
            If drClass("ShiftId").ToString <> "" Then

                'If (br.HasStdMetPrereqs(drAS("StuEnrollId").ToString, drClass("ClsSectionId").ToString, progId)) Then
                'If (br.HasStdMetPrereqs(drAS("StuEnrollId").ToString, drClass("ClsSectionId").ToString, progId)) Then

                ''CampusId added by Saraswathi lakshmanan on August 05 2009
                ''For mantis case 16753

                If (br.HasStdMetPrereqs(drClass("StuEnrollId").ToString, clsSection, drClass("PrgVerId").ToString, CampusId, MyAdvAppSettings.AppSettings("IgnorePrereqsForRunSchedules"))) Then
                    'If (dbReg.GetStdGrade(drAS("StuEnrollId").ToString, drClass("ClsSectionId").ToString, dr("ReqId").ToString) = "") Then
                    If Not (br.DoesStdHavePassingGrd(drClass("StuEnrollId").ToString, reqid, drClass("PrgVerId").ToString, clsSection)) Then
                        'if (br.DoesStdHavePassingGrd(drAS("StuEnrollId").ToString, ByVal ReqId As String, ByVal PrgVerId As String, ByVal ClsSectId As String) ) then
                        If ((progId = "") Or (progId = drClass("ProgId").ToString)) Then
                            If db.GetScheduleCount(clsSection, drClass("StuEnrollId").ToString) = 0 Then
                                resultCount = resultCount + 1
                            End If
                        End If
                    End If
                End If
            End If
        Next

        Return resultCount
    End Function

    Public Function RegisterStudentsInClassesRestrictedByStudentStart(ByVal dtStudentClasses As DataTable, ByVal user As String) As DataTable
        Dim db As New TranscriptDB
        Dim msg As String
        If dtStudentClasses.Rows.Count > 0 Then
            msg = db.RegisterStudentsForClasses(dtStudentClasses, user)

        Else
            'msg = "There were no available students found"
        End If
        Return dtStudentClasses
    End Function
    Public Function GetGradeDescriptionsForEnrollment(ByVal stuEnrollId As String) As DataTable
        Dim db As New TranscriptDB

        Return db.GetGradeDescriptionsForEnrollment(stuEnrollId)

    End Function
    Public Function GetGradeDescriptionsForEnrollmentNotNULL(ByVal stuEnrollId As String) As DataTable
        Dim db As New TranscriptDB

        Return db.GetGradeDescriptionsForEnrollmentNotNULL(stuEnrollId)

    End Function
    Public Function GetGradeDescriptionsForTranscript_SP(ByVal stuEnrollId As String) As DataSet
        Dim db As New TranscriptDB

        Return db.GetGradeDescriptionsForTranscript_SP(stuEnrollId)

    End Function
    'Public Function GetScheduleCount(ByVal clsSectionId As String, ByVal stuEnrollId As String) As Integer
    '    Dim db As New TranscriptDB
    '    Return db.GetScheduleCount(clsSectionId, stuEnrollId)
    'End Function

    Private Function HasClassBeenProcessed(ByVal clsSectionId As String, ByVal dtProcessed As DataTable) As Boolean
        Dim arrRows() As DataRow
        Dim convertedClsSectionId As Guid
        Dim filter As String
        'If the dataset is empty then simply returns false
        If dtProcessed.Rows.Count = 0 Then
            Return False
        Else
            'Search the DataTable

            convertedClsSectionId = XmlConvert.ToGuid(clsSectionId)
            filter = "ClsSectionId = '" & convertedClsSectionId.ToString & "'"
            arrRows = dtProcessed.Select(filter)
            If arrRows.Length = 0 Then
                Return False
            Else
                Return True
            End If
        End If
    End Function
    Private Function HasClassBeenProcessed(ByVal stuEnrollid As String, ByVal reqid As String, ByVal dtProcessed As DataTable) As Boolean
        Dim arrRows() As DataRow
        Dim convertedStuEnrollid As Guid
        Dim filter As String
        'If the dataset is empty then simply returns false
        If dtProcessed.Rows.Count = 0 Then
            Return False
        Else
            'Search the DataTable

            convertedStuEnrollid = XmlConvert.ToGuid(stuEnrollid)
            filter = "stuEnrollid = '" & convertedStuEnrollid.ToString & "' and code= '" & reqid & "'"
            arrRows = dtProcessed.Select(filter)
            If arrRows.Length = 0 Then
                Return False
            Else
                Return True
            End If
        End If
    End Function


    Private Function GetClassesBasedOnCourse(ByVal reqId As String, ByVal shiftid As String, ByVal termId As String, ByVal dt As DataTable) As DataRow()
        Dim arrRows() As DataRow
        Dim convertedReqId As Guid
        Dim convertedShiftId As Guid
        Dim convertedTermId As Guid
        Dim filter As String

        'Skip classes that do not have a shift on them. This process is meant to be run for classes
        'based on their shifts. We don't want the school to accidentally have students enrolled
        'in classes that are not the correct shift.
        If shiftid <> "" Then
            convertedReqId = XmlConvert.ToGuid(reqId)
            convertedShiftId = XmlConvert.ToGuid(shiftid)
            convertedTermId = XmlConvert.ToGuid(termId)

            filter = "ReqId = '" & convertedReqId.ToString & "' AND ShiftId = '" & convertedShiftId.ToString & "' AND TermId = '" & convertedTermId.ToString & "' "
            ''Filter Condition Added by Saraswathi Lakshmanan to bring the null values to the last
            ''Modified on Sept 26 2010
            arrRows = dt.Select(filter, "LeadGrpId desc,StudentStartDAte Desc ,ClsSection Asc")

        End If

        Return arrRows

    End Function
    Public Function GetProgramForTerm(ByVal TermId As String) As String
        Dim db As New TermProgressDB

        Return db.GetProgramForTerm(TermId)
    End Function

    Public Function OverrideGrd(ByVal PrgVerId As String, ByVal ReqId As String) As String
        Return (New TranscriptDB).OverrideGrd(PrgVerId, ReqId)
    End Function

    Public Sub UpdateOverRideGrade(ByVal GrdSysDetailId As String, ByVal PrgVerId As String, ByVal ReqId As String)
        Dim db As New TranscriptDB
        db.UpdateOverRideGrade(GrdSysDetailId, PrgVerId, ReqId)
    End Sub
    Public Function GetCourseGroup(ByVal ReqId As String) As Integer
        Dim DB As New TermProgressDB
        Return DB.GetCourseGroup(ReqId)
    End Function
    Public Function IsStudentScheduledForClasses(ByVal stuEnrollId As String) As Boolean
        Dim db As New TranscriptDB

        If db.GetNumberOfClassesScheduledForStudent(stuEnrollId) = 0 Then
            Return False
        Else
            Return True
        End If

    End Function
    ''Optional Parameter added by Saraswathi Lakshmanan
    ''When Transferrring from one Program Version to Another,
    'the details in the arresults table should be deleted for the courses 
    'which are not graded

    Public Function ReScheduleStudentWithCohortStartDate(ByVal stuEnrollId As String, ByVal newCohortStartDate As String, ByVal modUser As String, ByVal campusId As String, Optional ByVal oldStuEnrollId As String = "") As String
        Dim db As New TranscriptDB

        Return db.ReScheduleStudentWithCohortStartDate(stuEnrollId, newCohortStartDate, modUser, campusId, oldStuEnrollId)
    End Function

    Public Function GetExpGradDate(ByVal stuEnrollId As String) As String
        Dim db As New TranscriptDB
        Return db.GetExpGradDate(stuEnrollId)
    End Function

    Public Function IsStudentCurrentlyInSchool(ByVal stuEnrollId) As Boolean
        Dim db As New TranscriptDB

        Return db.IsStudentCurrentlyInSchool(stuEnrollId)

    End Function

    ''Able to graduate student who has transfer grade
    'refactored jguirado
    Public Function GetGradesSysDetailId() As List(Of String) 'DataSet'
        Dim db As New TranscriptDB
        Dim ds As DataSet = db.GetGradesSysDetailId()
        'Return ds
        Dim result As New List(Of String)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                result.Add(row("GrdSysDetailId").ToString())
            Next
        End If
        Return result
    End Function



    ''Reschedule reason added to a new table arSchReschReason
    Public Function AddRescheduleReason(ByVal stuEnrollId As String, ByVal modUser As String, ByVal reschReason As String) As String

        Dim db As New TranscriptDB

        Return db.AddRescheduleReason(stuEnrollId, modUser, reschReason)
    End Function

    ''Added by Saraswathi Lakshmanan on June 24 2010
    Public Function GetCoursesBelongingtotheProgramVersion(ByVal StuEnrollId As String, ByVal PrgVerID As String, Optional ByVal IncludeCourseEquivalents As Boolean = False) As DataTable

        Dim db As New TranscriptDB
        Return db.GetCoursesinaProgVersion(StuEnrollId, PrgVerID, IncludeCourseEquivalents)

    End Function
    Public Function isCourseCompleted(ByVal ResultId As String) As Boolean
        Dim db As New TranscriptDB
        Return db.isCourseCompleted(ResultId)
    End Function
    Public Function getExternshipCourses(ByVal StuEnrollId As String) As DataTable
        Dim db As New TranscriptDB
        Return db.getExternshipCourses(StuEnrollId)
    End Function

    Public Shared Function GetEnrollmentListWithAll(ByVal studentId As String, ByVal params As String) As List(Of EnrollmentItemsDto)

        Dim list As List(Of EnrollmentItemsDto) = GetEnrollmentList(studentId, HttpContext.Current.Request.Params("cmpid"))
        If (list.Count > 1) Then
            Dim dto As EnrollmentItemsDto = New EnrollmentItemsDto()
            dto.StuEnrollId = Guid.Empty.ToString()
            dto.PrgVerDescrip = Guid.Empty.ToString()
            dto.PrgVerDescrip = "All Enrollments"
            list.Add(dto)
        End If
        Return list
    End Function

    Public Function ShouldSelectedEnrollmentBeProcessed(ByVal stuEnrollId As String) As Boolean
        Dim db As New TranscriptDB
        Dim res As Boolean

        res = db.IsEnrollmentForCEProgramVersion(stuEnrollId)

        'If we are not dealing with a CE pv then we can go ahead and return true
        If res = False Then
            Return True
        Else
            'We are dealing with a CE pv so we need to make sure the student is registered for classes
            Return CBool(db.IsStudentRegisteredForClasses(stuEnrollId))
        End If
    End Function
End Class


