' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' PrefixesFacade.vb
'
' PrefixesFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class PrefixesFacade
    Public Function GetAllPrefixes() As DataSet

        '   Instantiate DAL component
        Dim prefixesDB As New prefixesDB

        '   get the dataset with all Prefixes
        Return prefixesDB.GetAllPrefixes()

    End Function
    Public Function GetAllCountries() As DataSet

        '   Instantiate DAL component
        Dim prefixesDB As New PrefixesDB

        '   get the dataset with all Prefixes
        Return prefixesDB.GetAllCountries()

    End Function
    Public Function GetAllAddressType() As DataSet

        '   Instantiate DAL component
        Dim prefixesDB As New prefixesDB

        '   get the dataset with all Prefixes
        Return prefixesDB.GetAllAddressTypes()

    End Function
End Class
