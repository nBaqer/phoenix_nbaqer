﻿Imports System.Diagnostics.Eventing.Reader
Imports FAME.AdvantageV1.BusinessFacade.StudentEnrollState
Imports Telerik.Web.Analytics

Public Class StatusCodeChangeFacade
    Private _statusType As Common.Enumerations.SystemStatuses
    Private _isSupportUser As Boolean

    Public Sub New(ByVal statusType As Common.Enumerations.SystemStatuses, ByVal isSupportUser As Boolean)
        _statusType = statusType
        _isSupportUser = isSupportUser
    End Sub

    Private Function ConvertModel(ByVal statusChange As SystemStatusChangeModel) As StudentChangeHistoryObj
        Dim record As New StudentChangeHistoryObj

        record.DateOfChange = statusChange.EffectiveDate
        record.CampusId = statusChange.CampusId
        record.CaseNumber = statusChange.CampusId

        If (_statusType = Enumerations.SystemStatuses.LeaveOfAbsence) Then
            record.LOARequestDate = statusChange.DateOfChange
            record.LoaReasonGuid = statusChange.Reason
            If (Not String.IsNullOrEmpty(statusChange.EndDate)) Then
                record.EndDate = DateTime.Parse(statusChange.EndDate)
            End If
        ElseIf (_statusType = Enumerations.SystemStatuses.Suspension) Then
            record.Reason = statusChange.Reason
            If (Not String.IsNullOrEmpty(statusChange.EndDate)) Then
                record.EndDate = DateTime.Parse(statusChange.EndDate)
            End If
        ElseIf (_statusType = Enumerations.SystemStatuses.Dropped) Then
            record.DropReasonGuid = statusChange.Reason
            record.DateDetermined = record.DateOfChange
        ElseIf (_statusType = Enumerations.SystemStatuses.CurrentlyAttending) Then
            record.DateOfReenrollment = statusChange.EffectiveDate
        ElseIf (_statusType = Enumerations.SystemStatuses.Graduated) Then
            record.ExpGradDate = record.DateOfChange
        End If

        record.ModDate = statusChange.DateOfChange
        record.ModUser = statusChange.UserId
        record.NewStatusGuid = statusChange.StatusCodeId.ToString()
        record.NewSysStatusId = statusChange.StatusId
        record.OrigStatusGuid = statusChange.PreviousStatusCodeId.ToString()
        record.OldSysStatusId = statusChange.PreviousStatusId
        record.StuEnrollId = statusChange.StudentEnrollmentId
        record.RequestedBy = statusChange.RequestedBy.ToString()
        'If Previous Status Was Academic Probation SAP
        'Set the PreviousStudentStatusChangeId to be the System Status Id of that previous status (Academic Probation SAP)
        If (statusChange.PreviousStatusId = Enumerations.SystemStatuses.AcademicProbationSap) Then
            'record.PreviousStudentStatusChangeId = statusChange.
        End If

        'record.ProbWarningTypeId'TODO Find where and how this is used
        'record.SapPeriod'TODO Find where and how this is used
        'record.StartDate'TODO Find where and how this is used
        'record.StudentStatus'TODO Find where and how this is used

        Return record
    End Function

    Public Function Add(ByVal statusChange As SystemStatusChangeModel) As Response
        Dim response As Response
        response.Message = String.Empty
        response.ResponseType = Common.Enumerations.ResponseTypes.Success

        Dim record As StudentChangeHistoryObj = ConvertModel(statusChange)
        response = ValidateFields(record, statusChange)

        If (response.ResponseType = Common.Enumerations.ResponseTypes.Success) Then
            Select Case _statusType

                Case Common.Enumerations.SystemStatuses.CurrentlyAttending
                    'To Change Status To Currently Attending, No Specials Validations are required.
                    Dim state As New CurrentlyAttendingState
                    response.Message = state.ChangeStatusToCurrAttending(record)

                Case Common.Enumerations.SystemStatuses.DisciplinaryProbation
                    response = CanChangeToDisciplinaryProbation(record)
                    If (response.ResponseType = Common.Enumerations.ResponseTypes.Success) Then
                        Dim facade As ProbationFacade
                        response.Message = facade.PlaceStudentOnProbation(record)
                    End If
                Case Common.Enumerations.SystemStatuses.Dropped
                    response = CanChangeToDropped(record)
                    If (response.ResponseType = Common.Enumerations.ResponseTypes.Success) Then
                        response.Message = TerminateFacade.Drop(record)
                    End If
                Case Common.Enumerations.SystemStatuses.Externship
                    response = CanChangeToExternship(record)
                    If (response.ResponseType = Common.Enumerations.ResponseTypes.Success) Then
                        Dim state As New CurrentlyAttendingState
                        response.Message = state.ChangeStatusToExternship(record)
                    End If
                Case Common.Enumerations.SystemStatuses.FutureStart
                    response = CanChangeToFutureStart(record)
                    If (response.ResponseType = Common.Enumerations.ResponseTypes.Success) Then
                        Dim state As New CurrentlyAttendingState
                        response.Message = state.ChangeStatusToFutureStart(record)
                    End If
                Case Common.Enumerations.SystemStatuses.LeaveOfAbsence
                    response = CanChangeToLeaveOfAbsent(record)
                    If (response.ResponseType = Common.Enumerations.ResponseTypes.Success) Then
                        Dim state As New LOAFacade
                        response.Message = state.PlaceStudentOnLOA(record)
                    End If
                Case Common.Enumerations.SystemStatuses.Graduated
                    response = CanChangeToGraduated(record)
                    If (response.ResponseType = Common.Enumerations.ResponseTypes.Success) Then
                        Dim state As New GraduatedState
                        response.Message = state.ChangeStatusToGraduated(record)
                    End If
                Case Common.Enumerations.SystemStatuses.Suspension
                    Dim facade As New LOAFacade
                    response = CanChangeToSuspended(record, facade)
                    If (response.ResponseType = Common.Enumerations.ResponseTypes.Success) Then
                        response.Message = facade.PlaceStudentOnSuspension(record)
                    End If
                Case Common.Enumerations.SystemStatuses.TransferOut
                    response = CanChangeToTransferOut(record)
                    If (response.ResponseType = Common.Enumerations.ResponseTypes.Success) Then
                        Dim state As New CurrentlyAttendingState
                        response.Message = state.ChangeStatusToTransferOut(record)
                    End If
                Case Common.Enumerations.SystemStatuses.WarningProbation
                    response = CanChangeToWarningProbation(record)
                    If (response.ResponseType = Common.Enumerations.ResponseTypes.Success) Then
                        Dim state As New ProbationFacade
                        response.Message = state.PlaceStudentOnProbation(record)
                    End If
                Case Common.Enumerations.SystemStatuses.NoStart
                    response = CanChangeToNoStart(record)
                    If (response.ResponseType = Common.Enumerations.ResponseTypes.Success) Then
                        Dim state As New CurrentlyAttendingState
                        response.Message = state.ChangeStatusToNoStart(record)
                    End If
                Case Else
                    response.ResponseType = Common.Enumerations.ResponseTypes.Warning
                    response.Message = "Unable to change to the selected status code"
            End Select

        End If

        If (response.Message <> "" And response.ResponseType = Common.Enumerations.ResponseTypes.Success) Then
            'Exception Happended Changing the Status
            response.ResponseType = Common.Enumerations.ResponseTypes.ExceptionHappened
        End If

        Return response
    End Function

    Public Function Remove(ByVal statusChange As SystemStatusChangeModel) As Response
        Dim response As Response
        response.Message = String.Empty
        response.ResponseType = Common.Enumerations.ResponseTypes.Success

        Dim record As StudentChangeHistoryObj
    End Function

    Public Function Update(ByVal statusChange As SystemStatusChangeModel) As Response
        Dim response As Response
        response.Message = String.Empty
        response.ResponseType = Common.Enumerations.ResponseTypes.Success

        Dim record As StudentChangeHistoryObj

    End Function

    Private Function ValidateFields(ByVal record As StudentChangeHistoryObj, ByVal inputModel As SystemStatusChangeModel) As Response
        Dim response As Response
        response.ResponseType = Common.Enumerations.ResponseTypes.Success

        If (String.IsNullOrEmpty(record.NewStatusGuid)) Then
            response.Message = "The Status field cannot be empty. Please select an option."
            response.ResponseType = Common.Enumerations.ResponseTypes.Warning
        ElseIf (record.StartDate Is Nothing) Then ' TODO Change this to take into consideration when is not Suspension but is LOA, Drop or other
            If (_isSupportUser) Then
                response.Message = "Effective Date is a required Field."
            Else
                response.Message = "Start Date is a required Field."
            End If
            response.ResponseType = Common.Enumerations.ResponseTypes.Warning
        ElseIf (String.IsNullOrEmpty(record.CampusId)) Then
            response.Message = "Campus is a required Field."
            response.ResponseType = Common.Enumerations.ResponseTypes.ExceptionHappened
        ElseIf (String.IsNullOrEmpty(record.ModUser)) Then
            response.Message = "Modified User is a required Field."
            response.ResponseType = Common.Enumerations.ResponseTypes.Warning
        ElseIf (record.EndDate Is Nothing) Then ' TODO Change this to take into consideration when is not Suspension but is LOA, Drop or other
            If (_isSupportUser) Then
                response.Message = "Date Determined is a required Field."
            Else
                response.Message = "End Date is a required Field."
            End If
        ElseIf (String.IsNullOrEmpty(record.StuEnrollId)) Then
            response.Message = "Student Enrollment is a required Field."
            response.ResponseType = Common.Enumerations.ResponseTypes.ExceptionHappened
        End If
        Dim startDate, graduationDate As DateTime
        If (DateTime.TryParse(inputModel.StudentEnrollmentStartDate, startDate)) Then
            If (inputModel.EffectiveDate < startDate) Then
                response.Message = "Effective Date can't be before the Student Enrollment Start Date."
                response.ResponseType = Common.Enumerations.ResponseTypes.ExceptionHappened
            End If
        End If
        If (DateTime.TryParse(inputModel.StudentEnrollmentStartDate, graduationDate)) Then
            If (inputModel.EffectiveDate > graduationDate) Then
                response.Message = "Effective Date can't be higher than the Student Enrollment Graduation Date."
                response.ResponseType = Common.Enumerations.ResponseTypes.ExceptionHappened
            End If
        End If

        Return response
    End Function
    Private Function CanChangeToDropped(ByVal record As StudentChangeHistoryObj) As Response
        Dim response As Response
        response.ResponseType = Common.Enumerations.ResponseTypes.Success

        Return response
    End Function

    Private Function CanChangeToExternship(ByVal record As StudentChangeHistoryObj) As Response
        Dim response As Response
        response.ResponseType = Common.Enumerations.ResponseTypes.Success

        Return response
    End Function

    Private Function CanChangeToFutureStart(ByVal record As StudentChangeHistoryObj) As Response
        Dim response As Response
        response.ResponseType = Common.Enumerations.ResponseTypes.Success
        'TODO CanChangeToFutureStart
        'If Not the Same Status Code and Date
        'If Student does not have posted attendance
        'If the student has no posted grades

        Return response
    End Function

    Private Function CanChangeToGraduated(ByVal record As StudentChangeHistoryObj) As Response
        Dim response As Response
        response.ResponseType = Common.Enumerations.ResponseTypes.Success

        'TODO CanChangeToGraduated
        'Must Validate the Student Clock Hours
        'In Batch Status Page Line Number 607

        Return response
    End Function

    Private Function CanChangeToLeaveOfAbsent(ByVal record As StudentChangeHistoryObj) As Response
        Dim response As Response
        response.ResponseType = Common.Enumerations.ResponseTypes.Success

        Return response
    End Function
    Private Function CanChangeToNoStart(ByVal record As StudentChangeHistoryObj) As Response
        Dim response As Response
        response.ResponseType = Common.Enumerations.ResponseTypes.Success
        'TODO CanChangeToNoStart
        'If Student does not have posted attendance
        'If the student has no posted grades

        Return response
    End Function

    Private Function CanChangeToSuspended(ByVal record As StudentChangeHistoryObj, ByVal facade As LOAFacade) As Response
        Dim response As Response
        response.ResponseType = Common.Enumerations.ResponseTypes.Success

        If (record.EndDate < record.StartDate) Then
            If (_isSupportUser) Then
                response.Message = "End Date on the Suspension must be greater than the Effective Date. "
            Else
                response.Message = "End Date on the Suspension must be greater than the Start Date. "
            End If

            response.ResponseType = Common.Enumerations.ResponseTypes.Warning
        Else
            Dim strStudentStartDate As String = facade.GetStudentStartDate(record.StuEnrollId)
            If (Not String.IsNullOrEmpty(strStudentStartDate)) Then
                If (record.StartDate < DateTime.Parse(strStudentStartDate)) Then
                    response.Message = "Cannot place student with a future start date on Suspension."
                    response.ResponseType = Common.Enumerations.ResponseTypes.Warning
                End If
            Else

            End If
        End If

        Return response
    End Function

    Private Function CanChangeToTransferOut(ByVal record As StudentChangeHistoryObj) As Response
        Dim response As Response
        response.ResponseType = Common.Enumerations.ResponseTypes.Success

        Return response
    End Function

    Private Function CanChangeToDisciplinaryProbation(ByVal record As StudentChangeHistoryObj) As Response
        Dim response As Response
        response.ResponseType = Common.Enumerations.ResponseTypes.Success

        Return response
    End Function

    Private Function CanChangeToWarningProbation(ByVal record As StudentChangeHistoryObj) As Response
        Dim response As Response
        response.ResponseType = Common.Enumerations.ResponseTypes.Success

        Return response
    End Function

End Class
