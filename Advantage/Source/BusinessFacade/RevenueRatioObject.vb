Public Class RevenueRatioObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim aidRec As New AidReceivedDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        If rptParamInfo.ResId = 384 Then
            ds = BuildReportSource(aidRec.GetRevenueRatio(rptParamInfo), aidRec.StudentIdentifier)
        ElseIf rptParamInfo.ResId = 632 Then
            ds = BuildReportSourceSummary(aidRec.GetRevenueRatioSummary(rptParamInfo), aidRec.StudentIdentifier)
        ElseIf rptParamInfo.ResId = 636 Then
            ds = BuildReportSourceByStudent(aidRec.GetSummaryReportByStudent9010(rptParamInfo), aidRec.StudentIdentifier)
            ''New Code Added By VIjay Ramteke On July 23, 2010
        ElseIf rptParamInfo.ResId = 637 Then
            ds = BuildReportDetailedForAStudent(aidRec.GetRevenueRatioDetailForAStudent(rptParamInfo), aidRec.StudentIdentifier)
            ''New Code Added By VIjay Ramteke On July 23, 2010
        End If
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim strName As String
        Dim facInputMasks As New InputMasksFacade
        Dim oldCmpGrp, oldCampus, oldFundSrc As String
        Dim received As Decimal
        Dim refunded As Decimal
        Dim stipend As Decimal
        Dim instCharges As Decimal
        Dim dr As DataRow
        Dim filter As String

        'Get mask for SSN
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count = 5 Then
                Dim dtDetail As DataTable = ds.Tables("RevenueRatioDetail")
                Dim dtCmpGrp As DataTable = ds.Tables("CampGrpTotals")
                Dim dtCampus As DataTable = ds.Tables("CampusTotals")
                Dim dtTitleIV As DataTable = ds.Tables("TitleIVTotals")
                Dim dtFundSrc As DataTable = ds.Tables("FSTotals")
                Dim rw As DataRow

                For Each dr In dtDetail.Rows
                    'Set up student name as: "LastName, FirstName MI."
                    strName = ""
                    If Not dr.IsNull("LastName") Then
                        If dr("LastName") <> "" Then
                            strName = dr("LastName")
                        End If
                    End If
                    If Not dr.IsNull("FirstName") Then
                        If dr("FirstName") <> "" Then
                            If strName <> "" Then
                                strName &= ", " & dr("FirstName")
                            Else
                                strName &= dr("FirstName")
                            End If
                        End If
                    End If
                    If Not dr.IsNull("MiddleName") Then
                        If dr("MiddleName") <> "" Then
                            If strName <> "" Then
                                strName &= " " & dr("MiddleName") & "."
                            Else
                                strName &= dr("MiddleName") & "."
                            End If
                        End If
                    End If
                    dr("StudentName") = strName
                    '
                    'Apply mask to SSN.
                    If StudentIdentifier = "SSN" Then
                        If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                            If dr("StudentIdentifier") <> "" Then
                                Dim temp As String = dr("StudentIdentifier")
                                dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                            End If
                        End If
                    End If
                    '
                    '
                    '
                    If Not dr.IsNull("TitleIV") Then
                        If dr("TitleIV") Then
                            dr("FundSourceDescrip") = "*" & dr("FundSourceDescrip")
                        End If
                    End If
                    '
                    If Not dr.IsNull("AwardTypeId") Then
                        If Not (dr("AwardTypeId").ToString = System.Guid.Empty.ToString) Then
                            dr("FundSourceDescrip") = "Fund Source: " & dr("FundSourceDescrip")
                        End If
                    End If
                    '
                    received = 0
                    refunded = 0
                    stipend = 0
                    instCharges = 0
                    '
                    If Not dr.IsNull("Received") Then
                        received = dr("Received")
                    Else
                        dr("Received") = 0
                    End If
                    '
                    If Not dr.IsNull("Refunded") Then
                        refunded = dr("Refunded")
                    Else
                        dr("Refunded") = 0
                    End If
                    dr("NetReceived") = received - refunded
                    '
                    '' Commented by Kamalesh as Stipend calculation formula is not known to be changes later on
                    'If Not (dr.IsNull("PmtPlanCount")) Then
                    '    If dr("PmtPlanCount") = 0 Then
                    '        If Not (dr.IsNull("InstCharges")) Then
                    '            instCharges = dr("InstCharges")
                    '        End If
                    '        If received > instCharges Then stipend = received - instCharges
                    '    End If
                    'Else
                    '    instCharges = dr("InstCharges")
                    '    If received > instCharges Then stipend = received - instCharges
                    'End If
                    dr("Stipends") = stipend
                    '
                    dr("NetReceivedMinusStipends") = received - refunded - stipend
                Next

                For Each dr In dtDetail.Rows
                    '   Update CampGroupTotals table
                    If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpDescrip") Then
                        filter = "CampGrpId='" & dr("CampGrpId").ToString & "'"
                        rw = dtCmpGrp.NewRow
                        rw("CampGrpDescrip") = dr("CampGrpDescrip")
                        rw("Received") = dtDetail.Compute("SUM(Received)", filter)
                        rw("Refunded") = dtDetail.Compute("SUM(Refunded)", filter)
                        rw("NetReceived") = dtDetail.Compute("SUM(NetReceived)", filter)
                        rw("Stipends") = dtDetail.Compute("SUM(Stipends)", filter)
                        rw("NetReceivedMinusStipends") = dtDetail.Compute("SUM(NetReceivedMinusStipends)", filter)
                        dtCmpGrp.Rows.Add(rw)
                        oldCmpGrp = dr("CampGrpDescrip")
                        oldCampus = ""
                        oldFundSrc = ""
                    End If
                    '   Update CampusTotals table
                    If oldCampus = "" Or oldCampus <> dr("CampDescrip") Then
                        filter = "CampGrpId='" & dr("CampGrpId").ToString & "' AND CampusId='" & dr("CampusId").ToString & "'"
                        rw = dtCampus.NewRow
                        rw("CampGrpDescrip") = oldCmpGrp
                        rw("CampDescrip") = dr("CampDescrip")
                        rw("Received") = dtDetail.Compute("SUM(Received)", filter)
                        rw("Refunded") = dtDetail.Compute("SUM(Refunded)", filter)
                        rw("NetReceived") = dtDetail.Compute("SUM(NetReceived)", filter)
                        rw("Stipends") = dtDetail.Compute("SUM(Stipends)", filter)
                        rw("NetReceivedMinusStipends") = dtDetail.Compute("SUM(NetReceivedMinusStipends)", filter)
                        dtCampus.Rows.Add(rw)
                        oldCampus = dr("CampDescrip")
                        oldFundSrc = ""
                    End If
                    '   Update FundSourceTotals table
                    If oldFundSrc = "" Or oldFundSrc <> dr("FundSourceDescrip") Then
                        filter = "CampGrpId='" & dr("CampGrpId").ToString & "' AND CampusId='" & dr("CampusId").ToString & "' AND AwardTypeId='" & dr("AwardTypeId").ToString & "'"
                        rw = dtFundSrc.NewRow
                        rw("CampGrpDescrip") = oldCmpGrp
                        rw("CampDescrip") = dr("CampDescrip")
                        rw("FundSourceDescrip") = dr("FundSourceDescrip")
                        rw("TitleIV") = dr("TitleIV")
                        rw("Received") = dtDetail.Compute("SUM(Received)", filter)
                        rw("Refunded") = dtDetail.Compute("SUM(Refunded)", filter)
                        rw("NetReceived") = dtDetail.Compute("SUM(NetReceived)", filter)
                        rw("Stipends") = dtDetail.Compute("SUM(Stipends)", filter)
                        rw("NetReceivedMinusStipends") = dtDetail.Compute("SUM(NetReceivedMinusStipends)", filter)
                        dtFundSrc.Rows.Add(rw)
                        oldFundSrc = dr("FundSourceDescrip")
                    End If
                Next

                '   Update TitleIVTotals table
                filter = "TitleIV=True"
                rw = dtTitleIV.NewRow
                rw("Received") = dtDetail.Compute("SUM(Received)", filter)
                rw("Refunded") = dtDetail.Compute("SUM(Refunded)", filter)
                rw("NetReceived") = dtDetail.Compute("SUM(NetReceived)", filter)
                rw("Stipends") = dtDetail.Compute("SUM(Stipends)", filter)
                rw("NetReceivedMinusStipends") = dtDetail.Compute("SUM(NetReceivedMinusStipends)", filter)
                received = dtCmpGrp.Compute("SUM(NetReceivedMinusStipends)", "")
                If received <> 0 And Not (rw.IsNull("NetReceivedMinusStipends")) Then
                    rw("RevenuePercentage") = System.Math.Round(rw("NetReceivedMinusStipends") / received * 100, 0)
                Else
                    rw("RevenuePercentage") = 0
                End If
                rw("Ratio") = 0
                dtTitleIV.Rows.Add(rw)

            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function
    Private Function BuildReportSourceSummary(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim strName As String
        Dim facInputMasks As New InputMasksFacade
        Dim oldCmpGrp, oldCampus, oldFundSrc As String
        Dim received As Decimal
        Dim refunded As Decimal
        Dim stipend As Decimal
        Dim instCharges As Decimal
        Dim dr As DataRow
        Dim filter As String

        'Get mask for SSN
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count = 5 Then
                Dim dtDetail As DataTable = ds.Tables("RevenueRatioDetail")
                Dim dtCmpGrp As DataTable = ds.Tables("CampGrpTotals")
                Dim dtCampus As DataTable = ds.Tables("CampusTotals")
                Dim dtTitleIV As DataTable = ds.Tables("TitleIVTotals")
                Dim dtFundSrc As DataTable = ds.Tables("FSTotals")
                Dim rw As DataRow

                For Each dr In dtDetail.Rows
                    'Set up student name as: "LastName, FirstName MI."
                    strName = ""
                    If Not dr.IsNull("LastName") Then
                        If dr("LastName") <> "" Then
                            strName = dr("LastName")
                        End If
                    End If
                    If Not dr.IsNull("FirstName") Then
                        If dr("FirstName") <> "" Then
                            If strName <> "" Then
                                strName &= ", " & dr("FirstName")
                            Else
                                strName &= dr("FirstName")
                            End If
                        End If
                    End If
                    If Not dr.IsNull("MiddleName") Then
                        If dr("MiddleName") <> "" Then
                            If strName <> "" Then
                                strName &= " " & dr("MiddleName") & "."
                            Else
                                strName &= dr("MiddleName") & "."
                            End If
                        End If
                    End If
                    dr("StudentName") = strName
                    '
                    'Apply mask to SSN.
                    If StudentIdentifier = "SSN" Then
                        If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                            If dr("StudentIdentifier") <> "" Then
                                Dim temp As String = dr("StudentIdentifier")
                                dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                            End If
                        End If
                    End If
                    '
                    '
                    '
                    If Not dr.IsNull("TitleIV") Then
                        If dr("TitleIV") Then
                            dr("FundSourceDescrip") = "*" & dr("FundSourceDescrip")
                        End If
                    End If
                    '
                    'If Not dr.IsNull("AwardTypeId") Then
                    '    If Not (dr("AwardTypeId").ToString = System.Guid.Empty.ToString) Then
                    '        dr("FundSourceDescrip") = "Fund Source: " & dr("FundSourceDescrip")
                    '    End If
                    'End If
                    '
                    received = 0
                    refunded = 0
                    stipend = 0
                    instCharges = 0
                    '
                    If Not dr.IsNull("Received") Then
                        received = dr("Received")
                    Else
                        dr("Received") = 0
                    End If
                    '
                    If Not dr.IsNull("Refunded") Then
                        refunded = dr("Refunded")
                    Else
                        dr("Refunded") = 0
                    End If
                    dr("NetReceived") = received - refunded
                    '
                    '' Commented by Kamalesh as Stipend calculation formula is not known to be changes later on

                    'If Not (dr.IsNull("PmtPlanCount")) Then
                    '    If dr("PmtPlanCount") = 0 Then
                    '        If Not (dr.IsNull("InstCharges")) Then
                    '            instCharges = dr("InstCharges")
                    '        End If
                    '        If received > instCharges Then stipend = received - instCharges
                    '    End If
                    'Else
                    '    instCharges = dr("InstCharges")
                    '    If received > instCharges Then stipend = received - instCharges
                    'End If

                    '''''''''''''''''''''''''''''''''
                    dr("Stipends") = stipend
                    '
                    dr("NetReceivedMinusStipends") = received - refunded - stipend
                Next

                For Each dr In dtDetail.Rows
                    '   Update CampGroupTotals table
                    If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpDescrip") Then
                        filter = "CampGrpId='" & dr("CampGrpId").ToString & "'"
                        rw = dtCmpGrp.NewRow
                        rw("CampGrpDescrip") = dr("CampGrpDescrip")
                        rw("Received") = dtDetail.Compute("SUM(Received)", filter)
                        rw("Refunded") = dtDetail.Compute("SUM(Refunded)", filter)
                        rw("NetReceived") = dtDetail.Compute("SUM(NetReceived)", filter)
                        rw("Stipends") = dtDetail.Compute("SUM(Stipends)", filter)
                        rw("NetReceivedMinusStipends") = dtDetail.Compute("SUM(NetReceivedMinusStipends)", filter)
                        dtCmpGrp.Rows.Add(rw)
                        oldCmpGrp = dr("CampGrpDescrip")
                        oldCampus = ""
                        oldFundSrc = ""
                    End If
                    '   Update CampusTotals table
                    If oldCampus = "" Or oldCampus <> dr("CampDescrip") Then
                        filter = "CampGrpId='" & dr("CampGrpId").ToString & "' AND CampusId='" & dr("CampusId").ToString & "'"
                        rw = dtCampus.NewRow
                        rw("CampGrpDescrip") = oldCmpGrp
                        rw("CampDescrip") = dr("CampDescrip")
                        rw("Received") = dtDetail.Compute("SUM(Received)", filter)
                        rw("Refunded") = dtDetail.Compute("SUM(Refunded)", filter)
                        rw("NetReceived") = dtDetail.Compute("SUM(NetReceived)", filter)
                        rw("Stipends") = dtDetail.Compute("SUM(Stipends)", filter)
                        rw("NetReceivedMinusStipends") = dtDetail.Compute("SUM(NetReceivedMinusStipends)", filter)
                        dtCampus.Rows.Add(rw)
                        oldCampus = dr("CampDescrip")
                        oldFundSrc = ""
                    End If
                    '   Update FundSourceTotals table
                    If oldFundSrc = "" Or oldFundSrc <> dr("FundSourceDescrip") Then
                        filter = "CampGrpId='" & dr("CampGrpId").ToString & "' AND CampusId='" & dr("CampusId").ToString & "' AND AwardTypeId='" & dr("AwardTypeId").ToString & "'"
                        rw = dtFundSrc.NewRow
                        rw("CampGrpDescrip") = oldCmpGrp
                        rw("CampDescrip") = dr("CampDescrip")
                        rw("FundSourceDescrip") = dr("FundSourceDescrip")
                        rw("TitleIV") = dr("TitleIV")
                        rw("Received") = dtDetail.Compute("SUM(Received)", filter)
                        rw("Refunded") = dtDetail.Compute("SUM(Refunded)", filter)
                        rw("NetReceived") = dtDetail.Compute("SUM(NetReceived)", filter)
                        rw("Stipends") = dtDetail.Compute("SUM(Stipends)", filter)
                        rw("NetReceivedMinusStipends") = dtDetail.Compute("SUM(NetReceivedMinusStipends)", filter)
                        dtFundSrc.Rows.Add(rw)
                        oldFundSrc = dr("FundSourceDescrip")
                    End If
                Next

                '   Update TitleIVTotals table
                filter = "TitleIV=True"
                rw = dtTitleIV.NewRow
                rw("Received") = dtDetail.Compute("SUM(Received)", filter)
                rw("Refunded") = dtDetail.Compute("SUM(Refunded)", filter)
                rw("NetReceived") = dtDetail.Compute("SUM(NetReceived)", filter)
                rw("Stipends") = dtDetail.Compute("SUM(Stipends)", filter)
                rw("NetReceivedMinusStipends") = dtDetail.Compute("SUM(NetReceivedMinusStipends)", filter)
                received = dtCmpGrp.Compute("SUM(NetReceivedMinusStipends)", "")
                If received <> 0 And Not (rw.IsNull("NetReceivedMinusStipends")) Then
                    rw("RevenuePercentage") = System.Math.Round(rw("NetReceivedMinusStipends") / received * 100, 0)
                Else
                    rw("RevenuePercentage") = 0
                End If
                rw("Ratio") = 0
                dtTitleIV.Rows.Add(rw)

            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function
    ''New Code Added By Vijay Ramteke On July 23, 2010
    Private Function BuildReportDetailedForAStudent(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim strName As String
        Dim facInputMasks As New InputMasksFacade
        Dim oldCmpGrp, oldCampus, oldFundSrc As String
        Dim received As Decimal
        Dim refunded As Decimal
        Dim stipend As Decimal
        Dim instCharges As Decimal
        Dim dr As DataRow
        Dim filter As String

        'Get mask for SSN
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            ''New Code Added By Vijay Ramteke On September 05, 2010
            ''If ds.Tables.Count = 1 Then
            If ds.Tables.Count = 2 Then
                ''New Code Added By Vijay Ramteke On September 05, 2010
                Dim dtDetail As DataTable = ds.Tables("RevenueRatioDetailForStudent")
                For Each dr In dtDetail.Rows
                    'Set up student name as: "LastName, FirstName MI."
                    strName = ""
                    If Not dr.IsNull("LastName") Then
                        If dr("LastName") <> "" Then
                            strName = dr("LastName")
                        End If
                    End If
                    If Not dr.IsNull("FirstName") Then
                        If dr("FirstName") <> "" Then
                            If strName <> "" Then
                                strName &= ", " & dr("FirstName")
                            Else
                                strName &= dr("FirstName")
                            End If
                        End If
                    End If
                    If Not dr.IsNull("MiddleName") Then
                        If dr("MiddleName") <> "" Then
                            If strName <> "" Then
                                strName &= " " & dr("MiddleName") & "."
                            Else
                                strName &= dr("MiddleName") & "."
                            End If
                        End If
                    End If
                    dr("StudentName") = strName
                    '
                    'Apply mask to SSN.
                    If StudentIdentifier = "SSN" Then
                        If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                            If dr("StudentIdentifier") <> "" Then
                                Dim temp As String = dr("StudentIdentifier")
                                dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                            End If
                        End If
                    End If
                    '
                Next
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function
    ''New Code Added By Vijay Ramteke On July 23, 2010

    Private Function BuildReportSourceByStudent(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim strName As String
        Dim facInputMasks As New InputMasksFacade
        Dim oldCmpGrp, oldCampus, oldFundSrc As String
        Dim received As Decimal
        Dim refunded As Decimal
        Dim stipend As Decimal
        Dim instCharges As Decimal
        Dim dr As DataRow
        Dim filter As String

        'Get mask for SSN
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count = 3 Then
                Dim dtDetail As DataTable = ds.Tables("RevenueRatioDetail")
                Dim dtCmpGrp As DataTable = ds.Tables("CampGrpTotals")
                Dim dtCampus As DataTable = ds.Tables("CampusTotals")

                Dim rw As DataRow

                For Each dr In dtDetail.Rows
                    'Set up student name as: "LastName, FirstName MI."
                    strName = ""
                    If Not dr.IsNull("LastName") Then
                        If dr("LastName") <> "" Then
                            strName = dr("LastName")
                        End If
                    End If
                    If Not dr.IsNull("FirstName") Then
                        If dr("FirstName") <> "" Then
                            If strName <> "" Then
                                strName &= ", " & dr("FirstName")
                            Else
                                strName &= dr("FirstName")
                            End If
                        End If
                    End If
                    If Not dr.IsNull("MiddleName") Then
                        If dr("MiddleName") <> "" Then
                            If strName <> "" Then
                                strName &= " " & dr("MiddleName") & "."
                            Else
                                strName &= dr("MiddleName") & "."
                            End If
                        End If
                    End If
                    dr("StudentName") = strName
                    '
                    'Apply mask to SSN.
                    If StudentIdentifier = "SSN" Then
                        If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                            If dr("StudentIdentifier") <> "" Then
                                Dim temp As String = dr("StudentIdentifier")
                                dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                            End If
                        End If
                    End If



                Next

                For Each dr In dtDetail.Rows
                    '   Update CampGroupTotals table
                    If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpDescrip") Then
                        filter = "CampGrpId='" & dr("CampGrpId").ToString & "'"
                        rw = dtCmpGrp.NewRow
                        rw("CampGrpDescrip") = dr("CampGrpDescrip")
                        rw("Numerator") = dtDetail.Compute("SUM(Numerator)", filter)
                        rw("Denominator") = dtDetail.Compute("SUM(Denominator)", filter)


                        dtCmpGrp.Rows.Add(rw)
                        oldCmpGrp = dr("CampGrpDescrip")
                        oldCampus = ""
                        oldFundSrc = ""
                    End If
                    '   Update CampusTotals table
                    If oldCampus = "" Or oldCampus <> dr("CampDescrip") Then
                        filter = "CampGrpId='" & dr("CampGrpId").ToString & "' AND CampusId='" & dr("CampusId").ToString & "'"
                        rw = dtCampus.NewRow
                        rw("CampGrpDescrip") = oldCmpGrp
                        rw("CampDescrip") = dr("CampDescrip")
                        rw("Numerator") = dtDetail.Compute("SUM(Numerator)", filter)
                        rw("Denominator") = dtDetail.Compute("SUM(Denominator)", filter)


                        dtCampus.Rows.Add(rw)
                        oldCampus = dr("CampDescrip")
                        oldFundSrc = ""
                    End If

                Next


            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function
#End Region

End Class
