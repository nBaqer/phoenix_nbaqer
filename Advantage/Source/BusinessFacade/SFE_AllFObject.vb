
Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllFObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SFE_AllF"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet
	Private RptParamInfo As ReportParamInfoIPEDS

	Public Overrides Function GetReportDataSet(ByVal RptParaminfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SFE_AllFObjectDB.GetReportDatasetRaw(RptParaminfo, MainTableName)

		' save passed-in report parameter info 
		Me.RptParamInfo = RptParaminfo

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		Dim drRpt As DataRow
		Dim CurTableName As String
		Dim i As Integer = 0
		Dim ProgIds As String()
		Dim ProgDescrips As String()


		' HACK - temp disable of Report Part F, Part 1, pending changes in Advantage 
		'	to distinguish betweeen Clock Hour vs. Credit Hour courses

		'' process data for report Part 1
		'CurTableName = MainTableName & "Part1"

		'' create table to hold final report data 
		'With dsRpt.Tables.Add(CurTableName).Columns
		'	.Add(RptStuIdentifierColName, GetType(System.String))
		'	.Add("Name", GetType(System.String))
		'	.Add("ProgCode", GetType(System.String))
		'	.Add("ProgName", GetType(System.String))
		'	.Add("Length", GetType(System.Single))
		'	.Add("StartDate", GetType(System.String))
		'	.Add("HoursPerWeek", GetType(System.Int32))
		'	.Add("WeeksInProg", GetType(System.Int32))
		'	.Add("WeekInstRepPeriod", GetType(System.Int32))
		'	.Add("ContactHours", GetType(System.Int32))
		'End With

		'' process raw data, if any
		'ProgIds = RptParaminfo.FilterProgramIDsMult1.Split(",")
		'ProgDescrips = IPEDSFacade.GetProgramDescripList(RptParamInfo.FilterProgramDescripsMult1)

		'If dsRaw.Tables.Contains(CurTableName) Then
		'	For i = 0 To UBound(ProgIds)
		'		ProcessRowsPart1(ProgIds(i), ProgDescrips(i), _
		'						 dsRaw.Tables(CurTableName), _
		'						 dsRpt.Tables(CurTableName))
		'	Next
		'Else
		'	For i = 0 To UBound(ProgIds)
		'		AddBlankRowPart1(ProgIds(i), ProgDescrips(i), _
		'						 dsRpt.Tables(CurTableName))
		'	Next
		'End If

		' process data for report Part 2
		CurTableName = MainTableName & "Part2"

		' create table to hold final report data 
		With dsRpt.Tables.Add(CurTableName).Columns
			.Add(RptStuIdentifierColName, GetType(System.String))
			.Add("Name", GetType(System.String))
			.Add("ProgCode", GetType(System.String))
			.Add("ProgName", GetType(System.String))
			.Add("StartDate", GetType(System.String))
			.Add("CreditHours", GetType(System.Single))
		End With

		' process raw data, if any
		ProgIds = RptParaminfo.FilterProgramIDsMult2.Split(",")
		ProgDescrips = IPEDSFacade.GetProgramDescripList(RptParamInfo.FilterProgramDescripsMult2)
		If dsRaw.Tables.Contains(CurTableName) Then
			For i = 0 To UBound(ProgIds)
				ProcessRowsPart2(ProgIds(i), ProgDescrips(i), RptParamInfo, _
								 dsRaw.Tables(CurTableName), _
								 dsRpt.Tables(CurTableName))
			Next
		Else
			For i = 0 To UBound(ProgIds)
				AddBlankRowPart2(ProgIds(i), ProgDescrips(i), _
								 dsRpt.Tables(CurTableName))
			Next
		End If



		' process data for report Part 3
		CurTableName = MainTableName & "Part3"

		' create table to hold final report data 
		With dsRpt.Tables.Add(CurTableName).Columns
			.Add(RptStuIdentifierColName, GetType(System.String))
			.Add("Name", GetType(System.String))
			.Add("ProgCode", GetType(System.String))
			.Add("ProgName", GetType(System.String))
			.Add("StartDate", GetType(System.String))
			.Add("CreditHours", GetType(System.Single))
		End With

		' process raw data, if any
		ProgIds = RptParamInfo.FilterProgramIDsMult3.Split(",")
		ProgDescrips = IPEDSFacade.GetProgramDescripList(RptParamInfo.FilterProgramDescripsMult3)
		If dsRaw.Tables.Contains(CurTableName) Then
			For i = 0 To UBound(ProgIds)
				ProcessRowsPart3(ProgIds(i), ProgDescrips(i), RptParamInfo, _
								 dsRaw.Tables(CurTableName), _
								 dsRpt.Tables(CurTableName))
			Next
		Else
			For i = 0 To UBound(ProgIds)
				AddBlankRowPart3(ProgIds(i), ProgDescrips(i), _
								 dsRpt.Tables(CurTableName))
			Next
		End If



		' process data for main report summary
		CurTableName = MainTableName & "Summary"

		' create table to hold final report data 
		With dsRpt.Tables.Add(CurTableName).Columns
			' HACK - temp disable of Report Part F, Part 1, pending changes in Advantage 
			'	to distinguish betweeen Clock Hour vs. Credit Hour courses
			'.Add("OccUnderGrad", GetType(System.Single))
			.Add("UnderGrad", GetType(System.Single))
			.Add("Grad", GetType(System.Single))
		End With

		' process data
		With dsRpt
			drRpt = .Tables(CurTableName).NewRow
			' HACK - temp disable of Report Part F, Part 1, pending changes in Advantage 
			'	to distinguish betweeen Clock Hour vs. Credit Hour courses
			'drRpt("OccUnderGrad") = .Tables(MainTableName & "Part1").Compute("Sum(ContactHours)", "")
			drRpt("UnderGrad") = .Tables(MainTableName & "Part2").Compute("Sum(CreditHours)", "")
			drRpt("Grad") = .Tables(MainTableName & "Part3").Compute("Sum(CreditHours)", "")
			.Tables(CurTableName).Rows.Add(drRpt)
		End With

		Return dsRpt

	End Function

	Private Sub ProcessRowsPart1(ByVal ProgId As String, ByVal ProgDescrip As String, ByVal dtRaw As DataTable, ByRef dtRpt As DataTable)
		Dim ProgCode As String = SFE_AllFObjectDB.GetProgramCodeById(ProgId)
		Dim dvRaw As New DataView(dtRaw, "ProgCode = '" & ProgCode & "'", "", DataViewRowState.CurrentRows)

		If dvRaw.Count = 0 Then
			AddBlankRowPart1(ProgId, ProgDescrip, dtRpt)
			Exit Sub
		End If

		Dim i As Integer
		Dim drRpt As DataRow

		For i = 0 To dvRaw.Count - 1
			drRpt = dtRpt.NewRow

			' Student information
			IPEDSFacade.SetStudentInfo(dvRaw(i), drRpt)

			' Program and Enrollment information
			drRpt("ProgCode") = ProgCode
			drRpt("ProgName") = dvRaw(i)("ProgDescrip").ToString
			drRpt("Length") = CSng(dvRaw(i)("Hours"))
			drRpt("StartDate") = Format(CDate(dvRaw(i)("EnrollmentStartDate")), "MM/dd/yyyy")

			' "Dummy" data, to be set in future
			drRpt("HoursPerWeek") = 0
			drRpt("WeeksInProg") = 0
			drRpt("WeekInstRepPeriod") = 0
			drRpt("ContactHours") = 0

			' add new report row to report table
			dtRpt.Rows.Add(drRpt)
		Next
	End Sub

	Private Sub AddBlankRowPart1(ByVal ProgID As String, ByVal ProgDescrip As String, ByRef dtRpt As DataTable)
		Dim drRpt As DataRow = dtRpt.NewRow

		drRpt(RptStuIdentifierColName) = DBNull.Value
		drRpt("Name") = DBNull.Value
		drRpt("ProgCode") = SFE_AllFObjectDB.GetProgramCodeById(ProgID)
		drRpt("ProgName") = ProgDescrip
		drRpt("Length") = DBNull.Value
		drRpt("StartDate") = DBNull.Value
		drRpt("HoursPerWeek") = DBNull.Value
		drRpt("WeeksInProg") = DBNull.Value
		drRpt("WeekInstRepPeriod") = DBNull.Value
		drRpt("ContactHours") = 0

		dtRpt.Rows.Add(drRpt)
	End Sub

	Private Sub ProcessRowsPart2(ByVal ProgId As String, ByVal ProgDescrip As String, ByVal RptParamInfo As ReportParamInfoIPEDS, ByVal dtRaw As DataTable, ByRef dtRpt As DataTable)
		Dim ProgCode As String = SFE_AllFObjectDB.GetProgramCodeById(ProgId)
		Dim dvRaw As New DataView(dtRaw, "ProgCode = '" & ProgCode & "'", "", DataViewRowState.CurrentRows)

		If dvRaw.Count = 0 Then
			AddBlankRowPart2(ProgId, ProgDescrip, dtRpt)
			Exit Sub
		End If

		Dim i As Integer = 0
		Dim drRpt As DataRow
		Dim CurStudentId As System.Guid

		While i < dvRaw.Count
			drRpt = dtRpt.NewRow

			' Student information
			IPEDSFacade.SetStudentInfo(dvRaw(i), drRpt)

			' Program and Enrollment information
			drRpt("ProgCode") = ProgCode
			drRpt("ProgName") = dvRaw(i)("ProgDescrip").ToString
			drRpt("StartDate") = Format(CDate(dvRaw(i)("EnrollmentStartDate")), "MM/dd/yyyy")

			' add the summed credit hours for the current program and student 
			'	to the report row
			drRpt("CreditHours") = SumCreditHours(dtRaw, ProgCode, dvRaw(i)("StudentId").ToString, RptParamInfo)

			' add new report row to report table
			dtRpt.Rows.Add(drRpt)

			' skip past all records for the StudentId just processed, to the the
			'	next StudentId to process
			CurStudentId = dvRaw(i)("StudentId")
			While i < dvRaw.Count AndAlso dvRaw(i)("StudentId").Equals(CurStudentId)
				i = i + 1
			End While
		End While
	End Sub

	Private Sub AddBlankRowPart2(ByVal ProgId As String, ByVal ProgDescrip As String, ByRef dtRpt As DataTable)
		Dim drRpt As DataRow = dtRpt.NewRow

		drRpt(RptStuIdentifierColName) = DBNull.Value
		drRpt("Name") = DBNull.Value
		drRpt("ProgCode") = SFE_AllFObjectDB.GetProgramCodeById(ProgId)
		drRpt("ProgName") = ProgDescrip
		drRpt("StartDate") = DBNull.Value
		drRpt("CreditHours") = 0

		dtRpt.Rows.Add(drRpt)
	End Sub

	Private Sub ProcessRowsPart3(ByVal ProgId As String, ByVal ProgDescrip As String, ByVal RptParamInfo As ReportParamInfoIPEDS, ByVal dtRaw As DataTable, ByRef dtRpt As DataTable)
		Dim ProgCode As String = SFE_AllFObjectDB.GetProgramCodeById(ProgId)
		Dim dvRaw As New DataView(dtRaw, "ProgCode = '" & ProgCode & "'", "", DataViewRowState.CurrentRows)

		If dvRaw.Count = 0 Then
			AddBlankRowPart3(ProgId, ProgDescrip, dtRpt)
			Exit Sub
		End If

		Dim i As Integer = 0
		Dim drRpt As DataRow
		Dim CurStudentId As System.Guid

		While i < dvRaw.Count
			drRpt = dtRpt.NewRow

			' Student information
			IPEDSFacade.SetStudentInfo(dvRaw(i), drRpt)

			' Program and Enrollment information
			drRpt("ProgCode") = ProgCode
			drRpt("ProgName") = dvRaw(i)("ProgDescrip").ToString
			drRpt("StartDate") = Format(CDate(dvRaw(i)("EnrollmentStartDate")), "MM/dd/yyyy")

			' add the summed credit hours for the current program and student 
			'	to the report row
			drRpt("CreditHours") = SumCreditHours(dtRaw, ProgCode, dvRaw(i)("StudentId").ToString, RptParamInfo)

			' add new report row to report table
			dtRpt.Rows.Add(drRpt)

			' skip past all records for the StudentId just processed, to the the
			'	next StudentId to process
			CurStudentId = dvRaw(i)("StudentId")
			While i < dvRaw.Count AndAlso dvRaw(i)("StudentId").Equals(CurStudentId)
				i = i + 1
			End While
		End While
	End Sub

	Private Sub AddBlankRowPart3(ByVal ProgId As String, ByVal ProgDescrip As String, ByRef dtRpt As DataTable)
		Dim drRpt As DataRow = dtRpt.NewRow

		drRpt(RptStuIdentifierColName) = DBNull.Value
		drRpt("Name") = DBNull.Value
		drRpt("ProgCode") = SFE_AllFObjectDB.GetProgramCodeById(ProgId)
		drRpt("ProgName") = ProgDescrip
		drRpt("StartDate") = DBNull.Value
		drRpt("CreditHours") = 0

		dtRpt.Rows.Add(drRpt)
	End Sub

	Private Function SumCreditHours(ByVal dtRaw As DataTable, ByVal ProgCode As String, ByVal StudentId As String, ByVal RptParamInfo As ReportParamInfoIPEDS) As Single
		Dim dvRawSum As New DataView(dtRaw)
		Dim i As Integer
		Dim CreditHoursSum As Single
		Dim ClsSecStart As DateTime, ClsSecEnd As DateTime
		Dim ClsSecStartInRange As Boolean, ClsSecEndInRange As Boolean
		Dim NumDaysClsSection As Single, NumDaysInRange As Single
		Dim CredMultiplier As Single

		dvRawSum.RowFilter = "ProgCode = '" & ProgCode & "' AND StudentId = '" & StudentId & "'"
		CreditHoursSum = 0

		For i = 0 To dvRawSum.Count - 1
			ClsSecStart = CDate(dvRawSum(i)("ClassSectionStartDate"))
			ClsSecEnd = CDate(dvRawSum(i)("ClassSectionEndDate"))
			ClsSecStartInRange = Date.Compare(ClsSecStart, RptParamInfo.CohortStartDate) >= 0
			ClsSecEndInRange = Date.Compare(ClsSecEnd, RptParamInfo.CohortEndDate) <= 0
			If ClsSecStartInRange And ClsSecEndInRange Then
				' if the start and end dates for the class section are both within the report date range, 
				'	add 100% of class section's credits to total
				CredMultiplier = 1
			Else
				' otherwise compute pro-rated percentage of class section's credits and add that to total
				NumDaysClsSection = ClsSecEnd.Subtract(ClsSecStart).Days
				If ClsSecStartInRange Then
					NumDaysInRange = RptParamInfo.CohortEndDate.Subtract(ClsSecStart).Days
				Else
					NumDaysInRange = ClsSecEnd.Subtract(RptParamInfo.CohortStartDate).Days
				End If
				CredMultiplier = NumDaysInRange / NumDaysClsSection
			End If
			' mutliply class section's credits by pro-rated multiplier, if any, and add to total
			CreditHoursSum += dvRawSum(i)("Credits") * CredMultiplier
		Next

		Return CreditHoursSum
	End Function

End Class