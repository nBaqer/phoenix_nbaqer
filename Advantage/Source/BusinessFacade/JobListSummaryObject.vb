Public Class JobListSummaryObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim jobList As New JobListingDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(jobList.GetJobListSummary(rptParamInfo))
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    ' Copy data from dataset to datatable.
    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        Dim contactName As String = ""

        Try
            ds.Tables(0).TableName = "JobListSummary"
            ds.Tables(0).Columns.Add(New DataColumn("ContactName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("AcceptanceDateRange", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("JobCount", System.Type.GetType("System.Int32")))

            For Each dr As DataRow In ds.Tables(0).Rows
                dr("JobCount") = ds.Tables(0).Rows.Count
                '
                If Not (dr("LastName") Is System.DBNull.Value) Then
                    contactName = dr("LastName")
                End If
                If Not (dr("FirstName") Is System.DBNull.Value) Then
                    If dr("FirstName") <> "" Then
                        If contactName <> "" Then
                            contactName &= ", " & dr("FirstName")
                        Else
                            contactName = dr("FirstName")
                        End If
                    End If
                End If
                If Not (dr("MiddleName") Is System.DBNull.Value) Then
                    If dr("MiddleName") <> "" Then
                        If contactName <> "" Then
                            contactName &= " " & dr("MiddleName") & "."
                        Else
                            contactName = dr("MiddleName") & "."
                        End If
                    End If
                End If
                dr("ContactName") = contactName
                contactName = ""
                '
                If Not (dr("OpenedFrom") Is System.DBNull.Value) And Not (dr("OpenedTo") Is System.DBNull.Value) Then
                    dr("AcceptanceDateRange") = Convert.ToDateTime(dr("OpenedFrom")).ToShortDateString & " - " & Convert.ToDateTime(dr("OpenedTo")).ToShortDateString
                End If
            Next

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region


End Class
