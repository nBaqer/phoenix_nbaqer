' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' LocationFacade.vb
'
' LocationFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class LocationFacade
    Public Function GetAllLocation() As DataSet

        '   Instantiate DAL component
        With New LocationDB

            '   get the dataset with all States
            Return .GetAllLocation()

        End With

    End Function
End Class
