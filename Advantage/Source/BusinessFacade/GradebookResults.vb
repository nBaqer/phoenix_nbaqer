
Imports FAME.Advantage.Common

Public Class GradebookResults
    Inherits BaseReportFacade

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim GradebookResultsObj As New GradebookResultsDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by report.
        'ds = BuildReportSource(GradebookResultsObj.GradebookResults(rptParamInfo), GradebookResultsObj.StudentIdentifier, rptParamInfo.ResId, rptParamInfo.FilterOtherString.Trim)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        ds = BuildDataForReport(rptParamInfo)
        Return ds
    End Function
    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String, ByVal resid As Integer, ByVal Otherwhere As String) As DataSet
        ''  Dim stuName As String

        Dim GradebookResultsObj As New GradebookResultsDB
        Try
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try
        Return ds
    End Function
    Public Function BuildDataForReport(ByVal ParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim dtReturnResult As New DataTable
        Dim objDB As New TransferGradeDB
        Dim score As Decimal = 0.0
        If ParamInfo.FilterOtherString <> "" Then
            score = CInt(ParamInfo.FilterOtherString)
        End If
        dtReturnResult = (New GradebookResultsDB).GetGradeBookResultsData(ParamInfo)
        Dim ds1 As New DataSet
        Dim dt As DataTable = ds1.Tables.Add("GradeBookResultsTable")
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        dt.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SSN", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("Class", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("InstructorName", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("Variance", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("Score", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("CampusDescrip", System.Type.GetType("System.String")))
        Dim row As DataRow
        For Each dr As DataRow In dtReturnResult.Rows
            row = dt.NewRow()
            row("Studentname") = dr("lastname").ToString + "," + dr("FirstName").ToString
            If MyAdvAppSettings.AppSettings("StudentIdentifier").ToString.ToLower = "ssn" Then
                If Not dr("SSN") Is System.DBNull.Value Then row("SSN") = "***-**-" & Mid(dr("SSN").ToString, 6, 4) Else row("SSN") = ""
            ElseIf MyAdvAppSettings.AppSettings("StudentIdentifier").ToString.ToLower = "studentid" Then
                row("SSN") = dr("StudentNumber").ToString
            Else
                row("SSN") = dr("EnrollmentId").ToString
            End If
            row("Class") = "(" + dr("Code").ToString.Trim + ") - " + dr("Descrip").ToString.Trim + " - " + dr("ClsSection").ToString.Trim
            row("InstructorName") = dr("InstructorName").ToString
            row("CampGrpDescrip") = dr("CampGrpDescrip").ToString
            row("CampusDescrip") = dr("CampusDescrip").ToString
            Dim decWeight As Decimal = 0.0
            Dim decFinalScore As Decimal = 0.0
            Dim decComputedScore As Decimal = 0.0
            If dr("GradeBkWeightLevel").ToString = "InstructorLevel" Then
                decWeight = dr("TotalWeight")
                decFinalScore = dr("FinalScore")
                If decWeight <> 0 Then
                    decComputedScore = Math.Round((decFinalScore / decWeight) * 100, 2)
                Else
                    decComputedScore = 0.0
                End If
            Else
                'Course Level
                'Compute score based on the Grading Policies
                Dim dtGBWResults As New DataTable
                dtGBWResults = objDB.GetGBWResultsForCourseLevel(dr("StuEnrollId").ToString, dr("ClsSectionId").ToString, True)
                Dim oldGBWDetailId As String = ""
                Dim weight As Decimal
                Dim grdPolicyId As Integer
                Dim param As Integer
                Dim gbwUtil As GrdBkWgtUtility
                Dim arrRows() As DataRow
                Dim accumWgt As Decimal = 0.0
                Dim ttlWgt As Decimal = 0.0
                'Modified by balaji on 04/30/2007 to fix Mantis 11012
                'the total weight and accumWgt needs to be reset for every student otherwise 
                'its keeps adding those weights for all students registered in that
                'class and the final score is miscalculated.
                ttlWgt = 0
                accumWgt = 0
                For Each row1 As DataRow In dtGBWResults.Rows
                    If oldGBWDetailId = "" Or oldGBWDetailId <> row1("InstrGrdBkWgtDetailId").ToString Then
                        oldGBWDetailId = row1("InstrGrdBkWgtDetailId").ToString
                        If row1.IsNull("Weight") Then
                            weight = 0
                        Else
                            weight = row1("Weight")
                        End If

                        If row1.IsNull("GrdPolicyId") Then
                            grdPolicyId = 0
                        Else
                            grdPolicyId = row1("GrdPolicyId")
                        End If
                        If row1.IsNull("Parameter") Then
                            param = 0
                        Else
                            param = row1("Parameter")
                        End If

                        'Get results for Grading Component Type
                        arrRows = dtGBWResults.Select("InstrGrdBkWgtDetailId='" & oldGBWDetailId & "'")
                        '
                        If arrRows.GetLength(0) > 0 Then
                            ttlWgt += weight
                            'Compute score using Strategy pattern
                            If Not (grdPolicyId = 0) Then
                                gbwUtil = New GrdBkWgtUtility(arrRows, weight, grdPolicyId, param)
                            Else
                                gbwUtil = New GrdBkWgtUtility(arrRows, weight)
                            End If
                            'Use gbwUtil.Weight property
                            accumWgt += gbwUtil.Weight
                        End If
                    End If
                Next

                If ttlWgt > 0 Then
                    decComputedScore = System.Math.Round((accumWgt / ttlWgt) * 100, 2)
                End If
            End If
            row("Score") = String.Format("{0:C2}", decComputedScore.ToString)
            If CType(row("Score"), Decimal) <= score Then
                row("Variance") = String.Format("{0:C2}", CType((score - row("Score")), Decimal).ToString)
                dt.Rows.Add(row)
            End If
        Next
        Return ds1
    End Function
End Class
