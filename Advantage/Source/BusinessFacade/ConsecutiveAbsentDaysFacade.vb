﻿Public Class ConsecutiveAbsentDaysFacade
    Inherits BaseReportFacade

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim conAbsDays As New ConsecutiveAbsentDaysDB
        Dim ds As DataSet
        ds = BuildReportDataset(conAbsDays.GetConsecutiveAbsentDays(rptParamInfo), conAbsDays.StudentIdentifier, rptParamInfo)
        Return ds
    End Function
#End Region
#Region "Protected Methods"
    Protected Function BuildReportDataset(ByRef ds As DataSet, ByVal StudentIdentifier As String, ByVal rptParamInfo As Common.ReportParamInfo)
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        Dim temp As String
        Dim tempStuEnrollId As String = ""
        Dim ColumnNames() As String = {"StuEnrollId", "LDA"}
        Dim dtDistinct As DataTable = ds.Tables(0).DefaultView.ToTable(True, ColumnNames)
        Dim dtFinalRecords As New DataTable
        dtFinalRecords.TableName = "ConsecutiveAbsentDays"
        dtFinalRecords = ds.Tables("ConsecutiveAbsentDays").Clone
        dtFinalRecords.Clear()
        Dim dataRow As DataRow
        '' New Code Added By Vijay Ramteke On August 19, 2010
        ''For Each row As DataRow In ds.Tables("ConsecutiveAbsentDays").Rows
        ''    dataRow = row
        ''    Dim dtRecordDateMax As DateTime = dataRow("RecordDateMax").ToString
        ''    Dim dtCompare As DateTime = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy")
        ''    If dtRecordDateMax.CompareTo(dtCompare) >= 0 Then
        ''        If StudentIdentifier = "SSN" Then
        ''            If Not (dataRow("StudentIdentifier") Is System.DBNull.Value) Then
        ''                If dataRow("StudentIdentifier") <> "" Then
        ''                    temp = dataRow("StudentIdentifier")
        ''                    dataRow("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
        ''                End If
        ''            End If
        ''        End If
        ''        If rptParamInfo.UsedScheduledDays = True Then
        ''            dataRow("DayMissed") = CalculateDaysMissed(dataRow("LDA").ToString, dataRow("StuEnrollId").ToString, dataRow("CampusId").ToString)
        ''        Else
        ''            dataRow("DayMissed") = dataRow("CountRecordDate")
        ''        End If
        ''        dtFinalRecords.ImportRow(dataRow)
        ''    End If
        ''Next


        Dim tempRecords() As DataRow

        For Each row As DataRow In dtDistinct.Rows
            Dim hasAbsent As Boolean = False
            Dim DaysMissed As Integer = 0
            tempRecords = ds.Tables("ConsecutiveAbsentDays").Select("StuEnrollId='" + row("StuEnrollId").ToString + "'")
            dataRow = tempRecords(0)
            If rptParamInfo.UsedScheduledDays = True Then
                HasConsecutiveAbsentForScheduleDays(dataRow("StuEnrollId").ToString, dataRow("CampusId").ToString, hasAbsent, DaysMissed, tempRecords, rptParamInfo.CompOperator, CType(rptParamInfo.NumberOfConsecutiveAbsentDays, Integer), CType(rptParamInfo.NumberOfConsecutiveAbsentDays2, Integer))
            Else
                HasConsecutiveAbsent(hasAbsent, DaysMissed, tempRecords, rptParamInfo.CompOperator, CType(rptParamInfo.NumberOfConsecutiveAbsentDays, Integer), CType(rptParamInfo.NumberOfConsecutiveAbsentDays2, Integer))
            End If

            If hasAbsent = True Then
                If StudentIdentifier = "SSN" Then
                    If Not (dataRow("StudentIdentifier") Is System.DBNull.Value) Then
                        If dataRow("StudentIdentifier") <> "" Then
                            temp = dataRow("StudentIdentifier")
                            dataRow("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                        End If
                    End If
                End If
                dataRow("DayMissed") = DaysMissed
                dtFinalRecords.ImportRow(dataRow)
            End If
        Next
        '' New Code Added By Vijay Ramteke On August 19, 2010

        ds.Tables.RemoveAt(0)
        ds.Tables.Add(dtFinalRecords)

        ''If rptParamInfo.UsedScheduledDays = True Then
        ''    Dim dt As New DataTable
        ''    dt.TableName = "ConsecutiveAbsentDays"
        ''    dt = ds.Tables("ConsecutiveAbsentDays").Clone
        ''    dt.Clear()
        ''    Dim tempRows() As DataRow
        ''    If rptParamInfo.CompOperator = "equalto" Then
        ''        tempRows = ds.Tables("ConsecutiveAbsentDays").Select("DayMissed=" & rptParamInfo.NumberOfConsecutiveAbsentDays)
        ''    ElseIf rptParamInfo.CompOperator = "greaterthan" Then
        ''        tempRows = ds.Tables("ConsecutiveAbsentDays").Select("DayMissed>" & rptParamInfo.NumberOfConsecutiveAbsentDays)
        ''    ElseIf rptParamInfo.CompOperator = "lessthan" Then
        ''        tempRows = ds.Tables("ConsecutiveAbsentDays").Select("DayMissed<" & rptParamInfo.NumberOfConsecutiveAbsentDays)
        ''    Else
        ''        tempRows = ds.Tables("ConsecutiveAbsentDays").Select("DayMissed>=" & rptParamInfo.NumberOfConsecutiveAbsentDays & " And DayMissed<=" & rptParamInfo.NumberOfConsecutiveAbsentDays2)
        ''    End If
        ''    For Each dr In tempRows
        ''        dt.ImportRow(dr)
        ''    Next
        ''    ds.Tables.RemoveAt(0)
        ''    ds.Tables.Add(dt)
        ''End If

        Return ds
    End Function

    Protected Function CalculateDaysMissed(ByVal StartDate As DateTime, ByVal StuEnrollId As String, ByVal CampusId As String) As Integer
        Dim conAbsDays As New ConsecutiveAbsentDaysDB
        Dim dsSchedules As New DataSet
        dsSchedules = conAbsDays.GetStudentSchedules(StuEnrollId, CampusId)
        Dim DaysMissed As Integer = 0
        Dim dtDate As DateTime = StartDate
        Dim dtDateToday As DateTime = DateTime.Now
        Dim blSunday As Boolean = False
        Dim blMonday As Boolean = False
        Dim blTuesday As Boolean = False
        Dim blWednesday As Boolean = False
        Dim blThursday As Boolean = False
        Dim blFriday As Boolean = False
        Dim blSaturday As Boolean = False
        For Each drdw As DataRow In dsSchedules.Tables("WeekDays").Rows
            If drdw("dw").ToString = "0" Then
                blSunday = True
            End If
            If drdw("dw").ToString = "1" Then
                blMonday = True
            End If
            If drdw("dw").ToString = "2" Then
                blTuesday = True
            End If
            If drdw("dw").ToString = "3" Then
                blWednesday = True
            End If
            If drdw("dw").ToString = "4" Then
                blThursday = True
            End If
            If drdw("dw").ToString = "5" Then
                blFriday = True
            End If
            If drdw("dw").ToString = "6" Then
                blSaturday = True
            End If
        Next
        While dtDate <= dtDateToday
            Dim drH() As DataRow 'Holiday
            drH = dsSchedules.Tables("Holidays").Select("HolidayStartDate<='" & dtDate.ToString("MM/dd/yyyy") & "' AND HolidayEndDate>='" & dtDate.ToString("MM/dd/yyyy") & "' ")
            If drH.Length = 0 Then
                If dtDate.DayOfWeek = 0 And blSunday = True Then
                    DaysMissed += 1
                ElseIf dtDate.DayOfWeek = 1 And blMonday = True Then
                    DaysMissed += 1
                ElseIf dtDate.DayOfWeek = 2 And blTuesday = True Then
                    DaysMissed += 1
                ElseIf dtDate.DayOfWeek = 3 And blWednesday = True Then
                    DaysMissed += 1
                ElseIf dtDate.DayOfWeek = 4 And blThursday = True Then
                    DaysMissed += 1
                ElseIf dtDate.DayOfWeek = 5 And blFriday = True Then
                    DaysMissed += 1
                ElseIf dtDate.DayOfWeek = 6 And blSaturday = True Then
                    DaysMissed += 1
                End If
                dtDate = dtDate.AddDays(1)
            Else
                dtDate = dtDate.AddDays(1)
            End If
        End While

        Return DaysMissed

    End Function
    Protected Sub HasConsecutiveAbsent(ByRef hasAbsent As Boolean, ByRef DaysMissed As Integer, ByVal tempRecords() As DataRow, ByVal CompareOperator As String, ByVal NumConAbsDay1 As Integer, ByVal NumConAbsDay2 As Integer)
        Dim row As DataRow = tempRecords(0)
        Dim StartDate As DateTime = row("RecordDate").ToString

        For Each dr As DataRow In tempRecords
            Dim CompareDate As DateTime = dr("RecordDate").ToString
            If StartDate.CompareTo(CompareDate) = 0 Then
                DaysMissed = DaysMissed + 1
                If CompareOperator.ToLower = "equalto" Then
                    If DaysMissed = NumConAbsDay1 Then
                        hasAbsent = True
                        Exit Sub
                    End If
                End If
            Else
                DaysMissed = 1
                StartDate = CompareDate
            End If
            StartDate = StartDate.AddDays(1)
        Next

        If CompareOperator.ToLower = "equalto" Then
            If DaysMissed = NumConAbsDay1 Then
                hasAbsent = True
            Else
                hasAbsent = False
            End If
        End If

        If CompareOperator.ToLower = "greaterthan" Then
            If DaysMissed > NumConAbsDay1 Then
                hasAbsent = True
            Else
                hasAbsent = False
            End If
        End If

        If CompareOperator.ToLower = "lessthan" Then
            If DaysMissed < NumConAbsDay1 Then
                hasAbsent = True
            Else
                hasAbsent = False
            End If
        End If

        If CompareOperator.ToLower = "between" Then
            If DaysMissed >= NumConAbsDay1 And DaysMissed <= NumConAbsDay2 Then
                hasAbsent = True
            Else
                hasAbsent = False
            End If
        End If
    End Sub
    Protected Sub HasConsecutiveAbsentForScheduleDays(ByVal StuEnrollId As String, ByVal CampusId As String, ByRef hasAbsent As Boolean, ByRef DaysMissed As Integer, ByVal tempRecords() As DataRow, ByVal CompareOperator As String, ByVal NumConAbsDay1 As Integer, ByVal NumConAbsDay2 As Integer)
        Dim conAbsDays As New ConsecutiveAbsentDaysDB
        Dim dsSchedules As New DataSet
        dsSchedules = conAbsDays.GetStudentSchedules(StuEnrollId, CampusId)
        Dim row As DataRow = tempRecords(0)
        Dim StartDate As DateTime = row("RecordDate").ToString
        Dim dtDate As DateTime = StartDate
        Dim dtDateToday As DateTime = DateTime.Now
        Dim blSunday As Boolean = False
        Dim blMonday As Boolean = False
        Dim blTuesday As Boolean = False
        Dim blWednesday As Boolean = False
        Dim blThursday As Boolean = False
        Dim blFriday As Boolean = False
        Dim blSaturday As Boolean = False
        For Each drdw As DataRow In dsSchedules.Tables("WeekDays").Rows
            If drdw("dw").ToString = "0" Then
                blSunday = True
            End If
            If drdw("dw").ToString = "1" Then
                blMonday = True
            End If
            If drdw("dw").ToString = "2" Then
                blTuesday = True
            End If
            If drdw("dw").ToString = "3" Then
                blWednesday = True
            End If
            If drdw("dw").ToString = "4" Then
                blThursday = True
            End If
            If drdw("dw").ToString = "5" Then
                blFriday = True
            End If
            If drdw("dw").ToString = "6" Then
                blSaturday = True
            End If
        Next

        For Each dr As DataRow In tempRecords
            Dim CompareDate As DateTime = dr("RecordDate").ToString
            If dtDate.CompareTo(CompareDate) = 0 Then
                Dim drH() As DataRow 'Holiday
                drH = dsSchedules.Tables("Holidays").Select("HolidayStartDate<='" & CompareDate.ToString("MM/dd/yyyy") & "' AND HolidayEndDate>='" & CompareDate.ToString("MM/dd/yyyy") & "' ")
                If drH.Length = 0 Then
                    If dtDate.DayOfWeek = 0 And blSunday = True Then
                        DaysMissed += 1
                    ElseIf dtDate.DayOfWeek = 1 And blMonday = True Then
                        DaysMissed += 1
                    ElseIf dtDate.DayOfWeek = 2 And blTuesday = True Then
                        DaysMissed += 1
                    ElseIf dtDate.DayOfWeek = 3 And blWednesday = True Then
                        DaysMissed += 1
                    ElseIf dtDate.DayOfWeek = 4 And blThursday = True Then
                        DaysMissed += 1
                    ElseIf dtDate.DayOfWeek = 5 And blFriday = True Then
                        DaysMissed += 1
                    ElseIf dtDate.DayOfWeek = 6 And blSaturday = True Then
                        DaysMissed += 1
                    End If
                    If CompareOperator.ToLower = "equalto" Then
                        If DaysMissed = NumConAbsDay1 Then
                            hasAbsent = True
                            Exit Sub
                        End If
                    End If
                End If
            Else
                DaysMissed = 1
                dtDate = CompareDate
            End If
            dtDate = dtDate.AddDays(1)
        Next

        If CompareOperator.ToLower = "equalto" Then
            If DaysMissed = NumConAbsDay1 Then
                hasAbsent = True
            Else
                hasAbsent = False
            End If
        End If

        If CompareOperator.ToLower = "greaterthan" Then
            If DaysMissed > NumConAbsDay1 Then
                hasAbsent = True
            Else
                hasAbsent = False
            End If
        End If

        If CompareOperator.ToLower = "lessthan" Then
            If DaysMissed < NumConAbsDay1 Then
                hasAbsent = True
            Else
                hasAbsent = False
            End If
        End If

        If CompareOperator.ToLower = "between" Then
            If DaysMissed >= NumConAbsDay1 And DaysMissed <= NumConAbsDay2 Then
                hasAbsent = True
            Else
                hasAbsent = False
            End If
        End If

    End Sub
#End Region
End Class
