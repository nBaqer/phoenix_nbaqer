' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' SuffixesFacade.vb
'
' SuffixesFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class SuffixesFacade
    Public Function GetAllSuffixes() As DataSet

        '   Instantiate DAL component
        Dim suffixesDB As New suffixesDB

        '   get the dataset with all Suffixes
        Return suffixesDB.GetAllSuffixes()

    End Function
End Class
