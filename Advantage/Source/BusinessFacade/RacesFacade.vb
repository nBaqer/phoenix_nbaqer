' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' RacesFacade.vb
'
' RacesFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class RacesFacade
    Public Function GetAllRaces() As DataSet

        '   Instantiate DAL component
        Dim racesDB As New RacesDB

        '   get the dataset with all Races
        Return racesDB.GetAllRaces()

    End Function
End Class
