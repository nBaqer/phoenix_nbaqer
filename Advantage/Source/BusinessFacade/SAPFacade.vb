Imports System.data
Imports System.text
Imports FAME.AdvantageV1.DataAccess
Public Class SAPFacade
    Public Function PopulateDataList(ByVal SAPId As String) As DataSet

        Dim db As New SapDB
        Dim ds As New DataSet
        ds = db.GetSAPPolicyTrigs(SAPId)
        'Add the concatenated FullName column to the datatable before sending it to the web page
        ds = BuildTrigDescrip(ds)
        Return ds
    End Function
    Public Function BuildTrigDescrip(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables(0)
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("SAPDetailId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("TrigDescrip", GetType(String))
            For Each row In tbl.Rows
                Dim tVal As Integer = Integer.Parse(row("TrigValue"))
                If row("TrigOffsetSeq").ToString() = "" Then
                    row("TrigDescrip") = Math.Round(tVal / 100, 2) & " " & row("TrigUnitTypDescrip") & " " & "after the " & row("TrigOffTypDescrip")
                Else
                    row("TrigDescrip") = Math.Round(tVal / 100, 2) & " " & row("TrigUnitTypDescrip") & " " & "after the " & row("TrigOffTypDescrip") & row("TrigOffsetSeq")
                End If

            Next
        End If
        Return ds
    End Function
    Public Function GetSAPPolicy(ByVal SAPID As String) As SAPInfo
        With New SapDB
            Return .GetSAPPolicy(SAPID)
        End With
    End Function
    Public Function InsertSAPCustomVerbiage(ByVal ListofSapCustomVerbiage As List(Of SAPCustomVerbiage), ByVal userId As String) As String
        With New SapDB
            Return .InsertSAPCustomVerbiage(ListofSapCustomVerbiage, userId)
        End With
    End Function

    Public Function UpdateSAPCustomVerbiage(ByVal ListofSapCustomVerbiage As List(Of SAPCustomVerbiage), ByVal userId As String) As String
        With New SapDB
            Return .UpdateSAPCustomVerbiage(ListofSapCustomVerbiage, userId)
        End With
    End Function
    Public Function SetSAPCustomVerbiageStatus(ByVal sapCustomVerbiage As SAPCustomVerbiage) As String
        With New SapDB
            Return .SetSAPCustomVerbiageStatus(sapCustomVerbiage)
        End With
    End Function
    Public Function GetSapNoticeDefaultMessages() As Dictionary(Of String, String)
        With New SapDB
            Return .GetSapNoticeDefaultMessages()
        End With
    End Function
    Public Function GetTrigUnits() As DataSet

        '   Instantiate DAL component
        Dim DB As New SapDB

        '   get the dataset with all degrees
        Return DB.GetTrigUnits()

    End Function

    Public Function GetTrigOffTyps(TrigUnitTypId As Integer) As DataSet

        '   Instantiate DAL component
        Dim DB As New SapDB

        '   get the dataset with all degrees
        Return DB.GetTrigOffTyps(TrigUnitTypId)

    End Function
    Public Function GetTrigOffTyps() As DataSet

        '   Instantiate DAL component
        Dim DB As New SapDB

        '   get the dataset with all degrees
        Return DB.GetTrigOffTyps()

    End Function

    Public Function GetQuantMinUnitTyps() As DataSet

        '   Instantiate DAL component
        Dim DB As New SapDB

        '   get the dataset with all degrees
        Return DB.GetQuantMinUnitTyps()

    End Function
    Public Function GetQualTyps(Optional ByVal qualMinType As Integer = 1) As DataSet

        '   Instantiate DAL component
        Dim DB As New SapDB

        '   get the dataset with all degrees

        Return DB.GetQualTyps(qualMinType)


    End Function
    Public Function GetConsequences() As DataSet

        '   Instantiate DAL component
        Dim DB As New SapDB

        '   get the dataset with all degrees
        Return DB.GetConsequences()

    End Function
    Public Function GetSAPInfoOnItemCmd(ByVal SAPDetailId As String) As SAPInfo

        '   Instantiate DAL component
        Dim DB As New SapDB
        Dim ds As DataSet
        Dim SAPInfo As New SAPInfo

        '   get the dataset with all degrees
        ds = DB.GetSAPInfo_SP(SAPDetailId)

        'Fill Requirement Type Object to send back to UI.
        SAPInfo = FillSAPInfoObject(ds)

        Return SAPInfo
    End Function
   
    Public Function FillSAPInfoObject(ByVal ds As DataSet) As SAPInfo
        Dim SAPObject As New SAPInfo
        Dim dt, dt1 As DataTable
        Dim row As DataRow
        dt = ds.Tables(0)
        dt1 = ds.Tables(1)
        '   build validation sequence string
        Dim sb As New StringBuilder
        For Each row In dt.Rows
            'If Not row.IsNull("TrigOffsetSeq") Then
            '    sb.Append(CType(row("Seq") * 10000000 + row("TrigOffsetSeq") * 100000 + row("TrigOffsetTypId") * 10000 + row("TrigValue") * 10 + row("TrigUnitTypId"), String).ToString)
            'Else
            '    sb.Append(CType(row("Seq") * 10000000 + 0 * 100000 + row("TrigOffsetTypId") * 10000 + row("TrigValue") * 10 + row("TrigUnitTypId"), String).ToString)
            'End If
            If Not row.IsNull("TrigOffsetSeq") Then
                sb.Append(CType(row("TrigOffsetSeq") * 100000 + row("TrigOffsetTypId") * 10000 + row("TrigValue") * 10 + row("TrigUnitTypId"), String).ToString)
            Else
                sb.Append(CType(row("TrigOffsetTypId") * 10000 + row("TrigValue") * 10 + row("TrigUnitTypId"), String).ToString)
            End If

            sb.Append(";")
        Next

        'remove the last ';'
        If sb.Length > 1 Then sb.Remove(sb.Length - 1, 1)

        'If ds.Tables.Count > 1 Then
        '    'Error - it returned more than one record
        'Else
        For Each row In dt.Rows
            SAPObject.SAPDetailId = row("SAPDetailId")
            SAPObject.Seq = row("Seq")
            SAPObject.TrigValue = row("TrigValue")
            SAPObject.TrigUnit = row("TrigUnitTypDescrip")
            SAPObject.TrigOffsetTyp = row("TrigOffTypDescrip")

            If Not row.IsNull("TrigOffsetSeq") Then
                SAPObject.TrigOffsetSeq = row("TrigOffsetSeq")
            Else
                'Do nothing. Use the default of 0.
            End If

            If Not row.IsNull("QualMinValue") Then
                SAPObject.QualMinValue = row("QualMinValue")
            Else
                SAPObject.QualMinValue = 0.0
            End If

            SAPObject.QuantMinValue = row("QuantMinValue")
            SAPObject.QuantMinUnitType = row("QuantMinTypDesc")
            SAPObject.QualType = row("QualMinTypDesc")
            SAPObject.Consequence = row("ConseqTypDesc")
            SAPObject.MinCredsCompltd = row("MinCredsCompltd")
            If Not row.IsNull("MinAttendanceValue") Then
                SAPObject.MinAttendanceValue = row("MinAttendanceValue")
            Else

            End If
            SAPObject.ValidationSequence = sb.ToString
            SAPObject.IsSAPPolicyUsed = row("IsSAPPolicyUsed")
            SAPObject.OverallAttendance = True
            If Not row("Accuracy") Is System.DBNull.Value Then SAPObject.Accuracy = row("Accuracy") Else SAPObject.Accuracy = 0
        Next
        If dt1.Rows.Count > 0 Then
            SAPObject.OverallAttendance = False
            Dim _QuantMinValueByInsTypes(dt1.Rows.Count) As QuantMinValueByInsType
            For i As Integer = 0 To dt1.Rows.Count - 1
                row = dt1.Rows(i)
                _QuantMinValueByInsTypes(i) = New QuantMinValueByInsType
                'SAPQuantInsTypeID,QuantMinValue,SAPDetailId,InstructionTypeId
                _QuantMinValueByInsTypes(i).InstructionTypeId = row("InstructionTypeId")
                _QuantMinValueByInsTypes(i).QuantMinValue = row("QuantMinValue")
                _QuantMinValueByInsTypes(i).SAPDetailId = row("SAPDetailId")
                _QuantMinValueByInsTypes(i).SAPQuantInsTypeID = row("SAPQuantInsTypeID")
                SAPObject.AddQuantMinValueByInsType(_QuantMinValueByInsTypes(i))
            Next

        End If
        'End If
        Return SAPObject
    End Function
    Public Function ValidateIncrement(ByVal Increment As Integer, ByVal ds As DataSet) As String


        Dim tbl1 As New DataTable
        Dim row As DataRow
        Dim msg As String

        tbl1 = ds.Tables(0)
        If (ds.Tables(0).Rows.Count = 0) Then

        End If
        'Make the ChildId column the primary key
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("SAPDetailId")}
        End With


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        For Each row In tbl1.Rows

            If (tbl1.Rows.Count > 0) Then
                If Not (row("Seq") = Increment) Then
                    'Validation successful
                Else
                    'Validation failed
                    msg = "Increment already exists,chose new increment value"
                End If

            End If
        Next

        Return msg
    End Function
    Public Function AddSAPDetails(ByVal SAPDetails As SAPInfo) As String
        Dim db As New SapDB

        Return db.AddSAPDetails(SAPDetails)
    End Function
    Public Function UpdateSAPDetails(ByVal SAPDetails As SAPInfo) As String
        Dim db As New SapDB
        Return db.UpdateSAPDetails(SAPDetails)
    End Function
    Public Function DeleteSAPDetails(ByVal SAPDetailId As Guid) As String
        Dim db As New SapDB
        Return db.DeleteSAPDetails(SAPDetailId)
    End Function

    Public Function GetSAPPolicyName(ByVal sapPolicyID As String) As String
        Dim DB As New SapDB

        Return DB.GetSAPPolicyName(sapPolicyID)
    End Function

    Public Function GetSAPDataForCurrentGPA() As DataTable
        Dim db As New SAPCheckDB
        Dim dt As New DataTable

        dt = db.GetSAPDataForCurrentGPA.Copy

        Return dt

    End Function

    'Public Function ProcessSAPForEPP(ByVal userName As String) As DataTable
    '    Dim dt As New DataTable
    '    Dim db As New SAPCheckDB
    '    Dim dr As DataRow
    '    Dim sapChkInfo As New SAPChkResultInfo
    '    Dim dt2 As New DataTable
    '    Dim dr2 As DataRow
    '    Dim sapChkResult As New SAPChkResultInfo
    '    Dim facInputMasks As New InputMasksFacade
    '    Dim SSNMask As String
    '    '' Dim zipMask As String

    '    'Add necessary columns to dt2
    '    Dim col1 As DataColumn = dt2.Columns.Add("SSN")
    '    Dim col2 As DataColumn = dt2.Columns.Add("Student")
    '    Dim col3 As DataColumn = dt2.Columns.Add("Program")
    '    Dim col4 As DataColumn = dt2.Columns.Add("NPL")
    '    Dim col5 As DataColumn = dt2.Columns.Add("MTF")
    '    Dim col6 As DataColumn = dt2.Columns.Add("UA")
    '    Dim col7 As DataColumn = dt2.Columns.Add("UC")
    '    Dim col8 As DataColumn = dt2.Columns.Add("PP")
    '    Dim col9 As DataColumn = dt2.Columns.Add("GPA")
    '    Dim col10 As DataColumn = dt2.Columns.Add("INC1")
    '    Dim col11 As DataColumn = dt2.Columns.Add("INC2")
    '    Dim col12 As DataColumn = dt2.Columns.Add("INC3")
    '    Dim col13 As DataColumn = dt2.Columns.Add("INC4")

    '    'Get SAP data
    '    dt = db.GetSAPDataForCurrentGPA.Copy

    '    'Process each row in dt.
    '    'If PP is less than 25% then we will not do anything.
    '    'If PP >= 25% and PP <= 49% check if increment 1 results have been recorded.
    '    'If not, then insert info for increment 1.
    '    'If PP >= 50% and PP <= 74% check if increment 2 results have been recorded.
    '    'If not, then insert info for increment 2.
    '    'If PP >= 75% and PP <= 99% check if increment 3 results have been recorded.
    '    'If not, then insert info for increment 3.
    '    'If PP >= 100% and PP <= 150% check if increment 4 results have been recorded.
    '    'If not, then insert info for increment 4.
    '    For Each dr In dt.Rows
    '        'Only process if PP is not null
    '        If Not dr.IsNull("PP") Then
    '            'Build SAPChkResultInfo object
    '            sapChkInfo.Comments = ""
    '            If Not dr.IsNull("UA") Then
    '                sapChkInfo.CreditsAttempted = dr("UA")
    '            Else
    '                sapChkInfo.CreditsAttempted = 0.0
    '            End If

    '            If Not dr.IsNull("UC") Then
    '                sapChkInfo.CreditsEarned = dr("UC")
    '            Else
    '                sapChkInfo.CreditsEarned = 0.0
    '            End If

    '            sapChkInfo.DatePerformed = Date.Now
    '            sapChkInfo.GPA = dr("GPA")

    '            If dr("GPA") >= 2 Then
    '                sapChkInfo.IsMakingSAP = True
    '            Else
    '                sapChkInfo.IsMakingSAP = False
    '            End If

    '            sapChkInfo.ModDate = Date.Now
    '            sapChkInfo.ModUser = userName
    '            sapChkInfo.StuEnrollId = dr("StuEnrollId").ToString()

    '            If sapChkInfo.IsMakingSAP = False Then
    '                dr2 = dt2.NewRow

    '                dr2("SSN") = dr("SSN")
    '                dr2("Student") = dr("Last Name") & ", " & dr("First Name")
    '                dr2("Program") = dr("Program")
    '                dr2("NPL") = Math.Round(dr("NPL"), 0)
    '                dr2("MTF") = Math.Round(dr("MTF"), 0)
    '                If Not dr.IsNull("UA") Then
    '                    dr2("UA") = Math.Round(dr("UA"), 0)
    '                Else
    '                    dr2("UA") = 0
    '                End If
    '                If Not dr.IsNull("UC") Then
    '                    dr2("UC") = Math.Round(dr("UC"), 0)
    '                Else
    '                    dr2("UC") = 0
    '                End If

    '                dr2("PP") = Math.Round(dr("PP"), 0).ToString() & "%"
    '                dr2("GPA") = Math.Round(dr("GPA"), 2)
    '                dr2("INC1") = ""
    '                dr2("INC2") = ""
    '                dr2("INC3") = ""
    '                dr2("INC4") = ""
    '            End If

    '            If dr("PP") < 25 Then
    '                'No need to add sap check result.
    '                'We still have to check if the GPA is less than 2.0. If that is the case then we need
    '                'to add a row to the datatable that will be returned to the client.


    '            ElseIf dr("PP") >= 25 And dr("PP") <= 49 Then
    '                'Check if increment 1 results have been posted for this StuEnrollId
    '                If Not db.DoesSAPIncrementHasResultsForEnrollment(1, dr("StuEnrollId").ToString()) Then
    '                    'Insert entry for increment 1
    '                    sapChkInfo.Increment = 1
    '                    db.AddSAPChkResult(sapChkInfo)

    '                    'If not making SAP then we can get the values from dr without having to waste time
    '                    'going to the database
    '                    If Not sapChkInfo.IsMakingSAP Then
    '                        dr2("INC1") = Math.Round(dr("GPA"), 2).ToString() & "/" & Math.Round(dr("UC") * 100 / dr("UA"), 0) & "%"
    '                    End If
    '                Else
    '                    'If not making SAP then we need to get the values from the database.
    '                    If Not sapChkInfo.IsMakingSAP Then
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 1)
    '                        dr2("INC1") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                    End If

    '                End If

    '            ElseIf dr("PP") >= 50 And dr("PP") <= 74 Then
    '                'Check if increment 2 results have been posted for this StuEnrollId
    '                If Not db.DoesSAPIncrementHasResultsForEnrollment(2, dr("StuEnrollId").ToString()) Then
    '                    'Insert entry for increment 2
    '                    sapChkInfo.Increment = 2
    '                    db.AddSAPChkResult(sapChkInfo)

    '                    'If not making SAP then we can get the values for inc2 dr without having to waste time
    '                    'going to the database
    '                    If Not sapChkInfo.IsMakingSAP Then
    '                        dr2("INC2") = Math.Round(dr("GPA"), 2).ToString() & "/" & Math.Round(dr("UC") * 100 / dr("UA"), 0) & "%"
    '                        'We still have to get increment1 values from the database
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 1)
    '                        dr2("INC1") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                    End If
    '                Else
    '                    'If not making SAP then we need to get inc1 and inc2 values from the database
    '                    If Not sapChkInfo.IsMakingSAP Then
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 1)
    '                        dr2("INC1") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 2)
    '                        dr2("INC2") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                    End If

    '                End If
    '            ElseIf dr("PP") >= 75 And dr("PP") <= 99 Then
    '                'Check if increment 3 results have been posted for this StuEnrollId
    '                If Not db.DoesSAPIncrementHasResultsForEnrollment(3, dr("StuEnrollId").ToString()) Then
    '                    'Insert entry for increment 3
    '                    sapChkInfo.Increment = 3
    '                    db.AddSAPChkResult(sapChkInfo)

    '                    'If not making SAP then we can get the values for inc3 without having to waste time
    '                    'going to the database
    '                    If Not sapChkInfo.IsMakingSAP Then
    '                        dr2("INC3") = Math.Round(dr("GPA"), 2).ToString() & "/" & Math.Round(dr("UC") * 100 / dr("UA"), 0) & "%"
    '                        'We still have to get inc1 and inc2 values from the database
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 1)
    '                        dr2("INC1") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 2)
    '                        dr2("INC2") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                    End If
    '                Else
    '                    'If not making SAP then we need to get inc1, inc2 and inc3 values from the database
    '                    If Not sapChkInfo.IsMakingSAP Then
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 1)
    '                        dr2("INC1") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 2)
    '                        dr2("INC2") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 3)
    '                        dr2("INC3") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                    End If
    '                End If
    '            ElseIf dr("PP") >= 100 And dr("PP") <= 150 Then
    '                'Check if increment 4 results have been posted for this StuEnrollId
    '                If Not db.DoesSAPIncrementHasResultsForEnrollment(4, dr("StuEnrollId").ToString()) Then
    '                    'Insert entry for increment 4
    '                    sapChkInfo.Increment = 4
    '                    db.AddSAPChkResult(sapChkInfo)

    '                    'If not making SAP then we can get the values for inc4 without having to waste time
    '                    'going to the database
    '                    If Not sapChkInfo.IsMakingSAP Then
    '                        dr2("INC4") = Math.Round(dr("GPA"), 2).ToString() & "/" & Math.Round(dr("UC") * 100 / dr("UA"), 0) & "%"
    '                        'We still have to get inc1, inc2 and inc3 values from the database
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 1)
    '                        dr2("INC1") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 2)
    '                        dr2("INC2") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 3)
    '                        dr2("INC3") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                    End If
    '                Else
    '                    'If not making SAP then we need to get inc1, inc2, inc3 and inc4 values from the database
    '                    If Not sapChkInfo.IsMakingSAP Then
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 1)
    '                        dr2("INC1") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 2)
    '                        dr2("INC2") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 3)
    '                        dr2("INC3") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                        sapChkResult = db.GetSAPChkResultInfo(dr("StuEnrollId").ToString(), 4)
    '                        dr2("INC4") = Math.Round(sapChkResult.GPA, 2).ToString() & "/" & Math.Round(sapChkResult.CreditsEarned * 100 / sapChkResult.CreditsAttempted, 0) & "%"
    '                    End If

    '                End If
    '            Else
    '                Throw New System.Exception("Invalid Percent Point for student: " & dr("First Name") & " " & dr("Last Name"))

    '            End If

    '            If sapChkInfo.IsMakingSAP = False Then
    '                dt2.Rows.Add(dr2)
    '            End If

    '        End If

    '    Next

    '    If dt2.Rows.Count > 0 Then
    '        'Format the SSN field
    '        'Get the mask for phone numbers and zip
    '        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

    '        For Each dr In dt2.Rows
    '            If dr("SSN").ToString.Length >= 1 Then
    '                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
    '                Dim temp As String = dr("SSN")
    '                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
    '            Else
    '                dr("SSN") = ""
    '            End If
    '        Next
    '    End If

    '    Return dt2

    'End Function

    Public Function GetSAPChkResultInfo(ByVal stuEnrollId As String, ByVal increment As Integer) As SAPChkResultInfo
        Dim DB As New SAPCheckDB

        Return DB.GetSAPChkResultInfo(stuEnrollId, increment)

    End Function
    Public Function GetValidationSequence(ByVal SAPId As String) As String
        '   return string
        Return (New SapDB).GetValidationSequence(SAPId)
    End Function
    Public Function InsertSAPPolicy(ByVal SAPPolicy As SAPInfo, ByVal user As String) As String
        Dim db As New SapDB

        Return db.InsertSAPPolicy(SAPPolicy, user)
    End Function

    Public Function DeleteSAPPolicy(ByVal SAPPolicyId As String) As String
        Dim DB As New SapDB

        Return DB.DeleteSAPPolicy(SAPPolicyId)
    End Function

    Public Function DeleteSAPCustomVerbiage(ByVal SAPPolicyId As String) As String
        Dim DB As New SapDB
        Return DB.DeleteSAPCustomVerbiage(SAPPolicyId)
    End Function
    Public Function UpdateSAPPolicy(ByVal SAPPolicy As SAPInfo, ByVal user As String) As String
        Dim DB As New SapDB

        Return DB.UpdateSAPPolicy(SAPPolicy, user)
    End Function

    Public Function GetSAPPolicyMinAttendanceValue(ByVal SAPPolicyID As String) As Decimal
        Return (New SapDB).GetSAPPolicyMinAttendanceValue(SAPPolicyID)
    End Function
End Class
