﻿Imports System
Imports System.IO
Imports System.Net
Imports System.Data
Imports System.Text
Imports System.Data.OleDb
Imports FAME.AdvantageV1.DataAccess
Public Class EDFacade
    Dim dtLogMessage, dtPATSData, dtPATSDisb, dtDirectLoan, dtDLSchDisb, dtDLActualDisb As New DataTable()
    Public Function ProcessEDFile(ByRef clsflFile As EDFileInfo, ByRef myMsgCollection As EDCollectionMessageInfos, Optional ByVal SourceFolderLocation As String = "") As Boolean
        Dim strRecord As String = ""
        Dim intLoopCount As Integer = 0
        Dim intBytesRead As Integer = 0
        Dim Request As WebRequest
        Dim ResponseStream As Stream
        Dim Response1 As WebResponse
        Dim myCredentials As NetworkCredential
        ProcessEDFile = False
        Try
            If clsflFile.datafromremotecomputer = True Then
                myCredentials = New NetworkCredential(clsflFile.networkuser, clsflFile.networkPassword, "")
            End If
            Request = System.Net.FileWebRequest.Create(clsflFile.strFileName) 'WebRequest.Create(strFileName) 'System.Net.FileWebRequest.Create("\\qaw2k\ESPFileIn\CLFAHEAD.000") 
            Request.Headers.Add("Translate: f")
            Request.Method = "GET"
            If clsflFile.datafromremotecomputer = True Then
                Request.Credentials = myCredentials
            Else
                Request.Credentials = CredentialCache.DefaultCredentials
            End If

            Response1 = CType(Request.GetResponse, WebResponse)
            ResponseStream = Response1.GetResponseStream
            Dim objIn As StreamReader
            objIn = New StreamReader(ResponseStream, System.Text.Encoding.Default)
            Dim contents As String
            'Dim intLoop As Integer = 1
            Do
                Try
                    contents = objIn.ReadLine().Trim()
                Catch ex As Exception
                    contents = Nothing
                    Exit Do
                End Try
                If Not contents + "" = "" Then
                    strRecord = contents
                    myMsgCollection.Add(clsflFile.strMsgType, strRecord, clsflFile.strFileName)
                End If
            Loop Until contents Is Nothing
            objIn.Close()
            'End If

            ''ResponseStream.Flush()
            ResponseStream.Close()
            Response1.Close()
            ProcessEDFile = True
        Catch e As ArgumentNullException
            ProcessEDFile = False
            ''Throw
        Catch e As ArgumentOutOfRangeException
            ProcessEDFile = False
            ''Throw
        Catch e As System.Exception
            ProcessEDFile = False
            ''Throw
        End Try

    End Function
    Public Function ProcessExceptionEDFile(ByRef clsflFile As EDFileInfo, ByRef myMsgCollection As EDCollectionMessageInfos, ByVal FileName As String, ByRef AcademicYearId As String, ByRef AwardStartDate As String, ByRef AwardEndDate As String, Optional ByVal SourceFolderLocation As String = "") As Boolean
        Dim strRecord As String = ""
        Dim intLoopCount As Integer = 0
        Dim intBytesRead As Integer = 0

        Dim db As New DataAccess.DataAccess
        Dim sbParent As New StringBuilder
        Dim sbChild As New StringBuilder
        Dim dsParent As DataSet
        Dim dsChild As DataSet

        ProcessExceptionEDFile = False
        Try
            If clsflFile.strMsgType.ToUpper = "PELL" Then '' For Pell, ACG, SMART and Teach
                With sbParent
                    .Append(" Select ExceptionReportId, DbIn, Filter, AwardAmount, AwardId, GrantType, AddDate, AddTime, OriginalSSN, UpdateDate, UpdateTime, OriginationStatus, AcademicYearId, AwardYearStartDate, AwardYearEndDate, ErrorMsg ")
                    .Append(" From syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable Where FileName='" & FileName & "'")
                End With
                Try
                    dsParent = db.RunSQLDataSet(sbParent.ToString)
                    sbParent = sbParent.Remove(0, sbParent.Length)
                    If dsParent.Tables(0).Rows.Count > 0 Then
                        For Each drP As DataRow In dsParent.Tables(0).Rows
                            If drP("DbIn").ToString = "T" Then
                                strRecord = drP("DbIn").ToString & "," & drP("Filter").ToString & "," & drP("AwardAmount").ToString & "," & drP("AwardId").ToString & "," & drP("GrantType").ToString & "," & drP("OriginationStatus").ToString & "," & drP("AddDate").ToString & "," & drP("AddTime").ToString & "," & drP("OriginalSSN").ToString & "," & drP("UpdateDate").ToString & "," & drP("UpdateTime").ToString
                            Else
                                strRecord = drP("DbIn").ToString & "," & drP("Filter").ToString & "," & drP("AwardAmount").ToString & "," & drP("AwardId").ToString & "," & drP("OriginationStatus").ToString & "," & drP("AddDate").ToString & "," & drP("AddTime").ToString & "," & drP("OriginalSSN").ToString & "," & drP("UpdateDate").ToString & "," & drP("UpdateTime").ToString
                            End If

                            AcademicYearId = drP("AcademicYearId").ToString
                            AwardStartDate = drP("AwardYearStartDate").ToString
                            AwardEndDate = drP("AwardYearEndDate").ToString
                            If Not strRecord + "" = "" Then
                                myMsgCollection.Add(clsflFile.strMsgType, strRecord, clsflFile.strFileName)
                                myMsgCollection.Items(myMsgCollection.Count - 1).strError = drP("ErrorMsg").ToString
                                myMsgCollection.Items(myMsgCollection.Count - 1).strAwardYearStartDate = drP("AwardYearStartDate").ToString
                                myMsgCollection.Items(myMsgCollection.Count - 1).strAwardYearEndDate = drP("AwardYearEndDate").ToString
                                myMsgCollection.Items(myMsgCollection.Count - 1).strAcademicYearId = drP("AcademicYearId").ToString
                            End If

                            With sbChild
                                .Append(" Select DbIn, Filter, AccDisbAmount, DisbDate, DisbNum, DisbRelIndi, DusbSeqNum, SimittedDisbAmount, ActionStatusDisb, ErrorMsg ")
                                .Append(" From syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable Where FileName='" & FileName & "' and ExceptionReportId='" & drP("ExceptionReportId").ToString & "' order by DisbNum")
                            End With
                            dsChild = db.RunSQLDataSet(sbChild.ToString)
                            For Each drC As DataRow In dsChild.Tables(0).Rows
                                If drP("DbIn").ToString = "T" Then
                                    strRecord = drC("DbIn").ToString & "," & drC("Filter").ToString & "," & drC("AccDisbAmount").ToString & "," & drC("ActionStatusDisb").ToString & "," & drC("DisbDate").ToString & "," & drC("DisbNum").ToString & "," & drC("DisbRelIndi").ToString & "," & drC("DusbSeqNum").ToString & "," & drC("SimittedDisbAmount").ToString
                                Else
                                    strRecord = drC("DbIn").ToString & "," & drC("Filter").ToString & "," & drC("AccDisbAmount").ToString & "," & drC("DisbDate").ToString & "," & drC("DisbNum").ToString & "," & drC("DisbRelIndi").ToString & "," & drC("DusbSeqNum").ToString & "," & drC("SimittedDisbAmount").ToString & "," & drC("ActionStatusDisb").ToString
                                End If

                                If Not strRecord + "" = "" Then
                                    myMsgCollection.Add(clsflFile.strMsgType, strRecord, clsflFile.strFileName)
                                    myMsgCollection.Items(myMsgCollection.Count - 1).strError = drC("ErrorMsg").ToString
                                    myMsgCollection.Items(myMsgCollection.Count - 1).strAwardYearStartDate = drP("AwardYearStartDate").ToString
                                    myMsgCollection.Items(myMsgCollection.Count - 1).strAwardYearEndDate = drP("AwardYearEndDate").ToString
                                    myMsgCollection.Items(myMsgCollection.Count - 1).strAcademicYearId = drP("AcademicYearId").ToString
                                End If
                            Next
                            sbChild = sbChild.Remove(0, sbChild.Length)
                        Next
                    Else
                        ProcessEDFile(clsflFile, myMsgCollection, SourceFolderLocation)
                    End If
                    ProcessExceptionEDFile = True
                Catch ex As Exception

                End Try
            Else  '' For Direct Loan
                With sbParent
                    .Append(" Select ExceptionReportId, DbIn, Filter, AddDate, AddTime, LoanAmtApproved, LoanFeePer, LoanId, LoanPeriodEndDate, LoanPeriodStartDate, LoanStatus, LoanType, MPNStatus, OriginalSSN, UpdateDate, UpdateTime, AcademicYearId, ErrorMsg ")
                    .Append(" From syEDExpressExceptionReportDirectLoan_StudentTable Where FileName='" & FileName & "'")
                End With
                Try
                    dsParent = db.RunSQLDataSet(sbParent.ToString)
                    sbParent = sbParent.Remove(0, sbParent.Length)
                    If dsParent.Tables(0).Rows.Count > 0 Then
                        For Each drP As DataRow In dsParent.Tables(0).Rows
                            strRecord = drP("DbIn").ToString & "," & drP("Filter").ToString & "," & drP("AddDate").ToString & "," & drP("AddTime").ToString & "," & drP("LoanAmtApproved").ToString & "," & drP("LoanFeePer").ToString & "," & drP("LoanId").ToString & "," & drP("LoanPeriodEndDate").ToString & "," & drP("LoanPeriodStartDate").ToString & "," & drP("LoanStatus").ToString & "," & drP("LoanType").ToString & "," & drP("MPNStatus").ToString & "," & drP("OriginalSSN").ToString & "," & drP("UpdateDate").ToString & "," & drP("UpdateTime").ToString
                            AcademicYearId = drP("AcademicYearId").ToString
                            If Not strRecord + "" = "" Then
                                myMsgCollection.Add(clsflFile.strMsgType, strRecord, clsflFile.strFileName)
                                myMsgCollection.Items(myMsgCollection.Count - 1).strError = drP("ErrorMsg").ToString
                            End If
                            With sbChild
                                .Append(" Select DbIn, Filter, DisbDate, DisbGrossAmount, DisbIntRebAmount, DisbLoanFeeAmount, DisbNetAdjAmount, DisbNetAmount, DisbNum, DisbSeqNum, DisbStatus_RelInd, DisbType, LoanId, ErrorMsg  ")
                                .Append(" From syEDExpressExceptionReportDirectLoan_DisbursementTable Where FileName='" & FileName & "' and ExceptionReportId='" & drP("ExceptionReportId").ToString & "' order by DisbNum")
                            End With
                            dsChild = db.RunSQLDataSet(sbChild.ToString)
                            For Each drC As DataRow In dsChild.Tables(0).Rows
                                If drC("DbIn").ToString = "M" Then
                                    strRecord = drC("DbIn").ToString & "," & drC("Filter").ToString & "," & drC("DisbDate").ToString & "," & drC("DisbGrossAmount").ToString & "," & drC("DisbIntRebAmount").ToString & "," & drC("DisbLoanFeeAmount").ToString & "," & drC("DisbNetAdjAmount").ToString & "," & drC("DisbNetAmount").ToString & "," & drC("DisbNum").ToString & "," & drC("DisbSeqNum").ToString & "," & drC("DisbStatus_RelInd").ToString & "," & drC("DisbType").ToString & "," & drC("LoanId").ToString
                                Else
                                    strRecord = drC("DbIn").ToString & "," & drC("Filter").ToString & "," & drC("DisbDate").ToString & "," & drC("DisbLoanFeeAmount").ToString & "," & drC("DisbGrossAmount").ToString & "," & drC("DisbIntRebAmount").ToString & "," & drC("LoanId").ToString & "," & drC("DisbNetAmount").ToString & "," & drC("DisbNum").ToString & "," & drC("DisbStatus_RelInd").ToString
                                End If

                                If Not strRecord + "" = "" Then
                                    myMsgCollection.Add(clsflFile.strMsgType, strRecord, clsflFile.strFileName)
                                    myMsgCollection.Items(myMsgCollection.Count - 1).strError = drC("ErrorMsg").ToString
                                End If
                            Next
                            sbChild = sbChild.Remove(0, sbChild.Length)
                        Next
                    Else
                        ProcessEDFile(clsflFile, myMsgCollection, SourceFolderLocation)
                    End If
                    ProcessExceptionEDFile = True
                Catch ex As Exception

                End Try
            End If

        Catch e As ArgumentNullException
            Throw
        Catch e As ArgumentOutOfRangeException
            Throw
        Catch e As System.Exception
            Throw
        End Try

    End Function
    Public Function ProcessEDMsgCollection(ByVal CxnString As String, ByRef myMsgCollection As EDCollectionMessageInfos, Optional ByVal SourcePath As String = "", Optional ByVal TargetPath As String = "", Optional ByVal FileName As String = "", Optional ByVal ExceptionPath As String = "", Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal Override As String = "", Optional ByVal Datafromremotecomputer As String = "no") As String
        Dim rowIndex As Integer
        Dim EDDX As New EDDataXfer
        Dim EDDA As New DataAccess.DataAccess
        Dim blResult As String
        Dim intStartIndex As Integer
        Dim strByPass As String
        Dim strOriginalSSN As String
        Dim strAwardId As String = ""
        Dim intDataExistsInException As Integer
        Dim strType As String = ""
        Dim strDBIndicator As String = ""
        Dim strOrigSSN As String = ""
        Dim strMPNStatus As String = ""
        Dim strFileMessageType As String = ""
        Dim intID As Integer = 1
        'ProcessEDMsgCollection = False
        EDDA.OpenConnection()

        Dim groupTrans As OleDb.OleDbTransaction = EDDA.StartTransaction()
        'EDDA.ConnectionString = CxnString

        Try
            BuilddtPATSData()
            BuilddtPATSDisb()
            BuildDtDirectLoan()
            BuilddtDLSchDisb()
            BuilddtDLActualDisb()
            For rowIndex = 0 To myMsgCollection.Count - 1 Step 1
                strType = myMsgCollection.Items(rowIndex).strMsgType
                strFileMessageType = myMsgCollection.Items(rowIndex).strDatabaseIndicator

                Select Case strType.ToUpper
                    Case "PELL"
                        Select Case strFileMessageType.ToUpper
                            Case "T"
                                'Process Data For Pell ACG, SMART and Teach Main
                                BuildPATSData(myMsgCollection.Items(rowIndex).strOriginalSSN, rowIndex, intID)
                                intID = intID + 1
                            Case "H"
                                'Process Data For Pell ACG, SMART and Teach Main
                                BuildPATSData(myMsgCollection.Items(rowIndex).strOriginalSSN, rowIndex, intID)
                                intID = intID + 1
                            Case "S", "C"
                                'Process Data For Pell ACG, SMART and Teach Detail
                                BuildPATSDisb(myMsgCollection.Items(rowIndex).strOriginalSSN, rowIndex, intID - 1)
                        End Select
                    Case "DL"
                        Select Case strFileMessageType.ToUpper
                            Case "A"
                                ' Do Nothing
                            Case "B"
                                ' Do Nothing
                            Case "D"
                                'Process Data For DL Direct Loan
                                strOrigSSN = myMsgCollection.Items(rowIndex).strOriginalSSN
                                strMPNStatus = myMsgCollection.Items(rowIndex).strMPNStatus
                                BuildDirectLoan(myMsgCollection.Items(rowIndex).strOriginalSSN, rowIndex, intID)
                                intID = intID + 1
                            Case "M"
                                'Process Data For DL Actual Disbusrement
                                myMsgCollection.Items(rowIndex).strOriginalSSN = strOrigSSN
                                myMsgCollection.Items(rowIndex).strMPNStatus = strMPNStatus
                                BuildDLActualDisb(myMsgCollection.Items(rowIndex).strOriginalSSN, rowIndex, intID - 1)

                            Case "N"
                                'Process Data For DL Anticipated Disburesment
                                myMsgCollection.Items(rowIndex).strOriginalSSN = strOrigSSN
                                myMsgCollection.Items(rowIndex).strMPNStatus = strMPNStatus
                                BuildDLSchDisb(myMsgCollection.Items(rowIndex).strOriginalSSN, rowIndex, intID - 1)

                        End Select
                End Select
            Next
            If strType.ToUpper = "PELL" Then
                blResult = Process_PELLMessage(EDDX, EDDA, groupTrans, myMsgCollection, dtPATSData, dtPATSDisb, ExceptionGUID, SourceToTarget, Override)
            Else
                blResult = Process_DLMessage(EDDX, EDDA, groupTrans, myMsgCollection, dtDirectLoan, dtDLSchDisb, dtDLActualDisb, ExceptionGUID, SourceToTarget, Override)
            End If
            If Not blResult.ToUpper.Contains("EXCEPTION") And Trim(SourceToTarget) = "sourcetotarget" Then
                MoveFileToEDArchiveFolder(SourcePath, TargetPath, FileName, Datafromremotecomputer)
                EDDX.DeleteFromExceptionReport(EDDA, FileName, strType)
                Return blResult
                Exit Function
            ElseIf Not blResult.ToUpper.Contains("EXCEPTION") And Trim(SourceToTarget) = "exceptiontotarget" Then
                MoveFileToEDArchiveFolder(SourcePath, TargetPath, FileName, Datafromremotecomputer)
                EDDX.DeleteFromExceptionReport(EDDA, FileName, strType)
                Return blResult
                Exit Function
            ElseIf blResult.ToUpper.Contains("EXCEPTION") And Trim(SourceToTarget) = "sourcetotarget" Then
                MoveFileToEDExceptionFolder(SourcePath, ExceptionPath, FileName, Datafromremotecomputer)
                Return blResult
                Exit Function
            Else
                Return blResult
                Exit Function
            End If

        Catch e As System.Exception
            'groupTrans.Rollback()
            Return blResult
            'Throw
        Finally
            EDDA.CloseConnection()
        End Try
    End Function
    Private Sub RenameExistingFile(ByVal strTargetPathWithFile As String)
        Dim strRandomNumber As New Random
        Dim strTargetPathWithNewFile As String = strTargetPathWithFile + strRandomNumber.Next.ToString
        If File.Exists(strTargetPathWithFile) Then
            Dim fs As FileStream = New FileStream(strTargetPathWithFile, FileMode.Open)
            fs.Close()
            File.Move(strTargetPathWithFile, strTargetPathWithNewFile)
        End If
    End Sub
    Private Sub MoveFileToEDArchiveFolder(ByVal SourcePath As String, ByVal TargetPath As String, ByVal FileName As String, Optional ByVal Datafromremotecomputer As String = "no")
        Dim uriSource As New Uri(SourcePath.ToString.ToLower)
        Dim uriTarget As New Uri(TargetPath.ToString.ToLower)
        Dim strSourceFile As String
        Dim strTargetFile As String
        Dim strMessage As String = ""
        If Datafromremotecomputer = "yes" Then
            strSourceFile = SourcePath + "\" + FileName
            strTargetFile = TargetPath + "\" + FileName
        Else
            strSourceFile = uriSource.AbsolutePath + FileName
            strTargetFile = uriTarget.AbsolutePath + FileName
        End If
        'modified by balaji on 10/11/2007
        'based on input from patrick - files from Fame ESP will have an extension that is sequencial
        'in other words we won't have a case where there will be two HEAD Files (or any other file types)
        'with same extension, the extension will be .000,.001,.002
        'so when that is the case there is no need to rename the file.
        Try
            RenameExistingFile(strTargetFile)
            'Directory.Move(strSourceFile, strTargetFile)
            File.Move(strSourceFile, strTargetFile)
        Catch e As System.Exception
        End Try
    End Sub
    Private Sub MoveFileToEDExceptionFolder(ByVal SourcePath As String, ByVal TargetPath As String, ByVal FileName As String, Optional ByVal DataFromRemoteComputer As String = "no")
        Dim uriSource As New Uri(SourcePath.ToString.ToLower)
        Dim uriTarget As New Uri(TargetPath.ToString.ToLower)
        Dim strSourceFile As String
        Dim strTargetFile As String
        Dim strMessage As String = ""
        If DataFromRemoteComputer = "yes" Then
            strSourceFile = SourcePath + "\" + FileName
            strTargetFile = TargetPath + "\" + FileName
        Else
            strSourceFile = uriSource.AbsolutePath + FileName
            strTargetFile = uriTarget.AbsolutePath + FileName
        End If

        'modified by balaji on 10/11/2007
        'based on input from patrick - files from Fame ESP will have an extension that is sequencial
        'in other words we won't have a case where there will be two HEAD Files (or any other file types)
        'with same extension, the extension will be .000,.001,.002
        'so when that is the case there is no need to rename the file.
        Try
            RenameExistingFile(strTargetFile)
            'Directory.Move(strSourceFile, strTargetFile)
            File.Move(strSourceFile, strTargetFile)
        Catch e As System.Exception
        End Try
    End Sub
    Public Function Process_PELLMessage(ByRef EDDX As EDDataXfer, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDbTransaction, ByRef myMsgCollection As EDCollectionMessageInfos, ByVal dtParent As DataTable, ByVal dtChild As DataTable, Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal override As String = "") As String
        Dim strStudentName As String = ""
        Dim strStudentEnrollmentID As String = ""
        Dim strStudentAwardID As String = ""
        Dim msgEntryParent As EDMessageInfo
        Dim msgEntryChild As EDMessageInfo
        Dim strExceptionMessage, strFileNameOnly, sMessage As String
        Dim strGetAward As String = ""
        Dim strFinalMessage As String = ""
        Dim strErrorMessage As String = ""
        Dim intPostedRecords As Integer = 0
        Dim intPostedDisbRecords As Integer = 0
        Dim intNotPostedRecords As Integer = 0
        Dim intNotPostedDisbRecords As Integer = 0
        Dim drTempRows() As DataRow
        Dim strAwardScheduleId As String = ""
        Dim strFundSourceDesc As String = ""
        Dim strFundSourdeId As String = ""
        Dim strTransactionId As String = ""
        Dim strCampusId As String = ""
        Dim strPmtDisbId As String = ""
        Dim IsTransactionPosted As Boolean = False
        Dim strDBI As String = ""
        Dim blHasException As Boolean = False
        Dim blValidate As Boolean = False
        For Each drP As DataRow In dtParent.Rows
            strStudentName = getStudentNameAndSSN(drP("SSN").ToString)
            msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drP("MCID").ToString))
            strDBI = msgEntryParent.strDatabaseIndicator
            Dim intStartIndex As Integer = InStrRev(msgEntryParent.strFileNameSource, "/")
            strFileNameOnly = msgEntryParent.strFileNameSource.Substring(intStartIndex)
            drTempRows = dtChild.Select("SSN = " + drP("SSN").ToString + " AND RMCID= " + drP("RMCID").ToString)
            blValidate = ValidateAwardId(drP, drTempRows, myMsgCollection)
            If blValidate = True Then
                If Trim(strStudentName) = "" Then
                    blHasException = True
                    sMessage = "Unable to find student with SSN:" & msgEntryParent.strOriginalSSN
                    EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
                    EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
                Else
                    Process_PELLMessage = False
                    Try
                        Dim strEnrollId As String = ""
                        strEnrollId = EDDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntryParent.strOriginalSSN)
                        strCampusId = EDDX.GetStuCampusIDbySSN(db, strCampusId, msgEntryParent.strOriginalSSN)
                        If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
                            sMessage = "Unable to find student with SSN:" & msgEntryParent.strOriginalSSN
                            blHasException = True
                            EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
                            EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
                        Else
                            If Not Trim(strEnrollId) = "" Then
                                Try
                                    strGetAward = EDDX.Add_faStudentAwardsFromPell(groupTrans, db, strStudentAwardID, strEnrollId, msgEntryParent, drTempRows.Length.ToString, strStudentName, SourceToTarget)
                                    EDDX.Check_StudentSchedulePell(db, groupTrans, strGetAward, myMsgCollection, drTempRows)
                                    If Not (strGetAward = "") And Not strGetAward.Contains("Unable") Then
                                        For Each drC As DataRow In drTempRows
                                            strAwardScheduleId = ""
                                            strTransactionId = ""
                                            msgEntryChild = myMsgCollection.Items(Convert.ToInt32(drC("MCID").ToString))
                                            EDDX.Add_faAwardScheduleForPell(db, groupTrans, strAwardScheduleId, strTransactionId, strStudentAwardID, msgEntryChild, strStudentName, SourceToTarget)
                                            Dim strFSDtl = EDDX.GetFundSourceDetails(db, strFundSourceDesc, strFundSourdeId, msgEntryChild.strGrandType, msgEntryChild.strMsgType)
                                            If strFSDtl = "" And Not strFSDtl.Contains("Unable") Then
                                                If msgEntryChild.strDisbursementRealeaseIndicator.ToUpper = "Y" Then
                                                    IsTransactionPosted = EDDX.Check_TransactionPosted(db, groupTrans, strAwardScheduleId, strTransactionId)
                                                    If IsTransactionPosted = False Then
                                                        EDDX.Add_saTransaction(db, groupTrans, strTransactionId, strEnrollId, strCampusId, msgEntryChild, strFundSourceDesc, strStudentName)
                                                        EDDX.Update_faAwardScheduleForPell(db, groupTrans, strAwardScheduleId, strTransactionId)
                                                        EDDX.Add_saPmtDisbRel(db, groupTrans, strPmtDisbId, strTransactionId, strGetAward, strAwardScheduleId, msgEntryChild)
                                                        EDDX.Add_saPayments(db, groupTrans, strTransactionId, msgEntryChild, strStudentName)
                                                    Else
                                                        EDDX.Update_faAwardScheduleForPell(db, groupTrans, strAwardScheduleId, strTransactionId)
                                                    End If
                                                    IsTransactionPosted = False
                                                End If
                                                intPostedDisbRecords += 1
                                            Else
                                                sMessage = strFSDtl
                                                blHasException = True
                                                EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
                                                EDDX.BuildExceptionReport(db, strGetAward, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
                                            End If
                                        Next
                                        intPostedRecords += 1
                                    Else
                                        blHasException = True
                                        EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
                                        EDDX.BuildExceptionReport(db, strGetAward, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
                                    End If
                                Catch ex As System.Exception
                                    sMessage = ex.Message.ToString
                                    EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
                                    EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
                                    blHasException = True
                                End Try
                            Else
                                sMessage = "Unable to match with student " & strStudentName & " enrollment records "
                                blHasException = True
                                EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
                                EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
                            End If
                        End If
                    Catch e As System.Exception
                        sMessage = e.Message.ToString
                        blHasException = True
                        EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
                        EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
                    End Try
                End If
            Else
                blHasException = True
                sMessage = "Award Id in file is not same for student with SSN:" & msgEntryParent.strOriginalSSN
                EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryParent.strMsgType, ExceptionGUID)
                EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "PELL", intNotPostedRecords, intNotPostedDisbRecords, drP, drTempRows, , ExceptionGUID)
            End If
        Next
        If blHasException = True Then
            strFinalMessage = "Exception Total number of records :  " & (intPostedRecords + intPostedDisbRecords + intNotPostedRecords + intNotPostedDisbRecords).ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " awards posted : " & intPostedRecords.ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " awards not posted : " & intNotPostedRecords.ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " Disbursements posted : " & intPostedDisbRecords.ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " Disbursements not posted : " & intNotPostedDisbRecords.ToString
        Else
            strFinalMessage = "Total number of records :  " & (intPostedRecords + intPostedDisbRecords + intNotPostedRecords + intNotPostedDisbRecords).ToString & vbLf & "Number of Pell posted : " & intPostedRecords.ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " awards not posted : " & intNotPostedRecords.ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " Disbursements posted : " & intPostedDisbRecords.ToString & vbLf & "Number of " + IIf(strDBI.ToUpper() = "H", "TEACH", "Pell") + " Disbursements not posted : " & intNotPostedDisbRecords.ToString
        End If
        Return strFinalMessage
    End Function
    Public Function Process_DLMessage(ByRef EDDX As EDDataXfer, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDbTransaction, ByRef myMsgCollection As EDCollectionMessageInfos, ByVal dtDL As DataTable, ByVal dtDLScheduleDisp As DataTable, ByVal dtDLActuleDesp As DataTable, Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal override As String = "") As String
        Dim strStudentName As String = ""
        Dim strStudentEnrollmentID As String = ""
        Dim strStudentAwardID As String = ""
        Dim strFinalMessage As String = ""
        Dim strErrorMessage As String = ""
        Dim intPostedRecords As Integer = 0
        Dim intPostedDisbRecords As Integer = 0
        Dim intNotPostedRecords As Integer = 0
        Dim intNotPostedDisbRecords As Integer = 0
        Dim intActualPostedRecords As Integer = 0
        Dim intNotActualPostedRecords As Integer = 0
        Dim msgEntryDL As EDMessageInfo
        Dim msgEntrySD As EDMessageInfo
        Dim msgEntryAD As EDMessageInfo
        Dim strExceptionMessage, strFileNameOnly, sMessage As String
        Dim strGetAward As String = ""
        Dim drTempRowsSD() As DataRow
        Dim drTempRowsAD() As DataRow
        Dim strAwardScheduleId As String = ""
        Dim IsTransactionPosted As Boolean = False
        Dim strFundSourceDesc As String = ""
        Dim strFundSourdeId As String = ""
        Dim strTransactionId As String = ""
        Dim strCampusId As String = ""
        Dim strPmtDisbId As String = ""
        Dim blHasException As Boolean = False
        Dim blValidate As Boolean = False
        For Each drDL As DataRow In dtDL.Rows
            strStudentName = getStudentNameAndSSN(drDL("SSN").ToString)
            msgEntryDL = myMsgCollection.Items(Convert.ToInt32(drDL("MCID").ToString))
            Dim intStartIndex As Integer = InStrRev(msgEntryDL.strFileNameSource, "/")
            strFileNameOnly = msgEntryDL.strFileNameSource.Substring(intStartIndex)
            drTempRowsSD = dtDLScheduleDisp.Select("SSN = " + drDL("SSN").ToString + " AND RMCID= " + drDL("RMCID").ToString)
            drTempRowsAD = dtDLActualDisb.Select("SSN = " + drDL("SSN").ToString + " AND RMCID= " + drDL("RMCID").ToString)
            blValidate = ValidateLoanId(drDL, drTempRowsSD, drTempRowsAD, myMsgCollection)
            If blValidate = True Then
                If Trim(strStudentName) = "" Then
                    sMessage = "Unable to find student with SSN:" & msgEntryDL.strOriginalSSN
                    EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
                    EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, drTempRowsSD, drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
                    blHasException = True
                Else
                    Process_DLMessage = False
                    Try
                        Dim strEnrollId As String = ""
                        strEnrollId = EDDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntryDL.strOriginalSSN)
                        strCampusId = EDDX.GetStuCampusIDbySSN(db, strCampusId, msgEntryDL.strOriginalSSN)
                        If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
                            sMessage = "Unable to find student with SSN:" & msgEntryDL.strOriginalSSN
                            EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
                            EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, drTempRowsSD, drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
                            blHasException = True
                        End If
                        If Not Trim(strEnrollId) = "" Then
                            Try
                                strGetAward = EDDX.Add_faStudentAwardsFromDL(groupTrans, db, strStudentAwardID, strEnrollId, msgEntryDL, IIf(drTempRowsSD.Length > 0, drTempRowsSD.Length.ToString, 0), strStudentName, SourceToTarget)
                                EDDX.Check_StudentScheduleDL(db, groupTrans, strGetAward, myMsgCollection, drTempRowsSD)
                                If Not (strGetAward = "") And Not strGetAward.Contains("Unable") Then
                                    For Each drSD As DataRow In drTempRowsSD
                                        msgEntrySD = myMsgCollection.Items(Convert.ToInt32(drSD("MCID").ToString))
                                        If msgEntrySD.blnIsParsed Then
                                            EDDX.Add_faAwardScheduleForDL(db, groupTrans, strAwardScheduleId, strTransactionId, strStudentAwardID, msgEntrySD, strStudentName, SourceToTarget)
                                            intPostedDisbRecords += 1
                                        End If
                                    Next
                                    For Each drAD As DataRow In drTempRowsAD
                                        msgEntryAD = myMsgCollection.Items(Convert.ToInt32(drAD("MCID").ToString))
                                        If msgEntryAD.blnIsParsed Then
                                            strAwardScheduleId = ""
                                            strTransactionId = ""
                                            EDDX.Add_faAwardScheduleForDL(db, groupTrans, strAwardScheduleId, strTransactionId, strStudentAwardID, msgEntryAD, strStudentName, SourceToTarget)
                                            Dim strFSDtl = EDDX.GetFundSourceDetails(db, strFundSourceDesc, strFundSourdeId, msgEntryAD.strActualDisbursementType, msgEntryAD.strMsgType)
                                            If strFSDtl = "" And Not strFSDtl.Contains("Unable") Then
                                                'If msgEntryAD.strActualDisbursementStatus.ToUpper = "R" Or msgEntryAD.strActualDisbursementStatus.ToUpper = "Y" Then
                                                IsTransactionPosted = EDDX.Check_TransactionPosted(db, groupTrans, strAwardScheduleId, strTransactionId)
                                                If IsTransactionPosted = False Then
                                                    EDDX.Add_saTransaction(db, groupTrans, strTransactionId, strEnrollId, strCampusId, msgEntryAD, strFundSourceDesc, strStudentName)
                                                    EDDX.Update_faAwardScheduleForDL(db, groupTrans, strAwardScheduleId, strTransactionId)
                                                    EDDX.Add_saPmtDisbRel(db, groupTrans, strPmtDisbId, strTransactionId, strGetAward, strAwardScheduleId, msgEntryAD)
                                                    EDDX.Add_saPayments(db, groupTrans, strTransactionId, msgEntryAD, strStudentName)
                                                    'End If
                                                    'intPostedDisbRecords += 1
                                                Else
                                                    EDDX.Update_faAwardScheduleForDL(db, groupTrans, strAwardScheduleId, strTransactionId)
                                                End If


                                                IsTransactionPosted = False
                                                intActualPostedRecords += 1
                                            Else
                                                blHasException = True
                                                sMessage = strFSDtl
                                                EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
                                                EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, , drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
                                                Exit For
                                            End If
                                        End If
                                    Next
                                    intPostedRecords += 1
                                Else
                                    EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
                                    EDDX.BuildExceptionReport(db, strGetAward, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, drTempRowsSD, drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
                                    blHasException = True
                                End If
                            Catch ex As System.Exception
                                sMessage = ex.Message.ToString
                                EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
                                EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, drTempRowsSD, drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
                            End Try
                        Else
                            sMessage = "Unable to match data with student " & strStudentName & " enrollment records "
                            EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
                            EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, drTempRowsSD, drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
                            blHasException = True
                        End If
                    Catch e As System.Exception
                        sMessage = e.Message.ToString
                        EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
                        EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, drTempRowsSD, drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
                        blHasException = True
                    End Try
                End If
            Else
                sMessage = "Loan Id in the file is not same for student with SSN:" & msgEntryDL.strOriginalSSN
                EDDX.DeleteFromExceptionReport(db, strFileNameOnly, msgEntryDL.strMsgType, ExceptionGUID)
                EDDX.BuildExceptionReport(db, sMessage, strFileNameOnly, myMsgCollection, "DL", intNotPostedRecords, intNotPostedDisbRecords, drDL, drTempRowsSD, drTempRowsAD, ExceptionGUID, intNotActualPostedRecords)
                blHasException = True
            End If
        Next
        If blHasException = True Then
            strFinalMessage = "Exception Total number of records :  " & (intPostedRecords + intPostedDisbRecords + intNotPostedRecords + intNotPostedDisbRecords + intActualPostedRecords + intNotActualPostedRecords).ToString & vbLf & "Number of Direct Loan posted : " & intPostedRecords.ToString & vbLf & "Number of Direct Loan not posted : " & intNotPostedRecords.ToString & vbLf & "Number of Direct Loan Disbursements posted : " & intPostedDisbRecords.ToString & vbLf & "Number of Direct Loan Disbursements not posted : " & intNotPostedDisbRecords.ToString & vbLf & "Number of Direct Loan Actual Disbursements posted : " & intActualPostedRecords.ToString & vbLf & "Number of Direct Loan Actual Disbursements not posted : " & intNotActualPostedRecords.ToString
        Else
            strFinalMessage = "Total number of records :  " & (intPostedRecords + intPostedDisbRecords + intNotPostedRecords + intNotPostedDisbRecords + intActualPostedRecords + intNotActualPostedRecords).ToString & vbLf & "Number of Direct Loan posted : " & intPostedRecords.ToString & vbLf & "Number of Direct Loan not posted : " & intNotPostedRecords.ToString & vbLf & "Number of Direct Loan Disbursements posted : " & intPostedDisbRecords.ToString & vbLf & "Number of Direct Loan Disbursements not posted : " & intNotPostedDisbRecords.ToString & vbLf & "Number of Direct Loan Actual Disbursements posted : " & intActualPostedRecords.ToString & vbLf & "Number of Direct Loan Actual Disbursements not posted : " & intNotActualPostedRecords.ToString
        End If
        Return strFinalMessage
    End Function
    Public Function getStudentNameAndSSN(ByVal SSN As String) As String
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        Dim zipMask As String
        Dim strMaskSSN As String
        Dim dr As DataRow
        Dim strStudentName As String = (New EDDataXfer).GetStudentInfo(SSN)
        If InStr(strStudentName, "Student not found") >= 1 Then
            Return ""
            Exit Function
        End If
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        If SSN.ToString.Length >= 1 Then
            Return strStudentName & " (" + SSN & ")"
        Else
            Return ""
        End If
    End Function
    Private Sub BuildDtDirectLoan()
        Try
            Dim dcSSN As New DataColumn("SSN", GetType(String))
            Dim dcMCID As New DataColumn("MCID", GetType(Integer))
            Dim dcRMCID As New DataColumn("RMCID", GetType(Integer))

            dtDirectLoan.Columns.Add(dcSSN)
            dtDirectLoan.Columns.Add(dcMCID)
            dtDirectLoan.Columns.Add(dcRMCID)
        Catch ex As System.Exception
        End Try
    End Sub
    Private Sub BuildDirectLoan(ByVal strSSN As String, ByVal intMCID As String, ByVal intRMCID As String)
        If Not strSSN = "" Then
            Dim drDirectLoan As DataRow
            drDirectLoan = dtDirectLoan.NewRow()
            drDirectLoan("SSN") = strSSN
            drDirectLoan("MCID") = intMCID
            drDirectLoan("RMCID") = intRMCID
            dtDirectLoan.Rows.Add(drDirectLoan)
        End If
    End Sub
    Private Sub BuilddtPATSData()
        Try
            Dim dcSSN As New DataColumn("SSN", GetType(String))
            Dim dcMCID As New DataColumn("MCID", GetType(Integer))
            Dim dcRMCID As New DataColumn("RMCID", GetType(Integer))

            dtPATSData.Columns.Add(dcSSN)
            dtPATSData.Columns.Add(dcMCID)
            dtPATSData.Columns.Add(dcRMCID)
        Catch ex As System.Exception
        End Try
    End Sub
    Private Sub BuildPATSData(ByVal strSSN As String, ByVal intMCID As String, ByVal intRMCID As String)
        If Not strSSN = "" Then
            Dim drPATS As DataRow
            drPATS = dtPATSData.NewRow()
            drPATS("SSN") = strSSN
            drPATS("MCID") = intMCID
            drPATS("RMCID") = intRMCID
            dtPATSData.Rows.Add(drPATS)
        End If
    End Sub
    Private Sub BuilddtPATSDisb()
        Try
            Dim dcSSN As New DataColumn("SSN", GetType(String))
            Dim dcMCID As New DataColumn("MCID", GetType(Integer))
            Dim dcRMCID As New DataColumn("RMCID", GetType(Integer))

            dtPATSDisb.Columns.Add(dcSSN)
            dtPATSDisb.Columns.Add(dcMCID)
            dtPATSDisb.Columns.Add(dcRMCID)
        Catch ex As System.Exception
        End Try
    End Sub
    Private Sub BuildPATSDisb(ByVal strSSN As String, ByVal intMCID As String, ByVal intRMCID As String)
        If Not strSSN = "" Then
            Dim drPATSDisb As DataRow
            drPATSDisb = dtPATSDisb.NewRow()
            drPATSDisb("SSN") = strSSN
            drPATSDisb("MCID") = intMCID
            drPATSDisb("RMCID") = intRMCID
            dtPATSDisb.Rows.Add(drPATSDisb)
        End If
    End Sub
    Private Sub BuilddtDLSchDisb()
        Try
            Dim dcSSN As New DataColumn("SSN", GetType(String))
            Dim dcMCID As New DataColumn("MCID", GetType(Integer))
            Dim dcRMCID As New DataColumn("RMCID", GetType(Integer))

            dtDLSchDisb.Columns.Add(dcSSN)
            dtDLSchDisb.Columns.Add(dcMCID)
            dtDLSchDisb.Columns.Add(dcRMCID)
        Catch ex As System.Exception
        End Try
    End Sub
    Private Sub BuildDLSchDisb(ByVal strSSN As String, ByVal intMCID As String, ByVal intRMCID As String)
        If Not strSSN = "" Then
            Dim drDLSchDisb As DataRow
            drDLSchDisb = dtDLSchDisb.NewRow()
            drDLSchDisb("SSN") = strSSN
            drDLSchDisb("MCID") = intMCID
            drDLSchDisb("RMCID") = intRMCID
            dtDLSchDisb.Rows.Add(drDLSchDisb)
        End If
    End Sub
    Private Sub BuilddtDLActualDisb()
        Try
            Dim dcSSN As New DataColumn("SSN", GetType(String))
            Dim dcMCID As New DataColumn("MCID", GetType(Integer))
            Dim dcRMCID As New DataColumn("RMCID", GetType(Integer))

            dtDLActualDisb.Columns.Add(dcSSN)
            dtDLActualDisb.Columns.Add(dcMCID)
            dtDLActualDisb.Columns.Add(dcRMCID)
        Catch ex As System.Exception
        End Try
    End Sub
    Private Sub BuildDLActualDisb(ByVal strSSN As String, ByVal intMCID As String, ByVal intRMCID As String)
        If Not strSSN = "" Then
            Dim drDLActualDisb As DataRow
            drDLActualDisb = dtDLActualDisb.NewRow()
            drDLActualDisb("SSN") = strSSN
            drDLActualDisb("MCID") = intMCID
            drDLActualDisb("RMCID") = intRMCID
            dtDLActualDisb.Rows.Add(drDLActualDisb)
        End If
    End Sub
    Private Function GetStudentData(ByVal dr As DataRow, ByVal OriginalSSN As String) As Boolean
        Dim db As New DataAccess.DataAccess
        Dim strSQL As String = "SELECT S.LastName, S.FirstName, S.SSN FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId Where S.SSN='" + OriginalSSN + "' AND SC.SysStatusId=9 "
        Dim dsTemp As DataSet = db.RunSQLDataSet(strSQL, "StudentInfo")
        If dsTemp.Tables("StudentInfo").Rows.Count > 0 Then
            dr("LastName") = dsTemp.Tables("StudentInfo").Rows(0)("LastName").ToString
            dr("FirstName") = dsTemp.Tables("StudentInfo").Rows(0)("FirstName").ToString
            If OriginalSSN.Length = 9 Then
                dr("SSN") = FormatSSN(OriginalSSN)
            Else
                dr("SSN") = OriginalSSN
            End If
            Return True
        Else
            dr("LastName") = ""
            dr("FirstName") = ""
            If OriginalSSN.Length = 9 Then
                dr("SSN") = FormatSSN(OriginalSSN)
            Else
                dr("SSN") = OriginalSSN
            End If

            Return False
        End If
    End Function
    Public Function FormatDate(ByRef DateToFormat As String) As String
        Dim strDD As String
        Dim strMM As String
        Dim strYYYY As String
        Try
            strYYYY = DateToFormat.Substring(0, 4)
            strMM = DateToFormat.Substring(4, 2)
            strDD = DateToFormat.Substring(6, 2)
            Return strMM + "/" + strDD + "/" + strYYYY
        Catch ex As Exception
            Return DateToFormat
        End Try
    End Function
    Public Function FormatTime(ByRef TimeToFormat As String) As String
        Dim strHH As String
        Dim strMM As String
        Dim strSS As String
        Try
            strHH = TimeToFormat.Substring(0, 2)
            strMM = TimeToFormat.Substring(2, 2)
            strSS = TimeToFormat.Substring(4, 2)
            Return strHH + ":" + strMM + ":" + strSS
        Catch ex As Exception
            Return TimeToFormat
        End Try
    End Function
    Public Function FormatSSN(ByRef SSNToFormat As String) As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        Return facInputMasks.ApplyMask(strSSNMask, "*****" & SSNToFormat.Substring(5))
    End Function
    Public Function getExceptionReport(ByVal ExceptionGUID As String, ByVal MsgType As String) As DataSet
        Dim ds As New DataSet
        ds = (New EDDataXfer).getExceptionReport(ExceptionGUID, MsgType)
        Return ds
    End Function
    Public Function ValidateAwardId(ByVal drTempParent As DataRow, ByVal drTempChild() As DataRow, ByVal myMsgCollection As EDCollectionMessageInfos) As Boolean
        Dim msgEntryParent As EDMessageInfo
        Dim msgEntryChild As EDMessageInfo
        msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drTempParent("MCID").ToString))
        For Each dr As DataRow In drTempChild
            msgEntryChild = myMsgCollection.Items(Convert.ToInt32(dr("MCID").ToString))
            If Not msgEntryParent.strAwardId = msgEntryChild.strAwardId Then
                Return False
            End If
        Next
        Return True
    End Function
    Public Function ValidateLoanId(ByVal drTempParent As DataRow, ByVal drTempSD() As DataRow, ByVal drTempAD() As DataRow, ByVal myMsgCollection As EDCollectionMessageInfos) As Boolean
        Dim msgEntryParent As EDMessageInfo
        Dim msgEntrySD As EDMessageInfo
        Dim msgEntryAD As EDMessageInfo
        msgEntryParent = myMsgCollection.Items(Convert.ToInt32(drTempParent("MCID").ToString))
        For Each drSD As DataRow In drTempSD
            msgEntrySD = myMsgCollection.Items(Convert.ToInt32(drSD("MCID").ToString))
            If msgEntrySD.blnIsParsed Then
                If Not msgEntryParent.strLoanId = msgEntrySD.strLoanId Then
                    Return False
                End If
            End If
        Next
        For Each drAD As DataRow In drTempSD
            msgEntryAD = myMsgCollection.Items(Convert.ToInt32(drAD("MCID").ToString))
            If msgEntryAD.blnIsParsed Then
                If Not msgEntryParent.strLoanId = msgEntryAD.strLoanId Then
                    Return False
                End If
            End If
        Next
        Return True
    End Function
End Class
