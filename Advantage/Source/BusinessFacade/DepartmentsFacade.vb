' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' DepartmentsFacade.vb
'
' DepartmentsFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class DepartmentsFacade
    Public Function GetAllDepartments() As DataSet

        '   Instantiate DAL component
        Dim departDB As New DepartmentsDB

        '   get the dataset with all degrees
        Return departDB.GetAllDepartments()

    End Function

    Public Function GetAllARDepartments() As DataSet

        '   Instantiate DAL component
        Dim departDB As New DepartmentsDB

        '   get the dataset with all degrees
        Return departDB.GetAllARDepartments()

    End Function
End Class
