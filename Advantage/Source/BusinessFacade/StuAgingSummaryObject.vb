Public Class StuAgingSummaryObject
    Inherits BaseReportFacade

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim stuAging As New StudentAgingDB
        Dim ds As DataSet
        ' Massage dataset to produce one with the columns and data expected by the report.
        ds = BuildReportSource(stuAging.GetStudentCharges(rptParamInfo), stuAging.StudentIdentifier)
        ' The Student Aging Summary report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim stuName As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String
        Dim periodRange As Byte

        'Get the mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            For Each dr As DataRow In ds.Tables(0).Rows
                'Apply summary of payments to summary of charges.
                If Not (dr("PymtOver120Day") Is System.DBNull.Value) Then
                    periodRange = 5
                    ApplyPayment(dr, dr("PymtOver120Day"), periodRange)
                End If
                If Not (dr("Pymt91To120Day") Is System.DBNull.Value) Then
                    periodRange = 4
                    ApplyPayment(dr, dr("Pymt91To120Day"), periodRange)
                End If
                If Not (dr("Pymt61To90Day") Is System.DBNull.Value) Then
                    periodRange = 3
                    ApplyPayment(dr, dr("Pymt61To90Day"), periodRange)
                End If
                If Not (dr("Pymt31To60Day") Is System.DBNull.Value) Then
                    periodRange = 2
                    ApplyPayment(dr, dr("Pymt31To60Day"), periodRange)
                End If
                If Not (dr("CurrentPymt") Is System.DBNull.Value) Then
                    periodRange = 1
                    ApplyPayment(dr, dr("CurrentPymt"), periodRange)
                End If
                '
                'Set up student name as: "LastName, FirstName MI."
                stuName = dr("LastName")
                If Not (dr("FirstName") Is System.DBNull.Value) Then
                    If dr("FirstName") <> "" Then
                        stuName &= ", " & dr("FirstName")
                    End If
                End If
                If Not (dr("MiddleName") Is System.DBNull.Value) Then
                    If dr("MiddleName") <> "" Then
                        stuName &= " " & dr("MiddleName") & "."
                    End If
                End If
                dr("StudentName") = stuName
                '
                'Apply mask to SSN.
                If StudentIdentifier = "SSN" Then
                    If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                        If dr("StudentIdentifier") <> "" And dr("StudentIdentifier") <> "N/A" And dr("StudentIdentifier") <> Nothing Then
                            Dim temp As String = dr("StudentIdentifier")
                            dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                        End If
                    End If
                End If
            Next

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

    Private Sub ApplyPayment(ByRef dr As DataRow, ByVal payment As Decimal, ByVal periodRange As Byte)

        If Not (dr Is Nothing) Then
            If Not (dr("BalOver120Day") Is System.DBNull.Value) And payment <> 0 Then
                If dr("BalOver120Day") > 0 Then
                    If payment <> 0 Then
                        dr("BalOver120Day") += payment
                        If dr("BalOver120Day") < 0 Then
                            'Abs(payment) > dr("BalOver120Day")
                            payment = dr("BalOver120Day")
                            dr("BalOver120Day") = 0
                        Else
                            payment = 0
                        End If
                    End If
                End If
            End If
            If Not (dr("Bal91To120Day") Is System.DBNull.Value) And payment <> 0 Then
                If dr("Bal91To120Day") > 0 Then
                    dr("Bal91To120Day") += payment
                    If dr("Bal91To120Day") < 0 Then
                        'Abs(payment) > dr("Bal91To120Day")
                        payment = dr("Bal91To120Day")
                        dr("Bal91To120Day") = 0
                    Else
                        payment = 0
                    End If
                End If
            End If
            If Not (dr("Bal61To90Day") Is System.DBNull.Value) And payment <> 0 Then
                If dr("Bal61To90Day") > 0 Then
                    dr("Bal61To90Day") += payment
                    If dr("Bal61To90Day") < 0 Then
                        'Abs(payment) > dr("Bal61To90Day")
                        payment = dr("Bal61To90Day")
                        dr("Bal61To90Day") = 0
                    Else
                        payment = 0
                    End If
                End If
            End If
            If Not (dr("Bal31To60Day") Is System.DBNull.Value) And payment <> 0 Then
                If dr("Bal31To60Day") > 0 Then
                    dr("Bal31To60Day") += payment
                    If dr("Bal31To60Day") < 0 Then
                        'Abs(payment) > dr("Bal31To60Day")
                        payment = dr("Bal31To60Day")
                        dr("Bal31To60Day") = 0
                    Else
                        payment = 0
                    End If
                End If
            End If
            If Not (dr("CurrentBal") Is System.DBNull.Value) Then
                If dr("CurrentBal") > 0 Then
                    dr("CurrentBal") += payment
                    If dr("CurrentBal") < 0 Then
                        'Abs(payment) > dr("CurrentBal")
                        payment = dr("CurrentBal")
                        dr("CurrentBal") = 0
                    Else
                        payment = 0
                    End If
                End If
            End If
            'Place remaining Payment into corresponding date-range column.
            If payment <> 0 Then
                Select Case periodRange
                    Case 1
                        If Not (dr("CurrentBal") Is System.DBNull.Value) Then
                            dr("CurrentBal") += payment
                        Else
                            dr("CurrentBal") = payment
                        End If
                    Case 2
                        If Not (dr("Bal31To60Day") Is System.DBNull.Value) Then
                            dr("Bal31To60Day") += payment
                        Else
                            dr("Bal31To60Day") = payment
                        End If
                    Case 3
                        If Not (dr("Bal61To90Day") Is System.DBNull.Value) Then
                            dr("Bal61To90Day") += payment
                        Else
                            dr("Bal61To90Day") = payment
                        End If
                    Case 4
                        If Not (dr("Bal91To120Day") Is System.DBNull.Value) Then
                            dr("Bal91To120Day") += payment
                        Else
                            dr("Bal91To120Day") = payment
                        End If
                    Case 5
                        If Not (dr("BalOver120Day") Is System.DBNull.Value) Then
                            dr("BalOver120Day") += payment
                        Else
                            dr("BalOver120Day") = payment
                        End If
                End Select
            End If
        End If
    End Sub

#End Region


End Class
