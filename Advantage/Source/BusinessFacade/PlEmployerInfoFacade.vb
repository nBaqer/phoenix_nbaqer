'=================================================
'FAME.AdvantageV1.BusinessFacade
'
'plEmployerInfoFacade.vb
'
'Placement EmployerInfo Service Interface
'
'=================================================
'CopyRight (c) 2003-2004 FAME Inc.
'
'All Rights Reserved
'=================================================

Public Class PlEmployerInfoFacade
    Public Function UpdateEmployerInfo(ByVal employerinfo As plEmployerInfo, ByVal user As String) As String

        With New PlEmployerInfoDB
            If (employerinfo.IsInDB = False) Then
                Return .AddEmployerInfo(employerinfo, user)
            Else
                Return .UpdateEmployerInfo(employerinfo, user)
            End If
        End With
    End Function
    Public Function UpdateEmployerJobCats(ByVal empId As String, ByVal user As String, ByVal selectedCertifications() As String) As Integer

        '   Instantiate DAL component
        Dim employerJobsDB As New PlEmployerInfoDB

        '   return query results
        Return employerJobsDB.UpdateEmployerJobCats(empId, user, selectedCertifications)

    End Function

    Public Function DeleteEmployerInfo(ByVal EmployerId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New PlEmployerInfoDB

            '   delete EmployerInfo ans return integer result
            Return .DeleteEmployerInfo(EmployerId, modDate)

        End With

    End Function

    Public Function GetEmployerInfo(ByVal employerid As String) As plEmployerInfo
        '   Instantiate DAL component
        With New PlEmployerInfoDB

            '   get the BankInfo
            Return .GetEmployerInfo(employerid)

        End With

    End Function

    Public Function GetJobsOffered(ByVal empId As String) As DataSet

        '   Instantiate DAL component
        Dim empDB As New PlEmployerInfoDB

        '   return Dataset
        Return empDB.GetJobsOffered(empId)

    End Function
    Public Function GetValidJobsOffered(ByVal empId As String) As Integer

        '   Instantiate DAL component
        Dim empDB As New PlEmployerInfoDB

        '   return Dataset
        Return empDB.GetValidJobsOffered(empId)

    End Function

    Public Function GetAllTitles() As DataSet

        '   Instantiate DAL component
        Dim TitlesDB As New PlEmployerInfoDB

        '   get the dataset with all Suffixes
        Return TitlesDB.GetAllTitles()

    End Function


End Class
