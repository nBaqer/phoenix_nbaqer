Option Explicit On

Imports System.Security.Principal
Imports System.Web
Imports System.Security.Cryptography
Imports System.Web.Security

Public Class UserSecurityFacade

    Public Function GetRolesForUser(ByVal UserSecurityInfo As UserSecurityInfo) As UserSecurityInfo
        Dim RoleDs As New DataSet
        Dim UserSecurityDB As New UserSecurityDB
        Dim ArrayCount As Int32 = 0
        Dim dt As DataTable
        Dim row As DataRow

        UserSecurityInfo = InstantiateCredentials(UserSecurityInfo)
        RoleDs = UserSecurityDB.GetRolesFromDatabase(UserSecurityInfo)

        dt = RoleDs.Tables("RoleList")
        'With dt
        '    .PrimaryKey = New DataColumn() {.Columns("RoleId")}
        'End With

        If dt.Rows.Count > 0 Or UserSecurityInfo.UserName = "sa" Then

            For Each row In dt.Rows
                UserSecurityInfo.UserRoles(ArrayCount) = row("RoleId").ToString
                ArrayCount += 1
            Next
            UserSecurityInfo.ResizeArray(ArrayCount - 1)
            ' Loop through RoleList to populate the Role List for the user
            UserSecurityInfo.UserRoleArrayNumber = ArrayCount
            ' Save the roles to session for the user
            UserSecurityInfo = GetUserEmpId(UserSecurityInfo)

            UserSecurityInfo.Roles = dt
            SaveState(UserSecurityInfo)
        Else
            UserSecurityInfo.UserRoleArrayNumber = 0
            UserSecurityInfo.ResizeArray(0)
        End If

        Return UserSecurityInfo
    End Function

    Public Function GetAllRoles() As DataTable
        Dim userSecuritDB As New UserSecurityDB
        Return userSecuritDB.GetAllRoles

    End Function
    Public Function GetCampusGroupsByUser(ByVal UserId As String) As DataTable
        Dim userSecuritDB As New UserSecurityDB
        Return userSecuritDB.GetCampusGroupsByUser(UserId)
    End Function
    Public Function GetCampusAndRolesByCampusGroups(ByVal UserId As String, ByVal CampGrpId As String) As DataSet
        Dim userSecuritDB As New UserSecurityDB
        Return userSecuritDB.GetCampusAndRolesByCampusGroups(UserId, CampGrpId)
    End Function
    Public Function GetAllUserRoles(ByVal UserId As String) As DataTable
        Dim userSecuritDB As New UserSecurityDB
        Return userSecuritDB.GetUserRolesForAllCampGrps(UserId)

    End Function
    Public Function GetRolesByCampusGroups(ByVal UserId As String, ByVal CampGrpId As String) As DataSet
        Dim userSecuritDB As New UserSecurityDB
        Return userSecuritDB.GetRolesByCampusGroups(UserId, CampGrpId)

    End Function
    Public Function GetUserEmpId(ByVal UserSecurityInfo As UserSecurityInfo) As UserSecurityInfo
        Dim dt As New DataTable
        Dim UserSecurityDB As New UserSecurityDB
        dt = UserSecurityDB.GetUserEmpId(UserSecurityInfo)
        Dim row As DataRow
        If dt.Rows.Count > 0 Then
            row = dt.Rows(0)
            UserSecurityInfo.EmpId = row("EmpId")
        Else
            UserSecurityInfo.EmpId = System.Guid.Empty
        End If
        Return UserSecurityInfo
    End Function

    Public Function InstantiateCredentials(ByVal UserSecurityInfo As UserSecurityInfo) As UserSecurityInfo
        ' Create the Generic Identity and Generic Principal objects for the current user
        Try
            UserSecurityInfo.UserSecurity = New GenericIdentity(UserSecurityInfo.UserName)
            UserSecurityInfo.UserPrincipal = New GenericPrincipal(UserSecurityInfo.UserSecurity, UserSecurityInfo.UserRoles)
            Return UserSecurityInfo
        Catch ExceptionObject As System.Exception
            'LogException(ExceptionObject)
            Throw New System.Exception("An error occurred setting credentials")
        End Try

    End Function

    Private Sub SaveState(ByVal UserSecurityInfo As UserSecurityInfo)
        Dim m_context As HttpContext
        m_context = HttpContext.Current

        If Not IsNothing(m_context) Then
            m_context.Session("UserName") = UserSecurityInfo.UserName
            m_context.Session("Roles") = UserSecurityInfo.UserRoles
            m_context.Session("RolesDT") = UserSecurityInfo.Roles
            m_context.Session("EmpId") = UserSecurityInfo.EmpId.ToString
            m_context.Session("UserId") = UserSecurityInfo.UserId
        End If
    End Sub

    Public Function Login(ByVal UserSecurityInfo As UserSecurityInfo) As Int32
        Dim UserSecurityDB As New UserSecurityDB
        Dim LoginStatus As Int32
        Dim userId As String

        LoginStatus = UserSecurityDB.VerifyUserNamePassword(UserSecurityInfo)
        If LoginStatus = 0 Then
            'Get the user id of the user
            userId = GetUserId(UserSecurityInfo.UserName, UserSecurityInfo.UserPassword)
            UserSecurityInfo.UserId = userId
            ' Log in the user, get his roles
            UserSecurityInfo = GetRolesForUser(UserSecurityInfo)
            If UserSecurityInfo.UserRoleArrayNumber = 0 And UserSecurityInfo.UserName <> "sa" Then
                LoginStatus = 3
            End If
            Return LoginStatus
        Else
            Return LoginStatus
        End If
    End Function
    Public Function Logout()
        Dim UserSecurityInfo As New UserSecurityInfo
        UserSecurityInfo.Context = HttpContext.Current
        UserSecurityInfo.Context.Session("UserName") = ""
        UserSecurityInfo.Context.Session("Roles") = ""
        UserSecurityInfo.Context.Session("EmpId") = ""
    End Function

    Public Function GetAccessLevels(ByVal ResourceId As Int32) As UserPagePermissionInfo
        ' Checks the maximum access level that the user has for a specific resource id
        Dim ds As New DataSet
        Dim intCount As Integer
        Dim UserSecurityDB As New UserSecurityDB
        ds = UserSecurityDB.GetAccessByRoleResource(ResourceId)
        intCount = ds.Tables("ResourceRoleList").Rows.Count
        Dim UserPagePermissionInfo As New UserPagePermissionInfo
        UserPagePermissionInfo.ResourceId = ResourceId
        If intCount = 0 Then
            Return UserPagePermissionInfo
        Else
            Dim UserSecurityInfo As New UserSecurityInfo
            UserSecurityInfo.Context = HttpContext.Current
            UserSecurityInfo.UserName = UserSecurityInfo.Context.Session("UserName")
            UserSecurityInfo.UserRoles = UserSecurityInfo.Context.Session("Roles")
            Dim Row As DataRow
            For Each Row In ds.Tables("ResourceRoleList").Rows
                Dim localValidRole As Boolean = False
                localValidRole = UserSecurityInfo.UserPrincipal.IsInRole(Row("RoleID").ToString())
                If localValidRole = True Then
                    Dim accessLevel As Integer = CType(Row("AccessLevel"), Integer)
                    'this is "Full" permissions
                    If accessLevel = 15 Then UserPagePermissionInfo.HasFull = True

                    'this is "Edit" permissions
                    If accessLevel - 8 >= 0 Then
                        UserPagePermissionInfo.HasEdit = True
                        accessLevel -= 8
                    End If

                    'this is "Add" permissions
                    If accessLevel - 4 >= 0 Then
                        UserPagePermissionInfo.HasAdd = True
                        accessLevel -= 4
                    End If

                    'this is "Delete" permissions
                    If accessLevel - 2 >= 0 Then
                        UserPagePermissionInfo.HasDelete = True
                        accessLevel -= 2
                    End If

                    'this is "Display" permissions
                    If accessLevel - 1 = 0 Then
                        UserPagePermissionInfo.HasDisplay = True
                    End If
                End If
            Next
            Return UserPagePermissionInfo
        End If
    End Function

    Public Function GetRoleAccessLevels(ByVal roleID As String) As DataTable
        Dim dbUserSecurity As New UserSecurityDB
        Return dbUserSecurity.GetRoleAccessLevels(roleID)
    End Function

    Public Sub DeleteRoleResourceAccessLevels(ByVal roleId As String, ByVal rtypeId As Integer)
        Dim dbUserSecurity As New UserSecurityDB

        dbUserSecurity.DeleteRoleResourceAccessLevels(roleId, rtypeId)

    End Sub

    Public Sub AddRoleResourceAccessLevel(ByVal roleId As String, ByVal resourceId As Integer, ByVal accessLevel As Integer, ByVal user As String)
        Dim dbUserSecurity As New UserSecurityDB

        dbUserSecurity.AddRoleResourceAccessLevel(roleId, resourceId, accessLevel, user)

    End Sub

    ' this section is for password salting etc.
    Private Function CreateSalt(ByVal size As Int32) As String
        Dim rnd As RNGCryptoServiceProvider = New RNGCryptoServiceProvider
        Dim buff() As Byte = New Byte(size) {}
        rnd.GetBytes(buff)
        Return Convert.ToBase64String(buff)

    End Function
    Private Function CreatePasswordHash(ByVal pwd As String, ByVal salt As String) As String
        Dim saltAndPwd As String = String.Concat(pwd, salt)
        Dim hashedPwd As String = FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPwd, "md5")
        Return hashedPwd
    End Function
    Public Function AddANewUser(ByVal UserSecurityInfo As UserSecurityInfo) As Boolean
        Dim UserSecurityDB As New UserSecurityDB
        Dim founduser As Int32
        founduser = UserSecurityDB.VerifyUserNamePassword(UserSecurityInfo)
        If founduser = 2 Then
            Dim Salt As String = CreateSalt(5)
            UserSecurityInfo.UserPassword = CreatePasswordHash(UserSecurityInfo.UserPassword, Salt)
            UserSecurityInfo.Salt = Salt
            UserSecurityDB.AddNewUser(UserSecurityInfo)
            Return True
        Else
            Return False
        End If
    End Function
    Public Function MatchNewUserToEmployee(ByVal UserSecurityInfo As UserSecurityInfo) As DataSet
        Dim UserSecurityDB As New UserSecurityDB
        Dim ds As New DataSet
        ds = UserSecurityDB.GetEmployeeBasedOnNewUser(UserSecurityInfo)
        Return ds
    End Function
    Public Function LinkNewUserToEmployee(ByVal EmpId As Guid, ByVal UserName As String) As Boolean
        Dim UserSecurityDB As New UserSecurityDB
        Dim LinkSuccessfull As Boolean
        LinkSuccessfull = UserSecurityDB.LinkNewUserToEmployee(EmpId, UserName)
        Return LinkSuccessfull
    End Function
    Public Function GetProfileQuestions(ByVal UserName As String) As DataSet
        Dim UserSecurityDB As New UserSecurityDB
        Dim ds As New DataSet
        ds = UserSecurityDB.GetProfileQuestions(UserName)
        Return ds
    End Function
    Public Function GetProfileAnswers(ByVal UserName As String) As DataSet
        Dim UserSecurityDB As New UserSecurityDB
        Dim ds As New DataSet
        ds = UserSecurityDB.GetProfileAnswers(UserName)
        Return ds
    End Function
    Public Function ResetPassword(ByVal UserName As String, Optional ByVal Password As String = "") As Boolean
        Dim Salt As String = CreateSalt(5)
        Dim NewPassword As String
        Dim UpdateSuccesfull As Boolean
        If Password = "" Then
            NewPassword = CreatePasswordHash("Advantage", Salt)
        Else
            NewPassword = CreatePasswordHash(Password, Salt)
        End If
        Dim UserSecurityDB As New UserSecurityDB
        UpdateSuccesfull = UserSecurityDB.ResetPassword(UserName, NewPassword, Salt)
        Return UpdateSuccesfull
    End Function
    Public Function VerifyUserName(ByVal UserName As String) As Int32
        Dim UserSecurityDB As New UserSecurityDB
        Dim UserSecurityInfo As New UserSecurityInfo
        Dim founduser As Int32
        UserSecurityInfo.UserName = UserName
        founduser = UserSecurityDB.VerifyUserNamePassword(UserSecurityInfo)
        Return founduser
    End Function
    Public Function VerifyUserNameWithPass(ByVal UserName As String, ByVal Password As String) As Int32
        Dim UserSecurityDB As New UserSecurityDB
        Dim UserSecurityInfo As New UserSecurityInfo
        Dim founduser As Int32
        UserSecurityInfo.UserName = UserName
        UserSecurityInfo.UserPassword = Password
        founduser = UserSecurityDB.VerifyUserNamePassword(UserSecurityInfo)
        Return founduser
    End Function
    Public Function GetAllProfileQuestions() As DataSet
        Dim UserSecurityDB As New UserSecurityDB
        Dim ds As New DataSet
        ds = UserSecurityDB.GetAllProfileQuestions
        Return ds
    End Function

    Public Function GetExistingUsers(Optional ByVal UserName As String = "", Optional ByVal CampusId As String = "") As DataTable
        Dim dbUserSecurity As New UserSecurityDB
        Dim dt As New DataTable
        dt = dbUserSecurity.GetExistingUsers(UserName, CampusId)
        Return dt
    End Function

    Public Function GetUserInfo(ByVal userId As String) As UserSecurityInfo
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.GetUserInfo(userId)

    End Function
    Public Function GetUserInfoUsingUserName(ByVal userName As String) As UserSecurityInfo
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.GetUserInfoUsingUserName(userName)

    End Function

    Public Function GetAllCampuses() As DataTable
        Dim dbUserSecurity As New UserSecurityDB
        Dim dt As New DataTable

        dt = dbUserSecurity.GetAllCampuses

        Return dt
    End Function

    Public Function GetAssignedCampuses(ByVal UserID As String) As DataTable
        Return (New UserSecurityDB).GetAssignedCampuses(UserID)
    End Function

    Public Function UpdateUserSecurityInfo(ByVal userSecurit As UserSecurityInfo, ByVal user As String, Optional ByVal userName As String = "") As String
        'Instantiate DAL component
        With New UserSecurityDB
            'If it is a new usersecurityinfo do an insert. If not, do an update
            If Not (userSecurit.IsInDB = True) Then
                'return integer with insert results
                Return .AddUserSecurityInfo(userSecurit, user)
            Else
                'return integer with update results
                Return .UpdateUserSecurityInfo(userSecurit, user, userName)
            End If
        End With
    End Function

    Public Function CheckDuplicateUserOrEmail(username As String, email As String) As Boolean
        With New UserSecurityDB
            Return .CheckDuplicateUserOrEmail(username, email)
        End With
    End Function

    Public Function CheckDuplicateUserOrEmail(username As String, email As String, userId As String) As Boolean
        With New UserSecurityDB
            Return .CheckDuplicateUserOrEmail(username, email, userId)
        End With

    End Function

    Public Function DeleteUserSecurityInfo(ByVal UserId As String, ByVal modDate As DateTime) As String

        'Instantiate DAL component
        With New UserSecurityDB

            'Delete UserSecurityInfo and return integer result
            Return .DeleteUserSecurityInfo(UserId, modDate)

        End With

    End Function

    Public Function DeleteAdvantageUser(ByVal userId As Guid) As String

        'Instantiate DAL component
        With New UserSecurityDB

            'Delete UserSecurityInfo and return integer result
            Return .DeleteAdvantageUser(userId)

        End With

    End Function

    Public Function GetUserAssignedRolesCampusGroups(ByVal userId As String, ByVal roleId As String) As DataTable
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.GetUserAssignedRolesCampusGroups(userId, roleId)
    End Function

    Public Function GetUserAvailableRolesCampusGroups(ByVal userId As String, ByVal roleId As String) As DataTable
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.GetUserAvailableRolesCampusGroups(userId, roleId)
    End Function

    Public Sub DeleteUserRoleCampusGroups(ByVal userId As String, ByVal roleid As String)
        Dim dbUserSecurit As New UserSecurityDB

        dbUserSecurit.DeleteUserRoleCampusGroups(userId, roleid)
    End Sub

    Public Sub AddUserRoleCampusGroup(ByVal userId As String, ByVal roleId As String, ByVal campGrpId As String, ByVal user As String)
        Dim dbUserSecurit As New UserSecurityDB

        dbUserSecurit.AddUserRoleCampusGroup(userId, roleId, campGrpId, user)
    End Sub

    Public Function UserHasAccessToDefaultCampus(ByVal userId As String) As Boolean
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.UserHasAccessToDefaultCampus(userId)
    End Function

    Public Function GetCampusGroupsWithCampus(ByVal campusId As String) As DataTable
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.GetCampusGroupsWithCampus(campusId)
    End Function

    Public Function GetUserDefaultCampus(ByVal userId As String) As String
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.GetUserDefaultCampus(userId)
    End Function

    Public Function GetUserDefaultModule(ByVal userId As String) As String
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.GetUserDefaultModule(userId)
    End Function

    Public Function GetUserId(ByVal userName As String, ByVal passWord As String) As String
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.GetUserId(userName, passWord)

    End Function
    Public Function GetFullNameByUserId(ByVal userId As String) As String
        Dim dbUserSecurit As New UserSecurityDB
        Return dbUserSecurit.GetFullNameByUserId(userId)
    End Function
    Public Function GetUserResourcePermissions(ByVal userId As String, ByVal resourceId As Integer, Optional ByVal campusId As String = Nothing) As UserPagePermissionInfo
        Dim dbUserSecurit As New UserSecurityDB
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable
        '' Dim context As HttpContext
        Dim uppInfo As New UserPagePermissionInfo
        Dim dRow1 As DataRow
        Dim dRows() As DataRow
        Dim sFilter As String
        Dim iCounter As Integer

        'Get the roles and their access levels that have access to the specified resource
        dt2 = dbUserSecurit.GetResourceRoles(resourceId)

        'If the user logged in is the special "sa" user then we will simply give full access to the page
        'without checking any table for permissions.
        If HttpContext.Current.Session("UserName") = "sa" Then
            uppInfo.HasFull = True
            uppInfo.HasAdd = True
            uppInfo.HasDelete = True
            uppInfo.HasDisplay = True
            uppInfo.HasEdit = True
            uppInfo.HasNone = False
            ''New Code Added By Vijay Ramteke On June 15, 2010 Form Mantis Id 
        ElseIf HttpContext.Current.Session("UserName").ToString.ToLower = "support" Then
            uppInfo.HasFull = True
            uppInfo.HasAdd = True
            uppInfo.HasDelete = True
            uppInfo.HasDisplay = True
            uppInfo.HasEdit = True
            uppInfo.HasNone = False
        ElseIf HttpContext.Current.Session("UserName").ToString.ToLower = "super" Then
            uppInfo.HasFull = True
            uppInfo.HasAdd = True
            uppInfo.HasDelete = True
            uppInfo.HasDisplay = True
            uppInfo.HasEdit = True
            uppInfo.HasNone = False
            ''New Code Added By Vijay Ramteke On June 15, 2010 Form Mantis Id 
        Else
            'There are some pages such as reports and search pages where we don't need a campus id since the
            'user can run a report for multiple campuses. However, for most common tasks pages we would need
            'to know campus id 

            'Get the roles for the user
            If Not campusId Is Nothing Then
                dt1 = dbUserSecurit.GetUserRolesForCurrentCampus(userId, campusId)
            Else
                dt1 = dbUserSecurit.GetUserRoles(userId)
            End If

            'Loop through each user role in dt1. If the role exists in dt2 then set the appropriate
            'property on the uppInfo object. For example, if the user has the Placement Advisor role
            'and this role exists in dt2 with an access level of 5 then we would set the HasFull property
            'to true on the uppInfo object.
            If dt1.Rows.Count > 0 Then
                For Each dRow1 In dt1.Rows
                    sFilter = "RoleId = '" + dRow1("RoleId").ToString() + "'"
                    dRows = dt2.Select(sFilter)

                    If dRows.Length > 0 Then
                        For iCounter = 0 To dRows.Length - 1
                            Dim accessLevel As Integer = CType(dRows(iCounter)("AccessLevel"), Integer)

                            'this is "None" permissions 
                            If accessLevel = 0 Then uppInfo.HasNone = True

                            'this is "Full" permissions
                            If accessLevel = 15 Then
                                uppInfo.HasFull = True
                                uppInfo.HasNone = False
                            End If

                            'this is "Edit" permissions
                            If accessLevel - 8 >= 0 Then
                                uppInfo.HasEdit = True
                                uppInfo.HasNone = False
                                accessLevel -= 8
                            End If

                            'this is "Add" permissions
                            If accessLevel - 4 >= 0 Then
                                uppInfo.HasAdd = True
                                uppInfo.HasNone = False
                                accessLevel -= 4
                            End If

                            'this is "Delete" permissions
                            If accessLevel - 2 >= 0 Then
                                uppInfo.HasDelete = True
                                uppInfo.HasNone = False
                                accessLevel -= 2
                            End If

                            'this is "Display" permissions
                            If accessLevel - 1 = 0 Then
                                uppInfo.HasDisplay = True
                                uppInfo.HasNone = False
                            End If

                            'Select Case dRows(iCounter)("AccessLevel")
                            '    Case 5
                            '        uppInfo.HasFull = True
                            '        uppInfo.HasNone = False
                            '    Case 4
                            '        uppInfo.HasEdit = True
                            '        uppInfo.HasNone = False
                            '    Case 3
                            '        uppInfo.HasAdd = True
                            '        uppInfo.HasNone = False
                            '    Case 2
                            '        uppInfo.HasDelete = True
                            '        uppInfo.HasNone = False
                            '    Case 1
                            '        uppInfo.HasDisplay = True
                            '        uppInfo.HasNone = False
                            '    Case 0
                            '        uppInfo.HasNone = True
                            'End Select
                        Next
                    End If
                Next
            End If

        End If

        'We also need to set the URL property of the permission object. We will use this to 
        'compare to the page requested to see if it matches the URL we have stored for the
        'resource id that was passed in. If they do not match then we need to alert the user.
        'Note that the way dt2 was built we are assuming that there is at least one role with
        'access to the resource. In the ComparePageNames function in the header control if this
        'URL is an empty string then we will have to go to the db and actually get the url. This
        'approach will almost eliminate the need to go to the db since almost all resources will
        'have at least one role with access to it. However, it might be possible that only the "sa" 
        'user has access to a resouce and so there is no entry in the tables for it so the URL on
        'the permission object will be an empty string.

        If dt2.Rows.Count > 0 AndAlso dt2.Columns("ResourceURL") IsNot Nothing Then
            'We can get the URL from the very first row
            ' 11/21/06 (BEN) - Fails if ResourceURL is null
            uppInfo.URL = dt2.Rows(0)("ResourceURL").ToString()
        End If

        uppInfo.ResourceId = resourceId

        Return uppInfo

    End Function
    Public Function GetUserCampuses(ByVal userId As String) As DataTable
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.GetUserCampuses(userId)
    End Function



    Public Function ShowDefaultCampusScreenAtLogin(ByVal userId As String) As Boolean
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.ShowDefaultCampusScreenAtLogin(userId)
    End Function

    Public Sub UpdateUserDefaultCampus(ByVal userId As String, ByVal campusId As String, ByVal showLogin As Boolean)
        Dim dbUserSecurit As New UserSecurityDB

        dbUserSecurit.UpdateUserDefaultCampus(userId, campusId, showLogin)
    End Sub

    Public Function GetCampusName(ByVal campusId As String) As String
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.GetCampusName(campusId)
    End Function

    Public Function GetResourceURL(ByVal resourceID As Integer) As String
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.GetResourceURL(resourceID)
    End Function

    Public Function GetUserResourcePermissionsForSubModule(ByVal userId As String, ByVal subModId As Integer, ByVal resource As String, ByVal campusId As String) As ArrayList
        Dim dbUserSecurit As New UserSecurityDB
        Dim dt1 As DataTable
        Dim arrUPP As New ArrayList
        Dim dr As DataRow
        Dim uppInfo As New UserPagePermissionInfo

        'Get Datatable with resources and their sequences that belong to the specified submodule
        dt1 = dbUserSecurit.GetResourcesForSubModule(subModId, resource)
        'For each resource in the submodule we want to get a permission object. If the user has some
        'access to the resource (HasNone=false) then we will add the permission object to the
        'ArrayList that will be returned to the client code.
        For Each dr In dt1.Rows
            uppInfo = GetUserResourcePermissions(userId, dr("ResourceId"), campusId)
            If Not uppInfo.HasNone Then
                arrUPP.Add(uppInfo)
            End If
        Next

        Return arrUPP

    End Function

    Public Function DoesUserHasAccessToSubModuleResource(ByVal arrUPP As ArrayList, ByVal resourceId As Integer) As Boolean
        Dim uppInfo As UserPagePermissionInfo
        'Check each object in the arraylist to see if the resource id matches the resource id passed in.
        'If there is a match return true.
        For Each uppInfo In arrUPP
            If uppInfo.ResourceId = resourceId Then
                Return True
            End If
        Next

        Return False
    End Function
    Public Function DoesDefaultAdminRepExistForCampus(ByVal ModuleId As Integer, ByVal CampusId As String) As Boolean

        'return boolean
        Return (New UserSecurityDB).DoesDefaultAdminRepExistForCampus(ModuleId, CampusId)

    End Function

    Public Function IsDirectorOfAdmissions(ByVal userId As String) As Boolean

        'return boolean
        Return (New UserSecurityDB).IsSAOrDirectorOfAdmissions(userId)

    End Function

    Public Function GetInstructors(ByVal campusId As String) As DataTable
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.GetInstructors(campusId)

    End Function
    ''Added by Saraswathi Lakshmanan to Solve issue  14385
    ''When entering a new lead information SA, Front Desk and 
    ''Director of Admissions should be able to assign them to admission rep 

    Public Function IsDirectorOfAdmissionsorFrontDesk(ByVal userId As String) As Boolean

        'return boolean
        Return (New UserSecurityDB).IsSAOrDirectorOfAdmissionsorFrontDesk(userId)

    End Function
    Public Sub UpdateIsLoggedIn(ByVal UserId As String, ByVal CampusId As String)

        Dim db As New UserSecurityDB

        db.UpdateIsLoggedIn(UserId, CampusId)

    End Sub
    Public Sub UpdateGradeRounding(ByVal GradeRounding As String)

        Dim db As New UserSecurityDB

        db.UpdateGradeRounding(GradeRounding)

    End Sub

    Public Sub UpdateWhenUserLogsOut(ByVal UserId As String, ByVal CampusId As String)

        Dim db As New UserSecurityDB

        db.UpdateWhenUserLogsOut(UserId, CampusId)

    End Sub


    ''Added by Saraswathi to Find the username and password of the user, given the userId

    Public Function GetUserNameandPassword(ByVal userId As String) As DataTable
        Dim dbUserSecurit As New UserSecurityDB

        Return dbUserSecurit.GetUserNameandPassword(userId)
    End Function
    ''Added by saraswathi lakshmanan to get the password for the selected campuses
    ''Added on may 13 2009

    Public Function GetPasswordforCampus(ByVal CampusID As String) As String
        Dim dbUserSecurit As New UserSecurityDB
        Return dbUserSecurit.GetPasswordforCampus(CampusID)
    End Function
    ''Added by Kamalesh Ahuja to get the Fame Link password for the selected campuses
    ''Added on may 27 2010

    Public Function GetPasswordforCampusFL(ByVal CampusID As String) As String
        Dim dbUserSecurit As New UserSecurityDB
        Return dbUserSecurit.GetPasswordforCampusFL(CampusID)
    End Function
    Public Function GetAllInstructors_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable
        Dim dbUserSecurit As New UserSecurityDB
        Return dbUserSecurit.GetAllInstructors_SP(showActiveOnly, campusId)

    End Function
    Public Function GetInstructor_SP() As DataTable
        Dim dbUserSecurit As New UserSecurityDB
        Return dbUserSecurit.GetInstructor_SP()

    End Function

    Public Function GetInstructorActive_SP() As DataTable
        Dim dbUserSecurit As New UserSecurityDB
        Return dbUserSecurit.GetInstructorActive_SP()

    End Function


    'US3434 - SSN field on the Student Master page to visible to only certain security roles 
    Public Function GetRolePermissionforSSN(ByVal UserId As String, ByVal campusId As String) As Boolean
        Dim dbUserSecurit As New UserSecurityDB
        Return dbUserSecurit.GetRolePermissionforSSN(UserId, campusId)
    End Function

    Public Function UpdateTenantUsers(ByVal UserId As String, ByVal SchoolName As String)
        Dim db As New UserSecurityDB
        Return db.UpdateTenantUsers(UserId, SchoolName)
    End Function

    Public Function DeleteTenantUsers(ByVal UserId As String)
        Dim db As New UserSecurityDB
        Return db.DeleteTenantUsers(UserId)
    End Function

    Public Function ResetUsers_UserName(ByVal UserId As String, ByVal UserName As String)
        Dim db As New UserSecurityDB
        Return db.ResetUsers_UserName(UserId, UserName)
    End Function

    Public Function GetDashboardRoleID() As DataSet
        Dim db As New UserSecurityDB
        Return db.GetDashboardRoleID()
    End Function

    Public Sub InsertCampusIdforNewUser(ByVal UserId As String, ByVal CampusId As String)
        Dim db As New UserSecurityDB
        db.InsertCampusIdforNewUser(UserId, CampusId)
    End Sub

    Public Function DisplayNameExists(ByVal userdisplayname As String, Optional ByVal userid As String = "") As Boolean
        With New UserSecurityDB
            Return .DisplayNameExists(userdisplayname, userid)
        End With
    End Function

    Public Function DoesUserBelongToRoleForCampus(ByVal userId As String, ByVal SysRoleId As Integer, ByVal campusId As String) As Boolean
        Dim db As New UserSecurityDB

        Return db.DoesUserBelongToRoleForCampus(userId, SysRoleId, campusId)

    End Function
     Public Sub InsertMRUsForFirstTimeUser(ByVal userId As String) 
        Dim db As New UserSecurityDB
        db.InsertMRUsForFirstTimeUser(userId)
    End Sub
End Class
