Public Class DepositsByTypeSummaryObject
    Inherits BaseReportFacade

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim objDB As New DepositsByTypeDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by report.
        ds = BuildReportSource(objDB.GetFundTypeSummary(rptParamInfo))
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region



#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        Dim payType As Byte
        Dim strFundType As String = ""
        Dim oldCmpGrp As String = "", oldCampus As String = ""
        Dim objSUM, objCOUNT As Object
        Dim row As DataRow

        Try
            If ds.Tables.Count > 0 Then
                If ds.Tables("FundTypeSummary").Rows.Count > 0 Then
                    Dim dtSummary As DataTable = ds.Tables("FundTypeSummary")
                    Dim dtTotal As DataTable = ds.Tables("Totals")
                    '
                    For Each dr As DataRow In dtSummary.Rows
                        'CampGrpDescrip and CampDescrip columns cannot be NULL.
                        If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpDescrip") Then
                            oldCmpGrp = dr("CampGrpDescrip")
                            oldCampus = ""
                        End If
                        '
                        If oldCampus = "" Or oldCampus <> dr("CampusDescrip") Then
                            oldCampus = dr("CampusDescrip")
                            'Compute COUNT
                            objCOUNT = dtSummary.Compute("SUM(PymtCount)", _
                                    "CampGrpDescrip='" & oldCmpGrp & "' AND CampusDescrip='" & oldCampus & "'")
                            'Compute SUM
                            objSUM = dtSummary.Compute("SUM(PymtSum)", _
                                    "CampGrpDescrip='" & oldCmpGrp & "' AND CampusDescrip='" & oldCampus & "'")

                            'Add new row into the Totals table: dtTotal
                            row = dtTotal.NewRow
                            row("CampGrpDescrip") = oldCmpGrp
                            row("CampusDescrip") = oldCampus
                            If Not (objCOUNT Is System.DBNull.Value) Then row("TotalCount") = Convert.ToInt32(objCOUNT)
                            If Not (objSUM Is System.DBNull.Value) Then row("TotalSum") = Convert.ToDecimal(objSUM)
                            dtTotal.Rows.Add(row)
                        End If
                    Next
                End If
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region


End Class
