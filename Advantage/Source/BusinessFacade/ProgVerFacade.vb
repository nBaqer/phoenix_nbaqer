Public Class ProgVerFacade

    Public Function GetProgVersions(Optional ByVal ProgId As String = "", Optional ByVal CampusId As String = "") As DataSet

        '   Instantiate DAL component
        Dim DB As New ProgVerDB

        '   get the dataset with all degrees
        Return DB.GetProgVersions(ProgId, CampusId)

    End Function
    Public Function IsProgramVersionPartOfCampusUserIsLoggedInTo(ByVal campusId As String, ByVal progId As String, ByVal prgVerId As String) As Boolean
        Return (New ProgVerDB).IsProgramVersionPartOfCampusUserIsLoggedInTo(campusId, progId, prgVerId)
    End Function
    Public Function GetProgVersionsByProgram(ByVal ProgId As String) As DataSet
        Return (New ProgVerDB).GetProgVersionsByProgram(ProgId)
    End Function
    '''<summary>
    ''' This method returns program deatils for the specified programe version id. 
    ''' </summary>
    ''' <param name="progVerId">The progVerId is the programe version id in arPrgVersions.</param>
    ''' <returns>Returns the programe deatils for the specified programe version id</returns>
    Public Function GetProgVersionsByProgramVerId(ByVal progVerId As String, ByVal campusId As String) As DataSet
        Return (New ProgVerDB).GetProgVersionsByProgramVerId(progVerId, campusId)
    End Function
    '
    Public Function GetFASAPProgVersionsByUser(Optional ByVal campusId As String = "", Optional ByVal username As String = "", Optional ByVal userid As String = "") As DataTable
        Dim DB As New ProgVerDB

        Return DB.GetFASAPProgVersionsByUser(campusId, username, userid)
    End Function
    Public Function GetProgVersionsByUser(Optional ByVal campusId As String = "", Optional ByVal username As String = "", Optional ByVal userid As String = "") As DataTable
        Dim DB As New ProgVerDB

        Return DB.GetProgVersionsByUser(campusId, username, userid)
    End Function
    Public Function IsPrgVerAttTypeMatchWithCourses(prgverid As String, unitTypeid As String) As DataTable
        Dim DB As New ProgVerDB


        Return DB.IsPrgVerAttTypeMatchWithCourses(prgverid, unitTypeid)
    End Function


    Public Function GetSchedulingMethods() As DataSet

        '   Instantiate DAL component
        Dim DB As New ProgVerDB

        '   get the dataset with all degrees
        Return DB.GetSchedulingMethods()

    End Function
    Public Function GetTermTypes() As DataSet

        '   Instantiate DAL component
        Dim DB As New ProgVerDB

        '   get the dataset with all degrees
        Return DB.GetTermTypes()

    End Function
    Public Function GetPrgVerStartDates(ByVal ProgVerId As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New ProgVerDB

        '   get the dataset with all degrees
        Return DB.GetPrgVerStartDates(ProgVerId)

    End Function
    Public Function HasThisProgramVersionStudentsEnrolled(ByVal PrgVerID As String) As Boolean
        'return boolean value
        Return (New ProgVerDB).HasThisProgramVersionStudentsEnrolled(PrgVerID)
    End Function

    Public Function HasThisProgramVersionhaveCourses(ByVal prgVerID As String) As Boolean
        Return (New ProgVerDB).HasThisProgramVersionhaveCourses(prgVerID)
    End Function
    Public Function DoesProgVerHasSameAttenddanceTypeAsCourse(ByVal prgVerId As String, ByVal attType As String) As Boolean
        'return boolean value
        Dim recCount As Integer = (New ProgVerDB).DoesProgVerHasSameAttenddanceTypeAsCourse(prgVerId, attType)
        If recCount = 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function DoesProgVerHasAttendanceRecords(ByVal prgVerId As String) As Boolean
        'return boolean value
        Dim recCount As Integer = (New ProgVerDB).GetProgVerAttendanceRecords(prgVerId)
        If recCount > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function DoesProgVerCodeAlreadyExists(ByVal prgVerCode As String, ByVal PrgVerDescrip As String, ByVal campGrpId As String) As Boolean
        'return boolean value
        Dim recCount As Integer = (New ProgVerDB).GetProgVerCodeRecords(prgVerCode, PrgVerDescrip, campGrpId)
        If recCount > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' This method is to determine whether program is Non-Term based or not.
    ''' </summary>
    ''' <param name="prgVerCode">The prgVerCode is program version code which is a Id of type string</param>
    ''' <param name="prgVerDescrip">The prgVerDescrip is program version description of type string</param>
    ''' <param name="campGrpId">The campGrpId is campus group Id of type string</param>
    ''' <param name="acId">The acId is academic calender Id of type Integer</param>
    ''' <returns>Returns boolean value that determines whether the specified program is Non-Term based.</returns>
    Public Function GetProgramType(ByVal prgVerCode As String, ByVal prgVerDescrip As String, ByVal campGrpId As String, ByVal acId As Integer) As Boolean
        'return boolean value
        Dim recCount As Integer = (New ProgVerDB).GetProgramType(prgVerCode, prgVerDescrip, campGrpId, acId)
        If recCount > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function DoesProgVerCodeAlreadyExists(ByVal prgVerCode As String, ByVal PrgVerDescrip As String, ByVal campGrpId As String, ByVal prgVerId As String) As Boolean
        'return boolean value
        Dim recCount As Integer = (New ProgVerDB).GetProgVerCodeRecords(prgVerCode, PrgVerDescrip, campGrpId, prgVerId)
        If recCount > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function GetAllCampusGroupsByProgramAndProgramVersion(ByVal ProgId As String, Optional ByVal PrgVerId As String = "") As DataSet
        Return (New ProgVerDB).GetAllCampusGroupsByProgramAndProgramVersion(ProgId, PrgVerId)
    End Function
    Public Function AllowCampusGroupChange(ByVal ProgId As String, ByVal NewCampGrpId As String, Optional ByVal CampusId As String = "") As Boolean
        Return (New ProgVerDB).AllowCampusGroupChange(ProgId, NewCampGrpId, CampusId)
    End Function
    Public Function HasThisProgramPrgVersionsActive(ByVal ProgId As String) As Boolean
        Return (New ProgVerDB).HasThisProgramPrgVersionsActive(ProgId)
    End Function
    Public Function PrgVersionBelowToInactiveProgram(ByVal ProgId As String) As Boolean
        Return (New ProgVerDB).PrgVersionBelowToInactiveProgram(ProgId)
    End Function
    Public Function IsCampusPartOfGroup(ByVal CampusId As String, ByVal CampGrpId As String)
        Return (New ProgVerDB).IsCampusPartOfGroup(CampusId, CampGrpId)
    End Function
    Public Function GetAllFASAPByProgram(ByVal ProgId As String, Optional ByVal PrgVerId As String = "") As DataSet
        Return (New ProgVerDB).GETFASAPSetup(ProgId)
    End Function
    Public Function UpdateFASAP(ByVal FASAPid As String, ByVal strPKValue As String, ByVal User As String) As Boolean
        Return (New ProgVerDB).UpdateFASAP(FASAPid, strPKValue, User)
    End Function
    Public Function GetAllCampusGroupsByProgram(ByVal ProgId As String, Optional ByVal PrgVerId As String = "") As DataSet
        Return (New ProgVerDB).GetAllCampusGroupsByProgram(ProgId)
    End Function
    Public Function GetProgVersionsByCampusGroupandProgram(ByVal ProgId As String, ByVal CampGrpId As String) As DataSet
        Return (New ProgVerDB).GetProgVersionsByCampusGroupandProgram(ProgId, CampGrpId)
    End Function
    Public Function HasThisProgramVersionStudentsGraded(ByVal PrgVerID As String) As Boolean
        'return boolean value
        Return (New ProgVerDB).HasThisProgramVersionStudentsGraded(PrgVerID)
    End Function
    Public Function UpdateDoCourseWeightIntoOverallGPA(ByVal prgVerId As String, ByVal checked As Boolean) As Boolean
        'return boolean value
        Return (New ProgVerDB).UpdateDoCourseWeightIntoOverallGPA(prgVerId, checked)
    End Function

    Public Function UpdateProgramVersionDefinitionWeightValue(ByVal prgVerDefId As String, ByVal weight As Decimal) As Boolean
        'return boolean value
        Return (New ProgVerDB).UpdateProgramVersionDefinitionWeightValue(prgVerDefId, weight)
    End Function
    Public Function GetLinkFilePermissions(ByVal userId As String, ByVal resource As String, Optional ByVal campusId As String = Nothing) As UserPagePermissionInfo
        Dim dbProgVer As New ProgVerDB
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable
        Dim context As HttpContext
        Dim uppInfo As New UserPagePermissionInfo
        Dim dRow1 As DataRow
        Dim dRows() As DataRow
        Dim sFilter As String
        Dim iCounter As Integer

        'Get the roles and their access levels that have access to the specified resource
        dt2 = dbProgVer.GetResourceRoles(resource)

        ''If the user logged in is the special "sa" user then we will simply give full access to the page
        ''without checking any table for permissions.
        'If HttpContext.Current.Session("UserName") = "sa" Then
        '    uppInfo.HasFull = True
        '    uppInfo.HasAdd = True
        '    uppInfo.HasDelete = True
        '    uppInfo.HasDisplay = True
        '    uppInfo.HasEdit = True
        '    uppInfo.HasNone = False
        'Else
        '    'There are some pages such as reports and search pages where we don't need a campus id since the
        '    'user can run a report for multiple campuses. However, for most common tasks pages we would need
        '    'to know campus id 

        '    'Get the roles for the user
        If Not campusId Is Nothing Then
            dt1 = dbProgVer.GetUserRolesForCurrentCampus(userId, campusId)
        Else
            dt1 = dbProgVer.GetUserRoles(userId)
        End If

        'Loop through each user role in dt1. If the role exists in dt2 then set the appropriate
        'property on the uppInfo object. For example, if the user has the Placement Advisor role
        'and this role exists in dt2 with an access level of 5 then we would set the HasFull property
        'to true on the uppInfo object.
        If dt1.Rows.Count > 0 Then
            For Each dRow1 In dt1.Rows
                sFilter = "RoleId = '" + dRow1("RoleId").ToString() + "'"
                dRows = dt2.Select(sFilter)

                If dRows.Length > 0 Then
                    For iCounter = 0 To dRows.Length - 1
                        Dim accessLevel As Integer = CType(dRows(iCounter)("AccessLevel"), Integer)

                        'this is "None" permissions 
                        If accessLevel = 0 Then uppInfo.HasNone = True

                        'this is "Full" permissions
                        If accessLevel = 15 Then
                            uppInfo.HasFull = True
                            uppInfo.HasNone = False
                        End If

                        'this is "Edit" permissions
                        If accessLevel - 8 >= 0 Then
                            uppInfo.HasEdit = True
                            uppInfo.HasNone = False
                            accessLevel -= 8
                        End If

                        'this is "Add" permissions
                        If accessLevel - 4 >= 0 Then
                            uppInfo.HasAdd = True
                            uppInfo.HasNone = False
                            accessLevel -= 4
                        End If

                        'this is "Delete" permissions
                        If accessLevel - 2 >= 0 Then
                            uppInfo.HasDelete = True
                            uppInfo.HasNone = False
                            accessLevel -= 2
                        End If

                        'this is "Display" permissions
                        If accessLevel - 1 = 0 Then
                            uppInfo.HasDisplay = True
                            uppInfo.HasNone = False
                        End If

                    Next
                End If
            Next
        End If

        ' End If

        'We also need to set the URL property of the permission object. We will use this to 
        'compare to the page requested to see if it matches the URL we have stored for the
        'resource id that was passed in. If they do not match then we need to alert the user.
        'Note that the way dt2 was built we are assuming that there is at least one role with
        'access to the resource. In the ComparePageNames function in the header control if this
        'URL is an empty string then we will have to go to the db and actually get the url. This
        'approach will almost eliminate the need to go to the db since almost all resources will
        'have at least one role with access to it. However, it might be possible that only the "sa" 
        'user has access to a resouce and so there is no entry in the tables for it so the URL on
        'the permission object will be an empty string.

        'If dt2.Rows.Count > 0 AndAlso dt2.Columns("ResourceURL") IsNot Nothing Then
        '    'We can get the URL from the very first row
        '    ' 11/21/06 (BEN) - Fails if ResourceURL is null
        '    uppInfo.URL = dt2.Rows(0)("ResourceURL").ToString()
        'End If

        'uppInfo.ResourceId = resourceId

        Return uppInfo

    End Function
    ''Added by Saraswathi lakshmanan on May 12 2009
    ''For Mantis case 16068
    Public Function DoesprogramVersionHasScheduleAttached(ByVal prgVerId As String, ByVal UseTimeclock As Boolean) As Boolean
        'return boolean value
        Dim ds As New DataSet
        ds = (New ProgVerDB).GetScheduleforProgVerCode(prgVerId)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If UseTimeclock.ToString = ds.Tables(0).Rows(0)("UseTimeClock").ToString Then
                    Return False
                Else
                    Return True
                End If
            Else
                Return False
            End If
        Else
            Return False
        End If

    End Function
    Public Function IsClockHourSchool() As Boolean
        Return (New ProgVerDB).IsClockHourSchool()
    End Function

    ''Added by Saraswathi lakshmanan on May 28 2010
    ''To insert the Charge Sequence details regarding Academic Year and Payment periods.
    Public Function InsertIntoPrgChargePeriodSeqTable(ByVal PrgVerID As String) As String
        Return (New ProgVerDB).InsertIntoPrgChargePeriodSeqTable(PrgVerID)
    End Function
    Public Function DoesCampusIsASubsetOfCampusGroup(ByVal CampGrpId As String, ByVal campusId As String, ByVal prgVerId As String) As Boolean
        'return boolean value
        Return (New ProgVerDB).DoesCampusIsASubsetOfCampusGroup(CampGrpId, campusId, prgVerId)

    End Function

    Public Function PrgVersionsGetListActiveAvailableForABillingMethod(ByVal BillingMethodId As String) As DataSet
        '   Instantiate DAL component
        Dim DB As New ProgVerDB

        '   get the dataset with all degrees
        Return DB.PrgVersionsGetListActiveAvailableForABillingMethod(BillingMethodId)
    End Function

    Public Function PrgVersionsGetListForABillingMethod(ByVal BillingMethodId As String) As DataSet
        '   Instantiate DAL component
        Dim DB As New ProgVerDB

        '   get the dataset with all degrees
        Return DB.PrgVersionsGetListForABillingMethod(BillingMethodId)
    End Function

    Public Function GetPrgVersionsForCurrentBillingMethod(ByVal BillingMethodId As String) As DataSet
        '   Instantiate DAL component
        Dim DB As New ProgVerDB

        '   get the dataset with all degrees
        Return DB.GetPrgVersionsForCurrentBillingMethod(BillingMethodId)
    End Function

    Public Sub UpdatePrgVersionsWithBillingMethod(ByVal strXML As String, ByVal BillingMethodId As String)
        '   Instantiate DAL component
        Dim DB As New ProgVerDB

        '   get the dataset with all degrees
        DB.UpdatePrgVersionsWithBillingMethod(strXML, BillingMethodId)
    End Sub

    ' 08/08/2011 JRobinson Clock Hour Project - Add new methods to support the arPrgVerInstructionType table



#Region "PrgVerInstructionType"

    Public Function GetAllProgVersionInstructionType(ByVal prgVerId As String) As DataTable

        '   get the dataset with all Instruction Types Associated with the selected Program Version 
        Return (New ProgVerDB).GetAllProgVersionInstructionType(prgVerId)

    End Function

    Public Function UpdateProgVersionInstructionType(ByVal dtPrgVerInstructionType As DataTable, ByVal UserName As String, ByVal PrgVerId As Guid)
        Dim PrgDB As New ProgVerDB
        PrgDB.UpdateProgVersionInstructionType(dtPrgVerInstructionType, UserName, PrgVerId)
    End Function




#End Region

    Public Function GetProgramForPrgVersion(ByVal prgVerId As String) As String
        Dim PrgDB As New PrgVerDefDB
        Return PrgDB.GetProgramForPrgVersion(prgVerId)
    End Function

    Public Function GetAreaForPrgVersion(ByVal prgVerId As String) As String
        Dim PrgDB As New PrgVerDefDB
        Return PrgDB.GetAreaForPrgVersion(prgVerId)
    End Function

    Public Function GetAreaForProgram(ByVal programId As String) As String
        Dim PrgDB As New PrgVerDefDB
        Return PrgDB.GetAreaForProgram(programId)
    End Function

    Public Function GetDoCourseWeightOverallGPAValue(programVersionId As String) As Boolean
        Dim DB As New ProgVerDB

        '   get the dataset with all degrees
        return DB.GetDoCourseWeightOverallGPAValue(programVersionId)
    End Function

    Public Function GetProgramVersionDefinitionId(prgVerId As String, requirementId As String) As String
        Dim DB As New ProgVerDB

        '   get the dataset with all degrees
        return DB.GetProgramVersionDefinitionId(prgVerId, requirementId)
    End Function
End Class
