' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' PeriodFacade.vb
'
' PeriodFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class PeriodFacade
    Public Function GetAllPeriods(ByVal campusId As String) As DataSet

        '   get the dataset with all Periods
        Return (New PeriodsDB).GetAllPeriods(campusId)

    End Function
    Public Function GetAllPeriods() As DataSet

        '   get the dataset with all Periods
        Return (New PeriodsDB).GetAllPeriods()

    End Function
    Public Function GetPeriodInfo(ByVal PeriodId As String) As PeriodInfo

        '   get the PeriodInfo
        Return (New PeriodsDB).GetPeriodInfo(PeriodId)

    End Function
    Public Function UpdatePeriodInfo(ByVal PeriodInfo As PeriodInfo, ByVal user As String) As String

        '   If it is a new account do an insert. If not, do an update
        If Not (PeriodInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New PeriodsDB).AddPeriodInfo(PeriodInfo, user)
        Else
            '   return integer with update results
            Return (New PeriodsDB).UpdatePeriodInfo(PeriodInfo, user)
        End If

    End Function
    Public Function DeletePeriodInfo(ByVal PeriodId As String, ByVal modDate As DateTime) As String

        '   delete PeriodInfo ans return string result
        Return (New PeriodsDB).DeletePeriodInfo(PeriodId, modDate)

    End Function
    Public Function GetCollectionOfMeetingDates(ByVal startDate As Date, ByVal endDate As Date, ByVal meetDays As Integer, ByVal selectWeek As Integer) As System.Collections.Generic.List(Of Date)
        '  return dates collection
        Return AdvantageCommonValues.GetCollectionOfDates(startDate, endDate, meetDays, selectWeek)
    End Function
    Public Function GetCollectionOfMeetingDates(ByVal startDate As Date, ByVal endDate As Date, ByVal periodId As String, ByVal selectWeek As Integer) As System.Collections.Generic.List(Of Date)
        '  return dates collection
        Return (New PeriodsDB).GetCollectionOfMeetingDates(startDate, endDate, periodId, selectWeek)
    End Function
    Public Function GetCollectionOfMeetingDatesWithCourseTypeID(ByVal clsSectionId As String, ByVal CourseTypeId As String, Optional ByVal CampusId As String = "") As System.Collections.Generic.List(Of AdvantageClassSectionMeeting)
        '  return dates collection
        Return (New PeriodsDB).GetCollectionOfMeetingDatesWithCourseTypeID(clsSectionId, CourseTypeId, CampusId)
    End Function

    Public Function NewGetCollectionOfMeetingDatesWithCourseTypeID(ByVal clsSectionId As String, ByVal ClSSectMeetingID As String, Optional ByVal CampusId As String = "") As System.Collections.Generic.List(Of AdvantageClassSectionMeeting)
        '  return dates collection
        Return (New PeriodsDB).NewGetCollectionOfMeetingDatesWithCourseTypeID(clsSectionId, ClSSectMeetingID, CampusId)
    End Function

    Public Function GetCollectionOfMeetingDates(ByVal clsSectionId As String, Optional ByVal CampusId As String = "") As System.Collections.Generic.List(Of AdvantageClassSectionMeeting)
        '  return dates collection
        Return (New PeriodsDB).GetCollectionOfMeetingDates(clsSectionId, CampusId)
    End Function
    Public Function GetCollectionOfMeetingDates_New(ByVal clsSectionId As String, Optional ByVal CampusId As String = "") As System.Collections.Generic.List(Of AdvantageClassSectionMeeting)
        '  return dates collection
        Return (New PeriodsDB).GetCollectionOfMeetingDates_New(clsSectionId, CampusId)
    End Function
    Public Function GetCollectionOfMeetingDatesForMeetings(ByVal clsSectionId As String, clsSectmeetingId As String, Optional ByVal CampusId As String = "") As System.Collections.Generic.List(Of AdvantageClassSectionMeeting)
        '  return dates collection
        Return (New PeriodsDB).GetCollectionOfMeetingDatesForMeetings(clsSectionId, clsSectmeetingId, CampusId)
    End Function

    Public Function GetWorkDayIdsAndStartAndEndTimeForPeriod(ByVal periodId As String) As ArrayList
        'return arrayList
        Return (New PeriodsDB).GetWorkDayIdsAndStartAndEndTimeForPeriod(periodId)
    End Function
    Public Function GetPeriodsDescrip() As DataTable
        Return (New PeriodsDB).GetPeriodsDescrip
    End Function

End Class
