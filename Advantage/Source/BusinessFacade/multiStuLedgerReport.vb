Imports FAME.Advantage.Common

Public Class multiStuLedgerReport
    Inherits BaseReportFacade


#Region "Public Methods"
    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim multistuTranscript As New multiStuLedgerReportDB
        Dim ds As DataSet
        'Get list of enrollments for ProgramVersion.
        'Massage dataset to produce one with the columns and data expected by the MultiTranscript report. 
        ds = BuildReportSource(multistuTranscript.GetStudentLedger(rptParamInfo), multistuTranscript.StudentIdentifier, rptParamInfo)
        Return ds
    End Function
#End Region


    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String, ByVal rptParamInfo As Common.ReportParamInfo) As DataSet
        Dim Transcript As New TranscriptFacade
        Dim br As New RegisterStudentsBR
        Dim stuEnrollId As String
        Dim StuIdentifier As String
        Dim stuName As String
        Dim ssn As String
        Dim facInputMasks As New InputMasksFacade
        Dim fac As New GraduateAuditFacade
        Dim rptfac As New ReportFacade
        Dim transcriptUtil As New FAME.AdvantageV1.Common.Utilities
        Dim temp As String
        Dim streetAddress As String = ""
        Dim cityStateZip As String = ""
        Dim row As DataRow
        Dim dtEnroll As DataTable = ds.Tables("MultipleStudentInformation")
        'Dim dtSummary As DataTable = ds.Tables(1)
        'ds.Tables(1).TableName = "MultipleStudentLedger"
        Dim dtSummary As DataTable = ds.Tables("MultipleStudentLedger")
        Dim dtCourses As DataTable = ds.Tables("MultipleStudentLedger")

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim strIncludeHours As String = MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade")
        Dim includeHours As Boolean
        If strIncludeHours.ToLower = "true" Then
            includeHours = True
        End If
        Dim termDescrip As String = String.Empty
        Dim totalScore As Decimal
        Dim stuId As String
        Dim TotalBalance As Decimal = 0.0
        'Dim ds As DataSet
        'Dim lastRow As Integer = ds.Tables(1).Rows.Count - 1

        'Get the mask for SSN, phone numbers and zip
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        Dim strMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        Dim zipMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        Try
            dtEnroll = ds.Tables(0)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr1 As DataRow In dtEnroll.Rows

                        stuName = dr1("LastName")
                        If Not dr1.IsNull("FirstName") Then
                            If dr1("FirstName") <> "" Then
                                stuName &= ", " & dr1("FirstName")
                            End If
                        End If
                        If Not dr1.IsNull("MiddleName") Then
                            If dr1("MiddleName") <> "" Then
                                stuName &= " " & dr1("MiddleName") & "."
                            End If
                        End If
                        dr1("StudentName") = stuName


                        'Get the ssn of the student
                        temp = dr1("StudentIdentifier")
                        dr1("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                        StuIdentifier = dr1("StudentIdentifier").ToString


                        'Get program student is enrolled into.
                        stuEnrollId = dr1("StuEnrollId").ToString

                        'Field to link tables in report.
                        stuId = dr1("StudentId").ToString

                        Dim strWhere As String
                        strWhere = rptParamInfo.FilterOther

                        'Dim strNewWhere As String = strWhere
                        'If (strWhere.ToUpper.IndexOf("TRANSDATE") > 0) Then
                        '    strNewWhere = strWhere.Substring(0, strWhere.ToUpper.Substring(0, strWhere.ToUpper.IndexOf("TRANSDATE") - 1).LastIndexOf("AND"))
                        '    strWhere = strWhere.Replace(strNewWhere, "")
                        'Else
                        '    strWhere = ""
                        'End If

                        'Dim dtSummary As DataTable

                        dtSummary = fac.GetStudentsByEnrollment(stuId, strWhere, stuEnrollId)

                        'ds.Tables(1).TableName = dtSummary
                        If dtSummary.Rows.Count > 0 Then
                            Dim lastRow As Integer = dtSummary.Rows.Count - 1

                            For Each dr2 As DataRow In dtSummary.Rows

                                row = dtCourses.NewRow

                                stuEnrollId = row("StuEnrollId").ToString

                                rptParamInfo.SchoolLogo = rptfac.GetSchoolLogo.Image
                                row("SchoolLogo") = rptParamInfo.SchoolLogo
                                row("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper 'SchoolName
                                row("ShowFilters") = rptParamInfo.ShowFilters
                                row("Filters") = "Filter criteria: " & rptParamInfo.FilterListString

                                row("StudentName") = stuName
                                row("TransactionId") = dr2("TransactionId").ToString
                                row("StudentId") = dr2("StudentId").ToString
                                row("StudentIdentifier") = StuIdentifier
                                row("StuEnrollId") = dr2("StuEnrollId").ToString
                                row("EnrollmentDescrip") = dr2("EnrollmentDescrip").ToString
                                row("TransCodeId") = dr2("TransCodeId").ToString
                                row("TransCodeDescrip") = dr2("TransCodeDescrip").ToString
                                row("TransDescrip") = dr2("TransDescrip").ToString

                                row("TransDate") = dr2("TransDate").ToString
                                row("TransAmount") = dr2("TransAmount").ToString
                                row("TransReference") = dr2("TransReference").ToString
                                row("AcademicYearId") = dr2("AcademicYearId").ToString
                                row("AcademicYearDescrip") = dr2("AcademicYearDescrip").ToString
                                row("TermId") = dr2("TermId").ToString
                                row("TermDescrip") = dr2("TermDescrip").ToString

                                row("TransTypeId") = dr2("TransTypeId").ToString
                                row("TransTypeDescrip") = dr2("TransTypeDescrip").ToString
                                row("ModUser") = dr2("ModUser").ToString
                                row("ModDate") = dr2("ModDate").ToString

                                'row("Balance") = dr2("Balance").ToString

                                If Not (dr2("Balance") Is System.DBNull.Value) Then
                                    If dr2("Balance") < 0 Then
                                        row("Balance") &= "$" & "(" & (Convert.ToDecimal(dr2("Balance")) * (-1)) & ")"
                                        'row("TotalBalance") &= "$" & "(" & ((Convert.ToDecimal(dtSummary.Rows(lastRow)("Balance"))) * (-1)) & ")"
                                    Else
                                        row("Balance") &= "$" & (Convert.ToDecimal(dr2("Balance")))
                                        'row("TotalBalance") &= "$" & (Convert.ToDecimal(dtSummary.Rows(lastRow)("Balance")))
                                    End If
                                Else
                                    row("Balance") = dr2("Balance")
                                End If


                                If (Convert.ToDecimal(dtSummary.Rows(lastRow)("Balance"))) < 0 Then
                                    row("TotalBalance") &= "$" & "(" & ((Convert.ToDecimal(dtSummary.Rows(lastRow)("Balance"))) * (-1)) & ")"
                                Else
                                    row("TotalBalance") &= "$" & (Convert.ToDecimal(dtSummary.Rows(lastRow)("Balance")))
                                End If


                                'row("TotalBalance") &= "$" & "(" & ((Convert.ToDecimal(dtSummary.Rows(lastRow)("Balance"))) * (-1)) & ")"

                                'If Not (dr2("TransAmount") Is System.DBNull.Value) Then
                                '    'Added By Hepsiba for adding $ , Charge and Payment display correctly
                                '    If row("TransTypeDescrip") = "Charge" Then
                                '        row("Charge") &= "$" & dr2("TransAmount")
                                '    Else
                                '        row("Payment") &= "$" & dr2("TransAmount") * (-1)
                                '    End If
                                'Else
                                '    row("Payment") &= "$" & dr2("TransAmount") * (-1)
                                'End If




                                If Not (dr2("TransAmount") Is System.DBNull.Value) Then
                                    If dr2("TransAmount") > 0 Then
                                        row("Charge") = dr2("TransAmount")
                                    Else
                                        row("Payment") = dr2("TransAmount") * (-1)
                                    End If
                                End If



                                'If Not (dr2("Balance") Is System.DBNull.Value) Then
                                '    If dr2("Balance") < 0 Then
                                '        rowC("Balance") &= "$" & "(" & dr2("Balance").ToString & ")"
                                '    Else
                                '        rowC("Balance") &= "$" & dr2("Balance").ToString
                                '    End If
                                'Else
                                '    rowC("Balance") = dr2("Balance")
                                'End If

                                dtCourses.Rows.Add(row)
                            Next

                        End If

                    Next

                    If dtCourses.Rows.Count = 0 Then
                        dtEnroll.Rows.Clear()
                    End If

                End If

            End If



        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function
End Class
