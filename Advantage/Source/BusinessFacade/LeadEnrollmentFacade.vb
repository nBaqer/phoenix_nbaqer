Public Class LeadEnrollmentFacade
    Public Function GetLeads(ByVal LeadID As String) As DataSet
        Dim ds As New DataSet
        With New LeadEnrollmentDB
            'get the dataset with all SkillGroups
            Return .GetLeads(LeadID)
        End With
    End Function
    'Public Function InsertLeadsToStudent(ByVal SelectedLead() As String, ByVal EnrollmentID() As String, ByVal user As String) As Integer
    '    Dim ds As New DataSet
    '    With New LeadEnrollmentDB
    '        'get the dataset with all SkillGroups
    '        Return .InsertLeadsToStudent(SelectedLead, EnrollmentID, user)
    '    End With
    'End Function
    Public Function CheckLeadExists(ByVal FirstName As String, ByVal LastName As String, ByVal SSN As String) As Integer
        With New LeadEnrollmentDB
            Return .CheckLeadExists(FirstName, LastName, SSN)
        End With
    End Function
    Public Function StudentName(ByVal StudentID As String) As String
        Dim ds As New DataSet
        With New LeadEnrollmentDB
            'get the dataset with all SkillGroups
            Return .GetStudentName(StudentID)
        End With
    End Function
    Public Function GetLeadEnrollmentInfo(ByVal LeadID As String) As LeadMasterInfo
        ' Dim ds As New DataSet
        With New LeadDB
            'get the dataset with all SkillGroups
            Return .GetLeadEnrollmentInfo(LeadID)
        End With
    End Function
    'UnEnrollLead
    'Public Function UnEnrollLead(ByVal LeadId As String, ByVal LeadStatus As String) As String
    '    With New LeadEnrollmentDB
    '        Return .UnEnrollLead(LeadId, LeadStatus)
    '    End With
    'End Function
    Public Function UnEnrollLeadBySP(ByVal LeadId As String, ByVal status As String) As Integer
        With New LeadEnrollmentDB
            Return .UnEnrollLeadBySP(LeadId, status)
        End With
    End Function
    'Public Function GetAllGradeLevel() As DataSet
    '    Dim ds As New DataSet
    '    With New LeadEnrollmentDB
    '        'get the dataset with all SkillGroups
    '        Return .GetAllGradeLevel(True)
    '    End With
    'End Function
    Public Function CheckLeadTakenTest(ByVal LeadId As String) As Integer
        With New LeadEnrollmentDB
            Return .CheckLeadTakenTest(LeadId)
        End With
    End Function
    Public Function CheckLeadTakenDocs(ByVal LeadId As String) As Integer
        With New LeadEnrollmentDB
            Return .CheckLeadTakenDocs(LeadId)
        End With
    End Function
    Public Function CheckIfLeadHasTakenTest(ByVal LeadId As String) As Integer
        With New LeadEnrollmentDB
            Return .CheckIfLeadHasTakenTest(LeadId)
        End With
    End Function
    'Public Function CheckLeadHasPassed(ByVal LeadId As String) As String
    '    With New LeadEnrollmentDB
    '        Return .CheckLeadHasPassed(LeadId)
    '    End With
    'End Function
    Public Function CheckLeadHasNotApprovedDocs(ByVal LeadId As String) As Integer
        With New LeadEnrollmentDB
            Return .CheckLeadHasNotApprovedDocs(LeadId)
        End With
    End Function
    'Public Function CheckLeadNoTestTaken(ByVal LeadId As String, ByVal PrgVerId As String) As String
    '    With New LeadEnrollmentDB
    '        Return .CheckLeadNoTestTaken(LeadId, PrgVerId)
    '    End With
    'End Function
    Public Function CheckIfLeadHasRequiredDocs(ByVal LeadId As String, ByVal PrgVerId As String) As String
        With New LeadEnrollmentDB
            Return .CheckIfLeadHasRequiredDocs(LeadId, PrgVerId)
        End With
    End Function
    Public Function CheckIfLeadHasRequiredDocsNoProgramVersion(ByVal LeadId As String) As String
        With New LeadEnrollmentDB
            Return .CheckIfLeadHasRequiredDocsNoProgramVersion(LeadId)
        End With
    End Function
    Public Function CheckIfLeadHasRequiredTest(ByVal LeadId As String, ByVal PrgVerId As String) As String
        With New LeadEnrollmentDB
            Return .CheckIfLeadHasRequiredTest(LeadId, PrgVerId)
        End With
    End Function
    Public Function CheckIfLeadHasRequiredTestNoProgramVersion(ByVal LeadId As String) As String
        With New LeadEnrollmentDB
            Return .CheckIfLeadHasRequiredTestNoProgramVersion(LeadId)
        End With
    End Function
    Public Function CheckIfLeadHasNotPassedTest(ByVal LeadId As String)
        With New LeadEnrollmentDB
            Return .CheckIfLeadHasNotPassedTest(LeadId)
        End With
    End Function
    'Public Function CheckLeadRequiredDocs(ByVal LeadId As String, ByVal PrgVerId As String) As String
    '    With New LeadEnrollmentDB
    '        Return .CheckLeadHasRequiredDocs(LeadId, PrgVerId)
    '    End With
    'End Function
    Public Function CheckIfLeadWasEnrolled(ByVal LeadId As String) As Boolean
        With New LeadEnrollmentDB
            Return .CheckIfLeadWasEnrolled(LeadId)
        End With
    End Function
    Public Function CheckIfLeadStatusMapsToEnrollment(ByVal CampusId As String, ByVal UserId As String, ByVal strCurrentLeadStatus As String) As String
        With New LeadEnrollmentDB
            Return .CheckIfLeadStatusMapsToEnrollment(CampusId, UserId, strCurrentLeadStatus)
        End With
    End Function

    Public Function GetExpectedGradDateForModuleStart(ByVal prgVerId As String, ByVal expStartDate As String, ByVal campusId As String, ByVal shiftId As String) As String
        With New LeadEnrollmentDB
            Return .GetExpectedGradDateForModuleStart(prgVerId, expStartDate, campusId, shiftId)
        End With
    End Function
    Public Function ImportAllLeads(ByVal FileName As String, ByVal UserName As String, _
                                  ByVal CampusId As String, ByVal UserId As String, ByVal DistributeLeads As Boolean, ByVal randNumber As Integer) As String
        With New LeadEnrollmentDB
            Return .ImportAllLeads(FileName, UserName, CampusId, UserId, DistributeLeads, randNumber)
        End With
    End Function
    Public Function GetLeadSummaryByAdmissionsRep(ByVal randNumber As String) As DataTable
        With New LeadEnrollmentDB
            Return .GetLeadSummaryByAdmissionsRep(randNumber)
        End With
    End Function
    Public Function GetonlineAdmissionRepsCount(ByVal CampusId As String) As Integer
        With New LeadEnrollmentDB
            Return .GetonlineAdmissionRepsCount(CampusId)
        End With
    End Function
    'Public Function ViewDataToImport(ByVal filePath As String) As DataSet
    '    With New LeadEnrollmentDB
    '        Return .ViewDataToImport(filePath)
    '    End With
    'End Function
    'Public Function ViewAttendanceDataFromExcel(ByVal filePath As String) As DataSet
    '    With New LeadEnrollmentDB
    '        Return .ViewAttendanceDataFromExcel(filePath)
    '    End With
    'End Function
    'Public Function ImportAttendance(ByVal ds As DataSet) As Integer
    '    With New LeadEnrollmentDB
    '        Return .ImportAttendance(ds)
    '    End With
    'End Function
    Public Function ImportAllLeadsforUnitek(ByVal FileName As String, ByVal UserName As String, _
                                  ByVal CampusId As String, ByVal UserId As String, ByVal DistributeLeads As Boolean, ByVal randNumber As Integer) As String
        With New LeadEnrollmentDB
            Return .ImportLeadsForUnitek(FileName, UserName, CampusId, UserId, DistributeLeads, randNumber)
        End With
    End Function

    Public Function GetLeadIdForEnrollment(ByVal StuEnrollId As String) As String
        Dim db As New LeadEnrollmentDB

        Return db.GetLeadIdForEnrollment(StuEnrollId)

    End Function

    Public Function GetImportLeadsFoldersPaths(ByVal CampusId As String, Optional ByRef ImportPath As String = "", Optional ByRef ArchivePath As String = "", Optional ByRef ExceptionPath As String = "", Optional ByRef RemoteUserName As String = "", Optional ByRef RemotePasswd As String = "") As String
        Return (New LeadEnrollmentDB).GetImportLeadsFoldersPaths(CampusId, ImportPath, ArchivePath, ExceptionPath, RemoteUserName, RemotePasswd)
    End Function

    Public Function CheckImportLeadsFoldersPaths(ByVal CampusId As String) As DataSet
        Return (New LeadEnrollmentDB).CheckImportLeadsFoldersPaths(CampusId)
    End Function

    Public Function IsRemoteServer(ByVal CampusId As String) As Boolean
        Return (New LeadEnrollmentDB).CheckIsRemoteServer(CampusId)
    End Function

    Public Function GetAdvImportLeadsFields() As DataTable
        'It seems like we cannot add the select opion to the ddl in the GridTemplate
        'so we have to do it here.
        Dim dt As New DataTable
        Dim db As New LeadEnrollmentDB
        Dim dr As DataRow

        dt = db.GetAdvImportLeadsFields()

        dr = dt.NewRow
        dr("FldName") = ""
        dr("Caption") = "Select"

        dt.Rows.InsertAt(dr, 0)
        dt.AcceptChanges()

        Return dt


    End Function

    Public Function DuplicateExistsForImportLeadRecord(firstName As String, lastName As String)
        Dim blnResult = False
        Dim db As New LeadEnrollmentDB
        Dim cnt As Integer

        cnt = db.GetCountForImportLeadRecord(firstName, lastName)

        If cnt > 0 Then
            blnResult = True
        End If

        Return blnResult

    End Function

    Public Function GetImportLeadFieldsWithLookups() As DataTable
        Dim db As New LeadEnrollmentDB

        Return db.GetImportLeadFieldsWithLookups()

    End Function

    'Public Function ProcessImportLeadFile(ByVal dt As DataTable) As String
    '    Dim db As New LeadEnrollmentDB

    '    Return db.ProcessImportLeadFile(dt)

    'End Function
    Public Function ProcessImportLeadFile(ByVal dtLeads As DataTable, ByVal strMapName As String, ByVal userName As String, ByVal dtFileAdvFldMappings As DataTable, ByVal dtLookupsMappedValues As DataTable) As String
        Dim db As New LeadEnrollmentDB

        Return db.ProcessImportLeadFile(dtLeads, strMapName, userName, dtFileAdvFldMappings, dtLookupsMappedValues)

    End Function

    Public Function GetSavedMappingsWithNumberOfFields(ByVal numFields As Integer, ByVal campusId As String) As DataTable
        Dim db As New LeadEnrollmentDB

        Return db.GetSavedMappingsWithNumberOfFields(numFields, campusId)

    End Function

    Public Function GetAdvFileFldMappings(ByVal mappingId As String) As DataTable
        Dim db As New LeadEnrollmentDB

        Return db.GetAdvFileFldMappings(mappingId)

    End Function

    Public Function GetLookupFldValsMappings(ByVal mappingId As String) As DataTable
        Dim db As New LeadEnrollmentDB

        Return db.GetLookupFldValsMappings(mappingId)
    End Function
End Class
