Public Class StartDatesFacade
    Public Function GetAllStartDates() As DataSet


        '   get the dataset with all XXXXXXXXs
        Return (New StartDatesDB).GetAllStartDates()

    End Function
    Public Function GetStartDateInfo(ByVal StartDateId As String) As StartDateInfo

        '   get the XXXXXXXXInfo
        Return (New StartDatesDB).GetStartDateInfo(StartDateId)

    End Function
    Public Function UpdateStartDateInfo(ByVal StartDatesInfo As StartDateInfo, ByVal user As String) As String


        '   If it is a new account do an insert. If not, do an update
        If Not (StartDatesInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New StartDatesDB).AddStartDateInfo(StartDatesInfo, user)
        Else
            '   return integer with update results
            Return (New StartDatesDB).UpdateStartDateInfo(StartDatesInfo, user)
        End If

    End Function
    Public Function DeleteStartDateInfo(ByVal StartDateId As String, ByVal modDate As DateTime) As String

        '   delete XXXXXXXXInfo ans return string result
        Return (New StartDatesDB).DeleteStartDateInfo(StartDateId, modDate)

    End Function

    Public Function GetStudentCount(ByVal termId As String) As Integer
        Return (New StartDatesDB).GetStudentCount(termId)
    End Function

    Public Function GetStartDatesShift(ByVal progid As String) As String
        Return (New StartDatesDB).GetStartDatesShift(progid)
    End Function
    Public Function GetStartDatesShiftForProgram(ByVal progid As String) As String
        Return (New StartDatesDB).GetStartDatesShiftForProgram(progid)
    End Function

    Public Function GetTermStartDate(ByVal progid As String, ByVal expStartDate As String) As Boolean
        Return (New StartDatesDB).GetTermStartDate(progid, expStartDate)
    End Function

End Class
