' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' CampusGroupsFacade.vb
'
' CampusGroupsFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================


Public Class CampusGroupsFacade
    Public Function GetAllCampusGroups() As DataSet
        '   Instantiate DAL component
        With New CampusGroupsDB
            '   get the dataset with all CampusGroups
            Return .GetAllCampusGroups()
        End With
    End Function
    Public Function GetAllCampusGroupsByUser(ByVal UserId As String) As DataSet
        With New CampusGroupsDB
            '   get the dataset with all CampusGroups
            Return .GetAllCampusGroupsByUser(UserId)
        End With
    End Function

    Public Function GetAllIndividualCampusGroupsByUserAndResourceID(ByVal UserId As String, resourceId As Integer) As DataSet
        With New CampusGroupsDB
            '   get the dataset with all CampusGroups By User And ResourceID
            Return .GetAllIndividualCampusGroupsByUserAndResourceID(UserId, resourceId)
        End With
    End Function


    Public Function GetCampusGroupsByUserAndResourceIDWithAccess(ByVal UserId As String, resourceId As Integer, CampusId As String) As DataSet
        With New CampusGroupsDB
            '   get the dataset with all CampusGroups User And ResourceID With Access
            Return .GetCampusGroupsByUserAndResourceIDWithAccess(UserId, resourceId, CampusId)
        End With
    End Function
    Public Function GetCampusGroupsItems(ByVal UserId As String, resourceId As Integer, CampusId As String, originalDataSet As DataSet) As DataSet
        With New CampusGroupsDB
            '   get the dataset with all CampusGroups Items Access
            Return .GetCampusGroupsItems(UserId, resourceId, CampusId, originalDataSet)
        End With
    End Function


    Public Function GetAllCampusGroupsIdByUser(ByVal userId As String) As Guid()
        With New CampusGroupsDB
            '   get the dataset with all CampusGroups
            Dim ds As DataSet = .GetAllCampusGroupsByUser(userId)
            If ds.Tables.Count > 0 Then
                Dim tabla As DataTable = ds.Tables(0)
                Dim guids() As Guid = New Guid(tabla.Rows.Count) {}
                Dim i As Integer
                For Each row As DataRow In tabla.Rows
                    Dim g As Guid = row("CampGrpId")
                    guids(i) = g
                    i = i + 1
                Next
                Return guids
            Else 
                Return New Guid() {}
            End If

        End With
    End Function

    Public Function GetAllCampusesUsedByLeads() As DataSet
        '   Instantiate DAL component
        '   get the dataset with all Campuses
        Return (New CampusGroupsDB).GetAllCampusesUsedByLeads()
    End Function

    ''' <summary>
    ''' Return the CampusGrups for ALL Campus.
    ''' </summary>
    ''' <returns>The All Campus Group ID</returns>
    Public Shared Function GetAllCampGrpId() As String
        Dim allCampusId As String = (New CampusGroupsDB).GetAllCampGrpId()
        Return allCampusId
    End Function

    Public Function GetAllCampusesUsedByStudents() As DataSet
        '   Instantiate DAL component
        '   get the dataset with all Campuses
        Return (New CampusGroupsDB).GetAllCampusesUsedByStudents()
    End Function
    Public Function GetAllCampusEnrollment() As DataSet
        '   Instantiate DAL component
        With New CampusGroupsDB
            '   get the dataset with all CampusGroups
            Return .GetAllCampusEnrollment()
        End With
    End Function
    Public Function GetAllCampusesByCampusId(ByVal campusId As String) As DataSet
        '   Instantiate DAL component
        With New CampusGroupsDB
            '   get the dataset with all CampusGroups
            Return .GetAllCampusesByCampusId(campusId)
        End With
    End Function
    Public Function GetCampusDetailsByCampusId(ByVal campusId As String) As DataSet
        '   Instantiate DAL component
        With New CampusGroupsDB
            '   get the dataset with all CampusGroups
            '
            Return .GetCampusDetailsByCampusId(campusId)
        End With
    End Function
    Public Function GetAllCampusEnrollmentByCampus(ByVal CampusId As String) As DataSet
        '   Instantiate DAL component
        With New CampusGroupsDB
            '   get the dataset with all CampusGroups
            Return .GetAllCampusEnrollmentByCampus(CampusId)
        End With

    End Function

    Public Function GetTermByCampus(ByVal CampusId As String) As DataSet
        '   Instantiate DAL component
        With New CampusGroupsDB
            '   get the dataset with all CampusGroups
            Return .GetTermByCampus(CampusId)
        End With
    End Function
    Public Function GetCampusesByUser(ByVal userId As String) As DataSet

        '   Instantiate DAL component
        With New CampusGroupsDB

            '   get the dataset with all Campuses available to this Employee
            Return .GetCampusesByUser(userId)

        End With

    End Function
    Public Function GetCampusDescrip(ByVal campusId As String) As String
        With New CampusGroupsDB
            Return .GetCampusDescrip(campusId)
        End With
    End Function
    Public Function GetCampusDataAsReceiptAddress(ByVal campusId As String) As ReceiptAddress
        With New CampusGroupsDB
            ''  Return .GetCampusDataAsReceiptAddress(campusId)
            ''Modified by saraswathi lakshmanan on April 13 2010
            ''Converted to stored Procedure
            Return .GetCampusDataAsReceiptAddress_sp(campusId)

        End With
    End Function
    Public Function InsertCampusIntoAllCampusGroup(ByVal campusid As String, ByVal Code As String, ByVal sStatusId As String, ByVal Description As String, ByVal user As String) As String
        With New CampusGroupsDB
            Return .InsertCampusIntoAllCampusGroup(campusid, Code, sStatusId, Description, user)
        End With
    End Function

    Public Function UpdateCampusBranch(ByVal campusId As String, isBranch As Boolean, Optional parentCampusId As Guid? = Nothing) As Boolean
        With New CampusGroupsDB
             Return .UpdateCampusBranch(campusId, isBranch, parentCampusId)
        End With
    End Function
    Public Function DeleteCampusFrmCampGrps(ByVal CampusId As String)
        With New CampusGroupsDB
            Return .DeleteCampusFrmCampGrps(CampusId)
        End With
    End Function
    Public Function GetAllCampGroupsByCampusId(ByVal campusId As String) As DataSet
        Return (New CampusGroupsDB).GetAllCampGroupsByCampusId(campusId)
    End Function
    Public Function GetCorporateInfoFromCampusId(ByVal campusId As String) As CorporateInfo
        Dim obj As New CampusGroupsDB
        Dim corpInfo As CorporateInfo
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        corpInfo = obj.GetCorporateInfoFromCampusId(campusId)

        With corpInfo
            If .Phone <> "" Then
                .Phone = facInputMasks.ApplyMask(strMask, .Phone)
            End If
            If .Fax <> "" Then
                .Fax = facInputMasks.ApplyMask(strMask, .Fax)
            End If
        End With

        Return corpInfo
    End Function
    Public Function GetCorporateInfoFromCampusId_SP(ByVal campusId As String) As CorporateInfo
        Dim obj As New CampusGroupsDB
        Dim corpInfo As CorporateInfo
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        corpInfo = obj.GetCorporateInfoFromCampusId_SP(campusId)

        With corpInfo
            If .Phone <> "" Then
                .Phone = facInputMasks.ApplyMask(strMask, .Phone)
            End If
            If .Fax <> "" Then
                .Fax = facInputMasks.ApplyMask(strMask, .Fax)
            End If
        End With

        Return corpInfo
    End Function
    Public Function CampusHasDeferredRevenuePostings(ByVal campusid) As Boolean
        Dim db As New CampusGroupsDB

        Return db.CampusHasDeferredRevenuePostings(campusid)



    End Function
    Public Function GetAllCampus_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable
        Dim db As New CampusGroupsDB
        Return db.GetAllCampus_SP(showActiveOnly, campusId)
    End Function
    '' Code Added by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
    Public Function GetAllCampuses() As DataSet
        Dim db As New CampusGroupsDB
        Return db.GetAllCampuses()
    End Function
End Class
