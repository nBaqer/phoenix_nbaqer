Public Class EmployeeSearchFacade
    Public Function EmployeeSearchResults(ByVal strStudentID As String, ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strStudentStatus As String, ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        With New EmployeeSearchDB
            ds = .EmployeeSearchResults(strStudentID, strLastName, strFirstName, strSSN, strStudentStatus, campusId)
        End With
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        'Dim zipMask As String
        Dim dr As DataRow

        'Get the mask for phone numbers and zip
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        ' Try
        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds
    End Function

    Public Function GetEmployeeID(ByVal Code As String) As String
        With New EmployeeSearchDB
            Return .GetEmployeeID(Code)
        End With
    End Function

    Public Function GetEmployeeCount(ByVal Code As String) As String
        With New EmployeeSearchDB
            Return .GetEmployeeCount(Code)
        End With
    End Function
    Public Function GetAllRolesUsedByEmployees() As DataSet

        '   Instantiate DAL component
        With New EmployeeSearchDB

            '   return Dataset
            Return .GetAllRolesUsedByEmployees()

        End With

    End Function
    Public Function GetAllEmployeeEmailsByEmployeeRole(ByVal rolesIdList(,) As String) As DataSet

        '   Instantiate DAL component
        With New EmployeeSearchDB

            '   return Dataset
            Return .GetAllEmployeeEmailsByEmployeeRole(rolesIdList)

        End With

    End Function
End Class
