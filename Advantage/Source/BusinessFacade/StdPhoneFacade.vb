Public Class StdPhoneFacade
    Public Function PopulateDataList(ByVal studentId As String, ByVal statusId As String) As DataSet
        Dim dr As DataRow
        Dim db As New StdPhoneDB
        Dim ds As New DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String


        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        ds = db.GetStudentPhones(studentId)

        For Each dr In ds.Tables(0).Rows
            dr("Phone") = facInputMasks.ApplyMask(strMask, dr("Phone"))
        Next

        Return ds

    End Function
    Public Function DoesDefaultPhoneExist(ByVal StudentId As String, ByVal StudentPhoneId As String) As Integer
        Dim ds As New DataSet
        Dim db As New StdPhoneDB
        Dim Exists As Integer

        Exists = db.DoesDefaultPhoneExist(StudentId, StudentPhoneId)

        Return Exists
    End Function
    Public Function GetAllStudentPhones(ByVal studentId As String) As DataSet

        Dim db As New StdPhoneDB
        Dim ds As New DataSet
        ds = db.GetStudentPhones(studentId)

        Dim facInputMasks As New InputMasksFacade
        Dim PhoneMask As String
        Dim zipMask As String
        Dim dr As DataRow

        'Get the mask for phone numbers and zip
        PhoneMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        For Each dr In ds.Tables(0).Rows
            If dr("ForeignPhone").ToString = False And dr("Phone").ToString.Length = 10 Then
                dr("Phone") = facInputMasks.ApplyMask(PhoneMask, dr("Phone"))
            End If
            'If dr("Phone").ToString.Length >= 1 And dr("ForeignPhone").ToString = False Then
            '    dr("Phone") = facInputMasks.ApplyMask(PhoneMask, dr("Phone"))
            'End If
        Next

        Return ds
    End Function

    Public Function GetStudentPhoneInfo(ByVal StdPhoneId As String) As StuPhoneInfo

        '   Instantiate DAL component
        With New StdPhoneDB

            '   get the BankInfo
            Return .GetStudentPhoneInfo(StdPhoneId)

        End With

    End Function
    Public Function UpdateStudentPhone(ByVal StuPhoneInfo As StuPhoneInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New StdPhoneDB

            '   If it is a new account do an insert. If not, do an update
            If Not (StuPhoneInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddStudentPhone(StuPhoneInfo, user)
            Else
                '   return integer with update results
                Return .UpdateStudentPhone(StuPhoneInfo, user)
            End If

        End With

    End Function
    Public Function DeleteStudentPhone(ByVal StdPhoneId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New StdPhoneDB

            '   delete BankInfo ans return integer result
            Return .DeleteStudentPhone(StdPhoneId, modDate)

        End With

    End Function
End Class
