﻿

Public Class TaskManager

    Public Function GetAdmissionRepSchedule() As DataTable
        With New TaskManagerDB
            Return .GetAdmissionRepSchedule_SP()
        End With
    End Function

    Public Function GetAdmissionRepUserDetails(ByVal userID As String) As DataTable
        With New TaskManagerDB
            Return .GetAdmissionRepUserDetails_SP(userID)
        End With
    End Function

    Public Function GetTaskDetails() As DataTable
        With New TaskManagerDB
            Return .GetTaskDetails_SP()
        End With
    End Function
    Public Function GetUserIdFromUserName(ByVal UserName As String) As String
        Dim dt As DataTable
        Dim tm As New TaskManagerDB
        dt = tm.GetUserIDFromUserName_SP(UserName)
        If dt.Rows.Count >= 1 Then
            Return dt.Rows(0)(0).ToString
        End If
    End Function

    Public Function GetUsernameForTasks(ByVal OwnerId As String, ByVal currentuserId As String) As DataTable
        Dim tm As New TaskManagerDB
        Return tm.GetUsernames_fortasks(OwnerId, currentuserId)
    End Function

    Public Function GetUserTasks(ByVal CampusId As String, _
                                    ByVal AssignedById As String, _
                                    ByVal OwnerId As String, _
                                    ByVal ReId As String, _
                                    ByVal Priority As String, _
                                    ByVal Status As TaskStatus, _
                                    ByVal DueDateMin As String, _
                                    ByVal DueDateMax As String, Optional ByVal filterOthers As String = "", Optional ByVal OwnerIdForAssignedToMe As String = "") As DataSet
        Dim tm As New TaskManagerDB
        Return tm.GetAll(CampusId, AssignedById, OwnerId, ReId, Priority, Status, DueDateMin, DueDateMax, filterOthers, OwnerIdForAssignedToMe)
    End Function

    Public Function GetUsersList_ForwhomtheUsercanAssignTasksTo(ByVal UserID As String, Optional ByVal RoleID As String = "", Optional ByVal CampusDesc As String = "", Optional ByVal Fullname As String = "") As DataTable
        With New TaskManagerDB
            Return .GetUsersList_ForwhomtheUsercanAssignTasksTo(UserID, RoleID, CampusDesc, Fullname)
        End With
    End Function
    Public Function GetUsersCampusList_ForwhomtheUsercanAssignTasksTo(ByVal UserID As String, Optional ByVal RoleID As String = "") As DataTable
        With New TaskManagerDB
            Return .GetUsersCampusList_ForwhomtheUsercanAssignTasksTo(UserID, RoleID)
        End With
    End Function
    Public Function GetRoles_ForwhomtheUsercanAssignTasksTo(ByVal UserID As String) As DataTable
        With New TaskManagerDB
            Return .GetRoles_ForwhomtheUsercanAssignTasksTo(UserID)
        End With
    End Function

    Public Function InsertUsersPreferenceList(ByVal UserID As String, ByVal OtherUsersId As String, Optional ByVal ViewSettings As String = "") As String
        With New TaskManagerDB
            Return .InsertUsersPreferenceList(UserID, OtherUsersId, ViewSettings)
        End With
    End Function

    Public Function GetUserTaskCalendarDefaultView_users(ByVal UserID As String) As DataSet
        With New TaskManagerDB
            Return .GetUserTaskCalendarDefaultView_Users(UserID)
        End With
    End Function

    Public Function GetUserTaskCalendarDefaultView_usersCount(ByVal UserID As String) As Integer
        Dim ds As New DataSet
        With New TaskManagerDB
            ds = .GetUserTaskCalendarDefaultView_Users(UserID)
        End With
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows.Count
            Else
                Return 0
            End If
        Else
            Return 0
        End If


    End Function

    Public Function GetUserTaskCalendarDefaultView(ByVal UserID As String) As String
        With New TaskManagerDB
            Return .GetUserTaskCalendarDefaultView(UserID)
        End With
    End Function

    Public Function DeleteUsersPreferenceList(ByVal UserID As String) As String
        With New TaskManagerDB
            Return .DeleteUsersPreferenceList(UserID)
        End With
    End Function
End Class
