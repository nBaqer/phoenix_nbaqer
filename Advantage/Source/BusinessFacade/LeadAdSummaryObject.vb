Public Class LeadAdSummaryObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim leadList As New LeadMasterDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(leadList.GetLeadAdvertisementSummary(rptParamInfo))
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        Try
            If ds.Tables.Count = 3 Then
                Dim dtLeads As DataTable = ds.Tables(0)
                Dim dtCatSrcTtl As DataTable = ds.Tables(1)
                Dim dtCatTtl As DataTable = ds.Tables(2)
                Dim ttlInquiries As Integer

                'Compute percentage for leads table
                If dtLeads.Rows.Count > 0 Then
                    ttlInquiries = dtLeads.Compute("SUM(NumberOfInquiries)", "")
                End If
                For Each dr As DataRow In dtLeads.Rows
                    If dr.IsNull("SourceCategoryId") Then
                        dr("Category") = " None"
                    End If
                    If dr.IsNull("SourceTypeId") Then
                        dr("Source") = " None"
                    End If
                    If dr.IsNull("SourceAdvertisement") Then
                        dr("Advertisement") = " None"
                    End If
                    If ttlInquiries > 0 Then
                        dr("Percentage") = dr("NumberOfInquiries") / ttlInquiries * 100
                    End If
                Next
                '
                'Compute percentage for categories-sources table
                If dtCatSrcTtl.Rows.Count > 0 Then
                    ttlInquiries = dtCatSrcTtl.Compute("SUM(NumberOfInquiries)", "")
                End If
                For Each dr As DataRow In dtCatSrcTtl.Rows
                    If dr.IsNull("SourceCategoryId") Then
                        dr("Category") = " None"
                    End If
                    If dr.IsNull("SourceTypeId") Then
                        dr("Source") = " None"
                    End If
                    If ttlInquiries > 0 Then
                        dr("Percentage") = dr("NumberOfInquiries") / ttlInquiries * 100
                    End If
                Next
                '
                'Compute percentage for categories-sources table
                If dtCatTtl.Rows.Count > 0 Then
                    ttlInquiries = dtCatTtl.Compute("SUM(NumberOfInquiries)", "")
                End If
                For Each dr As DataRow In dtCatTtl.Rows
                    If dr.IsNull("SourceCategoryId") Then
                        dr("Category") = " None"
                    End If
                    If ttlInquiries > 0 Then
                        dr("Percentage") = dr("NumberOfInquiries") / ttlInquiries * 100
                    End If
                Next
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
