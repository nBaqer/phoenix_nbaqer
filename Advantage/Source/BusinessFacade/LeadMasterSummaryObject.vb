Public Class LeadMasterSummaryObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim leadList As New LeadMasterDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(leadList.GetLeadSummaryList(rptParamInfo))
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        Dim stuName As String
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String

        'Get the mask for phone numbers
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        Try
            ds.Tables(0).TableName = "LeadMasterSummary"
            ds.Tables(0).Columns.Add(New DataColumn("LeadName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("LeadCount", System.Type.GetType("System.Int32")))

            For Each dr As DataRow In ds.Tables(0).Rows
                dr("LeadCount") = ds.Tables(0).Rows.Count
                'Set up student name as: "LastName, FirstName MI."
                stuName = dr("LastName")
                If Not (dr("FirstName") Is System.DBNull.Value) Then
                    If dr("FirstName") <> "" Then
                        stuName &= ", " & dr("FirstName")
                    End If
                End If
                If Not (dr("MiddleName") Is System.DBNull.Value) Then
                    If dr("MiddleName") <> "" Then
                        stuName &= " " & dr("MiddleName") & "."
                    End If
                End If
                dr("LeadName") = stuName
                '
                'Apply mask.
                If Not (dr("Phone") Is System.DBNull.Value) Then
                    If Not (dr("ForeignPhone") Is System.DBNull.Value) Then
                        If dr("Phone") <> "" And dr("ForeignPhone") = False Then
                            'Domestic Phone
                            dr("Phone") = facInputMasks.ApplyMask(strMask, dr("Phone"))
                        Else
                            'International Phone
                            'dr("Phone") = dr("Phone")
                        End If
                    Else
                        'Domestic Phone
                        If dr("Phone") <> "" Then
                            dr("Phone") = facInputMasks.ApplyMask(strMask, dr("Phone"))
                        End If
                    End If
                End If
            Next

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
