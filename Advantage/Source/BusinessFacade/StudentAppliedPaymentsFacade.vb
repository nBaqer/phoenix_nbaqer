﻿Public Class StudentAppliedPaymentsFacade
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim stuAppPmt As New StudentAppliedPaymentsDB
        Dim ds As New DataSet
        ds = BuildReportSource(stuAppPmt.GetAppliedPaymentsForStudents(rptParamInfo), stuAppPmt.StudentIdentifier)
        
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim strName As String
        Dim facInputMasks As New InputMasksFacade
        'Get mask for SSN
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count = 1 Then
                For Each dr In ds.Tables(0).Rows
                    'Set up student name as: "LastName, FirstName MI."
                    strName = ""
                    If Not dr.IsNull("LastName") Then
                        If dr("LastName") <> "" Then
                            strName = dr("LastName")
                        End If
                    End If
                    If Not dr.IsNull("FirstName") Then
                        If dr("FirstName") <> "" Then
                            If strName <> "" Then
                                strName &= ", " & dr("FirstName")
                            Else
                                strName &= dr("FirstName")
                            End If
                        End If
                    End If
                    If Not dr.IsNull("MiddleName") Then
                        If dr("MiddleName") <> "" Then
                            If strName <> "" Then
                                strName &= " " & dr("MiddleName") & "."
                            Else
                                strName &= dr("MiddleName") & "."
                            End If
                        End If
                    End If
                    dr("StudentName") = strName
                    '
                    'Apply mask to SSN.
                    If StudentIdentifier = "SSN" Then
                        If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                            If dr("StudentIdentifier") <> "" Then
                                Dim temp As String = dr("StudentIdentifier")
                                dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                            End If
                        End If
                    End If
                    ''The Payment and Applied Amount should be greater than zero
                    ''Added by Saraswathi lakshmanan on Feb 7 2011
                    ''To fix rally case DE1058 Name: QA: Issues on the Payment Application report. 

                    If Not dr.IsNull("PaymentAmount") Then
                        If dr("PaymentAmount") < 0 Then
                            dr("PaymentAmount") = -1 * (dr("PaymentAmount"))
                        End If
                    End If

                    If Not dr.IsNull("AppliedPayment") Then
                        If dr("AppliedPayment") < 0 Then
                            dr("AppliedPayment") = -1 * (dr("AppliedPayment"))
                        End If
                    End If

                Next
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function
#End Region

End Class