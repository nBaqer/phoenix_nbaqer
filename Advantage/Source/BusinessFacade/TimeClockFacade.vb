' ===============================================================================
' TimeClockFacade.vb
' Business Logic for interacting with Time Clocks
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Text
Imports System.Web
Imports FAME.AdvantageV1.DataAccess.TimeClock
Imports FAME.AdvantageV1.Common.TimeClock
Imports FAME.AdvantageV1.Common.AR
Imports FAME.AdvantageV1.DataAccess.AR
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports System.IO
Imports System.Net
Imports System.Data
Imports System.Threading.Tasks
Imports FAME.Advantage.Common
Imports FAME.Advantage.Api.Library.Models
Imports Advantage.AFA.Integration
Imports FAME.Advantage.Api.Library.AcademicRecords
Imports FAME.Advantage.Api.Library.TimeClock
Imports FAME.Advantage.Api.Library.Models.TimeClock
Imports FAME.Advantage.Api.Library.Models.Common

Namespace TimeClock

    Public Class TimeClockFacade

#Region "Private Vars"
        Private _log As New StringBuilder()
        Private bOverWritePunches As Boolean = False
#End Region
#Region "Constructor"
        Public Sub New()

        End Sub

        ''' <summary>
        ''' Returns an empty datatable with the proper schema to store punches
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function GetBlankPunchesDataTable()
            Dim dt As New DataTable("Punches")
            dt.Columns.Add("BadgeId", Type.GetType("System.String"))
            dt.Columns.Add("ClockId", Type.GetType("System.String"))
            dt.Columns.Add("PunchTime", Type.GetType("System.DateTime"))
            dt.Columns.Add("PunchType", Type.GetType("System.Int32"))
            dt.Columns.Add("Status", Type.GetType("System.Int32"))
            dt.Columns.Add("StuEnrollId", Type.GetType("System.String"))
            dt.Columns.Add("ScheduleId", Type.GetType("System.String"))
            dt.Columns.Add("AdjPunchTime", Type.GetType("System.DateTime"))
            dt.Columns.Add("FromSystem", Type.GetType("System.Boolean"))
            dt.Columns.Add("ClsSectMeetingId", Type.GetType("System.String"))
            dt.Columns.Add("SpecialCode", Type.GetType("System.String"))
            Return dt
        End Function
#End Region

#Region "Main entry point"
        Public Function GetFileType(ByVal buf As String) As TimeClockDataType
            Dim lBuf() As String = buf.Split(vbCrLf)

            If lBuf(0).Length <= 27 Then
                Return TimeClockDataType.ZON580
            Else
                Return TimeClockDataType.CS2000
            End If

        End Function

        Public Function Run(ByVal buf As String, ByVal UserName As String, ByVal CampusId As String, ByVal FILENAME As String) As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim PunchData() As String = buf.Split(vbCrLf)
            If (Not PunchData(0).StartsWith("\A!") And Not PunchData(0).StartsWith("\A'") And Not PunchData(0).StartsWith("PN") And Not PunchData(0).StartsWith("01") And Not PunchData(0).StartsWith("02")) Then
                AddMsg("File Type ", String.Format("Invalid"))
                Return ""
            End If
            Dim type As TimeClockDataType = GetFileType(buf)
            AddMsg("Run", String.Format("Detected file type is {0}", type.ToString()))

            Dim dtPunches As DataTable = Me.ImportTimeClockData(buf, type)
            LogResultOfImport(dtPunches)

            If dtPunches Is Nothing Then
                AddMsg("Run", "Failed to process punches.  Processing has been terminated because the import method returned nothing")
                Return ""
            End If
            FillEnrollmentInfo(dtPunches, CampusId)
            DeletePunchesinImportFile(dtPunches)

            AddPunchesToDatabase(dtPunches)
            Dim res As String
            res = ProcessTotalDailyHours(UserName, CampusId)

            Dim StuEnroll As DataTable
            StuEnroll = dtPunches.DefaultView.ToTable(True, "StuEnrollId")

            If MyAdvAppSettings.AppSettings("TrackSapAttendance", CampusId).ToString.ToLower = "byday" Then
                '  RecalculateGraduationDate(StuEnroll)
                Dim list = StuEnroll.AsEnumerable().Select(Function(r) r.Field(Of String)("StuEnrollId")).ToList()
                RecalculateGraduationDateAPI(list, MyAdvAppSettings)

            End If

            If (Not HttpContext.Current.Session("AdvantageApiToken") Is Nothing) Then
                Try
                    Dim tokenResponse As TokenResponse = CType(HttpContext.Current.Session("AdvantageApiToken"), TokenResponse)
                    Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString
                    Dim bucket As Guid = Guid.Empty
                    Dim enrollments = StuEnroll.AsEnumerable().Select(Function(r) r.Field(Of String)("StuEnrollId")).Where(Function(x) Guid.TryParse(x, bucket)).Select(Function(y) bucket).Distinct().ToList()

                    Dim result = New PaymentPeriodHelper(connectionString, tokenResponse.ApiUrl, tokenResponse.Token, UserName) _
                            .PostPaymentPeriodAttendance(CampusId, enrollments)

                    If (enrollments.Count > 0) Then
                        TitleIVSapUpdate(enrollments)

                    End If

                Catch ex As Exception

                End Try

            End If
            UpdateAttendanceForStudentSummaryPageAdhoc(StuEnroll, UserName)
            If res = "" Then
                Dim ds As New DataSet
                Dim archmsg As String
                ds = GetTimeClockSourceandTargetLocations(CampusId)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        archmsg = MoveFileToArchiveFolder(ds, FILENAME)
                        If archmsg = "" Then
                            AddMsg("Archive File", "The imported time clock file is Archived ")
                        Else
                            AddMsg("Archive File", "Failed to Archive the imported time clock file ")
                        End If

                    Else
                        AddMsg("Archive File", "Failed to Archive the imported time clock file")
                        Return ""
                    End If
                Else
                    AddMsg("Archive File", "Failed to Archive the imported time clock file")
                    Return ""
                End If

            End If
            Return ""
        End Function

        Protected Sub UpdateAttendanceForStudentSummaryPageAdhoc(ByVal dt As DataTable, ByVal UserName As String)
            Dim errMsg As String = ""
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            If (Not HttpContext.Current.Session("AdvantageApiToken") Is Nothing) Then
                Try
                    Dim tokenResponse As TokenResponse = CType(HttpContext.Current.Session("AdvantageApiToken"), TokenResponse)
                    Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString
                    Dim stuEnrolId As Guid
                    For Each dr As DataRow In dt.Rows
                        If Not dr("StuEnrollId") Is System.DBNull.Value Then
                            If (Guid.TryParse(dr("StuEnrollId"), stuEnrolId)) Then
                                Dim result = New PaymentPeriodHelper(connectionString, tokenResponse.ApiUrl, tokenResponse.Token, UserName).GetAttendanceSummary(stuEnrolId)
                            End If
                        End If
                    Next
                Catch ex As Exception

                End Try

            End If

        End Sub
        Public Function Run(ByVal buf As String, ByVal type As TimeClockDataType, ByVal UserName As String, ByVal CampusId As String, ByVal FILENAME As String) As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim PunchData() As String = buf.Split(vbCrLf)
            If type = TimeClockDataType.CS2000 Then
                If (Not PunchData(0).StartsWith("\A!") And Not PunchData(0).StartsWith("PN")) Then
                    AddMsg("File Type ", String.Format("Invalid"))
                    Return ""
                End If
            ElseIf type = TimeClockDataType.ZON580 Then
                If (Not PunchData(0).StartsWith("01") And Not PunchData(0).StartsWith("02")) Then
                    AddMsg("File Type ", String.Format("Invalid"))
                    Return ""
                End If
            End If


            Dim dtPunches As DataTable = Me.ImportTimeClockData(buf, type)
            If dtPunches Is Nothing Then
                AddMsg("Run", "Failed to process punches.  Processing has been terminated because the import method returned nothing")
                Return ""
            End If
            FillEnrollmentInfo(dtPunches, CampusId)
            DeletePunchesinImportFile(dtPunches)
            AddPunchesToDatabase(dtPunches)
            Dim res As String
            res = ProcessTotalDailyHours(UserName, CampusId)

            Dim StuEnroll As DataTable
            StuEnroll = dtPunches.DefaultView.ToTable(True, "StuEnrollId")
            If MyAdvAppSettings.AppSettings("TrackSapAttendance", CampusId).ToString.ToLower = "byday" Then
                RecalculateGraduationDate(StuEnroll)
            End If

            If (Not HttpContext.Current.Session("AdvantageApiToken") Is Nothing) Then
                Try
                    Dim bucket As Guid = Guid.Empty

                    Dim enrollments = StuEnroll.AsEnumerable().Select(Function(r) r.Field(Of String)("StuEnrollId")).Where(Function(x) Guid.TryParse(x, bucket)).Select(Function(y) bucket).Distinct().ToList()

                    If (enrollments.Count > 0) Then
                        TitleIVSapUpdate(enrollments)

                    End If

                    PostPaymentPeriodAttendanceAFA(CampusId, enrollments, MyAdvAppSettings, UserName)


                Catch ex As Exception

                End Try

            End If
            UpdateAttendanceForStudentSummaryPageAdhoc(StuEnroll, UserName)

            Return ""
        End Function

        Private Function MoveFileToArchiveFolder(ByVal dsTimeClock As DataSet, ByVal Filename As String) As String


            Dim strMessage As String = ""
            Dim strSourceFile As String
            Dim strTargetFile As String
            If dsTimeClock.Tables(0).Rows(0)("IsRemoteServer").ToString.ToLower = "true" Then
                Dim sFln As String = ""
                Dim TflN As String = ""

                sFln = dsTimeClock.Tables(0).Rows(0)("TCSourcePath").ToString.ToLower
                TflN = dsTimeClock.Tables(0).Rows(0)("TCTargetPath").ToString.ToLower
                If Not sFln.EndsWith("\") Then
                    sFln = sFln + "\"
                End If
                If Not TflN.EndsWith("\") Then
                    TflN = TflN + "\"
                End If
                strSourceFile = sFln + Filename
                strTargetFile = TflN + Filename


            Else
                Dim uriSource As New Uri(dsTimeClock.Tables(0).Rows(0)("TCSourcePath").ToString.ToLower)
                Dim uriTarget As New Uri(dsTimeClock.Tables(0).Rows(0)("TCTargetPath").ToString.ToLower)
                Dim SFln1 As String
                Dim TFln1 As String

                SFln1 = uriSource.AbsolutePath
                TFln1 = uriTarget.AbsolutePath

                If Not SFln1.EndsWith("/") Then
                    SFln1 = SFln1 + "/"
                End If
                If Not TFln1.EndsWith("/") Then
                    TFln1 = TFln1 + "/"
                End If
                strSourceFile = SFln1 + Filename
                strTargetFile = TFln1 + Filename
            End If
            Try
                If File.Exists(strSourceFile) Then
                    RenameExistingFile(strTargetFile)
                    File.Move(strSourceFile, strTargetFile)
                    Return ""
                End If
            Catch e As System.Exception
                Return "The Process cannot access the file " & Filename & " because it is being used by another process."
            End Try
            Return ""
        End Function
        Private Sub RenameExistingFile(ByVal strTargetPathWithFile As String)
            Dim sMessage As String = ""
            Dim strRandomNumber As New Random
            Dim rightNow As DateTime = DateTime.Now
            Dim s As String
            Dim RandomClass As New Random()
            s = "_" + rightNow.ToString("MMddyyyyhhmmtt")
            Dim strTargetPathWithNewFile As String

            Dim pos As Integer = strTargetPathWithFile.LastIndexOf(".")
            strTargetPathWithNewFile = strTargetPathWithFile.Substring(0, pos) + s + strTargetPathWithFile.Substring(pos, strTargetPathWithFile.Length - pos)

            If File.Exists(strTargetPathWithFile) Then
                If Not File.Exists(strTargetPathWithNewFile) Then
                    File.Move(strTargetPathWithFile, strTargetPathWithNewFile)
                Else
                    File.Move(strTargetPathWithFile, strTargetPathWithNewFile + RandomClass.Next(100).ToString)
                End If

            End If
        End Sub


        Public ReadOnly Property Log() As String
            Get
                Return Me._log.ToString()
            End Get
        End Property
#End Region

#Region "Processing"
        ''' <summary>
        ''' Fills enrollment and schedule info into the punches datatable
        ''' passed in as the "dt" argument.
        ''' </summary>
        ''' <param name="dt"></param>
        ''' <remarks></remarks>
        Protected Sub FillEnrollmentInfo(ByVal dt As DataTable, ByVal campusId As String)
            AddMsg("FillEnrollmentInfo", "Relating badges to student enrollment IDs")
            Dim dtStudents As DataTable = TimeClockDB.GetActiveStudents(campusId)

            For Each dr As DataRow In dt.Rows
                dr("Status") = PunchStatus.Inserted
                dr("FromSystem") = False

                Dim strBatchId As String = ""

                Dim sql As String = String.Format("BadgeNumber='{0}' AND CampusId ='{1}'", {trimLeadingChar("0", dr("BadgeId").ToString()), campusId.Trim()})
                Dim drs() As DataRow = dtStudents.Select(sql.ToString.Trim)
                Dim countDt As DataTable = New TimeClockDB().GetCountOfActiveEnrollment(trimLeadingChar("0", dr("BadgeId").ToString()), campusId)
                Dim count As Integer = 0
                If Not countDt Is Nothing Then
                    If countDt.Rows.Count > 0 Then
                        If (Not countDt.Rows(0)("CountOfActiveEnrollments") Is DBNull.Value) Then
                            count = CType(countDt.Rows(0)("CountOfActiveEnrollments").ToString(), Integer)
                        End If
                    End If
                End If

                If count = 0 Then
                    AddMsg("FillEnrollmentInfo", String.Format("No student found for BadgeNumber={0} or student out of school or Time clock schedule not available, PunchTime={1}",
                                     dr("BadgeId").ToString.PadLeft(9, "0"c), dr("PunchTime").ToString()))
                ElseIf count > 1 Then
                    AddMsg("FillEnrollmentInfo", "Found multiple students for BadgeNumber=" + dr("BadgeId").ToString.PadLeft(9, "0"c))
                Else
                    If (drs.Length = 0) Then
                        AddMsg("FillEnrollmentInfo", "Student with BadgeNumber = " + dr("BadgeId").ToString.PadLeft(9, "0"c) + " is not assigned to a program version with a time clock schedule")
                    Else
                        dr("StuEnrollId") = drs(0)("StuEnrollId").ToString()
                        dr("ScheduleId") = drs(0)("ScheduleId").ToString()
                    End If

                End If
            Next
            AddMsg("FillEnrollmentInfo", "Finished relating badges to student enrollment IDs")
        End Sub
        Public Function trimLeadingChar(ByVal charToRemove As String, ByVal stringToFix As String) As String
            Dim lngLC As Long
            For lngLC = 1 To Len(stringToFix)
                If Mid(stringToFix, lngLC, 1) <> Left(charToRemove, 1) Then
                    trimLeadingChar = Right(stringToFix, Len(stringToFix) - lngLC + 1)
                    Return trimLeadingChar
                    Exit Function
                End If
            Next lngLC
            trimLeadingChar = ""
        End Function
        ''' <summary>
        ''' returns a collection of TimeClockPunchInfo objects that can be used
        ''' in Freedom-like code.
        ''' </summary>
        ''' <param name="StuEnrollId"></param>
        ''' <param name="curDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function GetPunches(ByVal dt As DataTable, ByVal StuEnrollId As String, ByVal curDate As DateTime) As Collection
            Dim sql As String = String.Format("StuEnrollId='{0}' and PunchTime>='{1}' and PunchTime<'{2}'",
                                    StuEnrollId, curDate.ToShortDateString(), curDate.AddDays(1).ToShortDateString())
            Dim drsPunches() As DataRow = dt.Select(sql, "PunchTime ASC")

            Dim col As New Collection
            For Each dr As DataRow In drsPunches
                Dim info As TimeClockPunchInfo = Me.ToTimeClockPunchInfo(dr)
                col.Add(info)
            Next
            Return col
        End Function

        Protected Function GetPunchesDR(ByVal dt As DataTable, ByVal StuEnrollId As String, ByVal curDate As DateTime) As DataRow()
            Dim sql As String = String.Format("StuEnrollId='{0}' and PunchTime>='{1}' and PunchTime<'{2}'",
                                    StuEnrollId, curDate.ToShortDateString(), curDate.AddDays(1).ToShortDateString())
            Return dt.Select(sql, "PunchTime ASC")
        End Function


        Protected Function GetPunchesDR_ByClass(ByVal dt As DataTable, ByVal StuEnrollId As String, ByVal curDate As DateTime, ByVal ClsSectmeetingID As String) As DataRow()
            Dim sql As String = String.Format("StuEnrollId='{0}' and ClsSectMeetingID='{1}' and PunchTime>='{2}' and PunchTime<'{3}'",
                                    StuEnrollId, ClsSectmeetingID, curDate.ToShortDateString(), curDate.AddDays(1).ToShortDateString())
            Return dt.Select(sql, "PunchTime ASC")
        End Function

        ''' <summary>
        ''' Commits all the punches previosly loaded into dt
        ''' to the database.  "dt" is usually filled by ImportTimeClockData.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Sub AddPunchesToDatabase(ByVal dt As DataTable)
            For Each dr As DataRow In dt.Rows
                Dim info As TimeClockPunchInfo = ToTimeClockPunchInfo(dr)
                If (Not String.IsNullOrEmpty(info.StuEnrollId)) Then
                    Dim res As String = TimeClockDB.AddPunch(info)
                    If res <> "" AndAlso bOverWritePunches Then
                        TimeClockDB.UpdatePunch(info)
                    End If
                End If
            Next
        End Sub
        Protected Sub AddPunchesToDatabase_byClass(ByVal dt As DataTable, ByVal CampusID As String, ByRef ExceptionMessages As StringBuilder)
            For Each dr As DataRow In dt.Rows

                Dim info As TimeClockPunchInfo = ToTimeClockPunchInfo_ByClass(dr)

                Dim res As String = TimeClockDB.AddPunch_byClass(info, CampusID, ExceptionMessages)
                If res <> "" AndAlso bOverWritePunches Then
                    TimeClockDB.UpdatePunch(info)
                End If
            Next
        End Sub

        Protected Sub RecalculateGraduationDate(ByVal dt As DataTable)
            Dim errMsg As String = ""
            For Each dr As DataRow In dt.Rows
                Dim dtfacade As New FAME.AdvantageV1.BusinessRules.GetDateFromHoursBR
                Dim UseTimeClock As Boolean = False
                If Not dr("StuEnrollId") Is System.DBNull.Value Then
                    Dim dtExpectedDate As DateTime = dtfacade.GetDateFromHours(dr("StuEnrollId").ToString, errMsg, UseTimeClock, HttpContext.Current.Request.Params("cmpid").ToString)
                    If errMsg = "" And Not dtExpectedDate = DateTime.MaxValue And UseTimeClock = True Then
                        Dim upDateExpGradDatefacade As New StudentEnrollmentFacade
                        upDateExpGradDatefacade.UpdateExpectedGraduationDate(dtExpectedDate, dr("StuEnrollId").ToString)
                    End If
                End If
            Next
        End Sub
        Public Sub RecalculateGraduationDateAPI(ByVal StuEnroll As List(Of String), MyAdvAppSettings As AdvAppSettings)

            If (Not HttpContext.Current.Session("AdvantageApiToken") Is Nothing AndAlso Not StuEnroll Is Nothing AndAlso StuEnroll.Any()) Then
                Try
                    Dim tokenResponse As TokenResponse = CType(HttpContext.Current.Session("AdvantageApiToken"), TokenResponse)
                    Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString
                    Dim recalcGradDate = New RecalcGradDateRequest(tokenResponse.ApiUrl, tokenResponse.Token)
                    Dim result = recalcGradDate.UpdateGraduationDate(StuEnroll)

                Catch ex As Exception

                End Try

            End If

        End Sub

        Protected Sub DeletePunchesinImportFile(ByVal dt As DataTable)
            For Each dr As DataRow In dt.Rows
                Dim info As TimeClockPunchInfo = ToTimeClockPunchInfo(dr)
                If (Not String.IsNullOrEmpty(info.StuEnrollId)) Then
                    Dim res As String = TimeClockDB.UpdateAllPunchesToDuplicate(info)
                End If
            Next
        End Sub


        ''' <summary>
        ''' Computes total hours given all the new punches
        ''' </summary>
        ''' <remarks></remarks>
        Protected Function ProcessTotalDailyHours(ByVal UserName As String, ByVal CampusID As String) As String
            Dim dtNewPunches As New DataTable
            Dim dt As DataTable = TimeClockDB.GetPunches(PunchStatus.Inserted)
            Dim dv As DataView = New DataView(dt, "", "StuEnrollId, PunchTime ASC, PunchType ASC", DataViewRowState.None)
            dtNewPunches = dt.Clone

            Dim lastStuEnrollId As String = ""

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            For Each dr As DataRow In dv.Table.Rows
                Dim StuEnrollId As String = dr("StuEnrollId").ToString()
                Dim ScheduleId As String = dr("ScheduleId").ToString()
                Dim StuStartDate As String = dr("StartDate").ToString
                Dim StuGradDate As String = dr("ExpGradDate").ToString
                If StuEnrollId <> lastStuEnrollId Then
                    Dim schedInfo As ScheduleInfo = SchedulesDB.GetInfo(ScheduleId)
                    Dim sdInfo() As ScheduleDetailInfo = schedInfo.Details()
                    Dim sql As String = String.Format("StuEnrollId='{0}'", StuEnrollId)
                    Dim minDate As DateTime = dt.Compute("min(PunchTime)", sql)
                    Dim maxDate As DateTime = dt.Compute("max(PunchTime)", sql)
                    Dim curDate As DateTime = minDate

                    While curDate <= maxDate
                        Dim InPunches As Integer = 0
                        Dim OutPunches As Integer = 0
                        Dim CanPostonHoliday As Boolean = False
                        Dim StatusSet As Boolean = False

                        Dim dow As Integer = CType(curDate.DayOfWeek, Integer)
                        Dim drPunches() As DataRow = GetPunchesDR(dt, StuEnrollId, curDate)

                        Dim IsLOADate As Boolean
                        Dim IsSuspendedDate As Boolean
                        Dim inti As Integer
                        If curDate > Date.Now Then
                            For inti = 0 To drPunches.Length - 1
                                drPunches(inti)("Status") = PunchStatus.NotScheduled
                            Next
                            If drPunches.Length > 0 Then
                                StatusSet = True
                                AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                AddMsg("Date", curDate)
                                AddMsg("Not imported. Since,", "Future date is given")
                            End If

                        End If

                        If StatusSet = False Then
                            IsLOADate = (New AttendanceFacade).IsStudentLOADate(StuEnrollId, curDate)
                            If IsLOADate = True Then
                                For inti = 0 To drPunches.Length - 1
                                    drPunches(inti)("Status") = PunchStatus.NotScheduled
                                Next
                                If drPunches.Length > 0 Then
                                    StatusSet = True
                                    AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                    AddMsg("Date", curDate)
                                    AddMsg("Not imported. Since, ", "Student is in LOA")
                                End If
                            End If
                        End If
                        If StatusSet = False Then
                            IsSuspendedDate = (New AttendanceFacade).IsStudentSuspendedDate(StuEnrollId, curDate)
                            If IsSuspendedDate = True Then
                                For inti = 0 To drPunches.Length - 1
                                    drPunches(inti)("Status") = PunchStatus.NotScheduled
                                Next
                                If drPunches.Length > 0 Then
                                    StatusSet = True
                                    AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                    AddMsg("Date", curDate)
                                    AddMsg("Not imported. Since, Student ", " is Suspended")
                                End If
                            End If
                        End If
                        If StatusSet = False Then
                            If curDate < StuStartDate Then
                                For inti = 0 To drPunches.Length - 1
                                    drPunches(inti)("Status") = PunchStatus.NotScheduled
                                Next
                                If drPunches.Length > 0 Then
                                    StatusSet = True
                                    AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                    AddMsg("Date", curDate)
                                    AddMsg("Not imported. Since, Student", " Punch is before StartDate")
                                End If

                            End If
                        End If

                        If IsHoliday(curDate) Then
                            If Not MyAdvAppSettings.AppSettings("postattendanceonholiday") Is Nothing Then
                                If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                                    If StatusSet = False Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.NotScheduled
                                        Next
                                        If drPunches.Length > 0 Then
                                            StatusSet = True
                                            AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                            AddMsg("Date", curDate)
                                            AddMsg("Not imported. Since, Student", " Punch is on a Holiday")
                                        End If
                                    End If
                                ElseIf MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "yes" Then
                                    CanPostonHoliday = True
                                End If
                            Else
                                If StatusSet = False Then
                                    For inti = 0 To drPunches.Length - 1
                                        drPunches(inti)("Status") = PunchStatus.NotScheduled
                                    Next
                                    If drPunches.Length > 0 Then
                                        StatusSet = True
                                        AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Date", curDate)
                                        AddMsg("Not imported. Since, Student", " Punch is on a Holiday")
                                    End If
                                End If
                            End If
                        End If

                        If StatusSet = False Then
                            If drPunches.Length > 0 Then
                                For inti = 0 To drPunches.Length - 1
                                    If CType(drPunches(inti)("PunchType"), TimeClockPunchType) = TimeClockPunchType.PunchIn Then
                                        InPunches = InPunches + 1
                                    ElseIf CType(drPunches(inti)("PunchType"), TimeClockPunchType) = TimeClockPunchType.PunchOut Then
                                        OutPunches = OutPunches + 1
                                    End If
                                Next inti
                                If StatusSet = False Then
                                    If OutPunches = 0 Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.Invalid
                                        Next
                                        StatusSet = True
                                        AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Date", curDate)
                                        AddMsg("Not imported. Since, Student ", " Punch Out is not found")
                                    End If
                                End If
                                If StatusSet = False Then
                                    If InPunches = 0 Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.Invalid
                                        Next
                                        StatusSet = True
                                        AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Date", curDate)
                                        AddMsg("Not imported. Since, Student ", " Punch In is not found")
                                    End If
                                End If
                                If StatusSet = False Then
                                    If InPunches <> OutPunches Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.Invalid
                                        Next
                                        StatusSet = True
                                        AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Date", curDate)
                                        AddMsg("Not imported. Since, Student", " Punch In and Punch Out does not match")
                                    End If
                                End If
                                If StatusSet = False Then
                                    If CType(drPunches(0)("PunchType"), TimeClockPunchType) = TimeClockPunchType.PunchOut Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.OutofOrder
                                        Next
                                        StatusSet = True
                                        AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Date", curDate)
                                        AddMsg("Not imported. Since, Student", " Punch is not in order")
                                    End If
                                End If
                                Dim IsOutofOrder As Boolean = False
                                For inti = 0 To drPunches.Length - 1
                                    If inti + 1 <= drPunches.Length - 1 Then
                                        If drPunches(inti)("Id") > drPunches(inti + 1)("Id") Then
                                            IsOutofOrder = True
                                        End If
                                    End If
                                Next inti
                                If StatusSet = False Then
                                    If IsOutofOrder = True Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.OutofOrder
                                        Next
                                        If drPunches.Length > 0 Then
                                            StatusSet = True
                                            AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                            AddMsg("Date", curDate)
                                            AddMsg("Not imported. Since, Student", " Punch is Out of Order")
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        If drPunches.Length >= 2 Then
                            If drPunches(0)("Status") <> PunchStatus.Invalid And drPunches(0)("Status") <> PunchStatus.NotScheduled And drPunches(0)("Status") <> PunchStatus.OutofOrder Then
                                Dim totHrs As Decimal
                                If CanPostonHoliday = True Then
                                    totHrs = GetTotalDailyHoursForTimeClock(drPunches, sdInfo(dow))
                                Else
                                    totHrs = GetTotalDailyHours(drPunches, sdInfo(dow))

                                End If

                                Dim dtProcessPunches As New DataTable
                                dtProcessPunches = dt.Clone
                                Dim drdata As DataRow
                                For i As Integer = 0 To drPunches.Length - 1
                                    drdata = dtProcessPunches.NewRow()
                                    drdata("BadgeId") = drPunches(i)("BadgeId")
                                    drdata("ClockId") = drPunches(i)("ClockId")
                                    drdata("StuEnrollId") = drPunches(i)("StuEnrollId")
                                    drdata("PunchType") = drPunches(i)("PunchType")
                                    drdata("PunchTime") = drPunches(i)("PunchTime")
                                    drdata("FromSystem") = drPunches(i)("FromSystem")
                                    drdata("Status") = drPunches(i)("Status")
                                    dtProcessPunches.Rows.Add(drdata)

                                Next

                                AddMsg("ComputeHours", String.Format("{0} [{1}] Total Hours = {2}",
                                            drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c), curDate.ToShortDateString(), totHrs))
                                Dim res As String
                                If CanPostonHoliday = True Then
                                    res = TimeClockDB.SaveTotalDailyHours(curDate.ToShortDateString(), StuEnrollId, ScheduleId, totHrs, sdInfo(dow).Check_TardyIn, 0, schedInfo.PrgVerId, UserName, CampusID)
                                    CanPostonHoliday = False
                                Else
                                    res = TimeClockDB.SaveTotalDailyHours(curDate.ToShortDateString(), StuEnrollId, ScheduleId, totHrs, sdInfo(dow).Check_TardyIn, sdInfo(dow).Total, schedInfo.PrgVerId, UserName, CampusID)

                                End If

                                If res <> "" Then
                                    AddMsg("ComputeHours", res)
                                End If
                                Dim ProcessArr() As TimeClockPunchInfo = ToTimeClockPunchInfoArray(dtProcessPunches)
                                For Each info As TimeClockPunchInfo In ProcessArr
                                    If Not info.Status = PunchStatus.Invalid And Not info.Status = PunchStatus.NotScheduled And Not info.Status = PunchStatus.OutofOrder Then
                                        info.Status = PunchStatus.Ok
                                    End If

                                Next
                                Dim Processres As String = TimeClockDB.SaveTimeClockPunches(ProcessArr)
                                If Processres <> "" Then
                                    AddMsg("ComputeHours:SaveTimeClockPunches", Processres)
                                End If
                            End If
                        End If
                        TimeClockDB.SetPunchStatus(drPunches)
                        curDate = curDate.AddDays(1)
                    End While
                End If
                lastStuEnrollId = StuEnrollId
            Next

            Dim infoArr() As TimeClockPunchInfo = ToTimeClockPunchInfoArray(dtNewPunches)
            For Each info As TimeClockPunchInfo In infoArr
                If Not info.Status = PunchStatus.Invalid And Not info.Status = PunchStatus.NotScheduled And Not info.Status = PunchStatus.OutofOrder Then
                    info.Status = PunchStatus.Ok
                End If

            Next
            Dim res2 As String = TimeClockDB.SaveTimeClockPunches(infoArr)
            If res2 = "" Then
                AddMsg("ComputeHours:SaveTimeClockPunches", "Ok")
            Else
                AddMsg("ComputeHours:SaveTimeClockPunches", res2)
            End If
            Return res2

        End Function
        Private Function IsHoliday(ByVal hDate As Date, Optional ByVal CampusID As String = "", Optional ByVal StartTime As String = "", Optional ByVal EndTime As String = "") As Boolean
            Dim dsHols As New DataSet
            dsHols = (New HolidayFacade).GetAllHolidays(CampusID)

            For i As Integer = 0 To dsHols.Tables(0).Rows.Count - 1
                If hDate >= dsHols.Tables(0).Rows(i)("HolidayStartDate") And hDate <= dsHols.Tables(0).Rows(i)("HolidayEndDate") Then
                    If dsHols.Tables(0).Rows(i)("AllDay") = False Then
                        If StartTime <> "" And EndTime <> "" Then
                            If (StartTime >= dsHols.Tables(0).Rows(i)("StartTime") And StartTime <= dsHols.Tables(0).Rows(i)("EndTime")) Or (EndTime >= dsHols.Tables(0).Rows(i)("StartTime") And EndTime <= dsHols.Tables(0).Rows(i)("EndTime")) Then
                                Return True
                            ElseIf (StartTime <= dsHols.Tables(0).Rows(i)("StartTime") And EndTime >= dsHols.Tables(0).Rows(i)("EndTime")) Then
                                Return True
                            ElseIf (StartTime >= dsHols.Tables(0).Rows(i)("StartTime") And EndTime <= dsHols.Tables(0).Rows(i)("EndTime")) Then
                                Return True
                            End If
                        Else
                            Return True

                        End If

                    Else
                        Return True

                    End If
                End If
            Next
            Return False
        End Function
        Private Function GetTotalDailyHours(ByRef drPunches() As DataRow, ByVal schedInfo As ScheduleDetailInfo) As Decimal
            Dim bIsClosedDay As Boolean = False
            If schedInfo.TimeIn = DateTime.MinValue Or schedInfo.TimeOut = DateTime.MinValue Then
                Dim tothr As Decimal
                tothr = GetTotalDailyHoursForTimeClock(drPunches, schedInfo)
                Return tothr
            End If
            If drPunches.Length = 0 Then
                Return 0.0
            End If
            For Each dr As DataRow In drPunches
                dr("Status") = PunchStatus.Ok
            Next

            If Not schedInfo.Allow_EarlyIn Then ParseForEarlyIn(drPunches, schedInfo)
            If Not schedInfo.Allow_LateOut Then ParseForLateOut(drPunches, schedInfo)

            If schedInfo.Check_TardyIn Then ParseForTardyIn(drPunches, schedInfo)

            If schedInfo.MaxNoLunch > 0 Then
                drPunches = ParseForMaxNoLunch(drPunches, schedInfo)
            End If

            Dim Studenthrs As Decimal

            If schedInfo.Allow_ExtraHours = False Then
                Studenthrs = GetTotalDailyHoursForTimeClock(drPunches, schedInfo)
                If Studenthrs > schedInfo.Total Then
                    ModifyPunchesBasedonScheduledHrs(drPunches, schedInfo)
                End If
            End If

            Dim totHrs As Decimal = 0.0
            Dim i As Integer = -1
            Dim drPunchIn As DataRow = GetNextPunchIn(drPunches, i)
            Dim drPunchOut As DataRow = GetNextPunchOut(drPunches, i)

            While drPunchIn IsNot Nothing AndAlso drPunchOut IsNot Nothing
                Dim diff As Decimal = DateDiff(DateInterval.Minute, drPunchIn("PunchTime"), drPunchOut("PunchTime")) / 60.0

                If totHrs + diff <= schedInfo.Total And schedInfo.Allow_ExtraHours = False Then
                    totHrs += diff
                ElseIf schedInfo.Allow_ExtraHours = True Then
                    totHrs += diff
                Else
                    totHrs = schedInfo.Total
                    drPunchIn("Status") = PunchStatus.TooLate
                    drPunchOut("Status") = PunchStatus.TooLate
                End If
                drPunchIn = GetNextPunchIn(drPunches, i)
                drPunchOut = GetNextPunchOut(drPunches, i)
            End While
            Return Math.Round(totHrs, 2)
        End Function


        Private Function GetTotalDailyHours_byClass(ByRef drPunches() As DataRow, ByVal schedInfo As ClsSectMeetingScheduledetailsInfo) As Decimal
            Dim bIsClosedDay As Boolean = False

            If schedInfo.TimeIn = DateTime.MinValue Or schedInfo.TimeOut = DateTime.MinValue Then
                Dim tothr As Decimal
                tothr = GetTotalDailyHoursForTimeClock_byClass(drPunches)
                Return tothr
            End If

            If drPunches.Length = 0 Then
                Return 0.0
            End If

            For Each dr As DataRow In drPunches
                dr("Status") = PunchStatus.Ok
            Next

            If Not schedInfo.Allow_EarlyIn Then ParseForEarlyIn_ByClass(drPunches, schedInfo)
            If Not schedInfo.Allow_LateOut Then ParseForLateOut_byClass(drPunches, schedInfo)

            If schedInfo.Check_TardyIn Then ParseForTardyIn_byClass(drPunches, schedInfo)

            Dim Studenthrs As Decimal

            If schedInfo.Allow_ExtraHours = False Then
                Studenthrs = GetTotalDailyHoursForTimeClock_byClass(drPunches)
                If Studenthrs > schedInfo.Total Then
                    ModifyPunchesBasedonScheduledHrs_ByClass(drPunches, schedInfo)
                End If
            End If


            Dim totHrs As Decimal = 0.0
            Dim i As Integer = -1
            Dim drPunchIn As DataRow = GetNextPunchIn(drPunches, i)
            Dim drPunchOut As DataRow = GetNextPunchOut(drPunches, i)

            While drPunchIn IsNot Nothing AndAlso drPunchOut IsNot Nothing
                Dim diff As Decimal = DateDiff(DateInterval.Minute, drPunchIn("PunchTime"), drPunchOut("PunchTime"))



                If totHrs + diff <= schedInfo.Total And schedInfo.Allow_ExtraHours = False Then
                    totHrs += diff
                ElseIf schedInfo.Allow_ExtraHours = True Then
                    totHrs += diff
                Else
                    totHrs = schedInfo.Total
                    drPunchIn("Status") = PunchStatus.TooLate
                    drPunchOut("Status") = PunchStatus.TooLate
                End If
                drPunchIn = GetNextPunchIn(drPunches, i)
                drPunchOut = GetNextPunchOut(drPunches, i)
            End While
            Return Math.Round(totHrs, 2)
        End Function


        Private Sub ModifyPunchesBasedonScheduledHrs(ByVal drPunches As DataRow(), ByVal schedInfo As ScheduleDetailInfo)

            Dim totHrs As Decimal = 0.0
            Dim i As Integer = -1
            Dim drPunchIn As DataRow = GetNextPunchIn(drPunches, i)
            Dim drPunchOut As DataRow = GetNextPunchOut(drPunches, i)
            Dim RemainingHrs As Decimal
            While drPunchIn IsNot Nothing AndAlso drPunchOut IsNot Nothing
                Dim diff As Decimal = DateDiff(DateInterval.Minute, drPunchIn("PunchTime"), drPunchOut("PunchTime")) / 60.0
                If totHrs + diff <= schedInfo.Total Then
                    totHrs += diff
                Else
                    RemainingHrs = schedInfo.Total - totHrs
                    drPunchOut("PunchTime") = DateAdd(DateInterval.Minute, Math.Round((RemainingHrs * 60)), drPunchIn("PunchTime"))
                    drPunchOut("FromSystem") = True

                    drPunchIn = GetNextPunchIn(drPunches, i)
                    drPunchOut = GetNextPunchOut(drPunches, i)
                    While drPunchIn IsNot Nothing AndAlso drPunchOut IsNot Nothing
                        drPunchIn("Status") = PunchStatus.TooLate
                        drPunchOut("Status") = PunchStatus.TooLate

                        drPunchIn = GetNextPunchIn(drPunches, i)
                        drPunchOut = GetNextPunchOut(drPunches, i)
                    End While
                    Exit Sub
                End If
                drPunchIn = GetNextPunchIn(drPunches, i)
                drPunchOut = GetNextPunchOut(drPunches, i)
            End While



        End Sub


        Private Sub ModifyPunchesBasedonScheduledHrs_ByClass(ByVal drPunches As DataRow(), ByVal schedInfo As ClsSectMeetingScheduledetailsInfo)

            Dim totHrs As Decimal = 0.0
            Dim i As Integer = -1
            Dim drPunchIn As DataRow = GetNextPunchIn(drPunches, i)
            Dim drPunchOut As DataRow = GetNextPunchOut(drPunches, i)
            Dim RemainingHrs As Decimal
            While drPunchIn IsNot Nothing AndAlso drPunchOut IsNot Nothing
                Dim diff As Decimal = DateDiff(DateInterval.Minute, drPunchIn("PunchTime"), drPunchOut("PunchTime"))
                If totHrs + diff <= schedInfo.Total Then
                    totHrs += diff
                Else
                    RemainingHrs = schedInfo.Total - totHrs
                    drPunchOut("PunchTime") = DateAdd(DateInterval.Minute, (RemainingHrs), drPunchIn("PunchTime"))
                    drPunchOut("FromSystem") = True

                    drPunchIn = GetNextPunchIn(drPunches, i)
                    drPunchOut = GetNextPunchOut(drPunches, i)
                    While drPunchIn IsNot Nothing AndAlso drPunchOut IsNot Nothing
                        drPunchIn("Status") = PunchStatus.TooLate
                        drPunchOut("Status") = PunchStatus.TooLate

                        drPunchIn = GetNextPunchIn(drPunches, i)
                        drPunchOut = GetNextPunchOut(drPunches, i)
                    End While
                    Exit Sub
                End If
                drPunchIn = GetNextPunchIn(drPunches, i)
                drPunchOut = GetNextPunchOut(drPunches, i)
            End While



        End Sub


        ''' <summary>
        ''' returns the next valid punch in
        ''' </summary>
        ''' <param name="drPunches"></param>
        ''' <param name="i"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function GetNextPunchIn(ByVal drPunches() As DataRow, ByRef i As Integer) As DataRow
            If i = drPunches.Length - 1 Then Return Nothing

            While i < drPunches.Length - 1
                If CType(drPunches(i + 1)("PunchType"), TimeClockPunchType) = TimeClockPunchType.PunchIn AndAlso
                    CType(drPunches(i + 1)("Status"), PunchStatus) <> PunchStatus.Invalid AndAlso
CType(drPunches(i + 1)("Status"), PunchStatus) <> PunchStatus.NotScheduled Then
                    i += 1
                    If i < drPunches.Length Then
                        Return drPunches(i)
                    Else
                        Return Nothing
                    End If
                End If
                i += 1
            End While
            Return Nothing
        End Function

        ''' <summary>
        ''' returns the next valid punch out
        ''' </summary>
        ''' <param name="drPunches"></param>
        ''' <param name="i"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function GetNextPunchOut(ByVal drPunches() As DataRow, ByRef i As Integer) As DataRow

            If i = drPunches.Length - 1 Then Return Nothing

            While i < drPunches.Length - 1
                If CType(drPunches(i + 1)("PunchType"), TimeClockPunchType) = TimeClockPunchType.PunchOut AndAlso
                    CType(drPunches(i + 1)("Status"), PunchStatus) <> PunchStatus.Invalid AndAlso
                    CType(drPunches(i + 1)("Status"), PunchStatus) <> PunchStatus.NotScheduled Then
                    i += 1
                    If i < drPunches.Length Then
                        Return drPunches(i)
                    Else
                        Return Nothing
                    End If
                End If
                i += 1
            End While
            Return Nothing
        End Function

        ''' <summary>
        ''' If Not AllowEarlyPunchIn and first punch of the day is less than
        ''' Schedule's TimeIn Then Set first punch of the day to Schedule's TimeIn
        ''' </summary>
        ''' <param name="drPunches"></param>
        ''' <param name="schedInfo"></param>
        ''' <remarks></remarks>
        Private Sub ParseForEarlyIn(ByVal drPunches() As DataRow, ByVal schedInfo As ScheduleDetailInfo)
            If drPunches.Length = 0 Then Return

            Dim i As Integer = -1
            Dim drPunchIn As DataRow = GetNextPunchIn(drPunches, i)
            Dim drPunchOut As DataRow = GetNextPunchOut(drPunches, i)

            If drPunchIn Is Nothing Or drPunchOut Is Nothing Then
                Return
            End If

            Do While GetTime(drPunchIn("PunchTime")) < GetTime(schedInfo.TimeIn)
                If GetTime(drPunchOut("PunchTime")) < GetTime(schedInfo.TimeIn) And schedInfo.Allow_ExtraHours = False Then

                    drPunchIn("Status") = PunchStatus.NotScheduled
                    drPunchOut("Status") = PunchStatus.NotScheduled

                    drPunchIn = GetNextPunchIn(drPunches, i)
                    drPunchOut = GetNextPunchOut(drPunches, i)
                    If drPunchIn Is Nothing Or drPunchOut Is Nothing Then
                        Exit Do
                    End If
                Else
                    Dim dtTmp As DateTime = drPunchIn("PunchTime")
                    drPunchIn("PunchTime") = New DateTime(dtTmp.Year, dtTmp.Month, dtTmp.Day,
                                            schedInfo.TimeIn.Hour, schedInfo.TimeIn.Minute, schedInfo.TimeIn.Second)
                    drPunchIn("FromSystem") = True
                End If
            Loop
        End Sub

        Private Sub ParseForEarlyIn_ByClass(ByVal drPunches() As DataRow, ByVal schedInfo As ClsSectMeetingScheduledetailsInfo)

            If drPunches.Length = 0 Then Return

            Dim i As Integer = -1
            Dim drPunchIn As DataRow = GetNextPunchIn(drPunches, i)
            Dim drPunchOut As DataRow = GetNextPunchOut(drPunches, i)

            If drPunchIn Is Nothing Or drPunchOut Is Nothing Then
                Return
            End If

            Do While GetTime(drPunchIn("PunchTime")) < GetTime(schedInfo.TimeIn)
                If GetTime(drPunchOut("PunchTime")) < GetTime(schedInfo.TimeIn) And schedInfo.Allow_ExtraHours = False Then
                    drPunchIn("Status") = PunchStatus.NotScheduled
                    drPunchOut("Status") = PunchStatus.NotScheduled

                    drPunchIn = GetNextPunchIn(drPunches, i)
                    drPunchOut = GetNextPunchOut(drPunches, i)
                    If drPunchIn Is Nothing Or drPunchOut Is Nothing Then
                        Exit Do
                    End If
                Else
                    Dim dtTmp As DateTime = drPunchIn("PunchTime")
                    drPunchIn("PunchTime") = New DateTime(dtTmp.Year, dtTmp.Month, dtTmp.Day,
                                            schedInfo.TimeIn.Hour, schedInfo.TimeIn.Minute, schedInfo.TimeIn.Second)
                    drPunchIn("FromSystem") = True
                End If
            Loop
        End Sub

        ''' <summary>
        ''' If Not AllowLatePunchOut and last punch of the day is greater than
        ''' Schedule's TimeOut Then Set last punch of the day to Schedule's TimeOut
        ''' </summary>
        ''' <param name="drPunches"></param>
        ''' <param name="schedInfo"></param>
        ''' <remarks></remarks>
        Private Sub ParseForLateOut(ByVal drPunches() As DataRow, ByVal schedInfo As ScheduleDetailInfo)

            If drPunches.Length = 0 Then Return

            Dim i As Integer = drPunches.Length - 2
            Dim drPunchIn As DataRow = GetNextPunchIn(drPunches, i - 1)
            Dim drPunchOut As DataRow = GetNextPunchOut(drPunches, i)

            If drPunchIn Is Nothing Or drPunchOut Is Nothing Then
                Return
            End If


            Do While GetTime(drPunchOut("PunchTime")) > GetTime(schedInfo.TimeOut)
                If GetTime(drPunchIn("PunchTime")) > GetTime(schedInfo.TimeOut) And schedInfo.Allow_ExtraHours = False Then

                    drPunchIn("Status") = PunchStatus.NotScheduled
                    drPunchOut("Status") = PunchStatus.NotScheduled
                    i -= 2
                    If i > 0 Then
                        drPunchIn = drPunches(i - 1)
                        drPunchOut = drPunches(i)
                    Else
                        Exit Do
                    End If
                Else
                    Dim dtTmp As DateTime = drPunchOut("PunchTime")
                    If schedInfo.Allow_ExtraHours = True And GetTime(drPunches(i)("PunchTime")) > GetTime(schedInfo.TimeOut) Then
                        drPunchOut("PunchTime") = New DateTime(dtTmp.Year, dtTmp.Month, dtTmp.Day,
                                            dtTmp.Hour, dtTmp.Minute, dtTmp.Second)
                        Exit Do
                    Else
                        drPunchOut("PunchTime") = New DateTime(dtTmp.Year, dtTmp.Month, dtTmp.Day,
                                                                    schedInfo.TimeOut.Hour, schedInfo.TimeOut.Minute, schedInfo.TimeOut.Second)
                        drPunchOut("FromSystem") = True
                    End If
                End If
            Loop
        End Sub


        Private Sub ParseForLateOut_byClass(ByVal drPunches() As DataRow, ByVal schedInfo As ClsSectMeetingScheduledetailsInfo)

            If drPunches.Length = 0 Then Return

            Dim i As Integer = drPunches.Length - 2

            Dim drPunchIn As DataRow = GetNextPunchIn(drPunches, i - 1)
            Dim drPunchOut As DataRow = GetNextPunchOut(drPunches, i)

            If drPunchIn Is Nothing Or drPunchOut Is Nothing Then
                Return
            End If


            Do While GetTime(drPunchOut("PunchTime")) > GetTime(schedInfo.TimeOut)
                If GetTime(drPunchIn("PunchTime")) > GetTime(schedInfo.TimeOut) And schedInfo.Allow_ExtraHours = False Then
                    drPunchIn("Status") = PunchStatus.NotScheduled
                    drPunchOut("Status") = PunchStatus.NotScheduled
                    i -= 2
                    If i > 0 Then
                        drPunchIn = drPunches(i - 1)
                        drPunchOut = drPunches(i)
                    Else
                        Exit Do
                    End If
                Else
                    Dim dtTmp As DateTime = drPunchOut("PunchTime")
                    If schedInfo.Allow_ExtraHours = True And GetTime(drPunches(i)("PunchTime")) > GetTime(schedInfo.TimeOut) Then
                        drPunchOut("PunchTime") = New DateTime(dtTmp.Year, dtTmp.Month, dtTmp.Day,
                                            dtTmp.Hour, dtTmp.Minute, dtTmp.Second)
                        Exit Do
                    Else
                        drPunchOut("PunchTime") = New DateTime(dtTmp.Year, dtTmp.Month, dtTmp.Day,
                                                                    schedInfo.TimeOut.Hour, schedInfo.TimeOut.Minute, schedInfo.TimeOut.Second)
                        drPunchOut("FromSystem") = True
                    End If
                End If
            Loop
        End Sub
        ''' <summary>
        ''' If LimitPunchInTime and first punch of the day is later than MaxPunchIn then
        ''' Set first punch of the day to AssignedTimeIn.
        ''' </summary>
        ''' <param name="drPunches"></param>
        ''' <param name="schedInfo"></param>
        ''' <remarks></remarks>
        Private Sub ParseForTardyIn(ByVal drPunches() As DataRow, ByVal schedInfo As ScheduleDetailInfo)

            If drPunches.Length = 0 Then Return

            Dim i As Integer = -1
            Dim drPunchIn As DataRow = GetNextPunchIn(drPunches, i)
            Dim drPunchOut As DataRow = GetNextPunchOut(drPunches, i)

            If drPunchIn Is Nothing Or drPunchOut Is Nothing Then
                Return
            End If

            Do While GetTime(drPunchIn("PunchTime")) > GetTime(schedInfo.MaxBeforeTardy) AndAlso
                     GetTime(drPunchIn("PunchTime")) < GetTime(schedInfo.TardyInTime)
                If GetTime(drPunchOut("PunchTime")) < GetTime(schedInfo.TardyInTime) Then
                    drPunchIn("Status") = PunchStatus.NotScheduled
                    drPunchOut("Status") = PunchStatus.NotScheduled

                    drPunchIn = GetNextPunchIn(drPunches, i)
                    drPunchOut = GetNextPunchOut(drPunches, i)
                    If drPunchIn Is Nothing Or drPunchOut Is Nothing Then
                        Exit Do
                    End If
                Else
                    Dim dtTmp As DateTime = drPunchIn("PunchTime")
                    drPunchIn("PunchTime") = New DateTime(dtTmp.Year, dtTmp.Month, dtTmp.Day,
                                            schedInfo.TardyInTime.Hour, schedInfo.TardyInTime.Minute, schedInfo.TardyInTime.Second)
                    drPunchIn("FromSystem") = True
                    Exit Do
                End If
            Loop
        End Sub

        Private Sub ParseForTardyIn_byClass(ByVal drPunches() As DataRow, ByVal schedInfo As ClsSectMeetingScheduledetailsInfo)
            If drPunches.Length = 0 Then Return

            Dim i As Integer = -1
            Dim drPunchIn As DataRow = GetNextPunchIn(drPunches, i)
            Dim drPunchOut As DataRow = GetNextPunchOut(drPunches, i)

            If drPunchIn Is Nothing Or drPunchOut Is Nothing Then
                Return
            End If

            Do While GetTime(drPunchIn("PunchTime")) > GetTime(schedInfo.MaxBeforeTardy) AndAlso
                     GetTime(drPunchIn("PunchTime")) < GetTime(schedInfo.TardyInTime)
                If GetTime(drPunchOut("PunchTime")) < GetTime(schedInfo.TardyInTime) Then
                    drPunchIn("Status") = PunchStatus.NotScheduled
                    drPunchOut("Status") = PunchStatus.NotScheduled

                    drPunchIn = GetNextPunchIn(drPunches, i)
                    drPunchOut = GetNextPunchOut(drPunches, i)
                    If drPunchIn Is Nothing Or drPunchOut Is Nothing Then
                        Exit Do
                    End If
                Else
                    Dim dtTmp As DateTime = drPunchIn("PunchTime")
                    drPunchIn("PunchTime") = New DateTime(dtTmp.Year, dtTmp.Month, dtTmp.Day,
                                            schedInfo.TardyInTime.Hour, schedInfo.TardyInTime.Minute, schedInfo.TardyInTime.Second)
                    drPunchIn("FromSystem") = True
                    Exit Do
                End If
            Loop
        End Sub


        ''' <summary>
        ''' If CheckForLunchMark > 0 And Student only has one punch in and one punch out
        ''' for the day And total hours attended exceeds CheckForLunchMark Then
        ''' Assign student a punch out for lunch and a punch in from lunch.
        ''' </summary>
        ''' <param name="drPunches"></param>
        ''' <param name="schedInfo"></param>
        ''' <remarks></remarks>
        Private Function ParseForMaxNoLunch(ByVal drPunches() As DataRow, ByVal schedInfo As ScheduleDetailInfo) As DataRow()

            If drPunches.Length < 2 Then Return drPunches

            Dim i As Integer = -1
            Dim drPunchIn As DataRow = GetNextPunchIn(drPunches, i)
            Dim drPunchOut As DataRow = GetNextPunchOut(drPunches, i)


            If drPunchIn Is Nothing Or drPunchOut Is Nothing Then
                Return drPunches
            End If

            If DateDiff(DateInterval.Minute, drPunchIn("PunchTime"), drPunchOut("PunchTime")) <= (schedInfo.MaxNoLunch * 60) Then
                Return drPunches
            End If

            If GetTime(drPunchIn("PunchTime")) < GetTime(schedInfo.LunchOut) AndAlso
                GetTime(drPunchOut("PunchTime")) > GetTime(schedInfo.LunchOut) Then


                If GetTime(drPunchOut("PunchTime")) < GetTime(schedInfo.LunchIn) Then
                    Dim dtTmp As DateTime = drPunchOut("PunchTime")
                    drPunchOut("PunchTime") = New DateTime(dtTmp.Year, dtTmp.Month, dtTmp.Day,
                                            schedInfo.LunchOut.Hour, schedInfo.LunchOut.Minute, schedInfo.LunchOut.Second)
                    drPunchOut("FromSystem") = True

                    Return drPunches
                ElseIf GetTime(drPunchOut("PunchTime")) > GetTime(schedInfo.LunchIn) Then


                    Dim dtTmp As DateTime = drPunchIn("PunchTime")
                    Dim diff As Decimal = DateDiff(DateInterval.Minute, drPunchIn("PunchTime"), drPunchOut("PunchTime")) / 60.0
                    Dim RemainingHrs As Decimal
                    RemainingHrs = diff - schedInfo.MaxNoLunch
                    Dim c As Integer = drPunches.Length + 1

                    Dim drNewPunches(c) As DataRow
                    drNewPunches(0) = drPunches(0)
                    drNewPunches(3) = drPunches(1)
                    Dim drNewPunchOut As DataRow = drNewPunches(0).Table.NewRow
                    drNewPunchOut("BadgeId") = drPunchIn("BadgeId")
                    drNewPunchOut("ClockId") = drPunchIn("ClockId")
                    drNewPunchOut("StuEnrollId") = drPunchIn("StuEnrollId")
                    drNewPunchOut("PunchType") = TimeClockPunchType.PunchOut
                    drNewPunchOut("PunchTime") = New DateTime(dtTmp.Year, dtTmp.Month, dtTmp.Day,
                                        schedInfo.LunchOut.Hour, schedInfo.LunchOut.Minute, schedInfo.LunchOut.Second)
                    drNewPunchOut("FromSystem") = True
                    drNewPunchOut("Status") = PunchStatus.Ok
                    drNewPunches(1) = drNewPunchOut


                    Dim drNewPunchIn As DataRow = drNewPunches(0).Table.NewRow
                    drNewPunchIn("BadgeId") = drPunchIn("BadgeId")
                    drNewPunchIn("ClockId") = drPunchIn("ClockId")
                    drNewPunchIn("StuEnrollId") = drPunchIn("StuEnrollId")
                    drNewPunchIn("PunchType") = TimeClockPunchType.PunchIn
                    drNewPunchIn("PunchTime") = New DateTime(dtTmp.Year, dtTmp.Month, dtTmp.Day,
                                                         schedInfo.LunchIn.Hour, schedInfo.LunchIn.Minute, schedInfo.LunchIn.Second)

                    drNewPunchIn("FromSystem") = True
                    drNewPunchIn("Status") = PunchStatus.Ok
                    drNewPunches(2) = drNewPunchIn

                    Dim cnt As Integer
                    If drPunches.Length > 2 Then
                        For cnt = 4 To drPunches.Length + 1
                            drNewPunches(cnt) = drPunches(cnt - 2)

                        Next
                    End If

                    Return drNewPunches
                Else
                    Return drPunches
                End If
            Else
                Return drPunches
            End If

        End Function
#End Region

#Region "Import methods"
        ''' <summary>
        ''' Main function for importing time clock data.  Returns a DataTable
        ''' with the collection of punches.  
        ''' If you add a new TimeClock format, this is the place to do it.
        ''' </summary>
        ''' <param name="buf"></param>
        ''' <param name="type"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ImportTimeClockData(ByVal buf As String, ByVal type As TimeClockDataType) As DataTable
            Try
                Select Case type
                    Case TimeClockDataType.CS2000
                        AddMsg("ImportTimeClockData", "Importing CS200 File")
                        Return ImportTimeClock_CS2000(buf)

                    Case TimeClockDataType.ZON580
                        AddMsg("ImportTimeClockData", "Importing ZON580 File")
                        Return ImportTimeClock_ZON580(buf)
                End Select
            Catch ex As System.Exception
                AddMsg("ImportTimeClockData", ex.Message)
            End Try

            Return Nothing
        End Function

        Public Function ImportTimeClockData_byClass(ByVal buf As String, ByVal type As TimeClockDataType, ByVal CampusID As String) As DataTable
            Try
                Select Case type
                    Case TimeClockDataType.CS2000
                        AddMsg("ImportTimeClockData", "Importing CS200 File")
                        Return ImportTimeClock_CS2000_ByClass(buf, CampusID)

                    Case TimeClockDataType.ZON580
                        AddMsg("ImportTimeClockData", "Importing ZON580 File")
                        Return ImportTimeClock_ZON580(buf)
                End Select
            Catch ex As System.Exception
                AddMsg("ImportTimeClockData", ex.Message)
            End Try

            Return Nothing
        End Function

        ''' <summary>
        ''' Logs all the results of the file import 
        ''' </summary>
        ''' <param name="dtPunches"></param>
        ''' <remarks></remarks>
        Protected Sub LogResultOfImport(ByVal dtPunches As DataTable)
            AddMsg("LogResultOfImport", "--- Logging results of file import ---")
            AddMsg("LogResultOfImport", String.Format("{0}BadgeId{0}Type{0}Time", vbTab))
            For Each dr As DataRow In dtPunches.Rows
                Try
                    Dim s As String = vbTab
                    If dr("PunchType") = TimeClockPunchType.PunchIn Then
                        s += String.Format("{0}{1}{2}{3}{4}",
                                        dr("BadgeId").ToString.PadLeft(9, "0"c), vbTab,
                                        "IN:  ", vbTab,
                                        dr("PunchTime"))
                    Else
                        s += String.Format("{0}{1}{2}{3}{4}",
                                        dr("BadgeId").ToString.PadLeft(9, "0"c), vbTab,
                                        "OUT:  ", vbTab,
                                        dr("PunchTime"))
                    End If
                    AddMsg("LogResultOfImport", s)
                Catch ex As System.Exception
                    AddMsg("LogReultOfImport", ex.Message)
                End Try
            Next
            AddMsg("LogResultOfImport", "--- End of logging results of file import ---")
        End Sub

        ''' <summary>    
        ''' Record structure
        '''           1         2         3
        ''' 1234567890123456789012345678901234
        ''' ==================================
        ''' PN00 040609102230 1000AM000004602
        ''' Bytes 01-05: Record type (PN = punch, ignore any other types)
        ''' Bytes 06-11: Punch date in YYMMDD format
        ''' Bytes 12-17: Punch time in 24-hour HHMMSS format
        ''' Byte  18:    Unknown
        ''' Byte  19:    Punch type: 1 = in for day, 2 = in from lunch, 3 = in from break, 5 = any out
        ''' Bytes 20-23: Unknown
        ''' Byte  24:    Type of entry: K = keypad, M = mag stripe
        ''' Bytes 25-28: Unknown (actually badge number but should always be 0000 due to Freedom limits)
        ''' Bytes 29-33: Badge number
        ''' Byte  34:    Unknown (appears to always be blank)
        ''' Note: AccuEngine download utility configuration options can alter the record layout.
        ''' If "Prefix ID to terminal data records" is checked, records will
        ''' be prefixed with something like "ATS 18Apr05_14:20:08 009550 | ".
        ''' If "Strip protocol characters from terminal data" is NOT checked, each line
        ''' will be prefixed with the string "\A! ".  Also, the records will have unprintable
        ''' ASCII characters instead of spaces, but the format is the same.
        ''' The code below will attempt remove the ID and protocol prefixes before processing, so
        ''' We should be able to read the punches regardless of the AccuEngine options.
        ''' </summary>
        ''' <param name="buf"></param>        
        ''' <remarks></remarks>
        Protected Function ImportTimeClock_CS2000(ByVal buf As String) As DataTable
            Dim dt As DataTable = GetBlankPunchesDataTable()
            Dim lBuf() As String = buf.Split(vbCrLf)
            For Each sBuffer As String In lBuf
                Try
                    If sBuffer.Trim.Length < 10 Then
                        Exit Try
                    End If
                    If Not sBuffer.Trim.Substring(0, 2) = "PN" Then
                        If InStr(sBuffer.Trim, "PN") >= 1 Then
                            sBuffer = Mid(sBuffer.Trim, InStr(sBuffer.Trim, "P") - 1)
                            If Not sBuffer(0) = "P" Then
                                sBuffer = sBuffer.Substring(1)
                            End If
                            Dim dr As DataRow = dt.NewRow
                            Dim sPunchType As String = Mid(sBuffer.Trim, 19, 1)
                            If InStr("123", sPunchType) > 0 Then
                                dr("PunchType") = TimeClockPunchType.PunchIn
                            Else
                                dr("PunchType") = TimeClockPunchType.PunchOut
                            End If
                            dr("ClockId") = Mid(sBuffer.Trim, 1, 4)
                            Dim sDate As String = String.Format("{0}/{1}/{2} {3}:{4}",
                                                            sBuffer.Trim.Substring(7, 2),
                                                            sBuffer.Trim.Substring(9, 2),
                                                            sBuffer.Trim.Substring(5, 2),
                                                            sBuffer.Trim.Substring(11, 2),
                                                            sBuffer.Trim.Substring(13, 2))
                            Try
                                dr("PunchTime") = CType(sDate, DateTime)
                            Catch ex As System.Exception
                                dr("PunchTime") = DateTime.MinValue
                            End Try
                            dr("BadgeId") = Math.Abs(CInt(Mid(sBuffer.Trim, 25, 9))).ToString
                            dr("ClsSectMeetingId") = ""
                            dt.Rows.Add(dr)
                        End If
                    Else
                        Dim dr As DataRow = dt.NewRow
                        Dim sPunchType As String = Mid(sBuffer.Trim, 19, 1)
                        If InStr("123", sPunchType) > 0 Then
                            dr("PunchType") = TimeClockPunchType.PunchIn
                        Else
                            dr("PunchType") = TimeClockPunchType.PunchOut
                        End If

                        dr("ClockId") = Mid(sBuffer.Trim, 1, 4)
                        Dim sDate As String = String.Format("{0}/{1}/{2} {3}:{4}",
                                                        sBuffer.Trim.Substring(7, 2),
                                                        sBuffer.Trim.Substring(9, 2),
                                                        sBuffer.Trim.Substring(5, 2),
                                                        sBuffer.Trim.Substring(11, 2),
                                                        sBuffer.Trim.Substring(13, 2))

                        Try
                            dr("PunchTime") = CType(sDate, DateTime)
                        Catch ex As System.Exception
                            dr("PunchTime") = DateTime.MinValue
                        End Try
                        dr("BadgeId") = Mid(sBuffer.Trim, 25, 9)
                        dr("ClsSectMeetingId") = ""
                        dt.Rows.Add(dr)
                    End If
                Catch ex As System.Exception
                End Try
            Next
            AddMsg("ImportTimeClock_CS2000", String.Format("{0} records read.", dt.Rows.Count))
            Return dt
        End Function

        Protected Function ImportTimeClock_CS2000_ByClass(ByVal buf As String, ByVal CampusID As String) As DataTable
            Dim dt As DataTable = GetBlankPunchesDataTable()
            Dim db As TimeClockDB
            Dim PunchType As String
            Dim lBuf() As String = buf.Split(vbCrLf)
            For Each sBuffer As String In lBuf
                Try
                    If sBuffer.Trim.Length < 10 Then
                        Exit Try
                    End If
                    If Not sBuffer.Trim.Substring(0, 2) = "PN" Then
                        If InStr(sBuffer.Trim, "PN") >= 1 Then
                            sBuffer = Mid(sBuffer.Trim, InStr(sBuffer.Trim, "P") - 1)
                            If Not sBuffer(0) = "P" Then
                                sBuffer = sBuffer.Substring(1)
                            End If
                            Dim dr As DataRow = dt.NewRow
                            Dim sPunchType As String = Mid(sBuffer.Trim, 19, 4)
                            PunchType = db.GetPunchTypeForSpecialCode(sPunchType, CampusID)
                            dr("PunchType") = PunchType
                            dr("SpecialCode") = sPunchType

                            dr("ClockId") = Mid(sBuffer.Trim, 1, 4)
                            Dim sDate As String = String.Format("{0}/{1}/{2} {3}:{4}",
                                                            sBuffer.Trim.Substring(7, 2),
                                                            sBuffer.Trim.Substring(9, 2),
                                                            sBuffer.Trim.Substring(5, 2),
                                                            sBuffer.Trim.Substring(11, 2),
                                                            sBuffer.Trim.Substring(13, 2))
                            Try
                                dr("PunchTime") = CType(sDate, DateTime)
                            Catch ex As System.Exception
                                dr("PunchTime") = DateTime.MinValue
                            End Try
                            dr("BadgeId") = Math.Abs(CInt(Mid(sBuffer.Trim, 25, 9))).ToString
                            dr("ClsSectMeetingId") = ""
                            dt.Rows.Add(dr)
                        End If
                    Else
                        Dim dr As DataRow = dt.NewRow
                        Dim sPunchType As String = Mid(sBuffer.Trim, 19, 4)
                        PunchType = db.GetPunchTypeForSpecialCode(sPunchType, CampusID)
                        dr("PunchType") = PunchType
                        dr("SpecialCode") = sPunchType
                        dr("ClockId") = Mid(sBuffer.Trim, 1, 4)
                        Dim sDate As String = String.Format("{0}/{1}/{2} {3}:{4}",
                                                        sBuffer.Trim.Substring(7, 2),
                                                        sBuffer.Trim.Substring(9, 2),
                                                        sBuffer.Trim.Substring(5, 2),
                                                        sBuffer.Trim.Substring(11, 2),
                                                        sBuffer.Trim.Substring(13, 2))

                        Try
                            dr("PunchTime") = CType(sDate, DateTime)
                        Catch ex As System.Exception
                            dr("PunchTime") = DateTime.MinValue
                        End Try
                        dr("BadgeId") = Mid(sBuffer.Trim, 25, 9)
                        dr("ClsSectMeetingId") = ""
                        dt.Rows.Add(dr)
                    End If
                Catch ex As System.Exception
                End Try
            Next
            AddMsg("ImportTimeClock_CS2000", String.Format("{0} records read.", dt.Rows.Count))
            Return dt
        End Function


        ''' <summary>
        ''' Record structure
        '''          1         2         3
        ''' 123456789012345678901234567890
        ''' ==============================
        ''' 01123402030508311061200000
        ''' Bytes 01-02: Punch Type: 1 = In, 2 = Out
        ''' Bytes 03-06: Clock ID
        ''' Bytes 07-12: Punch date in YYMMDD format
        ''' Bytes 13-16: Punch time in 24-hour HHMM format
        ''' Byte  17-21: Badge number
        ''' Bytes 22-26: Unknown (appears to always be 0's)
        ''' </summary>
        ''' <param name="buf"></param>
        ''' <remarks></remarks>
        Protected Function ImportTimeClock_ZON580(ByVal buf As String) As DataTable
            Dim dt As DataTable = GetBlankPunchesDataTable()

            Dim lBuf() As String = buf.Split(vbCrLf)
            For Each sBuffer As String In lBuf
                Try
                    If Convert.ToInt32(sBuffer(0)) = 10 Then
                        sBuffer = sBuffer.Substring(1)
                    End If
                    If Not sBuffer = "" Then
                        Dim dr As DataRow = dt.NewRow()

                        dr("PunchType") = CLng(sBuffer.Substring(0, 2))
                        dr("ClockId") = sBuffer.Substring(2, 4)
                        Dim sDate As String = String.Format("{0}/{1}/{2} {3}:{4}",
                                                        sBuffer.Substring(6, 2),
                                                        sBuffer.Substring(8, 2),
                                                        sBuffer.Substring(10, 2),
                                                        sBuffer.Substring(12, 2),
                                                        sBuffer.Substring(14, 2))

                        dr("PunchTime") = CType(sDate, DateTime)
                        dr("BadgeId") = sBuffer.Substring(16, 9)
                        dr("ClsSectMeetingId") = ""
                        dt.Rows.Add(dr)
                    End If
                Catch ex As System.Exception
                    AddMsg("ImportTimeClock_ZON580", ex.Message)
                End Try
            Next
            AddMsg("ImportTimeClock_ZON580", String.Format("{0} records read.", dt.Rows.Count))
            Return dt
        End Function
#End Region

#Region "Utility Functions"
        Protected Sub AddMsg(ByVal srcMethod As String, ByVal s As String)
            _log.Append(srcMethod + ": " + s + vbCrLf)
        End Sub

        Protected Sub AddMsgNew(ByVal s As String)
            _log.Append(s + vbCrLf)
        End Sub

        Public Shared Function GetTime(ByVal dt As DateTime) As DateTime
            Return New DateTime(1, 1, 1, dt.Hour, dt.Minute, dt.Second)
        End Function
#End Region

#Region "Convertsion Functions"
        ''' <summary>
        ''' Returns a TimeClockPunchInfo object from a datarow
        ''' </summary>
        ''' <param name="dr"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Function ToTimeClockPunchInfo(ByVal dr As DataRow) As TimeClockPunchInfo
            Dim info As New TimeClockPunchInfo
            info.BadgeId = dr("BadgeId").ToString()
            info.ClockId = dr("ClockId").ToString()
            info.Status = dr("Status")
            info.PunchTime = dr("PunchTime")
            info.PunchType = dr("PunchType")
            info.StuEnrollId = dr("StuEnrollId").ToString()
            info.FromSystem = dr("FromSystem").ToString()
            Return info
        End Function

        Protected Function ToTimeClockPunchInfo_ByClass(ByVal dr As DataRow) As TimeClockPunchInfo
            Dim info As New TimeClockPunchInfo
            info.BadgeId = dr("BadgeId").ToString()
            info.ClockId = dr("ClockId").ToString()
            info.Status = dr("Status")
            info.PunchTime = dr("PunchTime")
            info.PunchType = dr("PunchType")
            info.StuEnrollId = dr("StuEnrollId").ToString()
            info.FromSystem = dr("FromSystem").ToString()
            info.ClsSectMeetingId = dr("ClsSectMeetingId").ToString()
            info.SpecialCode = dr("SpecialCode").ToString()
            Return info
        End Function

        ''' <summary>
        ''' Converts a DataTable to an array of TimeClockPunchInfo
        ''' </summary>
        ''' <param name="dt"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ToTimeClockPunchInfoArray_byClass(ByVal dt As DataTable) As TimeClockPunchInfo()
            Dim c As Integer = dt.Rows.Count

            Dim res(c - 1) As TimeClockPunchInfo
            For i As Integer = 0 To c - 1
                res(i) = New TimeClockPunchInfo
                Dim dr As DataRow = dt.Rows(i)
                res(i).AdjPunchTime = dr("PunchTime")
                res(i).PunchTime = dr("PunchTime")
                res(i).BadgeId = dr("BadgeId").ToString()
                res(i).ClockId = dr("ClockId").ToString()
                res(i).PunchType = dr("PunchType")
                res(i).Status = dr("Status")
                res(i).FromSystem = dr("FromSystem")
                res(i).ClsSectMeetingId = dr("ClsSectMeetingID").ToString()
                res(i).StuEnrollId = dr("StuEnrollId").ToString()
                res(i).SpecialCode = dr("SpecialCode").ToString()
            Next
            Return res
        End Function ''' 


        Public Shared Function ToTimeClockPunchInfoArray(ByVal dt As DataTable) As TimeClockPunchInfo()
            Dim c As Integer = dt.Rows.Count

            Dim res(c - 1) As TimeClockPunchInfo
            For i As Integer = 0 To c - 1
                res(i) = New TimeClockPunchInfo
                Dim dr As DataRow = dt.Rows(i)
                res(i).AdjPunchTime = dr("PunchTime")
                res(i).PunchTime = dr("PunchTime")
                res(i).BadgeId = dr("BadgeId").ToString()
                res(i).ClockId = dr("ClockId").ToString()
                res(i).PunchType = dr("PunchType")
                res(i).Status = dr("Status")
                res(i).FromSystem = dr("FromSystem")
                res(i).StuEnrollId = dr("StuEnrollId").ToString()


            Next
            Return res
        End Function

#End Region
        Public Function GetPunchesForaStudentonaGivenDate(ByVal stuEnrollid As String, ByVal SchDate As Date) As DataSet
            Dim DB As New TimeClockDB
            Return DB.GetPunchesForaStudentonaGivenDate(stuEnrollid, SchDate)

        End Function
        Public Function GetPunchesforClsSectMeetingForaStudentonaGivenDate_ByClass(ByVal stuEnrollid As String, ByVal SchDate As Date, ByVal ClsSectMeetingID As String) As DataSet
            Dim DB As New TimeClockDB
            Return DB.GetPunchesforClsSectMeetingForaStudentonaGivenDate_ByClass(stuEnrollid, SchDate, ClsSectMeetingID)

        End Function

        Public Function SavePunchesEntered(ByVal dt As DataTable, ByVal tardymarked As Boolean, ByVal ScheduledHours As Decimal, ByVal CampusID As String) As String
            AddPunchesToDatabasefortimeclock(dt)
            ProcessTotalDailyHoursforEnteredTimeClock(dt, tardymarked, ScheduledHours, CampusID)
            Return ""
        End Function

        Public Function SavePunchesEntered_byClass(ByVal dt As DataTable, ByVal tardymarked As Boolean, ByVal ScheduledHours As Decimal, ByVal CampusID As String, ByVal UnitTypeID As String) As String
            AddPunchesToDatabasefortimeclock_byClass(dt)
            ProcessTotalDailyHoursforEnteredTimeClock_ByClass(dt, tardymarked, ScheduledHours, CampusID, UnitTypeID)
            Return ""
        End Function

        Public Function GetStudentCompleteDetails(ByVal stuEnrollid As String) As DataSet
            Dim DB As New TimeClockDB
            Return DB.GetStudentCompleteDetails(stuEnrollid)

        End Function

        Public Function GetStudentCompleteDetails_byClass(ByVal stuEnrollid As String) As DataSet
            Dim DB As New TimeClockDB
            Return DB.GetStudentCompleteDetails_ByClass(stuEnrollid)

        End Function

        Protected Sub ProcessTotalDailyHoursforEnteredTimeClock(ByVal dt As DataTable, ByVal TardyMarked As Boolean, ByVal ScheduledHours As Decimal, ByVal CampusID As String)
            Dim dv As DataView = New DataView(dt, "", "StuEnrollId, PunchTime ASC, PunchType ASC", DataViewRowState.None)

            Dim StuEnrollId As String = dv.Table.Rows(0)("StuEnrollId").ToString()
            Dim ScheduleId As String = dv.Table.Rows(0)("ScheduleId").ToString()

            Dim schedInfo As ScheduleInfo = SchedulesDB.GetInfo(ScheduleId)
            Dim sdInfo() As ScheduleDetailInfo = schedInfo.Details()
            Dim sql As String = String.Format("StuEnrollId='{0}'", StuEnrollId)
            Dim minDate As DateTime = dt.Compute("min(PunchTime)", sql)
            Dim maxDate As DateTime = dt.Compute("max(PunchTime)", sql)
            Dim curDate As DateTime = minDate
            Dim InPunches As Integer = 0
            Dim OutPunches As Integer = 0
            Dim CanPostonHoliday As Boolean = False

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim dow As Integer = CType(curDate.DayOfWeek, Integer)
            Dim drPunches() As DataRow = GetPunchesDR(dt, StuEnrollId, curDate)
            If IsHoliday(curDate, CampusID) Then
                If Not MyAdvAppSettings.AppSettings("postattendanceonholiday") Is Nothing Then
                    If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "yes" Then
                        CanPostonHoliday = True
                    End If
                End If
            End If

            If drPunches.Length >= 2 Then
                Dim totHrs As Decimal = GetTotalDailyHoursForTimeClock(drPunches, sdInfo(dow))
                AddMsgNew(String.Format("Total Hours for [{1}] is  {2} Hrs",
                           drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c), curDate.ToShortDateString(), totHrs))
                Dim res As String

                If CanPostonHoliday = True Then
                    res = TimeClockDB.SaveTotalDailyHoursforTimeClock(curDate.ToShortDateString(), StuEnrollId, ScheduleId, totHrs, sdInfo(dow).Check_TardyIn, ScheduledHours, schedInfo.PrgVerId, TardyMarked, CampusID)
                    CanPostonHoliday = False
                Else
                    Dim tardy = 0
                    If Not sdInfo(dow) Is Nothing Then
                        tardy = sdInfo(dow).Check_TardyIn
                    End If

                    res = TimeClockDB.SaveTotalDailyHoursforTimeClock(curDate.ToShortDateString(), StuEnrollId, ScheduleId, totHrs, tardy, ScheduledHours, schedInfo.PrgVerId, TardyMarked, CampusID)
                End If


                If res <> "" Then
                    AddMsg("ComputeHours", res)
                End If
            End If
            TimeClockDB.SetPunchStatus(drPunches)


            Dim infoArr() As TimeClockPunchInfo = ToTimeClockPunchInfoArray(dt)
            For Each info As TimeClockPunchInfo In infoArr
                If Not info.Status = PunchStatus.Invalid And Not info.Status = PunchStatus.NotScheduled Then
                    info.Status = PunchStatus.Ok
                End If

            Next
            Dim res2 As String = TimeClockDB.SaveTimeClockPunches(infoArr)
            If res2 = "" Then
            Else
                AddMsg("ComputeHours:SaveTimeClockPunches", res2)
            End If
        End Sub
        Private Function GetTotalDailyHoursForTimeClock(ByVal drPunches() As DataRow, ByVal schedInfo As ScheduleDetailInfo) As Decimal
            Dim bIsClosedDay As Boolean = False
            If drPunches.Length = 0 Then
                Return 0.0
            End If

            For Each dr As DataRow In drPunches
                dr("Status") = PunchStatus.Ok
            Next

            Dim totHrs As Decimal = 0.0
            Dim i As Integer = -1
            Dim drPunchIn As DataRow = GetNextPunchIn(drPunches, i)
            Dim drPunchOut As DataRow = GetNextPunchOut(drPunches, i)

            While drPunchIn IsNot Nothing AndAlso drPunchOut IsNot Nothing
                Dim diff As Decimal = DateDiff(DateInterval.Minute, drPunchIn("PunchTime"), drPunchOut("PunchTime")) / 60.0

                totHrs += diff

                drPunchIn = GetNextPunchIn(drPunches, i)
                drPunchOut = GetNextPunchOut(drPunches, i)

            End While
            Return Math.Round(totHrs, 2)
        End Function

        Protected Sub ProcessTotalDailyHoursforEnteredTimeClock_ByClass(ByVal dt As DataTable, ByVal TardyMarked As Boolean, ByVal ScheduledHours As Decimal, ByVal CampusID As String, ByVal UnitTypeId As String)
            Dim dv As DataView = New DataView(dt, "", "StuEnrollId, PunchTime ASC, PunchType ASC", DataViewRowState.None)

            Dim StuEnrollId As String = dv.Table.Rows(0)("StuEnrollId").ToString()
            Dim ClsSectMeetingId As String
            ClsSectMeetingId = dv.Table.Rows(0)("ClsSectMeetingId").ToString()
            Dim schedInfo As ClsSectionMeetingScheduleInfo = TimeClockDB.GetClsSectionMeetingScheduleInfo(ClsSectMeetingId)
            Dim sdInfo() As ClsSectMeetingScheduledetailsInfo = schedInfo.Details()




            Dim objClsAttFac As New ClsSectAttendanceFacade
            Dim arrCancelDates As ArrayList = objClsAttFac.GetCancelDays_byClass(schedInfo.ClsSectionID, ClsSectMeetingId, CampusID)

            Dim sql As String = String.Format("StuEnrollId='{0}'  and ClsSectMeetingID='{1}'", StuEnrollId, ClsSectMeetingId)
            Dim minDate As DateTime = dt.Compute("min(PunchTime)", sql)
            Dim maxDate As DateTime = dt.Compute("max(PunchTime)", sql)
            Dim curDate As DateTime = minDate
            Dim InPunches As Integer = 0
            Dim OutPunches As Integer = 0
            Dim CanPostonHoliday As Boolean = False

            Dim selectWk As Integer = 0

            Dim aday As Date = schedInfo.ClassStartDate
            Dim firstDate As Date = aday.AddDays(-CInt(aday.DayOfWeek))

            selectWk = Math.Floor(DateDiff("d", firstDate, curDate) / 7) Mod 2


            Dim dow As Integer = CType(curDate.DayOfWeek, Integer)
            Dim drPunches() As DataRow = GetPunchesDR(dt, StuEnrollId, curDate)

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            If IsHoliday(curDate, CampusID, Format(sdInfo(dow).TimeIn, "HH:mm:ss"), Format(sdInfo(dow).TimeOut, "HH:mm:ss")) Then
                If Not MyAdvAppSettings.AppSettings("postattendanceonholiday") Is Nothing Then
                    If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "yes" Then
                        CanPostonHoliday = True
                    End If
                End If
            End If

            If isMeetingDayCancelled(arrCancelDates, curDate) Then
                CanPostonHoliday = True
            End If

            If drPunches.Length >= 2 Then
                Dim totHrs As Decimal = GetTotalDailyHoursForTimeClock_byClass(drPunches)
                AddMsgNew(String.Format("Total Hours for [{1}] is  {2} Hrs",
                           drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c), curDate.ToShortDateString(), totHrs))

                Dim res As String

                If CanPostonHoliday = True Then

                    res = TimeClockDB.SaveTotalDailyHoursforTimeClock_byClass(curDate.ToShortDateString(), StuEnrollId, ClsSectMeetingId, totHrs, sdInfo(dow).Check_TardyIn, 0, "test", TardyMarked, CampusID, UnitTypeId, drPunches, selectWk)

                    CanPostonHoliday = False
                Else
                    res = TimeClockDB.SaveTotalDailyHoursforTimeClock_byClass(curDate.ToShortDateString(), StuEnrollId, ClsSectMeetingId, totHrs, sdInfo(dow).Check_TardyIn, ScheduledHours, "test", TardyMarked, CampusID, UnitTypeId, drPunches, selectWk)
                End If


                If res <> "" Then
                    AddMsg("ComputeHours", res)
                End If

            Else
                TimeClockDB.SetPunchStatus_ByClass(drPunches)
            End If


            Dim infoArr() As TimeClockPunchInfo = ToTimeClockPunchInfoArray_byClass(dt)
            For Each info As TimeClockPunchInfo In infoArr
                If Not info.Status = PunchStatus.Invalid And Not info.Status = PunchStatus.NotScheduled Then
                    info.Status = PunchStatus.Ok
                End If

            Next
            Dim res2 As String = TimeClockDB.SaveTimeClockPunches_ByClass(infoArr)
            If res2 = "" Then

            Else
                AddMsg("ComputeHours:SaveTimeClockPunches", res2)
            End If
        End Sub
        Private Function GetTotalDailyHoursForTimeClock_byClass(ByVal drPunches() As DataRow) As Decimal
            Dim bIsClosedDay As Boolean = False
            If drPunches.Length = 0 Then
                Return 0.0
            End If

            For Each dr As DataRow In drPunches
                dr("Status") = PunchStatus.Ok
            Next

            Dim totHrs As Decimal = 0.0
            Dim i As Integer = -1
            Dim drPunchIn As DataRow = GetNextPunchIn(drPunches, i)
            Dim drPunchOut As DataRow = GetNextPunchOut(drPunches, i)

            While drPunchIn IsNot Nothing AndAlso drPunchOut IsNot Nothing
                Dim diff As Decimal = DateDiff(DateInterval.Minute, drPunchIn("PunchTime"), drPunchOut("PunchTime"))


                totHrs += diff

                drPunchIn = GetNextPunchIn(drPunches, i)
                drPunchOut = GetNextPunchOut(drPunches, i)

            End While
            Return Math.Round(totHrs, 2)
        End Function

        Private Sub FillEnrollmentInfotoPostZero(ByVal dt As DataTable)
            Dim dtStudents As DataTable = TimeClockDB.GetActiveStudents()

            For Each dr As DataRow In dt.Rows
                Dim strBatchId As String = ""

                Dim sql As String = String.Format("StuEnrollId='{0}'", trimLeadingChar("0", dr("StuEnrollId").ToString()))
                Dim drs() As DataRow = dtStudents.Select(sql.ToString.Trim)

                If drs.Length = 0 Then
                ElseIf drs.Length > 1 Then
                Else
                    dr("StuEnrollId") = drs(0)("StuEnrollId").ToString()
                    dr("ScheduleId") = drs(0)("ScheduleId").ToString()
                End If
            Next
        End Sub

        Public Function SaveTardyMarked(ByVal stuEnrollID As String, ByVal ScheduleId As String, ByVal RecordDate As Date) As String
            Dim DB As New TimeClockDB
            DB.SaveTardyMarked(stuEnrollID, ScheduleId, RecordDate)
            Return ""
        End Function
        Public Function GetHrsforaStudent(ByVal stuEnrollid As String, ByVal SchDate As Date) As DataSet

            Dim DB As New TimeClockDB
            Return DB.GetHrsforaStudent(stuEnrollid, SchDate)
        End Function

        Protected Sub AddPunchesToDatabasefortimeclock(ByVal dt As DataTable)
            Dim delelterows As String = TimeClockDB.DeletePunchfortimeclock(dt)
            For Each dr As DataRow In dt.Rows
                Dim info As TimeClockPunchInfo = ToTimeClockPunchInfo(dr)
                Dim res As String = TimeClockDB.AddPunch(info)
                If res <> "" AndAlso bOverWritePunches Then
                    TimeClockDB.UpdatePunch(info)
                End If
            Next
        End Sub

        Protected Sub AddPunchesToDatabasefortimeclock_byClass(ByVal dt As DataTable)
            Dim delelterows As String = TimeClockDB.DeletePunchfortimeclock_ByClass(dt)
            For Each dr As DataRow In dt.Rows

                Dim info As TimeClockPunchInfo = ToTimeClockPunchInfo_ByClass(dr)

                Dim res As String = TimeClockDB.AddPunch_byClass_PopUp(info)
                If res <> "" AndAlso bOverWritePunches Then
                    TimeClockDB.UpdatePunch_byClass(info)
                End If
            Next
        End Sub

        Public Function GetImportFilePermissions(ByVal userId As String, ByVal resource As String, Optional ByVal campusId As String = Nothing) As UserPagePermissionInfo
            Dim dbTimeClock As New TimeClockDB
            Dim dt1 As New DataTable
            Dim dt2 As New DataTable
            Dim uppInfo As New UserPagePermissionInfo
            Dim dRow1 As DataRow
            Dim dRows() As DataRow
            Dim sFilter As String
            Dim iCounter As Integer

            dt2 = dbTimeClock.GetResourceRoles(resource)

            If Not campusId Is Nothing Then
                dt1 = dbTimeClock.GetUserRolesForCurrentCampus(userId, campusId)
            Else
                dt1 = dbTimeClock.GetUserRoles(userId)
            End If

            If dt1.Rows.Count > 0 Then
                For Each dRow1 In dt1.Rows
                    sFilter = "RoleId = '" + dRow1("RoleId").ToString() + "'"
                    dRows = dt2.Select(sFilter)

                    If dRows.Length > 0 Then
                        For iCounter = 0 To dRows.Length - 1
                            Dim accessLevel As Integer = CType(dRows(iCounter)("AccessLevel"), Integer)

                            If accessLevel = 0 Then uppInfo.HasNone = True

                            If accessLevel = 15 Then
                                uppInfo.HasFull = True
                                uppInfo.HasNone = False
                            End If

                            If accessLevel - 8 >= 0 Then
                                uppInfo.HasEdit = True
                                uppInfo.HasNone = False
                                accessLevel -= 8
                            End If

                            If accessLevel - 4 >= 0 Then
                                uppInfo.HasAdd = True
                                uppInfo.HasNone = False
                                accessLevel -= 4
                            End If

                            If accessLevel - 2 >= 0 Then
                                uppInfo.HasDelete = True
                                uppInfo.HasNone = False
                                accessLevel -= 2
                            End If

                            If accessLevel - 1 = 0 Then
                                uppInfo.HasDisplay = True
                                uppInfo.HasNone = False
                            End If

                        Next
                    End If
                Next
            End If


            Return uppInfo

        End Function


        Public Function IsStudentTardy(ByVal StuEnrollId As String, ByVal HDate As Date, ByVal PrgVersionID As String) As DataSet
            Dim TimeDb As New TimeClockDB
            Return (TimeDb).IsStudentTardy(StuEnrollId, HDate, PrgVersionID)
        End Function

        Public Function GetTimeClockPunchExceptionforaStudentforgivendate(ByVal StuEnrollId As String, ByVal FromDate As String, ByVal ToDate As String) As DataSet
            Dim TimeDb As New TimeClockDB
            Return (TimeDb).GetTimeClockPunchExceptionforaStudentforgivendate(StuEnrollId, FromDate, ToDate)
        End Function

        Public Function GetTimeClockPunchExceptionforaStudentforgivendate_ByClass(ByVal StuEnrollId As String, ByVal FromDate As String, ByVal ToDate As String) As DataSet
            Dim TimeDb As New TimeClockDB
            Return (TimeDb).GetTimeClockPunchExceptionforaStudentforgivendate_ByClass(StuEnrollId, FromDate, ToDate)
        End Function

        Public Function GetClsSectMeetingsforaStudent(ByVal StuEnrollId As String, ByVal CampusID As String) As DataTable
            Dim TimeDb As New TimeClockDB
            Return (TimeDb).GetClsSectMeetingsforaStudent(StuEnrollId, CampusID)
        End Function
        Public Function GetSpecialCodesAndClsSectionMeetingforStudent(ByVal StuEnrollId As String, ByVal CampusID As String) As DataTable
            Dim TimeDb As New TimeClockDB
            Return (TimeDb).GetSpecialCodesAndClsSectionMeetingforStudent(StuEnrollId, CampusID)
        End Function


        Public Function GetSpecialCodesforgivenCampus(ByVal CampusID As String) As DataTable
            Dim TimeDb As New TimeClockDB
            Return (TimeDb).GetSpecialCodesforgivenCampus(CampusID)
        End Function
        Public Function SavePunchesandProcessHours(ByVal dsPunches As DataSet, ByVal Username As String, ByVal CampusID As String) As String

            AddPunchesToDatabasefromEditPendingPunches(dsPunches)
            ' 
            ProcessTotalDailyHoursEditPendingPunches(dsPunches, Username, CampusID)
            Return ""
        End Function

        Public Function SavePunchesandProcessHours_ByClass(ByVal dsPunches As DataSet, ByVal Username As String, ByVal CampusID As String) As String

            AddPunchesToDatabasefromEditPendingPunches_ByClass(dsPunches)
            ' 
            ProcessTotalDailyHoursEditPendingPunches_ByClass(dsPunches, Username, CampusID)
            Return ""
        End Function


        Protected Sub AddPunchesToDatabasefromEditPendingPunches(ByVal dspunch As DataSet)


            Dim StuPunDate As String
            Dim res As String
            Dim dtDelete As DataTable
            dtDelete = dspunch.Tables(2)
            Dim BadgeID As String
            Dim PunchType As Integer
            Dim PunchTime As Date
            For Each drrow As DataRow In dtDelete.Rows

                BadgeID = Math.Abs(CInt(drrow("BadgeId").ToString())).ToString
                PunchType = drrow("PunchType")
                PunchTime = drrow("PunchTime")
                res = TimeClockDB.DeletePunchfortimeclockEditPendingPunches(BadgeID, PunchType, PunchTime)
            Next

            Dim dt As DataTable
            dt = dspunch.Tables(0)
            For Each dr As DataRow In dt.Rows

                Dim info As New TimeClockPunchInfo
                info.BadgeId = Math.Abs(CInt(dr("BadgeId").ToString())).ToString
                info.ClockId = dr("ClockId").ToString()
                info.Status = PunchStatus.Inserted
                info.PunchType = dr("PunchType")
                info.StuEnrollId = dr("StuEnrollId").ToString()
                info.FromSystem = dr("FromSystem").ToString()

                StuPunDate = CDate(dr("Date")) + " " + dr("PunchTime1")
                Dim concatenatedDate As DateTime = DateTime.Parse(StuPunDate)
                info.PunchTime = concatenatedDate

                res = TimeClockDB.AddPunchTimeClockException(info)
                If res <> "" AndAlso bOverWritePunches Then
                    TimeClockDB.UpdatePunchTimeClockException(info)
                End If
            Next
        End Sub


        Protected Sub AddPunchesToDatabasefromEditPendingPunches_ByClass(ByVal dspunch As DataSet)


            Dim StuPunDate As String
            Dim res As String
            Dim dtDelete As DataTable
            dtDelete = dspunch.Tables(2)
            Dim BadgeID As String
            Dim PunchType As Integer
            Dim PunchTime As Date
            Dim ClsSectMeetingID As String
            Dim SpecialCode As String

            For Each drrow As DataRow In dtDelete.Rows

                BadgeID = Math.Abs(CInt(drrow("BadgeId").ToString())).ToString
                PunchType = drrow("PunchType")
                PunchTime = drrow("PunchTime")
                ClsSectMeetingID = drrow("ClsSectMeetingID")
                SpecialCode = drrow("SpecialCode")


                res = TimeClockDB.DeletePunchfortimeclockEditPendingPunches_ByClass(BadgeID, PunchType, PunchTime, ClsSectMeetingID, SpecialCode)
            Next

            Dim dt As DataTable
            dt = dspunch.Tables(0)
            Dim dRows() As DataRow
            For Each dr As DataRow In dt.Rows
                Dim errString As String = String.Empty
                Dim info As New TimeClockPunchInfo
                info.BadgeId = Math.Abs(CInt(dr("BadgeId").ToString())).ToString
                info.ClockId = dr("ClockId").ToString()
                info.Status = PunchStatus.Inserted
                info.PunchType = dr("PunchType")
                info.StuEnrollId = dr("StuEnrollId").ToString()
                info.FromSystem = dr("FromSystem").ToString()
                info.ClsSectMeetingId = dr("ClsSectMeetingID").ToString()
                info.SpecialCode = dr("SpecialCode").ToString()
                StuPunDate = dr("Date") + " " + dr("PunchTime1")
                Dim concatenatedDate As DateTime = DateTime.Parse(StuPunDate)
                info.PunchTime = concatenatedDate

                res = TimeClockDB.AddPunchTimeClockException_ByClass(info)
                If res <> "" AndAlso bOverWritePunches Then
                    TimeClockDB.UpdatePunchTimeClockException_ByClass(info)
                End If
            Next
        End Sub

        Protected Sub ProcessTotalDailyHoursEditPendingPunches(ByVal dsTimeClock As DataSet, ByVal UserName As String, ByVal CampusID As String)
            Dim dtNewPunches As New DataTable
            Dim rowcount As Integer
            Dim StuPunDate As String
            Dim dt As DataTable = dsTimeClock.Tables(0)

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            dt.Columns.Add("PunchTime", Type.GetType("System.DateTime"))
            For rowcount = 0 To dsTimeClock.Tables(0).Rows.Count - 1
                StuPunDate = CDate(dsTimeClock.Tables(0).Rows(rowcount)("Date")) + " " + dsTimeClock.Tables(0).Rows(rowcount)("PunchTime1")
                dt.Rows(rowcount)("PunchTime") = DateTime.Parse(StuPunDate)
                dt.Rows(rowcount)("Status") = PunchStatus.Inserted
            Next
            dt.Columns.Remove("Date")
            dt.Columns.Remove("PunchTime1")
            dt.AcceptChanges()

            Dim dv As DataView = New DataView(dt, "", "StuEnrollId, PunchTime ASC, PunchType ASC", DataViewRowState.None)
            dtNewPunches = dt.Clone

            Dim lastStuEnrollId As String = ""
            For Each dr As DataRow In dv.Table.Rows
                Dim StuEnrollId As String = dr("StuEnrollId").ToString()
                Dim ScheduleId As String = dr("ScheduleId").ToString()
                Dim StuStartDate As String = dsTimeClock.Tables(1).Rows(0)("StartDate").ToString
                Dim StuGradDate As String = dsTimeClock.Tables(1).Rows(0)("ExpGradDate").ToString
                If StuEnrollId <> lastStuEnrollId Then
                    Dim schedInfo As ScheduleInfo = SchedulesDB.GetInfo(ScheduleId)
                    Dim sdInfo() As ScheduleDetailInfo = schedInfo.Details()
                    Dim sql As String = String.Format("StuEnrollId='{0}'", StuEnrollId)
                    Dim minDate As DateTime = dt.Compute("min(PunchTime)", sql)
                    Dim maxDate As DateTime = dt.Compute("max(PunchTime)", sql)
                    Dim curDate As DateTime = minDate

                    While curDate <= maxDate
                        Dim InPunches As Integer = 0
                        Dim OutPunches As Integer = 0
                        Dim CanPostonHoliday As Boolean = False
                        Dim StatusSet As Boolean = False

                        Dim dow As Integer = CType(curDate.DayOfWeek, Integer)
                        Dim drPunches() As DataRow = GetPunchesDR(dt, StuEnrollId, curDate)


                        Dim IsLOADate As Boolean
                        Dim IsSuspendedDate As Boolean
                        Dim inti As Integer
                        If curDate > Date.Now Then
                            For inti = 0 To drPunches.Length - 1
                                drPunches(inti)("Status") = PunchStatus.NotScheduled
                            Next
                            If drPunches.Length > 0 Then
                                StatusSet = True
                                AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                AddMsg("Date:", curDate)
                                AddMsg("Not imported. Since,", "Future date is given")
                            End If

                        End If

                        If StatusSet = False Then
                            IsLOADate = (New AttendanceFacade).IsStudentLOADate(StuEnrollId, curDate)
                            If IsLOADate = True Then
                                For inti = 0 To drPunches.Length - 1
                                    drPunches(inti)("Status") = PunchStatus.NotScheduled
                                Next
                                If drPunches.Length > 0 Then
                                    StatusSet = True
                                    AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                    AddMsg("Date:", curDate)
                                    AddMsg("Not imported. Since, ", "Student is in LOA")
                                End If
                            End If
                        End If
                        If StatusSet = False Then

                            IsSuspendedDate = (New AttendanceFacade).IsStudentSuspendedDate(StuEnrollId, curDate)
                            If IsSuspendedDate = True Then
                                For inti = 0 To drPunches.Length - 1
                                    drPunches(inti)("Status") = PunchStatus.NotScheduled
                                Next
                                If drPunches.Length > 0 Then
                                    StatusSet = True
                                    AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                    AddMsg("Date:", curDate)
                                    AddMsg("Not imported. Since, Student ", " is Suspended")
                                End If
                            End If
                        End If
                        If StatusSet = False Then
                            If curDate < StuStartDate Then
                                For inti = 0 To drPunches.Length - 1
                                    drPunches(inti)("Status") = PunchStatus.NotScheduled
                                Next
                                If drPunches.Length > 0 Then
                                    StatusSet = True
                                    AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                    AddMsg("Date:", curDate)
                                    AddMsg("Not imported. Since, Student", " Punch is before StartDate")
                                End If
                            End If
                        End If

                        If IsHoliday(curDate) Then
                            If Not MyAdvAppSettings.AppSettings("postattendanceonholiday") Is Nothing Then
                                If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                                    If StatusSet = False Then

                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.NotScheduled
                                        Next
                                        If drPunches.Length > 0 Then
                                            StatusSet = True
                                            AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                            AddMsg("Date:", curDate)
                                            AddMsg("Not imported. Since, Student", " Punch is on a Holiday")
                                        End If
                                    End If
                                ElseIf MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "yes" Then
                                    CanPostonHoliday = True
                                End If
                            Else
                                If StatusSet = False Then

                                    For inti = 0 To drPunches.Length - 1
                                        drPunches(inti)("Status") = PunchStatus.NotScheduled
                                    Next
                                    If drPunches.Length > 0 Then
                                        StatusSet = True
                                        AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Date:", curDate)
                                        AddMsg("Not imported. Since, Student", " Punch is on a Holiday")
                                    End If
                                End If
                            End If

                        End If

                        If StatusSet = False Then
                            If drPunches.Length > 0 Then
                                For inti = 0 To drPunches.Length - 1
                                    If CType(drPunches(inti)("PunchType"), TimeClockPunchType) = TimeClockPunchType.PunchIn Then
                                        InPunches = InPunches + 1
                                    ElseIf CType(drPunches(inti)("PunchType"), TimeClockPunchType) = TimeClockPunchType.PunchOut Then
                                        OutPunches = OutPunches + 1
                                    End If
                                Next inti
                                If StatusSet = False Then
                                    If OutPunches = 0 Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.Invalid
                                        Next
                                        If drPunches.Length > 0 Then
                                            StatusSet = True
                                            AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                            AddMsg("Date:", curDate)
                                            AddMsg("Not imported. Since, Student ", " Punch Out is not found")
                                        End If
                                    End If
                                End If
                                If StatusSet = False Then
                                    If InPunches = 0 Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.Invalid
                                        Next
                                        If drPunches.Length > 0 Then
                                            StatusSet = True
                                            AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                            AddMsg("Date:", curDate)
                                            AddMsg("Not imported. Since, Student ", " Punch In is not found")
                                        End If
                                    End If
                                End If
                                If StatusSet = False Then
                                    If InPunches <> OutPunches Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.Invalid
                                        Next
                                        If drPunches.Length > 0 Then
                                            StatusSet = True
                                            AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                            AddMsg("Not imported. Since, Student", " Punch In and Punch Out does not match")
                                        End If
                                    End If
                                End If

                            End If
                        End If


                        If drPunches.Length >= 2 Then
                            If drPunches(0)("Status") <> PunchStatus.Invalid And drPunches(0)("Status") <> PunchStatus.NotScheduled Then
                                Dim totHrs As Decimal
                                If CanPostonHoliday = True Then
                                    totHrs = GetTotalDailyHoursForTimeClock(drPunches, sdInfo(dow))
                                Else
                                    totHrs = GetTotalDailyHours(drPunches, sdInfo(dow))

                                End If

                                Dim drdata As DataRow
                                For i As Integer = 0 To drPunches.Length - 1


                                    drdata = dtNewPunches.NewRow()
                                    drdata("BadgeId") = drPunches(i)("BadgeId")
                                    drdata("ClockId") = drPunches(i)("ClockId")
                                    drdata("StuEnrollId") = drPunches(i)("StuEnrollId")
                                    drdata("PunchType") = drPunches(i)("PunchType")
                                    drdata("PunchTime") = drPunches(i)("PunchTime")
                                    drdata("FromSystem") = drPunches(i)("FromSystem")
                                    drdata("Status") = drPunches(i)("Status")
                                    dtNewPunches.Rows.Add(drdata)

                                Next
                                AddMsg("ComputeHours", String.Format("{0} [{1}] Total Hours = {2}",
                                            drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c), curDate.ToShortDateString(), totHrs))
                                Dim res As String

                                If CanPostonHoliday = True Then
                                    res = TimeClockDB.SaveTotalDailyHours(curDate.ToShortDateString(), StuEnrollId, ScheduleId, totHrs, sdInfo(dow).Check_TardyIn, 0, schedInfo.PrgVerId, UserName, CampusID)
                                    CanPostonHoliday = False
                                Else
                                    res = TimeClockDB.SaveTotalDailyHours(curDate.ToShortDateString(), StuEnrollId, ScheduleId, totHrs, sdInfo(dow).Check_TardyIn, sdInfo(dow).Total, schedInfo.PrgVerId, UserName, CampusID)

                                End If
                                If res <> "" Then
                                    AddMsg("ComputeHours", res)
                                End If
                            End If
                        End If
                        TimeClockDB.SetPunchStatus(drPunches)
                        curDate = curDate.AddDays(1)
                    End While
                End If
                lastStuEnrollId = StuEnrollId
            Next

            Dim infoArr() As TimeClockPunchInfo = ToTimeClockPunchInfoArray(dtNewPunches)
            For Each info As TimeClockPunchInfo In infoArr
                If Not info.Status = PunchStatus.Invalid And Not info.Status = PunchStatus.NotScheduled Then
                    info.Status = PunchStatus.Ok
                End If

            Next
            Dim res2 As String = TimeClockDB.SaveTimeClockPunches(infoArr)
            If res2 = "" Then
                AddMsg("ComputeHours:SaveTimeClockPunches", "Ok")
            Else
                AddMsg("ComputeHours:SaveTimeClockPunches", res2)
            End If

            dt.Columns.Add("Date", Type.GetType("System.DateTime"))
            dt.Columns.Add("PunchTime1", Type.GetType("System.DateTime"))
            For rowcount = 0 To dsTimeClock.Tables(0).Rows.Count - 1
                dt.Rows(rowcount)("Date") = Format(dsTimeClock.Tables(0).Rows(0)("PunchTime"), "d")
                dt.Rows(rowcount)("PunchTime1") = Format(dsTimeClock.Tables(0).Rows(0)("PunchTime"), "hh:mm tt")
            Next
            dt.Columns.Remove("PunchTime")
            dt.AcceptChanges()

        End Sub



        Protected Sub ProcessTotalDailyHoursEditPendingPunches_ByClass(ByVal dsTimeClock As DataSet, ByVal UserName As String, ByVal CampusID As String)
            Dim dtNewPunches As New DataTable
            Dim rowcount As Integer
            Dim StuPunDate As String
            Dim dt As DataTable = dsTimeClock.Tables(0)

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            dt.Columns.Add("PunchTime", Type.GetType("System.DateTime"))
            For rowcount = 0 To dsTimeClock.Tables(0).Rows.Count - 1
                StuPunDate = dsTimeClock.Tables(0).Rows(rowcount)("Date") + " " + dsTimeClock.Tables(0).Rows(rowcount)("PunchTime1")
                dt.Rows(rowcount)("PunchTime") = DateTime.Parse(StuPunDate)
                dt.Rows(rowcount)("Status") = PunchStatus.Inserted
            Next
            dt.Columns.Remove("Date")
            dt.Columns.Remove("PunchTime1")
            dt.AcceptChanges()

            Dim dv As DataView = New DataView(dt, "", "StuEnrollId, PunchTime ASC, PunchType ASC", DataViewRowState.None)
            dtNewPunches = dt.Clone

            Dim ClsSectMeetingId As String

            Dim ClsSectMeetingQuery = (From PunchesTable In dt.AsEnumerable() Select ClsSectMeetId = PunchesTable("ClsSectMeetingID")).Distinct

            For Each meetingItem In ClsSectMeetingQuery

                ClsSectMeetingId = meetingItem.ToString
                Dim lastStuEnrollId As String = ""
                Dim StuEnrollId As String = dt.Rows(0)("StuEnrollId").ToString
                Dim StuStartDate As String = dsTimeClock.Tables(1).Rows(0)("StartDate").ToString
                Dim StuGradDate As String = dsTimeClock.Tables(1).Rows(0)("ExpGradDate").ToString

                Dim schedInfo As ClsSectionMeetingScheduleInfo = TimeClockDB.GetClsSectionMeetingScheduleInfo(ClsSectMeetingId)
                Dim sdInfo() As ClsSectMeetingScheduledetailsInfo = schedInfo.Details()

                Dim objClsAttFac As New ClsSectAttendanceFacade
                Dim arrCancelDates As ArrayList
                If Not ClsSectMeetingId = System.Guid.Empty.ToString Then
                    arrCancelDates = objClsAttFac.GetCancelDays_byClass(schedInfo.ClsSectionID, ClsSectMeetingId, CampusID)
                End If


                Dim sql As String = String.Format("StuEnrollId='{0}' and ClsSectMeetingID='{1}' ", StuEnrollId, ClsSectMeetingId)

                Dim minDate As DateTime = CDate(dt.Compute("min(PunchTime)", sql)).ToShortDateString
                Dim maxDate As DateTime = CDate(dt.Compute("max(PunchTime)", sql)).ToShortDateString
                Dim curDate As DateTime = minDate


                While curDate <= maxDate
                    Dim InPunches As Integer = 0
                    Dim OutPunches As Integer = 0
                    Dim CanPostonHoliday As Boolean = False
                    Dim StatusSet As Boolean = False

                    Dim dow As Integer = CType(curDate.DayOfWeek, Integer)
                    Dim drPunches() As DataRow = GetPunchesDR_ByClass(dt, StuEnrollId, curDate, ClsSectMeetingId)

                    Dim IsLOADate As Boolean
                    Dim IsSuspendedDate As Boolean
                    Dim inti As Integer

                    If curDate > Date.Now Then
                        For inti = 0 To drPunches.Length - 1
                            drPunches(inti)("Status") = PunchStatus.NotScheduled
                        Next
                        If drPunches.Length > 0 Then
                            StatusSet = True
                            AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                            AddMsg("Date:", curDate)
                            AddMsg("Not imported. Since,", "Future date is given")
                        End If

                    End If

                    If StatusSet = False Then
                        IsLOADate = (New AttendanceFacade).IsStudentLOADate(StuEnrollId, curDate)
                        If IsLOADate = True Then
                            For inti = 0 To drPunches.Length - 1
                                drPunches(inti)("Status") = PunchStatus.NotScheduled
                            Next
                            If drPunches.Length > 0 Then
                                StatusSet = True
                                AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                AddMsg("Date:", curDate)
                                AddMsg("Not imported. Since, ", "Student is in LOA")
                            End If
                        End If
                    End If
                    If StatusSet = False Then

                        IsSuspendedDate = (New AttendanceFacade).IsStudentSuspendedDate(StuEnrollId, curDate)
                        If IsSuspendedDate = True Then
                            For inti = 0 To drPunches.Length - 1
                                drPunches(inti)("Status") = PunchStatus.NotScheduled
                            Next
                            If drPunches.Length > 0 Then
                                StatusSet = True
                                AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                AddMsg("Date:", curDate)
                                AddMsg("Not imported. Since, Student ", " is Suspended")
                            End If
                        End If
                    End If
                    If StatusSet = False Then
                        If curDate < StuStartDate Then
                            For inti = 0 To drPunches.Length - 1
                                drPunches(inti)("Status") = PunchStatus.NotScheduled
                            Next
                            If drPunches.Length > 0 Then
                                StatusSet = True
                                AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                AddMsg("Date:", curDate)
                                AddMsg("Not imported. Since, Student", " Punch is before StartDate")
                            End If
                        End If
                    End If

                    If isMeetingDayCancelled(arrCancelDates, curDate) Then
                        CanPostonHoliday = True
                    End If
                    If Not sdInfo Is Nothing Then
                        If IsHoliday(curDate, CampusID, Format(sdInfo(dow).TimeIn, "HH:mm:ss"), Format(sdInfo(dow).TimeOut, "HH:mm:ss")) Then
                            If Not MyAdvAppSettings.AppSettings("postattendanceonholiday") Is Nothing Then
                                If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                                    If StatusSet = False Then

                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.NotScheduled
                                        Next
                                        If drPunches.Length > 0 Then
                                            StatusSet = True
                                            AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                            AddMsg("Date:", curDate)
                                            AddMsg("Not imported. Since, Student", " Punch is on a Holiday")
                                        End If
                                    End If
                                ElseIf MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "yes" Then
                                    CanPostonHoliday = True
                                End If
                            Else
                                If StatusSet = False Then

                                    For inti = 0 To drPunches.Length - 1
                                        drPunches(inti)("Status") = PunchStatus.NotScheduled
                                    Next
                                    If drPunches.Length > 0 Then
                                        StatusSet = True
                                        AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Date:", curDate)
                                        AddMsg("Not imported. Since, Student", " Punch is on a Holiday")
                                    End If
                                End If
                            End If

                        End If
                    Else
                        If IsHoliday(curDate, CampusID) Then
                            If Not MyAdvAppSettings.AppSettings("postattendanceonholiday") Is Nothing Then
                                If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                                    If StatusSet = False Then

                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.NotScheduled
                                        Next
                                        If drPunches.Length > 0 Then
                                            StatusSet = True
                                            AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                            AddMsg("Date:", curDate)
                                            AddMsg("Not imported. Since, Student", " Punch is on a Holiday")
                                        End If
                                    End If
                                ElseIf MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "yes" Then
                                    CanPostonHoliday = True
                                End If
                            Else
                                If StatusSet = False Then

                                    For inti = 0 To drPunches.Length - 1
                                        drPunches(inti)("Status") = PunchStatus.NotScheduled
                                    Next
                                    If drPunches.Length > 0 Then
                                        StatusSet = True
                                        AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Date:", curDate)
                                        AddMsg("Not imported. Since, Student", " Punch is on a Holiday")
                                    End If
                                End If
                            End If

                        End If
                    End If
                    If StatusSet = False Then
                        If drPunches.Length > 0 Then
                            For inti = 0 To drPunches.Length - 1
                                If CType(drPunches(inti)("PunchType"), TimeClockPunchType) = TimeClockPunchType.PunchIn Then
                                    InPunches = InPunches + 1
                                ElseIf CType(drPunches(inti)("PunchType"), TimeClockPunchType) = TimeClockPunchType.PunchOut Then
                                    OutPunches = OutPunches + 1
                                End If
                            Next inti
                            If StatusSet = False Then
                                If OutPunches = 0 Then
                                    For inti = 0 To drPunches.Length - 1
                                        drPunches(inti)("Status") = PunchStatus.Invalid
                                    Next
                                    If drPunches.Length > 0 Then
                                        StatusSet = True
                                        AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Date:", curDate)
                                        AddMsg("Not imported. Since, Student ", " Punch Out is not found")
                                    End If
                                End If
                            End If
                            If StatusSet = False Then
                                If InPunches = 0 Then
                                    For inti = 0 To drPunches.Length - 1
                                        drPunches(inti)("Status") = PunchStatus.Invalid
                                    Next
                                    If drPunches.Length > 0 Then
                                        StatusSet = True
                                        AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Date:", curDate)
                                        AddMsg("Not imported. Since, Student ", " Punch In is not found")
                                    End If
                                End If
                            End If
                            If StatusSet = False Then
                                If InPunches <> OutPunches Then
                                    For inti = 0 To drPunches.Length - 1
                                        drPunches(inti)("Status") = PunchStatus.Invalid
                                    Next
                                    If drPunches.Length > 0 Then
                                        StatusSet = True
                                        AddMsg("Badge Id:", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Not imported. Since, Student", " Punch In and Punch Out does not match")
                                    End If
                                End If
                            End If
                        End If
                    End If

                    If drPunches.Length >= 2 And Not ClsSectMeetingId = System.Guid.Empty.ToString Then
                        If drPunches(0)("Status") <> PunchStatus.Invalid And drPunches(0)("Status") <> PunchStatus.NotScheduled Then

                            Dim totHrs As Decimal
                            If CanPostonHoliday = True Then
                                totHrs = GetTotalDailyHoursForTimeClock_byClass(drPunches)
                            Else
                                totHrs = GetTotalDailyHours_byClass(drPunches, sdInfo(dow))

                            End If

                            Dim drdata As DataRow
                            For i As Integer = 0 To drPunches.Length - 1


                                drdata = dtNewPunches.NewRow()
                                drdata("BadgeId") = drPunches(i)("BadgeId")
                                drdata("ClockId") = drPunches(i)("ClockId")
                                drdata("StuEnrollId") = drPunches(i)("StuEnrollId")
                                drdata("PunchType") = drPunches(i)("PunchType")
                                drdata("PunchTime") = drPunches(i)("PunchTime")
                                drdata("FromSystem") = drPunches(i)("FromSystem")
                                drdata("Status") = drPunches(i)("Status")
                                drdata("ClsSectMeetingID") = drPunches(i)("ClsSectMeetingID")
                                drdata("SpecialCode") = drPunches(i)("SpecialCode")

                                dtNewPunches.Rows.Add(drdata)

                            Next

                            AddMsg("ComputeHours", String.Format("{0} [{1}] Total Hours = {2}",
                                        drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c), curDate.ToShortDateString(), totHrs))
                            Dim res As String
                            If CanPostonHoliday = True Then
                                res = TimeClockDB.SaveTotalDailyHours_ByClass(curDate.ToShortDateString(), StuEnrollId, ClsSectMeetingId, totHrs, sdInfo(dow).Check_TardyIn, 0, UserName, CampusID, schedInfo.UnitType, drPunches)
                                CanPostonHoliday = False
                            Else
                                res = TimeClockDB.SaveTotalDailyHours_ByClass(curDate.ToShortDateString(), StuEnrollId, ClsSectMeetingId, totHrs, sdInfo(dow).Check_TardyIn, sdInfo(dow).Total, UserName, CampusID, schedInfo.UnitType, drPunches)

                            End If
                            If res <> "" Then
                                AddMsg("ComputeHours", res)
                            End If
                        Else
                            TimeClockDB.SetPunchStatus_ByClass(drPunches)

                        End If
                    Else
                        TimeClockDB.SetPunchStatus_ByClass(drPunches)
                    End If
                    curDate = curDate.AddDays(1)
                End While
                lastStuEnrollId = StuEnrollId
            Next

            Dim infoArr() As TimeClockPunchInfo = ToTimeClockPunchInfoArray_byClass(dtNewPunches)
            For Each info As TimeClockPunchInfo In infoArr
                If Not info.Status = PunchStatus.Invalid And Not info.Status = PunchStatus.NotScheduled Then
                    info.Status = PunchStatus.Ok
                End If

            Next
            Dim res2 As String = TimeClockDB.SaveTimeClockPunches_ByClass(infoArr)
            If res2 = "" Then
                AddMsg("ComputeHours:SaveTimeClockPunches", "Ok")
            Else
                AddMsg("ComputeHours:SaveTimeClockPunches", res2)
            End If

            dt.Columns.Add("Date", Type.GetType("System.DateTime"))
            dt.Columns.Add("PunchTime1", Type.GetType("System.DateTime"))
            For rowcount = 0 To dsTimeClock.Tables(0).Rows.Count - 1
                dt.Rows(rowcount)("Date") = Format(dsTimeClock.Tables(0).Rows(0)("PunchTime"), "d")
                dt.Rows(rowcount)("PunchTime1") = Format(dsTimeClock.Tables(0).Rows(0)("PunchTime"), "hh:mm tt")
            Next
            dt.Columns.Remove("PunchTime")
            dt.AcceptChanges()

        End Sub



        Public Function DeletePunchandUpdateClockAttendance(ByVal stuEnrollId As String, ByVal PunchTime As Date, ByVal ScheduledHours As Decimal) As String
            Dim TimeDb As New TimeClockDB
            Return (TimeDb).DeletePunchandUpdateClockAttendance(stuEnrollId, PunchTime, ScheduledHours)
        End Function

        Public Function DeletePunchandUpdateClockAttendance_ByClass(ByVal stuEnrollId As String, ByVal PunchTime As Date, ByVal ScheduledHours As Decimal, ByVal ClsSectMeetingID As String) As String
            Dim TimeDb As New TimeClockDB
            Return (TimeDb).DeletePunchandUpdateClockAttendance_byClass(stuEnrollId, PunchTime, ScheduledHours, ClsSectMeetingID)
        End Function

        Public Function UpdateClockAttendance(ByVal stuEnrollId As String, ByVal PunchTime As Date, ByVal ScheduledHours As Decimal) As String
            Dim TimeDb As New TimeClockDB
            Return (TimeDb).UpdateClockAttendance(stuEnrollId, PunchTime, ScheduledHours)
        End Function

        Public Function UpdateScheduledHours(ByVal stuEnrollId As String, ByVal PunchTime As Date, ByVal ScheduledHours As Decimal, ByVal CampusID As String, ByVal TardyMarked As Boolean) As String
            Dim TimeDb As New TimeClockDB
            Return (TimeDb).UpdateScheduledHours(stuEnrollId, PunchTime, ScheduledHours, CampusID, TardyMarked)
        End Function

        Public Function GetTimeClockSourceandTargetLocations(ByVal Campusid As String) As DataSet
            Dim TimeDb As New TimeClockDB
            Return (TimeDb).GetTimeClockSourceandTargetLocations(Campusid)
        End Function
        Public Function ProcessFLFile(ByRef clsflFile As TCFileInfo, Optional ByVal SourceFolderLocation As String = "") As String
            Dim strRecord As String = ""
            Dim intLoopCount As Integer = 0
            Dim intBytesRead As Integer = 0
            Dim Request As WebRequest
            Dim ResponseStream As Stream
            Dim Response As WebResponse
            Dim contents As String = ""
            Dim myCredentials As NetworkCredential
            ProcessFLFile = False
            Try
                If clsflFile.datafromremotecomputer = True Then
                    myCredentials = New NetworkCredential(clsflFile.networkuser, clsflFile.networkPassword, "")
                End If
                Request = System.Net.FileWebRequest.Create(clsflFile.strFileName)
                Request.Headers.Add("Translate: f")
                If clsflFile.datafromremotecomputer = True Then
                    Request.Credentials = myCredentials
                Else
                    Request.Credentials = CredentialCache.DefaultCredentials
                End If
                Response = CType(Request.GetResponse, WebResponse)
                ResponseStream = Response.GetResponseStream

                Dim uriSource As New Uri(clsflFile.strFileName)
                Dim strSourcePath As String = uriSource.AbsolutePath
                Dim objIn As StreamReader
                If SourceFolderLocation = "" Then
                    objIn = New StreamReader(strSourcePath, System.Text.Encoding.Default)
                Else
                    objIn = New StreamReader(SourceFolderLocation, System.Text.Encoding.Default)
                End If


                Try
                    contents = objIn.ReadToEnd()
                Catch ex As Exception
                    contents = Nothing
                End Try

                objIn.Close()

                ResponseStream.Flush()
                ResponseStream.Close()
                Response.Close()
            Catch e As ArgumentNullException
                Throw
            Catch e As ArgumentOutOfRangeException
                Throw
            Catch e As System.Exception
                Throw
            End Try
            Return contents
        End Function



        Public Function GetClsCategories() As DataTable
            Dim TimeDb As New TimeClockDB

            Return TimeDb.GetAllClsCategories_SP()


        End Function


#Region "TimeClockSpecialCode"

        Public Function GetAllClockSpecialCodes() As DataSet

            Return (New TimeClockDB).GetAllClockSpecialCodes()

        End Function


        Public Function GetClockSpecialCodeInfo(ByVal TCSId As String) As TimeClockSpecialCodeInfo

            With New TimeClockDB

                Return .GetClockSpecialCodeInfo(TCSId)

            End With

        End Function

        Public Function UpdateClockSpecialCodeInfo(ByVal TimeClockSpecialCodeInfo As TimeClockSpecialCodeInfo, ByVal user As String) As String

            With New TimeClockDB

                If Not (TimeClockSpecialCodeInfo.IsInDB = True) Then
                    Return .AddClockSpecialCodeInfo(TimeClockSpecialCodeInfo, user)
                Else
                    Return .UpdateClockSpecialCodeInfo(TimeClockSpecialCodeInfo, user)
                End If

            End With

        End Function
        Public Function DeleteClockSpecialCodeInfo(ByVal TCSId As String, ByVal modDate As DateTime) As String

            With New TimeClockDB

                Return .DeleteClockSpecialCodeInfo(TCSId, modDate)

            End With

        End Function

        Public Function CheckTimeClockSpecialCodeDup(ByVal TCSpecialCode As String, ByVal CampusId As String) As String

            Dim DB As New TimeClockDB
            Return DB.CheckTimeClockSpecialCodeDup(TCSpecialCode, CampusId)

        End Function

        Public Function CheckTimeClockSpecialCodePunchDup(ByVal TCSpecialCode As String, ByVal PunchType As Integer) As String

            Dim DB As New TimeClockDB
            Return DB.CheckTimeClockSpecialCodePunchDup(TCSpecialCode, PunchType)

        End Function

#End Region


        Public Function Run_ByClass(ByVal buf As String, ByVal UserName As String, ByVal CampusId As String, ByVal FILENAME As String) As String
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Dim PunchData() As String = buf.Split(vbCrLf)
            If (Not PunchData(0).StartsWith("\A!") And Not PunchData(0).StartsWith("\A'") And Not PunchData(0).StartsWith("PN") And Not PunchData(0).StartsWith("01") And Not PunchData(0).StartsWith("02")) Then
                AddMsg("File Type ", String.Format("Invalid"))
                Return ""
            End If
            Dim type As TimeClockDataType = GetFileType(buf)
            AddMsg("Run", String.Format("Detected file type is {0}", type.ToString()))

            Dim dtPunches As DataTable = Me.ImportTimeClockData_byClass(buf, type, CampusId)
            LogResultOfImport(dtPunches)

            If dtPunches Is Nothing Then
                AddMsg("Run", "Failed to process punches.  Processing has been terminated because the import method returned nothing")
                Return ""
            End If
            FillEnrollmentInfoNew(dtPunches)
            DeletePunchesinImportFile_byClass(dtPunches)
            Dim ExceptionMessages As New StringBuilder
            AddPunchesToDatabase_byClass(dtPunches, CampusId, ExceptionMessages)
            If Not ExceptionMessages.ToString = String.Empty Then
                AddMsgNew("Exception: Class Meeting Not Identified")
                AddMsgNew(ExceptionMessages.ToString)
            End If
            Dim res As String
            res = ProcessTotalDailyHours_byClass(UserName, CampusId)

            Dim StuEnroll As DataTable
            StuEnroll = dtPunches.DefaultView.ToTable(True, "StuEnrollId")

            If (Not HttpContext.Current.Session("AdvantageApiToken") Is Nothing) Then
                Try
                    Dim bucket As Guid = Guid.Empty
                    Dim enrollments = StuEnroll.AsEnumerable().Select(Function(r) r.Field(Of String)("StuEnrollId")).Where(Function(x) Guid.TryParse(x, bucket)).Select(Function(y) bucket).Distinct().ToList()
                    If (enrollments.Count > 0) Then
                        TitleIVSapUpdate(enrollments)

                    End If

                    PostPaymentPeriodAttendanceAFA(CampusId, enrollments, MyAdvAppSettings, UserName)


                Catch ex As Exception

                End Try

            End If
            UpdateAttendanceForStudentSummaryPageAdhoc(StuEnroll, UserName)

            Return ""
        End Function

        Protected Sub FillEnrollmentInfoNew(ByVal dt As DataTable)

            AddMsg("FillEnrollmentInfo", "Relating badges to student enrollment IDs")



            Dim resQuery = (From BadgeNumbers In dt.AsEnumerable() Select bgno = BadgeNumbers("BadgeId")).Distinct.ToList
            Dim listofBadgenumber As String = ""
            For Each item In resQuery
                listofBadgenumber = listofBadgenumber + trimLeadingChar("0", item) + ","
            Next
            If listofBadgenumber <> "" Then
                listofBadgenumber = listofBadgenumber.Substring(0, listofBadgenumber.Length - 1)
            End If

            Dim dtStudents As DataTable = TimeClockDB.GetEnrolmentforBadgeNumbers(listofBadgenumber)

            For Each dr As DataRow In dt.Rows
                dr("Status") = PunchStatus.Inserted
                dr("FromSystem") = False

                Dim strBatchId As String = ""

                Dim sql As String = String.Format("BadgeNumber='{0}'", trimLeadingChar("0", dr("BadgeId").ToString()))
                Dim drs() As DataRow = dtStudents.Select(sql.ToString.Trim)

                If drs.Length = 0 Then
                    AddMsg("FillEnrollmentInfo", String.Format("No student found for BadgeNumber={0} or student out of school , PunchTime={1}",
                                     dr("BadgeId").ToString.PadLeft(9, "0"c), dr("PunchTime").ToString()))
                ElseIf drs.Length > 1 Then
                    AddMsg("FillEnrollmentInfo", "Found multiple students for BadgeNumber=" + dr("BadgeId").ToString.PadLeft(9, "0"c))
                Else
                    dr("StuEnrollId") = drs(0)("StuEnrollId").ToString()
                End If
            Next
            AddMsg("FillEnrollmentInfo", "Finished relating badges to student enrollment IDs")
        End Sub

        Protected Sub DeletePunchesinImportFile_byClass(ByVal dt As DataTable)
            For Each dr As DataRow In dt.Rows
                Dim info As TimeClockPunchInfo = ToTimeClockPunchInfo_ByClass(dr)
                If Not info.StuEnrollId.Trim Is String.Empty Then
                    Dim res As String = TimeClockDB.UpdateAllPunchesToDuplicate_byClass(info)
                End If

            Next
        End Sub

        Protected Sub FillClsSectionID(ByVal dt As DataTable)
            Dim result As String

            AddMsg("FillClsSectMeetingID", "Relating student enrollment IDs to ClassSection Meetings")


            Dim resQuery = (From StudentPunches In dt.AsEnumerable() Select EnrollmentID = StudentPunches("StuEnrollId")).Distinct.ToList

            For Each item In resQuery
                Dim DatesQuery = (From meetDates In dt.AsEnumerable() Where meetDates("StuEnrollId") = item Select PunchTime = Format(meetDates("PunchTime").ToString, "MM-dd-yyyy").Distinct).ToList

                For Each punchdate In DatesQuery
                    Dim Punches = (From punchinfoforday In dt.AsEnumerable() Where punchinfoforday("StuEnrollId") = item.EnrollmentID And Format(punchinfoforday("PunchTime"), "MM-dd-yyyy") = CType(punchdate, String) Select punchinfoforday Order By punchinfoforday("PunchTime")).ToList

                    For Each punch In DatesQuery
                        result = punch("PunchTime")

                    Next

                Next

            Next


            Dim dtStudents As DataTable

            For Each dr As DataRow In dt.Rows
                dr("Status") = PunchStatus.Inserted
                dr("FromSystem") = False

                Dim strBatchId As String = ""

                Dim sql As String = String.Format("BadgeNumber='{0}'", trimLeadingChar("0", dr("BadgeId").ToString()))
                Dim drs() As DataRow = dtStudents.Select(sql.ToString.Trim)

                If drs.Length = 0 Then
                    AddMsg("FillEnrollmentInfo", String.Format("No student found for BadgeNumber={0} or student out of school , PunchTime={1}",
                                     dr("BadgeId").ToString.PadLeft(9, "0"c), dr("PunchTime").ToString()))
                ElseIf drs.Length > 1 Then
                    AddMsg("FillEnrollmentInfo", "Found multiple students for BadgeNumber=" + dr("BadgeId").ToString.PadLeft(9, "0"c))
                Else
                    dr("StuEnrollId") = drs(0)("StuEnrollId").ToString()
                End If
            Next
            AddMsg("FillEnrollmentInfo", "Finished relating badges to student enrollment IDs")
        End Sub


        Public Function isMeetingDayCancelled(ByVal arrCancelDates As ArrayList, ByVal dtm As Date) As Boolean
            Dim rtn As Boolean = False
            Dim i As Integer = 0
            If Not arrCancelDates Is Nothing Then
                If (arrCancelDates.Count = 0) Then
                    rtn = False
                Else
                    For i = 0 To arrCancelDates.Count - 1
                        If (Format(CDate(arrCancelDates(i)), "MM/dd/yyyy") = Format(dtm, "MM/dd/yyyy")) Then
                            rtn = True
                            Exit For
                        End If
                    Next
                End If

            End If

            Return rtn

        End Function

        Protected Function ProcessTotalDailyHours_byClass(ByVal UserName As String, ByVal CampusID As String) As String
            Dim dtNewPunches As New DataTable
            Dim dt As DataTable = TimeClockDB.GetPunchesInsertedandExceptions_ByClass()

            Dim dv As DataView = New DataView(dt, "", "StuEnrollId,ClsSectMeetingID, PunchTime ASC, PunchType ASC", DataViewRowState.None)
            dtNewPunches = dt.Clone
            Dim StuEnrollId As String
            Dim StuStartDate As DateTime
            Dim ExpGradDate As DateTime

            Dim ClsSectMeetingId As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If



            Dim StudentQuery = (From PunchesTable In dt.AsEnumerable() Select StuEnrId = PunchesTable("StuEnrollId")).Distinct.ToList
            For Each Studentitem In StudentQuery
                StuEnrollId = Studentitem.ToString


                Dim ClsSectMeetingQuery = (From PunchesTable In dt.AsEnumerable() Where PunchesTable.Field(Of Guid)("StuEnrollId") = New Guid(StuEnrollId) Select ClsSectMeetId = PunchesTable("ClsSectMeetingID"), StartDate = PunchesTable("StartDate"), GradDate = PunchesTable("ExpGradDate")).Distinct

                For Each meetingItem In ClsSectMeetingQuery
                    ClsSectMeetingId = meetingItem.ClsSectMeetId.ToString
                    StuStartDate = meetingItem.StartDate
                    ExpGradDate = meetingItem.GradDate

                    Dim schedInfo As ClsSectionMeetingScheduleInfo = TimeClockDB.GetClsSectionMeetingScheduleInfo(ClsSectMeetingId)
                    Dim sdInfo() As ClsSectMeetingScheduledetailsInfo = schedInfo.Details()

                    Dim objClsAttFac As New ClsSectAttendanceFacade
                    Dim arrCancelDates As ArrayList = objClsAttFac.GetCancelDays_byClass(schedInfo.ClsSectionID, ClsSectMeetingId, CampusID)

                    Dim sql As String = String.Format("StuEnrollId='{0}' and ClsSectMeetingID='{1}' ", StuEnrollId, ClsSectMeetingId)

                    Dim minDate As DateTime = CDate(dt.Compute("min(PunchTime)", sql)).ToShortDateString
                    Dim maxDate As DateTime = CDate(dt.Compute("max(PunchTime)", sql)).ToShortDateString
                    Dim curDate As DateTime = minDate

                    While curDate <= maxDate
                        Dim InPunches As Integer = 0
                        Dim OutPunches As Integer = 0
                        Dim CanPostonHoliday As Boolean = False
                        Dim StatusSet As Boolean = False

                        Dim dow As Integer = CType(curDate.DayOfWeek, Integer)
                        Dim drPunches() As DataRow = GetPunchesDR_ByClass(dt, StuEnrollId, curDate, ClsSectMeetingId)

                        Dim IsLOADate As Boolean
                        Dim IsSuspendedDate As Boolean
                        Dim inti As Integer
                        If curDate > Date.Now Then
                            For inti = 0 To drPunches.Length - 1
                                drPunches(inti)("Status") = PunchStatus.NotScheduled
                            Next
                            If drPunches.Length > 0 Then
                                StatusSet = True
                                AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                AddMsg("Date", curDate)
                                AddMsg("Not imported. Since,", "Future date is given")
                            End If

                        End If

                        If StatusSet = False Then
                            IsLOADate = (New AttendanceFacade).IsStudentLOADate(StuEnrollId, curDate)
                            If IsLOADate = True Then
                                For inti = 0 To drPunches.Length - 1
                                    drPunches(inti)("Status") = PunchStatus.NotScheduled
                                Next
                                If drPunches.Length > 0 Then
                                    StatusSet = True
                                    AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                    AddMsg("Date", curDate)
                                    AddMsg("Not imported. Since, ", "Student is in LOA")
                                End If
                            End If
                        End If
                        If StatusSet = False Then
                            IsSuspendedDate = (New AttendanceFacade).IsStudentSuspendedDate(StuEnrollId, curDate)
                            If IsSuspendedDate = True Then
                                For inti = 0 To drPunches.Length - 1
                                    drPunches(inti)("Status") = PunchStatus.NotScheduled
                                Next
                                If drPunches.Length > 0 Then
                                    StatusSet = True
                                    AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                    AddMsg("Date", curDate)
                                    AddMsg("Not imported. Since, Student ", " is Suspended")
                                End If
                            End If
                        End If
                        If StatusSet = False Then
                            If curDate < StuStartDate Then
                                For inti = 0 To drPunches.Length - 1
                                    drPunches(inti)("Status") = PunchStatus.NotScheduled
                                Next
                                If drPunches.Length > 0 Then
                                    StatusSet = True
                                    AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                    AddMsg("Date", curDate)
                                    AddMsg("Not imported. Since, Student", " Punch is before StartDate")
                                End If

                            End If
                        End If

                        If isMeetingDayCancelled(arrCancelDates, curDate) Then
                            CanPostonHoliday = True
                        End If

                        If drPunches.Length > 0 Then


                            If IsHoliday(curDate, drPunches(0)("CampusId").ToString, drPunches(0)("StartTime"), drPunches(0)("EndTime")) Then
                                If Not MyAdvAppSettings.AppSettings("postattendanceonholiday") Is Nothing Then
                                    If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                                        If StatusSet = False Then
                                            For inti = 0 To drPunches.Length - 1
                                                drPunches(inti)("Status") = PunchStatus.NotScheduled
                                            Next
                                            If drPunches.Length > 0 Then
                                                StatusSet = True
                                                AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                                AddMsg("Date", curDate)
                                                AddMsg("Not imported. Since, Student", " Punch is on a Holiday")
                                            End If
                                        End If
                                    ElseIf MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "yes" Then
                                        CanPostonHoliday = True
                                    End If
                                Else
                                    If StatusSet = False Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.NotScheduled
                                        Next
                                        If drPunches.Length > 0 Then
                                            StatusSet = True
                                            AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                            AddMsg("Date", curDate)
                                            AddMsg("Not imported. Since, Student", " Punch is on a Holiday")
                                        End If
                                    End If
                                End If
                            End If

                        End If
                        If StatusSet = False Then
                            If drPunches.Length > 0 Then
                                For inti = 0 To drPunches.Length - 1
                                    If CType(drPunches(inti)("PunchType"), TimeClockPunchType) = TimeClockPunchType.PunchIn Then
                                        InPunches = InPunches + 1
                                    ElseIf CType(drPunches(inti)("PunchType"), TimeClockPunchType) = TimeClockPunchType.PunchOut Then
                                        OutPunches = OutPunches + 1
                                    End If
                                Next inti
                                If StatusSet = False Then
                                    If OutPunches = 0 Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.Invalid
                                        Next
                                        StatusSet = True
                                        AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Date", curDate)
                                        AddMsg("Not imported. Since, Student ", " Punch Out is not found")
                                    End If
                                End If
                                If StatusSet = False Then
                                    If InPunches = 0 Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.Invalid
                                        Next
                                        StatusSet = True
                                        AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Date", curDate)
                                        AddMsg("Not imported. Since, Student ", " Punch In is not found")
                                    End If
                                End If
                                If StatusSet = False Then
                                    If InPunches <> OutPunches Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.Invalid
                                        Next
                                        StatusSet = True
                                        AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Date", curDate)
                                        AddMsg("Not imported. Since, Student", " Punch In and Punch Out does not match")
                                    End If
                                End If
                                If StatusSet = False Then
                                    If CType(drPunches(0)("PunchType"), TimeClockPunchType) = TimeClockPunchType.PunchOut Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.OutofOrder
                                        Next
                                        StatusSet = True
                                        AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                        AddMsg("Date", curDate)
                                        AddMsg("Not imported. Since, Student", " Punch is not in order")
                                    End If
                                End If
                                Dim IsOutofOrder As Boolean = False
                                For inti = 0 To drPunches.Length - 1
                                    If inti + 1 <= drPunches.Length - 1 Then
                                        If drPunches(inti)("Id") > drPunches(inti + 1)("Id") Then
                                            IsOutofOrder = True
                                        End If
                                    End If
                                Next inti
                                If StatusSet = False Then
                                    If IsOutofOrder = True Then
                                        For inti = 0 To drPunches.Length - 1
                                            drPunches(inti)("Status") = PunchStatus.OutofOrder
                                        Next
                                        If drPunches.Length > 0 Then
                                            StatusSet = True
                                            AddMsg("Badge Id", drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c))
                                            AddMsg("Date", curDate)
                                            AddMsg("Not imported. Since, Student", " Punch is Out of Order")
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        If drPunches.Length >= 2 Then
                            If StatusSet = False Then
                                Dim totHrs As Decimal
                                If CanPostonHoliday = True Then
                                    totHrs = GetTotalDailyHoursForTimeClock_byClass(drPunches)
                                Else
                                    totHrs = GetTotalDailyHours_byClass(drPunches, sdInfo(dow))

                                End If

                                Dim dtprocessPunches As New DataTable
                                dtprocessPunches = dt.Clone
                                Dim drdata As DataRow
                                For i As Integer = 0 To drPunches.Length - 1

                                    drdata = dtprocessPunches.NewRow()
                                    drdata("BadgeId") = drPunches(i)("BadgeId")
                                    drdata("ClockId") = drPunches(i)("ClockId")
                                    drdata("StuEnrollId") = drPunches(i)("StuEnrollId")
                                    drdata("PunchType") = drPunches(i)("PunchType")
                                    drdata("PunchTime") = drPunches(i)("PunchTime")
                                    drdata("FromSystem") = drPunches(i)("FromSystem")
                                    drdata("Status") = drPunches(i)("Status")
                                    drdata("ClsSectMeetingID") = drPunches(i)("ClsSectMeetingID")
                                    drdata("SpecialCode") = drPunches(i)("SpecialCode")

                                    dtprocessPunches.Rows.Add(drdata)

                                Next
                                Dim displayToHrs As Decimal = Math.Round(totHrs / 60, 2)
                                AddMsg("ComputeHours", String.Format("{0} [{1}] Total Hours = {2}",
                                            drPunches(0)("BadgeId").ToString.PadLeft(9, "0"c), curDate.ToShortDateString(), displayToHrs))

                                Dim res As String
                                If CanPostonHoliday = True Then
                                    res = TimeClockDB.SaveTotalDailyHours_ByClass(curDate.ToShortDateString(), StuEnrollId, ClsSectMeetingId, totHrs, sdInfo(dow).Check_TardyIn, 0, UserName, CampusID, schedInfo.UnitType, drPunches)
                                    CanPostonHoliday = False
                                Else
                                    res = TimeClockDB.SaveTotalDailyHours_ByClass(curDate.ToShortDateString(), StuEnrollId, ClsSectMeetingId, totHrs, sdInfo(dow).Check_TardyIn, sdInfo(dow).Total, UserName, CampusID, schedInfo.UnitType, drPunches)

                                End If

                                If res <> "" Then
                                    AddMsg("ComputeHours", res)
                                End If
                                Dim ProcessArr() As TimeClockPunchInfo = ToTimeClockPunchInfoArray_byClass(dtprocessPunches)
                                For Each info As TimeClockPunchInfo In ProcessArr
                                    If Not info.Status = PunchStatus.Invalid And Not info.Status = PunchStatus.NotScheduled And Not info.Status = PunchStatus.OutofOrder Then
                                        info.Status = PunchStatus.Ok
                                    End If
                                Next
                                Dim Processres As String = TimeClockDB.SaveTimeClockPunches_ByClass(ProcessArr)
                                If Processres <> "" Then
                                    AddMsg("ComputeHours:SaveTimeClockPunches", Processres)
                                End If

                            Else
                                TimeClockDB.SetPunchStatus_ByClass(drPunches)
                            End If
                        Else
                            TimeClockDB.SetPunchStatus_ByClass(drPunches)
                        End If
                        curDate = curDate.AddDays(1)
                    End While

                Next


            Next

            Dim infoArr() As TimeClockPunchInfo = ToTimeClockPunchInfoArray_byClass(dtNewPunches)
            For Each info As TimeClockPunchInfo In infoArr
                If Not info.Status = PunchStatus.Invalid And Not info.Status = PunchStatus.NotScheduled And Not info.Status = PunchStatus.OutofOrder Then
                    info.Status = PunchStatus.Ok
                End If

            Next
            Dim res2 As String = TimeClockDB.SaveTimeClockPunches_ByClass(infoArr)
            If res2 = "" Then
                AddMsg("ComputeHours:SaveTimeClockPunches", "Ok")
            Else
                AddMsg("ComputeHours:SaveTimeClockPunches", res2)
            End If
            Return res2

        End Function
        Public Function IsClsSectionUsesTimeClock(ByVal ClsSectionID As String) As Boolean
            Dim TimeDb As New TimeClockDB
            Return (TimeDb).IsClsSectionUsesTimeClock(ClsSectionID)
        End Function

        Public Function GetStartTimeForClassSectionMeeting_byClass(ByVal ClsSectMeetingID As String, ByVal CAmpusID As String) As String
            Dim TimeDb As New TimeClockDB
            Return (TimeDb).GetStartTimeForClassSectionMeeting_byClass(ClsSectMeetingID, CAmpusID)
        End Function

        Public Function GetStartTimeForClassSectionMeeting(ByVal ClsSectMeetingID As String, ByVal CAmpusID As String, selectWk As Integer) As String
            Dim TimeDb As New TimeClockDB
            Return (TimeDb).GetStartTimeForClassSectionMeeting(ClsSectMeetingID, CAmpusID, selectWk)
        End Function

        Private Async Function PostPaymentPeriodAttendanceAFA(CampusId As String, StudentEnrollments As List(Of Guid), MyAdvAppSettings As FAME.Advantage.Common.AdvAppSettings, UserName As String) As Task(Of Boolean)
            Dim tokenResponse As TokenResponse = CType(HttpContext.Current.Session("AdvantageApiToken"), TokenResponse)
            Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString

            Return Await New PaymentPeriodHelper(connectionString, tokenResponse.ApiUrl, tokenResponse.Token, UserName) _
                    .PostPaymentPeriodAttendance(CampusId, StudentEnrollments)
            Return False
        End Function

        Private Sub TitleIVSapUpdate(StudentEnrollmentsList)
            If (Not HttpContext.Current.Session("AdvantageApiToken") Is Nothing) Then
                Dim tokenResponse As TokenResponse = CType(HttpContext.Current.Session("AdvantageApiToken"), TokenResponse)

                Dim TitleIVSAPRequest As New TitleIVSAPRequest(tokenResponse.ApiUrl, tokenResponse.Token)
                Dim pass As Boolean? = TitleIVSAPRequest.TitleIVSapCheck(StudentEnrollmentsList)
            End If
        End Sub

        Public Function UpsertTimeClockImportLog(logParams As TimeClockLogParams) As ActionResult(Of String)
            If (Not HttpContext.Current.Session("AdvantageApiToken") Is Nothing) Then
                Dim tokenResponse As TokenResponse = CType(HttpContext.Current.Session("AdvantageApiToken"), TokenResponse)
                Dim logRequest = New LogTimeClockImportRequest(tokenResponse.ApiUrl, tokenResponse.Token)
                Dim logResult = logRequest.UpsertTimeClockLog(logParams)
                Return logResult
            End If
        End Function
    End Class

End Namespace

