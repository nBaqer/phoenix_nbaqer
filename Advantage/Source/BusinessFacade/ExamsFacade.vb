Imports Telerik.Web.UI
Imports Advantage.Business.Objects



Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports BO = Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports System.Text
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects
Imports FAME.Advantage.DataAccess.LINQ.Common
Imports FAME.Advantage.DataAccess.LINQ.PostExams
Imports FAME.Advantage.Common.FAME.Advantage.Common
Imports Telerik.Windows.Documents.Flow.Model.Lists

Public Class ExamsFacade

#Region "Post Services"

    Public Function GetClassroomWorkTerm(ByVal stuEnrollId As String) As DataTable
        Dim ds As New DataSet, dt As New DataTable
        ds = (New ExamsDB).GetClassroomWorkTerm(stuEnrollId)
        dt = ds.Tables(0)
        Return dt
    End Function

    Public Function GetClassroomWorkClass(ByVal stuEnrollId As String, ByVal termId As String) As DataTable
        Dim ds As New DataSet, dt As New DataTable
        ds = (New ExamsDB).GetClassroomWorkClass(stuEnrollId, termId)
        dt = ds.Tables(0)
        Return dt
    End Function

    Public Function GetClassroomWorkServices(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal gbCompType As Integer) As DataTable
        Dim ds As New DataSet, dt As New DataTable
        ds = (New ExamsDB).GetClassroomWorkServices(stuEnrollId, clsSectionId, gbCompType)
        dt = ds.Tables(0)
        Return dt
    End Function
    Public Function DeleteClassroomWorkServices(ByVal GrdBkResultId As String) As DataTable
        Dim ds As New DataSet, dt As New DataTable
        ds = (New ExamsDB).DeleteClassroomWorkServices(GrdBkResultId)
        Return dt
    End Function

    Public Function GetClassroomWorkServicePosts(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal gbWeightDetail As String) As DataTable
        Dim ds As New DataSet, dt As New DataTable
        ds = (New ExamsDB).GetClassroomWorkServicePosts(stuEnrollId, clsSectionId, gbWeightDetail)
        dt = ds.Tables(0)
        Return dt
    End Function

    Public Function PostServices(ByVal posts As List(Of StdGrdBkInfo), ByVal user As String) As String
        Dim sb As New StringBuilder
        For Each post In posts
            sb.Append(PostService(post, user))
        Next
        Return sb.ToString()
    End Function

    Public Function PostServiceBatch(ByVal stdGrdObj As StdGrdBkInfo, ByVal user As String, ByVal adjustment As Integer) As String
        Dim result As String
        stdGrdObj.GrdBkResultId = System.Guid.NewGuid.ToString
        With New ExamsDB
            result = .InsertPostedServiceBatch(stdGrdObj, user, adjustment)
            result += .SetIsCourseCompletedForServices(stdGrdObj.StuEnrollId, stdGrdObj.ClassId)
        End With
        Return result
    End Function

    Public Function PostService(ByVal stdGrdObj As StdGrdBkInfo, ByVal user As String) As String
        Dim result As String
        stdGrdObj.GrdBkResultId = System.Guid.NewGuid.ToString
        With New ExamsDB
            result = .InsertPostedService(stdGrdObj, user)
            result += .SetIsCourseCompletedForServices(stdGrdObj.StuEnrollId, stdGrdObj.ClassId)
        End With
        Return result
    End Function

    Public Function GetPostedServicesByDay(ByVal stdGrdObj As StdGrdBkInfo, ByVal day As Date) As DataSet
        Return (New ExamsDB).GetPostedServicesByDay(stdGrdObj.StuEnrollId, stdGrdObj.ClassId, stdGrdObj.GrdBkWgtDetailId, day)
    End Function

    Private Function GetNextServiceResnum(ByVal stdGrdObj As StdGrdBkInfo) As Integer
        Dim resNum As Integer = 0
        Dim ds As New DataSet, dt As New DataTable
        ds = GetPostedServicesByDay(stdGrdObj, Date.Now)
        dt = ds.Tables(0)
        If dt.Rows.Count > 0 Then
            resNum = dt.Compute("Max(ResNum)", "") + 1
        End If
        Return resNum
    End Function

#End Region

    Public Function GetExamsResultsByStudent(ByVal stuEnrollId As String, ByVal sysComponentTypeId As Integer) As DataTable
        Return (New ExamsDB).GetExamsResultsByStudent(stuEnrollId, sysComponentTypeId)
    End Function

    Public Function GetGradeBookResultsByStudent_SP(ByVal stuEnrollId As String) As String
        Return (New ExamsDB).GetGradeBookResultsByStudent_SP(stuEnrollId)
    End Function


    Public Function PostExamResultsObj(ByVal postExamListFinal As List(Of FAME.Advantage.Common.FAME.Advantage.Common.postExamResultsObject), ByVal stuEnrollId As String, ByVal user As String) As String
        Return (New ExamsDB).PostExamResultsObj(postExamListFinal, stuEnrollId, user)
    End Function

    Public Function GetDataToPostExamsResultsByStudent(ByVal stuEnrollId As String, ByVal type As Integer) As DataTable
        Return (New ExamsDB).GetDataToPostExamsResultsByStudent(stuEnrollId, type)
    End Function

    Public Function GetClinicServicesAndHoursByStudent(ByVal stuEnrollId As String, ByVal type As Integer, Optional ByVal CourseId As String = "", Optional ByVal Courses As String = "All") As DataTable
        Return (New ExamsDB).GetClinicServicesAndHoursByStudent(stuEnrollId, type, CourseId, Courses)
    End Function

    Public Function IsRemainingLabCount(ByVal stuEnrollId As String, ByVal type As Integer, Optional ByVal CourseId As String = "") As Boolean
        Return (New ExamsDB).IsRemainingLabCount(stuEnrollId, type, CourseId)
    End Function

    Public Function IsRemainingLabCount_SP(ByVal stuEnrollId As String, ByVal type As Integer, ByVal CourseId As String) As Boolean
        Return (New ExamsDB).IsRemainingLabCount_SP(stuEnrollId, type, CourseId)
    End Function

    Public Function GetReqId(ByVal TermId As String, ByVal ClsSectionId As String) As String
        Return (New ExamsDB).GetReqId(TermId, ClsSectionId)
    End Function

    Public Function isClinicCourseCompletlySatisfied(ByVal stuEnrollId As String, ByVal type As Integer, Optional ByVal CourseId As String = "") As Boolean
        Return (New ExamsDB).isClinicCourseCompletlySatisfied(stuEnrollId, type, CourseId)
    End Function

    Public Function isClinicCourseCompletlySatisfied_SP(ByVal stuEnrollId As String, ByVal type As Integer, ByVal CourseId As String) As Boolean
        Return (New ExamsDB).isClinicCourseCompletlySatisfied_SP(stuEnrollId, type, CourseId)
    End Function

    Public Function isCourseALabWorkOrLabHourCourseCombination(ByVal ReqId As String) As Boolean
        Return (New TransferGradeDB).isCourseALabWorkOrLabHourCourseCombination(ReqId)
    End Function

    Public Function isClinicCourseCompletlySatisfiedCombination(ByVal stuEnrollId As String, ByVal type As Integer, Optional ByVal CourseId As String = "") As Boolean
        Return (New ExamsDB).isClinicCourseCompletlySatisfiedForCombination(stuEnrollId, type, CourseId)
    End Function

    Public Function isClinicCourseCompletlySatisfiedByTermId(ByVal stuEnrollId As String, ByVal type As Integer, Optional ByVal CourseId As String = "", Optional ByVal Courses As String = "All", Optional ByVal TermId As String = "") As Boolean
        Return (New ExamsDB).isClinicCourseCompletlySatisfiedByTerm(stuEnrollId, type, CourseId, Courses, TermId)
    End Function

    Public Function UpdateScoreForComponent(ByVal stuEnrollId As String, Optional ByVal CourseId As String = "", Optional ByVal User As String = "sa") As String
        Return (New PostAverageForCourse).UpdateScoreForComponent(stuEnrollId, CourseId, User)
    End Function

    Public Function GetOverallAverageByStudent(ByVal stuEnrollId As String, ByVal sysComponentTypeId1 As Integer, ByVal sysComponentTypeId2 As Integer) As Decimal
        '   Instantiate DAL componen
        Dim writtenExamsAverage As Decimal
        Dim practicalExamsAverage As Decimal
        Dim writtenExamsSumOfWeights As Decimal
        Dim practicalExamsSumOfWeights As Decimal
        Dim dtWrittenExams As DataTable = (New ExamsDB).GetExamsResultsByStudent(stuEnrollId, sysComponentTypeId1)
        Dim dtPracticalExams As DataTable = (New ExamsDB).GetExamsResultsByStudent(stuEnrollId, sysComponentTypeId2)
        Dim writtenCount As Integer
        Dim practicalCount As Integer
        Dim Result As Decimal
        If dtWrittenExams.Rows.Count > 0 Then
            'writtenCount = dtWrittenExams.Rows.Count
            For writtenCount = 0 To dtWrittenExams.Rows.Count - 1
                If Not (IsDBNull(dtWrittenExams.Rows(writtenCount)("Score")) Or IsDBNull(dtWrittenExams.Rows(writtenCount)("Weight"))) Then
                    writtenExamsAverage += CType(dtWrittenExams.Rows(writtenCount)("Score"), Decimal) * CType(dtWrittenExams.Rows(writtenCount)("Weight"), Decimal)
                    writtenExamsSumOfWeights += CType(dtWrittenExams.Rows(writtenCount)("Weight"), Decimal)
                End If

            Next
        End If
        If dtPracticalExams.Rows.Count > 0 Then
            'practicalCount = dtPracticalExams.Rows.Count
            For practicalCount = 0 To dtPracticalExams.Rows.Count - 1
                If Not (IsDBNull(dtPracticalExams.Rows(practicalCount)("Score")) Or IsDBNull(dtPracticalExams.Rows(practicalCount)("Weight"))) Then
                    practicalExamsAverage += CType(dtPracticalExams.Rows(practicalCount)("Score"), Decimal) * CType(dtPracticalExams.Rows(practicalCount)("Weight"), Decimal)
                    practicalExamsSumOfWeights += CType(dtPracticalExams.Rows(practicalCount)("Weight"), Decimal)
                End If

            Next
        End If
        If (writtenExamsSumOfWeights + practicalExamsSumOfWeights) > 0 Then
            Result = CType((writtenExamsAverage + practicalExamsAverage) / (writtenExamsSumOfWeights + practicalExamsSumOfWeights), Decimal)
            Return Result
        End If
    End Function

    Public Function GetOverallAverageForEnrollment(ByVal StuEnrollId As String) As String
        Dim dtTransferGrades As New DataTable
        Dim dtARResults As New DataTable
        Dim db1 As New ExamsDB
        Dim db2 As New ExamsDB
        Dim SumOfScoresFromTransferGrades As Decimal
        Dim SumOfScoresFromARResults As Decimal
        Dim CountOfCoursesFromTransferGrades As Int16
        Dim CountOfCoursesFromARResults As Int16

        dtTransferGrades = db1.GetOverallAverageFromTransferGrades(StuEnrollId)
        dtARResults = db2.GetOverallAverageFromARResults(StuEnrollId)

        If Not dtTransferGrades.Rows(0)("SumOfScores") Is System.DBNull.Value Then
            SumOfScoresFromTransferGrades = dtTransferGrades.Rows(0)("SumOfScores")
            CountOfCoursesFromTransferGrades = dtTransferGrades.Rows(0)("NumCourses")
        Else
            SumOfScoresFromTransferGrades = 0.0
            CountOfCoursesFromTransferGrades = 0
        End If


        If Not dtARResults.Rows(0)("SumOfScores") Is System.DBNull.Value Then
            SumOfScoresFromARResults = dtARResults.Rows(0)("SumOfScores")
            CountOfCoursesFromARResults = dtARResults.Rows(0)("NumCourses")
        Else
            SumOfScoresFromARResults = 0.0
            CountOfCoursesFromARResults = 0
        End If

        If (CountOfCoursesFromTransferGrades = 0 And CountOfCoursesFromARResults = 0) Then
            Return ""
        Else
            Return Math.Round((SumOfScoresFromTransferGrades + SumOfScoresFromARResults) / (CountOfCoursesFromTransferGrades + CountOfCoursesFromARResults), 2)
        End If

    End Function

End Class
