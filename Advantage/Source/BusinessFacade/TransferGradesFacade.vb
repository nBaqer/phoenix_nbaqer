Imports FAME.AdvantageV1.Common.Tables

Public Class TransferGradesFacade
    Public Function PopulateDataGrid(ByVal student As String, ByVal campusId As String) As DataSet

        Dim db As New TransferGradesDB
        Dim ds As DataSet
        ds = db.GetDataGridInfo(student, campusId)
        Return ds

    End Function

    Public Function GetGrades(ByVal stuEnrollId As String) As DataSet

        '   Instantiate DAL component
        With New TransferGradesDB

            '   get the dataset with all TuitionCategories
            Return .GetGradeSystemDetails(stuEnrollId)
        End With
    End Function

    Public Function GetTerms() As DataSet

        '   Instantiate DAL component
        Dim db As New DropStudentDB

        '   get the dataset with all degrees
        Return db.GetCurrentTerms()

    End Function

    Public Function UpdateTransferGrade(ByVal finalGrdObj As FinalGradeInfo, ByVal user As String)
        Dim db As New TransferGradesDB

        db.UpdateTransferGrade(finalGrdObj, user)
    End Function

    Public Function GetStudentEnrollments(ByVal student As String, ByVal campusId As String) As DataSet

        Dim db As New TransferGradesDB
        Dim ds As DataSet

        ds = db.GetStudentEnrollments(student, campusId)

        Return ds
    End Function

    Public Function TransferGradeFromOneEnrollmentToAnother(ByVal finalGrdObj As FinalGradeInfo, ByVal user As String, ByVal newEnrollId As String)
        Dim db As New TransferGradesDB

        db.TransferGradeFromOneEnrollmentToAnother(finalGrdObj, user, newEnrollId)
    End Function

    Public Shared Function GetListOfTransferredEnrollments(ByVal listenrollmentsId As List(Of String), ByVal reqId As String) As List(Of ArTransferGrades)
        Dim db As New TransferGradesDB
        Dim list As List(Of ArTransferGrades) = New List(Of ArTransferGrades)()
        For Each s As String In listenrollmentsId
            Dim record = db.GetRequirementTransferredRecord(s, reqId)
            If (record.EnrollmentId <> Guid.Empty.ToString()) Then
                list.Add(record)
            End If
        Next
        Return list
    End Function

    Public Shared Sub SetTransferTable(ByVal listToTransfer As IList(Of ArTransferGrades))
        Dim db As New TransferGradesDB
        db.SetTransferTable(listToTransfer)
    End Sub


    ''' <summary>
    ''' Test if the enrollment was transferred for the requirement.
    ''' </summary>
    ''' <param name="stuEnrollId">Enroll to be tested</param>
    ''' <param name="reqId">given requirement </param>
    ''' <returns>true if was transferred false if not</returns>
    Public Shared Function IsEnrollmentTransferredForTheReq(stuEnrollId As String, reqId As String) As Boolean
        Dim db As New TransferGradesDB
        Dim isTransferred As Boolean = db.IsEnrollmentTransferredForTheReq(stuEnrollId, reqId)
        Return isTransferred
    End Function

End Class
