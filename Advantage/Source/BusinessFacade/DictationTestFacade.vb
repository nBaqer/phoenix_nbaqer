﻿Public Class DictationTestFacade
    Inherits BaseReportFacade
    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim GradebookResultsObj As New DictationTestDB
        Dim ds As New DataSet
        ds = BuildDataForReport(rptParamInfo)
        Return ds
    End Function
    Public Function BuildDataForReport(ByVal ParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim dtReturnResult As New DataTable
        Dim ds As New DataSet

        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim zipMask As String
        Dim ssnMask As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        ds = (New DictationTestDB).GetDictationSpeedTestDetailsForReport(ParamInfo, _
                                                                         ssnMask, _
                                                                         strMask, _
                                                                         zipMask)

        ''Apply Mask
        'For Each dr As DataRow In ds.Tables(0).Rows

        'Next
        Return ds
    End Function
    Public Function IsStudentRegisteredInDictationTestCourse(ByVal StudentId As String) As Boolean
        Return (New DictationTestDB).IsStudentRegisteredInDictationTestCourse(StudentId)
    End Function
    Public Function IsStudentRegisteredInDictationTestCourseByEnrollment(ByVal StudentId As String, _
                                                                         ByVal StuEnrollId As String _
                                                                         ) As Boolean
        Return (New DictationTestDB).IsStudentRegisteredInDictationTestCourseByEnrollment(StudentId, StuEnrollId)
    End Function
End Class
