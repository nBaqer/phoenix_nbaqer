Imports System.Collections.Generic
Imports System.Web.UI

Public Class StuEnrollFacade
    Public Function GetAllPrgVersions() As DataSet

        '   Instantiate DAL component
        Dim DB As New StuEnrollDB

        '   get the dataset with all Prefixes
        Return DB.GetAllPrgVersions()

    End Function
    Public Function GetScheduleByProgramVersion(ByVal PrgVerId As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New StuEnrollDB

        '   get the dataset with all Prefixes
        Return DB.GetScheduleByProgramVersion(PrgVerId)

    End Function

    Public Function SaveScheduleForEnrollment(ByVal StuEnrollId As string, ByVal  ScheduleId As String, ByVal User As String,
                                              ByVal PrgVerId As String) As String
        Dim DB As New StuEnrollDB

        '   get the dataset with all Prefixes
        Return DB.SaveScheduleForEnrollment(StuEnrollId, ScheduleId, User, PrgVerId)
    End Function
    Public Function GetAllActiveInActivePrgVersions(ByVal CampusId As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New StuEnrollDB

        '   get the dataset with all Prefixes
        Return DB.GetAllActiveInActivePrgVersions(CampusId)

    End Function

    Public Function GetAllPrgVersionsForCampus(ByVal NewCampusId As String) As DataSet
        Dim db As New StuEnrollDB

        Return db.GetAllPrgVersionsForCampus(NewCampusId)
    End Function
    Public Function GetCampuses(ByVal PrgVerId As String, ByVal CurrentCampus As String) As DataSet
        Dim db As New StuEnrollDB

        Return db.GetCampuses(PrgVerId, CurrentCampus)
    End Function
    Public Function GetAllProgramTypes() As DataSet

        '   get the dataset with all ProgramTypes
        Return (New PrgTypesDB).GetAllProgramTypes()

    End Function
    Public Function GetAllProgVersions(ByVal ProgId As String) As DataSet
        '   Instantiate DAL component
        Dim DB As New StuEnrollDB

        '   get the dataset with all Prefixes
        Return DB.GetAllProgVersions()

    End Function
    Public Function GetAllPrograms() As DataSet

        '   get the dataset with all Programs
        Return (New StuEnrollDB).GetAllPrograms()

    End Function
    Public Function GetAllProgVersionsByProgramId(ByVal ProgId As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New StuEnrollDB

        '   get the dataset with all Prefixes
        Return DB.GetAllProgVersionsByProgramId(ProgId)

    End Function
    Public Function GetAcadAdvisors(ByVal CampId As String, Optional ByVal advisorID As String = "") As DataSet

        '   Instantiate DAL component
        Dim DB As New StuEnrollDB
        Dim ds As New DataSet

        '   get the dataset with all Prefixes
        ds = (DB.GetAcademicAdvisors(CampId, advisorID))
        Return ConcatenateStudentLastFirstName(ds)
    End Function
    Public Function GetFinancialAdvisorsPerCampus(ByVal campusId As String, Optional ByVal advisorID As String = "") As DataSet
        Dim objDB As New StuEnrollDB

        '   return Dataset
        Return objDB.GetFinancialAidAdvisorsPerCampus(campusId, advisorID)

    End Function
    Public Function GetAdminReps(ByVal CampId As String, Optional ByVal repID As String = "") As DataSet

        '   Instantiate DAL component
        Dim DB As New StuEnrollDB
        Dim ds As New DataSet

        '   get the dataset with all Prefixes
        ds = (DB.GetAdmissionReps(CampId, repID))

        If ds.Tables(0).Rows.Count >= 1 Then
            Return ConcatenateStudentLastFirstName(ds)
        Else
            Return ds
        End If
    End Function
    Public Function GetEnrollStatuses() As DataSet

        '   Instantiate DAL component
        Dim DB As New StuEnrollDB
        Dim ds As New DataSet

        '   get the dataset with all Prefixes
        ds = (DB.GetEnrollStatuses())
        Return ds
    End Function
    Public Function ConcatenateStudentLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        ''   Dim row As DataRow

        tbl = ds.Tables(0)
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("a.UserId")}
        End With
        'If ds.Tables(0).Rows.Count > 0 Then
        '    'Add a new column called fullname to the datatable 
        '    Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
        '    For Each row In tbl.Rows
        '        row("FullName") = row("LastName") & ", " & row("FirstName")
        '    Next
        'End If
        Return ds
    End Function
    Public Function GetPrgVerBillingMethod(ByVal PrgVerId As String) As String
        Dim db As New StuEnrollDB
        Dim BMethod As String

        BMethod = db.GetPrgVersionBillingMethod(PrgVerId)

        Return (BMethod)

    End Function
    Public Function GetPrgVerBillingMethod(ByVal PrgVerId As String, ByVal campusId As String) As String
        Dim db As New StuEnrollDB
        Dim BMethod As String

        BMethod = db.GetPrgVersionBillingMethod(PrgVerId, campusId)

        Return (BMethod)

    End Function

    Public Function PopulateDataList(ByVal studentId As String, ByVal showByStatus As AdvantageCommonValues.IncludeStatus, Optional ByVal campusId As String = Nothing) As DataSet
        Dim db As New StuEnrollDB
        Dim ds As DataSet
        ds = db.GetStudentEnrollments(studentId, showByStatus, campusId)
        Return ds
    End Function

    Public Sub InsertFields(ByVal stuEnrollmentID As String, ByVal stuEnrollObj As StudentEnrollmentInfo, ByVal user As String)
        Dim studentEnrollment As New StuEnrollDB
        studentEnrollment.InsertStudentEnrollment(stuEnrollmentID, stuEnrollObj, user)
    End Sub
    Public Function DeleteStuEnrollment(ByVal StuEnrollId As String, ByVal modDate As DateTime) As String
        Dim db As New StuEnrollDB
        Return db.DeleteStudentEnrollment(StuEnrollId, modDate)
    End Function
    Public Sub UpdateFields(ByVal StuEnrollmentID As String, ByVal StuEnrollObj As StudentEnrollmentInfo, ByVal user As String)
        Dim StudentEnrollment As New StuEnrollDB
        StudentEnrollment.UpdateStudentEnrollment(StuEnrollmentID, StuEnrollObj, user)

    End Sub

    Public sub updateLeadBadgeNumber(ByVal stuEnrollId As String,ByVal badgeNum As String) 
        Dim StudentEnrollment As New StuEnrollDB
        StudentEnrollment.updateLeadBadgeNumber(stuEnrollId, badgeNum)
    End sub

    Public Function GetStudentDetails(ByVal stuEnrollID As String) As StudentEnrollmentInfo
        With New StuEnrollDB
            Return .GetStudentByEnrollID(stuEnrollID)
        End With
    End Function
    Public Function GetStudentDetails_UI(ByVal StuEnrollID As String) As StudentEnrollmentInfo
        With New StuEnrollDB
            Return .GetStudentByEnrollID_UI(StuEnrollID)
        End With
    End Function
    Public Function OKToAddEnrollmentWithSameProgramVersion(ByVal stuEnrollId As String, ByVal PrgVerId As String) As Boolean
        With New StuEnrollDB
            Return .OKToAddEnrollmentWithSameProgramVersion(stuEnrollId, PrgVerId)
        End With
    End Function
    Public Function StudentName(ByVal StudentID As String) As String
        Dim ds As New DataSet
        With New StuEnrollDB
            'get the dataset with all SkillGroups
            Return .GetStudentName(StudentID)
        End With
    End Function
    Public Function GetLeadName(ByVal LeadID As String) As String
        Dim ds As New DataSet
        With New StuEnrollDB
            'get the dataset with all SkillGroups
            Return .GetLeadName(LeadID)
        End With
    End Function
    Public Function GetProbationCount(ByVal StuEnrollId As String) As Integer
        ''  Dim count As Integer

        With New StuEnrollDB
            Return .GetStdProbations(StuEnrollId)
        End With
    End Function

    Public Function GetCurrentProbations(ByVal StuEnrollId As String) As List(Of String)
        With New StuEnrollDB
            Return .GetCurrentProbations(StuEnrollId)
        End With

    End Function
    Public Function GetLOACount(ByVal StuEnrollId As String) As Integer
        ''  Dim count As Integer

        With New StuEnrollDB
            Return .GetStdLOAs(StuEnrollId)
        End With
    End Function
    Public Function GetWarningCount(ByVal StuEnrollId As String) As Integer
        ''  Dim count As Integer

        With New StuEnrollDB
            Return .GetStdWarnings(StuEnrollId)
        End With
    End Function
    Public Function GetSuspensionCount(ByVal StuEnrollId As String) As Integer
        ''  Dim count As Integer

        With New StuEnrollDB
            Return .GetStdSuspensionCount(StuEnrollId)
        End With
    End Function
    Public Function GetStudentsInPrgVersion(ByVal prgVerId As String, ByVal campusId As String) As DataSet
        Dim obj As New StuEnrollDB
        Dim ds As New DataSet

        ds = obj.GetStudentsInPrgVersion(prgVerId, campusId)

        'Massage datatable
        Dim dt As DataTable = ds.Tables(0)
        dt.TableName = "StudentEnrollments"
        Dim dc As DataColumn = dt.Columns.Add("StudentName", Type.GetType("System.String"))
        dc.AllowDBNull = True
        dc.Expression = "LastName + ', ' + FirstName"

        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String

        'Get mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr As DataRow In dt.Rows
            'Apply mask on SSN
            If Not dr.IsNull("SSN") Then
                If dr("SSN") <> "" Then
                    'dr("SSN") = facInputMasks.ApplyMask(strSSNMask, dr("SSN"))
                    Dim temp As String = dr("SSN")
                    dr("SSN") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                End If
            End If
        Next

        Return ds
    End Function
    Public Function GetStudentsInSameProgram(ByVal prgVerId As String, ByVal campusId As String) As DataSet
        Dim obj As New StuEnrollDB
        Dim ds As New DataSet

        ds = obj.GetStudentsInSameProgram(prgVerId, campusId)

        'Massage datatable
        Dim dt As DataTable = ds.Tables(0)
        dt.TableName = "StudentEnrollments"
        Dim dc As DataColumn = dt.Columns.Add("StudentName", Type.GetType("System.String"))
        dc.AllowDBNull = True
        dc.Expression = "LastName + ', ' + FirstName"

        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String

        'Get mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr As DataRow In dt.Rows
            'Apply mask on SSN
            If Not dr.IsNull("SSN") Then
                If dr("SSN") <> "" Then
                    'dr("SSN") = facInputMasks.ApplyMask(strSSNMask, dr("SSN"))
                    Dim temp As String = dr("SSN")
                    dr("SSN") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                End If
            End If
        Next

        Return ds
    End Function
    Public Function UpdatePrgVersionForStuEnrollments(ByVal arrStuEnrollId As ArrayList, ByVal PrgVerId As String, ByVal user As String) As String
        Return (New StuEnrollDB).UpdatePrgVersionForStuEnrollments(arrStuEnrollId, PrgVerId, user)
    End Function
    Public Function UpdateCampusAndPrgVersionForStuEnrollments(ByVal arrStuEnrollId As ArrayList, ByVal NewCampusId As String, ByVal PrgVerId As String, ByVal user As String) As String
        Return (New StuEnrollDB).UpdateCampusAndPrgVersionForStuEnrollments(arrStuEnrollId, NewCampusId, PrgVerId, user)
    End Function
    Public Function GetStudentsForIPEDSHelper(ByVal genderId As String, ByVal programId As String, _
                                                ByVal EthCodeId As String, ByVal maritalStatusId As String, _
                                                ByVal statusCodeId As String, ByVal minAge As String, _
                                                ByVal maxAge As String, ByVal minStartDate As String, _
                                                ByVal maxStartDate As String, ByVal zipCode As String) As DataSet
        Dim obj As New StuEnrollDB
        Dim ds As New DataSet

        ds = obj.GetStudentsForIPEDSHelper(genderId, programId, EthCodeId, maritalStatusId, statusCodeId, minAge, maxAge, minStartDate, maxStartDate, zipCode)

        'Massage datatable
        Dim dt As DataTable = ds.Tables(0)
        dt.TableName = "StudentEnrollments"
        Dim dc As DataColumn = dt.Columns.Add("StudentName", Type.GetType("System.String"))
        dc.AllowDBNull = True
        dc.Expression = "LastName + ', ' + FirstName"

        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String

        'Get mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr As DataRow In dt.Rows
            'Apply mask on SSN
            If Not dr.IsNull("SSN") Then
                If dr("SSN") <> "" Then
                    'dr("SSN") = facInputMasks.ApplyMask(strSSNMask, dr("SSN"))
                    Dim temp As String = dr("SSN")
                    dr("SSN") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                End If
            End If
        Next

        Return ds
    End Function
    Public Function GetAllAdmissionRepsByCampusAndUserId(ByVal strCampusId As String, ByVal strUserId As String, Optional ByVal StuEnrollId As String = "") As DataSet
        Return (New StuEnrollDB).GetAllAdmissionRepsByCampusAndUserId(strCampusId, strUserId, StuEnrollId)
    End Function
    Public Function GetAllAdmissionRepsByCampusAndUserIdAndUserRoles(ByVal strCampusId As String, ByVal strUserId As String, ByVal RoleId As Integer, Optional ByVal StuEnrollId As String = "") As DataSet
        'connect to the database
        Return (New StuEnrollDB).GetAllAdmissionRepsByCampusAndUserIdAndUserRoles(strCampusId, strUserId, RoleId, StuEnrollId)
    End Function



    ''' <summary>
    ''' To find the number of Reschedule Reasons found for a student
    ''' </summary>
    ''' <param name="stuEnrollId"></param>
    ''' <param name="CampusId"></param>
    ''' <returns></returns>
    ''' <remarks>Functionality not use at 3.7</remarks>
    Public Function GetRescheduleReasonCount(ByVal stuEnrollId As String) As Integer
        'Public Function GetRescheduleReasonCount(ByVal stuEnrollId As String, ByVal CampusId As String) As Integer
        ''   Dim count As Integer

        With New StuEnrollDB
            Return .GetRescheduleReasonCount(stuEnrollId)

            'Return .GetRescheduleReasonCount(StuEnrollId, CampusId)
        End With
    End Function

    ''Added by Saraswathi Lakshmanan on Oct-1-2008 
    ''to Update the cohort start date alone
    Public Sub UpdateCohortStartDateinStudentEnrollment(ByVal StuEnrollmentID As String, ByVal CohortStartDate As Date?, ByVal user As String)
        Dim StudentEnrollment As New StuEnrollDB
        StudentEnrollment.UpdateCohortStartDateinStudentEnrollment(StuEnrollmentID, CohortStartDate, user)
    End Sub

    ''Added by Saraswathi Lakshmanan on Oct-3-2008
    ''To find the ShiftId for the new program version
    Public Function GetShiftIdforProgramVersion(ByVal PrgVerId As String) As String
        Dim db As New StuEnrollDB
        Dim ShiftID As String

        ShiftID = db.GetShiftIdforProgramVersion(PrgVerId)

        Return (ShiftID)

    End Function
    ''Function Added by Vijay Ramteke on May 18, 2010
    ''To get the Count of Status Change History for the student
    Public Function GetStatusChangeHistoryCount(ByVal StuEnrollId As String) As Integer
        ''   Dim count As Integer

        With New StuEnrollDB
            Return .GetStatusChangeHistoryCount(StuEnrollId)
        End With
    End Function

    Public Function IsStudentProgramClockHourType(ByVal StuEnrollId As String) As Boolean
        Dim db As New StuEnrollDB
        Return db.IsStudentProgramClockHourType(StuEnrollId)
    End Function

    Public Function IsProgramVersionTimeClock(ByVal prgVerId As String) As Boolean
        Dim db As New PrgVerDefDB
        Return db.IsProgramVersionTimeClock(prgVerId)
    End Function
    Public Function IsProgramVersionClockHour(ByVal prgVerId As String) As Boolean
        Dim db As New PrgVerDefDB
        Return db.IsProgramVersionClockHour(prgVerId)
    End Function
    Public Function IsStudentLOA(ByVal StuEnrollmentId As String) As Boolean
        Dim db As New StuEnrollDB
        Return db.IsStudentLOA(StuEnrollmentId)
    End Function

    Public Sub UpdateLDAForManuallySetLDAAsNO(StuEnrollid As String)
        Dim db As New StuEnrollDB
        db.UpdateLDAForManuallySetLDAAsNO(StuEnrollid)
    End Sub

    Public Function GetBillingMethodForChargingMethods(ByVal campusId As String) As DataSet

        '   get the dataset with all active billing methods
        Return (New StuEnrollDB).GetBillingMethodForChargingMethods(campusId)

    End Function
    ' ''' <summary>
    ' ''' Return information from AdLead Table
    ' ''' </summary>
    ' ''' <param name="stuEnrollID"></param>
    ' ''' <returns></returns>
    'Public Function GetCommonInformationInAdLeadTable(ByVal stuEnrollID As String) As LeadMasterInfo
    '    Dim db As New StuEnrollDB
    '    Dim db1 As New LeadDB

    '    'Get the lead associated with the Enrollment 
    '    Dim leadid As String = db.GetLeadId(stuEnrollID)

    '    'Get the Lead Info from AdLeads
    '    Dim output As LeadMasterInfo = db1.GetLeadEnrollmentInfo(leadid)
    '    Return output

    'End Function

    ' ''' <summary>
    ' ''' Get Enrollment info. this is information in the enrollment page
    ' ''' </summary>
    ' ''' <param name="stuEnrollID"></param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Public Function GetEnrollmentInfo(ByVal stuEnrollID As String) As EnrollmentInfo
    '    Dim db As New StuEnrollDB
    '    Dim result As EnrollmentInfo = db.GetEnrollmentInfo(stuEnrollID)
    '    Return result
    'End Function
End Class
