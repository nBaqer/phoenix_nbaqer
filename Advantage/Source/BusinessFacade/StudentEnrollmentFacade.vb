Public Class StudentEnrollmentFacade

    Public Sub UpdateStudentID(ByVal studentID As String, ByVal stuEnrollmentID As String)
        Dim studentEnrollment As New StudentEnrollmentDB
        studentEnrollment.UpdateStudent(studentID, stuEnrollmentID)
    End Sub

    Public Function IsThisEnrollmentBeingUsed(ByVal stuEnrollmentID As String) As Boolean
        'return boolean value
        Return (New StudentEnrollmentDB).IsThisEnrollmentBeingUsed(stuEnrollmentID)
    End Function

    Public Function GetStudentRescheduleReason(ByVal stuEnrollId As String, ByVal campusId As String) As DataSet
        '   Instantiate DAL component
        Dim db As New StuEnrollDB

        '   get the dataset with all drop statuses          
        Dim ds As DataSet = db.GetRescheduleReason(stuEnrollId, campusId)
        Return ds
    End Function

    'Added By Vijay Ramteke on Feb 18, 2010
    Public Sub UpdateExpectedGraduationDate(ByVal expectedGraduationDate As DateTime, ByVal stuEnrollmentID As String)
        Dim studentEnrollment As New StudentEnrollmentDB
        studentEnrollment.UpdateExpectedGraduationDate(expectedGraduationDate, stuEnrollmentID)
    End Sub

    Public Function GetSchedLeadGrpCnt_SP(ByVal leadGrpId As String) As Integer
        Return (New StuEnrollDB).GetSchedLeadGrpCnt_SP(leadGrpId)
    End Function

    Public Shared Function GetFasapStudentInformation(ByVal enrollmentid As String) As List(Of StudentEnrollmentFasapDto)
        
        Dim sap As SAPCheckDB = New SAPCheckDB()
        Dim list As List (Of StudentEnrollmentFasapDto) = sap.GetFasapStudentInformation(enrollmentId)
        return list
    End Function
End Class
