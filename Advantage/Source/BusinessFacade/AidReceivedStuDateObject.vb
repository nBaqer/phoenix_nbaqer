Public Class AidReceivedStuDateObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim aidRec As New AidReceivedDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(aidRec.GetAidReceivedByStudentDate(rptParamInfo), aidRec.StudentIdentifier)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim strName As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String
        Dim oldCmpGrp As String = ""
        Dim oldCampus As String = ""
        Dim oldAidPrg As String = ""
        Dim oldMonth As String = ""
        Dim oldYr As String = ""
        Dim aidRec As New AidReceivedDB
        Dim dt2 As DataTable
        Dim dt As DataTable
        Dim row As DataRow
        Dim dr2 As DataRow = Nothing

        'Get mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count = 3 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    dt = ds.Tables("TotalAmounts")
                    dt2 = ds.Tables("MonthTotal")
                    '
                    For Each dr As DataRow In ds.Tables(0).Rows
                        'Compute Total Amounts per Campus Groups and Campuses.
                        'CampGrpDescrip and CampDescrip columns cannot be NULL.
                        If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpDescrip").ToString().Replace("'","") Then
                            'Get a row everytime there is a Campus Group.
                            dr2 = dt.NewRow
                            dr2("CampGrpDescrip") = dr("CampGrpDescrip").ToString().Replace("'","")
                            dr2("CampGrpTotal") = ds.Tables(0).Compute("SUM(TransAmount)", "CampGrpDescrip='" & dr("CampGrpDescrip").ToString().Replace("'","") & "'")
                            oldCmpGrp = dr("CampGrpDescrip").ToString().Replace("'","")
                            oldCampus = ""
                            oldAidPrg = ""
                            oldMonth = ""
                            oldYr = ""
                        End If
                        If oldCmpGrp = dr("CampGrpDescrip").ToString().Replace("'","") And oldCampus = "" Then
                            'Add the row created on the above code into dt table.
                            dr2("CampDescrip") = dr("CampDescrip").ToString().Replace("'","")
                            dr2("CampusTotal") = ds.Tables(0).Compute("SUM(TransAmount)", "CampGrpDescrip='" & dr("CampGrpDescrip").ToString().Replace("'","") & "' AND CampDescrip='" & dr("CampDescrip").ToString().Replace("'","") & "'")
                            dt.Rows.Add(dr2)
                            oldCampus = dr("CampDescrip").ToString().Replace("'","")
                            '
                        ElseIf oldCmpGrp = dr("CampGrpDescrip").ToString().Replace("'","") And oldCampus <> dr("CampDescrip").ToString().Replace("'","") Then
                            'For every different Campus, get a new row and add it to dt table.
                            'This section of the code will be used when a Campus Group has several Campuses.
                            dr2 = dt.NewRow
                            dr2("CampGrpDescrip") = oldCmpGrp
                            dr2("CampGrpTotal") = ds.Tables(0).Compute("SUM(TransAmount)", "CampGrpDescrip='" & oldCmpGrp & "'")
                            dr2("CampDescrip") = dr("CampDescrip").ToString().Replace("'","")
                            dr2("CampusTotal") = ds.Tables(0).Compute("SUM(TransAmount)", "CampGrpDescrip='" & oldCmpGrp & "' AND CampDescrip='" & dr("CampDescrip").ToString().Replace("'","") & "'")
                            dt.Rows.Add(dr2)
                            oldCampus = dr("CampDescrip").ToString().Replace("'","")
                            oldAidPrg = ""
                            oldMonth = ""
                            oldYr = ""
                        End If

                        'TransDate cannot be NULL.
                        dr("Month") = Date.Parse(dr("TransDate")).Month
                        dr("Year") = Date.Parse(dr("TransDate")).Year

                        If oldAidPrg = "" Or oldAidPrg <> dr("AwardTypeDescrip") Then
                            'New Aid Program, so we need to compute the sum of all transaction in month.
                            row = dt2.NewRow
                            row("CampGrpDescrip") = oldCmpGrp
                            row("CampDescrip") = oldCampus
                            row("AwardTypeDescrip") = dr("AwardTypeDescrip")
                            row("Month") = dr("Month")
                            row("Year") = dr("Year")
                            row("Label") = "Net for " & Date.Parse(dr("TransDate")).ToString("MMMM, yyyy") & ":"
                            row("MonthTotal") = aidRec.GetMonthTotal(dr("CampGrpId").ToString, dr("CampusId").ToString, _
                                                            dr("AwardTypeId").ToString, dr("TransDate"))
                            dt2.Rows.Add(row)
                            oldAidPrg = dr("AwardTypeDescrip")
                            oldMonth = dr("Month")
                            oldYr = dr("Year")
                            '
                        ElseIf oldMonth <> dr("Month") Or oldYr <> dr("Year") Then
                            'Same Aid Program but different month or year, so we need to compute the sum of all transaction in month.
                            row = dt2.NewRow
                            row("CampGrpDescrip") = oldCmpGrp
                            row("CampDescrip") = oldCampus
                            row("AwardTypeDescrip") = oldAidPrg
                            row("Month") = dr("Month")
                            row("Year") = dr("Year")
                            row("Label") = "Net for " & Date.Parse(dr("TransDate")).ToString("MMMM, yyyy") & ":"
                            row("MonthTotal") = aidRec.GetMonthTotal(dr("CampGrpId").ToString, dr("CampusId").ToString, _
                                                            dr("AwardTypeId").ToString, dr("TransDate"))
                            dt2.Rows.Add(row)
                            oldMonth = dr("Month")
                            oldYr = dr("Year")
                        End If

                        'Set up student name as: "LastName, FirstName MI."
                        strName = ""
                        If Not (dr("LastName") Is System.DBNull.Value) Then
                            If dr("LastName") <> "" Then
                                strName = dr("LastName")
                            End If
                        End If
                        If Not (dr("FirstName") Is System.DBNull.Value) Then
                            If dr("FirstName") <> "" Then
                                If strName <> "" Then
                                    strName &= ", " & dr("FirstName")
                                Else
                                    strName &= dr("FirstName")
                                End If
                            End If
                        End If
                        If Not (dr("MiddleName") Is System.DBNull.Value) Then
                            If dr("MiddleName") <> "" Then
                                If strName <> "" Then
                                    strName &= " " & dr("MiddleName") & "."
                                Else
                                    strName &= dr("MiddleName") & "."
                                End If
                            End If
                        End If
                        dr("StudentName") = strName
                        '
                        'Apply mask to SSN.
                        If StudentIdentifier = "SSN" Then
                            If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                                If dr("StudentIdentifier") <> "" Then
                                    Dim temp As String = dr("StudentIdentifier")
                                    dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                End If
                            End If
                        End If
                    Next
                End If
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
