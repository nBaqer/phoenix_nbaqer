Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class FComp_SummaryObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "FComp_Summary"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet
	Private RptParamInfo As ReportParamInfoIPEDS

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
        'dsRaw = FComp_SummaryObjectDB.GetReportDatasetRaw(RptParamInfo)
        dsRaw = FComp_DetailObjectDB.GetReportDatasetRaw(RptParamInfo)

		' save passed-in report parameter info 
		Me.RptParamInfo = RptParamInfo

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add("ProgDescrip", GetType(String))
			.Add("EthCodeDescrip", GetType(String))
			.Add("Count_Male", GetType(System.Int32))
			.Add("Count_Female", GetType(System.Int32))
		End With

		' loop thru program descriptions, and ethnic codes for each program, and process matching 
		'	records in raw dataset
		For Each ProgDescrip As String In IPEDSFacade.GetProgramDescripList(RptParamInfo.FilterProgramDescrips)
			For Each drEthnicInfo As DataRow In IPEDSFacade.GetIPEDSEthnicInfo.Rows
				ProcessRows(ProgDescrip, drEthnicInfo("EthCodeDescrip"))
			Next
		Next

		' return the Report DataSet
		Return dsRpt

	End Function

	Private Sub ProcessRows(ByVal ProgDescrip As String, ByVal EthCodeDescrip As String)
		Dim i As Integer
		Dim drRpt As DataRow
		Dim dvStudents As DataView

		Dim Count_Male As Integer = 0
		Dim Count_Female As Integer = 0

		' create dataview from students in raw dataset, filtered according to the passed in program 
		'	and ethnic code descriptions
		dvStudents = New DataView(dsRaw.Tables(TblNameStudents), _
								  "ProgDescrip LIKE '" & ProgDescrip & "' AND EthCodeDescrip LIKE '" & EthCodeDescrip & "'", _
								  "", DataViewRowState.CurrentRows)

		' if dataview has records, loop thru records and process raw data 
		For i = 0 To dvStudents.Count - 1
			' set indicators as appropriate
			If dvStudents(i)("GenderDescrip") = "Men" Then
				Count_Male += 1
			Else
				Count_Female += 1
			End If
		Next

		' add new summary row to report
		drRpt = dsRpt.Tables(MainTableName).NewRow

		' set program and ethnic code descriptions
		drRpt("ProgDescrip") = ProgDescrip
		drRpt("EthCodeDescrip") = EthCodeDescrip

		' set counts for men/women
		drRpt("Count_Male") = Count_Male
		drRpt("Count_Female") = Count_Female

		' add summary row to report table
		dsRpt.Tables(MainTableName).Rows.Add(drRpt)

	End Sub

End Class
