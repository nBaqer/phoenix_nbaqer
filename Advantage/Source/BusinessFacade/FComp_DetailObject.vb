Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class FComp_DetailObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "FComp_Detail"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet
	Private RptParamInfo As ReportParamInfoIPEDS

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = FComp_DetailObjectDB.GetReportDatasetRaw(RptParamInfo)

		' save passed-in report parameter info 
		Me.RptParamInfo = RptParamInfo

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add(RptStuIdentifierColName, GetType(String))
			.Add("Name", GetType(String))
			.Add("ProgDescrip", GetType(String))
			.Add("EthCodeDescrip", GetType(String))
			.Add("Ind_Male", GetType(String))
			.Add("Ind_Female", GetType(String))
		End With

		' loop thru program descriptions, and ethnic codes for each program, and process matching 
		'	records in raw dataset
		For Each ProgDescrip As String In IPEDSFacade.GetProgramDescripList(RptParamInfo.FilterProgramDescrips)
			For Each drEthnicInfo As DataRow In IPEDSFacade.GetIPEDSEthnicInfo.Rows
				ProcessRows(ProgDescrip, drEthnicInfo("EthCodeDescrip"))
			Next
		Next

		' return the Report DataSet
		Return dsRpt

	End Function

	Private Sub ProcessRows(ByVal ProgDescrip As String, ByVal EthCodeDescrip As String)
		Dim i As Integer
		Dim drRpt As DataRow
		Dim dvStudents As DataView

		' create dataview from students in raw dataset, filtered according to the passed in program 
		'	and ethnic code descriptions
		dvStudents = New DataView(dsRaw.Tables(TblNameStudents), _
								  "ProgDescrip LIKE '" & ProgDescrip & "' AND EthCodeDescrip LIKE '" & EthCodeDescrip & "'", _
								  "", DataViewRowState.CurrentRows)

		' if dataview has records, loop thru records and process raw data to create report records
		If dvStudents.Count > 0 Then
			For i = 0 To dvStudents.Count - 1
				drRpt = dsRpt.Tables(MainTableName).NewRow

				' set student identifier and name
				IPEDSFacade.SetStudentInfo(dvStudents(i), drRpt)

				' set program and ethnic code descriptions
				drRpt("ProgDescrip") = ProgDescrip
				drRpt("EthCodeDescrip") = EthCodeDescrip

				' set indicators as appropriate
				drRpt("Ind_Male") = DBNull.Value
				drRpt("Ind_Female") = DBNull.Value
				If dvStudents(i)("GenderDescrip") = "Men" Then
					drRpt("Ind_Male") = "X"
				Else
					drRpt("Ind_Female") = "X"
				End If

				dsRpt.Tables(MainTableName).Rows.Add(drRpt)
			Next

		' if dataview has no records, create record with program and ethnic code descriptions, 
		'	NULL data otherwise
		Else
			drRpt = dsRpt.Tables(MainTableName).NewRow

			drRpt(RptStuIdentifierColName) = DBNull.Value
			drRpt("Name") = DBNull.Value
			drRpt("ProgDescrip") = ProgDescrip
			drRpt("EthCodeDescrip") = EthCodeDescrip
			drRpt("Ind_Male") = DBNull.Value
			drRpt("Ind_Female") = DBNull.Value

			' add empty report row to report table
			dsRpt.Tables(MainTableName).Rows.Add(drRpt)

		End If

	End Sub

End Class
