Public Class FAFSARenewalLetterFacade
    Public Function GetFAFSADataGridDS(ByVal startDate As Date, ByVal cutOffDate As Date, ByVal campusId As String) As DataSet
        Return (New FAFSARenewalLetterDB).GetFAFSADataGridDS(startDate, cutOffDate, campusId)
    End Function

    Public Function GetFAFSALetterDS(ByVal StudentID As String) As DataSet
        Return (New FAFSARenewalLetterDB).GetFAFSALetterDS(StudentID)
    End Function
End Class
