Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.AdvantageV1.BusinessFacade.SGR_CommonObject

Public Class SGR_4YR2DetailObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SGR_4YR2Detail"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet
	Private RptParamInfo As ReportParamInfoIPEDS

	Private DateFilter As String
	Private dvEnrollments As DataView

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SGR_4YR2DetailObjectDB.GetReportDatasetRaw(RptParamInfo)

		' save passed-in report parameter info 
		Me.RptParamInfo = RptParamInfo

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet

		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		Dim dtGenderInfo As DataTable = IPEDSFacade.GetIPEDSGenderInfo
		Dim drGenderInfo As DataRow
		Dim dtEthnicInfo As DataTable = IPEDSFacade.GetIPEDSEthnicInfo
		Dim drEthnicInfo As DataRow

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add(RptStuIdentifierColName, GetType(String))
			.Add("Name", GetType(String))
			.Add("GenderDescrip", GetType(String))
			.Add("EthCodeDescrip", GetType(String))
			.Add("Ind_CompLT2Yr", GetType(String))
			.Add("Ind_Comp2to4Yr", GetType(String))
			.Add("Ind_CompBachelors", GetType(String))
			.Add("Ind_Bachelors4Yr", GetType(String))
			.Add("Ind_Bachelors5yr", GetType(String))
			.Add("Ind_TransferOut", GetType(String))
			.Add("Ind_Excluded", GetType(String))
			.Add("Ind_StillEnrolled", GetType(String))
		End With

		' create student Enrollments DataView to be used when processing records
		dvEnrollments = New DataView(dsRaw.Tables(TblNameEnrollments))

		' set up date filter to be used when processing records
		DateFilter = EnrollDateFilter(RptParamInfo)

		' loop thru genders, and ethnic groups for each gender, and process matching records in raw dataset
		For Each drGenderInfo In dtGenderInfo.Rows
			For Each drEthnicInfo In dtEthnicInfo.Rows
				ProcessRows(drGenderInfo("GenderDescrip"), drEthnicInfo("EthCodeDescrip"))
			Next
		Next

		' return the Report DataSet
		Return dsRpt

	End Function

	Private Sub ProcessRows(ByVal GenderDescrip As String, ByVal EthCodeDescrip As String)
		Dim i As Integer
		Dim drRpt As DataRow
		Dim dvStudents As DataView
		Dim AddedRows As Boolean = False
		Dim StudentId As String

		' create dataview from students in raw dataset, filtered according to passed in gender and ethnic ids
		dvStudents = New DataView(dsRaw.Tables(TblNameStudents), _
								  "GenderDescrip LIKE '" & GenderDescrip & "' AND EthCodeDescrip LIKE '" & EthCodeDescrip & "'", _
								  "", DataViewRowState.CurrentRows)

		If dvStudents.Count > 0 Then
			For i = 0 To dvStudents.Count - 1
				drRpt = dsRpt.Tables(MainTableName).NewRow

				StudentId = dvStudents(i)("StudentId").ToString

				' determine if student is a "transfer out"
				drRpt("Ind_TransferOut") = DBNull.Value
				If TransferOut(StudentId, dvEnrollments) Then
					drRpt("Ind_TransferOut") = "X"
				End If

				' determine if student is an exclusion due to drop reason
				drRpt("Ind_Excluded") = DBNull.Value
				If ExcludedFromCohort(StudentId, dvEnrollments) Then
					drRpt("Ind_Excluded") = "X"
				End If

				' if student is not a transfer out and is not excluded, determine if student completed 
				'	programs within 150% of normal time to completion, and if and when completed 
				'	Bachelors degree
				drRpt("Ind_CompLT2Yr") = DBNull.Value
				drRpt("Ind_Comp2to4Yr") = DBNull.Value
				drRpt("Ind_CompBachelors") = DBNull.Value
				drRpt("Ind_Bachelors4Yr") = DBNull.Value
				drRpt("Ind_Bachelors5Yr") = DBNull.Value
				If Not drRpt("Ind_TransferOut").Equals("X") And Not drRpt("Ind_Excluded").Equals("X") Then
					' determine if student completed any program of less than 2 years within 150% of 
					'	normal time to completion
					If Completed150Pct(StudentId, dvEnrollments, "Weeks < " & NumWeeks_2Yrs, DateFilter) Then
						drRpt("Ind_CompLT2Yr") = "X"
					End If

					' determine if student completed any program of 2 years and less than 4 years within 150% of 
					'	normal time to completion
					If Completed150Pct(StudentId, dvEnrollments, "Weeks >= " & NumWeeks_2Yrs & " AND Weeks < " & NumWeeks_4Yrs, DateFilter) Then
						drRpt("Ind_Comp2to4Yr") = "X"
					End If

					' determine if student completed any program of 4 years or greater within 150% of 
					'	normal time to completion
					If Completed150Pct(StudentId, dvEnrollments, "Weeks >= " & NumWeeks_4Yrs, DateFilter) Then
						drRpt("Ind_CompBachelors") = "X"
					End If

					' determine if student attained a Bachelor's degree in either 4 or 5 years or less
					If AttainedBachelors(StudentId, dvEnrollments, 4, DateFilter) Then
						drRpt("Ind_Bachelors4Yr") = "X"
					ElseIf AttainedBachelors(StudentId, dvEnrollments, 5, DateFilter) Then
						drRpt("Ind_Bachelors5Yr") = "X"
					End If
				End If

				' determine if student is still enrolled in programs of 5 years or longer
				drRpt("Ind_StillEnrolled") = DBNull.Value
				If StillEnrolled(StudentId, dvEnrollments, NumWeeks_5Yrs, RptParamInfo) Then
					drRpt("Ind_StillEnrolled") = "X"
				End If

				' if any of the student indicators were set, set the rest of student row info, add row to report
				If drRpt("Ind_CompLT2Yr").Equals("X") Or drRpt("Ind_Comp2to4Yr").Equals("X") Or _
				   drRpt("Ind_CompBachelors").Equals("X") Or _
				   drRpt("Ind_Bachelors4Yr").Equals("X") Or drRpt("Ind_Bachelors5Yr").Equals("X") Or _
				   drRpt("Ind_TransferOut").Equals("X") Or _
				   drRpt("Ind_Excluded").Equals("X") Or drRpt("Ind_StillEnrolled").Equals("X") Then
					' set student identifier and name
					IPEDSFacade.SetStudentInfo(dvStudents(i), drRpt)
					' set student Gender and Ethnic Code descriptions
					drRpt("GenderDescrip") = GenderDescrip
					drRpt("EthCodeDescrip") = EthCodeDescrip
					' add new row to report
					dsRpt.Tables(MainTableName).Rows.Add(drRpt)
					' indicate we added rows, so no blank row
					AddedRows = True
				End If
			Next
		End If

		' if didn't add any rows to report, either because no students match the passed-in Gender/Ethnic Code, 
		'	or because no student indicators were set, add row to report with only Gender and 
		'	Ethnic Code descriptions, blank data otherwise
		If Not AddedRows Then
			drRpt = dsRpt.Tables(MainTableName).NewRow

			drRpt(RptStuIdentifierColName) = DBNull.Value
			drRpt("Name") = DBNull.Value
			drRpt("GenderDescrip") = GenderDescrip
			drRpt("EthCodeDescrip") = EthCodeDescrip
			drRpt("Ind_CompLT2Yr") = DBNull.Value
			drRpt("Ind_Comp2to4Yr") = DBNull.Value
			drRpt("Ind_CompBachelors") = DBNull.Value
			drRpt("Ind_Bachelors4Yr") = DBNull.Value
			drRpt("Ind_Bachelors5Yr") = DBNull.Value
			drRpt("Ind_TransferOut") = DBNull.Value
			drRpt("Ind_Excluded") = DBNull.Value
			drRpt("Ind_StillEnrolled") = DBNull.Value

			' add empty report row to report 
			dsRpt.Tables(MainTableName).Rows.Add(drRpt)
		End If

	End Sub

End Class
