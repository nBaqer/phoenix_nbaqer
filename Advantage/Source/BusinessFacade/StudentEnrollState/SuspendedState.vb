Namespace StudentEnrollState

    Public Class SuspendedState
        Implements IStudentEnrollmentState

        Public Function ChangeStatusToFutureStartNew(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStartNew
            Return String.Empty
        End Function

        Public Function DropStudent(record As StudentChangeHistoryObj) Implements IStudentEnrollmentState.DropStudent
            Dim message As String = TerminateFacade.Drop(record)
            Return message
        End Function
       
        Public Function PlaceStudentOnLOA(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnLOA
            Const str = "Cannot place a student on LOA since the student is Suspended."
            Return str
        End Function

        Public Function PlaceStudentOnProbation(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnProbation
            'Can place student on probation if status is suspended
            Const str = "Cannot place the student on Probation/Warning since the student is Suspended."
            Return str
        End Function

        ''' <summary>
        ''' Change Not Allowed Suspension to another school Suspension Status.
        ''' </summary>
        ''' <param name="change"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function PlaceStudentOnSuspension(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnSuspension
            
                Dim str = String.Format("Student status is already {0}", change.StudentStatus)
                Return str

        End Function


        Public Function PostAttendance() As String Implements IStudentEnrollmentState.PostAttendance
            Return ""
        End Function

        Public Function PostFinalGrade() As String Implements IStudentEnrollmentState.PostFinalGrade
            Return ""
        End Function

        ''' <summary>
        ''' In this case maybe it is not change to Current Attendance. From suspension you need to return to previous state
        ''' </summary>
        ''' <param name="change"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ChangeStatusToCurrAttending(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToCurrAttending
            Dim result As String
            'Check if the Suspension End Date < Suspension End Date. You can only change the suspension state inside the period.
            If (change.EndDate < change.DateOfChange) Then
                result = "You can not change the Suspended Status after the end of suspension Date"
                Return result
            End If

            If (change.StartDate > change.DateOfChange) Then

                result = "You can not change the Suspended Status before the begin of suspension Status Date"
                Return result

            End If

            Dim db = New BatchStatusChangeDB()
            result = db.ChangeStatusFromSuspendedToCurrentlyAttending(change)

            Return result
        End Function

        Public Function ChangeStatusToFutureStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStart
            Return ""
        End Function

        Public Function ChangeStatusToNoStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToNoStart

            Const str = "Cannot place the student on no start since the student is in Suspended State."
            Return str
        End Function

        Public Function ChangeStatusToExternship(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToExternship
        End Function
        'Public Function ChangeStatusToExternship(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal CurrAttendingStatusId As String = "") As String Implements StudentEnrollmentState.ChangeStatusToExternship

        'End Function

        Public Function ChangeStatusToTransferOut(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToTransferOut
            Const str = "Cannot place the student on Transfer out since the student is Suspended State."
            Return str
        End Function
        'Public Function ChangeStatusToTransferOut(ByVal StuEnrollId As String, ByVal User As String, ByVal TransferOutStatusId As String) As String Implements StudentEnrollmentState.ChangeStatusToTransferOut

        'End Function
        Public Function ChangeStatusToGraduated(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToGraduated
            Const str = "Cannot graduate the student since the student is Suspended State."
            Return str
        End Function

    End Class
End Namespace