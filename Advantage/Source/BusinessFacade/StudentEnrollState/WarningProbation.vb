﻿Namespace StudentEnrollState
    Public Class WarningProbation
        Implements IStudentEnrollmentState
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="record"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function DropStudent(ByVal record As StudentChangeHistoryObj) As Object Implements IStudentEnrollmentState.DropStudent
            Throw New NotImplementedException()
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function PostAttendance() As String Implements IStudentEnrollmentState.PostAttendance
            Throw New NotImplementedException()
        End Function

        Public Function PostFinalGrade() As String Implements IStudentEnrollmentState.PostFinalGrade
            Throw New NotImplementedException()
        End Function

        Public Function PlaceStudentOnLOA(ByVal change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnLOA
            Throw New NotImplementedException()
        End Function

        Public Function PlaceStudentOnProbation(ByVal change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnProbation
            Throw New NotImplementedException()
        End Function

        Public Function PlaceStudentOnSuspension(ByVal change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnSuspension
            Dim db As New BatchStatusChangeDB
            Return db.ChangeStatusToSuspension(change)
        End Function

        Public Function ChangeStatusToCurrAttending(ByVal change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToCurrAttending
            Throw New NotImplementedException()
        End Function

        Public Function ChangeStatusToExternship(ByVal change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToExternship
            Throw New NotImplementedException()
        End Function

        Public Function ChangeStatusToFutureStart(ByVal change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStart
            Throw New NotImplementedException()
        End Function

        Public Function ChangeStatusToFutureStartNew(ByVal change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStartNew
            Throw New NotImplementedException()
        End Function

        Public Function ChangeStatusToNoStart(ByVal change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToNoStart
            Throw New NotImplementedException()
        End Function

        Public Function ChangeStatusToGraduated(ByVal change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToGraduated
            Throw New NotImplementedException()
        End Function

        Public Function ChangeStatusToTransferOut(ByVal change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToTransferOut
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace