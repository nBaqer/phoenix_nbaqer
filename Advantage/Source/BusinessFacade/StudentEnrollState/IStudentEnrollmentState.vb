Namespace StudentEnrollState

    ''' <summary>
    ''' Object to control the change between states.
    ''' </summary>
    ''' <remarks></remarks>
    Public Interface IStudentEnrollmentState
        ' Function DropStudent(ByVal stuEnrollId As String, ByVal LDA As String, ByVal DateDetermined As String, ByVal Status As String, ByVal DropReason As String, ByVal user As String) As String
        Function DropStudent(record As StudentChangeHistoryObj)

        Function PostAttendance() As String
        Function PostFinalGrade() As String

        Function PlaceStudentOnLOA(change As StudentChangeHistoryObj) As String
        'Function PlaceStudentOnLOA(ByVal StuEnrollId As String, ByVal LOAReasonId As String, ByVal StartDate As String, ByVal EndDate As String, ByVal LOAStatus As String, ByVal user As String, ByVal RequestDate As String) As String
        Function PlaceStudentOnProbation(change As StudentChangeHistoryObj) As String
        'Function PlaceStudentOnProbation(ByVal StuEnrollId As String, ByVal Reason As String, ByVal StartDate As String, ByVal EndDate As String, ByVal ProbType As String, ByVal user As String) As String

        Function PlaceStudentOnSuspension(change As StudentChangeHistoryObj) As String
        'Function PlaceStudentOnSuspension(ByVal StuEnrollId As String, ByVal SuspensionStatusId As String, ByVal Reason As String, ByVal StartDate As String, ByVal EndDate As String, ByVal user As String) As String

        'Function ChangeStatusToCurrAttending(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal CurrAttendingStatusId As String = "", Optional ByVal RevisedGradDate As String = "") As String

        Function ChangeStatusToCurrAttending(change As StudentChangeHistoryObj) As String

        Function ChangeStatusToExternship(change As StudentChangeHistoryObj) As String
        'Function ChangeStatusToExternship(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal CurrAttendingStatusId As String = "") As String
        Function ChangeStatusToFutureStart(change As StudentChangeHistoryObj) As String
        'Function ChangeStatusToFutureStart(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal FutureStartStatusId As String = "") As String
        Function ChangeStatusToFutureStartNew(change As StudentChangeHistoryObj) As String
        'Function ChangeStatusToFutureStartNew(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal FutureStartStatusId As String = "", Optional ByVal ReEnrollmentDate As String = "") As String
        Function ChangeStatusToNoStart(change As StudentChangeHistoryObj) As String
        ' Function ChangeStatusToNoStart(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal NoStartStatusId As String = "") As String
        Function ChangeStatusToGraduated(change As StudentChangeHistoryObj) As String
        'Function ChangeStatusToGraduated(ByVal StuEnrollId As String, ByVal GradDate As String, ByVal User As String, Optional ByVal GradStatusId As String = "") As String
        'Function ChangeStatusToReEntry(ByVal StuEnrollId As String, ByVal User As String, ByVal ReEntryStatusId As String, ByVal ReEnrollmentDate As String) As String
        Function ChangeStatusToTransferOut(change As StudentChangeHistoryObj) As String
        'Function ChangeStatusToTransferOut(ByVal StuEnrollId As String, ByVal User As String, ByVal TransferOutStatusId As String) As String
    End Interface
End Namespace