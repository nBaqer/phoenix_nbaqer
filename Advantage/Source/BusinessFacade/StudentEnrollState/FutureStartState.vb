Namespace StudentEnrollState

    Public Class FutureStartState
        Implements IStudentEnrollmentState

        Public Function ChangeStatusToFutureStartNew(record As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStartNew
            Const str As String = "You should not use this function."
            Return str
        End Function

        Public Function DropStudent(record As StudentChangeHistoryObj) Implements IStudentEnrollmentState.DropStudent
            Const str As String = "Cannot drop student that is a future start"
            Return str
        End Function

        Public Function PlaceStudentOnLOA(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnLOA
            Const str As String = "Cannot place a future start student on LOA."
            Return str
        End Function

        Public Function PlaceStudentOnProbation(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnProbation
            Const str As String = "Cannot place the student on probation since the student has a future start date."
            Return str
        End Function

        Public Function PlaceStudentOnSuspension(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnSuspension
            Const str As String = "Cannot place the student on suspension since the student has a future start date."
            Return str
        End Function

        Public Function PostAttendance() As String Implements IStudentEnrollmentState.PostAttendance
            Return ""
        End Function

        Public Function PostFinalGrade() As String Implements IStudentEnrollmentState.PostFinalGrade
            Return ""
        End Function

        Public Function ChangeStatusToCurrAttending(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToCurrAttending
            Const str As String = "Cannot place the student in Current Attendance. This is done automatically by the system!"
            Return str
        End Function

        Public Function ChangeStatusToFutureStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStart
            'If we are try to change to the same school status. You forbidden then
            If change.NewStatusGuid = change.OrigStatusGuid Then
                Dim str = String.Format("Student status is already {0}", change.StudentStatus)
                Return str
            End If

            ' You are changing from one school NoStart to another school NoStart 
            Dim db As New BatchStatusChangeDB
            Return db.ChangeStatusToFutureStart(change)
        End Function

        Public Function ChangeStatusToExternship(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToExternship
            Const str As String = "Cannot place a student in an Externship status since the student has a future start date."
            Return str
        End Function

        ''' <summary>
        ''' This is possible if the start data is before to today
        ''' </summary>
        ''' <param name="change"></param>
        ''' <returns></returns>
        ''' <remarks>Change to no validate date US7804 </remarks>
        Public Function ChangeStatusToNoStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToNoStart
            ' Test if the actual start date is in the future. 
            'If (change.StartDate > DateTime.Now) Then

            Return (New BatchStatusChangeDB).ChangeStatusToNoStart(change)
            'Else
            'Const str As String = "You can only change student to No Start Status if the start date is greater than today "
            'Return str
            'End If

        End Function

        ''' <summary>
        ''' Only Allowed in Transfer Out page
        ''' </summary>
        ''' <param name="change"></param>
        ''' <returns></returns>
        ''' <remarks>Use now as Date Of change</remarks>
        Public Function ChangeStatusToTransferOut(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToTransferOut
            Dim serv As BatchStatusChangeDB = New BatchStatusChangeDB()
            Dim result As String = serv.ChangeStatusToTransferOut(change)
            Return result
        End Function


        Public Function ChangeStatusToGraduated(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToGraduated
            Const str As String = "Cannot graduate the student since the student is a future start."
            Return str
        End Function

    End Class
End Namespace