Namespace StudentEnrollState

    ''' <summary>
    ''' Control the changes allowed from No Start To Another
    ''' </summary>
    ''' <remarks>The only possible change in this status is future Start</remarks>
    Public Class NoStartState
        Implements IStudentEnrollmentState
        'Public Function ChangeStatusToFutureStartNew(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal FutureStartStatusId As String = "", Optional ByVal RevisedGradDate As String = "") As String Implements StudentEnrollmentState.ChangeStatusToFutureStartNew

        'End Function
        Public Function DropStudent(record As StudentChangeHistoryObj) Implements IStudentEnrollmentState.DropStudent
            Const str = "Cannot drop the student since the student is a no start."
            Return str
        End Function

        Public Function PlaceStudentOnLOA(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnLOA
            Const str = "Cannot place the student on LOA since the student is a no start."
            Return str
        End Function

        Public Function PlaceStudentOnProbation(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnProbation
            Const str = "Cannot place the student on probation since the student is a no start."
            Return str
        End Function

        Public Function PlaceStudentOnSuspension(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnSuspension
            Const str = "Cannot place the student on suspension since the student is a no start."
            Return str
        End Function


        Public Function PostAttendance() As String Implements IStudentEnrollmentState.PostAttendance

        End Function

        Public Function PostFinalGrade() As String Implements IStudentEnrollmentState.PostFinalGrade

        End Function

        Public Function ChangeStatusToExternship(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToExternship
            Const str = "Cannot place the student on Extern-ship since the student is a no start."
            Return str
        End Function

        Public Function ChangeStatusToCurrAttending(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToCurrAttending

            Const str = "Cannot place the student on Current Attending since the student is a no start."
            Return str
            'Update the student enrollment status to 'Currently Attending' 
            'Also, update the student status to 'Active'
            'Return (New BatchStatusChangeDB).ChangeStatusToCurrentlyAttendingAndActive(change)
        End Function


        ''' <summary>
        ''' This is the only allowed change of status.
        ''' </summary>
        ''' <param name="change"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ChangeStatusToFutureStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStart
            Dim db = New BatchStatusChangeDB()
            'Update the student enrollment status to 'Future Start'
            Dim result As String = db.ChangeStatusToFutureStart(change)
            Return result
        End Function


        Public Function ChangeStatusToNoStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToNoStart
            'If we are try to change to the same school status. You forbidden then
            If change.NewStatusGuid = change.OrigStatusGuid Then
                Dim str = String.Format("Student status is already {0}", change.StudentStatus)
                Return str
            End If

            ' You are changing from one school NoStart to another school NoStart 
            Dim db As New BatchStatusChangeDB
            Return db.ChangeStatusToNoStart(change)
        End Function


        Public Function ChangeStatusToTransferOut(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToTransferOut
            Const str = "Cannot place the student on Transfer out since the student is a no start."
            Return str
        End Function

        Public Function ChangeStatusToGraduated(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToGraduated
            Const str = "Cannot graduate the student since the student is a no start."
            Return str
        End Function

        Public Function ChangeStatusToFutureStartNew(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStartNew
            Const str = "You should not use this function."
            Return str
        End Function
    End Class
End Namespace