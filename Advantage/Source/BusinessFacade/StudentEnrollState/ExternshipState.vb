
Namespace StudentEnrollState

    Public Class ExternshipState
        Implements IStudentEnrollmentState
        Public Function ChangeStatusToFutureStartNew(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStartNew

        End Function

        Public Function DropStudent(record As StudentChangeHistoryObj) Implements IStudentEnrollmentState.DropStudent
            Dim message As String = TerminateFacade.Drop(record)
            Return message
        End Function

        Public Function PlaceStudentOnLOA(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnLOA
            Dim db As New LOADB
            Dim errmessage As String = db.AreStudentLOADatesOverlapping(change.StuEnrollId, change.StartDate, change.StartDate)
            If errmessage = String.Empty Then
                Dim facade = New BatchStatusChangeDB()
                Return facade.PlaceStudentOnLOA(change)
            Else : Return errmessage
            End If
        End Function

        Public Function PlaceStudentOnProbation(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnProbation
            Dim db = New BatchStatusChangeDB()
            'Update the student enrollment status to 'dropped' and the student status
            'to 'inactive'
            Try
                Return db.PlaceStudentOnProbation(change)
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Function PlaceStudentOnSuspension(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnSuspension
            Dim db As New BatchStatusChangeDB
            Return db.ChangeStatusToSuspension(change)
        End Function

        Public Function PostAttendance() As String Implements IStudentEnrollmentState.PostAttendance

        End Function

        Public Function PostFinalGrade() As String Implements IStudentEnrollmentState.PostFinalGrade

        End Function

        ''' <summary>
        ''' Change the status to externShip to Current Attendance.
        ''' Enrollment page is altered the status and a record with the day of today is created in Student Status change table
        ''' </summary>
        ''' <param name="change"></param>
        ''' <returns></returns>
        ''' <remarks>This is a new change before you can not do this.</remarks>
        Public Function ChangeStatusToCurrAttending(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToCurrAttending
            Dim db As New BatchStatusChangeDB
            change.DateOfChange = DateTime.Now
            Dim result = db.ChangeStatusToCurrentlyAttending(change)
            Return result
        End Function


        Public Function ChangeStatusToFutureStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStart
        End Function


        Public Function ChangeStatusToNoStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToNoStart
            Const str = "Cannot place the student on No Start out since the student is in  extern-ship status.."
            Return str
        End Function


        Public Function ChangeStatusToTransferOut(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToTransferOut
            Const str = "Cannot place the student on Transfer out since the student is in extern-ship status.."
            Return str
        End Function


        Public Function ChangeStatusToGraduated(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToGraduated
            'Update student enrollment status to 'Graduated'
            'Also, update the student status to 'Inactive' if the student does not have 
            'any other enrollment active.
            Return (New BatchStatusChangeDB).ChangeStatusToGraduated(change)
        End Function

        Public Function ChangeStatusToExternship(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToExternship
            Const str = "The student is already in an Externship status."
            Return str
        End Function

    End Class

End Namespace