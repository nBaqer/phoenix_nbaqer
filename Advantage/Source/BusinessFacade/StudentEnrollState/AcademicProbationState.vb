Namespace StudentEnrollState

    Public Class AcademicProbationState
        Implements IStudentEnrollmentState
        Public Function ChangeStatusToFutureStartNew(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStartNew

        End Function

        Public Function DropStudent(record As StudentChangeHistoryObj) Implements IStudentEnrollmentState.DropStudent
            Dim message As String = TerminateFacade.Drop(record)
            Return message
        End Function
      
        Public Function PlaceStudentOnLOA(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnLOA
            Dim db As New LOADB
            Dim errmessage As String = db.AreStudentLOADatesOverlapping(change.StuEnrollId, change.StartDate, change.StartDate)
            If errmessage = String.Empty Then
                Dim facade As BatchStatusChangeDB = New BatchStatusChangeDB()
                Return facade.PlaceStudentOnLOA(change)
            Else : Return errmessage
            End If
        End Function

       Public Function PlaceStudentOnProbation(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnProbation
            Const str As String = "Cannot place student on academic probation since the student is already on academic probation."
            Return str
        End Function

        Public Function PlaceStudentOnSuspension(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnSuspension
            'Dim db As New BatchStatusChangeDB
            'Return db.ChangeStatusToSuspension(change)
            Const str As String = "Cannot place the student on suspension since the student is on academic probation."
            Return str
        End Function

        'Public Function PlaceStudentOnSuspension(ByVal StuEnrollId As String, ByVal SuspensionStatusId As String, ByVal Reason As String, ByVal StartDate As String, ByVal EndDate As String, ByVal user As String) As String Implements StudentEnrollmentState.PlaceStudentOnSuspension
        '    Dim db As New BatchStatusChangeDB

        '    'Update the student enrollment status to 'dropped' and the student status
        '    'to 'inactive'
        '    Return db.ChangeStatusToSuspension(StuEnrollId, SuspensionStatusId, Reason, StartDate, EndDate, user)
        'End Function

        Public Function PostAttendance() As String Implements IStudentEnrollmentState.PostAttendance

        End Function

        Public Function PostFinalGrade() As String Implements IStudentEnrollmentState.PostFinalGrade

        End Function

        ''' <summary>
        ''' This is only allowed to do by SAPCHECK 
        ''' </summary>
        ''' <param name="change"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ChangeStatusToCurrAttending(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToCurrAttending

            Dim batch As BatchStatusChangeDB = New BatchStatusChangeDB()
            Dim result As String = batch.ChangeStatusToCurrentlyAttending(change)
            Return result
            'Return (New BatchStatusChangeDB).ChangeStatusToCurrentlyAttending(change)

        End Function

        'Public Function ChangeStatusToCurrAttending(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal CurrAttendingStatusId As String = "", Optional ByVal RevisedGradDate As String = "") As String Implements StudentEnrollmentState.ChangeStatusToCurrAttending
        '      'only director of academics (new role added to build 2305 2.3.0) will be able to change status 
        '      'from academic probation to currently attending
        '      Dim boolIsUserDirectorofAcademics As Boolean = False
        '      boolIsUserDirectorofAcademics = (New LeadDB).IsDirectorofAcademic(User)
        '      If boolIsUserDirectorofAcademics = True Then
        '          Return (New BatchStatusChangeDB).ChangeStatusToCurrentlyAttending(StuEnrollId, User, CurrAttendingStatusId)
        '      End If
        '  End Function

        Public Function ChangeStatusToFutureStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStart
        End Function

        'Public Function ChangeStatusToFutureStart(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal FutureStartStatusId As String = "") As String Implements StudentEnrollmentState.ChangeStatusToFutureStart
        'End Function


        Public Function ChangeStatusToNoStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToNoStart
            Const str As String = "Cannot place the student on No Start out since the student is in probation state."
            Return str
        End Function
        'Public Function ChangeStatusToNoStart(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal NoStartStatusId As String = "") As String Implements StudentEnrollmentState.ChangeStatusToNoStart

        'End Function

        Public Function ChangeStatusToTransferOut(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToTransferOut
            Const str As String = "Cannot place the student on Transfer out since the student is in probation state."
            Return str
        End Function

        'Public Function ChangeStatusToTransferOut(ByVal StuEnrollId As String, ByVal User As String, ByVal TransferOutStatusId As String) As String Implements StudentEnrollmentState.ChangeStatusToTransferOut

        'End Function

        Public Function ChangeStatusToGraduated(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToGraduated
            Const str As String = "Cannot graduate the student since the student is in probation state."
            Return str
        End Function

        'Public Function ChangeStatusToGraduated(ByVal StuEnrollId As String, ByVal GradDate As String, ByVal User As String, Optional ByVal GradStatusId As String = "") As String Implements StudentEnrollmentState.ChangeStatusToGraduated

        'End Function
        Public Function ChangeStatusToExternship(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToExternship
        End Function

        'Public Function ChangeStatusToExternship(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal CurrAttendingStatusId As String = "") As String Implements StudentEnrollmentState.ChangeStatusToExternship

        'End Function
    End Class
End Namespace