Namespace StudentEnrollState

    ''' <summary>
    ''' Changes from Currently attendance to other State
    ''' </summary>
    ''' <remarks></remarks>
    Public Class CurrentlyAttendingState
        Implements IStudentEnrollmentState

        Public Function ChangeStatusToFutureStartNew(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStartNew
            Return String.Empty
        End Function
        'Public Function DropStudent(ByVal stuEnrollId As String, ByVal lda As String, ByVal dateDetermined As String, ByVal status As String, ByVal dropReason As String, ByVal user As String) As String Implements StudentEnrollmentState.DropStudent

        ''' <summary>
        ''' Drop the student and enter value in student Enrollment and StatusChange table.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function DropStudent(record As StudentChangeHistoryObj) Implements IStudentEnrollmentState.DropStudent
            Dim message As String = TerminateFacade.Drop(record)
            Return message
            ' Return TerminateFacade.Drop(stuEnrollId, lda, dateDetermined, status, dropReason, user)
        End Function


        Public Function PlaceStudentOnLOA(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnLOA
            Dim db As New LOADB
            Dim errmessage As String = db.AreStudentLOADatesOverlapping(change.StuEnrollId, change.StartDate, change.StartDate)
            If errmessage = String.Empty Then
                Dim facade As BatchStatusChangeDB = New BatchStatusChangeDB()
                Return facade.PlaceStudentOnLOA(change)
            Else : Return errmessage
            End If
        End Function

        Public Function PlaceStudentOnProbation(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnProbation
            Dim db As BatchStatusChangeDB = New BatchStatusChangeDB()

            ' Get previous status
            change.OrigStatusGuid = db.GetSchoolDefinedStatusGuid(CType(StatusCodeDB.eStatusCodes.CurrentlyAttending, Integer), change.CampusId)
            change.DropReasonGuid = Nothing ' Drop reason is only referred to drop state or LOA. use reason for other states

            'Place student in Probation
            Try
                Return db.PlaceStudentOnProbation(change)
            Catch ex As Exception
                Return ex.Message
            End Try

        End Function

        Public Function PlaceStudentOnSuspension(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnSuspension
            Dim db As New BatchStatusChangeDB
            Return db.ChangeStatusToSuspension(change)
        End Function

        Public Function PostAttendance() As String Implements IStudentEnrollmentState.PostAttendance
            Return ""
        End Function

        Public Function PostFinalGrade() As String Implements IStudentEnrollmentState.PostFinalGrade
            Return ""
        End Function

        Public Function ChangeStatusToCurrAttending(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToCurrAttending

            ' If we are try to change to the same school status. You forbidden then
            If change.NewStatusGuid = change.OrigStatusGuid Then
                Dim str = String.Format("Student status is already {0}", change.StudentStatus)
                Return str
            End If

            ' You are changing from one school currently attendance to another school currently attendance 
            Dim db As New BatchStatusChangeDB
            Return db.ChangeStatusToCurrentlyAttending(change)
        End Function

        Public Function ChangeStatusToFutureStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStart
            Dim serv As BatchStatusChangeDB = New BatchStatusChangeDB()
            Dim result As String = serv.ChangeStatusToFutureStart(change)
            Return result
        End Function

        Public Function ChangeStatusToNoStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToNoStart
            Dim db As BatchStatusChangeDB = New BatchStatusChangeDB()
            Dim result As String = db.ChangeStatusToNoStart(change)
            Return result
        End Function
        'Public Function ChangeStatusToNoStart(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal NoStartStatusId As String = "") As String Implements StudentEnrollmentState.ChangeStatusToNoStart
        '    'Update student enrollment status to 'No Start'
        '    Return (New BatchStatusChangeDB).ChangeStatusToNoStart(StuEnrollId, User, NoStartStatusId)
        'End Function

        Public Function ChangeStatusToTransferOut(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToTransferOut
            'Update student enrollment status to Transfer Out
            Dim db As BatchStatusChangeDB = New BatchStatusChangeDB()
            Dim result As String = db.ChangeStatusToTransferOut(change)
            Return result
        End Function


        'Public Function ChangeStatusToTransferOut(ByVal StuEnrollId As String, ByVal User As String, ByVal TransferOutStatusId As String) As String Implements StudentEnrollmentState.ChangeStatusToTransferOut
        '    'Update student enrollment status to 'No Start'
        '    Return (New BatchStatusChangeDB).ChangeStatusToTransferOut(StuEnrollId, User, TransferOutStatusId)
        'End Function

        Public Function ChangeStatusToGraduated(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToGraduated
            'Update student enrollment status to 'Graduated'
            'Also, update the student status to 'Inactive' if the student does not have 
            'any other enrollment active.
            Return (New BatchStatusChangeDB).ChangeStatusToGraduated(change)
        End Function


        ''' <summary>
        ''' Put the student in ExternShip, This is change the status in Enrollment table and enter a new record in Student Status Change
        ''' The day used for do the change is NOW.
        ''' </summary>
        ''' <param name="change"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ChangeStatusToExternship(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToExternship
            Dim db As BatchStatusChangeDB = New BatchStatusChangeDB()
            change.DateOfChange = DateTime.Now
            Dim result As String = db.ChangeStatusToExternship(change)
            Return result
        End Function


    End Class
End Namespace