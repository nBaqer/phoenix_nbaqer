Namespace StudentEnrollState

    Public Class TransferedOutState
        Implements IStudentEnrollmentState
        Public Function DropStudent(record As StudentChangeHistoryObj) Implements IStudentEnrollmentState.DropStudent
            Const str As String = "Cannot drop the student since the student has already transferred out."
            Return str
        End Function
        'Public Function DropStudent(ByVal stuEnrollId As String, ByVal LDA As String, ByVal DateDetermined As String, ByVal Status As String, ByVal DropReason As String, ByVal user As String) As String Implements StudentEnrollmentState.DropStudent
        '    Dim Str As String = "Cannot drop the student since the student has already transferred out."
        '    Return Str
        'End Function

        Public Function PlaceStudentOnLOA(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnLOA
            Const str As String = "Cannot place the student on LOA since the student has already transferred out."
            Return str
        End Function

        'Public Function PlaceStudentOnLOA(ByVal StuEnrollId As String, ByVal LOAReasonId As String, ByVal StartDate As String, ByVal EndDate As String, ByVal LOAStatus As String, ByVal user As String, ByVal RequestDate As String) As String Implements IStudentEnrollmentState.PlaceStudentOnLOA
        '    Dim Str As String = "Cannot place the student on LOA since the student has already transferred out."
        '    Return Str
        'End Function

        Public Function PlaceStudentOnProbation(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnProbation
            Const str As String = "Cannot place the student on probation since the student has already transferred out."
            Return str
        End Function

        'Public Function PlaceStudentOnProbation(ByVal StuEnrollId As String, ByVal Reason As String, ByVal StartDate As String, ByVal EndDate As String, ByVal ProbType As String, ByVal user As String) As String Implements IStudentEnrollmentState.PlaceStudentOnProbation
        '    Dim Str As String = "Cannot place the student on probation since the student has already transferred out."
        '    Return Str
        'End Function

        Public Function PlaceStudentOnSuspension(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnSuspension
            Const str As String = "Cannot place the student on suspension since the student has already transferred out."
            Return str
        End Function

        'Public Function PlaceStudentOnSuspension(ByVal StuEnrollId As String, ByVal SuspensionStatusId As String, ByVal Reason As String, ByVal StartDate As String, ByVal EndDate As String, ByVal user As String) As String Implements StudentEnrollmentState.PlaceStudentOnSuspension
        '    Dim Str As String = "Cannot place the student on suspension since the student has already transferred out."
        '    Return Str

        '    'Update the student enrollment status to 'dropped' and the student status
        '    'to 'inactive'
        '    'Return db.PlaceStdonProbation(StuEnrollId, Reason, StartDate, EndDate, ProbType, user)
        'End Function

        Public Function PostAttendance() As String Implements IStudentEnrollmentState.PostAttendance
            Return ""
        End Function

        Public Function PostFinalGrade() As String Implements IStudentEnrollmentState.PostFinalGrade
            Return ""
        End Function

        Public Function ChangeStatusToCurrAttending(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToCurrAttending

            'Update the student enrollment status to 'Currently Attending' 
            'Also, update the student status to 'Active'
            Return (New BatchStatusChangeDB).ChangeStatusToCurrentlyAttendingAndActive(change)
        End Function

        'Public Function ChangeStatusToCurrAttending(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal CurrAttendingStatusId As String = "", Optional ByVal RevisedGradDate As String = "") As String Implements StudentEnrollmentState.ChangeStatusToCurrAttending
        '    Return (New BatchStatusChangeDB).ChangeStatusToCurrentlyAttendingAndActive(StuEnrollId, User, CurrAttendingStatusId, RevisedGradDate)
        'End Function

        Public Function ChangeStatusToFutureStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStart
            Return String.Empty
        End Function

        'Public Function ChangeStatusToFutureStart(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal FutureStartStatusId As String = "") As String Implements StudentEnrollmentState.ChangeStatusToFutureStart
        '    Return ""
        'End Function

        Public Function ChangeStatusToExternship(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToExternship
        End Function

        'Public Function ChangeStatusToExternship(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal CurrAttendingStatusId As String = "") As String Implements StudentEnrollmentState.ChangeStatusToExternship

        'End Function
        Public Function ChangeStatusToFutureStartNew(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStartNew
            Dim db As New BatchStatusChangeDB

            'Update the student enrollment status to 'Future Start' 
            Return db.ChangeStatusToFutureStart(change)
        End Function

        Public Function ChangeStatusToNoStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToNoStart

            Const str As String = "Cannot place the student on no start since the student is Transfer Out."
            Return str
        End Function

        'Public Function ChangeStatusToNoStart(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal NoStartStatusId As String = "") As String Implements StudentEnrollmentState.ChangeStatusToNoStart

        'End Function

        Public Function ChangeStatusToTransferOut(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToTransferOut
            'Update student enrollment status to Transfer Out
            Dim db As BatchStatusChangeDB = New BatchStatusChangeDB()
            Dim result As String = db.ChangeStatusToTransferOut(change)
            Return result
        End Function

        'Public Function ChangeStatusToTransferOut(ByVal StuEnrollId As String, ByVal User As String, ByVal TransferOutStatusId As String) As String Implements StudentEnrollmentState.ChangeStatusToTransferOut
        '    Return (New BatchStatusChangeDB).ChangeStatusToTransferOut(StuEnrollId, User, TransferOutStatusId)
        'End Function

        Public Function ChangeStatusToGraduated(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToGraduated
            Const str As String = "Cannot graduate the student since the student has already transferred out."
            Return str
        End Function
        'Public Function ChangeStatusToGraduated(ByVal StuEnrollId As String, ByVal GradDate As String, ByVal User As String, Optional ByVal GradStatusId As String = "") As String Implements StudentEnrollmentState.ChangeStatusToGraduated
        '    Dim Str As String = "Cannot graduate the student since the student has already transferred out."
        '    Return Str
        'End Function
    End Class
End Namespace