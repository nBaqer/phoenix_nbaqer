﻿Namespace StudentEnrollState

    Public Class DroppedState
        Implements IStudentEnrollmentState

        Public Function DropStudent(record As StudentChangeHistoryObj) Implements IStudentEnrollmentState.DropStudent
            Const str As String = "Cannot drop the student because the student has already been dropped."
            Return str
        End Function

        Public Function PlaceStudentOnLOA(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnLOA
            Const str As String = "Cannot place student on LOA since the student has already been dropped."
            Return str
        End Function

        Public Function PlaceStudentOnProbation(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnProbation
            Const str As String = "Cannot place the student on probation since the student has already been dropped."
            Return str
        End Function

        Public Function PlaceStudentOnSuspension(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnSuspension
            Const str As String = "Cannot place the student on suspension since the student has already been dropped."
            Return str
        End Function

        Public Function PostAttendance() As String Implements IStudentEnrollmentState.PostAttendance
            Return "The student is dropped!"
        End Function

        Public Function PostFinalGrade() As String Implements IStudentEnrollmentState.PostFinalGrade
            Return "The student is dropped!"
        End Function

        Public Function ChangeStatusToCurrAttending(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToCurrAttending
            Return "You can not place a dropped student in current Attendance. Please place hi/her first in Future Start"
        End Function


        Public Function ChangeStatusToExternship(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToExternship
            Return "You can not place a dropped student in Extern Ship"
        End Function

        ''' <summary>
        ''' Place Student from Dropped to Future Start
        ''' </summary>
        ''' <param name="change"></param>
        ''' <returns></returns>
        ''' <remarks>This is the only possibility with a dropped student</remarks>
        Public Function ChangeStatusToFutureStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStart
            Dim db As BatchStatusChangeDB = New BatchStatusChangeDB()
            'Update the student enrollment status to 'Future Start'
            Dim result As String = db.ChangeStatusToFutureStart(change)
            Return result
        End Function

        Public Function ChangeStatusToFutureStartNew(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStartNew
            Dim db As New BatchStatusChangeDB

            'Update the student enrollment status to 'Future Start' 
            Return db.ChangeStatusToFutureStart(change)
        End Function

        Public Function ChangeStatusToNoStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToNoStart

            Const str As String = "Cannot place the student on no start since the student has already been dropped."
            Return str
        End Function

        Public Function ChangeStatusToTransferOut(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToTransferOut

            Return "You can not place a dropped student in Transfer out"
        End Function

        Public Function ChangeStatusToGraduated(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToGraduated
            Const str As String = "Cannot graduate the student since the student has already been dropped."
            Return str
        End Function

    End Class

End Namespace

