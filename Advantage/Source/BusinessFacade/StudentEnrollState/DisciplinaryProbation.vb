﻿Namespace StudentEnrollState
    Public Class DisciplinaryProbation
        Implements IStudentEnrollmentState

        ''' <summary>
        ''' Change the status of the enrollment to CurrentAttending and store the change status in the change status table
        ''' </summary>
        ''' <param name="change">The day of change is </param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ChangeStatusToCurrAttending(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToCurrAttending
            Return String.Empty
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="change"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ChangeStatusToExternship(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToExternship
            Return String.Empty
        End Function

        Public Function ChangeStatusToFutureStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStart
            Return String.Empty
        End Function

        Public Function ChangeStatusToFutureStartNew(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStartNew
            Return String.Empty
        End Function

        Public Function ChangeStatusToGraduated(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToGraduated
            Return String.Empty
        End Function

        Public Function ChangeStatusToNoStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToNoStart

            Const str As String = "Cannot place the student on no start since the student is in Probation State."
            Return str
        End Function

        Public Function ChangeStatusToTransferOut(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToTransferOut
            Return String.Empty
        End Function

        Public Function DropStudent(record As StudentChangeHistoryObj) As Object Implements IStudentEnrollmentState.DropStudent
            Return String.Empty
        End Function

        Public Function PlaceStudentOnLOA(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnLOA
            Dim db As BatchStatusChangeDB = New BatchStatusChangeDB()
            Return db.PlaceStudentOnLOA(change)
        End Function

        Public Function PlaceStudentOnProbation(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnProbation
            Return String.Empty
        End Function

        ''' <summary>
        ''' Change from Disciplinary Probation To Suspension. Only possible on Suspension Page.
        ''' Change the enrollment status to suspended and store a status change record and
        ''' also create a suspension record if the data of suspension is before today or today.
        ''' </summary>
        ''' <param name="change"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function PlaceStudentOnSuspension(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnSuspension
            Dim db As New BatchStatusChangeDB
            Return db.ChangeStatusToSuspension(change)
        End Function

        Public Function PostAttendance() As String Implements IStudentEnrollmentState.PostAttendance
            Return String.Empty
        End Function

        Public Function PostFinalGrade() As String Implements IStudentEnrollmentState.PostFinalGrade
            Return String.Empty
        End Function
    End Class
End Namespace