
Namespace StudentEnrollState

    Public Class GraduatedState
        Implements IStudentEnrollmentState

        Public Function DropStudent(record As StudentChangeHistoryObj) Implements IStudentEnrollmentState.DropStudent
            Const str = "Cannot drop the student since the student has already graduated."
            Return str
        End Function

        Public Function PlaceStudentOnLOA(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnLOA
            Const str = "Cannot place a student on LOA since the student has already graduated"
            Return str
        End Function

        Public Function PlaceStudentOnProbation(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnProbation
            Const str = "Cannot place the student on probation since the student has already graduated."
            Return str
        End Function

        Public Function PlaceStudentOnSuspension(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnSuspension
            Const str = "Cannot place the student on suspension since the student has already graduated."
            Return str
        End Function

        Public Function PostAttendance() As String Implements IStudentEnrollmentState.PostAttendance
            Return ""
        End Function

        Public Function PostFinalGrade() As String Implements IStudentEnrollmentState.PostFinalGrade
            Return ""
        End Function

        Public Function ChangeStatusToExternship(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToExternship

        End Function

        ''' <summary>
        ''' It is not allowed
        ''' </summary>
        ''' <param name="change"></param>
        ''' <returns></returns>
        ''' <remarks>TO implement a undo function to support be careful and revert the graduated actions</remarks>
        Public Function ChangeStatusToCurrAttending(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToCurrAttending

            Const str = "Cannot place the student on Current attending since the student has already graduated."
            Return str
        End Function


        Public Function ChangeStatusToFutureStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStart
            Const str = "Cannot place the student on future start since the student has already graduated."
            Return str
        End Function

        Public Function ChangeStatusToNoStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToNoStart

            Const str = "Cannot place the student on no start since the student has already graduated."
            Return str
        End Function

        Public Function ChangeStatusToTransferOut(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToTransferOut
            Const str = "Cannot place the student on Transfer out since the student is already graduated."
            Return str
        End Function

        ''' <summary>
        ''' You can change from one graduated school state to another.
        ''' in Status Change Page
        ''' </summary>
        ''' <param name="change"></param>
        ''' <returns></returns>
        Public Function ChangeStatusToGraduated(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToGraduated
            'If we are try to change to the same school status. You forbidden then
            If change.NewStatusGuid = change.OrigStatusGuid Then
                Dim str = String.Format("Student status is already {0}", change.StudentStatus)
                Return str
            End If

            ' You are changing from one school Graduated to another school Graduated 
            Dim db As New BatchStatusChangeDB
            Return db.ChangeStatusToGraduated(change)
        End Function

 
        Public Function ChangeStatusToFutureStartNew(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStartNew
            Return String.Empty
        End Function
    End Class
End Namespace