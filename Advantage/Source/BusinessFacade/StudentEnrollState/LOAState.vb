Namespace StudentEnrollState

    Public Class LOAState
        Implements IStudentEnrollmentState
        Public Function ChangeStatusToFutureStartNew(record As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStartNew
            Return String.Empty
        End Function

        Public Function DropStudent(record As StudentChangeHistoryObj) Implements IStudentEnrollmentState.DropStudent
            Dim message As String = TerminateFacade.Drop(record)
            Return message
        End Function

        ''' <summary>
        ''' Change Not Allowed LOA to another school LOA Status
        ''' </summary>
        ''' <param name="change"></param>
        ''' <returns>String showing the error</returns>
        Public Function PlaceStudentOnLOA(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnLOA

            Const str = "Cannot place the student on LOA since the student is already on LOA."
            Return str
        End Function

        Public Function PlaceStudentOnProbation(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnProbation
            'Update student enrollment status to 'Academic Probation' insert syStudentStatusChange
            'Also, update on the arStudentLOAs table, the LOAReturnDate for this student enrollment

            Dim db = New BatchStatusChangeDB()

            ' Get previous status
            change.OrigStatusGuid = db.GetSchoolDefinedStatusGuid(CType(StatusCodeDB.eStatusCodes.LeaveOfAbsence, Integer), change.CampusId)
            change.DropReasonGuid = Nothing ' Drop reason is only referred to drop state or LOA. use reason for other states

            'Place student in Probation
            Return db.ChangeStatusFromLOAToProbation(change)
            'Return db.PlaceStudentOnProbation(change)
        End Function

        'Public Function PlaceStudentOnProbation(ByVal StuEnrollId As String, ByVal Reason As String, ByVal StartDate As String, ByVal EndDate As String, ByVal ProbType As String, ByVal user As String) As String Implements IStudentEnrollmentState.PlaceStudentOnProbation
        '    Dim Str As String = "Cannot place the student on Probation/Warning since the student is on LOA."
        '    Return Str
        'End Function

        Public Function PlaceStudentOnSuspension(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.PlaceStudentOnSuspension
            Const str = "Cannot place the student on suspension since the student is on LOA."
            Return str
        End Function

        Public Function PostAttendance() As String Implements IStudentEnrollmentState.PostAttendance
            Return ""
        End Function

        Public Function PostFinalGrade() As String Implements IStudentEnrollmentState.PostFinalGrade
            Return ""
        End Function

        Public Function ChangeStatusToCurrAttending(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToCurrAttending
            'Update student enrollment status to 'Currently Attending'
            'Also, update on the arStudentLOAs table, the LOAReturnDate for this student enrollment
            Return (New BatchStatusChangeDB).ChangeStatusFromLOAToCurrentlyAttending(change)
        End Function

        'Public Function ChangeStatusToCurrAttending(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal CurrAttendingStatusId As String = "", Optional ByVal RevisedGradDate As String = "") As String Implements StudentEnrollmentState.ChangeStatusToCurrAttending
        '    'Update student enrollment status to 'Currently Attending'
        '    'Also, update on the arStudentLOAs table, the LOAReturnDate for this student enrollment
        '    Return (New BatchStatusChangeDB).ChangeStatusFromLOAToCurrentlyAttending(StuEnrollId, User, CurrAttendingStatusId, RevisedGradDate)
        'End Function
        Public Function ChangeStatusToFutureStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToFutureStart
            Return String.Empty
        End Function

        'Public Function ChangeStatusToFutureStart(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal FutureStartStatusId As String = "") As String Implements StudentEnrollmentState.ChangeStatusToFutureStart
        '    Return ""
        'End Function

        Public Function ChangeStatusToNoStart(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToNoStart

            Const str = "Cannot place the student on no start since the student is in on LOA."
            Return str
        End Function

        'Public Function ChangeStatusToNoStart(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal NoStartStatusId As String = "") As String Implements StudentEnrollmentState.ChangeStatusToNoStart
        '    Return ""
        'End Function

        Public Function ChangeStatusToTransferOut(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToTransferOut
            Const str = "Cannot place the student on Transfer out since the student is on LOA."
            Return str
        End Function

        'Public Function ChangeStatusToTransferOut(ByVal StuEnrollId As String, ByVal User As String, ByVal TransferOutStatusId As String) As String Implements StudentEnrollmentState.ChangeStatusToTransferOut

        'End Function

        Public Function ChangeStatusToGraduated(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToGraduated
            Const str = "Cannot graduate the student since the student is on LOA."
            Return str
        End Function

        'Public Function ChangeStatusToGraduated(ByVal StuEnrollId As String, ByVal GradDate As String, ByVal User As String, Optional ByVal GradStatusId As String = "") As String Implements StudentEnrollmentState.ChangeStatusToGraduated
        '    Dim Str As String = "Cannot graduate the student since the student is on LOA."
        '    Return Str
        'End Function

        Public Function ChangeStatusToExternship(change As StudentChangeHistoryObj) As String Implements IStudentEnrollmentState.ChangeStatusToExternship
        End Function

        'Public Function ChangeStatusToExternship(ByVal StuEnrollId As String, ByVal User As String, Optional ByVal CurrAttendingStatusId As String = "") As String Implements StudentEnrollmentState.ChangeStatusToExternship

        'End Function
    End Class
End Namespace