Public Class CopyPrgVerFacade
    'Public Function GetPrgVersions() As DataSet

    '    '   Instantiate DAL component
    '    Dim DB As New PrgVerDetailsDB

    '    '   get the dataset with all degrees
    '    Return DB.GetAllPrgVersions()

    'End Function
    Public Function GetPrgVersions(Optional ByVal campusId As String = "") As DataSet

        '   Instantiate DAL component
        Dim DB As New PrgVerDetailsDB

        '   get the dataset with all degrees
        Return DB.GetAllPrgVersions(campusId)

    End Function

    Public Function GetCampGrps() As DataSet

        '   Instantiate DAL component
        Dim DB As New CopyPrgVerDB

        '   get the dataset with all degrees
        Return DB.GetAllCampGrps()

    End Function

    Public Function GetCampGrpsForUser(ByVal UserId As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New CopyPrgVerDB

        '   get the dataset with all degrees
        Return DB.GetAllCampGrpsForUser(UserId)

    End Function

    Public Function CreateProgramVersion(ByVal Code As String, ByVal Descrip As String, ByVal CampGrp As String, ByVal PrgVer As String, ByVal user As String, ByRef createdId as Guid) As String
        Dim DB As New CopyPrgVerDB

        Return DB.CreateProgramVersion(Code, Descrip, CampGrp, PrgVer, user, createdId)
    End Function
End Class
