Public Class AuditHistoryFacade
    Public Sub AddAuditTriggers(ByVal tables As String, Optional ByVal triggerTypes As String = "IUD")
        Dim objAuditHistoryDB As New AuditHistoryDB
        objAuditHistoryDB.AddAuditTriggers(tables, triggerTypes)
    End Sub

    Public Sub DropAuditTriggers(ByVal tables As String, Optional ByVal triggerTypes As String = "IUD")
        Dim objAuditHistoryDB As New AuditHistoryDB
        objAuditHistoryDB.DropAuditTriggers(tables, triggerTypes)
    End Sub

    Public Function GetAuditHist(ByVal resourceId As Integer, ByVal strRowIds As String) As DataTable
        Dim dt As DataTable
        Dim objAuditHistoryDB As New AuditHistoryDB
        Dim dr As DataRow

        dt = objAuditHistoryDB.GetAuditHist(resourceId, strRowIds)
        'We want to change the actions from their abbreviated form to more user friendly ones
        For Each dr In dt.Rows
            Select Case CType(dr("Event"), String)
                Case "U"
                    dr("Event") = "Update"
                Case "I"
                    dr("Event") = "Insert"
                Case "D"
                    dr("Event") = "Delete"
            End Select
        Next

        Return dt
    End Function

    Public Function GetLeadIdFromStudentId(ByVal StudentId As String) As String
        Dim objAuditHistoryDB As New AuditHistoryDB

        Return objAuditHistoryDB.GetLeadIdFromStudentId(StudentId)
    End Function

End Class
