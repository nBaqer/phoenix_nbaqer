' ===============================================================================
' FacultyFacade.vb
' Business Logic for Faculry
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports FAME.AdvantageV1.DataAccess.FA
Imports FAME.AdvantageV1.Common.FA

Namespace FA

    Public Class FacultyFacade
        Public Shared Function GetClassSectionsForTerm(ByVal CampusId As String, ByVal TermId As String) As DataSet
            Return FacultyDB.GetClassSectionsForTerm(CampusId, TermId)
        End Function
    End Class

    Public Class GrdPostingsFacade
        Public Shared Function GetStudentsForClassSection(ByVal ClsSectionId As String) As DataSet
            Return GrdPostingsDB.GetStudensForClassSection(ClsSectionId)
        End Function

        Public Shared Function GetLabsForClassSection(ByVal ClsSectionId As String) As DataSet
            Return GrdPostingsDB.GetLabsForClassSection(ClsSectionId)
        End Function

        Public Shared Function GetLabResults(ByVal ClsSectionId As String, ByVal LabId As String, ByVal PostDate As DateTime) As DataTable
            Return GrdPostingsDB.GetLabResults(ClsSectionId, LabId, PostDate)
        End Function

        Public Shared Function UpdateGrdPosting(ByVal info As GrdPostingsInfo, ByVal user As String) As String
            ' Dim intRowCount As Integer = GetCount(info)
            'If intRowCount = 0 Then
            If info.IsInDB = False Then
                Return GrdPostingsDB.Add(info, user)
            Else
                Return GrdPostingsDB.Update(info, user)
            End If
        End Function
        'Public Shared Function GetCount(ByVal info As GrdPostingsInfo) As Integer
        '    Dim intRowCount As Integer = 0
        '    intRowCount = GrdPostingsDB.GetCount(info)
        '    Return intRowCount
        'End Function
        Public Shared Function DeleteGrdPosting(ByVal GrdBkResultId As String) As String
            Return GrdPostingsDB.Delete(GrdBkResultId)
        End Function

        Public Shared Function GetPostDates(ByVal ClsSectionId As String, ByVal LabId As String) As DataTable
            Return GrdPostingsDB.GetPostDates(ClsSectionId, LabId)
        End Function

        Public Shared Function GetTotalLabCounts(ByVal ClsSectionId As String, ByVal LabId As String) As DataTable
            Return GrdPostingsDB.GetTotalLabCounts(ClsSectionId, LabId)
        End Function

        Public Shared Function GetLabDetails(ByVal Id As String, ByVal term As String) As DataTable
            Return GrdPostingsDB.GetLabDetails(Id, term)
        End Function

        Public Shared Sub SetIsCourseCompleted(ByVal StuEnrollId As String, ByVal ClassSectionId As String)
            GrdPostingsDB.SetIsCourseCompleted(StuEnrollId, ClassSectionId)
        End Sub
    End Class

End Namespace
