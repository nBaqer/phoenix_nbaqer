Public Class ReportingAgencyFacade
    Public Function PopulateDataGrid() As DataSet

        Dim db As New ReportingAgencyDB
        Dim ds As New DataSet

        ds = db.GetDataGridInfo()

        Return ds

    End Function

    Public Function AddReportAgencyFields(ByVal AgencyId As String, ByVal RptFldId As String)
        Dim DB As New ReportingAgencyDB

        DB.AddReportAgencyFields(AgencyId, RptFldId)
    End Function
    Public Function DeleteReportAgencyFields(ByVal AgencyId As String, ByVal RptFldId As String)
        Dim DB As New ReportingAgencyDB

        DB.DeleteReportAgencyFields(AgencyId, RptFldId)
    End Function

    Public Function GetReportAgencies() As DataSet

        '   Instantiate DAL component
        Dim DB As New ReportingAgencyDB

        '   get the dataset with all degrees
        Return DB.GetReportingAgencies()

    End Function

    Public Function GetAgencyFields(ByVal AgencyId As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New ReportingAgencyDB

        '   get the dataset with all degrees
        Return DB.GetAgencyFields(AgencyId)

    End Function
    Public Function GetAgencyFieldValues(ByVal AgencyFldId As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New ReportingAgencyDB

        '   get the dataset with all degrees
        Return DB.GetAgencyFieldValues(AgencyFldId)

    End Function
    Public Function UpdateAgencyFieldValues(ByVal dt As DataTable) As String

        '   Instantiate DAL component
        With New ReportingAgencyDB
            '   update dataset and return result in a string
            Return .UpdateAgencyFieldValues(dt)

        End With

    End Function
    Public Function GetMaxRptAgencyFldValId() As Integer

        '   Instantiate DAL component
        Dim DB As New ReportingAgencyDB

        '   get the dataset with all degrees
        Return DB.GetMaxRptAgencyFldValId()

    End Function
    ' DE 1131 Janet Robinson 04/25/2011
    Public Function InsertValues(ByVal strValueId As String, Optional ByVal selectedRptAgencyFldValId As ArrayList = Nothing, Optional ByVal UpdTable As String = "", Optional bUpdIPEDS As Boolean = True) As Integer
        Dim DB As New ReportingAgencyDB
        Return DB.InsertValues(strValueId, selectedRptAgencyFldValId, UpdTable, bupdIPEDS)
    End Function
    ' DE 1131 Janet Robinson 04/25/2011 new function to update IPEDS
    Public Function UpdateIPEDSValues(ByVal RptAgencyFldValId As Integer, ByVal ValueId As String, Optional ByVal UpdTable As String = "") As Integer
        Try
            Dim DB As New ReportingAgencyDB
            DB.BuildIPEDSValue(RptAgencyFldValId, ValueId, UpdTable)
            Return 0
        Catch ex As Exception
            Return -1
        End Try

    End Function
    'US7083 GE - Agency Mapping Tool  -- jtorres 03/05/2015 
    Public Function UpdateGainfulEmploymentValues(ByVal RptAgencyFldValId As Integer, ByVal ValueId As String, Optional ByVal UpdTable As String = "") As Integer
        Try
            Dim DB As New ReportingAgencyDB
            DB.UpdateGainfulEmploymentValues(RptAgencyFldValId, ValueId, UpdTable)
            Return 0
        Catch ex As Exception
            Return -1
        End Try

    End Function
End Class
