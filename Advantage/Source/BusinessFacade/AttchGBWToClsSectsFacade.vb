Public Class AttchGbwToClsSectsFacade
    'Public Function GetAllTerms() As DataSet

    '    '   Instantiate DAL component
    '    Dim DB As New AttchGBWToClsSectsDB

    '    '   get the dataset with all degrees
    '    Return DB.GetAllTerms()

    'End Function
    Public Function GetAllTerms(Optional ByVal campusid As String = "") As DataSet

        '   Instantiate DAL component
        Dim db As New AttchGBWToClsSectsDB

        '   get the dataset with all degrees
        Return db.GetAllTerms(campusid)

    End Function
    Public Function GetTerms(ByVal term1 As String, Optional ByVal campusid As String = "") As DataTable ' As Hashtable
        Dim ds As  DataSet
        Dim db As New AttchGBWToClsSectsDB
        Dim copyDB As New CopyClsSectDB
        Dim rowCount As Integer
        ds = db.GetAllTerms(campusid)
        rowCount = ds.Tables("Terms").Rows.Count
        'Dim TermList As New ArrayList
        Dim ht As New Hashtable
        Dim dt As New DataTable
        dt.Columns.Add("TermId", GetType(Guid))
        dt.Columns.Add("TermDescrip", GetType(String))


        For i As Integer = 0 To rowCount - 1
            If copyDB.GetCourseCount(term1, ds.Tables("Terms").Rows(i)("TermId").ToString, campusid) > 0 Then
                Dim dr As DataRow
                dr = dt.NewRow
                dr("TermId") = ds.Tables("Terms").Rows(i)("TermId")
                dr("TermDescrip") = ds.Tables("Terms").Rows(i)("TermDescrip").ToString
                dt.Rows.Add(dr)
            End If
        Next
       Return dt
    End Function


    Public Function GetAllStatuses() As DataSet

        '   Instantiate DAL component
        Dim db As New AttchGBWToClsSectsDB

        '   get the dataset with all degrees
        Return db.GetAllStatuses()

    End Function
    Public Function GetGrdBkWgts(ByVal status As String, ByVal instructor As String) As DataSet

        '   Instantiate DAL component
        Dim db As New AttchGBWToClsSectsDB

        '   get the dataset with all degrees
        Return db.GetGradeBkWeights(status, instructor)

    End Function

    Public Function GetAvailSelectedClsSections(ByVal instructor As String, ByVal term As String, ByVal grdBkWeight As String, ByVal campusId As String) As DataSet

        '   Instantiate DAL component
        Dim db As New AttchGBWToClsSectsDB
        Dim ds As DataSet

        '   get the dataset with all degrees          
        ds = db.GetAvailSelectedClsSects(instructor, term, grdBkWeight, campusId)

        Return ProcessClsSectInfo(ds)

    End Function

    Public Function ProcessClsSectInfo(ByVal ds As DataSet) As DataSet
        Dim db As New GrdPostingsDB
        Dim tbl1 As  DataTable
        Dim tbl2 As  DataTable
        Dim row As DataRow
        Dim row2 As DataRow


        tbl1 = ds.Tables("AvailClsSects")
        tbl2 = ds.Tables("SelectedClsSects")

        'Make the ChildId column the primary key
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("ClsSectionId")}
        End With
        With tbl2
            .PrimaryKey = New DataColumn() {.Columns("ClsSectionId")}
        End With

        If ds.Tables("AvailClsSects").Rows.Count > 0 Then
            'Load a datatable with ChildId, ChildTyp and ChildDescrip
            'Create a datable with a new column called ChildId, make this the primary key.

            'Add a new column called ChildDescrip to the datatable 
            'Dim col9 As DataColumn = 
             tbl1.Columns.Add("CourseSectDescrip", GetType(String))

            For Each row In tbl1.Rows

                row("CourseSectDescrip") = row("Descrip") & " - " & row("ClsSection")

            Next

        End If

        If ds.Tables("SelectedClsSects").Rows.Count > 0 Then
            'Load a datatable with ChildId, ChildTyp and ChildDescrip
            'Create a datable with a new column called ChildId, make this the primary key.

            'Add a new column called ChildDescrip to the datatable 
            'Dim col10 As DataColumn = 
                tbl2.Columns.Add("CourseSectDescrip", GetType(String))

            For Each row2 In tbl2.Rows

                row2("CourseSectDescrip") = row2("Descrip") & " - " & row2("ClsSection")

            Next
        End If

        Return ds
    End Function

    Public Function UpdateClsSection(ByVal grdBkWgtId As String, ByVal clsSectId As String)
        Dim db As New AttchGBWToClsSectsDB

        db.UpdateClsSection(grdBkWgtId, clsSectId)
    End Function
End Class
