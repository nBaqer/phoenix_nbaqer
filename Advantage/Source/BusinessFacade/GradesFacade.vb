Public Class GradesFacade
    Public Function GetGradeSystemsDS() As DataSet

        '   Instantiate DAL component
        With New GradesDB
            '   get the dataset with all GradeSystems
            Return .GetGradeSystemsDS()

        End With

    End Function
    Public Function UpdateGradeSystemsDS(ByVal ds As DataSet) As String

        '   Instantiate DAL component
        With New GradesDB
            '   update dataset and return result in a string
            Return .UpdateGradeSystemsDS(ds)

        End With

    End Function
    Public Function GetTimeCollisions(ByVal instructorId As String, ByVal roomId As String, ByVal workDaysId As String, ByVal startTimeId As String, ByVal endTimeId As String, ByVal startDate As Date, ByVal endDate As Date, ByVal clsSectMeetingId As String) As String

        '   Instantiate DAL component
        Dim tc As New DALExceptions

        Return tc.TimeCollisions(instructorId, roomId, workDaysId, startTimeId, endTimeId, startDate, endDate, clsSectMeetingId)

    End Function
    Public Function GetTimeCollisionsByClsSectionId(ByVal instructorId As String, ByVal roomId As String, ByVal workDaysId As String, ByVal startTimeId As String, ByVal endTimeId As String, ByVal startDate As Date, ByVal endDate As Date, ByVal clsSectionId As String) As String

        '   Instantiate DAL component
        Dim tc As New DALExceptions

        Return tc.TimeCollisionsByClsSectionId(instructorId, roomId, workDaysId, startTimeId, endTimeId, startDate, endDate, clsSectionId)

    End Function
    'Public Function GetGradeBookDisplayDS(ByVal empId As String, ByVal termId As String) As DataSet

    '    '   Instantiate DAL component
    '    With New GradesDB

    '        Return .GetGradeBookDisplayDS(empId, termId)

    '    End With

    'End Function
    Public Function GetGradeScalesDS() As DataSet

        '   Instantiate DAL component
        With New GradesDB
            '   get the dataset with all GradeScales
            Return .GetGradeScalesDS()

        End With

    End Function
    Public Sub UpdateGrades(ByVal GrdSysDetailId As String, ByVal Quality As String, ByVal GradeDescription As String)
        With New GradesDB
            .UpdateGrades(GrdSysDetailId, Quality, GradeDescription)
        End With
    End Sub
    Public Function GetGradesInfo(ByVal GrdSysDetailId As String) As GrdPostingsInfo
        With New GradesDB
            Return .GetGradesInfo(GrdSysDetailId)
        End With
    End Function
    Public Function GetAllGradesDescripSystems() As DataSet
        With New GradesDB
            Return .GetAllGradesDescripSystems
        End With
    End Function
    Public Function GetAllGrades(ByVal GrdSystemId As String) As DataSet
        '   Instantiate DAL component
        With New GradesDB
            '   get the dataset with all GradeScales
            Return .GetAllGrades(GrdSystemId)
        End With
    End Function
    Public Function UpdateGradeScalesDS(ByVal ds As DataSet) As String

        '   Instantiate DAL component
        With New GradesDB
            '   update dataset and return result in a string
            Return .UpdateGradeScalesDS(ds)

        End With

    End Function
    Public Function GetAllGradeSystems(ByVal showActiveOnly As Boolean) As DataSet

        '   Instantiate DAL component
        With New GradesDB
            '   get the dataset with all GradeScales
            Return .GetAllGradeSystems(showActiveOnly)

        End With

    End Function
    'Public Function DoGrdScalesExistFrGrdSys(ByVal GradeSystem As String) As Integer
    '    Dim db As New GradesDB

    '    Return db.DoGrdScalesExistFrGrdSys(GradeSystem)
    'End Function
    Public Function GetGradesByResultId(ByVal grdSysDetailId As String) As DataSet
        Return (New GradesDB).GetGradesByResultId(grdSysDetailId)
    End Function
    Public Function GetAllGradeScales_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable
        Dim db As New GradesDB
        Return db.GetAllGradeScales_SP(showActiveOnly, campusId)

    End Function
    Public Function GetSysGradeBookComponents() As DataSet
        Dim db As New GradesDB
        Return db.GetSysGradeBookComponents()
    End Function
    Public Function GetSystemComponents(ByVal GrdComponentTypeId As String) As Integer
        Dim db As New GradesDB
        Return db.GetSystemComponents(GrdComponentTypeId)
    End Function
    Public Sub UpdateSysComponentTypesandAddCourses(ByVal GrdComponentTypeId As String, Optional ByVal xmlCourses As String = "")
        Dim db As New GradesDB
        db.UpdateSysComponentTypesandAddCourses(GrdComponentTypeId.ToString, xmlCourses)
    End Sub
    Public Function GetCoursesByCampusIdandStatusId(ByVal CampusId As String, _
                                                    ByVal StatusId As String) As DataSet
        Return (New GradesDB).GetCoursesByCampusIdandStatusId(CampusId, StatusId)
    End Function
    Public Function GetCoursesByGrdComponentTypeId(ByVal GrdComponentTypeId As String) As DataSet
        Return (New GradesDB).GetCoursesByGrdComponentTypeId(GrdComponentTypeId)
    End Function
    Public Function GetCountbySysCompTypeIdandCourseId(ByVal CourseId As String) As Integer
        Return (New GradesDB).GetCountbySysCompTypeIdandCourseId(CourseId)
    End Function
    Public Sub UpdateSysComponentTypes(ByVal GrdComponentTypeId As String, ByVal SysCompTypeId As Integer)
        Dim GrdDB As New GradesDB
        GrdDB.UpdateSysComponentTypes(GrdComponentTypeId, SysCompTypeId)
    End Sub
    Public Function boolDictationSpeedTestComponentExist(ByVal ResourceId As Integer) As Boolean
        ' Used for Unit Testing
        Dim grdDB As New GradesDB
        Dim boolResourceExist As Boolean
        boolResourceExist = grdDB.boolDictationSpeedTestComponentExist(ResourceId)
        Return boolResourceExist
    End Function
    Public Function GetTestType() As DataTable
        Dim grdDB As New GradesDB
        Return (grdDB.GetTestType())
    End Function
    Public Function GetCCRCoursesByCampusIdandStatusId(ByVal CampusId As String, _
                                                   ByVal StatusId As String) As DataSet
        Return (New GradesDB).GetCCRCoursesByCampusIdandStatusId(CampusId, StatusId)
    End Function
    Public Function GetPages(ByVal roleId As String, ByVal moduleResourceId As Integer, ByVal pageType As Integer, _
                             ByVal intSchoolOptions As Integer, ByVal strCampusID As String) As DataTable
        Return (New GradesDB).GetPages(roleId, moduleResourceId, pageType, intSchoolOptions, strCampusID)
    End Function
    Public Function AddPagesToModules(ByVal intSchoolOption As Integer) As DataTable
        Return (New GradesDB).AddPagesToModules(intSchoolOption)
    End Function
    Public Function GetARCommonTasksChild(ByVal ParentResourceId As Integer) As DataTable
        Return (New GradesDB).GetARCommonTasksChild(ParentResourceId)
    End Function
    Public Sub UpdateProgramCIPCode(ByVal CIPCode As String, _
                                     ByVal CredentialLevel As String, _
                                     ByVal ProgId As String, _
                                     Optional ByVal IsGEProgram As Boolean = True, _
                                     Optional ByVal Is1098T As Boolean = True)
        Dim updateDB As New GradesDB
        updateDB.UpdateProgramCIPCode(CIPCode, CredentialLevel, ProgId, IsGEProgram, Is1098T)
    End Sub
    Public Function GetProgramCIPCode(ByVal ProgId As String) As DataSet
        Return (New GradesDB).GetProgramCIPCode(ProgId)
    End Function
    Public Function BuildDSForDataExport(ByVal AwardYear As String, ByVal CampusId As String, Optional ByVal ProgramId As String = "") As DataSet
        Return (New GradesDB).BuildDSForDataExport(AwardYear, CampusId, ProgramId)
    End Function

    Public Function GetCampusCodeFromCampusId(ByVal campusId As String) As String
        Return (New GradesDB).GetCampusCodeFromCampusId(campusId)
    End Function

    Public Function GetProgCodeFromProgId(ByVal progId As String) As String
        Return (New GradesDB).GetProgCodeFromProgId(progId)
    End Function

    'Public Function getPermissions(ByVal UserId As String) As DataTable
    '    Return (New GradesFacade).getPermissions(UserId)
    'End Function
    Public Function getRowsCount(ByVal CampusId As String, ByVal TransStartDate As Date, ByVal TransEndDate As Date, ByVal Transaction As String) As Integer
        Return (New GradesDB).getRowsCount(CampusId, TransStartDate, TransEndDate, Transaction)
    End Function
End Class
