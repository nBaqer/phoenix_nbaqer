' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' StudentNotesFacade.vb
'
' StudentNotesFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class StudentNotesFacade

    Public Function GetStudentNoteInfo(ByVal StudentNoteId As String) As StudentNoteInfo

        '   Instantiate DAL component
        With New StudentNotesDB

            '   get the StudentNoteInfo
            Return .GetStudentNoteInfo(StudentNoteId)

        End With

    End Function
    Public Function UpdateStudentNoteInfo(ByVal StudentNoteInfo As StudentNoteInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New StudentNotesDB

            '   If it is a new account do an insert. If not, do an update
            If Not (StudentNoteInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddStudentNoteInfo(StudentNoteInfo, user)
            Else
                '   return integer with update results
                Return .UpdateStudentNoteInfo(StudentNoteInfo, user)
            End If

        End With

    End Function
    Public Function DeleteStudentNoteInfo(ByVal StudentNoteId As String, ByVal modDate As Date) As String

        '   Instantiate DAL component
        With New StudentNotesDB

            '   delete BankInfo ans return integer result
            Return .DeleteStudentNoteInfo(StudentNoteId, modDate)

        End With

    End Function
    Public Function GetNotesForStudentByModuleDS(ByVal StudentId As String, ByVal modCode As String, ByVal fromDate As Date, ByVal toDate As Date) As DataSet

        '   get dataset with StudentNoteInfo's
        Return (New StudentNotesDB).GetNotesForStudentByModuleDS(StudentId, modCode, fromDate, toDate)

    End Function
    Public Function GetMinAndMaxDatesFromNotes() As Date()

        '   return date Array
        Return (New StudentNotesDB).GetMinAndMaxDatesFromNotes()

    End Function
End Class
