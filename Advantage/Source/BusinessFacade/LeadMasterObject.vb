Public Class LeadMasterObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim leadList As New LeadMasterDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(leadList.GetLeadList(rptParamInfo))
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function
    Public Function getTransactionsforApplicantLedger(ByVal LeadId As String) As DataSet
        Dim leadList As New LeadMasterDB
        Dim ds As New DataSet
        ds = leadList.getTransactionsforApplicantLedger(LeadId)
        Return ds
    End Function
    Public Sub MakeATransactionVoid(ByVal TransactionId As String)
        Dim leadList As New LeadMasterDB
        leadList.MakeATransactionVoid(TransactionId)

    End Sub
    Public Sub UpdateReversalReason(ByVal TransactionId As String, _
                                   ByVal ReversalReason As String, _
                                   ByVal strLeadId As String, ByVal strTransCodeId As String, _
                                   ByVal strTransReference As String, ByVal strTransDescrip As String, _
                                   ByVal strTransAmount As String, _
                                   ByVal dtTransDate As Date, _
                                   ByVal strCampusId As String, ByVal intTransTypeId As Integer, _
                                   ByVal boolstrEnrolled As Boolean, ByVal boolVoided As Boolean, ByVal strModUser As String, ByVal dtModDate As Date)
        Dim leadList As New LeadMasterDB
        leadList.UpdateReversalReason(TransactionId, ReversalReason, strLeadId, strTransCodeId, strTransReference, strTransDescrip, strTransAmount, dtTransDate, strCampusId, intTransTypeId, _
                                       boolstrEnrolled, boolVoided, strModUser, dtModDate)
    End Sub
    Public Sub insertTransactionsforApplicantLedger(ByVal LeadId As String, _
                                                       ByVal TransCodeId As String, _
                                                       ByVal TransReference As String, _
                                                       ByVal TransDescrip As String,
                                                       ByVal TransAmount As Decimal, _
                                                       ByVal TransDate As DateTime, _
                                                       ByVal ModUser As String, _
                                                       ByVal ModDate As DateTime, _
                                                       ByVal CampusId As String, _
                                                       ByVal MaptoTransactionId As String)
        Dim leadlist As New LeadMasterDB
        leadlist.insertTransactionsforApplicantLedger(LeadId, TransCodeId, TransReference,
                                                      TransDescrip, TransAmount, TransDate,
                                                      ModUser, ModDate, CampusId, MaptoTransactionId)
    End Sub
#End Region


#Region "Private Methods"

    ' Copy data from dataset to datatable.
    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim zipMask As String
        Dim strName As String = ""
        Dim streetAddress As String = ""
        Dim cityStateZip As String = ""

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        Try
            ds.Tables(0).TableName = "LeadMasterDetail"
            ds.Tables(0).Columns.Add(New DataColumn("LeadName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("FullAddress", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("LeadCount", System.Type.GetType("System.Int32")))

            For Each dr As DataRow In ds.Tables(0).Rows
                dr("LeadCount") = ds.Tables(0).Rows.Count
                streetAddress = ""
                cityStateZip = ""
                strName = ""
                'Only Lead's MiddleName allows NULL.
                If Not (dr("MiddleName") Is System.DBNull.Value) Then
                    If dr("MiddleName") <> "" Then
                        dr("LeadName") = dr("LastName") & ", " & dr("FirstName") & _
                                            " " & dr("MiddleName") & "."
                    Else
                        dr("LeadName") = dr("LastName") & ", " & dr("FirstName")
                    End If
                Else
                    dr("LeadName") = dr("LastName") & ", " & dr("FirstName")
                End If
                '
                'Address1 allows NULL.
                If Not (dr("Address1") Is System.DBNull.Value) Then
                    If dr("Address1") <> "" Then
                        streetAddress = dr("Address1")
                    End If
                Else
                    streetAddress = ""
                End If
                'Address2 allows NULL.
                If Not (dr("Address2") Is System.DBNull.Value) Then
                    If dr("Address2") <> "" Then
                        streetAddress &= " " & dr("Address2")
                    End If
                End If
                dr("FullAddress") = streetAddress
                'City allows NULL.
                If Not (dr("City") Is System.DBNull.Value) Then
                    If dr("City") <> "" Then
                        cityStateZip = dr("City")
                    End If
                Else
                    cityStateZip = ""
                End If
                'State allows NULL.
                If Not (dr("StateDescrip") Is System.DBNull.Value) Then
                    If Not (dr("ForeignZip") Is System.DBNull.Value) Then
                        If dr("StateDescrip") <> "" And dr("ForeignZip") = False Then
                            'Domestic State
                            If cityStateZip <> "" Then
                                cityStateZip &= ", " & dr("StateDescrip")
                            Else
                                cityStateZip = dr("StateDescrip")
                            End If
                        End If
                    Else
                        'Domestic State
                        If dr("StateDescrip") <> "" Then
                            If cityStateZip <> "" Then
                                cityStateZip &= ", " & dr("StateDescrip")
                            Else
                                cityStateZip = dr("StateDescrip")
                            End If
                        End If
                    End If
                End If
                'StateOther allows NULL.
                If Not (dr("OtherState") Is System.DBNull.Value) Then
                    If Not (dr("ForeignZip") Is System.DBNull.Value) Then
                        If dr("OtherState") <> "" And dr("ForeignZip") = True Then
                            'International State
                            If cityStateZip <> "" Then
                                cityStateZip &= ", " & dr("OtherState")
                            Else
                                cityStateZip = dr("OtherState")
                            End If
                        End If
                    End If
                End If
                'Zip allows NULL.
                If Not (dr("Zip") Is System.DBNull.Value) Then
                    If Not (dr("ForeignZip") Is System.DBNull.Value) Then
                        If dr("Zip") <> "" And dr("ForeignZip") = False Then
                            'Domestic Zip
                            If cityStateZip <> "" Then
                                cityStateZip &= " " & facInputMasks.ApplyMask(zipMask, dr("Zip"))
                            Else
                                cityStateZip = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                            End If
                        Else
                            'International Zip
                            If dr("Zip") <> "" Then
                                If cityStateZip <> "" Then
                                    cityStateZip &= " " & dr("Zip")
                                Else
                                    cityStateZip &= dr("Zip")
                                End If
                            End If
                        End If
                    Else
                        'Domestic Zip
                        If dr("Zip") <> "" Then
                            If cityStateZip <> "" Then
                                cityStateZip &= " " & facInputMasks.ApplyMask(zipMask, dr("Zip"))
                            Else
                                cityStateZip = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                            End If
                        End If
                    End If
                End If
                If cityStateZip <> "" Then
                    If dr("FullAddress") <> "" Then
                        dr("FullAddress") &= " " & cityStateZip
                    Else
                        dr("FullAddress") = cityStateZip
                    End If
                End If
                'Country allows NULL.
                If Not (dr("CountryDescrip") Is System.DBNull.Value) Then
                    If dr("CountryDescrip") <> "" Then
                        If dr("FullAddress") <> "" Then
                            dr("FullAddress") &= Chr(10) & dr("CountryDescrip")
                        Else
                            dr("FullAddress") = dr("CountryDescrip")
                        End If
                    End If
                End If
                '
                'Apply mask.
                If Not (dr("Phone") Is System.DBNull.Value) Then
                    If Not (dr("ForeignPhone") Is System.DBNull.Value) Then
                        If dr("Phone") <> "" And dr("ForeignPhone") = False Then
                            'Domestic Phone
                            dr("Phone") = facInputMasks.ApplyMask(strMask, dr("Phone"))
                        Else
                            'International Phone
                            'dr("Phone") = dr("Phone")
                        End If
                    Else
                        'Domestic Phone
                        If dr("Phone") <> "" Then
                            dr("Phone") = facInputMasks.ApplyMask(strMask, dr("Phone"))
                        End If
                    End If
                End If
            Next

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
