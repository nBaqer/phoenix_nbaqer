Public Class BuildingFacade
    Public Function GetAllBuildings() As DataSet

        Dim db As New BuildingDB
        Dim ds As New DataSet
        ds = db.GetAllBuildings()
        Return ds
    End Function

    Public Function GetBldgInfo(ByVal BldgId As String) As BuildingInfo

        '   Instantiate DAL component
        With New BuildingDB

            '   get the BankInfo
            Return .GetBldgInfo(BldgId)

        End With

    End Function
    Public Function UpdateBuildingInfo(ByVal BldgInfo As BuildingInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New BuildingDB

            '   If it is a new account do an insert. If not, do an update
            If Not (BldgInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddBuilding(BldgInfo, user)
            Else
                '   return integer with update results
                Return .UpdateBuilding(BldgInfo, user)
            End If

        End With

    End Function
    Public Function DeleteBuilding(ByVal BldgId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New BuildingDB

            '   delete BankInfo ans return integer result
            Return .DeleteBuilding(BldgId, modDate)

        End With

    End Function


End Class
