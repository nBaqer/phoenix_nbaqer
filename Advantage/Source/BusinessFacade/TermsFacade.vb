Public Class TermsFacade
    Public Function GetAllTermIdsWithProgIds(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "") As DataSet
        '   get the dataset with all degrees
        Return (New TermsDB).GetAllTermIdsWithProgIds(showActiveOnly, campusId)
    End Function
    Public Function GetCohortStartDatebyUserandRoles(ByVal userId As String, ByVal userName As String, ByVal isAcademicAdvisor As Boolean, Optional ByVal campusId As String = "") As DataSet
        'Dim objClsSectAtt As New ClsSectAttendanceDB
        'Return objClsSectAtt.GetTerms 'commented for bug fix 4044

        '   Instantiate DAL component
        Dim DB As New TermsDB
        '   get the dataset with all degrees
        Return DB.GetCohortStartDateforNoneFutureActiveTermsbyUserandRoles(userId, userName, isAcademicAdvisor, campusId)

    End Function
    Public Function GetAllTerms(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "") As DataSet
        Dim DB As New TermsDB
        Return DB.GetAllTerms(showActiveOnly, campusId)
    End Function
    Public Function GetAllTermsByUser(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "", Optional ByVal username As String = "", Optional ByVal userid As String = "") As DataSet
        Dim DB As New TermsDB
        Return DB.GetAllTermsByUser(showActiveOnly, campusId, username, userid)
    End Function

    Public Function GetTermInfo(ByVal TermId As String) As TermInfo
        Dim DB As New TermsDB
        Return DB.GetTermInfo(TermId)
    End Function
    Public Function GetAllTerms_SP(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "") As DataTable
        Dim DB As New TermsDB
        Return DB.GetAllTerms_SP(showActiveOnly, campusId)
    End Function
End Class
