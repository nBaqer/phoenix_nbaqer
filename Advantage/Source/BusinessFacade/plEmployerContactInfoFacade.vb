Public Class plEmployerContactInfoFacade
    Public Function UpdateEmployerContactInfo(ByVal employerinfo As plEmployerContact, ByVal user As String) As String

        With New PlEmployerContactInfoDB
            If (employerinfo.IsInDB = False) Then
                Return .AddEmployerInfo(employerinfo, user)
            Else
                Return .UpdateEmployerInfo(employerinfo, user)
            End If
        End With
    End Function
    Public Function GetEmployerContactInfo(ByVal employerid As String) As plEmployerContact

        With New PlEmployerContactInfoDB
            Return .GetEmployerContactInfo(employerid)
        End With
    End Function
    Public Function GetAllEmployerContactsByEmployerId(ByVal showActiveOnly As Boolean, ByVal EmployerId As String) As DataSet

        '   Instantiate DAL component
        With New PlEmployerContactInfoDB

            '   get the dataset with all BankCodes
            Return .GetAllEmployerContactsByEmployerId(showActiveOnly, EmployerId)

        End With
    End Function
    Public Function GetAllEmployerContacts(ByVal showActiveOnly As String, ByVal EmployerId As String) As DataSet
        '   Instantiate DAL component
        With New PlEmployerContactInfoDB
            '   get the dataset with all BankCodes
            Return .GetAllEmployerContacts(showActiveOnly, EmployerId)
        End With
    End Function
    Public Function DeleteEmployerContactInfo(ByVal EmployerId As String, ByVal moddate As DateTime) As String

        '   Instantiate DAL component
        With New PlEmployerContactInfoDB

            '   delete EmployerInfo ans return integer result
            Return .DeleteEmployerInfo(EmployerId, moddate)

        End With

    End Function
End Class
