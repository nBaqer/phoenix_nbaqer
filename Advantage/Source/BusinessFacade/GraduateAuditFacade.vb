Imports System.Collections
Imports System.Data.SqlClient
Imports FAME.Advantage.Common
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.Expressions

Public Class GraduateAuditFacade

#Region "Private Data Members"

    Private dtGraduateAudit As New DataTable
    Private dtResults As New DataTable
    '
    Private mGpa As Decimal
    Private mAttemptedCredits As Decimal
    Private mAllAttemptedCredits As Decimal
    Private mEarnedCredits As Decimal
    Private mEarnedFinAidCredits As Decimal
    Private mCompletedHours As Decimal
    Private mRemainingCredits As Decimal
    Private mRemainingHours As Decimal
    Private mTotalClasses As Integer
    Private mPercentageEarnedCredits As Decimal
    Private mGradeInGpa As Boolean
    Private mTotalScheduledHours As Decimal
    Private mAllScheduledHours As Decimal
    Private mCreditZeroWeight As Decimal
    Private mAttemptedCreditsForSAP As Decimal
    Private mAllAttemptedCreditsForSAP As Decimal
    Private mEarnedCreditsForSAP As Decimal
    Private mPercentageEarnedCreditsForSAP As Decimal

    Private ReadOnly myAdvAppSettings As FAME.Advantage.Common.AdvAppSettings
    Private ReadOnly dbTranscript As TranscriptDB
    Private ReadOnly dbTransferGrade As TransferGradeDB
    Private ReadOnly dbExams As ExamsDB
    Private ReadOnly dbStuTranscript As StuTranscriptDB
    Private ReadOnly brRegisterStudent As RegisterStudentsBR

#End Region

#Region "Properties"

    Public ReadOnly Property GPA() As Decimal
        Get
            Return mGpa
        End Get
    End Property
    Public ReadOnly Property AttemptedCredits() As Decimal
        Get
            Return mAttemptedCredits
        End Get
    End Property
    Public ReadOnly Property AllAttemptedCredits() As Decimal
        Get
            Return mAllAttemptedCredits
        End Get
    End Property
    ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
    Public ReadOnly Property AllScheduledHours() As Decimal
        Get
            Return mAllScheduledHours
        End Get
    End Property
    Public ReadOnly Property TotalScheduledHours() As Decimal
        Get
            Return mTotalScheduledHours
        End Get
    End Property
    ''
    Public ReadOnly Property EarnedCredits() As Decimal
        Get
            Return mEarnedCredits
        End Get
    End Property
    Public ReadOnly Property EarnedFinAidCredits() As Decimal
        Get
            Return mEarnedFinAidCredits
        End Get
    End Property

    Public ReadOnly Property CompletedHours() As Decimal
        Get
            Return mCompletedHours
        End Get
    End Property
    Public ReadOnly Property TotalClasses() As Integer
        Get
            Return mTotalClasses
        End Get
    End Property
    Public ReadOnly Property RemainingCredits() As Decimal
        Get
            Return mRemainingCredits
        End Get
    End Property
    Public ReadOnly Property RemainingHours() As Decimal
        Get
            Return mRemainingHours
        End Get
    End Property
    Public ReadOnly Property PercentageEarnedCredits() As Decimal
        Get
            Return mPercentageEarnedCredits
        End Get
    End Property
    Public ReadOnly Property GradeInGPA() As Boolean
        Get
            Return mGradeInGpa
        End Get
    End Property

    Public ReadOnly Property CreditZeroWeight() As Decimal
        Get
            Return mCreditZeroWeight
        End Get
    End Property

    Public ReadOnly Property AttemptedCreditsForSAP() As Decimal
        Get
            Return mAttemptedCreditsForSAP
        End Get
    End Property

    Public ReadOnly Property EarnedCreditsForSAP() As Decimal
        Get
            Return mEarnedCreditsForSAP
        End Get
    End Property

    Public ReadOnly Property PercentageEarnedCreditsForSAP() As Decimal
        Get
            Return mPercentageEarnedCreditsForSAP
        End Get
    End Property

    Public ReadOnly Property AllAttemptedCreditsForSAP() As Decimal
        Get
            Return mAllAttemptedCreditsForSAP
        End Get
    End Property
#End Region

    Sub New()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()

        'Init Dependables Services
        dbTranscript = New TranscriptDB()
        dbTransferGrade = New TransferGradeDB()
        dbExams = New ExamsDB()
        dbStuTranscript = New StuTranscriptDB()
        brRegisterStudent = New RegisterStudentsBR()
    End Sub

#Region "Private Methods"

    Private Sub AddRowToDt(ByVal dr As DataRow, ByVal ds As DataSet, ByVal dt As DataTable,
                            ByVal parentId As String, ByVal level As Integer, ByVal gradeReps As String, ByVal includeHours As Boolean)
        Dim r As DataRow = dt.NewRow

        Dim aRows() As DataRow
        Dim aRowsGroup() As DataRow
        'Dim iCount As Integer
        'Dim dtReqsForCourseGrp As DataTable
        'Dim fac As New TranscriptFacade
        'Dim transDB As New TranscriptDB
        'Dim br As New RegisterStudentsBR
        Dim equivReqid As String
        Dim isPass As Boolean = False
        'Course/Course Group/Program Version Description
        r("Descrip") = dr("Req")

        'Requirement Id
        r("ReqId") = dr("ReqId")

        'Course Code
        r("Code") = dr("Code")

        'Requirement Type Id
        r("ReqTypeId") = dr("ReqTypeId")

        'ParentId
        If parentId <> "" Then r("ParentId") = parentId

        'Level
        r("Level") = level

        'Credits
        r("Credits") = dr("Credits").ToString()
        r("FinAidCredits") = dr("FinAidCredits").ToString()
        'Hours
        r("Hours") = dr("Hours").ToString()

        r("PrgVerId") = dr("PrgVerId").ToString()
        r("IsPass") = False

        'Grade, GPA, Start Date
        '       If we are dealing with a course then we will lookup the grade from the Results
        '       datatable in the dataset
        If dr("ReqTypeId") = 1 Then 'Course

            Dim score As Decimal
            Dim gpal As Decimal

            If gradeReps.ToUpper = "LATEST" Then
                'Latest attempt
                aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "EndDate DESC, StartDate DESC")
            ElseIf gradeReps.ToUpper = "BEST" Then
                'Best GPA
                aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "GPA DESC,IsPass DESC")

            Else
                aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "GPA DESC,IsPass DESC")
                Dim reqid As String = dr("ReqId").ToString()
                If (ds.Tables("Results").Compute("Avg(GPA)", "ReqId = '" & reqid & "'")) Is DBNull.Value Then
                    gpal = 0.0
                Else
                    gpal = ds.Tables("Results").Compute("Avg(GPA)", "ReqId = '" & reqid & "'")
                End If

                If (ds.Tables("Results").Compute("Avg(Score)", "ReqId = '" & reqid & "'")) Is DBNull.Value Then
                    score = 0.0
                Else
                    score = ds.Tables("Results").Compute("Avg(Score)", "ReqId = '" & reqid & "'")
                End If
            End If

            If aRows.Length > 0 Then 'There are results for this course
                'Check if any of the results (grades) is included in credits earned
                Dim aRow As DataRow = aRows(0)
                r("Grade") = aRow("Grade")
                If Not aRow.IsNull("Score") Then

                    If gradeReps.ToUpper = "AVERAGE" Then
                        r("Score") = score
                    Else
                        r("Score") = CType(aRow("Score"), Decimal)
                    End If

                End If
                If aRow("Descrip").ToString.ToLower.Trim <> dr("Req").ToString.ToLower.Trim Then
                    r("EquivCourseDesc") = aRow("Descrip").ToString.Trim

                End If
                '
                If Not aRow.IsNull("IsInGPA") Then
                    If aRow("IsInGPA") = 1 Or aRow("IsInGPA") = True Then
                        If Not aRow.IsNull("GPA") Then

                            If gradeReps.ToUpper = "AVERAGE" Then
                                r("GPA") = gpal
                            Else
                                r("GPA") = aRow("GPA")
                            End If

                        End If
                    End If
                End If
                '
                'Start Date
                r("StartDate") = aRow("StartDate")
                r("grdSysDetailid") = aRow("grdSysDetailId")
                ''New Code Added By Vijay Ramteke For Rally Id DE1393 On Decemabr 22, 2010 
                r("IsTransferGrade") = aRow("IsTransferGrade")
            End If
        End If

        'IsCreditsEarned, Credits Earned and Hours Completed 
        '       For a course we need to lookup that from the Results datatable in the dataset
        '       if we are dealing with a course
        If dr("ReqTypeId") = 1 Then 'Course

            aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "GPA DESC")

            If aRows.Length > 0 Then 'There are results for this course
                'Check if any of the results (grades) is included in credits earned
                Dim aRow As DataRow = aRows(0)
                If aRow("Descrip").ToString.ToLower.Trim <> dr("Req").ToString.ToLower.Trim Then
                    r("EquivCourseDesc") = aRow("Descrip").ToString.Trim
                    r("Credits") = aRow("Credits").ToString()
                    r("FinAidCredits") = aRow("FinAidCredits").ToString()
                    r("Hours") = aRow("Hours").ToString()
                End If
                isPass = brRegisterStudent.GrdOverRide(dr("PrgVerId").ToString(), dr("reqId").ToString, aRow("Grade").ToString)
                r("IsPass") = isPass
                r("IsCreditsEarned") = aRow("IsCreditsEarned")
                r("grdSysDetailid") = aRow("grdSysDetailId")
                If Not aRow.IsNull("IsInSAP") Then
                    r("IsInSAP") = aRow("IsInSAP")
                Else
                    r("IsInSAP") = False
                End If

                Try
                    Dim iscredit As Boolean = If(aRow("IsCreditsEarned") Is DBNull.Value, False, aRow("IsCreditsEarned"))
                    If iscredit And isPass Then
                        '  If aRow("IsCreditsEarned") And isPass Then
                        ' Call the function to add credits earned
                        Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                        If isHrsComp = True Then
                            r("CreditsEarned") = r("Credits")
                            r("EarnedFinAidCredits") = r("FinAidCredits")
                        Else
                            r("CreditsEarned") = 0
                            r("EarnedFinAidCredits") = 0
                        End If
                        If Not aRow.IsNull("IsInSAP") Then
                            If aRow("IsInSAP") = 1 Or aRow("IsInSAP") = True Then
                                r("CreditsEarnedForSAP") = r("Credits")
                            Else
                                r("CreditsEarnedForSAP") = 0
                            End If
                        Else
                            r("CreditsEarnedForSAP") = 0
                        End If


                    Else
                        r("CreditsEarned") = 0
                        r("EarnedFinAidCredits") = 0
                    End If
                Catch ex As Exception
                    r("CreditsEarned") = 0
                    r("EarnedFinAidCredits") = 0
                End Try
                '
                'Even the student fails the course he/she should still be given full credit for the hours
                'completed. However, if the student drops the course then he/she should not be given any
                'credit for the hours completed.
                Try
                    If aRow.IsNull("IsDrop") Then
                        r("HoursCompleted") = 0
                        Exit Try
                    End If
                    If aRow("IsDrop") Then
                        r("HoursCompleted") = 0
                    Else
                        'Consider variable from the web.config:
                        '   When there is a failed grade and IncludeHoursForFailingGrade=False, HoursCompleted=0. 
                        '   Any other case, account for the hours. 
                        If Not aRow("IsPass") And Not includeHours Then
                            r("HoursCompleted") = 0
                        Else
                            If aRow("IsCreditsEarned") And isPass Then
                                ' ''' ''Call the function to add credits earned
                                Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                                If isHrsComp = True Then
                                    r("HoursCompleted") = r("Hours")
                                Else
                                    r("HoursCompleted") = 0
                                End If
                                '   r("HoursCompleted") = r("Hours")
                            Else
                                r("HoursCompleted") = 0
                            End If
                        End If
                    End If
                Catch ex As Exception
                    r("HoursCompleted") = 0
                End Try
                '
                'To compute CreditsAttempted, ONLY take into account 'Best' or 'Latest' try accordingly for this particular course.
                Dim aRow0 As DataRow = aRows(0)
                'r("CreditsAttempted") = 0 'Commented by BN 10/05/06 to take only best or latest try into account
                If Not aRow.IsNull("Credits") Then
                    If Not aRow0.IsNull("IsCreditsAttempted") Then
                        If aRow0("IsCreditsAttempted") = True Or aRow0("IsCreditsAttempted") = 1 Then
                            r("CreditsAttempted") = aRow0("Credits")
                            r("IsCreditsAttempted") = (r("CreditsAttempted") > 0)
                            r("AllCreditsAttempted") = aRow0("Credits") * aRows.Length
                            If Not aRow0.IsNull("IsInSAP") Then
                                If aRow0("IsInSAP") = True Or aRow0("IsInSAP") = 1 Then
                                    r("CreditsAttemptedForSAP") = aRow0("Credits")
                                    r("AllCreditsAttemptedForSAP") = aRow0("Credits") * aRows.Length
                                Else
                                    r("CreditsAttemptedForSAP") = 0
                                    r("AllCreditsAttemptedForSAP") = 0
                                End If
                            Else
                                r("CreditsAttemptedForSAP") = 0
                                r("AllCreditsAttemptedForSAP") = 0
                            End If

                        Else
                            r("CreditsAttempted") = 0
                            r("IsCreditsAttempted") = 0 '(r("CreditsAttempted") > 0)
                            r("AllCreditsAttempted") = 0
                            r("CreditsAttemptedForSAP") = 0
                            r("AllCreditsAttemptedForSAP") = 0
                        End If
                    Else
                        r("CreditsAttempted") = 0 'aRow0("Credits")
                        r("IsCreditsAttempted") = 0 '(r("CreditsAttempted") > 0)
                        r("AllCreditsAttempted") = 0 'aRow0("Credits") * aRows.Length
                        r("CreditsAttemptedForSAP") = 0
                        r("AllCreditsAttemptedForSAP") = 0
                    End If
                End If

                If Not aRow.IsNull("ScheduledHours") Then
                    If Not aRow0.IsNull("IsCreditsAttempted") Then
                        If aRow0("IsCreditsAttempted") = True Or aRow0("IsCreditsAttempted") = 1 Then
                            r("TotalScheduledHours") = aRow0("ScheduledHours")
                            r("AllScheduledHours") = aRow0("ScheduledHours") * aRows.Length
                        Else
                            r("TotalScheduledHours") = 0.0
                            r("AllScheduledHours") = 0.0
                        End If
                    Else
                        r("TotalScheduledHours") = 0.0
                        r("AllScheduledHours") = 0.0
                    End If
                End If
                '''''''''''''''''''''''''''''''''
                ' No results for this course.
                'check for the equivalent courses

            Else
                r("IsCreditsEarned") = False
                r("IsCreditsAttempted") = False
                r("CreditsEarned") = 0
                r("EarnedFinAidCredits") = 0
                r("HoursCompleted") = 0
                r("CreditsAttempted") = 0
                r("TotalScheduledHours") = 0.0
                r("AllScheduledHours") = 0.0
                ''''''''''''''''
                r("CreditsEarnedForSAP") = 0
                r("CreditsAttemptedForSAP") = 0
                r("IsInSAP") = False
            End If
        End If

        If Not r.IsNull("CreditsEarned") Then
            If r("IsPass") = False And dr("ReqTypeId") = 1 Then
                equivReqid = GetEquivalentReqId(dr("ReqId").ToString).ToString
                If equivReqid <> "" Then
                    aRows = ds.Tables("Results").Select("ReqId = '" & equivReqid.ToString & "'", "GPA DESC")
                    If aRows.Length > 0 Then 'There are results for this course

                        'Check if any of the results (grades) is included in credits earned
                        Dim aRow As DataRow = aRows(0)
                        isPass = brRegisterStudent.GrdOverRide(dr("PrgVerId").ToString(), equivReqid.ToString, aRow("Grade").ToString)
                        Dim eqDescrip As String = dbTranscript.GetCourseDescrip(equivReqid.ToString)
                        'If isPass Then

                        If r.IsNull("Grade") Then
                            r("EquivCourseDesc") = eqDescrip
                            r("Grade") = aRow("Grade")
                            r("Credits") = aRow("Credits").ToString()
                            r("FinAidCredits") = aRow("FinAidCredits").ToString()
                            r("Hours") = aRow("Hours").ToString()
                            r("grdSysDetailid") = aRow("grdSysDetailId")
                            r("IsInSAP") = aRow("IsInSAP")
                        End If
                        'Dim aRow0 As DataRow = aRows(0)
                        If Not aRow.IsNull("Credits") Then
                            If Not aRow.IsNull("IsCreditsAttempted") Then
                                If aRow("IsCreditsAttempted") = True Or aRow("IsCreditsAttempted") = 1 Then
                                    r("CreditsAttempted") = aRow("Credits")
                                    r("IsCreditsAttempted") = (r("CreditsAttempted") > 0)
                                    r("AllCreditsAttempted") = aRow("Credits") * aRows.Length

                                    If aRow("IsInSAP") = True Or aRow("IsInSAP") = 1 Then
                                        r("CreditsAttemptedForSAP") = aRow("Credits")
                                        r("AllCreditsAttemptedForSAP") = aRow("Credits") * aRows.Length
                                    Else
                                        r("CreditsAttemptedForSAP") = 0
                                        r("AllCreditsAttemptedForSAP") = 0
                                    End If
                                Else
                                    r("CreditsAttempted") = 0
                                    r("IsCreditsAttempted") = 0 '(r("CreditsAttempted") > 0)
                                    r("AllCreditsAttempted") = 0
                                    r("CreditsAttemptedForSAP") = 0
                                    r("AllCreditsAttemptedForSAP") = 0
                                End If

                            Else
                                r("CreditsAttempted") = 0 'aRow0("Credits")
                                r("IsCreditsAttempted") = 0 '(r("CreditsAttempted") > 0)
                                r("AllCreditsAttempted") = 0 'aRow0("Credits") * aRows.Length
                                r("CreditsAttemptedForSAP") = 0
                                r("AllCreditsAttemptedForSAP") = 0
                            End If
                        End If
                        If Not aRow.IsNull("IsInGPA") Then
                            If aRow("IsInGPA") = 1 Or aRow("IsInGPA") = True Then
                                If Not aRow.IsNull("GPA") Then
                                    r("GPA") = aRow("GPA")
                                End If
                            End If
                        End If
                        'End If

                        If Not aRow.IsNull("ScheduledHours") Then
                            If Not aRow.IsNull("IsCreditsAttempted") Then
                                If aRow("IsCreditsAttempted") = True Or aRow("IsCreditsAttempted") = 1 Then
                                    r("TotalScheduledHours") = aRow("ScheduledHours")
                                    r("AllScheduledHours") = aRow("ScheduledHours") * aRows.Length
                                Else
                                    r("TotalScheduledHours") = 0.0
                                    r("AllScheduledHours") = 0.0
                                End If
                            Else
                                r("TotalScheduledHours") = 0.0
                                r("AllScheduledHours") = 0.0
                            End If
                        End If
                        '''''''''''''''''''''''''''''''''

                        If isPass Then
                            r("IsCreditsEarned") = aRow("IsCreditsEarned")
                            r("EquivCourseDesc") = eqDescrip
                            r("Grade") = aRow("Grade")
                            r("Credits") = aRow("Credits").ToString()
                            r("FinAidCredits") = aRow("FinAidCredits").ToString()
                            r("Hours") = aRow("Hours").ToString()
                            r("grdSysDetailid") = aRow("grdSysDetailId")
                            Try
                                If aRow("IsCreditsEarned") And isPass Then
                                    ' ''' ''Call the function to add credits earned
                                    Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                                    If isHrsComp = True Then
                                        r("CreditsEarned") = r("Credits")
                                        r("EarnedFinAidCredits") = r("FinAidCredits")
                                    Else
                                        r("CreditsEarned") = 0
                                        r("EarnedFinAidCredits") = 0
                                    End If
                                Else
                                    r("CreditsEarned") = 0
                                    r("EarnedFinAidCredits") = 0
                                End If
                            Catch ex As Exception
                                r("CreditsEarned") = 0
                                r("EarnedFinAidCredits") = 0
                            End Try
                            '
                            'Even the student fails the course he/she should still be given full credit for the hours
                            'completed. However, if the student drops the course then he/she should not be given any
                            'credit for the hours completed.
                            Try
                                If aRow.IsNull("IsDrop") Then
                                    r("HoursCompleted") = 0
                                    Exit Try
                                End If
                                If aRow("IsDrop") Then
                                    r("HoursCompleted") = 0
                                Else
                                    'Consider variable from the web.config:
                                    '   When there is a failed grade and IncludeHoursForFailingGrade=False, HoursCompleted=0. 
                                    '   Any other case, account for the hours. 
                                    If Not aRow("IsPass") And Not includeHours Then
                                        r("HoursCompleted") = 0
                                    Else
                                        ''Modified by saraswathi on May 20 2009
                                        ''To fix issue 16249

                                        If aRow("IsCreditsEarned") And isPass Then
                                            ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                            ' ''' ''Call the function to add credits earned
                                            Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                                            If isHrsComp = True Then
                                                r("HoursCompleted") = r("Hours")
                                            Else
                                                r("HoursCompleted") = 0
                                            End If
                                            'r("HoursCompleted") = r("Hours")
                                        Else
                                            r("HoursCompleted") = 0
                                        End If
                                    End If
                                End If
                            Catch ex As Exception
                                r("HoursCompleted") = 0
                            End Try
                            '
                        End If
                    End If
                End If

            End If
            'Credits Remaining. We can simply use the difference between the credits column 
            '       and the credits earned column.
            If r.IsNull("CreditsEarned") Then
                r("CreditsRemaining") = dr("Credits")
            Else
                r("CreditsRemaining") = r("Credits") - r("CreditsEarned")
            End If


            'Hours Remaining. We can simply use the difference between the hours column 
            '       and the hours completed column.
            If r.IsNull("HoursCompleted") Then
                r("HoursRemaining") = r("Hours")
            Else
                r("HoursRemaining") = r("Hours") - r("HoursCompleted")
            End If
            Dim boolIsCourseALab As Boolean
            If aRows.Length > 0 Then
                boolIsCourseALab = isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                Dim boolClinicSatisfied As Boolean = dbExams.isClinicCourseCompletlySatisfied(dr("StuEnrollId").ToString(), 500, dr("ReqId").ToString)
                If boolIsCourseALab Then
                    Dim boolRemaining As Boolean = dbExams.IsRemainingLabCount(dr("StuEnrollId").ToString(), 500, dr("ReqId").ToString)
                    If boolRemaining = False And boolClinicSatisfied = True Then
                        If isCourseALabWorkOrLabHourCourseCombination(dr("ReqId").ToString) Then
                            If isPass Then
                                r("HoursCompleted") = r("Hours")
                                r("HoursRemaining") = 0
                            Else
                                r("HoursCompleted") = 0
                                r("HoursRemaining") = r("Hours")
                            End If
                        Else
                            r("HoursCompleted") = r("Hours")
                            r("HoursRemaining") = 0
                        End If

                    End If
                End If
            End If

        End If

        '   Add row into datatable
        dt.Rows.Add(r)


        'If we are dealing with a course group then we want to render its children as well.
        'A course group can contain other course groups so we will have to make a recursive
        'call to ensure that all children of the course group is rendered.
        If dr("ReqTypeId") = 2 Then
            'Get the direct children for the group from the GroupChildren dt datatable in
            'the dataset. We will then call RenderRow for each row.
            aRowsGroup = ds.Tables("GroupChildren").Select("GrpId = '" & dr("ReqId").ToString() & "'")
            If aRowsGroup.Length > 0 Then
                Dim aRowGroup As DataRow
                For Each aRowGroup In aRowsGroup
                    AddRowToDt(aRowGroup, ds, dt, dr("ReqId").ToString(), level + 1, gradeReps, includeHours)
                Next
            End If
        End If

    End Sub
    Private Sub AddRowToDT_SP(ByVal dr As DataRow, ByVal ds As DataSet, ByVal dt As DataTable,
                           ByVal parentId As String, ByVal level As Integer, ByVal gradeReps As String, ByVal includeHours As Boolean)
        Dim r As DataRow = dt.NewRow

        Dim aRows() As DataRow
        Dim aRowsGroup() As DataRow
        'Dim iCount As Integer
        'Dim dtReqsForCourseGrp As DataTable
        Dim fac As New TranscriptFacade
        Dim transDB As New TranscriptDB
        'Dim br As New RegisterStudentsBR
        Dim equivReqid As String
        Dim isPass As Boolean = False
        'Course/Course Group/Program Version Description
        r("Descrip") = dr("Req")

        'Requirement Id
        r("ReqId") = dr("ReqId")

        'Course Code
        r("Code") = dr("Code")

        'Requirement Type Id
        r("ReqTypeId") = dr("ReqTypeId")

        'ParentId

        If parentId <> "" Then r("ParentId") = parentId

        'Level
        r("Level") = level

        'Credits
        r("Credits") = dr("Credits").ToString()
        r("FinAidCredits") = dr("FinAidCredits").ToString()
        'Hours
        r("Hours") = dr("Hours").ToString()

        r("PrgVerId") = dr("PrgVerId").ToString()
        r("IsPass") = False


        Dim score As Decimal
        Dim GPA As Decimal

        'Grade, GPA, Start Date
        '       If we are dealing with a course then we will lookup the grade from the Results
        '       datatable in the dataset
        If dr("ReqTypeId") = 1 Then 'Course


            '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
            If gradeReps.ToUpper = "LATEST" Then
                'Latest attempt
                aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "EndDate DESC, StartDate DESC")
            ElseIf gradeReps.ToUpper = "BEST" Then
                'Best GPA
                aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "GPA DESC,IsPass DESC")

            Else

                aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "GPA DESC,IsPass DESC")


                Dim reqId As String = dr("ReqId").ToString()
                GPA = CalculateAverage(ds, "GPA", reqId)
                score = CalculateAverage(ds, "Score", reqId)

            End If
            ''''''''''''''''

            If aRows.Length > 0 Then 'There are results for this course
                'Check if any of the results (grades) is included in credits earned
                Dim aRow As DataRow = aRows(0)
                r("Grade") = aRow("Grade")
                If Not aRow.IsNull("Score") Then
                    'r("Score") = CType(aRow("Score"), Decimal)
                    '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
                    If gradeReps.ToUpper = "AVERAGE" Then
                        r("Score") = score
                    Else
                        r("Score") = CType(aRow("Score"), Decimal)
                    End If
                    ''''''''''''''
                End If
                If aRow("Descrip").ToString.ToLower.Trim <> dr("Req").ToString.ToLower.Trim Then
                    r("EquivCourseDesc") = aRow("Descrip").ToString.Trim

                End If
                '
                If Not aRow.IsNull("IsInGPA") Then
                    If aRow("IsInGPA") = 1 Or aRow("IsInGPA") = True Then
                        If Not aRow.IsNull("GPA") Then
                            'r("GPA") = aRow("GPA")
                            '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
                            If gradeReps.ToUpper = "AVERAGE" Then
                                r("GPA") = GPA
                            Else
                                r("GPA") = aRow("GPA")
                            End If
                            '''''''''''
                        End If
                    End If
                End If
                '
                'Start Date
                r("StartDate") = aRow("StartDate")
                r("grdSysDetailid") = aRow("grdSysDetailId")
                ''New Code Added By Vijay Ramteke For Rally Id DE4346 On January 24, 2011 
                r("IsTransferGrade") = aRow("IsTransferGrade")

            End If
        End If


        'IsCreditsEarned, Credits Earned and Hours Completed 
        '       For a course we need to lookup that from the Results datatable in the dataset
        '       if we are dealing with a course
        If dr("ReqTypeId") = 1 Then 'Course

            aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "GPA DESC")

            If aRows.Length > 0 Then 'There are results for this course
                'Check if any of the results (grades) is included in credits earned
                Dim aRow As DataRow = aRows(0)
                If aRow("Descrip").ToString.ToLower.Trim <> dr("Req").ToString.ToLower.Trim Then
                    r("EquivCourseDesc") = aRow("Descrip").ToString.Trim
                    r("Credits") = aRow("Credits").ToString()
                    r("FinAidCredits") = aRow("FinAidCredits").ToString()
                    r("Hours") = aRow("Hours").ToString()
                End If
                isPass = brRegisterStudent.GrdOverRide_SP(dr("PrgVerId").ToString(), dr("reqId").ToString, aRow("Grade").ToString)
                r("IsPass") = isPass
                r("IsCreditsEarned") = aRow("IsCreditsEarned")
                r("grdSysDetailid") = aRow("grdSysDetailId")
                Try
                    If aRow("IsCreditsEarned") And isPass Then
                        ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                        ' ''' ''Call the function to add credits earned
                        Dim isHrsComp As Boolean = transDB.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                        If isHrsComp = True Then
                            r("CreditsEarned") = r("Credits")
                            r("EarnedFinAidCredits") = r("FinAidCredits")
                        Else
                            r("CreditsEarned") = 0
                            r("EarnedFinAidCredits") = 0
                        End If
                        If Not aRow.IsNull("IsInSAP") Then
                            If aRow("IsInSAP") = 1 Or aRow("IsInSAP") = True Then
                                r("CreditsEarnedForSAP") = r("Credits")
                            Else
                                r("CreditsEarnedForSAP") = 0
                            End If
                        Else
                            r("CreditsEarnedForSAP") = 0
                        End If


                    Else
                        r("CreditsEarned") = 0
                        r("EarnedFinAidCredits") = 0
                    End If
                Catch ex As Exception
                    r("CreditsEarned") = 0
                    r("EarnedFinAidCredits") = 0
                End Try
                '
                'Even the student fails the course he/she should still be given full credit for the hours
                'completed. However, if the student drops the course then he/she should not be given any
                'credit for the hours completed.
                Try
                    If aRow.IsNull("IsDrop") Then
                        r("HoursCompleted") = 0
                        Exit Try
                    End If
                    If aRow("IsDrop") Then
                        r("HoursCompleted") = 0
                    Else
                        'Consider variable from the web.config:
                        '   When there is a failed grade and IncludeHoursForFailingGrade=False, HoursCompleted=0. 
                        '   Any other case, account for the hours. 
                        If Not aRow("IsPass") And Not includeHours Then
                            r("HoursCompleted") = 0
                        Else
                            ''Modified by saraswathi lakshmanan on may 20 2009
                            ''To fix issue 16249
                            If aRow("IsCreditsEarned") And isPass Then
                                ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                ' ''' ''Call the function to add credits earned
                                Dim isHrsComp As Boolean = transDB.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                                If isHrsComp = True Then
                                    r("HoursCompleted") = r("Hours")
                                Else
                                    r("HoursCompleted") = 0
                                End If
                                '   r("HoursCompleted") = r("Hours")
                            Else
                                r("HoursCompleted") = 0
                            End If
                        End If
                    End If
                Catch ex As Exception
                    r("HoursCompleted") = 0
                End Try
                '
                'To compute CreditsAttempted, ONLY take into account 'Best' or 'Latest' try accordingly for this particular course.
                Dim aRow0 As DataRow = aRows(0)
                'r("CreditsAttempted") = 0 'Commented by BN 10/05/06 to take only best or latest try into account
                If Not aRow.IsNull("Credits") Then
                    If Not aRow0.IsNull("IsCreditsAttempted") Then
                        If aRow0("IsCreditsAttempted") = True Or aRow0("IsCreditsAttempted") = 1 Then
                            r("CreditsAttempted") = aRow0("Credits")
                            r("IsCreditsAttempted") = (r("CreditsAttempted") > 0)
                            r("AllCreditsAttempted") = aRow0("Credits") * aRows.Length
                            If Not aRow0.IsNull("IsInSAP") Then
                                If aRow0("IsInSAP") = True Or aRow0("IsInSAP") = 1 Then
                                    r("CreditsAttemptedForSAP") = aRow0("Credits")
                                Else
                                    r("CreditsAttemptedForSAP") = 0
                                End If
                            Else
                                r("CreditsAttemptedForSAP") = 0
                            End If

                        Else
                            r("CreditsAttempted") = 0
                            r("IsCreditsAttempted") = 0 '(r("CreditsAttempted") > 0)
                            r("AllCreditsAttempted") = 0
                            r("CreditsAttemptedForSAP") = 0
                        End If
                    Else
                        r("CreditsAttempted") = 0 'aRow0("Credits")
                        r("IsCreditsAttempted") = 0 '(r("CreditsAttempted") > 0)
                        'If SingletonAppSettings.AppSettings("IncludeCreditsAttemptedForRepeatedCourses").ToLower = "yes" Then
                        r("AllCreditsAttempted") = 0 'aRow0("Credits") * aRows.Length
                        r("CreditsAttemptedForSAP") = 0

                    End If
                End If

                ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
                If Not aRow.IsNull("ScheduledHours") Then
                    If Not aRow0.IsNull("IsCreditsAttempted") Then
                        If aRow0("IsCreditsAttempted") = True Or aRow0("IsCreditsAttempted") = 1 Then
                            r("TotalScheduledHours") = aRow0("ScheduledHours")
                            r("AllScheduledHours") = aRow0("ScheduledHours") * aRows.Length
                        Else
                            r("TotalScheduledHours") = 0.0
                            r("AllScheduledHours") = 0.0
                        End If
                    Else
                        r("TotalScheduledHours") = 0.0
                        r("AllScheduledHours") = 0.0
                    End If
                End If
                '''''''''''''''''''''''''''''''''

                ' No results for this course.
                'check for the equivalent courses

            Else
                r("IsCreditsEarned") = False
                r("IsCreditsAttempted") = False
                r("CreditsEarned") = 0
                r("EarnedFinAidCredits") = 0
                r("HoursCompleted") = 0
                r("CreditsAttempted") = 0
                r("TotalScheduledHours") = 0.0
                r("AllScheduledHours") = 0.0
                ''''''''''''''''
                r("CreditsEarnedForSAP") = 0
                r("CreditsAttemptedForSAP") = 0

            End If
        End If

        If Not r.IsNull("CreditsEarned") Then
            If r("IsPass") = False And dr("ReqTypeId") = 1 Then
                equivReqid = GetEquivalentReqId_SP(dr("ReqId").ToString)
                If equivReqid <> "" Then
                    'tmpRows = dtGraduateAudit.Select("ReqId='" & equivReqid.ToString & "'")
                    aRows = ds.Tables("Results").Select("ReqId = '" & equivReqid.ToString & "'", "GPA DESC")
                    If aRows.Length > 0 Then 'There are results for this course
                        'Check if any of the results (grades) is included in credits earned
                        Dim aRow As DataRow = aRows(0)
                        isPass = brRegisterStudent.GrdOverRide_SP(dr("PrgVerId").ToString(), equivReqid.ToString, aRow("Grade").ToString)
                        Dim eqDescrip As String = transDB.GetCourseDescrip_SP(equivReqid.ToString)
                        If isPass Then

                            If r.IsNull("Grade") Then
                                r("EquivCourseDesc") = eqDescrip
                                r("Grade") = aRow("Grade")
                                r("Credits") = aRow("Credits").ToString()
                                r("FinAidCredits") = aRow("FinAidCredits").ToString()
                                r("Hours") = aRow("Hours").ToString()
                                r("grdSysDetailid") = aRow("grdSysDetailId")
                            End If
                            'Dim aRow0 As DataRow = aRows(0)
                            If Not aRow.IsNull("Credits") Then
                                If Not aRow.IsNull("IsCreditsAttempted") Then
                                    r("CreditsAttempted") = aRow("Credits")
                                    r("IsCreditsAttempted") = (r("CreditsAttempted") > 0)
                                    r("AllCreditsAttempted") = aRow("Credits") * aRows.Length
                                Else
                                    r("CreditsAttempted") = 0 'aRow0("Credits")
                                    r("IsCreditsAttempted") = 0 '(r("CreditsAttempted") > 0)
                                    r("AllCreditsAttempted") = 0 'aRow0("Credits") * aRows.Length
                                End If
                            End If

                            ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
                            If Not aRow.IsNull("ScheduledHours") Then
                                If Not aRow.IsNull("IsCreditsAttempted") Then
                                    If aRow("IsCreditsAttempted") = True Or aRow("IsCreditsAttempted") = 1 Then
                                        r("TotalScheduledHours") = aRow("ScheduledHours")
                                        r("AllScheduledHours") = aRow("ScheduledHours") * aRows.Length
                                    Else
                                        r("TotalScheduledHours") = 0.0
                                        r("AllScheduledHours") = 0.0
                                    End If
                                Else
                                    r("TotalScheduledHours") = 0.0
                                    r("AllScheduledHours") = 0.0
                                End If
                            End If
                            '''''''''''''''''''''''''''''''''

                            If Not aRow.IsNull("IsInGPA") Then
                                If aRow("IsInGPA") = 1 Or aRow("IsInGPA") = True Then
                                    If Not aRow.IsNull("GPA") Then
                                        r("GPA") = aRow("GPA")
                                    End If
                                End If
                            End If
                        End If
                        If isPass Then
                            r("IsCreditsEarned") = aRow("IsCreditsEarned")
                            r("EquivCourseDesc") = eqDescrip
                            r("Grade") = aRow("Grade")
                            r("Credits") = aRow("Credits").ToString()
                            r("FinAidCredits") = aRow("FinAidCredits").ToString()
                            r("Hours") = aRow("Hours").ToString()
                            r("grdSysDetailid") = aRow("grdSysDetailId")
                            'r("Grade") = aRow("Grade")

                            'r("EquivCourseDesc") = transDB.GetCourseDescrip(equivReqid.ToString)
                            Try
                                If aRow("IsCreditsEarned") And isPass Then
                                    ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                    ' ''' ''Call the function to add credits earned
                                    Dim isHrsComp As Boolean = transDB.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                                    If isHrsComp = True Then
                                        r("CreditsEarned") = r("Credits")
                                        r("EarnedFinAidCredits") = r("FinAidCredits")
                                    Else
                                        r("CreditsEarned") = 0
                                        r("EarnedFinAidCredits") = 0
                                    End If
                                Else
                                    r("CreditsEarned") = 0
                                    r("EarnedFinAidCredits") = 0
                                End If
                            Catch ex As Exception
                                r("CreditsEarned") = 0
                                r("EarnedFinAidCredits") = 0
                            End Try
                            '
                            'Even the student fails the course he/she should still be given full credit for the hours
                            'completed. However, if the student drops the course then he/she should not be given any
                            'credit for the hours completed.
                            Try
                                If aRow.IsNull("IsDrop") Then
                                    r("HoursCompleted") = 0
                                    Exit Try
                                End If
                                If aRow("IsDrop") Then
                                    r("HoursCompleted") = 0
                                Else
                                    'Consider variable from the web.config:
                                    '   When there is a failed grade and IncludeHoursForFailingGrade=False, HoursCompleted=0. 
                                    '   Any other case, account for the hours. 
                                    If Not aRow("IsPass") And Not includeHours Then
                                        r("HoursCompleted") = 0
                                    Else
                                        ''Modified by saraswathi on May 20 2009
                                        ''To fix issue 16249

                                        If aRow("IsCreditsEarned") And isPass Then
                                            ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                            ' ''' ''Call the function to add credits earned
                                            Dim isHrsComp As Boolean = transDB.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                                            If isHrsComp = True Then
                                                r("HoursCompleted") = r("Hours")
                                            Else
                                                r("HoursCompleted") = 0
                                            End If
                                            'r("HoursCompleted") = r("Hours")
                                        Else
                                            r("HoursCompleted") = 0
                                        End If
                                    End If
                                End If
                            Catch ex As Exception
                                r("HoursCompleted") = 0
                            End Try
                            '
                            'To compute CreditsAttempted, ONLY take into account 'Best' or 'Latest' try accordingly for this particular course.

                            'r("CreditsAttempted") = 0 'Commented by BN 10/05/06 to take only best or latest try into account

                        End If
                    End If
                End If

            End If
            'Credits Remaining. We can simply use the difference between the credits column 
            '       and the credits earned column.
            If r.IsNull("CreditsEarned") Then
                r("CreditsRemaining") = dr("Credits")
            Else
                r("CreditsRemaining") = r("Credits") - r("CreditsEarned")
            End If


            'Hours Remaining. We can simply use the difference between the hours column 
            '       and the hours completed column.
            If r.IsNull("HoursCompleted") Then
                r("HoursRemaining") = r("Hours")
            Else
                r("HoursRemaining") = r("Hours") - r("HoursCompleted")
            End If
            Dim boolIsCourseALab As Boolean = False
            If aRows.Length > 0 Then
                'boolIsCourseALab = isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                r("IsCourseALab") = aRows(0)("IsCourseALab")
                r("decCredits") = aRows(0)("decCredits")
                boolIsCourseALab = aRows(0)("IsCourseALab")
                If boolIsCourseALab Then
                    Dim boolRemaining As Boolean = (New ExamsFacade).IsRemainingLabCount_SP(dr("StuEnrollId").ToString(), 500, dr("ReqId").ToString)
                    If boolRemaining = False Then
                        Dim boolClinicSatisfied As Boolean = (New ExamsFacade).isClinicCourseCompletlySatisfied_SP(dr("StuEnrollId").ToString(), 500, dr("ReqId").ToString)
                        If boolClinicSatisfied = True Then
                            If isCourseALabWorkOrLabHourCourseCombination_SP(dr("ReqId").ToString) Then
                                If isPass Then
                                    r("HoursCompleted") = r("Hours")
                                    r("HoursRemaining") = 0
                                Else
                                    r("HoursCompleted") = 0
                                    r("HoursRemaining") = r("Hours")
                                End If
                            Else
                                r("HoursCompleted") = r("Hours")
                                r("HoursRemaining") = 0
                            End If
                        End If
                    End If
                End If
            End If

        End If

        '   Add row into datatable
        dt.Rows.Add(r)


        'If we are dealing with a course group then we want to render its children as well.
        'A course group can contain other course groups so we will have to make a recursive
        'call to ensure that all children of the course group is rendered.
        If dr("ReqTypeId") = 2 Then
            'Get the direct children for the group from the GroupChildren dt datatable in
            'the dataset. We will then call RenderRow for each row.
            aRowsGroup = ds.Tables("GroupChildren").Select("GrpId = '" & dr("ReqId").ToString() & "'")
            If aRowsGroup.Length > 0 Then
                Dim aRowGroup As DataRow
                For Each aRowGroup In aRowsGroup
                    AddRowToDT_SP(aRowGroup, ds, dt, dr("ReqId").ToString(), level + 1, gradeReps, includeHours)
                Next
            End If
        End If

    End Sub
    Public Function BuildGraduateAuditDT(ByVal StuEnrollId As String, ByVal gradeReps As String, ByVal includeHours As Boolean, Optional ByVal termEndDate As DateTime = #1/1/1001#, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal campusID As String = "") As DataTable   'DataSet 
        ' New Optional Parameter add termCond and classCond by Vijay Ramteke on May, 06 2009
        Dim ds As DataSet

        'Get PrgVerId from StuEnrollId because when the ds returned is empty (the student do not have results or is continueEducation  )
        Dim PrgVerId As Guid

        If StuEnrollId <> "" Then
            PrgVerId = (New ProgVerDB()).GetPrgVerIdForAGivenStuEnrollId(StuEnrollId)
        End If

        If myAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric" Then
            ds = dbTranscript.GetGraduateAuditByEnrollmentForNumericForCourseEquivalent(StuEnrollId, termEndDate, termCond, classCond, campusID)
        ElseIf dbTranscript.IsContinuingEdPrgVersion(StuEnrollId) Then
            ds = dbTranscript.GetGraduateAuditByCEProgEnrollmentForCourseEquivalent(StuEnrollId, termEndDate, termCond, classCond, campusID)
        Else
            ds = dbTranscript.GetGraduateAuditByEnrollmentForCourseEquivalent(StuEnrollId, termEndDate, termCond, classCond, campusID)
        End If

        Return BuildGraduateAuditDT(ds, PrgVerId, gradeReps, includeHours, campusID)
    End Function
    Private Function BuildGraduateAuditDT(ByVal ds As DataSet, ByVal pPrgVerId As Guid, ByVal gradeReps As String, ByVal includeHours As String, Optional ByVal campusid As String = "") As DataTable
        'Dim br As New RegisterStudentsBR
        Dim dt As New DataTable("GraduateAudit")
        Dim dr As DataRow

        dt.Columns.Add("Descrip", Type.GetType("System.String"))
        dt.Columns.Add("Code", Type.GetType("System.String"))
        dt.Columns.Add("ReqId", Type.GetType("System.String"))
        dt.Columns.Add("StartDate", Type.GetType("System.DateTime"))
        dt.Columns.Add("ReqTypeId", Type.GetType("System.String"))
        dt.Columns.Add("ParentId", Type.GetType("System.String"))
        dt.Columns.Add("Level", Type.GetType("System.Int32"))
        dt.Columns.Add("IsCreditsEarned", Type.GetType("System.Boolean"))
        dt.Columns.Add("Credits", Type.GetType("System.Decimal"))
        dt.Columns.Add("Hours", Type.GetType("System.Decimal"))
        dt.Columns.Add("Grade", Type.GetType("System.String"))
        dt.Columns.Add("Score", Type.GetType("System.Decimal"))
        dt.Columns.Add("GPA", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditsRemaining", Type.GetType("System.Decimal"))
        dt.Columns.Add("HoursCompleted", Type.GetType("System.Decimal"))
        dt.Columns.Add("HoursRemaining", Type.GetType("System.Decimal"))
        dt.Columns.Add("IsCreditsAttempted", Type.GetType("System.Boolean"))
        dt.Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
        dt.Columns.Add("AllCreditsAttempted", Type.GetType("System.Decimal"))
        dt.Columns.Add("IsContinuingEd", Type.GetType("System.Boolean"))
        dt.Columns.Add("IsPass", Type.GetType("System.Boolean"))
        dt.Columns.Add("PrgVerId", Type.GetType("System.String"))
        dt.Columns.Add("IsInGPA", Type.GetType("System.Boolean"))
        dt.Columns.Add("EarnedFinAidCredits", Type.GetType("System.Decimal"))
        dt.Columns.Add("FinAidCredits", Type.GetType("System.Decimal"))
        dt.Columns.Add("EquivCourseDesc", Type.GetType("System.String"))
        dt.Columns.Add("StuEnrollId", Type.GetType("System.String"))
        dt.Columns.Add("GrdSysDetailId", Type.GetType("System.String"))
        dt.Columns.Add("IsCourseALab", Type.GetType("System.Boolean"))
        dt.Columns.Add("decCredits", Type.GetType("System.Decimal"))
        dt.Columns.Add("ScheduledHours", Type.GetType("System.Decimal"))
        dt.Columns.Add("IsTransferGrade", Type.GetType("System.Boolean"))
        dt.Columns.Add("TotalScheduledHours", Type.GetType("System.Decimal"))
        dt.Columns.Add("AllScheduledHours", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditZeroWeight", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditsEarnedForSAP", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditsAttemptedForSAP", Type.GetType("System.Decimal"))
        dt.Columns.Add("AllCreditsAttemptedForSAP", Type.GetType("System.Decimal"))
        dt.Columns.Add("IsInSAP", Type.GetType("System.Boolean"))
        ''''''''''''''''''''''''''''
        Dim checkCredits As String = myAdvAppSettings.AppSettings("CheckCreditsForContiningEd").ToLower()

        If ds.Tables("DirectChildren").Rows.Count > 0 Then
            dr = dt.NewRow

            'First row is for the program version
            dt.Rows.Add(dr)

            'Loop thru the direct children table
            For Each dr In ds.Tables("DirectChildren").Rows
                AddRowToDt(dr, ds, dt, "", 0, gradeReps, includeHours)
            Next

            'The credits required for the program version can be retrieved from the DirectChildren dt in the DataSet
            dt.Rows(0)("Descrip") = ds.Tables("DirectChildren").Rows(0)("PrgVerDescrip").ToString()
            dt.Rows(0)("Credits") = ds.Tables("DirectChildren").Rows(0)("ProgCredits").ToString()
            dt.Rows(0)("Hours") = ds.Tables("DirectChildren").Rows(0)("ProgHours").ToString()
            dt.Rows(0)("IsContinuingEd") = ds.Tables("DirectChildren").Rows(0)("IsContinuingEd")
            dt.Rows(0)("PrgVerId") = ds.Tables("DirectChildren").Rows(0)("PrgVerId")
            dt.Rows(0)("IsPass") = True
            dt.Rows(0)("IsInGPA") = False
            dt.Rows(0)("StuEnrollId") = ds.Tables("DirectChildren").Rows(0)("StuEnrollId")
            dt.Rows(0)("CreditZeroWeight") = 0.0

            For Each dr In ds.Tables("Results").Rows
                If Not (brRegisterStudent.GrdOverRide(dt.Rows(0)("PrgVerId"), dr("reqId").ToString, dr("grade").ToString)) Then
                    Dim dRows() As DataRow = ds.Tables("Results").Select("reqid='" & dr("ReqId").ToString & "'", "startdate desc")
                    If dRows.Length > 1 Then
                        If Not (brRegisterStudent.GrdOverRide(dt.Rows(0)("PrgVerId"), dRows(0)("reqId").ToString, dRows(0)("grade").ToString)) Then
                            dt.Rows(0)("IsPass") = False
                            Exit For
                        End If
                    Else
                        dt.Rows(0)("IsPass") = False
                        Exit For
                    End If
                End If
            Next
            Dim gpaProv As Boolean = False
            For Each dr In ds.Tables("Results").Rows
                Try
                    Dim isgpa As Boolean = If(dr("IsInGPA") Is DBNull.Value, False, dr("IsInGPA"))
                    gpaProv = gpaProv Or isgpa
                    'dt.Rows(0)("IsInGPA") = isgpa
                    'If isgpa = True Then
                    '    dt.Rows(0)("IsInGPA") = True
                    '    Exit For
                    'End If
                Catch ex As Exception
                End Try
            Next
            dt.Rows(0)("IsInGPA") = gpaProv

            For Each dr In ds.Tables("Results").Rows
                Try
                    If dr.IsNull("GrdSysDetailId") Then
                        If dbStuTranscript.GetWeightSum(dr("Reqid").ToString) = 0 Then
                            Dim dtWorkUnit As DataTable
                            dtWorkUnit = dbStuTranscript.GetWorkUnitResults(dt.Rows(0)("StuEnrollId").ToString, dr("ReqId").ToString)
                            If myAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "true" Then
                                If myAdvAppSettings.AppSettings("GradesFormat", campusid).ToLower = "numeric" Then
                                    'rowC("CreditsAttempted") = 0
                                    Dim arrWU() As DataRow
                                    arrWU = dtWorkUnit.Select("Score >= 0")
                                    If dtWorkUnit.Rows.Count > 0 Then
                                        If GetCreditsEarned(dtWorkUnit, dr("Score")) Then
                                            If Not dr.IsNull("IsCreditsEarned") Then
                                                If dr("IsCreditsEarned").ToString = "True" Or dr("IsCreditsEarned").ToString = "1" Then
                                                    dt.Rows(0)("CreditZeroWeight") += dr("Credits")
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                Catch ex As Exception
                    dt.Rows(0)("CreditZeroWeight") = 0.0
                End Try
            Next

            'Nested Course Groups
            'Credits Attempted, Hours Completed, Credits Remaining and Hours Remaining
            Dim maxLevel As Integer = dt.Compute("MAX(Level)", "")
            Dim obj, objSAP, obj1, objSAPAll As Object
            Dim objSchHrs, objSchHrs1 As Object
            Dim objFin As Object
            Dim objSchHours As Object
            Dim arrRows() As DataRow
            'Find out if there are Course Groups in the program version definition
            Dim arrReqGrps() As DataRow = dt.Select("ReqTypeId=2")
            If arrReqGrps.GetLength(0) > 0 Then
                If maxLevel = 0 Then
                    'This section will catch course groups without definitions. In other words, course groups without courses.
                    For Each dr In arrReqGrps
                        dr("CreditsEarned") = 0
                        dr("EarnedFinAidCredits") = 0
                        dr("CreditsAttempted") = 0
                        dr("AllCreditsAttempted") = 0
                        dr("HoursCompleted") = 0
                        dr("CreditsRemaining") = dr("Credits") - dr("CreditsEarned")
                        dr("HoursRemaining") = dr("Hours") - dr("HoursCompleted")
                        dr("CreditsEarnedForSAP") = 0
                        dr("CreditsAttemptedForSAP") = 0
                        dr("AllCreditsAttemptedForSAP") = 0
                    Next
                Else
                    Do While maxLevel > 0
                        For Each dr In arrReqGrps
                            'Find out if there are children of level = maxLevel for current Course Group
                            arrRows = dt.Select("ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel)
                            If arrRows.GetLength(0) > 0 Then
                                obj = dt.Compute("SUM(CreditsEarned)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                                objSAP = dt.Compute("SUM(CreditsEarnedForSAP)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsEarned = 1 OR ReqTypeId = 2) AND (IsInSAP = 1 OR ReqTypeId = 2)")
                                objFin = dt.Compute("SUM(EarnedFinAidCredits)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                                'Added BY Vijay Ramteke on Feb 16, 2010
                                objSchHours = dt.Compute("SUM(TotalScheduledHours)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                                'Added BY Vijay Ramteke on Feb 16, 2010
                                If Not IsDBNull(obj) Then
                                    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                    If Convert.ToDecimal(obj) < dr("Credits") Or checkCredits = "no" Then
                                        dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                        If Not IsDBNull(objSAP) Then
                                            dt.Rows(0)("CreditsEarnedForSAP") = Convert.ToDecimal(objSAP)
                                        End If

                                        dr("CreditsEarned") = Convert.ToDecimal(obj)
                                        If Not IsDBNull(objSAP) Then
                                            dr("CreditsEarnedForSAP") = Convert.ToDecimal(objSAP)
                                        End If

                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean
                                            boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    decCredits = dbTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr1("ReqId").ToString)
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsEarned") += decCredits
                                                End If
                                            End If
                                            If myAdvAppSettings.AppSettings("GradesFormat", campusid).ToLower = "numeric" And dr1.IsNull("GrdSysDetailId") Then
                                                'rowC("CreditsAttempted") = 0
                                                Dim dtWorkUnit As DataTable
                                                dtWorkUnit = (New StuTranscriptDB).GetWorkUnitResults_SP(dt.Rows(0)("StuEnrollId").ToString, dr1("ReqId").ToString)
                                                If dtWorkUnit.Rows.Count > 0 Then
                                                    If GetCreditsEarned(dtWorkUnit, dr1("Score")) Then
                                                        ''Check if iscreditsearned is set , then add credits else donot add
                                                        ''Added on may 13 2009
                                                        If Not dr1.IsNull("IsCreditsEarned") Then
                                                            If dr1("IsCreditsEarned").ToString = "True" Or dr1("IsCreditsEarned").ToString = "1" Then
                                                                dt.Rows(0)("CreditsEarned") += dr1("Credits")
                                                            End If
                                                        End If
                                                    End If
                                                End If

                                            End If
                                        Next
                                        If IsDBNull(objFin) Then
                                            dr("EarnedFinAidCredits") = Convert.ToDecimal(0.0)
                                        Else
                                            dr("EarnedFinAidCredits") = Convert.ToDecimal(objFin)
                                        End If
                                        If IsDBNull(objSchHours) Then
                                            dr("ScheduledHours") = Convert.ToDecimal("0.0")
                                        Else
                                            dr("ScheduledHours") = Convert.ToDecimal(objSchHours)
                                        End If
                                    Else
                                        If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                            dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                            'added by Theresa G on 6/22/09 for this issue,the problem is with course groups
                                            '16562: BUG: The total credits attempted and total credits earned are not adding up correctly since the upgrade to 2.2
                                            dr("CreditsEarned") = Convert.ToDecimal(obj)
                                        Else
                                            dt.Rows(0)("CreditsEarned") = 0
                                        End If
                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean
                                            boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    decCredits = dbTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr1("ReqId").ToString)
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsEarned") += decCredits
                                                End If
                                            End If
                                        Next
                                        dr("EarnedFinAidCredits") = dr("FinAidCredits")
                                        dr("ScheduledHours") = dr("ScheduledHours")
                                    End If
                                Else
                                    dt.Rows(0)("CreditsEarned") = 0
                                    For Each dr1 As DataRow In ds.Tables("Results").Rows
                                        Dim boolIsCourseALab As Boolean
                                        boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal
                                            Try
                                                decCredits = dbTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr1("ReqId").ToString)
                                            Catch ex As Exception
                                                decCredits = 0
                                            End Try
                                            If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                                dt.Rows(0)("CreditsEarned") += decCredits
                                            End If
                                        End If
                                    Next
                                    dr("EarnedFinAidCredits") = 0
                                    dr("ScheduledHours") = dr("ScheduledHours")
                                End If
                                '
                                '

                                ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
                                objSchHrs = dt.Compute("SUM(TotalScheduledHours)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
                                objSchHrs1 = dt.Compute("SUM(AllScheduledHours)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")

                                If Not IsDBNull(objSchHrs) Then
                                    dt.Rows(0)("TotalScheduledHours") = Convert.ToDecimal(objSchHrs)
                                Else
                                    dt.Rows(0)("TotalScheduledHours") = 0.0
                                End If
                                If Not IsDBNull(objSchHrs1) Then
                                    dt.Rows(0)("AllScheduledHours") = Convert.ToDecimal(objSchHrs1)
                                Else
                                    dt.Rows(0)("AllScheduledHours") = 0.0
                                End If

                                '''''''''''

                                obj = dt.Compute("SUM(CreditsAttempted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
                                obj1 = dt.Compute("SUM(AllCreditsAttempted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
                                objSAP = dt.Compute("SUM(CreditsAttemptedForSAP)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsAttempted = 1 OR ReqTypeId = 2) AND (IsInSAP = 1 OR ReqTypeId = 2)")
                                objSAPAll = dt.Compute("SUM(AllCreditsAttemptedForSAP)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsAttempted = 1 OR ReqTypeId = 2) AND (IsInSAP = 1 OR ReqTypeId = 2)")

                                If IsDBNull(objSAP) Then
                                    objSAP = 0.0
                                End If

                                If IsDBNull(objSAPAll) Then
                                    objSAPAll = 0.0
                                End If

                                If Not IsDBNull(obj) Then

                                    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                    If Convert.ToDecimal(obj) < dr("Credits") Or checkCredits = "no" Then
                                        dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
                                        dt.Rows(0)("CreditsAttemptedForSAP") = Convert.ToDecimal(objSAP)
                                        dt.Rows(0)("AllCreditsAttemptedForSAP") = Convert.ToDecimal(objSAPAll)
                                        dr("CreditsAttempted") = Convert.ToDecimal(obj)

                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean
                                            boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    decCredits = dbTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr1("ReqId").ToString)
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsAttempted") += decCredits
                                                End If
                                            End If
                                        Next
                                        dr("CreditsAttempted") = Convert.ToDecimal(obj)
                                        dr("AllCreditsAttempted") = Convert.ToDecimal(obj1)
                                        dr("CreditsAttemptedForSAP") = Convert.ToDecimal(objSAP)
                                        dr("AllCreditsAttemptedForSAP") = Convert.ToDecimal(objSAPAll)
                                    Else

                                        dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
                                        dt.Rows(0)("CreditsAttemptedForSAP") = Convert.ToDecimal(objSAP)
                                        dr("CreditsAttempted") = Convert.ToDecimal(obj)
                                        dr("AllCreditsAttempted") = Convert.ToDecimal(obj1)
                                        dr("CreditsAttemptedForSAP") = Convert.ToDecimal(objSAP)
                                        dr("AllCreditsAttemptedForSAP") = Convert.ToDecimal(objSAPAll)

                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean
                                            boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    decCredits = dbTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr1("ReqId").ToString)
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsAttempted") += decCredits
                                                End If
                                            End If
                                        Next

                                    End If
                                Else

                                    dt.Rows(0)("CreditsAttempted") = 0
                                    dr("AllCreditsAttempted") = 0
                                    dr("CreditsAttemptedForSAP") = 0
                                    dr("AllCreditsAttemptedForSAP") = 0

                                    For Each dr1 As DataRow In ds.Tables("Results").Rows
                                        Dim boolIsCourseALab As Boolean
                                        boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal
                                            Try
                                                decCredits = dbTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr1("ReqId").ToString)
                                            Catch ex As Exception
                                                decCredits = 0
                                            End Try
                                            If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                                dt.Rows(0)("CreditsAttempted") += decCredits
                                            End If
                                        End If
                                    Next


                                End If
                                '
                                '
                                obj = dt.Compute("SUM(HoursCompleted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel)
                                If Not IsDBNull(obj) Then
                                    If Convert.ToDecimal(obj) < dr("Hours") Then
                                        dr("HoursCompleted") = Convert.ToDecimal(obj)
                                    Else
                                        dr("HoursCompleted") = dr("Hours")
                                    End If
                                Else
                                    dr("HoursCompleted") = 0
                                End If
                                '
                                '
                                If Not dr("CreditsEarned") Is DBNull.Value Then
                                    dr("CreditsRemaining") = dr("Credits") - dr("CreditsEarned")
                                Else
                                    dr("CreditsRemaining") = dr("Credits")
                                End If
                                dr("HoursRemaining") = dr("Hours") - dr("HoursCompleted")
                            End If
                        Next
                        maxLevel -= 1
                    Loop
                End If
            End If

            'Total credits earned for the program version (Level = 0).
            'If there are no results we will set credits earned and hours completed to 0.
            arrRows = dt.Select("Level=0")
            If arrRows.GetLength(0) > 0 Then
                'modified by Theresa G on 6/22/09 for this issue,the problem is with course groups does not have any GrdSysdetailid
                '16562: BUG: The total credits attempted and total credits earned are not adding up correctly since the upgrade to 2.2
                'obj = dt.Compute("SUM(CreditsEarned)", "Level=0 AND  GrdSysDetailId is NOT NULL AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                obj = dt.Compute("SUM(CreditsEarned)", "Level=0 AND  (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                objSAP = dt.Compute("SUM(CreditsEarnedForSAP)", "Level=0")
                objFin = dt.Compute("SUM(EarnedFinAidCredits)", "Level=0 AND  (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                'Added By Vijay Ramateke on Feb 16, 2010
                objSchHours = dt.Compute("SUM(ScheduledHours)", "Level=0 AND  (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                'Added By Vijay Ramteke on Feb 16, 2010
                If Not IsDBNull(obj) Then
                    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                    If Not IsDBNull(objSAP) Then
                        dt.Rows(0)("CreditsEarnedForSAP") = Convert.ToDecimal(objSAP)
                    End If

                    If Convert.ToDecimal(obj) < dt.Rows(0)("Credits") Or checkCredits = "no" Then

                        dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                        For Each dr In ds.Tables("Results").Rows
                            Dim boolIsCourseALab As Boolean
                            boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                            If boolIsCourseALab = True Then
                                Dim boolIsCourseACombination As Boolean = dbTransferGrade.isCourseALabWorkOrLabHourCourseCombination(dr("ReqId").ToString)
                                If boolIsCourseACombination = False Then
                                    Dim decCredits As Decimal
                                    Try
                                        decCredits = dbTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
                                    Catch ex As Exception
                                        decCredits = 0
                                    End Try
                                    If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                        dt.Rows(0)("CreditsEarned") += decCredits
                                    End If
                                End If
                            End If
                        Next
                    End If
                    dt.Rows(0)("EarnedFinAidCredits") = Convert.ToDecimal(objFin)
                    'Added By Vijay Ramteke on Feb 16, 2010
                    If Not objSchHours Is DBNull.Value Then
                        dt.Rows(0)("ScheduledHours") = Convert.ToDecimal(objSchHours)
                    Else
                        dt.Rows(0)("ScheduledHours") = Convert.ToDecimal("0.0")
                    End If
                Else
                    dt.Rows(0)("CreditsEarned") = 0
                    ' End If
                    For Each dr In ds.Tables("Results").Rows
                        Dim boolIsCourseALab As Boolean
                        boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                        If boolIsCourseALab = True Then
                            Dim boolIsCourseACombination As Boolean = dbTransferGrade.isCourseALabWorkOrLabHourCourseCombination(dr("ReqId").ToString)
                            If boolIsCourseACombination = False Then
                                Dim decCredits As Decimal
                                Try
                                    decCredits = dbTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
                                Catch ex As Exception
                                    decCredits = 0
                                End Try
                                If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                    dt.Rows(0)("CreditsEarned") += decCredits
                                End If
                            End If
                        End If
                    Next
                    If dt.Rows(0) Is Nothing OrElse IsDBNull(dt.Rows(0)("EarnedFinAidCredits")) Then
                        dt.Rows(0)("EarnedFinAidCredits") = 0
                    Else
                        If Not Decimal.TryParse(objFin, dt.Rows(0)("EarnedFinAidCredits")) Then
                            dt.Rows(0)("EarnedFinAidCredits") = 0
                        End If
                    End If

                    If dt.Rows(0) Is Nothing OrElse IsDBNull(dt.Rows(0)("ScheduledHours")) Then
                        dt.Rows(0)("ScheduledHours") = 0
                    Else
                        If Not Decimal.TryParse(objSchHours, dt.Rows(0)("ScheduledHours")) Then
                            dt.Rows(0)("ScheduledHours") = 0
                        End If
                    End If

                End If
            Else
                dt.Rows(0)("CreditsEarned") = 0
                For Each dr In ds.Tables("Results").Rows
                    Dim boolIsCourseALab As Boolean
                    boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                    If boolIsCourseALab = True Then
                        Dim decCredits As Decimal
                        Try
                            decCredits = dbTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
                        Catch ex As Exception
                            decCredits = 0
                        End Try
                        If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                            dt.Rows(0)("CreditsEarned") += decCredits
                        End If
                    End If
                Next
            End If
            '
            '
            'code modified by Balaji on 06/03/2009 
            obj = dt.Compute("SUM(HoursCompleted)", "Level=0")
            If Not IsDBNull(obj) Then
                'If Convert.ToDecimal(obj) < dt.Rows(0)("Hours") Then
                dt.Rows(0)("HoursCompleted") = Convert.ToDecimal(obj)
                'Else
                '    dt.Rows(0)("HoursCompleted") = dt.Rows(0)("Hours")
                'End If
            Else
                dt.Rows(0)("HoursCompleted") = 0
            End If
            obj = dt.Compute("SUM(CreditsAttempted)", "Level=0 AND  (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            obj1 = dt.Compute("SUM(AllCreditsAttempted)", "Level=0 AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            objSAP = dt.Compute("SUM(CreditsAttemptedForSAP)", "Level=0 AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            objSAPAll = dt.Compute("SUM(AllCreditsAttemptedForSAP)", "Level=0 AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")


            objSchHrs = dt.Compute("SUM(TotalScheduledHours)", "Level=0 AND  (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            objSchHrs1 = dt.Compute("SUM(AllScheduledHours)", "Level=0 AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")

            If Not IsDBNull(objSchHrs) Then
                dt.Rows(0)("TotalScheduledHours") = Convert.ToDecimal(objSchHrs)
            Else
                dt.Rows(0)("TotalScheduledHours") = 0.0
            End If
            If Not IsDBNull(objSchHrs1) Then
                dt.Rows(0)("AllScheduledHours") = Convert.ToDecimal(objSchHrs1)
            Else
                dt.Rows(0)("AllScheduledHours") = 0.0
            End If

            '''''''''''

            If Not IsDBNull(obj) Then

                dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)

                If Not IsDBNull(objSAP) Then
                    dt.Rows(0)("CreditsAttemptedForSAP") = Convert.ToDecimal(objSAP)
                End If

                If Not IsDBNull(objSAPAll) Then
                    dt.Rows(0)("AllCreditsAttemptedForSAP") = Convert.ToDecimal(objSAPAll)
                End If

                For Each dr In ds.Tables("Results").Rows
                    Dim boolIsCourseALab As Boolean
                    boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                    If boolIsCourseALab = True Then
                        Dim boolIsCombination As Boolean
                        boolIsCombination = dbTransferGrade.isCourseALabWorkOrLabHourCourseCombination(dr("ReqId").ToString)
                        Dim decCredits As Decimal
                        If boolIsCombination = False Then 'if course is not a combination get the credits attempted for the course
                            Try
                                decCredits = dbTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
                            Catch ex As Exception
                                decCredits = 0
                            End Try
                            If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                dt.Rows(0)("CreditsAttempted") += decCredits
                            End If
                        End If
                    End If
                Next

                dt.Rows(0)("AllCreditsAttempted") = Convert.ToDecimal(obj1)
            Else

                dt.Rows(0)("CreditsAttempted") = 0
                dt.Rows(0)("AllCreditsAttempted") = 0
                dt.Rows(0)("CreditsAttemptedForSAP") = 0
                dt.Rows(0)("AllCreditsAttemptedForSAP") = 0

                For Each dr In ds.Tables("Results").Rows
                    Dim boolIsCourseALab As Boolean
                    boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                    If boolIsCourseALab = True Then
                        Dim boolIsCombination As Boolean
                        boolIsCombination = dbTransferGrade.isCourseALabWorkOrLabHourCourseCombination(dr("ReqId").ToString)
                        If boolIsCombination = False Then
                            Dim decCredits1 As Decimal
                            Try
                                decCredits1 = dbTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
                            Catch ex As Exception
                                decCredits1 = 0
                            End Try
                            If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                dt.Rows(0)("CreditsAttempted") += decCredits1
                            End If
                        End If
                    End If
                Next


            End If
            '
            '
            'Remaining Credits for the program version
            Try
                If dt.Rows(0)("Credits") > dt.Rows(0)("CreditsEarned") Then
                    dt.Rows(0)("CreditsRemaining") = dt.Rows(0)("Credits") - dt.Rows(0)("CreditsEarned")
                Else
                    dt.Rows(0)("CreditsRemaining") = 0
                End If
            Catch ex As Exception
                dt.Rows(0)("CreditsRemaining") = 0
            End Try

            '
            '
            'Remaining hours for the program version
            Try
                If dt.Rows(0)("Hours") > dt.Rows(0)("HoursCompleted") Then
                    dt.Rows(0)("HoursRemaining") = dt.Rows(0)("Hours") - dt.Rows(0)("HoursCompleted")
                Else
                    dt.Rows(0)("HoursRemaining") = 0
                End If
            Catch ex As Exception
                dt.Rows(0)("HoursRemaining") = 0
            End Try
            dt.Rows(0)("GPA") = ComputeGPA(dt.Select("GPA NOT IS NULL"), pPrgVerId)

        End If

        Return dt
    End Function
    Private Function BuildGraduateAuditDT_SP(ByVal ds As DataSet, ByVal pPrgVerId As Guid, ByVal gradeReps As String, ByVal includeHours As String, Optional ByVal Campusid As String = "") As DataTable
        'Dim br As New RegisterStudentsBR                               
        Dim dt As New DataTable("GraduateAudit")
        Dim dr As DataRow

        dt.Columns.Add("Descrip", Type.GetType("System.String"))
        dt.Columns.Add("Code", Type.GetType("System.String"))
        dt.Columns.Add("ReqId", Type.GetType("System.String"))
        dt.Columns.Add("StartDate", Type.GetType("System.DateTime"))
        dt.Columns.Add("ReqTypeId", Type.GetType("System.String"))
        dt.Columns.Add("ParentId", Type.GetType("System.String"))
        dt.Columns.Add("Level", Type.GetType("System.Int32"))
        dt.Columns.Add("IsCreditsEarned", Type.GetType("System.Boolean"))
        dt.Columns.Add("Credits", Type.GetType("System.Decimal"))
        dt.Columns.Add("Hours", Type.GetType("System.Decimal"))
        dt.Columns.Add("Grade", Type.GetType("System.String"))
        dt.Columns.Add("Score", Type.GetType("System.Decimal"))
        dt.Columns.Add("GPA", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditsRemaining", Type.GetType("System.Decimal"))
        dt.Columns.Add("HoursCompleted", Type.GetType("System.Decimal"))
        dt.Columns.Add("HoursRemaining", Type.GetType("System.Decimal"))
        dt.Columns.Add("IsCreditsAttempted", Type.GetType("System.Boolean"))
        dt.Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
        dt.Columns.Add("AllCreditsAttempted", Type.GetType("System.Decimal"))
        dt.Columns.Add("IsContinuingEd", Type.GetType("System.Boolean"))
        dt.Columns.Add("IsPass", Type.GetType("System.Boolean"))
        dt.Columns.Add("PrgVerId", Type.GetType("System.String"))
        dt.Columns.Add("IsInGPA", Type.GetType("System.Boolean"))
        dt.Columns.Add("EarnedFinAidCredits", Type.GetType("System.Decimal"))
        dt.Columns.Add("FinAidCredits", Type.GetType("System.Decimal"))
        dt.Columns.Add("EquivCourseDesc", Type.GetType("System.String"))
        dt.Columns.Add("StuEnrollId", Type.GetType("System.String"))
        dt.Columns.Add("GrdSysDetailId", Type.GetType("System.String"))
        dt.Columns.Add("IsCourseALab", Type.GetType("System.Boolean"))
        dt.Columns.Add("decCredits", Type.GetType("System.Decimal"))
        dt.Columns.Add("ScheduledHours", Type.GetType("System.Decimal"))
        dt.Columns.Add("IsTransferGrade", Type.GetType("System.Boolean"))
        dt.Columns.Add("TotalScheduledHours", Type.GetType("System.Decimal"))
        dt.Columns.Add("AllScheduledHours", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditZeroWeight", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditsEarnedForSAP", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditsAttemptedForSAP", Type.GetType("System.Decimal"))
        dt.Columns.Add("IsInSAP", Type.GetType("System.Boolean"))
        ''''''''''''''''''''''''''''

        Dim checkCredits As String = myAdvAppSettings.AppSettings("CheckCreditsForContiningEd").ToLower()

        If ds.Tables("DirectChildren").Rows.Count > 0 Then
            dr = dt.NewRow

            'First row is for the program version
            dt.Rows.Add(dr)

            'Loop thru the direct children table
            For Each dr In ds.Tables("DirectChildren").Rows
                AddRowToDT_SP(dr, ds, dt, "", 0, gradeReps, includeHours)
            Next

            'The credits required for the program version can be retrieved from the DirectChildren dt in the DataSet
            dt.Rows(0)("Descrip") = ds.Tables("DirectChildren").Rows(0)("PrgVerDescrip").ToString()
            dt.Rows(0)("Credits") = ds.Tables("DirectChildren").Rows(0)("ProgCredits").ToString()
            dt.Rows(0)("Hours") = ds.Tables("DirectChildren").Rows(0)("ProgHours").ToString()
            dt.Rows(0)("IsContinuingEd") = ds.Tables("DirectChildren").Rows(0)("IsContinuingEd")
            dt.Rows(0)("PrgVerId") = ds.Tables("DirectChildren").Rows(0)("PrgVerId")
            dt.Rows(0)("IsPass") = True
            dt.Rows(0)("IsInGPA") = False
            dt.Rows(0)("StuEnrollId") = ds.Tables("DirectChildren").Rows(0)("StuEnrollId")
            dt.Rows(0)("CreditZeroWeight") = 0.0

            For Each dr In ds.Tables("Results").Rows
                If Not (brRegisterStudent.GrdOverRide_SP(dt.Rows(0)("PrgVerId"), dr("reqId").ToString, dr("grade").ToString)) Then
                    Dim dRows() As DataRow = ds.Tables("Results").Select("reqid='" & dr("ReqId").ToString & "'", "startdate desc")
                    If dRows.Length > 1 Then
                        If Not (brRegisterStudent.GrdOverRide_SP(dt.Rows(0)("PrgVerId"), dRows(0)("reqId").ToString, dRows(0)("grade").ToString)) Then
                            dt.Rows(0)("IsPass") = False
                            Exit For
                        End If
                    Else
                        dt.Rows(0)("IsPass") = False
                        Exit For
                    End If
                End If
            Next
            For Each dr In ds.Tables("Results").Rows
                Try
                    If dr("IsInGPA") = True Then
                        dt.Rows(0)("IsInGPA") = True
                        Exit For
                    End If
                Catch ex As Exception
                    dt.Rows(0)("IsInGPA") = False
                End Try
            Next
            For Each dr In ds.Tables("Results").Rows
                Try
                    If dr.IsNull("GrdSysDetailId") Then
                        If (New StuTranscriptDB).GetWeightSum(dr("Reqid").ToString) = 0 Then
                            Dim dtWorkUnit As DataTable
                            dtWorkUnit = (New StuTranscriptDB).GetWorkUnitResults(dt.Rows(0)("StuEnrollId").ToString, dr("ReqId").ToString)
                            If myAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "true" Then
                                If myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToLower = "numeric" Then
                                    'rowC("CreditsAttempted") = 0
                                    Dim arrWU() As DataRow
                                    arrWU = dtWorkUnit.Select("Score >= 0")
                                    If dtWorkUnit.Rows.Count > 0 Then
                                        If GetCreditsEarned(dtWorkUnit, dr("Score")) Then
                                            If Not dr.IsNull("IsCreditsEarned") Then
                                                If dr("IsCreditsEarned").ToString = "True" Or dr("IsCreditsEarned").ToString = "1" Then
                                                    dt.Rows(0)("CreditZeroWeight") += dr("Credits")
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                Catch ex As Exception
                    dt.Rows(0)("CreditZeroWeight") = 0.0
                End Try
            Next
            'Nested Course Groups
            'Credits Attempted, Hours Completed, Credits Remaining and Hours Remaining
            Dim maxLevel As Integer = dt.Compute("MAX(Level)", "")
            Dim obj, obj1 As Object
            Dim objSchHrs, objSchHrs1 As Object
            Dim objFin As Object
            Dim objSchHours As Object
            Dim arrRows() As DataRow
            'Find out if there are Course Groups in the program version definition
            Dim arrReqGrps() As DataRow = dt.Select("ReqTypeId=2")
            If arrReqGrps.GetLength(0) > 0 Then
                If maxLevel = 0 Then
                    'This section will catch course groups without definitions. In other words, course groups without courses.
                    For Each dr In arrReqGrps
                        dr("CreditsEarned") = 0
                        dr("EarnedFinAidCredits") = 0
                        dr("CreditsAttempted") = 0
                        dr("AllCreditsAttempted") = 0
                        dr("HoursCompleted") = 0
                        dr("CreditsRemaining") = dr("Credits") - dr("CreditsEarned")
                        dr("HoursRemaining") = dr("Hours") - dr("HoursCompleted")
                        dr("CreditsEarnedForSAP") = 0
                        dr("CreditsAttemptedForSAP") = 0
                    Next
                Else
                    Do While maxLevel > 0
                        For Each dr In arrReqGrps
                            Dim strGrp As String
                            strGrp = dr("Descrip")
                            'Find out if there are children of level = maxLevel for current Course Group
                            arrRows = dt.Select("ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel)
                            If arrRows.GetLength(0) > 0 Then
                                obj = dt.Compute("SUM(CreditsEarned)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                                objFin = dt.Compute("SUM(EarnedFinAidCredits)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                                'Added BY Vijay Ramteke on Feb 16, 2010
                                objSchHours = dt.Compute("SUM(ScheduledHours)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                                'Added BY Vijay Ramteke on Feb 16, 2010
                                If Not IsDBNull(obj) Then
                                    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                    If Convert.ToDecimal(obj) < dr("Credits") Or checkCredits = "no" Then
                                        dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                        dr("CreditsEarned") = Convert.ToDecimal(obj)

                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean
                                            boolIsCourseALab = dr1("IsCourseALab")
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    decCredits = dr1("decCredits")
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsEarned") += decCredits
                                                End If
                                            End If
                                            If myAdvAppSettings.AppSettings("GradesFormat", Campusid).ToLower = "numeric" And dr1.IsNull("GrdSysDetailId") Then
                                                'rowC("CreditsAttempted") = 0
                                                Dim dtWorkUnit As DataTable
                                                dtWorkUnit = (New StuTranscriptDB).GetWorkUnitResults_SP(dt.Rows(0)("StuEnrollId").ToString, dr1("ReqId").ToString)
                                                If dtWorkUnit.Rows.Count > 0 Then
                                                    If GetCreditsEarned(dtWorkUnit, dr1("Score")) Then
                                                        If Not dr1.IsNull("IsCreditsEarned") Then
                                                            If dr1("IsCreditsEarned").ToString = "True" Or dr1("IsCreditsEarned").ToString = "1" Then
                                                                dt.Rows(0)("CreditsEarned") += dr1("Credits")
                                                            End If
                                                        End If
                                                    End If
                                                End If

                                            End If
                                        Next
                                        dr("EarnedFinAidCredits") = Convert.ToDecimal(objFin)
                                        dr("ScheduledHours") = 0.0
                                        If IsDBNull(objSchHours) = False Then
                                            If Decimal.TryParse(objSchHours, dr("ScheduledHours")) = False Then
                                                dr("ScheduledHours") = 0.0
                                            End If
                                        End If
                                    Else
                                        If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                            dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                            'added by Theresa G on 6/22/09 for this issue,the problem is with course groups
                                            '16562: BUG: The total credits attempted and total credits earned are not adding up correctly since the upgrade to 2.2
                                            dr("CreditsEarned") = Convert.ToDecimal(obj)
                                        Else
                                            dt.Rows(0)("CreditsEarned") = 0
                                        End If
                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean = False
                                            ' boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                            boolIsCourseALab = dr1("IsCourseALab")
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal = 0.0
                                                If IsDBNull(dr1("decCredits")) = False Then
                                                    If Decimal.TryParse(dr1("decCredits"), decCredits) = False Then
                                                        decCredits = 0.0
                                                    End If
                                                End If

                                                If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsEarned") += decCredits
                                                End If
                                            End If
                                        Next
                                        dr("EarnedFinAidCredits") = dr("FinAidCredits")
                                        dr("ScheduledHours") = dr("ScheduledHours")
                                    End If
                                Else
                                    dt.Rows(0)("CreditsEarned") = 0
                                    For Each dr1 As DataRow In ds.Tables("Results").Rows
                                        Dim boolIsCourseALab As Boolean
                                        boolIsCourseALab = dr1("IsCourseALab")
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal = 0.0
                                            If Decimal.TryParse(dr1("decCredits"), decCredits) = False Then
                                                decCredits = 0.0
                                            End If
                                            If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                                dt.Rows(0)("CreditsEarned") += decCredits
                                            End If
                                        End If
                                    Next
                                    dr("EarnedFinAidCredits") = 0
                                    dr("ScheduledHours") = dr("ScheduledHours")
                                End If
                                objSchHrs = dt.Compute("SUM(TotalScheduledHours)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
                                objSchHrs1 = dt.Compute("SUM(AllScheduledHours)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")

                                If Not IsDBNull(objSchHrs) Then
                                    dt.Rows(0)("TotalScheduledHours") = Convert.ToDecimal(objSchHrs)
                                Else
                                    dt.Rows(0)("TotalScheduledHours") = 0.0
                                End If
                                If Not IsDBNull(objSchHrs1) Then
                                    dt.Rows(0)("AllScheduledHours") = Convert.ToDecimal(objSchHrs1)
                                Else
                                    dt.Rows(0)("AllScheduledHours") = 0.0
                                End If

                                '''''''''''

                                obj = dt.Compute("SUM(CreditsAttempted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
                                obj1 = dt.Compute("SUM(AllCreditsAttempted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
                                If Not IsDBNull(obj) Then
                                    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                    If Convert.ToDecimal(obj) < dr("Credits") Or checkCredits = "no" Then

                                        dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
                                        dr("CreditsAttempted") = Convert.ToDecimal(obj)

                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean
                                            boolIsCourseALab = dr1("IsCourseALab")
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    decCredits = dr1("decCredits")
                                                Catch ex As Exception
                                                    decCredits = 0

                                                End Try
                                                If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsAttempted") += decCredits
                                                End If
                                            End If
                                        Next
                                        dr("CreditsAttempted") = Convert.ToDecimal(obj)
                                        dr("AllCreditsAttempted") = Convert.ToDecimal(obj1)

                                    Else
                                        dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
                                        dr("CreditsAttempted") = Convert.ToDecimal(obj)

                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean
                                            boolIsCourseALab = dr1("IsCourseALab")
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    decCredits = dr1("decCredits")
                                                Catch ex As Exception
                                                    decCredits = 0

                                                End Try
                                                If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsAttempted") += decCredits
                                                End If
                                            End If
                                        Next
                                        dr("AllCreditsAttempted") = Convert.ToDecimal(obj1)
                                    End If
                                Else

                                    dt.Rows(0)("CreditsAttempted") = 0
                                    For Each dr1 As DataRow In ds.Tables("Results").Rows
                                        Dim boolIsCourseALab As Boolean
                                        boolIsCourseALab = dr1("IsCourseALab")
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal
                                            Try
                                                decCredits = dr1("decCredits")
                                            Catch ex As Exception
                                                decCredits = 0

                                            End Try
                                            If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                                dt.Rows(0)("CreditsAttempted") += decCredits
                                            End If
                                        End If
                                    Next
                                    dr("AllCreditsAttempted") = 0
                                End If
                                '
                                '
                                obj = dt.Compute("SUM(HoursCompleted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel)
                                If Not IsDBNull(obj) Then
                                    If Convert.ToDecimal(obj) < dr("Hours") Then
                                        dr("HoursCompleted") = Convert.ToDecimal(obj)
                                    Else
                                        dr("HoursCompleted") = dr("Hours")
                                    End If
                                Else
                                    dr("HoursCompleted") = 0
                                End If
                                '
                                '
                                If Not dr("CreditsEarned") Is DBNull.Value Then
                                    dr("CreditsRemaining") = dr("Credits") - dr("CreditsEarned")
                                Else
                                    dr("CreditsRemaining") = dr("Credits")
                                End If
                                dr("HoursRemaining") = dr("Hours") - dr("HoursCompleted")
                            End If
                        Next
                        maxLevel -= 1
                    Loop
                End If
            End If

            'Total credits earned for the program version (Level = 0).
            'If there are no results we will set credits earned and hours completed to 0.
            arrRows = dt.Select("Level=0")
            If arrRows.GetLength(0) > 0 Then
                obj = dt.Compute("SUM(CreditsEarned)", "Level=0 AND  (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                objFin = dt.Compute("SUM(EarnedFinAidCredits)", "Level=0 AND  (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                objSchHours = dt.Compute("SUM(ScheduledHours)", "Level=0 AND  (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                If Not IsDBNull(obj) Then
                    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                    If Convert.ToDecimal(obj) < dt.Rows(0)("Credits") Or checkCredits = "no" Then

                        dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)

                        For Each dr In ds.Tables("Results").Rows
                            Dim boolIsCourseALab As Boolean
                            boolIsCourseALab = dr("IsCourseALab")
                            If boolIsCourseALab = True Then
                                Dim boolIsCourseACombination As Boolean = dbTransferGrade.isCourseALabWorkOrLabHourCourseCombination_SP(dr("ReqId").ToString)
                                If boolIsCourseACombination = False Then
                                    Dim decCredits As Decimal
                                    Try
                                        decCredits = dr("decCredits")
                                    Catch ex As Exception
                                        decCredits = 0

                                    End Try
                                    If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                        dt.Rows(0)("CreditsEarned") += decCredits
                                    End If
                                End If
                            End If
                        Next
                    End If
                    dt.Rows(0)("EarnedFinAidCredits") = Convert.ToDecimal(objFin)
                    'Added By Vijay Ramteke on Feb 16, 2010
                    If Not objSchHours Is DBNull.Value Then
                        dt.Rows(0)("ScheduledHours") = Convert.ToDecimal(objSchHours)
                    Else
                        dt.Rows(0)("ScheduledHours") = Convert.ToDecimal("0.0")
                    End If

                Else

                    dt.Rows(0)("CreditsEarned") = 0
                    For Each dr In ds.Tables("Results").Rows
                        Dim boolIsCourseALab As Boolean
                        boolIsCourseALab = dr("IsCourseALab")
                        If boolIsCourseALab = True Then
                            Dim boolIsCourseACombination As Boolean = dbTransferGrade.isCourseALabWorkOrLabHourCourseCombination_SP(dr("ReqId").ToString)
                            If boolIsCourseACombination = False Then
                                Dim decCredits As Decimal
                                Try
                                    decCredits = dr("decCredits")
                                Catch ex As Exception
                                    decCredits = 0

                                End Try
                                If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                    dt.Rows(0)("CreditsEarned") += decCredits
                                End If
                            End If
                        End If
                    Next
                    Try
                        dt.Rows(0)("EarnedFinAidCredits") = Convert.ToDecimal(objFin)
                    Catch ex As Exception
                        dt.Rows(0)("EarnedFinAidCredits") = 0
                    End Try
                    Try
                        dt.Rows(0)("ScheduledHours") = Convert.ToDecimal(objSchHours)
                    Catch ex As Exception
                        dt.Rows(0)("ScheduledHours") = 0
                    End Try
                End If
            Else
                dt.Rows(0)("CreditsEarned") = 0
                For Each dr In ds.Tables("Results").Rows
                    Dim boolIsCourseALab As Boolean
                    boolIsCourseALab = dr("IsCourseALab")
                    If boolIsCourseALab = True Then
                        Dim decCredits As Decimal
                        Try
                            decCredits = dr("decCredits")
                        Catch ex As Exception
                            decCredits = 0

                        End Try
                        If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                            dt.Rows(0)("CreditsEarned") += decCredits
                        End If
                    End If
                Next
            End If
            '
            '
            obj = dt.Compute("SUM(HoursCompleted)", "Level=0")
            If Not IsDBNull(obj) Then
                dt.Rows(0)("HoursCompleted") = Convert.ToDecimal(obj)
            Else
                dt.Rows(0)("HoursCompleted") = 0
            End If

            objSchHrs = dt.Compute("SUM(TotalScheduledHours)", "Level=0 AND  (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            objSchHrs1 = dt.Compute("SUM(AllScheduledHours)", "Level=0 AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")

            If Not IsDBNull(objSchHrs) Then
                dt.Rows(0)("TotalScheduledHours") = Convert.ToDecimal(objSchHrs)
            Else
                dt.Rows(0)("TotalScheduledHours") = 0.0
            End If
            If Not IsDBNull(objSchHrs1) Then
                dt.Rows(0)("AllScheduledHours") = Convert.ToDecimal(objSchHrs1)
            Else
                dt.Rows(0)("AllScheduledHours") = 0.0
            End If

            '''''''''''

            'modified by Theresa G on 6/22/09 for this issue,the problem is with course groups does not have any GrdSysdetailid
            '16562: BUG: The total credits attempted and total credits earned are not adding up correctly since the upgrade to 2.2
            'obj = dt.Compute("SUM(CreditsAttempted)", "Level=0 AND GrdSysDetailId is NOT NULL AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            obj = dt.Compute("SUM(CreditsAttempted)", "Level=0 AND  (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            obj1 = dt.Compute("SUM(AllCreditsAttempted)", "Level=0 AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            If Not IsDBNull(obj) Then

                dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)

                For Each dr In ds.Tables("Results").Rows
                    Dim boolIsCourseALab As Boolean
                    boolIsCourseALab = dr("IsCourseALab")
                    If boolIsCourseALab = True Then
                        Dim boolIsCombination As Boolean
                        boolIsCombination = dbTransferGrade.isCourseALabWorkOrLabHourCourseCombination_SP(dr("ReqId").ToString)
                        Dim decCredits As Decimal
                        If boolIsCombination = False Then 'if course is not a combination get the credits attempted for the course
                            Try
                                decCredits = dr("decCredits")
                            Catch ex As Exception
                                decCredits = 0

                            End Try
                            If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                dt.Rows(0)("CreditsAttempted") += decCredits
                            End If
                        End If
                    End If
                Next
                dt.Rows(0)("AllCreditsAttempted") = Convert.ToDecimal(obj1)
            Else

                dt.Rows(0)("CreditsAttempted") = 0
                For Each dr In ds.Tables("Results").Rows
                    Dim boolIsCourseALab As Boolean
                    boolIsCourseALab = dr("IsCourseALab")
                    If boolIsCourseALab = True Then
                        Dim boolIsCombination As Boolean
                        boolIsCombination = dbTransferGrade.isCourseALabWorkOrLabHourCourseCombination_SP(dr("ReqId").ToString)
                        'Dim decCredits As Decimal = 0.0
                        If boolIsCombination = False Then
                            Dim decCredits1 As Decimal
                            Try
                                decCredits1 = dr("decCredits")
                            Catch ex As Exception
                                decCredits1 = 0

                            End Try
                            If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                dt.Rows(0)("CreditsAttempted") += decCredits1
                            End If
                        End If
                    End If
                Next
                dt.Rows(0)("AllCreditsAttempted") = 0
            End If
            '
            '
            'Remaining Credits for the program version
            Try
                If dt.Rows(0)("Credits") > dt.Rows(0)("CreditsEarned") Then
                    dt.Rows(0)("CreditsRemaining") = dt.Rows(0)("Credits") - dt.Rows(0)("CreditsEarned")
                Else
                    dt.Rows(0)("CreditsRemaining") = 0
                End If
            Catch ex As Exception
                dt.Rows(0)("CreditsRemaining") = 0
            End Try

            '
            '
            'Remaining hours for the program version
            Try
                If dt.Rows(0)("Hours") > dt.Rows(0)("HoursCompleted") Then
                    dt.Rows(0)("HoursRemaining") = dt.Rows(0)("Hours") - dt.Rows(0)("HoursCompleted")
                Else
                    dt.Rows(0)("HoursRemaining") = 0
                End If
            Catch ex As Exception
                dt.Rows(0)("HoursRemaining") = 0
            End Try

            dt.Rows(0)("GPA") = ComputeGPA(dt.Select("GPA NOT IS NULL"), pPrgVerId)

        End If
        'End If

        Return dt
    End Function
    Private Function RetakenPolicyFilter(dtIn As DataTable, gradeReps As String) As DataTable
        Dim dtOut As New DataTable
        Dim dRow As DataRow
        Dim aRow As DataRow
        Dim outRow As DataRow
        Dim hTable As New Hashtable
        Dim reqId As String
        Dim gpaAve As Decimal

        dtOut = dtIn.Clone 'same stucture

        For Each dRow In dtIn.Rows
            reqId = dRow("ReqId").ToString()
            If Not (hTable.Contains(reqId)) Then
                hTable.Add(reqId, String.Empty)

                If gradeReps.ToUpper() = "LATEST" Then
                    'Latest attempt
                    aRow = dtIn.Select("ReqId = '" & reqId & "'", "EndDate DESC, StartDate DESC").First()
                    dtOut.ImportRow(aRow)
                ElseIf gradeReps.ToUpper() = "BEST" Then
                    'Best GPA
                    aRow = dtIn.Select("ReqId = '" & reqId & "'", "GPA DESC,IsPass DESC").First()
                    dtOut.ImportRow(aRow)
                ElseIf gradeReps.ToUpper = "AVERAGE" Then
                    If (dtIn.Compute("Avg(GPA)", "ReqId = '" & reqId & "'")) Is DBNull.Value Then
                        gpaAve = 0.0
                    Else
                        gpaAve = dtIn.Compute("Avg(GPA)", "ReqId = '" & reqId & "'")
                    End If
                    'for GPA calculation only count one course of retaken 
                    aRow = dtIn.Select("ReqId = '" & reqId & "'").First()
                    outRow = dtOut.NewRow
                    outRow.ItemArray = aRow.ItemArray
                    outRow("GPA") = gpaAve
                    dtOut.Rows.Add(outRow)

                    'For Each aRow In dtIn.Select("ReqId = '" & reqId & "'")
                    '    aRow("GPA") = gpaAve
                    '    dtOut.ImportRow(aRow)
                    'Next
                Else
                    'Best GPA
                    aRow = dtIn.Select("ReqId = '" & reqId & "'", "GPA DESC,IsPass DESC").First()
                    dtOut.ImportRow(aRow)
                End If
            End If
        Next

        Return dtOut
    End Function
    Private Sub AddCoursesToCummProgress(ByRef dtProgress As DataTable, ByVal arTermCourses() As DataRow)
        Dim row As DataRow
        Dim arRows() As DataRow

        'First time = first attended term
        For Each dr As DataRow In arTermCourses
            row = dtProgress.NewRow
            row.ItemArray = dr.ItemArray
            'row("ReqId") = dr("ReqId")
            'row("Credits") = dr("Credits")
            'row("IsCreditsEarned") = dr("IsCreditsEarned")
            'row("IsCreditsAttempted") = dr("IsCreditsAttempted")
            'If row("IsCreditsEarned") Then
            '    row("CreditsEarned") = dr("Credits")
            'Else
            '    row("CreditsEarned") = 0.0
            'End If
            'If row("IsCreditsAttempted") Then
            '    row("CreditsAttempted") = dr("Credits")
            'Else
            '    row("CreditsAttempted") = 0.0
            'End If
            'row("GPA") = dr("GPA")
            'row("Score") = dr("Score")
            dtProgress.Rows.Add(row)
        Next
    End Sub
    Private Sub AddCoursesToCummProgress_SP(ByRef dtProgress As DataTable, ByVal arTermCourses() As DataRow, ByVal gradeReps As String)
        Dim row As DataRow
        Dim arRows() As DataRow

        If dtProgress.Rows.Count = 0 Then
            'First time = first attended term
            For Each dr As DataRow In arTermCourses
                row = dtProgress.NewRow
                row("ReqId") = dr("ReqId")
                row("Credits") = dr("Credits")
                row("IsCreditsEarned") = dr("IsCreditsEarned")
                row("IsCreditsAttempted") = dr("IsCreditsAttempted")
                If row("IsCreditsEarned") Then
                    row("CreditsEarned") = dr("Credits")
                Else
                    row("CreditsEarned") = 0.0
                End If
                If row("IsCreditsAttempted") Then
                    row("CreditsAttempted") = dr("Credits")
                Else
                    row("CreditsAttempted") = 0.0
                End If
                row("GPA") = dr("GPA")
                row("Score") = dr("Score")
                dtProgress.Rows.Add(row)
            Next

        Else
            'Check if course has already been taken. 
            'If course was already taken, increment credits attempted and use grade based on a 
            'variable from the web.config
            'If not, add it to CummProgress table.
            For Each dr As DataRow In arTermCourses
                arRows = dtProgress.Select("ReqId='" & dr("ReqId").ToString & "'")
                If arRows.GetLength(0) > 0 Then
                    'Course repetition
                    'Course was alredy taken
                    row = arRows(0)
                    row("Score") = dr("Score")
                    If gradeReps.ToUpper = "LATEST" Then
                        'LATEST grade
                        row("GPA") = dr("GPA")
                    ElseIf gradeReps.ToUpper = "BEST" Then
                        'BEST grade
                        If Not (dr.IsNull("GPA")) Then
                            If Not (row.IsNull("GPA")) Then
                                If dr("GPA") > row("GPA") Then
                                    row("GPA") = dr("GPA")
                                    'Troy:10/27/2006 The current requirement is that we have to allow
                                    'for situations where a student has a passing grade (for eg. C) but
                                    'retakes the course to get a better grade such as an "A". This means
                                    'that the row("IsCreditsEarned") is already set to True in those cases.
                                    row("IsCreditsEarned") = True
                                    row("CreditsEarned") += dr("Credits")

                                Else
                                    'Do nothing because row("GPA") >= dr("GPA")
                                End If
                            Else
                                'row("GPA") is Null
                                row("GPA") = dr("GPA")
                                row("IsCreditsEarned") = True
                                row("CreditsEarned") += dr("Credits")
                            End If
                        End If
                    Else
                        'Average grade
                        Dim avggpa As Decimal
                        For Each dr1 As DataRow In arRows
                            Try
                                avggpa += CType(dr1("GPA"), Decimal)
                            Catch ex As Exception
                                avggpa += 0
                            End Try

                        Next
                        Try
                            avggpa += CType(dr("GPA"), Decimal)
                        Catch ex As Exception
                            avggpa += 0
                        End Try
                        avggpa = avggpa / (arRows.Length + 1)
                        row("GPA") = avggpa
                        row("IsCreditsEarned") = True
                        row("CreditsEarned") += dr("Credits")
                    End If

                    'CreditsEarned: Do nothing
                    'Troy:10/27/2006:We actually need to inlcude it in credits earned since the failed
                    'grade would not have been included in the credits earned prior to this. See the 
                    'comments added above.


                    'CreditsAttempted
                    'Troy:10/27/2006:The following line was commented out since the credits
                    'for the course is already included in the attempted credits and we should
                    'not count it twice. Remember that we are using only one record for the course
                    'whether that is the best or latest.
                    'row("CreditsAttempted") += dr("Credits")
                    '
                    '
                Else
                    'Course has not been previously taken
                    row = dtProgress.NewRow
                    row("ReqId") = dr("ReqId")
                    row("Credits") = dr("Credits")
                    row("IsCreditsEarned") = dr("IsCreditsEarned")
                    row("IsCreditsAttempted") = dr("IsCreditsAttempted")
                    If row("IsCreditsEarned") Then
                        row("CreditsEarned") = dr("Credits")
                    Else
                        row("CreditsEarned") = 0.0
                    End If
                    If row("IsCreditsAttempted") Then
                        row("CreditsAttempted") = dr("Credits")
                    Else
                        row("CreditsAttempted") = 0.0
                    End If
                    row("GPA") = dr("GPA")
                    row("Score") = dr("Score")
                    dtProgress.Rows.Add(row)
                End If

            Next
        End If
    End Sub
    Private Function IsValueAlreadyInTable(ByVal valueToSearchFor As String, ByVal columnName As String, ByVal dt As DataTable) As Boolean
        Dim result As Boolean

        For Each dr As DataRow In dt.Rows
            If Not dr.IsNull(columnName) Then
                If dr(columnName).ToString = valueToSearchFor Then
                    result = True
                    Exit For
                End If
            End If
        Next

        Return result
    End Function
    Private Function ComputeGPA(ByVal arrRows() As DataRow, ByVal prgVerId As Guid, Optional ByVal isResults As Boolean = False) As Decimal
        Dim gpaMethod As String = myAdvAppSettings.AppSettings("GPAMethod").ToLower
        Dim sum As Decimal
        Dim cnt As Decimal
        Dim gpa As Decimal
        Dim dr As DataRow

        If gpaMethod = "simpleavg" _
        Or Not (New ProgVerDB()).CheckIfAPrgVersionTrackCredits(prgVerId) Then

            'This code is to compute a simple average GPA.
            'SimpleGPA = SUM (Course GPA) / Number of Courses

            For Each dr In arrRows
                If Not (dr("GPA") Is DBNull.Value) Then
                    cnt += 1
                    sum += dr("GPA")
                End If
            Next
        Else

            'gpaMethod = "WeightedAVG"

            'This code is to compute a weigthed average GPA.
            'WeightedAVG = SUM (Course GPA * Course Credits) / Total Credits Attempted

            For Each dr In arrRows

                If Not (dr("GPA") Is DBNull.Value) Then

                    If isResults Then
                        'Results table
                        sum += dr("GPA") * dr("Credits")


                        If Not (dr("IsCreditsAttempted") Is DBNull.Value) Then
                            If dr("IsCreditsAttempted") Then
                                cnt += dr("Credits")
                            End If
                        End If


                    Else

                        'GraduateAudit table
                        '----------commented by Theresa G on 7/9/2009 for mantis [Advantage NEW 16452]: Review: Galen -  Transcript totals are incorrect.
                        '----------the cumGPA will be calculated if the GPA is checked in the grade system and even though the student does earn the credits.
                        'If Not (dr("IsCreditsEarned") Is System.DBNull.Value) Then
                        '    If dr("IsCreditsEarned") Then       'Or dr("ReqTypeId") = 2 
                        sum += dr("GPA") * dr("Credits")
                        '    End If
                        'End If
                        If Not (dr("IsCreditsAttempted") Is DBNull.Value) Then
                            If dr("IsCreditsAttempted") Then    'Or dr("ReqTypeId") = 2 
                                cnt += dr("CreditsAttempted")
                            End If
                        End If

                    End If

                End If
            Next

        End If

        If cnt <> 0 Then
            gpa = sum / cnt
        End If

        Return Math.Round(gpa, 2)
    End Function
    Private Function ComputeGPA(ByVal dt As DataTable, ByVal prgVerId As Guid) As Decimal

        Dim gpaMethod As String = myAdvAppSettings.AppSettings("GPAMethod").ToLower
        Dim sum As Decimal
        Dim cnt As Decimal
        Dim gpa As Decimal
        Dim dr As DataRow

        If gpaMethod = "simpleavg" _
        Or Not (New ProgVerDB()).CheckIfAPrgVersionTrackCredits(prgVerId) Then

            'This code is to compute a simple average GPA.
            'SimpleGPA = SUM (Course GPA) / Number of Courses

            For Each dr In dt.Rows
                If Not (dr("GPA") Is DBNull.Value) Then
                    cnt += 1
                    sum += dr("GPA")
                End If
            Next

        Else
            'This code is to compute a weigthed average GPA.
            'WeightedAVG = SUM (Course GPA * Course Credits) / Total Credits Attempted

            For Each dr In dt.Rows
                If Not (dr("GPA") Is DBNull.Value) Then

                    '----------commented by Theresa G on 7/9/2009 for mantis [Advantage NEW 16452]: Review: Galen -  Transcript totals are incorrect.
                    '----------the cumGPA will be calculated if the GPA is checked in the grade system and even though the student does earn the credits.
                    'If dr("IsCreditsEarned") Then
                    sum += dr("GPA") * dr("Credits")
                    'End If
                    If dr("IsCreditsAttempted") Then
                        cnt += dr("CreditsAttempted")
                    End If
                End If
            Next

        End If

        If cnt <> 0 Then
            gpa = sum / cnt
        End If

        Return Math.Round(gpa, 2)
    End Function
    Private Function ComputeCumGPA(ByVal dt As DataTable) As Decimal

        Dim gpaMethod As String = myAdvAppSettings.AppSettings("GPAMethod").ToLower
        Dim sum As Decimal
        Dim cnt As Decimal
        Dim gpa As Decimal
        Dim dr As DataRow


        For Each dr In dt.Rows
            If Not (dr("Score") Is DBNull.Value) Then
                cnt += 1
                sum += dr("Score")
            End If
        Next


        If cnt <> 0 Then
            gpa = sum / cnt
        End If

        Return Math.Round(gpa, 2)
    End Function
    Private Sub SetProperties()

        If dtGraduateAudit.Rows.Count > 0 Then
            Dim firstRow As DataRow
            firstRow = dtGraduateAudit.Rows(0)
            If Not firstRow("AllCreditsAttempted") Is DBNull.Value Then mAllAttemptedCredits = firstRow("AllCreditsAttempted") Else mAllAttemptedCredits = 0
            If Not firstRow("GPA") Is DBNull.Value Then mGpa = firstRow("GPA") Else mGpa = 0
            If Not firstRow("CreditsAttempted") Is DBNull.Value Then mAttemptedCredits = firstRow("CreditsAttempted") Else mAttemptedCredits = 0
            If Not firstRow("CreditsEarned") Is DBNull.Value Then mEarnedCredits = firstRow("CreditsEarned") Else mEarnedCredits = 0
            If Not firstRow("EarnedFinAidCredits") Is DBNull.Value Then mEarnedFinAidCredits = firstRow("EarnedFinAidCredits") Else mEarnedFinAidCredits = 0
            If Not firstRow("HoursCompleted") Is DBNull.Value Then mCompletedHours = firstRow("HoursCompleted") Else mCompletedHours = 0
            If Not firstRow("CreditsRemaining") Is DBNull.Value Then mRemainingCredits = firstRow("CreditsRemaining") Else mRemainingCredits = 0
            If Not firstRow("HoursRemaining") Is DBNull.Value Then mRemainingHours = firstRow("HoursRemaining") Else mRemainingHours = 0
            If dtResults.Rows.Count >= 1 Then mTotalClasses = dtResults.Rows.Count Else mTotalClasses = 0
            If Not firstRow("IsInGPA") Is DBNull.Value Then mGradeInGpa = firstRow("IsInGPA") Else mGradeInGpa = 0
            If Not firstRow("CreditZeroWeight") Is DBNull.Value Then mCreditZeroWeight = firstRow("CreditZeroWeight") Else mCreditZeroWeight = 0
            ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
            If Not firstRow("TotalScheduledHours") Is DBNull.Value Then mTotalScheduledHours = firstRow("TotalScheduledHours") Else mTotalScheduledHours = 0
            If Not firstRow("AllScheduledHours") Is DBNull.Value Then mAllScheduledHours = firstRow("AllScheduledHours") Else mAllScheduledHours = 0
            ''
            If Not firstRow("CreditsEarnedForSAP") Is DBNull.Value Then mEarnedCreditsForSAP = firstRow("CreditsEarnedForSAP") Else mEarnedCreditsForSAP = 0
            If Not firstRow("CreditsAttemptedForSAP") Is DBNull.Value Then mAttemptedCreditsForSAP = firstRow("CreditsAttemptedForSAP") Else mAttemptedCreditsForSAP = 0
            If Not firstRow("AllCreditsAttemptedForSAP") Is DBNull.Value Then mAllAttemptedCreditsForSAP = firstRow("AllCreditsAttemptedForSAP") Else mAllAttemptedCreditsForSAP = 0

            If mAttemptedCredits <> 0 Then
                mPercentageEarnedCredits = Math.Round(mEarnedCredits / mAttemptedCredits * 100, 2)
            Else
                mPercentageEarnedCredits = 0
            End If

            If mAttemptedCreditsForSAP <> 0 Then
                If myAdvAppSettings.AppSettings("IncludeAllCreditsAttemptedinTotal").ToString.ToLower = "yes" Then
                    mPercentageEarnedCreditsForSAP = Math.Round(mEarnedCreditsForSAP / mAllAttemptedCreditsForSAP * 100, 2)
                Else
                    mPercentageEarnedCreditsForSAP = Math.Round(mEarnedCreditsForSAP / mAttemptedCreditsForSAP * 100, 2)
                End If
            Else
                mPercentageEarnedCreditsForSAP = 0
            End If
        Else
            mGpa = 0
            mAttemptedCredits = 0
            mAllAttemptedCredits = 0
            mEarnedCredits = 0
            mEarnedFinAidCredits = 0
            mCompletedHours = 0
            mRemainingCredits = 0
            mRemainingHours = 0
            mTotalClasses = 0
            mPercentageEarnedCredits = 0
            mGradeInGpa = False
            mEarnedCreditsForSAP = 0
            mAttemptedCreditsForSAP = 0

        End If
    End Sub

#End Region

#Region "Public Methods"
    Public Function GetGraduateAuditByEnrollment(ByVal stuEnrollId As String, ByVal gradeReps As String, ByVal includeHours As Boolean, Optional ByVal campusID As String = "") As DataTable   'DataSet 
        Dim dbTranscript As New TranscriptDB
        Dim ds As DataSet

        If myAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric" Then
            ds = dbTranscript.GetGraduateAuditByEnrollmentForNumeric(stuEnrollId)
        ElseIf dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
            ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
        Else
            ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
        End If

        dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, "1/1/1001", "", "", campusID)
        dtResults = ds.Tables("Results")

        SetProperties()

        Return dtGraduateAudit

    End Function
    Public Function GetGraduateAuditByEnrollmentForCourseEquivalent(ByVal stuEnrollId As String, ByVal gradeReps As String, ByVal includeHours As Boolean, Optional ByVal campusID As String = "") As List(Of GraduateAuditGroupDto)  'DataTable 

        Dim ds As DataSet

        'Get PrgVerId from StuEnrollId because when the ds returned is empty (the student do not have results or is continueEducation  )
        Dim PrgVerId As Guid
        If stuEnrollId <> "" Then
            PrgVerId = (New ProgVerDB()).GetPrgVerIdForAGivenStuEnrollId(stuEnrollId)
        End If

        If myAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric" Then
            ds = dbTranscript.GetGraduateAuditByEnrollmentForNumericForCourseEquivalent(stuEnrollId)
        ElseIf dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
            ds = dbTranscript.GetGraduateAuditByCEProgEnrollmentForCourseEquivalent(stuEnrollId)
        Else
            ds = dbTranscript.GetGraduateAuditByEnrollmentForCourseEquivalent(stuEnrollId)
        End If

        dtGraduateAudit = BuildGraduateAuditDT(ds, PrgVerId, gradeReps, includeHours)
        dtResults = ds.Tables("Results")

        SetProperties()

        Dim result As New List(Of GraduateAuditGroupDto)

        For Each dr As DataRow In dtGraduateAudit.Rows
            Dim dto = New GraduateAuditGroupDto()
            dto.PrgVerId = If(IsDBNull(dr("PrgVerId")), String.Empty, dr("PrgVerId").ToString())
            dto.Code = dr("Code").ToString()
            dto.Credits = dr("Credits")
            dto.Descrip = dr("Descrip").ToString()
            dto.IsCreditsEarned = If(IsDBNull(dr("IsCreditsEarned")), False, dr("IsCreditsEarned"))
            dto.Level = If(IsDBNull(dr("Level")), Nothing, dr("Level"))
            'dto.ParentId = If(IsDBNull(dr("ParentId")), String.Empty, dr("ParentId"))
            dto.ReqId = If(IsDBNull(dr("ReqID")), String.Empty, dr("ReqID").ToString())
            dto.ReqTypeId = If(IsDBNull(dr("ReqTypeId")), Nothing, Convert.ToInt32(dr("ReqTypeId")))
            dto.StartDate = If(IsDBNull(dr("StartDate")), Nothing, dr("StartDate"))
            dto.CreditsEarned = If(IsDBNull(dr("CreditsEarned")), Nothing, dr("CreditsEarned"))
            dto.CreditsRemaining = If(IsDBNull(dr("CreditsRemaining")), Nothing, dr("CreditsRemaining"))
            dto.EquivCourseDesc = If(IsDBNull(dr("EquivCourseDesc")), String.Empty, dr("EquivCourseDesc"))
            dto.Grade = If(IsDBNull(dr("Grade")), String.Empty, dr("Grade"))
            dto.Score = If(IsDBNull(dr("Score")), Nothing, dr("Score"))
            dto.Hours = If(IsDBNull(dr("Hours")), 0, Convert.ToDecimal(dr("Hours")))
            dto.HoursCompleted = If(IsDBNull(dr("HoursCompleted")), Nothing, Convert.ToDecimal(dr("HoursCompleted")))
            dto.HoursRemaining = If(IsDBNull(dr("HoursRemaining")), Nothing, Convert.ToDecimal(dr("HoursRemaining")))
            dto.GrdSysDetailId = If(IsDBNull(dr("GrdSysDetailId")), String.Empty, dr("GrdSysDetailId"))
            dto.IsContinuingEd = If(IsDBNull(dr("IsContinuingEd")), False, dr("IsContinuingEd"))
            dto.IsTransferGrade = If(IsDBNull(dr("IsTransferGrade")), False, dr("IsTransferGrade"))
            result.Add(dto)
        Next

        'Return dtGraduateAudit
        Return result
    End Function
    Public Function GetResultsByEnrollment(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal campusID As String = "") As DataTable
        ' New Optional Parameter add termCond and classCond by Vijay Ramteke on May, 06 2009
        Dim ds As DataSet

        Dim transcriptType As String = String.Empty
        If Not myAdvAppSettings.AppSettings("TranscriptType", campusID) Is Nothing Then
            transcriptType = myAdvAppSettings.AppSettings("TranscriptType", campusID)
        End If
        If stuEnrollId <> "" Then

            If transcriptType.ToLower = "traditional_numeric" Then

                ds = dbTranscript.GetGraduateAuditByEnrollmentForNumeric(stuEnrollId, , termCond, classCond)
            ElseIf dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId, , termCond, classCond)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId, , termCond, classCond, campusID)

            End If
            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , termCond, classCond, campusID)
            dtResults = ds.Tables("Results")
            '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
            If transcriptType.ToLower = "traditional_numeric" Then
                GetAverageScore()
            Else
                GetAverageGpa()
            End If

            ''''''
            SetProperties()
        End If

        Return dtResults
    End Function
    '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
    Private Sub GetAverageScore()
        Dim dcSa As New DataColumn
        dcSa.DataType = Type.GetType("System.Decimal")
        dcSa.ColumnName = "AvgScoreNew"
        Dim dcGa As New DataColumn
        dcGa.DataType = Type.GetType("System.Decimal")
        dcGa.ColumnName = "AvgGPANew"
        Try


            Dim dr() As DataRow
            dr = dtGraduateAudit.Select("Score is Not null")

            Dim scoreAvg As Decimal = 0.0
            'Dim GPAAvg As Decimal = 0.0
            Dim i As Integer = 0
            For Each dr1 In dr
                i += 1
                scoreAvg += DirectCast(dr1("Score"), Decimal)
            Next

            dcSa.DefaultValue = If(i > 0, (scoreAvg / i), 0)

            dcGa.DefaultValue = 0
            dtResults.Columns.Add(dcSa)
            dtResults.Columns.Add(dcGa)

        Catch ex As Exception
            dcSa.DefaultValue = 0
            dcGa.DefaultValue = 0
            dtResults.Columns.Add(dcSa)
            dtResults.Columns.Add(dcGa)
        End Try


    End Sub
    '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
    Private Sub GetAverageGpa()
        Dim dcSa As New DataColumn
        dcSa.DataType = Type.GetType("System.Decimal")
        dcSa.ColumnName = "AvgScoreNew"
        Dim dcGa As New DataColumn
        dcGa.DataType = Type.GetType("System.Decimal")
        dcGa.ColumnName = "AvgGPANew"
        ' Try


        Dim dr() As DataRow
        dr = dtGraduateAudit.Select("GPA is Not null")

        'Dim ScoreAvg As Decimal = 0.0
        Dim gpaAvg As Decimal = 0.0
        Dim i As Integer = 0
        For Each dr1 In dr
            i += 1
            gpaAvg += DirectCast(dr1("GPA"), Decimal)
        Next
        dcSa.DefaultValue = 0
        dcGa.DefaultValue = If(i = 0, 0.0, gpaAvg / i)
        'dcGA.DefaultValue = GPAAvg / i

        dtResults.Columns.Add(dcSa)
        dtResults.Columns.Add(dcGa)

    End Sub
    Public Function HasStudentMetAllRequirements(campusid As String, Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal gradesFormat As String = "Letter", Optional ByRef reason As String = "") As Boolean
        Dim result As Boolean
        Dim dbreq As New RequirementsDB

        result = False

        If gradesFormat.ToUpper = "LETTER" Then
            result = HasStudentMetAllRequirementsForLetterGrades(campusid, stuEnrollId, gradeReps, includeHours, reason)
        ElseIf gradesFormat.ToUpper = "NUMERIC" Then
            result = HasStudentMetAllRequirementsForNumericGrades(campusid, stuEnrollId, gradeReps, includeHours, reason)
        End If
        ''Added by Saraswathi Lakshmanan to find if the student has met all the graduation Requirements
        If result = True Then
            result = HasStudentMetAlltheGraduationRequirements(stuEnrollId, reason)
        End If

        Return result

    End Function
    Public Function HasStudentMetAlltheGraduationRequirements(ByVal stuEnrollId As String, Optional ByRef reason As String = "") As Boolean
        Dim strEnrollStatus As String
        Dim errmessage As String
        Dim strTestStatus As String = CheckandListRequiredtest_ForGraduation(stuEnrollId)
        Dim strDocStatus As String = CheckandListRequiredDocuments_ForGraduation(stuEnrollId)
        Dim strReqsMetStatus As String = CheckandListGradreqGroups(stuEnrollId)

        Dim strTestStatusEnrl As String = CheckandListRequiredTest_ForEnrollment(stuEnrollId)
        ''   Dim strDocStatusEnrl As String = CheckRequiredDocuments_ForEnrollment(stuEnrollId)
        Dim strDocStatusEnrl As String = CheckandListRequiredDocuments_ForEnrollment(stuEnrollId)
        Dim strReqsMetStatusEnrl As String = CheckandListEnrollreqGroups(stuEnrollId)


        errmessage = strReqsMetStatusEnrl + strTestStatusEnrl + strDocStatusEnrl + strReqsMetStatus + strTestStatus + strDocStatus
        If strTestStatus = "" And strDocStatus = "" And strReqsMetStatus = "" And strTestStatusEnrl = "" And strDocStatusEnrl = "" And strReqsMetStatusEnrl = "" Then
            strEnrollStatus = "Eligible"
        Else
            reason = "The student hasn't met the following requirements:; " + vbCrLf + errmessage
            strEnrollStatus = "Not Eligible"
        End If
        Return IIf(strEnrollStatus = "Eligible", True, False)

    End Function
    ''Added by Saraswathi lakshmanan on Sept 22 2010 For document tracking
    ''This function helps to find if the required test dcuments are passed by the student
    ''For use in post payments
    Public Function CheckIfRequiredTest_ForGraduation(ByVal StuEnrollId As String) As String
        Dim intStudentHasTaken As Integer
        Dim studentFacade As New StudentsAccountsFacade
        intStudentHasTaken = studentFacade.CheckIfStudentHasPassedRequiredTestWithProgramVersion_Graduation(StuEnrollId)

        If intStudentHasTaken >= 1 Then
            Return "Not Eligible"
        Else
            Return "Eligible"
        End If

    End Function
    Public Function CheckIfRequiredTest_ForEnrollment(ByVal StuEnrollId As String) As String
        Dim intStudentHasTaken As Integer
        Dim studentFacade As New StudentsAccountsFacade
        intStudentHasTaken = studentFacade.CheckIfStudentHasPassedRequiredTestWithProgramVersion_Enrollment(StuEnrollId)

        If intStudentHasTaken >= 1 Then
            Return "Not Eligible"
        Else
            Return "Eligible"
        End If

    End Function
    Public Function CheckRequiredDocuments_ForGraduation(ByVal StuEnrollID As String) As String
        Dim intStudentHasTaken As Integer
        Dim studentFacade As New StudentsAccountsFacade

        intStudentHasTaken = studentFacade.CheckAllApprovedDocumentsWithPrgVersion_Graduation(StuEnrollID)

        If intStudentHasTaken >= 1 Then
            Return "Not Eligible"
        Else
            Return "Eligible"
        End If
    End Function
    Public Function CheckRequiredDocuments_ForEnrollment(ByVal StuEnrollID As String) As String
        Dim intStudentHasTaken As Integer
        Dim studentFacade As New StudentsAccountsFacade

        intStudentHasTaken = studentFacade.CheckAllApprovedDocumentsWithPrgVersion_Enrollment(StuEnrollID)

        If intStudentHasTaken >= 1 Then
            Return "Not Eligible"
        Else
            Return "Eligible"
        End If
    End Function
    Public Function CheckandListRequiredDocuments_ForEnrollment(ByVal StuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListEnrollmentDocumentRequirements_Sp(StuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    strdocs.Append(row("Descrip") + ";")
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function
    Public Function HasStudentMetAllRequirementsForLetterGrades(campusid As String, Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByRef reason As String = "") As Boolean
        Dim ds As DataSet


        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId, , , , campusid)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusid)
            dtResults = ds.Tables("Results")

            SetProperties()
        End If

        Dim result As Boolean
        ''To get the transfer grades, with credits earrned not checked.
        Dim dsgradesysdetailids As DataSet
        dsgradesysdetailids = dbTranscript.GetGradesSysDetailId()
        Dim objCredits As Object

        If dsgradesysdetailids.Tables(0).Rows.Count > 0 Then

            Dim filtercond As String = ""
            If dsgradesysdetailids.Tables(0).Rows.Count > 1 Then
                For i = 0 To dsgradesysdetailids.Tables(0).Rows.Count - 1
                    filtercond = filtercond + " GrdSysDetailId='" + dsgradesysdetailids.Tables(0).Rows(i)(0).ToString + "'  OR"
                Next
                filtercond = filtercond.Remove(filtercond.Length - 2, 2)
            Else
                filtercond = " GrdSysDetailId='" + dsgradesysdetailids.Tables(0).Rows(0)(0).ToString + "' "
            End If
            objCredits = dtGraduateAudit.Compute("SUM(Credits)", filtercond)
            ''   ObjHours = dtGraduateAudit.Compute("SUM(Hours)", filtercond)

        End If

        If dtGraduateAudit.Rows.Count > 0 Then
            Dim firstRow As DataRow
            firstRow = dtGraduateAudit.Rows(0)

            If firstRow("IsContinuingEd") Then
                'Comment line below if want to prevent from graduating CE students if she/he has scheduled courses
                result = True

                'Uncomment code below if want to prevent from graduating CE students if she/he has scheduled courses
                Dim rows() As DataRow = dtGraduateAudit.Select("CreditsRemaining > 0 OR HoursRemaining > 0")
                If rows.GetLength(0) = 0 Then
                    If (firstRow("IsPass") = False) Then
                        result = False
                        reason = reason + "Failed the course;"
                    Else
                        If myAdvAppSettings.AppSettings("CheckCreditsForContiningEd").ToLower = "yes" Then
                            Dim totalCredits As Decimal = dbTranscript.getCredits(firstRow("PrgVerId"))
                            If firstRow("CreditsEarned") < totalCredits Then
                                result = False
                                reason = reason + "Credits are not satisfied;"
                            Else
                                result = True
                            End If
                        Else
                            result = True
                        End If

                    End If

                Else
                    result = False
                    reason = reason + "Credits and Hours are remaining;"
                End If

            Else
                ''Added by saraswathi lakshmanan on may 06 2009
                ''if it is a transferGrade, and Credits earned is not checked. Allow the student to graduate, If the student is transfered from a different campus, the student will not earn the credit. the credits will be awarded to them.
                ''For mantis case 15845 and 11918
                If dsgradesysdetailids.Tables(0).Rows.Count > 0 Then
                    If Not IsDBNull(objCredits) Then
                        If (firstRow("CreditsRemaining") = Convert.ToDecimal(objCredits)) Then
                            result = True
                        Else
                            result = (firstRow("CreditsRemaining") <= 0 And firstRow("HoursRemaining") <= 0)
                            If result = False Then
                                reason = reason + "Credits or Hours are remaining;"
                            End If
                        End If
                    Else
                        result = (firstRow("CreditsRemaining") <= 0 And firstRow("HoursRemaining") <= 0)
                        If result = False Then
                            reason = reason + "Credits or Hours are remaining;"
                        End If
                    End If
                Else
                    result = (firstRow("CreditsRemaining") <= 0 And firstRow("HoursRemaining") <= 0)
                    If result = False Then
                        reason = reason + "Credits or Hours are remaining;"
                    End If
                End If
            End If
        Else
            'added by Theresa G on 5/12/2009
            'now we added the config entry CheckCreditsForContiningEd to treat it a regular course or continuing ed
            'if set to yes,it has to treat like a regular program version and the student has to fullfill the requirements

            If myAdvAppSettings.AppSettings("CheckCreditsForContiningEd").ToLower = "yes" Then
                result = False
            Else
                'There is no rows on the Graduate Audit table => no Program Version Definition
                'Continuing Education?
                'Assume all requirements have been met
                result = True
            End If


        End If
        Dim isClockHourProgram As Boolean
        isClockHourProgram = (New StuEnrollFacade).IsStudentProgramClockHourType(stuEnrollId)
        If myAdvAppSettings.AppSettings("TrackSapAttendance", campusid).ToLower = "byday" Then
            Dim gradHrsResult As Boolean = True

            If isClockHourProgram = True Then
                gradHrsResult = (New StuEnrollDB).HasStudentMetGraduationHoursRequirement(stuEnrollId)
                If gradHrsResult = False Then
                    reason = reason + "Student has not satisfied the graduation hours requirements;"
                End If
            End If

            If result = True And gradHrsResult = True Then
                Return True
            Else

                Return False

            End If
        ElseIf myAdvAppSettings.AppSettings("TimeClockClassSection", campusid).ToLower = "yes" Then
            Dim gradHrsResult As Boolean = True
            If isClockHourProgram = True Then
                gradHrsResult = (New StuEnrollDB).HasStudentMetGraduationHoursRequirementByClass(stuEnrollId, campusid)
            End If
            If result = True And gradHrsResult = True Then
                Return True
            Else
                reason = reason + "Student has not satisfied the graduation hours requirements;"
                Return False

            End If
        End If

        Return result




    End Function
    Public Function HasStudentMetAllRequirementsForNumericGrades(campusId As String, ByVal stuEnrollId As String, ByVal gradeReps As String, ByVal includeHours As Boolean, Optional ByRef reason As String = "") As Boolean
        Dim result As Boolean
        Dim stuPr As New StuProgressReportDB
        Dim dtWU As DataTable
        Dim dtMcr As DataTable
        Dim facPro As New StuProgressReportObject


        result = False

        'Get the WorkUnitResults info
        dtWU = (stuPr.GetWorkUnitResults(Nothing, stuEnrollId).Copy)

        'Get the ModuleCoursesResults info
        dtMcr = stuPr.GetModulesCoursesResults(Nothing, stuEnrollId).Copy

        'Update dtMCR to include the Completed and Credits Earned for each course
        facPro.UpdateCompletedAndCreditsEarned(dtMcr, dtWU, "", Nothing)

        Dim rows() As DataRow = dtMcr.Select("Completed = false")

        If rows.Length = 0 And dtMcr.Rows.Count > 0 Then
            result = True
        End If
        If result = False Then
            reason = reason + "Student has failed the credit requirements;"
        End If
        Dim isClockHourProgram As Boolean
        isClockHourProgram = (New StuEnrollFacade).IsStudentProgramClockHourType(stuEnrollId)
        If myAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToLower = "byday" Then
            Dim gradHrsResult As Boolean = True

            If isClockHourProgram = True Then
                gradHrsResult = (New StuEnrollDB).HasStudentMetGraduationHoursRequirement(stuEnrollId)
                If gradHrsResult = False Then
                    reason = reason + "Student has not satisfied the graduation hours requirements;"
                End If
            End If

            If result = True And gradHrsResult = True Then
                Return True
            Else
                Return False
            End If
        ElseIf myAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToLower = "yes" Then
            Dim gradHrsResult As Boolean = True
            If isClockHourProgram = True Then
                gradHrsResult = (New StuEnrollDB).HasStudentMetGraduationHoursRequirementByClass(stuEnrollId, campusId)
            End If
            If result = True And gradHrsResult = True Then
                Return True
            Else
                If gradHrsResult = False Then
                    reason = reason + "Student has not satisfied the graduation hours requirements;"
                End If
                Return False

            End If
        End If
        Return result

    End Function
    Public Function HasStudentMetRequirement(ByVal reqId As String, Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As Boolean
        Dim ds As DataSet

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusId)
            dtResults = ds.Tables("Results")

            SetProperties()
        End If

        Dim result As Boolean

        If dtGraduateAudit.Rows.Count > 0 Then
            Dim rows() As DataRow
            rows = dtGraduateAudit.Select("ReqId='" & reqId & "'")

            If rows.GetLength(0) > 0 Then
                Dim firstRow As DataRow
                firstRow = dtGraduateAudit.Rows(0)
                result = (firstRow("CreditsRemaining") <= 0 And firstRow("HoursRemaining") <= 0)
            End If
        End If

        Return result
    End Function
    Public Function GetTotalClasses(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As Integer

        Dim ds As DataSet

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusId)
            dtResults = ds.Tables("Results")

            SetProperties()
        End If

        'Return dtResults.Rows.Count

        Return TotalClasses
    End Function
    Public Function GetCreditsEarned(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As Decimal

        Dim ds As DataSet

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusId)
            dtResults = ds.Tables("Results")


            SetProperties()
        End If

        Return EarnedCredits
    End Function
    Public Function GetCreditsAttempted(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As Decimal

        Dim ds As DataSet

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusId)
            dtResults = ds.Tables("Results")

            SetProperties()
        End If

        Return AttemptedCredits
    End Function
    Public Function GetCreditsAttemptedForSAP(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As Decimal

        Dim ds As DataSet

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusId)
            dtResults = ds.Tables("Results")

            SetProperties()
        End If

        If myAdvAppSettings.AppSettings("IncludeAllCreditsAttemptedinTotal").ToString.ToLower = "yes" Then
            Return AllAttemptedCreditsForSAP
        Else
            Return AttemptedCreditsForSAP
        End If
    End Function
    Public Function GetGPA(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal termEndDate As DateTime = #1/1/1001#, Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As Decimal

        Dim ds As DataSet

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId, termEndDate, , , campusId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId, termEndDate, , , campusId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, termEndDate, , , campusId)
            dtResults = ds.Tables("Results")


            SetProperties()
        End If

        Return GPA
    End Function
    Public Function GetHoursCompleted(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As Decimal

        Dim ds As DataSet

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusId)
            dtResults = ds.Tables("Results")


            SetProperties()
        End If


        Return CompletedHours
    End Function
    Public Function GetTermProgressByEnrollment(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As DataTable 'DataSet 
        Dim ds As DataSet

        'Get PrgVerId from StuEnrollId because when the ds returned is empty (the student do not have results or is continueEducation  )
        Dim PrgVerId As Guid
        If stuEnrollId <> "" Then
            PrgVerId = (New ProgVerDB()).GetPrgVerIdForAGivenStuEnrollId(stuEnrollId)
        End If

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusId)
            dtResults = ds.Tables("Results")

            SetProperties()
        End If
        '
        '
        '
        Dim termCtr As Integer = 1
        Dim arrRows() As DataRow
        Dim dr As DataRow
        Dim dtGradAudit As DataTable = dtGraduateAudit.Copy
        Dim dtTermProgress As New DataTable

        '   structure of the TermProgress table
        With dtTermProgress
            .Columns.Add("TermId", Type.GetType("System.Guid"))
            .Columns.Add("TermDescrip", Type.GetType("System.String"))
            .Columns.Add("DescripXTranscript", Type.GetType("System.String"))
            .Columns.Add("StartDate", Type.GetType("System.DateTime"))
            .Columns.Add("EndDate", Type.GetType("System.DateTime"))
            .Columns.Add("TakenCourses", Type.GetType("System.Int32"))
            .Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
            .Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
            .Columns.Add("TermGPA", Type.GetType("System.Decimal"))
            .Columns.Add("GPA", Type.GetType("System.Decimal"))
        End With
        '
        '
        '   if the GradAudit exit function and return an empty TermProgress table
        If dtGradAudit.Rows.Count = 0 Then
            Return dtTermProgress
        End If
        '
        '
        '
        '********************************************************************************
        '**** Massage GraduateAudit and Results table to produce TermProgress table. ****
        '********************************************************************************
        '
        '   add DISTINCT Terms into TermProgress table
        If dtGradAudit.Rows.Count > 0 And dtResults.Rows.Count > 0 Then

            For Each dr In dtResults.Rows
                If Not dr.IsNull("TermId") Then
                    If Not IsValueAlreadyInTable(dr("TermId").ToString, "TermId", dtTermProgress) Then
                        '   add row into TermProgress table
                        Dim row As DataRow
                        row = dtTermProgress.NewRow
                        row("TermId") = dr("TermId")
                        row("TermDescrip") = dr("TermDescrip")
                        row("DescripXTranscript") = dr("DescripXTranscript")
                        row("StartDate") = dr("StartDate")
                        row("EndDate") = dr("EndDate")
                        dtTermProgress.Rows.Add(row)
                        '
                        '   add columns corresponding to distinct Terms into temporary copy of GraduateAudit table
                        dtGradAudit.Columns.Add("EarnedCR" & termCtr, Type.GetType("System.Decimal"))
                        dtGradAudit.Columns.Add("AttemptedCR" & termCtr, Type.GetType("System.Decimal"))
                        termCtr += 1
                    End If
                End If
            Next
        End If
        '
        '
        '   CreditsEarned and CreditsAttempted for each course on every particular term
        '   TermGPA and Cummulative GPA for every term
        termCtr = 0
        'Dim cummGPA As Decimal
        'Dim courseCtr As Integer
        Dim termCourseCtr As Integer
        Dim oneTerm As String
        Dim cummTerms As New StringBuilder

        For Each dr In dtTermProgress.Rows
            termCtr += 1
            termCourseCtr = 0
            arrRows = dtResults.Select("TermId='" & dr("TermId").ToString & "'")

            If arrRows.GetLength(0) > 0 Then

                For Each resultDr As DataRow In arrRows
                    Dim reqRow() As DataRow = dtGradAudit.Select("ReqId='" & resultDr("ReqId").ToString & "'")

                    If reqRow.GetLength(0) > 0 Then
                        Dim gradAuditDr As DataRow = reqRow(0)
                        If resultDr("IsCreditsEarned") Then gradAuditDr("EarnedCR" & termCtr.ToString) = resultDr("Credits")
                        If resultDr("IsCreditsAttempted") Then gradAuditDr("AttemptedCR" & termCtr.ToString) = resultDr("Credits")

                    End If
                Next

                dr("TakenCourses") = arrRows.GetLength(0)


                '   Compute TermGPA and cummulative GPA
                '   Select rows from dtGradAudit for current term and compute term GPA.
                oneTerm = "(EarnedCR" & termCtr.ToString & " IS NOT NULL AND AttemptedCR" & termCtr.ToString & " IS NOT NULL)"
                arrRows = dtGradAudit.Select(oneTerm)
                dr("TermGPA") = ComputeGPA(arrRows, PrgVerId)

                '   Compute cummulative GPA
                '   Select rows from dtGradAudit for current and previous terms and compute cummulative GPA.
                cummTerms.Append(oneTerm)
                arrRows = dtGradAudit.Select(cummTerms.ToString)
                dr("GPA") = ComputeGPA(arrRows, PrgVerId)

                cummTerms.Append(" OR ")

            End If
        Next
        '
        '
        '   Nested Course Groups
        '   Credits Earned and Credits Attempted for Course Groups on every particular term
        Dim maxLevel As Integer = dtGradAudit.Compute("MAX(Level)", "")
        Dim temp1 As Decimal
        Dim temp2 As Decimal

        '   find out if there are Course Groups in the Program Version definition
        Dim arrReqGrps() As DataRow = dtGradAudit.Select("ReqTypeId=2")

        If arrReqGrps.GetLength(0) > 0 Then
            termCtr = 0

            For Each row As DataRow In dtTermProgress.Rows
                termCtr += 1

                '   iterate (decrementing) through levels of nested Course Groups
                Do While maxLevel > 0

                    For Each dr In arrReqGrps

                        '   find out if there are children of level = maxLevel for current Course Group
                        arrRows = dtGradAudit.Select("ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")

                        If arrRows.GetLength(0) > 0 Then

                            For Each gradAuditDR As DataRow In arrRows
                                If Not gradAuditDR.IsNull("EarnedCR" & termCtr.ToString) Then
                                    temp1 += gradAuditDR("EarnedCR" & termCtr.ToString)
                                End If
                                If Not gradAuditDR.IsNull("AttemptedCR" & termCtr.ToString) Then
                                    temp2 += gradAuditDR("AttemptedCR" & termCtr.ToString)
                                End If
                            Next

                            '   update CreditsEarned for the current Course Group in the particular term
                            If temp1 < dr("CreditsEarned") Then
                                dr("EarnedCR" & termCtr.ToString) = temp1
                            Else
                                '   CreditsEarned capping
                                dr("EarnedCR" & termCtr.ToString) = dr("CreditsEarned")
                            End If

                            '   update CreditsAttempted for the current Course Group in the particular term
                            If temp2 < dr("CreditsAttempted") Then
                                dr("AttemptedCR" & termCtr.ToString) = temp2
                            Else
                                '   CreditsAttempted capping
                                dr("AttemptedCR" & termCtr.ToString) = dr("CreditsAttempted")
                            End If
                        End If

                    Next

                    maxLevel -= 1
                Loop
            Next
        End If
        '
        '
        '   CreditsEarned and CreditsAttempted for every particular term at Level = 0
        termCtr = 0
        Dim cumEarnedCR As Decimal
        Dim cumAttemptedCR As Decimal
        Dim excess As Decimal

        For Each row As DataRow In dtTermProgress.Rows
            termCtr += 1
            arrRows = dtGradAudit.Select("Level=0")

            If arrRows.GetLength(0) > 0 Then
                temp1 = 0
                temp2 = 0

                For Each gradAuditDR As DataRow In arrRows
                    If Not gradAuditDR.IsNull("EarnedCR" & termCtr.ToString) Then
                        temp1 += gradAuditDR("EarnedCR" & termCtr.ToString)
                    End If
                    If Not gradAuditDR.IsNull("AttemptedCR" & termCtr.ToString) Then
                        temp2 += gradAuditDR("AttemptedCR" & termCtr.ToString)
                    End If
                Next

                cumEarnedCR += temp1
                cumAttemptedCR += temp2

                '   update CreditsEarned for particular term
                If cumEarnedCR <= dtGradAudit.Rows(0)("CreditsEarned") Then
                    dtGradAudit.Rows(0)("EarnedCR" & termCtr.ToString) = temp1
                Else
                    '   CreditsEarned capping
                    excess = cumEarnedCR - dtGradAudit.Rows(0)("CreditsEarned")
                    If temp1 > excess Then
                        dtGradAudit.Rows(0)("EarnedCR" & termCtr.ToString) = temp1 - excess
                    Else
                        '   this case should never occur
                        dtGradAudit.Rows(0)("EarnedCR" & termCtr.ToString) = 0
                    End If
                End If

                '   update CreditsAttempted for particular term
                If cumAttemptedCR <= dtGradAudit.Rows(0)("CreditsAttempted") Then
                    dtGradAudit.Rows(0)("AttemptedCR" & termCtr.ToString) = temp2
                Else
                    '   CreditsAttempted capping
                    excess = cumAttemptedCR - dtGradAudit.Rows(0)("CreditsAttempted")
                    If temp1 > excess Then
                        dtGradAudit.Rows(0)("AttemptedCR" & termCtr.ToString) = temp2 - excess
                    Else
                        '   this case should never occur
                        dtGradAudit.Rows(0)("AttemptedCR" & termCtr.ToString) = 0
                    End If
                End If
            End If
        Next
        '
        '
        '   populate TermProgress table with data on first row of dtGradAudit table
        '   first row contains CreditsEarned and CreditsAttempted for every particular term
        termCtr = 0
        For Each row As DataRow In dtTermProgress.Rows
            termCtr += 1
            row("CreditsEarned") = dtGradAudit.Rows(0)("EarnedCR" & termCtr.ToString)
            row("CreditsAttempted") = dtGradAudit.Rows(0)("AttemptedCR" & termCtr.ToString)
        Next

        Return dtTermProgress

    End Function
    ' Added Newly By Uvarajan
    Public Function GetStudentsByEnrollment(ByVal stuId As String, Optional ByVal strWhere As String = "", Optional ByVal stuEnrollId As String = "") As DataTable

        Dim objTrans As New TransactionsDB
        Dim dtSummary As DataTable
        Dim ds2 As DataSet
        Dim ds As New DataSet


        If stuEnrollId <> "" Then

            ds2 = objTrans.GetMultipleStudentLedger(stuId, stuEnrollId, Guid.Empty.ToString, strWhere)

            For Each dr As DataRow In ds2.Tables(0).Rows
                If dr("RecordType") < 0 Then
                    dr.Delete()
                End If
            Next
            ds2.AcceptChanges()
            ds.Tables.Add(ds2.Tables(0).Copy)
            dtSummary = BuildGraduateAuditDTStudent(ds)
        End If

        'Return dtResults

        Return dtSummary
    End Function
    ' Added By uvarajan
    Private Function BuildGraduateAuditDTStudent(ByVal ds As DataSet)

        Dim dt As New DataTable("MultipleStudentLedger")
        dt = ds.Tables(0)
        dt.TableName = ("MultipleStudentLedger")

        Return dt

    End Function
    Public Function GetTermProgressFromResults(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal campusID As String = "") As DataTable 'DataSet 
        ' Dim br As New RegisterStudentsBR
        Dim ds As DataSet
        Dim PrgVerId As Guid

        If stuEnrollId <> "" Then
            PrgVerId = (New ProgVerDB()).GetPrgVerIdForAGivenStuEnrollId(stuEnrollId)
        End If


        If stuEnrollId <> "" Then
            If myAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric" Then
                ds = dbTranscript.GetGraduateAuditByEnrollmentForNumeric(stuEnrollId, , termCond, classCond)
            ElseIf dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId, , termCond, classCond)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId, , termCond, classCond)
            End If
            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , termCond, classCond, campusID)
            dtResults = ds.Tables("Results")
            SetProperties()
        End If
        '
        '
        '
        Dim termCtr As Integer = 1
        Dim arrRows() As DataRow
        Dim dr As DataRow
        Dim dtTermProgress As New DataTable
        Dim dtCummProgress As New DataTable
        '
        '   structure of the TermProgress table
        With dtTermProgress
            .Columns.Add("TermId", Type.GetType("System.Guid"))
            .Columns.Add("TermDescrip", Type.GetType("System.String"))
            .Columns.Add("DescripXTranscript", Type.GetType("System.String"))
            .Columns.Add("StartDate", Type.GetType("System.DateTime"))
            .Columns.Add("EndDate", Type.GetType("System.DateTime"))
            .Columns.Add("TakenCourses", Type.GetType("System.Int32"))
            .Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
            .Columns.Add("FACreditsEarned", Type.GetType("System.Decimal"))
            .Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
            .Columns.Add("TermGPA", Type.GetType("System.Decimal"))
            .Columns.Add("GPA", Type.GetType("System.Decimal"))
            .Columns.Add("Score", Type.GetType("System.Decimal"))
            .Columns.Add("CumCreditsAttempted", Type.GetType("System.Decimal"))
        End With
        '
        '
        '   if the GradAudit is empty, return an empty TermProgress table
        If dtGraduateAudit.Rows.Count = 0 Then
            Return dtTermProgress
        End If
        '
        '
        dtCummProgress = dtResults.Clone 'Same structure
        '
        '********************************************************************************
        '**** Massage GraduateAudit and Results table to produce TermProgress table. ****
        '********************************************************************************
        '
        '   add DISTINCT Terms into TermProgress table
        If dtGraduateAudit.Rows.Count > 0 And dtResults.Rows.Count > 0 Then
            Dim drs() As DataRow = dtResults.Select("", "StartDate")

            ' For Each dr In dtResults.Rows
            For Each dr In drs
                If Not dr.IsNull("TermId") Then
                    If Not IsValueAlreadyInTable(dr("TermId").ToString, "TermId", dtTermProgress) Then
                        '   add row into TermProgress table
                        Dim row As DataRow
                        row = dtTermProgress.NewRow
                        row("TermId") = dr("TermId")
                        row("TermDescrip") = dr("TermDescrip")
                        row("DescripXTranscript") = dr("DescripXTranscript")
                        row("StartDate") = dr("StartDate")
                        row("EndDate") = dr("EndDate")
                        dtTermProgress.Rows.Add(row)
                    End If
                End If
            Next
        End If
        '
        '   CreditsEarned and CreditsAttempted for each course on every particular term
        '   TermGPA and Cummulative GPA for every term
        termCtr = 0
        Dim oneTerm As String
        Dim cummTerms As New StringBuilder
        Dim equivReqid As String
        Dim tmpRows() As DataRow
        Dim dtIn As New DataTable
        dtIn = dtResults.Clone() 'same stucture
        Dim dtGPACalc As New DataTable
        dtGPACalc = dtResults.Clone() 'same stucture
        For Each dr In dtTermProgress.Rows
            termCtr += 1
            arrRows = dtResults.Select("TermId='" & dr("TermId").ToString & "'")

            If arrRows.GetLength(0) > 0 Then
                dr("TakenCourses") = arrRows.GetLength(0)
                dr("CreditsEarned") = 0
                dr("FACreditsEarned") = 0
                dr("CreditsAttempted") = 0
                If (myAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric") Then
                    For Each resultDr As DataRow In arrRows

                        'Verify that course belong to program version definition:
                        '       Look for course in the Graduate Audit table.
                        tmpRows = dtGraduateAudit.Select("ReqId='" & resultDr("ReqId").ToString & "'")

                        If tmpRows.GetLength(0) > 0 Then
                            Dim isPass As Boolean = brRegisterStudent.GrdOverRide(tmpRows(0)("PrgVerId").ToString(), resultDr("reqId").ToString, resultDr("Grade").ToString)
                            'Course belongs to program version definition

                            If Not IsDBNull(resultDr("IsCreditsAttempted")) Then
                                If resultDr("IsCreditsAttempted") = True Or resultDr("IsCreditsAttempted") = 1 Then
                                    If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                        Dim boolIsCourseALab As Boolean
                                        boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDr("ReqId").ToString)
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal
                                            Try
                                                decCredits = dbTranscript.ComputeWithCreditsPerService(stuEnrollId, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDr("ReqId").ToString)
                                            Catch ex As Exception
                                                decCredits = 0
                                            End Try
                                            dr("CreditsAttempted") += decCredits
                                        Else
                                            dr("CreditsAttempted") += 0
                                        End If
                                    Else
                                        dr("CreditsAttempted") += resultDr("Credits")
                                    End If
                                Else
                                    dr("CreditsAttempted") += 0.0
                                End If
                            End If

                            If isPass = True Then

                                ' ''' ''Call the function to add credits earned
                                Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDr("TestId").ToString())
                                If isHrsComp = True Then
                                    If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                        Dim boolIsCourseALab As Boolean
                                        boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDr("ReqId").ToString)
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal
                                            Try
                                                decCredits = dbTranscript.ComputeWithCreditsPerService(stuEnrollId, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDr("ReqId").ToString)
                                            Catch ex As Exception
                                                decCredits = 0
                                            End Try
                                            dr("CreditsEarned") += decCredits
                                            dr("FACreditsEarned") += resultDr("FinAidCredits")
                                        Else
                                            dr("CreditsEarned") += 0
                                            dr("FACreditsEarned") += 0
                                        End If
                                    Else
                                        ''Earn credits only if, credits earned is checked.
                                        If resultDr("IsCreditsEarned").ToString = True Then
                                            dr("CreditsEarned") += resultDr("Credits")
                                            dr("FACreditsEarned") += resultDr("FinAidCredits")
                                        Else
                                            dr("CreditsEarned") += 0
                                            dr("FACreditsEarned") += 0
                                        End If
                                    End If
                                Else
                                    If (myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                        If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                            Dim boolIsCourseALab As Boolean
                                            boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDr("ReqId").ToString)
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    decCredits = dbTranscript.ComputeWithCreditsPerService(stuEnrollId, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDr("ReqId").ToString)
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                dr("CreditsEarned") += decCredits
                                                dr("FACreditsEarned") += resultDr("FinAidCredits")
                                            Else
                                                dr("CreditsEarned") += 0
                                                dr("FACreditsEarned") += 0
                                            End If
                                        Else
                                            ''Earn credits only if, credits earned is checked.
                                            If resultDr("IsCreditsEarned").ToString = True Then
                                                dr("CreditsEarned") += resultDr("Credits")
                                                dr("FACreditsEarned") += resultDr("FinAidCredits")
                                            Else
                                                dr("CreditsEarned") += 0
                                                dr("FACreditsEarned") += 0
                                            End If
                                        End If
                                    Else
                                        dr("CreditsEarned") += 0.0
                                        dr("FACreditsEarned") += 0
                                    End If
                                End If

                            Else
                                If (myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                    ''Earn credits only if, credits earned is checked.
                                    If resultDr("IsCreditsEarned").ToString = True Then
                                        dr("CreditsEarned") += resultDr("Credits")
                                        dr("FACreditsEarned") += resultDr("FinAidCredits")
                                    Else
                                        dr("CreditsEarned") += 0
                                        dr("FACreditsEarned") += 0
                                    End If
                                Else
                                    dr("CreditsEarned") += 0.0
                                    dr("FACreditsEarned") += 0
                                End If
                                If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                    Dim boolIsCourseALab As Boolean
                                    boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDr("ReqId").ToString)
                                    If boolIsCourseALab = True Then
                                        Dim decCredits As Decimal
                                        Try
                                            decCredits = dbTranscript.ComputeWithCreditsPerService(stuEnrollId, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDr("ReqId").ToString)
                                        Catch ex As Exception
                                            decCredits = 0
                                        End Try
                                        dr("CreditsEarned") += decCredits
                                        dr("FACreditsEarned") += resultDr("FinAidCredits")
                                    Else
                                        dr("CreditsEarned") += 0
                                        dr("FACreditsEarned") += 0
                                    End If
                                End If
                            End If
                        Else
                            'Course does not belong to program version definition
                            'Adjust number of courses taken on this term
                            dr("TakenCourses") -= 1
                        End If
                    Next
                    '   Compute TermGPA 
                    '   Select rows from dtResults for current term and compute term GPA.
                    oneTerm = "(TermId='" & dr("TermId").ToString & "' AND IsInGPA=True AND IsCreditsAttempted=True)"
                    arrRows = dtResults.Select(oneTerm)
                    For Each dRow In arrRows
                        dtIn.ImportRow(dRow)
                    Next
                    'if there are repeated courses in same Term we should apply retaken policy
                    '  -
                    '   if Best only select the best GPA remove othes from arrRows
                    '   if Last olly select the last CourseEndDate, CourseStartDate
                    '   if Average  get average GPA and change the GPA of Each for Average
                    '--> so  correct arrRows
                    dtGPACalc = RetakenPolicyFilter(dtIn, gradeReps)
                    arrRows = dtGPACalc.Select()
                    dr("TermGPA") = ComputeGPA(arrRows, PrgVerId, True)
                    dtIn.Rows.Clear()
                    dtGPACalc.Rows.Clear()
                    '   Compute cummulative GPA
                    '   Select rows from dtResults for cummulative GPA
                    oneTerm = "(TermId='" & dr("TermId").ToString & "' AND IsInGPA=True AND IsCreditsAttempted=True)"
                    arrRows = dtResults.Select(oneTerm)
                    AddCoursesToCummProgress(dtCummProgress, arrRows)
                    dtGPACalc = RetakenPolicyFilter(dtCummProgress, gradeReps)
                    dr("GPA") = ComputeGPA(dtGPACalc.Select("GPA NOT IS NULL"), PrgVerId, True)
                    dtGPACalc.Rows.Clear()
                Else
                    For Each resultDr As DataRow In arrRows
                        'Verify that course belong to program version definition:
                        '       Look for course in the Graduate Audit table.
                        tmpRows = dtGraduateAudit.Select("ReqId='" & resultDr("ReqId").ToString & "'")

                        If tmpRows.GetLength(0) > 0 Then
                            Dim isPass As Boolean = brRegisterStudent.GrdOverRide(tmpRows(0)("PrgVerId").ToString(), resultDr("reqId").ToString, resultDr("Grade").ToString)
                            'Course belongs to program version definition
                            '
                            '
                            If Not IsDBNull(resultDr("IsCreditsAttempted")) Then
                                If resultDr("IsCreditsAttempted") = True Or resultDr("IsCreditsAttempted") = 1 Then
                                    If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                        Dim boolIsCourseALab As Boolean
                                        boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDr("ReqId").ToString)
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal
                                            Try
                                                decCredits = dbTranscript.ComputeWithCreditsPerService(stuEnrollId, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDr("ReqId").ToString)
                                            Catch ex As Exception
                                                decCredits = 0
                                            End Try
                                            dr("CreditsAttempted") += decCredits
                                        Else
                                            dr("CreditsAttempted") += 0
                                        End If
                                    Else
                                        dr("CreditsAttempted") += resultDr("Credits")
                                    End If
                                Else
                                    dr("CreditsAttempted") += 0.0
                                End If
                            Else
                                If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                    Dim boolIsCourseALab As Boolean
                                    boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDr("ReqId").ToString)
                                    If boolIsCourseALab = True Then
                                        Dim decCredits As Decimal
                                        Try
                                            decCredits = dbTranscript.ComputeWithCreditsPerService(stuEnrollId, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDr("ReqId").ToString)
                                        Catch ex As Exception
                                            decCredits = 0
                                        End Try
                                        dr("CreditsAttempted") += decCredits
                                    Else
                                        dr("CreditsAttempted") += 0
                                    End If
                                Else
                                    dr("CreditsAttempted") += resultDr("Credits")
                                End If
                            End If

                            If isPass = True Then

                                ' ''' ''Call the function to add credits earned
                                Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDr("TestId").ToString())
                                If isHrsComp = True Then
                                    If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                        Dim boolIsCourseALab As Boolean
                                        boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDr("ReqId").ToString)
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal
                                            Try
                                                decCredits = dbTranscript.ComputeWithCreditsPerService(stuEnrollId, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDr("ReqId").ToString)
                                            Catch ex As Exception
                                                decCredits = 0
                                            End Try
                                            dr("CreditsEarned") += decCredits
                                            dr("FACreditsEarned") += resultDr("FinAidCredits")
                                        Else
                                            dr("CreditsEarned") += 0
                                            dr("FACreditsEarned") += 0
                                        End If
                                    Else
                                        ''Earn credits only if, credits earned is checked.
                                        If resultDr("IsCreditsEarned").ToString = True Then
                                            dr("CreditsEarned") += resultDr("Credits")
                                            dr("FACreditsEarned") += resultDr("FinAidCredits")
                                        Else
                                            dr("CreditsEarned") += 0
                                            dr("FACreditsEarned") += 0
                                        End If

                                    End If
                                Else
                                    If (myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                        If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                            Dim boolIsCourseALab As Boolean
                                            boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDr("ReqId").ToString)
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    decCredits = dbTranscript.ComputeWithCreditsPerService(stuEnrollId, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDr("ReqId").ToString)
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                dr("CreditsEarned") += decCredits
                                                dr("FACreditsEarned") += resultDr("FinAidCredits")
                                            Else
                                                dr("CreditsEarned") += 0
                                                dr("FACreditsEarned") += 0
                                            End If
                                        Else
                                            ''Earn credits only if, credits earned is checked.
                                            If resultDr("IsCreditsEarned").ToString = True Then
                                                dr("CreditsEarned") += resultDr("Credits")
                                                dr("FACreditsEarned") += resultDr("FinAidCredits")
                                            Else
                                                dr("CreditsEarned") += 0
                                                dr("FACreditsEarned") += 0
                                            End If
                                        End If
                                    Else
                                        dr("CreditsEarned") += 0.0
                                        dr("FACreditsEarned") += 0
                                    End If
                                End If

                            Else
                                If Not resultDr("GrdSysDetailId") Is DBNull.Value And (myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then

                                    ''Earn credits only if, credits earned is checked.
                                    If resultDr("IsCreditsEarned").ToString = True Then
                                        dr("CreditsEarned") += resultDr("Credits")
                                        dr("FACreditsEarned") += resultDr("FinAidCredits")
                                    Else
                                        dr("CreditsEarned") += 0
                                        dr("FACreditsEarned") += 0
                                    End If
                                Else
                                    dr("CreditsEarned") += 0.0
                                    dr("FACreditsEarned") += 0
                                End If
                                If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                    Dim boolIsCourseALab As Boolean
                                    boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDr("ReqId").ToString)
                                    If boolIsCourseALab = True Then
                                        Dim decCredits As Decimal
                                        Try
                                            decCredits = dbTranscript.ComputeWithCreditsPerService(stuEnrollId, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDr("ReqId").ToString)
                                        Catch ex As Exception
                                            decCredits = 0
                                        End Try
                                        dr("CreditsEarned") += decCredits
                                        dr("FACreditsEarned") += resultDr("FinAidCredits")
                                    Else
                                        dr("CreditsEarned") += 0
                                        dr("FACreditsEarned") += 0
                                    End If
                                End If
                            End If
                        Else
                            'Course does not belong to program version definition
                            'Adjust number of courses taken on this term
                            equivReqid = GetEquivalentReqId(resultDr("ReqId").ToString).ToString
                            If equivReqid <> "" Then
                                tmpRows = dtGraduateAudit.Select("ReqId='" & equivReqid.ToString & "'")
                                'tmpRows = dtGraduateAudit.Select("ReqId='" & resultDr("ReqId").ToString & "'")
                                If tmpRows.GetLength(0) > 0 Then
                                    'Dim IsPass As Boolean = br.GrdOverRide(tmpRows(0)("PrgVerId").ToString(), equivReqid.ToString, resultDR("Grade").ToString)
                                    Dim isPass As Boolean = brRegisterStudent.GrdOverRide(tmpRows(0)("PrgVerId").ToString(), resultDr("ReqId").ToString, resultDr("Grade").ToString)
                                    'code modified by balaji on 04/06/2009 mantis:15802
                                    'modification starts here
                                    If Not IsDBNull(resultDr("IsCreditsAttempted")) Then
                                        If resultDr("IsCreditsAttempted") = True Or resultDr("IsCreditsAttempted") = 1 Then
                                            If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                                Dim boolIsCourseALab As Boolean
                                                boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDr("ReqId").ToString)
                                                If boolIsCourseALab = True Then
                                                    Dim decCredits As Decimal
                                                    Try
                                                        decCredits = dbTranscript.ComputeWithCreditsPerService(stuEnrollId, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDr("ReqId").ToString)
                                                    Catch ex As Exception
                                                        decCredits = 0
                                                    End Try
                                                    dr("CreditsAttempted") += decCredits
                                                Else
                                                    dr("CreditsAttempted") += 0
                                                End If
                                            Else
                                                dr("CreditsAttempted") += resultDr("Credits")
                                            End If
                                        Else
                                            dr("CreditsAttempted") += 0.0
                                        End If
                                    End If
                                    'modification ends here

                                    If isPass = True Then
                                        ' ''' ''Call the function to add credits earned
                                        Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDr("TestId").ToString())
                                        If isHrsComp = True Then
                                            If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                                Dim boolIsCourseALab As Boolean
                                                boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDr("ReqId").ToString)
                                                If boolIsCourseALab = True Then
                                                    Dim decCredits As Decimal
                                                    Try
                                                        decCredits = dbTranscript.ComputeWithCreditsPerService(stuEnrollId, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDr("ReqId").ToString)
                                                    Catch ex As Exception
                                                        decCredits = 0
                                                    End Try
                                                    dr("CreditsEarned") += decCredits
                                                    dr("FACreditsEarned") += resultDr("FinAidCredits")
                                                Else
                                                    dr("CreditsEarned") += 0
                                                    dr("FACreditsEarned") += 0
                                                End If
                                            Else
                                                ''Added by saraswathi lakshmanan on May 06 2009
                                                ''Earn credits only if, credits earned is checked.
                                                If resultDr("IsCreditsEarned").ToString = True Then
                                                    dr("CreditsEarned") += resultDr("Credits")
                                                    dr("FACreditsEarned") += resultDr("FinAidCredits")
                                                Else
                                                    dr("CreditsEarned") += 0
                                                    dr("FACreditsEarned") += 0
                                                End If
                                            End If
                                        Else
                                            If (myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                                If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                                    Dim boolIsCourseALab As Boolean
                                                    boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDr("ReqId").ToString)
                                                    If boolIsCourseALab = True Then
                                                        Dim decCredits As Decimal
                                                        Try
                                                            decCredits = dbTranscript.ComputeWithCreditsPerService(stuEnrollId, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDr("ReqId").ToString)
                                                        Catch ex As Exception
                                                            decCredits = 0
                                                        End Try
                                                        dr("CreditsEarned") += decCredits
                                                        dr("FACreditsEarned") += resultDr("FinAidCredits")
                                                    Else
                                                        dr("CreditsEarned") += 0
                                                        dr("FACreditsEarned") += 0
                                                    End If
                                                Else
                                                    ''Earn credits only if, credits earned is checked.
                                                    If resultDr("IsCreditsEarned").ToString = True Then
                                                        dr("CreditsEarned") += resultDr("Credits")
                                                        dr("FACreditsEarned") += resultDr("FinAidCredits")
                                                    Else
                                                        dr("CreditsEarned") += 0
                                                        dr("FACreditsEarned") += 0
                                                    End If
                                                End If
                                            Else
                                                dr("CreditsEarned") += 0.0
                                                dr("FACreditsEarned") += 0
                                            End If
                                        End If

                                    Else
                                        If Not resultDr("GrdSysDetailId") Is DBNull.Value And (myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                            ''Added by saraswathi lakshmanan on May 06 2009
                                            ''Earn credits only if, credits earned is checked.
                                            If resultDr("IsCreditsEarned").ToString = True Then
                                                dr("CreditsEarned") += resultDr("Credits")
                                                dr("FACreditsEarned") += resultDr("FinAidCredits")
                                            Else
                                                dr("CreditsEarned") += 0
                                                dr("FACreditsEarned") += 0
                                            End If
                                        Else
                                            dr("CreditsEarned") += 0.0
                                            dr("FACreditsEarned") += 0
                                        End If
                                        If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                            Dim boolIsCourseALab As Boolean
                                            boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDr("ReqId").ToString)
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    decCredits = dbTranscript.ComputeWithCreditsPerService(stuEnrollId, myAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDr("ReqId").ToString)
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                dr("CreditsEarned") += decCredits
                                                dr("FACreditsEarned") += resultDr("FinAidCredits")
                                            Else
                                                dr("CreditsEarned") += 0
                                                dr("FACreditsEarned") += 0
                                            End If
                                        End If
                                    End If
                                Else
                                    dr("TakenCourses") -= 1
                                End If
                            Else
                                dr("TakenCourses") -= 1
                            End If
                        End If
                    Next
                    '   Compute TermGPA 
                    '   Select rows from dtResults for current term and compute term GPA.
                    oneTerm = "(TermId='" & dr("TermId").ToString & "' AND IsInGPA=True AND IsCreditsAttempted=True)"
                    arrRows = dtResults.Select(oneTerm)
                    For Each dRow In arrRows
                        dtIn.ImportRow(dRow)
                    Next
                    'if there are repeated courses in same Term we should apply retaken policy
                    '  -
                    '   if Best only select the best GPA remove othes from arrRows
                    '   if Last olly select the last CourseEndDate, CourseStartDate
                    '   if Average  get average GPA and change the GPA of Each for Average
                    '--> so  correct arrRows
                    dtGPACalc = RetakenPolicyFilter(dtIn, gradeReps)
                    arrRows = dtGPACalc.Select()
                    dr("TermGPA") = ComputeGPA(arrRows, PrgVerId, True)
                    dtIn.Rows.Clear()
                    dtGPACalc.Rows.Clear()
                    '   Compute cummulative GPA
                    '   Select rows from dtResults for  compute cummulative GPA
                    oneTerm = "(TermId='" & dr("TermId").ToString & "' AND IsInGPA=True AND IsCreditsAttempted=True)"
                    arrRows = dtResults.Select(oneTerm)
                    AddCoursesToCummProgress(dtCummProgress, arrRows)
                    dtGPACalc = RetakenPolicyFilter(dtCummProgress, gradeReps)
                    dr("GPA") = ComputeGPA(dtGPACalc.Select("GPA NOT IS NULL"), PrgVerId, True)
                    dtGPACalc.Rows.Clear()
                End If
            End If
        Next

        'At this point we can full in the cum credits attempted field
        For Each dr2 As DataRow In dtTermProgress.Rows
            dr2("CumCreditsAttempted") = dtTermProgress.Compute("SUM(CreditsAttempted)", "EndDate <= '" & dr2("EndDate") & "'")
        Next

        Return dtTermProgress
    End Function
    Public Function GetTermProgressFromResults_SP(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal campusID As String = "") As DataSet
        'Dim br As New RegisterStudentsBR
        Dim ds As DataSet
        Dim rtnDs As New DataSet
        Dim PrgVerId As Guid

        If stuEnrollId <> "" Then
            PrgVerId = (New ProgVerDB()).GetPrgVerIdForAGivenStuEnrollId(stuEnrollId)
        End If

        If stuEnrollId <> "" Then
            If myAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric" Then

                ds = dbTranscript.GetGraduateAuditByEnrollmentForNumericForCourseEquivalent_SP(stuEnrollId, , termCond, classCond)

            ElseIf dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then

                ds = dbTranscript.GetGraduateAuditByCEProgEnrollmentForCourseEquivalent_SP(stuEnrollId, , termCond, classCond)

            Else

                ds = dbTranscript.GetGraduateAuditByEnrollmentForCourseEquivalent_SP(stuEnrollId, , termCond, classCond)

            End If


            For Each drRow As DataRow In ds.Tables("Results").Rows
                drRow("IsCourseALab") = isCourseALabWorkOrLabHourCourse(drRow("ReqId").ToString)
                If drRow("IsCourseALab") Then
                    drRow("decCredits") = dbTranscript.ComputeWithCreditsPerService_SP(stuEnrollId, drRow("ReqId").ToString)
                End If
            Next
            ds.AcceptChanges()
            dtGraduateAudit = BuildGraduateAuditDT_SP(ds, PrgVerId, gradeReps, includeHours)
            ' Code Added by vijay Ramteke on May, 06 2009
            'dtResults = ds.Tables("Results")
            dtResults = dbTranscript.GetDTResultsLetter(stuEnrollId, termCond, classCond)

            '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
            If myAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric" Then
                GetAverageScore()
            Else
                GetAverageGpa()
            End If

            ''''''

            SetProperties()
        End If

        rtnDs.Tables.Add(dtResults.Copy())
        '
        '
        '
        Dim arrRows() As DataRow
        Dim dr As DataRow
        Dim dtTermProgress As New DataTable
        Dim dtCummProgress As New DataTable
        '
        '   structure of the TermProgress table
        With dtTermProgress
            .Columns.Add("TermId", Type.GetType("System.Guid"))
            .Columns.Add("TermDescrip", Type.GetType("System.String"))
            .Columns.Add("DescripXTranscript", Type.GetType("System.String"))
            .Columns.Add("StartDate", Type.GetType("System.DateTime"))
            .Columns.Add("EndDate", Type.GetType("System.DateTime"))
            .Columns.Add("TakenCourses", Type.GetType("System.Int32"))
            .Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
            .Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
            .Columns.Add("TermGPA", Type.GetType("System.Decimal"))
            .Columns.Add("GPA", Type.GetType("System.Decimal"))
            .Columns.Add("Score", Type.GetType("System.Decimal"))
        End With
        '
        '
        '   if the GradAudit is empty, return an empty TermProgress table
        If dtGraduateAudit.Rows.Count = 0 Then
            'Return dtTermProgress
            Return rtnDs
        End If
        '
        '
        '
        '   structure of the CummProgress table
        With dtCummProgress
            .Columns.Add("ReqId", Type.GetType("System.String"))
            .Columns.Add("ReqTypeId", Type.GetType("System.String"))
            .Columns.Add("Credits", Type.GetType("System.Decimal"))
            .Columns.Add("IsCreditsEarned", Type.GetType("System.Boolean"))
            .Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
            .Columns.Add("IsCreditsAttempted", Type.GetType("System.Boolean"))
            .Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
            .Columns.Add("GPA", Type.GetType("System.Decimal"))
            .Columns.Add("Score", Type.GetType("System.Decimal"))
        End With
        '
        '
        '********************************************************************************
        '**** Massage GraduateAudit and Results table to produce TermProgress table. ****
        '********************************************************************************
        '
        '   add DISTINCT Terms into TermProgress table
        If dtGraduateAudit.Rows.Count > 0 And dtResults.Rows.Count > 0 Then
            Dim drs() As DataRow = dtResults.Select("", "StartDate")

            ' For Each dr In dtResults.Rows
            For Each dr In drs
                If Not dr.IsNull("TermId") Then
                    If Not IsValueAlreadyInTable(dr("TermId").ToString, "TermId", dtTermProgress) Then
                        '   add row into TermProgress table
                        Dim row As DataRow
                        row = dtTermProgress.NewRow
                        row("TermId") = dr("TermId")
                        row("TermDescrip") = dr("TermDescrip")
                        row("DescripXTranscript") = dr("DescripXTranscript")
                        row("StartDate") = dr("StartDate")
                        row("EndDate") = dr("EndDate")
                        dtTermProgress.Rows.Add(row)
                    End If
                End If
            Next
        End If
        '
        '
        '   CreditsEarned and CreditsAttempted for each course on every particular term
        '   TermGPA and Cummulative GPA for every term
        'Dim cummGPA As Decimal
        'Dim courseCtr As Integer
        Dim oneTerm As String
        Dim cummTerms As New StringBuilder
        Dim equivReqid As String
        'Dim rrows() As DataRow
        Dim tmpRows() As DataRow

        For Each dr In dtTermProgress.Rows
            arrRows = dtResults.Select("TermId='" & dr("TermId").ToString & "'")

            If arrRows.GetLength(0) > 0 Then

                dr("TakenCourses") = arrRows.GetLength(0)
                dr("CreditsEarned") = 0
                dr("CreditsAttempted") = 0
                If (myAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric") Then
                    For Each resultDr As DataRow In arrRows

                        'Verify that course belong to program version definition:
                        '       Look for course in the Graduate Audit table.
                        tmpRows = dtGraduateAudit.Select("ReqId='" & resultDr("ReqId").ToString & "'")

                        If tmpRows.GetLength(0) > 0 Then
                            Dim isPass As Boolean = brRegisterStudent.GrdOverRide_SP(tmpRows(0)("PrgVerId").ToString(), resultDr("reqId").ToString, resultDr("Grade").ToString)
                            'Dim IsPass As Boolean = tmpRows(0)("IsPass")
                            If Not IsDBNull(resultDr("IsCreditsAttempted")) Then
                                If resultDr("IsCreditsAttempted") = True Or resultDr("IsCreditsAttempted") = 1 Then
                                    If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                        Dim boolIsCourseALab As Boolean
                                        'boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                        boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal
                                            decCredits = tmpRows(0)("decCredits")

                                            dr("CreditsAttempted") += decCredits
                                        Else
                                            dr("CreditsAttempted") += 0
                                        End If
                                    Else
                                        dr("CreditsAttempted") += resultDr("Credits")
                                    End If
                                Else
                                    dr("CreditsAttempted") += 0.0
                                End If
                            End If


                            If isPass = True Then

                                ' ''' ''Call the function to add credits earned
                                Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDr("TestId").ToString())
                                If isHrsComp = True Then
                                    If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                        Dim boolIsCourseALab As Boolean
                                        'boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                        boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal
                                            decCredits = tmpRows(0)("decCredits")

                                            dr("CreditsEarned") += decCredits
                                        Else
                                            dr("CreditsEarned") += 0
                                        End If
                                    Else
                                        ''Earn credits only if, credits earned is checked.
                                        If resultDr("IsCreditsEarned").ToString = True Then
                                            dr("CreditsEarned") += resultDr("Credits")
                                        Else
                                            dr("CreditsEarned") += 0
                                        End If
                                    End If
                                Else
                                    If (myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                        If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                            Dim boolIsCourseALab As Boolean
                                            'boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                            boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                decCredits = tmpRows(0)("decCredits")

                                                dr("CreditsEarned") += decCredits
                                            Else
                                                dr("CreditsEarned") += 0
                                            End If
                                        Else
                                            ''Added by saraswathi lakshmanan on May 06 2009
                                            ''Earn credits only if, credits earned is checked.
                                            If resultDr("IsCreditsEarned").ToString = True Then
                                                dr("CreditsEarned") += resultDr("Credits")
                                            Else
                                                dr("CreditsEarned") += 0
                                            End If
                                        End If
                                    Else
                                        dr("CreditsEarned") += 0.0
                                    End If
                                End If


                            Else
                                If (myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                    ''Added by saraswathi lakshmanan on May 06 2009
                                    ''Earn credits only if, credits earned is checked.
                                    If resultDr("IsCreditsEarned").ToString = True Then
                                        dr("CreditsEarned") += resultDr("Credits")
                                    Else
                                        dr("CreditsEarned") += 0
                                    End If
                                Else
                                    dr("CreditsEarned") += 0.0
                                End If
                                If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                    Dim boolIsCourseALab As Boolean
                                    ' boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                    boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                    If boolIsCourseALab = True Then
                                        Dim decCredits As Decimal
                                        decCredits = tmpRows(0)("decCredits")

                                        dr("CreditsEarned") += decCredits
                                    Else
                                        dr("CreditsEarned") += 0
                                    End If
                                End If
                            End If
                        Else
                            'Course does not belong to program version definition
                            'Adjust number of courses taken on this term
                            dr("TakenCourses") -= 1
                        End If
                    Next

                    '   Compute TermGPA and cummulative GPA
                    '   Select rows from dtResults for current term and compute term GPA.
                    oneTerm = "(TermId='" & dr("TermId").ToString & "' AND IsInGPA=True AND IsCreditsAttempted=True)"
                    arrRows = dtResults.Select(oneTerm)
                    dr("TermGPA") = ComputeGPA(arrRows, PrgVerId, True)

                    '   Compute cummulative GPA
                    AddCoursesToCummProgress_SP(dtCummProgress, arrRows, gradeReps)

                    dr("GPA") = ComputeGPA(dtCummProgress.Select("GPA NOT IS NULL"), PrgVerId, True)
                Else
                    For Each resultDr As DataRow In arrRows

                        'Verify that course belong to program version definition:
                        '       Look for course in the Graduate Audit table.
                        tmpRows = dtGraduateAudit.Select("ReqId='" & resultDr("ReqId").ToString & "'")

                        If tmpRows.GetLength(0) > 0 Then
                            Dim isPass As Boolean = brRegisterStudent.GrdOverRide_SP(tmpRows(0)("PrgVerId").ToString(), resultDr("reqId").ToString, resultDr("Grade").ToString)
                            'Dim IsPass As Boolean = tmpRows(0)("IsPass")

                            If isPass = True Then

                                ''Code added by Saraswathi on April 6 2009
                                ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                ' ''' ''Call the function to add credits earned
                                Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDr("TestId").ToString())
                                If isHrsComp = True Then
                                    If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                        Dim boolIsCourseALab As Boolean
                                        ' boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                        boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal
                                            decCredits = tmpRows(0)("decCredits")

                                            dr("CreditsEarned") += decCredits
                                        Else
                                            dr("CreditsEarned") += 0
                                        End If
                                    Else
                                        ''Added by saraswathi lakshmanan on May 06 2009
                                        ''Earn credits only if, credits earned is checked.
                                        If resultDr("IsCreditsEarned").ToString = True Then
                                            dr("CreditsEarned") += resultDr("Credits")
                                        Else
                                            dr("CreditsEarned") += 0
                                        End If

                                    End If
                                Else
                                    If (myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                        If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                            Dim boolIsCourseALab As Boolean
                                            'boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                            boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                decCredits = tmpRows(0)("decCredits")

                                                dr("CreditsEarned") += decCredits
                                            Else
                                                dr("CreditsEarned") += 0
                                            End If
                                        Else
                                            ''Added by saraswathi lakshmanan on May 06 2009
                                            ''Earn credits only if, credits earned is checked.
                                            If resultDr("IsCreditsEarned").ToString = True Then
                                                dr("CreditsEarned") += resultDr("Credits")
                                            Else
                                                dr("CreditsEarned") += 0
                                            End If
                                        End If
                                    Else
                                        dr("CreditsEarned") += 0.0
                                    End If
                                End If

                                'dr("Credits") = resultDR("Credits")
                                'dr("Hours") = resultDR("Hours")
                            Else
                                If Not resultDr("GrdSysDetailId") Is DBNull.Value And (myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then

                                    ''Earn credits only if, credits earned is checked.
                                    If resultDr("IsCreditsEarned").ToString = True Then
                                        dr("CreditsEarned") += resultDr("Credits")
                                    Else
                                        dr("CreditsEarned") += 0
                                    End If
                                Else
                                    dr("CreditsEarned") += 0.0
                                End If
                                If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                    Dim boolIsCourseALab As Boolean
                                    'boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                    boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                    If boolIsCourseALab = True Then
                                        Dim decCredits As Decimal
                                        decCredits = tmpRows(0)("decCredits")

                                        dr("CreditsEarned") += decCredits
                                    Else
                                        dr("CreditsEarned") += 0
                                    End If
                                End If
                            End If
                        Else
                            'Course does not belong to program version definition
                            'Adjust number of courses taken on this term
                            equivReqid = GetEquivalentReqId(resultDr("ReqId").ToString).ToString
                            If equivReqid <> "" Then
                                'tmpRows = dtGraduateAudit.Select("ReqId='" & equivReqid.ToString & "'")
                                tmpRows = dtGraduateAudit.Select("ReqId='" & resultDr("ReqId").ToString & "'")
                                If tmpRows.GetLength(0) > 0 Then
                                    'Dim IsPass As Boolean = br.GrdOverRide(tmpRows(0)("PrgVerId").ToString(), equivReqid.ToString, resultDR("Grade").ToString)
                                    Dim isPass As Boolean = brRegisterStudent.GrdOverRide_SP(tmpRows(0)("PrgVerId").ToString(), resultDr("ReqId").ToString, resultDr("Grade").ToString)
                                    'Dim IsPass As Boolean = tmpRows(0)("IsPass")
                                    'code modified by balaji on 04/06/2009 mantis:15802
                                    'modification starts here
                                    If Not IsDBNull(resultDr("IsCreditsAttempted")) Then
                                        If resultDr("IsCreditsAttempted") = True Or resultDr("IsCreditsAttempted") = 1 Then
                                            If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                                Dim boolIsCourseALab As Boolean
                                                ' boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                                boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                                If boolIsCourseALab = True Then
                                                    Dim decCredits As Decimal
                                                    decCredits = tmpRows(0)("decCredits")

                                                    dr("CreditsAttempted") += decCredits
                                                Else
                                                    dr("CreditsAttempted") += 0
                                                End If
                                            Else
                                                dr("CreditsAttempted") += resultDr("Credits")
                                            End If
                                        Else
                                            dr("CreditsAttempted") += 0.0
                                        End If
                                    End If

                                    If isPass = True Then

                                        ''Code added by Saraswathi on April 6 2009
                                        ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                        ' ''' ''Call the function to add credits earned
                                        Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDr("TestId").ToString())
                                        If isHrsComp = True Then
                                            If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                                Dim boolIsCourseALab As Boolean
                                                'boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                                boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                                If boolIsCourseALab = True Then
                                                    Dim decCredits As Decimal
                                                    decCredits = tmpRows(0)("decCredits")

                                                    dr("CreditsEarned") += decCredits
                                                Else
                                                    dr("CreditsEarned") += 0
                                                End If
                                            Else
                                                ''Earn credits only if, credits earned is checked.
                                                If resultDr("IsCreditsEarned").ToString = True Then
                                                    dr("CreditsEarned") += resultDr("Credits")
                                                Else
                                                    dr("CreditsEarned") += 0
                                                End If
                                            End If
                                        Else
                                            If (myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                                If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                                    Dim boolIsCourseALab As Boolean
                                                    ' boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                                    boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                                    If boolIsCourseALab = True Then
                                                        Dim decCredits As Decimal
                                                        decCredits = tmpRows(0)("decCredits")

                                                        dr("CreditsEarned") += decCredits
                                                    Else
                                                        dr("CreditsEarned") += 0
                                                    End If
                                                Else
                                                    ''Earn credits only if, credits earned is checked.
                                                    If resultDr("IsCreditsEarned").ToString = True Then
                                                        dr("CreditsEarned") += resultDr("Credits")
                                                    Else
                                                        dr("CreditsEarned") += 0
                                                    End If
                                                End If
                                            Else
                                                dr("CreditsEarned") += 0.0
                                            End If
                                        End If

                                    Else
                                        If Not resultDr("GrdSysDetailId") Is DBNull.Value And (myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                            ''Earn credits only if, credits earned is checked.
                                            If resultDr("IsCreditsEarned").ToString = True Then
                                                dr("CreditsEarned") += resultDr("Credits")
                                            Else
                                                dr("CreditsEarned") += 0
                                            End If
                                        Else
                                            dr("CreditsEarned") += 0.0
                                        End If
                                        If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                            Dim boolIsCourseALab As Boolean
                                            ' boolIsCourseALab = dbTransferGrade.isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                            boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                decCredits = tmpRows(0)("decCredits")

                                                dr("CreditsEarned") += decCredits
                                            Else
                                                dr("CreditsEarned") += 0
                                            End If
                                        End If
                                    End If
                                Else
                                    dr("TakenCourses") -= 1
                                End If
                            Else
                                dr("TakenCourses") -= 1
                            End If

                        End If
                    Next

                    '   Compute TermGPA and cummulative GPA
                    '   Select rows from dtResults for current term and compute term GPA.
                    oneTerm = "(TermId='" & dr("TermId").ToString & "' AND IsInGPA=True AND IsCreditsAttempted=True)"
                    arrRows = dtResults.Select(oneTerm)
                    dr("TermGPA") = ComputeGPA(arrRows, PrgVerId, True)

                    '   Compute cummulative GPA
                    AddCoursesToCummProgress_SP(dtCummProgress, arrRows, gradeReps)

                    'dr("GPA") = ComputeCumGPA(dtCummProgress)
                    dr("GPA") = ComputeGPA(dtCummProgress.Select("GPA NOT IS NULL"), PrgVerId, True)

                End If


            End If
        Next
        rtnDs.Tables.Add(dtTermProgress)
        Return rtnDs

    End Function

#End Region

    Public Function RemoveDuplicateRows(ByVal dTable As DataTable, ByVal colName As String) As DataTable
        Dim hTable As New Hashtable
        Dim duplicateList As New ArrayList
        For Each drow As DataRow In dTable.Rows
            If hTable.Contains(drow(colName)) Then
                duplicateList.Add(drow)
            Else
                hTable.Add(drow(colName), String.Empty)
            End If
        Next

        For Each drow As DataRow In duplicateList
            dTable.Rows.Remove(drow)
        Next
        Return dTable
    End Function
    Public Function GetEquivalentCourseDescrip(ByVal Reqid As String, ByVal stuEnrollId As String)
        Return dbTranscript.GetEquivalentCourseDescrip(Reqid, stuEnrollId)
    End Function
    Public Function GetEquivalentReqId(ByVal Reqid As String) As String
        Return dbTranscript.GetEquivalentReqId(Reqid)
    End Function
    Public Function GetEquivalentReqId_SP(ByVal Reqid As String) As String
        Return dbTranscript.GetEquivalentReqId_SP(Reqid)
    End Function
    ' Added for Multile Student Attendance
    Public Function GetStudentHours(ByVal stuEnrollid, ByVal Otherwhere)
        Dim dbTranscript As New TranscriptDB
        Dim ds As DataSet
        ds = dbTranscript.GetHours(stuEnrollid, Otherwhere)
        Dim dtGraduateAuditl As DataTable = BuildStudentAttendance(ds)
        Return dtGraduateAuditl

    End Function
    ' Added  for Multiple Student Attendance
    Public Function BuildStudentAttendance(ByVal ds As DataSet) As DataTable
        Dim dt As New DataTable("MultiStudentAttend")
        dt = ds.Tables(0)
        dt.TableName = "MultiStudentAttend"
        Return dt
    End Function
    Public Function isCourseALabWorkOrLabHourCourseCombination(ByVal ReqId As String) As Boolean
        Return dbTransferGrade.isCourseALabWorkOrLabHourCourseCombination(ReqId)
    End Function
    Public Function isCourseALabWorkOrLabHourCourseCombination_SP(ByVal ReqId As String) As Boolean
        Return dbTransferGrade.isCourseALabWorkOrLabHourCourseCombination_SP(ReqId)
    End Function
    Public Function isCourseALabWorkOrLabHourCourse(ByVal ReqId As String) As Boolean
        Return dbTransferGrade.isCourseALabWorkOrLabHourCourse_SP(ReqId)
    End Function
    Public Function ComputeWithCreditsPerService(ByVal StuEnrollId As String, ByVal addcreditsbyservice As String, ByVal CourseId As String) As Decimal
        Return dbTranscript.ComputeWithCreditsPerService(StuEnrollId, addcreditsbyservice, CourseId)
    End Function
    Public Function GetEquivalentReqIdDS(ByVal reqid As String) As DataSet
        Return dbTranscript.GetEquivalentReqIdDS(reqid)
    End Function
    Public Function GetEquivalentReqIdDS_SP(ByVal reqid As String) As DataTable
        Return dbTranscript.GetEquivalentReqIdDS_SP(reqid)
    End Function
    Public Function CheckandListRequiredDocuments_ForGraduation(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListGradDocumentRequirements_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    strdocs.Append(row("Descrip") + ";")
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function
    Public Function CheckandListRequiredTest_ForEnrollment(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListEnrollmentTestRequirements_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    strdocs.Append(row("Descrip") + ";")
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function
    Public Function CheckandListRequiredtest_ForGraduation(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListGradTestRequirements_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    strdocs.Append(row("Descrip") + ";")
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If

    End Function
    Public Function CheckandListEnrollreqGroups(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListEnrollReqGroups_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    If row("Descrip") = "School Level Requirements" Then
                        strdocs.Append("-")
                    Else
                        strdocs.Append(row("Descrip") + ";")
                    End If
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If

    End Function
    Public Function CheckandListGradreqGroups(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListGradReqGroups_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    If row("Descrip") = "School Level Requirements" Then
                        strdocs.Append("-")
                    Else
                        strdocs.Append(row("Descrip") + ";")
                    End If

                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function
    ''--------------------------------------------------------------
    Public Function CheckandListRequiredDocuments_ForFinAid(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListFinAidDocumentRequirements_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    strdocs.Append(row("Descrip") + ";")
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function
    Public Function CheckandListRequiredtest_ForFinAid(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListFinAidTestRequirements_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    strdocs.Append(row("Descrip") + ";")
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function
    Public Function CheckandListFinAidreqGroups(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListFinAidReqGroups_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    If row("Descrip") = "School Level Requirements" Then
                        strdocs.Append("-")
                    Else
                        strdocs.Append(row("Descrip") + ";")
                    End If
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function
    Private Function GetCreditsEarned(ByVal dtWU As DataTable, ByVal score As Object) As Boolean
        Dim wuSatisfied As Boolean = True
        Dim i As Integer

        If dtWU.Rows.Count > 0 Then
            If IsDBNull(score) Then
                For i = 0 To dtWU.Rows.Count - 1

                    If dtWU.Rows(i)("Required") = True Or dtWU.Rows(i)("MustPass") = True Then
                        'If dtWU.Rows(i)("Required") = True Then
                        If IsDBNull(dtWU.Rows(i)("Score")) Then
                            wuSatisfied = False
                            Exit For
                        End If
                    End If

                Next

            ElseIf score >= 0 Then
                For i = 0 To dtWU.Rows.Count - 1
                    If IsDBNull(dtWU.Rows(i)("score")) Then
                        If dtWU.Rows(i)("Required") = True Or dtWU.Rows(i)("MustPass") = True Then
                            wuSatisfied = False
                            Exit For
                        Else
                            wuSatisfied = True
                        End If
                    ElseIf dtWU.Rows(i)("score") < dtWU.Rows(i)("MinResult") Then
                        'First check if it is required
                        'wuSatisfied = False
                        'Exit For
                        If dtWU.Rows(i)("Required") = True And dtWU.Rows(i)("MustPass") = True Then
                            wuSatisfied = False
                            Exit For
                        End If
                    End If
                Next


            End If
        End If
        If wuSatisfied Then
            'If the score is empty we can say right away that the course has been completed
            If (score.ToString = "" Or IsDBNull(score)) Then
                Return True
            Else
                'Check if the score is a passing score
                If ScoreIsAPass(score) Then
                    Return True
                Else
                    Return False
                End If
            End If
        End If
        Return wuSatisfied



    End Function
    Private Function ScoreIsAPass(ByVal csrScore As Decimal) As Boolean
        Dim db As New StuProgressReportDB
        Dim minPassScore As Decimal

        minPassScore = db.GetMinPassingScore

        If csrScore >= minPassScore Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Function GetGraduateAuditEnrollmentForCourseEquivalent(stuEnrollId As String, gradeReps As String, includeHours As Boolean, enrollment As List(Of String)) As List(Of GraduateAuditGroupDto)

        Dim listMaters As New List(Of GraduateAuditGroupDto)

        Dim headerDescription As String = "All Enrollments"

        If (stuEnrollId = Guid.Empty.ToString()) Then
            'This is the All Case
            'For each here take the resumed value and condense in one record.




            For Each enrollId As String In enrollment
                If (enrollId = Guid.Empty.ToString()) Then
                    Continue For
                End If
                Dim listEnroll As List(Of GraduateAuditGroupDto) = GetGraduateAuditByEnrollmentForCourseEquivalent(enrollId, gradeReps, includeHours)
                For Each groupDto As GraduateAuditGroupDto In listEnroll
                    Dim isNotthere As Boolean = listMaters.All(Function(mater) mater.ReqId <> groupDto.ReqId)
                    If isNotthere = True Then
                        listMaters.Add(groupDto)
                    End If
                Next
            Next
        Else

            Dim listEnroll As List(Of GraduateAuditGroupDto) = GetGraduateAuditByEnrollmentForCourseEquivalent(stuEnrollId, gradeReps, includeHours)

            For Each groupDto As GraduateAuditGroupDto In listEnroll
                If groupDto.Level Is Nothing Then
                    headerDescription = groupDto.Descrip
                End If
                listMaters.Add(groupDto)
            Next
        End If

        ' Unify Header in one.
        Dim header As GraduateAuditGroupDto = GraduateAuditGroupDto.Factory()
        header.Descrip = headerDescription
        For Each dto As GraduateAuditGroupDto In listMaters
            If Not dto.ReqTypeId Is Nothing Then
                If dto.ReqTypeId = 1 Then
                    'header.Credits += dto.Credits
                    header.CreditsEarned += dto.CreditsEarned
                    'header.CreditsRemaining += dto.CreditsRemaining
                    'header.Hours += dto.Hours
                    header.HoursCompleted += dto.HoursCompleted
                    'header.HoursRemaining += dto.HoursRemaining
                    header.Score += dto.Score
                    header.Level = Nothing
                End If
            End If

        Next

        For Each dto As GraduateAuditGroupDto In listMaters
            If dto.Level Is Nothing Then
                header.Credits = dto.Credits
                header.CreditsRemaining = dto.Credits - header.CreditsEarned
                header.Hours = dto.Hours
                header.HoursRemaining = dto.Hours - header.HoursCompleted
                header.Level = Nothing
            End If

        Next

        'Integrate in lisMaters
        listMaters.Insert(0, header)
        Return listMaters

    End Function
    Public Shared Function GetHeaderTitles(ByVal auditInfo As GraduateAuditInfo) As List(Of String)
        'Construct the Header Title for the Summary table
        Dim headerTitles As List(Of String) = New List(Of String)()
        Dim score As String = If(auditInfo.StrGradesFormat.ToLower = "letter", "Grade", "Score")
        If auditInfo.StuEnrollmentId <> Guid.Empty.ToString() Then
            headerTitles.Add("Description")
            headerTitles.Add("Equivalent Course")
            headerTitles.Add("Credits")
            headerTitles.Add("Hours")
            headerTitles.Add(score)
            headerTitles.Add("Credits Earned")
            headerTitles.Add("Credits Remaining")
            headerTitles.Add("Hours Completed")
            headerTitles.Add("Hours Remaining")
        Else
            headerTitles.Add("Description")
            headerTitles.Add("Equivalent Course")
            headerTitles.Add("Total Credits")
            headerTitles.Add("Total Hours")
            headerTitles.Add(score)
            headerTitles.Add("Total Credits Earned")
            headerTitles.Add("Total Credits Remaining")
            headerTitles.Add("Total Hours Completed")
            headerTitles.Add("Total Hours Remaining")
        End If
        Return headerTitles
    End Function
    Public Shared Function GetHeaderData(ByVal dto As List(Of GraduateAuditGroupDto), ByVal objCredits As Decimal, ByVal objHours As Decimal) As GraduateAuditGroupDto
        Dim headerData As GraduateAuditGroupDto

        For Each groupDto As GraduateAuditGroupDto In From groupDto1 In dto Where groupDto1.Level Is Nothing
            headerData = groupDto
            Exit For
        Next
        If headerData Is Nothing Then
            Return headerData
        End If

        If headerData.CreditsEarned Is Nothing Then
            headerData.CreditsEarned = 0.0
        Else
            headerData.CreditsEarned = Math.Round(CType(headerData.CreditsEarned, Decimal), 2)
        End If
        If headerData.IsContinuingEd Then
            If headerData.CreditsRemaining Is Nothing Then
                headerData.CreditsRemaining = 0.0
            Else
                headerData.CreditsRemaining = Math.Round(CType(headerData.CreditsRemaining, Decimal), 2)
                headerData.CreditsRemaining = headerData.CreditsRemaining - objCredits
            End If
            headerData.HoursRemaining = headerData.HoursRemaining - objHours
        End If

        Return headerData
    End Function
    Public Function CreateReportList(ByVal auditInfo As GraduateAuditInfo, ByRef headerTitles As IList(Of String), ByRef headerData As GraduateAuditGroupDto, ByRef detailDataList As IList(Of GraduateAuditGroupDto)) As List(Of GraduateAuditGroupDto)
        Dim datalist As List(Of String) = (From dto As ListItem In auditInfo.ItemCollection Select dto.Value).ToList()
        Dim dt As List(Of GraduateAuditGroupDto) = GetGraduateAuditEnrollmentForCourseEquivalent(auditInfo.StuEnrollmentId, auditInfo.GradeReps, auditInfo.IncludeHours, datalist)
        If dt.Count > 0 Then

            Dim ds As DataSet = dbTranscript.GetGradesSysDetailId()
            Dim dsgradesysdetailids As New List(Of String)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    dsgradesysdetailids.Add(row("GrdSysDetailId").ToString())
                Next
            End If
            'Dim transcript As New TranscriptFacade
            'Dim dsgradesysdetailids As List(Of String) = transcript.GetGradesSysDetailId()
            Dim objCredits As Decimal
            Dim objHours As Decimal
            If dsgradesysdetailids.Count > 0 Then
                objCredits += dt.Sum(Function(dto) (From detail In dsgradesysdetailids Where dto.GrdSysDetailId = detail).Sum(Function(detail) dto.Credits))
                objHours += dt.Sum(Function(dto) (From detail In dsgradesysdetailids Where dto.GrdSysDetailId = detail).Sum(Function(detail) dto.Hours))
            End If
            headerTitles = GetHeaderTitles(auditInfo) 'Create Headers
            headerData = GetHeaderData(dt, objCredits, objHours) 'Create Header Data
            detailDataList = GetDetailData(dt, auditInfo)
        End If
        Return dt
    End Function
    Private Function GetDetailData(ByVal dt As List(Of GraduateAuditGroupDto), ByVal auditInfo As GraduateAuditInfo) As IList(Of GraduateAuditGroupDto)
        Dim resultlist As IList(Of GraduateAuditGroupDto) = New List(Of GraduateAuditGroupDto)
        For Each dr As GraduateAuditGroupDto In dt
            If IsNothing(dr.Level) Then
                Continue For
            End If
            If IsNothing(dr.Grade) Then
                dr.EquivCourseDesc = String.Empty
            End If

            dr.Credits = CType(IIf(IsNothing(dr.Credits), 0, dr.Credits), Decimal) 'Credits
            dr.Hours = CType(IIf(IsNothing(dr.Hours), 0, dr.Hours), Decimal) 'Hours

            If auditInfo.StrGradesFormat.ToLower = "letter" Then
                dr.Grade = CType(IIf(IsNothing(dr.Grade), "", dr.Grade), String)
            Else
                If dr.IsTransferGrade Then
                    dr.Grade = CType(IIf(IsNothing(dr.Grade), "", dr.Grade), String)
                Else
                    dr.Grade = CType(IIf(IsNothing(dr.Score), "", dr.Score), String)
                End If

            End If
            Dim boolIsCourseALab As Boolean
            boolIsCourseALab = isCourseALabWorkOrLabHourCourse(dr.ReqId.ToString)
            If auditInfo.TranscripType.ToLower = "traditional_numeric" Then
                'check to make sure if the course is a lab work or course work

                If boolIsCourseALab = True Then
                    Dim decCredits As Decimal
                    Dim boolisCourseACombination As Boolean = CType(isCourseALabWorkOrLabHourCourseCombination(dr.ReqId).ToString, Boolean)
                    If boolisCourseACombination = False Then
                        Try
                            decCredits = CType(ComputeWithCreditsPerService(auditInfo.StuEnrollmentId, auditInfo.Addcreditsbyservice.ToLower, dr.ReqId).ToString, Decimal)
                        Catch ex As Exception
                            decCredits = 0
                        End Try
                        dr.CreditsEarned = Math.Round(decCredits, 2)
                    End If
                End If
            Else
                If boolIsCourseALab = True Then
                    Dim decCredits As Decimal
                    Dim boolisCourseACombination As Boolean = isCourseALabWorkOrLabHourCourseCombination(dr.ReqId.ToString)
                    If boolisCourseACombination = False Then
                        Try
                            decCredits = ComputeWithCreditsPerService(auditInfo.StuEnrollmentId, auditInfo.Addcreditsbyservice.ToLower, dr.ReqId.ToString)
                        Catch ex As Exception
                            decCredits = 0
                        End Try
                        dr.CreditsEarned = Math.Round(decCredits, 2)
                    End If
                End If
            End If

            dr.CreditsEarned = CType(IIf(IsNothing(dr.CreditsEarned), 0, dr.CreditsEarned), Decimal) 'Credits Earned
            dr.CreditsRemaining = CType(IIf(IsNothing(dr.CreditsRemaining), 0, dr.CreditsRemaining), Decimal) ' Credit Remaining

            Dim isTransferGrade As Boolean = False
            If auditInfo.Dsgradesysdetailids.Count > 0 Then
                Dim gradeSysDetailId As String
                gradeSysDetailId = dr.GrdSysDetailId.ToString
                Dim i As Integer
                isTransferGrade = False
                For i = 0 To auditInfo.Dsgradesysdetailids.Count - 1
                    If gradeSysDetailId = auditInfo.Dsgradesysdetailids(i) Then
                        isTransferGrade = True
                        Exit For
                    End If
                Next

            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim tempCreditRemained As Decimal

            If auditInfo.TranscripType.ToLower = "traditional_numeric" Then
                'Credits Remaining 
                Dim parcial As Decimal = dr.Credits - dr.CreditsEarned
                tempCreditRemained = parcial
                If boolIsCourseALab = True Then

                    If dr.CreditsEarned >= dr.Credits Then
                        tempCreditRemained = 0 ' For clinic service course if Credits Earned is equal to credits then set Credits Remaining to 0
                    Else
                        tempCreditRemained = parcial 'CType(IIf(IsNothing(dr.CreditsRemaining), "0", dr.Credits.ToString - dr.CreditsEarned.ToString), String)
                    End If
                    'End If
                End If
            Else

                Dim parcial As Decimal = dr.Credits - dr.CreditsEarned
                If isTransferGrade = True Then
                    tempCreditRemained = 0
                Else
                    tempCreditRemained = dr.CreditsRemaining
                End If
                If boolIsCourseALab = True Then
                    'If Not IsNothing(dr.CreditsEarned) Then
                    If dr.CreditsEarned >= dr.Credits Then
                        tempCreditRemained = 0 'CType(IIf(IsNothing(dr.CreditsRemaining), "0", 0), String) ' For clinic service course if Credits Earned is equal to credits then set Credits Remaining to 0
                    ElseIf dr.CreditsEarned < dr.Credits And auditInfo.Addcreditsbyservice.ToLower() = "yes" Then
                        tempCreditRemained = parcial ' dr.Credits - dr.CreditsEarned), String)
                    End If
                    'End If
                End If

                If boolIsCourseALab = True And auditInfo.Addcreditsbyservice.ToLower() = "no" Then
                    'If Not IsNothing(dr.CreditsEarned) Then
                    If dr.CreditsEarned >= dr.Credits Then
                        tempCreditRemained = 0 ' For clinic service course if Credits Earned is equal to credits then set Credits Remaining to 0
                    End If
                    'End If
                End If
            End If
            dr.CreditsRemaining = tempCreditRemained
            Dim tempHoursCompleted As Decimal
            Dim tempHoursRemaining As Decimal

            'Hours Completed and 'Hours Remaining
            If boolIsCourseALab = True Then
                Dim boolIsClinicCompletlySatisfied As Boolean = dbExams.isClinicCourseCompletlySatisfied(auditInfo.StuEnrollmentId, 500, dr.ReqId.ToString)
                Dim boolRemaining As Boolean = CType(dbExams.IsRemainingLabCount(auditInfo.StuEnrollmentId, 500, dr.ReqId).ToString, Boolean)
                Dim boolIsCoursePartOfCombination As String = dbTransferGrade.isCourseALabWorkOrLabHourCourseCombination(dr.ReqId).ToString
                Dim boolIsCombinationSatisfied As Boolean = False
                Dim boolFinalGradePosted As Boolean = False
                Dim intFinalGradePosted As Integer
                If boolIsCoursePartOfCombination = True Then
                    boolIsCombinationSatisfied = dbExams.isClinicCourseCompletlySatisfiedForCombination(auditInfo.StuEnrollmentId, 500, dr.ReqId.ToString)
                    intFinalGradePosted = dbTransferGrade.CheckIfFinalGradePostedForCombination(auditInfo.StuEnrollmentId, dr.ReqId.ToString)
                    If intFinalGradePosted >= 1 Then
                        boolFinalGradePosted = True
                    Else
                        boolFinalGradePosted = False
                    End If
                End If


                If boolRemaining = False Then
                    If isTransferGrade = True Then
                        tempHoursCompleted = dr.Hours
                        tempHoursRemaining = 0
                    Else
                        tempHoursCompleted = dr.Hours
                        tempHoursRemaining = 0
                    End If
                Else
                    If isTransferGrade = True Then
                        tempHoursCompleted = 0
                        tempHoursRemaining = 0
                    Else
                        tempHoursCompleted = CType(IIf(IsNothing(dr.HoursCompleted), 0, dr.HoursCompleted), Decimal)
                        tempHoursRemaining = CType(IIf(IsNothing(dr.HoursRemaining), 0, dr.HoursRemaining), Decimal)
                    End If
                End If
                If boolIsClinicCompletlySatisfied = False Then
                    tempHoursCompleted = "0" 'IIf(dr.IsNull("Hours"), "0", "0.00")
                    tempHoursRemaining = CType(IIf(IsNothing(dr.HoursRemaining), 0, dr.Hours), Decimal)

                End If
                If boolIsClinicCompletlySatisfied = True And boolIsCoursePartOfCombination = True And boolIsCombinationSatisfied = False Then
                    tempHoursCompleted = "0" ' IIf(dr.IsNull("Hours"), "0", "0.00")
                    tempHoursRemaining = CType(IIf(IsNothing(dr.HoursRemaining), 0, dr.Hours), Decimal)
                End If
                If boolIsClinicCompletlySatisfied = True And boolIsCoursePartOfCombination = True And boolIsCombinationSatisfied = True And boolFinalGradePosted = False Then
                    tempHoursCompleted = "0" 'IIf(dr.IsNull("Hours"), "0", "0.00")
                    tempHoursRemaining = CType(IIf(IsNothing(dr.HoursRemaining), 0, dr.Hours), Decimal)
                ElseIf boolIsClinicCompletlySatisfied = True And boolIsCoursePartOfCombination = True And boolIsCombinationSatisfied = False And boolFinalGradePosted = True Then
                    tempHoursCompleted = CType(IIf(IsNothing(dr.Hours), 0, dr.Hours), String)
                    tempHoursRemaining = 0
                ElseIf boolIsClinicCompletlySatisfied = True And boolIsCoursePartOfCombination = True And boolIsCombinationSatisfied = True And boolFinalGradePosted = True Then
                    If Not IsNothing(dr.HoursRemaining) Then
                        If dr.HoursRemaining > 0 Then
                            tempHoursCompleted = 0 'dr.Hours'.ToString() 'IIf(dr.IsNull("Hours"), "0", "0.00")
                            tempHoursRemaining = CType(IIf(IsNothing(dr.HoursRemaining), 0, dr.Hours), Decimal)
                        Else
                            tempHoursCompleted = IIf(IsNothing(dr.Hours), 0, dr.Hours)
                            tempHoursRemaining = 0 'CType(IIf(IsNothing(dr.HoursRemaining), "0", "0.00"), String)
                        End If

                    End If

                End If
            Else
                If isTransferGrade = True Then
                    tempHoursCompleted = 0
                    tempHoursRemaining = 0
                Else
                    tempHoursCompleted = CType(IIf(IsNothing(dr.HoursCompleted), 0, dr.HoursCompleted), Decimal)
                    tempHoursRemaining = CType(IIf(IsNothing(dr.HoursRemaining), 0, dr.HoursRemaining), Decimal)
                End If
            End If
            dr.HoursCompleted = tempHoursCompleted
            dr.HoursRemaining = tempHoursRemaining
            resultlist.Add(dr)
        Next
        Return resultlist
    End Function
    ''' <summary>
    ''' Calculate the average of a determinate colum in a table.
    ''' The ds must have a datatable named Result.
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <param name="fieldToCalculate"></param>
    ''' <param name="reqId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function CalculateAverage(ByRef ds As DataSet, fieldToCalculate As String, ByVal reqId As String) As Decimal
        Dim score As Decimal = 0
        If ds.Tables("Results").Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables("Results").Rows
                If row.Item("ReqId").ToString() = reqId AndAlso IsNothing(row.Item(fieldToCalculate)) = False AndAlso String.IsNullOrEmpty((row.Item(fieldToCalculate).ToString())) = False Then
                    Dim sco As Decimal
                    If Decimal.TryParse(row.Item(fieldToCalculate).ToString(), sco) Then
                        score += sco
                    End If
                End If
            Next
            score = score / ds.Tables("Results").Rows.Count
        End If
        Return score
    End Function
    ''' <summary>
    ''' Get info GetTermEnrollSummary or Create record from arTermEnrollSummary table
    ''' </summary>
    ''' <param name="pTermId"></param>
    ''' <param name="pStuEnrollId"></param>
    ''' <param name="pUser"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetTermEnrollSummary(ByVal pTermId As String, ByVal pStuEnrollId As String, ByVal pUser As String) As DataSet
        Dim ds As DataSet

        ds = dbTranscript.GetTermEnrollSummary(pTermId, pStuEnrollId, pUser)

        Return ds
    End Function
    ''' <summary>
    ''' Update info TermEnrollSummary in arTermEnrollSummary table
    ''' </summary>
    ''' <param name="termEnrollSummary"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function UpdateTermEnrollSummary(termEnrollSummary As TermEnrollSummary) As String
        Dim result As String
        result = dbTranscript.UpdateTermEnrollSummary(termEnrollSummary)

        Return result
    End Function
    ''' <summary>
    ''' Response true or false to the question 
    ''' if the program version is continuing Education
    ''' </summary>
    ''' <param name="stuEnrollId">The enrollment Id </param>
    ''' <returns>true is continuing education</returns>
    Public Shared Function IsContinuingEdPrgVersion(stuEnrollId As String) As Boolean
        'connect to the database

        Dim sb As New StringBuilder
        Dim myAdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Dim conn = New SqlConnection(connectionString)
        With sb
            .Append("SELECT ")
            .Append("       IsContinuingEd ")
            .Append("FROM   arPrgVersions ")
            .Append("WHERE  prgVerId=(SELECT prgVerId FROM arStuEnrollments WHERE StuEnrollId= @StuEnrollId) ")
        End With

        Dim comm = New SqlCommand(sb.ToString(), conn)
        comm.Parameters.AddWithValue("@StuEnrollId", stuEnrollId)

        '   Execute the query       
        conn.Open()
        Try
            Return CType(comm.ExecuteScalar(), Boolean)
        Finally
            conn.Close()
        End Try
    End Function


    Public Function IsStudentRegisteredForClockHour(ByVal StuEnrollId As String) As Integer
        'Check if student is in a clock hour Program 
        Dim sb As New StringBuilder
        Dim myAdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim connectionString = myAdvAppSettings.AppSettings("ConnectionString")
        Dim conn = New SqlConnection(connectionString)
        Dim programACId As Integer


        With sb
            .Append("Select TOP 1 P.ACId ")
            .Append(" From dbo.arStuEnrollments E ")
            .Append(" Join dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId ")
            .Append(" Join dbo.arPrograms P ON P.ProgId = PV.ProgId ")
            .Append("WHERE  E.StuEnrollId = @StuEnrollId ")

        End With
        Dim comm = New SqlCommand(sb.ToString(), conn)
        comm.Parameters.AddWithValue("@StuEnrollId", StuEnrollId)

        '   Execute the query       
        conn.Open()
        Try
            programACId = CType(comm.ExecuteScalar(), Integer)
        Finally
            conn.Close()
        End Try

        If programACId = 5 Then
            Return True
        Else
            Return False
        End If


    End Function
End Class
