Imports System.data
Imports System.Data.OleDb
Imports FAME.AdvantageV1.DataAccess

Public Class GrdPostingsFacade
    Public Function GetAllTerms() As DataSet

        '   Instantiate DAL component
        Dim GrdPostDB As New GrdPostingsDB

        '   get the dataset with all degrees
        Return GrdPostDB.GetAllTerms()

    End Function
    Public Function GetCurrentAndPastTerms(Optional ByVal instructorId As String = "") As DataSet

        '   Instantiate DAL component
        Dim GrdPostDB As New GrdPostingsDB

        '   get the dataset with all degrees
        Return GrdPostDB.GetCurrentAndPastTerms(instructorId)
    End Function
    'Public Function GetCurrentAndPastTermsForAnyUser(ByVal instructorId As String, ByVal userName As String) As DataSet

    '    '   Instantiate DAL component
    '    Dim GrdPostDB As New GrdPostingsDB

    '    '   get the dataset with all degrees
    '    Return GrdPostDB.GetCurrentAndPastTermsForAnyUser(instructorId, userName)
    'End Function
    Public Function GetCurrentAndPastTermsForAnyUser(ByVal instructorId As String, ByVal userName As String, Optional ByVal campusId As String = "") As DataSet

        '   Instantiate DAL component
        Dim GrdPostDB As New GrdPostingsDB

        '   get the dataset with all degrees
        Return GrdPostDB.GetCurrentAndPastTermsForAnyUser(instructorId, userName, campusId)
    End Function

    Public Function GetClsSections(ByVal Term As String, ByVal Instructor As String, ByVal CampusId As String) As DataSet

        '   Instantiate DAL component
        Dim GrdPostDB As New GrdPostingsDB
        Dim ds As New DataSet

        '   get the dataset with all degrees          
        ds = GrdPostDB.GetClsSects(Term, Instructor, CampusId)

        Return ProcessClsSectInfo(ds)

    End Function
    Public Function GetClsSections(ByVal Term As String, ByVal Instructor As String, ByVal CampusId As String, ByVal UserName As String) As DataSet
        Dim ds As New DataSet
        ds = (New GrdPostingsDB).GetClsSects(Term, Instructor, CampusId, UserName)
        Return ProcessClsSectInfo(ds)
    End Function
    Public Function GetClsSectInstructor(ByVal ClsSection As String) As String
        '   Instantiate DAL component
        Dim GrdPostDB As New GrdPostingsDB
        Dim ds As New DataSet
        Dim Instructor As String

        '   get the dataset with all degrees          
        Instructor = GrdPostDB.GetClsSectInstructor(ClsSection)

        Return Instructor

    End Function

    Public Function ProcessClsSectInfo(ByVal ds As DataSet) As DataSet
        Dim db As New GrdPostingsDB
        Dim tbl1 As New DataTable
        Dim tbl2 As New DataTable
        Dim row As DataRow


        tbl1 = ds.Tables("TermClsSects")
        'Make the ChildId column the primary key
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("ClsSectionId")}
        End With


        If ds.Tables("TermClsSects").Rows.Count > 0 Then
            'Load a datatable with ChildId, ChildTyp and ChildDescrip
            'Create a datable with a new column called ChildId, make this the primary key.

            'Add a new column called ChildDescrip to the datatable 
            Dim col9 As DataColumn = tbl1.Columns.Add("CourseSectDescrip", GetType(String))

            For Each row In tbl1.Rows

                row("CourseSectDescrip") = row("Descrip") & " - " & row("ClsSection")

            Next
        End If

        Return ds
    End Function
    Public Function ValidateScore(ByVal Score As Integer, ByVal StuEnrollId As String) As String

        Dim ds As New DataSet
        Dim db As New StdGrdBkDB
        Dim MinScore As String
        Dim MaxScore As String
        Dim errStr As String = ""

        Dim MinMaxScore() As String = Split(db.GetGrdScaleScoreLimit(StuEnrollId))
        MaxScore = MinMaxScore(0)
        MinScore = MinMaxScore(1)

        If Score < MinScore Then
            errStr = "Score is lesser than the accepted score limit"
        ElseIf Score > MaxScore Then
            errStr = "Score is greater than the accepted score limit"
        End If


        Return errStr
    End Function
    ''Optional Parameter isAcademicAdvisor added by Saraswathi Lakshmanan
    ''To filter based on Academic advisor
    Public Function GetGrdCriteria(ByVal ClsSect As String, ByVal Instr As String, ByVal username As String, Optional ByVal isAcademicAdvisor As Boolean = False) As DataSet

        '   Instantiate DAL component
        Dim GrdPostDB As New GrdPostingsDB

        '   get the dataset with all degrees
        Return GrdPostDB.GetGrdCriteria(ClsSect, Instr, username, isAcademicAdvisor)

    End Function
    Public Function GetCourseLevelGrdCriteria(ByVal ClsSect As String) As DataSet

        '   Instantiate DAL component
        Dim GrdPostDB As New GrdPostingsDB

        '   get the dataset with all degrees
        Return GrdPostDB.GetCourseLevelGrdCriteria(ClsSect)

    End Function

    Public Function PopulateDataGrid(ByVal Term As Object, ByVal ClsSect As String, ByVal GrdCriteria As String, Optional ByVal ResNum As Integer = 0) As DataSet

        Dim db As New GrdPostingsDB
        Dim ds As DataSet
        Dim da As New OleDbDataAdapter
        ds = db.GetClsSectDataGrid(Term, ClsSect, GrdCriteria, ResNum)
        Return BuildStuInfoDS(ds)

    End Function
    Public Function InsertGrdPosting(ByVal GrdPostingObj As GrdPostingsInfo, ByVal user As String) As String
        Return (New GrdPostingsDB).InsertGrdPosting(GrdPostingObj, user)
    End Function
    Public Function DeleteGrdPosting(ByVal GrdPostingObj As GrdPostingsInfo) As String
        Return (New GrdPostingsDB).DeleteGrdPosting(GrdPostingObj)
    End Function
    Public Function UpdateGrdPosting(ByVal GrdPostingObj As GrdPostingsInfo, ByVal user As String) As String
        Return (New GrdPostingsDB).UpdateGrdPosting(GrdPostingObj, user)
    End Function

    Public Function BuildStuInfoDS(ByVal ds As DataSet) As DataSet
        Dim tbl1 As New DataTable
        Dim row As DataRow
        'Dim selHours As Integer
        'Dim selCnt As Integer
        'Dim txtChildHrs As String
        'Dim txtChildCrds As String
        'Dim hcRow As DataRow
        Dim tbl2 As New DataTable
        Dim db As New StdGrdBkDB


        tbl1 = ds.Tables("ClsSectStdGrds")
        If (ds.Tables("ClsSectStdGrds").Rows.Count = 0) Then

        End If
        'Make the ChildId column the primary key
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("StuEnrollId")}
        End With

        tbl2 = ds.Tables("ClsSectGrds")
        'Make the ChildId column the primary key
        With tbl2
            .PrimaryKey = New DataColumn() {.Columns("StuEnrollId")}
        End With
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If ds.Tables("ClsSectStdGrds").Rows.Count > 0 Then
            'Load a datatable with ChildId, ChildTyp and ChildDescrip
            'Create a datable with a new column called ChildId, make this the primary key.

            'Add a new column called ChildDescrip to the datatable 
            Dim col10 As DataColumn = tbl1.Columns.Add("Score", GetType(String))
            Dim col11 As DataColumn = tbl1.Columns.Add("Comments", GetType(String))

            For Each row In tbl1.Rows

                If (tbl2.Rows.Count > 0) Then
                    If (row.IsNull("GrdSysDetailId")) Then
                        'get the corresponding default grade 
                        row("Grade") = db.GetDefaultGrade(row("StuEnrollId").ToString)
                    End If
                    Dim row2 As DataRow = tbl2.Rows.Find(row("StuEnrollId"))
                    If Not (row2 Is Nothing) Then
                        row("Score") = row2("Score")
                        row("DateCompleted")= row2("DateCompleted")
                        row("Comments") = row2("Comments")
                        row("IsCompGraded") = row2("IsCompGraded")
                    End If
                ElseIf (tbl2.Rows.Count = 0) Then
                    row("Grade") = db.GetDefaultGrade(row("StuEnrollId").ToString)
                End If
            Next

        End If
        Return ds
    End Function
    Public Function GetTermsByInstructor(ByVal instructorId As String) As DataSet

        '   Instantiate DAL component
        With New GrdPostingsDB

            '   get the dataset with all degrees
            Return .GetTermsByInstructor(instructorId)

        End With

    End Function

    ''Added by Saraswathi lakshmanan to fix issue 14296
    ''Academic advisors and Instructor supervisor able to 
    ''post and modify attendance and grade
    ''The term is populated based on the Roles  the User has
    ''New Parameters are added to the existing function
    ''Changed on 26-Sept -2008
    Public Function GetCurrentAndPastTermsForAnyUserbyRoles(ByVal userId As String, ByVal userName As String, ByVal isAcademicAdvisor As Boolean, Optional ByVal campusId As String = "") As DataSet

        '   Instantiate DAL component
        Dim GrdPostDB As New GrdPostingsDB

        '   get the dataset with all degrees
        Return GrdPostDB.GetCurrentAndPastTermsForAnyUserbyRoles(userId, userName, isAcademicAdvisor, campusId)
    End Function

    ''Get the CohortStartDate for the current and past terms which are active
    ''Added by Saraswathi lakshmanan on October 14- 2008
    Public Function GetCohortStartDateforCurrentAndPastTermsForAnyUserbyRoles(ByVal userId As String, ByVal userName As String, ByVal isAcademicAdvisor As Boolean, Optional ByVal campusId As String = "") As DataSet

        '   Instantiate DAL component
        Dim GrdPostDB As New GrdPostingsDB

        '   get the dataset with all degrees
        Return GrdPostDB.GetCohortStartDateforCurrentAndPastTermsForAnyUserbyRoles(userId, userName, isAcademicAdvisor, campusId)
    End Function
    Public Function CurrentAndPastTermsWithCoursesMappedToDictationSpeedTestComponent(ByVal CampusId As String, _
                                                 ByVal UserId As String, _
                                                 ByVal isAcademicAdvisor As Boolean, _
                                                 ByVal UserName As String) As DataSet
        Dim GrdPostDB As New GrdPostingsDB
        Return GrdPostDB.CurrentAndPastTermsWithCoursesMappedToDictationSpeedTestComponent(CampusId, _
                                                                                           UserId, _
                                                                                           isAcademicAdvisor, _
                                                                                           UserName)
    End Function
    'Public Function GetStudentsRegisteredInSelectedTerm(ByVal CampusId As String, _
    '                                                    ByVal TermId As String, _
    '                                                    ByVal FirstName As String, _
    '                                                    ByVal LastName As String) As DataSet
    '    Dim GrdPostDB As New GrdPostingsDB
    '    Return GrdPostDB.GetStudentsRegisteredInSelectedTerm(CampusId, TermId, FirstName, LastName)
    'End Function
    Public Function GetTermStudentIsCurrentlyRegisteredIn(ByVal StudentId As String, _
                                                          ByVal campusId As String) As DataSet
        Dim GrdPostDB As New GrdPostingsDB
        Return GrdPostDB.GetTermStudentIsCurrentlyRegisteredIn(StudentId, campusId)
    End Function
    Public Function GetGradeComponentsMappedToSystemComponent(ByVal SysComponentTypeId As Integer) As DataSet
        Dim GrdPostDB As New GrdPostingsDB
        Return GrdPostDB.GetGradeComponentsMappedToSystemComponent(612)
    End Function
    Public Function GetGrades(ByVal StuEnrollId As String) As DataSet
        Dim GrdPostDB As New GrdPostingsDB
        Return GrdPostDB.GetGrades(StuEnrollId)
    End Function
    Public Function GetDictationScorePostedByStudentEnrollment(ByVal StuEnrollId As String) As DataSet
        Dim GrdPostDB As New GrdPostingsDB
        Return GrdPostDB.GetDictationScorePostedByStudentEnrollment(StuEnrollId)
    End Function
    Public Function GetInstructorsByCampusId(ByVal CampusId As String) As DataSet
        Dim GrdPostDB As New GrdPostingsDB
        Return GrdPostDB.GetInstructorsByCampusId(CampusId)
    End Function
    Public Function GetEnrollmentsByStudent(ByVal StudentId As String) As DataSet
        Dim GrdPostDB As New GrdPostingsDB
        Return GrdPostDB.GetEnrollmentsByStudent(StudentId)
    End Function
    Public Sub PostScoresForDictationSpeedTest(ByVal dtEffectiveDate As DateTime, _
                              ByVal xmlScoreDetails As String)
        Dim GrdPostDB As New GrdPostingsDB
        GrdPostDB.PostScoresForDictationSpeedTest(dtEffectiveDate, xmlScoreDetails)
    End Sub
    Public Sub UpdatePostedScoresForDictationSpeedTest(ByVal dtEffectiveDate As DateTime, _
                              ByVal xmlScoreDetails As String)
        Dim GrdPostDB As New GrdPostingsDB
        GrdPostDB.UpdatePostedScoresForDictationSpeedTest(dtEffectiveDate, xmlScoreDetails)
    End Sub
    Public Sub DeletePostedScoresForDictationSpeedTest(ByVal PKValue As String)
        Dim GrdPostDB As New GrdPostingsDB
        GrdPostDB.DeletePostedScoresForDictationSpeedTest(PKValue)
    End Sub
End Class
