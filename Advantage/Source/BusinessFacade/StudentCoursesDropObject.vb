﻿

Public Class StudentCoursesDropObject
    Inherits BaseReportFacade
#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim stuCourseDrops As New StudentCoursesDropDB
        Dim ds As New DataSet
        Dim stuEnrollId As String = ""
        Dim dtTemp As New DataTable
        Dim dtMain As New DataTable
        Dim boolAssign As Boolean = False

        '' New Code Added By VIjay Ramteke On September 28, 2010 Form Mantis Id 17685
        ''    If paramInfo.FilterOther.Contains("arStuEnrollments.StuEnrollId") Then
        ''        stuEnrollId = Mid(paramInfo.FilterOther, InStr(paramInfo.FilterOther, "'") + 1, 36)
        ''    End If
        ''    If stuEnrollId <> "" Then
        ''        Try
        ''            ' Massage dataset to produce one with the columns and data expected by report.
        ''            ds = BuildReportSource(stuCourseDrops.GetDroppedCourses(paramInfo, stuEnrollId), stuCourseDrops.StudentIdentifier)
        ''            ' The report, whatever its frontend is, 
        ''            ' will only need to display the data in the dataset.
        ''            If Not ds Is Nothing Then
        ''                dtTemp = ds.Tables(0)
        ''                dtMain = dtTemp.Clone()
        ''                dtMain.Clear()
        ''                For Each drT As DataRow In dtTemp.Rows
        ''                    dtMain.ImportRow(drT)
        ''                Next
        ''            End If
        ''        Catch ex As System.Exception
        ''        End Try
        ''    Else
        ''        '   connect to the database
        ''        Dim db As New DataAccess.DataAccess
        ''        db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
        ''        Dim DsStudEnrollMents As DataSet
        ''        Dim count As Boolean = False
        ''        'build the sql query
        ''        Dim sb As New Text.StringBuilder
        ''        With sb
        ''            .Append(" SELECT arStuEnrollments.StuEnrollId, arStudent.LastName ")
        ''            .Append(" FROM arStudent,arStuEnrollments,syCampGrps,syCmpGrpCmps,syCampuses,arPrgVersions , syStatusCodes , syStatuses  ")
        ''            .Append(" WHERE syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
        ''            .Append(" AND arStuEnrollments.StudentId = arStudent.StudentId ")
        ''            .Append(" AND syCampGrps.CampGrpId = syCmpGrpCmps.CampGrpId ")
        ''            .Append(" AND syCampuses.CampusId = arStuEnrollments.CampusId ")
        ''            .Append(" AND arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId ")
        ''            .Append("       AND syStatusCodes.StatusId=syStatuses.StatusId AND SysStatusId in (12,19) AND arStuEnrollments.StatusCodeId=syStatusCodes.StatusCodeId ")
        ''            .Append(" AND " & paramInfo.FilterList & " Order By arStudent.LastName")
        ''        End With
        ''        Try
        ''            DsStudEnrollMents = db.RunSQLDataSet(sb.ToString)
        ''            Dim totRecords As Integer = 0
        ''            Dim processRecords As Integer = 0
        ''            For Each dr As DataRow In DsStudEnrollMents.Tables(0).Rows
        ''                stuEnrollId = dr("StuEnrollId").ToString
        ''                ds = BuildReportSource(stuCourseDrops.GetDroppedCourses(paramInfo, stuEnrollId), stuCourseDrops.StudentIdentifier)
        ''                If Not ds Is Nothing Then
        ''                    If count = False Then
        ''                        count = True
        ''                        dtTemp = ds.Tables(0)
        ''                        dtMain = dtTemp.Clone()
        ''                        dtMain.Clear()
        ''                        For Each drT As DataRow In dtTemp.Rows
        ''                            dtMain.ImportRow(drT)
        ''                        Next
        ''                    Else
        ''                        For Each dr1 As DataRow In ds.Tables(0).Rows
        ''                            dtMain.ImportRow(dr1)
        ''                        Next
        ''                    End If
        ''                End If
        ''            Next
        ''        Catch ex As System.Exception
        ''        End Try
        ''    End If
        ''    If Not ds Is Nothing Then
        ''        If ds.Tables.Count > 0 Then
        ''            ds.Tables.RemoveAt(0)
        ''            ds.Tables.Add(dtMain)
        ''        Else
        ''            If dtMain.Rows.Count > 0 Then
        ''                ds = New DataSet()
        ''                ds.Tables.Add(dtMain)
        ''            End If
        ''        End If
        ''    End If

        ''New Code Added By Vijay Ramteke On November 04, 2010 For Rally Id DE1218
        ''If paramInfo.FilterOther.Contains("arStuEnrollments.StuEnrollId") Then
        'Troy: 6/7/2012 DE7725: Dropped student report issue
        If (paramInfo.StuEnrollId <> "" And paramInfo.StuEnrollId <> "StuEnrollmentId") Then
            ''stuEnrollId = Mid(paramInfo.FilterOther, InStr(paramInfo.FilterOther, "'") + 1, 36)
            stuEnrollId = paramInfo.StuEnrollId
        Else
            stuEnrollId = ""
        End If
        ''New Code Added By Vijay Ramteke On November 04, 2010 For Rally Id DE1218

        If stuEnrollId <> "" Then
            Try
                ds = BuildReportSource(stuCourseDrops.GetDroppedCourses(paramInfo, stuEnrollId), stuCourseDrops.StudentIdentifier)
            Catch ex As System.Exception
            End Try
        Else
            Try
                ds = BuildReportSource(stuCourseDrops.GetDroppedCourses(paramInfo), stuCourseDrops.StudentIdentifier)
            Catch ex As System.Exception
            End Try
        End If
        '' New Code Added By VIjay Ramteke On September 28, 2010 Form Mantis Id 17685

        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim facInputMasks As New InputMasksFacade

        'Get the mask for SSN
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        Dim strPhoneMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        Try
            For Each dr As DataRow In ds.Tables(0).Rows

                'Apply mask to SSN.
                If StudentIdentifier = "SSN" Then
                    If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                        If dr("StudentIdentifier") <> "" Then
                            Dim temp As String = dr("StudentIdentifier")
                            dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                        End If
                    End If
                End If
                If Not (dr("Phone") Is System.DBNull.Value) Then
                    If dr("Phone") <> "" Then
                        Dim temp As String = dr("Phone")
                        dr("Phone") = facInputMasks.ApplyMask(strPhoneMask, temp)
                    End If
                End If
            Next

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region
End Class
