Public Class ReportExceptionsFacade

    Public Sub RefreshLeadStatus(ByVal CampusId As String)
        Dim refreshDB As New ReportExceptionsDB
        refreshDB.RefreshLeadStatus(CampusId)
    End Sub

    Public Sub RefreshAdmissionsRep(ByVal CampusId As String)
        Dim refreshDB As New ReportExceptionsDB
        refreshDB.RefreshAdmissionsRep(CampusId)
    End Sub

    Public Sub RefreshInstructor()
        Dim refreshDB As New ReportExceptionsDB
        refreshDB.RefreshInstructor()
    End Sub

    Public Sub RefreshStuEnrollmentStatus()
        Dim refreshDB As New ReportExceptionsDB
        refreshDB.RefreshStuEnrollmentStatus()
    End Sub

End Class
