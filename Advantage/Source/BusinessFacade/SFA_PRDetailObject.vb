Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.AdvantageV1.BusinessFacade.SFA_PRCommonObject

Public Class SFA_PRDetailObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SFA_PRDetail"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet
	Private dvCustomPrefInfo As DataView
	Private dtStuInfoRaw As DataTable
	Private dvAwards As DataView

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' save the custom pref info from the report parameters into DataView, then set custom pref info to
		'	Nothing to avoid overhead of sending unnecessary data to DataAccess object
		dvCustomPrefInfo = New DataView(RptParamInfo.CustomPrefInfo.Copy)
		RptParamInfo.CustomPrefInfo = Nothing

		' get raw DataSet using passed-in parameters
		dsRaw = SFA_PRDetailObjectDB.GetReportDatasetRaw(RptParamInfo)

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		Dim drRpt As DataRow
		Dim CurTableName As String
		Dim i As Integer = 0
		Dim drRaw As DataRow

		dtStuInfoRaw = PreProcessRawData(dsRaw)

		' process data for report Part 1
		CurTableName = MainTableName & "Part1"

		' create table to hold final report data 
		With dsRpt.Tables.Add(CurTableName).Columns
			.Add(RptStuIdentifierColName, GetType(System.String))
			.Add("Name", GetType(System.String))
			.Add("Ind_FirstTime", GetType(System.String))
			.Add("Ind_FinAid", GetType(System.String))
		End With

		' process raw data
		For Each drRaw In dtStuInfoRaw.Rows
			drRpt = dsRpt.Tables(CurTableName).NewRow

			' set Student Identifier and Name
			IPEDSFacade.SetStudentInfo(drRaw, drRpt)

			' set indicators as appropriate
			drRpt("Ind_FirstTime") = DBNull.Value
			drRpt("Ind_FinAid") = DBNull.Value
			If drRaw("DegCertSeekingDescrip").ToString.StartsWith(DegCertSeekingFirstTime) Then
				' student is First-time, Degree-seeking 
				drRpt("Ind_FirstTime") = "X"
				If CInt(drRaw("FinAidCount")) > 0 Then
					' student received financial aid
					drRpt("Ind_FinAid") = "X"
				End If
			End If

			' add row to report dataset
			dsRpt.Tables(CurTableName).Rows.Add(drRpt)
		Next



		' process data for report Part 2
		CurTableName = MainTableName & "Part2"

		' create table to hold final report data 
		With dsRpt.Tables.Add(CurTableName).Columns
			.Add(RptStuIdentifierColName, GetType(String))
			.Add("Name", GetType(String))
			.Add("Ind_FedGrants", GetType(String))
			.Add("Sum_FedGrants", GetType(Single))
			.Add("Ind_StateLocalGrants", GetType(String))
			.Add("Sum_StateLocalGrants", GetType(Single))
			.Add("Ind_InstGrants", GetType(String))
			.Add("Sum_InstGrants", GetType(Single))
			.Add("Ind_FedLoans", GetType(String))
			.Add("Sum_FedLoans", GetType(Single))
		End With

		dvAwards = New DataView(dsRaw.Tables("Awards"))

		' process raw data
		For Each drRaw In dtStuInfoRaw.Rows
			ProcessRowsPart2(drRaw, dsRpt.Tables(CurTableName))
		Next


		' return report DataSet
		Return dsRpt

	End Function

	Private Sub ProcessRowsPart2(ByVal drStuInfoRaw As DataRow, ByRef dtRptPart2 As DataTable)
		Dim StudentIdFilter As String = "StudentId = '" & drStuInfoRaw("StudentId").ToString & "'"
		Dim drRpt As DataRow = dtRptPart2.NewRow
		Dim i As Integer
		Dim Sum_StateLocalGrants As Single = 0
		Dim Sum_InstGrants As Single = 0
		Dim sb As New System.Text.StringBuilder

		' set Student Identifier and Name
		IPEDSFacade.SetStudentInfo(drStuInfoRaw, drRpt)

		' sum Federal Grants and indicate if any were found 
		dvAwards.RowFilter = StudentIdFilter & " AND TitleIV = 1 AND AwardTypeDescrip LIKE 'Grant'"
		drRpt("Sum_FedGrants") = CompAwardSum(dvAwards, "Amount")
		drRpt("Ind_FedGrants") = IIf(drRpt("Sum_FedGrants") > 0, "X", DBNull.Value)

		' sum Federal Loans and indicate if any were found 
		dvAwards.RowFilter = StudentIdFilter & " AND TitleIV = 1 AND AwardTypeDescrip LIKE 'Loan'"
		drRpt("Sum_FedLoans") = CompAwardSum(dvAwards, "Amount")
		drRpt("Ind_FedLoans") = IIf(drRpt("Sum_FedLoans") > 0, "X", DBNull.Value)

		' sum Fund Sources designated by user as "State/local government grants" and indicate if any were found 
		dvCustomPrefInfo.RowFilter = "RptCatId = " & RptCat_StateLocalGrants
		If dvCustomPrefInfo.Count > 0 Then
			' if any Fund Sources are designated as "State/local government grants",
			'	build a list of those Fund Source ids
			With sb
				For i = 0 To dvCustomPrefInfo.Count - 1
					.Append("'" & LCase(dvCustomPrefInfo(i)("FundSourceId")) & "',")
				Next
				.Remove(.Length - 1, 1)	' get rid of extra "," at end
			End With
			' sum any records from the raw data which belong to the current Student,
			'	and which contain data for any of the Fund Sources in the list just built
			dvAwards.RowFilter = StudentIdFilter & " AND FundSourceId IN (" & sb.ToString & ")"
			drRpt("Sum_StateLocalGrants") = CompAwardSum(dvAwards, "Amount")
		Else
			' no Fund Sources are designated as "State/local government grants", enter value of 0
			drRpt("Sum_StateLocalGrants") = 0
		End If
		drRpt("Ind_StateLocalGrants") = IIf(drRpt("Sum_StateLocalGrants") > 0, "X", DBNull.Value)


		' sum Fund Sources designated by user as "Institutional grants" and indicate if any were found 
		dvCustomPrefInfo.RowFilter = "RptCatId = " & RptCat_InstGrants
		If dvCustomPrefInfo.Count > 0 Then
			' if any Fund Sources are designated as "Institutional grants",
			'	build a list of those Fund Source ids
			With sb
				.Remove(0, .Length)
				For i = 0 To dvCustomPrefInfo.Count - 1
					.Append("'" & LCase(dvCustomPrefInfo(i)("FundSourceId")) & "',")
				Next
				.Remove(.Length - 1, 1)	' get rid of extra "," at end
			End With
			' sum any records from the raw data which belong to the current Student,
			'	and contain data for any of the Fund Sources in the list just built
			dvAwards.RowFilter = StudentIdFilter & " AND FundSourceId IN (" & sb.ToString & ")"
			drRpt("Sum_InstGrants") = CompAwardSum(dvAwards, "Amount")
		Else
			' no Fund Sources are designated as "Institutional grants", enter value of 0
			drRpt("Sum_InstGrants") = 0
		End If
		drRpt("Ind_InstGrants") = IIf(drRpt("Sum_InstGrants") > 0, "X", DBNull.Value)

		' add row to report DataSet
		dtRptPart2.Rows.Add(drRpt)
	End Sub

End Class