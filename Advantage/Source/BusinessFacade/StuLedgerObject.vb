Public Class StuLedgerObject
    Inherits BaseReportFacade

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim stuLedger As New StuLedgerDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by report.
        ds = BuildReportSource(stuLedger.GetStudentLedger(rptParamInfo), stuLedger.StudentIdentifier)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset.  
        Return ds
    End Function


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim stuName As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String

        'Get the mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            'Added By Vijay Ramteke on Feb 02, 2010
            ''If ds.Tables.Count = 2 Then
            If ds.Tables.Count = 5 Then
                'Added By Vijay Ramteke on Feb 02, 2010
                If ds.Tables(1).Rows.Count > 0 Then
                    Dim lastRow As Integer = ds.Tables(1).Rows.Count - 1
                    '
                    '''''''''''''''''''''''''

                    'Change sign of Payment so it always prints positive.
                    For Each dr As DataRow In ds.Tables(1).Rows
                        dr("StuEnrollIdStr") = dr("StuEnrollId").ToString
                        '
                        If Not (dr("TransAmount") Is System.DBNull.Value) Then
                            If dr("TransAmount") > 0 Then
                                dr("Charge") = dr("TransAmount")
                            Else
                                dr("Payment") = dr("TransAmount") * (-1)
                            End If
                        End If
                        ''Added by Saraswathi lakshmanan on July 21 2009
                        ''To fix mantis 16578
                        ''The document id is added to the report
                        If Not (dr("CheckNumber") Is System.DBNull.Value) Then
                            Select Case (dr("PaymentTypeId").ToString)
                                Case PaymentType.Check
                                    dr("CheckNumber") = "Check Number: " + dr("CheckNumber").ToString
                                Case PaymentType.Credit_Card
                                    dr("CheckNumber") = "C/C Authorization: " + dr("CheckNumber").ToString
                                Case PaymentType.EFT
                                    dr("CheckNumber") = "EFT Number: " + dr("CheckNumber").ToString
                                Case PaymentType.Money_Order
                                    dr("CheckNumber") = "Money Order Number: " + dr("CheckNumber").ToString
                                Case PaymentType.Non_Cash
                                    dr("CheckNumber") = "Non Cash Reference #: " + dr("CheckNumber").ToString
                            End Select
                        End If

                    Next
                    '
                    For Each dr As DataRow In ds.Tables(0).Rows
                        dr("StuEnrollIdStr") = dr("StuEnrollId").ToString

                        '
                        'Set up student name as: "LastName, FirstName MI."
                        stuName = dr("LastName")
                        If Not (dr("FirstName") Is System.DBNull.Value) Then
                            If dr("FirstName") <> "" Then
                                stuName &= ", " & dr("FirstName")
                            End If
                        End If
                        If Not (dr("MiddleName") Is System.DBNull.Value) Then
                            If dr("MiddleName") <> "" Then
                                stuName &= " " & dr("MiddleName") & "."
                            End If
                        End If
                        dr("StudentName") = stuName
                        '
                        dr("Balance") = Convert.ToDecimal(ds.Tables(1).Rows(lastRow)("Balance"))
                        dr("LastName") = dr("CampusCount").ToString
                        '
                        'Apply mask to SSN.
                        If StudentIdentifier = "SSN" Then
                            If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                                If dr("StudentIdentifier") <> "" Then
                                    Dim temp As String = dr("StudentIdentifier")
                                    dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                End If
                            End If
                        End If
                    Next
                Else
                    'If no transactions were found, then clear the Student Info. collection of rows. 
                    ds.Tables(0).Rows.Clear()
                End If
            End If


        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
