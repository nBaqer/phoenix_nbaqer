Public Class SDFModulePageFacade
    Public Function GetAllSDF() As DataSet
        'Instantiate DAL component
        With New SDFModulePageDB
            'get the dataset with all SkillGroups
            Return .GetAllSDF()
        End With
    End Function
    'Public Function GetAllSDFVisibilty() As DataSet
    '    'Instantiate DAL component
    '    With New SDFModulePageDB
    '        'get the dataset with all SkillGroups
    '        Return .GetAllSDFVisibilty()
    '    End With
    'End Function
    Public Function UpdateListValue(ByVal UserName As String, ByVal selectedList() As String, ByVal sdfId As String) As String
        With New SDFModulePageDB
            Return .updateListValue(UserName, selectedList, sdfId)
        End With
    End Function
    Public Function GetAllModuleByResourceID(ByVal ResourceId As String) As DataSet
        'Instantiate DAL component
        With New SDFModulePageDB
            'get the dataset with all SkillGroups
            Return .GetAllModuleByResourceID(ResourceId)
        End With
    End Function
    Public Function GetAllModulesBySDF() As DataSet
        'Instantiate DAL component
        With New SDFModulePageDB
            'get the dataset with all SkillGroups
            Return .GetAllModuleBySDF()

        End With
    End Function
    Public Function DeleteSDF(ByVal SDFID As String, ByVal ModDate As DateTime) As String
        With New SDFModulePageDB
            Return .DeleteSDF(SDFID, ModDate)
        End With
    End Function
    'Public Function GetAllVisibilityByModSDF(ByVal SDFID As String, ByVal SDFText As String, ByVal ModuleCode As String) As DataSet
    '    'Instantiate DAL component

    '    With New SDFModulePageDB
    '        'get the dataset with all SkillGroups
    '        Return .GetAllVisibilityByModSDF(SDFID, SDFText, ModuleCode)
    '    End With
    'End Function
    Public Function GetAllFilterBySDF(ByVal SDFID As String) As DataSet
        'Instantiate DAL component
        With New SDFModulePageDB
            'get the dataset with all SkillGroups
            Return .GetAllModulePagesBySDF(SDFID)
        End With
    End Function
    'Public Function GetAllResourceSelected(ByVal SDFID As String, ByVal intModuleResId As Integer, Optional ByVal EntityId As Integer = 0) As DataSet
    '    'Instantiate DAL component
    '    With New SDFModulePageDB
    '        'get the dataset with all SkillGroups
    '        Return .GetAllResourceSelected(SDFID, intModuleResId, EntityId)
    '    End With
    'End Function
    'Public Function GetAllResourceNotSelected(ByVal SDFID As String, ByVal ModuleID As String, ByVal strURL As String) As DataSet
    '    'Instantiate DAL component
    '    With New SDFModulePageDB
    '        'get the dataset with all SkillGroups
    '        Return .GetAllResourceNotSelected(SDFID, ModuleID, strURL)
    '    End With
    'End Function
    Public Function GetAllPages(ByVal ModuleCode As String) As DataSet
        'Instantiate DAL component
        With New SDFModulePageDB
            'get the dataset with all SkillGroups
            Return .GetAllModulePages(ModuleCode)
        End With
    End Function
    Public Function GetAllUDFDatas(ByVal boolStatus As String) As DataSet
        With New SDFModulePageDB
            Return .GetAllUDFDatas(boolStatus)
        End With
    End Function
    Public Function GetUDFDetails(ByVal UdfId As String) As UDFInfo
        With New SDFModulePageDB
            Return .GetUDFDetails(UdfId)
        End With
    End Function
    'Public Function UpdateModule(ByVal ModulePage As StudentDocsInfo, ByVal PKID As String, ByVal user As String) As Integer

    '    With New SDFModulePageDB
    '        If (ModulePage.IsInDb = False) Then
    '            Return .AddModulePage(ModulePage, PKID, user)
    '        Else
    '            Return .UpdateModulePage(ModulePage, PKID, user)
    '        End If
    '    End With
    'End Function
    'Public Function GetModuleInfo(ByVal PKId As String) As StudentDocsInfo

    '    'Instantiate DAL component
    '    With New SDFModulePageDB
    '        'get the dataset with all SkillGroups
    '        Return .GetModuleDetails(PKId)
    '    End With

    'End Function
    'Public Function UpdateSDFModule(ByVal SDFID As String, ByVal intModuleResId As Integer, _
    '                                ByVal user As String, ByVal selectedCertifications() As String, _
    '                                ByVal selectedVisibilty() As String, _
    '                                ByVal entityId As Integer) As Integer

    '    '   Instantiate DAL component
    '    Dim ModulePage As New SDFModulePageDB

    '    'Update The Skills Based on StudentId and Skill GroupId
    '    Return ModulePage.UpdateModules(SDFID, intModuleResId, user, selectedCertifications, selectedVisibilty, entityId)
    'End Function
    Public Function UpdateUDFField(ByVal UDFInformations As UDFInfo, ByVal user As String) As String
        With New SDFModulePageDB
            If (UDFInformations.IsInDB = False) Then
                Return .AddUDF(UDFInformations, user)
            Else
                Return .UpdateUDF(UDFInformations, user)
            End If
        End With
    End Function
    'Public Function GetAvailableUDFPagesByResource(ByVal SDFID As String, ByVal intModuleResId As Integer, Optional ByVal intEntityId As Integer = 0) As DataSet
    '    With New SDFModulePageDB
    '        Return .GetAvailableUDFPagesByResource(SDFID, intModuleResId, intEntityId)
    '    End With
    'End Function
    Public Function GetAvailableModulesbyEntityId(ByVal entityID As Integer) As DataSet
        With New SDFModulePageDB
            Return .GetModulesForEntity(entityID)
        End With
    End Function
End Class
