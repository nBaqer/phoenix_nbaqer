'
' FAME
' Copyright (c) 2005, 2006
' Written by ThinkTron Corporation (www.thinktron.com)
'
' Project:  AdHoc Reports
' Description: Defines functions that are common throughout AdHoc Reporting

Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports FAME.AdvantageV1.DataAccess.Reports
Imports FAME.Advantage.Common
Imports Telerik.Web.UI

Namespace Reports

    Public Class RPTCommon
#Region "Advantage Specific integration"
        ''' <summary>
        ''' Initalizes parameters that used throughout TM
        ''' These parameters are stored in session variables which include
        ''' Session("userid") and Session("campusid")
        ''' </summary>        
        ''' <remarks></remarks>
        Public Shared Sub AdvInit(ByVal pg As Page)
            ' setup params needed throughout 
            If HttpContext.Current.Session("UserId") Is Nothing Then
                HttpContext.Current.Session("UserId") = HttpContext.Current.Request.Params("UserId")
            End If
            If HttpContext.Current.Session("UserName") Is Nothing Then
                HttpContext.Current.Session("UserName") = GetUserNameFromUserId(HttpContext.Current.Session("UserId"))
            End If
            If HttpContext.Current.Session("cmpid") Is Nothing Then
                HttpContext.Current.Session("cmpid") = HttpContext.Current.Request.Params("cmpid")
            End If
            If HttpContext.Current.Session("resid") Is Nothing Then
                HttpContext.Current.Session("resid") = HttpContext.Current.Request.Params("resid")
            End If

            ' display an error if the session has timed out
            If GetCurrentUserId() Is Nothing Then
                HttpContext.Current.Session("Error") = "Session has expired"
                Dim url As String = HttpContext.Current.Server.MapPath("ErrorPage.aspx")
                HttpContext.Current.Response.Redirect(url)
            End If
        End Sub

        Public Shared Function GetCurrentUserId() As String
            If Not HttpContext.Current.Request.Params("UserId") Is Nothing Then
                Return HttpContext.Current.Request.Params("UserId")
            End If
            Return HttpContext.Current.Session("UserId")
        End Function

        Public Shared Function GetCampusID() As String
            If Not HttpContext.Current.Request.Params("cmpid") Is Nothing Then
                Return HttpContext.Current.Request.Params("cmpid")
            End If
            Return HttpContext.Current.Session("cmpid")
        End Function

        Public Shared Function GetAdvAdminEmail() As String
            Dim myAdvAppSettings As AdvAppSettings

            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If
            Return myAdvAppSettings.AppSettings("FromEmailAddress")
        End Function

        ''' <summary>
        ''' Get a fullname from a userid
        ''' </summary>
        ''' <param name="UserId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetUserNameFromUserId(ByVal userId As String) As String
            Dim db As New DataAccess.DataAccess

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            ' check for a blank userid
            If userId Is Nothing Or userId = "" Then Return ""
            ' format select sql
            Dim sql As String = "select UserName from syUsers where UserId='" + userId + "'"
            Dim name As String = ""
            Try
                name = db.RunParamSQLScalar(sql)
            Catch ex As Exception
            End Try
            Return name
        End Function
#End Region
#Region "Debugging and error"
        Public Shared Sub DisplayErrorMessage(ByVal pg As Page, ByVal msg As String)
            Debug.WriteLine(msg)
            Alert(pg, msg)
        End Sub
#End Region
#Region "Browser Javascript Methods"
        Public Shared Sub SetBrowserStatusBar(ByRef p As Page, ByVal strStatus As String)
            Dim js As New StringBuilder
            js.Append("<script type=""text/javascript"">" & vbCrLf)
            js.Append("window.status=""" & strStatus)
            js.Append(""" " & vbCrLf)
            js.Append("</script>")
            p.ClientScript.RegisterStartupScript(p.GetType(), "jsStatus", js.ToString())
        End Sub

        Public Shared Sub SetBrowserTitle(ByRef p As Page, ByVal strStatus As String)
            Dim js As New StringBuilder
            js.Append("<script type=""text/javascript"">" & vbCrLf)
            js.Append("parent.document.title=""" & strStatus)
            js.Append(""" " & vbCrLf)
            js.Append("</script>")
            p.ClientScript.RegisterStartupScript(p.GetType(), "jsStatus", js.ToString())
        End Sub

        Public Shared Function GetJavsScriptPopup(ByVal popupUrl As String, _
                                                  ByVal width As Integer, _
                                                  ByVal height As Integer, _
                                                  ByVal retControlId As String) As String
            Dim js As New StringBuilder
            js.Append("var strReturn; " & vbCrLf)
            js.Append("strReturn=window.showModalDialog('")
            js.Append(popupUrl)
            js.Append("',null,'resizable:no;status:no;dialogWidth:")
            js.Append(width.ToString())
            js.Append("px;dialogHeight:")
            js.Append(height.ToString())
            js.Append("px;dialogHide:true;help:no;scroll:yes'); " & vbCrLf)
            If Not retControlId Is Nothing Then
                If retControlId.Length <> 0 Then
                    js.Append("if (strReturn != null) " & vbCrLf)
                    js.Append("     document.getElementById('")
                    js.Append(retControlId)
                    js.Append("').value=strReturn;" & vbCrLf)
                End If
            End If
            Return js.ToString()
        End Function

        Public Shared Sub Alert(ByRef p As Page, ByVal msg As String)
            msg = msg.Replace(Environment.NewLine, "\n")
            'msg = msg.Replace("\n", String.Empty)
            msg = msg.Replace("'", String.Empty)
            p.ClientScript.RegisterStartupScript(p.GetType(), "sendJS", "<script language='javascript'>window.alert('" & msg & "')</script>")
        End Sub

        Public Shared Sub AlertAjax(ByRef p As Page, ByVal msg As String, ByRef ajaxManager As RadAjaxManager)
            msg = msg.Replace(Environment.NewLine, "\n")
            msg = msg.Replace("'", String.Empty)
            msg = "window.alert('" & msg & "')"
            ajaxManager.ResponseScripts.Add(msg)
            'p.ClientScript.RegisterStartupScript(p.GetType(), "sendJS", "<script language='javascript'>window.alert('" & msg & "')</script>")
        End Sub
#End Region
#Region "Build DDL Methods"
        Public Shared Sub BuildStatusDDL(ByVal ddl As DropDownList)
            ddl.Items.Clear()
            ddl.Items.Add(New ListItem("--Select--", "true"))
            ddl.Items.Add(New ListItem("Active", "True"))
            ddl.Items.Add(New ListItem("Inactive", "False"))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildReportTypesDDL(ByVal userId As String, ByVal ddl As DropDownList)
            'Modified on 09/20/2006 By Balaji 
            'ddl.DataSource = AdHocRptFacade.GetReportTypes()
            'Get all entities as Report Types except Lender
            ddl.DataSource = AdHocRptFacade.GetReportTypesExceptLender(434)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "EntityId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildCategoriesDDL(ByVal reportType As Integer, ByVal ddl As DropDownList)
            ddl.DataSource = AdHocRptDB.GetCategories(reportType)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "CategoryId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildFieldsLB(ByVal categoryId As String, ByVal lb As ListBox)
            lb.DataSource = AdHocRptFacade.GetFields(categoryId)
            lb.DataTextField = "Caption"
            lb.DataValueField = "TblFldsId"
            lb.DataBind()
        End Sub

        Public Shared Sub BuildFieldsLBEx(ByVal categoryId As String, ByVal lb As ListBox)
            lb.DataSource = AdHocRptFacade.GetFields(categoryId)
            lb.DataTextField = "Caption"
            lb.DataValueField = "FieldData"
            lb.DataBind()
        End Sub

        Public Shared Sub BuildReportFieldsLB(ByVal reportId As String, ByVal lb As ListBox)
            lb.DataSource = AdHocRptDB.GetReportFields(reportId)
            lb.DataTextField = "Descrip"
            lb.DataValueField = "AdHocFieldId"
            lb.DataBind()
        End Sub
        Public Shared Sub BuildCampusGroups(ByVal ddl As DropDownList)
            'Modified on 09/20/2006 
            'ddl.DataSource = AdHocRptFacade.GetCampusGroups()
            ddl.DataSource = AdHocRptFacade.GetCampusGroups(GetCampusID(), GetCurrentUserId())
            ddl.DataTextField = "CampGrpDescrip"
            ddl.DataValueField = "CampGrpId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            'Modified on 09/20/2006
            'If ddl.Items.Count > 1 Then
            '    ddl.SelectedIndex = 1
            'Else
            '    ddl.SelectedIndex = 0
            'End If
            ddl.SelectedIndex = 0
        End Sub
        Public Shared Sub BuildModulesDL(ByVal UserId As String, ByVal ReportId As String, ByVal dl As DataList)
            Dim ds As DataSet = Nothing
            'If ReportId Is Nothing Or ReportId = "" Then
            'ds = RolesFacade.GetValidModulesForUser(UserId)
            'Else
            ds = AdHocRptFacade.GetPermissions(UserId, ReportId)
            'End If
            dl.DataSource = ds
            dl.DataBind()
        End Sub
        ' here we had to make changes to show(filter) only category fields allowed for a given template
        Public Shared Sub BuildFieldGroupsLB(ByRef lb As ListBox)
            Dim ds As New DataSet
            Try
                lb.Items.Clear()
                ' read from the xml file all the categories
                Dim xmlFileName As String = lb.Page.MapPath("xml/MsgCategories.xml")
                ds.ReadXml(xmlFileName)
                lb.DataTextField = "Category"
                lb.DataValueField = "Category"
                lb.DataSource = ds
                lb.DataBind()
            Catch ex As Exception
            End Try
            ds.Dispose()
        End Sub
#End Region
    End Class

End Namespace