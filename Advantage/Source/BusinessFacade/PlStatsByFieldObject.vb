Public Class PlStatsByFieldObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim plStatsByFld As New PlStatsByFieldDB
        Dim ds As New DataSet       
        ds = plStatsByFld.GetStatsByField(rptParamInfo)
        ' The report, whatever its frontend is, will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region

End Class
