
Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllBDetailObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SFE_AllBDetail"

	Private dsRaw As New DataSet
	Private dtRptRaw As New DataTable
    Private dsRpt As New DataSet
    Private dteAgeAsOf As DateTime

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		'get raw DataSet using passed-in parameters
		dsRaw = SFE_AllBDetailObjectDB.GetReportDataSetRaw(RptParamInfo)

        ' set date on which to calculate students' ages
        dteAgeAsOf = RptParamInfo.RptEndDate

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		' pre-process raw data, first process student enrollment info, then process with raw student 
		'	data to obtain prepared raw data for processing
        dtRptRaw = SFE_AllBCommonObject.PreProcessRawData(dsRaw.Tables(TblNameStudents), IPEDSFacade.GetProcessedEnrollInfo(dsRaw), dteAgeAsOf)

		' get IPEDS Age Categories
		Dim dtAgeCatInfo As DataTable = IPEDSFacade.GetIPEDSAgeCatInfo
		Dim drAgeCatInfo As DataRow

		' create tables to hold final report data
		For i As Integer = 1 To SFE_AllBCommonObject.NumRptParts
			With dsRpt.Tables.Add(MainTableName & "Part" & i).Columns
				.Add(RptStuIdentifierColName, GetType(System.String))
				.Add("Name", GetType(System.String))
				.Add("AgeDescrip", GetType(System.String))
				.Add("Ind_Male", GetType(System.String))
				.Add("Ind_Female", GetType(System.String))
			End With
		Next

		' for each Program and Attendance type, loop thru age categories, and process rows for each age category

		' process age categories for full time undergrads
		For Each drAgeCatInfo In dtAgeCatInfo.Rows
			ProcessRows(drAgeCatInfo, ProgTypeUndergraduate, AttendTypeFullTime, dsRpt.Tables(MainTableName & "Part1"))
		Next

		' process age categories for part time undergrads
		For Each drAgeCatInfo In dtAgeCatInfo.Rows
			ProcessRows(drAgeCatInfo, ProgTypeUndergraduate, AttendTypePartTime, dsRpt.Tables(MainTableName & "Part2"))
		Next

		' process age categories for full time graduates
		For Each drAgeCatInfo In dtAgeCatInfo.Rows
			ProcessRows(drAgeCatInfo, ProgTypeGraduate, AttendTypeFullTime, dsRpt.Tables(MainTableName & "Part3"))
		Next

		' process age categories for part time graduates
		For Each drAgeCatInfo In dtAgeCatInfo.Rows
			ProcessRows(drAgeCatInfo, ProgTypeGraduate, AttendTypePartTime, dsRpt.Tables(MainTableName & "Part4"))
		Next

		' process age categories for full time first professionals
		For Each drAgeCatInfo In dtAgeCatInfo.Rows
			ProcessRows(drAgeCatInfo, ProgTypeFirstProf, AttendTypeFullTime, dsRpt.Tables(MainTableName & "Part5"))
		Next

		' process age categories for part time first professionals
		For Each drAgeCatInfo In dtAgeCatInfo.Rows
			ProcessRows(drAgeCatInfo, ProgTypeFirstProf, AttendTypePartTime, dsRpt.Tables(MainTableName & "Part6"))
		Next

		' return report dataset
		Return dsRpt

	End Function

	Private Sub ProcessRows(ByVal drAgeCatInfo As DataRow, ByVal ProgTypeDescrip As String, ByVal AttendTypeDescrip As String, ByRef dtRpt As DataTable)
		Dim i As Integer
		Dim drRpt As DataRow
		Dim AgeRangeLower As Integer = 0, AgeRangeUpper As Integer = 0
		Dim FilterString As String

		' create dataview from students in raw dataset, filtered according to the 
		'	passed in upper and lower age limits, and passed in program type and attendance type
		Dim dvRptRaw As New DataView(dtRptRaw)

		If Not (drAgeCatInfo("AgeCatRangeLower") Is DBNull.Value) Then
			AgeRangeLower = CInt(drAgeCatInfo("AgeCatRangeLower"))
		End If
		If Not (drAgeCatInfo("AgeCatRangeUpper") Is DBNull.Value) Then
			AgeRangeUpper = CInt(drAgeCatInfo("AgeCatRangeUpper"))
		End If

		If AgeRangeLower <> 0 And AgeRangeUpper <> 0 Then
			FilterString = "Age >= " & AgeRangeLower & " AND Age <= " & AgeRangeUpper
		ElseIf AgeRangeLower = 0 Then
			FilterString = "Age <= " & AgeRangeUpper
		Else ' AgeRangeUpper = 0
			FilterString = "Age >= " & AgeRangeLower
		End If

		FilterString &= "AND ProgTypeDescrip LIKE '" & ProgTypeDescrip & "' AND " & _
						"AttendTypeDescrip LIKE '" & AttendTypeDescrip & "'"

		dvRptRaw.RowFilter = FilterString

		' if student dataview has records, loop thru records and process raw data 
		'	to create report records
		If dvRptRaw.Count > 0 Then
			For i = 0 To dvRptRaw.Count - 1
				drRpt = dtRpt.NewRow

				' set Student Id and name 
				IPEDSFacade.SetStudentInfo(dvRptRaw(i), drRpt)

				' record age category description
				drRpt("AgeDescrip") = drAgeCatInfo("AgeCatDescrip").ToString

				' set indicators as appropriate
				drRpt("Ind_Male") = DBNull.Value
				drRpt("Ind_Female") = DBNull.Value
				If dvRptRaw(i)("GenderDescrip") = "Men" Then
					drRpt("Ind_Male") = "X"
				Else
					drRpt("Ind_Female") = "X"
				End If

				' add new report row to report table
				dtRpt.Rows.Add(drRpt)
			Next

		' if student dataview has no records, create record with description for
		'	age category, NULL data otherwise
		Else
			drRpt = dtRpt.NewRow

			drRpt(RptStuIdentifierColName) = DBNull.Value
			drRpt("Name") = DBNull.Value
			drRpt("AgeDescrip") = drAgeCatInfo("AgeCatDescrip").ToString
			drRpt("Ind_Male") = DBNull.Value
			drRpt("Ind_Female") = DBNull.Value

			' add empty report row to report table
			dtRpt.Rows.Add(drRpt)

		End If

	End Sub

End Class
