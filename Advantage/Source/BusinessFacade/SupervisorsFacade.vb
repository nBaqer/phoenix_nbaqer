' ===============================================================================
' FAME AdvantageV1.BusinessFacade
'
' SupervisorsFacade.vb
'
' Supervisors and FinancialAidSupervisors Services Interface (Facade). 
'
' This class will be used to invoke the Data Access Logic for Academic Supervisors and Financial Aid Supervisors.
' An SupervisorTypeId param is passed to all methods of this class. This param is used to call either the 
' Supervisors methods or the FinancialAidSupervisors methods on SupervisorsDB.
'
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class SupervisorsFacade
    Public Function GetSupervisorsPerCampus(ByVal campusId As String) As DataSet
        '   return Dataset
        Return (New SupervisorsDB).GetSupervisorsPerCampus(campusId)
    End Function
    Public Function GetInstructorsAssignedToASpecificSupervisor(ByVal SupervisorId As String, ByVal CampusId As String) As DataSet
        '   return Dataset
        Return (New SupervisorsDB).GetInstructorsAssignedToASpecificSupervisor(SupervisorId, CampusId)
    End Function
    Public Function GetInstructorsNotAssignedToAnySupervisor(ByVal campusId As String, ByVal supervisorId As String) As DataSet
        '   return Dataset
        Return (New SupervisorsDB).GetInstructorsNotAssignedToAnySupervisor(campusId, supervisorId)
    End Function
    Public Function UpdateListOfInstructorsAssignedToAnSupervisor(ByVal campusId As String, ByVal SupervisorId As String, ByVal user As String, ByVal selectedInstructors() As String) As String
        '   return Dataset
        Return (New SupervisorsDB).UpdateListOfInstructorsAssignedToAnSupervisor(campusId, SupervisorId, user, selectedInstructors)
    End Function

End Class

