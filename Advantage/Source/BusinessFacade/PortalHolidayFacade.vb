' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' StudentAccountsFacade.vb
'
' StudentsAccountsFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class PortalHolidayFacade
    Public Function GetAllHolidays(Optional ByVal campusid As String = "") As DataSet

        '   get the dataset with all Holidays
        Return (New PortalHolidaysDB).GetAllHolidays(campusid)

    End Function
    Public Function GetHolidayInfo(ByVal HolidayId As String) As HolidayInfo

        '   get the HolidayInfo
        Return (New PortalHolidaysDB).GetHolidayInfo(HolidayId)

    End Function
    Public Function UpdateHolidayInfo(ByVal HolidayInfo As HolidayInfo, ByVal user As String) As String

        '   If it is a new account do an insert. If not, do an update
        If Not (HolidayInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New PortalHolidaysDB).AddHolidayInfo(HolidayInfo, user)
        Else
            '   return integer with update results
            Return (New PortalHolidaysDB).UpdateHolidayInfo(HolidayInfo, user)
        End If

    End Function
    Public Function DeleteHolidayInfo(ByVal HolidayId As String, ByVal modDate As DateTime) As String

        '   delete HolidayInfo ans return string result
        Return (New PortalHolidaysDB).DeleteHolidayInfo(HolidayId, modDate)

    End Function
    Public Function IsHoliday(ByVal [date] As Date) As Boolean
        'return boolean
        Return (New PortalHolidaysDB).IsHoliday([date])
    End Function
End Class
