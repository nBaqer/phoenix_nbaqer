Public Class JobSearchFacade
    Public Function GetProgramVersions() As DataSet
        Dim PrgVer As New JobSearchDB
        Return PrgVer.GetProgramVersions()
    End Function
    Public Function GetSkills() As DataSet
        Dim PrgVer As New JobSearchDB
        Return PrgVer.GetSkills()
    End Function
    Public Function GetCounties() As DataSet
        Dim PrgVer As New JobSearchDB
        Return PrgVer.GetCounties
    End Function
    Public Function GetCountyCodes() As DataSet
        Dim PrgVer As New JobSearchDB
        Return PrgVer.GetCountyCodes
    End Function
    Public Function GetTitles() As DataSet
        Dim PrgVer As New JobSearchDB
        Return PrgVer.GetJobTitles
    End Function
    Public Function GetExpertiseLevel() As DataSet
        Dim PrgVer As New JobSearchDB
        Return PrgVer.GetExpertiseLevel()
    End Function
    Public Function GetFullPartTime() As DataSet
        Dim PrgVer As New JobSearchDB
        Return PrgVer.GetFullPartTime
    End Function

    Public Function JobSearchResults(ByVal strPrgVerID As String, ByVal strMinSalary As String, ByVal strMaxSalary As String, ByVal strCountyId As String, ByVal strAvailableDate As String, ByVal strSkillId As String, ByVal strCampus As String, ByVal strFullTimeId As String, ByVal strAvailableDateTo As String) As DataSet
        Dim SearchResult As New JobSearchDB
        Dim ds As New DataSet
        ds = SearchResult.JobSearchResults(strPrgVerID, strMinSalary, strMaxSalary, strCountyId, strAvailableDate, strSkillId, strCampus, strFullTimeId, strAvailableDateTo)

        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        ''  Dim zipMask As String
        Dim dr As DataRow

        'Get the mask for phone numbers and zip
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds
    End Function
End Class
