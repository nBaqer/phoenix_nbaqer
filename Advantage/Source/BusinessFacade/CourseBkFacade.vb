Public Class CourseBkFacade
    Public Function LoadDataSet(ByVal CourseID As String) As DataSet

        Dim cfDB As New CourseBkDB
        Dim ds As New DataSet
        Dim tbl1 As New DataTable
        Dim tbl2 As New DataTable

        ds = cfDB.LoadDS(CourseID)

        Return ds
    End Function

    Public Function InsertCourseBkInfo(ByVal CourseBkObj As CourseBkInfo, ByVal CourseID As String)
        Dim cfDB As New CourseBkDB

        cfDB.InsertCourseBkToDB(CourseBkObj, CourseID)
    End Function

    Public Function DeleteCourseBkInfo(ByVal CourseBkObj As CourseBkInfo, ByVal CourseID As String)
        Dim cfDB As New CourseBkDB

        cfDB.DeleteCourseBkFrmDB(CourseBkObj, CourseID)
    End Function

    Public Function UpdateCourseBkInfo(ByVal CourseFeeObj As CourseBkInfo, ByVal CourseID As String)
        Dim cfDB As New CourseBkDB

        cfDB.UpdateCourseBkInDB(CourseFeeObj, CourseID)
    End Function
End Class
