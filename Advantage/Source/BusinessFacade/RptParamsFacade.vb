Imports FAME.AdvantageV1.DataAccess

Public Class RptParamsFacade
    Public Function GetSavedPrefsList() As DataTable
        Dim rpt As New RptParams
        Return rpt.GetSavedPrefsList
    End Function

    Public Function GetUserRptPrefs(ByVal prefid As String) As DataSet
        Dim rpt As New RptParams
        Return rpt.GetUserRptPrefs(prefid)
    End Function

    Public Function GetRptParams() As DataSet
        Dim rpt As New RptParams
        Return rpt.GetRptParams
    End Function

    Public Function GetRptParamsForFilterOther(ByVal resourceId As Integer) As DataTable
        Dim rpt As New RptParams
        Return rpt.GetRptParamsForFilterOther(resourceId)
    End Function

    Public Sub AddUserPrefInfo(ByVal prefId As String, ByVal userId As String, _
            ByVal resId As Integer, ByVal prefName As String)
        Dim rpt As New RptParams
        rpt.AddUserPrefInfo(prefId, userId, resId, prefName)
    End Sub

    Public Sub UpdateUserPrefInfo(ByVal prefId As String, ByVal prefName As String)
        Dim rpt As New RptParams
        rpt.UpdateUserPrefInfo(prefId, prefName)
    End Sub

    Public Sub DeleteUserPrefInfo(ByVal prefId As String)
        Dim rpt As New RptParams
        rpt.DeleteUserPrefInfo(prefId)
    End Sub

    Public Sub AddUserSortPrefs(ByVal prefId As String, ByVal rptParamId As Integer, _
            ByVal Seq As Integer)
        Dim rpt As New RptParams
        rpt.AddUserSortPrefs(prefId, rptParamId, Seq)
    End Sub

    Public Sub DeleteUserSortPrefs(ByVal prefId As String)
        Dim rpt As New RptParams
        rpt.DeleteUserSortPrefs(prefId)
    End Sub

    Public Function GetFilterListValues(ByVal ddlId As Object, Optional ByVal showall As Boolean = False, Optional userId As String = "") As DataTable
        Dim rpt As New RptParams
        Return rpt.GetFilterListValues(ddlId, showall, userId)
    End Function

    Public Sub AddUserFilterListPrefs(ByVal prefId As String, ByVal rptParamId As Integer, _
            ByVal fldValue As String)
        Dim rpt As New RptParams
        rpt.AddUserFilterListPrefs(prefId, rptParamId, fldValue)
    End Sub

    Public Sub DeleteUserFilterListPrefs(ByVal prefId As String)
        Dim rpt As New RptParams
        rpt.DeleteUserFilterListPrefs(prefId)
    End Sub

    Public Sub AddUserFilterOtherPrefs(ByVal prefId As String, ByVal rptParamId As Integer, _
        ByVal opId As Integer, ByVal fldValue As String)
        Dim rpt As New RptParams
        rpt.AddUserFilterOherPrefs(prefId, rptParamId, opId, fldValue)
    End Sub

    Public Sub DeleteUserFilterOtherPrefs(ByVal prefId As String)
        Dim rpt As New RptParams
        rpt.DeleteUserFilterOtherPrefs(prefId)
    End Sub

    Public Function HasRptParams() As Boolean
        Dim rpt As New RptParams
        Return rpt.HasRptParams
    End Function
    ''Added by Saraswathi
    ''Added by saraswathi lakshmanan
    ''Added on May 11 2009
    ''Fix for mantis case: 14745
    ''When a campus group is selected, The campus available in htat campus group is found. 
    ''And  the other campus groups which holds the same campus from the selected group is found.
    ''Thus the entities whose, campus groups mapped to the selected campus groups are listed.
    ''EG: Miami Campusgroup has Miami campus and Orlando Campus group has orlando campus.
    ''Florida campus Group has MI and Or campus. Tampa Campus group has Tampa Campus
    ''All campus group has MI, Or, Ta Campuses
    ''When we select Florida Campus Group, Anything mapped to Florida, Miami, Orlando and All are listed 
    Public Function GetCampusgroups(ByVal CampGrpid As String) As DataSet
        Dim rpt As New RptParams
        Return rpt.GetCampusgroups(CampGrpid)
    End Function
    Public Sub AddUserPrefForDailyCompletedHours(ByVal prefId As String, ByVal CampGrpId As String, _
          ByVal TermId As String, ByVal PrgVerId As String, ByVal FirstName As String, ByVal LastName As String, _
          ByVal TermStartDate As DateTime, ByVal TermEndDate As DateTime, ByVal AttendanceStartDate As DateTime, _
          ByVal AttendanceEndDate As DateTime)
        Dim rpt As New RptParams
        rpt.AddUserPrefForDailyCompletedHours(prefId, CampGrpId, TermId, PrgVerId, FirstName, LastName, TermStartDate, TermEndDate, AttendanceStartDate, AttendanceEndDate)
    End Sub
    Public Function GetUserRptPrefsForDailyCompletedHours(ByVal prefId As String) As DataSet
        Dim rpt As New RptParams
        Return rpt.GetUserRptPrefsForDailyCompletedHours(prefId)
    End Function
    Public Sub UpdateUserPrefForDailyCompletedHours(ByVal prefId As String, ByVal CampGrpId As String, _
          ByVal TermId As String, ByVal PrgVerId As String, ByVal FirstName As String, ByVal LastName As String, _
          ByVal TermStartDate As DateTime, ByVal TermEndDate As DateTime, ByVal AttendanceStartDate As DateTime, _
          ByVal AttendanceEndDate As DateTime)
        Dim rpt As New RptParams
        rpt.UpdateUserPrefForDailyCompletedHours(prefId, CampGrpId, TermId, PrgVerId, FirstName, LastName, TermStartDate, TermEndDate, AttendanceStartDate, AttendanceEndDate)
    End Sub
    Public Sub DeleteUserRptPrefsForDailyCompletedHours(ByVal prefId As String)
        Dim rpt As New RptParams
        rpt.DeleteUserRptPrefsForDailyCompletedHours(prefId)
    End Sub
    ''New Code Added By Vijay Ramteke On September 08, 2010 For Mantis Id 19639
    Public Function HasStudentGroupForScheduling(Optional ByVal CampusId As String = "") As Boolean
        Dim rpt As New RptParams
        Return rpt.HasStudentGroupForScheduling(CampusId)
    End Function
    ''New Code Added By Vijay Ramteke On September 08, 2010 For Mantis Id 19639
End Class
