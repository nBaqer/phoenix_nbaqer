
Public Class AttNoPostedObject
    Inherits BaseReportFacade

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As ReportParamInfo) As DataSet
        Dim obj As New AttendanceDB
        Dim ds As DataSet = New DataSet()
        Dim startDate As DateTime
        Dim endDate As DateTime

        'Set Start end Date Period
         Dim dates() As String = rptParamInfo.FilterOtherString.Trim().Replace("AND", ",").Replace("Meeting Dates Between (", "").Replace(")", "").Split(",")
         startDate = dates(0)
         endDate = dates(1)
    
        startDate =New DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0) 
        
        'Check if endDate > yesterday and organize according
        endDate = If(endDate > DateTime.Now.Date.AddDays(-1), DateTime.Now.Date.AddDays(-1), endDate)
        endDate = New DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59)
        
        'Check if start >= end period
        If endDate < startDate Then
             Return ds 'Return ds = 0 if so
        End If
        
        'Incorpore new Values in rptParamInfo
        rptParamInfo.StartDate = startDate
        rptParamInfo.EndDate = EndDate
        Debug.WriteLine("BeginData: {0}  EndDate: {1}", startDate, endDate)
 
       'Get the list of holidays day of the school from DB
        Dim holidayDab As HolidaysDB = New HolidaysDB()
        Dim holidaysInfoList As IList (Of HolidayDay) = holidayDab.GetHolidaysDays(startDate , endDate )

        'Get the NoAttendanceCourses prospect list from DB
        Dim noAttendanceCourseProspectsList As IList(Of NoAttendanceCourses) = obj.GetCalendarTemplate(rptParamInfo)

        'For Each Course
        For Each course As NoAttendanceCourses In noAttendanceCourseProspectsList

        'Calculate the data period to use and if startdate <= enddate
           course.DateStart =  If(course.DateStart < startDate, startDate,course.DateStart)
            course.DateEnd = If(course.DateEnd > endDate, endDate, course.DateEnd)
            If course.DateStart > course.DateEnd Then
                course.MarkToDelete = True
                Continue For 'Out of course range
            End If
        
            course.FillCalendarDate() 'Calculate the calendar day for the Course
            course.CleanHolidayDays(holidaysInfoList) 'Clean the data schedule of Holidays days

            'Get from database all attendance reported days for the period of time
            Dim attList = obj.GetAttendancePerMeetingId(course.ClsSectionMeetingId, course.DateStart, course.DateEnd)

           'Compare all get dates with the DateList and delete if has coincidence
            course.MarkToDeleteCompleteAttendanceReported(attList)
        Next 

        'Create the dataset for cristal report
        ds = NoAttendanceCourses.CreateDataSet(noAttendanceCourseProspectsList)

        rptParamInfo.FilterOtherString = rptParamInfo.FilterOtherString.Replace("AND", " - ")
        
        Return ds 'return the created DataTable.

    End Function


#Region "Original Report"

    'Public Overrides Function GetReportDataSet(ByVal rptParamInfo As ReportParamInfo) As DataSet
    '    Dim obj As New AttendanceDB
    '    Dim ds As DataSet
    '    ds = obj.GetNoAttendancePosted(rptParamInfo)
    '    Return ds
    'End Function

#End Region

End Class
