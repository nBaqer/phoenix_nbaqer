Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.AdvantageV1.BusinessFacade.SGR_CommonObject

Public Class SGR_4YR1SummaryObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SGR_4YR1Summary"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet
	Private RptParamInfo As ReportParamInfoIPEDS

	Private DateFilter As String
	Private dvStuEnrollInfo As DataView

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As Common.ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SGR_4YR1SummaryObjectDB.GetReportDatasetRaw(RptParamInfo)

		' save passed-in report parameter info 
		Me.RptParamInfo = RptParamInfo

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		Dim dtGenderInfo As DataTable = IPEDSFacade.GetIPEDSGenderInfo
		Dim drGenderInfo As DataRow
		Dim dtEthnicInfo As DataTable = IPEDSFacade.GetIPEDSEthnicInfo
		Dim drEthnicInfo As DataRow

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add("GenderDescrip", GetType(String))
			.Add("EthCodeDescrip", GetType(String))
			.Add("Count_RevCohort", GetType(System.Int32))
			.Add("Count_Bachelors", GetType(System.Int32))
		End With

		' create student Enrollments DataView to be used when processing records
		dvStuEnrollInfo = New DataView(IPEDSFacade.GetProcessedEnrollInfo(dsRaw))

		' set up date filter to be used when processing records
		DateFilter = EnrollDateFilter(RptParamInfo)

		' loop thru genders, and ethnic groups for each gender, and process matching records in raw dataset
		For Each drGenderInfo In dtGenderInfo.Rows
			For Each drEthnicInfo In dtEthnicInfo.Rows
				ProcessRows(drGenderInfo("GenderDescrip"), drEthnicInfo("EthCodeDescrip"))
			Next
		Next

		' return the Report DataSet
		Return dsRpt
	End Function

	Private Sub ProcessRows(ByVal GenderDescrip As String, ByVal EthCodeDescrip As String)
		Dim i As Integer
		Dim drRpt As DataRow
		Dim dvStudents As DataView

		Dim Count_Bachelors As Integer = 0

		' create dataview from students in raw dataset, filtered according to passed in gender and ethnic ids
		dvStudents = New DataView(dsRaw.Tables(TblNameStudents), _
								  "GenderDescrip LIKE '" & GenderDescrip & "' AND EthCodeDescrip LIKE '" & EthCodeDescrip & "'", _
								  "", DataViewRowState.CurrentRows)

		For i = 0 To dvStudents.Count - 1
			' count if student was Bachelor's or equivalent
			dvStuEnrollInfo.RowFilter = "StudentId = '" & dvStudents(i)("StudentId").ToString & "'"
			If (dvStuEnrollInfo(0)("DegCertSeekingDescrip").ToString = DegCertSeekingOtherDegree Or _
				dvStuEnrollInfo(0)("DegCertSeekingDescrip").ToString.StartsWith(DegCertSeekingFirstTime)) And _
			   dvStuEnrollInfo(0)("ProgTypeDescrip") = ProgTypeUndergraduate Then
				Count_Bachelors += 1
			End If
		Next

		' add new summary row to report
		drRpt = dsRpt.Tables(MainTableName).NewRow

		' set Gender and Ethnic Code descriptions
		drRpt("GenderDescrip") = GenderDescrip
		drRpt("EthCodeDescrip") = EthCodeDescrip

		' set summary counters
		drRpt("Count_RevCohort") = dvStudents.Count
		drRpt("Count_Bachelors") = Count_Bachelors

		' add row to report DataSet
		dsRpt.Tables(MainTableName).Rows.Add(drRpt)

	End Sub

End Class
