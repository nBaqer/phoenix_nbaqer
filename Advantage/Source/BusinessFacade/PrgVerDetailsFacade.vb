Public Class PrgVerDetailsFacade
    Public Function GetAllPrgVerDS(ByVal PrgVerId As String, Optional ByVal CampusId As String = "") As DataSet
        Dim ds As New DataSet
        Dim row As DataRow
        Dim db As New PrgVerDetailsDB

        '   get the dataset with all PrgVerDocs
        ds = db.GetAllPrgVerDS(PrgVerId, CampusId)


        'Add a new column called ChildDescrip to the datatable 
        Dim col1 As DataColumn = ds.Tables("Selected").Columns.Add("ID", GetType(String))
        'Dim col2 As DataColumn = ds.Tables("Selected").Columns.Add("Descrip", GetType(String))
        Dim col3 As DataColumn = ds.Tables("Selected").Columns.Add("Type", GetType(Integer))

        If ds.Tables("Selected").Rows.Count = 0 Then
            'Create a new column called ChildId, make this the primary key.
            Dim fk0(0) As DataColumn
            fk0(0) = ds.Tables("Selected").Columns("ID")
            ds.Tables("Selected").PrimaryKey = fk0
        End If
        If ds.Tables("Selected").Rows.Count > 0 Then
            'Load a datatable with ChildId, ChildTyp and ChildDescrip

            For Each row In ds.Tables("Selected").Rows
                If Not row.IsNull("adReqId") Then
                    row("ID") = row("adReqId")
                    'row("Descrip") = db.GetReqName(row("adReqId").ToString)
                    row("Type") = 1
                ElseIf Not row.IsNull("ReqGrpId") Then
                    row("ID") = row("ReqGrpId")
                    'row("Descrip") = db.GetReqGrpName(row("ReqGrpId").ToString)
                    row("Type") = 2
                End If

            Next
            'Create a new column called ChildId, make this the primary key.
            Dim fk0(0) As DataColumn
            fk0(0) = ds.Tables("Selected").Columns("ID")
            ds.Tables("Selected").PrimaryKey = fk0
            ds.Tables("Selected").AcceptChanges()
        End If

        Return ds
    End Function
    Public Function GetCourseGrpName(ByVal courseGrpId As String) As String
        Dim DB As New CourseGrpsDB

        Return DB.GetCourseGrpName(courseGrpId)
    End Function
    Public Function GetPrgVersions(Optional ByVal CampusId As String = "") As DataSet

        '   Instantiate DAL component
        Dim DB As New PrgVerDetailsDB

        '   get the dataset with all degrees
        Return DB.GetAllPrgVersions(CampusId)

    End Function
    Public Function GetReqsForReqGroup(ByVal ReqGrpId As String) As DataTable
        Dim db As New PrgVerDetailsDB

        Return db.GetReqsForReqGroup(ReqGrpId)
    End Function


    Public Function DoesReqExistsInCourseGroup(ByVal reqId As String, ByVal ReqGrpId As String) As Boolean
        Dim dt As DataTable
        Dim dr As DataRow

        dt = GetReqsForReqGroup(ReqGrpId)

        For Each dr In dt.Rows
            If dr("adReqId").ToString() = reqId Then
                Return True
            End If
        Next

        Return False

    End Function
    Public Function GetFirstCommonRequirement(ByVal ReqGrpId As String, ByVal reqsCollection As ArrayList) As String
        Dim dt1 As DataTable
        Dim dt2 As DataTable
        Dim dr As DataRow
        Dim dr2 As DataRow
        Dim rInfo As New ReqInfo

        'Get the reqs for the course grp that is passed in
        dt1 = GetReqsForReqGroup(ReqGrpId)

        If dt1.Rows.Count = 0 Or reqsCollection.Count = 0 Then
            Return ""
        Else
            'For each item in the collection. If the item is a course group then we will populate dt2 with the
            'reqs for it. We will then loop through and see if there are any common courses.
            For Each rInfo In reqsCollection

                If rInfo.ReqTypeId = 1 Then
                    For Each dr In dt1.Rows
                        If dr("adReqId").ToString() = rInfo.ReqId Then
                            Return rInfo.ReqDescription
                        End If
                    Next
                    Return ""
                ElseIf rInfo.ReqTypeId = 2 Then
                    dt2 = GetReqsForReqGroup(rInfo.ReqId.ToString)
                    For Each dr In dt1.Rows
                        For Each dr2 In dt2.Rows
                            If dr("adReqId").ToString() = dr2("adReqId").ToString() Then
                                Return dr("Descrip")
                            End If
                        Next
                    Next
                    Return ""

                End If

            Next
        End If

    End Function

    Public Function AssignReqsToPrgVer(ByVal ChildInfoObj As ReqInfo, ByVal ID As String, ByVal user As String)
        Dim PrgDB As New PrgVerDetailsDB

        PrgDB.AssignReqsToPrgVer(ChildInfoObj, ID, user)
    End Function

    Public Function DeleteReqsFrmPrgVer(ByVal ChildInfoObj As ReqInfo, ByVal ID As String) As String
        Dim PrgDB As New PrgVerDetailsDB

        Return PrgDB.DeleteReqsFrmPrgVer(ChildInfoObj, ID)

    End Function
    Public Function UpdatePrgVerDef(ByVal ChildInfoObj As ReqInfo, ByVal ID As String, ByVal user As String)
        Dim PrgDB As New PrgVerDetailsDB

        'PrgDB.UpdatePrgVerDef(ChildInfoObj, ID, user)
    End Function
End Class
