Public Class CoursePrereqFacade
    Public Function GetAllCourses(ByVal CampusId As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New CoursePrereqDB
        Dim ds As New DataSet

        '   get the dataset with all degrees
        ds = DB.GetAllCourses(CampusId)

        With ds.Tables(0)
            '.PrimaryKey = New DataColumn() {.Columns("ReqId")}

            If .Rows.Count > 0 Then
                'Add a new column called CodeAndDescrip to the datatable 
                Dim col As DataColumn = .Columns.Add("CodeAndDescrip", Type.GetType("System.String"))

                '   column contains course code and description in this format: "(Code) Descrip"
                col.AllowDBNull = True
                col.Expression = "'(' + Code + ') ' + Descrip"
            End If
        End With
        Return ds
    End Function
    Public Function GetAvailSelectedCourses(ByVal CourseId As String, ByVal CampusId As String, ByVal Prgverid As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New CoursePrereqDB
        Dim ds As New DataSet
        Dim tbl1 As New DataTable
        Dim tbl2 As New DataTable

        '   get the dataset with all degrees          
        ds = DB.GetAvailSelectedCourses(CourseId, CampusId, Prgverid)

        ds = ConcatenateCourseCodeAndDescrip(ds, "Courses")

        tbl1 = ds.Tables("AvailCourses")
        tbl2 = ds.Tables("SelectedCourses")

        'Make the ChildId column the primary key
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("ReqId")}
        End With
        With tbl2
            .PrimaryKey = New DataColumn() {.Columns("PreCoReqId")}
        End With

        Return ds

    End Function
    Public Function ConcatenateCourseCodeAndDescrip(ByVal ds As DataSet, ByVal tableName As String) As DataSet
        'Dim dt As DataTable = ds.Tables(tableName)

        With ds.Tables("AvailCourses")
            '.PrimaryKey = New DataColumn() {.Columns("ReqId")}

            If .Rows.Count > 0 Then
                'Add a new column called CodeAndDescrip to the datatable 
                Dim col As DataColumn = .Columns.Add("CodeAndDescrip", Type.GetType("System.String"))

                '   column contains course code and description in this format: "(Code) Descrip"
                col.AllowDBNull = True
                col.Expression = "'(' + Code + ') ' + Descrip"
            End If
        End With

        With ds.Tables("SelectedCourses")
            '.PrimaryKey = New DataColumn() {.Columns("ReqId")}

            If .Rows.Count > 0 Then
                'Add a new column called CodeAndDescrip to the datatable 
                Dim col As DataColumn = .Columns.Add("CodeAndDescrip", Type.GetType("System.String"))

                '   column contains course code and description in this format: "(Code) Descrip"
                col.AllowDBNull = True
                col.Expression = "'(' + Code + ') ' + Descrip"
            End If
        End With

        Return ds
    End Function
    ''Modified by Saraswathi on July 29 2009
    ''to fix issue 16753: BUG: Cannot apply pre-req to just one program. 
    'Public Function InsertCoursePrereq(ByVal ReqId As String, ByVal PrereqId As String, ByVal CourseReqTyp As Integer, ByVal user As String)
    '    Dim DB As New CoursePrereqDB

    '    DB.InsertCoursePreCoreq(ReqId, PrereqId, CourseReqTyp, user)
    'End Function
    Public Function InsertCoursePrereq(ByVal progverid As String, ByVal ReqId As String, ByVal PrereqId As String, ByVal CourseReqTyp As Integer, ByVal user As String)
        Dim DB As New CoursePrereqDB

        DB.InsertCoursePreCoreq(progverid, ReqId, PrereqId, CourseReqTyp, user)
    End Function
    ''Modified by Saraswathi on July 29 2009
    ''to fix issue 16753: BUG: Cannot apply pre-req to just one program. 
    Public Function DeleteCoursePrereq(ByVal ReqId As String, ByVal PrereqId As String, ByVal Progverid As String)
        Dim DB As New CoursePrereqDB

        Return DB.DeleteCoursePreCoreq(ReqId, PrereqId, Progverid)
    End Function

    ''Added by Saraswathi Laksshmanan on July 29 2009
    ''to fix issue 16753: BUG: Cannot apply pre-req to just one program. 

    Public Function GetTheCoursesforProgverid(ByVal ProgverId As String, ByVal CampusId As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New CoursePrereqDB
        Dim ds As New DataSet

        '   get the dataset with all degrees
        ds = DB.GetTheCoursesforProgverid(ProgverId, CampusId)

        With ds.Tables(0)
            '.PrimaryKey = New DataColumn() {.Columns("ReqId")}

            If .Rows.Count > 0 Then
                'Add a new column called CodeAndDescrip to the datatable 
                Dim col As DataColumn = .Columns.Add("CodeAndDescrip", Type.GetType("System.String"))

                '   column contains course code and description in this format: "(Code) Descrip"
                col.AllowDBNull = True
                col.Expression = "'(' + Code + ') ' + Descrip"
            End If
        End With
        Return ds
    End Function

    ''Added by Saraswathi Lakshmanan For prereqs mantis 16753
    Public Function getPrereqsandReqsForSelectediteminAvailListBox(ByVal CampusId As String, ByVal ReqId As String, ByVal Prgverid As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New CoursePrereqDB
        Dim ds As New DataSet
        Dim tbl1 As New DataTable
        Dim tbl2 As New DataTable

        '   get the dataset with all degrees          
        ds = DB.getPrereqsandReqsForSelectediteminAvailListBox(CampusId, ReqId, Prgverid)
        With ds.Tables(0)
            If .Rows.Count > 0 Then
                'Add a new column called CodeAndDescrip to the datatable 
                Dim col As DataColumn = .Columns.Add("CodeAndDescrip", Type.GetType("System.String"))

                '   column contains course code and description in this format: "(Code) Descrip"
                col.AllowDBNull = True
                col.Expression = "'(' + Code + ') ' + Descrip"
            End If
        End With
        Return ds

    End Function
End Class
