' ===============================================================================
' ProgramsFacade.vb
' Business Logic for Programs
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports FAME.AdvantageV1.DataAccess.AR
Imports FAME.AdvantageV1.Common.AR

Namespace AR

#Region "Programs"
    Public Class ProgramsFacade
        Public Shared Function GetPrograms(ByVal campusId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Return ProgramsDB.GetAll(ShowActive, ShowInactive, campusId)
        End Function

        Public Shared Function GetProgramInfo(ByVal ProgId As String) As ProgramInfo
            Return ProgramsDB.GetInfo(ProgId)
        End Function

        Public Shared Function UpdateProgram(ByVal info As ProgramInfo, ByVal user As String) As String
            If info.IsInDB = False Then
                Return ProgramsDB.Add(info, user)
            Else
                Return ProgramsDB.Update(info, user)
            End If
        End Function

        Public Shared Function DeleteProgram(ByVal ProgId As String, ByVal modDate As DateTime) As String
            Return ProgramsDB.Delete(ProgId, modDate)
        End Function
    End Class
#End Region

#Region "Program Version"
    Public Class PrgVersionsFacade
        Public Shared Function GetPrgVersions(ByVal ProgId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean, Optional ByVal campusId As String = "") As DataSet
            Return PrgVersionsDB.GetAll(ProgId, ShowActive, ShowInactive, campusId)
        End Function

        Public Shared Function GetPrgVersionInfo(ByVal PrgVerId As String) As PrgVersionInfo
            Return PrgVersionsDB.GetInfo(PrgVerId)
        End Function



        Public Shared Function UpdatePrgVersion(ByVal info As PrgVersionInfo, ByVal user As String) As String
            If info.IsInDB = False Then
                Return PrgVersionsDB.Add(info, user)
            Else
                Return PrgVersionsDB.Update(info, user)
            End If
        End Function

        Public Shared Function DeletePrgVersion(ByVal PrgVerId As String, ByVal modDate As DateTime) As String
            Return PrgVersionsDB.Delete(PrgVerId, modDate)
        End Function

        Public Shared Function IsUsingTimeClock(ByVal PrgVerId As String) As Boolean
            Return PrgVersionsDB.IsUsingTimeClock(PrgVerId)
        End Function
    End Class
#End Region

#Region "Fees"
    Public Class PrgVersionFeesFacade
        Public Shared Function GetFees(ByVal PrgVerId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Return PrgVersionFeesDB.GetAll(PrgVerId, ShowActive, ShowInactive)
        End Function

        Public Shared Function GetPrgVersionFeeInfo(ByVal PrgVerFeeId As String) As PrgVersionFeeInfo
            Return PrgVersionFeesDB.GetInfo(PrgVerFeeId)
        End Function

        Public Shared Function UpdatePrgVersionFee(ByVal info As PrgVersionFeeInfo, ByVal user As String) As String
            If info.IsInDB = False Then
                Return PrgVersionFeesDB.Add(info, user)
            Else
                Return PrgVersionFeesDB.Update(info, user)
            End If
        End Function

        Public Shared Function DeletePrgVersionFee(ByVal PrgVerFeeId As String, ByVal modDate As DateTime) As String
            Return PrgVersionFeesDB.Delete(PrgVerFeeId, modDate)
        End Function
    End Class
#End Region

#Region "Schedules"
    Public Class SchedulesFacade
        Public Shared Function GetSchedules(ByVal PrgVerId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Return SchedulesDB.GetAll(PrgVerId, ShowActive, ShowInactive)
        End Function

        Public Shared Function GetScheduleInfo(ByVal ScheduleId As String) As ScheduleInfo
            Return SchedulesDB.GetInfo(ScheduleId)
        End Function

        Public Shared Function UpdateSchedule(ByVal info As ScheduleInfo, ByVal user As String) As String
            If info.IsInDB = False Then
                Return SchedulesDB.Add(info, user)
            Else
                Return SchedulesDB.Update(info, user)
            End If
        End Function

        Public Shared Function DeleteSchedule(ByVal ScheduleId As String, ByVal modDate As DateTime) As String
            Return SchedulesDB.Delete(ScheduleId, modDate)
        End Function

        Public Shared Function GetBlankSchedule() As DataTable
            Return SchedulesDB.GetDetailsDT(Guid.Empty.ToString())
        End Function

        Public Shared Function GetDailyScheduledHours(ByVal timein As DateTime, ByVal timeout As DateTime, _
        ByVal lunchout As DateTime, ByVal lunchin As DateTime) As Decimal
            Dim tot As Long = 0.0
            Dim tot_lunch As Long = 0.0
            If timein <> DateTime.MinValue AndAlso timeout <> DateTime.MinValue AndAlso timeout >= timein Then
                tot = DateDiff(DateInterval.Minute, timein, timeout)
            End If
            If lunchout <> DateTime.MinValue AndAlso lunchin <> DateTime.MinValue AndAlso lunchin >= lunchout Then
                tot_lunch = DateDiff(DateInterval.Minute, lunchout, lunchin)
            End If

            Return (tot - tot_lunch) / 60.0
        End Function
        Public Shared Function CheckTardyIn(ByVal PrgVerId As String, ByVal StuEnrollId As String) As DataSet
            Return SchedulesDB.CheckTardyIn(PrgVerId, StuEnrollId)
        End Function
        Public Shared Function IsPrgVerUsingTimeClockSchedule(ByVal PrgVerId As String) As Boolean
            Return SchedulesDB.IsPrgVerUsingTimeClockSchedule(PrgVerId)
        End Function

        Public Shared Function PrgVerAttendanceType(ByVal PrgVerId As String) As String
            Return SchedulesDB.PrgVerAttendanceType(PrgVerId)
        End Function
        Public Shared Function TrackTardy(ByVal PrgVerId As String) As String
            Return SchedulesDB.TrackTardy(PrgVerId)
        End Function


        Public Shared Function GetStudentSchedules(ByVal StuEnrollId As String) As DataSet
            Return SchedulesDB.GetStudentSchedules(StuEnrollId)
        End Function

        Public Shared Function GetActiveStudentSchedule(ByVal StuEnrollId As String) As DataTable
            Dim ds As DataSet = GetStudentSchedules(StuEnrollId)
            If ds Is Nothing Then Return Nothing
            If ds.Tables.Count = 0 Then Return Nothing

            Dim drs() As DataRow = ds.Tables(0).Select("Active=1")
            Dim ds2 As DataSet = ds.Clone()
            For Each dr As DataRow In drs
                ds2.Tables(0).ImportRow(dr)
            Next
            Return ds2.Tables(0)
        End Function

        Public Shared Function GetActiveStudentScheduleInfo(ByVal StuEnrollId As String) As StudentScheduleInfo
            Return SchedulesDB.GetActiveStudentScheduleInfo(StuEnrollId)
        End Function

        Public Shared Function AssignStudentScheduleFromSchedulePage(ByVal info As StudentScheduleInfo, ByVal user As String) As Integer
            Return SchedulesDB.AssignStudentScheduleFromSchedulePage(info, user)
        End Function

        Public Shared Function AssignStudentSchedule(ByVal info As StudentScheduleInfo, ByVal user As String) As String
            Return SchedulesDB.AssignStudentSchedule(info, user)
        End Function

        Public Shared Function IsBadgeNumberInUse(ByVal BadgeNumber As String, ByVal StuEnrollId As String, ByVal CampusId As String) As Boolean
            Return SchedulesDB.IsBadgeNumberInUse(BadgeNumber, StuEnrollId, CampusId)
        End Function
        
        Protected Function DeleteAllStudentSchedules(ByVal StuEnrollId As String) As String
            Return SchedulesDB.DeleteAllStudentSchedules(StuEnrollId)
        End Function

        Public Shared Function GetBadgeNumberforStudentEnrollment(ByVal StuEnrollId As String) As String
            Return SchedulesDB.GetBadgeNumberforStudentEnrollment(StuEnrollId)
        End Function
    End Class
#End Region

#Region "GradeBook"

    Public Class GradeBookFacade

#Region "Grade Book Components"
        Public Shared Function GetGradeBookComponents(ByVal CampGrpId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Return GradeBookDB.GetGradeBookComponents(CampGrpId, ShowActive, ShowInactive)
        End Function

        Public Shared Function GetGradeBookComponentInfo(ByVal GrdComponentTypeId As String) As GradeBookComponentInfo
            Return GradeBookDB.GetGradeBookComponentInfo(GrdComponentTypeId)
        End Function

        Public Shared Function UpdateGradeBookComponent(ByVal info As GradeBookComponentInfo, ByVal user As String) As String
            If info.IsInDB = False Then
                Return GradeBookDB.Add(info, user)
            Else
                Return GradeBookDB.Update(info, user)
            End If
        End Function

        Public Shared Function DeleteGradeBookComponent(ByVal GrdComponentId As String, ByVal modDate As DateTime) As String
            Return GradeBookDB.Delete(GrdComponentId, modDate)
        End Function
#End Region

#Region "Grade Book Weights"
        Public Shared Function GetGrdBkWgts(ByVal CourseId As String, ByVal InstructorId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Return GradeBookDB.GetGrdBkWgts(CourseId, InstructorId, ShowActive, ShowInactive)
        End Function
        Public Shared Function GetGrdBookWgtsInfo(ByVal GrdBookWgtId As String) As GrdBkWgtsInfo
            Return GradeBookDB.GetGrdBookWgtsInfo(GrdBookWgtId)
        End Function
        Public Shared Function UpdateGrdBkWgts(ByVal info As GrdBkWgtsInfo, ByVal user As String) As String
            If info.IsInDb = False Then
                Return GradeBookDB.AddGrdBkWgt(info, user)
            Else
                Return GradeBookDB.UpdateGrdBkWgt(info, user)
            End If
        End Function
        Public Shared Function DeleteGrdBkWgt(ByVal GrdBkWgtId As String, ByVal modDate As DateTime) As String
            Return GradeBookDB.DeleteGrdBkWgt(GrdBkWgtId, modDate)
        End Function
        Public Shared Function GetEmptyGrdBkWgtDetails(ByVal CampusId As String) As DataTable
            Return GradeBookDB.GetEmptyGrdBkWgtDetails(CampusId)
        End Function
        Public Shared Function IsGrdBkWgtInUse(ByVal GrdBkWgtId As String) As Boolean
            Return GradeBookDB.IsGrdBkWgtInUse(GrdBkWgtId)
        End Function
#End Region

    End Class
#End Region

#Region "Courses"
    Public Class CoursesFacade
        Public Shared Function GetCourses(ByVal CampGrpId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Return CoursesDB.GetAll(CampGrpId, ShowActive, ShowInactive)
        End Function

        Public Shared Function GetCoureseInfo(ByVal ReqId As String) As CourseInfo
            Return CoursesDB.GetInfo(ReqId)
        End Function
        Public Function GetCoursesByProgramVersion(ByVal campusId As String, ByVal prgVerId As String, ByVal statusId As String) As DataTable
            Return (New CoursesDB).GetCoursesByProgramVersion(campusId, prgVerId, statusId)
        End Function
        Public Function GetCoursesByCampus(ByVal campusId As String) As DataTable
            Return (New CoursesDB).GetCoursesByCampus(campusId)
        End Function
        Public Function GetTermsByCampus(ByVal campusId As String, ByVal showActiveOnly As Boolean) As DataTable
            Return (New CoursesDB).GetTermsByCampus(campusId, showActiveOnly)
        End Function
        Public Function GetEnrollmentStatusByCampus(ByVal campusId As String, ByVal showActiveOnly As Boolean) As DataTable
            Return (New CoursesDB).GetEnrollmentStatusByCampus(campusId, showActiveOnly)
        End Function
        Public Function GetStudentGroupByCampus(ByVal campusId As String, ByVal showActiveOnly As Boolean) As DataTable
            Return (New CoursesDB).GetStudentGroupByCampus(campusId, showActiveOnly)
        End Function
        Public Function GetCoursesByProgramVersionAndIgnoreSelectedCourses(ByVal campusId As String, ByVal prgVerId As String, ByVal selectedCourses As String) As DataTable
            Return (New CoursesDB).GetCoursesByProgramVersionAndIgnoreSelectedCourses(campusId, prgVerId, selectedCourses)
        End Function
        Public Function GetStatesByCampus(ByVal campusId As String) As DataTable
            Return (New CoursesDB).GetStatesByCampus(campusId)
        End Function
        Public Function GetChargingMethodsByProgramVersion(ByVal campusId As String, ByVal prgVerId As String, ByVal statusId As String) As DataTable
            Return (New CoursesDB).GetChargingMethodsByProgramVersion(campusId, prgVerId, statusId)
        End Function
        Public Function GetChargingMethodsByCampus(ByVal campusId As String) As DataTable
            Return (New CoursesDB).GetChargingMethodsByCampus(campusId)
        End Function
        Public Function GetChargingMethodsByProgramVersionAndIgnoreSelectedChargingMethods(ByVal campusId As String, ByVal prgVerId As String, ByVal selectedCourses As String) As DataTable
            Return (New CoursesDB).GetChargingMethodsByProgramVersionAndIgnoreSelectedChargingMethods(campusId, prgVerId, selectedCourses)
        End Function
        Public Function GetAvailableTermsAndIgnoreSelectedTerms(ByVal campusId As String, ByVal selectedTerms As String, _
                                                                ByVal showActiveOnly As Boolean) As DataTable
            Return (New CoursesDB).GetAvailableTermsAndIgnoreSelectedTerms(campusId, selectedTerms, showActiveOnly)
        End Function
        Public Function GetAvailableEnrollmentStatusAndIgnoreSelectedStatus(ByVal campusId As String, ByVal selectedStatus As String, _
                                                                ByVal showActiveOnly As Boolean) As DataTable
            Return (New CoursesDB).GetAvailableEnrollmentStatusAndIgnoreSelectedStatus(campusId, selectedStatus, showActiveOnly)
        End Function
        Public Function GetAvailableStudentGroupsAndIgnoreSelectedGroups(ByVal campusId As String, ByVal selectedStudentGroups As String, _
                                                              ByVal showActiveOnly As Boolean) As DataTable
            Return (New CoursesDB).GetAvailableStudentGroupsAndIgnoreSelectedGroups(campusId, selectedStudentGroups, showActiveOnly)
        End Function
        Public Function GetAvailableStudentsWithGpa(ByVal prgVerId As String, ByVal enrollmentStatusId As String, _
                                                               ByVal studentGrpId As String) As DataTable
            Return (New CoursesDB).GetAvailableStudentsWithGpa(prgVerId, enrollmentStatusId, studentGrpId)
        End Function
    End Class
#End Region

#Region "Utilities"
    Public Class UtiltiesFacade
        Public Shared Function GetPrgVerId(ByVal StuEnrollId As String) As String
            Return UtilitiesDB.GetPrgVerId(StuEnrollId)
        End Function

        Public Shared Function GetAttendanceUnitTypes() As DataSet
            Return UtilitiesDB.GetAttendanceUnitTypes()
        End Function
    End Class
#End Region

End Namespace

