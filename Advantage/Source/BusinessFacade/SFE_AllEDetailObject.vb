
Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllEDetailObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SFE_AllEDetail"

	Private dsRaw As New DataSet
	Private dtRptRaw As New DataTable
	Private dsRpt As New DataSet

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SFE_AllEDetailObjectDB.GetReportDatasetRaw(RptParamInfo)

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		' pre-process raw data, first process student enrollment info, then process with raw student 
		'	data to obtain prepared raw data for processing
		dtRptRaw = SFE_AllECommonObject.PreProcessRawData(dsRaw.Tables(TblNameStudents), IPEDSFacade.GetProcessedEnrollInfo(dsRaw))

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add(RptStuIdentifierColName, GetType(System.String))
			.Add("Name", GetType(System.String))
			.Add("GenderDescrip", GetType(System.String))
			.Add("EthCodeDescrip", GetType(System.String))
			.Add("Ind_Undergrad", GetType(System.String))
			.Add("Ind_Grad", GetType(System.String))
			.Add("Ind_FirstProf", GetType(System.String))
		End With

		' get IPEDS Gender and Ethnic Codes, for use in processing
		Dim dtGenders As DataTable = IPEDSFacade.GetIPEDSGenderInfo
		Dim dtEthCodes As DataTable = IPEDSFacade.GetIPEDSEthnicInfo

		' loop thru genders, and ethnic groups for each gender, and process
		'	matching records in raw dataset
		For Each drGenderInfo As DataRow In dtGenders.Rows
			For Each drEthnicInfo As DataRow In dtEthCodes.Rows
				ProcessRows(drGenderInfo("GenderDescrip"), drEthnicInfo("EthCodeDescrip"))
			Next
		Next

		' return report dataset
		Return dsRpt

	End Function

	Private Sub ProcessRows(ByVal GenderDescrip As String, ByVal EthCodeDescrip As String)
		Dim i As Integer
		Dim drRpt As DataRow

		' create dataview from students in raw dataset, filtered according to passed in gender and ethnic code
		Dim dvRptRaw As New DataView(dtRptRaw, "GenderDescrip LIKE '" & GenderDescrip & "' AND EthCodeDescrip LIKE '" & EthCodeDescrip & "'", "", DataViewRowState.CurrentRows)

		' if student dataview has records, loop thru records and process raw data to create report records
		If dvRptRaw.Count > 0 Then
			For i = 0 To dvRptRaw.Count - 1
				drRpt = dsRpt.Tables(MainTableName).NewRow

				' set Student Id and name
				IPEDSFacade.SetStudentInfo(dvRptRaw(i), drRpt)

				' get descriptions for gender and ethnic ids
				drRpt("GenderDescrip") = dvRptRaw(i)("GenderDescrip").ToString
				drRpt("EthCodeDescrip") = dvRptRaw(i)("EthCodeDescrip").ToString

				' set indicators as appropriate
				drRpt("Ind_Undergrad") = DBNull.Value
				drRpt("Ind_Grad") = DBNull.Value
				drRpt("Ind_FirstProf") = DBNull.Value
				Select Case dvRptRaw(i)("ProgTypeDescrip").ToString
					Case ProgTypeUndergraduate
						drRpt("Ind_Undergrad") = "X"
					Case ProgTypeGraduate
						drRpt("Ind_Grad") = "X"
					Case ProgTypeFirstProf
						drRpt("Ind_FirstProf") = "X"
				End Select

				' add new report row to report table
				dsRpt.Tables(MainTableName).Rows.Add(drRpt)
			Next

		' if student dataview has no records, create record with descriptions for gender and ethnic codes, 
		'	NULL data otherwise
		Else
			drRpt = dsRpt.Tables(MainTableName).NewRow

			drRpt(RptStuIdentifierColName) = DBNull.Value
			drRpt("Name") = DBNull.Value
			drRpt("GenderDescrip") = GenderDescrip
			drRpt("EthCodeDescrip") = EthCodeDescrip
			drRpt("Ind_Undergrad") = DBNull.Value
			drRpt("Ind_Grad") = DBNull.Value
			drRpt("Ind_FirstProf") = DBNull.Value

			' add empty report row to report table
			dsRpt.Tables(MainTableName).Rows.Add(drRpt)

		End If

	End Sub

End Class
