' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' MessagingFacade.vb
'
' MessagingFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class MessagingFacade
    Public Function GetAllMessageTemplates(ByVal campusId As String) As DataSet

        '   get the dataset with all MessageTemplates
        Return (New MessagingDB).GetAllMessageTemplates(campusId)

    End Function
    Public Function GetActiveMessageTemplatesOnly(ByVal campusId As String) As DataSet

        '   get the dataset with all ActiveMessageTemplates
        Return (New MessagingDB).GetActiveMessageTemplatesOnly(campusId)

    End Function
    Public Function GetAllFreedomTemplates(ByVal campusId As String) As DataSet

        '   get the dataset with all MessageTemplates
        Return (New MessagingDB).GetAllFreedomTemplates(campusId)

    End Function
    Public Function GetAllMessageGroups(ByVal campusId As String) As DataSet

        '   get the dataset with all MessageGroups
        Return (New MessagingDB).GetAllMessageGroups(campusId)

    End Function
    Public Function GetAllOndemandMessageTemplates(ByVal campusId As String) As DataSet

        '   get the dataset with all MessageTemplates
        Return (New MessagingDB).GetAllOnDemandMessageTemplates(campusId)

    End Function
    Public Function GetAllQueryMessageTemplates(ByVal campusId As String) As DataSet

        '   get the dataset with all MessageTemplates
        Return (New MessagingDB).GetAllQueryMessageTemplates(campusId)

    End Function
    Public Function GetMessageSchemaInfo(ByVal MessageSchemaId As String) As MessageSchemaInfo

        '   get the MessageSchemaInfo
        Return (New MessagingDB).GetMessageSchemaInfo(MessageSchemaId)

    End Function
    Public Function GetMessageTemplateInfo(ByVal MessageTemplateId As String) As MessageTemplateInfo

        '   get the MessageTemplateInfo
        Return (New MessagingDB).GetMessageTemplateInfo(MessageTemplateId)

    End Function
    Public Function GetFreedomTemplateInfo(ByVal freedomTemplateId As String) As FreedomTemplateInfo

        '   get the FreedomTemplateInfo
        Return (New MessagingDB).GetFreedomTemplateInfo(freedomTemplateId)

    End Function
    Public Function GetMessageGroupInfo(ByVal MessageGroupId As String) As MessageGroupInfo

        '   get the MessageGroupInfo
        Return (New MessagingDB).GetMessageGroupInfo(MessageGroupId)

    End Function
    Public Function UpdateMessageTemplateInfo(ByVal MessageTemplateInfo As MessageTemplateInfo, ByVal user As String) As String

        '   If it is a new account do an insert. If not, do an update
        If Not (MessageTemplateInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New MessagingDB).AddMessageTemplateInfo(MessageTemplateInfo, user)
        Else
            '   return integer with update results
            Return (New MessagingDB).UpdateMessageTemplateInfo(MessageTemplateInfo, user)
        End If

    End Function
    Public Function UpdateFreedomTemplateInfo(ByVal freedomTemplateInfo As FreedomTemplateInfo, ByVal user As String) As String

        '   If it is a new account do an insert. If not, do an update
        If Not (freedomTemplateInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New MessagingDB).AddFreedomTemplateInfo(freedomTemplateInfo, user)
        Else
            '   return integer with update results
            Return (New MessagingDB).UpdateFreedomTemplateInfo(freedomTemplateInfo, user)
        End If

    End Function
    Public Function UpdateMessageGroupInfo(ByVal MessageGroupInfo As MessageGroupInfo, ByVal user As String) As String

        '   If it is a new account do an insert. If not, do an update
        If Not (MessageGroupInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New MessagingDB).AddMessageGroupInfo(MessageGroupInfo, user)
        Else
            '   return integer with update results
            Return (New MessagingDB).UpdateMessageGroupInfo(MessageGroupInfo, user)
        End If

    End Function
    Public Function UpdateMessageSchemaInfo(ByVal messageSchemaInfo As MessageSchemaInfo, ByVal user As String) As String


        '   If it is a new account do an insert. If not, do an update
        If Not (messageSchemaInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New MessagingDB).AddMessageSchemaInfo(messageSchemaInfo, user)
        Else
            '   return integer with update results
            Return (New MessagingDB).UpdateMessageSchemaInfo(messageSchemaInfo, user)
        End If

    End Function
    Public Function DeleteMessageSchemaInfo(ByVal MessageSchemaId As String, ByVal modDate As DateTime) As String

        '   delete MessageTemplateInfo ans return string result
        Return (New MessagingDB).DeleteMessageSchemaInfo(MessageSchemaId, modDate)

    End Function
    Public Function DeleteMessageTemplateInfo(ByVal MessageTemplateId As String, ByVal modDate As DateTime) As String

        '   delete MessageTemplateInfo ans return string result
        Return (New MessagingDB).DeleteMessageTemplateInfo(MessageTemplateId, modDate)

    End Function
    Public Function DeleteFreedomTemplateInfo(ByVal FreedomTemplateId As String, ByVal modDate As DateTime) As String

        '   delete MessageTemplateInfo ans return string result
        Return (New MessagingDB).DeleteFreedomTemplateInfo(FreedomTemplateId, modDate)

    End Function
    Public Function DeleteMessageGroupInfo(ByVal MessageGroupId As String, ByVal modDate As DateTime) As String

        '   delete MessageGroupInfo ans return string result
        Return (New MessagingDB).DeleteMessageGroupInfo(MessageGroupId, modDate)

    End Function
    Public Sub AddAdvantageMessage(ByVal sender As Object, ByVal e As AdvantageMessagingEventArgs)

        'add addresschange message to the queue
        Call (New MessagingDB).AddAdvantageMessage(sender, e)

    End Sub
    Public Function GetAllMessageSchemas(ByVal campusId As String) As DataSet

        '   get the dataset with all MessageSchemas
        Return (New MessagingDB).GetAllMessageSchemas(campusId)

    End Function
    Public Function GetAllMessageGroupSchemas(ByVal campusId As String) As DataSet

        '   get the dataset with all MessageSchemas
        Return (New MessagingDB).GetAllMessageGroupSchemas(campusId)

    End Function
    Public Function GetAllMessagesToProcess(ByVal campusId As String) As DataSet

        '   get the dataset with all Messages To Process
        Return (New MessagingDB).GetAllMessagesToProcess(campusId)

    End Function
    Public Function GetAllMessagesByStudent(ByVal studentId As String) As DataSet

        '   get the dataset with all Messages by student
        Return (New MessagingDB).GetAllMessagesByStudent(studentId)

    End Function
    Public Function GetAllMessagesToBeSentImmediately(ByVal campusId As String) As String

        '   get the dataset with all MessagesToBeSentImmediately
        Dim ds As DataSet = (New MessagingDB).GetAllMessagesToBeSentImmediately(campusId)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim sb As New System.Text.StringBuilder
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim r As DataRow = ds.Tables(0).Rows(i)
                sb.Append(CType(r("MessageId"), Guid).ToString)
                sb.Append(",")
                sb.Append(CType(r("MessageTypeId"), MessageType))
                sb.Append(";")
            Next
            Return sb.ToString
        Else
            Return ""
        End If

    End Function
    Public Function GetMessagingInfo(ByVal messageId As String) As MessagingInfo

        '   get the MessagingInfo
        Return (New MessagingDB).GetMessagingInfo(messageId)

    End Function
    Public Function BuildStudentMessagingInfo(ByVal messageTemplateId As String, ByVal studentId As String) As MessagingInfo
        '   get the MessagingInfo
        Return (New MessagingDB).BuildStudentMessagingInfo(messageTemplateId, studentId)
    End Function
    Public Function UpdateMessagingInfoContent(ByVal messagingInfo As MessagingInfo, ByVal user As String) As String
        '   return string with update results
        Return (New MessagingDB).UpdateMessagingInfoContent(messagingInfo, user)
    End Function
    Public Function UpdateMessagingInfoSentDate(ByVal messageId As String, ByVal user As String) As String
        '   return string with update results
        Return (New MessagingDB).UpdateMessagingInfoSentDate(messageId, user)
    End Function
    Public Function UpdateMessagingInfoSentDate(ByVal messageIds() As String, ByVal user As String) As String
        '   return string with update results
        Return (New MessagingDB).UpdateMessagingInfoSentDate(messageIds, user)
    End Function
    Public Function UpdateMessagingInfo(ByVal messagingInfo As MessagingInfo, ByVal user As String) As String
        '   return string with update results
        Return (New MessagingDB).UpdateMessagingInfo(messagingInfo, user)
    End Function
    Public Function GetXmlDocGroupList(ByVal messageGroupId As String, ByVal xmlDocWithSqlParams As String, ByVal user As String) As XmlDocument
        'get xmlDocument that contains a list of the members of the group
        Return (New MessagingDB).GetXmlDocGroupList(messageGroupId, xmlDocWithSqlParams, user)
    End Function
    Public Function GetFormattedMessages(ByVal messageIds() As String) As FormattedMessage
        '   get the FormattedMessages
        Return (New MessagingDB).GetFormattedMessages(messageIds)
    End Function
    Public Function GetAdvMessages(ByVal messageIds() As String) As AdvMessage
        '   get the FormattedMessages
        Return (New MessagingDB).GetAdvMessages(messageIds)
    End Function
    Public Function GenerateMessagesInHTMLForMigration() As String
        'GenerateMessages
        Return (New MessagingDB).GenerateMessagesInHTMLForMigration()
    End Function
End Class
