Public Class DroppedCoursesSummaryObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim ds As New DataSet
        Dim getSummary As New CourseDropsDB
        ' Massage dataset to produce one with the columns and data expected by report.
        ds = getSummary.GetSummaryOfCourseDrops(paramInfo)
        Return ds
    End Function
#End Region
#Region "Private Methods"
    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        Return ds
    End Function
#End Region
End Class
