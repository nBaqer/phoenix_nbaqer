'
' FAME
' Copyright (c) 2005, 2006
' Written by ThinkTron Corporation (www.thinktron.com)
'
' Project:  AdvMessaging
' Description: Defines functions that are common throughout AdvMessaging.
'   This was done because the messaging project was built independent of Advantage.
'   The final step of this project will be to integrate Messaging with Advantage
'   which should require simple modification to this file only.

Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Microsoft.Win32
Imports FAME.AdvantageV1.DataAccess.MSG
Imports FAME.AdvantageV1.DataAccess.Reports
Imports FAME.Advantage.Common


Namespace MSG

    Public Class MSGCommon
#Region "Advantage Specific integration"
        ''' <summary>
        ''' Starts the Messaging client window.
        ''' </summary>
        ''' <param name="p"></param>
        ''' <param name="target"></param>
        ''' <param name="UserId"></param>
        ''' <param name="CampusId"></param>
        ''' <remarks></remarks>
        Public Shared Sub OpenMsg(ByRef p As Page, ByVal target As String, ByVal userId As String, ByVal campusId As String)
            If target Is Nothing Or target = "" Then
                target = "_self"
            End If

            p.Session("UserId") = userId
            p.Session("cmpid") = campusId
            Dim js As New StringBuilder
            js.Append("<script type=""text/javascript"">" & vbCrLf)
            js.Append("     window.open('" + p.Request.ApplicationPath + "/DefaultMSG.html','" + target + "');")
            js.Append("</script>")
            p.ClientScript.RegisterStartupScript(p.GetType(), "openscript", js.ToString())
        End Sub

        ''' <summary>
        ''' Initalizes parameters that used throughout TM
        ''' These parameters are stored in session variables which include
        ''' Session("userid") and Session("campusid")
        ''' </summary>        
        ''' <remarks></remarks>
        Public Shared Sub AdvInit(ByVal pg As Page)
            ' setup params needed throughout 
            Try
                If HttpContext.Current.Session("UserId") Is Nothing Then
                    HttpContext.Current.Session("UserId") = HttpContext.Current.Request.Params("UserId")
                End If
                If HttpContext.Current.Session("UserName") Is Nothing Then
                    HttpContext.Current.Session("UserName") = GetUserNameFromUserId(HttpContext.Current.Session("UserId"))
                End If
                If HttpContext.Current.Session("cmpid") Is Nothing Then
                    HttpContext.Current.Session("cmpid") = HttpContext.Current.Request.Params("cmpid")
                End If
                If HttpContext.Current.Session("resid") Is Nothing Then
                    HttpContext.Current.Session("resid") = HttpContext.Current.Request.Params("resid")
                End If

                ' display an error if the session has timed out
                If GetCurrentUserId() Is Nothing Then
                    HttpContext.Current.Session("Error") = "Session has expired"
                    Dim url As String = HttpContext.Current.Server.MapPath("ErrorPage.aspx")
                    HttpContext.Current.Response.Redirect(url)
                End If

                SetBrowserTitle(pg, "Advantage Messaging")
            Catch ex As Exception
            End Try
           
        End Sub

        Public Shared Function GetCurrentUserId() As String
            If Not HttpContext.Current.Request.Params("UserId") Is Nothing Then
                Return HttpContext.Current.Request.Params("UserId")
            End If
            Return HttpContext.Current.Session("UserId")
        End Function

        Public Shared Function GetCampusID() As String
            If Not HttpContext.Current.Request.Params("cmpid") Is Nothing Then
                Return HttpContext.Current.Request.Params("cmpid")
            End If
            Return HttpContext.Current.Session("cmpid")
        End Function

        Public Shared Function GetAdvAdminEmail() As String

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            Return myAdvAppSettings.AppSettings("FromEmailAddress")
        End Function

        ''' <summary>
        ''' Get a fullname from a userid
        ''' </summary>
        ''' <param name="UserId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetUserNameFromUserId(ByVal userId As String) As String
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' check for a blank userid
            If userId Is Nothing Or userId = "" Then Return ""
            ' format select sql
            Dim sql As String = "select FullName from syUsers where UserId='" + userId + "'"
            Dim name As String = ""
            Try
                name = db.RunParamSQLScalar(sql)
            Catch ex As Exception
            End Try
            Return name
        End Function
        ''New Code Added  By Vijay Ramteke On June 27, 2010 For Mantis Id 19269
        ''' <summary>
        ''' Get a fullname from a userid
        ''' </summary>
        ''' <param name="ReportId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetCampusGroupDescFromReportId(ByVal ReportId As String) As String
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' check for a blank userid
            If ReportId Is Nothing Or ReportId = "" Then Return ""
            ' format select sql
            Dim sql As String = "select Distinct top 1 (select CampGrpCode from syCampGrps where CampGrpId=UR.CampGrpId) as CampGrpDescrip from syUserResources UR, syUserResPermissions UP where UR.ResourceId = UP.UserResourceId and UR.ResourceId='" + ReportId + "'"
            Dim name As String = ""
            Try
                name = db.RunParamSQLScalar(sql)
            Catch ex As Exception
            End Try
            Return name
        End Function
        ''New Code Added  By Vijay Ramteke On June 27,2010 For Mantis Id 19269
#End Region
#Region "Misc Methods"
        Public Shared Function GetWorkflowSchedule() As String

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            Return myAdvAppSettings.AppSettings("AdvMsg_WorkflowSchedule")
        End Function

        ' Method: SetSchedulerLastRun
        ' Description: Updates the registry with the last time the scheduler was run
        '   Note: To execute this, the running process must have permission to write to
        '   the registry
        Public Shared Sub SetSchedulerLastRun(ByVal dt As DateTime)
            Dim regKey As RegistryKey
            Dim regKey2 As RegistryKey
            regKey = Registry.LocalMachine.OpenSubKey("SOFTWARE", True)
            Try
                regKey.CreateSubKey("Advantage")
                regKey.Close()
                regKey2 = Registry.LocalMachine.OpenSubKey("SOFTWARE\Advantage", True)
                regKey2.SetValue("Scheduler Last Run", dt.ToString())
                regKey2.Close()
            Catch ex As Exception
            End Try
        End Sub

        ' Method: GetSchedulerLastRun
        ' Description: Returns the last time the scheduler ran
        Public Shared Function GetSchedulerLastRun() As DateTime
            Dim regKey As RegistryKey
            regKey = Registry.LocalMachine.OpenSubKey("Software\Advantage")
            Dim dt As DateTime = Date.MinValue
            Try
                dt = regKey.GetValue("Scheduler Last Run", Date.MinValue)
            Catch ex As Exception
            End Try
            regKey.Close()
            Return dt.ToLongDateString() & " " & dt.ToLongTimeString()
        End Function
#End Region
#Region "Debugging and error"
        Public Shared Sub DisplayErrorMessage(ByVal pg As Page, ByVal msg As String)
            Debug.WriteLine(msg)
            Alert(pg, msg)
        End Sub
#End Region
#Region "FCKEditor methods"
        ' Setup the FCK editor 
        ' 1) Set the base path for the editor
        ' 2) Set the height of the editor
        ' 3) Optional/TODO: setup the toolbars
        ' 4) TODO: Research how to make the height dynmically change with changes to the size of IE
        Public Shared Sub SetupEditor(ByRef ed As FCKeditor, ByVal strToolBar As String)
            Try

                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                Dim fckPath As String = MyAdvAppSettings.AppSettings("fckeditor.fckPath")
                ed.BasePath = fckPath + "/FCKeditor/"
                ed.BaseHref = fckPath + "/MSG/"
                ed.PluginsPath = fckPath + "/MSG/Plugins"
                ed.SkinPath = fckPath + "/FCKeditor/editor/skins/office2003/"
                ed.ImageBrowserURL = fckPath + "/FCKeditor/editor/filemanager/browser/default/browser.html?Type=Image&Connector=connectors/aspx/connector.aspx"
                ed.LinkBrowserURL = fckPath + "/FCKeditor/editor/filemanager/browser/default/browser.html?Connector=connectors/aspx/connector.aspx"
                ed.ToolbarSet = strToolBar
                Dim protocol As String = "http://"
                If HttpContext.Current.Request.IsSecureConnection Then protocol = "https://"
                HttpContext.Current.Application("FCKeditor:UserFilesPath") = protocol + HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath + "/MSG/UserFiles/"
                HttpContext.Current.Application("FCKeditor:UserFilesDir") = HttpContext.Current.Server.MapPath("./UserFiles")
                ed.Visible = True
            Catch ex As Exception

            End Try
        End Sub
#End Region
#Region "Browser Javascript Methods"
        Public Shared Sub SetBrowserStatusBar(ByRef p As Page, ByVal strStatus As String)
            Dim js As New StringBuilder
            js.Append("<script type=""text/javascript"">" & vbCrLf)
            js.Append("window.status=""" & strStatus)
            js.Append(""" " & vbCrLf)
            js.Append("</script>")
            p.ClientScript.RegisterStartupScript(p.GetType(), "jsStatus", js.ToString())
        End Sub

        Public Shared Sub SetBrowserTitle(ByRef p As Page, ByVal strStatus As String)
            Dim js As New StringBuilder
            js.Append("<script type=""text/javascript"">" & vbCrLf)
            js.Append("parent.document.title=""" & strStatus)
            js.Append(""" " & vbCrLf)
            js.Append("</script>")
            p.ClientScript.RegisterStartupScript(p.GetType(), "jsStatus", js.ToString())
        End Sub

        Public Shared Function GetJavsScriptPopup(ByVal popupUrl As String, _
                                                  ByVal width As Integer, _
                                                  ByVal height As Integer, _
                                                  ByVal retControlId As String) As String
            Dim js As New StringBuilder
            js.Append("var strReturn; " & vbCrLf)
            js.Append("strReturn=window.showModalDialog('")
            js.Append(popupUrl)
            js.Append("',null,'resizable:no;status:no;dialogWidth:")
            js.Append(width.ToString())
            js.Append("px;dialogHeight:")
            js.Append(height.ToString())
            js.Append("px;dialogHide:true;help:no;scroll:yes'); " & vbCrLf)
            If Not retControlId Is Nothing Then
                If retControlId.Length <> 0 Then
                    js.Append("if (strReturn != null) " & vbCrLf)
                    js.Append("     document.getElementById('")
                    js.Append(retControlId)
                    js.Append("').value=strReturn;" & vbCrLf)
                End If
            End If
            Return js.ToString()
        End Function

        Public Shared Sub Alert(ByRef p As Page, ByVal msg As String)
            p.ClientScript.RegisterStartupScript(p.GetType(), "sendJS", "<script language='javascript'>window.alert('" & msg.Replace("'", "") & "')</script>")
        End Sub
#End Region
#Region "Build DDL Methods"
        Public Shared Sub BuildStatusDDL(ByVal ddl As DropDownList)
            ddl.Items.Clear()
            ddl.Items.Add(New ListItem("--Select--", "true"))
            ddl.Items.Add(New ListItem("Active", "True"))
            ddl.Items.Add(New ListItem("Inactive", "False"))
            ddl.SelectedIndex = 0
        End Sub

        ' fill up the template categories drop down list
        Public Shared Sub BuildCategoryDDL(ByVal campusID As String, ByRef ddl As DropDownList)
            ddl.DataSource = GroupsFacade.GetGroups(True, False)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "GroupId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildModulesDDL(ByVal UserId As String, ByRef ddl As DropDownList)
            Try
                ddl.DataSource = ModulesDB.GetModules(UserId)
                ddl.DataTextField = "ModuleName"
                ddl.DataValueField = "ModuleId"
                ddl.DataBind()
                ddl.Items.Insert(0, New ListItem("--Select--", "None"))
                ddl.Items.Insert(1, New ListItem("--All--", ""))
                ddl.SelectedIndex = 0
            Catch ex As Exception
                DisplayErrorMessage(ddl.Page, "Error building module dropdown")
            End Try
        End Sub

        Public Shared Sub BuildModuleEntitiesDDL(ByVal ModuleId As String, ByRef ddl As DropDownList)
            Try
                ddl.DataSource = ModulesDB.GetModuleEntities(ModuleId)
                ddl.DataTextField = "Descrip"
                ddl.DataValueField = "ModuleEntityId"
                ddl.DataBind()
                ddl.Items.Insert(0, New ListItem("--Select--", ""))
                ddl.SelectedIndex = 0
            Catch ex As Exception
                DisplayErrorMessage(ddl.Page, "Error building entities dropdown")
            End Try
        End Sub

        Public Shared Sub BuildEntitiesDDL(ByVal ddl As DropDownList)
            ddl.DataSource = ModulesDB.GetEntities()
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "EntityId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildDeliveryTypeDDL(ByRef ddl As DropDownList)
            ddl.Items.Clear()
            ddl.Items.Add(New ListItem("--Select--", ""))
            ddl.Items.Add("Email")
            ddl.Items.Add("Printer")
            'ddl.Items.Add("Email & Printer") ' Disabled for this version
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildRecipientTypeDDL(ByRef ddl As DropDownList)
            BuildEntitiesDDL(ddl)
        End Sub

        Public Shared Sub BuildCampusGroups(ByVal ddl As DropDownList)
            ddl.DataSource = CampGroupsDB.GetCampusGroups()
            ddl.DataTextField = "CampGrpDescrip"
            ddl.DataValueField = "CampGrpId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            If ddl.Items.Count > 1 Then
                ddl.SelectedIndex = 1
            Else
                ddl.SelectedIndex = 0
            End If
        End Sub

        Public Shared Sub BuildTemplatesDDL(ByVal CampGroupId As String, ByVal ModuleEntityId As String, ByVal ddl As DropDownList)
            ddl.DataSource = TemplatesFacade.GetTemplates(CampGroupId, ModuleEntityId, True, False)
            ddl.DataValueField = "TemplateId"
            ddl.DataTextField = "Descrip"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            If ddl.Items.Count > 1 Then
                ddl.SelectedIndex = 1
            Else
                ddl.SelectedIndex = 0
            End If
        End Sub

        Public Shared Sub BuildAvailableTemplates(ByVal UserId As String, ByVal CampusId As String, _
                                                    ByVal ModuleId As String, ByVal EntityId As String, _
                                                    ByVal ddl As DropDownList)
            ddl.DataSource = TemplatesFacade.GetAllForUser(UserId, CampusId, ModuleId, EntityId)
            ddl.DataValueField = "TemplateId"
            ddl.DataTextField = "Descrip"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildTemplatesForEntity(ByVal CampusId As String, ByVal EntityId As String, ByVal ddl As DropDownList)
            ddl.DataSource = TemplatesFacade.GetAllForEntity(CampusId, EntityId)
            ddl.DataValueField = "TemplateId"
            ddl.DataTextField = "Descrip"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub

        ' here we had to make changes to show(filter) only category fields allowed for a given template
        Public Shared Sub BuildFieldGroupsLB(ByRef lb As ListBox)
            Dim ds As New DataSet
            Try
                lb.Items.Clear()
                ' read from the xml file all the categories
                Dim xmlFileName As String = lb.Page.MapPath("xml/MsgCategories.xml")
                ds.ReadXml(xmlFileName)
                lb.DataTextField = "Category"
                lb.DataValueField = "Category"
                lb.DataSource = ds
                lb.DataBind()
            Catch ex As Exception
            End Try
            ds.Dispose()
        End Sub

        Public Shared Sub BuildRulesDDL(ByVal ddl As DropDownList)
            Dim ds As New DataSet
            Try
                ddl.Items.Clear()
                ' read from the xml file
                Dim xmlFileName As String = ddl.Page.MapPath("xml/MsgRules.xml")
                ds.ReadXml(xmlFileName)
                ddl.DataSource = ds
                ddl.DataTextField = "RuleDescrip"
                ddl.DataValueField = "RuleId"
                ddl.DataBind()
                ddl.Items.Insert(0, "--Select--")
                ddl.SelectedIndex = 0
            Catch ex As Exception
            End Try
            ds.Dispose()
        End Sub
        'New Code Added By Vijay Ramteke on May 07, 2010
        Public Shared Sub BuildAdHocDDL(ByVal UserId As String, ByVal ModuleId As String, ByVal CampusId As String, ByRef ddl As DropDownList)
            Try
                ddl.DataSource = ReportsDB.GetReportsForUserMsg(UserId, ModuleId, CampusId)
                ddl.DataTextField = "ReportName"
                ddl.DataValueField = "ReportId"
                ddl.DataBind()
                ddl.Items.Insert(0, New ListItem("--Select--", "0"))
                ddl.SelectedIndex = 0
            Catch ex As Exception
                DisplayErrorMessage(ddl.Page, "Error building AdHoc dropdown")
            End Try
        End Sub
        'New Code Added By Vijay Ramteke on May 07, 2010
#End Region
#Region "Utilities"
        ''' <summary>
        ''' This method needs to be called so MSG/SpecialFieldsPopup.aspx knows
        ''' which fields to display
        ''' </summary>
        ''' <param name="EntityId"></param>
        ''' <remarks></remarks>
        Public Shared Sub SetSpecialFieldPopupParam(ByVal EntityId As String)
            HttpContext.Current.Session("CurrentEntityId") = EntityId
        End Sub

        Public Shared Function GetRecipientLookupJS(ByVal ctlReturn As String, _
                                                    ByVal ctlReType As String, _
                                                    ByVal ctlReId As String, _
                                                    ByVal ReTypeEnable As Boolean) As String
            Dim js As New StringBuilder
            js.Append("var opt='resizable:no;status:no;dialogWidth:600px;dialogHeight:400px;dialogHide:true;help:no;scroll:yes';" + vbCrLf)
            js.Append("var retype=document.getElementById('" + ctlReType + "').value;" + vbCrLf)
            js.Append("var reid=document.getElementById('" + ctlReturn + "').value;" + vbCrLf)
            js.Append("var url='RecipientLookup.aspx';" + vbCrLf)
            js.Append("var p1='?enableretype=" + ReTypeEnable.ToString() + "';" + vbCrLf)
            js.Append("var p2='&initretype=' + retype; " + vbCrLf)
            js.Append("var p3='&initreid=' + reid; " + vbCrLf)
            js.Append("var curl=url + p1 + p2 + p3; " + vbCrLf)
            js.Append("var strValue = window.showModalDialog(curl,null,opt);" & vbCrLf)
            js.Append("if(strValue == undefined) strValue = window.returnValue;" & vbCrLf)
            js.Append("if(strValue != null) {" & vbCrLf)
            js.Append("     var iPosition = strValue.indexOf('|'); " & vbCrLf)
            js.Append("     if (iPosition > 0) {" & vbCrLf)
            js.Append("         var strId = strValue.substr(iPosition+1);" & vbCrLf)
            js.Append("         document.getElementById('")
            js.Append(ctlReId)
            js.Append("').value = strId; " & vbCrLf)
            js.Append("         strValue = strValue.substring(0,iPosition);" & vbCrLf)
            js.Append("         iPosition = strValue.indexOf(':');" & vbCrLf)
            js.Append("         document.getElementById('")
            js.Append(ctlReturn)
            js.Append("').value = strValue.substr(iPosition+1); " & vbCrLf)
            js.Append("         document.getElementById('")
            js.Append(ctlReType)
            js.Append("').value = strValue.substring(0,iPosition); " & vbCrLf)
            js.Append("     }" & vbCrLf)
            js.Append("}" & vbCrLf)
            Return js.ToString
        End Function

        ' Method: ApplySSNMaskToDS
        ' Description: Apply the SSN mask to the SSN column of the dataset passed in
        Public Shared Sub ApplySSNMaskToDS(ByRef ds As DataSet)
            ' Apply the masks to all the SSNs
            Dim fac As New InputMasksFacade
            Dim ssnMask As String = fac.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
            Dim dr As DataRow
            For Each dr In ds.Tables(0).Rows
                Try
                    If Not IsDBNull(dr("SSN")) Then
                        'dr("SSN") = fac.ApplyMask(ssnMask, dr("SSN"))
                        Dim temp As String = dr("SSN")
                        dr("SSN") = fac.ApplyMask(ssnMask, "*****" & temp.Substring(5))
                    End If
                Catch ex As Exception

                End Try
            Next
        End Sub

        ''' <summary>
        ''' Prints a document using the fck editor
        ''' 10-10-06: Ben: changed javascript in this function to address changes in FCKEditor.  Advantage used 
        ''' a new version of FCKEditor on one of the builds for 1.2.1 which broke printing.
        ''' </summary>
        ''' <param name="pg"></param>
        ''' <param name="s"></param>
        ''' <remarks></remarks>
        Public Shared Sub PrintDocument(ByRef pg As Page, ByRef s As String)
            Dim ns As New StringBuilder(s)
            ns.Replace("'", "&#146;")
            ns.Replace(vbCrLf, "")

            Dim sb As New StringBuilder
            sb.Append("<script type=""text/javascript"" src=""../FCKeditor/fckeditor.js""></script>" & vbCrLf)
            sb.Append("<script language=""javascript"">" & vbCrLf)
            sb.Append("     var fck = new FCKeditor('fckPrint');" & vbCrLf)
            sb.Append("     fck.BasePath = ""../FCKeditor/""" & vbCrLf)
            sb.Append("     fck.Width = 0; fck.Height = 0; fck.Create();" & vbCrLf)
            sb.Append("     function FCKeditor_OnComplete(ed) { " & vbCrLf)
            sb.Append("         if(ed.Name=='fckPrint') ed.SetHTML('")
            sb.Append(ns.ToString)
            sb.Append("', false);" & vbCrLf)
            sb.Append("         if(ed.Name=='fckPrint') ed.Events.AttachEvent( 'OnAfterSetHTML', FCKeditor_OnAfterSetHTML ); " + vbCrLf)
            sb.Append("     }" + vbCrLf)
            sb.Append(vbCrLf)
            sb.Append("     function FCKeditor_OnAfterSetHTML(ed) { " & vbCrLf)
            sb.Append("         if(ed.Name=='fckPrint') ed.ExecuteNamedCommand('Print','');" & vbCrLf)
            sb.Append("     }" & vbCrLf)
            sb.Append("</script>" & vbCrLf)
            pg.ClientScript.RegisterStartupScript(pg.GetType(), "printJS", sb.ToString)
        End Sub

        ' Method: Prints a document using the fck editor
        Public Shared Sub PreviewDocument(ByRef pg As Page, ByRef s As String)
            Dim ns As New StringBuilder(s)
            ns.Replace("'", "&#146;")
            ns.Replace(vbCrLf, "")

            Dim sb As New StringBuilder
            sb.Append("<script type=""text/javascript"" src=""../FCKeditor/fckeditor.js""></script>" & vbCrLf)
            sb.Append("<script language=""javascript"">" & vbCrLf)
            sb.Append("     var fck = new FCKeditor('fckPreview');" & vbCrLf)
            sb.Append("     fck.BasePath = ""../FCKeditor/""" & vbCrLf)
            sb.Append("     fck.Width = 0; fck.Height = 0; fck.Create();" & vbCrLf)
            sb.Append("     function FCKeditor_OnComplete(ed) { " & vbCrLf)
            sb.Append("     if(ed.Name=='fckPreview') ed.SetHTML('")
            sb.Append(ns.ToString)
            sb.Append("', false);" & vbCrLf)
            sb.Append("     if(ed.Name=='fckPreview') ed.Preview();" & vbCrLf)
            sb.Append("}" & vbCrLf)
            sb.Append("</script>" & vbCrLf)
            pg.ClientScript.RegisterStartupScript(pg.GetType(), "printJS", sb.ToString)
        End Sub

        Public Shared Function HTMLEncode(ByVal s As String)
            s = s.Replace(" ", "&nbsp;")
            s = s.Replace("""", "&quot;")
            Return s
        End Function
#End Region
    End Class

End Namespace