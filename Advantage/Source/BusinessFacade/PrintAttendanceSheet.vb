Public Class PrintAttendanceSheet
    Inherits BaseReportFacade

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim printAttendance As New PrintAttendanceSheetRep
        Dim ds As New DataSet

     

        ' Massage dataset to produce one with the columns and data expected by report.
        ds = BuildReportSource(printAttendance.GetPrintAttendanceSheet(rptParamInfo), printAttendance.StudentIdentifier, rptParamInfo)

        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function
#End Region

#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String, ByVal rptParamInfo As Common.ReportParamInfo) As DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String
        Dim oldCmpGrp As String = ""
        Dim oldCampus As String = ""
        Dim dt As DataTable
        Dim RecordStartDate As Date
        Dim RecordEndDate As Date
        Dim icount As Integer
        Dim noofDays As Integer = 1
        '  Dim reportStartDate As Date

        Dim arrValues() As String
        If rptParamInfo.FilterOtherString <> "" Then
            arrValues = rptParamInfo.FilterOtherString.Split(";")
            For icount = 0 To arrValues.Length - 1
                If arrValues(icount).Contains("Number of Days=") Then
                    noofDays = CInt(arrValues(icount).Substring(arrValues(icount).IndexOf("=") + 1))
                    ' noofDays = noofDays - NZ 11/19 assignment has no effect
                ElseIf arrValues(icount).Contains("Report Start Date=") Then
                    RecordStartDate = Format(CDate(arrValues(icount).Substring(arrValues(icount).IndexOf("=") + 1)), "MM/dd/yyyy")
                End If
            Next
        End If
        If noofDays = 1 Then
            RecordEndDate = RecordStartDate
        Else
            RecordEndDate = DateAdd(DateInterval.Day, noofDays - 1, RecordStartDate)
        End If

        'Get the mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'Use table to store Total Amounts per Campus Groups and Campuses.
                    dt = ds.Tables(0)
                    For Each dr As DataRow In ds.Tables(0).Rows

                        'Apply mask to SSN.
                        If StudentIdentifier = "SSN" Then
                            If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                                If dr("StudentIdentifier") <> "" Then
                                    Dim temp As String = dr("StudentIdentifier")
                                    dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                End If
                            End If
                        End If
                        ''10001-- Leave
                        ''10002-- Suspended
                        ''10003-- Holiday

                        For Each drLOA As DataRow In ds.Tables(1).Rows
                            If drLOA("StuEnrollId") = dr("StuEnrollId") Then
                                Dim RecordStartDate1 As Date = RecordStartDate
                                While RecordStartDate1 <= RecordEndDate
                                    If RecordStartDate1 >= drLOA("StartDate") And RecordStartDate1 <= drLOA("EndDate") Then
                                        If CDate(RecordStartDate1).DayOfWeek = 0 Then
                                            dr("Sun") = "10001"
                                        ElseIf CDate(RecordStartDate1).DayOfWeek = 1 Then
                                            dr("Mon") = "10001"
                                        ElseIf CDate(RecordStartDate1).DayOfWeek = 2 Then
                                            dr("Tue") = "10001"
                                        ElseIf CDate(RecordStartDate1).DayOfWeek = 3 Then
                                            dr("Wed") = "10001"
                                        ElseIf CDate(RecordStartDate1).DayOfWeek = 4 Then
                                            dr("Thur") = "10001"
                                        ElseIf CDate(RecordStartDate1).DayOfWeek = 5 Then
                                            dr("Fri") = "10001"
                                        ElseIf CDate(RecordStartDate1).DayOfWeek = 6 Then
                                            dr("Sat") = "10001"
                                        End If
                                    End If
                                    RecordStartDate1 = CDate(RecordStartDate1).AddDays(1)
                                End While
                            End If

                        Next

                        For Each drSus As DataRow In ds.Tables(2).Rows
                            If drSus("StuEnrollId") = dr("StuEnrollId") Then
                                Dim RecordStartDate1 As Date = RecordStartDate
                                While RecordStartDate1 <= RecordEndDate
                                    If RecordStartDate1 >= drSus("StartDate") And RecordStartDate1 <= drSus("EndDate") Then
                                        If CDate(RecordStartDate1).DayOfWeek = 0 Then
                                            dr("Sun") = "10002"
                                        ElseIf CDate(RecordStartDate1).DayOfWeek = 1 Then
                                            dr("Mon") = "10002"
                                        ElseIf CDate(RecordStartDate1).DayOfWeek = 2 Then
                                            dr("Tue") = "10002"
                                        ElseIf CDate(RecordStartDate1).DayOfWeek = 3 Then
                                            dr("Wed") = "10002"
                                        ElseIf CDate(RecordStartDate1).DayOfWeek = 4 Then
                                            dr("Thur") = "10002"
                                        ElseIf CDate(RecordStartDate1).DayOfWeek = 5 Then
                                            dr("Fri") = "10002"
                                        ElseIf CDate(RecordStartDate1).DayOfWeek = 6 Then
                                            dr("Sat") = "10002"
                                        End If
                                    End If
                                    RecordStartDate1 = CDate(RecordStartDate1).AddDays(1)
                                End While
                                'End If
                            End If

                        Next
                        For Each drHol As DataRow In ds.Tables(3).Rows
                            Dim RecordStartDate1 As Date = RecordStartDate

                            While RecordStartDate1 <= RecordEndDate
                                If RecordStartDate1 >= drHol("StartDate") And RecordStartDate1 <= drHol("EndDate") Then

                                    If CDate(RecordStartDate1).DayOfWeek = 0 Then
                                        dr("Sun") = "10003"
                                    ElseIf CDate(RecordStartDate1).DayOfWeek = 1 Then
                                        dr("Mon") = "10003"
                                    ElseIf CDate(RecordStartDate1).DayOfWeek = 2 Then
                                        dr("Tue") = "10003"
                                    ElseIf CDate(RecordStartDate1).DayOfWeek = 3 Then
                                        dr("Wed") = "10003"
                                    ElseIf CDate(RecordStartDate1).DayOfWeek = 4 Then
                                        dr("Thur") = "10003"
                                    ElseIf CDate(RecordStartDate1).DayOfWeek = 5 Then
                                        dr("Fri") = "10003"
                                    ElseIf CDate(RecordStartDate1).DayOfWeek = 6 Then
                                        dr("Sat") = "10003"
                                    End If
                                End If
                                RecordStartDate1 = CDate(RecordStartDate1).AddDays(1)
                            End While
                        Next
                    Next

                End If
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try
        ds.AcceptChanges()
        Return ds
    End Function

#End Region
End Class
