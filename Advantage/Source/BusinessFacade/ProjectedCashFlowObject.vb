Public Class ProjectedCashFlowObject
    Inherits BaseReportFacade

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim aidRec As New AidReceivedDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(aidRec.GetProjectedCashFlow(rptParamInfo), aidRec.StudentIdentifier)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim strName As String
        Dim facInputMasks As New InputMasksFacade
        Dim oldCmpGrp, oldCampus, oldStuEnrollId, oldPrgVer, filter, stuId As String
        Dim dr As DataRow

        filter = ""
        oldCmpGrp = ""
        oldCampus = ""
        oldPrgVer = ""
        oldStuEnrollId = ""
        stuId = ""

        'Get mask for SSN
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Dim dsRpt As New DataSet
        Dim dt As New DataTable("ProjectedCashFlow")
        dt.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StudentIdentifier", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("PrgVerDescrip", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StatusCodeDescrip", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("Days30", System.Type.GetType("System.Decimal")))
        dt.Columns.Add(New DataColumn("Days3160", System.Type.GetType("System.Decimal")))
        dt.Columns.Add(New DataColumn("Days6190", System.Type.GetType("System.Decimal")))
        dt.Columns.Add(New DataColumn("Days91120", System.Type.GetType("System.Decimal")))
        dt.Columns.Add(New DataColumn("Over120Days", System.Type.GetType("System.Decimal")))
        dsRpt.Tables.Add(dt)

        Try
            If ds.Tables.Count = 5 Then
                Dim dtProj As DataTable = ds.Tables("ProjectionsDT")
                Dim dtCmpGrp As DataTable = ds.Tables("CampGrpTotals")
                Dim dtCampus As DataTable = ds.Tables("CampusTotals")
                Dim dtPrgVer As DataTable = ds.Tables("PrgVerTotals")
                Dim dtGrandTtl As DataTable = ds.Tables("GrandTotals")

                Dim rw As DataRow

                For Each dr In dtProj.Rows
                    'Set up student name as: "LastName, FirstName MI."
                    strName = ""
                    If Not dr.IsNull("LastName") Then
                        If dr("LastName") <> "" Then
                            strName = dr("LastName")
                        End If
                    End If
                    If Not dr.IsNull("FirstName") Then
                        If dr("FirstName") <> "" Then
                            If strName <> "" Then
                                strName &= ", " & dr("FirstName")
                            Else
                                strName &= dr("FirstName")
                            End If
                        End If
                    End If
                    If Not dr.IsNull("MiddleName") Then
                        If dr("MiddleName") <> "" Then
                            If strName <> "" Then
                                strName &= " " & dr("MiddleName") & "."
                            Else
                                strName &= dr("MiddleName") & "."
                            End If
                        End If
                    End If
                    'Apply mask to SSN.
                    If Not dr.IsNull("StudentIdentifier") Then
                        If StudentIdentifier = "SSN" Then
                            If dr("StudentIdentifier") <> "" Then
                                Dim temp As String = dr("StudentIdentifier")
                                stuId = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                'stuId = facInputMasks.ApplyMask(strSSNMask, dr("StudentIdentifier"))
                            Else
                                stuId = dr("StudentIdentifier")
                            End If
                        Else
                            stuId = dr("StudentIdentifier")
                        End If
                    End If

                    If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpDescrip") Then
                        oldCmpGrp = dr("CampGrpDescrip")
                        oldCampus = ""
                        oldStuEnrollId = ""
                    End If
                    If oldCampus = "" Or oldCampus <> dr("CampDescrip") Then
                        oldCampus = dr("CampDescrip")
                    End If

                    If oldStuEnrollId = "" Or oldStuEnrollId <> dr("StuEnrollId").ToString Then
                        oldStuEnrollId = dr("StuEnrollId").ToString
                        filter = "CampGrpId='" & dr("CampGrpId").ToString & "'" & _
                                    " AND CampusId='" & dr("CampusId").ToString & "'" & _
                                    " AND StuEnrollId='" & oldStuEnrollId & "'"
                        rw = dt.NewRow
                        rw("StudentName") = strName
                        rw("StudentIdentifier") = stuId
                        rw("CampGrpDescrip") = oldCmpGrp
                        rw("CampDescrip") = oldCampus
                        rw("PrgVerDescrip") = dr("PrgVerDescrip")
                        rw("StatusCodeDescrip") = dr("StatusCodeDescrip")
                        rw("Days30") = dtProj.Compute("SUM(Amount)", filter & " AND TimeFrame=30")
                        rw("Days3160") = dtProj.Compute("SUM(Amount)", filter & " AND TimeFrame=60")
                        rw("Days6190") = dtProj.Compute("SUM(Amount)", filter & " AND TimeFrame=90")
                        rw("Days91120") = dtProj.Compute("SUM(Amount)", filter & " AND TimeFrame=120")
                        rw("Over120Days") = dtProj.Compute("SUM(Amount)", filter & " AND TimeFrame=121")
                        dt.Rows.Add(rw)
                    End If
                Next

                oldCmpGrp = ""
                oldCampus = ""
                oldPrgVer = ""

                For Each dr In dt.Rows
                    '   Update CampGroupTotals table
                    If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpDescrip") Then
                        oldCmpGrp = dr("CampGrpDescrip").ToString().Replace("'", "''")
                        oldCampus = ""
                        oldPrgVer = ""
                        filter = "CampGrpDescrip='" & oldCmpGrp & "'"
                        rw = dtCmpGrp.NewRow
                        rw("CampGrpDescrip") = oldCmpGrp
                        rw("Days30") = dt.Compute("SUM(Days30)", filter)
                        rw("Days3160") = dt.Compute("SUM(Days3160)", filter)
                        rw("Days6190") = dt.Compute("SUM(Days6190)", filter)
                        rw("Days91120") = dt.Compute("SUM(Days91120)", filter)
                        rw("Over120Days") = dt.Compute("SUM(Over120Days)", filter)
                        dtCmpGrp.Rows.Add(rw)
                    End If
                    '   Update CampusTotals table
                    If oldCampus = "" Or oldCampus <> dr("CampDescrip") Then
                        oldCmpGrp = dr("CampGrpDescrip").ToString().Replace("'", "''")
                        oldPrgVer = ""
                        filter = "CampGrpDescrip='" & oldCmpGrp & "' AND CampDescrip='" & oldCampus & "'"
                        rw = dtCampus.NewRow
                        rw("CampGrpDescrip") = oldCmpGrp
                        rw("CampDescrip") = oldCampus
                        rw("Days30") = dt.Compute("SUM(Days30)", filter)
                        rw("Days3160") = dt.Compute("SUM(Days3160)", filter)
                        rw("Days6190") = dt.Compute("SUM(Days6190)", filter)
                        rw("Days91120") = dt.Compute("SUM(Days91120)", filter)
                        rw("Over120Days") = dt.Compute("SUM(Over120Days)", filter)
                        dtCampus.Rows.Add(rw)
                    End If
                    '   Update PrgVerTotals table
                    If oldPrgVer = "" Or oldPrgVer <> dr("PrgVerDescrip") Then
                        oldPrgVer = dr("PrgVerDescrip").ToString().Replace("'", "''")
                        filter = "CampGrpDescrip='" & oldCmpGrp & "' AND CampDescrip='" & oldCampus & "'" & _
                                    " AND PrgVerDescrip='" & oldPrgVer & "'"
                        rw = dtPrgVer.NewRow
                        rw("CampGrpDescrip") = oldCmpGrp
                        rw("CampDescrip") = oldCampus
                        rw("PrgVerDescrip") = oldPrgVer
                        rw("Days30") = dt.Compute("SUM(Days30)", filter)
                        rw("Days3160") = dt.Compute("SUM(Days3160)", filter)
                        rw("Days6190") = dt.Compute("SUM(Days6190)", filter)
                        rw("Days91120") = dt.Compute("SUM(Days91120)", filter)
                        rw("Over120Days") = dt.Compute("SUM(Over120Days)", filter)
                        dtPrgVer.Rows.Add(rw)
                    End If
                Next

                'Update GrandTotals table
                rw = dtGrandTtl.NewRow
                rw("Days30") = dt.Compute("SUM(Days30)", filter)
                rw("Days3160") = dt.Compute("SUM(Days3160)", filter)
                rw("Days6190") = dt.Compute("SUM(Days6190)", filter)
                rw("Days91120") = dt.Compute("SUM(Days91120)", filter)
                rw("Over120Days") = dt.Compute("SUM(Over120Days)", filter)
                rw("StudentCount") = dt.Rows.Count
                dtGrandTtl.Rows.Add(rw)

                'Add total tables to report dataset
                dsRpt.Tables.Add(dtCmpGrp.Copy)
                dsRpt.Tables.Add(dtCampus.Copy)
                dsRpt.Tables.Add(dtPrgVer.Copy)
                dsRpt.Tables.Add(dtGrandTtl.Copy)

            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return dsRpt    'ds
    End Function

#End Region
End Class
