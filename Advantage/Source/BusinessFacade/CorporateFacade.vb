Public Class CorporateFacade
    Public Function GetCorporateInfo() As CorporateInfo
        Dim obj As New CorporateDB
        Dim corpInfo As New CorporateInfo
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        corpInfo = obj.GetCorporateInfo

        With corpInfo
            If .Phone <> "" Then
                .Phone = facInputMasks.ApplyMask(strMask, RemoveSomeChars(.Phone))
            End If
            If .Fax <> "" Then
                .Fax = facInputMasks.ApplyMask(strMask, RemoveSomeChars(.Fax))
            End If
        End With

        Return corpInfo
    End Function

    ''Added by saraswathi lakshmanan on June 17 2009
    ''to fix mantis case 13365
    ''To print invoce address in the student invoice receipts
    Public Function GetInvoiceAddressInfo(ByVal Campusid As String) As CorporateInfo
        Dim obj As New CorporateDB
        Dim corpInfo As New CorporateInfo
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        corpInfo = obj.GetInvoiceAddressInfo(Campusid)
        Try
            With corpInfo
                If .Phone <> "" Then
                    .Phone = facInputMasks.ApplyMask(strMask, RemoveSomeChars(.Phone))
                End If
                If .Fax <> "" Then
                    .Fax = facInputMasks.ApplyMask(strMask, RemoveSomeChars(.Fax))
                End If
            End With
        Catch ex As Exception
            'do nothing
        End Try
        Return corpInfo
    End Function

    Private Function RemoveSomeChars(ByVal strText As String) As String
        Return strText.Replace(".", "").Replace("-", "").Replace(" ", "").Replace("_", "").Replace("'", "")
    End Function

    ''Added by saraswathi lakshmanan on June 18 2009 
    ''To check if invoice address information is available for the selected campus.
    Public Function ValidateIfInvoiceAddressAvialable(ByVal CampusId As String) As Boolean

        Dim obj As New CorporateDB
        Return obj.ValidateIfInvoiceAddressAvialable(CampusId)
    End Function

End Class
