' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' IndustryFacade.vb
'
' IndustryFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class IndustryFacade
    Public Function GetAllIndustry() As DataSet

        '   Instantiate DAL component
        With New IndustryDB

            '   get the dataset with all States
            Return .GetAllIndustry()

        End With

    End Function

    Public Function GetAllIndustryForCampus(ByVal campusId As String) As DataSet

        '   Instantiate DAL component
        With New IndustryDB

            '   get the dataset with all States
            Return .GetAllIndustryForCampus(campusId)

        End With

    End Function
End Class
