Imports System.Web.UI.WebControls

Public Class StudentMasterFacade
    Public Function UpdateStudentMaster(ByVal StudentInfo As StudentMasterInfo, ByVal user As String, ByVal strObjective As String, Optional ByVal useRegent As Boolean = False, Optional ByVal bUpdateSSN As Boolean = True) As String
        With New StudentMasterDB
            If (StudentInfo.IsInDB = False) Then
               ' From 3.8 Version No more new students can be added
               ' Return .AddStudent(StudentInfo, user, strObjective, useRegent)
            Else
                Return .UpdateStudent(StudentInfo, user, strObjective, useRegent, bUpdateSSN)
            End If
        End With
    End Function
    Public Function DeleteStudentMaster(ByVal studentid As String, ByVal modDate As DateTime) As String
        With New StudentMasterDB
            Return .DeleteStudent(studentid, modDate)
        End With
    End Function
    Public Function GetStudentInfo(ByVal StudentId As String, Optional ByVal useRegent As Boolean = False, Optional _
                                      ByVal enrollmentId As String = nothing) As StudentMasterInfo
        Dim GetStudent As New StudentMasterDB
        With GetStudent
            Return .GetStudentInfo(StudentId, useRegent, enrollmentId)
        End With
    End Function
    Public Function checkChildExists(ByVal EducationInstId As String) As Integer
        Dim getCheckChild As New StudentMasterDB
        With getCheckChild
            Return .checkChildExists(EducationInstId)
        End With
    End Function

    Public Function CheckDuplicateSSN(ByVal FirstName As String, ByVal LastName As String, ByVal SSN As String) As String
        Dim GetStudent As New StudentMasterDB
        With GetStudent
            Return .CheckDuplicateSSN(FirstName, LastName, SSN)
        End With
    End Function

    Public Function AddStudentIdFormat(ByVal strFormatType As String, ByVal strYearNumber As Integer, ByVal strMonthNumber As Integer, ByVal strDateNumber As Integer, ByVal strLNameNumber As Integer, ByVal strFNameNumber As Integer, ByVal strSeqNumber As Integer, ByVal strStartingSeqNumber As Integer, ByVal user As String) As Integer
        Dim getStudent As New StudentMasterDB
        With getStudent
            Return .AddStudentIdFormat(strFormatType, strYearNumber, strMonthNumber, strDateNumber, strLNameNumber, strFNameNumber, strSeqNumber, strStartingSeqNumber, user)
        End With
    End Function
    Public Function GetStudentFormat() As StudentMasterInfo
        Dim getStudent As New StudentMasterDB
        Return getStudent.GetStudentFormat()
    End Function
    Public Function GetDependencyType() As DataSet
        Dim getDDLData As New StudentMasterDB
        Return getDDLData.GetAllDependencyType()
    End Function
    Public Function GetGeographicType() As DataSet
        Dim getDDLData As New StudentMasterDB
        Return getDDLData.GetAllGeographicType()
    End Function
    Public Function GetAdminCriteria() As DataSet
        Dim getDDLData As New StudentMasterDB
        Return getDDLData.GetAllAdminCriteria()
    End Function
    Public Function GetHousing() As DataSet
        Dim getDDLData As New StudentMasterDB
        Return getDDLData.GetAllHousingType()
    End Function

    Public Shared Function GetStudentImage(studentId As String) As Byte()
        Dim db As New StudentMasterDB
        Return db.GetStudentImage(studentId)
    End Function

    Public Shared Function GetShadowLead(studentId As String) As String
        Dim db As New StudentMasterDB
        Return db.GetShadowLead(studentId)
    End Function
End Class
