Public Class DocStatusesFacade
    Public Function GetAllDocStatuses() As DataSet


        '   get the dataset with all DocStatuses
        Return (New DocStatusesDB).GetAllDocStatuses()

    End Function
    Public Function GetDocStatusInfo(ByVal DocStatusId As String) As DocStatusInfo

        '   get the DocStatusInfo
        Return (New DocStatusesDB).GetDocStatusInfo(DocStatusId)

    End Function
    Public Function UpdateDocStatusInfo(ByVal DocStatusInfo As DocStatusInfo, ByVal user As String) As String


        '   If it is a new account do an insert. If not, do an update
        If Not (DocStatusInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New DocStatusesDB).AddDocStatusInfo(DocStatusInfo, user)
        Else
            '   return integer with update results
            Return (New DocStatusesDB).UpdateDocStatusInfo(DocStatusInfo, user)
        End If

    End Function
    Public Function DeleteDocStatusInfo(ByVal DocStatusId As String, ByVal modDate As DateTime) As String

        '   delete DocStatusInfo ans return string result
        Return (New DocStatusesDB).DeleteDocStatusInfo(DocStatusId, modDate)

    End Function

End Class
