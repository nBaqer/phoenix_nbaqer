Public Class StudentDocsFacade
    'Public Function GetAllDocuments() As DataSet
    '    'Instantiate DAL component
    '    With New StudentDB
    '        'get the dataset with all SkillGroups
    '        Return .GetAllDocuments()
    '    End With
    'End Function
    Public Function GetAllReqTypes() As DataSet
        'Instantiate DAL component
        With New StudentDB
            'get the dataset with all SkillGroups
            Return .GetAllReqTypes()
        End With
    End Function
    Public Function GetStudentDataToPostToSchoolDocs(ByVal StudentId As String) As SchoolDocsInfo
        With New StudentDB
            Return .GetStudentDataToPostToSchoolDocs(StudentId)
        End With
    End Function
    Public Function DeleteFile(ByVal StudentDocId As String) As Object
        With New StudentDB
            Return .DeleteFile(StudentDocId)
        End With
    End Function
    Public Function GetAllDocuments1(ByVal ModuleID As Integer) As DataSet
        'Instantiate DAL component
        With New StudentDB
            'get the dataset with all SkillGroups
            Return .GetAllDocuments1(ModuleID)
        End With
    End Function
    Public Function GetAllDocumentStatus(ByVal CampusId As String, Optional ByVal StudentDocId As String = "") As DataSet
        'Instantiate DAL component
        With New StudentDB
            'get the dataset with all SkillGroups
            Return .GetAllDocumentStatus(CampusId, StudentDocId)
        End With
    End Function
    Public Function GetAllDocumentModules() As DataSet
        'Instantiate DAL component
        With New StudentDB
            'get the dataset with all SkillGroups
            Return .GetAllDocumentModules()
        End With
    End Function
    Public Function GetAllStudents() As DataSet
        'Instantiate DAL component
        With New StudentDB
            'get the dataset with all SkillGroups
            Return .GetAllStudents()
        End With
    End Function
    'Public Function UpdateStudentDocs(ByVal StudentDocsInfo As StudentDocsInfo, ByVal user As String, ByVal StudentDocsId As String, ByVal ModuleID As Integer, ByVal imagedata() As Byte, ByVal imagetype As String) As String
    '    With New StudentDB
    '        If (StudentDocsInfo.IsInDb = False) Then
    '            Return .AddStudentDocs(StudentDocsInfo, user, ModuleID, imagedata, imagetype)
    '        Else
    '            Return .UpdateStudentDocs(StudentDocsInfo, user, StudentDocsId, ModuleID, imagedata, imagetype)
    '        End If
    '    End With
    'End Function
    Public Function UpdateStudentDocs(ByVal StudentDocsInfo As StudentDocsInfo, ByVal user As String, ByVal StudentDocsId As String, ByVal ModuleID As Integer) As String
        With New StudentDB
            If (StudentDocsInfo.IsInDb = False) Then
                Return .AddStudentDocs(StudentDocsInfo, user, ModuleID)
            Else
                Return .UpdateStudentDocs(StudentDocsInfo, user, StudentDocsId, ModuleID)
            End If
        End With
    End Function
    'Public Function UpdateStudentDocsNoImage(ByVal StudentDocsInfo As StudentDocsInfo, ByVal user As String, ByVal StudentDocsId As String, ByVal ModuleID As Integer) As String
    '    With New StudentDB
    '        If (StudentDocsInfo.IsInDb = False) Then
    '            Return .AddStudentDocsNoImage(StudentDocsInfo, user, ModuleID)
    '        Else
    '            Return .UpdateStudentDocsNoImage(StudentDocsInfo, user, StudentDocsId, ModuleID)
    '        End If
    '    End With
    'End Function
    Public Function UpdateDocsByStudent(ByVal StudentDocsInfo As StudentDocsInfo, ByVal user As String, ByVal StudentDocsId As String) As Integer
        With New StudentDB
            If (StudentDocsInfo.IsInDb = False) Then
                Return .AddDocsByStudent(StudentDocsInfo, user)
            Else
                Return .UpdateDocsByStudent(StudentDocsInfo, user, StudentDocsId)
            End If
        End With
    End Function
    'Public Function GetDocsStudentDetails(ByVal StudentDocId As String) As StudentDocsInfo

    '    With New StudentDB
    '        Return .GetStudentDocumentDetails(StudentDocId)
    '    End With
    'End Function

    Public Function GetStudentDocs(ByVal StudentDocId As String) As StudentDocsInfo

        With New StudentDB
            Return .GetStudentDetails(StudentDocId)
        End With
    End Function
    'Public Function GetDocsByStudentId(ByVal StudentId As String) As DataSet
    '    With New StudentDB
    '        Return .GetDocsByStudentId(StudentId)
    '    End With
    'End Function

    'Public Function GetDocsByStudentId1(ByVal StudentId As String, ByVal ModuleID As Integer) As DataSet
    '    With New StudentDB
    '        Return .GetDocsByStudentId1(StudentId, ModuleID)
    '    End With
    'End Function
    Public Function GetAllDocsStudentByStatus(ByVal DocumentStatusId As String, ByVal ModuleID As Integer, ByVal StudentId As String, ByVal campusid As String) As DataSet
        Dim ds As New DataSet
        With New StudentDB
            ds = .GetAllDocsStudentByStatus(DocumentStatusId, ModuleID, StudentId, campusid)
        End With
        ds = ConcatenateDocStudentLastFirstName(ds)
        Return ds
    End Function
    Public Function ConcatenateStudentDocsLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("StudentDocDT")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("StudentDocId")}
        End With
        If ds.Tables("StudentDocDT").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                ' DE7626 
                'row("FullName") = row("FirstName") & " " & row("LastName") & " " & row("MiddleName") & " - " & row("DocumentDescription")
                row("FullName") = row("FirstName") & " " & row("MiddleName") & " " & row("LastName") & " - " & row("DocumentDescription")
            Next
        End If
        Return ds
    End Function
    'Public Function GetDocsStudentByStudentId(ByVal ModuleID As Integer, ByVal StudentId As String) As DataSet
    '    Dim ds As New DataSet
    '    With New StudentDB
    '        ds = .GetDocsStudentByStudentId(ModuleID, StudentId)
    '    End With
    '    'ds = ConcatenateDocStudentLastFirstName(ds)
    '    Return ds
    'End Function
    Public Function GetDocsStudent(ByVal ModuleID As Integer, ByVal StudentId As String, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        With New StudentDB
            Return .GetDocsStudent(ModuleID, StudentId, CampusId)
        End With
        ds = ConcatenateDocStudentLastFirstName(ds)
    End Function
    Public Function GetDocsStudentByCampus(ByVal ModuleID As Integer, ByVal StudentId As String, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        With New StudentDB
            Return .GetDocsStudent(ModuleID, StudentId, CampusId)
        End With
        ds = ConcatenateDocStudentLastFirstName(ds)
    End Function
    Public Function ConcatenateDocStudentLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("StudentDT56")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("StudentDocId")}
        End With
        If ds.Tables("StudentDT56").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                ' DE7626 
                'row("FullName") = row("FirstName") & " " & row("LastName") & " " & row("MiddleName") & " - " & row("Descrip")
                row("FullName") = row("FirstName") & " " & row("MiddleName") & " " & row("LastName") & " - " & row("Descrip")
            Next
        End If
        Return ds
    End Function
    Public Function ConcatenateDocStudentLastFirstName1(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("StudentDT52")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("StudentDocId")}
        End With
        If ds.Tables("StudentDT52").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                ' DE7626 
                ' row("FullName") = row("FirstName") & " " & row("LastName") & " " & row("MiddleName") & " - " & row("DocumentDescription")
                row("FullName") = row("FirstName") & " " & row("MiddleName") & " " & row("LastName") & " - " & row("DocumentDescription")
            Next
        End If
        Return ds
    End Function
    'Public Function GetDocsByStatusId(ByVal DocumentStatusId As String, ByVal StudentID As String, ByVal ModuleID As Integer) As DataSet
    '    With New StudentDB
    '        Return .GetDocsByStatus(DocumentStatusId, StudentID, ModuleID)
    '    End With
    'End Function
    Public Function DeleteStudentDocs(ByVal StudentId As String, ByVal studId As String, ByVal DocumentId As String) As Integer
        With New StudentDB
            'get the dataset with all Possible Extracurriculars Based on Groups Available
            Return .DeleteStudentDocs(StudentId, studId, DocumentId)
        End With
    End Function
    Public Function GetStudentsByDocument(ByVal DocumentId As String, ByVal DocStatusId As String) As DataSet
        With New StudentDB
            'get the dataset with all Possible Extracurriculars Based on Groups Available
            Return .GetStudentsByDocument(DocumentId, DocStatusId)
        End With
    End Function
    'Public Function GetDocumentsByStudent(ByVal StudentId As String, ByVal DocStatusId As String) As DataSet
    '    With New StudentDB
    '        'get the dataset with all Possible Extracurriculars Based on Groups Available
    '        Return .GetDocumentsByStudent(StudentId, DocStatusId)
    '    End With
    'End Function
    Public Function GetStudentsByDocumentOnly(ByVal DocumentId As String) As DataSet
        With New StudentDB
            'get the dataset with all Possible Extracurriculars Based on Groups Available
            Return .GetStudentsByDocumentOnly(DocumentId)
        End With
    End Function
    Public Function GetAllExistingDocumentStatus(ByVal StudentDocumentId As String, ByVal campusId As String) As DataSet
        With New StudentDB
            'get the dataset with all SkillGroups
            Return .GetAllExistingDocumentStatus(StudentDocumentId, campusId)
        End With
    End Function
    Public Function GetAllLeadExistingDocumentStatus(ByVal LeadDocumentId As String, ByVal campusId As String) As DataSet
        With New StudentDB
            'get the dataset with all SkillGroups
            Return .GetAllLeadExistingDocumentStatus(LeadDocumentId, campusId)
        End With
    End Function
End Class
