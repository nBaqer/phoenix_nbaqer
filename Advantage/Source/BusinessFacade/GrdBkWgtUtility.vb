Public Class GrdBkWgtUtility
    Private m_score As Decimal
    Private m_weight As Decimal
    Private m_WGT As Decimal
    Private m_resultsDT As DataTable

    Public Sub New(ByVal arrRows() As DataRow, ByVal weight As Decimal, ByVal grdPolicyId As Integer, ByVal param As Integer)
        MyBase.New()

        m_WGT = weight
        Dim dt As DataTable = BuildScoreTable(arrRows)

        Dim strategyObj As GradingPolicyStrategyPattern
        strategyObj = GradingPolicyFactory.CreateGradingPolicyObj(grdPolicyId)
        m_resultsDT = strategyObj.GetResultsSet(dt, param)

        m_score = ComputeScore()
        m_weight = ComputeWeight()
    End Sub

    Public Sub New(ByVal arrRows() As DataRow, ByVal weight As Decimal)
        MyBase.New()

        m_WGT = weight

        'No Grading Policy, so there is no call to the Strategy Pattern
        m_resultsDT = BuildScoreTable(arrRows)

        m_score = ComputeScore()
        m_weight = ComputeWeight()
    End Sub

    Public ReadOnly Property Score() As Decimal
        Get
            Return System.Math.Round(m_score, 2)
        End Get
    End Property
    Public ReadOnly Property Weight() As Decimal
        Get
            Return m_weight 'System.Math.Round(m_weight, 0)
        End Get
    End Property

    Private Function BuildScoreTable(ByVal arrRows() As DataRow) As DataTable
        Dim dt As New DataTable
        Dim row As DataRow

        dt.Columns.Add("Score", GetType(System.Decimal))
        dt.Columns.Add("PostDate", GetType(System.DateTime))

        For Each dr As DataRow In arrRows
            row = dt.NewRow
            row("Score") = dr("Score")
            row("PostDate") = dr("PostDate")
            dt.Rows.Add(row)
        Next

        Return dt
    End Function
    Private Function ComputeScore() As Decimal
        Dim sum As Decimal = 0
        Dim score As Decimal = 0
        Dim int_resultsDT As Integer = 0

        If m_resultsDT.Rows.Count > 0 Then
            int_resultsDT = m_resultsDT.Rows.Count
        End If

        For Each dr As DataRow In m_resultsDT.Rows
            If dr("Score") Is System.DBNull.Value Then
                'there may be duplicate scores and while calculating average leave out the 
                'row with null score
                sum += 0
                int_resultsDT = int_resultsDT - 1
            Else
                sum += dr("Score")
            End If
        Next
        If int_resultsDT > 0 Then
            score = sum / int_resultsDT
        End If
        Return score
    End Function
    Private Function ComputeWeight() As Decimal
        Return (m_score * m_WGT) / 100
    End Function
End Class
