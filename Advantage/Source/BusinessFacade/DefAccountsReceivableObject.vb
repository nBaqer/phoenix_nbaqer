Public Class DefAccountsReceivableObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim objDB As New StuAccountBalanceDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(objDB.GetDeferredIncome(rptParamInfo), objDB.StudentIdentifier)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        ds.DataSetName = "dsDefReceiveIncome"
        ds.Tables(0).TableName = "DeferredIncomeDetail"
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim strName As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String
        Dim result As Boolean = False

        'Get mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count = 1 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dtDefRevenue As DataTable = ds.Tables("DeferredRevenueDetail")
                 
                    '
                    For Each dr As DataRow In dtDefRevenue.Rows
                        'Compute Total Amounts per Campus Groups and Campuses.
                        'CampGrpDescrip and CampDescrip columns cannot be NULL.

                        result = False
                        Try

                       
                            'check to exclude the student
                            If dr("sysStatusId") = 12 Or dr("sysStatusId") = 14 Or dr("sysStatusId") = 8 Then
                                result = ExcludeStudent(dr("sysStatusId"), dr("SDate"), dr("LDA").ToString, dr("ExpGradDate").ToString, dr("DateDetermined").ToString, dr("BalanceOwed"), dr("LastTransactionDate"))
                            End If
                            If result Then
                                dr.Delete()

                            Else



                                'Set up student name as: "LastName, FirstName MI."
                                strName = ""
                                If Not (dr("LastName") Is System.DBNull.Value) Then
                                    If dr("LastName") <> "" Then
                                        strName = dr("LastName")
                                    End If
                                End If
                                If Not (dr("FirstName") Is System.DBNull.Value) Then
                                    If dr("FirstName") <> "" Then
                                        If strName <> "" Then
                                            strName &= ", " & dr("FirstName")
                                        Else
                                            strName &= dr("FirstName")
                                        End If
                                    End If
                                End If
                                If Not (dr("MiddleName") Is System.DBNull.Value) Then
                                    If dr("MiddleName") <> "" Then
                                        If strName <> "" Then
                                            strName &= " " & dr("MiddleName") & "."
                                        Else
                                            strName &= dr("MiddleName") & "."
                                        End If
                                    End If
                                End If
                                dr("StudentName") = strName
                                '
                                'Apply mask to SSN.
                                If StudentIdentifier = "SSN" Then
                                    If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                                        If dr("StudentIdentifier") <> "" Then
                                            Dim temp As String = dr("StudentIdentifier")
                                            dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                        End If
                                    End If
                                End If
                                '
                                'dr("RecCount") = dtDefRevenue.Rows.Count
                            End If
                        Catch ex As Exception
                            Throw New Exception("Error building report dataset - " & ex.Message)
                        End Try
                    Next
                End If
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

    Private Function ExcludeStudent(ByVal sysStatusId As Integer, ByVal SDate As String, ByVal m_LDA As String, ByVal m_ExpGradDate As String, ByVal m_DateDetermined As String, ByVal BalanceOwed As Decimal, ByVal LastTransactionDate As Date) As Boolean
        Dim rtn As Boolean = False
        Dim dDate As Date
        Try

            Dim LDA As Date
            Dim ExpGradDate As Date
            Dim DateDetermined As Date
            If m_LDA.ToString = "" Then
                LDA = Date.Parse("1/1/1800")
            Else
                LDA = Date.Parse(m_LDA)
            End If

            If m_ExpGradDate.ToString = "" Then
                ExpGradDate = Date.Parse("1/1/1800")
            Else
                ExpGradDate = Date.Parse(m_ExpGradDate)
            End If

            If m_DateDetermined.ToString = "" Then
                DateDetermined = Date.Parse("1/1/1800")
            Else
                DateDetermined = Date.Parse(m_DateDetermined)
            End If



            If LDA > Date.Parse(SDate) And LDA <> "1/1/1800" Then
                dDate = LDA
            Else
                If sysStatusId = 14 And ExpGradDate <> "1/1/1800" Then
                    dDate = ExpGradDate
                Else
                    If DateDetermined <> "1/1/1800" Then
                        dDate = DateDetermined
                    Else
                        Return False
                    End If
                End If
            End If
         
            If BalanceOwed = 0 And LastTransactionDate <= Date.Parse(SDate) And dDate < Date.Parse(SDate) Then
                rtn = True
            End If



        Catch ex As Exception
            Dim strErr As String = ex.Message
        End Try
        Return rtn
    End Function
#End Region

End Class
