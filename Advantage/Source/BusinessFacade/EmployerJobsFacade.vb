' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' EmployerJobsFacade.vb
'
' EmployerJobsFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================

Public Class EmployerJobsFacade
    Public Function GetAllEmployerJobs() As DataSet

        'Instantiate DAL component
        With New EmployerJobsDB
            'get the dataset with all States
            Return .GetAllEmployerJobs()
        End With

    End Function
    Public Function GetAllEmployerJobsByCategory(ByVal JobCatId As String) As DataSet
        'Instantiate DAL component
        With New EmployerJobsDB
            'get the dataset with all States
            Return .GetAllEmployerJobsByCategory(JobCatId)
        End With
    End Function
    Public Function GetAllEmployerGroup() As DataSet

        'Instantiate DAL component
        With New EmployerJobsDB
            'get the dataset with all States
            Return .GetAllEmployerGroup()
        End With

    End Function

    Public Function GetAllJobGroup() As DataSet

        'Instantiate DAL component
        With New EmployerJobsDB
            'Get The Dataset with all states
            Return .GetAllJobGroups
        End With
    End Function
    Public Function GetAllJobType() As DataSet

        'Instantiate DAL component
        With New EmployerJobsDB
            'Get The Dataset with all states
            Return .GetAllJobType
        End With
    End Function
    Public Function GetAllJobTitles() As DataSet

        'Instantiate DAL component
        With New EmployerJobsDB
            'Get The Dataset with all states
            Return .GetAllJobTitles
        End With
    End Function

    Public Function GetAllJobSchedule() As DataSet
        'Instantiate DAL component
        With New EmployerJobsDB
            'Get The Dataset with all states
            Return .GetAllJobSchedule
        End With
    End Function
    Public Function GetAllJobBenefit() As DataSet
        'Instantiate DAL component
        With New EmployerJobsDB
            'Get The Dataset with all states
            Return .GetAllJobBenefits
        End With
    End Function
    Public Function GetAllJobContacts() As DataSet
        'Instantiate DAL component
        With New EmployerJobsDB
            'Get The Dataset with all states
            Return .GetAllJobContacts
        End With
    End Function
    Public Function GetAllJobContactsByEmployer(ByVal EmployerId As String) As DataSet
        'Instantiate DAL component
        Dim ds As New DataSet
        With New EmployerJobsDB
            'Get The Dataset with all states
            ds = .GetAllJobContactsByEmployer(EmployerId)
        End With
        ds = ConcatenateEmployerContactLastFirstName(ds)
        Return ds
    End Function

    Public Function UpdateEmployerInfo(ByVal employerinfo As EmployerJobsInfo, ByVal user As String, ByVal JobPostedDate As String) As String

        With New EmployerJobsDB
            If (employerinfo.IsInDB = False) Then
                Return .AddEmployerInfo(employerinfo, user, JobPostedDate)
            Else
                Return .UpdateEmployerInfo(employerinfo, user, JobPostedDate)
            End If
        End With
    End Function
    Public Function GetEmployerJobsInfo(ByVal employerjobsid As String) As EmployerJobsInfo

        With New EmployerJobsDB
            Return .GetEmployerJobsInfo(employerjobsid)
        End With
    End Function
    Public Function GetEmployerPlacementHistory(ByVal PlacementId As String) As PlacementHistoryInfo

        With New EmployerJobsDB
            Return .GetPlacementHistoryInfo(PlacementId)
        End With
    End Function
    Public Function GetAllEmployerJobsDesc(ByVal showActiveOnly As Boolean) As DataSet
        '   Instantiate DAL component
        With New EmployerJobsDB
            '   get the dataset with all BankCodes
            Return .GetAllEmployerJobsDesc(showActiveOnly)
        End With
    End Function
    Public Function GetAllEmployerJobsDescByEmployer(ByVal showActiveOnly As String, ByVal EmployerId As String) As DataSet
        '   Instantiate DAL component
        With New EmployerJobsDB
            '   get the dataset with all BankCodes
            Return .GetAllEmployerJobsDescByEmployer(showActiveOnly, EmployerId)
        End With
    End Function
    Public Function GetAllPlacementHistory(ByVal EmployerId As String, ByVal strLastName As String, ByVal strFirstName As String, ByVal strEmployerJobId As String) As DataSet
        With New EmployerJobsDB
            '   get the dataset with all Placement History
            Return .GetAllPlacementHistory1(EmployerId, strLastName, strFirstName, strEmployerJobId)
        End With
    End Function
    Public Function GetAllJobWorkDays() As DataSet
        'Instantiate DAL component
        With New EmployerJobsDB
            'Get The Dataset with all states
            Return .GetAllJobWorkDays
        End With
    End Function
    Public Function UpdateEmployerJobWorkDays(ByVal empId As String, ByVal user As String, ByVal selectedCertifications() As String) As Integer

        '   Instantiate DAL component
        Dim JobWorkDays As New EmployerJobsDB

        '   return query results
        Return JobWorkDays.UpdateEmployerJobWorkDays(empId, user, selectedCertifications)

    End Function

    Public Function GetWorkDays(ByVal empId As String) As DataSet

        '   Instantiate DAL component
        Dim empDB As New EmployerJobsDB

        '   return Dataset
        Return empDB.GetWorkdays(empId)

    End Function
    Public Function GetValidWorkdays(ByVal empId As String) As Integer

        '   Instantiate DAL component
        Dim empDB As New EmployerJobsDB

        '   return Dataset
        Return empDB.GetValidWorkDays(empId)
    End Function
    Public Function DeleteEmployerJobs(ByVal EmployerId As String, ByVal moddate As DateTime) As String

        '   Instantiate DAL component
        With New EmployerJobsDB

            '   delete EmployerInfo ans return integer result
            Return .DeleteEmployerJobs(EmployerId, moddate)

        End With

    End Function
    Public Function ConcatenateEmployerContactLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("EmployerContactId")}
        End With
        If ds.Tables("InstructorDT").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                If row("MiddleName") Is System.DBNull.Value Then
                    row("FullName") = row("FirstName") & " " & row("LastName")
                Else
                    row("FullName") = row("FirstName") & " " & row("MiddleName") & " " & row("LastName")
                End If
            Next
        End If
        Return ds
    End Function
    Public Function GetJobTitlesByEmployer(ByVal EmployerId As String) As DataSet
        Dim getjobsByemployerDb As New EmployerJobsDB
        Return getjobsByemployerDb.GetJobTitlesByEmployer(EmployerId)

    End Function
End Class
