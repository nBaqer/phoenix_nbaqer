Imports FAME.AdvantageV1.BusinessRules.StudentRegistration.Lib

Public Class RegistrationProcessFacade

    Public Function RegisterStudentIncompleteCourses(ByVal students As IEnumerable(Of  FAME.Advantage.Common.StudentRegistration.Lib.Student), Optional ByVal User As String = "") As String
        Dim _registrationservice As IRegistration
        Dim _transferservice As ITransfer
        Dim _archiveservice As IArchive
        Dim _registerInCompleteClass As registerInCompleteClass

        _registrationservice = New RegistrationService()
        _transferservice = New TransferService()
        _archiveservice = New ArchiveService()

        _registerInCompleteClass = New registerInCompleteClass(students, _registrationservice, _transferservice, _archiveservice, User)
        _registerInCompleteClass.RegisterStudent()

    End Function

End Class

