Public Class GrdBkWgtsFacade
    Public Function GetAllStatuses() As DataSet

        '   Instantiate DAL component
        Dim DB As New GrdBkWgtsDB

        '   get the dataset with all degrees
        Return DB.GetAllStatuses()

    End Function
    Public Function GetGrdComponentTypes() As DataSet

        '   Instantiate DAL component
        Dim DB As New GrdBkWgtsDB

        '   get the dataset with all degrees
        Return DB.GetGrdComponentTypes()

    End Function
    Public Function GetInstrGrdBkWgts(ByVal Instructor As String) As DataSet

        Dim db As New GrdBkWgtsDB
        Dim ds As New DataSet

        ds = db.GetInstrGrdBkWgts(Instructor)

        Return ds

    End Function
    Public Function GetGrdBkWgtDescStatus(ByVal strGuid As String) As DataSet

        Dim db As New GrdBkWgtsDB
        Dim ds As New DataSet

        ds = db.GetGrdBkWgtDescStatus(strGuid)

        Return ds

    End Function

    Public Function GetGrdBkWgtsOnItemCmd(ByVal strGuid As String) As DataSet

        Dim db As New GrdBkWgtsDB
        Dim ds As New DataSet

        ds = db.GetGrdBkWgtsOnItmCmd(strGuid)

        Return ds

    End Function

    Public Function PopulateDataList(ByVal InstructorId As String) As DataSet

        Dim db As New GrdBkWgtsDB
        Dim ds As New DataSet
        ds = db.GetInstrGrdBkWgts(InstructorId)
        Return ds

    End Function

    Public Function ProcessLstBoxInfo(ByVal ds As DataSet) As DataSet
        Dim DB As New GrdBkWgtsDB
        Dim tbl1 As New DataTable
        Dim tbl2 As New DataTable
        Dim row As DataRow


        tbl1 = ds.Tables("GrdBkWgtDetails")
        With ds.Tables("GrdBkWgtDetails")
            'Create primary key
            .PrimaryKey = New DataColumn() {tbl1.Columns("InstrGrdBkWgtDetailId")}
        End With


        If ds.Tables("GrdBkWgtDetails").Rows.Count > 0 Then
            'Load a datatable with ChildId, ChildTyp and ChildDescrip
            'Create a datable with a new column called ChildId, make this the primary key.

            'Add a new column called ChildDescrip to the datatable 
            Dim col9 As DataColumn = tbl1.Columns.Add("GrdBkWgtDescrip", GetType(String))

            For Each row In tbl1.Rows

                row("GrdBkWgtDescrip") = row("Descrip") & " ( " & row("Weight") & "%" & " ) "

            Next
        End If

        Return ds
    End Function

    Public Function InsertGrdBkWgt(ByVal GrdBkWgtObj As GrdBkWgtsInfo, ByVal user As String) As String
        Dim DB As New GrdBkWgtsDB

        Return DB.InsertGrdBkWgtToDB(GrdBkWgtObj, user)
    End Function

    Public Function UpdateGrdBkWgt(ByVal GrdBkWgtObj As GrdBkWgtsInfo, ByVal user As String)
        Dim DB As New GrdBkWgtsDB

        DB.UpdateGrdBkWgtToDB(GrdBkWgtObj, user)
    End Function

    Public Function InsertGrdBkWgtDetail(ByVal GrdBkWgtDetObj As GrdBkWgtDetailsInfo, ByVal user As String) As String
        Dim DB As New GrdBkWgtsDB

        Return DB.InsertGrdBkWgtDet(GrdBkWgtDetObj, user)
    End Function

    Public Function UpdateGrdBkWgtDetail(ByVal GrdBkWgtDetObj As GrdBkWgtDetailsInfo, ByVal user As String)
        Dim DB As New GrdBkWgtsDB

        Return DB.UpdateGrdBkWgtDet(GrdBkWgtDetObj, user)
    End Function

    Public Function DeleteGrdBkWgtDetail(ByVal GrdBkWgtObj As GrdBkWgtDetailsInfo, ByVal modDate As DateTime) As String
        Dim DB As New GrdBkWgtsDB

        Return DB.DeleteGrdBkWgtDet(GrdBkWgtObj, modDate)
    End Function
    Public Function DeleteGrdBkWgtDetails(ByVal GrdBkWgtId As String, ByVal modDate As DateTime) As String
        Dim db As New GrdBkWgtsDB
        Return db.DeleteGrdBkWgtDetails(GrdBkWgtId, modDate)
    End Function
    Public Function DeleteGrdBkWeight(ByVal GrdBkWgtId As String, ByVal modDate As DateTime) As String
        Dim DB As New GrdBkWgtsDB

        Return DB.DeleteGrdBkWgt(GrdBkWgtId, modDate)
    End Function
    Public Function UpdateGradeBookWeightsDS(ByVal dt As DataTable) As String

        '   Instantiate DAL component
        With New GrdBkWgtsDB
            '   update dataset and return result in a string
            Return .UpdateGradeBookWeightsDS(dt)
        End With

    End Function
    Public Function GetGradebookWeightsByCourse(ByVal courseId As String) As DataSet
        'return dataset
        Return (New GrdBkWgtsDB).GetGradebookWeightsByCourse(courseId)
    End Function
    Public Function GetInstrGrdBkWgtsInfo(ByVal instrGrdBkWgtId As String) As InstrGrdBkWgtsInfo
        'return GrdBkWgtsInfo
        Return (New GrdBkWgtsDB).GetInstrGrdBkWgtsInfo(instrGrdBkWgtId)
    End Function
    Public Function UpdateInstrGradeBookWeightsDS(ByVal ds As DataSet) As String

        '   update dataset and return result in a string
        Return (New GrdBkWgtsDB).UpdateInstrGradeBookWeightsDS(ds)

    End Function
    Public Function GetInstrGrdBkWgtsInfoEmpty(ByVal courseId As String) As InstrGrdBkWgtsInfo
        'return GrdBkWgtsInfo
        Return (New GrdBkWgtsDB).GetInstrGrdBkWgtsInfoEmpty(courseId)
    End Function
    Public Function AreThereDuplicatedRecordsInGradeBookWeightings(ByVal ds As DataSet) As Boolean

        '   return boolean
        Return (New GrdBkWgtsDB).AreThereDuplicatedRecordsInGradeBookWeightings(ds)

    End Function
End Class
