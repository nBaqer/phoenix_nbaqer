' ===============================================================================
' MSGFacade.vb
' Business Logic for MSG project. 
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed partially by ThinkTron Corporation
' ===============================================================================
' 8/21/06 - Ben: Change to ContactSearchFacade so that param RecipientType reflects the
'           ResourceId in syResources rather than the resource string.

Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.UI
Imports FAME.AdvantageV1.DataAccess.MSG
Imports FAME.AdvantageV1.Common.MSG
Imports FAME.Advantage.Common
Imports System.Net
Imports System.Net.Http
Imports FAME.AdvantageV1.Common.smtp

Namespace MSG

#Region "Contact Search"
    Public Class ContactSearchFacade
        ' Search for a recipient for a message
        ' The important return is a GUID that represents the id of the recipient
        Public Shared Function RecipientSearch(ByVal recipientType As String, ByVal search As String, ByVal campusId As String) As DataSet
            Select Case recipientType
                Case "394"
                    Return SearchStudent(search, campusId)
                Case "395"
                    Return SearchLead(search, campusId)
                Case "434"
                    Return SearchLender(search, campusId)
                Case "396"
                    Return SearchEmployees(search, campusId)
                Case "397"
                    Return SearchEmployers(search, campusId)
            End Select
            Return Nothing ' Bad param sent
        End Function

        Public Shared Function SearchStudent(ByVal search As String, ByVal campusId As String) As DataSet
            Dim db As New DataAccess.DataAccess

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("   TOP 50 S.StudentId RecipientId, " & vbCrLf)
            '' Code changed by kamalesh ahuja on June 05 2010 to resolve mantis issue id 19137
            ''sb.Append("   (S.FirstName + ' ' + S.LastName) Name, " & vbCrLf)
            sb.Append("   (Replace(S.FirstName,'''',' ') + ' ' + Replace(S.LastName,'''',' ')) Name, " & vbCrLf)
            '''''''''''''''
            sb.Append("   S.SSN SSN, " & vbCrLf)
            sb.Append("   '' SecondIdDesc " & vbCrLf)
            sb.Append("FROM arStudent S " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            Dim strData As String = search.Trim
            Dim iPosition As Integer = strData.IndexOf(" ", StringComparison.Ordinal)
            If iPosition > 0 Then
                sb.Append("(S.LastName like '" + Right(strData, iPosition + 1) + "%' " & vbCrLf)
                sb.Append("OR S.FirstName like '" + Left(strData, iPosition) + "%') " & vbCrLf)
            Else
                sb.Append("(S.LastName like '" + strData + "%' " & vbCrLf)
                sb.Append("OR S.FirstName like '" + strData + "%') " & vbCrLf)
            End If

            ' restrict the search to this campusid
            If Not campusId Is Nothing AndAlso campusId <> "" Then
                sb.AppendFormat(" AND '{0}' in (select CampusId from arStuEnrollments SE where S.StudentId=SE.StudentId) {1}", campusId, vbCrLf)
            End If

            sb.Append("ORDER BY S.LastName, S.FirstName" & vbCrLf)
            'Execute the query a
            Dim ds As DataSet = db.RunSQLDataSet(sb.ToString)
            MSGCommon.ApplySSNMaskToDS(ds)
            Return ds
        End Function

        Public Shared Function SearchLead(ByVal search As String, ByVal CampusId As String) As DataSet
            Dim db As New DataAccess.DataAccess

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            '   build query to select students
            Dim sb As New StringBuilder
            Dim strData As String = search.Trim
            Dim iPosition As Integer = strData.IndexOf(" ", StringComparison.Ordinal)
            sb.Append(" SELECT TOP 50 LeadID RecipientId, " & vbCrLf)
            '' Code changed by kamalesh ahuja on June 05 2010 to resolve mantis issue id 19137
            ''sb.Append("   (FirstName + ' ' + LastName) Name, " & vbCrLf)
            sb.Append("   (Replace(FirstName,'''',' ') + ' ' + Replace(LastName,'''',' ')) Name, " & vbCrLf)
            '''''''''''''''
            sb.Append("   SSN SSN, " & vbCrLf)
            sb.Append("   '' SecondIdDesc ")
            sb.Append("FROM adLeads" & vbCrLf)

            If Not CampusId Is Nothing AndAlso CampusId <> "" Then
                sb.Append("WHERE " & vbCrLf)
                sb.Append("   CampusId = '" + CampusId + "' AND " & vbCrLf)
            End If

            If iPosition > 0 Then
                sb.Append("(LastName like '" + Right(strData, iPosition + 1) + "%' " & vbCrLf)
                sb.Append("OR FirstName like '" + Left(strData, iPosition) + "%') " & vbCrLf)
            Else
                sb.Append("(LastName like '" + strData + "%' " & vbCrLf)
                sb.Append("OR FirstName like '" + strData + "%') " & vbCrLf)
            End If


            sb.Append("ORDER BY Name")

            Dim ds As DataSet = db.RunSQLDataSet(sb.ToString)
            ' apply the ssn mask
            MSGCommon.ApplySSNMaskToDS(ds)
            Return ds
        End Function

        Public Shared Function SearchLender(ByVal search As String, ByVal CampusId As String) As DataSet
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build query to select students
            Dim sb As New StringBuilder
            sb.Append("SELECT TOP 50 LenderID RecipientId, ")
            sb.Append("   LenderDescrip as Name, ")
            sb.Append("   '' SSN, " & vbCrLf)
            sb.Append("   '' SecondIdDesc " & vbCrLf)
            sb.Append("FROM faLenders" & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("   StatusId is not NULL " & vbCrLf)
            sb.Append("   AND LenderDescrip like '" + search.Trim + "%' " & vbCrLf)
            sb.Append("ORDER BY Name")

            'Execute the query a
            Return db.RunSQLDataSet(sb.ToString)
        End Function

        Public Shared Function SearchEmployees(ByVal search As String, ByVal CampusId As String) As DataSet
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("   E.EmpId RecipientId, " & vbCrLf)
            '' Code changed by kamalesh Ahuja on June 05 2010 to resolve mantis issue id 19137
            '' sb.Append("   (E.FirstName + ' ' + E.LastName) Name, " & vbCrLf)
            sb.Append("   (Replace(E.FirstName,'''',' ') + ' ' + Replace(E.LastName,'''',' ')) Name, " & vbCrLf)
            '''''''''''''''
            sb.Append("   E.SSN SSN, " & vbCrLf)
            sb.Append("   '' SecondIdDesc " & vbCrLf)
            sb.Append("FROM hrEmployees E " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            Dim strData As String = search.Trim
            Dim iPosition As Integer = strData.IndexOf(" ")
            If iPosition > 0 Then
                sb.Append("(E.LastName like '" + Right(strData, iPosition + 1) + "%' " & vbCrLf)
                sb.Append("OR E.FirstName like '" + Left(strData, iPosition) + "%') " & vbCrLf)
            Else
                sb.Append("(E.LastName like '" + strData + "%' " & vbCrLf)
                sb.Append("OR E.FirstName like '" + strData + "%') " & vbCrLf)
            End If
            sb.Append("ORDER BY E.LastName, E.FirstName" & vbCrLf)
            'Execute the query a
            Dim ds As DataSet = db.RunSQLDataSet(sb.ToString)
            MSGCommon.ApplySSNMaskToDS(ds)
            Return ds
        End Function

        Public Shared Function SearchEmployers(ByVal search As String, ByVal CampusId As String) As DataSet
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("   EmployerId RecipientId, " & vbCrLf)
            sb.Append("   EmployerDescrip Name, " & vbCrLf)
            sb.Append("   '' SSN, " & vbCrLf)
            sb.Append("   '' SecondIdDesc " & vbCrLf)
            sb.Append("FROM plEmployers " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            Dim strData As String = search.Trim
            Dim iPosition As Integer = strData.IndexOf(" ")
            If iPosition > 0 Then
                sb.Append("EmployerDescrip like '" + Right(strData, iPosition + 1) + "%' " & vbCrLf)
            Else
                sb.Append("EmployerDescrip like '" + strData + "%' " & vbCrLf)
            End If

            If Not CampusId Is Nothing AndAlso CampusId <> "" Then
                sb.AppendFormat(" AND CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId='{0}')", CampusId)
            End If
            sb.Append("ORDER BY EmployerDescrip" & vbCrLf)
            'Execute the query a
            Dim ds As DataSet = db.RunSQLDataSet(sb.ToString)
            Return ds
        End Function
    End Class
#End Region

#Region "Groups"
    Public Class GroupsFacade
        Public Shared Function GetGroups(ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Return GroupsDB.GetAll(ShowActive, ShowInactive)
        End Function

        Public Shared Function GetGroupInfo(ByVal GroupId As String) As GroupInfo
            Return GroupsDB.GetInfo(GroupId)
        End Function

        Public Shared Function UpdateGroup(ByVal info As GroupInfo, ByVal user As String) As Integer
            If info.IsInDB = False Then
                Return GroupsDB.Add(info, user)
            Else
                Return GroupsDB.Update(info, user)
            End If
        End Function

        Public Shared Function DeleteGroup(ByVal GroupId As String, ByVal modDate As DateTime) As String
            Return GroupsDB.Delete(GroupId, modDate)
        End Function

        Public Shared Function DeleteAllGroups() As Boolean
            Return GroupsDB.DeleteAll()
        End Function
    End Class
#End Region

#Region "Messages"
    Public Class MessagingFacade
#Region "Message Queue Methods"
        Public Shared Function GetAllMessages_OutBox(ByVal filterRecipientType As String, ByVal filterRecipientId As String,
                ByVal filterDeliveryType As String, ByVal filterMessageGroupId As String, ByVal filterTemplateId As String,
                ByVal filterShowOnlyErrors As Boolean, ByVal filterMessageId As String) As DataSet
            'Return MessagingDB.GetAll_OutBox(filterRecipientType, filterRecipientId, filterDeliveryType, filterMessageGroupId, filterTemplateId, filterShowOnlyErrors)


            Dim gRecipientId As Guid
            Dim gGroupId As Guid
            Dim gTemplateId As Guid
            Dim gMessageId As Guid

            If Not String.IsNullOrEmpty(filterRecipientId) Then
                Guid.TryParse(filterRecipientId, gRecipientId)
            End If
            If Not String.IsNullOrEmpty(filterMessageGroupId) Then
                Guid.TryParse(filterMessageGroupId, gGroupId)
            End If
            If Not String.IsNullOrEmpty(filterTemplateId) Then
                Guid.TryParse(filterTemplateId, gTemplateId)
            End If
            If Not String.IsNullOrEmpty(filterMessageId) Then
                Guid.TryParse(filterMessageId, gMessageId)
            End If

            Return MessagingDB.GetAll_OutBox(filterRecipientType, gRecipientId, filterDeliveryType, gGroupId, gTemplateId, filterShowOnlyErrors, gMessageId)
        End Function



        Public Shared Function GetAllMessages_Sent(ByVal filterRecipientType As String, ByVal filterRecipientId As String, _
                ByVal filterDeliveryType As String, ByVal filterMessageGroupId As String, ByVal filterTemplateId As String, _
                ByVal filterShowOnlyErrors As Boolean) As DataSet
            Return MessagingDB.GetAll_Sent(filterRecipientType, filterRecipientId, filterDeliveryType, filterMessageGroupId, filterTemplateId, filterShowOnlyErrors)
        End Function

        Public Shared Function GetMessageInfo(ByVal MessageId As String) As MessageInfo
            Return MessagingDB.GetInfo(MessageId)
        End Function

        Public Shared Function AddMessage(ByVal info As MessageInfo, ByVal user As String) As Boolean
            Return MessagingDB.Add(info, user)
        End Function

        Public Shared Function DeleteMessage(ByVal MessageId As String) As Integer
            Return MessagingDB.Delete(MessageId)
        End Function
        Public Shared Function DeleteMessageAdHoc(ByVal MessageIds As String) As Integer
            Return MessagingDB.DeleteAdHoc(MessageIds)
        End Function

        Public Shared Function UpdateMessage(msgInfo As MessageInfo, user As String) As Boolean
            If msgInfo.IsInDB = False Then
                Return MessagingDB.Add(msgInfo, user)
            Else
                Return MessagingDB.Update(msgInfo, user)
            End If
        End Function

        ' Method: DeliverMessage #1
        ' Description: Delivers a message.  This method only support email because the Page object
        '   is needed to print.
        ' The parameter "bCommit" if set to true, sends a message to the Sent Items if the
        ' delivery type is Print or unsupported
        Public Shared Function DeliverMessage(ByVal MessageID As String, ByVal bCommit As Boolean, ByVal user As String) As String
            Try
                Dim msgInfo As MessageInfo = GetMessageInfo(MessageID)
                If msgInfo.DeliveryType.ToLower = "email" Then
                    Return _DeliverMessage_Email(msgInfo, user)
                ElseIf msgInfo.DeliveryType.ToLower = "printer" Then
                    ' send it to the "Sent Items"
                    msgInfo.LastDeliveryAttempt = Date.Now
                    msgInfo.LastDeliveryMsg = ""
                    msgInfo.DateDelivered = Date.Now
                    UpdateMessage(msgInfo, user)
                    Return "Wrong method used to print.  Use <DeliverMessage(pg,MessageId,user)>."
                    'Return _DeliverMessage_Printer(msgInfo, user)
                Else
                    ' send it to the "Sent Items"
                    msgInfo.LastDeliveryAttempt = Date.Now
                    msgInfo.LastDeliveryMsg = ""
                    msgInfo.DateDelivered = Date.Now
                    UpdateMessage(msgInfo, user)
                    Return "Unsupported delivery type"
                End If
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        ' Method: DeliverMessage #2
        ' Description: This is the preferred method used to deliver a message because the page object is passed.
        '   This method support both Email and Print.
        Public Shared Function DeliverMessage(ByRef pg As Page, ByVal MessageID As String, ByVal user As String) As String
            Try
                Dim msgInfo As MessageInfo = GetMessageInfo(MessageID)
                If msgInfo.DeliveryType.ToLower = "email" Then
                    Return _DeliverMessage_Email(msgInfo, user)
                ElseIf msgInfo.DeliveryType.ToLower = "printer" Then
                    Return _DeliverMessage_Printer(pg, msgInfo, user)
                Else
                    Return "Unsupported delivery type"
                End If
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function
        ''New Code Added By Vijay Ramteke On May 07, 2010
        Public Shared Function DeliverMessageAdHoc(ByRef pg As Page, ByVal MessageID As String, ByVal user As String) As String
            Try
                Dim msgInfo As MessageInfo = GetMessageInfo(MessageID)
                If msgInfo.DeliveryType.ToLower = "email" Then
                    Return _DeliverMessage_Email(msgInfo, user)
                ElseIf msgInfo.DeliveryType.ToLower = "printer" Then
                    Return ""
                    'Return _DeliverMessage_Printer(pg, msgInfo, user)
                Else
                    Return "Unsupported delivery type"
                End If
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function DeliverMessageAdHocPrinter(ByRef pg As Page, msgContent As String) As String
            Try
                '_DeliverMessage_PrinterAdHoc(pg, msgContent)
                MSGCommon.PrintDocument(pg, msgContent)
                Return String.Empty
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        ''New Code Added By Vijay Ramteke On May 07, 2010
        Private Shared Function _DeliverMessage_Email(ByRef msgInfo As MessageInfo, user As String) As String
            Try
                msgInfo.LastDeliveryAttempt = Date.Now
                Dim MailFrom As String = msgInfo.MailFrom
                Dim Mailto As String = msgInfo.MailTo
                Dim Subject = msgInfo.TemplateDescrip
                If Mailto = "" Then
                    msgInfo.LastDeliveryMsg = "No email defined for this contact"
                Else
                    msgInfo.LastDeliveryMsg = Mail.SendMail(MailFrom, Mailto, "", Subject, msgInfo.MsgContent, "", "html", "", "", "", "")
                End If

                If msgInfo.LastDeliveryMsg = String.Empty Then msgInfo.DateDelivered = Date.Now
            Catch ex As Exception
                msgInfo.LastDeliveryMsg = ex.Message
            End Try
            Try
                UpdateMessage(msgInfo, user)
            Catch ex As Exception
                msgInfo.LastDeliveryMsg = ex.Message
            End Try

            Return msgInfo.LastDeliveryMsg
        End Function

        Private Shared Function _DeliverMessage_Printer(pg As Page, ByRef msgInfo As MessageInfo, user As String) As String
            Try
                msgInfo.LastDeliveryAttempt = Date.Now
                
                ' Print the document
                MSGCommon.PrintDocument(pg, msgInfo.MsgContent)
                msgInfo.LastDeliveryMsg = ""
                msgInfo.DateDelivered = Date.Now
            Catch ex As Exception
                msgInfo.LastDeliveryMsg = ex.Message
            End Try

            Try
                UpdateMessage(msgInfo, user)
            Catch ex As Exception
                msgInfo.LastDeliveryMsg = ex.Message
            End Try

            Return msgInfo.LastDeliveryMsg
        End Function
        ''
        'Private Shared Function _DeliverMessage_PrinterAdHoc(pg As Page, msgContent As String) As String
        '    Dim strReturn = ""
        '    Try
        '        MSGCommon.PrintDocument(pg, msgContent)
        '    Catch ex As Exception
        '        strReturn = ex.Message
        '    End Try
        '    Return strReturn
        'End Function
#End Region
    End Class
#End Region

#Region "Rules"
    Public Class RulesFacade
        Public Shared Function GetAllRules(ByVal bShowActive As Boolean, ByVal bShowInActive As Boolean) As DataSet
            Return RulesDB.GetAll(bShowActive, bShowInActive)
        End Function

        Public Shared Function GetRuleInfo(ByVal RuleId As String) As RuleInfo
            Return RulesDB.GetInfo(RuleId)
        End Function

        Public Shared Function UpdateRule(ByVal info As RuleInfo, ByVal user As String) As Boolean
            '   If it is a new account do an insert. If not, do an update
            If info.IsInDB = False Then
                Return RulesDB.Add(info, user)
            Else
                Return RulesDB.Update(info, user)
            End If
        End Function

        Public Shared Function DeleteAllRules() As Boolean
            Return RulesDB.DeleteAll()
        End Function

        ' Method: Sub RunAllRules
        ' Description: Get all active rules and process them.
        ' The typical rule has some sql statement stored in RuleSql.  Executing this sql statement
        ' should return a set of recipients, all of which should get a message sent to them based on 
        ' TemplateId.  
        ' There is one special type of Rule that sends all messages in the outbox.  This is
        ' the rule where RuleType = 1.
        Public Shared Function RunAllRules() As String
            Try
                ' create the facade.  Note: This might need to reference somthing else
                ' during the integration with Advantage            
                ' Retrieve all rules
                Dim dsRules As DataSet = GetAllRules(True, False)
                Dim dr As DataRow
                For Each dr In dsRules.Tables(0).Rows
                    Dim ruleId As String = dr("RuleId").ToString()
                    ' process this specific rule and log the result
                    Dim strResult As String = RunRule(ruleId)
                Next
            Catch ex As Exception
                Return ex.Message
            End Try
            Return ""
        End Function

        ' Method: RunRule
        ' Description: Runs a specific rule.  This method is called by ProcessRules.
        '   Take the RuleSql and execute it.  The result should be a set of RecipientIDs.
        ' A message specified by TemplateId will get generated for each recipientID.
        Public Shared Function RunRule(ByVal RuleId As String) As String
            Try
                ' Get the RuleInfo object for the rule specifiec by RuleId
                Dim rInfo As RuleInfo = GetRuleInfo(RuleId)
                Dim strRes As String = ""
                Try
                    If rInfo.Type = 1 Then
                        strRes = _RunSpecialRule(rInfo)
                    Else
                        strRes = _RunSqlRule(rInfo)
                    End If
                Catch ex As Exception
                    strRes = ex.Message
                End Try

                ' update the rule's LastRunDate and LastRunError            
                If strRes = "" Then
                    rInfo.LastRun = Date.Now
                    rInfo.LastRunError = "Success"
                Else
                    rInfo.LastRunError = strRes
                End If
                rInfo.IsInDB = True
                If Not UpdateRule(rInfo, MSGCommon.GetCurrentUserId()) Then
                    Return "Failed to update rules"
                End If
                Return strRes ' return the result
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        ' process messages that are in the outbox
        Private Shared Function _RunSpecialRule(ByRef rInfo As RuleInfo) As String
            Try
                ' create the facade.  Note: This might need to reference somthing else
                ' during the integration with Advantage
                ' Retrieve all pending messages waiting to be sent from the outbox
                Dim dsMsg As DataSet = MessagingFacade.GetAllMessages_OutBox(Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
                Dim dr As DataRow
                For Each dr In dsMsg.Tables(0).Rows
                    Try
                        Dim MessageId As String = dr("MessageId").ToString
                        Dim DeliveryType As String = dr("DeliveryType")
                        If DeliveryType = "Email" Then
                            MessagingFacade.DeliverMessage(MessageId, False, "sa")
                        ElseIf DeliveryType = "Printer" Then
                            ' Not sure if we should do anything here because
                            ' printing will print on the server which is not 
                            ' really the desired effect.  
                            ' Perhaps they should all be delivered to a pdf?
                        End If
                    Catch ex As Exception
                        ' Log the error in to the Message Error Log
                        'LogError(ex.Message)
                    End Try
                Next
            Catch ex As Exception
            End Try
            Return ""
        End Function

        ' Run a normal rule that has RuleSql defined.  This method is executed for
        ' any rule that does not have RuleType = 1
        Private Shared Function _RunSqlRule(ByRef rInfo As RuleInfo) As String

            ' connect to the database
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder(rInfo.RuleSql)
            ' Look for keywords and replace them
            If rInfo.LastRun <> Date.MinValue Then
                sb.Replace("[LASTRUNDATE]", rInfo.LastRun.ToString())
            Else
                sb.Replace("[LASTRUNDATE]", Date.Now.AddSeconds(-10).ToString())
            End If

            sb.Replace("[NOW]", Date.Now.ToString())
            ' debuging only
            Dim str As String = sb.ToString()
            ' Run the sql and return the dataset
            Dim ds As DataSet = db.RunSQLDataSet(sb.ToString)
            ' check if there is an error in this statement (ie, there must be a RecipientID defined
            If ds.Tables(0).Columns("RecipientID") Is Nothing Then
                Return "Invalid sql statement for this rule.  [RecipientID] is not in the select statement."
            End If

            ' Get the template that this rule should generate
            Dim tInfo As TemplateInfo = TemplatesFacade.GetTemplateInfo(rInfo.TemplateId)

            ' generate a message for each recipient
            Dim dr As DataRow
            'Dim recipientID As String
            Dim sbResult As New StringBuilder
            For Each dr In ds.Tables(0).Rows
                Try
                    ' create a new messageinfo structure
                    Dim msgInfo As New MessageInfo
                    msgInfo.IsInDB = False
                    msgInfo.DeliveryType = rInfo.DeliveryType

                    ' add the recipient type
                    msgInfo.RecipientType = rInfo.RecipientType
                    msgInfo.RecipientId = dr("RecipientID").ToString()
                    Try
                        msgInfo.ReType = rInfo.ReType
                        msgInfo.ReId = dr("ReID").ToString()
                    Catch ex As Exception
                    End Try
                    ' assign the ChangeId
                    Try
                        msgInfo.ChangeId = dr("ChangeId").ToString()
                    Catch ex As Exception
                    End Try

                    msgInfo.DeliveryDate = Date.Now
                    msgInfo.TemplateId = rInfo.TemplateId

                    ' do the special field substitution
                    Dim params As New SpecialFields_Params(MSGCommon.GetCampusID(), msgInfo)
                    Dim strMsg As String = SpecialFields.SubSpecialFields(tInfo.Data, params)

                    msgInfo.MsgContent = strMsg

                    ' add the message to the queue
                    Dim strRes As String = MessagingFacade.AddMessage(msgInfo, "sa")
                    If strRes <> "" Then sb.Append(strRes & vbCrLf)
                Catch ex As Exception
                    sbResult.Append(ex.Message & vbCrLf)
                End Try
            Next
            Return sbResult.ToString.Replace("'", "")
        End Function
    End Class
#End Region

#Region "Special Fields"
    ''' <summary>
    ''' Class for passing parameters to the special field processor, SpecialFields
    ''' </summary>
    ''' <remarks></remarks>
    Public Class SpecialFields_Params
#Region " Variables "
        Public _RecipientType As String
        Public _RecipientId As String
        Public _ReType As String
        Public _ReId As String
        Public _CampusID As String
        Public _ChangeId As String
#End Region

#Region "Constructor/Destructor"
        Public Sub New()

        End Sub
        Public Sub New(ByVal CampusId As String, ByVal RecipientType As String, ByVal RecipientId As String, _
                                    ByVal ReType As String, ByVal ReId As String, ByVal ChangeId As String)
            _CampusID = CampusId
            _RecipientType = RecipientType
            _RecipientId = RecipientId
            _ReType = ReType
            _ReId = ReId
            _ChangeId = ChangeId

            If _RecipientId Is Nothing Or _RecipientId = "" Then _RecipientId = Guid.Empty.ToString
            If _ReId Is Nothing Or _ReId = "" Then _ReId = Guid.Empty.ToString
            If _ChangeId Is Nothing Or _ChangeId = "" Then _ChangeId = Guid.Empty.ToString

        End Sub

        Public Sub New(ByVal CampusId As String, ByRef mInfo As MessageInfo)
            _CampusID = CampusId
            _RecipientType = mInfo.RecipientType
            _RecipientId = mInfo.RecipientId
            _ReType = mInfo.ReType
            _ReId = mInfo.ReId
            _ChangeId = mInfo.ChangeId

            If _RecipientId Is Nothing Or _RecipientId = "" Then _RecipientId = Guid.Empty.ToString
            If _ReId Is Nothing Or _ReId = "" Then _ReId = Guid.Empty.ToString
            If _ChangeId Is Nothing Or _ChangeId = "" Then _ChangeId = Guid.Empty.ToString
        End Sub
#End Region
    End Class

    Public Class SpecialFields
        ' Substitutes special fields with active content from Advantage database.
        ' Param: s: text that will be processed
        ' Param: RecipientType: one of "student", "lead", "lender"
        ' Param: RecipientGuid: guid which identifies the recipient
        Public Shared Function SubSpecialFields(ByVal s As String, ByVal params As SpecialFields_Params) As String
            ' return an empty string if that's what was sent
            If s Is Nothing Or s = "" Then Return ""

            ' we work with a sb for efficiency
            Dim sb As New StringBuilder(s)

            Dim dsFields As DataSet = _LoadSpecialFields()
            Dim dsAllFields As New DataSet

            ' simply loop through the string searching for the start of
            ' a special field which is denoted by a "[".
            Dim si As Integer = 0   ' start index
            Dim spField As String = ""
            Do
                spField = _GetNextSpecialField(s, si)
                If spField.Length > 0 Then
                    _DoSub(sb, dsAllFields, dsFields, spField, params)
                End If
            Loop While si > -1

            Return sb.ToString() ' return the substituted text
        End Function
        ''New Code Added By Vijay Ramteke On May 06, 2010
        Public Shared Function SubSpecialFieldsAdHoc(ByVal s As String, ByVal ds As DataSet) As String
            ' return an empty string if that's what was sent
            If s Is Nothing Or s = "" Then Return ""

            ' we work with a sb for efficiency
            Dim sbAdHoc As New StringBuilder()
            Dim dsFields As DataSet = _LoadSpecialFields()
            Dim dtColumnName As New DataTable
            dtColumnName.Columns.Add("ColumnName", Type.GetType("System.String"))
            Dim strColunmName As String = ""
            Dim intLastRow = ds.Tables(0).Rows.Count
            Dim intCurrentRow = 1
            Dim i As Integer = 0
            For Each dc As DataColumn In ds.Tables(0).Columns
                Dim drC As DataRow = dtColumnName.NewRow()
                drC("ColumnName") = dc.ColumnName
                dtColumnName.Rows.Add(drC)
            Next
            For Each drAH As DataRow In ds.Tables(0).Rows
                If Not drAH("RecipientId").ToString = Guid.Empty.ToString Then
                    Dim sb As New StringBuilder(s)
                    Dim si As Integer = 0   ' start index
                    Dim spField As String = ""
                    Do
                        spField = _GetNextSpecialField(s, si)
                        If spField.Length > 0 Then
                            _DoSubAdHoc(sb, dsFields, spField, drAH, dtColumnName)
                        End If
                    Loop While si > -1
                    sbAdHoc.Append(sb.ToString)
                    If intCurrentRow < intLastRow Then
                        sbAdHoc.Append("<p style='page-break-before: always'>")
                    End If
                    intCurrentRow = intCurrentRow + 1
                End If
            Next
            If sbAdHoc.ToString + "" = "" Then
                sbAdHoc.Append(s)
            End If


            Return sbAdHoc.ToString() ' return the substituted text
        End Function
        ''New Code Added By Vijay Ramteke On May 06, 2010
#Region "Helper functions"
        ' Method: _DoSub
        ' Description: Does the actual substibution
        Private Shared Sub _DoSub(ByRef sb As StringBuilder, ByRef dsAllFields As DataSet, _
                                ByRef dsFields As DataSet, ByVal spField As String, ByRef params As SpecialFields_Params)
            Try
                ' first, lookup the field
                Dim strSelect As String = "name='" & spField & "'"
                Dim drs() As DataRow = dsFields.Tables("AdvMessaging_Fields").Select(strSelect)
                If drs.Length > 1 Or drs.Length = 0 Then
                    ' error
                    'MSGCommon.DisplayErrorMessage(Me, "Error in MsgSpecialFields::_DoSub, drs.Length>1 or drs.Length=0 for field " & spField)
                    Return ' leave the routine
                End If

                Dim dr As DataRow = drs(0)
                ' With the name of the special field, we can get the query id/number that correspond to it.
                Dim qid As Integer = dr("queryid")
                Dim qtype As String = dr("querytype")
                Dim column As String = dr("column")
                Dim fmt As String = dr("format")

                ' check if this query has been performed already.  If it has not,
                ' perform the query and add it to the dsAllFields dataset.
                If dsAllFields.Tables(qid.ToString()) Is Nothing Then
                    Dim tmpdt As DataTable = _GetQueryResults(qid, qtype, params)
                    dsAllFields.Tables.Add(tmpdt)
                End If

                Dim dt As DataTable = dsAllFields.Tables(qid.ToString())
                If dt Is Nothing Then
                    ' error
                    'MSGCommon.DisplayErrorMessage("Error in MsgSpecialFields::_DoSub, nothing was returned from _GetQueryResults for qid=" & qid.ToString())
                    Return ' leave the routine
                End If

                Dim activeContent As New StringBuilder
                Dim tmp As String
                For Each dr In dt.Rows
                    tmp = _Format(fmt, dr(column))
                    activeContent.Append(tmp)
                    If dt.Rows.Count > 1 Then activeContent.Append(", ")
                Next
                sb.Replace(spField, activeContent.ToString())
            Catch ex As Exception
                'MSGCommon.DisplayErrorMessage(ex.Message)
            End Try
        End Sub
        ''New Code Added By Vijay Ramteke On May 06, 2010
        Private Shared Sub _DoSubAdHoc(ByRef sb As StringBuilder, ByRef dsFields As DataSet, ByVal spField As String, ByVal drAH As DataRow, ByRef dtColumn As DataTable)
            Try
                ' first, lookup the field
                Dim strSelect As String = "name='" & spField & "'"
                Dim drs() As DataRow = dsFields.Tables("AdvMessaging_Fields").Select(strSelect)
                If drs.Length > 1 Or drs.Length = 0 Then
                    ' error
                    'MSGCommon.DisplayErrorMessage(Me, "Error in MsgSpecialFields::_DoSub, drs.Length>1 or drs.Length=0 for field " & spField)
                    Return ' leave the routine
                End If

                Dim dr As DataRow = drs(0)
                ' With the name of the special field, we can get the query id/number that correspond to it.
                Dim qid As Integer = dr("queryid")
                Dim qtype As String = dr("querytype")
                Dim column As String = dr("column")
                Dim fmt As String = dr("format")
                Dim strColName As String = ""
                Dim activeContent As New StringBuilder
                Dim tmp As String
                If Not column = "t" AndAlso Not column = "d" Then
                    ''New Code Added By Vijay Ramteke On June 29, 2010 For Mantis Id 19136
                    ''For Each strCM As String In ColuumnName
                    ''    ''New Code Added By Vijay Ramteke On June 15, 2010 For Mantis Id 19136
                    ''    If column.ToLower.Contains("state") And strCM.ToLower = "stateid" Then
                    ''        strColName = strCM
                    ''        Exit For
                    ''    End If
                    ''    If column.ToLower.Contains("homephone") And strCM.ToLower = "phone" Then
                    ''        strColName = strCM
                    ''        Exit For
                    ''    End If
                    ''    ''New Code Added By Vijay Ramteke On June 15, 2010 For Mantis Id 19136
                    ''    If strCM = column Then
                    ''        strColName = strCM
                    ''        Exit For
                    ''    End If
                    ''Next

                    If qtype = "web.config" And qid = 2 Then
                        Dim tmpdt As DataTable = _GetQueryResultsForAdHoc(qid, qtype)
                        tmp = tmpdt.Rows(0)(column).ToString
                    Else
                        Dim tempColumn() As DataRow
                        tempColumn = dtColumn.Select("ColumnName='" & column & "'")
                        If tempColumn.Length > 0 Then
                            strColName = tempColumn(0)("ColumnName").ToString
                        End If
                    End If
                End If

                If Not strColName = "" Then
                    tmp = drAH(strColName).ToString
                    '' New Code Added By Vijay Ramteke On June 27, 2010 For Mantis Id 19271
                    'tmp = _Format(fmt, tmp)
                    '' New Code Added By Vijay Ramteke On June 27, 2010 For Mantis Id 19271
                    activeContent.Append(tmp)
                ElseIf qtype = "web.config" And qid = 2 Then
                    activeContent.Append(tmp)
                Else
                    If column = "t" Then
                        tmp = _Format(fmt, DateTime.Now)
                        activeContent.Append(tmp)
                    ElseIf column = "d" Then
                        tmp = _Format(fmt, DateTime.Now)
                        activeContent.Append(tmp)
                    End If
                End If
                sb.Replace(spField, activeContent.ToString())
            Catch ex As Exception
                'MSGCommon.DisplayErrorMessage(ex.Message)
            End Try
        End Sub
        ''New Code Added By Vijay Ramteke On May 06, 2010
        ' Method: _Format
        ' Description: Returns a properly formatted string given a format string and the object
        ' to be formatted.  The primary method of formatting is the use of format string
        ' defined in String.Format.  There are specific format string to accomodate Advantage
        ' for things like "phone", "zip" and "ssn"
        Private Shared Function _Format(ByVal fmt As String, ByVal s As Object) As String
            Try
                If fmt = "phone" Then
                    Dim fac As New InputMasksFacade
                    Dim Mask As String = fac.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
                    Return fac.ApplyMask(Mask, s)

                ElseIf fmt = "zip" Then
                    Dim fac As New InputMasksFacade
                    Dim Mask As String = fac.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
                    Return fac.ApplyMask(Mask, s)

                ElseIf fmt = "ssn" Then
                    Dim fac As New InputMasksFacade
                    Dim Mask As String = fac.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
                    Return fac.ApplyMask(Mask, s)

                ElseIf fmt <> "" Then
                    Return String.Format(fmt, s)
                Else
                    Return s.ToString
                End If
            Catch ex As Exception
                'MSGCommon.DisplayErrorMessage("Error in MsgSpecialFields::_Format - error=" & ex.Message)
                Return ""
            End Try
        End Function

        ' Method: _GetQueryResults
        ' Description: Return the DataTable for a particular Query specified in MsgQueries.xml.
        ' A query is identified by a queryid.
        ' A query does not have to be sql.  It could be pulled from the web.config
        Private Shared Function _GetQueryResults(ByVal qid As Integer, ByVal qtype As String, ByVal params As SpecialFields_Params) As DataTable
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                Dim sql As String = _GetQueryString(qid, params)
                ' return nothing if there is no query associated with the qid
                If sql.Length = 0 Then
                    Return Nothing
                End If

                db.OpenConnection()
                Dim ds As DataSet = Nothing

                If qtype Is Nothing Or qtype = "sql" Then
                    ' just do a standard sql query
                    ds = db.RunSQLDataSet(sql)
                ElseIf qtype = "web.config" Then
                    ' specialized query that uses values in web.config
                    Dim dt As New DataTable
                    Dim keys() As String = sql.Split(",")
                    Dim key As String
                    ' create the schema for this table where there is a column name for each key that were interested in
                    For Each key In keys
                        dt.Columns.Add(key)
                    Next
                    ' now create a new row with this table schema
                    dt.Rows.Add(dt.NewRow())
                    For Each key In keys
                        dt.Rows(0)(key) = MyAdvAppSettings.AppSettings(key)
                    Next
                    ds = New DataSet
                    ds.Tables.Add(dt)
                End If

                ' assign the tablename and return the datatable
                ds.Tables(0).TableName = qid.ToString()
                Return ds.Tables(0).Copy()
            Catch ex As Exception
                'MSGCommon.DisplayErrorMessage(ex.Message)
            Finally
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
            End Try
            Return Nothing
        End Function

        ' Method: _GetQueryString
        ' Description: Return a query string from those that are defined within MsgQueries.xml
        ' Active parameters get subbed into the query for the return
        Private Shared Function _GetQueryString(ByVal qid As Integer, ByVal params As SpecialFields_Params) As String
            Try
                Dim xmlFileName As String = HttpContext.Current.Server.MapPath("xml/MsgQueries.xml")
                Dim dsQueries As New DataSet
                dsQueries.ReadXml(xmlFileName)
                Dim drs() As DataRow = dsQueries.Tables(0).Select("qid='" & qid.ToString() & "'")
                Dim dr As DataRow = drs(0)
                Dim sb As New StringBuilder(dr("query").ToString())

                sb.Replace("[CAMPUSID]", params._CampusID)
                sb.Replace("[RID]", params._RecipientId)
                sb.Replace("[REID]", params._ReId)
                sb.Replace("[CHANGEID]", params._ChangeId)
                ' more replace cases to come

                ' just for debuging
                Dim str As String = sb.ToString
                Return sb.ToString
            Catch ex As Exception

            End Try
            Return "" ' Error
        End Function

        ' Method: _GetNextSpecialField
        ' Description: Retrieve the next special field from the string s starting at index si
        ' Special fields have the format of "[special field]"
        Private Shared Function _GetNextSpecialField(ByRef s As String, ByRef si As Integer) As String
            si = s.IndexOf("[", si, StringComparison.Ordinal)
            If si > -1 Then
                ' look for the end of the special field denoted by a "]"
                Dim ei As Integer = s.IndexOf("]", si + 1, StringComparison.Ordinal)
                Dim tsi As Integer = si ' temp variable
                If ei > -1 Then
                    si = ei + 1 ' advance the start index for the search next time around
                    ' note: index is advance to 1 + ei because this is the ending index of the last field
                    ' we have found a special field
                    Return s.Substring(tsi, ei - tsi + 1)
                Else
                    si = s.Length - 1
                    Return String.Empty ' Mal format in special field, return none.
                End If
            End If
            Return String.Empty ' no special field found
        End Function

        ' Method: _LoadSpecialFields
        ' Description: Load the special fields from the xml file into a dataset
        Private Shared Function _LoadSpecialFields() As DataSet
            Dim xmlFileName As String = HttpContext.Current.Server.MapPath("xml/MsgFields.xml")
            Dim ds As New DataSet
            ds.ReadXml(xmlFileName, XmlReadMode.InferSchema)
            Return ds
        End Function
        ''New Fuctions Added By Vijay Ramteke
        Private Shared Function _GetQueryResultsForAdHoc(ByVal qid As Integer, ByVal qtype As String) As DataTable
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                Dim sql As String = _GetQueryStringForAdHoc(qid)
                ' return nothing if there is no query associated with the qid
                If sql.Length = 0 Then
                    Return Nothing
                End If

                db.OpenConnection()
                Dim ds As DataSet = Nothing
                If qtype = "web.config" Then
                    ' specialized query that uses values in web.config
                    Dim dt As New DataTable
                    Dim keys() As String = sql.Split(",")
                    Dim key As String
                    ' create the schema for this table where there is a column name for each key that were interested in
                    For Each key In keys
                        dt.Columns.Add(key)
                    Next
                    ' now create a new row with this table schema
                    dt.Rows.Add(dt.NewRow())
                    For Each key In keys
                        dt.Rows(0)(key) = MyAdvAppSettings.AppSettings(key)
                    Next
                    ds = New DataSet
                    ds.Tables.Add(dt)
                End If

                ' assign the tablename and return the datatable
                ds.Tables(0).TableName = qid.ToString()
                Return ds.Tables(0).Copy()
            Catch ex As Exception
                'MSGCommon.DisplayErrorMessage(ex.Message)
            Finally
                If db.Connection.State = ConnectionState.Open Then db.CloseConnection()
            End Try
            Return Nothing
        End Function
        Private Shared Function _GetQueryStringForAdHoc(ByVal qid As Integer) As String
            Try
                Dim xmlFileName As String = HttpContext.Current.Server.MapPath("xml/MsgQueries.xml")
                Dim dsQueries As New DataSet
                dsQueries.ReadXml(xmlFileName)
                Dim drs() As DataRow = dsQueries.Tables(0).Select("qid='" & qid.ToString() & "'")
                Dim dr As DataRow = drs(0)
                Dim sb As New StringBuilder(dr("query").ToString())
                Dim str As String = sb.ToString
                Return sb.ToString
            Catch ex As Exception

            End Try
            Return "" ' Error
        End Function
        ''New Fuctions Added By Vijay Ramteke 
#End Region
    End Class
#End Region

#Region "Templates"
    Public Class TemplatesFacade
        ''' <summary>
        ''' Method used in maintenance pages
        ''' </summary>
        ''' <param name="CampGroupId"></param>
        ''' <param name="ModuleEntityId"></param>
        ''' <param name="ShowActive"></param>
        ''' <param name="ShowInactive"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetTemplates(ByVal CampGroupId As String, ByVal ModuleEntityId As String, _
                                            ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Return TemplatesDB.GetTemplates(CampGroupId, ModuleEntityId, ShowActive, ShowInactive)
        End Function

        Public Shared Function GetAllForUser(ByVal UserId As String, ByVal CampusId As String, _
                                            ByVal ModuleId As String, ByVal EntityId As String) As DataSet
            Return TemplatesDB.GetAllForUser(UserId, CampusId, ModuleId, EntityId)
        End Function

        Public Shared Function GetAllForEntity(ByVal CampusId As String, ByVal EntityId As String) As DataSet
            Return TemplatesDB.GetAllForEntity(CampusId, EntityId)
        End Function

        Public Shared Function GetTemplateInfo(ByVal TemplateId As String) As TemplateInfo
            Return TemplatesDB.GetInfo(TemplateId)
        End Function

        Public Shared Function UpdateTemplate(ByVal info As TemplateInfo, ByVal user As String) As Boolean
            If info.IsInDB = False Then
                Return TemplatesDB.Add(info, user)
            Else
                Return TemplatesDB.Update(info, user)
            End If
        End Function

        Public Shared Function DeleteTemplate(ByVal TemplateId As String, ByVal modDate As DateTime) As String
            Return TemplatesDB.Delete(TemplateId, modDate)
        End Function
    End Class
#End Region

#Region "Special Fields / Entity to Field Name mappings"
    Public Class EntitiesFieldGroupsFacade
        Public Shared Function GetAll(ByVal EntityId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Return EntitiesFieldGroupsDB.GetAll(EntityId, ShowActive, ShowInactive)
        End Function

        Public Shared Function GetEntities() As DataSet
            Return EntitiesFieldGroupsDB.GetEntities()
        End Function

        Public Shared Function GetEntityId(ByVal ModuleEntityId As String) As String
            Return EntitiesFieldGroupsDB.GetEntityId(ModuleEntityId)
        End Function

        Public Shared Function GetEntityFieldGroupInfo(ByVal EntityFieldGroupId As String) As EntityFieldGroupInfo
            Return EntitiesFieldGroupsDB.GetInfo(EntityFieldGroupId)
        End Function

        Public Shared Function Update(ByVal info As EntityFieldGroupInfo, ByVal user As String) As Integer
            If info.IsInDB = False Then
                Return EntitiesFieldGroupsDB.Add(info, user)
            Else
                Return EntitiesFieldGroupsDB.Update(info, user)
            End If
        End Function

        Public Shared Function Delete(ByVal EntityFieldGroupId As String) As Boolean
            Return EntitiesFieldGroupsDB.Delete(EntityFieldGroupId)
        End Function

        Public Shared Function DeleteAll(ByVal EntityId) As Boolean
            Return EntitiesFieldGroupsDB.DeleteAll(EntityId)
        End Function
    End Class
#End Region

#Region "Input Masks"
    Public Class InputMasksFacade

        Public Enum InputMaskItem
            SSN = 1
            Phone = 2
            Zip = 3
        End Enum

        Public Function GetInputMasks() As DataTable
            Dim dbInputMasks As New InputMasksDB

            Return dbInputMasks.GetInputMasks
        End Function

        Public Function GetInputMaskForItem(ByVal itemId As InputMaskItem) As String
            Dim dbInputMasks As New InputMasksDB

            Return dbInputMasks.GetInputMaskForItem(itemId)
        End Function

        Public Sub UpdateInputMask(ByVal inputMaskId As Integer, ByVal mask As String)
            Dim dbInputMasks As New InputMasksDB

            dbInputMasks.UpdateInputMask(inputMaskId, mask)
        End Sub

        Public Function UpdateInputMaskInfo(ByVal iMaskInfo As InputMaskInfo, ByVal user As String) As String
            Dim dbInputMasks As New InputMasksDB

            Return dbInputMasks.UpdateInputMaskInfo(iMaskInfo, user)
        End Function

        Public Function UpdateInputMaskDS(ByVal ds As DataSet) As String
            Dim dbInputMasks As New InputMasksDB

            Return dbInputMasks.UpdateInputMaskDS(ds)
        End Function

        Public Function ApplyMask(ByVal strMask As String, ByVal strVal As String) As String
            Dim arrMask As New ArrayList
            Dim arrVal As New ArrayList
            Dim chr As Char
            'Dim strMaskChars As String = "#&?AULH\"
            Dim strMaskChars As String = "#"
            Dim intCounter As Integer
            Dim intCounter2 As Integer
            Dim strCorrVal As String
            Dim strReturn As String = ""
            Dim strPrev As String = ""

            'Add each character in strMask to arrMask
            'Modified by Balaji on 2/18/2005 (If statement added)
            If Not strMask = "" Then
                For Each chr In strMask
                    arrMask.Add(chr.ToString)
                Next
            End If

            'Add each character in strVal to arrVal
            For Each chr In strVal
                arrVal.Add(chr)
            Next

            If arrVal.Count >= 1 Then
                'We need to loop through the arrMask ArrayList and see if each item is a
                'mask character. If it is, we can use intCounter to get the corresponding
                'value from the arrVal ArrayList. Note that we ignore the \ character unless
                'it was preceded by another \. This means that at the end if we have \\ it
                'should be replaced by a \ and if we have a \ then it should be replaced
                'with an empty space.
                For intCounter2 = 0 To arrMask.Count - 1
                    If arrMask(intCounter2).ToString() = "\" And strPrev <> "\" Then
                        'ignore
                    ElseIf strMaskChars.IndexOf(arrMask(intCounter2).ToString()) <> -1 Then
                        strCorrVal = arrVal(intCounter).ToString()
                        arrMask(intCounter2) = strCorrVal
                        intCounter += 1
                    End If
                    strPrev = arrMask(intCounter2).ToString()
                Next

                Dim strChars(arrMask.Count) As String
                arrMask.CopyTo(strChars)
                strReturn = String.Join("", strChars)


                If strReturn.IndexOf("\\") <> -1 Then
                    strReturn = strReturn.Replace("\\", "\")

                ElseIf strReturn.IndexOf("\") <> -1 Then
                    strReturn = strReturn.Replace("\", "")
                Else
                    Return strReturn
                End If

                Return strReturn
            Else
                Return ""
            End If
        End Function

        Public Function RemoveMask(ByVal strMask As String, ByVal strVal As String) As String
            Dim arrMask As New ArrayList
            Dim arrVal As New ArrayList
            Dim arrRemovedVals As New ArrayList
            Dim chr As Char
            'Dim strMaskChars As String = "#&?A"
            Dim strMaskChars As String = "#"
            Dim intCounter As Integer
            Dim strCorrVal As String

            'Add each character in strMask to arrMask
            For Each chr In strMask
                arrMask.Add(chr.ToString)
            Next

            'Add each character in strVal to arrVal
            For Each chr In strVal
                arrVal.Add(chr)
            Next

            'We need to loop through the arrMask ArrayList and see if each item is a
            'mask character. If it is, we can use intCounter to get the corresponding
            'value from the arrVal ArrayList. 
            For intCounter = 0 To arrMask.Count - 1
                If strMaskChars.IndexOf(arrMask(intCounter).ToString()) <> -1 Then
                    strCorrVal = arrVal(intCounter).ToString()
                    arrRemovedVals.Add(strCorrVal)
                End If

            Next
            Dim strChars(arrRemovedVals.Count) As String
            arrRemovedVals.CopyTo(strChars)
            Return String.Join("", strChars)
        End Function

        Public Function GetRegEx(ByVal strMask As String) As String

            Dim arrMask As New ArrayList
            Dim chr As Char
            'Dim strMaskChars As String = "#&?A"
            Dim strMaskChars As String = "#"
            Dim intCounter As Integer

            'Add each character in strMask to arrMask
            For Each chr In strMask
                arrMask.Add(chr.ToString)
            Next

            'We need to loop through the arrMask ArrayList and see if each item is a
            'mask character. If it is we will replace it with the equivalent
            'RegularExpressionValidator character. If it is not then it is a literal and we need
            'to escape it with a \. For example if we have (305)-789-9500 then the first character
            'should be recorded as \(.
            For intCounter = 0 To arrMask.Count - 1
                If strMaskChars.IndexOf(arrMask(intCounter).ToString()) <> -1 Then
                    arrMask(intCounter) = GetValidationChar(arrMask(intCounter).ToString())
                Else
                    arrMask(intCounter) = "\" & arrMask(intCounter)
                End If
            Next
            Dim strChars(arrMask.Count) As String
            arrMask.CopyTo(strChars)
            Return String.Join("", strChars)
        End Function

        Private Function GetValidationChar(ByVal chr As Char) As String

            Select Case chr
                Case "#"
                    Return "\d"
                    'Case "&"
                    'Return "\s"
                    'Case "?"
                    'Return "\D"
                    'Case "A"
                    'Return "\w"
            End Select
            Return Nothing
        End Function

        Public Function ValidateStringWithInputMask(ByVal strMask As String, ByVal strVal As String) As Boolean
            Dim objRegEx As Regex
            Dim regularExpression As String

            'If the mask and the string value are not the same length then we should return false.
            'This check is important because the IsMatch method of the Regex class only searches the
            'input string for a match. This means we could have a regular expression such as "\d{5}
            'but a string such "123456" will still return true because the pattern of five (5) 
            'consecutive digits is within the string.
            If strMask.Length <> strVal.Length Then
                Return False
            End If

            regularExpression = GetRegEx(strMask)
            objRegEx = New Regex(regularExpression)
            If objRegEx.IsMatch(strVal) Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function CanZipMaskBeChanged(ByVal newZipMask As String) As Boolean
            'The zip mask cannot be changed if there are existing records (none international) that have
            'a different number of digits from the proposed new mask. For example, if there are existing records
            'with 5 digits then we should not allow the mask to be changed to 10 digits.
            Dim dbInputMasks As New InputMasksDB
            Dim zipLenToCheck As Integer
            Dim numFound As Integer
            Dim numPoundSymbol As Integer

            'Find out how many # symbols are in the zip mask passed in
            numPoundSymbol = GetNumberOfOccurencesOfCharInString(newZipMask, "#")
            If numPoundSymbol = 5 Then
                zipLenToCheck = 10
            ElseIf numPoundSymbol = 10 Then
                zipLenToCheck = 5
            End If

            'We are going to check each table that uses the zip field to see if there are any possible conflict.
            'As soon as we find a table that has a potential problem we will return false

            'adLeads
            numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("adLeads", zipLenToCheck)
            If numFound > 0 Then Return False

            'adColleges
            'numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("adColleges", zipLenToCheck)
            'If numFound > 0 Then Return False

            'syInstitutions
            'numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("syInstitutions", zipLenToCheck)
            'If numFound > 0 Then Return False

            'arStudAddresses
            numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("arStudAddresses", zipLenToCheck)
            If numFound > 0 Then Return False

            'plEmployers
            numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("plEmployers", zipLenToCheck)
            If numFound > 0 Then Return False

            'plEmployerContact
            numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("plEmployerContact", zipLenToCheck)
            If numFound > 0 Then Return False

            'faLenders
            numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("faLenders", zipLenToCheck)
            If numFound > 0 Then Return False


            'If we get to this point then we can return true
            Return True

        End Function

        Public Function GetNumberOfOccurencesOfCharInString(ByVal stringToCheck As String, ByVal charToCheckFor As Char) As Integer
            Dim intCounter As Integer
            Dim arrCharsToCheck As New ArrayList
            Dim chr As Char
            Dim numFound As Integer

            For Each chr In stringToCheck
                arrCharsToCheck.Add(chr.ToString)
            Next

            numFound = 0
            For intCounter = 0 To arrCharsToCheck.Count - 1
                If arrCharsToCheck(intCounter) = charToCheckFor Then
                    numFound += 1
                End If
            Next

            Return numFound

        End Function
    End Class
#End Region

#Region "Mail"
    Public Enum MailFormat
        Text
        Html
    End Enum

    Public Enum MailPriority
        Normal
        Low
        High
    End Enum


    Public Class Mail

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        ''' <summary>Send a simple email.</summary>
        ''' </summary>
        ''' <param name="MailFrom"></param>
        ''' <param name="MailTo"></param>
        ''' <param name="Bcc"></param>
        ''' <param name="Subject"></param>
        ''' <param name="Body"></param>
        ''' <param name="Attachment"></param>
        ''' <param name="BodyType"></param>
        ''' <param name="SMTPServer"></param>
        ''' <param name="SMTPAuthentication"></param>
        ''' <param name="SMTPUsername"></param>
        ''' <param name="SMTPPassword"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' <history>
        '''     [cnurse]        09/29/2005  Moved to Mail class
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Shared Function SendMail(ByVal mailFrom As String, ByVal mailTo As String, ByVal Bcc As String, ByVal subject As String, ByVal body As String, ByVal Attachment As String, ByVal bodyType As String, ByVal SMTPServer As String, ByVal SMTPAuthentication As String, ByVal SMTPUsername As String, ByVal SMTPPassword As String) As String

            Dim smtp = New SmtpSendMessageInputModel()
            smtp.From = mailFrom
            Dim bytebody = Encoding.UTF8.GetBytes(body)
            smtp.Body64 = Convert.ToBase64String(bytebody)
            smtp.Command = 1
            smtp.ContentType = "text/html"
            smtp.Subject = subject
            smtp.To = mailTo
            smtp.ContentType = If(bodyType = "html", "text/html", "text")

            Dim result As String = SendEmailService(smtp)
            Return result

            'Dim message As MailMessage = BuildEmailMessage(mailFrom, mailTo, subject, body, Nothing)
            'Return SendEmailMessage(message)

        End Function

        ''' <summary>
        ''' Send a Email using the Email Service. The type of email is selected based in the flag OAuth
        ''' in the Application Setting. This is resolved in the service itself.
        ''' </summary>
        ''' <param name="smtpObject">
        ''' Configuration parameters.
        ''' </param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function SendEmailService(smtpObject As SmtpSendMessageInputModel) As String

            Dim settings = AdvAppSettings.GetAppSettings()

            'Get Uri Service
            Dim url As String = settings.AppSettings("AdvantageServiceUri").ToString()
            url = url + "api/SystemStuff/Maintenance/SmtpSendEmail/Post"
            Dim apikey As String = settings.AppSettings("AdvantageServiceAuthenticationKey").ToString()

            'Call Email service...............
            Dim client = New HttpClient()
            client.DefaultRequestHeaders.Accept.Clear()

            client.DefaultRequestHeaders.Add("AuthKey", apikey)
            Dim result As IGenericOutput = GenericOutput.Factory()

            Dim task = client.PostAsJsonAsync(url, smtpObject).ContinueWith(Sub(taskwithmsg)
                                                                                Dim res = taskwithmsg
                                                                                If res.Result.StatusCode = HttpStatusCode.OK Then
                                                                                    Dim resul1 = res.Result.Content.ReadAsAsync(Of GenericOutput)().Result
                                                                                    result.Note = resul1.Note
                                                                                    result.IsPassed = 1
                                                                                Else
                                                                                    result.Note = res.Result.ReasonPhrase
                                                                                    result.IsPassed = 0
                                                                                End If
                                                                            End Sub)
            task.Wait()
            
            'Return Results.......................................
            Return If(result.IsPassed = 1, String.Empty, result.Note)
        End Function




        ''' -----------------------------------------------------------------------------
        ' ''' <summary>Send a simple email.</summary>
        ' ''' <history>
        ' ''' 	[Nik Kalyani]	10/15/2004	Replaced brackets in member names
        ' '''     [cnurse]        09/29/2005  Moved to Mail class
        ' ''' </history>
        ' ''' -----------------------------------------------------------------------------
        'Public Shared Function SendMail(ByVal MailFrom As String, ByVal MailTo As String, _
        '    ByVal Cc As String, ByVal Bcc As String, ByVal Priority As MailPriority, _
        '    ByVal Subject As String, ByVal BodyFormat As MailFormat, _
        '    ByVal BodyEncoding As Encoding, ByVal Body As String, _
        '    ByVal Attachment As String, ByVal SMTPServer As String, ByVal SMTPAuthentication As String, _
        '    ByVal SMTPUsername As String, ByVal SMTPPassword As String) As String

        '    Dim objMail As New MailMessage(MailFrom, MailTo)
        '    If Cc <> "" Then
        '        objMail.CC.Add(Cc)
        '    End If
        '    If Bcc <> "" Then
        '        objMail.Bcc.Add(Bcc)
        '    End If
        '    objMail.Priority = CType(Priority, System.Net.Mail.MailPriority)
        '    objMail.IsBodyHtml = CBool(IIf(BodyFormat = MailFormat.Html, True, False))

        '    If Attachment <> "" Then
        '        objMail.Attachments.Add(New Attachment(Attachment))
        '    End If

        '    ' message
        '    objMail.Subject = Subject
        '    objMail.BodyEncoding = BodyEncoding
        '    objMail.Body = Body
        '    'US3519: set to HTML
        '    objMail.IsBodyHtml = True
        '    ' external SMTP server
        '    Dim smtpClient As New SmtpClient()
        '    If SMTPServer <> "" Then
        '        smtpClient.Host = SMTPServer
        '        Select Case SMTPAuthentication
        '            Case "", "0" ' anonymous
        '            Case "1" ' basic
        '                If SMTPUsername <> "" And SMTPPassword <> "" Then
        '                    smtpClient.Credentials = New NetworkCredential(SMTPUsername, SMTPPassword)
        '                End If
        '            Case "2" ' NTLM
        '                smtpClient.UseDefaultCredentials = True
        '        End Select
        '    End If
        '    Try
        '        smtpClient.Send(objMail)
        '        SendMail = ""
        '    Catch objException As Exception
        '        ' mail configuration problem
        '        SendMail = objException.Message
        '    End Try
        'End Function

        ''' <summary>
        ''' From Advantage
        ''' </summary>
        ''' <param name="fromEmailAddress"></param>
        ''' <param name="toEmailAddress"></param>
        ''' <param name="subject"></param>
        ''' <param name="body"></param>
        ''' <param name="attachments"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        'Public Shared Function BuildEmailMessage(ByVal fromEmailAddress As String, ByVal toEmailAddress As String, ByRef subject As String, ByRef body As String, ByVal attachments As ArrayList) As MailMessage

        '    Dim insMail As New MailMessage(New MailAddress(fromEmailAddress), New MailAddress(toEmailAddress))
        '    insMail.Priority = MailPriority.High
        '    'insMail.IsBodyHtml = False
        '    'US3519: set HTML 
        '    insMail.IsBodyHtml = True
        '    With insMail
        '        .Subject = subject
        '        .Body = body
        '        '.CC.Add(New MailAddress(strCC))
        '        If Not attachments Is Nothing Then
        '            For Each obj As Object In attachments
        '                .Attachments.Add(obj)
        '            Next
        '        End If
        '    End With
        '    Return insMail
        'End Function
        ' ''' <summary>
        ' ''' From Advantage
        ' ''' </summary>
        ' ''' <param name="emailMessage"></param>
        ' ''' <returns></returns>
        ' ''' <remarks></remarks>
        'Public Shared Function SendEmailMessage(ByRef emailMessage As MailMessage) As String

        '    Dim MyAdvAppSettings As AdvAppSettings
        '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        '    Else
        '        MyAdvAppSettings = New AdvAppSettings
        '    End If

        '    Try
        '        Dim smtp As New SmtpClient
        '        'Comment US3519
        '        smtp.Port = MyAdvAppSettings.AppSettings("FameSMTPPort")
        '        smtp.Host = MyAdvAppSettings.AppSettings("FameSMTPServer")

        '        'US3519: check for extra settings 
        '        If Not IsNothing(MyAdvAppSettings.AppSettings("FameSMTPUsername")) AndAlso Not IsNothing(MyAdvAppSettings.AppSettings("FameSMTPPassword")) Then
        '            smtp.Credentials = New NetworkCredential(MyAdvAppSettings.AppSettings("FameSMTPUsername").ToString, MyAdvAppSettings.AppSettings("FameSMTPPassword").ToString)
        '        End If

        '        If Not IsNothing(MyAdvAppSettings.AppSettings("FameSMTPSSL")) Then
        '            If UCase(MyAdvAppSettings.AppSettings("FameSMTPSSL")) = "YES" Then
        '                smtp.EnableSsl = True
        '            Else
        '                smtp.EnableSsl = False
        '            End If
        '        Else
        '            smtp.EnableSsl = False
        '        End If

        '        'smtp.UseDefaultCredentials = False
        '        'smtp.Credentials = New System.Net.NetworkCredential("donotreply@fameinc.com", "Fame$6451")
        '        smtp.Send(emailMessage)
        '    Catch e As Exception
        '        Return "SMTP Server is not configured properly or SMTP " + vbCrLf + "Server: " + MyAdvAppSettings.AppSettings("SMTPServer") + " is not allowing relay of Emails." + vbCrLf + "No Email(s) sent."

        '    End Try

        '    Return ""
        'End Function
    End Class
#End Region

End Namespace
