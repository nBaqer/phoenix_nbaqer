' ===============================================================================
' tmCommon.vb
' Description: Defines functions that are common throughout Task Manager.
'   The goal of this class is to make it easier to integrate with the rest of 
'   Advantage.
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
'Imports FAME.AdvantageV1.DataAccess
Imports FAME.AdvantageV1.DataAccess.TM
Imports FAME.AdvantageV1.Common.TM
Imports FAME.Advantage.Common

Namespace TM
    Public Class TMCommon
#Region "Advantage Specific integration"
        ''' <summary>
        ''' Starts the Task Manager client window.
        ''' </summary>
        ''' <param name="p"></param>
        ''' <param name="target"></param>
        ''' <param name="UserId"></param>
        ''' <param name="CampusId"></param>
        ''' <remarks></remarks>
        Public Shared Sub OpenTM(ByRef p As Page, ByVal target As String, ByVal UserId As String, ByVal CampusId As String)
            If target Is Nothing Or target = "" Then
                target = "_self"
            End If

            p.Session("UserId") = UserId
            p.Session("cmpid") = CampusId
            Dim js As New StringBuilder
            js.Append("<script type=""text/javascript"">" & vbCrLf)
            js.Append("     window.open('" + p.Request.ApplicationPath + "/DefaultTM.html','" + target + "');")
            js.Append("</script>")
            p.ClientScript.RegisterStartupScript(p.GetType(), "openscript", js.ToString())
        End Sub

        ''' <summary>
        ''' Initalizes parameters that used throughout TM
        ''' These parameters are stored in session variables which include
        ''' Session("userid") and Session("campusid")
        ''' </summary>        
        ''' <remarks></remarks>
        Public Shared Sub AdvInit(ByVal pg As Page)
            ' setup params needed throughout 
            If HttpContext.Current.Session("UserId") Is Nothing Then
                HttpContext.Current.Session("UserId") = HttpContext.Current.Request.Params("UserId")
            End If
            If HttpContext.Current.Session("UserName") Is Nothing Then
                HttpContext.Current.Session("UserName") = GetUserNameFromUserId(HttpContext.Current.Session("UserId"))
            End If
            If HttpContext.Current.Session("cmpid") Is Nothing Then
                HttpContext.Current.Session("cmpid") = HttpContext.Current.Request.Params("cmpid")
            End If
            If HttpContext.Current.Session("resid") Is Nothing Then
                HttpContext.Current.Session("resid") = HttpContext.Current.Request.Params("resid")
            End If

            ' display an error if the session has timed out
            If GetCurrentUserId() Is Nothing Then
                HttpContext.Current.Session("Error") = "Session has expired"
                Dim url As String = HttpContext.Current.Server.MapPath("ErrorPage.aspx")
                HttpContext.Current.Response.Redirect(url)
            End If

            SetBrowserTitle(pg, "Advantage Task Manager")
        End Sub

        Public Shared Function GetCurrentUserId() As String
            If Not HttpContext.Current.Request.Params("UserId") Is Nothing Then
                Return HttpContext.Current.Request.Params("UserId")
            End If
            Return HttpContext.Current.Session("UserId")
        End Function

        Public Shared Function GetCampusID() As String
            If Not HttpContext.Current.Request.Params("cmpid") Is Nothing Then
                Return HttpContext.Current.Request.Params("cmpid")
            End If
            Return HttpContext.Current.Session("cmpid")
        End Function

        Public Shared Function GetAdvAdminEmail() As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Return MyAdvAppSettings.AppSettings("FromEmailAddress")
        End Function

        ''' <summary>
        ''' Get a fullname from a userid
        ''' </summary>
        ''' <param name="UserId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetUserNameFromUserId(ByVal UserId As String) As String
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' check for a blank userid
            If UserId Is Nothing Or UserId = "" Then Return ""
            ' format select sql
            Dim sql As String = "select FullName from syUsers where UserId='" + UserId + "'"
            Dim name As String = ""
            Try
                name = db.RunParamSQLScalar(sql)
            Catch ex As System.Exception
            End Try
            Return name
        End Function
#End Region
#Region "Debugging and error"
        Public Shared Sub DisplayErrorMessage(ByVal pg As Page, ByVal msg As String)
            System.Diagnostics.Debug.WriteLine(msg)
            TMCommon.Alert(pg, msg)
        End Sub
#End Region
#Region "Browser Javascript Methods"
        Public Shared Sub SetBrowserStatusBar(ByRef p As Page, ByVal strStatus As String)
            Dim js As New StringBuilder
            js.Append("<script type=""text/javascript"">" & vbCrLf)
            js.Append("window.status=""" & strStatus)
            js.Append(""" " & vbCrLf)
            js.Append("</script>")
            p.ClientScript.RegisterStartupScript(p.GetType(), "jsStatus", js.ToString())
        End Sub

        Public Shared Sub SetBrowserTitle(ByRef p As Page, ByVal strStatus As String)
            Dim js As New StringBuilder
            js.Append("<script type=""text/javascript"">" & vbCrLf)
            js.Append("parent.document.title=""" & strStatus)
            js.Append(""" " & vbCrLf)
            js.Append("</script>")
            p.ClientScript.RegisterStartupScript(p.GetType(), "jsStatus", js.ToString())
        End Sub

        Public Shared Function GetJavsScriptPopup(ByVal popupUrl As String, _
                                                  ByVal width As Integer, _
                                                  ByVal height As Integer, _
                                                  ByVal retControlId As String, _
                                                  Optional ByVal showToolbar As Boolean = True) As String
            Dim js As New StringBuilder
            js.Append("var strReturn; " & vbCrLf)
            js.Append("strReturn=window.showModalDialog('")
            js.Append(popupUrl)
            js.Append("','Dialog Box Arguments # 1','resizable:no;status:no;dialogWidth:")
            js.Append(width.ToString())
            js.Append("px;dialogHeight:")
            js.Append(height.ToString())
            js.Append("px;dialogHide:true;help:no;scroll:yes;center:yes'); " & vbCrLf)
            If Not retControlId Is Nothing Then
                If retControlId.Length <> 0 Then
                    js.Append("if (strReturn != null) " & vbCrLf)
                    js.Append("     document.getElementById('")
                    js.Append(retControlId)
                    js.Append("').value=strReturn;" & vbCrLf)
                End If
            End If
            Return js.ToString()
        End Function

        Public Shared Sub Alert(ByRef p As Page, ByVal msg As String)
            p.ClientScript.RegisterStartupScript(p.GetType(), "sendJS", "<script language='javascript'>window.alert('" & msg.Replace("'", "") & "')</script>")
        End Sub

        Public Shared Function Confirm(ByVal Page As System.Web.UI.Page, ByVal Msg As String) As Boolean
            Dim st As String = "<script>var ret=confirm('" + Msg + "');</script>"
            Page.Response.Write(st)
        End Function

#End Region
#Region "Referrer URL Methods"
#End Region
#Region "Build DDL Methods"
        Public Shared Sub BuildStatusDDL(ByVal ddl As DropDownList)
            ddl.Items.Clear()
            ddl.Items.Add(New ListItem("--Select--", "true"))
            ddl.Items.Add(New ListItem("Active", "True"))
            ddl.Items.Add(New ListItem("Inactive", "False"))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildContactTypeDDL(ByRef ddl As DropDownList)
            ddl.Items.Clear()
            ddl.Items.Add("--Select--")
            ddl.Items.Add("Student")
            ddl.Items.Add("Lead")
            ddl.Items.Add("Lender")
            ddl.Items.Add("Employees")
            ddl.Items.Add("Employers")
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildHoursDDL(ByVal ddl As DropDownList)
            ddl.Items.Clear()
            Dim i As Integer
            For i = 1 To 12
                ddl.Items.Add(i.ToString())
            Next
        End Sub

        Public Shared Sub BuildCategoryDDL(ByVal campusID As String, ByRef ddl As DropDownList)
            ddl.DataSource = CategoriesFacade.GetCategories(campusID, True, False)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "CategoryId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildCampusGroups(ByVal ddl As DropDownList)
            ddl.DataSource = CampGroupsDB.GetCampusGroups()
            ddl.DataTextField = "CampGrpDescrip"
            ddl.DataValueField = "CampGrpId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            If ddl.Items.Count > 1 Then
                ddl.SelectedIndex = 1
            Else
                ddl.SelectedIndex = 0
            End If
        End Sub

        Public Shared Sub BuildCampuses(ByVal CampGroupId As String, ByVal ddl As DropDownList)
            ddl.DataSource = CampGroupsDB.GetCampuses(CampGroupId)
            ddl.DataTextField = "CampDescrip"
            ddl.DataValueField = "CampusId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildReTypeDDL(ByRef ddl As DropDownList)
            ddl.Items.Add(New ListItem("--Select--", ""))
            ddl.Items.Add("Student")
            ddl.Items.Add("Lead")
            ddl.Items.Add("Lender")
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildModulesDDL(ByVal UserId As String, ByRef ddl As DropDownList)
            ddl.DataSource = ModulesDB.GetModules(UserId)
            ddl.DataTextField = "ModuleName"
            ddl.DataValueField = "ModuleId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ' ddl.Items.Insert(1, New ListItem("--All--", ""))
            ddl.SelectedIndex = 0
        End Sub
        Public Shared Sub BuildModuleEntitiesDDL(ByVal ModuleId As String, ByRef ddl As DropDownList)
            ddl.DataSource = ModulesDB.GetModuleEntities(ModuleId)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ModuleEntityId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub
        Public Shared Sub BuildEntityStatusDDL(ByVal EntityID As String, ByRef ddl As DropDownList, Optional ByVal campusId As String = "")
            ddl.DataSource = ModulesDB.GetEntityStatus(EntityID, campusId)
            ddl.DataTextField = "StatusCodeDescrip"
            ddl.DataValueField = "StatusCodeId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub
        Public Shared Sub BuildAvialableResultsLB(ByRef lb As ListBox)
            lb.DataSource = ResultsFacade.GetResults(True, False)
            lb.DataTextField = "Descrip"
            lb.DataValueField = "ResultId"
            lb.DataBind()
        End Sub
        Public Shared Sub BuildAvialableResultsDDL(ByRef ddl As DropDownList)
            ddl.DataSource = ResultsFacade.GetResults(True, False)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ResultId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildAvialableResultsDDL(ByVal TaskId As String, ByRef ddl As DropDownList)
            ddl.DataSource = TaskResultsFacade.GetTaskResults(TaskId)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ResultId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildTasksForUserDDL(ByVal UserId As String, ByVal CampusId As String, _
                                                ByVal ModuleId As String, ByRef ddl As DropDownList)
            ddl.DataSource = TasksFacade.GetTasksForUser(UserId, CampusId, ModuleId)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "TaskId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildAvialableTasksDDL(ByVal CampusId As String, ByVal ModuleId As String, _
                                                 ByVal EntityId As String, ByRef ddl As DropDownList)
            ddl.DataSource = TasksFacade.GetTasks(CampusId, ModuleId, EntityId, True, False)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "TaskId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildAvialableTasksLB(ByVal CampGroupId As String, ByVal ModuleId As String, _
                                                ByVal EntityId As String, ByRef lb As ListBox)
            lb.DataSource = TasksFacade.GetTasks(CampGroupId, ModuleId, EntityId, True, False)
            lb.DataTextField = "Descrip"
            lb.DataValueField = "TaskId"
            lb.DataBind()
        End Sub

        Public Shared Sub BuildPriorityDLL(ByRef ddl As DropDownList)
            ddl.Items.Clear()
            ddl.Items.Add(New ListItem("Normal", 0))
            ddl.Items.Add(New ListItem("High", 1))
            ddl.Items.Add(New ListItem("Low", 2))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildTaskStatusDDL(ByRef ddl As DropDownList)
            ddl.Items.Clear()
            ddl.Items.Add(New ListItem("All", TaskStatus.None))
            ddl.Items.Add(New ListItem("Pending", TaskStatus.Pending))
            ddl.Items.Add(New ListItem("Completed", TaskStatus.Completed))
            ddl.Items.Add(New ListItem("Cancelled", TaskStatus.Cancelled))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildAssignedToDDL(ByVal UserId As String, ByVal CampusId As String, ByRef ddl As DropDownList)
            ddl.DataSource = PermissionsFacade.GetAuthorizedAssignedToUsers(UserId, CampusId)
            ddl.DataTextField = "FullName"
            ddl.DataValueField = "UserId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("Me", UserId))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildResultActionsDDL(ByRef ddl As DropDownList)
            ddl.DataSource = ResultsDB.GetResultActions()
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ResultActionId"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildResultValuesDDL(ByVal ResultActionId As String, ByRef ddl As DropDownList)
            ddl.DataSource = ResultsDB.GetResultActionValues(ResultActionId)
            ddl.DataTextField = "DESCRIP"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select--", ""))
            ddl.SelectedIndex = 0
        End Sub
#End Region
#Region "Utilities"
        Public Shared Function GetContactLookupJS(ByVal ctlReturn As String, _
                                                  ByVal ctlReType As String, _
                                                  ByVal ctlReId As String, _
                                                  ByVal ReTypeEnable As Boolean) As String
            Dim js As New StringBuilder
            js.Append("var opt='resizable:no;status:no;dialogWidth:500px;dialogHeight:500px;dialogHide:true;help:no;scroll:yes';" + vbCrLf)
            js.Append("var retype=document.getElementById('" + ctlReType + "').value;" + vbCrLf)
            js.Append("var reid=document.getElementById('" + ctlReturn + "').value;" + vbCrLf)
            js.Append("var url='ContactLookup.aspx';" + vbCrLf)
            js.Append("var p1='?enableretype=" + ReTypeEnable.ToString() + "';" + vbCrLf)
            js.Append("var p2='&initretype=' + retype; " + vbCrLf)
            js.Append("var p3='&initreid=' + reid; " + vbCrLf)
            js.Append("var curl=url + p1 + p2 + p3; " + vbCrLf)
            js.Append("var strValue = window.showModalDialog(curl,null,opt);" & vbCrLf)
            js.Append("var strText, strId;" & vbCrLf)
            js.Append("if(strValue != null) {" & vbCrLf)
            js.Append("     var iPosition = strValue.indexOf('|'); " & vbCrLf)
            js.Append("     if (iPosition > 0) {" & vbCrLf)
            js.Append("         strText = strValue.substring(0,iPosition);" & vbCrLf)
            js.Append("         strId=strValue.substr(iPosition+1);" & vbCrLf)
            js.Append("         document.getElementById('")
            js.Append(ctlReturn)
            js.Append("').value = strText; " & vbCrLf)
            js.Append("         document.getElementById('")
            js.Append(ctlReId)
            js.Append("').value = strId; " & vbCrLf)
            js.Append("         iPosition=strText.indexOf(':');" & vbCrLf)
            js.Append("     }" & vbCrLf)
            js.Append("}" & vbCrLf)
            Return js.ToString
        End Function

        ' Method: ApplySSNMaskToDS
        ' Description: Apply the SSN mask to the SSN column of the dataset passed in
        Public Shared Sub ApplySSNMaskToDS(ByRef ds As DataSet)
            ' Apply the masks to all the SSNs
            Dim fac As New InputMasksFacade
            Dim ssnMask As String = fac.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
            Dim strMask As String
            'Get the mask for phone numbers 
            strMask = fac.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

            Dim dr As DataRow
            For Each dr In ds.Tables(0).Rows
                Try
                    If Not Information.IsDBNull(dr("SSN")) Then
                        Dim temp As String = dr("SSN")
                        dr("SSN") = fac.ApplyMask(ssnMask, "*****" & temp.Substring(5))
                    End If
                    If Not dr("Phone") Is System.DBNull.Value Then
                        Dim strPhone As String
                        Try
                            strPhone = fac.ApplyMask(strMask, dr("Phone"))
                        Catch ex As Exception
                            strPhone = dr("Phone")
                        End Try

                        dr("Name") = dr("Name") & " - " & strPhone
                        If Not dr("Email") Is System.DBNull.Value Then
                            dr("Name") = dr("Name") & " - " & dr("Email")
                        End If
                    Else
                        If Not dr("Email") Is System.DBNull.Value Then
                            dr("Name") = dr("Name") & " - " & dr("Email")
                        End If
                    End If
                Catch ex As System.Exception
                End Try
            Next
        End Sub
        Public Shared Sub ApplyPhoneMaskToDS(ByRef ds As DataSet)
            ' Apply the masks to all the SSNs
            Dim fac As New InputMasksFacade
            Dim ssnMask As String = fac.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
            Dim strMask As String
            'Get the mask for phone numbers 
            strMask = fac.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

            Dim dr As DataRow
            For Each dr In ds.Tables(0).Rows
                Try
                    If Not dr("Phone") Is System.DBNull.Value Then
                        Dim strPhone As String
                        Try
                            strPhone = fac.ApplyMask(strMask, dr("Phone"))
                        Catch ex As Exception
                            strPhone = dr("Phone")
                        End Try

                        dr("ReName") = dr("ReName") & " - " & strPhone
                        If Not dr("Email") Is System.DBNull.Value Then
                            dr("ReName") = dr("ReName") & " - " & dr("Email")
                        End If
                    Else
                        If Not dr("Email") Is System.DBNull.Value Then
                            dr("ReName") = dr("ReName") & " - " & dr("Email")
                        End If
                    End If
                Catch ex As System.Exception
                End Try
            Next
        End Sub
#End Region
    End Class

End Namespace
