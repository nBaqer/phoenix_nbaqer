
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports FAME.AdvantageV1.Common.AR
Imports FAME.Advantage.Common
Imports System.Data.Linq
Public Class atClsSectAttedance
    Inherits BaseReportFacade

#Region "Public Methods"
    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim ClsSectAttedance As New atClsSectAttedanceDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by report.
        'GetclassSectionAttendance

        Dim arr() As String
        arr = rptParamInfo.FilterOther.Split("'")
        Dim meetDate As String = arr(3)
        Dim meetstartdate As String = ""
        If Not arr(1).Contains("1/1/1900") Then
            meetstartdate = arr(1)
        End If
        ds = BuildReportSource(ClsSectAttedance.GetclassSectionAttendance(rptParamInfo), ClsSectAttedance.StudentIdentifier, rptParamInfo.FilterOtherString.Trim, meetDate, rptParamInfo.CampusId, rptParamInfo.ResId, meetstartdate)

        'ds = ClsSectAttedance.GetclassSectionAttendance(rptParamInfo)

        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String, ByVal Otherwhere As String, ByVal meetDate As String, campusiD As String, Optional ResId As Integer = 0, Optional ByVal meetstartdate As String = "") As DataSet
        Dim stuName As String
        Dim row As DataRow
        Dim temp As String
        Dim attendanceType As String = ""
        Dim atClsSectionAttendance As New atClsSectAttedanceDB
        Dim facInputMasks As New InputMasksFacade
        Dim position As Integer
        Dim substring As String
        Dim substring2 As String
        Dim decimalVal As Decimal = 0.0
        Dim decimalVal2 As Decimal = 0.0
        Dim rowNew As DataRow
        Dim strSplit() As String
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        Dim br As New AttendancePercentageBR

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim ScheduledHours As Decimal = 0.0
        Dim CompletedHours As Decimal = 0.0
        Dim AbsentHours As Decimal = 0.0
        Dim Attendance As Decimal = 0.0
        'GetScheduledHoursForStudent
        Dim ds1 As New DataSet
        Dim dt As New DataTable("dtStudents")
        dt.Columns.Add(New DataColumn("StuEnrollId", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StudentIdentifier", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("ScheduledHours", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("CompletedHours", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("AbsentHours", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("Attendance", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("CampDesrip", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("PrgVerDescrip", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("CohortStartDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SuppressDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MakingSAP", System.Type.GetType("System.String")))
        Try
            If Not IsNothing(ds) Then
                If ds.Tables.Count > 0 Then
                    Dim dtEnroll As DataTable = ds.Tables(0)

                    For Each row In dtEnroll.Rows
                        'Apply mask to SSN.
                        ScheduledHours = 0.0
                        CompletedHours = 0.0
                        AbsentHours = 0.0
                        Attendance = 0.0

                        If MyAdvAppSettings.AppSettings("TrackSapAttendance", campusiD).ToLower = "byclass" Then
                            Dim attInfo As Common.AttendancePercentageInfo
                            If ResId = 554 Then
                                attendanceType = (New ClsSectAttendanceDB).GetPrgVersionAttendanceType(row("StuEnrollId").ToString, False)
                                If MyAdvAppSettings.AppSettings("UseImportedAttendance", campusiD).ToLower = "true" And Not IsNothing(attendanceType) AndAlso attendanceType.ToUpper = "CLOCK HOURS" Then
                                    attInfo = br.GetImportedAttendanceBetweenDateRange(row("StuEnrollId").ToString, Date.Parse(meetDate), meetstartdate)
                                    ScheduledHours = Math.Round((attInfo.TotalScheduled) / 60, 2)
                                    CompletedHours = Math.Round((attInfo.TotalPresent) / 60, 2)
                                    AbsentHours = Math.Round((attInfo.TotalAbsent) / 60, 2)
                                Else
                                    attInfo = br.GetPercentageAttendanceInfoForPresentAbsentHoursReport(row("StuEnrollId").ToString, Date.Parse(meetDate), meetstartdate)

                                    ScheduledHours = Math.Round((attInfo.TotalScheduled) / 60, 2)
                                    CompletedHours = Math.Round((attInfo.TotalScheduled - attInfo.TotalAbsentAdjusted) / 60, 2)
                                    AbsentHours = Math.Round((attInfo.TotalAbsentAdjusted) / 60, 2)
                                End If

                            Else
                                attInfo = br.GetPercentageAttendanceInfoForReport(row("StuEnrollId").ToString, Date.Parse(meetDate))
                                ScheduledHours = Math.Round((attInfo.TotalScheduled) / 60, 2)
                                CompletedHours = Math.Round((attInfo.TotalPresentAdjusted) / 60, 2)
                                AbsentHours = Math.Round((attInfo.TotalAbsentAdjusted) / 60, 2)
                            End If


                        Else
                            Dim attInfo As ClockHourAttendanceInfo = (New AttendanceFacade).GetAttendanceSummaryForDateRange(row("StuEnrollId").ToString,meetstartdate, Date.Parse(meetDate))
                            ScheduledHours = attInfo.TotalHoursSched
                            CompletedHours = attInfo.TotalHoursPresent
                            AbsentHours = attInfo.TotalHoursAbsent
                        End If
                      
                        If ScheduledHours > 0 Then
                            Attendance = Math.Round(CompletedHours * 100 / ScheduledHours, 2)
                            If Otherwhere.IndexOf("And") >= 0 Then
                                strSplit = Otherwhere.Split(" AND ")

                                substring = strSplit(0)
                                decimalVal = System.Convert.ToDecimal(substring)
                                substring2 = strSplit(2)
                                decimalVal2 = System.Convert.ToDecimal(substring2)
                            Else
                                substring = Otherwhere
                                decimalVal = System.Convert.ToDecimal(substring)
                            End If

                            If (Decimal.Parse(Attendance) >= Decimal.Parse(decimalVal) And Decimal.Parse(Attendance) <= Decimal.Parse(decimalVal2)) Then
                                rowNew = dt.NewRow()
                                If StudentIdentifier = "SSN" Then
                                    If Not row.IsNull("StudentIdentifier") Then
                                        'If row("StudentIdentifier") <> "" Then
                                        '    row("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, row("StudentIdentifier"))
                                        'End If
                                        If row("StudentIdentifier").ToString.Length >= 1 Then
                                            temp = row("StudentIdentifier")
                                            row("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                            'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))C:\TFS\Development\Advantage\Development\3.6\Source\BusinessFacade\TimeClockFacade.vb
                                        Else
                                            row("StudentIdentifier") = ""
                                        End If
                                    End If

                                End If
                                rowNew("StuEnrollId") = row("stuEnrollid").ToString
                                rowNew("StudentIdentifier") = row("StudentIdentifier").ToString
                                rowNew("StudentName") = row("StudentName").ToString
                                rowNew("ScheduledHours") = ScheduledHours
                                rowNew("AbsentHours") = AbsentHours
                                rowNew("CompletedHours") = CompletedHours
                                rowNew("Attendance") = Attendance
                                rowNew("CampGrpDescrip") = row("CampGrpDescrip").ToString
                                rowNew("CampDesrip") = row("CampDesrip").ToString
                                rowNew("PrgVerDescrip") = row("PrgVerDescrip").ToString
                                If Not row("CohortStartDate") Is DBNull.Value Then
                                    rowNew("CohortStartDate") = CDate(row("CohortStartDate")).ToShortDateString
                                End If
                                Try
                                    rowNew("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                                Catch ex As System.Exception
                                    rowNew("SuppressDate") = "no"
                                End Try

                                rowNew("MakingSAP") = row("MakingSAP").ToString
                                dt.Rows.Add(rowNew)



                            End If



                        ElseIf (Decimal.Parse(Attendance) >= Decimal.Parse(decimalVal) And Decimal.Parse(Attendance) <= Decimal.Parse(decimalVal2)) Or (Decimal.Parse(Attendance) >= Decimal.Parse(decimalVal)) Then
                            rowNew = dt.NewRow()
                            If StudentIdentifier = "SSN" Then
                                If Not row.IsNull("StudentIdentifier") Then
                                    'If row("StudentIdentifier") <> "" Then
                                    '    row("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, row("StudentIdentifier"))
                                    'End If
                                    If row("StudentIdentifier").ToString.Length >= 1 Then
                                        temp = row("StudentIdentifier")
                                        row("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                        'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))C:\TFS\Development\Advantage\Development\3.6\Source\BusinessFacade\TimeClockFacade.vb
                                    Else
                                        row("StudentIdentifier") = ""
                                    End If
                                End If

                            End If
                            rowNew("StuEnrollId") = row("stuEnrollid").ToString
                            rowNew("StudentIdentifier") = row("StudentIdentifier").ToString
                            rowNew("StudentName") = row("StudentName").ToString
                            rowNew("ScheduledHours") = ScheduledHours
                            rowNew("AbsentHours") = AbsentHours
                            rowNew("CompletedHours") = CompletedHours
                            rowNew("Attendance") = Attendance
                            rowNew("CampGrpDescrip") = row("CampGrpDescrip").ToString
                            rowNew("CampDesrip") = row("CampDesrip").ToString
                            rowNew("PrgVerDescrip") = row("PrgVerDescrip").ToString
                            If Not row("CohortStartDate") Is DBNull.Value Then
                                rowNew("CohortStartDate") = CDate(row("CohortStartDate")).ToShortDateString
                            End If
                            Try
                                rowNew("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                            Catch ex As System.Exception
                                rowNew("SuppressDate") = "no"
                            End Try

                            rowNew("MakingSAP") = row("MakingSAP").ToString



                            dt.Rows.Add(rowNew)



                        End If
                    Next
            End If

            End If

            ds1.Tables.Add(dt)


        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds1
    End Function





End Class

