Public Class ClassRosterObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim clsRoster As New ClassRosterDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by report.
        ds = BuildReportSource(clsRoster.GetClassRoster(rptParamInfo), clsRoster.StudentIdentifier, rptParamInfo.ResId)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region

#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String, ByVal resid As Integer) As DataSet
        Dim stuName As String
        Dim facInputMasks As New InputMasksFacade
        'Get the mask for SSN
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count > 0 Then
                'Report dataset.
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        'Store GUID field as string so it can be used to link to subreport.
                        dr("ClsSectionStr") = dr("ClsSectionId").ToString
                    Next
                End If
                '
                'Subreport dataset.
                If ds.Tables(1).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(1).Rows
                        'Store GUID field as string so it can be used to link to main report.
                        dr("TestIdStr") = dr("TestId").ToString
                        '
                        'Store StudentIdentifier label.
                        dr("StuIdentifierField") = StudentIdentifier
                        '
                        'Set up student name as: "LastName, FirstName MI."
                        If Not (dr("LastName") Is System.DBNull.Value) Then
                            If dr("LastName") <> "" Then
                                stuName = dr("LastName")
                            End If
                        End If
                        If Not (dr("FirstName") Is System.DBNull.Value) Then
                            If dr("FirstName") <> "" Then
                                If stuName <> "" Then
                                    stuName &= ", " & dr("FirstName")
                                Else
                                    stuName &= dr("FirstName")
                                End If
                            End If
                        End If
                        If Not (dr("MiddleName") Is System.DBNull.Value) Then
                            If dr("MiddleName") <> "" Then
                                If stuName <> "" Then
                                    stuName &= " " & dr("MiddleName") & "."
                                Else
                                    stuName &= dr("MiddleName") & "."
                                End If
                            End If
                        End If
                        dr("StudentName") = stuName
                        '
                        'Apply mask to SSN.
                        'just ignore for sign-in sheet report we get enrollmentid
                        If resid <> 551 Then
                            If StudentIdentifier = "SSN" Then
                                If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                                    If dr("StudentIdentifier") <> "" Then
                                        Dim temp As String = dr("StudentIdentifier")
                                        'Troy: 2/17/2015 Commented out the following code line for US7019. We now want to show just the last four digits of the SSN
                                        'dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                        dr("StudentIdentifier") = temp.Substring(5, 4)
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
