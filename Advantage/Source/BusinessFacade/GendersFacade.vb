' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' GendersFacade.vb
'
' GendersFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class GendersFacade
    Public Function GetAllGenders() As DataSet

        '   Instantiate DAL component
        Dim gendersDB As New gendersDB

        '   get the dataset with all genders
        Return gendersDB.GetAllGenders()

    End Function
    Public Function GetAllGendersNoUnknownOptions() As DataSet
        '   Instantiate DAL component
        Dim gendersDB As New gendersDB

        '   get the dataset with all genders
        Return gendersDB.GetAllGendersNoUnknownOptions()
    End Function
End Class

