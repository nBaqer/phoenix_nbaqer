Public Class ListOfPastDueDisbObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim aidRec As New AidReceivedDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(aidRec.GetListOfPastDueDisb(rptParamInfo), aidRec.StudentIdentifier)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim stuName As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String

        'Get the mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count = 3 Then
                Dim dtPastDueDisb As DataTable = ds.Tables("ListOfPastDueDisb")
                Dim dtCampusTtl As DataTable = ds.Tables("CampusTotals")
                Dim dtCampGrpTtl As DataTable = ds.Tables("CampusGroupTotals")
                Dim rowC As DataRow
                Dim rowCG As DataRow
                Dim oldCmpGrp As String = ""
                Dim oldCampus As String = ""

                For Each dr As DataRow In dtPastDueDisb.Rows
                    dr("StudentCount") = dtPastDueDisb.Rows.Count

                    'Compute Totals
                    'Get campus group and campus
                    If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpDescrip") Then
                        'Compute totals for campus group
                        rowCG = dtCampGrpTtl.NewRow
                        rowCG("CampGrpDescrip") = dr("CampGrpDescrip")
                        rowCG("CGStuCount") = dtPastDueDisb.Compute("COUNT(LastName)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                        rowCG("CGTotalAmount") = dtPastDueDisb.Compute("SUM(Amount)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                        dtCampGrpTtl.Rows.Add(rowCG)
                        oldCmpGrp = dr("CampGrpDescrip")
                        oldCampus = ""
                    End If

                    If oldCmpGrp = dr("CampGrpDescrip") And oldCampus = "" Then
                        'Compute totals for campus
                        rowC = dtCampusTtl.NewRow
                        rowC("CampGrpDescrip") = oldCmpGrp
                        rowC("CampusDescrip") = dr("CampDescrip")
                        rowC("CStuCount") = dtPastDueDisb.Compute("COUNT(LastName)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                        "CampusId='" & dr("CampusId").ToString & "'")
                        rowC("CTotalAmount") = dtPastDueDisb.Compute("SUM(Amount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                        "CampusId='" & dr("CampusId").ToString & "'")
                        dtCampusTtl.Rows.Add(rowC)
                        oldCampus = dr("CampDescrip")
                        '
                    ElseIf oldCmpGrp = dr("CampGrpDescrip") And oldCampus <> dr("CampDescrip") Then
                        'Compute totals for campus
                        rowC = dtCampusTtl.NewRow
                        rowC("CampGrpDescrip") = oldCmpGrp
                        rowC("CampusDescrip") = dr("CampDescrip")
                        rowC("CStuCount") = dtPastDueDisb.Compute("COUNT(LastName)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                "CampusId='" & dr("CampusId").ToString & "'")
                        rowC("CTotalAmount") = dtPastDueDisb.Compute("SUM(Amount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                        "CampusId='" & dr("CampusId").ToString & "'")
                        dtCampusTtl.Rows.Add(rowC)
                        oldCampus = dr("CampDescrip")
                    End If

                    'Set up student name as: "LastName, FirstName MI."
                    stuName = dr("LastName")
                    If Not (dr("FirstName") Is System.DBNull.Value) Then
                        If dr("FirstName") <> "" Then
                            stuName &= ", " & dr("FirstName")
                        End If
                    End If
                    If Not (dr("MiddleName") Is System.DBNull.Value) Then
                        If dr("MiddleName") <> "" Then
                            stuName &= " " & dr("MiddleName")
                            If Len(dr("MiddleName").ToString) = 1 Then
                                stuName &= "."
                            End If
                        End If
                    End If
                    dr("StudentName") = stuName
                    '
                    '
                    'Apply mask to SSN.
                    If StudentIdentifier = "SSN" Then
                        If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                            If dr("StudentIdentifier") <> "" Then
                                Dim temp As String = dr("StudentIdentifier")
                                dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                            End If
                        End If
                    End If
                Next
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
