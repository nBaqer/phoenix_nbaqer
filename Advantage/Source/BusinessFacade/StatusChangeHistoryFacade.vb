﻿Public Class StatusChangeHistoryFacade


    Public Function GetStatusChangeHistory(ByVal stuEnrollId As String) As IList(Of StatusChangeHistoryObj)
        Dim objDB As New StatusChangeHistoryDB

        Dim list As IList(Of StatusChangeHistoryObj) = objDB.GetStatusChangeHistory(stuEnrollId)

        'Dim list As IList(Of StatusChangeHistoryObj) = BuildStuStatusChangeHistorySource(objDB.GetStatusChangeHistory(stuEnrollId), stuEnrollId)
        Return list
    End Function

    ''' <summary>
    ''' Return a string with Enrollment Name and Student Name for a particular Enrollment Id separate by ,
    ''' Example: Mercadeo a traves de Redes Sociales , Sandra  Diaz
    ''' </summary>
    ''' <param name="stuEnrollId">Enrollment Guid</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetEnrollmentNameAndStudentName(ByVal stuEnrollId As String) As String
        Dim objDB As New StatusChangeHistoryDB

        Dim result As String = objDB.GetEnrollmentNameAndStudentName(stuEnrollId)
        Return result

    End Function
    ''' <summary>
    ''' creates a temperory table in database for the selection in the report.
    ''' </summary>
    Public Sub CreateTempStatusChangeHistoryTable()
        Try
            Dim objDb As New StatusChangeHistoryDB
            objDb.CreateTempStatusChangeHistoryTable()
        Catch ex As Exception
            Throw New Exception(ex.ToString())
        End Try
    End Sub
    ''' <summary>
    ''' inserts the row of status cahnge table in a temperory table in database for the selection in the report.
    ''' </summary>
    Public Function InsertInTempStatusChangeHistory(studentEnrollmentId As String, effectiveDate As DateTime, statusDescription As String, reason As String, requestedBy As String, changedBy As String, caseNo As String, dateOfChange As DateTime)
        Try
            Dim objDb As New StatusChangeHistoryDB
            objDb.InsertInTempStatusChangeHistory(studentEnrollmentId, effectiveDate, statusDescription, reason, requestedBy, changedBy, caseNo, dateOfChange)
        Catch ex As Exception
            Throw New Exception(ex.ToString())
        End Try
    End Function

#Region "Private Methods"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <param name="stuEnrollId"></param>
    ''' <returns></returns>
    ''' <remarks>TODO All this function should be wiped out and take all information from syStudentStatusChange!!!!</remarks>
    Private Function BuildStuStatusChangeHistorySource(ByVal ds As DataSet, ByVal stuEnrollId As String) As DataSet
        Dim objDB As New StatusChangeHistoryDB
        Dim cntLoa As Integer = 0
        For Each dr As DataRow In ds.Tables("StudentHistory").Rows
            Dim strDetail As String
            Dim newStatusId As String = dr("NewStatusId").ToString
            '' Get LOA Start, End Date And Reason
            If dr("SysStatusId").ToString = "10" Then
                cntLoa = cntLoa + 1
                strDetail = objDB.GetLOA(stuEnrollId, newStatusId, cntLoa)
                dr("Details") = strDetail
            End If
            '' Get Grad Date 
            ''''''''''''''''''''''''''''''''''''''''''''''''''
            ' JG - 02/19/2015 - REMOVED AS PER Lori Doody
            'If dr("SysStatusId").ToString = "14" Then
            '    strDetail = objDB.GetGradDate(StuEnrollId)
            '    dr("Details") = strDetail
            'End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''
            '' Get Drop Date Or Transfer Out, Date determined and Reason
            ''If dr("SysStatusId").ToString = "12" Or dr("SysStatusId").ToString = "19" Then
            'If dr("SysStatusId").ToString = "19" Then 'Transfer Out
            '    strDetail = objDB.GetDropDate(stuEnrollId, dr("StudentStatusChangeId").ToString)
            '    dr("Details") = strDetail
            'End If
            '' Get Academic Probation
            If dr("SysStatusId").ToString = "20" Then
                strDetail = objDB.GetAcademicProbation(stuEnrollId, newStatusId)
                dr("Details") = strDetail
            End If
            '' Code Added by kamalesh Ahuja on 25th and 26 August to resolve mantis issue id 19485
            '' Get Suspention
            If dr("SysStatusId").ToString = "11" Then
                strDetail = objDB.GetSuspension(stuEnrollId, newStatusId)
                dr("Details") = strDetail
            End If
            '''
        Next
        ds.Tables("StudentHistory").AcceptChanges()
        Return ds
    End Function

#End Region

End Class
