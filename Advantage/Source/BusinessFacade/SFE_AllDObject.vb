
Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllDObject
Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SFE_AllD"

	Private dsRaw As New DataSet
	Private dtRptRaw As New DataTable
	Private dsRpt As New DataSet

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SFE_AllDObjectDB.GetReportDataSetRaw(RptParamInfo)

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		Dim drRpt As DataRow
		Dim drStudent As DataRow

		' get DataTable of processed student enrollment info, create DataView of student enrollment info, 
		'	filtered to only get Undergraduate students, sorted by StudentId, for use when processing 
		Dim dvStuEnrollInfo As New DataView(IPEDSFacade.GetProcessedEnrollInfo(dsRaw), _
											"ProgTypeDescrip LIKE '" & ProgTypeUndergraduate & "'", _
											"StudentId", DataViewRowState.CurrentRows)
		Dim RowPtr As Integer
		Dim drvStuEnrollInfo As DataRowView

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add(RptStuIdentifierColName, GetType(String))
			.Add("Name", GetType(String))
			.Add("Ind_FullTime", GetType(String))
			.Add("Ind_PartTime", GetType(String))
			.Add("Ind_Transfer", GetType(String))
			.Add("Ind_NonDeg", GetType(String))
		End With

		' loop thru student records in raw dataset and process raw data to create report records
		For Each drStudent In dsRaw.Tables(TblNameStudents).Rows
			' find student's record in enrollment info data - if student doesn't have a record in 
			'	enrollment info DataView, then student is not Undergraduate, so don't include in report
			RowPtr = dvStuEnrollInfo.Find(drStudent("StudentId"))
			If RowPtr >= 0 And RowPtr <= dvStuEnrollInfo.Count - 1 Then
				drvStuEnrollInfo = dvStuEnrollInfo(RowPtr)
			Else
				Continue For
			End If

			drRpt = dsRpt.Tables(MainTableName).NewRow

			' set Student Id and name
			IPEDSFacade.SetStudentInfo(drStudent, drRpt)

			' set indicators as appropriate

			' if student is Other Degree/Cert Seeking or First-time, indicate full- or part-time student
			drRpt("Ind_FullTime") = DBNull.Value
			drRpt("Ind_PartTime") = DBNull.Value
			If drvStuEnrollInfo("DegCertSeekingDescrip").ToString = DegCertSeekingOtherDegree Or _
			   drvStuEnrollInfo("DegCertSeekingDescrip").ToString.StartsWith(DegCertSeekingFirstTime) Then
				If drvStuEnrollInfo("AttendTypeDescrip") = AttendTypeFullTime Then
					drRpt("Ind_FullTime") = "X"
				Else
					drRpt("Ind_PartTime") = "X"
				End If
			End If

			' indicate if student is a transfer-in
			drRpt("Ind_Transfer") = IIf(CInt(drStudent("TransferGrades")) > 0, "X", DBNull.Value)

			' indicate if student is non-degree seeking 
			drRpt("Ind_NonDeg") = IIf(drvStuEnrollInfo("DegCertSeekingDescrip").ToString = DegCertSeekingNonDegree, "X", DBNull.Value)

			' add new report row to report table
			dsRpt.Tables(MainTableName).Rows.Add(drRpt)
		Next

		' return report dataset
		Return dsRpt

	End Function

End Class
