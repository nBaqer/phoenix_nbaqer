Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.AdvantageV1.BusinessFacade.SGR_CommonObject

Public Class SGR_2YR3DetailObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SGR_2YR3Detail"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet
	Private RptParamInfo As ReportParamInfoIPEDS

	Private DateFilter As String
	Private dvEnrollments As DataView

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SGR_2YR3DetailObjectDB.GetReportDatasetRaw(RptParamInfo)

		' save passed-in report parameter info 
		Me.RptParamInfo = RptParamInfo

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		Dim dtGenderInfo As DataTable = IPEDSFacade.GetIPEDSGenderInfo
		Dim drGenderInfo As DataRow
		Dim dtEthnicInfo As DataTable = IPEDSFacade.GetIPEDSEthnicInfo
		Dim drEthnicInfo As DataRow

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add(RptStuIdentifierColName, GetType(String))
			.Add("Name", GetType(String))
			.Add("GenderDescrip", GetType(String))
			.Add("EthCodeDescrip", GetType(String))
			.Add("Ind_RevCohort", GetType(String))
			.Add("Ind_Comp", GetType(String))
			.Add("Ind_TransferOut", GetType(String))
			.Add("Ind_Excluded", GetType(String))
		End With

		' create student Enrollments DataView to be used when processing records
		dvEnrollments = New DataView(dsRaw.Tables(TblNameEnrollments))

		' set up date filter to be used when processing records
		DateFilter = EnrollDateFilter(RptParamInfo)

		' loop thru genders, and ethnic groups for each gender, and process
		'	matching records in raw dataset
		For Each drGenderInfo In dtGenderInfo.Rows
			For Each drEthnicInfo In dtEthnicInfo.Rows
				ProcessRows(drGenderInfo("GenderDescrip"), drEthnicInfo("EthCodeDescrip"))
			Next
		Next

		' return the Report DataSet
		Return dsRpt

	End Function

	Private Sub ProcessRows(ByVal GenderDescrip As String, ByVal EthCodeDescrip As String)
		Dim i As Integer, j As Integer
		Dim drRpt As DataRow
		Dim dvStudents As DataView
		Dim AddedRows As Boolean = False
		Dim StudentId As String

		' create dataview from students in raw dataset, filtered according to the 
		'	passed in gender and ethnic ids
		dvStudents = New DataView(dsRaw.Tables(TblNameStudents), _
								  "GenderDescrip LIKE '" & GenderDescrip & "' AND EthCodeDescrip LIKE '" & EthCodeDescrip & "'", _
								  "", DataViewRowState.CurrentRows)

		If dvStudents.Count > 0 Then
			For i = 0 To dvStudents.Count - 1
				drRpt = dsRpt.Tables(MainTableName).NewRow

				StudentId = dvStudents(i)("StudentId").ToString

				' determine if student is a "transfer out"
				drRpt("Ind_TransferOut") = DBNull.Value
				If TransferOut(StudentId, dvEnrollments) Then
					drRpt("Ind_TransferOut") = "X"
				End If

				' determine if student is an exclusion due to drop reason
				drRpt("Ind_Excluded") = DBNull.Value
				If ExcludedFromCohort(StudentId, dvEnrollments) Then
					drRpt("Ind_Excluded") = "X"
				End If

				' if student is not a transfer out and is not excluded, determine if student completed 
				'	program within 150% of normal time to completion
				drRpt("Ind_Comp") = DBNull.Value
				If (Not drRpt("Ind_TransferOut").Equals("X") And Not drRpt("Ind_Excluded").Equals("X")) AndAlso _
				   Completed150Pct(StudentId, dvEnrollments, "Weeks <= " & NumWeeks_2Yrs, DateFilter) Then
					drRpt("Ind_Comp") = "X"
				End If

				' if any of the student indicators were set, set the rest of student row info, add row to report

				' assume student won't be in revised cohort
				drRpt("Ind_RevCohort") = DBNull.Value
				If drRpt("Ind_Comp").Equals("X") Or drRpt("Ind_TransferOut").Equals("X") Or _
				   drRpt("Ind_Excluded").Equals("X") Then
					' indicate student belongs to revised cohort
					drRpt("Ind_RevCohort") = "X"
					' set student identifier and name
					IPEDSFacade.SetStudentInfo(dvStudents(i), drRpt)
					' set student Gender description
					drRpt("GenderDescrip") = GenderDescrip
					' set student Ethnic description
					drRpt("EthCodeDescrip") = EthCodeDescrip
					' add new row to report
					dsRpt.Tables(MainTableName).Rows.Add(drRpt)
					' indicate we added rows, so no blank row
					AddedRows = True
				End If
			Next
		End If

		' if didn't add any rows to report, either because no students match the passed-in Gender/Ethnic Code, 
		'	or because no student indicators were set, add row to report with only Gender and 
		'	Ethnic Code descriptions, blank data otherwise
		If Not AddedRows Then
			drRpt = dsRpt.Tables(MainTableName).NewRow

			drRpt(RptStuIdentifierColName) = DBNull.Value
			drRpt("Name") = DBNull.Value
			drRpt("GenderDescrip") = GenderDescrip
			drRpt("EthCodeDescrip") = EthCodeDescrip
			drRpt("Ind_RevCohort") = DBNull.Value
			drRpt("Ind_Comp") = DBNull.Value
			drRpt("Ind_TransferOut") = DBNull.Value
			drRpt("Ind_Excluded") = DBNull.Value

			' add empty report row to report 
			dsRpt.Tables(MainTableName).Rows.Add(drRpt)
		End If

	End Sub

End Class