Public Class StudentGradeBookResultsFromConversionFacade
    Public Function GetCourseResultsForEnrollment(ByVal stuEnrollId As String, ByVal reqId As String) As DataTable
        Dim db As New StudentGradeBookResultsFromConversionDB

        Return db.GetCourseResultsForEnrollment(stuEnrollId, reqId)

    End Function

    Public Function GetCoursesWithResultsForEnrollment(ByVal stuEnrollId As String) As DataTable
        Dim db As New StudentGradeBookResultsFromConversionDB

        Return db.GetCoursesWithResultsForEnrollment(stuEnrollId)

    End Function
End Class
