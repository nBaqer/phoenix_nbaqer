Public Class FourteenDayLetterFacade
    Public Function GetFourteenDayDS(ByVal StartDate As String, ByVal EndDate As String, ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        Dim dr As DataRow
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        ds = (New FourteenDayLetterDB).GetFourteenDayDS(StartDate, EndDate, campusId)
        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds

    End Function

    Public Function GetLetterDS(ByVal TransactionID As String) As DataSet
        Return (New FourteenDayLetterDB).GetLetterDS(TransactionID)
    End Function
End Class
