Public Class TerminateFacade

    'Public Function TerminateStudent(ByVal stuEnrollId As String, ByVal lda As String, ByVal dateDetermined As String, ByVal dropStatus As String, ByVal dropReason As String, ByVal user As String) As String
    '    Dim db As New TerminateStudent
    '    Dim studentStatus As StateEnum
    '    Dim createObj As New StateFactory

    '    'Retrieve current status of student
    '    studentStatus = db.GetStudentStatus(stuEnrollId)

    '    'Based on current status on student, create appropriate state object
    '    Dim stateObj As StudentEnrollmentState = StateFactory.CreateStateObj(studentStatus)

    '    Return stateObj.DropStudent(stuEnrollId, lda, dateDetermined, dropStatus, dropReason, user)

    'End Function

    ''' <summary>
    ''' All Facade with all parameters
    ''' </summary>
    ''' <param name="stuEnrollId"></param>
    ''' <param name="lda"></param>
    ''' <param name="dateDetermined"></param>
    ''' <param name="status"></param>
    ''' <param name="dropReason"></param>
    ''' <param name="user"></param>
    ''' <returns></returns>
    ''' <remarks>This call Drop with object as input</remarks>
    Public Shared Function Drop(ByVal stuEnrollId As String, ByVal lda As String, ByVal dateDetermined As String, ByVal status As String, ByVal dropReason As String, ByVal user As String, campusId As String) As String
        Dim objDB As New StatusChangeHistoryDB
        Dim db1 As New ArStudentStatusChangeDb
        Dim record As New StudentChangeHistoryObj
        'Update the student enrollment status to 'dropped' and the student status
        'to 'inactive'
        'Get last state
        record.OrigStatusGuid = db1.GetPreviousStatusChange(stuEnrollId)
        record.StuEnrollId = stuEnrollId
        record.DateOfChange = dateDetermined
        record.NewStatusGuid = status
        record.DropReasonGuid = dropReason
        record.ModUser = user
        record.Lda = objDB.GetLdaDate(stuEnrollId)
        record.CampusId = campusId
        Return Drop(record)
        End Function

    ''' <summary>
    ''' New with input parameter in one object
    ''' </summary>
    ''' <param name="record"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Drop(record As StudentChangeHistoryObj)
        Dim dbc As BatchStatusChangeDB = New BatchStatusChangeDB()
        Try
            Dim result = dbc.TerminateStudent(record)
            Return result
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function IsDropStatus(ByVal stuEnrollId As String) As Boolean
        Dim db As New TerminateStudent
        Dim studentStatus As StateEnum
        Dim createObj As New StateFactory


        'Retrieve current status of student
        studentStatus = db.GetStudentStatus(stuEnrollId)

        If studentStatus = StateEnum.Dropped Then
            Return True
        Else
            Return False
        End If

    End Function

    ''' <summary>
    ''' Get the student actual state
    ''' </summary>
    ''' <param name="stuEnrollId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetStudentStatus(stuEnrollId As String)
        Dim studentStatus As StateEnum
        Dim db As New TerminateStudent
        'Retrieve current status of student
        studentStatus = db.GetStudentStatus(stuEnrollId)
        Return studentStatus
    End Function

    ''' <summary>
    ''' Return the StatuscodeId of the syStatusCodes as a Guid converted to string
    ''' </summary>
    ''' <param name="stuEnrollId">The filtered enrollment id</param>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Public Shared Function GetStudentStatusId(stuEnrollId As String) As String
        Dim studentStatus As String
        Dim db As New TerminateStudent
        'Retrieve current status of student
        studentStatus = db.GetStudentStatusId(stuEnrollId)
        Return studentStatus
    End Function


    Public Function GetDropStatuses() As DataSet
        '   Instantiate DAL component
        Dim db As New TerminateStudent
        Dim ds As DataSet = db.GetDropStatuses() '   get the dataset with all drop statuses   
        Return ds
    End Function

    Public Function GetDropReasons(ByVal campusid As String) As DataSet
        '   Instantiate DAL component
        Dim db As New TerminateStudent
        Dim ds As DataSet = db.GetDropReasons(campusid) '   get the dataset with all drop statuses   
        Return ds
    End Function

    'Public Function PopulateDataGrid(ByVal stuEnrollId As String) As DataSet

    '    Dim db As New TerminateStudent
    '    Dim ds As DataSet = db.GetDataGridInfo(stuEnrollId)
    '    Return ds
    'End Function


End Class
