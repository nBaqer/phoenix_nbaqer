﻿Public Class SetDictationTestRulesFacade
    Public Function GetTestByCourse(ByVal CourseId As String) As DataSet
        Return (New SetDictationTestRulesDB).GetTestByCourse(CourseId)
    End Function
    Public Sub SetupRules(ByVal dtEffectiveDate As DateTime, _
                               ByVal xmlRules As String, _
                               Optional ByVal dtOldEffectiveDate As DateTime = #1/1/1900#)
        Dim db As New SetDictationTestRulesDB
        db.SetupRules(dtEffectiveDate, xmlRules, dtOldEffectiveDate)
    End Sub
    Public Sub SetupMentorProctoredTestRequirement(ByVal dtEffectiveDate As DateTime, _
                              ByVal xmlRules As String, _
                              Optional ByVal dtOldEffectiveDate As DateTime = #1/1/1900#, _
                              Optional ByVal strModUser As String = "sa", _
                              Optional ByVal dtModDate As DateTime = #1/1/1900#)
        Dim db As New SetDictationTestRulesDB
        db.SetupMentorProctoredTestRequirement(dtEffectiveDate, xmlRules, dtOldEffectiveDate, strModUser, dtModDate)
    End Sub
    Public Function GetEffectiveDates(ByVal reqId As String) As DataSet
        Return (New SetDictationTestRulesDB).GetEffectiveDates(reqId)
    End Function
    Public Function GetRulesByEffectiveDateAndCourse(ByVal EffectiveDate As DateTime, _
                                                     ByVal reqId As String) As DataSet
        Return (New SetDictationTestRulesDB).GetRulesByEffectiveDateAndCourse(EffectiveDate, reqId)
    End Function
    Public Sub DeleteRulesByEffectiveDateAndCourse(ByVal EffectiveDate As DateTime, _
                                                   ByVal reqId As String)
        Dim RulesDB As New SetDictationTestRulesDB

        RulesDB.DeleteRulesByEffectiveDateAndCourse(EffectiveDate, reqId)
    End Sub
    Public Sub SetupPreRequisites(ByVal dtEffectiveDate As DateTime, _
                                ByVal ReqId As String, _
                                ByVal MinperCategory As Integer, _
                                ByVal MaxperCategory As Integer, _
                                ByVal MinperCombination As Integer, _
                                ByVal MaxperCombination As Integer, _
                                ByVal PreReqId As String, _
                                ByVal mentorproctored As Integer, _
                                ByVal moduser As String, _
                                ByVal moddate As DateTime, _
                                Optional ByVal dtOldEffectiveDate As DateTime = #1/1/1900#)
        Dim RulesDB As New SetDictationTestRulesDB
        RulesDB.SetupPreRequisites(dtEffectiveDate, ReqId, MinperCategory, MaxperCategory, _
                                   MinperCombination, MaxperCombination, PreReqId, mentorproctored, moduser, moddate, dtOldEffectiveDate)
    End Sub
    Public Sub ResetPreRequisites(ByVal ReqId As String, Optional ByVal dtEffectiveDate As DateTime = #1/1/1900#)
        Dim RulesDB As New SetDictationTestRulesDB
        RulesDB.ResetPreRequisites(ReqId, dtEffectiveDate)
    End Sub
    Public Function GetPreRequisitesByEffectiveDateAndCourse(ByVal EffectiveDate As DateTime, _
                                                    ByVal reqId As String) As DataSet
        Dim ds As New DataSet
        ds = (New SetDictationTestRulesDB).GetPreRequisitesByEffectiveDateAndCourse(EffectiveDate, reqId)
        Return ds
    End Function
    Public Function GetMentorProctoredTestRequirement(ByVal reqId As String, ByVal PrereqId As String) As DataSet
        Dim ds As New DataSet
        ds = (New SetDictationTestRulesDB).GetMentorProctoredTestRequirement(reqId, PrereqId)
        Return ds
    End Function
    Public Function GetMentorProctoredTestByEffectiveDateAndCourse(ByVal EffectiveDate As DateTime, _
                                                     ByVal reqId As String) As DataSet
        Dim ds As New DataSet
        ds = (New SetDictationTestRulesDB).GetMentorProctoredTestByEffectiveDateAndCourse(EffectiveDate, reqId)
        Return ds
    End Function
    Public Function GetEffectivedatesbyCourse(ByVal reqId As String) As DataSet
        Dim ds As New DataSet
        ds = (New SetDictationTestRulesDB).GetEffectivedatesbyCourse(reqId)
        Return ds
    End Function
    Public Function GetTopSpeedTestCategory(ByVal EffectiveDate As DateTime, _
                                                    ByVal reqId As String) As DataSet
        Dim ds As New DataSet
        ds = (New SetDictationTestRulesDB).GetTopSpeedTestCategory(EffectiveDate, reqId)
        Return ds
    End Function
    'Public Function GetMinimumCombination(ByVal EffectiveDate As DateTime, _
    '                                               ByVal reqId As String) As DataSet
    '    Dim ds As New DataSet
    '    ds = (New SetDictationTestRulesDB).GetMinimumCombination(EffectiveDate, reqId, "")
    '    Return ds
    'End Function
    Public Function GetSAPShortHandSkillLevel() As DataSet
        Dim ds As New DataSet
        ds = (New SetDictationTestRulesDB).GetSAPShortHandSkillLevel()
        Return ds
    End Function
    Public Function GetShortHandCourses() As Integer
        Dim intRowCount As New Integer
        intRowCount = (New SetDictationTestRulesDB).GetShortHandCourses()
        Return intRowCount
    End Function
    Public Function BindMentorProctoredTestRequirement(ByVal EffectiveDate As DateTime, _
                                         ByVal reqId As String) As DataSet
        Dim ds As New DataSet
        ds = (New SetDictationTestRulesDB).BindMentorProctoredTestRequirement(EffectiveDate, reqId)
        Return ds
    End Function
    Public Sub SetupSAPShortHandSkillRequirement(ByVal SAPDetailId As String, _
                                                                                                ByVal xmlRules As String, _
                                                                                                Optional ByVal strModUser As String = "sa", _
                                                                                                Optional ByVal dtModDate As DateTime = #1/1/1900#)
        Dim db As New SetDictationTestRulesDB
        db.SetupSAPShortHandSkillRequirement(SAPDetailId, xmlRules, strModUser, dtModDate)
    End Sub
    Public Function BindShortHandSkillRequirement(ByVal SAPDetailid As String) As DataSet
        Return (New SetDictationTestRulesDB).BindShortHandSkillRequirement(SAPDetailid)
    End Function
    Public Sub DeletePreRequisitesByEffectiveDateAndCourse(ByVal EffectiveDate As DateTime, _
                                               ByVal reqId As String)
        Dim db As New SetDictationTestRulesDB
        db.DeletePreRequisitesByEffectiveDateAndCourse(EffectiveDate, reqId)
    End Sub
    Public Sub DeleteMentorTestByEffectiveDateAndCourse(ByVal EffectiveDate As DateTime, _
                                               ByVal reqId As String)
        Dim db As New SetDictationTestRulesDB
        db.DeleteMentorTestByEffectiveDateAndCourse(EffectiveDate, reqId)
    End Sub
    Public Function GetMentorRequirementText(ByVal EffectiveDate As DateTime, _
                                      ByVal reqId As String) As DataSet
        Dim db As New SetDictationTestRulesDB
        Return (db.GetMentorRequirementText(EffectiveDate, reqId))
    End Function
    Public Function GetSAPRequirement(ByVal SAPDetailid As String) As DataSet
        Dim db As New SetDictationTestRulesDB
        Return (db.GetSAPRequirement(SAPDetailid))
    End Function
    Public Function RegistrationInSHCourses(ByVal StuEnrollId As String, _
                                                                                  ByVal CourseCode As String, _
                                                                                  ByVal SHRepeatLimit As Integer, _
                                                                                  ByVal ReqId As String, _
                                                                                  ByVal ValidForregistration As String, _
                                                                                  Optional ByVal SHSemesterLimit As Integer = 11) As String
        Dim db As New SetDictationTestRulesDB
        Return (db.RegistrationInSHCourses(StuEnrollId, CourseCode, SHRepeatLimit, ReqId, ValidForregistration, SHSemesterLimit))
    End Function
    Public Sub ResetMentorRequirement(ByVal ReqId As String, Optional ByVal dtEffectiveDate As DateTime = #1/1/1900#)
        Dim db As New SetDictationTestRulesDB
        db.ResetMentorRequirement(ReqId, dtEffectiveDate)
    End Sub
    Public Sub SetupPageLevelPermission(ByVal xmlRules As String)
        Dim db As New SetDictationTestRulesDB
        db.SetupPageLevelPermission(xmlRules)
    End Sub
    Public Sub AddStudentPagesToOtherModules(ByVal xmlRules As String)
        Dim db As New SetDictationTestRulesDB
        db.AddStudentPagesToOtherModules(xmlRules)
    End Sub
End Class
