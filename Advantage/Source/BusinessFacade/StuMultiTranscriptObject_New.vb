Imports FAME.Advantage.Common

Public Class StuMultiTranscriptObject_New
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim stuTranscript As New StuTranscriptDB
        Dim ds, ds1 As DataSet
        'Get list of enrollments for ProgramVersion.
        'Massage dataset to produce one with the columns and data expected by the MultiTranscript report. 
        'The MultiTranscript report, whatever its frontend is, will only need to display the data in the dataset.
        ds1 = stuTranscript.GetEnrollmentList_SP(paramInfo)
        ds = BuildReportSource(ds1, stuTranscript.StudentIdentifier, stuTranscript.ShowProgramOnTranscript, paramInfo.CampusId, paramInfo)
        Return ds
    End Function

#End Region


#Region "Private Methods"
    Private Function BuildReportSource(ByVal EnrollmentDS As DataSet, ByVal StudentIdentifier As String, ByVal showProg As Boolean, Optional ByVal campusId As String = "", Optional ByVal paraminfo As ReportParamInfo = Nothing) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim rptDataSet As New DataSet
        Dim Transcript As New TranscriptFacade
        Dim TransDb As New TranscriptDB
        Dim br As New RegisterStudentsBR
        Dim StuEnrollId As String
        Dim stuName As String
        Dim stuLName As String
        Dim stuFName As String
        Dim facInputMasks As New InputMasksFacade
        Dim transcriptUtil As New FAME.AdvantageV1.Common.Utilities
        Dim temp As String
        Dim streetAddress As String = ""
        Dim cityStateZip As String = ""
        Dim row As DataRow
        Dim strIncludeHours As String = MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade")
        Dim includeHours As Boolean
        If strIncludeHours.ToLower = "true" Then
            includeHours = True
        End If

        'Get the mask for SSN, phone numbers and zip
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        Dim strMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        Dim zipMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        'Dim schoolClockHour As Boolean = (New ProgVerDB).IsClockHourSchool
        'Code Added by Vijay Ramteke on May, 06 2009
        Dim termCond As String = ""
        Dim classCond As String = ""

        'Code Added by Vijay Ramteke on May, 06 2009
        Try
            If EnrollmentDS.Tables.Count = 6 Then

                Dim dtEnroll As DataTable = EnrollmentDS.Tables("MultiTranscripts")
                Dim dtSummary As DataTable = EnrollmentDS.Tables("EnrollmentSummary")
                Dim dtCourses As DataTable = EnrollmentDS.Tables("EnrollmentCourses")
                Dim dtRptParams As DataTable = EnrollmentDS.Tables("ReportParams")
                Dim dtGradeDesc As DataTable = EnrollmentDS.Tables("GradeDescriptions")
                Dim dtLegend As DataTable = EnrollmentDS.Tables("Legend")
                Dim rowC As DataRow
                Dim ShowClassDate As Integer = 0
                Dim ShowTermProgressDescription As Boolean = CType(dtRptParams.Rows(0).Item("ShowTermProgressDescription").ToString(), Boolean)
                If MyAdvAppSettings.AppSettings("TranscriptType", campusId).ToLower = "traditional_b" Then
                    If paraminfo.ShowRptClassDates = True Then
                        ShowClassDate = 1
                    End If
                End If

                Dim strTermAndClass(2) As String
                strTermAndClass = GetTermAndClassCond(paraminfo)
                termCond = strTermAndClass(0)
                classCond = strTermAndClass(1)

                For Each row In dtEnroll.Rows

                    streetAddress = ""
                    cityStateZip = ""
                    row("ShowClassDate") = ShowClassDate
                    If paraminfo.ShowRptDateIssue Then
                        row("ShowDateIssue") = 1
                    Else
                        row("ShowDateIssue") = 0
                    End If

                    'Code Added by Vijay Ramteke on May, 06 2009
                    'termCond = row("TermCond")
                    'classCond = row("ClassCond")
                    'Code Added by Vijay Ramteke on May, 06 2009
                    'Set up student name as: "LastName, FirstName MI."
                    StuEnrollId = row("StuEnrollId").ToString
                    stuName = row("LastName")
                    stuLName = row("LastName")
                    If Not row.IsNull("FirstName") Then
                        stuFName = row("FirstName")
                        If row("FirstName") <> "" Then
                            stuName &= ", " & row("FirstName")
                        End If
                    End If
                    If Not row.IsNull("MiddleName") Then
                        If row("MiddleName") <> "" Then
                            stuName &= " " & row("MiddleName") & "."
                        End If
                    End If
                    row("StudentName") = stuName
                    row("StudentName2") = stuName

                    'If schoolClockHour Then
                    '    row("ClockHourSchool") = "yes"
                    'Else
                    '    row("ClockHourSchool") = "no"
                    'End If
                    If (New ProgVerDB).IsClockHourProgramVersion(StuEnrollId) Then
                        row("ClockHourSchool") = "yes"
                    Else
                        row("ClockHourSchool") = "no"
                    End If
                    row("ShowTermOrModule") = paraminfo.ShowTermOrModule

                    'Apply mask to SSN.
                    row("StudentIdentifier") = row(StudentIdentifier)
                    If StudentIdentifier = "SSN" Then
                        If Not (row("StudentIdentifier") Is System.DBNull.Value) Then
                            If row("StudentIdentifier") <> "" Then
                                temp = row("StudentIdentifier")
                                row("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                            End If
                        End If
                    End If

                    'Address1 allows NULL.
                    If Not row.IsNull("Address1") Then
                        If row("Address1") <> "" Then
                            streetAddress = row("Address1")
                        End If
                    Else
                        streetAddress = ""
                    End If
                    'Address2 allows NULL.
                    If Not row.IsNull("Address2") Then
                        If row("Address2") <> "" Then
                            streetAddress &= " " & row("Address2")
                        End If
                    End If
                    row("FullAddress") = streetAddress

                    'City allows NULL.
                    If Not row.IsNull("City") Then
                        If row("City") <> "" Then
                            cityStateZip = row("City")
                        End If
                    Else
                        cityStateZip = ""
                    End If

                    'State allows NULL.
                    If Not row.IsNull("StateDescrip") Then
                        If Not row.IsNull("ForeignZip") Then
                            If row("StateDescrip") <> "" And row("ForeignZip") = False Then
                                'Domestic State
                                If cityStateZip <> "" Then
                                    cityStateZip &= ", " & row("StateDescrip")
                                Else
                                    cityStateZip = row("StateDescrip")
                                End If
                            End If
                        Else
                            'Domestic State
                            If row("StateDescrip") <> "" Then
                                If cityStateZip <> "" Then
                                    cityStateZip &= ", " & row("StateDescrip")
                                Else
                                    cityStateZip = row("StateDescrip")
                                End If
                            End If
                        End If
                    End If

                    'StateOther allows NULL.
                    If Not row.IsNull("OtherState") Then
                        If Not row.IsNull("ForeignZip") Then
                            If row("OtherState") <> "" And row("ForeignZip") = True Then
                                'International State
                                If cityStateZip <> "" Then
                                    cityStateZip &= ", " & row("OtherState")
                                Else
                                    cityStateZip = row("OtherState")
                                End If
                            End If
                        End If
                    End If

                    'Zip allows NULL.
                    If Not row.IsNull("Zip") Then
                        If Not row.IsNull("ForeignZip") Then
                            If row("Zip") <> "" And row("ForeignZip") = False Then
                                'Domestic Zip
                                If cityStateZip <> "" Then
                                    cityStateZip &= " " & facInputMasks.ApplyMask(zipMask, row("Zip"))
                                Else
                                    cityStateZip = facInputMasks.ApplyMask(zipMask, row("Zip"))
                                End If
                            Else
                                'International Zip
                                If row("Zip") <> "" Then
                                    If cityStateZip <> "" Then
                                        cityStateZip &= " " & row("Zip")
                                    Else
                                        cityStateZip &= row("Zip")
                                    End If
                                End If
                            End If
                        Else
                            'Domestic Zip
                            If row("Zip") <> "" Then
                                If cityStateZip <> "" Then
                                    cityStateZip &= " " & facInputMasks.ApplyMask(zipMask, row("Zip"))
                                Else
                                    cityStateZip = facInputMasks.ApplyMask(zipMask, row("Zip"))
                                End If
                            End If
                        End If
                    End If

                    If cityStateZip <> "" Then
                        If row("FullAddress") <> "" Then
                            row("FullAddress") &= Chr(10) & cityStateZip
                        Else
                            row("FullAddress") = cityStateZip
                        End If
                    End If

                    'Country allows NULL.
                    If Not row.IsNull("CountryDescrip") Then
                        If row("CountryDescrip") <> "" Then
                            If row("FullAddress") <> "" Then
                                row("FullAddress") &= Chr(10) & row("CountryDescrip")
                            Else
                                row("FullAddress") = row("CountryDescrip")
                            End If
                        End If
                    End If

                    'Based on variable from the web.config "ShowProgramOnTranscript" 
                    'display on transcript the Program or the Program Version the student is enrolled into.
                    If Not showProg Then
                        row("ProgramDescrip") = row("PrgVerDescrip")
                    End If

                    'Get program student is enrolled into.


                    'Field to link tables in report.
                    row("StrStuEnrollId") = StuEnrollId

                    '**************************************************************************************************
                    'Troy:4/15/2007:This section was added to format the SSN whether or not it is being used as the
                    'student idientifier
                    '**************************************************************************************************
                    If Not row.IsNull("SSN") Then
                        If row("SSN") <> "" Then
                            'row("SSN") = facInputMasks.ApplyMask(strSSNMask, row("SSN"))
                            temp = row("SSN")
                            row("SSN") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                            'row("SSN") = facInputMasks.ApplyMask(strSSNMask, "" & temp.Substring(0))
                        End If
                    End If


                    row("SuppressGrades") = If(IsNothing(MyAdvAppSettings.AppSettings("SuppressGradeDescriptionInTranscript")), "no", MyAdvAppSettings.AppSettings("SuppressGradeDescriptionInTranscript").ToLower)
                    'Try
                    '   row("SuppressGrades") = MyAdvAppSettings.AppSettings("SuppressGradeDescriptionInTranscript").ToLower
                    'Catch ex As Exception
                    '    row("SuppressGrades") = "no"
                    'End Try
                    Try
                        row("SuppressStudentAddress") = MyAdvAppSettings.AppSettings("SuppressStudentAddressInTranscript").ToLower
                    Catch ex As Exception
                        row("SuppressStudentAddress") = "no"
                    End Try
                    Try
                        row("SuppressStudentId") = MyAdvAppSettings.AppSettings("SuppressStudentIdInTranscript").ToLower
                    Catch ex As Exception
                        row("SuppressStudentId") = "no"
                    End Try
                    Try
                        row("SuppressLDA") = MyAdvAppSettings.AppSettings("SuppressLDAInTranscript").ToLower
                    Catch ex As Exception
                        row("SuppressLDA") = "no"
                    End Try
                    'DisplayAsCourseCode
                    Try
                        row("DisplayAsCourseCode") = MyAdvAppSettings.AppSettings("DisplayCodeAsCourseCodeInTranscript").ToLower
                    Catch ex As Exception
                        row("DisplayAsCourseCode") = "no"
                    End Try
                    Try
                        row("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                    Catch ex As Exception
                        row("SuppressDate") = "no"
                    End Try
                    Try
                        row("IncludeAllCreditsAttempted") = MyAdvAppSettings.AppSettings("IncludeAllCreditsAttemptedinTotal").ToLower
                    Catch ex As Exception
                        row("IncludeAllCreditsAttempted") = "no"
                    End Try
                    ' Theresa: these are added to change the title label for credits in the Transcript requested by NWTech
                    row("CreditsDesc") = MyAdvAppSettings.AppSettings("TranscriptCredits")
                    row("SignDesc") = MyAdvAppSettings.AppSettings("TranscriptOfficialDescription")
                    '************************************************************************

                    'Troy:4/15/2007:Get the term progress summary
                    'Dim facGradAud As New GraduateAuditFacade
                    Dim fac As New GraduateAuditFacade
                    Dim dtTermProgress As DataTable
                    Dim gradeReps As String = MyAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod")
                    Dim strIncludeHrs As String = MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade")

                    '************************************************************************
                    Dim dsResults As DataSet = fac.GetTermProgressFromResults_SP(StuEnrollId, gradeReps, strIncludeHrs, termCond, classCond, paraminfo.CampusId)

                    'For every enrollment, get results and summary info.
                    Dim transcriptDB As New StuTranscriptDB
                    Dim dtResults As DataTable
                    dtResults = dsResults.Tables(0)
                    If dtResults.Rows.Count > 0 Then
                        
                        dtTermProgress = dsResults.Tables(1)

                        '   Summary information.
                        Dim dtComputeSummary As New DataTable("ComputeSummary")
                        dtComputeSummary.Columns.Add(New DataColumn("ReqId", Type.GetType("System.Guid")))
                        dtComputeSummary.Columns.Add(New DataColumn("Code", Type.GetType("System.String")))
                        dtComputeSummary.Columns.Add(New DataColumn("Descrip", Type.GetType("System.String")))
                        dtComputeSummary.Columns.Add(New DataColumn("Credits", Type.GetType("System.Decimal")))
                        dtComputeSummary.Columns.Add(New DataColumn("Grade", Type.GetType("System.String")))
                        dtComputeSummary.Columns.Add(New DataColumn("IsPass", Type.GetType("System.Boolean")))
                        dtComputeSummary.Columns.Add(New DataColumn("GPA", Type.GetType("System.String")))
                        dtComputeSummary.Columns.Add(New DataColumn("IsCreditsAttempted", Type.GetType("System.Boolean")))
                        dtComputeSummary.Columns.Add(New DataColumn("IsCreditsEarned", Type.GetType("System.Boolean")))
                        dtComputeSummary.Columns.Add(New DataColumn("IsInGPA", Type.GetType("System.Boolean")))
                        dtComputeSummary.Columns.Add(New DataColumn("Hours", Type.GetType("System.Decimal")))

                        Dim decCreditsEarnedSummary As Decimal = 0.0
                        Dim decCreditsAttemptedSummary As Decimal = 0.0
                        Dim decCreditsAttemptedSummaryForGpaCalculation As Decimal = 0.0 'sum of letter grades
                        Dim decGradePointsSummary As Decimal = 0.0
                        If MyAdvAppSettings.AppSettings("TranscriptType", campusId).ToLower = "traditional_b" And _
                           MyAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod").ToLower = "latest" Then
                            For Each drValidateResults As DataRow In dtResults.Rows

                                Dim drComputeSummaryRow As DataRow
                                drComputeSummaryRow = dtComputeSummary.NewRow
                                If dtComputeSummary.Rows.Count = 0 Then
                                    drComputeSummaryRow("ReqId") = CType(drValidateResults("ReqId"), Guid).ToString
                                    drComputeSummaryRow("Code") = drValidateResults("Code")
                                    drComputeSummaryRow("Descrip") = drValidateResults("Descrip")
                                    drComputeSummaryRow("Credits") = drValidateResults("Credits")
                                    drComputeSummaryRow("Grade") = drValidateResults("Grade")
                                    drComputeSummaryRow("IsPass") = drValidateResults("IsPass")
                                    drComputeSummaryRow("GPA") = drValidateResults("GPA")
                                    drComputeSummaryRow("IsCreditsAttempted") = drValidateResults("IsCreditsAttempted")
                                    drComputeSummaryRow("IsCreditsEarned") = drValidateResults("IsCreditsEarned")
                                    drComputeSummaryRow("IsInGPA") = drValidateResults("IsInGPA")
                                    drComputeSummaryRow("Hours") = drValidateResults("Hours")
                                    dtComputeSummary.Rows.Add(drComputeSummaryRow)
                                    If Not drValidateResults("IsCreditsEarned") Is System.DBNull.Value Then
                                        If CType(drValidateResults("IsCreditsEarned"), Boolean) = True Then
                                            decCreditsEarnedSummary += drValidateResults("Credits")
                                        End If
                                    End If
                                    If Not drValidateResults("IsCreditsAttempted") Is System.DBNull.Value Then
                                        If CType(drValidateResults("IsCreditsAttempted"), Boolean) = True Then
                                            decCreditsAttemptedSummary += drValidateResults("Credits")
                                            'If GPA is set to True and if student has received a passing grade
                                            'take credits attempted for GPA calculation
                                            If Not drValidateResults("IsInGPA") Is System.DBNull.Value Then
                                                If CType(drValidateResults("IsInGPA"), Boolean) = True Then
                                                    decCreditsAttemptedSummaryForGpaCalculation += drValidateResults("Credits")
                                                End If
                                                If CType(drValidateResults("IsInGPA"), Boolean) = True Then
                                                    decGradePointsSummary += drValidateResults("Credits") * drValidateResults("GPA")
                                                End If
                                            End If
                                        End If
                                    End If

                                Else
                                    'if this record is the second record in the datatable
                                    Dim dvComputeSummary As DataView
                                    dvComputeSummary = New DataView(dtComputeSummary)
                                    dvComputeSummary.RowFilter = "ReqId='" & CType(drValidateResults("ReqId"), Guid).ToString & "'"

                                    'If course is retaken then dvComputeSummary.Count will be greater than or equal to 1
                                    If dvComputeSummary.Count >= 1 Then
                                        'Just increment the Credits Attempted
                                        'No Need to increment Credits Earned as per rule we need only latest score
                                        If Not drValidateResults("IsCreditsAttempted") Is System.DBNull.Value Then
                                            If CType(drValidateResults("IsCreditsAttempted"), Boolean) = True Then
                                                decCreditsAttemptedSummary += drValidateResults("Credits")
                                            End If
                                        End If
                                    Else
                                        drComputeSummaryRow("ReqId") = CType(drValidateResults("ReqId"), Guid).ToString
                                        drComputeSummaryRow("Code") = drValidateResults("Code")
                                        drComputeSummaryRow("Descrip") = drValidateResults("Descrip")
                                        drComputeSummaryRow("Credits") = drValidateResults("Credits")
                                        drComputeSummaryRow("Grade") = drValidateResults("Grade")
                                        drComputeSummaryRow("IsPass") = drValidateResults("IsPass")
                                        drComputeSummaryRow("GPA") = drValidateResults("GPA")
                                        drComputeSummaryRow("IsCreditsAttempted") = drValidateResults("IsCreditsAttempted")
                                        drComputeSummaryRow("IsCreditsEarned") = drValidateResults("IsCreditsEarned")
                                        drComputeSummaryRow("IsInGPA") = drValidateResults("IsInGPA")
                                        drComputeSummaryRow("Hours") = drValidateResults("Hours")
                                        dtComputeSummary.Rows.Add(drComputeSummaryRow)
                                        If Not drValidateResults("IsCreditsEarned") Is System.DBNull.Value Then
                                            If CType(drValidateResults("IsCreditsEarned"), Boolean) = True Then
                                                decCreditsEarnedSummary += drValidateResults("Credits")
                                            End If
                                        End If
                                        If Not drValidateResults("IsCreditsAttempted") Is System.DBNull.Value Then
                                            If CType(drValidateResults("IsCreditsAttempted"), Boolean) = True Then
                                                decCreditsAttemptedSummary += drValidateResults("Credits")
                                                'If GPA is set to True take credits attempted for GPA calculation
                                                If Not drValidateResults("IsInGPA") Is System.DBNull.Value Then
                                                    If CType(drValidateResults("IsInGPA"), Boolean) = True Then
                                                        'And CType(drValidateResults("IsPass"), Boolean) = True Then
                                                        'Dim equivReqId As String = ""
                                                        Dim dsEquivReqId As New DataTable
                                                        'Dim summaryReqId As String = ""
                                                        Dim boolEquivalentCourseAlreadyComputed As Boolean = False
                                                        dsEquivReqId = (New GraduateAuditFacade).GetEquivalentReqIdDS_SP(drComputeSummaryRow("ReqId").ToString)
                                                        If Not dsEquivReqId Is Nothing Then
                                                            For Each drEquivCourseRow As DataRow In dsEquivReqId.Rows
                                                                dvComputeSummary.RowFilter = "ReqId='" & CType(drEquivCourseRow("EquivReqId"), Guid).ToString & "'"
                                                                If dvComputeSummary.Count >= 1 Then
                                                                    boolEquivalentCourseAlreadyComputed = True
                                                                    Exit For
                                                                End If
                                                            Next
                                                        End If
                                                        'if course has no equivalency and if the equivalent course has not been taken
                                                        If boolEquivalentCourseAlreadyComputed = False Then
                                                            decCreditsAttemptedSummaryForGpaCalculation += drValidateResults("Credits")
                                                            decGradePointsSummary += drValidateResults("Credits") * drValidateResults("GPA")
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If

                            Next
                        End If
                        '   Summary information.
                        Dim dr As DataRow
                        Dim arrWU() As DataRow
                        dr = dtSummary.NewRow
                        dr("StrStuEnrollId") = StuEnrollId
                        'Dim fac As New GraduateAuditFacade
                        dr("TotalClasses") = fac.GetTotalClasses
                        'dr("TotalClasses") = dtResults.Rows.Count


                        If MyAdvAppSettings.AppSettings("TranscriptType", campusId).ToLower = "traditional_b" And _
                           MyAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod").ToLower = "latest" Then
                            dr("TotalCreditsAttempted") = decCreditsAttemptedSummary
                            dr("TotalCreditsEarned") = decCreditsEarnedSummary
                            Try
                                dr("TotalGPA") = decGradePointsSummary / decCreditsAttemptedSummaryForGpaCalculation
                            Catch ex As Exception
                                dr("TotalGPA") = 0
                            End Try
                            dr("TotalGradePoints") = decGradePointsSummary
                            dr("TotalHours") = fac.CompletedHours
                        Else
                            If MyAdvAppSettings.AppSettings("IncludeCreditsAttemptedForRepeatedCourses").ToLower = "yes" Then
                                dr("TotalCreditsAttempted") = fac.AllAttemptedCredits
                                ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
                                dr("TotalScheduledHours") = fac.AllScheduledHours
                            Else
                                dr("TotalCreditsAttempted") = fac.AttemptedCredits
                                ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
                                dr("TotalScheduledHours") = fac.TotalScheduledHours
                            End If
                            dr("ShowROSSOnlyTabsForStudent") = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower()

                            'dr("TotalCreditsEarned") = fac.GetCreditsEarned
                            dr("TotalCreditsEarned") = fac.EarnedCredits

                            'dr("TotalGPA") = fac.GetGPA
                            dr("TotalGPA") = fac.GPA

                            'dr("TotalGradePoints") = 0
                            'dr("TotalHours") = fac.GetHoursCompleted
                            dr("TotalHours") = fac.CompletedHours

                            Dim decCalcTotalGradePoints As Decimal = 0.0
                            'Dim decCalcHours As Decimal = 0.0
                            For Each drResultRow As DataRow In dtResults.Rows
                                If Not drResultRow("IsCreditsAttempted") Is System.DBNull.Value Then
                                    If CType(drResultRow("IsCreditsAttempted"), Boolean) = True Then
                                        'If GPA is set to True, then Grade Point Summary will be calculated.
                                        If Not drResultRow("IsInGPA") Is System.DBNull.Value Then
                                            If CType(drResultRow("IsInGPA"), Boolean) = True Then
                                                decCalcTotalGradePoints += drResultRow("Credits") * drResultRow("GPA")
                                            End If
                                        End If
                                        'decCalcHours += drResultRow("Hours")
                                    End If
                                End If

                            Next
                            dr("TotalGradePoints") = decCalcTotalGradePoints

                        End If

                        dr("ShowROSSOnlyTabsForStudent") = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower()
                        dr("LastName") = stuLName
                        dr("FirstName") = stuFName
                        dtSummary.Rows.Add(dr)
                        'If stuLName = "Albert" And stuFName = "Carla" Then
                        '    Dim strName As String = ""
                        'End If


                        '   Results table.
                        Dim cnt As Integer = 0
                        Dim dtWorkUnit As DataTable
                        For Each dr2 As DataRow In dtResults.Rows
                            dtWorkUnit = transcriptDB.GetWorkUnitResults_SP(StuEnrollId, dr2("ReqId").ToString)
                            cnt = cnt + 1
                            rowC = dtCourses.NewRow
                            rowC("StrStuEnrollId") = StuEnrollId        'Field to link tables in report.
                            rowC("TermId") = dr2("TermId")
                            rowC("TermDescrip") = dr2("TermDescrip")
                            If (ShowTermProgressDescription = True) Then
                                rowC("DescripXTranscript") = dr2("DescripXTranscript")
                            Else
                                rowC("DescripXTranscript") = String.Empty
                            End If
                            rowC("ReqId") = dr2("ReqId")
                            rowC("Code") = dr2("Code")
                            rowC("Descrip") = dr2("Descrip")
                            rowC("StrCourseCategory") = dr2("CourseCategoryId").ToString
                            rowC("CourseCategory") = dr2("CourseCategory")
                            rowC("IsPass") = br.GrdOverRide_SP(row("PrgVerId").ToString, dr2("reqId").ToString, dr2("grade").ToString)
                            rowC("Credits") = 0
                            Dim boolIsCourseALab As Boolean = False
                            ' boolIsCourseALab = (New TransferGradeDB).isCourseALabWorkOrLabHourCourse(dr2("ReqId").ToString)
                            boolIsCourseALab = dr2("IsCourseALab")

                            If Not IsDBNull(dr2("IsCreditsAttempted")) Then
                                If dr2("IsCreditsAttempted") = True Or dr2("IsCreditsAttempted") = 1 Then
                                    If dr2("GrdSysDetailId") Is DBNull.Value Then

                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal = 0.0
                                            Try
                                                '  decCredits = (New TranscriptDB).ComputeWithCreditsPerService(StuEnrollId, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr2("ReqId").ToString)
                                                decCredits = dr2("decCredits")
                                            Catch ex As System.Exception
                                                decCredits = 0
                                            End Try
                                            rowC("CreditsAttempted") = decCredits
                                            'Add By Kamalesh Ahuja on April 02, 2010
                                            rowC("ScheduledHours") = dr2("ScheduledHours")
                                            'Add By Kamalesh Ahuja on April 02, 2010
                                        Else
                                            rowC("CreditsAttempted") = 0
                                            'Add By Kamalesh Ahuja on April 02, 2010
                                            rowC("ScheduledHours") = 0
                                            'Add By Kamalesh Ahuja on April 02, 2010
                                        End If
                                    Else
                                        rowC("CreditsAttempted") = dr2("Credits")
                                        'Add By Kamalesh Ahuja on April 02, 2010
                                        rowC("ScheduledHours") = dr2("ScheduledHours")
                                        'Add By Kamalesh Ahuja on April 02, 2010
                                    End If
                                Else
                                    rowC("CreditsAttempted") = 0
                                    'Add By Kamalesh Ahuja on April 02, 2010
                                    rowC("ScheduledHours") = 0
                                    'Add By Kamalesh Ahuja on April 02, 2010
                                End If
                            End If

                            rowC("Score") = dr2("Score")
                            'Code Added By Vijay Ramteke on May, 11 2009
                            If Not dr2("IsDrop") Is DBNull.Value Then
                                If dr2("IsDrop") = True And Not dr2.IsNull("DropDate") Then
                                    rowC("DateIssue") = dr2("DropDate")
                                Else
                                    rowC("DateIssue") = dr2("DateIssue")
                                End If
                            Else
                                rowC("DateIssue") = dr2("DateIssue")
                            End If

                            rowC("ShowDateIssue") = row("ShowDateIssue")
                            'Code Added By Vijay Ramteke on May, 11 2009
                            If MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                                If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                                    If Not IsDBNull(dr2("Score")) Then rowC("Score") = Math.Round(dr2("Score"))
                                End If
                            End If

                            If Not rowC("IsPass") Is DBNull.Value Then
                                If rowC("IsPass") = True Then

                                    ''Code added by Saraswathi on April 6 2009
                                    ''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                    ''Call the function to add credits earned
                                    Dim isHrsComp As Boolean = TransDb.IsCourseCombinationandPass(StuEnrollId, dr2("TestId").ToString())
                                    If isHrsComp = True Then
                                        If dr2("GrdSysDetailId") Is DBNull.Value Then
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                If (Decimal.TryParse(dr2("decCredits"), decCredits) = False) Then
                                                    decCredits = 0
                                                End If
                                                'Try
                                                '    decCredits = dr2("decCredits")
                                                'Catch ex As Exception
                                                '    decCredits = 0
                                                'End Try
                                                'rowC("Credits") = decCredits
                                                ''Added by saraswathi To fix issue 15845 and 16178
                                                ''Check if iscreditsearned is set , then add credits else donot add
                                                ''Added on may 13 2009
                                                If Not dr2.IsNull("IsCreditsEarned") Then
                                                    If dr2("IsCreditsEarned").ToString = "True" Or dr2("IsCreditsEarned").ToString = "1" Then
                                                        rowC("Credits") = decCredits
                                                        rowC("Hours") = dr2("Hours")
                                                    Else
                                                        rowC("Credits") = 0
                                                        rowC("Hours") = 0
                                                    End If
                                                Else
                                                    rowC("Credits") = 0
                                                    rowC("Hours") = 0
                                                End If
                                            Else
                                                rowC("Credits") = 0
                                                rowC("Hours") = 0
                                            End If
                                        Else
                                            'rowC("Credits") = dr2("Credits")
                                            ''Added by saraswathi To fix issue 15845 and 16178
                                            ''Check if iscreditsearned is set , then add credits else donot add
                                            ''Added on may 13 2009
                                            If Not dr2.IsNull("IsCreditsEarned") Then
                                                If dr2("IsCreditsEarned").ToString = "True" Or dr2("IsCreditsEarned").ToString = "1" Then
                                                    rowC("Credits") = dr2("Credits")
                                                    rowC("Hours") = dr2("Hours")
                                                Else
                                                    rowC("Credits") = 0
                                                    rowC("Hours") = 0
                                                End If
                                            Else
                                                rowC("Credits") = 0
                                                rowC("Hours") = 0
                                            End If
                                        End If
                                        ''Modified by saraswathi on may 29 2009
                                        ''to fix issue 16249
                                        '  rowC("Hours") = dr2("Hours")
                                    Else
                                        If (MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                            If dr2("GrdSysDetailId") Is DBNull.Value Then
                                                If boolIsCourseALab = True Then
                                                    Dim decCredits As Decimal
                                                    If (Decimal.TryParse(dr2("decCredits"), decCredits) = False) Then
                                                        decCredits = 0
                                                    End If
                                                    'Try
                                                    '    'decCredits = (New TranscriptDB).ComputeWithCreditsPerService(StuEnrollId, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr2("ReqId").ToString)
                                                    '    decCredits = dr2("decCredits")
                                                    'Catch ex As System.Exception
                                                    '    decCredits = 0
                                                    'End Try
                                                    If Not dr2.IsNull("IsCreditsEarned") Then
                                                        If dr2("IsCreditsEarned").ToString = "True" Or dr2("IsCreditsEarned").ToString = "1" Then
                                                            rowC("Credits") = decCredits
                                                            rowC("Hours") = dr2("Hours")
                                                        Else
                                                            rowC("Credits") = 0
                                                            rowC("Hours") = 0
                                                        End If
                                                    Else
                                                        rowC("Credits") = 0
                                                        rowC("Hours") = 0
                                                    End If
                                                Else
                                                    rowC("Credits") = 0
                                                    rowC("Hours") = 0
                                                End If
                                            Else
                                                ' rowC("Credits") = dr2("Credits")
                                                ''Added by saraswathi To fix issue 15845 and 16178
                                                ''Check if iscreditsearned is set , then add credits else donot add
                                                ''Added on may 13 2009
                                                If Not dr2.IsNull("IsCreditsEarned") Then
                                                    If dr2("IsCreditsEarned").ToString = "True" Or dr2("IsCreditsEarned").ToString = "1" Then
                                                        rowC("Credits") = dr2("Credits")
                                                        rowC("Hours") = dr2("Hours")
                                                    Else
                                                        rowC("Credits") = 0
                                                        rowC("Hours") = 0
                                                    End If
                                                Else
                                                    rowC("Credits") = 0
                                                    rowC("Hours") = 0
                                                End If

                                            End If
                                            ''Modified by saraswathi on may 29 2009
                                            ''to fix issue 16249
                                            ' rowC("Hours") = dr2("Hours")
                                        Else
                                            rowC("Credits") = 0.0
                                            rowC("Hours") = 0.0
                                        End If
                                    End If

                                    'rowC("Credits") = dr2("Credits")
                                    'rowC("Hours") = dr2("Hours")
                                Else
                                    If (MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                        'rowC("Credits") = dr2("Credits")
                                        ''Added by saraswathi To fix issue 15845 and 16178
                                        ''Check if iscreditsearned is set , then add credits else donot add
                                        ''Added on may 13 2009
                                        If Not dr2.IsNull("IsCreditsEarned") Then
                                            If dr2("IsCreditsEarned").ToString = "True" Or dr2("IsCreditsEarned").ToString = "1" Then
                                                rowC("Credits") = dr2("Credits")
                                                rowC("Hours") = dr2("Hours")
                                            Else
                                                rowC("Credits") = 0
                                                rowC("Hours") = 0
                                            End If
                                        Else
                                            rowC("Credits") = 0
                                            rowC("Hours") = 0
                                        End If
                                        ''Modified by saraswathi on may 29 2009
                                        ''to fix issue 16249
                                        ' rowC("Hours") = dr2("Hours")
                                    Else
                                        rowC("Credits") = 0.0
                                        rowC("Hours") = 0.0
                                    End If
                                    If dr2("GrdSysDetailId") Is DBNull.Value Then
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal
                                            Try
                                                'decCredits = (New TranscriptDB).ComputeWithCreditsPerService(StuEnrollId, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr2("ReqId").ToString)
                                                decCredits = dr2("decCredits")
                                            Catch ex As System.Exception
                                                decCredits = 0
                                            End Try
                                            ' rowC("Credits") = decCredits
                                            ''Added by saraswathi To fix issue 15845 and 16178
                                            ''Check if iscreditsearned is set , then add credits else donot add
                                            ''Added on may 13 2009
                                            If Not dr2.IsNull("IsCreditsEarned") Then
                                                If dr2("IsCreditsEarned").ToString = "True" Or dr2("IsCreditsEarned").ToString = "1" Then
                                                    rowC("Credits") = decCredits
                                                Else
                                                    rowC("Credits") = 0
                                                End If
                                            End If
                                        Else
                                            rowC("Credits") = 0
                                        End If
                                    End If
                                End If
                            End If

                            rowC("StartDate") = dr2("StartDate")
                            rowC("EndDate") = dr2("EndDate")
                            rowC("GrdSysDetailId") = dr2("GrdSysDetailId")
                            rowC("Grade") = dr2("Grade")
                            'rowC("IsPass") = dr2("IsPass")
                            rowC("GPA") = dr2("GPA")

                            '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
                            rowC("AvgScoreNew") = dr2("AvgScoreNew")
                            rowC("AvgGPANew") = dr2("AvgGPANew")
                            '''''''''''''

                            If dr2("IsCreditsAttempted") Is System.DBNull.Value Then
                                rowC("IsCreditsAttempted") = False
                            Else
                                rowC("IsCreditsAttempted") = dr2("IsCreditsAttempted")
                            End If
                            If dr2("IsCreditsEarned") Is System.DBNull.Value Then
                                rowC("IsCreditsEarned") = False
                            Else
                                rowC("IsCreditsEarned") = dr2("IsCreditsEarned")
                            End If

                            'code modified by Balaji on 09/16/2009 for Traditional_B Report Starts here
                            rowC("ClassStartDate") = dr2("ClassStartDate")
                            rowC("ClassEndDate") = dr2("ClassEndDate")
                            'code modified by Balaji on 09/16/2009 for Traditional_B Report Ends here

                            ''Added By Vijay Ramteke on Feb 16, 2010
                            If MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "true" Then
                                If MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "numeric" Then
                                    'rowC("CreditsAttempted") = 0

                                    arrWU = dtWorkUnit.Select("Score >= 0")
                                    If arrWU.Length > 0 Then rowC("CreditsAttempted") = dr2("Credits")
                                    If dtWorkUnit.Rows.Count > 0 Then
                                        If getCreditsEarned(dtWorkUnit, rowC("Score")) Then
                                            'rowC("Credits") = dr2("Credits")
                                            ''Added by saraswathi To fix issue 15845 and 16178
                                            ''Check if iscreditsearned is set , then add credits else donot add
                                            ''Added on may 13 2009
                                            If Not dr2.IsNull("IsCreditsEarned") Then
                                                If dr2("IsCreditsEarned").ToString = "True" Or dr2("IsCreditsEarned").ToString = "1" Then
                                                    rowC("Credits") = dr2("Credits")
                                                Else
                                                    rowC("Credits") = 0
                                                End If
                                            Else
                                                rowC("Credits") = 0
                                            End If
                                            'rowC("CreditsAttempted") = dr2("Credits")
                                        Else
                                            rowC("Credits") = 0
                                            'rowC("CreditsAttempted") = 0.0
                                        End If
                                    End If
                                End If
                            End If
                            If boolIsCourseALab = True Then
                                Dim boolRemaining As Boolean = (New ExamsFacade).IsRemainingLabCount_SP(StuEnrollId, 500, dr2("ReqId").ToString)
                                Dim boolIsClinicCompletlySatisfied1 As Boolean = (New ExamsFacade).isClinicCourseCompletlySatisfied_SP(StuEnrollId, 500, dr2("ReqId").ToString)
                                If boolRemaining = False Then
                                    If (New TransferGradeDB).isCourseALabWorkOrLabHourCourseCombination(dr2("ReqId").ToString) Then
                                        If rowC("IsPass") = True Then
                                            rowC("Hours") = dr2("Hours")
                                        Else
                                            rowC("Hours") = 0
                                        End If
                                    Else
                                        If Not dr2.IsNull("IsCreditsEarned") Then
                                            If dr2("IsCreditsEarned").ToString = "True" Or dr2("IsCreditsEarned").ToString = "1" Then
                                                rowC("Hours") = dr2("Hours")
                                            Else
                                                rowC("Hours") = 0
                                            End If
                                        Else
                                            rowC("Hours") = 0
                                        End If


                                    End If
                                End If
                                If dr2.IsNull("IsCreditsEarned") And boolIsClinicCompletlySatisfied1 = True Then
                                    rowC("Hours") = dr2("Hours")
                                    rowC("Credits") = dr2("Credits")
                                    rowC("CreditsAttempted") = dr2("Credits")
                                ElseIf dr2.IsNull("IsCreditsEarned") And boolIsClinicCompletlySatisfied1 = False And MyAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower = "yes" Then
                                    Dim decCredits As Decimal = 0.0
                                    Try
                                        decCredits = dr2("decCredits")
                                        'decCredits = (New TranscriptDB).ComputeWithCreditsPerService(StuEnrollId, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr2("ReqId").ToString)
                                    Catch ex As Exception
                                        decCredits = 0
                                    End Try
                                    rowC("Hours") = 0
                                    rowC("Credits") = decCredits
                                    rowC("CreditsAttempted") = decCredits
                                End If
                            End If

                            If Not dr2.IsNull("IsInGPA") Then
                                rowC("IsInGPA") = dr2("IsInGPA")
                            Else
                                rowC("IsInGPA") = False
                            End If


                            'Look up the TermGPA and CumGPA in the dtTermProgress datatable
                            Dim arrTrms() As DataRow
                            ' Code Added By Vijay Ramteke on May, 07 2009 
                            arrTrms = dtTermProgress.Select("TermDescrip = '" + dr2("TermDescrip").ToString.Replace("'", "''") + "'")
                            ' Code Added By Vijay Ramteke on May, 07 2009 
                            Try
                                If (arrTrms.Length > 0) Then
                                    rowC("TermGPA") = arrTrms(0)("TermGPA")
                                    rowC("CumGPA") = arrTrms(0)("GPA")
                                End If
                            Catch ex As Exception
                                Trace.WriteLine("971 empty catch exception: " + ex.Message)
                            End Try

                            '' Code Added By Vijay Ramteke For RallyId DE1393 On December 23, 2010
                            rowC("IsTransferGrade") = dr2("IsTransferGrade")
                            '' Code Added By Vijay Ramteke For RallyId DE1393 On December 23, 2010

                            dtCourses.Rows.Add(rowC)
                        Next
                    End If

                    '   If there is no results, then empty Enrollments table.
                    If IsNothing(dtResults) = False AndAlso IsNothing(dtResults.Rows) = False AndAlso dtResults.Rows.Count = 0 AndAlso IsNothing(row) = False Then
                        row.Delete() 'mark row for deletion because there were no courses for this enrollment.
                    End If
                Next

                '   Commit all changes.
                EnrollmentDS.AcceptChanges()

                '   Compute Course Categories Totals
                For Each row In dtCourses.Rows
                    If Not row.IsNull("CourseCategory") Then
                        row("TotalCredits") = dtCourses.Compute("SUM(Credits)", "StrStuEnrollId='" & row("StrStuEnrollId") & "' AND CourseCategory='" & row("CourseCategory") & "'")
                        row("TotalHours") = dtCourses.Compute("SUM(Hours)", "StrStuEnrollId='" & row("StrStuEnrollId") & "' AND CourseCategory='" & row("CourseCategory") & "'")
                    Else
                        row("TotalCredits") = dtCourses.Compute("SUM(Credits)", "StrStuEnrollId='" & row("StrStuEnrollId") & "' AND CourseCategory IS NULL")
                        row("TotalHours") = dtCourses.Compute("SUM(Hours)", "StrStuEnrollId='" & row("StrStuEnrollId") & "' AND CourseCategory IS NULL")
                    End If
                    row("LastName") = stuLName
                    row("FirstName") = stuFName
                Next

                'Troy:4/15/2007:Get the grade descriptions for the enrollment
                Dim facTF As New TranscriptFacade
                Dim dtGrdDescrip As DataTable = EnrollmentDS.Tables("GradeDescriptions")
                Dim dsGrdDescrip As DataSet
                Dim dtGrdRow As DataRow

                'This code should only be executed if students met the criteria
                If MyAdvAppSettings.AppSettings("TranscriptType", campusId).ToLower = "traditional_b" Then
                    If StuEnrollId <> "" And Not StuEnrollId Is Nothing Then
                        dsGrdDescrip = facTF.GetGradeDescriptionsForTranscript_SP(StuEnrollId)
                        'dtGrdDescrip = facTF.GetGradeDescriptionsForEnrollment(StuEnrollId)
                        dtGrdDescrip = dsGrdDescrip.Tables(0)

                        For Each drGrade As DataRow In dtGrdDescrip.Rows
                            dtGrdRow = dtGradeDesc.NewRow
                            If Not drGrade("Grade") Is System.DBNull.Value Then
                                dtGrdRow("Grade") = drGrade("Grade").ToString.Trim
                            Else
                                dtGrdRow("Grade") = ""
                            End If
                            If Not drGrade("GPA") Is DBNull.Value Then
                                dtGrdRow("GPA") = drGrade("GPA").ToString.Trim
                            Else
                                dtGrdRow("GPA") = "Not Used in GPA/CGPA"
                            End If

                            Try
                                If Not drGrade("Range") Is DBNull.Value Then
                                    dtGrdRow("Range") = drGrade("Range").ToString.Trim
                                Else
                                    dtGrdRow("Range") = "N/A"
                                End If
                            Catch ex As Exception
                                dtGrdRow("Range") = "N/A"
                            End Try
                            If Not drGrade("GradeDescription") Is DBNull.Value Then
                                dtGrdRow("GradeDescription") = drGrade("Grade").ToString.Trim & " - " & drGrade("GradeDescription")
                            Else
                                dtGrdRow("GradeDescription") = ""
                            End If
                            If Not drGrade("Quality") Is DBNull.Value Then
                                dtGrdRow("Quality") = drGrade("Quality").ToString.Trim
                            Else
                                dtGrdRow("Quality") = "N/A"
                            End If
                            Try
                                dtGrdRow("SuppressGradeDescription") = MyAdvAppSettings.AppSettings("SuppressGradeDescriptionInTranscript").ToLower
                            Catch ex As Exception
                                dtGrdRow("SuppressGradeDescription") = "no"
                            End Try

                            dtGradeDesc.Rows.Add(dtGrdRow)
                        Next


                        Dim dtLegendDescrip As DataTable = EnrollmentDS.Tables("Legend")
                        Dim dtLegendRow As DataRow
                        'dtLegendDescrip = facTF.GetGradeDescriptionsForEnrollmentNotNULL(StuEnrollId)
                        dtLegendDescrip = dsGrdDescrip.Tables(1)
                        For Each dtLegRow As DataRow In dtLegendDescrip.Rows
                            dtLegendRow = dtLegend.NewRow
                            If Not dtLegRow("GradeDescription") Is DBNull.Value Then
                                dtLegendRow("GradeDescription") = dtLegRow("Grade").ToString.Trim & " - " & dtLegRow("GradeDescription")
                            End If
                            dtLegend.Rows.Add(dtLegendRow)
                        Next
                    End If
                End If
                '   Put data into ReportParams table
                Dim corpInfo As CorporateInfo = (New CampusGroupsFacade).GetCorporateInfoFromCampusId_SP(campusId)

                '   Massage ReportParams table
                row = dtRptParams.Rows(0)

                With corpInfo
                    row("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                    row("CorporateName") = .CorporateName
                    streetAddress = .Address1
                    streetAddress &= " " & .Address2
                    row("FullAddress") = streetAddress
                    cityStateZip = .City
                    If .State <> "" Then
                        If cityStateZip <> "" Then
                            cityStateZip &= ", " & .State
                        Else
                            cityStateZip = .State
                        End If
                    End If
                    If .Zip <> "" Then
                        If cityStateZip <> "" Then
                            cityStateZip &= " " & facInputMasks.ApplyMask(zipMask, .Zip)
                        Else
                            cityStateZip = facInputMasks.ApplyMask(zipMask, .Zip)
                        End If
                    End If
                    If cityStateZip <> "" Then
                        If row("FullAddress") <> "" Then
                            row("FullAddress") &= Chr(10) & cityStateZip
                        Else
                            row("FullAddress") = cityStateZip
                        End If
                    End If

                    If .Phone <> "" Then
                        row("Phone") = "Phone: " & .Phone
                        'row("Phone") = "Phone: " & facInputMasks.ApplyMask(strMask, .Phone)
                    End If
                    If .Fax <> "" Then
                        row("Fax") = "Fax: " & .Fax
                        'row("Fax") = "Fax: " & facInputMasks.ApplyMask(strMask, .Fax)
                    End If
                    If .TranscriptAuthznTitle <> "" Then
                        row("TranscriptAuthznTitle") = .TranscriptAuthznTitle
                    End If
                    If .TranscriptAuthznName <> "" Then
                        row("TranscriptAuthznName") = .TranscriptAuthznName
                    End If
                    If .Website <> "" Then
                        row("Website") = .Website
                    End If
                End With

            End If

        Catch ex As Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return EnrollmentDS
    End Function
    Private Function getCreditsEarned(ByVal dtWU As DataTable, ByVal score As Object) As Boolean
        Dim wuSatisfied As Boolean = True
        Dim i As Integer

        'Check the work units that have been atempted.
        'arrWU = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "' AND Score >= 0")
        If dtWU.Rows.Count > 0 Then
            If IsDBNull(score) Then
                For i = 0 To dtWU.Rows.Count - 1

                    If dtWU.Rows(i)("Required") = True Or dtWU.Rows(i)("MustPass") = True Then
                        'If dtWU.Rows(i)("Required") = True Then
                        If IsDBNull(dtWU.Rows(i)("Score")) Then
                            wuSatisfied = False
                            Exit For
                        End If
                    End If

                Next

            ElseIf score >= 0 Then
                For i = 0 To dtWU.Rows.Count - 1
                    If IsDBNull(dtWU.Rows(i)("score")) Then
                        If dtWU.Rows(i)("Required") = True Or dtWU.Rows(i)("MustPass") = True Then
                            wuSatisfied = False
                            Exit For
                        Else
                            wuSatisfied = True
                        End If
                    ElseIf dtWU.Rows(i)("score") < dtWU.Rows(i)("MinResult") Then
                        'First check if it is required
                        'wuSatisfied = False
                        'Exit For
                        If dtWU.Rows(i)("Required") = True And dtWU.Rows(i)("MustPass") = True Then
                            wuSatisfied = False
                            Exit For
                        End If
                    End If
                Next


            End If
        End If
        If wuSatisfied Then
            'If the score is empty we can say right away that the course has been completed
            If (score.ToString = "" Or IsDBNull(score)) Then
                Return True
            Else
                'Check if the score is a passing score
                If ScoreIsAPass(score) Then
                    Return True
                Else
                    Return False
                End If
            End If
        End If
        Return wuSatisfied



    End Function
    Private Function ScoreIsAPass(ByVal csrScore As Decimal) As Boolean
        Dim db As New StuProgressReportDB
        Dim minPassScore As Decimal

        minPassScore = db.GetMinPassingScore

        If csrScore >= minPassScore Then
            Return True
        Else
            Return False
        End If

    End Function
    Private Function GetTermAndClassCond(ByVal paramInfo As ReportParamInfo) As String()
        Dim rtn(2) As String
        Dim strWhere As String
        Dim strOrderBy As String
        Dim strStuID As String

        'Code Added by Vijay Ramteke on May, 06 2009
        Dim strTempWhere As String = ""
        Dim strTerm As String = ""
        Dim strClassDate As String = ""
        'Code Added by Vijay Ramteke on May, 06 2009

        'Get ProgVerId and Where Clause from paramInfo.FilterList
        If paramInfo.FilterList <> "" Then
            strWhere &= " AND " & paramInfo.FilterList
        End If

        'Get StudentId and rest of Where Clause from paramInfo.FilterOther
        If paramInfo.FilterOther <> "" Then
            strWhere &= " AND " & paramInfo.FilterOther
        End If




        strTempWhere = strWhere
        strTempWhere = strTempWhere.ToLower.Replace("and", ";")
        strWhere = ""
        Dim strArr As String() = strTempWhere.Remove(strTempWhere.IndexOf(";"), 1).Split(";")
        Dim i As Integer
        For i = 0 To strArr.Length - 1
            If strArr(i).ToLower.IndexOf("arterm") > 0 Then
                strTerm &= " AND " & strArr(i)
            ElseIf strArr(i).ToLower.IndexOf("arclasssections") > 0 Then
                strClassDate &= " AND " & strArr(i)
            Else
                strWhere &= " AND " & strArr(i)
            End If
        Next
        rtn(0) = strTerm
        rtn(1) = strClassDate
        Return rtn
    End Function


#End Region


End Class
