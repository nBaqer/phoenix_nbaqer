' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' MaritalStatusesFacade.vb
'
' MaritalStatusesFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class MaritalStatusesFacade
    Public Function GetAllMaritalStatuses() As DataSet

        '   Instantiate DAL component
        Dim maritalStatusesDB As New maritalStatusesDB

        '   get the dataset with all MaritalStatuses
        Return maritalStatusesDB.GetAllMaritalStatuses()

    End Function
End Class
