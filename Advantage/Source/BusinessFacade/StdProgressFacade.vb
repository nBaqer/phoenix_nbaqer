Public Class StdProgressFacade
    Public Function GetStudentProgInfo(ByVal StuEnrollId As String) As StudentProgressInfo

        Dim db As New StdProgressDB
        Dim ds As New DataSet
        Return (db.GetStudentProgress(StuEnrollId))

    End Function


    Public Function PopulateDataList(ByVal StudentId As String, ByVal boolStatus As String, Optional ByVal campusId As String = Nothing) As DataSet

        Dim db As New StdProgressDB
        Dim ds As New DataSet
        Dim tbl1 As New DataTable
        ds = db.GetStudentEnrollments(StudentId, boolStatus, campusId)

        tbl1 = ds.Tables("StdEnrollments")

        'Commented by Michelle R. Rodriguez on 03/17/2006
        'A student may enroll twice on the same program version. For instance, the first time a student enrolls,
        'she/he drops due to a family emergency and, sometime later she enrolls again on the same program version.
        'AMC had that problem and the primary contraint that is enforced below was making fail the Term Progress page.
        'With tbl1
        '    .PrimaryKey = New DataColumn() {.Columns("PrgVerId")}
        'End With

        Return ds

    End Function


End Class
