Public Class JobListingObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim jobList As New JobListingDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(jobList.GetJobListing(rptParamInfo))
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    ' Copy data from dataset to datatable.
    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        Dim contactName As String = ""
        Dim streetAddress As String = ""
        Dim cityStateZip As String = ""
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim zipMask As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        Try
            ds.Tables(0).TableName = "JobListing"
            ds.Tables(0).Columns.Add(New DataColumn("ContactName", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("FullAddress", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("JobCount", System.Type.GetType("System.Int32")))

            For Each dr As DataRow In ds.Tables(0).Rows
                dr("JobCount") = ds.Tables(0).Rows.Count
                '
                contactName = ""
                streetAddress = ""
                cityStateZip = ""
                If Not (dr("LastName") Is System.DBNull.Value) Then
                    If dr("LastName") <> "" Then
                        contactName = dr("LastName")
                    End If
                Else
                    contactName = ""
                End If
                If Not (dr("FirstName") Is System.DBNull.Value) Then
                    If dr("FirstName") <> "" Then
                        If contactName <> "" Then
                            contactName &= ", " & dr("FirstName")
                        Else
                            contactName = dr("FirstName")
                        End If
                    End If
                End If
                If Not (dr("MiddleName") Is System.DBNull.Value) Then
                    If dr("MiddleName") <> "" Then
                        If contactName <> "" Then
                            contactName &= " " & dr("MiddleName") & "."
                        Else
                            contactName = dr("MiddleName") & "."
                        End If
                    End If
                End If
                dr("ContactName") = contactName
                '
                If Not (dr("Address1") Is System.DBNull.Value) Then
                    'Address1 allows NULL.
                    If dr("Address1") <> "" Then
                        streetAddress = dr("Address1")
                    End If
                Else
                    streetAddress = ""
                End If
                If Not (dr("Address2") Is System.DBNull.Value) Then
                    'Address2 allows NULL.
                    If dr("Address2") <> "" Then
                        streetAddress &= " " & dr("Address2")
                    End If
                End If
                dr("FullAddress") = streetAddress
                '
                If Not (dr("City") Is System.DBNull.Value) Then
                    'City allows NULL.
                    If dr("City") <> "" Then
                        cityStateZip = dr("City")
                    End If
                Else
                    cityStateZip = ""
                End If
                If Not (dr("State") Is System.DBNull.Value) Then
                    'State allows NULL.
                    'If dr("State") <> "" Then
                    '    If cityStateZip <> "" Then
                    '        cityStateZip &= ", " & dr("State")
                    '    Else
                    '        cityStateZip = dr("State")
                    '    End If
                    'End If
                    If Not (dr("ForeignZip") Is System.DBNull.Value) Then
                        If dr("State") <> "" And dr("ForeignZip") = False Then
                            'Domestic State
                            If cityStateZip <> "" Then
                                cityStateZip &= ", " & dr("State")
                            Else
                                cityStateZip = dr("State")
                            End If
                        End If
                    Else
                        'Domestic State
                        If dr("State") <> "" Then
                            If cityStateZip <> "" Then
                                cityStateZip &= ", " & dr("State")
                            Else
                                cityStateZip = dr("State")
                            End If
                        End If
                    End If
                End If
                'StateOther allows NULL.
                If Not (dr("OtherState") Is System.DBNull.Value) Then
                    If Not (dr("ForeignZip") Is System.DBNull.Value) Then
                        If dr("OtherState") <> "" And dr("ForeignZip") = True Then
                            'International State
                            If cityStateZip <> "" Then
                                cityStateZip &= ", " & dr("OtherState")
                            Else
                                cityStateZip = dr("OtherState")
                            End If
                        End If
                    End If
                End If
                If Not (dr("Zip") Is System.DBNull.Value) Then
                    'Zip allows NULL.
                    'If dr("Zip") <> "" Then
                    '    If cityStateZip <> "" Then
                    '        cityStateZip &= " " & facInputMasks.ApplyMask(zipMask, dr("Zip"))
                    '    Else
                    '        cityStateZip = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                    '    End If
                    'End If
                    If Not (dr("ForeignZip") Is System.DBNull.Value) Then
                        If dr("Zip") <> "" And dr("ForeignZip") = False Then
                            'Domestic Zip
                            If cityStateZip <> "" Then
                                cityStateZip &= " " & facInputMasks.ApplyMask(zipMask, dr("Zip"))
                            Else
                                cityStateZip = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                            End If
                        Else
                            'International Zip
                            If dr("Zip") <> "" Then
                                If cityStateZip <> "" Then
                                    cityStateZip &= " " & dr("Zip")
                                Else
                                    cityStateZip &= dr("Zip")
                                End If
                            End If
                        End If
                    Else
                        'Domestic Zip
                        If dr("Zip") <> "" Then
                            If cityStateZip <> "" Then
                                cityStateZip &= " " & facInputMasks.ApplyMask(zipMask, dr("Zip"))
                            Else
                                cityStateZip = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                            End If
                        End If
                    End If
                End If
                '
                If cityStateZip <> "" Then
                    If dr("FullAddress") <> "" Then
                        dr("FullAddress") &= " " & cityStateZip
                    Else
                        dr("FullAddress") = cityStateZip
                    End If
                End If
                '
                'Apply mask.
                If Not (dr("WorkPhone") Is System.DBNull.Value) Then
                    'If dr("WorkPhone") <> "" Then
                    '    dr("WorkPhone") = facInputMasks.ApplyMask(strMask, dr("WorkPhone"))
                    'End If
                    If Not (dr("ForeignWorkPhone") Is System.DBNull.Value) Then
                        If dr("WorkPhone") <> "" And dr("ForeignWorkPhone") = False Then
                            'Domestic Phone
                            dr("WorkPhone") = facInputMasks.ApplyMask(strMask, dr("WorkPhone"))
                        Else
                            'International Phone
                            'dr("WorkPhone") = dr("WorkPhone")
                        End If
                    Else
                        'Domestic Phone
                        If dr("WorkPhone") <> "" Then
                            dr("WorkPhone") = facInputMasks.ApplyMask(strMask, dr("WorkPhone"))
                        End If
                    End If
                End If
                '
                'Apply mask.
                If Not (dr("HomePhone") Is System.DBNull.Value) Then
                    'If dr("HomePhone") <> "" Then
                    '    dr("HomePhone") = facInputMasks.ApplyMask(strMask, dr("HomePhone"))
                    'End If
                    If Not (dr("ForeignHomePhone") Is System.DBNull.Value) Then
                        If dr("HomePhone") <> "" And dr("ForeignHomePhone") = False Then
                            'Domestic Phone
                            dr("HomePhone") = facInputMasks.ApplyMask(strMask, dr("HomePhone"))
                        Else
                            'International Phone
                            'dr("HomePhone") = dr("HomePhone")
                        End If
                    Else
                        'Domestic Phone
                        If dr("HomePhone") <> "" Then
                            dr("HomePhone") = facInputMasks.ApplyMask(strMask, dr("HomePhone"))
                        End If
                    End If
                End If
                '
                'Apply mask.
                If Not (dr("CellPhone") Is System.DBNull.Value) Then
                    'If dr("CellPhone") <> "" Then
                    '    dr("CellPhone") = facInputMasks.ApplyMask(strMask, dr("CellPhone"))
                    'End If
                    If Not (dr("ForeignCellPhone") Is System.DBNull.Value) Then
                        If dr("CellPhone") <> "" And dr("ForeignCellPhone") = False Then
                            'Domestic Phone
                            dr("CellPhone") = facInputMasks.ApplyMask(strMask, dr("CellPhone"))
                        Else
                            'International Phone
                            'dr("CellPhone") = dr("CellPhone")
                        End If
                    Else
                        'Domestic Phone
                        If dr("CellPhone") <> "" Then
                            dr("CellPhone") = facInputMasks.ApplyMask(strMask, dr("CellPhone"))
                        End If
                    End If
                End If
                '
                'Apply mask.
                'If Not (dr("Beeper") Is System.DBNull.Value) Then
                '    If dr("Beeper") <> "" Then
                '        dr("Beeper") = facInputMasks.ApplyMask(strMask, dr("Beeper"))
                '    End If
                'End If
            Next

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region


End Class
