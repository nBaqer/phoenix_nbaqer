Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class FIC_C4CommonObject

	Public Shared Function PreProcessScores(ByVal dtStudentsRaw As DataTable, ByVal dtScoresRaw As DataTable) As DataTable
		Dim dtScoresProc As DataTable = dtScoresRaw.Copy

		With dtScoresProc
			Dim PKey(1) As DataColumn
			PKey(0) = .Columns("StudentId")
			PKey(1) = .Columns("TestDescrip")
			.PrimaryKey = PKey
		End With

		Dim PKeyVals(1) As Object

		Dim SAT1VerbalIdx As Integer, SAT1MathIdx As Integer
		Dim ACTCompIdx As Integer, ACTEnglishIdx As Integer, ACTMathIdx As Integer

		For Each drStudent As DataRow In dtStudentsRaw.Rows
			PKeyVals(0) = drStudent("StudentId")

			With dtScoresProc.Rows
				PKeyVals(1) = EntrTestSAT1Verbal
				SAT1VerbalIdx = .IndexOf(.Find(PKeyVals))
				PKeyVals(1) = EntrTestSAT1Math
				SAT1MathIdx = .IndexOf(.Find(PKeyVals))
				If SAT1VerbalIdx = -1 Or SAT1MathIdx = -1 Then
					If SAT1VerbalIdx <> -1 Then
						.RemoveAt(SAT1VerbalIdx)
					End If
					If SAT1MathIdx <> -1 Then
						.RemoveAt(SAT1MathIdx)
					End If
				End If

				PKeyVals(1) = EntrTestACTComp
				ACTCompIdx = .IndexOf(.Find(PKeyVals))
				PKeyVals(1) = EntrTestACTEnglish
				ACTEnglishIdx = .IndexOf(.Find(PKeyVals))
				PKeyVals(1) = EntrTestACTMath
				ACTMathIdx = .IndexOf(.Find(PKeyVals))
				If ACTCompIdx = -1 Or ACTEnglishIdx = -1 Or ACTMathIdx = -1 Then
					If ACTCompIdx <> -1 Then
						.RemoveAt(ACTCompIdx)
					End If
					If ACTEnglishIdx <> -1 Then
						.RemoveAt(ACTEnglishIdx)
					End If
					If ACTMathIdx <> -1 Then
						.RemoveAt(ACTMathIdx)
					End If
				End If
			End With
		Next

		Return dtScoresProc.Copy

	End Function

End Class
