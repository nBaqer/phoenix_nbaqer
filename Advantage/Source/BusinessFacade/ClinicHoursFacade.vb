Public Class ClinicHoursFacade
    Public Function GetAllCoursesByEnrollment(ByVal strStuEnrollId As String) As DataSet
        Dim ds As New DataSet
        ds = (New ClinicHoursDB).GetAllCoursesByEnrollment(strStuEnrollId)
        For Each dr As DataRow In ds.Tables(0).Rows
            dr("Descrip") = "(" & dr("Code") & ") " & dr("Descrip")
        Next
        Return ds
    End Function
    Public Function GetAllClinicServices(ByVal strStuEnrollId As String, Optional ByVal CourseId As String = "", Optional ByVal Courses As String = "All", Optional ByVal ClinicType As String = "LabHours") As DataSet
        Dim ds As New DataSet
        ds = (New ClinicHoursDB).GetAllClinicServices(strStuEnrollId, CourseId, Courses, ClinicType)

        For Each dr As DataRow In ds.Tables(0).Rows
            If dr("Completed") >= dr("Required") Then
                Select Case ClinicType
                    Case "Lab Hours"
                        dr("Remaining") = "0.0"
                    Case "Lab Work"
                        dr("Remaining") = "0"
                        dr("Completed") = CInt(dr("Completed"))
                End Select
            ElseIf dr("Completed") < dr("Required") Then
                Select Case ClinicType
                    Case "Lab Hours"
                        dr("Remaining") = (dr("Required") - dr("Completed")).ToString
                    Case "Lab Work"
                        dr("Remaining") = CInt(dr("Required") - dr("Completed")).ToString
                        dr("Completed") = CInt(dr("Completed"))
                End Select
            End If
        Next
        Return ds
    End Function
    Public Function GetAllEnrollments(ByVal Studentid As String) As DataSet
        Return (New ClinicHoursDB).GetAllEnrollments(Studentid)
    End Function
End Class
