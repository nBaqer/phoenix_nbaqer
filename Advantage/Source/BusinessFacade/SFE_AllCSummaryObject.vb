
Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllCSummaryObject
Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SFE_AllCSummary"

	Private dsRaw As New DataSet
	Private dtRptRaw As New DataTable
	Private dsRpt As New DataSet
	Private RptParamInfo As ReportParamInfoIPEDS

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SFE_AllCSummaryObjectDB.GetReportDataSetRaw(RptParamInfo)

		' save passed-in parameter info for use in processing
		Me.RptParamInfo = RptParamInfo

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		Dim drState As DataRow

		' pre-process raw data, first process student enrollment info, then process with raw student 
		'	data to obtain prepared raw data for processing
		dtRptRaw = SFE_AllCCommonObject.PreProcessRawData(dsRaw.Tables(TblNameStudents), IPEDSFacade.GetProcessedEnrollInfo(dsRaw))

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add("StateDescrip", GetType(String))
			.Add("FIPSCode", GetType(String))
			.Add("Count_FirstTime", GetType(Integer))
			.Add("Count_Grad", GetType(Integer))
		End With

		' get IPEDS States list, for use in processing
		Dim dtStates As DataTable = StatesFacade.GetStatesIPEDS

		' loop thru IPEDS states, and process matching records in raw dataset
		For Each drState In dtStates.Rows
			' no need to process rows for unknown/unreported residence, since this report shows
			'	"ED calculates" for this
			If drState("StateDescrip") <> StateResUnreportedDescrip Then
				ProcessRows(drState)
			End If
		Next

		' return report dataset
		Return dsRpt

	End Function

	Private Sub ProcessRows(ByVal drState As DataRow)
		Dim i As Integer
		Dim drRpt As DataRow
		Dim DateLastYear As Date = RptParamInfo.RptEndDate.AddYears(-1)

		' create dataview from students in raw dataset, filtered according to the passed in state description
		Dim dvRptRaw As New DataView(dtRptRaw, "StateDescrip LIKE '" & drState("StateDescrip") & "'", _
									 "", DataViewRowState.CurrentRows)

		' initialize counter for HS grads in last year
		Dim Count_Grad As Integer = 0

		' if student dataview has records, loop thru records and process raw data 
		For i = 0 To dvRptRaw.Count - 1
			If dvRptRaw(i)("LastEdInstType").ToString = SFE_AllCCommonObject.LastEdHS AndAlso _
			   Date.Compare(CDate(dvRptRaw(i)("LastEdGradDate")), DateLastYear) < 0 Then
				Count_Grad += 1
			End If
		Next

		' create new summary report row and add to report DataSet
		drRpt = dsRpt.Tables(MainTableName).NewRow

		' record the state description and FIPS code
		drRpt("StateDescrip") = drState("StateDescrip")
		drRpt("FIPSCode") = drState("FIPSCode")

		' record number of first-time students - will always be same as count of records in student raw 
		'	DataView, since every student in this report is a first-time student
		drRpt("Count_FirstTime") = dvRptRaw.Count

		' record the number HS grads in last year			
		drRpt("Count_Grad") = Count_Grad

		dsRpt.Tables(MainTableName).Rows.Add(drRpt)
	End Sub

End Class
