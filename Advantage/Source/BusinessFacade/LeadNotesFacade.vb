' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' LeadNotesFacade.vb
'
' LeadNotesFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class LeadNotesFacade

    Public Function GetLeadNoteInfo(ByVal LeadNoteId As String) As LeadNoteInfo

        '   Instantiate DAL component
        With New LeadNotesDB

            '   get the LeadNoteInfo
            Return .GetLeadNoteInfo(LeadNoteId)

        End With

    End Function
    Public Function UpdateLeadNoteInfo(ByVal LeadNoteInfo As LeadNoteInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New LeadNotesDB

            '   If it is a new account do an insert. If not, do an update
            If Not (LeadNoteInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddLeadNoteInfo(LeadNoteInfo, user)
            Else
                '   return integer with update results
                Return .UpdateLeadNoteInfo(LeadNoteInfo, user)
            End If

        End With

    End Function
    Public Function DeleteLeadNoteInfo(ByVal LeadNoteId As String, ByVal modDate As Date) As String

        '   Instantiate DAL component
        With New LeadNotesDB

            '   delete BankInfo ans return integer result
            Return .DeleteLeadNoteInfo(LeadNoteId, modDate)

        End With

    End Function
    Public Function GetNotesForLeadByModuleDS(ByVal LeadId As String, ByVal modCode As String, ByVal fromDate As Date, ByVal toDate As Date) As DataSet

        '   get dataset with LeadNoteInfo's
        Return (New LeadNotesDB).GetNotesForLeadByModuleDS(LeadId, modCode, fromDate, toDate)

    End Function
    Public Function GetMinAndMaxDatesFromNotes() As Date()

        '   return date Array
        Return (New LeadNotesDB).GetMinAndMaxDatesFromNotes()

    End Function
End Class
