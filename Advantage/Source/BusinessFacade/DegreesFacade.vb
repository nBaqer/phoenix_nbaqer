' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' DegreesFacade.vb
'
' DegreesFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class DegreesFacade
    Public Function GetAllDegrees() As DataSet

        '   Instantiate DAL component
        Dim degreesDB As New DegreesDB

        '   get the dataset with all degrees
        Return degreesDB.GetAllDegrees()

    End Function
End Class

