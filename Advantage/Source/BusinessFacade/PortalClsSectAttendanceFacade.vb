Imports System.Collections.Generic
Public Class PortalClsSectAttendanceFacade
    Public Function GetTerms(Optional ByVal campusId As String = "") As DataSet
        'Dim objClsSectAtt As New PortalClsSectAttendanceDB
        'Return objClsSectAtt.GetTerms 'commented for bug fix 4044

        '   Instantiate DAL component
        Dim DB As New PortalTermsDB
        '   get the dataset with all degrees
        Return DB.GetNoneFutureActiveTerms(campusId)

    End Function
    Public Function GetTermsStudentIsRegisteredFor(ByVal stuEnrollid As String) As DataSet
        Return (New PortalClsSectAttendanceDB).GetTermsStudentIsRegisteredFor(stuEnrollid)
    End Function

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''Optional Parameter isAcademicSupervisor Added by Saraswathi Lakshmanan to fix Issue 14296

    Public Function GetClsSectionsForTermForAttendance(ByVal termID As String, ByVal shiftID As String, ByVal asofclasstartdate As Date, ByVal campusId As String, ByVal instructorId As String, ByVal userName As String, Optional ByVal isAcademicAdvisor As Boolean = False) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Dim dt As New DataTable

        dt = objClsSectAtt.GetClsSectionsForTermForAttendance(termID, shiftID, asofclasstartdate, campusId, instructorId, userName, isAcademicAdvisor)
        'We need to concatenate the course and section of the class section
        Dim dc As DataColumn = dt.Columns.Add("Class", Type.GetType("System.String"))
        dc.AllowDBNull = True
        dc.Expression = "'(' + Code + ') ' + Descrip + ' - ' + ClsSection"

        Return dt
    End Function

    Public Function GetClsSectionDateRange(ByVal classSectionId As String) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Dim dateRange As New DataTable

        dateRange = objClsSectAtt.GetClsSectionDateRange(classSectionId)

        Return dateRange
    End Function

    Public Function GetStudentsInClsSection(ByVal clsSectionId As String, ByVal ClsSectmeetingID As String) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        Return objClsSectAtt.GetStudentsInClsSection(clsSectionId, ClsSectmeetingID)

    End Function

    Public Function GetClsSectionMeetingDays(ByVal clsSectionId As String, ByVal usePeriods As Boolean) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        Return objClsSectAtt.GetClsSectionMeetingDays(clsSectionId, usePeriods)
    End Function

    Public Function GetClsSectionMeetingDaysgivenClsSectionandMeeting(ByVal clsSectionId As String, ByVal ClsSectMeetingID As String, ByVal usePeriods As Boolean) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        Return objClsSectAtt.GetClsSectionMeetingDaysgivenClssectandmeeting(clsSectionId, ClsSectMeetingID, usePeriods)
    End Function

    Public Function GetClsSectionMeetingDays(ByVal StuEnrollId As String, ByVal TermId As String, ByVal usePeriods As Boolean) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        Return objClsSectAtt.GetClsSectionMeetingDays(StuEnrollId, TermId, usePeriods)
    End Function
    Public Function GetStudentsAttendanceForClsSection(ByVal clsSectionId As String, ByVal startDate As DateTime, ByVal endDate As DateTime) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        Return objClsSectAtt.GetStudentsAttendanceForClsSection(clsSectionId, startDate, endDate)
    End Function
    Public Function GetStudentsAttendanceForClsSectionGivenClsSectionandMeeting(ByVal clsSectionId As String, ByVal ClsSectmeetingID As String, _
                                                                                ByVal startDate As DateTime, ByVal endDate As DateTime, _
                                                                                Optional ByVal CampusId As String = "") As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        Return objClsSectAtt.GetStudentsAttendanceForClsSectionGivenClsSectandMeeting(clsSectionId, ClsSectmeetingID, startDate, endDate, CampusId)
    End Function
    Public Function GetStudentAttendance(ByVal StuEnrollId As String, ByVal clsSectionId As String, ByVal TermId As String) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        Return objClsSectAtt.GetStudentAttendance(StuEnrollId, clsSectionId, TermId)
    End Function

    Public Function GetDatesArrayListFromRange(ByVal startDate As String, ByVal endDate As String) As ArrayList
        Dim arrDates As New ArrayList
        Dim dtm As DateTime
        Dim counter As Integer

        'Add the start date to the arraylist
        dtm = Convert.ToDateTime(startDate)
        arrDates.Add(dtm)

        If Convert.ToDateTime(startDate) <> Convert.ToDateTime(endDate) Then
            'Use a loop to determine the other dates to be added.
            For counter = 1 To 365
                dtm = dtm.AddDays(1)
                arrDates.Add(dtm)

                If dtm = Convert.ToDateTime(endDate) Then
                    Exit For
                End If
            Next
        End If
        Return arrDates

    End Function

    Public Function GetShortMonthName(ByVal dtm As DateTime) As String
        Select Case dtm.Month
            Case 1
                Return "Jan"
            Case 2
                Return "Feb"
            Case 3
                Return "Mar"
            Case 4
                Return "Apr"
            Case 5
                Return "May"
            Case 6
                Return "Jun"
            Case 7
                Return "Jul"
            Case 8
                Return "Aug"
            Case 9
                Return "Sep"
            Case 10
                Return "Oct"
            Case 11
                Return "Nov"
            Case 12
                Return "Dec"
        End Select
    End Function

    Public Function GetShortDayName(ByVal dtm As DateTime) As String
        Select Case dtm.DayOfWeek
            Case DayOfWeek.Sunday
                Return "Sun"
            Case DayOfWeek.Monday
                Return "Mon"
            Case DayOfWeek.Tuesday
                Return "Tue"
            Case DayOfWeek.Wednesday
                Return "Wed"
            Case DayOfWeek.Thursday
                Return "Thu"
            Case DayOfWeek.Friday
                Return "Fri"
            Case DayOfWeek.Saturday
                Return "Sat"
        End Select
    End Function

    Public Overloads Sub AddStudentClsSectionAttendanceRecord(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal meetDate As DateTime, ByVal actual As Decimal, tardy As Boolean)
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        objClsSectAtt.AddStudentClsSectionAttendanceRecord(stuEnrollId, clsSectionId, meetDate, actual, tardy)

    End Sub

    Public Overloads Sub AddStudentClsSectionAttendanceRecord(ByVal stuEnrollId As String, ByVal clsSectionId As String, _
           ByVal meetDate As DateTime, ByVal actual As Decimal, ByVal tardy As Boolean, ByVal excused As Boolean, Optional scheduled As Integer = 0, Optional ClsSectMeetingId As String = "")
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        objClsSectAtt.AddStudentClsSectionAttendanceRecord(stuEnrollId, clsSectionId, meetDate, actual, tardy, excused, scheduled, ClsSectMeetingId)
    End Sub

    Public Overloads Sub UpdateStudentClsSectionAttendanceRecord(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal meetDate As DateTime, ByVal actual As Decimal, tardy As Boolean)
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        objClsSectAtt.UpdateStudentClsSectionAttendanceRecord(stuEnrollId, clsSectionId, meetDate, actual, tardy)

    End Sub

    Public Overloads Sub UpdateStudentClsSectionAttendanceRecord(ByVal stuEnrollId As String, _
        ByVal clsSectionId As String, ByVal meetDate As DateTime, ByVal actual As Decimal, _
        ByVal tardy As Boolean, ByVal excused As Boolean, Optional ClsSectMeetingId As String = "")

        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        objClsSectAtt.UpdateStudentClsSectionAttendanceRecord(stuEnrollId, clsSectionId, meetDate, actual, tardy, excused, ClsSectMeetingId)
    End Sub


    Public Sub DeleteStudentClsSectionAttendanceRecord(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal meetDate As DateTime, Optional ClsSectMeetingId As String = "")
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        objClsSectAtt.DeleteStudentClsSectionAttendanceRecord(stuEnrollId, clsSectionId, meetDate, ClsSectMeetingId)
    End Sub

    Public Function GetClassSectionAttendanceType(ByVal clsSectionId As String) As String
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        Return objClsSectAtt.GetClassSectionAttendanceType(clsSectionId)

    End Function

    Public Function IsMeetDateUnposted(ByVal clsSectionId As String, ByVal meetDate As DateTime) As Boolean
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        Return objClsSectAtt.IsMeetDateUnposted(clsSectionId, meetDate)
    End Function

    Public Function IsDateHoliday(ByVal dtm As DateTime) As Boolean
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        Return objClsSectAtt.IsDateHoliday(dtm)

    End Function

    Public Function GetScheduledMinutesForMeetDay(ByVal clsSectionId As String, ByVal meetDate As DateTime) As String

    End Function

    Public Function IsMeetDay(ByVal dtMeetDays As DataTable, ByVal dtm As DateTime) As Boolean
        Dim dr As DataRow
        Dim sDay As String

        'Get the short day value
        sDay = GetShortDayName(dtm)

        If dtMeetDays.Rows.Count = 0 Then
            Return False
        Else
            For Each dr In dtMeetDays.Rows
                If dr("WorkDaysDescrip").ToString() = sDay Then
                    Return True
                End If
            Next
            Return False
        End If
    End Function

    Public Function IsMeetDay(ByVal MeetDays() As DataRow, ByVal dtm As DateTime) As Boolean
        Dim dr As DataRow
        Dim sDay As String

        'Get the short day value
        sDay = GetShortDayName(dtm)

        If MeetDays.GetLength(0) = 0 Then
            Return False
        Else
            For Each dr In MeetDays
                If dr("WorkDaysDescrip").ToString() = sDay Then
                    'dtm = CDate(dtm.Date & " " & CType(dr("TimeIntervalDescrip"), Date).TimeOfDay.ToString)
                    Return True
                End If
            Next
            Return False
        End If
    End Function

    Public Function GetCancelDays(ByVal ClsSectionId As String) As ArrayList
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Dim dtCancelDays As DataTable = objClsSectAtt.GetCancelDays(ClsSectionId)

        Dim arrDates As New ArrayList
        Dim rtn As Boolean = False
        Dim Index As Integer = 0
        For Index = 0 To dtCancelDays.Rows.Count - 1
            Dim dr As DataRow = dtCancelDays.Rows(Index)
            Dim totalDays As Integer = DateDiff(DateInterval.Day, dr("StartDate"), dr("EndDate"))
            Dim sDate As Date = dr("StartDate")
            arrDates.Add(sDate)
            Dim i As Integer = 0
            For i = 1 To totalDays
                arrDates.Add(DateAdd(DateInterval.Day, i, sDate))
            Next

        Next
        Return arrDates


    End Function
    Public Function GetCancelDaysForMeeting(ByVal ClsMeetingId As String) As ArrayList
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Dim dtCancelDays As DataTable = objClsSectAtt.GetCancelDaysForMeeting(ClsMeetingId)

        'Dim arrReScheduleDays As ArrayList = GetReScheduleDaysForMeeting(ClsMeetingId)
        Dim arrDates As New ArrayList
        Dim rtn As Boolean = False
        Dim Index As Integer = 0
        For Index = 0 To dtCancelDays.Rows.Count - 1
            Dim dr As DataRow = dtCancelDays.Rows(Index)
            Dim totalDays As Integer = DateDiff(DateInterval.Day, dr("StartDate"), dr("EndDate"))
            Dim sDate As Date = dr("StartDate")
            sDate = sDate.Date
            'arrDates.Add(sDate)
            Dim i As Integer = 0
            For i = 0 To totalDays
                'If Not CheckReSCheduleDate(arrReScheduleDays, sDate) Then
                arrDates.Add(DateAdd(DateInterval.Day, i, sDate))
                'End If
            Next

        Next
        Return arrDates


    End Function

    Public Function GetReScheduleDaysForMeeting(ByVal ClsMeetingId As String) As ArrayList
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Dim dtReScheduleDays As DataTable = objClsSectAtt.GetReScheduleDaysForMeeting(ClsMeetingId)

        Dim arrDates As New ArrayList
        Dim rtn As Boolean = False
        Dim Index As Integer = 0
        For Index = 0 To dtReScheduleDays.Rows.Count - 1
            Dim dr As DataRow = dtReScheduleDays.Rows(Index)
            'Dim totalDays As Integer = DateDiff(DateInterval.Day, dr("StartDate"), dr("EndDate"))
            'Dim sDate As Date = dr("StartDate")
            'sDate = sDate.Date
            ''arrDates.Add(sDate)
            'Dim i As Integer = 0
            'For i = 0 To totalDays
            'arrDates.Add(DateAdd(DateInterval.Day, i, sDate))
            'Next
            arrDates.Add(dr("ClassReSchduledTo"))
        Next
        Return arrDates


    End Function

    Public Function GetCancelDays_byClass(ByVal ClsSectionId As String, ByVal ClsSectMeetingID As String, ByVal CampusID As String) As ArrayList
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Dim dtCancelDays As DataTable = objClsSectAtt.GetCancelDays_byClass(ClsSectionId, ClsSectMeetingID)
        Dim objTimeClock As FAME.AdvantageV1.DataAccess.TimeClock.PortalTimeClockDB
        Dim StartTime As String
        StartTime = (objTimeClock).GetStartTimeForClassSectionMeeting_byClass(ClsSectMeetingID, CampusID)
        Dim arrDates As New ArrayList
        Dim rtn As Boolean = False
        Dim cancelDate As DateTime
        Dim Index As Integer = 0
        For Index = 0 To dtCancelDays.Rows.Count - 1
            Dim dr As DataRow = dtCancelDays.Rows(Index)
            Dim totalDays As Integer = DateDiff(DateInterval.Day, dr("StartDate"), dr("EndDate"))
            Dim sDate As Date = dr("StartDate")
            arrDates.Add(DateTime.Parse(Format(sDate, "MM/dd/yyyy") + " " + StartTime))
            Dim i As Integer = 0
            For i = 1 To totalDays
                cancelDate = DateTime.Parse(Format(DateAdd(DateInterval.Day, i, sDate), "MM/dd/yyyy") + " " + StartTime)
                arrDates.Add(cancelDate)
            Next

        Next
        Return arrDates


    End Function


    Public Function CheckCancellDate(ByVal arrCancelDates As ArrayList, ByVal dtm As Date) As Boolean
        Dim rtn As Boolean = False
        Dim i As Integer = 0
        If (arrCancelDates.Count = 0) Then
            rtn = False
        Else
            For i = 0 To arrCancelDates.Count - 1
                If (arrCancelDates(i) = dtm) Then
                    rtn = True
                    Exit For
                End If
            Next
        End If

        Return rtn

    End Function
    Public Function CheckReSCheduleDate(ByVal arrReSCheduleDates As ArrayList, ByVal cancelDate As Date) As Boolean
        Dim rtn As Boolean = False
        Dim i As Integer = 0
        If (arrReSCheduleDates.Count = 0) Then
            rtn = False
        Else
            For i = 0 To arrReSCheduleDates.Count - 1
                If (arrReSCheduleDates(i) = cancelDate) Then
                    rtn = True
                    Exit For
                End If
            Next
        End If

        Return rtn

    End Function

    Public Function CheckCancellDate_ByClass(ByVal arrCancelDates As ArrayList, ByVal dtm As DateTime) As Boolean
        Dim rtn As Boolean = False
        Dim i As Integer = 0
        If (arrCancelDates.Count = 0) Then
            rtn = False
        Else
            For i = 0 To arrCancelDates.Count - 1
                If (arrCancelDates(i) = dtm) Then
                    rtn = True
                    Exit For
                End If
            Next
        End If

        Return rtn

    End Function

    Public Function CheckDateAdded(ByVal arrCancelDates As ArrayList, ByVal dtm As Date) As Boolean
        Dim meetingtime As Date = dtm.Date.ToString
        Dim rtn As Boolean = False
        Dim i As Integer = 0
        If (arrCancelDates.Count = 0) Then
            rtn = False
        Else
            For i = 0 To arrCancelDates.Count - 1
                If (CDate(arrCancelDates(i)).ToString("d") = meetingtime) Then
                    rtn = True
                    Exit For
                End If
            Next
        End If

        Return rtn

    End Function
    'meeting.MeetingDateAndTime.Date.ToString

    Public Function GetCollectionOfHolidayDates() As System.Collections.Generic.List(Of AdvantageHoliday)
        Return (New PortalHolidaysDB).GetCollectionOfHolidayDates()
    End Function



    Public Function GetCollectionOfHolidayDateswithCampusID(ByVal CampusID As String) As System.Collections.Generic.List(Of AdvantageHoliday)
        Return (New PortalHolidaysDB).GetCollectionOfHolidayDates(CampusID)
    End Function


    Public Function GetMeetDatesArrayListFromRange(ByVal dtMeetDays As DataTable, ByVal ClsSectionId As String, _
                                            ByVal StartDate As String, ByVal EndDate As String, _
                                            Optional ByVal PostByException As Boolean = False, _
                                            Optional ByVal CampusId As String = "", Optional ByVal InstructionTypeID As String = "") As ArrayList
        Dim arrDates As New ArrayList
        Dim arrCancelDates As ArrayList
        Dim dtm As DateTime
        Dim counter As Integer
        Dim Rows() As DataRow
        Dim dtmWithTime As DateTime
        'Add the start date to the arraylist if it is a meet day. If post by exception is selected
        'then we will only add the date if it is unposted.

        arrCancelDates = GetCancelDays(ClsSectionId)

        If Convert.ToDateTime(StartDate) <= Convert.ToDateTime(EndDate) Then
            dtm = Convert.ToDateTime(StartDate)

            'get collection of holidays
            Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New PortalHolidaysDB).GetCollectionOfHolidayDates(CampusId)
            If InstructionTypeID = "" Then
                Rows = dtMeetDays.Select("ClsSectionId = '" & ClsSectionId & "'")
            Else
                Rows = dtMeetDays.Select("ClsSectionId = '" & ClsSectionId & "' AND InstructionTypeID='" & InstructionTypeID & "'")
            End If


            'If IsMeetDay(Rows, dtm) And Not IsHoliday(dtm, collectionOfHolidays) Then
            If Not IsHoliday(dtm, collectionOfHolidays) Then
                If PostByException Then
                    If IsMeetDateUnposted(ClsSectionId, dtm) Then
                        If Not CheckCancellDate(arrCancelDates, dtm) Then
                            dtmWithTime = Convert.ToDateTime(dtm & " " & GetStartTimeForMeetDate(Rows, dtm))
                            arrDates.Add(dtmWithTime)
                        End If
                    End If
                Else
                    If Not CheckCancellDate(arrCancelDates, dtm) Then
                        dtmWithTime = Convert.ToDateTime(dtm & " " & GetStartTimeForMeetDate(Rows, dtm))
                        arrDates.Add(dtmWithTime)
                    End If
                End If
            End If

            If Convert.ToDateTime(StartDate) <> Convert.ToDateTime(EndDate) Then
                'Use a loop to determine the other dates to be added.
                'We will only add a date if it is a meet date. In addition, if post by exception is
                'selected we will only add unposted days - days for which there are no attendance records.
                For counter = 1 To 365
                    dtm = dtm.AddDays(1)
                    'If IsMeetDay(Rows, dtm) And Not IsHoliday(dtm, collectionOfHolidays) Then
                    If Not IsHoliday(dtm, collectionOfHolidays) Then
                        If PostByException Then
                            If IsMeetDateUnposted(ClsSectionId, dtm) Then
                                If Not CheckCancellDate(arrCancelDates, dtm) Then
                                    dtmWithTime = Convert.ToDateTime(dtm & " " & GetStartTimeForMeetDate(Rows, dtm))
                                    arrDates.Add(dtmWithTime)
                                End If
                            End If
                        Else
                            If Not CheckCancellDate(arrCancelDates, dtm) Then
                                dtmWithTime = Convert.ToDateTime(dtm & " " & GetStartTimeForMeetDate(Rows, dtm))
                                arrDates.Add(dtmWithTime)
                            End If
                        End If

                    End If

                    If dtm = Convert.ToDateTime(EndDate) Then
                        Exit For
                    End If
                Next
            End If
        End If

        Return arrDates
    End Function
    Public Function GetMeetDatesArrayListFromRange(ByVal dtMeetDays As DataTable, ByVal ClsSectionId As String, _
                                            ByVal StartDate As String, ByVal EndDate As String, campusid As String) As ArrayList
        Dim arrDates As New ArrayList
        Dim arrCancelDates As ArrayList
        Dim dtm As DateTime
        Dim counter As Integer
        Dim Rows() As DataRow
        Dim dtmWithTime As DateTime
        'Add the start date to the arraylist if it is a meet day. If post by exception is selected
        'then we will only add the date if it is unposted.

        arrCancelDates = GetCancelDays(ClsSectionId)

        If Convert.ToDateTime(StartDate) <= Convert.ToDateTime(EndDate) Then
            dtm = Convert.ToDateTime(StartDate)

            'get collection of holidays
            Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New PortalHolidaysDB).GetCollectionOfHolidayDates(campusid)

            Rows = dtMeetDays.Select("ClsSectionId = '" & ClsSectionId & "'")



            'If IsMeetDay(Rows, dtm) And Not IsHoliday(dtm, collectionOfHolidays) Then
            If Not IsHoliday(dtm, collectionOfHolidays) Then

                If Not CheckCancellDate(arrCancelDates, dtm) Then
                    dtmWithTime = Convert.ToDateTime(dtm & " " & GetStartTimeForMeetDate(Rows, dtm))
                    arrDates.Add(dtmWithTime)
                End If

            End If

            If Convert.ToDateTime(StartDate) <> Convert.ToDateTime(EndDate) Then
                'Use a loop to determine the other dates to be added.
                'We will only add a date if it is a meet date. In addition, if post by exception is
                'selected we will only add unposted days - days for which there are no attendance records.
                For counter = 1 To 365
                    dtm = dtm.AddDays(1)
                    'If IsMeetDay(Rows, dtm) And Not IsHoliday(dtm, collectionOfHolidays) Then
                    If Not IsHoliday(dtm, collectionOfHolidays) Then

                        If Not CheckCancellDate(arrCancelDates, dtm) Then
                            dtmWithTime = Convert.ToDateTime(dtm & " " & GetStartTimeForMeetDate(Rows, dtm))
                            arrDates.Add(dtmWithTime)
                        End If
                    End If



                    If dtm = Convert.ToDateTime(EndDate) Then
                        Exit For
                    End If
                Next
            End If
        End If

        Return arrDates
    End Function

    Public Function GetMeetDatesForMeetingsArrayListFromRange(ByVal dtMeetDays As DataRow(), ByVal ClsSectionId As String, _
                                           ClsSectMeetingID As String, StartDate As String, ByVal EndDate As String, _
                                            Optional ByVal CampusId As String = "") As ArrayList
        Dim arrDates As New ArrayList
        Dim arrCancelDates As ArrayList
        Dim dtm As DateTime
        Dim counter As Integer
        Dim Rows() As DataRow
        Dim dtmWithTime As DateTime
        'Add the start date to the arraylist if it is a meet day. If post by exception is selected
        'then we will only add the date if it is unposted.

        arrCancelDates = GetCancelDays(ClsSectionId)

        If Convert.ToDateTime(StartDate) <= Convert.ToDateTime(EndDate) Then
            dtm = Convert.ToDateTime(StartDate)

            'get collection of holidays
            Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New PortalHolidaysDB).GetCollectionOfHolidayDates(CampusId)

            Rows = dtMeetDays.CopyToDataTable().Select("ClsSectionId = '" & ClsSectionId & "' and ClsSectMeetingId = '" & ClsSectMeetingID & "'")



            'If IsMeetDay(Rows, dtm) And Not IsHoliday(dtm, collectionOfHolidays) Then
            If Not IsHoliday(dtm, collectionOfHolidays) Then

                If Not CheckCancellDate(arrCancelDates, dtm) Then
                    dtmWithTime = Convert.ToDateTime(dtm & " " & GetStartTimeForMeetDate(Rows, dtm))
                    arrDates.Add(dtmWithTime)
                End If

            End If

            If Convert.ToDateTime(StartDate) <> Convert.ToDateTime(EndDate) Then
                'Use a loop to determine the other dates to be added.
                'We will only add a date if it is a meet date. In addition, if post by exception is
                'selected we will only add unposted days - days for which there are no attendance records.
                For counter = 1 To 365
                    dtm = dtm.AddDays(1)
                    'If IsMeetDay(Rows, dtm) And Not IsHoliday(dtm, collectionOfHolidays) Then
                    'If Not IsHoliday(dtm, collectionOfHolidays) Then

                    If Not CheckCancellDate(arrCancelDates, dtm) Then
                        If Not IsHoliday(dtm, collectionOfHolidays) Then
                            dtmWithTime = Convert.ToDateTime(dtm & " " & GetStartTimeForMeetDate(Rows, dtm))
                            arrDates.Add(dtmWithTime)
                        End If

                    End If


                    'End If

                    If dtm = Convert.ToDateTime(EndDate) Then
                        Exit For
                    End If
                Next
            End If
        End If

        Return arrDates
    End Function
    Public Function GetMeetDatesArrayListFromRange_New(ByVal dtMeetDays As DataTable, ByVal ClsSectionId As String, _
                                           ByVal StartDate As String, ByVal EndDate As String, _
                                           Optional ByVal PostByException As Boolean = False, _
                                           Optional ByVal CampusId As String = "", Optional ByVal InstructionTypeID As String = "") As ArrayList
        Dim arrDates As New ArrayList
        Dim arrCancelDates As ArrayList
        Dim dtm As DateTime
        Dim counter As Integer
        Dim Rows() As DataRow

        'Add the start date to the arraylist if it is a meet day. If post by exception is selected
        'then we will only add the date if it is unposted.

        arrCancelDates = GetCancelDays(ClsSectionId)

        If Convert.ToDateTime(StartDate) <= Convert.ToDateTime(EndDate) Then
            dtm = Convert.ToDateTime(StartDate)

            'get collection of holidays
            Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New PortalHolidaysDB).GetCollectionOfHolidayDates(CampusId)
            If InstructionTypeID = "" Then
                Rows = dtMeetDays.Select("ClsSectionId = '" & ClsSectionId & "'")
            Else
                Rows = dtMeetDays.Select("ClsSectionId = '" & ClsSectionId & "' AND InstructionTypeID='" & InstructionTypeID & "'")
            End If


            If IsMeetDay(Rows, dtm) And Not IsHoliday(dtm, collectionOfHolidays) Then
                If PostByException Then
                    If IsMeetDateUnposted(ClsSectionId, dtm) Then
                        If Not CheckCancellDate(arrCancelDates, dtm) Then
                            arrDates.Add(dtm)
                        End If
                    End If
                Else
                    If Not CheckCancellDate(arrCancelDates, dtm) Then
                        arrDates.Add(dtm)
                    End If
                End If
            End If

            If Convert.ToDateTime(StartDate) <> Convert.ToDateTime(EndDate) Then
                'Use a loop to determine the other dates to be added.
                'We will only add a date if it is a meet date. In addition, if post by exception is
                'selected we will only add unposted days - days for which there are no attendance records.
                For counter = 1 To 365
                    dtm = dtm.AddDays(1)
                    If IsMeetDay(Rows, dtm) And Not IsHoliday(dtm, collectionOfHolidays) Then
                        If PostByException Then
                            If IsMeetDateUnposted(ClsSectionId, dtm) Then
                                If Not CheckCancellDate(arrCancelDates, dtm) Then
                                    arrDates.Add(dtm)
                                End If
                            End If
                        Else
                            If Not CheckCancellDate(arrCancelDates, dtm) Then
                                arrDates.Add(dtm)
                            End If
                        End If

                    End If

                    If dtm = Convert.ToDateTime(EndDate) Then
                        Exit For
                    End If
                Next
            End If
        End If

        Return arrDates
    End Function

    Public Function NewGetMeetDatesArrayListFromRange(ByVal dtMeetDays As DataTable, ByVal ClsSectionId As String, _
                                          ByVal StartDate As String, ByVal EndDate As String, _
                                          Optional ByVal PostByException As Boolean = False, _
                                          Optional ByVal CampusId As String = "") As ArrayList
        Dim arrDates As New ArrayList
        Dim arrCancelDates As ArrayList
        Dim dtm As DateTime
        Dim counter As Integer
        Dim Rows() As DataRow

        'Add the start date to the arraylist if it is a meet day. If post by exception is selected
        'then we will only add the date if it is unposted.

        arrCancelDates = GetCancelDays(ClsSectionId)

        If Convert.ToDateTime(StartDate) <= Convert.ToDateTime(EndDate) Then
            '  dtm = Convert.ToDateTime(StartDate)
            dtm = Date.Parse(StartDate + " " + CDate(dtMeetDays.Rows(0)("TimeIntervalDescrip")).TimeOfDay.ToString)
            'get collection of holidays
            Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New PortalHolidaysDB).GetCollectionOfHolidayDates(CampusId)

            ''  Rows = dtMeetDays.Select("ClsSectionId = '" & ClsSectionId & "' AND InstructionTypeID='" & InstructionTypeID & "'")
            Rows = dtMeetDays.Select()

            If IsMeetDay(Rows, dtm) And Not IsHoliday(dtm, collectionOfHolidays) Then
                If PostByException Then
                    If IsMeetDateUnposted(ClsSectionId, dtm) Then
                        If Not CheckCancellDate(arrCancelDates, dtm) Then
                            arrDates.Add(dtm)
                        End If
                    End If
                Else
                    If Not CheckCancellDate(arrCancelDates, dtm) Then
                        arrDates.Add(dtm)
                    End If
                End If
            End If

            If Convert.ToDateTime(StartDate) <> Convert.ToDateTime(EndDate) Then
                'Use a loop to determine the other dates to be added.
                'We will only add a date if it is a meet date. In addition, if post by exception is
                'selected we will only add unposted days - days for which there are no attendance records.
                For counter = 1 To 365
                    dtm = dtm.AddDays(1)
                    If IsMeetDay(Rows, dtm) And Not IsHoliday(dtm, collectionOfHolidays) Then
                        If PostByException Then
                            If IsMeetDateUnposted(ClsSectionId, dtm) Then
                                If Not CheckCancellDate(arrCancelDates, dtm) Then
                                    arrDates.Add(dtm)
                                End If
                            End If
                        Else
                            If Not CheckCancellDate(arrCancelDates, dtm) Then
                                arrDates.Add(dtm)
                            End If
                        End If

                    End If

                    If dtm = Convert.ToDateTime(EndDate) Then
                        Exit For
                    End If
                Next
            End If
        End If

        Return arrDates
    End Function


    Public Function NewGetMeetDatesArrayListFromRange_Dict(ByVal dtMeetDays As DataTable, ByVal ClsSectionId As String, ByVal ClsSectMeetingID As String, _
                                         ByVal StartDate As String, ByVal EndDate As String, _
                                         Optional ByVal PostByException As Boolean = False, _
                                         Optional ByVal CampusId As String = "") As Dictionary(Of Date, Date)

        Dim arrDates As New Dictionary(Of Date, Date)
        Dim arrCancelDates As ArrayList
        Dim dtm As DateTime
        Dim counter As Integer
        Dim Rows() As DataRow

        'Add the start date to the arraylist if it is a meet day. If post by exception is selected
        'then we will only add the date if it is unposted.

        arrCancelDates = GetCancelDays_byClass(ClsSectionId, ClsSectMeetingID, CampusId)

        If Convert.ToDateTime(StartDate) <= Convert.ToDateTime(EndDate) Then
            ' dtm = Convert.ToDateTime(StartDate)
            dtm = Date.Parse(StartDate + " " + CDate(dtMeetDays.Rows(0)("TimeIntervalDescrip")).TimeOfDay.ToString)

            'get collection of holidays
            Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New PortalHolidaysDB).GetCollectionOfHolidayDates(CampusId)

            ''  Rows = dtMeetDays.Select("ClsSectionId = '" & ClsSectionId & "' AND InstructionTypeID='" & InstructionTypeID & "'")
            Rows = dtMeetDays.Select()

            If IsMeetDay(Rows, dtm) And Not IsHoliday(dtm, collectionOfHolidays) Then
                If PostByException Then
                    If IsMeetDateUnposted(ClsSectionId, dtm) Then
                        If Not CheckCancellDate(arrCancelDates, dtm) Then
                            arrDates.Add(dtm, dtm)
                        End If
                    End If
                Else
                    If Not CheckCancellDate(arrCancelDates, dtm) Then
                        arrDates.Add(dtm, dtm)
                    End If
                End If
            End If

            If Convert.ToDateTime(StartDate) <> Convert.ToDateTime(EndDate) Then
                'Use a loop to determine the other dates to be added.
                'We will only add a date if it is a meet date. In addition, if post by exception is
                'selected we will only add unposted days - days for which there are no attendance records.
                For counter = 1 To 365
                    dtm = dtm.AddDays(1)
                    If IsMeetDay(Rows, dtm) And Not IsHoliday(dtm, collectionOfHolidays) Then
                        If PostByException Then
                            If IsMeetDateUnposted(ClsSectionId, dtm) Then
                                If Not CheckCancellDate(arrCancelDates, dtm) Then
                                    arrDates.Add(dtm, dtm)
                                End If
                            End If
                        Else
                            If Not CheckCancellDate(arrCancelDates, dtm) Then
                                arrDates.Add(dtm, dtm)
                            End If
                        End If

                    End If

                    If dtm = Convert.ToDateTime(EndDate) Then
                        Exit For
                    End If
                Next
            End If
        End If

        Return arrDates
    End Function


    Public Function GetMeetDatesArrayListFromRangeGiveClsSectandMeeting(ByVal dtMeetDays As DataTable, ByVal ClsSectionId As String, ByVal ClsSectMeetingID As String,
                                           ByVal StartDate As String, ByVal EndDate As String, _
                                           Optional ByVal PostByException As Boolean = False, _
                                           Optional ByVal CampusId As String = "", Optional ByVal InstructionTypeID As String = "") As ArrayList
        Dim arrDates As New ArrayList
        Dim arrCancelDates As ArrayList
        Dim dtm As DateTime
        Dim counter As Integer
        Dim Rows() As DataRow

        'Add the start date to the arraylist if it is a meet day. If post by exception is selected
        'then we will only add the date if it is unposted.

        arrCancelDates = GetCancelDays(ClsSectionId)

        If Convert.ToDateTime(StartDate) <= Convert.ToDateTime(EndDate) Then
            dtm = Convert.ToDateTime(StartDate)

            'get collection of holidays
            Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New PortalHolidaysDB).GetCollectionOfHolidayDates(CampusId)

            Rows = dtMeetDays.Select("ClsSectionId = '" & ClsSectionId & "' AND InstructionTypeID='" & InstructionTypeID & "'")

            If IsMeetDay(Rows, dtm) And Not IsHoliday(dtm, collectionOfHolidays) Then
                If PostByException Then
                    If IsMeetDateUnposted(ClsSectionId, dtm) Then
                        If Not CheckCancellDate(arrCancelDates, dtm) Then
                            arrDates.Add(dtm)
                        End If
                    End If
                Else
                    If Not CheckCancellDate(arrCancelDates, dtm) Then
                        arrDates.Add(dtm)
                    End If
                End If
            End If

            If Convert.ToDateTime(StartDate) <> Convert.ToDateTime(EndDate) Then
                'Use a loop to determine the other dates to be added.
                'We will only add a date if it is a meet date. In addition, if post by exception is
                'selected we will only add unposted days - days for which there are no attendance records.
                For counter = 1 To 365
                    dtm = dtm.AddDays(1)
                    If IsMeetDay(Rows, dtm) And Not IsHoliday(dtm, collectionOfHolidays) Then
                        If PostByException Then
                            If IsMeetDateUnposted(ClsSectionId, dtm) Then
                                If Not CheckCancellDate(arrCancelDates, dtm) Then
                                    arrDates.Add(dtm)
                                End If
                            End If
                        Else
                            If Not CheckCancellDate(arrCancelDates, dtm) Then
                                arrDates.Add(dtm)
                            End If
                        End If

                    End If

                    If dtm = Convert.ToDateTime(EndDate) Then
                        Exit For
                    End If
                Next
            End If
        End If

        Return arrDates
    End Function


    Private Function IsHoliday(ByVal d As Date, ByVal collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday)) As Boolean
        For Each holiday As AdvantageHoliday In collectionOfHolidays
            If System.Math.Abs(d.Subtract(holiday.HolidayDateAndTime).TotalDays) < 1 Then Return True
        Next
        Return False
    End Function
    Public Function GetMeetDatesArrayListFromRange(ByVal ClsSectionId As String, ByVal StartDate As String, ByVal EndDate As String, Optional ByVal PostByException As Boolean = False, Optional ByVal CampusId As String = "", Optional ByVal InstructionTypeID As String = "") As ArrayList

        'this is the collection of AdvantageClasssectionMeetings
        Dim acsm As System.Collections.Generic.List(Of AdvantageClassSectionMeeting)
        If InstructionTypeID = "" Then
            acsm = (New PortalPeriodFacade).GetCollectionOfMeetingDates(ClsSectionId, CampusId)

        Else
            acsm = (New PortalPeriodFacade).GetCollectionOfMeetingDatesWithCourseTypeID(ClsSectionId, CampusId, InstructionTypeID)

        End If

        'this is the array list to be filled with dates
        Dim arrDates As ArrayList = New ArrayList()
        Dim arrCancelDates As ArrayList = GetCancelDays(ClsSectionId)

        'filter all class meetings by start and end date
        Dim sd As Date = Date.Parse(StartDate)
        Dim ed As Date = Date.Parse(EndDate).Add(New TimeSpan(23, 59, 59))
        For Each meeting As AdvantageClassSectionMeeting In acsm
            If meeting.MeetingDateAndTime.ToShortDateString >= sd And meeting.MeetingDateAndTime.ToShortDateString <= ed Then
                If Not CheckCancellDate(arrCancelDates, meeting.MeetingDateAndTime.Date.ToString) Then
                    arrDates.Add(meeting.MeetingDateAndTime)
                End If
            End If
        Next

        'return array of dates
        Return arrDates
    End Function


    Public Function GetMeetDatesArrayListFromRange_New(ByVal ClsSectionId As String, ByVal StartDate As String, ByVal EndDate As String, Optional ByVal PostByException As Boolean = False, Optional ByVal CampusId As String = "", Optional ByVal InstructionTypeID As String = "") As ArrayList

        'this is the collection of AdvantageClasssectionMeetings
        Dim acsm As System.Collections.Generic.List(Of AdvantageClassSectionMeeting)
        If InstructionTypeID = "" Then
            acsm = (New PortalPeriodFacade).GetCollectionOfMeetingDates_New(ClsSectionId, CampusId)

        Else
            acsm = (New PortalPeriodFacade).GetCollectionOfMeetingDatesWithCourseTypeID(ClsSectionId, CampusId, InstructionTypeID)

        End If
        'While Date.Parse(StartDate) <= Date.Parse(EndDate)
        '    Dim ss As String = String.Empty
        'End While
        'this is the array list to be filled with dates
        Dim arrDates As ArrayList = New ArrayList()
        Dim arrCancelDates As ArrayList = GetCancelDays(ClsSectionId)

        'filter all class meetings by start and end date
        Dim sd As Date = Date.Parse(StartDate)
        Dim ed As Date = Date.Parse(EndDate).Add(New TimeSpan(23, 59, 59))
        For Each meeting As AdvantageClassSectionMeeting In acsm
            If Not arrDates.Contains(meeting.MeetingDateAndTime) Then
                'If Not CheckDuplicates(arrDates, meeting.MeetingDateAndTime) Then
                If meeting.MeetingDateAndTime.ToShortDateString >= sd And meeting.MeetingDateAndTime.ToShortDateString <= ed Then
                    If Not CheckCancellDate(arrCancelDates, meeting.MeetingDateAndTime.Date.ToString) Then
                        arrDates.Add(meeting.MeetingDateAndTime)
                    End If
                    'End If
                End If
            End If

        Next
        Dim iDate As DateTime = StartDate
        While iDate <= EndDate
            If Not CheckDateAdded(arrDates, iDate) Then
                arrDates.Add(iDate)
            End If
            iDate = iDate.AddDays(1)
        End While

        arrDates.Sort()
        Return arrDates

    End Function
    Public Function GetMeetDatesForMeetingsArrayListFromRange(ByVal ClsSectionId As String, clsSectmeetingId As String, ByVal StartDate As String, ByVal EndDate As String, campusid As String) As ArrayList

        'this is the collection of AdvantageClasssectionMeetings
        Dim acsm As System.Collections.Generic.List(Of AdvantageClassSectionMeeting)
        Dim sTime As String
        acsm = (New PortalPeriodFacade).GetCollectionOfMeetingDatesForMeetings(ClsSectionId, clsSectmeetingId, campusid)
        If acsm.Count > 0 Then sTime = acsm(0).MeetingDateAndTime.TimeOfDay.ToString
        'While Date.Parse(StartDate) <= Date.Parse(EndDate)
        '    Dim ss As String = String.Empty
        'End While
        'this is the array list to be filled with dates
        Dim arrDates As ArrayList = New ArrayList()
        ' Dim arrReScheduleDates As ArrayList = GetReScheduleDaysForMeeting(clsSectmeetingId)
        Dim arrCancelDates As ArrayList = GetCancelDaysForMeeting(clsSectmeetingId)


        'filter all class meetings by start and end date
        Dim sd As Date = Date.Parse(StartDate)
        Dim ed As Date = Date.Parse(EndDate).Add(New TimeSpan(23, 59, 59))
        For Each meeting As AdvantageClassSectionMeeting In acsm
            If Not arrDates.Contains(meeting.MeetingDateAndTime) Then
                'If Not CheckDuplicates(arrDates, meeting.MeetingDateAndTime) Then
                If meeting.MeetingDateAndTime.ToShortDateString >= sd And meeting.MeetingDateAndTime.ToShortDateString <= ed Then
                    If Not CheckCancellDate(arrCancelDates, meeting.MeetingDateAndTime.Date) Then
                        arrDates.Add(meeting.MeetingDateAndTime)
                    End If
                    'End If
                End If
            End If

        Next
        Dim iDate As DateTime = StartDate
        While iDate <= EndDate
            If Not CheckDateAdded(arrDates, iDate) Then
                'If Not CheckCancellDate(arrCancelDates, iDate) Then
                arrDates.Add(iDate)
                ' End If
                'If CheckCancellDate(arrReScheduleDates, iDate) Then
                '    iDate = CDate(iDate & " " & sTime)
                'End If

            End If
            iDate = iDate.AddDays(1)
        End While

        arrDates.Sort()
        Return arrDates

    End Function

    Public Function NewGetMeetDatesArrayListFromRange(ByVal ClsSectionId As String, ByVal ClsSectMeetingID As String, ByVal StartDate As String, ByVal EndDate As String, Optional ByVal PostByException As Boolean = False, Optional ByVal CampusId As String = "") As ArrayList

        'this is the collection of AdvantageClasssectionMeetings
        Dim acsm As System.Collections.Generic.List(Of AdvantageClassSectionMeeting) = (New PortalPeriodFacade).NewGetCollectionOfMeetingDatesWithCourseTypeID(ClsSectionId, ClsSectMeetingID, CampusId)

        'this is the array list to be filled with dates
        Dim arrDates As ArrayList = New ArrayList()
        Dim arrCancelDates As ArrayList = GetCancelDays(ClsSectionId)

        'filter all class meetings by start and end date
        Dim sd As Date = Date.Parse(StartDate)
        Dim ed As Date = Date.Parse(EndDate).Add(New TimeSpan(23, 59, 59))
        For Each meeting As AdvantageClassSectionMeeting In acsm
            If meeting.MeetingDateAndTime.ToShortDateString >= sd And meeting.MeetingDateAndTime.ToShortDateString <= ed Then
                If Not CheckCancellDate(arrCancelDates, meeting.MeetingDateAndTime.Date.ToString) Then
                    arrDates.Add(meeting.MeetingDateAndTime)
                End If
            End If
        Next

        'return array of dates
        Return arrDates
    End Function
    Public Function NewGetMeetDatesArrayListFromRange_Dict(ByVal ClsSectionId As String, ByVal ClsSectMeetingID As String, ByVal StartDate As String, ByVal EndDate As String, Optional ByVal PostByException As Boolean = False, Optional ByVal CampusId As String = "") As Dictionary(Of Date, Date)

        'this is the collection of AdvantageClasssectionMeetings
        Dim acsm As System.Collections.Generic.List(Of AdvantageClassSectionMeeting) = (New PortalPeriodFacade).NewGetCollectionOfMeetingDatesWithCourseTypeID(ClsSectionId, ClsSectMeetingID, CampusId)

        'this is the array list to be filled with dates
        Dim arrDates As Dictionary(Of Date, Date) = New Dictionary(Of Date, Date)
        Dim arrCancelDates As ArrayList = GetCancelDays_ByClass(ClsSectionId, ClsSectMeetingID, CampusId)

        'filter all class meetings by start and end date
        Dim sd As Date = Date.Parse(StartDate)
        Dim ed As Date = Date.Parse(EndDate).Add(New TimeSpan(23, 59, 59))
        For Each meeting As AdvantageClassSectionMeeting In acsm
            If meeting.MeetingDateAndTime.ToShortDateString >= sd And meeting.MeetingDateAndTime.ToShortDateString <= ed Then
                If Not CheckCancellDate_ByClass(arrCancelDates, meeting.MeetingDateAndTime) Then
                    arrDates.Add(meeting.MeetingDateAndTime, meeting.MeetingDateAndTime)
                End If
            End If
        Next

        'return array of dates
        Return arrDates
    End Function
    Public Function GetMeetDateFromMeetingNumber(ByVal arrMeetDates As ArrayList, ByVal meetNumber As Integer) As DateTime
        Dim dtm As DateTime
        ''   Dim sDate As String

        Try
            dtm = arrMeetDates.Item(meetNumber - 1)
        Catch ex As Exception
            dtm = Convert.ToDateTime("1/1/1900")
        End Try

        Return dtm

    End Function

    Public Function GetStartTimeForMeetDate(ByVal dtMeetDays As DataTable, ByVal dtm As DateTime) As String
        Dim sDay As String
        Dim dr As DataRow

        'Get the short day value
        sDay = GetShortDayName(dtm)

        ''Modified BY Saraswathi on 12 Jan 2009 
        'Loop through the days that the class section meets and see which one this day matches one
        ''Match the day, then,meetdate between Start and enddate and then meettime is matched
        ''Previously, the startime was never returned since, the dtm was never equal to the TimeIntervalDescrip
        ''because the TimeIntervalDescrip had only the time to be correct and not the date.the date was an irrelevant one.

        For Each dr In dtMeetDays.Rows
            If dr("WorkDaysDescrip").ToString() = sDay Then
                If dtm.Date >= CType(dr("StartDate"), Date).Date And dtm.Date <= CType(dr("EndDate"), Date).Date Then
                    If dtm.TimeOfDay = CType(dr("TimeIntervalDescrip"), Date).TimeOfDay Then
                        Return dtm.TimeOfDay.ToString
                    End If
                End If
            End If
        Next


        ''Loop through the days that the class section meets and see which one this day matches one
        'For Each dr In dtMeetDays.Rows
        '    If dr("WorkDaysDescrip").ToString() = sDay And dtm.ToShortDateString = CType(dr("TimeIntervalDescrip"), Date).ToShortDateString Then
        '        Return dr("TimeIntervalDescrip")
        '    End If
        'Next
    End Function

    Public Function GetStartTimeForMeetDate(ByVal MeetDays() As DataRow, ByVal dtm As DateTime) As String
        Dim sDay As String
        Dim dr As DataRow

        'Get the short day value
        sDay = GetShortDayName(dtm)

        'Loop through the days that the class section meets and see which one this day matches one
        For Each dr In MeetDays
            If dr("WorkDaysDescrip").ToString() = sDay Then
                Return dr("TimeIntervalDescrip")
            End If
        Next
    End Function

    Public Function GetMeetingTimeForMeetDate(ByVal MeetDays() As DataRow, ByVal dtm As DateTime) As String
        Dim sDay As String
        Dim dr As DataRow

        'Get the short day value
        sDay = GetShortDayName(dtm)

        'Loop through the days that the class section meets and see which one this day matches one
        For Each dr In MeetDays
            If dr("WorkDaysDescrip").ToString() = sDay Then
                Return Convert.ToDateTime(dr("TimeIntervalDescrip")).ToShortTimeString & " - " & Convert.ToDateTime(dr("EndTime")).ToShortTimeString
            End If
        Next
    End Function

    Public Function GetClsSectionsForStudent(ByVal StuEnrollId As String, ByVal termID As String) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Dim dt As New DataTable

        dt = objClsSectAtt.GetClsSectionsForStudent(StuEnrollId, termID)
        'We need to concatenate the course and section of the class section
        Dim dc As DataColumn = dt.Columns.Add("Class", Type.GetType("System.String"))
        dc.AllowDBNull = True
        dc.Expression = "'(' + Code + ') ' + Descrip + ' - ' + ClsSection"

        'We need to concatenate the course and section of the class section
        dt.Columns.Add("ClassSectionDuration", Type.GetType("System.String"))
        For Each dr As DataRow In dt.Rows
            dr("ClassSectionDuration") = dr("StartDate") & "-" & dr("EndDate")
        Next

        Return dt
    End Function

    Public Function GetCourseTypesandClsSectionsForStudent(ByVal StuEnrollId As String, ByVal termID As String, ByVal PrgVerTracksAttendance As Boolean) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Dim dt As New DataTable

        dt = objClsSectAtt.GetCourseTypesandClsSectionsForStudent(StuEnrollId, termID, PrgVerTracksAttendance)
        'We need to concatenate the course and section of the class section
        Dim dc As DataColumn = dt.Columns.Add("Class", Type.GetType("System.String"))
        dc.AllowDBNull = True
        dc.Expression = "'(' + Code + ') ' + Descrip + ' - ' + ClsSection"

        'We need to concatenate the course and section of the class section
        dt.Columns.Add("ClassSectionDuration", Type.GetType("System.String"))
        For Each dr As DataRow In dt.Rows
            dr("ClassSectionDuration") = dr("StartDate") & "-" & dr("EndDate")
        Next

        Return dt
    End Function
    Public Function GetClsSectionsForStudent(ByVal StuEnrollId As String, ByVal termID As String, ByVal PrgVerTracksAttendance As Boolean) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Dim dt As New DataTable

        dt = objClsSectAtt.GetClsSectionsForStudent(StuEnrollId, termID, PrgVerTracksAttendance)
        'We need to concatenate the course and section of the class section
        Dim dc As DataColumn = dt.Columns.Add("Class", Type.GetType("System.String"))
        dc.AllowDBNull = True
        dc.Expression = "'(' + Code + ') ' + Descrip + ' - ' + ClsSection"

        'We need to concatenate the course and section of the class section
        dt.Columns.Add("ClassSectionDuration", Type.GetType("System.String"))
        For Each dr As DataRow In dt.Rows
            dr("ClassSectionDuration") = dr("StartDate") & "-" & dr("EndDate")
        Next

        Return dt
    End Function
    Public Function GetAllClsSectionsForStudent(ByVal StuEnrollId As String) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Dim dt As New DataTable

        dt = objClsSectAtt.GetAllClsSectionsForStudent(StuEnrollId)
        'We need to concatenate the course and section of the class section
        Dim dc As DataColumn = dt.Columns.Add("Class", Type.GetType("System.String"))
        dc.AllowDBNull = True
        dc.Expression = "'(' + Code + ') ' + Descrip + ' - ' + ClsSection"

        Return dt
    End Function

    Public Function TranslatePALetterToQuantity(ByVal letterValue As String) As Decimal
        Select Case letterValue.ToUpper
            Case "A", "E"
                Return 0
            Case "P", "T"
                Return 1
        End Select

    End Function

    Public Function TranslatePALetterToTardyBoolean(ByVal letterValue As String) As Boolean
        If letterValue.ToUpper = "T" Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function TranslatePALetterToExcusedBoolean(ByVal letterValue As String) As Boolean
        If letterValue.ToUpper = "E" Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function GetLOAInfoForStudentsInClassSection(ByVal classSectionId As String) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        Return objClsSectAtt.GetLOAInfoForStudentsInClassSection(classSectionId)

    End Function
    Public Function GetDropAndLOAInfoForStudent(ByVal stuEnrollId As String) As DataSet
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        Return objClsSectAtt.GetDropAndLOAInfoForStudent(stuEnrollId)

    End Function

    Public Function IsStudentOnLOA(ByVal stuEnrollId As String, ByVal testDate As DateTime, ByVal dtLOAInfo As DataTable) As Boolean
        Dim bln As Boolean = False
        Dim drRows() As DataRow
        Dim dr As DataRow

        'First check if there are any matching rows in the dtLOAInfo datatable for the specified stuEnrolliD
        'If there are no matching rows then we can easily return false since the student was never on LOA.
        'If there are matching rows we need to check each to see if the testDate is between the start and end
        'date range of the LOA.
        drRows = dtLOAInfo.Select("StuEnrollId = '" & stuEnrollId & "'")

        If drRows.Length = 0 Then
            'No matching records so we can return false.
            Return False
        Else
            For Each dr In drRows
                If testDate >= dr("StartDate") And testDate <= dr("EndDate") Then
                    Return True
                End If
            Next

        End If


        Return bln
    End Function

    Public Function DoesEnrollmentHaveStartDate(ByVal stuEnrollId As String) As Boolean
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        Return objClsSectAtt.DoesEnrollmentHaveStartDate(stuEnrollId)

    End Function
    Public Function ChangeStatusToCurrAttending(ByVal StuEnrollId As String) As String
        Dim db As New PortalClsSectAttendanceDB
        Dim StudentStatus As String
        Dim createObj As New StateFactory


        'Retrieve current status of student
        StudentStatus = db.GetCurrAttendingStatus()

        Return db.ChangeStatusToCurrAttending(StuEnrollId, StudentStatus)

    End Function

    Public Function GetTardiesMakingAbsence(ByVal stuEnrollId As String, ByVal clsSectionId As String) As Integer
        Dim factor As Integer = 0

        '   Check if attendance is tracked at the Program Version level
        If DoesPrgVersionTrackAttendance(stuEnrollId, False) Then
            '   Get number of tardies making an absence at the program version level
            factor = GetTardiesMakingAbsenceForPrgVersion(stuEnrollId, False)
        Else
            '   Get number of tardies making an absence at the course level
            factor = GetTardiesMakingAbsenceForCourse(clsSectionId, False)
        End If

        Return factor
    End Function

    Public Function GetAbsencesFromTardies(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal tardies As Decimal) As Decimal
        Dim factor As Integer
        Dim absencesFromTardies As Integer = 0

        If tardies <> 0 Then
            factor = GetTardiesMakingAbsence(stuEnrollId, clsSectionId)

            If factor > 0 Then
                absencesFromTardies = tardies \ factor
            End If
        End If

        Return absencesFromTardies
    End Function

    Public Function DoesPrgVersionTrackAttendance(ByVal strGuid As String, ByVal IsPrgVersion As Boolean) As Boolean
        Return (New PortalClsSectAttendanceDB).DoesPrgVersionTrackAttendance(strGuid, IsPrgVersion)
    End Function

    Public Function GetPrgVersionAttendanceType(ByVal strGuid As String, ByVal IsPrgVersion As Boolean) As String
        Return (New PortalClsSectAttendanceDB).GetPrgVersionAttendanceType(strGuid, IsPrgVersion)
    End Function

    Public Function GetTardiesMakingAbsenceForPrgVersion(ByVal strGuid As String, ByVal IsPrgVersion As Boolean) As Integer
        Return (New PortalClsSectAttendanceDB).GetTardiesMakingAbsenceForPrgVersion(strGuid, IsPrgVersion)
    End Function

    Public Function GetTardiesMakingAbsenceForCourse(ByVal strGuid As String, ByVal IsCourse As Boolean) As Integer
        Return (New PortalClsSectAttendanceDB).GetTardiesMakingAbsenceForCourse(strGuid, IsCourse)
    End Function

    Public Function ComputePercentageOfPresent(ByVal ttlPresent As Decimal, ByVal ttlAbsent As Decimal, ByVal ttlTardy As Decimal, ByVal ttlExcused As Decimal) As Decimal
        Dim percentage As Decimal
        If (ttlPresent + ttlAbsent + ttlTardy + ttlExcused) <> 0 Then
            'percentage = (ttlPresent + ttlTardy) / (ttlPresent + ttlAbsent + ttlTardy + ttlExcused) * 100
            percentage = (ttlPresent + ttlTardy) / (ttlPresent + ttlAbsent + ttlTardy + ttlExcused) * 100
        End If
        Return percentage
    End Function
    Public Function ComputePercentageOfPresentforMinutes(ByVal ttlPresent As Decimal, ByVal ttlAbsent As Decimal, ByVal ttlTardy As Decimal, ByVal ttlExcused As Decimal) As Decimal
        Dim percentage As Decimal
        If (ttlPresent + ttlAbsent + ttlTardy + ttlExcused) <> 0 Then
            'percentage = (ttlPresent + ttlTardy) / (ttlPresent + ttlAbsent + ttlTardy + ttlExcused) * 100
            percentage = ttlPresent / (ttlPresent + ttlAbsent + ttlTardy + ttlExcused) * 100
        End If
        Return percentage
    End Function


    Public Function BuildTermList(ByVal ds As DataSet) As String
        Dim termList As String = ""

        If ds.Tables.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                termList &= "'" & dr("TermId").ToString & "',"
            Next
        End If

        '   remove last comma
        If termList <> "" Then termList = termList.Remove(termList.Length - 1, 1)

        Return termList
    End Function

    Public Function GetPercentageAttendanceInfo(ByVal StuEnrollId As String, ByVal cutOffDate As Date, ByVal clsSectionId As String) As AttendancePercentageInfo

        'return attendance percentage
        'Return (New PortalClsSectAttendanceDB).GetPercentageAttendanceInfo(StuEnrollId, cutOffDate, clsSectionId)
        Return (New PortalAttendancePercentageBR).GetPercentageAttendanceInfo(StuEnrollId, cutOffDate, clsSectionId)

    End Function
    Public Function GetOverallPercentageAttendanceInfo(ByVal StuEnrollId As String, campusId As String) As AttendancePercentageInfo

        'return attendance percentage
        Return (New PortalAttendancePercentageBR).GetOverallPercentageAttendanceInfo(StuEnrollId, campusId)

    End Function

    Public Function GetInstructorName(ByVal clsSectionId As String) As String
        Return (New PortalClsSectAttendanceDB).GetInstructorName(clsSectionId)
    End Function

    Public Function IsTimeIntervalClsSection(ByVal clsSectionId As String) As Boolean
        Return (New PortalClsSectAttendanceDB).IsTimeIntervalClsSection(clsSectionId)
    End Function
    Public Function GetStudentStatus(ByVal StuEnrollId As String) As String
        Return (New PortalClsSectAttendanceDB).GetStudentStatus(StuEnrollId)
    End Function
    Public Function GetCollectionOfMeetingDatesAttendance(ByVal clsSectionId As String, ByVal stuEnrollId As String) As System.Collections.Generic.List(Of AdvantageClassSectionMeetingAttendance)
        Return (New PortalAttendanceDB).GetCollectionOfMeetingDatesAttendance(clsSectionId, stuEnrollId)
    End Function
    Public Function GetLDA(ByVal StuEnrollId As String) As DateTime
        Return (New PortalAttendanceDB).GetLda(StuEnrollId)
    End Function
    Public Function GetStudentLDA(ByVal StuEnrollId As String) As DateTime
        Return (New PortalAttendanceDB).GetStudentLDA(StuEnrollId)
    End Function


    '''  Added by Saraswathi Lakshmanan to fix issue
    '''  issue 14296: Instructor Supervisors and Education Directors 
    '''  need to be able to post and modify attendance. 
    '''  To find if the User is a Academic Advisor
    Public Function GetIsRoleAcademicAdvisor(ByVal UserId As String) As Boolean
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Return objClsSectAtt.GetIsRoleAcademicAdvisor(UserId)
    End Function
    Public Function GetIsRoleAcademicAdvisorORInstructorSuperVisor(ByVal UserId As String) As Boolean
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Return objClsSectAtt.GetIsRoleAcademicAdvisorORInstructorSuperVisor(UserId)
    End Function
    '''  Added by Saraswathi Lakshmanan on 25-Sept-2008 to fix issue
    '''  issue 14296: Instructor Supervisors and Education Directors 
    '''  need to be able to post and modify attendance. 
    '''  The term is populated based on the role of the user 
    ''' The Academic advisors can view all the terms and 
    ''' the instructor supervisors can view their respective instructors terms only
    ''' 
    Public Function GetTermsbyUserandRoles(ByVal userId As String, ByVal userName As String, ByVal isAcademicAdvisor As Boolean, Optional ByVal campusId As String = "") As DataSet
        'Dim objClsSectAtt As New PortalClsSectAttendanceDB
        'Return objClsSectAtt.GetTerms 'commented for bug fix 4044

        '   Instantiate DAL component
        Dim DB As New PortalTermsDB
        '   get the dataset with all degrees
        Return DB.GetNoneFutureActiveTermsbyUserandRoles(userId, userName, isAcademicAdvisor, campusId)

    End Function

    Public Function GetActiveShiftsByCampus(ByVal campusId As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New PortalTermsDB

        Dim ds As DataSet

        ds = DB.GetActiveShiftsByCampus(campusId)

        '   get the dataset with all active shifts for the campus
        Return ds

    End Function







    ''Added by Saraswthi Lakshmanan to find the Class Sections based on CohortStartDate
    Public Function GetClsSectionsForCohortStartDateForAttendance(ByVal CohortStartDAte As String, ByVal campusId As String, ByVal instructorId As String, ByVal userName As String, Optional ByVal isAcademicAdvisor As Boolean = False) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Dim dt As New DataTable

        dt = objClsSectAtt.GetClsSectionsForCohortStartDateForAttendance(CohortStartDAte, campusId, instructorId, userName, isAcademicAdvisor)
        'We need to concatenate the course and section of the class section
        Dim dc As DataColumn = dt.Columns.Add("Class", Type.GetType("System.String"))
        dc.AllowDBNull = True
        dc.Expression = "Descrip + ' - ' + ClsSection"

        Return dt
    End Function
    Public Function GetCourseTypesForStudent(ByVal StuEnrollId As String, ByVal termID As String, ByVal TrackPrgVerAttendance As Boolean, ByVal ClsSectionID As String) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Dim dt As New DataTable

        dt = objClsSectAtt.GetCourseTypesForStudent(StuEnrollId, termID, TrackPrgVerAttendance, ClsSectionID)

        Return dt
    End Function

    Public Function GetCoursetypesfortheClsSection(ByVal ClsSectionID As String) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB
        Dim dt As New DataTable
        dt = objClsSectAtt.GetCoursetypesfortheClsSection(ClsSectionID)

        Return dt
    End Function
    Public Function GetClsSectionMeetingsForClsSection(ByVal ClsSectionID As String, ByVal usePeriods As Boolean) As DataTable
        Dim objClsSectAtt As New PortalClsSectAttendanceDB

        Return objClsSectAtt.GetClsSectMeetingsforClsSection(ClsSectionID, usePeriods)
    End Function

    Public Function PostClsSectAttendance(ByVal xmlValues As String) As String
        Dim db As New PortalClsSectAttendanceDB
        db.PostClsSectAttendance(xmlValues)
        Return ""
    End Function


    Public Function InsertClsSectAttendanceInfo(ByVal ClSSectAttinfo As ClsSectAttendanceInfo) As String
        Dim db As New PortalClsSectAttendanceDB

        Return db.InsertClsSectAttendanceInfo(ClSSectAttinfo)
    End Function

    Public Function GetLOASuspensionandHolidaysforStudentsinClsSection(ByVal ClSSectionId As String, ByVal CampusId As String) As DataSet
        Dim db As New PortalClsSectAttendanceDB
        Return db.GetLOASuspandHolidaysforStudentsInClassSection(ClSSectionId, CampusId)
    End Function
    'Public Function GetPercentageAttendanceInfoForReportWithImportedAttendance(ByVal stuEnrollId As String, ByVal cutOffDate As Date) As Common.AttendancePercentageInfo
    '    Return (New AttendancePercentageBR).GetPercentageAttendanceInfoForReportWithImportedAttendance(stuEnrollId, cutOffDate)
    'End Function
    Public Function GetImportedAttendance_SP(ByVal StuEnrollId As String) As DataTable
        Return (New PortalClsSectAttendanceDB).GetImportedAttendance_SP(StuEnrollId)
    End Function
End Class

