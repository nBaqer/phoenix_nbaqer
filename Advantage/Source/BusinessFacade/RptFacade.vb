
' ===============================================================================
' RptFacade.vb
' Business Logic for Reports
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports FAME.AdvantageV1.DataAccess.Reports
Imports FAME.AdvantageV1.Common.Reports
Imports FAME.AdvantageV1.DataAccess.MSG

Namespace Reports
#Region "General"
    Public MustInherit Class BaseReportFacade
        MustOverride Function GetReportDataSet(ByVal rptParamInfo As ReportParamInfo) As DataSet
    End Class

    Public Class ReportFacade
        Inherits BaseReportFacade

        Public Overrides Function GetReportDataSet(ByVal rptParamInfo As ReportParamInfo) As DataSet
            Return ReportsDB.GetSimpleReportDataSet(rptParamInfo)
        End Function

        Public Function GetRecordCount(ByVal sqlID As Integer, Optional ByVal sortParams As String = "", _
                Optional ByVal filterListParams As String = "", Optional ByVal filterOtherParams As String = "") As String
            Return ReportsDB.GetRecordCount(sqlID, sortParams, filterListParams, filterOtherParams)
        End Function

        'Public Shared Function GetSQLByResource(ByVal ResourceID As Integer) As String
        '    Return ReportsDB.GetSQLByResource(ResourceID).ToString
        'End Function

        'Public Shared Function GetOBJName(ByVal objID As Integer) As String
        '    Return ReportsDB.GetOBJName(objID)
        'End Function

        Public Shared Function GetReportInfo(ByVal reportId As String) As ReportInfo
            Return ReportsDB.GetReportInfo(reportId)
        End Function

        Public Shared Function GetSchoolLogo(Optional ByVal useOfficialLogo As Boolean = False) As AdvantageLogoImage
            Return ReportsDB.GetSchoolLogo(useOfficialLogo)
        End Function

        Public Shared Function GetSchoolLogo(ByVal schoolId As Integer) As AdvantageLogoImage
            Return ReportsDB.GetSchoolLogo(schoolId)
        End Function

        Public Shared Function GetReportsForUser(ByVal userId As String, ByVal campusId As String, ByVal moduleId As String, _
                                            ByVal showPreDef As Boolean, ByVal showAdHoc As Boolean) As DataSet
            Return ReportsDB.GetReportsForUser(userId, campusId, moduleId, showPreDef, showAdHoc)
        End Function

#Region "Utility Methods"
        Public Shared Function GetReportParams(ByVal ReportId As String, ByVal IsAdHoc As Boolean) As DataSet
            Return ReportsDB.GetReportFieldsDS(ReportId, IsAdHoc)
        End Function

        Public Shared Function GetValuesForField(ByVal DDLId As String, Optional ByVal CampGrpId As String = "", Optional ByVal strUserId As String = "") As DataTable
            Return ReportsDB.GetValuesForField(DDLId, CampGrpId, strUserId)
        End Function
        Public Shared Function GetValuesForSDFField(ByVal SDFId As String) As DataTable
            Return ReportsDB.GetValuesForSDFField(SDFId)
        End Function

        Public Shared Function GetValuesForBitField() As DataTable
            Return ReportsDB.GetValuesForBitField()
        End Function

        Public Shared Function GetDTFromTblFldIds(ByVal TblFldIds() As Integer) As DataTable
            Return ReportsDB.GetDTFromTblFldIds(TblFldIds)
        End Function
#End Region
    End Class
#End Region

#Region "Report Parameters"
    Public Class RptParamsFacade
        Public Shared Function GetPrefsForReport(ByVal UserId As String, ByVal ReportId As String, _
                                            Optional ByVal IsAdHoc As Boolean = False) As DataTable
            Return RptParamsDB.GetAll(UserId, ReportId, IsAdHoc)
        End Function

        Public Shared Function GetUserPrefsInfo(ByVal PrefId As String) As UserPrefsInfo
            Return RptParamsDB.GetInfo(PrefId)
        End Function

        Public Shared Function GetUserPrefsDS(ByVal PrefId As String) As DataSet
            Return RptParamsDB.GetFiltersDS(PrefId)
        End Function

        Public Shared Function UpdateUserPref(ByVal info As UserPrefsInfo, ByVal user As String) As String
            If info.IsInDB = False Then
                Return RptParamsDB.Add(info, user)
            Else
                Return RptParamsDB.Update(info, user)
            End If
        End Function

        Public Shared Function DeleteUserPref(ByVal PrefId As String) As String
            Return RptParamsDB.Delete(PrefId)
        End Function
    End Class
#End Region

#Region "AdHoc Reports"
    Public Class AdHocRptFacade
        ''' <summary>
        ''' The return is a dataset with an array of tables.  
        ''' The query has a format like this:
        '''    select [FIELDS with AdHocRptInfo.Fields.Visible SET] 
        '''    from [TABLES belonging to FIELDS within AdhocRptInfo.Fields] 
        '''    where [TABLE_JOINS OBTAINED FROM FK RELATIONSHIPS]
        '''    and [ADDITIONAL FILTER OBTAINE FROM AdHocRptInfo.Filter]
        '''    order by [FIELDS with AdHocRptInfo.Fields.SortBy SET]
        '''    compute count([FIELDS with AdHocRptInfo.Fields.GroupBy SET]
        '''    by [ALL FIELDS inf order by and compute clause]
        ''' Because of the way the sql is made, 
        ''' each odd indexed table contains actual data and each even table contains unusable count information.
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="deleteAfterRun"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function RunReport(ByVal info As AdHocRptInfo, ByVal deleteAfterRun As Boolean, Optional ByVal isSummary As Boolean = False) As DataSet
            ' check for invalid report
            If info Is Nothing Or info.ReportId Is Nothing Then
                Return Nothing
            End If

            Dim ds As DataSet = AdHocRptDB.GetReportDataSet(info, isSummary)
            Return ds
        End Function

        Public Shared Function GetColumnSqlFromSDF(ByVal reportId As String, ByVal SDFId As String) As String
            Return AdHocRptDB.GetColumnSqlFromSDF(reportId, SDFId)
        End Function
        Public Shared Function GetColumnSqlFromSDFForDate(ByVal reportId As String, ByVal SDFId As String) As String
            Return AdHocRptDB.GetColumnSqlFromSDFForDate(ReportId, SDFId)
        End Function
        ''New Code Added By Vijay Ramteke On May 06, 2010
        Public Shared Function RunReportAdHoc(ByVal info As AdHocRptInfo, ByVal EntityId As String, ByVal DeleteAfterRun As Boolean, Optional ByVal IsSummary As Boolean = False) As DataSet
            ' check for invalid report
            If info Is Nothing Or info.ReportId Is Nothing Then
                Return Nothing
            End If

            Dim ds As DataSet = AdHocRptDB.GetReportDataSetAdHoc(info, EntityId, IsSummary)
            Return ds
        End Function
        ''New Code Added By Vijay Ramteke On May 06, 2010
        Public Shared Function GetExportReportSummary(ByVal info As AdHocRptInfo, ByVal reportDS As DataSet) As DataSet
            ' check for invalid report
            If info Is Nothing Or info.ReportId Is Nothing Then
                Return Nothing
            End If

            Dim ds As DataSet = AdHocRptDB.AddExportReportSummary(info, reportDS)
            Return ds
        End Function
        Public Shared Function GetReportSql(ByVal info As AdHocRptInfo) As String
            ' check for invalid report
            If info Is Nothing Or info.ReportId Is Nothing Then
                Return Nothing
            End If
            'Dim params() As String = Nothing
            Return AdHocRptDB.GetReportSql(info)
        End Function

        Public Shared Function GetReports(ByVal userId As String, ByVal campusID As String, ByVal moduleId As String, ByVal entityId As String, _
                                            ByVal showActive As Boolean, ByVal showInactive As Boolean) As DataSet
            Return AdHocRptDB.GetAll(userId, campusID, moduleId, entityId, showActive, showInactive)
        End Function

        Public Shared Function GetReportsForUser(ByVal UserId As String, ByVal campusId As String, ByVal ModuleId As String, ByVal EntityId As String) As DataSet
            Return AdHocRptDB.GetAllForUser(UserId, campusId, ModuleId, EntityId)
        End Function
        Public Shared Function GetReportInfo(ByVal reportId As String) As AdHocRptInfo
            Return AdHocRptDB.GetInfo(reportId)
        End Function
        Public Shared Function UpdateReport(ByVal info As AdHocRptInfo, ByVal user As String) As Boolean
            If info.IsInDB = False Then
                Return AdHocRptDB.Add(info, user) ' Create a new Report...
            Else
                Return AdHocRptDB.Update(info, user) ' Edit reports...
            End If
        End Function
        Public Shared Function DeleteReport(ByVal reportId As String, ByVal user As String) As String
            Return AdHocRptDB.Delete(reportId, user)
        End Function

        Public Shared Function GetReportFieldsDS(ByVal reportId As String) As DataSet
            Return ReportsDB.GetReportFieldsDS(reportId, True)
        End Function

        'Public Shared Function GetReportFieldsSDF(ByVal reportId As String) As AdHocFieldInfo()
        '    Return AdHocRptDB.GetReportFieldsSDF(reportId)
        'End Function


        ''' <summary>
        ''' Returns fields for the given category
        ''' 10/20/06 - BEN: Added support for UDF
        ''' </summary>
        ''' <param name="CategoryId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetFields(ByVal CategoryId As String) As DataSet
            Try
                If Convert.ToInt32(CategoryId) < 10 Then
                    Return AdHocRptDB.GetUDFFields(CategoryId)
                End If
                Return AdHocRptDB.GetFields(CategoryId)
            Catch ex As Exception
            End Try
            Return Nothing
        End Function

        Public Shared Function GetPermissions(ByVal UserId As String, ByVal ReportId As String) As DataSet
            Return AdHocRptDB.GetPermissionsDS(UserId, ReportId)
        End Function

        ''' <summary>
        ''' Returns all the Field Categories for the specified Entity
        ''' </summary>
        ''' <param name="EntityId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetCategories(ByVal EntityId As String) As DataSet
            Return AdHocRptDB.GetCategories(EntityId)
        End Function

        ''' <summary>
        ''' Returns all the valid report types.  These are just the list
        ''' of entities defined in the system.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetReportTypes() As DataSet
            Return ModulesDB.GetEntities()
        End Function
        ''' <summary>
        ''' Returns all the valid report types except Lenders.
        ''' Added on 09/20/2006 by Balaji
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetReportTypesExceptLender(ByVal EntityId As Integer) As DataSet
            Return ModulesDB.GetEntities(EntityId)
        End Function
        ''' <summary>
        ''' Returns all the campus groups defined in the system
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetCampusGroups() As DataSet
            Return CampGroupsDB.GetCampusGroups()
        End Function
        Public Shared Function GetCampusGroups(ByVal campusId As String, ByVal UserId As String) As DataSet
            Return CampGroupsDB.GetCampusGroups(campusId, UserId)
        End Function

        'Get in a string the TblFldId of the calculated fields.
        'Shared Function GetCalculatedfieldTblFldIdString() As String
        '    Dim dt As DataTable = AdHocRptDB.GetCalculatedfieldTblFldId()
        '    Dim calculatedString As String = String.Empty
        '    For Each row As DataRow In dt.Rows
        '        calculatedString = calculatedString & ", " & row.Item(0)
        '    Next
        '    calculatedString = " ( " & calculatedString.Remove(0, 1) & ")"
        '    Return calculatedString
        'End Function

        Public Shared Function FilterCalculatedFieldFromTable(dtFields As DataTable) As DataTable
            Dim dt As DataTable = AdHocRptDB.GetCalculatedfieldTblFldId()
            Dim calculatedString As String = String.Empty
            For Each row As DataRow In dt.Rows
                calculatedString = calculatedString & ", " & row.Item(0)
            Next
            calculatedString = " ( " & calculatedString.Remove(0, 1) & ")"
            Dim dt1 As DataTable '= New DataTable()
            dt1 = dtFields.Clone()
            Dim dr = dtFields.Select("TblFldsId NOT IN " & calculatedString)
            For Each row As DataRow In dr
                Dim rowvalues As object() = row.ItemArray
                dt1.Rows.Add(rowvalues)
            Next
            Return dt1
        End Function

    End Class
#End Region
End Namespace
