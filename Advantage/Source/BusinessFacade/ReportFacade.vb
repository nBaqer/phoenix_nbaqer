
Imports FAME.AdvantageV1.Common.Reports

Public MustInherit Class BaseReportFacade
    Public MustOverride Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
End Class


Public Class ReportFacade
    Inherits BaseReportFacade
    '
    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As DataSet
        Dim rptDB As New ReportsDB
        Return rptDB.GetSimpleReportDataSet(rptParamInfo)
    End Function

    Public Function GetRecordCount(ByVal sqlID As Integer, Optional ByVal sortParams As String = "", _
            Optional ByVal filterListParams As String = "", Optional ByVal filterOtherParams As String = "") As String
        Dim rptDB As New ReportsDB
        Return rptDB.GetRecordCount(sqlID, sortParams, filterListParams, filterOtherParams)
    End Function

    Public Function GetSQLByResource(ByVal ResourceID As Integer) As String
        Dim rptDB As New ReportsDB
        Return rptDB.GetSQLByResource(ResourceID).ToString
    End Function

    Public Function GetOBJName(ByVal objID As Integer) As String
        Dim rptDB As New ReportsDB
        Return rptDB.GetOBJName(objID)
    End Function

    Public Function GetReportInfo(ByVal ResourceID As String) As FAME.AdvantageV1.Common.ReportInfo
        Dim rptDB As New ReportsDB
        Return rptDB.GetReportInfo(ResourceID)
    End Function

    Public Function GetSchoolLogo(Optional ByVal useOfficialLogo As Boolean = False) As Common.AdvantageLogoImage ' As Byte()
        Dim rptDB As New ReportsDB

        'return AdvantageLogoImage
        Return rptDB.GetSchoolLogo(useOfficialLogo)

    End Function
    Public Function GetSchoolLogo(ByVal schoolId As Integer) As Common.AdvantageLogoImage

        'return AdvantageLogoImage
        Return (New ReportsDB).GetSchoolLogo(schoolId)

    End Function
    Public Function GetRequiredFiltersForReports(ByVal ResourceId As Integer) As DataSet
        Return (New ReportsDB).GetRequiredFiltersForReports(ResourceId)
    End Function
    Public Function GetClockHourProgramVersions_SP(ByVal prgverId As String) As String
        Return (New StuProgressReportDB).GetClockHourProgramVersions_SP(prgverId)
    End Function
    '' Code Added by kamalesh Ahuja on 08th Oct 2010 to resolve mantis issue id 19726
    Public Function GetCurrentlyAttendingStatusId(ByVal ObjID As String) As Boolean
        Dim rptDB As New ReportsDB
        Return rptDB.GetCurrentlyAttendingStatusId(ObjID)
    End Function
    ''
    Public Shared Function GetReportsItem(report As String) As ReportServerCatalog
        Dim db As New ReportsDB
        Return db.GetReportServerItem(report)
    End Function
End Class
