Imports System.Data

Public Class ResourcesRelationsFacade
    Public Function GetDefaultModules() As DataTable
        Dim dbResRels As New ResourcesRelationsDB

        Return dbResRels.GetDefaultModules

    End Function

    Public Function GetModules() As DataTable
        Dim dbResRels As New ResourcesRelationsDB

        Return dbResRels.GetModules

    End Function
    Public Function GetModulesByRoleId(ByVal roleId As String) As DataTable

        Return (New ResourcesRelationsDB).GetModulesByRoleId(roleId)

    End Function

    Public Function GetResourceTypes() As DataTable
        Dim dbResRels As New ResourcesRelationsDB

        Return dbResRels.GetResourceTypes
    End Function

    Public Function GetResourceTypesForSchlReqFields() As DataTable
        Dim dbResRels As New ResourcesRelationsDB

        Return dbResRels.GetResourceTypesForSchlReqFields
    End Function

    Public Function GetResourceRelations() As DataSet
        Dim dbResRels As New ResourcesRelationsDB

        Return dbResRels.GetResourcesRelations

    End Function

    Public Function GetFieldsForPage(ByVal resourceId As Integer) As DataTable
        Dim resDB As New ResourcesRelationsDB

        Return resDB.GetFieldsForPage(resourceId)

    End Function

    Public Function GetSchoolRequiredFldsForResource(ByVal resourceId As Integer) As DataTable
        Dim resDB As New ResourcesRelationsDB

        Return resDB.GetSchoolRequiredFldsForResource(resourceId)
    End Function

    Public Sub AddResourceRequiredField(ByVal resReqFldId As String, ByVal resourceId As Integer, ByVal fldId As Integer, ByVal user As String)
        Dim resDB As New ResourcesRelationsDB

        resDB.AddResourceRequiredField(resReqFldId, resourceId, fldId, user)

    End Sub

    Public Sub DeleteResourceRequiredField(ByVal resourceId As Integer, ByVal fldId As Integer)
        Dim resDB As New ResourcesRelationsDB

        resDB.DeleteResourceRequiredField(resourceId, fldId)

    End Sub

    Public Function DoesResourceBelongToASubmodule(ByVal resourceId As Integer) As Boolean
        Dim resDB As New ResourcesRelationsDB

        Return resDB.DoesResourceBelongToASubmodule(resourceId)
    End Function

    Public Function GetRolesResourcesPermissionsDS() As DataSet
        'return dataset
        Return (New NavigationNodesDB).GetRolesResourcesPermissionsDS()
    End Function
    Public Function UpdateRolesResourcesPermissionsDS(ByVal rolesResourcesPermissionsDS As DataSet) As String
        'return Dataset
        Return (New NavigationNodesDB).UpdateRolesResourcesPermissionsDS(rolesResourcesPermissionsDS)
    End Function
End Class
