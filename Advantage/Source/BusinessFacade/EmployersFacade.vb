' ===============================================================================
' FAME AdvantageV1.BusinessFacade
'
' EmployersFacade.vb
'
' Employers Services Interface (Facade). 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class EmployersFacade
    Public Function GetAllEmployersForEmail(ByVal statusId As String, ByVal campGrpId As String) As DataSet

        '   Instantiate DAL component
        Dim empDB As New EmployerSearchDB

        '   return Dataset
        Return empDB.GetAllEmployersForEmail(statusId, campGrpId)

    End Function
End Class
