Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class FIC_C3CommonObject

	Public Shared Function PreProcessRawData(ByVal dsRaw As DataSet) As DataTable
	' Pre-process raw report data, for final processing in report Business Facade objects
	'	This routine creates and returns a DataTable containing Student info, including a Student's
	'	original LeadId, if any, to be used by the Business Facade object to crosss reference each Lead, 
	'	to determine if a particular Lead is a student.

		' Create DataView of Leads in raw report data, sorted by SSN
		Dim dvLeads As New DataView(dsRaw.Tables(TblNameLeads), "", "SSN", DataViewRowState.CurrentRows)

		' Point utility DataTable variable to Students in raw report data
		Dim dtStudents As DataTable = dsRaw.Tables(TblNameStudents)

        ' Call Business Facade routine to get DataTable of Student Enrollmemt info,
        '	create DataView of Student Enrollmemt info, sorted by StudentId
        Dim dvStuEnrollInfo As New DataView(IPEDSFacade.GetProcessedEnrollInfo(dsRaw), "", _
                 "StudentId", DataViewRowState.CurrentRows)

        Dim RowPtr As Integer

        ' determine if this is for the detail report
        Dim IsDetailRpt As Boolean = dtStudents.Columns.Contains("LastName")

        ' create data table to hold pre-processed raw data
        Dim dtRptRaw As New DataTable
        With dtRptRaw.Columns
            .Add("LeadId", GetType(System.Guid)) ' student's original LeadId, if any
            .Add("SSN", GetType(System.String))
            If IsDetailRpt Then
                ' if this is for the detail report, add columns for name
                .Add("LastName", GetType(System.String))
                .Add("FirstName", GetType(System.String))
                .Add("MiddleName", GetType(System.String))
            End If
            .Add("GenderDescrip", GetType(System.String))
            .Add("AttendTypeDescrip", GetType(System.String))
        End With
        Dim drRptRaw As DataRow

        For Each drStudent As DataRow In dtStudents.Rows
            drRptRaw = dtRptRaw.NewRow

            ' get Student's original LeadId, if any
            ' if a record in Leads info contains current Student's SSN, get LeadId from that record
            RowPtr = dvLeads.Find(drStudent("SSN"))
            If RowPtr <> -1 Then
                drRptRaw("LeadId") = dvLeads(RowPtr)("LeadId")
                ' if a record in the Enrollment info contains current StudentId, get LeadId from that record
            Else
                RowPtr = dvStuEnrollInfo.Find(drStudent("StudentId"))
                If RowPtr <> -1 Then
                    drRptRaw("LeadId") = dvStuEnrollInfo(RowPtr)("LeadId")
                End If
            End If

            ' record the SSN
            drRptRaw("SSN") = drStudent("SSN")

            ' if this is for detail report, save student name
            If IsDetailRpt Then
                drRptRaw("LastName") = drStudent("LastName")
                drRptRaw("FirstName") = drStudent("FirstName")
                drRptRaw("MiddleName") = drStudent("MiddleName")
            End If

            ' set the Gender
            drRptRaw("GenderDescrip") = drStudent("GenderDescrip")

            ' get the Attendance type from Enrollment info
            RowPtr = dvStuEnrollInfo.Find(drStudent("StudentId"))
            drRptRaw("AttendTypeDescrip") = dvStuEnrollInfo(RowPtr)("AttendTypeDescrip")

            ' add processed raw DataRow to raw DataTable
            dtRptRaw.Rows.Add(drRptRaw)
        Next

        ' return copy of processed raw data
        Return dtRptRaw.Copy

    End Function

	Public Shared Function SetSSN(ByVal SSN As String) As String
	' Return correctly formatted SSN, or standard blank indicator for SSN

		If SSN <> "" Then
			With New InputMasksFacade
				Return .ApplyMask(.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN), SSN)
			End With
		End If

		Return "-"
	End Function

End Class
