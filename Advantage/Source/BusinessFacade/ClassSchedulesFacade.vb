Public Class ClassSchedulesFacade
    Public Function GetDatesInPeriod(ByVal StartDate As String, ByVal EndDate As String) As ArrayList
        Dim arrDates As New ArrayList
        Dim dtm As DateTime
        Dim counter As Integer
        Dim Rows() As DataRow
        Dim numDays As Integer
        Dim boolIsDayMarkedAsHoliday As Boolean = False
        If Convert.ToDateTime(StartDate) <= Convert.ToDateTime(EndDate) Then
            dtm = Convert.ToDateTime(StartDate)
            'code modified by balaji on april 2,2009 to fix issue 15386.
            'modification starts here
            boolIsDayMarkedAsHoliday = (New ClassSchedulesDB).isDayMarkedAsHoliday(dtm)
            If boolIsDayMarkedAsHoliday = False Then
                arrDates.Add(dtm)
            End If
            'modification ends here
            If Convert.ToDateTime(StartDate) <> Convert.ToDateTime(EndDate) Then
                For counter = 1 To 365
                    dtm = dtm.AddDays(1)
                    'If day has been marked as holiday skip the day, do not add it to
                    'the days collection
                    boolIsDayMarkedAsHoliday = (New ClassSchedulesDB).isDayMarkedAsHoliday(dtm)
                    If boolIsDayMarkedAsHoliday = False Then
                        arrDates.Add(dtm)
                    End If
                    If dtm = Convert.ToDateTime(EndDate) Then
                        Exit For
                    End If
                Next
            End If
        End If
        Return arrDates
    End Function

    Public Function GetMeetings(ByVal StartDate As String, ByVal EndDate As String, ByVal CampusId As String, ByVal InstructorId As String) As DataTable
        Dim db As New ClassSchedulesDB

        Return db.GetMeetings(StartDate, EndDate, CampusId, InstructorId)

    End Function
    ''Modified by Saraswathi on Feb 18 2009
    ''To fix mantis 15378
    ''Instructor Ids are also taken and filtered instead of Instructor name, due to SPecial characters in Instructor Name.

    Public Function GetInstructorHours(ByVal StartDate As String, ByVal EndDate As String, ByVal CampusId As String, ByVal InstructorId As String) As DataTable
        Dim db As New ClassSchedulesDB
        Dim facAttendance As New ClsSectAttendanceFacade
        Dim dtMeetings As New DataTable
        Dim arrDates As New ArrayList
        Dim dtStaging As New DataTable
        Dim dtProcessed As New DataTable
        Dim dtFinal As New DataTable
        Dim meetingRows() As DataRow
        Dim instructor As String
        Dim instructorUIDS As String


        dtMeetings = db.GetMeetings(StartDate, EndDate, CampusId, InstructorId)
        arrDates = GetDatesInPeriod(StartDate, EndDate)
        ''Added by Saraswathi Lakshmanan on August 24 2009
        ''to fix issue  15387: Bug: Partial holidays are not taken into account on the Instructor hours and Class schedules page. 
        Dim dtPartialHols As DataTable
        dtPartialHols = GetPartialHolidays(StartDate, EndDate, CampusId)
        'Add the structure of dtStaging
        ''Added by Saraswathi Lakshmanan ON Feb 18 2009
        ''To fix mantis 15378, Special characters in instructor name giving errors
        dtStaging.Columns.Add("InstructorUIDS", Type.GetType("System.String"))
        dtStaging.Columns.Add("Instructor", Type.GetType("System.String"))
        dtStaging.Columns.Add("Minutes", Type.GetType("System.Int32"))


        'Add the structure of dtProcessed
        dtProcessed.Columns.Add("Instructor", Type.GetType("System.String"))
        dtProcessed.Columns.Add("InstructorUIDS", Type.GetType("System.String"))

        'Add the structure of dtFinal
        '  dtFinal.Columns.Add("InstructorUIDS", Type.GetType("System.String"))
        dtFinal.Columns.Add("Instructor", Type.GetType("System.String"))
        dtFinal.Columns.Add("Classes", Type.GetType("System.Int32"))
        dtFinal.Columns.Add("Hours", Type.GetType("System.Decimal"))

        'Use dt to prepare another Datatable with Instructor, Classes and Hours to be used in the UI
        'The dt that is returned simply has the instructors and the classes that have a duration that
        'cover the start and end dates. However, this does not necessarily mean that there is a meeting
        'during the date range specified. For example, the class might meet from 8/15/08 to 9/30/2008
        'on Monday, Wednesday and Friday. If the user selects 9/2/2008 as the start and end date
        'then the class should not show up.
        'First, we need to loop through each day in the meet period and get the classes that actually
        'meet on those days. Once we have that, we can do a summary of the number of meetings and
        'the actual hours.
        For Each dtmPeriod As Date In arrDates
            meetingRows = dtMeetings.Select("WorkDaysDescrip = '" & facAttendance.GetShortDayName(dtmPeriod) & "' AND StartDate <= '" & dtmPeriod & "' AND EndDate >= '" & dtmPeriod & "'")

            If meetingRows.Length > 0 Then

                ''If day is a partial Holiday
                ''Remove the meetings for those timings alone

                ''Code Modified By Saraswathi lakshmanan on August 24 2009
                ''To fix issue 15387: Bug: Partial holidays are not taken into account on the Instructor hours and Class schedules page. 
                ''If the Class Falls between the holiday time, then that class is not shown. or else if it is overlapping with the Holiday time , then it is shown.
                ''Eg: 1-4 PM is HOliday and Class is between 2-4PM then this class is not shown.
                ''If the holiday is between 1-4 and Class is between 12-3 then it is Shown. Both the start and End time of the Class should fall between the Start and end time of the Holiday.


                Dim PartialHolsDateRow() As DataRow
                Dim PartialHolsAvail() As DataRow
                Dim icountpartialhols As Integer = 0
                PartialHolsDateRow = dtPartialHols.Select("HolidayDate='" & dtmPeriod & "'")
                If PartialHolsDateRow.Length > 0 Then
                    Dim i As Integer
                    For i = 0 To meetingRows.Length - 1
                        PartialHolsAvail = dtPartialHols.Select("HolidayDate='" & dtmPeriod & "' AND StartTime<='" & meetingRows(i)("StartTime") & "' AND EndTime>='" & meetingRows(i)("EndTime") & "'")
                        If PartialHolsAvail.Length > 0 Then
                            icountpartialhols = icountpartialhols + 1
                        End If
                    Next

                End If

                If icountpartialhols < meetingRows.Length Then

                    'Add these to the dtStaging datatable
                    For Each dr As DataRow In meetingRows
                        Dim drM As DataRow = dtStaging.NewRow
                        drM("InstructorUIDS") = dr("InstructorUIDS")
                        drM("Instructor") = dr("Instructor")
                        drM("Minutes") = dr("Minutes")


                        ''Code Modified By Saraswathi lakshmanan on August 24 2009
                        ''To fix issue 15387: Bug: Partial holidays are not taken into account on the Instructor hours and Class schedules page. 

                        If dtPartialHols.Select("HolidayDate='" & dtmPeriod & "' AND StartTime<='" & dr("StartTime") & "' AND EndTime>='" & dr("EndTime") & "'").Length = 0 Then
                            dtStaging.Rows.Add(drM)
                        End If

                    Next
                End If
            End If
        Next

        'If we have records in the staging table it means that we have meetings that needs
        'to be summarized.
        If dtStaging.Rows.Count > 0 Then
            'Loop through each record and get the number of meetings and the total hours for each instructor
            For Each drIM As DataRow In dtStaging.Rows
                If drIM.IsNull("InstructorUIDS") Then
                    instructorUIDS = ""
                Else
                    instructorUIDS = drIM("InstructorUIDS")
                End If
                If drIM.IsNull("Instructor") Then
                    instructor = ""
                Else
                    instructor = drIM("Instructor")
                End If

                If Not HasInstructorBeenProcessed(instructorUIDS, dtProcessed) Then
                    Dim drFinal As DataRow = dtFinal.NewRow

                    drFinal("Instructor") = instructor
                    '   drFinal("InstructorUIDS") = instructorUIDS
                    'Get the count of meetings
                    drFinal("Classes") = CInt(dtStaging.Compute("COUNT(InstructorUIDS)", "InstructorUIDS = '" & instructorUIDS & "'"))
                    drFinal("Hours") = CDec(dtStaging.Compute("SUM(Minutes)", "InstructorUIDS = '" & instructorUIDS & "'")) / 60
                    Dim dtInstructors() As DataRow
                    Try
                        If dtFinal.Rows.Count >= 1 Then
                            dtInstructors = dtFinal.Select("Instructor='" & instructor & "'")
                            If dtInstructors.Length > 0 Then
                                'Instructor and the hours have already been added to dtFinal table, so no need to add instructor to dtFinal
                            Else
                                dtFinal.Rows.Add(drFinal)
                            End If
                        Else
                            dtFinal.Rows.Add(drFinal)
                        End If
                    Catch ex As System.Exception
                    End Try

                    'Add a row the processed table for this instructor
                    Dim drP As DataRow = dtProcessed.NewRow
                    drP("Instructor") = instructor
                    drP("InstructorUIDS") = instructorUIDS
                    dtProcessed.Rows.Add(drP)
                End If
            Next
        End If
        Return dtFinal
    End Function

    'Public Function GetInstructorHours(ByVal StartDate As String, ByVal EndDate As String, ByVal CampusId As String, ByVal InstructorId As String) As DataTable
    '    Dim db As New ClassSchedulesDB
    '    Dim facAttendance As New ClsSectAttendanceFacade
    '    Dim dtMeetings As New DataTable
    '    Dim arrDates As New ArrayList
    '    Dim dtStaging As New DataTable
    '    Dim dtProcessed As New DataTable
    '    Dim dtFinal As New DataTable
    '    Dim meetingRows() As DataRow
    '    Dim instructor As String

    '    dtMeetings = db.GetMeetings(StartDate, EndDate, CampusId, InstructorId)
    '    arrDates = GetDatesInPeriod(StartDate, EndDate)

    '    'Add the structure of dtStaging
    '    dtStaging.Columns.Add("Instructor", Type.GetType("System.String"))
    '    dtStaging.Columns.Add("Minutes", Type.GetType("System.Int32"))


    '    'Add the structure of dtProcessed
    '    dtProcessed.Columns.Add("Instructor", Type.GetType("System.String"))

    '    'Add the structure of dtFinal
    '    dtFinal.Columns.Add("Instructor", Type.GetType("System.String"))
    '    dtFinal.Columns.Add("Classes", Type.GetType("System.Int32"))
    '    dtFinal.Columns.Add("Hours", Type.GetType("System.Decimal"))

    '    'Use dt to prepare another Datatable with Instructor, Classes and Hours to be used in the UI
    '    'The dt that is returned simply has the instructors and the classes that have a duration that
    '    'cover the start and end dates. However, this does not necessarily mean that there is a meeting
    '    'during the date range specified. For example, the class might meet from 8/15/08 to 9/30/2008
    '    'on Monday, Wednesday and Friday. If the user selects 9/2/2008 as the start and end date
    '    'then the class should not show up.
    '    'First, we need to loop through each day in the meet period and get the classes that actually
    '    'meet on those days. Once we have that, we can do a summary of the number of meetings and
    '    'the actual hours.
    '    For Each dtmPeriod As Date In arrDates
    '        meetingRows = dtMeetings.Select("WorkDaysDescrip = '" & facAttendance.GetShortDayName(dtmPeriod) & "' AND StartDate <= '" & dtmPeriod & "' AND EndDate >= '" & dtmPeriod & "'")

    '        If meetingRows.Length > 0 Then
    '            'Add these to the dtStaging datatable
    '            For Each dr As DataRow In meetingRows
    '                Dim drM As DataRow = dtStaging.NewRow
    '                drM("Instructor") = dr("Instructor")
    '                drM("Minutes") = dr("Minutes")
    '                dtStaging.Rows.Add(drM)
    '            Next
    '        End If

    '    Next

    '    'If we have records in the staging table it means that we have meetings that needs
    '    'to be summarized.
    '    If dtStaging.Rows.Count > 0 Then
    '        'Loop through each record and get the number of meetings and the total hours for each instructor
    '        For Each drIM As DataRow In dtStaging.Rows
    '            If drIM.IsNull("Instructor") Then
    '                instructor = ""
    '            Else
    '                instructor = drIM("Instructor")
    '            End If

    '            If Not HasInstructorBeenProcessed(instructor, dtProcessed) Then
    '                Dim drFinal As DataRow = dtFinal.NewRow

    '                drFinal("Instructor") = instructor
    '                'Get the count of meetings
    '                drFinal("Classes") = CInt(dtStaging.Compute("COUNT(Instructor)", "Instructor = '" & instructor & "'"))
    '                drFinal("Hours") = CDec(dtStaging.Compute("SUM(Minutes)", "Instructor = '" & instructor & "'")) / 60
    '                dtFinal.Rows.Add(drFinal)

    '                'Add a row the processed table for this instructor
    '                Dim drP As DataRow = dtProcessed.NewRow
    '                drP("Instructor") = instructor
    '                dtProcessed.Rows.Add(drP)
    '            End If
    '        Next
    '    End If

    '    Return dtFinal

    'End Function

    Private Function HasInstructorBeenProcessed(ByVal Instructor As String, ByVal dtProcessed As DataTable) As Boolean
        Dim arrRows() As DataRow
        Dim filter As String
        'If the dt is empty then simply returns false
        If Instructor = "" Then
            Return True
        ElseIf dtProcessed.Rows.Count = 0 Then
            Return False
        Else
            'Search the DataTable
            filter = "Instructor = '" & Instructor & "'"
            arrRows = dtProcessed.Select(filter)
            If arrRows.Length = 0 Then
                Return False
            Else
                Return True
            End If
        End If
    End Function
    ''Code Addded By Saraswathi lakshmanan on August 24 2009
    ''To fix issue 15387: Bug: Partial holidays are not taken into account on the Instructor hours and Class schedules page. 


    Public Function GetPartialHolidays(ByVal StartDate As DateTime, ByVal EndDate As DateTime, ByVal CampusId As String) As DataTable
        Dim DB As New ClassSchedulesDB
        Return DB.GetPartialHolidays(StartDate, EndDate, CampusId)
    End Function
End Class
