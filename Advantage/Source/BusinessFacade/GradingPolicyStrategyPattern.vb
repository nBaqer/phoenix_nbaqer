Public Interface GradingPolicyStrategyPattern
    Function GetResultsSet(ByVal resultSet As DataTable, Optional ByVal Param As Integer = 0) As DataTable
End Interface

Public Class DropLowest
    Implements GradingPolicyStrategyPattern
    Public Function GetResultsSet(ByVal resultSet As DataTable, Optional ByVal Param As Integer = 0) As DataTable Implements GradingPolicyStrategyPattern.GetResultsSet
        resultSet.DefaultView.Sort = "Score ASC"
        Dim dt As DataTable = resultSet.DefaultView.ToTable(False, "Score", "PostDate")
        If resultSet.Rows.Count > 0 AndAlso resultSet.Rows.Count > Param Then
            For i = 0 To (Param - 1)
                dt.Rows.Remove(dt.Rows(0))
            Next
            dt.AcceptChanges()
        End If
        Return dt
    End Function
End Class

Public Class Best
    Implements GradingPolicyStrategyPattern
    Public Function GetResultsSet(ByVal resultSet As DataTable, Optional ByVal Param As Integer = 0) As DataTable Implements GradingPolicyStrategyPattern.GetResultsSet
        resultSet.DefaultView.Sort = "Score ASC"
        Dim dt As DataTable = resultSet.DefaultView.ToTable(False, "Score", "PostDate")
        If resultSet.Rows.Count > 0 AndAlso resultSet.Rows.Count > Param Then
            For i = 0 To (resultSet.Rows.Count - Param - 1)
                dt.Rows.Remove(dt.Rows(0))
            Next
            dt.AcceptChanges()
        End If
        Return dt
    End Function
End Class

Public Class Latest
    Implements GradingPolicyStrategyPattern
    Public Function GetResultsSet(ByVal resultSet As DataTable, Optional ByVal Param As Integer = 0) As DataTable Implements GradingPolicyStrategyPattern.GetResultsSet
        resultSet.DefaultView.Sort = "PostDate ASC, Score ASC"
        Dim dt As DataTable = resultSet.DefaultView.ToTable(False, "Score", "PostDate")
        If resultSet.Rows.Count > 0 AndAlso resultSet.Rows.Count > Param Then
            For i = 0 To (resultSet.Rows.Count - Param - 1)
                dt.Rows.Remove(dt.Rows(0))
            Next
            dt.AcceptChanges()
        End If
        Return dt
    End Function
End Class
