' ****************************************************************
' Utility routines and objects used for report custom preferences 
' for both detail and summary reports
' ****************************************************************

Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFA_PRCommonObject

	Public Const RptCat_None As Integer = 0
	Public Const RptCat_StateLocalGrants As Integer = 1
	Public Const RptCat_InstGrants As Integer = 2

	Public Shared Function GetFundSources(ByVal CampusId As String) As DataTable
		Return SFA_PRCommonObjectDB.GetFundSources(CampusId)
	End Function

	Public Shared Sub AddUserPref(ByVal PrefId As String, ByVal dtRptCatumns As DataTable)
		SFA_PRCommonObjectDB.AddUserPref(PrefId, dtRptCatumns)
	End Sub

	Public Shared Sub DeleteUserPref(ByVal PrefId As String)
		SFA_PRCommonObjectDB.DeleteUserPref(PrefId)
	End Sub

	Public Shared Function GetUserPref(ByVal PrefId As String) As DataTable
		Return SFA_PRCommonObjectDB.GetUserPref(PrefId)
	End Function

	Public Shared Function PreProcessRawData(ByVal dsRaw As DataSet) As DataTable
		Dim dtStudents As DataTable = dsRaw.Tables(TblNameStudents)
		Dim dtStuEnrollInfo As DataTable = IPEDSFacade.GetProcessedEnrollInfo(dsRaw)

		' determine if this is for the detail report
		Dim IsDetailRpt As Boolean = dtStudents.Columns.Contains(RptStuIdentifierColName)

		' create data table to hold processed raw data
		Dim dtRptRaw As New DataTable
		With dtRptRaw.Columns
			.Add("StudentId", GetType(Guid))
			If IsDetailRpt Then
				' if this is for the detail report, add columns for student identifier and name
				.Add(RptStuIdentifierColName, GetType(String))
				.Add("LastName", GetType(String))
				.Add("FirstName", GetType(String))
				.Add("MiddleName", GetType(String))
			End If
			.Add("FinAidCount", GetType(System.Int32))
			.Add("DegCertSeekingDescrip", GetType(String))
		End With

		Dim drRptRaw As DataRow

		' create DataView of student enrollment info for use when processing
		Dim dvStuEnrollInfo As New DataView(dtStuEnrollInfo, "", "StudentId", DataViewRowState.CurrentRows)
		Dim RowPtr As Integer

		For Each drStudent As DataRow In dtStudents.Rows
			drRptRaw = dtRptRaw.NewRow

			' save Student ID
			drRptRaw("StudentId") = drStudent("StudentId")

			' save basic student information
			If IsDetailRpt Then
				' if this is for the detail report, save student identifier and name
				drRptRaw(RptStuIdentifierColName) = drStudent("StudentIdentifier")
				drRptRaw("LastName") = drStudent("LastName")
				drRptRaw("FirstName") = drStudent("FirstName")
				drRptRaw("MiddleName") = drStudent("MiddleName")
			End If

			' record count of Financial Aid payments made to the student
			drRptRaw("FinAidCount") = drStudent("FinAidCount")

			' find student's record in enrollment info data, record Degree/Certificate Seeking description
			RowPtr = dvStuEnrollInfo.Find(drStudent("StudentId"))
			drRptRaw("DegCertSeekingDescrip") = dvStuEnrollInfo(RowPtr)("DegCertSeekingDescrip")

			' add processed raw DataRow to raw DataTable
			dtRptRaw.Rows.Add(drRptRaw)
		Next

		' return copy of processed raw data
		Return dtRptRaw.Copy

	End Function

	Public Shared Function CompAwardSum(ByVal dv As DataView, ByVal SumCol As String) As Single
		Dim Sum As Single = 0
		Dim i As Integer

		For i = 0 To dv.Count - 1
			Sum += CSng(dv(i)(SumCol))
		Next

		Return Sum
	End Function

End Class
