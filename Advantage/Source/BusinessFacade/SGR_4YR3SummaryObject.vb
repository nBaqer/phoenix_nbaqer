Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.AdvantageV1.BusinessFacade.SGR_CommonObject

Public Class SGR_4YR3SummaryObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SGR_4YR3Summary"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet
	Private RptParamInfo As ReportParamInfoIPEDS

	Private DateFilter As String
	Private dvEnrollments As DataView

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SGR_4YR3SummaryObjectDB.GetReportDatasetRaw(RptParamInfo)

		' save passed-in report parameter info 
		Me.RptParamInfo = RptParamInfo

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		Dim dtGenderInfo As DataTable = IPEDSFacade.GetIPEDSGenderInfo
		Dim drGenderInfo As DataRow
		Dim dtEthnicInfo As DataTable = IPEDSFacade.GetIPEDSEthnicInfo
		Dim drEthnicInfo As DataRow

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add("GenderDescrip", GetType(String))
			.Add("EthCodeDescrip", GetType(String))
			.Add("Count_CompLT2Yr", GetType(Integer))
			.Add("Count_Comp2to4Yr", GetType(Integer))
			.Add("Count_CompBachelors", GetType(Integer))
			.Add("Count_TransferOut", GetType(Integer))
			.Add("Count_Excluded", GetType(Integer))
			.Add("Count_StillEnrolled", GetType(Integer))
		End With

		' create student Enrollments DataView to be used when processing records
		dvEnrollments = New DataView(dsRaw.Tables(TblNameEnrollments))

		' set up date filter to be used when processing records
		DateFilter = EnrollDateFilter(RptParamInfo)

		' loop thru genders, and ethnic groups for each gender, and process matching records in raw dataset
		For Each drGenderInfo In dtGenderInfo.Rows
			For Each drEthnicInfo In dtEthnicInfo.Rows
				ProcessRows(drGenderInfo("GenderDescrip"), drEthnicInfo("EthCodeDescrip"))
			Next
		Next

		' return the Report DataSet
		Return dsRpt

	End Function

	Private Sub ProcessRows(ByVal GenderDescrip As String, ByVal EthCodeDescrip As String)
		Dim i As Integer
		Dim drRpt As DataRow
		Dim dvStudents As DataView
		Dim AddedRows As Boolean = False
		Dim StudentId As String
		Dim IsTransferOut As Boolean, IsExcluded As Boolean

		Dim Count_CompLT2Yr As Integer = 0
		Dim Count_Comp2to4Yr As Integer = 0
		Dim Count_CompBachelors As Integer = 0
		Dim Count_TransferOut As Integer = 0
		Dim Count_Excluded As Integer = 0
		Dim Count_StillEnrolled As Integer = 0

		' create dataview from students in raw dataset, filtered according to passed in gender and ethnic ids
		dvStudents = New DataView(dsRaw.Tables(TblNameStudents), _
								  "GenderDescrip LIKE '" & GenderDescrip & "' AND EthCodeDescrip LIKE '" & EthCodeDescrip & "'", _
								  "", DataViewRowState.CurrentRows)

		For i = 0 To dvStudents.Count - 1
			StudentId = dvStudents(i)("StudentId").ToString

			' determine if student is a "transfer out"
			IsTransferOut = TransferOut(StudentId, dvEnrollments)
			If IsTransferOut Then
				Count_TransferOut += 1
			End If

			' determine if student is an exclusion due to drop reason
			IsExcluded = ExcludedFromCohort(StudentId, dvEnrollments)
			If IsExcluded Then
				Count_Excluded += 1
			End If

			' if student is not a transfer out and is not excluded, determine if student completed 
			'	programs within 150% of normal time to completion
			If Not IsTransferOut And Not IsExcluded Then
				' determine if student completed any program of less than 2 years within 150% of 
				'	normal time to completion
				If Completed150Pct(StudentId, dvEnrollments, "Weeks < " & NumWeeks_2Yrs, DateFilter) Then
					Count_CompLT2Yr += 1
				End If

				' determine if student completed any program of 2 years and less than 4 years within 150% of 
				'	normal time to completion
				If Completed150Pct(StudentId, dvEnrollments, "Weeks >= " & NumWeeks_2Yrs & " AND Weeks < " & NumWeeks_4Yrs, DateFilter) Then
					Count_Comp2to4Yr += 1
				End If

				' determine if student completed any program of 4 years or greater within 150% of 
				'	normal time to completion
				If Completed150Pct(StudentId, dvEnrollments, "Weeks >= " & NumWeeks_4Yrs, DateFilter) Then
					Count_CompBachelors += 1
				End If
			End If

			' determine if student is still enrolled in programs of 5 years or longer
			If StillEnrolled(StudentId, dvEnrollments, NumWeeks_5Yrs, RptParamInfo) Then
				Count_StillEnrolled += 1
			End If
		Next

		' add new summary row to report
		drRpt = dsRpt.Tables(MainTableName).NewRow

		' set Gender and Ethnic Code descriptions
		drRpt("GenderDescrip") = GenderDescrip
		drRpt("EthCodeDescrip") = EthCodeDescrip

		' set summary counters
		drRpt("Count_CompLT2Yr") = Count_CompLT2Yr
		drRpt("Count_Comp2to4Yr") = Count_Comp2to4Yr
		drRpt("Count_CompBachelors") = Count_CompBachelors
		drRpt("Count_TransferOut") = Count_TransferOut
		drRpt("Count_Excluded") = Count_Excluded
		drRpt("Count_StillEnrolled") = Count_StillEnrolled

		' add row to report DataSet
		dsRpt.Tables(MainTableName).Rows.Add(drRpt)

	End Sub

End Class
