Public Class ReportCommonUtilsFacade

    Public Function GetStuNameAndPrgVersion(ByVal StuEnrollId As String) As DataTable
        Dim rptUtil As New ReportCommonUtilsDB
        Dim dt As DataTable

        dt = rptUtil.GetStudentNameAndPrgVersion(StuEnrollId)
        Return dt
    End Function

    Public Function GetStudentName(ByVal StudentId As String) As DataTable
        Dim rptUtil As New ReportCommonUtilsDB
        Dim dt As DataTable

        dt = rptUtil.GetStudentName(StudentId)
        Return dt
    End Function

    Public Function GetAcademicType(ByVal StuEnrollIdList As String) As String
        Dim rptUtil As New ReportCommonUtilsDB
        Dim AcademictType As String

        AcademictType = rptUtil.GetAcademicType(StuEnrollIdList)
        Return AcademictType
    End Function

    Public Sub DeleteReportSetting(ByVal strGuid As String)
        Dim rptUtil As New ReportCommonUtilsDB
        rptUtil.DeleteReportSetting(strGuid)
    End Sub
End Class
