Imports System.data
Imports System.Data.OleDb
'Imports FAME.AdvantageV1.BusinessRules
Imports FAME.AdvantageV1.DataAccess
Public Class UnscheduleClsFacade
    Public Function GetAllTerms() As DataSet

        '   Instantiate DAL component
        Dim DB As New UnscheduleClsDB

        '   get the dataset with all degrees
        Return DB.GetAllTerms()

    End Function
    Public Function GetCourses(ByVal Term As String, ByVal CampusId As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New UnscheduleClsDB
        Dim ds As New DataSet

        '   get the dataset with all degrees          
        ds = DB.GetCourses(Term, CampusId)

        Return ds

    End Function

    Public Function GetInstructors(ByVal Course As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New UnscheduleClsDB
        Dim ds As New DataSet

        '   get the dataset with all degrees
        ds = DB.GetInstructors(Course)

        'Add the concatenated FullName column to the datatable before sending it to the web page
        'ds = ConcatenateEmployeeLastFirstName(ds)

        Return ds
    End Function

    Public Function ConcatenateEmployeeLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("Instructors")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("InstructorId")}
        End With
        If ds.Tables("Instructors").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                row("FullName") = row("LastName") & ", " & row("FirstName")
            Next
        End If
        Return ds
    End Function

    Public Function PopulateDataGrid(ByVal SDate As String, ByVal EDate As String, ByVal CampusId As String) As DataSet

        Dim db As New UnscheduleClsDB
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        ds = db.GetDataGridInfo(SDate, EDate, CampusId)

        Return ds

    End Function
    Public Function PopulateDataGridOnItemCmd(ByVal SDate As String, ByVal EDate As String) As DataSet

        Dim db As New UnscheduleClsDB
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        ds = db.GetDataGridInfoOnItmCmd(SDate, EDate)
        Return ds

    End Function

    'Public Function BuildDataGridInfo(ByVal ds As DataSet) As DataSet
    '    Dim tbl1 As New DataTable
    '    Dim row As DataRow
    '    Dim selHours As Integer
    '    Dim selCnt As Integer
    '    Dim txtChildHrs As String
    '    Dim txtChildCrds As String
    '    Dim hcRow As DataRow
    '    Dim tbl2 As New DataTable


    '    tbl1 = ds.Tables("Unschedule")
    '    If (ds.Tables("Unschedule").Rows.Count = 0) Then

    '    End If
    '    'Make the ChildId column the primary key
    '    With tbl1
    '        .PrimaryKey = New DataColumn() {.Columns("ClsSectionId")}
    '    End With

    '    tbl2 = ds.Tables("UnschedClsSects")
    '    'Make the ChildId column the primary key
    '    With tbl2
    '        .PrimaryKey = New DataColumn() {.Columns("ClsSectionId")}
    '    End With
    '    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '    If ds.Tables("Unschedule").Rows.Count > 0 Then
    '        'Load a datatable with ChildId, ChildTyp and ChildDescrip
    '        'Create a datable with a new column called ChildId, make this the primary key.

    '        'Add a new column called ChildDescrip to the datatable 
    '        Dim col10 As DataColumn = tbl1.Columns.Add("Checked", GetType(Boolean))

    '        For Each row In tbl1.Rows

    '            If (tbl2.Rows.Count > 0) Then
    '                Dim row2 As DataRow = tbl2.Rows.Find(row("ClsSectionId"))
    '                If Not (row2 Is Nothing) Then
    '                    row("Checked") = True
    '                Else
    '                    row("Checked") = False
    '                End If
    '            End If
    '        Next

    '    End If
    '    Return ds
    'End Function
    Public Function PopulateDataList(ByVal Term As String, ByVal Course As String, ByVal Instructor As String, ByVal campusId As String) As DataSet

        Dim db As New UnscheduleClsDB
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        Dim UnschedDateInfo As New UnscheduleClsInfo
        ds = db.PopulateClsSectDataList(Term, Course, Instructor, campusId)

        Return ds
    End Function

    Public Function InsertUnschedDate(ByVal UnschedObj As UnscheduleClsInfo, ByVal user As String)
        Dim db As New UnscheduleClsDB

        db.InsertUnschedDate(UnschedObj, user)
    End Function

    Public Function DeleteUnschedDate(ByVal UnschedObj As UnscheduleClsInfo)
        Dim DB As New UnscheduleClsDB

        DB.DeleteUnschedDate(UnschedObj)
    End Function
    'Public Function CancelClassesAndReSchedule(ByVal dsCancelledClasses As DataSet)
    '    Dim db As New UnscheduleClsDB
    '    db.CancelClassesAndReSchedule(dsCancelledClasses)
    'End Function
    Public Sub SetupRules(ByVal xmlRules As String)
        Dim db As New UnscheduleClsDB
        db.SetupRules(xmlRules)
    End Sub
    Public Function CheckIfAttendancePostedOnCancelledDate(ByVal ClsMeetingId As String, ByVal StartDate As Date, ByVal EndDate As Date) As Boolean
        Dim db As New UnscheduleClsDB
        Return (db.CheckIfAttendancePostedOnCancelledDate(ClsMeetingId, StartDate, EndDate))
    End Function
    Public Sub SetupMeetings(ByVal xmlRules As String)
        Dim db As New UnscheduleClsDB
        db.SetupMeetings(xmlRules)
    End Sub
    Public Function RescheduleClassMeetings(ByVal xmlRules As String) As Integer
        Dim db As New UnscheduleClsDB
        Return (db.RescheduleClassMeetings(xmlRules))
    End Function
End Class
