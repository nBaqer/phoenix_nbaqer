'
' FAME
' Copyright (c) 2006
' Written by ThinkTron Corporation (www.thinktron.com)
'
' Project:  AR 
' Description: Defines functions that are common throughout the AR Module

Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports FAME.AdvantageV1.DataAccess.AR
Imports FAME.Advantage.Common

Namespace AR

    Public Class ARCommon
#Region "Advantage Specific integration"
        ''' <summary>
        ''' Initalizes parameters that used throughout TM
        ''' These parameters are stored in session variables which include
        ''' Session("userid") and Session("campusid")
        ''' </summary>        
        ''' <remarks></remarks>
        Public Shared Sub AdvInit(ByVal pg As Page)
            ' setup params needed throughout 
            If HttpContext.Current.Session("UserId") Is Nothing Then
                HttpContext.Current.Session("UserId") = HttpContext.Current.Request.Params("UserId")
            End If
            If HttpContext.Current.Session("UserName") Is Nothing Then
                HttpContext.Current.Session("UserName") = GetUserNameFromUserId(HttpContext.Current.Session("UserId"))
            End If
            If HttpContext.Current.Session("cmpid") Is Nothing Then
                HttpContext.Current.Session("cmpid") = HttpContext.Current.Request.Params("cmpid")
            End If
            If HttpContext.Current.Session("resid") Is Nothing Then
                HttpContext.Current.Session("resid") = HttpContext.Current.Request.Params("resid")
            End If

            ' display an error if the session has timed out
            If GetCurrentUserId() Is Nothing Then
                HttpContext.Current.Session("Error") = "Session has expired"
                Dim url As String = HttpContext.Current.Server.MapPath("ErrorPage.aspx")
                HttpContext.Current.Response.Redirect(url)
            End If
        End Sub

        Public Shared Function GetCurrentUserId() As String
            If Not HttpContext.Current.Request.Params("UserId") Is Nothing Then
                Return HttpContext.Current.Request.Params("UserId")
            End If
            Return HttpContext.Current.Session("UserId")
        End Function

        Public Shared Function GetCampusID() As String
            If Not HttpContext.Current.Request.Params("cmpid") Is Nothing Then
                Return HttpContext.Current.Request.Params("cmpid")
            End If
            Return HttpContext.Current.Session("cmpid")
        End Function

        Public Shared Function GetAdvAdminEmail() As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Return MyAdvAppSettings.AppSettings("FromEmailAddress")
        End Function

        ''' <summary>
        ''' Get a fullname from a userid
        ''' </summary>
        ''' <param name="UserId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetUserNameFromUserId(ByVal UserId As String) As String
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            ' check for a blank userid
            If UserId Is Nothing Or UserId = "" Then Return ""
            ' format select sql
            Dim sql As String = "select UserName from syUsers where UserId='" + UserId + "'"
            Dim name As String = ""
            Try
                name = db.RunParamSQLScalar(sql)
            Catch ex As Exception
            End Try
            Return name
        End Function
#End Region
#Region "Debugging and error"
        Public Shared Sub DisplayErrorMessage(ByVal pg As Page, ByVal msg As String)
            System.Diagnostics.Debug.WriteLine(msg)
            msg = msg.Replace("'", "")
            msg = msg.Replace(vbCrLf, "\n")
            ARCommon.Alert(pg, msg)
        End Sub
#End Region
#Region "Browser Javascript Methods"
        Public Shared Sub SetBrowserStatusBar(ByRef p As Page, ByVal strStatus As String)
            Dim js As New StringBuilder
            js.Append("<script type=""text/javascript"">" & vbCrLf)
            js.Append("window.status=""" & strStatus)
            js.Append(""" " & vbCrLf)
            js.Append("</script>")
            p.ClientScript.RegisterStartupScript(p.GetType(), "jsStatus", js.ToString())
        End Sub

        Public Shared Sub SetBrowserTitle(ByRef p As Page, ByVal strStatus As String)
            Dim js As New StringBuilder
            js.Append("<script type=""text/javascript"">" & vbCrLf)
            js.Append("parent.document.title=""" & strStatus)
            js.Append(""" " & vbCrLf)
            js.Append("</script>")
            p.ClientScript.RegisterStartupScript(p.GetType(), "jsStatus", js.ToString())
        End Sub

        Public Shared Function GetJavsScriptPopup(ByVal popupUrl As String, _
                                                  ByVal width As Integer, _
                                                  ByVal height As Integer, _
                                                  ByVal retControlId As String) As String
            Dim js As New StringBuilder
            js.Append("var strReturn; " & vbCrLf)
            js.Append("strReturn=window.showModalDialog('")
            js.Append(popupUrl)
            js.Append("',null,'resizable:yes;status:no;scrollbars=yes;minimize:yes; maximize:yes; dialogWidth:")
            js.Append(width.ToString())
            js.Append("px;dialogHeight:")
            js.Append(height.ToString())
            js.Append("px;dialogHide:true;help:no;scroll:yes'); " & vbCrLf)
            If Not retControlId Is Nothing Then
                If retControlId.Length <> 0 Then
                    js.Append("if (strReturn != null) " & vbCrLf)
                    js.Append("     document.getElementById('")
                    js.Append(retControlId)
                    js.Append("').value=strReturn;" & vbCrLf)
                End If
            End If
            Return js.ToString()
        End Function

        Public Shared Sub Alert(ByRef p As Page, ByVal msg As String)
            p.ClientScript.RegisterStartupScript(p.GetType(), "sendJS", "<script language='javascript'>window.alert('" & msg.Replace("'", "") & "')</script>")
        End Sub
#End Region
#Region "Build DDL Methods"
        Public Shared Sub BuilStatusesDDL(ByVal ddl As DropDownList)
            ddl.Items.Clear()
            ddl.Items.Add(New ListItem("Active", "True"))
            ddl.Items.Add(New ListItem("Inactive", "False"))
        End Sub

        Public Shared Sub BuildDegreesDDL(ByVal CampGrpId As String, ByVal ddl As DropDownList)
            ddl.DataSource = ProgramsDB.GetDegrees(CampGrpId)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildAcademicCalendarDDL(ByVal ddl As DropDownList)
            ddl.DataSource = ProgramsDB.GetAcademicCalendars()
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "Id"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildCampusGroupsDDL(ByVal ddl As DropDownList)
            ddl.DataSource = UtilitiesDB.GetCampusGroups()
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "Id"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildProgramGroupsDDL(ByVal ddl As DropDownList)
            ddl.DataSource = PrgVersionsDB.GetProgramGroups()
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildDepartmentsDDL(ByVal ddl As DropDownList)
            ddl.DataSource = UtilitiesDB.GetDepartments()
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildChargingMethodsDDL(ByVal ddl As DropDownList)
            ddl.DataSource = UtilitiesDB.GetChargingMethods()
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildSAPDDL(ByVal ddl As DropDownList)
            ddl.DataSource = UtilitiesDB.GetSAPPolcies()
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildGradeSystemDDL(ByVal CampGrpId As String, ByVal ddl As DropDownList)
            ddl.DataSource = UtilitiesDB.GetGradeSystems(CampGrpId)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildProgramTypesDDL(ByVal CampGrpId As String, ByVal ddl As DropDownList)
            ddl.DataSource = UtilitiesDB.GetProgramTypes(CampGrpId)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildTuitionEarningsDDL(ByVal CampGrpId As String, ByVal ddl As DropDownList)
            ddl.DataSource = UtilitiesDB.GetTuitionEarnings(CampGrpId)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildTestingModelDDL(ByVal ddl As DropDownList)
            ddl.DataSource = UtilitiesDB.GetTestingModels()
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildAttendanceTypesDDL(ByVal ddl As DropDownList)
            ddl.Items.Clear()
            ddl.Items.Add("None")
            ddl.Items.Add("Present Absent")
            ddl.Items.Add("Minutes")
            ddl.Items.Add("Clock Hours")
        End Sub

        Public Shared Sub BuildTransCodesDDL(ByVal ddl As DropDownList)
            ddl.DataSource = PrgVersionFeesDB.GetTransCodes()
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildTutionCategoriesDDL(ByVal ddl As DropDownList)
            ddl.DataSource = PrgVersionFeesDB.GetTuitionCategories()
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildRateSchedulesDDL(ByVal ddl As DropDownList)
            ddl.DataSource = PrgVersionFeesDB.GetRateSchedules()
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildUnitTypesDDL(ByVal PrgVerId As String, ByVal ddl As DropDownList)
            ddl.DataSource = PrgVersionFeesDB.GetUnitTypes(PrgVerId)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildSysGradeBookComponents(ByVal ddl As DropDownList)
            ddl.DataSource = GradeBookDB.GetSysGradeBookComponents()
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub

        Public Shared Sub BuildStuEnrollmentStatuses(ByVal ddl As DropDownList, Optional ByVal campusId As String = "")
            ddl.DataSource = StudentEnrollmentsDB.GetEnrollmentStatusCodes(campusId)
            ddl.DataTextField = "Descrip"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("---Select---", ""))
            ddl.SelectedIndex = 0
        End Sub
#End Region
    End Class

End Namespace