Public Class LeadFacade
    Public Function UpdateStudentEducation(ByVal educationInfo As PlacementInfo, ByVal user As String, ByVal major As String) As String
        With New LeadDB
            If (educationInfo.IsInDb = False) Then
                Return .AddStudentEducation(educationInfo, user)
            Else
                Return .UpdateStudentEducation(educationInfo, user, major)
            End If
        End With
    End Function
    Public Function CheckDuplicates(ByVal EducationInstId As String, ByVal leadId As String, ByVal GraduateDate As String, ByVal CertificateId As String) As Integer
        With New LeadDB
            Return .CheckDuplicates(EducationInstId, leadId, GraduateDate, CertificateId)
        End With
    End Function
    'Public Function GetDocumentsByPrgVersion(ByVal LeadId As String) As DataSet
    '    With New LeadDB
    '        Return .GetDocumentsByPrgVersion(LeadId)
    '    End With
    'End Function
    Public Function InsertEntranceTest(ByVal LeadId As String, ByVal selectedEntranceTest() As String, ByVal selectedRequired() As String, ByVal selectedPass() As String, ByVal selectedTestTaken() As String, ByVal selectedActualScore() As String, ByVal selectedMinScore() As String, ByVal selectedComments() As String, ByVal selectedOverRide() As String, ByVal User As String) As Integer
        With New AdReqsDB
            Return .InsertEntranceTest(LeadId, selectedEntranceTest, selectedRequired, selectedPass, selectedTestTaken, selectedActualScore, selectedMinScore, selectedComments, selectedOverRide, User)
        End With
    End Function
    Public Function InsertStudentEntranceTest(ByVal StudentId As String, ByVal selectedEntranceTest() As String, ByVal selectedRequired() As String, ByVal selectedPass() As String, ByVal selectedTestTaken() As String, ByVal selectedActualScore() As String, ByVal selectedMinScore() As String, ByVal selectedComments() As String, ByVal selectedOverRide() As String, ByVal User As String) As Integer
        With New AdReqsDB
            Return .InsertStudentEntranceTest(StudentId, selectedEntranceTest, selectedRequired, selectedPass, selectedTestTaken, selectedActualScore, selectedMinScore, selectedComments, selectedOverRide, User)
        End With
    End Function
    Public Function UpdateOverRide(ByVal LeadId As String, ByVal selectedEntranceTest() As String, ByVal selectedPass() As String, ByVal selectedDocumentStatus() As String, ByVal selectedReqType() As String, ByVal selectedOverRide() As String, ByVal User As String) As Integer
        With New AdReqsDB
            Return .UpdateOverRide(LeadId, selectedEntranceTest, selectedPass, selectedDocumentStatus, selectedReqType, selectedOverRide, User)
        End With
    End Function
    Public Function StandardReqByGrpAndEffectiveDates(ByVal LeadId As String, ByVal strEnrollDate As Date) As String
        With New AdReqsDB
            Return .StandardReqByGrpAndEffectiveDates(LeadId, strEnrollDate)
        End With
    End Function
    Public Function GetDocumentsByProgramVersionAndStatus(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        With New LeadDB
            Return .GetDocumentsByProgramVersionAndStatus(LeadId, PrgVerId)
        End With
    End Function
    Public Function GetLeadGrpCount() As Integer
        With New AdReqsDB
            Return .GetLeadGroupCount()
        End With
    End Function
    Public Function GetAllLeadGroupsByRequirement(ByVal adReqId As String) As DataSet
        With New AdReqsDB
            Return .GetAllLeadGroupsByRequirement(adReqId)
        End With
    End Function
    Public Function GetReqsByRequirementgroup(ByVal ReqGrpId As String, ByVal Status As String) As DataSet
        Dim ds As New DataSet
        With New LeadEntranceDB
            ds = .GetReqsByRequirementgroup(ReqGrpId, Status)
        End With
        Dim dr As DataRow
        For Each dr In ds.Tables(0).Rows
            If Not dr("Required") Is System.DBNull.Value Then
                If dr("Required") = True Then
                    dr("Required") = True
                Else
                    dr("Required") = False
                End If
            Else
                dr("Required") = False
            End If
        Next
        Return ds
    End Function
    Public Function InsertLeadGroupsByRequirement(ByVal adReqId As String, ByVal LeadGrpId() As String, ByVal Required() As String, ByVal user As String) As Integer
        With New AdReqsDB
            Return .InsertLeadGroupsByRequirement(adReqId, LeadGrpId, Required, user)
        End With
    End Function
    Public Function DeleteLeadGroups(ByVal RefEffectiveDateId As String) As Integer
        With New AdReqsDB
            Return .DeleteLeadGroups(RefEffectiveDateId)
        End With
    End Function
    Public Function DeleteReqGrpDef(ByVal LeadgrpId As String, ByVal ReqGrpId As String) As Integer
        With New LeadEntranceDB
            .DeleteReqGrpDef(LeadgrpId, ReqGrpId)
        End With
    End Function
    Public Function InsertReqsByLeadGrp(ByVal adReqId As String, ByVal LeadGrpId() As String, ByVal Required() As String, ByVal Selected() As String, ByVal User As String) As Integer
        With New AdReqsDB
            Return .InsertReqsByLeadGrp(adReqId, LeadGrpId, Required, Selected, User)
        End With
    End Function
    Public Function GetAllRequiredReqsCountByLeadGroup(ByVal adReqId As String) As Integer
        With New AdReqsDB
            Return .GetAllRequiredReqsCountByLeadGroup(adReqId)
        End With
    End Function
    Public Function GetAllAdmissionLeadGroups() As DataSet
        With New AdReqsDB
            Return .GetAllLeadGroups
        End With
    End Function
    Public Function GetUseScheduleAdmissionLeadGroups() As DataSet
        With New AdReqsDB
            Return .GetUseScheduleLeadGroups
        End With
    End Function
    Public Function GetAllRequiredReqsLeadGroups(ByVal adReqId As String) As DataSet
        With New AdReqsDB
            Return .GetAllRequiredReqsByLeadGroup(adReqId)
        End With
    End Function
    'Public Function GetOptionalDocumentsByPrgVersion(ByVal LeadId As String) As DataSet
    '    With New LeadDB
    '        Return .GetOptionalDocumentsByPrgVersion(LeadId)
    '    End With
    'End Function
    Public Function CheckIfPrgVersionExists(ByVal LeadId As String) As Integer
        With New LeadDB
            Return .CheckIfPrgVersionExists(LeadId)
        End With
    End Function
    Public Function GetCollegeNamesByID(ByVal InstType As String, ByVal StudentID As String) As DataSet
        'Instantiate DAL component
        With New LeadDB
            'get the dataset with all SkillGroups
            Return .GetCollegeNamesByID(InstType, StudentID)
        End With
    End Function
    Public Function GetCollegeNamesByLeadID(ByVal InstType As String, ByVal LeadID As String) As DataSet
        'Instantiate DAL component
        With New LeadDB
            'get the dataset with all SkillGroups
            Return .GetCollegeNamesByLeadID(InstType, LeadID)
        End With
    End Function
    Public Function GetStudentInfo(ByVal LeadId As String, ByVal CollegeId As String, ByVal InstType As String) As PlacementInfo
        'Instantiate DAL component
        With New LeadDB
            'get the dataset with all SkillGroups
            Return .GetStudentInfo(LeadId, CollegeId, InstType)
        End With
    End Function
    Public Function GetLeadsNotEnroll(Optional ByVal campusId As String = "", Optional ByVal userId As String = "") As DataSet
        With New LeadDB
            Return .GetLeadsNotEnroll(campusId, userId)
        End With
    End Function
    Public Function GetLeadStatusesByUserAndCampus(ByVal campusId As String, ByVal userId As String) As DataSet
        With New LeadDB
            Return .GetLeadStatusesByUserAndCampus(campusId, userId)
        End With
    End Function
    Public Function GetReassignLeadStatus(Optional ByVal strActiveOnly As String = "", Optional ByVal campGrpId As String = "") As DataSet
        With New LeadDB
            Return .GetReassignLeadStatus(strActiveOnly, campGrpId)
        End With
    End Function
    Public Function GetNewLeadStatus() As DataSet
        With New LeadDB
            Return .GetNewLeadStatus()
        End With
    End Function
    'GetLeadsNotEnroll()
    Public Function GetEnrolledLeadStatus(Optional ByVal campusId As String = "") As DataSet
        With New LeadDB
            Return .GetEnrolledLeadStatus(campusId)
        End With
    End Function

    Public Function GetReassignLeadStatusNoEnroll(ByVal campusId As String, ByVal userId As String) As DataSet
        With New LeadDB
            Return .GetReassignLeadStatusNoEnroll(campusId, userId)
        End With
    End Function
    Public Function GetRaces() As DataSet
        With New RacesDB
            Return .GetAllRaces()
        End With
    End Function
    Public Function GetMaritalStatus() As DataSet
        With New MaritalStatusesDB
            Return .GetAllMaritalStatuses()
        End With
    End Function
    Public Function GetGenders() As DataSet
        With New GendersDB
            Return .GetAllGenders()
        End With
    End Function
    Public Function GetCountries() As DataSet
        With New CountyDB
            Return .GetAllCountry()
        End With
    End Function
    Public Function GetCitizenShip() As DataSet
        With New CountyDB
            Return .GetAllCitizenShip()
        End With
    End Function
    Public Function GetNationality() As DataSet
        With New CountyDB
            Return .GetAllNationality()
        End With
    End Function
    Public Function GetShifts() As DataSet
        With New ShiftsDB
            Return .GetAllShifts()
        End With
    End Function
    Public Function GetPrograms(ByVal ProgramGrpID As String) As DataSet
        With New LeadDB
            Return .GetAllPrograms(ProgramGrpID)
        End With
    End Function
    Public Function GetAllProgramsByCampusAndUser(ByVal PrgGrpID As String, ByVal campusId As String, ByVal userId As String, Optional ByVal boolDisplayInActive As Boolean = False) As DataSet
        With New LeadDB
            Return .GetAllProgramsByCampusAndUser(PrgGrpID, campusId, userId, boolDisplayInActive)
        End With
    End Function
    Public Function GetAllProgramVersionByCampusAndUser(ByVal ProgID As String, ByVal campusId As String, ByVal userId As String, Optional ByVal boolDisplayInActive As Boolean = False) As DataSet
        With New LeadDB
            Return .GetAllProgramVersionByCampusAndUser(ProgID, campusId, userId, boolDisplayInActive)
        End With
    End Function
    Public Function GetStudentsByCampusFASAP(ByVal CampusId As String, Optional ByVal progVerId As String = "") As ArrayList
        With New LeadDB
            Return .GetStudentsByCampusFASAP(CampusId, progVerId)
        End With
    End Function

    Public Function GetStudentsByCampus(ByVal CampusId As String, Optional ByVal progVerId As String = "") As ArrayList
        With New LeadDB
            Return .GetStudentsByCampus(CampusId, progVerId)
        End With
    End Function
    'Public Function UpdateLeadMaster(ByVal leadinfo As LeadMasterInfo, ByVal user As String, ByVal strObjective As String) As String
    '    With New LeadDB
    '        If (leadinfo.IsInDB = False) Then
    '            Return .AddLead(leadinfo, user, strObjective)
    '        Else
    '            Return .UpdateLead(leadinfo, user, strObjective)
    '        End If
    '    End With
    'End Function
    Public Function UpdateLeadMaster(ByVal leadinfo As LeadMasterInfo, ByVal user As String, ByVal strObjective As String, Optional ByVal useRegent As Boolean = False, Optional ByVal bUpdateSSN As Boolean = True) As String
        With New LeadDB
            If (leadinfo.IsInDB = False) Then
                Return .AddLead(leadinfo, user, strObjective, useRegent)
            Else
                Return .UpdateLead(leadinfo, user, strObjective, useRegent, bUpdateSSN)
            End If
        End With
    End Function
    Public Function GetDocsStudentByStatus(ByVal DocStatusId As String, ByVal ModuleID As Integer, ByVal StudentId As String, ByVal CampusId As String) As DataSet
        With New LeadDB
            Return .GetDocsStudentByStatus(DocStatusId, ModuleID, StudentId, CampusId)
        End With
    End Function
    Public Function GetDocsStudentByStatusAndModule(ByVal DocStatusId As String, ByVal ModuleID As Integer, ByVal StudentId As String, ByVal CampusId As String, ByVal TypeofRequirement As Integer, ByVal DocumentTypeId As String) As DataSet
        With New LeadDB
            Return .GetDocsStudentByStatusAndModule(DocStatusId, ModuleID, StudentId, CampusId, TypeofRequirement,DocumentTypeId)
        End With
    End Function
    Public Function GetDocsStudent(ByVal ModuleID As Integer, ByVal StudentId As String, ByVal CampusId As String) As DataSet
        With New LeadDB
            Return .GetDocsStudent(ModuleID, StudentId, CampusId)
        End With
    End Function
    Public Function GetDocsByStudentAndModule(ByVal ModuleID As Integer, ByVal StudentId As String, ByVal CampusId As String) As DataSet
        With New LeadDB
            Return .GetDocsByStudentAndModule(ModuleID, StudentId, CampusId)
        End With
    End Function
    Public Function GetAllDocsStudentByCampus(ByVal CampusId As String) As DataSet
        With New LeadDB
            Return .GetAllDocsStudentByCampus(CampusId)
        End With
    End Function
    Public Function GetDocsStudentByModuleIdAndStatus(ByVal ModuleID As Integer, ByVal StudentId As String, ByVal CampusId As String, ByVal StatusId As String, ByVal TypeofRequirement As Integer, ByVal DocumentTypeId As String) As DataSet
        With New LeadDB
            Return .GetDocsStudentByModuleIdAndStatus(ModuleID, StudentId, CampusId, StatusId, TypeofRequirement, DocumentTypeId)
        End With
    End Function
    Public Function GetDocsStudentNoModule(ByVal StudentId As String, ByVal CampusId As String) As DataSet
        With New LeadDB
            Return .GetDocsStudentNoModule(StudentId, CampusId)
        End With
    End Function
    'Public Function UpdateLeadStudent(ByVal leadinfo As LeadMasterInfo, ByVal user As String, Optional ByVal stuEnrollmentId As String = "") As String
    '    With New LeadEnrollmentDB
    '        Return .AddLeadStudentEnrollment(leadinfo, user, stuEnrollmentId)
    '    End With
    'End Function
    Public Function DeleteLeadMaster(ByVal leadid As String, ByVal modDate As DateTime) As String
        With New LeadDB
            Return .DeleteLead(leadid, modDate)
        End With
    End Function
    Public Function CheckLeadHasGender(ByVal LeadId As String) As Integer
        With New LeadEnrollmentDB
            Return .CheckLeadHasGender(LeadId)
        End With
    End Function
    Public Function GetSourceCatagory() As DataSet
        With New LeadDB
            Return .GetAllSourceCatagory()
        End With
    End Function

    Public Function GetSponsors() As DataSet
        With New LeadDB
            Return .GetAllSponsor()
        End With
    End Function

    'Public Function GetAllFamilyIncome() As DataSet
    '    With New LeadDB
    '        Return .GetAllFamilyIncome()
    '    End With
    'End Function
    Public Function GetAllFamilyIncome(Optional ByVal strStatus As String = "Active") As DataSet
        With New LeadDB
            Return .GetAllFamilyIncome(strStatus)
        End With
    End Function
    Public Function CheckDuplicateFamilyIncome(ByVal ViewOrder As Integer) As DataSet
        With New LeadDB
            Return .CheckDuplicateFamilyIncome(ViewOrder)
        End With
    End Function
    Public Function GetPhoneStatuses() As DataSet
        With New LeadDB
            Return .GetAllPhoneStatuses()
        End With
    End Function
    Public Function GetPhoneTypes() As DataSet
        With New LeadDB
            Return .GetAllPhoneTypes()
        End With
    End Function
    Public Function GetAllLeadGroups() As DataSet
        With New LeadDB
            Return .GetAllLeadGroups()
        End With
    End Function
    Public Function GetAllProgramsByGroupAndLead(ByVal LeadId As String) As DataSet
        With New LeadDB
            Return .GetAllProgramsByGroupAndLead(LeadId)
        End With
    End Function
    Public Function GetAllProgramsByGroup() As DataSet
        With New LeadDB
            Return .GetAllProgramsByGroup()
        End With
    End Function
    Public Function GetAllProgramsByGroupAndCampusId(ByVal campusid As String) As DataSet
        With New LeadDB
            Return .GetAllProgramsByGroupAndCampusId(campusid)
        End With
    End Function
    Public Function GetAllPrograms(ByVal PrgGrpID As String) As DataSet
        With New LeadDB
            Return .GetAllPrograms(PrgGrpID)
        End With
    End Function
    Public Function GetAllSourceAdv(ByVal SourceTypeID As String) As DataSet
        With New LeadDB
            Return .GetAllSourceAdv(SourceTypeID)
        End With
    End Function
    Public Function GetAllLeadTypes() As DataSet
        With New LeadDB
            Return .GetAllLeadTypes()
        End With
    End Function
    Public Function GetAllPlacementRepsByCampusAndUserId(ByVal strCampusId As String, ByVal strUserId As String) As DataSet
        With New LeadDB
            Return .GetAllPlacementRepsByCampusAndUserId(strCampusId, strUserId)
        End With
    End Function
    Public Function GetAllAdmissionRepsByCampusAndUserId(ByVal strCampusId As String, ByVal strUserId As String, Optional ByVal LeadId As String = "") As DataSet
        Dim getAdminRep As New LeadDB
        Dim ds As New DataSet
        ds = getAdminRep.GetAllAdmissionRepsByCampusAndUserId(strCampusId, strUserId, LeadId)
        Return ds
    End Function
    Public Function GetAllAdmissionRepsByCampusAndUserIdAndUserRoles(ByVal strCampusId As String, ByVal strUserId As String, ByVal RoleId As Integer, Optional ByVal LeadId As String = "") As DataSet
        Dim getAdminRep As New LeadDB
        Dim ds As New DataSet
        ds = getAdminRep.GetAllAdmissionRepsByCampusAndUserIdAndUserRoles(strCampusId, strUserId, RoleId, LeadId)
        Return ds
    End Function
    Public Function GetAllAdmissionRepsByCampus(ByVal strCampusId As String, Optional ByVal repID As String = "") As DataSet
        Dim getAdminRep As New LeadDB
        Return getAdminRep.GetAllAdmissionRepsByCampus(strCampusId, repID)
    End Function
    Public Function GetAllAdmissionReps() As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .GetAllAdmissionReps()
            'Add the concatenated FullName column to the datatable before sending it to the web page
        End With
        ds = ConcatenateEmployeeLastFirstName(ds)
        Return ds
    End Function
    Public Function GetAllReassignAdmissionReps(ByVal AdmissionRepID As String, ByVal strCampusId As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .GetAllReassignAdmissionReps(AdmissionRepID, strCampusId)
            'Add the concatenated FullName column to the datatable before sending it to the web page
        End With
        'ds = ConcatenateEmployeeLastFirstName(ds)
        Return ds
    End Function
    Public Function GetAddressStatuses() As DataSet
        With New LeadDB
            Return .GetAllAddressStatuses()
        End With
    End Function
    'Public Function GetAllPreviousEducationByProgramVersion(ByVal PrgVerId As String) As DataSet
    '    With New LeadDB
    '        Return .GetAllPreviousEducationByProgramVersion(PrgVerId)
    '    End With
    'End Function
    'Public Function GetAllPreviousEducationByProgramVersion(ByVal PrgVerId As String, ByVal PreviousEducation As String) As DataSet
    '    With New LeadDB
    '        Return .GetAllPreviousEducationByProgramVersion(PrgVerId, PreviousEducation)
    '    End With
    'End Function
    'Public Function GetAllPreviousEducationByAnyProgramVersion(ByVal PrgVerId As String) As Integer
    '    With New LeadDB
    '        Return .GetAllPreviousEducationByAnyProgramVersion(PrgVerId)
    '    End With
    'End Function
    Public Function GetAllPreviousEducation() As DataSet
        With New LeadDB
            Return .GetAllPreviousEducation()
        End With
    End Function
    Public Function GetSourceType(ByVal SourceCatagoryID As String) As DataSet
        With New LeadDB
            Return .GetAllSourceType(SourceCatagoryID)
        End With
    End Function
    Public Function GenerateLead(ByVal AdmissionRepID As String, ByVal StartDate As String, ByVal EndDate As String, ByVal LeadStatus As String, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .GenerateLead(AdmissionRepID, StartDate, EndDate, LeadStatus, CampusId)
        End With
        ds = ConcatenateEmployeeLastFirstName1(ds)
        Return ds
    End Function
    Public Function GetAllStudentNames() As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .GetAllStudentNames()
        End With
        ds = ConcatenateStudentLastFirstName(ds)
        Return ds
    End Function
    Public Function GetAllDocumentStudentNames(ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .GetAllDocumentStudentNames(campusId)
        End With
        ds = ConcatenateStudentLastFirstName(ds)
        Return ds
    End Function
    Public Function GenerateLeadNoAdmissionRep(ByVal AdmissionRepID As String, ByVal StartDate As String, ByVal EndDate As String, ByVal LeadStatus As String, ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .GenerateLeadNoAdmissionRep(AdmissionRepID, StartDate, EndDate, LeadStatus, campusId)
        End With
        ds = ConcatenateEmployeeLastFirstName1(ds)
        Return ds
    End Function
    Public Function CheckDuplicateLead(ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strDOB As String, ByVal strCampusId As String, ByVal repid As String, ByVal Address As String, ByVal Phone As String, ByVal Phone2 As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .CheckDuplicateLead(strLastName, strFirstName, strSSN, strDOB, strCampusId, repid, Address, Phone, Phone2)
        End With

        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        'Dim zipMask As String
        Dim dr As DataRow

        'Get the mask for phone numbers and zip
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds
    End Function
    Public Function ContactStudentSearch(ByVal strSearch As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .ContactStudent(strSearch)
        End With
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        'Dim zipMask As String
        Dim phoneMask As String
        Dim dr As DataRow

        'Get the mask for phone numbers and zip
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        phoneMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
            Else
                dr("SSN") = ""
            End If
        Next

        For Each dr In ds.Tables(0).Rows
            If dr("Phone").ToString.Length >= 1 Then
                dr("Phone") = facInputMasks.ApplyMask(phoneMask, dr("Phone"))
            Else
                dr("Phone") = ""
            End If
        Next
        Return ds
    End Function
    Public Function GetBillingMethodByPrgVersion(ByVal PrgVerId As String) As DataSet
        With New LeadDB
            Return .GetBillingMethodByPrgVersion(PrgVerId)
        End With
    End Function
    Public Function CheckCountDuplicateStudent(ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strDOB As String) As Integer
        With New LeadDB
            Return .CheckCountDuplicateStudent(strLastName, strFirstName, strSSN, strDOB)
        End With
    End Function
    Public Function IsClockHourProg(ByVal PrgVerId As String) As Boolean
        With New LeadDB
            Return .IsClockHourProg(PrgVerId)
        End With
    End Function
    'Added by Theresa G on 8/26/2010 for mantis 19519: QA: Transfer hours should be shown only for clock hour programs and byDay schools.
    Public Function IsClockHourProgram(ByVal PrgVerId As String) As Boolean
        With New LeadDB
            Return .IsClockHourProgram(PrgVerId)
        End With
    End Function
    Public Function CheckDuplicateStudent(ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strDOB As String, ByVal strCampusId As String, ByVal strUserId As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .CheckDuplicateStudent(strLastName, strFirstName, strSSN, strDOB, strCampusId, strUserId)
        End With
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        'Dim zipMask As String
        Dim dr As DataRow

        'Get the mask for phone numbers and zip
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds
    End Function
    Public Function GenerateLeadByAdmissionRepId(ByVal AdmissionRepID As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .GenerateLeadByAdmissionRep(AdmissionRepID)
        End With
        ds = ConcatenateLeadNamesOnly(ds)
        Return ds
    End Function
    Public Function GenerateUnAssignedLeadByAdmissionRepId(ByVal AdmissionRepID As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .GenerateUnAssignedLeadByAdmissionRep(AdmissionRepID)
        End With
        ds = ConcatenateLeadNamesByCampusOnly(ds)
        Return ds
    End Function
    'GenerateLeadByAdmissionRe
    Public Function PopulateReassignLeads(ByVal LeadID As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .PopulateReassignLeads(LeadID)
        End With
        ds = ConcatenateLeadNames(ds)
        Return ds
    End Function
    Public Function GetLeadByAdmissionReps(ByVal AdmissionRepID As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .GetAllLeadsByAdmissionRep(AdmissionRepID)
        End With
        ds = ConcatenateLeadNames(ds)
        Return ds
    End Function
    Public Function PopulateAssignLeadsToCampus(ByVal LeadID As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .PopulateAssignLeadsToCampus(LeadID)
        End With
        ds = ConcatenateLeadNamesAndCampus(ds)
        Return ds
    End Function
    Public Function ConcatenateLeadNamesAndCampus(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT")
        With tbl
            .PrimaryKey = New DataColumn() { .Columns("LeadId")}
        End With
        If ds.Tables("InstructorDT").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                '                row("FullName") = row("LeadFirstName") & " " & row("LeadMiddleName") & " " & row("LeadLastName") & " - " & row("CampusDescrip")
                row("FullName") = row("LeadFirstName") & " " & row("LeadMiddleName") & " " & row("LeadLastName")
            Next
        End If
        Return ds
    End Function
    Public Function ConcatenateLeadNames(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT")
        With tbl
            .PrimaryKey = New DataColumn() { .Columns("LeadId")}
        End With
        If ds.Tables("InstructorDT").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                row("FullName") = row("LeadFirstName") & " " & row("LeadMiddleName") & " " & row("LeadLastName")
            Next
        End If
        Return ds
    End Function
    Public Function ConcatenateLeadNamesOnly(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT2")
        With tbl
            .PrimaryKey = New DataColumn() { .Columns("LeadId")}
        End With
        If ds.Tables("InstructorDT2").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                row("FullName") = row("FirstName") & " " & row("MiddleName") & " " & row("LastName")
            Next
        End If
        Return ds
    End Function
    Public Function ConcatenateLeadNamesByCampusOnly(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT2")
        With tbl
            .PrimaryKey = New DataColumn() { .Columns("LeadId")}
        End With
        If ds.Tables("InstructorDT2").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                row("FullName") = row("FirstName") & " " & row("MiddleName") & " " & row("LastName") & " - " & row("CampusDescrip") & " - " & row("StatusDescrip")
            Next
        End If
        Return ds
    End Function
    Public Function AssignmentLead(ByVal selectedLeads() As String, ByVal CurrAdmissionRepID As String, ByVal user As String, ByVal selectedDegrees() As String, ByVal selectedStatus() As String, ByVal StartLeadDate As String, ByVal EndLeadDate As String) As Integer
        Dim ds As New DataSet
        With New LeadDB
            Return .UpdateAssignmentLead(selectedLeads, CurrAdmissionRepID, user, selectedDegrees, selectedStatus, StartLeadDate, EndLeadDate)
        End With
    End Function
    Public Function GetSAPResults(ByVal CampusId As String, Optional ByVal prgVerId As String = "") As DataSet
        Dim ds As New DataSet
        With New LeadDB
            Return .GetSAPResults(CampusId, prgVerId)
        End With
    End Function
    Public Function GetSAPResults1(ByVal CampusId As String, ByVal chkDate As String, Optional ByVal prgVerId As String = "") As DataSet
        Dim ds As New DataSet
        With New LeadDB
            Return .GetSAPResults1(CampusId, chkDate, prgVerId)
        End With
    End Function
    Public Function GetFASAPResults1(ByVal CampusId As String, ByVal chkDate As String, Optional ByVal prgVerId As String = "") As DataSet
        Dim ds As New DataSet
        With New LeadDB
            Return .GetFASAPResults1(CampusId, chkDate, prgVerId)
        End With
    End Function
    Public Function GetSAPResultsByStudent(ByVal StuEnrollId As String) As DataSet
        Dim ds As New DataSet
        Dim db As New LeadDB

        ds = db.GetSAPResultsByStudent(StuEnrollId)
        ds = BuildTrigDescrip(ds)
        Return ds

    End Function
    Public Function BuildTrigDescrip(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        'Dim SAPDetail As DataRow
        'Dim db As SAPCheckDB
        'Dim SAPDetailDS As DataSet

        tbl = ds.Tables(0)


        If ds.Tables(0).Rows.Count > 0 Then

            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("TrigDescrip", GetType(String))
            For Each row In tbl.Rows

                If row("TrigOffsetSeq").ToString() = "" Then
                    row("TrigDescrip") = Math.Round(row("TrigValue") / 100, 2) & " " & row("TrigUnitTypDescrip") & " " & "after the " & row("TrigOffTypDescrip")
                Else
                    row("TrigDescrip") = Math.Round(row("TrigValue") / 100, 2) & " " & row("TrigUnitTypDescrip") & " " & "after the " & row("TrigOffTypDescrip") & row("TrigOffsetSeq")
                End If

            Next
        End If
        Return ds
    End Function

    Public Function GetTestExists(ByVal TestId As String) As Integer
        Dim ds As New DataSet
        With New LeadDB
            Return .GetTestExists(TestId)
        End With
    End Function
    'GetSAPResults
    Public Function AssignmentLeadToCampus(ByVal selectedLeads() As String, ByVal CurrAdmissionRepID As String, ByVal SelectedCampusId() As String, ByVal user As String) As Integer
        Dim ds As New DataSet
        With New LeadDB
            Return .UpdateAssignmentLeadToCampus(selectedLeads, CurrAdmissionRepID, SelectedCampusId, user)
        End With
    End Function
    'UpdateAssignmentLead(ByVal LeadId As String, ByVal user As String, ByVal selectedDegrees() As String, ByVal selectedLeads() As String, ByVal selectedStatus() As String)
    Public Function GetNames() As DataSet
        'Dim ds As New DataSet
        'With New LeadDB
        '    ds = .GetAllAdmissionReps()
        '    'Add the concatenated FullName column to the datatable before sending it to the web page
        '    ds = ConcatenateEmployeeLastFirstName(ds)
        '    Return ds
        'End With
    End Function
    Public Function ConcatenateEmployeeLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT")
        With tbl
            .PrimaryKey = New DataColumn() { .Columns("EmpId")}
        End With
        If ds.Tables("InstructorDT").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                row("FullName") = row("LastName") & " " & row("FirstName") & " " & row("MI")
            Next
        End If
        Return ds
    End Function
    Public Function ConcatenateEmployeeLastFirstName1(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT")
        With tbl
            .PrimaryKey = New DataColumn() { .Columns("LeadId")}
        End With
        If ds.Tables("InstructorDT").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                ' DE7626 
                'row("FullName") = row("FirstName") & " " & row("LastName") & " " & row("MiddleName") & " - " & row("StatusDescrip")
                row("FullName") = row("FirstName") & " " & row("MiddleName") & " " & row("LastName") & " - " & row("StatusDescrip")
            Next
        End If
        Return ds
    End Function
    Public Function ConcatenateStudentLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorStudent")
        With tbl
            .PrimaryKey = New DataColumn() { .Columns("StudentId")}
        End With
        If ds.Tables("InstructorStudent").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                ' DE7626 
                'row("FullName") = row("FirstName") & " " & row("LastName") & " " & row("MiddleName")
                row("FullName") = row("FirstName") & " " & row("MiddleName") & " " & row("LastName")
            Next
        End If
        Return ds
    End Function
    Public Function ConcatenateLeadLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT")
        With tbl
            .PrimaryKey = New DataColumn() { .Columns("LeadId")}
        End With
        If ds.Tables("InstructorDT").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            Dim strSSN As String
            For Each row In tbl.Rows
                If Not row("ssn") Is System.DBNull.Value Then
                    strSSN = "***-**-" & Mid(row("ssn"), 6, 4)
                Else
                    strSSN = ""
                End If
                If Not strSSN = "" Then
                    ' DE7626 
                    'row("FullName") = row("FirstName") & " " & row("LastName") & " " & row("MiddleName") & " ( " & strSSN & ")"
                    row("FullName") = row("FirstName") & " " & row("MiddleName") & " " & row("LastName") & " ( " & strSSN & ")"
                Else
                    'row("FullName") = row("FirstName") & " " & row("LastName") & " " & row("MiddleName")
                    row("FullName") = row("FirstName") & " " & row("MiddleName") & " " & row("LastName")
                End If
            Next
        End If
        Return ds
    End Function
    Public Function GetLeadsEnrollment(ByVal LeadStatus As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .GetAllLeads(LeadStatus)
        End With
        ds = ConcatenateLeadLastFirstName(ds)
        Return ds
    End Function
    Public Function GetLeadsEnrollmentByCampus(ByVal LeadStatus As String, ByVal CampusId As String, ByVal userId As String, ByVal FirstName As String, ByVal LastName As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .GetAllLeadsByCampus(LeadStatus, CampusId, userId, FirstName, LastName)
        End With
        Dim dr As DataRow
        For Each dr In ds.Tables(0).Rows
            If Not dr("SSN") Is System.DBNull.Value Then
                'dr("SSN") = "***-**-" & Mid(dr("SSN"), 6, 4)
                dr("SSN") = dr("SSN")
            Else
                dr("SSN") = ""
            End If
        Next
        ds = ConcatenateLeadLastFirstName(ds)
        Return ds
    End Function
    Public Function GetAllLeadsByCampusUserAndNameFilters(ByVal LeadStatus As String, ByVal CampusId As String, ByVal userId As String, ByVal FirstName As String, ByVal LastName As String) As DataSet
        Dim ds As DataSet
        With New LeadDB
            ds = .GetAllLeadsByCampusUserAndNameFilters(LeadStatus, CampusId, userId, FirstName, LastName)
        End With
        Dim dr As DataRow
        For Each dr In ds.Tables(0).Rows
            If Not dr("SSN") Is System.DBNull.Value Then
                dr("SSN") = dr("SSN")
            Else
                dr("SSN") = ""
            End If
        Next
        ds = ConcatenateLeadLastFirstName(ds)
        Return ds
    End Function
    Public Function GetAllUnEnrolledLeadsByCampus(ByVal leadId As String, ByVal CampusId As String, ByVal userId As String, ByVal FirstName As String, ByVal LastName As String, ByVal ssn As String, ByVal leadstatus As String) As DataSet
        Dim ds As DataSet
        With New LeadDB
            ds = .GetAllUnEnrolledLeadsByCampus(leadId, CampusId, userId, FirstName, LastName, ssn, leadstatus)
        End With
        ds = ConcatenateLeadLastFirstName(ds)
        Return ds
    End Function
    Public Function GetAllRequirements() As DataSet
        Dim ds As DataSet
        With New LeadDB
            ds = .GetAllRequirementBydate()
        End With
        Return ds
    End Function
    Public Function GetAllLeads() As DataSet
        Dim ds As DataSet
        With New LeadDB
            ds = .GetAllLeadsNoFilter()
        End With
        ds = ConcatenateLeadLastFirstName(ds)
        Return ds
    End Function

    Public Function GetAllEnrolledLeads(ByVal LeadStatus As String, ByVal StartDate As String, ByVal EndDate As String, ByVal UserId As String, ByVal CampusId As String, Optional ByVal FirstName As String = "", Optional ByVal LastName As String = "") As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .GetAllEnrolledLeads(LeadStatus, StartDate, EndDate, UserId, CampusId, FirstName, LastName)
        End With
        ds = ConcatenateLeadLastFirstName(ds)
        Return ds
    End Function

    Public Function GetLeadsByCampusforSearchCtl(ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .GetLeadsByCampusforSearchCtl(CampusId)
        End With

        Return ds
    End Function


    'GetAllEnrolledLeads
    'Public Function GetLeadsInfo(ByVal LeadId As String) As LeadMasterInfo
    '    Dim GetLeads As New LeadDB
    '    With GetLeads
    '        Return .GetLeadInfo(LeadId)
    '    End With
    'End Function
    Public Function GetLeadsInfo(ByVal LeadId As String, Optional ByVal useRegent As Boolean = False) As LeadMasterInfo
        Dim GetLeads As New LeadDB
        With GetLeads
            Return .GetLeadInfo(LeadId, useRegent)
        End With
    End Function
    Public Function GetResponsibility(ByVal StudentId As String, ByVal EmployerId As String) As String
        Dim getResp As New LeadDB
        With getResp
            Return .GetResponsibility(StudentId, EmployerId)
        End With
    End Function
    Public Function GetLeadsUnEnrollmentInfo(ByVal LeadId As String) As LeadMasterInfo
        Dim GetLeads As New LeadDB
        With GetLeads
            Return .GetLeadUnEnrollmentInfo(LeadId)
        End With
    End Function
    Public Function AddExpRoles(ByVal StudentId As String, ByVal strEmployerId As String, ByVal strRoles As String) As Integer
        Dim strExpRoles As New LeadDB
        With strExpRoles
            Return .AddExpRoles(StudentId, strEmployerId, strRoles)
        End With
    End Function
    Public Function AddObjective(ByVal StudentId As String, ByVal strObjective As String) As Integer
        Dim strObj As New LeadDB
        With strObj
            Return .AddObjective(StudentId, strObjective)
        End With
    End Function
    Public Function DeleteObjective(ByVal StudentId As String) As Integer
        Dim strObj As New LeadDB
        With strObj
            Return .DeleteObjective(StudentId)
        End With
    End Function
    Public Function GetObjective(ByVal StudentId As String) As DataSet
        Dim strObj As New LeadDB
        With strObj
            Return .GetObjective(StudentId)
        End With
    End Function
    Public Function GetLeadObjective(ByVal StudentId As String) As String
        Dim strObj As New LeadDB
        With strObj
            Return .GetLeadObjective(StudentId)
        End With
    End Function
    Public Function GetJobRole(ByVal StudentId As String, ByVal EmployerId As String) As DataSet
        Dim strObj As New LeadDB
        With strObj
            Return .GetJobRole(StudentId, EmployerId)
        End With
    End Function
    Public Function IsLeadEnrolled(ByVal leadId As String) As Boolean
        Return (New LeadDB).IsLeadEnrolled(leadId)
    End Function
    Public Function AddReqGrp(ByVal ReqGrpInfo As AdReqGrpInfo, ByVal User As String) As String
        With New RequirementsDB
            If (ReqGrpInfo.IsInDB = False) Then
                Return .AddRequirementGroup(ReqGrpInfo, User)
            Else
                Return .UpdateRequirementGroup(ReqGrpInfo, User)
            End If
        End With
    End Function
    Public Function GetReqGroupInfo(ByVal ReqGrpId As String) As AdReqGrpInfo
        With New RequirementsDB
            Return .GetReqGroupInfo(ReqGrpId)
        End With
    End Function
    Public Function GetReqGroups() As DataSet
        With New RequirementsDB
            Return .GetReqGroups()
        End With
    End Function
    Public Function GetReqGroupsByStatus(ByVal Status As String) As DataSet
        With New RequirementsDB
            Return .GetReqGroupsByStatus(Status)
        End With
    End Function
    Public Function GetLeadDataForHeaderInfo(ByVal leadId As String) As LeadDataForHeaderInfo
        ' return LeadDataForHeaderInfo
        Return (New LeadDB).GetLeadDataForHeaderInfo(leadId)
    End Function
    Public Function GetAllLeadsForEmail(ByVal statusId As String, ByVal campGrpId As String, ByVal prgVerId As String) As DataSet
        'Instantiate DAL component
        With New LeadDB
            'return Dataset
            Return .GetAllLeadsForEmail(statusId, campGrpId, prgVerId)
        End With
    End Function
    Public Function DeleteCampusFromCampusGroups(ByVal CampusId As String) As String
        With New LeadDB
            Return .DeleteCampusFromCampusGroups(CampusId)
        End With
    End Function
    Public Function GetAllProgramsAJAX(ByVal PrgGrpID As String) As DataSet
        With New LeadDB
            Return .GetAllProgramsAJAX(PrgGrpID)
        End With
    End Function
    Public Function GetAllDataAJAX(ByVal ValueColumnName As String, ByVal DisplayColumnName As String, ByVal TableName As String, ByVal WhereColumnName As String, ByVal WhereColumnValue As String, ByVal OrderByColumnName As String) As DataSet
        With New LeadDB
            Return .GetAllDataAJAX(ValueColumnName, DisplayColumnName, TableName, WhereColumnName, WhereColumnValue, OrderByColumnName)
        End With
    End Function
    Public Function GetAllProgVersionsByProgramIdAJAX(ByVal ProgramId As String) As DataSet
        With New LeadDB
            Return .GetAllProgVersionsByProgramIdAJAX(ProgramId)
        End With
    End Function
    Public Function GetSourceTypeAJAX(ByVal SourceCatagoryID As String) As DataSet
        With New LeadDB
            Return .GetAllSourceTypeAJAX(SourceCatagoryID)
        End With
    End Function
    Public Function GetAllSourceAdvAJAX(ByVal SourceTypeID As String) As DataSet
        With New LeadDB
            Return .GetAllSourceAdvAJAX(SourceTypeID)
        End With
    End Function
    Public Function GetLeadsByAdmissionsRep(ByVal admissionsRepId As String, ByVal leadStatus As String) As DataSet
        'get leads by Admissions Rep
        Return (New LeadDB).GetLeadsByAdmissionsRep(admissionsRepId, leadStatus)
    End Function
    Public Function GetAllSourceCategoryByCampus(ByVal campusid As String, ByVal LeadId As String) As DataSet
        With New LeadDB
            Return .GetAllSourceCategoryByCampus(campusid, LeadId)
        End With
    End Function
    Public Function GetAllSourceTypeByCampus(ByVal SourceCategoryId As String, ByVal campusid As String, ByVal LeadId As String, Optional ByVal boolDisplayInActive As Boolean = False) As DataSet
        With New LeadDB
            Return .GetAllSourceTypeByCampus(SourceCategoryId, campusid, LeadId, boolDisplayInActive)
        End With
    End Function
    Public Function GetAllSourceAdvertisementByCampus(ByVal SourceTypeId As String, ByVal campusid As String, ByVal LeadId As String, Optional ByVal boolDisplayInActive As Boolean = False) As DataSet
        With New LeadDB
            Return .GetAllSourceAdvertisementByCampus(SourceTypeId, campusid, LeadId, boolDisplayInActive)
        End With
    End Function
    Public Function GetDefaultLeadStatusCodeId() As String
        With New LeadDB
            Return .GetDefaultLeadStatusCodeId()
        End With
    End Function
    Public Function GetDefaultLeadStatusCodeId(ByVal CampusId As String) As String
        With New LeadDB
            Return .GetDefaultLeadStatusCodeId(CampusId)
        End With
    End Function
    Public Function GetNewDefaultLeadStatusCodeId(ByVal campusId As String, ByVal UserId As String) As String
        With New LeadDB
            Return .GetNewDefaultLeadStatusCodeId(campusId, UserId)
        End With
    End Function
    Public Function GetExistingDefaultLeadStatusCodeId(ByVal CampusId As String, ByVal UserId As String, ByVal CurrentStatusId As String, ByVal LeadId As String) As String
        With New LeadDB
            Return .GetExistingDefaultLeadStatusCodeId(CampusId, UserId, CurrentStatusId, LeadId)
        End With
    End Function
    Public Function GetQuickLeadFields() As DataSet
        With New LeadDB
            Return .GetQuickLeadFields()
        End With
    End Function
    Public Function CheckDuplicateLeadsWithSamePhoneNumber(Optional ByVal CampusId As String = "",
                                                           Optional ByVal Phone1 As String = "",
                                                           Optional ByVal Phone2 As String = "") As Integer
        With New LeadDB
            Return .CheckDuplicateLeadsWithSamePhoneNumber(CampusId, Phone1, Phone2)
        End With
    End Function
    Public Function CheckDuplicateLeadsWithSameEmail(Optional ByVal CampusId As String = "",
                                                           Optional ByVal WorkEmail As String = "",
                                                           Optional ByVal HomeEmail As String = "") As Integer
        With New LeadDB
            Return .CheckDuplicateLeadsWithSameEmail(CampusId, WorkEmail, HomeEmail)
        End With
    End Function
    Public Function CheckDuplicateLeadsWithSameAddress(Optional ByVal CampusId As String = "",
                                                   Optional ByVal Address1 As String = "",
                                                   Optional ByVal Address2 As String = "",
                                                   Optional ByVal city As String = "",
                                                   Optional ByVal state As String = "",
                                                   Optional ByVal zip As String = "") As Integer
        With New LeadDB
            Return .CheckDuplicateLeadsWithSameAddress(CampusId, Address1, Address2, city, state, zip)
        End With
    End Function
    Public Function CheckDuplicateLeads(ByVal FirstName As String, ByVal LastName As String, Optional ByVal SSN As String = "", Optional ByVal DOB As String = "", Optional ByVal MiddleName As String = "", Optional ByVal CampusId As String = "") As Integer
        With New LeadDB
            Return .CheckDuplicateLeads(FirstName, LastName, SSN, DOB, MiddleName, CampusId)
        End With
    End Function

    Public Function CheckDuplicateSSN(ByVal FirstName As String, ByVal LastName As String, ByVal SSN As String) As String
        With New LeadDB
            Return .CheckDuplicateSSN(FirstName, LastName, SSN)
        End With
    End Function

    Public Function AddElectronicLead(ByVal selectedLeadId() As String,
                               ByVal selectedFirstName() As String,
                               ByVal selectedLastName() As String,
                               ByVal selectedAddress() As String,
                               ByVal selectedCity() As String,
                               ByVal selectedState() As String,
                               ByVal selectedZip() As String,
                               ByVal selectedPhone() As String,
                               ByVal selectedPhoneType() As String,
                               ByVal selectedEmail() As String,
                               ByVal selectedEmailType() As String,
                               ByVal selectedMaritalStatus() As String,
                               ByVal selectedSourceCategory() As String,
                               ByVal selectedSourceType() As String,
                               ByVal selectedSourceAdvertisement() As String,
                               ByVal selectedArea() As String,
                               ByVal selectedProgramType() As String,
                               ByVal selectedProgramVersion() As String,
                               ByVal CampusId As String, ByVal UserId As String, ByVal SubmittedDate As DateTime) As Integer

        With New LeadDB
            Return .AddElectronicLead(selectedLeadId,
                                selectedFirstName,
                                selectedLastName,
                                selectedAddress,
                                selectedCity,
                                selectedState,
                                selectedZip,
                                selectedPhone,
                                selectedPhoneType,
                                selectedEmail,
                                selectedEmailType,
                                selectedMaritalStatus,
                                selectedSourceCategory,
                                selectedSourceType,
                                selectedSourceAdvertisement,
                                selectedArea,
                                selectedProgramType,
                                selectedProgramVersion,
                                CampusId,
                                UserId, SubmittedDate)
        End With
    End Function
    Public Function GetAllImportedLeads(ByVal CampusId As String, ByVal DuplicateCount As String, ByVal SubmittedDate As DateTime) As DataSet
        Return (New LeadDB).GetAllImportedLeads(CampusId, DuplicateCount, SubmittedDate)
    End Function
    Public Function CheckDuplicateImportedLead(ByVal strFirstName As String, ByVal strLastName As String, ByVal strPhone As String) As Integer
        Return (New LeadDB).CheckDuplicateImportedLead(strFirstName, strLastName, strPhone)
    End Function
    Public Function GetAdmissionRepsAndLeadAssignedCount(ByVal CampusId As String, ByVal SubmittedDate As DateTime) As DataSet
        Return (New LeadDB).GetAdmissionRepsAndLeadAssignedCount(CampusId, SubmittedDate)
    End Function
    Public Function CheckIfStatusMappedToFutureStart() As String
        Return (New LeadEnrollmentDB).CheckIfStatusMappedToFutureStart
    End Function
    Public Function GetLeadGroupsByCampus(ByVal campusId As String) As DataSet
        With New LeadDB
            Return .GetLeadGroupsByCampus(campusId)
        End With
    End Function
    ''Added by Saraswathi Lakshmanan to fix issue 14385
    ''When entering a new lead information SA, Front Desk and Director
    '' of Admissions should be able to assign them to admission rep 
    ''Modified the checking condition from Dir of Admn to DirOfAdmn and Frontdesk and Username with sa

    Public Function GetAllAdmissionRepsByCampusUserIdUserRoles(ByVal strCampusId As String, ByVal strUserId As String, ByVal UserName As String, ByVal isDirOfAdOrFDesk As Integer, Optional ByVal LeadId As String = "") As DataSet
        Dim getAdminRep As New LeadDB
        Dim ds As New DataSet
        ds = getAdminRep.GetAllAdmissionRepsByCampusUserIdUserRoles(strCampusId, strUserId, UserName, isDirOfAdOrFDesk, LeadId)
        Return ds
    End Function
    Public Function GetLeadGroups_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable
        Dim db As New LeadDB
        Return db.GetLeadGroups_SP(showActiveOnly, campusId)
    End Function
    '' Code Added by kamalesh Ahuja on 11 June 2010 to Resolve mantis issue id 18411
    Public Function GetAllLeadsByCampusUserAndNameFiltersNew(ByVal LeadStatus As String, ByVal CampusId As String, ByVal userId As String, ByVal FirstName As String, ByVal LastName As String, ByVal LeadCampuses As String, Optional ByVal HasEditOtherPerm As Boolean = False) As DataSet
        Dim ds As New DataSet
        With New LeadDB
            ds = .GetAllLeadsByCampusUserAndNameFiltersNew(LeadStatus, CampusId, userId, FirstName, LastName, LeadCampuses, HasEditOtherPerm)
        End With
        Dim dr As DataRow
        For Each dr In ds.Tables(0).Rows
            If Not dr("SSN") Is System.DBNull.Value Then
                dr("SSN") = dr("SSN")
            Else
                dr("SSN") = ""
            End If
        Next
        ds = ConcatenateLeadLastFirstName(ds)
        Return ds
    End Function
    ''''' Code changes to Fix mantis issue id 19549 by kamalesh Ahuja on 16 Aug 2010 '''
    Public Function CheckLeadUser(ByVal UserId As String, ByVal LeadId As String) As Integer
        With New LeadDB
            Return .CheckLeadUser(UserId, LeadId)
        End With
    End Function
End Class
