Public Class RevenuePostedObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim objDB As New StuAccountBalanceDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(objDB.GetRevenuePosted(rptParamInfo), objDB.StudentIdentifier)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim strName As String
        Dim facInputMasks As New InputMasksFacade
        Dim oldCmpGrp As String = ""
        Dim oldCampus As String = ""
        Dim oldPrgVer As String = ""
        Dim filter As String = ""
        Dim dr As DataRow

        'Get mask for SSN
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count = 5 Then
                Dim dtDetail As DataTable = ds.Tables("RevenuePosted")
                Dim dtCmpGrp As DataTable = ds.Tables("CampusGroupTotals")
                Dim dtCampus As DataTable = ds.Tables("CampusTotals")
                Dim dtPrgVer As DataTable = ds.Tables("PrgVerTotals")
                Dim rw As DataRow

                For Each dr In dtDetail.Rows
                    'Compute totals
                    If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpDescrip") Then
                        oldCmpGrp = dr("CampGrpDescrip")
                        oldCampus = ""
                        oldPrgVer = ""
                        filter = "CampGrpDescrip='" & dr("CampGrpDescrip") & "'"
                        'Compute total by campus group
                        rw = dtCmpGrp.NewRow
                        rw("CampGrpDescrip") = oldCmpGrp
                        rw("TotalAmount") = dtDetail.Compute("SUM(TransAmount)", filter)
                        dtCmpGrp.Rows.Add(rw)
                    End If
                    If oldCampus = "" Or oldCampus <> dr("CampDescrip") Then
                        oldCampus = dr("CampDescrip")
                        oldPrgVer = ""
                        filter = "CampGrpDescrip='" & oldCmpGrp & "' AND CampDescrip='" & oldCampus & "'"
                        'Compute total by campus group and campus
                        rw = dtCampus.NewRow
                        rw("CampGrpDescrip") = oldCmpGrp
                        rw("CampusDescrip") = oldCampus
                        rw("TotalAmount") = dtDetail.Compute("SUM(TransAmount)", filter)
                        dtCampus.Rows.Add(rw)
                    End If
                    If oldPrgVer = "" Or oldPrgVer <> dr("PrgVerDescrip") Then
                        oldPrgVer = dr("PrgVerDescrip")
                        filter = "CampGrpDescrip='" & oldCmpGrp & "' " & _
                                    "AND CampDescrip='" & oldCampus & "' " & _
                                    "AND PrgVerDescrip='" & oldPrgVer.Replace("'", "''") & "'"
                        'Compute total by campus group, campus and program version
                        rw = dtPrgVer.NewRow
                        rw("CampGrpDescrip") = oldCmpGrp
                        rw("CampusDescrip") = oldCampus
                        rw("PrgVerDescrip") = oldPrgVer
                        rw("TotalAmount") = dtDetail.Compute("SUM(TransAmount)", filter)
                        dtPrgVer.Rows.Add(rw)
                    End If

                    'Set up record counter
                    dr("RecCount") = dtDetail.Rows.Count

                    'Set up student name as: "LastName, FirstName MI."
                    strName = ""
                    If Not dr.IsNull("LastName") Then
                        If dr("LastName") <> "" Then
                            strName = dr("LastName")
                        End If
                    End If
                    If Not dr.IsNull("FirstName") Then
                        If dr("FirstName") <> "" Then
                            If strName <> "" Then
                                strName &= ", " & dr("FirstName")
                            Else
                                strName &= dr("FirstName")
                            End If
                        End If
                    End If
                    If Not dr.IsNull("MiddleName") Then
                        If dr("MiddleName") <> "" Then
                            If strName <> "" Then
                                strName &= " " & dr("MiddleName") & "."
                            Else
                                strName &= dr("MiddleName") & "."
                            End If
                        End If
                    End If
                    dr("StudentName") = strName
                    '
                    'Apply mask to SSN.
                    If StudentIdentifier = "SSN" Then
                        If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                            If dr("StudentIdentifier") <> "" Then
                                Dim temp As String = dr("StudentIdentifier")
                                dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                            End If
                        End If
                    End If
                Next


            End If


        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
