﻿Public Class AdStudentGroupsFacade
    ''New Code Added By Vijay Ramteke On June 20, 2010
    Public Function GetStudentGroupsList(ByVal userid As String, ByVal CampusId As String, ByVal isUserSa As Boolean) As DataSet
        ''New Code Added By Vijay Ramteke On June 20, 2010
        ''Return (New AdStudentGroupsDB).GetStudentGroups(userid)
        Return (New AdStudentGroupsDB).GetStudentGroups(userid, CampusId, isUserSa)
        ''New Code Added By Vijay Ramteke On June 20, 2010
    End Function
    Public Function StatusIdGenerator() As DataSet
        Return (New AdStudentGroupsDB).StatusIdGenerator()
    End Function
    Public Function GetGroupTypes() As DataSet
        Return (New AdStudentGroupsDB).GetGroupTypes()
    End Function
    Public Function GetStudentGroupInfo(ByVal StuGrpId As String) As AdReqGrpInfo
        Return (New AdStudentGroupsDB).GetStudentGroupInfo(StuGrpId)
    End Function
    Public Function ValidateStudentGroup(ByVal StuGrpName As String) As String
        Return (New AdStudentGroupsDB).ValidateStudentGroup(StuGrpName)
    End Function
    Public Function AddStuGrp(ByVal StuGrpInfo As AdReqGrpInfo, ByVal User As String) As String
        With New AdStudentGroupsDB
            If (StuGrpInfo.IsInDB = False) Then
                Return .AddStudentGroup(StuGrpInfo, User)
            Else
                Return .UpdateStudentGroup(StuGrpInfo, User)
            End If
        End With
    End Function
    Public Function DeleteStudentGroup(ByVal StuGrpId As String) As String
        Dim getDB As New AdStudentGroupsDB
        Return getDB.DeleteStudentGroup(StuGrpId)
    End Function
    Public Function GetGroupStudents(ByVal StuGrpId As String) As DataSet
        Return (New AdStudentGroupsDB).GetGroupStudents(StuGrpId)
    End Function
    Public Function DeleteGroupStudents(ByVal StuGrpStuId As String, ByVal User As String) As String
        Dim getDB As New AdStudentGroupsDB
        Return getDB.DeleteGroupStudents(StuGrpStuId, User)
    End Function
    Public Function GetAllStudentGroup(ByVal StudentId As String) As DataSet
        Return (New AdStudentGroupsDB).GetAllStudentGroup(StudentId)
    End Function
    ''New Code Added By VIjay Ramteke On June 20, 2010 For Mantis Id 19197
    Public Function ValidateStudentGroupStudent(ByVal StudentId As String, ByVal StuGroupId As String, ByVal StuEnrollId As String) As String
        ''Return (New AdStudentGroupsDB).ValidateStudentGroupStudent(StudentId, StuGroupId)
        Return (New AdStudentGroupsDB).ValidateStudentGroupStudent(StudentId, StuGroupId, StuEnrollId)
    End Function
    Public Function AddStudentGroupStudent(ByRef StuGrpInfo As AdReqGrpInfo, ByVal User As String) As String
        Return (New AdStudentGroupsDB).AddStudentGroupStudent(StuGrpInfo, User)
    End Function
    ''New Code Added By VIjay Ramteke On June 20, 2010 For Mantis Id 19197
    Public Function CheckForStudentGroupOwner(ByVal StuGrpId As String, ByVal UserId As String) As String
        Return (New AdStudentGroupsDB).CheckForStudentGroupOwner(StuGrpId, UserId)
    End Function
    Public Function GetStudentGroupOwner(ByVal OwnerId As String) As DataSet
        Return (New AdStudentGroupsDB).GetStudentGroupOwner(OwnerId)
    End Function
    Public Function GetAllStudentEnrollments(ByVal StudentId As String) As DataSet
        Return (New AdStudentGroupsDB).GetAllStudentEnrollments(StudentId)
    End Function
    Public Function GetStudentGroupsByEnrollment(ByVal StuEnrollId As String) As DataSet
        Return (New AdStudentGroupsDB).GetStudentGroupsByEnrollment(StuEnrollId)
    End Function
    Public Function IsStudentOnRegistrationHold(ByVal StuEnrollId As String) As Boolean
        Dim onhold As Boolean = False
        Dim ds As New DataSet
        ds = (New AdStudentGroupsDB).GetStudentGroupsByEnrollment(StuEnrollId)
        For Each dr As DataRow In ds.Tables(0).Rows
            If dr("IsRegHold") = True Then
                onhold = True
                Exit For
            End If
        Next
        Return onhold
    End Function
End Class
