Imports System.Data
Imports System.Text.RegularExpressions


Public Class InputMasksFacade

    Public Enum InputMaskItem
        SSN = 1
        Phone = 2
        Zip = 3
    End Enum

    Public Function GetInputMasks() As DataTable
        Dim dbInputMasks As New InputMasksDB

        Return dbInputMasks.GetInputMasks
    End Function

    Public Function GetInputMaskForItem(ByVal itemId As InputMaskItem) As String
        Dim dbInputMasks As New InputMasksDB

        Return dbInputMasks.GetInputMaskForItem(itemId)
    End Function

    Public Sub UpdateInputMask(ByVal inputMaskId As Integer, ByVal mask As String)
        Dim dbInputMasks As New InputMasksDB

        dbInputMasks.UpdateInputMask(inputMaskId, mask)
    End Sub

    Public Function UpdateInputMaskInfo(ByVal iMaskInfo As InputMaskInfo, ByVal user As String) As String
        Dim dbInputMasks As New InputMasksDB

        Return dbInputMasks.UpdateInputMaskInfo(iMaskInfo, user)
    End Function

    Public Function UpdateInputMaskDS(ByVal ds As DataSet) As String
        Dim dbInputMasks As New InputMasksDB

        Return dbInputMasks.UpdateInputMaskDS(ds)
    End Function

    Public Function ApplyMask(ByVal strMask As String, ByVal strVal As String) As String
        Dim arrMask As New ArrayList
        Dim arrVal As New ArrayList
        Dim chr As Char
        'Dim strMaskChars As String = "#&?AULH\"
        Dim strMaskChars As String = "#"
        Dim intCounter As Integer
        Dim intCounter2 As Integer
        Dim strCorrVal As String
        Dim strReturn As String = ""
        Dim strPrev As String

        'Add each character in strMask to arrMask
        'Modified by Balaji on 2/18/2005 (If statement added)
        If Not strMask = "" Then
            For Each chr In strMask
                arrMask.Add(chr.ToString)
            Next
        End If

        'Add each character in strVal to arrVal
        For Each chr In strVal
            arrVal.Add(chr)
        Next

        If arrVal.Count >= 1 Then
            'We need to loop through the arrMask ArrayList and see if each item is a
            'mask character. If it is, we can use intCounter to get the corresponding
            'value from the arrVal ArrayList. Note that we ignore the \ character unless
            'it was preceded by another \. This means that at the end if we have \\ it
            'should be replaced by a \ and if we have a \ then it should be replaced
            'with an empty space.
            For intCounter2 = 0 To arrMask.Count - 1
                If arrMask(intCounter2).ToString() = "\" And strPrev <> "\" Then
                    'ignore
                ElseIf strMaskChars.IndexOf(arrMask(intCounter2).ToString()) <> -1 And arrVal.Count > intCounter Then
                    strCorrVal = arrVal(intCounter).ToString()
                    arrMask(intCounter2) = strCorrVal
                    intCounter += 1
                End If
                strPrev = arrMask(intCounter2).ToString()
            Next

            Dim strChars(arrMask.Count) As String
            arrMask.CopyTo(strChars)
            strReturn = String.Join("", strChars)


            If strReturn.IndexOf("\\") <> -1 Then
                strReturn = strReturn.Replace("\\", "\")

            ElseIf strReturn.IndexOf("\") <> -1 Then
                strReturn = strReturn.Replace("\", "")
            Else
                Return strReturn
            End If

            Return strReturn
        Else
            Return ""
        End If
    End Function

    Public Function RemoveMask(ByVal strMask As String, ByVal strVal As String) As String
        Dim arrMask As New ArrayList
        Dim arrVal As New ArrayList
        Dim arrRemovedVals As New ArrayList
        Dim chr As Char
        'Dim strMaskChars As String = "#&?A"
        Dim strMaskChars As String = "#"
        Dim intCounter As Integer
        Dim strCorrVal As String
        ''  Dim strPrev As String

        'Add each character in strMask to arrMask
        For Each chr In strMask
            arrMask.Add(chr.ToString)
        Next

        'Add each character in strVal to arrVal
        For Each chr In strVal
            arrVal.Add(chr)
        Next

        'We need to loop through the arrMask ArrayList and see if each item is a
        'mask character. If it is, we can use intCounter to get the corresponding
        'value from the arrVal ArrayList. 
        For intCounter = 0 To arrMask.Count - 1
            If strMaskChars.IndexOf(arrMask(intCounter).ToString()) <> -1 Then
                strCorrVal = arrVal(intCounter).ToString()
                arrRemovedVals.Add(strCorrVal)
            End If

        Next
        Dim strChars(arrRemovedVals.Count) As String
        arrRemovedVals.CopyTo(strChars)
        Return String.Join("", strChars)
    End Function

    Public Function GetRegEx(ByVal strMask As String) As String

        Dim arrMask As New ArrayList
        Dim chr As Char
        'Dim strMaskChars As String = "#&?A"
        Dim strMaskChars As String = "#"
        Dim intCounter As Integer

        'Add each character in strMask to arrMask
        For Each chr In strMask
            arrMask.Add(chr.ToString)
        Next

        'We need to loop through the arrMask ArrayList and see if each item is a
        'mask character. If it is we will replace it with the equivalent
        'RegularExpressionValidator character. If it is not then it is a literal and we need
        'to escape it with a \. For example if we have (305)-789-9500 then the first character
        'should be recorded as \(.
        For intCounter = 0 To arrMask.Count - 1
            If strMaskChars.IndexOf(arrMask(intCounter).ToString()) <> -1 Then
                arrMask(intCounter) = GetValidationChar(arrMask(intCounter).ToString())
            Else
                arrMask(intCounter) = "\" & arrMask(intCounter)
            End If
        Next
        Dim strChars(arrMask.Count) As String
        arrMask.CopyTo(strChars)
        Return String.Join("", strChars)
    End Function

    Private Function GetValidationChar(ByVal chr As Char) As String

        Select Case chr
            Case "#"
                Return "\d"
                'Case "&"
                'Return "\s"
                'Case "?"
                'Return "\D"
                'Case "A"
                'Return "\w"
        End Select
    End Function

    Public Function ValidateStringWithInputMask(ByVal strMask As String, ByVal strVal As String) As Boolean
        Dim objRegEx As Regex
        Dim regularExpression As String

        'If the mask and the string value are not the same length then we should return false.
        'This check is important because the IsMatch method of the Regex class only searches the
        'input string for a match. This means we could have a regular expression such as "\d{5}
        'but a string such "123456" will still return true because the pattern of five (5) 
        'consecutive digits is within the string.
        If strMask.Length <> strVal.Length Then
            Return False
        End If

        regularExpression = GetRegEx(strMask)
        objRegEx = New Regex(regularExpression)
        If objRegEx.IsMatch(strVal) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function CanZipMaskBeChanged(ByVal newZipMask As String) As Boolean
        'The zip mask cannot be changed if there are existing records (none international) that have
        'a different number of digits from the proposed new mask. For example, if there are existing records
        'with 5 digits then we should not allow the mask to be changed to 10 digits.
        Dim dbInputMasks As New InputMasksDB
        Dim zipLenToCheck As Integer
        Dim numFound As Integer
        Dim numPoundSymbol As Integer

        'Find out how many # symbols are in the zip mask passed in
        numPoundSymbol = GetNumberOfOccurencesOfCharInString(newZipMask, "#")
        If numPoundSymbol = 5 Then
            zipLenToCheck = 10
        ElseIf numPoundSymbol = 10 Then
            zipLenToCheck = 5
        End If

        'We are going to check each table that uses the zip field to see if there are any possible conflict.
        'As soon as we find a table that has a potential problem we will return false

        'adLeads
        numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("adLeads", zipLenToCheck)
        If numFound > 0 Then Return False

        'adColleges
        'numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("adColleges", zipLenToCheck)
        'If numFound > 0 Then Return False

        'syInstitutions
        'numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("syInstitutions", zipLenToCheck)
        'If numFound > 0 Then Return False

        'arStudAddresses
        numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("arStudAddresses", zipLenToCheck)
        If numFound > 0 Then Return False

        'plEmployers
        numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("plEmployers", zipLenToCheck)
        If numFound > 0 Then Return False

        'plEmployerContact
        numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("plEmployerContact", zipLenToCheck)
        If numFound > 0 Then Return False

        'faLenders
        numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("faLenders", zipLenToCheck)
        If numFound > 0 Then Return False


        'If we get to this point then we can return true
        Return True

    End Function

    Public Function GetNumberOfOccurencesOfCharInString(ByVal stringToCheck As String, ByVal charToCheckFor As Char) As Integer
        Dim intCounter As Integer
        Dim arrCharsToCheck As New ArrayList
        Dim chr As Char
        Dim numFound As Integer

        For Each chr In stringToCheck
            arrCharsToCheck.Add(chr.ToString)
        Next

        numFound = 0
        For intCounter = 0 To arrCharsToCheck.Count - 1
            If arrCharsToCheck(intCounter) = charToCheckFor Then
                numFound += 1
            End If
        Next

        Return numFound

    End Function
End Class
