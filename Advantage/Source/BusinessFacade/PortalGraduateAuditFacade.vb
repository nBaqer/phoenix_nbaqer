Imports System.Collections
Imports FAME.Advantage.Common
Imports System.Web.UI.WebControls

Public Class PortalGraduateAuditFacade

#Region "Private Data Members"

    Private dtGraduateAudit As New DataTable
    Private dtResults As New DataTable
    '
    Private m_GPA As Decimal
    Private m_AttemptedCredits As Decimal
    Private m_AllAttemptedCredits As Decimal
    Private m_EarnedCredits As Decimal
    Private m_EarnedFinAidCredits As Decimal
    Private m_CompletedHours As Decimal
    Private m_RemainingCredits As Decimal
    Private m_RemainingHours As Decimal
    Private m_TotalClasses As Integer
    Private m_PercentageEarnedCredits As Decimal
    Private m_GradeInGPA As Boolean
    ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
    Private m_TotalScheduledHours As Decimal
    Private m_AllScheduledHours As Decimal
    Private m_CreditZeroWeight As Decimal
    '''

#End Region

#Region "Properties"

    Public ReadOnly Property GPA() As Decimal
        Get
            Return m_GPA
        End Get
    End Property
    Public ReadOnly Property AttemptedCredits() As Decimal
        Get
            Return m_AttemptedCredits
        End Get
    End Property
    Public ReadOnly Property AllAttemptedCredits() As Decimal
        Get
            Return m_AllAttemptedCredits
        End Get
    End Property
    ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
    Public ReadOnly Property AllScheduledHours() As Decimal
        Get
            Return m_AllScheduledHours
        End Get
    End Property
    Public ReadOnly Property TotalScheduledHours() As Decimal
        Get
            Return m_TotalScheduledHours
        End Get
    End Property
    ''
    Public ReadOnly Property EarnedCredits() As Decimal
        Get
            Return m_EarnedCredits
        End Get
    End Property
    Public ReadOnly Property EarnedFinAidCredits() As Decimal
        Get
            Return m_EarnedFinAidCredits
        End Get
    End Property

    Public ReadOnly Property CompletedHours() As Decimal
        Get
            Return m_CompletedHours
        End Get
    End Property
    Public ReadOnly Property TotalClasses() As Integer
        Get
            Return m_TotalClasses
        End Get
    End Property
    Public ReadOnly Property RemainingCredits() As Decimal
        Get
            Return m_RemainingCredits
        End Get
    End Property
    Public ReadOnly Property RemainingHours() As Decimal
        Get
            Return m_RemainingHours
        End Get
    End Property
    Public ReadOnly Property PercentageEarnedCredits() As Decimal
        Get
            Return m_PercentageEarnedCredits
        End Get
    End Property
    Public ReadOnly Property GradeInGPA() As Boolean
        Get
            Return m_GradeInGPA
        End Get
    End Property

    Public ReadOnly Property CreditZeroWeight() As Decimal
        Get
            Return m_CreditZeroWeight
        End Get
    End Property
#End Region

#Region "Private Methods"

    Private Sub AddRowToDt(ByVal dr As DataRow, ByVal ds As DataSet, ByVal dt As DataTable, _
                            ByVal parentId As String, ByVal level As Integer, ByVal gradeReps As String, _
                            ByVal includeHours As Boolean, _
                            Optional ByRef fac As PortalTranscriptFacade = Nothing, _
                            Optional ByRef transDB As MyPortalTranscriptDb = Nothing, _
                            Optional ByRef br As PortalRegisterStudentsBR = Nothing, _
                            Optional ByRef transferGradedb As PortalTransferGradeDB = Nothing, _
                            Optional ByRef examsDb As MyPortalExamsDb = Nothing _
 )
        Debug.WriteLine("AddRowToDt entering...")
        Dim r As DataRow = dt.NewRow
        Dim aRows() As DataRow
        ' Instantiate services if  they not.
        Dim aRowsGroup() As DataRow
        If fac Is Nothing Then
            fac = New PortalTranscriptFacade
            transDB = New MyPortalTranscriptDb
            br = New PortalRegisterStudentsBR
            transferGradedb = New PortalTransferGradeDB()
            examsDb = New MyPortalExamsDb()
        End If

        Dim equivReqid As String
        Dim isPass As Boolean = False
        r("Descrip") = dr("Req") 'Course/Course Group/Program Version Description
        'Requirement Id
        r("ReqId") = dr("ReqId")
        'Course Code
        r("Code") = dr("Code")

        'Requirement Type Id
        r("ReqTypeId") = dr("ReqTypeId")

        'ParentId
        If parentId <> "" Then r("ParentId") = parentId

        'Level
        r("Level") = level

        'Credits
        r("Credits") = dr("Credits").ToString()
        r("FinAidCredits") = dr("FinAidCredits").ToString()
        'Hours
        r("Hours") = dr("Hours").ToString()

        r("PrgVerId") = dr("PrgVerId").ToString()
        r("IsPass") = False

' This part running over Courses ....
        If dr("ReqTypeId") = 1 Then 'Course

            Dim score As Decimal
            Dim gpal As Decimal

            If gradeReps.ToUpper = "LATEST" Then
                'Latest attempt
                aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "EndDate DESC, StartDate DESC")
            ElseIf gradeReps.ToUpper = "BEST" Then
                'Best GPA
                aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "GPA DESC,IsPass DESC")

            Else
                aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "GPA DESC,IsPass DESC")
                Dim reqid As String = dr("ReqId").ToString()
                If (ds.Tables("Results").Compute("Avg(GPA)", "ReqId = '" & reqid & "'")) Is DBNull.Value Then
                    gpal = 0.0
                Else
                    gpal = ds.Tables("Results").Compute("Avg(GPA)", "ReqId = '" & reqid & "'")
                End If

                If (ds.Tables("Results").Compute("Avg(Score)", "ReqId = '" & reqid & "'")) Is DBNull.Value Then
                    score = 0.0
                Else
                    score = ds.Tables("Results").Compute("Avg(Score)", "ReqId = '" & reqid & "'")
                End If

            End If

            If aRows.Length > 0 Then 'There are results for this course
                'Check if any of the results (grades) is included in credits earned
                Dim aRow As DataRow = aRows(0)
                r("Grade") = aRow("Grade")
                If Not aRow.IsNull("Score") Then

                    'r("Score") = CType(aRow("Score"), Decimal)
                    If gradeReps.ToUpper = "AVERAGE" Then
                        r("Score") = score
                    Else
                        r("Score") = CType(aRow("Score"), Decimal)
                    End If

                End If
                If aRow("Descrip").ToString.ToLower.Trim <> dr("Req").ToString.ToLower.Trim Then
                    r("EquivCourseDesc") = aRow("Descrip").ToString.Trim

                End If
                '
                If Not aRow.IsNull("IsInGPA") Then
                    If aRow("IsInGPA") = 1 Or aRow("IsInGPA") = True Then
                        If Not aRow.IsNull("GPA") Then
                           If gradeReps.ToUpper = "AVERAGE" Then
                                r("GPA") = gpal
                            Else
                                r("GPA") = aRow("GPA")
                            End If

                        End If
                    End If
                End If
                '
                'Start Date
                r("StartDate") = aRow("StartDate")
                r("grdSysDetailid") = aRow("grdSysDetailId")
                r("IsTransferGrade") = aRow("IsTransferGrade")

            End If
        End If

        'IsCreditsEarned, Credits Earned and Hours Completed 
        '       For a course we need to lookup that from the Results datatable in the dataset
        '       if we are dealing with a course
        If dr("ReqTypeId") = 1 Then 'Course

            aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "GPA DESC")

            If aRows.Length > 0 Then 'There are results for this course
                'Check if any of the results (grades) is included in credits earned
                Dim aRow As DataRow = aRows(0)
                If aRow("Descrip").ToString.ToLower.Trim <> dr("Req").ToString.ToLower.Trim Then
                    r("EquivCourseDesc") = aRow("Descrip").ToString.Trim
                    r("Credits") = aRow("Credits").ToString()
                    r("FinAidCredits") = aRow("FinAidCredits").ToString()
                    r("Hours") = aRow("Hours").ToString()
                End If
                isPass = br.GrdOverRide(dr("PrgVerId").ToString(), dr("reqId").ToString, aRow("Grade").ToString)
                r("IsPass") = isPass
                r("IsCreditsEarned") = aRow("IsCreditsEarned")
                r("grdSysDetailid") = aRow("grdSysDetailId")
                Try
                    If aRow("IsCreditsEarned") And isPass Then
                        ' ''' ''Call the function to add credits earned
                        Dim isHrsComp As Boolean = transDB.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                        If isHrsComp = True Then
                            r("CreditsEarned") = r("Credits")
                            r("EarnedFinAidCredits") = r("FinAidCredits")
                        Else
                            r("CreditsEarned") = 0
                            r("EarnedFinAidCredits") = 0
                        End If

                    Else
                        r("CreditsEarned") = 0
                        r("EarnedFinAidCredits") = 0
                    End If
                Catch ex As Exception
                    r("CreditsEarned") = 0
                    r("EarnedFinAidCredits") = 0
                End Try
                '
                'Even the student fails the course he/she should still be given full credit for the hours
                'completed. However, if the student drops the course then he/she should not be given any
                'credit for the hours completed.
                Try
                    If aRow.IsNull("IsDrop") Then
                        r("HoursCompleted") = 0
                        Exit Try
                    End If
                    If aRow("IsDrop") Then
                        r("HoursCompleted") = 0
                    Else
                        'Consider variable from the web.config:
                        '   When there is a failed grade and IncludeHoursForFailingGrade=False, HoursCompleted=0. 
                        '   Any other case, account for the hours. 
                        If Not aRow("IsPass") And Not includeHours Then
                            r("HoursCompleted") = 0
                        Else
                            If aRow("IsCreditsEarned") And isPass Then
                                ' ''' ''Call the function to add credits earned
                                Dim isHrsComp As Boolean = transDB.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                                If isHrsComp = True Then
                                    r("HoursCompleted") = r("Hours")
                                Else
                                    r("HoursCompleted") = 0
                                End If
                            Else
                                r("HoursCompleted") = 0
                            End If
                        End If
                    End If
                Catch ex As Exception
                    r("HoursCompleted") = 0
                End Try
                '
                'To compute CreditsAttempted, ONLY take into account 'Best' or 'Latest' try accordingly for this particular course.
                Dim aRow0 As DataRow = aRows(0)
                'r("CreditsAttempted") = 0 'Commented by BN 10/05/06 to take only best or latest try into account
                If Not aRow.IsNull("Credits") Then
                    If Not aRow0.IsNull("IsCreditsAttempted") Then
                        If aRow0("IsCreditsAttempted") = True Or aRow0("IsCreditsAttempted") = 1 Then
                            r("CreditsAttempted") = aRow0("Credits")
                            r("IsCreditsAttempted") = (r("CreditsAttempted") > 0)
                            r("AllCreditsAttempted") = aRow0("Credits") * aRows.Length
                        Else
                            r("CreditsAttempted") = 0
                            r("IsCreditsAttempted") = 0 '(r("CreditsAttempted") > 0)
                            r("AllCreditsAttempted") = 0
                        End If
                    Else
                        r("CreditsAttempted") = 0 'aRow0("Credits")
                        r("IsCreditsAttempted") = 0 '(r("CreditsAttempted") > 0)
                        r("AllCreditsAttempted") = 0 'aRow0("Credits") * aRows.Length

                    End If
                End If
                If Not aRow.IsNull("ScheduledHours") Then
                    If Not aRow0.IsNull("IsCreditsAttempted") Then
                        If aRow0("IsCreditsAttempted") = True Or aRow0("IsCreditsAttempted") = 1 Then
                            r("TotalScheduledHours") = aRow0("ScheduledHours")
                            r("AllScheduledHours") = aRow0("ScheduledHours") * aRows.Length
                        Else
                            r("TotalScheduledHours") = 0.0
                            r("AllScheduledHours") = 0.0
                        End If
                    Else
                        r("TotalScheduledHours") = 0.0
                        r("AllScheduledHours") = 0.0
                    End If
                End If

                ' No results for this course.
                'check for the equivalent courses
            Else
                r("IsCreditsEarned") = False
                r("IsCreditsAttempted") = False
                r("CreditsEarned") = 0
                r("EarnedFinAidCredits") = 0
                r("HoursCompleted") = 0
                r("CreditsAttempted") = 0
                r("TotalScheduledHours") = 0.0
                r("AllScheduledHours") = 0.0
                ''''''''''''''''

            End If
        End If

        If Not r.IsNull("CreditsEarned") Then
            If r("IsPass") = False And dr("ReqTypeId") = 1 Then
                equivReqid = GetEquivalentReqId(dr("ReqId").ToString).ToString
                If equivReqid <> "" Then
                    'tmpRows = dtGraduateAudit.Select("ReqId='" & equivReqid.ToString & "'")
                    aRows = ds.Tables("Results").Select("ReqId = '" & equivReqid.ToString & "'", "GPA DESC")
                    If aRows.Length > 0 Then 'There are results for this course
                        'Check if any of the results (grades) is included in credits earned
                        Dim aRow As DataRow = aRows(0)
                        isPass = br.GrdOverRide(dr("PrgVerId").ToString(), equivReqid.ToString, aRow("Grade").ToString)
                        Dim eqDescrip As String = transDB.GetCourseDescrip(equivReqid.ToString)
                        If isPass Then

                            If r.IsNull("Grade") Then
                                r("EquivCourseDesc") = eqDescrip
                                r("Grade") = aRow("Grade")
                                r("Credits") = aRow("Credits").ToString()
                                r("FinAidCredits") = aRow("FinAidCredits").ToString()
                                r("Hours") = aRow("Hours").ToString()
                                r("grdSysDetailid") = aRow("grdSysDetailId")
                            End If
                            If Not aRow.IsNull("Credits") Then
                                If Not aRow.IsNull("IsCreditsAttempted") Then
                                    r("CreditsAttempted") = aRow("Credits")
                                    r("IsCreditsAttempted") = (r("CreditsAttempted") > 0)
                                    r("AllCreditsAttempted") = aRow("Credits") * aRows.Length
                                Else
                                    r("CreditsAttempted") = 0 'aRow0("Credits")
                                    r("IsCreditsAttempted") = 0 '(r("CreditsAttempted") > 0)
                                    r("AllCreditsAttempted") = 0 'aRow0("Credits") * aRows.Length
                                End If
                            End If
                            If Not aRow.IsNull("IsInGPA") Then
                                If aRow("IsInGPA") = 1 Or aRow("IsInGPA") = True Then
                                    If Not aRow.IsNull("GPA") Then
                                        r("GPA") = aRow("GPA")
                                    End If
                                End If
                            End If
                        End If

                        If Not aRow.IsNull("ScheduledHours") Then
                            If Not aRow.IsNull("IsCreditsAttempted") Then
                                If aRow("IsCreditsAttempted") = True Or aRow("IsCreditsAttempted") = 1 Then
                                    r("TotalScheduledHours") = aRow("ScheduledHours")
                                    r("AllScheduledHours") = aRow("ScheduledHours") * aRows.Length
                                Else
                                    r("TotalScheduledHours") = 0.0
                                    r("AllScheduledHours") = 0.0
                                End If
                            Else
                                r("TotalScheduledHours") = 0.0
                                r("AllScheduledHours") = 0.0
                            End If
                        End If
                        '''''''''''''''''''''''''''''''''

                        If isPass Then
                            r("IsCreditsEarned") = aRow("IsCreditsEarned")
                            r("EquivCourseDesc") = eqDescrip
                            r("Grade") = aRow("Grade")
                            r("Credits") = aRow("Credits").ToString()
                            r("FinAidCredits") = aRow("FinAidCredits").ToString()
                            r("Hours") = aRow("Hours").ToString()
                            r("grdSysDetailid") = aRow("grdSysDetailId")

                            Try
                                If aRow("IsCreditsEarned") And isPass Then
                                    ' ''' ''Call the function to add credits earned
                                    Dim isHrsComp As Boolean = transDB.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                                    If isHrsComp = True Then
                                        r("CreditsEarned") = r("Credits")
                                        r("EarnedFinAidCredits") = r("FinAidCredits")
                                    Else
                                        r("CreditsEarned") = 0
                                        r("EarnedFinAidCredits") = 0
                                    End If
                                Else
                                    r("CreditsEarned") = 0
                                    r("EarnedFinAidCredits") = 0
                                End If
                            Catch ex As Exception
                                r("CreditsEarned") = 0
                                r("EarnedFinAidCredits") = 0
                            End Try
                            '
                            'Even the student fails the course he/she should still be given full credit for the hours
                            'completed. However, if the student drops the course then he/she should not be given any
                            'credit for the hours completed.
                            Try
                                If aRow.IsNull("IsDrop") Then
                                    r("HoursCompleted") = 0
                                    Exit Try
                                End If
                                If aRow("IsDrop") Then
                                    r("HoursCompleted") = 0
                                Else
                                    'Consider variable from the web.config:
                                    '   When there is a failed grade and IncludeHoursForFailingGrade=False, HoursCompleted=0. 
                                    '   Any other case, account for the hours. 
                                    If Not aRow("IsPass") And Not includeHours Then
                                        r("HoursCompleted") = 0
                                    Else

                                        If aRow("IsCreditsEarned") And isPass Then
                                            ' ''' ''Call the function to add credits earned
                                            Dim isHrsComp As Boolean = transDB.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                                            If isHrsComp = True Then
                                                r("HoursCompleted") = r("Hours")
                                            Else
                                                r("HoursCompleted") = 0
                                            End If
                                        Else
                                            r("HoursCompleted") = 0
                                        End If
                                    End If
                                End If
                            Catch ex As Exception
                                r("HoursCompleted") = 0
                            End Try
                        End If
                    End If
                End If

            End If
            'Credits Remaining. We can simply use the difference between the credits column 
            '       and the credits earned column.
            If r.IsNull("CreditsEarned") Then
                r("CreditsRemaining") = dr("Credits")
            Else
                r("CreditsRemaining") = r("Credits") - r("CreditsEarned")
            End If

            'Hours Remaining. We can simply use the difference between the hours column 
            '       and the hours completed column.
            If r.IsNull("HoursCompleted") Then
                r("HoursRemaining") = r("Hours")
            Else
                r("HoursRemaining") = r("Hours") - r("HoursCompleted")
            End If
            Dim boolIsCourseALab As Boolean
            If aRows.Length > 0 Then
                boolIsCourseALab = transferGradedb.isCourseALabWorkOrLabHourCourse_SP(dr("ReqId").ToString)
                Dim boolClinicSatisfied As Boolean = examsDb.isClinicCourseCompletlySatisfied(dr("StuEnrollId").ToString(), 500, dr("ReqId").ToString)
                If boolIsCourseALab Then
                    Dim boolRemaining As Boolean = examsDb.IsRemainingLabCount(dr("StuEnrollId").ToString(), 500, dr("ReqId").ToString)
                    If boolRemaining = False And boolClinicSatisfied = True Then
                        If transferGradedb.isCourseALabWorkOrLabHourCourseCombination(dr("ReqId").ToString) Then
                            If isPass Then
                                r("HoursCompleted") = r("Hours")
                                r("HoursRemaining") = 0
                            Else
                                r("HoursCompleted") = 0
                                r("HoursRemaining") = r("Hours")
                            End If
                        Else
                            r("HoursCompleted") = r("Hours")
                            r("HoursRemaining") = 0
                        End If

                    End If
                End If
            End If

        End If

        '   Add row into datatable
        dt.Rows.Add(r)

        'If we are dealing with a course group then we want to render its children as well.
        'A course group can contain other course groups so we will have to make a recursive
        'call to ensure that all children of the course group is rendered.
        If dr("ReqTypeId") = 2 Then
            'Get the direct children for the group from the GroupChildren dt datatable in
            'the dataset. We will then call RenderRow for each row.
            aRowsGroup = ds.Tables("GroupChildren").Select("GrpId = '" & dr("ReqId").ToString() & "'")
            If aRowsGroup.Length > 0 Then
                Dim aRowGroup As DataRow
                For Each aRowGroup In aRowsGroup
                    AddRowToDt(aRowGroup, ds, dt, dr("ReqId").ToString(), level + 1, gradeReps, includeHours, fac, transDB, br, transferGradedb, examsDb)
                Next
            End If
        End If

    End Sub

    Private Sub AddRowToDT_SP(ByVal dr As DataRow, ByVal ds As DataSet, ByVal dt As DataTable, _
                           ByVal parentId As String, ByVal level As Integer, ByVal gradeReps As String, ByVal includeHours As Boolean)
        Dim r As DataRow = dt.NewRow

        Dim aRows() As DataRow
        Dim aRowsGroup() As DataRow
        'Dim iCount As Integer
        'Dim dtReqsForCourseGrp As DataTable
        Dim fac As New PortalTranscriptFacade
        Dim transDB As New MyPortalTranscriptDb
        Dim br As New PortalRegisterStudentsBR
        Dim equivReqid As String
        Dim IsPass As Boolean = False
        'Course/Course Group/Program Version Description
        r("Descrip") = dr("Req")

        'Requirement Id
        r("ReqId") = dr("ReqId")

        'Course Code
        r("Code") = dr("Code")

        'Requirement Type Id
        r("ReqTypeId") = dr("ReqTypeId")

        'ParentId

        If parentId <> "" Then r("ParentId") = parentId

        'Level
        r("Level") = level

        'Credits
        r("Credits") = dr("Credits").ToString()
        r("FinAidCredits") = dr("FinAidCredits").ToString()
        'Hours
        r("Hours") = dr("Hours").ToString()

        r("PrgVerId") = dr("PrgVerId").ToString()
        r("IsPass") = False


        Dim Score As Decimal
        Dim GPA As Decimal

        'Grade, GPA, Start Date
        '       If we are dealing with a course then we will lookup the grade from the Results
        '       datatable in the dataset
        If dr("ReqTypeId") = 1 Then 'Course

            'If gradeReps.ToUpper = "LATEST" Then
            '    'Latest attempt
            '    aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "EndDate DESC, StartDate DESC")
            'Else
            '    'Best GPA
            '    aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "GPA DESC,IsPass DESC")
            'End If

            '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
            If gradeReps.ToUpper = "LATEST" Then
                'Latest attempt
                aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "EndDate DESC, StartDate DESC")
            ElseIf gradeReps.ToUpper = "BEST" Then
                'Best GPA
                aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "GPA DESC,IsPass DESC")

            Else

                aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "GPA DESC,IsPass DESC")



                'Try
                '    GPA = ds.Tables("Results").Compute("Avg(GPA)", "ReqId = '" & dr("ReqId").ToString() & "'")
                'Catch ex As Exception
                '    GPA = 0.0
                'End Try


                Dim reqId As String = dr("ReqId").ToString()
                GPA = CalculateAverage(ds, "GPA", reqId)
                Score = CalculateAverage(ds, "Score", reqId)

                'Try
                '    Score = ds.Tables("Results").Compute("Avg(Score)", "ReqId = '" & dr("ReqId").ToString() & "'")
                'Catch ex As Exception
                '    Score = 0.0
                'End Try

            End If
            ''''''''''''''''

            If aRows.Length > 0 Then 'There are results for this course
                'Check if any of the results (grades) is included in credits earned
                Dim aRow As DataRow = aRows(0)
                r("Grade") = aRow("Grade")
                If Not aRow.IsNull("Score") Then
                    'r("Score") = CType(aRow("Score"), Decimal)
                    '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
                    If gradeReps.ToUpper = "AVERAGE" Then
                        r("Score") = Score
                    Else
                        r("Score") = CType(aRow("Score"), Decimal)
                    End If
                    ''''''''''''''
                End If
                If aRow("Descrip").ToString.ToLower.Trim <> dr("Req").ToString.ToLower.Trim Then
                    r("EquivCourseDesc") = aRow("Descrip").ToString.Trim

                End If
                '
                If Not aRow.IsNull("IsInGPA") Then
                    If aRow("IsInGPA") = 1 Or aRow("IsInGPA") = True Then
                        If Not aRow.IsNull("GPA") Then
                            'r("GPA") = aRow("GPA")
                            '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
                            If gradeReps.ToUpper = "AVERAGE" Then
                                r("GPA") = GPA
                            Else
                                r("GPA") = aRow("GPA")
                            End If
                            '''''''''''
                        End If
                    End If
                End If
                '
                'Start Date
                r("StartDate") = aRow("StartDate")
                r("grdSysDetailid") = aRow("grdSysDetailId")
                ''New Code Added By Vijay Ramteke For Rally Id DE4346 On January 24, 2011 
                r("IsTransferGrade") = aRow("IsTransferGrade")
                ''New Code Added By Vijay Ramteke For Rally Id DE1393 On January 24, 2011
                '
                ''For Each aRow In aRows
                ''    r("Grade") = arow("Grade")
                ''Next
            End If
        End If


        'IsCreditsEarned, Credits Earned and Hours Completed 
        '       For a course we need to lookup that from the Results datatable in the dataset
        '       if we are dealing with a course
        If dr("ReqTypeId") = 1 Then 'Course

            aRows = ds.Tables("Results").Select("ReqId = '" & dr("ReqId").ToString() & "'", "GPA DESC")

            If aRows.Length > 0 Then 'There are results for this course
                'Check if any of the results (grades) is included in credits earned
                Dim aRow As DataRow = aRows(0)
                If aRow("Descrip").ToString.ToLower.Trim <> dr("Req").ToString.ToLower.Trim Then
                    r("EquivCourseDesc") = aRow("Descrip").ToString.Trim
                    r("Credits") = aRow("Credits").ToString()
                    r("FinAidCredits") = aRow("FinAidCredits").ToString()
                    r("Hours") = aRow("Hours").ToString()
                End If
                IsPass = br.GrdOverRide_SP(dr("PrgVerId").ToString(), dr("reqId").ToString, aRow("Grade").ToString)
                r("IsPass") = IsPass
                r("IsCreditsEarned") = aRow("IsCreditsEarned")
                r("grdSysDetailid") = aRow("grdSysDetailId")
                Try
                    If aRow("IsCreditsEarned") And IsPass Then
                        ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                        ' ''' ''Call the function to add credits earned
                        Dim isHrsComp As Boolean = transDB.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                        If isHrsComp = True Then
                            r("CreditsEarned") = r("Credits")
                            r("EarnedFinAidCredits") = r("FinAidCredits")
                        Else
                            r("CreditsEarned") = 0
                            r("EarnedFinAidCredits") = 0
                        End If

                    Else
                        r("CreditsEarned") = 0
                        r("EarnedFinAidCredits") = 0
                    End If
                Catch ex As Exception
                    r("CreditsEarned") = 0
                    r("EarnedFinAidCredits") = 0
                End Try
                '
                'Even the student fails the course he/she should still be given full credit for the hours
                'completed. However, if the student drops the course then he/she should not be given any
                'credit for the hours completed.
                Try
                    If aRow.IsNull("IsDrop") Then
                        r("HoursCompleted") = 0
                        Exit Try
                    End If
                    If aRow("IsDrop") Then
                        r("HoursCompleted") = 0
                    Else
                        'Consider variable from the web.config:
                        '   When there is a failed grade and IncludeHoursForFailingGrade=False, HoursCompleted=0. 
                        '   Any other case, account for the hours. 
                        If Not aRow("IsPass") And Not includeHours Then
                            r("HoursCompleted") = 0
                        Else
                            ''Modified by saraswathi lakshmanan on may 20 2009
                            ''To fix issue 16249
                            If aRow("IsCreditsEarned") And IsPass Then
                                ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                ' ''' ''Call the function to add credits earned
                                Dim isHrsComp As Boolean = transDB.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                                If isHrsComp = True Then
                                    r("HoursCompleted") = r("Hours")
                                Else
                                    r("HoursCompleted") = 0
                                End If
                                '   r("HoursCompleted") = r("Hours")
                            Else
                                r("HoursCompleted") = 0
                            End If
                        End If
                    End If
                Catch ex As Exception
                    r("HoursCompleted") = 0
                End Try
                '
                'To compute CreditsAttempted, ONLY take into account 'Best' or 'Latest' try accordingly for this particular course.
                Dim aRow0 As DataRow = aRows(0)
                'r("CreditsAttempted") = 0 'Commented by BN 10/05/06 to take only best or latest try into account
                If Not aRow.IsNull("Credits") Then
                    If Not aRow0.IsNull("IsCreditsAttempted") Then
                        If aRow0("IsCreditsAttempted") = True Or aRow0("IsCreditsAttempted") = 1 Then
                            r("CreditsAttempted") = aRow0("Credits")
                            r("IsCreditsAttempted") = (r("CreditsAttempted") > 0)
                            r("AllCreditsAttempted") = aRow0("Credits") * aRows.Length
                        Else
                            r("CreditsAttempted") = 0
                            r("IsCreditsAttempted") = 0 '(r("CreditsAttempted") > 0)
                            r("AllCreditsAttempted") = 0
                        End If
                    Else
                        r("CreditsAttempted") = 0 'aRow0("Credits")
                        r("IsCreditsAttempted") = 0 '(r("CreditsAttempted") > 0)
                        'If SingletonAppSettings.AppSettings("IncludeCreditsAttemptedForRepeatedCourses").ToLower = "yes" Then
                        r("AllCreditsAttempted") = 0 'aRow0("Credits") * aRows.Length

                    End If
                End If

                ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
                If Not aRow.IsNull("ScheduledHours") Then
                    If Not aRow0.IsNull("IsCreditsAttempted") Then
                        If aRow0("IsCreditsAttempted") = True Or aRow0("IsCreditsAttempted") = 1 Then
                            r("TotalScheduledHours") = aRow0("ScheduledHours")
                            r("AllScheduledHours") = aRow0("ScheduledHours") * aRows.Length
                        Else
                            r("TotalScheduledHours") = 0.0
                            r("AllScheduledHours") = 0.0
                        End If
                    Else
                        r("TotalScheduledHours") = 0.0
                        r("AllScheduledHours") = 0.0
                    End If
                End If
                '''''''''''''''''''''''''''''''''

                'Following Code Commented by BN 10/05/06 to take only best or latest try into account
                'For Each rrow As DataRow In aRows
                '    If rrow("IsCreditsAttempted") Then
                '        r("CreditsAttempted") += rrow("Credits")
                '    End If
                'Next
                'r("IsCreditsAttempted") = (r("CreditsAttempted") > 0)


                ' No results for this course.
                'check for the equivalent courses



            Else
                r("IsCreditsEarned") = False
                r("IsCreditsAttempted") = False
                r("CreditsEarned") = 0
                r("EarnedFinAidCredits") = 0
                r("HoursCompleted") = 0
                r("CreditsAttempted") = 0

                ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
                r("TotalScheduledHours") = 0.0
                r("AllScheduledHours") = 0.0
                ''''''''''''''''

            End If
        End If

        If Not r.IsNull("CreditsEarned") Then
            If r("IsPass") = False And dr("ReqTypeId") = 1 Then
                equivReqid = GetEquivalentReqId_SP(dr("ReqId").ToString)
                If equivReqid <> "" Then
                    'tmpRows = dtGraduateAudit.Select("ReqId='" & equivReqid.ToString & "'")
                    aRows = ds.Tables("Results").Select("ReqId = '" & equivReqid.ToString & "'", "GPA DESC")
                    If aRows.Length > 0 Then 'There are results for this course
                        'Check if any of the results (grades) is included in credits earned
                        Dim aRow As DataRow = aRows(0)
                        IsPass = br.GrdOverRide_SP(dr("PrgVerId").ToString(), equivReqid.ToString, aRow("Grade").ToString)
                        Dim eqDescrip As String = transDB.GetCourseDescrip_SP(equivReqid.ToString)
                        If IsPass Then

                            If r.IsNull("Grade") Then
                                r("EquivCourseDesc") = eqDescrip
                                r("Grade") = aRow("Grade")
                                r("Credits") = aRow("Credits").ToString()
                                r("FinAidCredits") = aRow("FinAidCredits").ToString()
                                r("Hours") = aRow("Hours").ToString()
                                r("grdSysDetailid") = aRow("grdSysDetailId")
                            End If
                            'Dim aRow0 As DataRow = aRows(0)
                            If Not aRow.IsNull("Credits") Then
                                If Not aRow.IsNull("IsCreditsAttempted") Then
                                    r("CreditsAttempted") = aRow("Credits")
                                    r("IsCreditsAttempted") = (r("CreditsAttempted") > 0)
                                    r("AllCreditsAttempted") = aRow("Credits") * aRows.Length
                                Else
                                    r("CreditsAttempted") = 0 'aRow0("Credits")
                                    r("IsCreditsAttempted") = 0 '(r("CreditsAttempted") > 0)
                                    r("AllCreditsAttempted") = 0 'aRow0("Credits") * aRows.Length
                                End If
                            End If

                            ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
                            If Not aRow.IsNull("ScheduledHours") Then
                                If Not aRow.IsNull("IsCreditsAttempted") Then
                                    If aRow("IsCreditsAttempted") = True Or aRow("IsCreditsAttempted") = 1 Then
                                        r("TotalScheduledHours") = aRow("ScheduledHours")
                                        r("AllScheduledHours") = aRow("ScheduledHours") * aRows.Length
                                    Else
                                        r("TotalScheduledHours") = 0.0
                                        r("AllScheduledHours") = 0.0
                                    End If
                                Else
                                    r("TotalScheduledHours") = 0.0
                                    r("AllScheduledHours") = 0.0
                                End If
                            End If
                            '''''''''''''''''''''''''''''''''

                            If Not aRow.IsNull("IsInGPA") Then
                                If aRow("IsInGPA") = 1 Or aRow("IsInGPA") = True Then
                                    If Not aRow.IsNull("GPA") Then
                                        r("GPA") = aRow("GPA")
                                    End If
                                End If
                            End If
                        End If
                        If IsPass Then
                            r("IsCreditsEarned") = aRow("IsCreditsEarned")
                            r("EquivCourseDesc") = eqDescrip
                            r("Grade") = aRow("Grade")
                            r("Credits") = aRow("Credits").ToString()
                            r("FinAidCredits") = aRow("FinAidCredits").ToString()
                            r("Hours") = aRow("Hours").ToString()
                            r("grdSysDetailid") = aRow("grdSysDetailId")
                            'r("Grade") = aRow("Grade")

                            'r("EquivCourseDesc") = transDB.GetCourseDescrip(equivReqid.ToString)
                            Try
                                If aRow("IsCreditsEarned") And IsPass Then
                                    ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                    ' ''' ''Call the function to add credits earned
                                    Dim isHrsComp As Boolean = transDB.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                                    If isHrsComp = True Then
                                        r("CreditsEarned") = r("Credits")
                                        r("EarnedFinAidCredits") = r("FinAidCredits")
                                    Else
                                        r("CreditsEarned") = 0
                                        r("EarnedFinAidCredits") = 0
                                    End If
                                Else
                                    r("CreditsEarned") = 0
                                    r("EarnedFinAidCredits") = 0
                                End If
                            Catch ex As Exception
                                r("CreditsEarned") = 0
                                r("EarnedFinAidCredits") = 0
                            End Try
                            '
                            'Even the student fails the course he/she should still be given full credit for the hours
                            'completed. However, if the student drops the course then he/she should not be given any
                            'credit for the hours completed.
                            Try
                                If aRow.IsNull("IsDrop") Then
                                    r("HoursCompleted") = 0
                                    Exit Try
                                End If
                                If aRow("IsDrop") Then
                                    r("HoursCompleted") = 0
                                Else
                                    'Consider variable from the web.config:
                                    '   When there is a failed grade and IncludeHoursForFailingGrade=False, HoursCompleted=0. 
                                    '   Any other case, account for the hours. 
                                    If Not aRow("IsPass") And Not includeHours Then
                                        r("HoursCompleted") = 0
                                    Else
                                        ''Modified by saraswathi on May 20 2009
                                        ''To fix issue 16249

                                        If aRow("IsCreditsEarned") And IsPass Then
                                            ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                            ' ''' ''Call the function to add credits earned
                                            Dim isHrsComp As Boolean = transDB.IsCourseCombinationandPass(dr("StuEnrollId").ToString(), aRow("TestId").ToString())
                                            If isHrsComp = True Then
                                                r("HoursCompleted") = r("Hours")
                                            Else
                                                r("HoursCompleted") = 0
                                            End If
                                            'r("HoursCompleted") = r("Hours")
                                        Else
                                            r("HoursCompleted") = 0
                                        End If
                                    End If
                                End If
                            Catch ex As Exception
                                r("HoursCompleted") = 0
                            End Try
                            '
                            'To compute CreditsAttempted, ONLY take into account 'Best' or 'Latest' try accordingly for this particular course.

                            'r("CreditsAttempted") = 0 'Commented by BN 10/05/06 to take only best or latest try into account

                        End If
                    End If
                End If

            End If
            'Credits Remaining. We can simply use the difference between the credits column 
            '       and the credits earned column.
            If r.IsNull("CreditsEarned") Then
                r("CreditsRemaining") = dr("Credits")
            Else
                r("CreditsRemaining") = r("Credits") - r("CreditsEarned")
            End If


            'Hours Remaining. We can simply use the difference between the hours column 
            '       and the hours completed column.
            If r.IsNull("HoursCompleted") Then
                r("HoursRemaining") = r("Hours")
            Else
                r("HoursRemaining") = r("Hours") - r("HoursCompleted")
            End If
            Dim boolIsCourseALab As Boolean = False
            If aRows.Length > 0 Then
                'boolIsCourseALab = isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                r("IsCourseALab") = aRows(0)("IsCourseALab")
                r("decCredits") = aRows(0)("decCredits")
                boolIsCourseALab = aRows(0)("IsCourseALab")
                If boolIsCourseALab Then
                    Dim boolRemaining As Boolean = (New PortalExamsFacade).IsRemainingLabCount_SP(dr("StuEnrollId").ToString(), 500, dr("ReqId").ToString)
                    If boolRemaining = False Then
                        Dim boolClinicSatisfied As Boolean = (New PortalExamsFacade).isClinicCourseCompletlySatisfied_SP(dr("StuEnrollId").ToString(), 500, dr("ReqId").ToString)
                        If boolClinicSatisfied = True Then
                            If isCourseALabWorkOrLabHourCourseCombination_SP(dr("ReqId").ToString) Then
                                If IsPass Then
                                    r("HoursCompleted") = r("Hours")
                                    r("HoursRemaining") = 0
                                Else
                                    r("HoursCompleted") = 0
                                    r("HoursRemaining") = r("Hours")
                                End If
                            Else
                                r("HoursCompleted") = r("Hours")
                                r("HoursRemaining") = 0
                            End If
                        End If
                    End If
                End If
            End If

        End If

        '   Add row into datatable
        dt.Rows.Add(r)


        'If we are dealing with a course group then we want to render its children as well.
        'A course group can contain other course groups so we will have to make a recursive
        'call to ensure that all children of the course group is rendered.
        If dr("ReqTypeId") = 2 Then
            'Get the direct children for the group from the GroupChildren dt datatable in
            'the dataset. We will then call RenderRow for each row.
            aRowsGroup = ds.Tables("GroupChildren").Select("GrpId = '" & dr("ReqId").ToString() & "'")
            If aRowsGroup.Length > 0 Then
                Dim aRowGroup As DataRow
                For Each aRowGroup In aRowsGroup
                    AddRowToDT_SP(aRowGroup, ds, dt, dr("ReqId").ToString(), level + 1, gradeReps, includeHours)
                Next
            End If
        End If

    End Sub



    Public Function BuildGraduateAuditDT(ByVal StuEnrollId As String, ByVal gradeReps As String, ByVal includeHours As Boolean, Optional ByVal termEndDate As DateTime = #1/1/1001#, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal campusID As String = "") As DataTable   'DataSet 
        ' New Optional Parameter add termCond and classCond by Vijay Ramteke on May, 06 2009
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim ds As DataSet

        Dim myPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        If myPortalAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric" Then
            ds = dbTranscript.GetGraduateAuditByEnrollmentForNumericForCourseEquivalent(StuEnrollId, termEndDate, termCond, classCond, campusID)
        ElseIf dbTranscript.IsContinuingEdPrgVersion(StuEnrollId) Then
            ds = dbTranscript.GetGraduateAuditByCEProgEnrollmentForCourseEquivalent(StuEnrollId, termEndDate, termCond, classCond, campusID)
        Else
            ds = dbTranscript.GetGraduateAuditByEnrollmentForCourseEquivalent(StuEnrollId, termEndDate, termCond, classCond, campusID)
        End If
        Return BuildGraduateAuditDT(ds, gradeReps, includeHours, campusID)
    End Function

    'Private Function BuildGraduateAuditDT(ByVal ds As DataSet, ByVal gradeReps As String, ByVal includeHours As String) As DataTable
    '    Dim br As New PortalRegisterStudentsBR
    '    Dim dt As New DataTable("GraduateAudit")
    '    Dim dr As DataRow

    '    dt.Columns.Add("Descrip", Type.GetType("System.String"))
    '    dt.Columns.Add("Code", Type.GetType("System.String"))
    '    dt.Columns.Add("ReqId", Type.GetType("System.String"))
    '    dt.Columns.Add("StartDate", Type.GetType("System.DateTime"))
    '    dt.Columns.Add("ReqTypeId", Type.GetType("System.String"))
    '    dt.Columns.Add("ParentId", Type.GetType("System.String"))
    '    dt.Columns.Add("Level", Type.GetType("System.Int32"))
    '    dt.Columns.Add("IsCreditsEarned", Type.GetType("System.Boolean"))
    '    dt.Columns.Add("Credits", Type.GetType("System.Decimal"))
    '    dt.Columns.Add("Hours", Type.GetType("System.Decimal"))
    '    dt.Columns.Add("Grade", Type.GetType("System.String"))
    '    dt.Columns.Add("Score", Type.GetType("System.Decimal"))
    '    dt.Columns.Add("GPA", Type.GetType("System.Decimal"))
    '    dt.Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
    '    dt.Columns.Add("CreditsRemaining", Type.GetType("System.Decimal"))
    '    dt.Columns.Add("HoursCompleted", Type.GetType("System.Decimal"))
    '    dt.Columns.Add("HoursRemaining", Type.GetType("System.Decimal"))
    '    dt.Columns.Add("IsCreditsAttempted", Type.GetType("System.Boolean"))
    '    dt.Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
    '    dt.Columns.Add("AllCreditsAttempted", Type.GetType("System.Decimal"))
    '    dt.Columns.Add("IsContinuingEd", Type.GetType("System.Boolean"))
    '    dt.Columns.Add("IsPass", Type.GetType("System.Boolean"))
    '    dt.Columns.Add("PrgVerId", Type.GetType("System.String"))
    '    dt.Columns.Add("IsInGPA", Type.GetType("System.Boolean"))
    '    dt.Columns.Add("EarnedFinAidCredits", Type.GetType("System.Decimal"))
    '    dt.Columns.Add("FinAidCredits", Type.GetType("System.Decimal"))
    '    dt.Columns.Add("EquivCourseDesc", Type.GetType("System.String"))
    '    dt.Columns.Add("StuEnrollId", Type.GetType("System.String"))



    '    Dim finAid As Decimal = 0.0

    '    If ds.Tables("DirectChildren").Rows.Count > 0 Then
    '        dr = dt.NewRow

    '        'First row is for the program version
    '        dt.Rows.Add(dr)


    '        'Loop thru the direct children table
    '        For Each dr In ds.Tables("DirectChildren").Rows
    '            AddRowToDT(dr, ds, dt, "", 0, gradeReps, includeHours)
    '        Next



    '        'The credits required for the program version can be retrieved from the DirectChildren dt in the DataSet
    '        dt.Rows(0)("Descrip") = ds.Tables("DirectChildren").Rows(0)("PrgVerDescrip").ToString()
    '        dt.Rows(0)("Credits") = ds.Tables("DirectChildren").Rows(0)("ProgCredits").ToString()
    '        dt.Rows(0)("Hours") = ds.Tables("DirectChildren").Rows(0)("ProgHours").ToString()
    '        dt.Rows(0)("IsContinuingEd") = ds.Tables("DirectChildren").Rows(0)("IsContinuingEd")
    '        dt.Rows(0)("PrgVerId") = ds.Tables("DirectChildren").Rows(0)("PrgVerId")
    '        dt.Rows(0)("IsPass") = True
    '        dt.Rows(0)("IsInGPA") = False
    '        dt.Rows(0)("StuEnrollId") = ds.Tables("DirectChildren").Rows(0)("StuEnrollId")

    '        'For Each dr In ds.Tables("Results").Rows
    '        '    finAid = finAid + CDec(dr("FinAidCredits"))
    '        'Next
    '        'dt.Rows(0)("FinAidCredits") = finAid
    '        For Each dr In ds.Tables("Results").Rows
    '            If Not (br.GrdOverRide(dt.Rows(0)("PrgVerId"), dr("reqId").ToString, dr("grade").ToString)) Then
    '                Dim dRows() As DataRow = ds.Tables("Results").Select("reqid='" & dr("ReqId").ToString & "'", "startdate desc")
    '                If dRows.Length > 1 Then
    '                    If Not (br.GrdOverRide(dt.Rows(0)("PrgVerId"), dRows(0)("reqId").ToString, dRows(0)("grade").ToString)) Then
    '                        dt.Rows(0)("IsPass") = False
    '                        Exit For
    '                    End If
    '                Else
    '                    dt.Rows(0)("IsPass") = False
    '                    Exit For
    '                End If
    '            End If
    '        Next
    '        For Each dr In ds.Tables("Results").Rows
    '            Try
    '                If dr("IsInGPA") = True Then
    '                    dt.Rows(0)("IsInGPA") = True
    '                    Exit For
    '                End If
    '            Catch ex As System.Exception
    '                dt.Rows(0)("IsInGPA") = False
    '            End Try
    '        Next

    '        'Nested Course Groups
    '        'Credits Attempted, Hours Completed, Credits Remaining and Hours Remaining
    '        Dim maxLevel As Integer = dt.Compute("MAX(Level)", "")
    '        Dim obj, obj1 As Object
    '        Dim objFin As Object
    '        Dim arrRows() As DataRow
    '        'Find out if there are Course Groups in the program version definition
    '        Dim arrReqGrps() As DataRow = dt.Select("ReqTypeId=2")
    '        If arrReqGrps.GetLength(0) > 0 Then
    '            If maxLevel = 0 Then
    '                'This section will catch course groups without definitions. In other words, course groups without courses.
    '                For Each dr In arrReqGrps
    '                    dr("CreditsEarned") = 0
    '                    dr("EarnedFinAidCredits") = 0
    '                    dr("CreditsAttempted") = 0
    '                    dr("AllCreditsAttempted") = 0
    '                    dr("HoursCompleted") = 0
    '                    dr("CreditsRemaining") = dr("Credits") - dr("CreditsEarned")
    '                    dr("HoursRemaining") = dr("Hours") - dr("HoursCompleted")
    '                Next
    '            Else
    '                Do While maxLevel > 0
    '                    For Each dr In arrReqGrps
    '                        Dim strGrp As String
    '                        strGrp = dr("Descrip")
    '                        'Find out if there are children of level = maxLevel for current Course Group
    '                        arrRows = dt.Select("ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel)
    '                        If arrRows.GetLength(0) > 0 Then
    '                            obj = dt.Compute("SUM(CreditsEarned)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
    '                            objFin = dt.Compute("SUM(EarnedFinAidCredits)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")

    '                            If Not IsDBNull(obj) Then
    '                                If Convert.ToDecimal(obj) < dr("Credits") Then
    '                                    If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
    '                                        Dim boolIsCourseALab As Boolean = False
    '                                        boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
    '                                        If boolIsCourseALab = True Then
    '                                            Dim decCredits As Decimal = 0.0
    '                                            Try
    '                                                decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
    '                                            Catch ex As System.Exception
    '                                                decCredits = 0
    '                                            End Try
    '                                            dr("CreditsEarned") = decCredits
    '                                        End If
    '                                    Else
    '                                        dr("CreditsEarned") = Convert.ToDecimal(obj)
    '                                    End If
    '                                    dr("EarnedFinAidCredits") = Convert.ToDecimal(objFin)
    '                                Else
    '                                    If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
    '                                        Dim boolIsCourseALab As Boolean = False
    '                                        boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
    '                                        If boolIsCourseALab = True Then
    '                                            Dim decCredits As Decimal = 0.0
    '                                            Try
    '                                                decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
    '                                            Catch ex As System.Exception
    '                                                decCredits = 0
    '                                            End Try
    '                                            dr("CreditsEarned") = decCredits
    '                                        End If
    '                                    Else
    '                                        dr("CreditsEarned") = dr("Credits")
    '                                    End If
    '                                    dr("EarnedFinAidCredits") = dr("FinAidCredits")
    '                                End If
    '                            Else
    '                                If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
    '                                    Dim boolIsCourseALab As Boolean = False
    '                                    boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
    '                                    If boolIsCourseALab = True Then
    '                                        Dim decCredits As Decimal = 0.0
    '                                        Try
    '                                            decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
    '                                        Catch ex As System.Exception
    '                                            decCredits = 0
    '                                        End Try
    '                                        dr("CreditsEarned") = decCredits
    '                                    End If
    '                                Else
    '                                    dr("CreditsEarned") = 0
    '                                End If
    '                                dr("EarnedFinAidCredits") = 0
    '                            End If
    '                            '
    '                            '
    '                            obj = dt.Compute("SUM(CreditsAttempted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
    '                            obj1 = dt.Compute("SUM(AllCreditsAttempted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
    '                            If Not IsDBNull(obj) Then
    '                                If Convert.ToDecimal(obj) < dr("Credits") Then
    '                                    If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
    '                                        Dim boolIsCourseALab As Boolean = False
    '                                        boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
    '                                        If boolIsCourseALab = True Then
    '                                            Dim decCredits As Decimal = 0.0
    '                                            Try
    '                                                decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId"), SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
    '                                            Catch ex As System.Exception
    '                                                decCredits = 0
    '                                            End Try
    '                                            dr("CreditsAttempted") = decCredits
    '                                        End If
    '                                    Else
    '                                        dr("CreditsAttempted") = Convert.ToDecimal(obj)
    '                                    End If
    '                                    dr("AllCreditsAttempted") = Convert.ToDecimal(obj1)
    '                                Else
    '                                    If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
    '                                        Dim boolIsCourseALab As Boolean = False
    '                                        boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
    '                                        If boolIsCourseALab = True Then
    '                                            Dim decCredits As Decimal = 0.0
    '                                            Try
    '                                                decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId"), SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
    '                                            Catch ex As System.Exception
    '                                                decCredits = 0
    '                                            End Try
    '                                            dr("CreditsAttempted") = decCredits
    '                                        End If
    '                                    Else
    '                                        dr("CreditsAttempted") = dr("Credits")
    '                                    End If
    '                                    dr("AllCreditsAttempted") = dr("Credits")
    '                                End If
    '                            Else
    '                                If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
    '                                    Dim boolIsCourseALab As Boolean = False
    '                                    boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
    '                                    If boolIsCourseALab = True Then
    '                                        Dim decCredits As Decimal = 0.0
    '                                        Try
    '                                            decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId"), SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
    '                                        Catch ex As System.Exception
    '                                            decCredits = 0
    '                                        End Try
    '                                        dr("CreditsAttempted") = decCredits
    '                                    End If
    '                                Else
    '                                    dr("CreditsAttempted") = 0
    '                                End If
    '                                dr("AllCreditsAttempted") = 0
    '                            End If
    '                            '
    '                            '
    '                            obj = dt.Compute("SUM(HoursCompleted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel)
    '                            If Not IsDBNull(obj) Then
    '                                If Convert.ToDecimal(obj) < dr("Hours") Then
    '                                    dr("HoursCompleted") = Convert.ToDecimal(obj)
    '                                Else
    '                                    dr("HoursCompleted") = dr("Hours")
    '                                End If
    '                            Else
    '                                dr("HoursCompleted") = 0
    '                            End If
    '                            '
    '                            '
    '                            dr("CreditsRemaining") = dr("Credits") - dr("CreditsEarned")
    '                            dr("HoursRemaining") = dr("Hours") - dr("HoursCompleted")
    '                        End If
    '                    Next
    '                    maxLevel -= 1
    '                Loop
    '            End If
    '        End If




    '        'Total credits earned for the program version (Level = 0).
    '        'If there are no results we will set credits earned and hours completed to 0.
    '        arrRows = dt.Select("Level=0")
    '        If arrRows.GetLength(0) > 0 Then
    '            obj = dt.Compute("SUM(CreditsEarned)", "Level=0 AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
    '            objFin = dt.Compute("SUM(EarnedFinAidCredits)", "Level=0 AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
    '            If Not IsDBNull(obj) Then
    '                If Convert.ToDecimal(obj) < dt.Rows(0)("Credits") Then
    '                    If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
    '                        Dim boolIsCourseALab As Boolean = False
    '                        boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
    '                        If boolIsCourseALab = True Then
    '                            Dim decCredits As Decimal = 0.0
    '                            Try
    '                                decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
    '                            Catch ex As System.Exception
    '                                decCredits = 0
    '                            End Try
    '                            dt.Rows(0)("CreditsEarned") = decCredits
    '                        Else
    '                            dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
    '                        End If
    '                    Else
    '                        If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
    '                            Dim boolIsCourseALab As Boolean = False
    '                            boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
    '                            If boolIsCourseALab = True Then
    '                                Dim decCredits As Decimal = 0.0
    '                                Try
    '                                    decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
    '                                Catch ex As System.Exception
    '                                    decCredits = 0
    '                                End Try
    '                                dt.Rows(0)("CreditsEarned") = decCredits
    '                            Else
    '                                dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
    '                            End If
    '                        Else
    '                            dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
    '                        End If
    '                    End If
    '                    dt.Rows(0)("EarnedFinAidCredits") = Convert.ToDecimal(objFin)
    '                Else
    '                    If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
    '                        Dim boolIsCourseALab As Boolean = False
    '                        boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
    '                        If boolIsCourseALab = True Then
    '                            Dim decCredits As Decimal = 0.0
    '                            Try
    '                                decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
    '                            Catch ex As System.Exception
    '                                decCredits = 0
    '                            End Try
    '                            dt.Rows(0)("CreditsEarned") = decCredits
    '                        Else
    '                            dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
    '                        End If
    '                    Else
    '                        dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
    '                    End If
    '                    Try
    '                        dt.Rows(0)("EarnedFinAidCredits") = Convert.ToDecimal(objFin)
    '                    Catch ex As System.Exception
    '                        dt.Rows(0)("EarnedFinAidCredits") = 0
    '                    End Try
    '                End If
    '            Else
    '                If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
    '                    Dim boolIsCourseALab As Boolean = False
    '                    boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
    '                    If boolIsCourseALab = True Then
    '                        Dim decCredits As Decimal = 0.0
    '                        Try
    '                            decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
    '                        Catch ex As System.Exception
    '                            decCredits = 0
    '                        End Try
    '                        dt.Rows(0)("CreditsEarned") = decCredits
    '                        dt.Rows(0)("EarnedFinAidCredits") = 0
    '                    Else
    '                        dt.Rows(0)("CreditsEarned") = 0
    '                        dt.Rows(0)("EarnedFinAidCredits") = 0
    '                    End If
    '                Else
    '                    dt.Rows(0)("CreditsEarned") = 0
    '                    dt.Rows(0)("EarnedFinAidCredits") = 0
    '                End If
    '            End If
    '            '
    '            '
    '            obj = dt.Compute("SUM(HoursCompleted)", "Level=0")
    '            If Not IsDBNull(obj) Then
    '                'If Convert.ToDecimal(obj) < dt.Rows(0)("Hours") Then
    '                dt.Rows(0)("HoursCompleted") = Convert.ToDecimal(obj)
    '                'Else
    '                '    dt.Rows(0)("HoursCompleted") = dt.Rows(0)("Hours")
    '                'End If
    '            Else
    '                dt.Rows(0)("HoursCompleted") = 0
    '            End If
    '            '
    '            '
    '            obj = dt.Compute("SUM(CreditsAttempted)", "Level=0 AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
    '            obj1 = dt.Compute("SUM(AllCreditsAttempted)", "Level=0 AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
    '            If Not IsDBNull(obj) Then
    '                'If Convert.ToDecimal(obj) < dt.Rows(0)("Credits") Then
    '                If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
    '                    Dim boolIsCourseALab As Boolean = False
    '                    boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
    '                    If boolIsCourseALab = True Then
    '                        Dim decCredits As Decimal = 0.0
    '                        Try
    '                            decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
    '                        Catch ex As System.Exception
    '                            decCredits = 0
    '                        End Try
    '                        dt.Rows(0)("CreditsAttempted") = decCredits
    '                    End If
    '                Else
    '                    dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
    '                End If
    '                dt.Rows(0)("AllCreditsAttempted") = Convert.ToDecimal(obj1)
    '            Else
    '                If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
    '                    Dim boolIsCourseALab As Boolean = False
    '                    boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
    '                    If boolIsCourseALab = True Then
    '                        Dim decCredits As Decimal = 0.0
    '                        Try
    '                            decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
    '                        Catch ex As System.Exception
    '                            decCredits = 0
    '                        End Try
    '                        dt.Rows(0)("CreditsAttempted") = decCredits
    '                    Else
    '                        dt.Rows(0)("CreditsAttempted") = 0
    '                    End If
    '                Else
    '                    dt.Rows(0)("CreditsAttempted") = 0
    '                End If
    '                dt.Rows(0)("AllCreditsAttempted") = 0
    '            End If
    '            '
    '            '
    '            'Remaining Credits for the program version
    '            If dt.Rows(0)("Credits") > dt.Rows(0)("CreditsEarned") Then
    '                dt.Rows(0)("CreditsRemaining") = dt.Rows(0)("Credits") - dt.Rows(0)("CreditsEarned")
    '            Else
    '                dt.Rows(0)("CreditsRemaining") = 0
    '            End If
    '            '
    '            '
    '            'Remaining hours for the program version
    '            If dt.Rows(0)("Hours") > dt.Rows(0)("HoursCompleted") Then
    '                dt.Rows(0)("HoursRemaining") = dt.Rows(0)("Hours") - dt.Rows(0)("HoursCompleted")
    '            Else
    '                dt.Rows(0)("HoursRemaining") = 0
    '            End If
    '            '
    '            '
    '            'This code is to compute a simple average GPA.
    '            'SimpleGPA = SUM (Course GPA) / Number of Courses
    '            'obj = dt.Compute("COUNT(GPA)", "GPA NOT IS NULL")
    '            'Dim obj2 As Object
    '            'obj2 = dt.Compute("SUM(GPA)", "GPA NOT IS NULL")
    '            'If Not IsDBNull(obj) And Not IsDBNull(obj2) Then
    '            '    If Convert.ToInt32(obj) > 0 Then
    '            '        Return System.Math.Round((Convert.ToDecimal(obj2) / Convert.ToInt32(obj)), 2)
    '            '    End If
    '            'Else
    '            '    Return 0
    '            'End If

    '            dt.Rows(0)("GPA") = ComputeGPA(dt.Select("GPA NOT IS NULL"))

    '        End If
    '    End If

    '    Return dt
    'End Function

    Private Function BuildGraduateAuditDT(ByVal ds As DataSet, ByVal gradeReps As String, ByVal includeHours As String, Optional ByVal campusid As String = "") As DataTable

        Dim fac As PortalTranscriptFacade = New PortalTranscriptFacade
        Dim transDB As MyPortalTranscriptDb = New MyPortalTranscriptDb
        Dim br As PortalRegisterStudentsBR = New PortalRegisterStudentsBR
        'Dim examFacade As PortalExamsFacade = New PortalExamsFacade()
        Dim transferGradedb As PortalTransferGradeDB = New PortalTransferGradeDB()
        Dim examsDb As MyPortalExamsDb = New MyPortalExamsDb()

        Dim dt As New DataTable("GraduateAudit")
        Dim dr As DataRow

        Dim myPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        Debug.WriteLine("1- Entering in BuildGraduateAuditDT {0}", DateTime.Now)

        dt.Columns.Add("Descrip", Type.GetType("System.String"))
        dt.Columns.Add("Code", Type.GetType("System.String"))
        dt.Columns.Add("ReqId", Type.GetType("System.String"))
        dt.Columns.Add("StartDate", Type.GetType("System.DateTime"))
        dt.Columns.Add("ReqTypeId", Type.GetType("System.String"))
        dt.Columns.Add("ParentId", Type.GetType("System.String"))
        dt.Columns.Add("Level", Type.GetType("System.Int32"))
        dt.Columns.Add("IsCreditsEarned", Type.GetType("System.Boolean"))
        dt.Columns.Add("Credits", Type.GetType("System.Decimal"))
        dt.Columns.Add("Hours", Type.GetType("System.Decimal"))
        dt.Columns.Add("Grade", Type.GetType("System.String"))
        dt.Columns.Add("Score", Type.GetType("System.Decimal"))
        dt.Columns.Add("GPA", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditsRemaining", Type.GetType("System.Decimal"))
        dt.Columns.Add("HoursCompleted", Type.GetType("System.Decimal"))
        dt.Columns.Add("HoursRemaining", Type.GetType("System.Decimal"))
        dt.Columns.Add("IsCreditsAttempted", Type.GetType("System.Boolean"))
        dt.Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
        dt.Columns.Add("AllCreditsAttempted", Type.GetType("System.Decimal"))
        dt.Columns.Add("IsContinuingEd", Type.GetType("System.Boolean"))
        dt.Columns.Add("IsPass", Type.GetType("System.Boolean"))
        dt.Columns.Add("PrgVerId", Type.GetType("System.String"))
        dt.Columns.Add("IsInGPA", Type.GetType("System.Boolean"))
        dt.Columns.Add("EarnedFinAidCredits", Type.GetType("System.Decimal"))
        dt.Columns.Add("FinAidCredits", Type.GetType("System.Decimal"))
        dt.Columns.Add("EquivCourseDesc", Type.GetType("System.String"))
        dt.Columns.Add("StuEnrollId", Type.GetType("System.String"))
        dt.Columns.Add("GrdSysDetailId", Type.GetType("System.String"))
        dt.Columns.Add("ScheduledHours", Type.GetType("System.Decimal"))
        dt.Columns.Add("IsTransferGrade", Type.GetType("System.Boolean"))
        dt.Columns.Add("TotalScheduledHours", Type.GetType("System.Decimal"))
        dt.Columns.Add("AllScheduledHours", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditZeroWeight", Type.GetType("System.Decimal"))
        ''''''''''''''''''''''''''''
        Dim checkCredits As String = myPortalAdvAppSettings.AppSettings("CheckCreditsForContiningEd").ToLower()
        If ds.Tables("DirectChildren").Rows.Count > 0 Then
            dr = dt.NewRow

            'First row is for the program version
            dt.Rows.Add(dr)
            Debug.WriteLine("1.1-  BuildGraduateAuditDT Entering AddRowToDt {0}", DateTime.Now)

            'Loop thru the direct children table
            For Each dr In ds.Tables("DirectChildren").Rows
                AddRowToDt(dr, ds, dt, "", 0, gradeReps, includeHours, fac, transDB, br, transferGradedb, examsDb)
            Next
            Debug.WriteLine("1.2-  BuildGraduateAuditDT Leaving AddRowToDt {0}", DateTime.Now)

            'The credits required for the program version can be retrieved from the DirectChildren dt in the DataSet
            dt.Rows(0)("Descrip") = ds.Tables("DirectChildren").Rows(0)("PrgVerDescrip").ToString()
            dt.Rows(0)("Credits") = ds.Tables("DirectChildren").Rows(0)("ProgCredits").ToString()
            dt.Rows(0)("Hours") = ds.Tables("DirectChildren").Rows(0)("ProgHours").ToString()
            dt.Rows(0)("IsContinuingEd") = ds.Tables("DirectChildren").Rows(0)("IsContinuingEd")
            dt.Rows(0)("PrgVerId") = ds.Tables("DirectChildren").Rows(0)("PrgVerId")
            dt.Rows(0)("IsPass") = True
            dt.Rows(0)("IsInGPA") = False
            dt.Rows(0)("StuEnrollId") = ds.Tables("DirectChildren").Rows(0)("StuEnrollId")
            dt.Rows(0)("CreditZeroWeight") = 0.0

            Debug.WriteLine("2- BuildGraduateAuditDT After Create Table records and before loop 1633{0}", DateTime.Now)

            For Each dr In ds.Tables("Results").Rows
                If Not (br.GrdOverRide(dt.Rows(0)("PrgVerId"), dr("reqId").ToString, dr("grade").ToString)) Then
                    Dim dRows() As DataRow = ds.Tables("Results").Select("reqid='" & dr("ReqId").ToString & "'", "startdate desc")
                    If dRows.Length > 1 Then
                        If Not (br.GrdOverRide(dt.Rows(0)("PrgVerId"), dRows(0)("reqId").ToString, dRows(0)("grade").ToString)) Then
                            dt.Rows(0)("IsPass") = False
                            Exit For
                        End If
                    Else
                        dt.Rows(0)("IsPass") = False
                        Exit For
                    End If
                End If
            Next

            Debug.WriteLine("3- BuildGraduateAuditDT After loop 1635 before loop 1652 {0}", DateTime.Now)

            For Each dr In ds.Tables("Results").Rows
                Try
                    If dr("IsInGPA") = True Then
                        dt.Rows(0)("IsInGPA") = True
                        Exit For
                    End If
                Catch ex As Exception
                    dt.Rows(0)("IsInGPA") = False
                End Try
            Next

            Debug.WriteLine("4- BuildGraduateAuditDT After loop 1652 before loop 1664 {0}", DateTime.Now)

            For Each dr In ds.Tables("Results").Rows
                Try
                    If dr.IsNull("GrdSysDetailId") Then
                        If (New StuTranscriptDB).GetWeightSum(dr("Reqid").ToString) = 0 Then
                            Dim dtWorkUnit As DataTable
                            dtWorkUnit = (New StuTranscriptDB).GetWorkUnitResults(dt.Rows(0)("StuEnrollId").ToString, dr("ReqId").ToString)
                            If myPortalAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "true" Then
                                If myPortalAdvAppSettings.AppSettings("GradesFormat", campusid).ToLower = "numeric" Then
                                    'rowC("CreditsAttempted") = 0
                                    Dim arrWU() As DataRow
                                    dtWorkUnit.Select("Score >= 0")
                                    If dtWorkUnit.Rows.Count > 0 Then
                                        If GetCreditsEarned(dtWorkUnit, dr("Score")) Then
                                            If Not dr.IsNull("IsCreditsEarned") Then
                                                If dr("IsCreditsEarned").ToString = "True" Or dr("IsCreditsEarned").ToString = "1" Then
                                                    dt.Rows(0)("CreditZeroWeight") += dr("Credits")
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                Catch ex As Exception
                    dt.Rows(0)("CreditZeroWeight") = 0.0
                End Try
            Next

            Debug.WriteLine("5- BuildGraduateAuditDT After loop 1664 before Nested Course Groups 1698 {0}", DateTime.Now)

            'Nested Course Groups
            'Credits Attempted, Hours Completed, Credits Remaining and Hours Remaining
            Dim maxLevel As Integer = dt.Compute("MAX(Level)", "")
            Dim obj, obj1 As Object
            Dim objSchHrs, objSchHrs1 As Object
            Dim objFin As Object
            Dim objSchHours As Object
            Dim arrRows() As DataRow
            'Find out if there are Course Groups in the program version definition
            Dim arrReqGrps() As DataRow = dt.Select("ReqTypeId=2")
            If arrReqGrps.GetLength(0) > 0 Then
                If maxLevel = 0 Then
                    'This section will catch course groups without definitions. In other words, course groups without courses.
                    For Each dr In arrReqGrps
                        dr("CreditsEarned") = 0
                        dr("EarnedFinAidCredits") = 0
                        dr("CreditsAttempted") = 0
                        dr("AllCreditsAttempted") = 0
                        dr("HoursCompleted") = 0
                        dr("CreditsRemaining") = dr("Credits") - dr("CreditsEarned")
                        dr("HoursRemaining") = dr("Hours") - dr("HoursCompleted")
                    Next
                Else

                    Debug.WriteLine("Entered While LOOP maxLevel > 0 in BuildGraduateAuditDT Line 1715 {0}", DateTime.Now)

                    Do While maxLevel > 0
                        For Each dr In arrReqGrps
                            'Find out if there are children of level = maxLevel for current Course Group
                            arrRows = dt.Select("ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel)
                            If arrRows.GetLength(0) > 0 Then
                                obj = dt.Compute("SUM(CreditsEarned)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                                objFin = dt.Compute("SUM(EarnedFinAidCredits)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                                objSchHours = dt.Compute("SUM(ScheduledHours)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                                If Not IsDBNull(obj) Then
                                    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                    If Convert.ToDecimal(obj) < dr("Credits") Or checkCredits = "no" Then
                                        dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                        dr("CreditsEarned") = Convert.ToDecimal(obj)
                                        Dim servicePortalTransferGrade As PortalTransferGradeDB = New PortalTransferGradeDB()
                                        Dim servicePortalTranscript As MyPortalTranscriptDb = New MyPortalTranscriptDb()
                                        Dim serviceTrascriptDb As StuTranscriptDB = New StuTranscriptDB()

                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean
                                            boolIsCourseALab = servicePortalTransferGrade.IsCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    decCredits = servicePortalTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr1("ReqId").ToString)
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsEarned") += decCredits
                                                End If
                                            End If
                                            If myPortalAdvAppSettings.AppSettings("GradesFormat", campusid).ToLower = "numeric" And dr1.IsNull("GrdSysDetailId") Then
                                                Dim dtWorkUnit As DataTable
                                                dtWorkUnit = serviceTrascriptDb.GetWorkUnitResults_SP(dt.Rows(0)("StuEnrollId").ToString, dr1("ReqId").ToString)
                                                If dtWorkUnit.Rows.Count > 0 Then
                                                    If GetCreditsEarned(dtWorkUnit, dr1("Score")) Then
                                                        If Not dr1.IsNull("IsCreditsEarned") Then
                                                            If dr1("IsCreditsEarned").ToString = "True" Or dr1("IsCreditsEarned").ToString = "1" Then
                                                                dt.Rows(0)("CreditsEarned") += dr1("Credits")
                                                            End If
                                                        End If
                                                    End If
                                                End If

                                            End If
                                        Next

                                        dr("EarnedFinAidCredits") = Convert.ToDecimal(objFin)
                                        'jguirado. Avoiding the exception....                                        
                                        If IsDBNull(dr("ScheduledHours")) Then
                                            dr("ScheduledHours") = Convert.ToDecimal("0.0")
                                        Else
                                            dr("ScheduledHours") = Convert.ToDecimal(objSchHours)
                                        End If
                                    Else
                                        If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                            dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                            dr("CreditsEarned") = Convert.ToDecimal(obj)
                                        Else
                                            dt.Rows(0)("CreditsEarned") = 0
                                        End If

                                        Dim servicePortalTransferGrade As PortalTransferGradeDB = New PortalTransferGradeDB()
                                        Dim servicePortalTranscript As MyPortalTranscriptDb = New MyPortalTranscriptDb()

                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean
                                            boolIsCourseALab = servicePortalTransferGrade.IsCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    decCredits = servicePortalTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr1("ReqId").ToString)
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsEarned") += decCredits
                                                End If
                                            End If
                                        Next
                                        dr("EarnedFinAidCredits") = dr("FinAidCredits")
                                        dr("ScheduledHours") = dr("ScheduledHours")
                                    End If
                                Else
                                    dt.Rows(0)("CreditsEarned") = 0
                                    Dim servicePortalTransferGrade As PortalTransferGradeDB = New PortalTransferGradeDB()
                                    Dim servicePortalTranscript As MyPortalTranscriptDb = New MyPortalTranscriptDb()

                                    For Each dr1 As DataRow In ds.Tables("Results").Rows
                                        Dim boolIsCourseALab As Boolean
                                        boolIsCourseALab = servicePortalTransferGrade.IsCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal
                                            Try
                                                decCredits = servicePortalTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr1("ReqId").ToString)
                                            Catch ex As Exception
                                                decCredits = 0
                                            End Try
                                            If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                                dt.Rows(0)("CreditsEarned") += decCredits
                                            End If
                                        End If
                                    Next
                                    dr("EarnedFinAidCredits") = 0
                                    dr("ScheduledHours") = dr("ScheduledHours")
                                End If
                                '
                                '

                                ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
                                objSchHrs = dt.Compute("SUM(TotalScheduledHours)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
                                objSchHrs1 = dt.Compute("SUM(AllScheduledHours)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")

                                If Not IsDBNull(objSchHrs) Then
                                    dt.Rows(0)("TotalScheduledHours") = Convert.ToDecimal(objSchHrs)
                                Else
                                    dt.Rows(0)("TotalScheduledHours") = 0.0
                                End If
                                If Not IsDBNull(objSchHrs1) Then
                                    dt.Rows(0)("AllScheduledHours") = Convert.ToDecimal(objSchHrs1)
                                Else
                                    dt.Rows(0)("AllScheduledHours") = 0.0
                                End If

                                '''''''''''

                                obj = dt.Compute("SUM(CreditsAttempted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
                                obj1 = dt.Compute("SUM(AllCreditsAttempted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
                                If Not IsDBNull(obj) Then
                                    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                    If Convert.ToDecimal(obj) < dr("Credits") Or checkCredits = "no" Then
                                        dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
                                        dr("CreditsAttempted") = Convert.ToDecimal(obj)

                                        Dim servicePortalTransferGrade As PortalTransferGradeDB = New PortalTransferGradeDB()
                                        Dim servicePortalTranscript As MyPortalTranscriptDb = New MyPortalTranscriptDb()

                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean
                                            boolIsCourseALab = servicePortalTransferGrade.IsCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    decCredits = servicePortalTranscript.ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr1("ReqId").ToString)
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsAttempted") += decCredits
                                                End If
                                            End If
                                        Next
                                        dr("CreditsAttempted") = Convert.ToDecimal(obj)
                                        dr("AllCreditsAttempted") = Convert.ToDecimal(obj1)

                                    Else
                                        dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
                                        dr("CreditsAttempted") = Convert.ToDecimal(obj)

                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean
                                            boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr1("ReqId").ToString)
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsAttempted") += decCredits
                                                End If
                                            End If
                                        Next

                                        dr("AllCreditsAttempted") = Convert.ToDecimal(obj1)
                                    End If
                                Else

                                    dt.Rows(0)("CreditsAttempted") = 0
                                    For Each dr1 As DataRow In ds.Tables("Results").Rows
                                        Dim boolIsCourseALab As Boolean
                                        boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal
                                            Try
                                                decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr1("ReqId").ToString)
                                            Catch ex As Exception
                                                decCredits = 0
                                            End Try
                                            If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                                dt.Rows(0)("CreditsAttempted") += decCredits
                                            End If
                                        End If
                                    Next

                                    dr("AllCreditsAttempted") = 0
                                End If
                                '
                                '
                                obj = dt.Compute("SUM(HoursCompleted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel)
                                If Not IsDBNull(obj) Then
                                    If Convert.ToDecimal(obj) < dr("Hours") Then
                                        dr("HoursCompleted") = Convert.ToDecimal(obj)
                                    Else
                                        dr("HoursCompleted") = dr("Hours")
                                    End If
                                Else
                                    dr("HoursCompleted") = 0
                                End If
                                '
                                '
                                If Not dr("CreditsEarned") Is DBNull.Value Then
                                    dr("CreditsRemaining") = dr("Credits") - dr("CreditsEarned")
                                Else
                                    dr("CreditsRemaining") = dr("Credits")
                                End If
                                dr("HoursRemaining") = dr("Hours") - dr("HoursCompleted")
                            End If
                        Next
                        maxLevel -= 1
                    Loop
                End If
            End If

            ' End of a loop.......................................
            Debug.WriteLine("Leaving While LOOP in BuildGraduateAuditDT Line 1941 {0}", DateTime.Now)



            'Total credits earned for the program version (Level = 0).
            'If there are no results we will set credits earned and hours completed to 0.
            arrRows = dt.Select("Level=0")
            If arrRows.GetLength(0) > 0 Then
                'modified by Theresa G on 6/22/09 for this issue,the problem is with course groups does not have any GrdSysdetailid
                '16562: BUG: The total credits attempted and total credits earned are not adding up correctly since the upgrade to 2.2
                'obj = dt.Compute("SUM(CreditsEarned)", "Level=0 AND  GrdSysDetailId is NOT NULL AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                obj = dt.Compute("SUM(CreditsEarned)", "Level=0 AND  (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                objFin = dt.Compute("SUM(EarnedFinAidCredits)", "Level=0 AND  (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                'Added By Vijay Ramateke on Feb 16, 2010
                objSchHours = dt.Compute("SUM(ScheduledHours)", "Level=0 AND  (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                'Added By Vijay Ramteke on Feb 16, 2010
                If Not IsDBNull(obj) Then
                    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                    If Convert.ToDecimal(obj) < dt.Rows(0)("Credits") Or checkCredits = "no" Then
                        dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                        For Each dr In ds.Tables("Results").Rows
                            Dim boolIsCourseALab As Boolean = False
                            boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                            If boolIsCourseALab = True Then
                                Dim boolIsCourseACombination As Boolean = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourseCombination(dr("ReqId").ToString)
                                If boolIsCourseACombination = False Then
                                    Dim decCredits As Decimal = 0.0
                                    Try
                                        decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
                                    Catch ex As Exception
                                        decCredits = 0
                                    End Try
                                    If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                        dt.Rows(0)("CreditsEarned") += decCredits
                                    End If
                                End If
                            End If
                        Next
                    End If
                    dt.Rows(0)("EarnedFinAidCredits") = Convert.ToDecimal(objFin)
                    If Not objSchHours Is DBNull.Value Then
                        dt.Rows(0)("ScheduledHours") = Convert.ToDecimal(objSchHours)
                    Else
                        dt.Rows(0)("ScheduledHours") = Convert.ToDecimal("0.0")
                    End If
                Else
                    dt.Rows(0)("CreditsEarned") = 0
                    ' End If
                    For Each dr In ds.Tables("Results").Rows
                        Dim boolIsCourseALab As Boolean = False
                        boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                        If boolIsCourseALab = True Then
                            Dim boolIsCourseACombination As Boolean = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourseCombination(dr("ReqId").ToString)
                            If boolIsCourseACombination = False Then
                                Dim decCredits As Decimal = 0.0
                                Try
                                    decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
                                Catch ex As Exception
                                    decCredits = 0
                                End Try
                                If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                    dt.Rows(0)("CreditsEarned") += decCredits
                                End If
                            End If
                        End If
                    Next

                    If dt.Rows(0) Is Nothing OrElse IsDBNull(dt.Rows(0)("EarnedFinAidCredits")) Then
                        dt.Rows(0)("EarnedFinAidCredits") = 0
                    Else
                        If Not Decimal.TryParse(objFin, dt.Rows(0)("EarnedFinAidCredits")) Then
                            dt.Rows(0)("EarnedFinAidCredits") = 0
                        End If
                    End If

                    If dt.Rows(0) Is Nothing OrElse IsDBNull(dt.Rows(0)("ScheduledHours")) Then
                        dt.Rows(0)("ScheduledHours") = 0
                    Else
                        If Not Decimal.TryParse(objSchHours, dt.Rows(0)("ScheduledHours")) Then
                            dt.Rows(0)("ScheduledHours") = 0
                        End If
                    End If
                End If
            Else
                dt.Rows(0)("CreditsEarned") = 0
                For Each dr In ds.Tables("Results").Rows
                    Dim boolIsCourseALab As Boolean
                    boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                    If boolIsCourseALab = True Then
                        Dim decCredits As Decimal
                        Try
                            decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
                        Catch ex As Exception
                            decCredits = 0
                        End Try
                        If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                            dt.Rows(0)("CreditsEarned") += decCredits
                        End If
                    End If
                Next

            End If
            '
            '
            'code modified by Balaji on 06/03/2009 
            obj = dt.Compute("SUM(HoursCompleted)", "Level=0")
            If Not IsDBNull(obj) Then
                dt.Rows(0)("HoursCompleted") = Convert.ToDecimal(obj)
            Else
                dt.Rows(0)("HoursCompleted") = 0
            End If

            '
            'modified by Theresa G on 6/22/09 for this issue,the problem is with course groups does not have any GrdSysdetailid
            '16562: BUG: The total credits attempted and total credits earned are not adding up correctly since the upgrade to 2.2
            'obj = dt.Compute("SUM(CreditsAttempted)", "Level=0 AND GrdSysDetailId is NOT NULL AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            obj = dt.Compute("SUM(CreditsAttempted)", "Level=0 AND  (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            obj1 = dt.Compute("SUM(AllCreditsAttempted)", "Level=0 AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            objSchHrs = dt.Compute("SUM(TotalScheduledHours)", "Level=0 AND  (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            objSchHrs1 = dt.Compute("SUM(AllScheduledHours)", "Level=0 AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")

            If Not IsDBNull(objSchHrs) Then
                dt.Rows(0)("TotalScheduledHours") = Convert.ToDecimal(objSchHrs)
                ''dr("TotalScheduledHours") = Convert.ToDecimal(objSchHrs)
            Else
                dt.Rows(0)("TotalScheduledHours") = 0.0
                ''dr("TotalScheduledHours") = 0.0
            End If
            If Not IsDBNull(objSchHrs1) Then
                dt.Rows(0)("AllScheduledHours") = Convert.ToDecimal(objSchHrs1)
                ''dr("AllScheduledHours") = Convert.ToDecimal(objSchHrs1)
            Else
                dt.Rows(0)("AllScheduledHours") = 0.0
                ''dr("AllScheduledHours") = Convert.ToDecimal(objSchHrs1)
            End If

            '''''''''''

            If Not IsDBNull(obj) Then
                dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)

                For Each dr In ds.Tables("Results").Rows
                    Dim boolIsCourseALab As Boolean = False
                    boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                    If boolIsCourseALab = True Then
                        Dim boolIsCombination As Boolean = False
                        boolIsCombination = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourseCombination(dr("ReqId").ToString)
                        Dim decCredits As Decimal = 0.0
                        If boolIsCombination = False Then 'if course is not a combination get the credits attempted for the course
                            Try
                                decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
                            Catch ex As Exception
                                decCredits = 0
                            End Try
                            If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                dt.Rows(0)("CreditsAttempted") += decCredits
                            End If
                        End If
                    End If
                Next

                dt.Rows(0)("AllCreditsAttempted") = Convert.ToDecimal(obj1)
            Else

                dt.Rows(0)("CreditsAttempted") = 0
                For Each dr In ds.Tables("Results").Rows
                    Dim boolIsCourseALab As Boolean = False
                    boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                    If boolIsCourseALab = True Then
                        Dim boolIsCombination As Boolean
                        boolIsCombination = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourseCombination(dr("ReqId").ToString)
                        'Dim decCredits As Decimal = 0.0
                        If boolIsCombination = False Then
                            Dim decCredits1 As Decimal
                            Try
                                decCredits1 = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(dt.Rows(0)("StuEnrollId").ToString, myPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, dr("ReqId").ToString)
                            Catch ex As Exception
                                decCredits1 = 0
                            End Try
                            If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                dt.Rows(0)("CreditsAttempted") += decCredits1
                            End If
                        End If
                    End If
                Next

                dt.Rows(0)("AllCreditsAttempted") = 0
            End If
            '
            '
            'Remaining Credits for the program version
            Try
                If dt.Rows(0)("Credits") > dt.Rows(0)("CreditsEarned") Then
                    dt.Rows(0)("CreditsRemaining") = dt.Rows(0)("Credits") - dt.Rows(0)("CreditsEarned")
                Else
                    dt.Rows(0)("CreditsRemaining") = 0
                End If
            Catch ex As Exception
                dt.Rows(0)("CreditsRemaining") = 0
            End Try

            '
            '
            'Remaining hours for the program version
            Try
                If dt.Rows(0)("Hours") > dt.Rows(0)("HoursCompleted") Then
                    dt.Rows(0)("HoursRemaining") = dt.Rows(0)("Hours") - dt.Rows(0)("HoursCompleted")
                Else
                    dt.Rows(0)("HoursRemaining") = 0
                End If
            Catch ex As Exception
                dt.Rows(0)("HoursRemaining") = 0
            End Try

            dt.Rows(0)("GPA") = ComputeGPA(dt.Select("GPA NOT IS NULL"))

        End If
        'End If
        Debug.WriteLine("Leaving BuildGraduateAuditDT {0}", DateTime.Now)
        Return dt
    End Function
    Private Function BuildGraduateAuditDT_SP(ByVal ds As DataSet, ByVal gradeReps As String, ByVal includeHours As String, Optional ByVal Campusid As String = "") As DataTable
        Dim br As New PortalRegisterStudentsBR
        Dim dt As New DataTable("GraduateAudit")
        Dim dr As DataRow

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        dt.Columns.Add("Descrip", Type.GetType("System.String"))
        dt.Columns.Add("Code", Type.GetType("System.String"))
        dt.Columns.Add("ReqId", Type.GetType("System.String"))
        dt.Columns.Add("StartDate", Type.GetType("System.DateTime"))
        dt.Columns.Add("ReqTypeId", Type.GetType("System.String"))
        dt.Columns.Add("ParentId", Type.GetType("System.String"))
        dt.Columns.Add("Level", Type.GetType("System.Int32"))
        dt.Columns.Add("IsCreditsEarned", Type.GetType("System.Boolean"))
        dt.Columns.Add("Credits", Type.GetType("System.Decimal"))
        dt.Columns.Add("Hours", Type.GetType("System.Decimal"))
        dt.Columns.Add("Grade", Type.GetType("System.String"))
        dt.Columns.Add("Score", Type.GetType("System.Decimal"))
        dt.Columns.Add("GPA", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditsRemaining", Type.GetType("System.Decimal"))
        dt.Columns.Add("HoursCompleted", Type.GetType("System.Decimal"))
        dt.Columns.Add("HoursRemaining", Type.GetType("System.Decimal"))
        dt.Columns.Add("IsCreditsAttempted", Type.GetType("System.Boolean"))
        dt.Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
        dt.Columns.Add("AllCreditsAttempted", Type.GetType("System.Decimal"))
        dt.Columns.Add("IsContinuingEd", Type.GetType("System.Boolean"))
        dt.Columns.Add("IsPass", Type.GetType("System.Boolean"))
        dt.Columns.Add("PrgVerId", Type.GetType("System.String"))
        dt.Columns.Add("IsInGPA", Type.GetType("System.Boolean"))
        dt.Columns.Add("EarnedFinAidCredits", Type.GetType("System.Decimal"))
        dt.Columns.Add("FinAidCredits", Type.GetType("System.Decimal"))
        dt.Columns.Add("EquivCourseDesc", Type.GetType("System.String"))
        dt.Columns.Add("StuEnrollId", Type.GetType("System.String"))
        dt.Columns.Add("GrdSysDetailId", Type.GetType("System.String"))
        dt.Columns.Add("IsCourseALab", Type.GetType("System.Boolean"))
        dt.Columns.Add("decCredits", Type.GetType("System.Decimal"))
        'Added By Vijay Ramteke on Feb 16, 2010
        dt.Columns.Add("ScheduledHours", Type.GetType("System.Decimal"))
        'Added By Vijay Ramteke on Feb 16, 2010
        ''New Code Added By Vijay Ramteke For Rally Id DE4346 On January 24, 2011 
        dt.Columns.Add("IsTransferGrade", Type.GetType("System.Boolean"))
        ''New Code Added By Vijay Ramteke For Rally Id DE4346 On January 24, 2011 


        ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
        dt.Columns.Add("TotalScheduledHours", Type.GetType("System.Decimal"))
        dt.Columns.Add("AllScheduledHours", Type.GetType("System.Decimal"))
        dt.Columns.Add("CreditZeroWeight", Type.GetType("System.Decimal"))
        ''''''''''''''''''''''''''''

        Dim checkCredits As String = MyPortalAdvAppSettings.AppSettings("CheckCreditsForContiningEd").ToLower()

        Dim finAid As Decimal = 0.0
        If ds.Tables("DirectChildren").Rows.Count > 0 Then
            dr = dt.NewRow

            'First row is for the program version
            dt.Rows.Add(dr)


            'Loop thru the direct children table
            For Each dr In ds.Tables("DirectChildren").Rows
                AddRowToDT_SP(dr, ds, dt, "", 0, gradeReps, includeHours)
            Next



            'The credits required for the program version can be retrieved from the DirectChildren dt in the DataSet
            dt.Rows(0)("Descrip") = ds.Tables("DirectChildren").Rows(0)("PrgVerDescrip").ToString()
            dt.Rows(0)("Credits") = ds.Tables("DirectChildren").Rows(0)("ProgCredits").ToString()
            dt.Rows(0)("Hours") = ds.Tables("DirectChildren").Rows(0)("ProgHours").ToString()
            dt.Rows(0)("IsContinuingEd") = ds.Tables("DirectChildren").Rows(0)("IsContinuingEd")
            dt.Rows(0)("PrgVerId") = ds.Tables("DirectChildren").Rows(0)("PrgVerId")
            dt.Rows(0)("IsPass") = True
            dt.Rows(0)("IsInGPA") = False
            dt.Rows(0)("StuEnrollId") = ds.Tables("DirectChildren").Rows(0)("StuEnrollId")
            dt.Rows(0)("CreditZeroWeight") = 0.0
            'For Each dr In ds.Tables("Results").Rows
            '    finAid = finAid + CDec(dr("FinAidCredits"))
            'Next
            'dt.Rows(0)("FinAidCredits") = finAid
            For Each dr In ds.Tables("Results").Rows
                'dt.Rows(0)("GrdSysDetailId") = dr("GrdSysDetailId")
                If Not (br.GrdOverRide_SP(dt.Rows(0)("PrgVerId"), dr("reqId").ToString, dr("grade").ToString)) Then
                    Dim dRows() As DataRow = ds.Tables("Results").Select("reqid='" & dr("ReqId").ToString & "'", "startdate desc")
                    If dRows.Length > 1 Then
                        If Not (br.GrdOverRide_SP(dt.Rows(0)("PrgVerId"), dRows(0)("reqId").ToString, dRows(0)("grade").ToString)) Then
                            dt.Rows(0)("IsPass") = False
                            Exit For
                        End If
                    Else
                        dt.Rows(0)("IsPass") = False
                        Exit For
                    End If
                End If
                'Dim boolIsCourseSatisfied As Boolean = (New PortalExamsFacade).isClinicCourseCompletlySatisfied(dt.Rows(0)("StuEnrollId"), 500, dr("ReqId").ToString)
                'If boolIsCourseSatisfied = True Then

                'End If
            Next
            For Each dr In ds.Tables("Results").Rows
                Try
                    If dr("IsInGPA") = True Then
                        dt.Rows(0)("IsInGPA") = True
                        Exit For
                    End If
                Catch ex As Exception
                    dt.Rows(0)("IsInGPA") = False
                End Try
            Next
            For Each dr In ds.Tables("Results").Rows
                Try
                    If dr.IsNull("GrdSysDetailId") Then
                        If (New StuTranscriptDB).GetWeightSum(dr("Reqid").ToString) = 0 Then
                            Dim dtWorkUnit As DataTable
                            dtWorkUnit = (New StuTranscriptDB).GetWorkUnitResults(dt.Rows(0)("StuEnrollId").ToString, dr("ReqId").ToString)
                            If MyPortalAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "true" Then
                                If MyPortalAdvAppSettings.AppSettings("GradesFormat", Campusid).ToLower = "numeric" Then
                                    'rowC("CreditsAttempted") = 0
                                    Dim arrWU() As DataRow
                                    arrWU = dtWorkUnit.Select("Score >= 0")
                                    If dtWorkUnit.Rows.Count > 0 Then
                                        If GetCreditsEarned(dtWorkUnit, dr("Score")) Then
                                            'rowC("Credits") = dr2("Credits")
                                            ''Added by saraswathi To fix issue 15845 and 16178
                                            ''Check if iscreditsearned is set , then add credits else donot add
                                            ''Added on may 13 2009
                                            If Not dr.IsNull("IsCreditsEarned") Then
                                                If dr("IsCreditsEarned").ToString = "True" Or dr("IsCreditsEarned").ToString = "1" Then
                                                    'rowC("Credits") = dr2("Credits")
                                                    dt.Rows(0)("CreditZeroWeight") += dr("Credits")
                                                End If
                                            End If
                                            'rowC("CreditsAttempted") = dr2("Credits")
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                Catch ex As Exception
                    dt.Rows(0)("CreditZeroWeight") = 0.0
                End Try
            Next
            'Nested Course Groups
            'Credits Attempted, Hours Completed, Credits Remaining and Hours Remaining
            Dim maxLevel As Integer = dt.Compute("MAX(Level)", "")
            Dim obj, obj1 As Object
            Dim objSchHrs, objSchHrs1 As Object
            Dim objFin As Object
            'Added By Vijay Ramteke on Feb 16, 2010
            Dim objSchHours As Object
            'Added By Vijay Ramteke on Feb 16, 2010
            Dim arrRows() As DataRow
            'Find out if there are Course Groups in the program version definition
            Dim arrReqGrps() As DataRow = dt.Select("ReqTypeId=2")
            If arrReqGrps.GetLength(0) > 0 Then
                If maxLevel = 0 Then
                    'This section will catch course groups without definitions. In other words, course groups without courses.
                    For Each dr In arrReqGrps
                        dr("CreditsEarned") = 0
                        dr("EarnedFinAidCredits") = 0
                        dr("CreditsAttempted") = 0
                        dr("AllCreditsAttempted") = 0
                        dr("HoursCompleted") = 0
                        dr("CreditsRemaining") = dr("Credits") - dr("CreditsEarned")
                        dr("HoursRemaining") = dr("Hours") - dr("HoursCompleted")
                    Next
                Else
                    Do While maxLevel > 0
                        For Each dr In arrReqGrps
                            Dim strGrp As String
                            strGrp = dr("Descrip")
                            'Find out if there are children of level = maxLevel for current Course Group
                            arrRows = dt.Select("ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel)
                            If arrRows.GetLength(0) > 0 Then
                                obj = dt.Compute("SUM(CreditsEarned)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                                objFin = dt.Compute("SUM(EarnedFinAidCredits)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                                'Added BY Vijay Ramteke on Feb 16, 2010
                                objSchHours = dt.Compute("SUM(ScheduledHours)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                                'Added BY Vijay Ramteke on Feb 16, 2010
                                If Not IsDBNull(obj) Then
                                    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                    If Convert.ToDecimal(obj) < dr("Credits") Or checkCredits = "no" Then
                                        dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                        dr("CreditsEarned") = Convert.ToDecimal(obj)

                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean = False
                                            boolIsCourseALab = dr1("IsCourseALab")
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal
                                                Try
                                                    'decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService_SP(dt.Rows(0)("StuEnrollId").ToString, dr1("ReqId").ToString)
                                                    decCredits = dr1("decCredits")
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsEarned") += decCredits
                                                End If
                                            End If
                                            If MyPortalAdvAppSettings.AppSettings("GradesFormat", Campusid).ToLower = "numeric" And dr1.IsNull("GrdSysDetailId") Then
                                                'rowC("CreditsAttempted") = 0
                                                Dim dtWorkUnit As DataTable
                                                dtWorkUnit = (New StuTranscriptDB).GetWorkUnitResults_SP(dt.Rows(0)("StuEnrollId").ToString, dr1("ReqId").ToString)
                                                If dtWorkUnit.Rows.Count > 0 Then
                                                    If GetCreditsEarned(dtWorkUnit, dr1("Score")) Then
                                                        'rowC("Credits") = dr2("Credits")
                                                        ''Added by saraswathi To fix issue 15845 and 16178
                                                        ''Check if iscreditsearned is set , then add credits else donot add
                                                        ''Added on may 13 2009
                                                        If Not dr1.IsNull("IsCreditsEarned") Then
                                                            If dr1("IsCreditsEarned").ToString = "True" Or dr1("IsCreditsEarned").ToString = "1" Then
                                                                dt.Rows(0)("CreditsEarned") += dr1("Credits")
                                                            End If
                                                        End If
                                                    End If
                                                End If

                                            End If
                                        Next
                                        dr("EarnedFinAidCredits") = Convert.ToDecimal(objFin)
                                        dr("ScheduledHours") = 0.0
                                        If IsDBNull(objSchHours) = False Then
                                            If Decimal.TryParse(objSchHours, dr("ScheduledHours")) = False Then
                                                dr("ScheduledHours") = 0.0
                                            End If
                                        End If
                                        'Try
                                        '    dr("ScheduledHours") = Convert.ToDecimal(objSchHours)
                                        'Catch ex As Exception
                                        '    dr("ScheduledHours") = Convert.ToDecimal("0.0")
                                        'End Try
                                    Else
                                        If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                            dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                            'added by Theresa G on 6/22/09 for this issue,the problem is with course groups
                                            '16562: BUG: The total credits attempted and total credits earned are not adding up correctly since the upgrade to 2.2
                                            dr("CreditsEarned") = Convert.ToDecimal(obj)
                                        Else
                                            dt.Rows(0)("CreditsEarned") = 0
                                        End If
                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean = False
                                            ' boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                            boolIsCourseALab = dr1("IsCourseALab")
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal = 0.0
                                                If IsDBNull(dr1("decCredits")) = False Then
                                                    If Decimal.TryParse(dr1("decCredits"), decCredits) = False Then
                                                        decCredits = 0.0
                                                    End If
                                                End If

                                                'Try
                                                '    'decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService_SP(dt.Rows(0)("StuEnrollId").ToString, dr1("ReqId").ToString)
                                                '    decCredits = dr1("decCredits")
                                                'Catch ex As System.Exception
                                                '    decCredits = 0

                                                'End Try
                                                If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsEarned") += decCredits
                                                End If
                                            End If
                                        Next
                                        dr("EarnedFinAidCredits") = dr("FinAidCredits")
                                        dr("ScheduledHours") = dr("ScheduledHours")
                                    End If
                                Else
                                    dt.Rows(0)("CreditsEarned") = 0
                                    For Each dr1 As DataRow In ds.Tables("Results").Rows
                                        Dim boolIsCourseALab As Boolean
                                        boolIsCourseALab = dr1("IsCourseALab")
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal = 0.0
                                            If Decimal.TryParse(dr1("decCredits"), decCredits) = False Then
                                                decCredits = 0.0
                                            End If

                                            'Try
                                            '    'decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService_SP(dt.Rows(0)("StuEnrollId").ToString, dr1("ReqId").ToString)
                                            '    decCredits = dr1("decCredits")
                                            'Catch ex As System.Exception
                                            '    decCredits = 0

                                            'End Try
                                            If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                                dt.Rows(0)("CreditsEarned") += decCredits
                                            End If
                                        End If
                                    Next
                                    dr("EarnedFinAidCredits") = 0
                                    dr("ScheduledHours") = dr("ScheduledHours")
                                End If
                                '
                                '


                                ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
                                objSchHrs = dt.Compute("SUM(TotalScheduledHours)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
                                objSchHrs1 = dt.Compute("SUM(AllScheduledHours)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")

                                If Not IsDBNull(objSchHrs) Then
                                    dt.Rows(0)("TotalScheduledHours") = Convert.ToDecimal(objSchHrs)
                                    ''dr("TotalScheduledHours") = Convert.ToDecimal(objSchHrs)
                                Else
                                    dt.Rows(0)("TotalScheduledHours") = 0.0
                                    ''dr("TotalScheduledHours") = 0.0
                                End If
                                If Not IsDBNull(objSchHrs1) Then
                                    dt.Rows(0)("AllScheduledHours") = Convert.ToDecimal(objSchHrs1)
                                    ''dr("AllScheduledHours") = Convert.ToDecimal(objSchHrs1)
                                Else
                                    dt.Rows(0)("AllScheduledHours") = 0.0
                                    ''dr("AllScheduledHours") = 0.0
                                End If

                                '''''''''''

                                obj = dt.Compute("SUM(CreditsAttempted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND GrdSysDetailId is NOT NULL AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
                                obj1 = dt.Compute("SUM(AllCreditsAttempted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
                                If Not IsDBNull(obj) Then
                                    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                                    If Convert.ToDecimal(obj) < dr("Credits") Or checkCredits = "no" Then
                                        'If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
                                        'If Not dt.Rows(0)("CreditsAttempted") Is System.DBNull.Value Then
                                        '    dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
                                        '    'added by Theresa G on 6/22/09 for this issue,the problem is with course groups
                                        '    '16562: BUG: The total credits attempted and total credits earned are not adding up correctly since the upgrade to 2.2
                                        '    dr("CreditsAttempted") = Convert.ToDecimal(obj)
                                        'Else
                                        '    dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
                                        '    'added by Theresa G on 6/22/09 for this issue,the problem is with course groups
                                        '    '16562: BUG: The total credits attempted and total credits earned are not adding up correctly since the upgrade to 2.2
                                        '    dr("CreditsAttempted") = Convert.ToDecimal(obj)
                                        'End If
                                        'NZ 11/19/2012: the IF and Else parts of the statement above are identical
                                        dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
                                        dr("CreditsAttempted") = Convert.ToDecimal(obj)

                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean = False
                                            'boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                            boolIsCourseALab = dr1("IsCourseALab")
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal = 0.0
                                                Try
                                                    'decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService_SP(dt.Rows(0)("StuEnrollId").ToString, dr1("ReqId").ToString)
                                                    decCredits = dr1("decCredits")
                                                Catch ex As Exception
                                                    decCredits = 0

                                                End Try
                                                If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsAttempted") += decCredits
                                                End If
                                            End If
                                        Next
                                        dr("CreditsAttempted") = Convert.ToDecimal(obj)
                                        dr("AllCreditsAttempted") = Convert.ToDecimal(obj1)

                                    Else
                                        'If Not dt.Rows(0)("CreditsAttempted") Is System.DBNull.Value Then
                                        '    dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
                                        '    'added by Theresa G on 6/22/09 for this issue,the problem is with course groups
                                        '    '16562: BUG: The total credits attempted and total credits earned are not adding up correctly since the upgrade to 2.2
                                        '    dr("CreditsAttempted") = Convert.ToDecimal(obj)
                                        'Else
                                        '    dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
                                        '    'added by Theresa G on 6/22/09 for this issue,the problem is with course groups
                                        '    '16562: BUG: The total credits attempted and total credits earned are not adding up correctly since the upgrade to 2.2
                                        '    dr("CreditsAttempted") = Convert.ToDecimal(obj)
                                        'End If
                                        'NZ 11/19/2012: the IF and Else parts of the statement above are identical
                                        dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
                                        dr("CreditsAttempted") = Convert.ToDecimal(obj)

                                        For Each dr1 As DataRow In ds.Tables("Results").Rows
                                            Dim boolIsCourseALab As Boolean = False
                                            ' boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                            boolIsCourseALab = dr1("IsCourseALab")
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal = 0.0
                                                Try
                                                    'decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService_SP(dt.Rows(0)("StuEnrollId").ToString, dr1("ReqId").ToString)
                                                    decCredits = dr1("decCredits")
                                                Catch ex As Exception
                                                    decCredits = 0

                                                End Try
                                                If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                                    dt.Rows(0)("CreditsAttempted") += decCredits
                                                End If
                                            End If
                                        Next
                                        dr("AllCreditsAttempted") = Convert.ToDecimal(obj1)
                                    End If
                                Else

                                    dt.Rows(0)("CreditsAttempted") = 0
                                    For Each dr1 As DataRow In ds.Tables("Results").Rows
                                        Dim boolIsCourseALab As Boolean = False
                                        ' boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr1("ReqId").ToString)
                                        boolIsCourseALab = dr1("IsCourseALab")
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal = 0.0
                                            Try
                                                'decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService_SP(dt.Rows(0)("StuEnrollId").ToString, dr1("ReqId").ToString)
                                                decCredits = dr1("decCredits")
                                            Catch ex As Exception
                                                decCredits = 0

                                            End Try
                                            If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                                dt.Rows(0)("CreditsAttempted") += decCredits
                                            End If
                                        End If
                                    Next
                                    dr("AllCreditsAttempted") = 0
                                End If
                                '
                                '
                                obj = dt.Compute("SUM(HoursCompleted)", "ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel)
                                If Not IsDBNull(obj) Then
                                    If Convert.ToDecimal(obj) < dr("Hours") Then
                                        dr("HoursCompleted") = Convert.ToDecimal(obj)
                                    Else
                                        dr("HoursCompleted") = dr("Hours")
                                    End If
                                Else
                                    dr("HoursCompleted") = 0
                                End If
                                '
                                '
                                If Not dr("CreditsEarned") Is DBNull.Value Then
                                    dr("CreditsRemaining") = dr("Credits") - dr("CreditsEarned")
                                Else
                                    dr("CreditsRemaining") = dr("Credits")
                                End If
                                dr("HoursRemaining") = dr("Hours") - dr("HoursCompleted")
                            End If
                        Next
                        maxLevel -= 1
                    Loop
                End If
            End If




            'Total credits earned for the program version (Level = 0).
            'If there are no results we will set credits earned and hours completed to 0.
            arrRows = dt.Select("Level=0")
            If arrRows.GetLength(0) > 0 Then
                'modified by Theresa G on 6/22/09 for this issue,the problem is with course groups does not have any GrdSysdetailid
                '16562: BUG: The total credits attempted and total credits earned are not adding up correctly since the upgrade to 2.2
                'obj = dt.Compute("SUM(CreditsEarned)", "Level=0 AND  GrdSysDetailId is NOT NULL AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                obj = dt.Compute("SUM(CreditsEarned)", "Level=0 AND  (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                objFin = dt.Compute("SUM(EarnedFinAidCredits)", "Level=0 AND  (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                'Added By Vijay Ramateke on Feb 16, 2010
                objSchHours = dt.Compute("SUM(ScheduledHours)", "Level=0 AND  (IsCreditsEarned = 1 OR ReqTypeId = 2)")
                'Added By Vijay Ramteke on Feb 16, 2010
                If Not IsDBNull(obj) Then
                    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                    If Convert.ToDecimal(obj) < dt.Rows(0)("Credits") Or checkCredits = "no" Then
                        'If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
                        'If Not dt.Rows(0)("CreditsEarned") Is System.DBNull.Value Then
                        '    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                        'Else
                        '    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                        'End If
                        'NZ 11/19/2012: the IF and Else parts of the statement above are identical
                        dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)

                        For Each dr In ds.Tables("Results").Rows
                            Dim boolIsCourseALab As Boolean = False
                            'boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                            boolIsCourseALab = dr("IsCourseALab")
                            If boolIsCourseALab = True Then
                                Dim boolIsCourseACombination As Boolean = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourseCombination_SP(dr("ReqId").ToString)
                                If boolIsCourseACombination = False Then
                                    Dim decCredits As Decimal = 0.0
                                    Try
                                        'decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService_SP(dt.Rows(0)("StuEnrollId").ToString, dr("ReqId").ToString)
                                        decCredits = dr("decCredits")
                                    Catch ex As Exception
                                        decCredits = 0

                                    End Try
                                    If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                        dt.Rows(0)("CreditsEarned") += decCredits
                                    End If
                                End If
                            End If
                        Next
                    End If
                    dt.Rows(0)("EarnedFinAidCredits") = Convert.ToDecimal(objFin)
                    'Added By Vijay Ramteke on Feb 16, 2010
                    If Not objSchHours Is DBNull.Value Then
                        dt.Rows(0)("ScheduledHours") = Convert.ToDecimal(objSchHours)
                    Else
                        dt.Rows(0)("ScheduledHours") = Convert.ToDecimal("0.0")
                    End If
                    'Added By Vijay Ramteke on Feb 16, 2010
                Else
                    'If Not dt.Rows(0)("CreditsEarned") Is System.DBNull.Value Then
                    '    dt.Rows(0)("CreditsEarned") = Convert.ToDecimal(obj)
                    'Else
                    dt.Rows(0)("CreditsEarned") = 0
                    ' End If
                    For Each dr In ds.Tables("Results").Rows
                        Dim boolIsCourseALab As Boolean = False
                        'boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                        boolIsCourseALab = dr("IsCourseALab")
                        If boolIsCourseALab = True Then
                            Dim boolIsCourseACombination As Boolean = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourseCombination_SP(dr("ReqId").ToString)
                            If boolIsCourseACombination = False Then
                                Dim decCredits As Decimal = 0.0
                                Try
                                    'decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService_SP(dt.Rows(0)("StuEnrollId").ToString, dr("ReqId").ToString)
                                    decCredits = dr("decCredits")
                                Catch ex As Exception
                                    decCredits = 0

                                End Try
                                If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                                    dt.Rows(0)("CreditsEarned") += decCredits
                                End If
                            End If
                        End If
                    Next
                    Try
                        dt.Rows(0)("EarnedFinAidCredits") = Convert.ToDecimal(objFin)
                    Catch ex As Exception
                        dt.Rows(0)("EarnedFinAidCredits") = 0
                    End Try
                    'Added By Vijay Ramteke on Feb 16 ,2010
                    Try
                        dt.Rows(0)("ScheduledHours") = Convert.ToDecimal(objSchHours)
                    Catch ex As Exception
                        dt.Rows(0)("ScheduledHours") = 0
                    End Try
                    'Added By Vijay Ramteke on Feb 16 ,2010
                End If
            Else
                dt.Rows(0)("CreditsEarned") = 0
                For Each dr In ds.Tables("Results").Rows
                    Dim boolIsCourseALab As Boolean = False
                    ' boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                    boolIsCourseALab = dr("IsCourseALab")
                    If boolIsCourseALab = True Then
                        Dim decCredits As Decimal = 0.0
                        Try
                            'decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService_SP(dt.Rows(0)("StuEnrollId").ToString, dr("ReqId").ToString)
                            decCredits = dr("decCredits")
                        Catch ex As Exception
                            decCredits = 0

                        End Try
                        If Not dt.Rows(0)("CreditsEarned") Is DBNull.Value Then
                            dt.Rows(0)("CreditsEarned") += decCredits
                        End If
                    End If
                Next
            End If
            '
            '
            'code modified by Balaji on 06/03/2009 
            obj = dt.Compute("SUM(HoursCompleted)", "Level=0")
            If Not IsDBNull(obj) Then
                'If Convert.ToDecimal(obj) < dt.Rows(0)("Hours") Then
                dt.Rows(0)("HoursCompleted") = Convert.ToDecimal(obj)
                'Else
                '    dt.Rows(0)("HoursCompleted") = dt.Rows(0)("Hours")
                'End If
            Else
                dt.Rows(0)("HoursCompleted") = 0
            End If

            ''''''Added By Kamalesh Ahuja on Jan 31, 2011 for Rally Issue Id 1208
            objSchHrs = dt.Compute("SUM(TotalScheduledHours)", "Level=0 AND  (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            objSchHrs1 = dt.Compute("SUM(AllScheduledHours)", "Level=0 AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")

            If Not IsDBNull(objSchHrs) Then
                dt.Rows(0)("TotalScheduledHours") = Convert.ToDecimal(objSchHrs)
                ''dr("TotalScheduledHours") = Convert.ToDecimal(objSchHrs)
            Else
                dt.Rows(0)("TotalScheduledHours") = 0.0
                ''dr("TotalScheduledHours") = 0.0
            End If
            If Not IsDBNull(objSchHrs1) Then
                dt.Rows(0)("AllScheduledHours") = Convert.ToDecimal(objSchHrs1)
                ''dr("AllScheduledHours") = Convert.ToDecimal(objSchHrs1)
            Else
                dt.Rows(0)("AllScheduledHours") = 0.0
                ''dr("AllScheduledHours") = Convert.ToDecimal(objSchHrs1)
            End If

            '''''''''''

            'modified by Theresa G on 6/22/09 for this issue,the problem is with course groups does not have any GrdSysdetailid
            '16562: BUG: The total credits attempted and total credits earned are not adding up correctly since the upgrade to 2.2
            'obj = dt.Compute("SUM(CreditsAttempted)", "Level=0 AND GrdSysDetailId is NOT NULL AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            obj = dt.Compute("SUM(CreditsAttempted)", "Level=0 AND  (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            obj1 = dt.Compute("SUM(AllCreditsAttempted)", "Level=0 AND (IsCreditsAttempted = 1 OR ReqTypeId = 2)")
            If Not IsDBNull(obj) Then
                'If Convert.ToDecimal(obj) < dt.Rows(0)("Credits") Then
                'If SingletonAppSettings.AppSettings("TranscriptType", CampusId).ToString.ToLower = "traditional_numeric" Then
                'If Not dt.Rows(0)("CreditsAttempted") Is System.DBNull.Value Then
                '    dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
                'Else
                '    dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)
                'End If
                'NZ 11/19/2012: the IF and Else parts of the statement above are identical
                dt.Rows(0)("CreditsAttempted") = Convert.ToDecimal(obj)

                For Each dr In ds.Tables("Results").Rows
                    Dim boolIsCourseALab As Boolean
                    'boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                    boolIsCourseALab = dr("IsCourseALab")
                    If boolIsCourseALab = True Then
                        Dim boolIsCombination As Boolean
                        boolIsCombination = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourseCombination_SP(dr("ReqId").ToString)
                        Dim decCredits As Decimal
                        If boolIsCombination = False Then 'if course is not a combination get the credits attempted for the course
                            Try
                                'decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService_SP(dt.Rows(0)("StuEnrollId").ToString, dr("ReqId").ToString)
                                decCredits = dr("decCredits")
                            Catch ex As Exception
                                decCredits = 0

                            End Try
                            If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                dt.Rows(0)("CreditsAttempted") += decCredits
                            End If
                        End If
                    End If
                Next
                dt.Rows(0)("AllCreditsAttempted") = Convert.ToDecimal(obj1)
            Else

                dt.Rows(0)("CreditsAttempted") = 0
                For Each dr In ds.Tables("Results").Rows
                    Dim boolIsCourseALab As Boolean = False
                    ' boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                    boolIsCourseALab = dr("IsCourseALab")
                    If boolIsCourseALab = True Then
                        Dim boolIsCombination As Boolean = False
                        boolIsCombination = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourseCombination_SP(dr("ReqId").ToString)
                        'Dim decCredits As Decimal = 0.0
                        If boolIsCombination = False Then
                            Dim decCredits1 As Decimal = 0.0
                            Try
                                'decCredits1 = (New MyPortalTranscriptDB).ComputeWithCreditsPerService_SP(dt.Rows(0)("StuEnrollId").ToString, dr("ReqId").ToString)
                                decCredits1 = dr("decCredits")
                            Catch ex As Exception
                                decCredits1 = 0

                            End Try
                            If Not dt.Rows(0)("CreditsAttempted") Is DBNull.Value Then
                                dt.Rows(0)("CreditsAttempted") += decCredits1
                            End If
                        End If
                    End If
                Next
                dt.Rows(0)("AllCreditsAttempted") = 0
            End If
            '
            '
            'Remaining Credits for the program version
            Try
                If dt.Rows(0)("Credits") > dt.Rows(0)("CreditsEarned") Then
                    dt.Rows(0)("CreditsRemaining") = dt.Rows(0)("Credits") - dt.Rows(0)("CreditsEarned")
                Else
                    dt.Rows(0)("CreditsRemaining") = 0
                End If
            Catch ex As Exception
                dt.Rows(0)("CreditsRemaining") = 0
            End Try

            '
            '
            'Remaining hours for the program version
            Try
                If dt.Rows(0)("Hours") > dt.Rows(0)("HoursCompleted") Then
                    dt.Rows(0)("HoursRemaining") = dt.Rows(0)("Hours") - dt.Rows(0)("HoursCompleted")
                Else
                    dt.Rows(0)("HoursRemaining") = 0
                End If
            Catch ex As Exception
                dt.Rows(0)("HoursRemaining") = 0
            End Try

            dt.Rows(0)("GPA") = ComputeGPA(dt.Select("GPA NOT IS NULL"))

        End If
        'End If

        Return dt
    End Function
    Private Sub AddCoursesToCummProgress(ByRef dtProgress As DataTable, ByVal arTermCourses() As DataRow, ByVal gradeReps As String)
        Dim row As DataRow
        Dim arRows() As DataRow

        If dtProgress.Rows.Count = 0 Then
            'First time = first attended term
            For Each dr As DataRow In arTermCourses
                row = dtProgress.NewRow
                row("ReqId") = dr("ReqId")
                row("Credits") = dr("Credits")
                row("IsCreditsEarned") = dr("IsCreditsEarned")
                row("IsCreditsAttempted") = dr("IsCreditsAttempted")
                If row("IsCreditsEarned") Then
                    row("CreditsEarned") = dr("Credits")
                Else
                    row("CreditsEarned") = 0.0
                End If
                If row("IsCreditsAttempted") Then
                    row("CreditsAttempted") = dr("Credits")
                Else
                    row("CreditsAttempted") = 0.0
                End If
                row("GPA") = dr("GPA")
                row("Score") = dr("Score")
                dtProgress.Rows.Add(row)
            Next

        Else
            'Check if course has already been taken. 
            'If course was already taken, increment credits attempted and use grade based on a 
            'variable from the web.config
            'If not, add it to CummProgress table.
            For Each dr As DataRow In arTermCourses
                arRows = dtProgress.Select("ReqId='" & dr("ReqId").ToString & "'")
                If arRows.GetLength(0) > 0 Then
                    'Course repetition
                    'Course was alredy taken
                    row = arRows(0)
                    row("Score") = dr("Score")
                    If gradeReps.ToUpper = "LATEST" Then
                        'LATEST grade
                        row("GPA") = dr("GPA")
                    ElseIf gradeReps.ToUpper = "BEST" Then
                        'BEST grade
                        If Not (dr.IsNull("GPA")) Then
                            If Not (row.IsNull("GPA")) Then
                                If dr("GPA") > row("GPA") Then
                                    row("GPA") = dr("GPA")
                                    'Troy:10/27/2006 The current requirement is that we have to allow
                                    'for situations where a student has a passing grade (for eg. C) but
                                    'retakes the course to get a better grade such as an "A". This means
                                    'that the row("IsCreditsEarned") is already set to True in those cases.
                                    row("IsCreditsEarned") = True
                                    row("CreditsEarned") += dr("Credits")

                                Else
                                    'Do nothing because row("GPA") >= dr("GPA")
                                End If
                            Else
                                'row("GPA") is Null
                                row("GPA") = dr("GPA")
                                row("IsCreditsEarned") = True
                                row("CreditsEarned") += dr("Credits")
                            End If
                        End If
                    Else
                        'Average grade
                        Dim AVGGPA As Decimal
                        For Each dr1 As DataRow In arRows
                            Try
                                AVGGPA += CType(dr1("GPA"), Decimal)
                            Catch ex As Exception
                                AVGGPA += 0
                            End Try

                        Next
                        Try
                            AVGGPA += CType(dr("GPA"), Decimal)
                        Catch ex As Exception
                            AVGGPA += 0
                        End Try
                        AVGGPA = AVGGPA / (arRows.Length + 1)
                        row("GPA") = AVGGPA
                        row("IsCreditsEarned") = True
                        row("CreditsEarned") += dr("Credits")
                    End If

                    'CreditsEarned: Do nothing
                    'Troy:10/27/2006:We actually need to inlcude it in credits earned since the failed
                    'grade would not have been included in the credits earned prior to this. See the 
                    'comments added above.


                    'CreditsAttempted
                    'Troy:10/27/2006:The following line was commented out since the credits
                    'for the course is already included in the attempted credits and we should
                    'not count it twice. Remember that we are using only one record for the course
                    'whether that is the best or latest.
                    'row("CreditsAttempted") += dr("Credits")
                    '
                    '
                Else
                    'Course has not been previously taken
                    row = dtProgress.NewRow
                    row("ReqId") = dr("ReqId")
                    row("Credits") = dr("Credits")
                    row("IsCreditsEarned") = dr("IsCreditsEarned")
                    row("IsCreditsAttempted") = dr("IsCreditsAttempted")
                    If row("IsCreditsEarned") Then
                        row("CreditsEarned") = dr("Credits")
                    Else
                        row("CreditsEarned") = 0.0
                    End If
                    If row("IsCreditsAttempted") Then
                        row("CreditsAttempted") = dr("Credits")
                    Else
                        row("CreditsAttempted") = 0.0
                    End If
                    row("GPA") = dr("GPA")
                    row("Score") = dr("Score")
                    dtProgress.Rows.Add(row)
                End If

            Next
        End If
    End Sub

    Private Function IsValueAlreadyInTable(ByVal valueToSearchFor As String, ByVal columnName As String, ByVal dt As DataTable) As Boolean
        Dim result As Boolean

        For Each dr As DataRow In dt.Rows
            If Not dr.IsNull(columnName) Then
                If dr(columnName).ToString = valueToSearchFor Then
                    result = True
                    Exit For
                End If
            End If
        Next

        Return result
    End Function

    Private Function ComputeGPA(ByVal arrRows() As DataRow, Optional ByVal isResults As Boolean = False) As Decimal

        Dim myPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        Dim gpaMethod As String = myPortalAdvAppSettings.AppSettings("GPAMethod").ToLower
        Dim sum As Decimal
        Dim cnt As Decimal
        Dim gpa As Decimal
        Dim dr As DataRow

        If gpaMethod = "simpleavg" Then

            'This code is to compute a simple average GPA.
            'SimpleGPA = SUM (Course GPA) / Number of Courses


            For Each dr In arrRows
                If Not (dr("GPA") Is DBNull.Value) Then
                    cnt += 1
                    sum += dr("GPA")
                End If
            Next

        Else

            'gpaMethod = "WeightedAVG"

            'This code is to compute a weigthed average GPA.
            'WeightedAVG = SUM (Course GPA * Course Credits) / Total Credits Attempted

            For Each dr In arrRows

                If Not (dr("GPA") Is DBNull.Value) Then

                    If isResults Then
                        'Results table

                        '----------commented by Theresa G on 7/9/2009 for mantis [Advantage NEW 16452]: Review: Galen -  Transcript totals are incorrect.
                        '----------the cumGPA will be calculated if the GPA is checked in the grade system and even though the student does earn the credits.
                        'If Not (dr("IsCreditsEarned") Is System.DBNull.Value) Then
                        '    If dr("IsCreditsEarned") Then
                        '        sum += dr("GPA") * dr("Credits")
                        '    End If
                        'End If


                        sum += dr("GPA") * dr("Credits")


                        If Not (dr("IsCreditsAttempted") Is DBNull.Value) Then
                            If dr("IsCreditsAttempted") Then
                                cnt += dr("Credits")
                            End If
                        End If


                    Else

                        'GraduateAudit table
                        '----------commented by Theresa G on 7/9/2009 for mantis [Advantage NEW 16452]: Review: Galen -  Transcript totals are incorrect.
                        '----------the cumGPA will be calculated if the GPA is checked in the grade system and even though the student does earn the credits.
                        'If Not (dr("IsCreditsEarned") Is System.DBNull.Value) Then
                        '    If dr("IsCreditsEarned") Then       'Or dr("ReqTypeId") = 2 
                        sum += dr("GPA") * dr("Credits")
                        '    End If
                        'End If
                        If Not (dr("IsCreditsAttempted") Is DBNull.Value) Then
                            If dr("IsCreditsAttempted") Then    'Or dr("ReqTypeId") = 2 
                                cnt += dr("CreditsAttempted")
                            End If
                        End If

                    End If

                End If
            Next

        End If

        If cnt <> 0 Then
            gpa = sum / cnt
        End If

        Return Math.Round(gpa, 2)
    End Function

    Private Function ComputeGPA(ByVal dt As DataTable) As Decimal

        Dim myPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        Dim gpaMethod As String = myPortalAdvAppSettings.AppSettings("GPAMethod").ToLower
        Dim sum As Decimal
        Dim cnt As Decimal
        Dim gpa As Decimal
        Dim dr As DataRow

        If gpaMethod = "simpleavg" Then

            'This code is to compute a simple average GPA.
            'SimpleGPA = SUM (Course GPA) / Number of Courses


            For Each dr In dt.Rows
                If Not (dr("GPA") Is DBNull.Value) Then
                    cnt += 1
                    sum += dr("GPA")
                End If
            Next

        Else

            'gpaMethod = "WeightedAVG"

            'This code is to compute a weigthed average GPA.
            'WeightedAVG = SUM (Course GPA * Course Credits) / Total Credits Attempted

            For Each dr In dt.Rows
                If Not (dr("GPA") Is DBNull.Value) Then

                    '----------commented by Theresa G on 7/9/2009 for mantis [Advantage NEW 16452]: Review: Galen -  Transcript totals are incorrect.
                    '----------the cumGPA will be calculated if the GPA is checked in the grade system and even though the student does earn the credits.
                    'If dr("IsCreditsEarned") Then
                    sum += dr("GPA") * dr("Credits")
                    'End If
                    If dr("IsCreditsAttempted") Then
                        cnt += dr("CreditsAttempted")
                    End If
                End If
            Next

        End If

        If cnt <> 0 Then
            gpa = sum / cnt
        End If

        Return Math.Round(gpa, 2)
    End Function

    Private Function ComputeCumGPA(ByVal dt As DataTable) As Decimal

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        Dim gpaMethod As String = MyPortalAdvAppSettings.AppSettings("GPAMethod").ToLower
        Dim sum As Decimal
        Dim cnt As Decimal
        Dim gpa As Decimal
        Dim dr As DataRow


        For Each dr In dt.Rows
            If Not (dr("Score") Is DBNull.Value) Then
                cnt += 1
                sum += dr("Score")
            End If
        Next


        If cnt <> 0 Then
            gpa = sum / cnt
        End If

        Return Math.Round(gpa, 2)
    End Function
    Private Sub SetProperties()

        If dtGraduateAudit.Rows.Count > 0 Then
            Dim firstRow As DataRow
            firstRow = dtGraduateAudit.Rows(0)
            If Not firstRow("AllCreditsAttempted") Is DBNull.Value Then m_AllAttemptedCredits = firstRow("AllCreditsAttempted") Else m_AllAttemptedCredits = 0
            If Not firstRow("GPA") Is DBNull.Value Then m_GPA = firstRow("GPA") Else m_GPA = 0
            If Not firstRow("CreditsAttempted") Is DBNull.Value Then m_AttemptedCredits = firstRow("CreditsAttempted") Else m_AttemptedCredits = 0
            If Not firstRow("CreditsEarned") Is DBNull.Value Then m_EarnedCredits = firstRow("CreditsEarned") Else m_EarnedCredits = 0
            If Not firstRow("EarnedFinAidCredits") Is DBNull.Value Then m_EarnedFinAidCredits = firstRow("EarnedFinAidCredits") Else m_EarnedFinAidCredits = 0
            If Not firstRow("HoursCompleted") Is DBNull.Value Then m_CompletedHours = firstRow("HoursCompleted") Else m_CompletedHours = 0
            If Not firstRow("CreditsRemaining") Is DBNull.Value Then m_RemainingCredits = firstRow("CreditsRemaining") Else m_RemainingCredits = 0
            If Not firstRow("HoursRemaining") Is DBNull.Value Then m_RemainingHours = firstRow("HoursRemaining") Else m_RemainingHours = 0
            If dtResults.Rows.Count >= 1 Then m_TotalClasses = dtResults.Rows.Count Else m_TotalClasses = 0
            If Not firstRow("IsInGPA") Is DBNull.Value Then m_GradeInGPA = firstRow("IsInGPA") Else m_GradeInGPA = 0
            If Not firstRow("CreditZeroWeight") Is DBNull.Value Then m_CreditZeroWeight = firstRow("CreditZeroWeight") Else m_CreditZeroWeight = 0
            ''''''Added By Kamalesh Ahuja on Jan 30, 2011 for Rally Issue Id 1208
            If Not firstRow("TotalScheduledHours") Is DBNull.Value Then m_TotalScheduledHours = firstRow("TotalScheduledHours") Else m_TotalScheduledHours = 0
            If Not firstRow("AllScheduledHours") Is DBNull.Value Then m_AllScheduledHours = firstRow("AllScheduledHours") Else m_AllScheduledHours = 0
            ''

            If m_AttemptedCredits <> 0 Then
                m_PercentageEarnedCredits = Math.Round(m_EarnedCredits / m_AttemptedCredits * 100, 2)
            Else
                m_PercentageEarnedCredits = 0
            End If
        Else
            m_GPA = 0
            m_AttemptedCredits = 0
            m_AllAttemptedCredits = 0
            m_EarnedCredits = 0
            m_EarnedFinAidCredits = 0
            m_CompletedHours = 0
            m_RemainingCredits = 0
            m_RemainingHours = 0
            m_TotalClasses = 0
            m_PercentageEarnedCredits = 0
            m_GradeInGPA = False

        End If
    End Sub

#End Region

#Region "Public Methods"

    Public Function GetGraduateAuditByEnrollment(ByVal stuEnrollId As String, ByVal gradeReps As String, ByVal includeHours As Boolean, Optional ByVal campusID As String = "") As DataTable   'DataSet 
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim ds As DataSet

        Dim myPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        If myPortalAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric" Then
            ds = dbTranscript.GetGraduateAuditByEnrollmentForNumeric(stuEnrollId)
        ElseIf dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
            ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
        Else
            ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
        End If

        dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, "1/1/1001", "", "", campusID)
        dtResults = ds.Tables("Results")

        SetProperties()

        Return dtGraduateAudit

        'Dim ds2 As New DataSet
        'ds2.Tables.Add(dtGraduateAudit.Copy)
        'Return ds2
    End Function

    Public Function GetGraduateAuditByEnrollmentForCourseEquivalent(ByVal stuEnrollId As String, ByVal gradeReps As String, ByVal includeHours As Boolean, Optional ByVal campusID As String = "") As List(Of GraduateAuditGroupDto)  'DataTable 
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim ds As DataSet

        Dim strTranscriptType As String = "traditional"

        If strTranscriptType = "traditional_numeric" Then
            ds = dbTranscript.GetGraduateAuditByEnrollmentForNumericForCourseEquivalent(stuEnrollId)
        ElseIf dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
            ds = dbTranscript.GetGraduateAuditByCEProgEnrollmentForCourseEquivalent(stuEnrollId)
        Else
            ds = dbTranscript.GetGraduateAuditByEnrollmentForCourseEquivalent(stuEnrollId)
        End If

        dtGraduateAudit = BuildGraduateAuditDT(ds, gradeReps, includeHours)
        dtResults = ds.Tables("Results")

        SetProperties()

        Dim result As New List(Of GraduateAuditGroupDto)

        For Each dr As DataRow In dtGraduateAudit.Rows
            Dim dto = New GraduateAuditGroupDto()
            dto.PrgVerId = If(IsDBNull(dr("PrgVerId")), String.Empty, dr("PrgVerId").ToString())
            dto.Code = dr("Code").ToString()
            dto.Credits = dr("Credits")
            dto.Descrip = dr("Descrip").ToString()
            dto.IsCreditsEarned = If(IsDBNull(dr("IsCreditsEarned")), False, dr("IsCreditsEarned"))
            dto.Level = If(IsDBNull(dr("Level")), Nothing, dr("Level"))
            'dto.ParentId = If(IsDBNull(dr("ParentId")), String.Empty, dr("ParentId"))
            dto.ReqId = If(IsDBNull(dr("ReqID")), String.Empty, dr("ReqID").ToString())
            dto.ReqTypeId = If(IsDBNull(dr("ReqTypeId")), Nothing, Convert.ToInt32(dr("ReqTypeId")))
            dto.StartDate = If(IsDBNull(dr("StartDate")), Nothing, dr("StartDate"))
            dto.CreditsEarned = If(IsDBNull(dr("CreditsEarned")), Nothing, dr("CreditsEarned"))
            dto.CreditsRemaining = If(IsDBNull(dr("CreditsRemaining")), Nothing, dr("CreditsRemaining"))
            dto.EquivCourseDesc = If(IsDBNull(dr("EquivCourseDesc")), String.Empty, dr("EquivCourseDesc"))
            dto.Grade = If(IsDBNull(dr("Grade")), String.Empty, dr("Grade"))
            dto.Score = If(IsDBNull(dr("Score")), Nothing, dr("Score"))
            dto.Hours = If(IsDBNull(dr("Hours")), 0, Convert.ToDecimal(dr("Hours")))
            dto.HoursCompleted = If(IsDBNull(dr("HoursCompleted")), Nothing, Convert.ToDecimal(dr("HoursCompleted")))
            dto.HoursRemaining = If(IsDBNull(dr("HoursRemaining")), Nothing, Convert.ToDecimal(dr("HoursRemaining")))
            dto.GrdSysDetailId = If(IsDBNull(dr("GrdSysDetailId")), String.Empty, dr("GrdSysDetailId"))
            dto.IsContinuingEd = If(IsDBNull(dr("IsContinuingEd")), False, dr("IsContinuingEd"))
            dto.IsTransferGrade = If(IsDBNull(dr("IsTransferGrade")), False, dr("IsTransferGrade"))
            result.Add(dto)
        Next

        'Return dtGraduateAudit
        Return result
        'Dim ds2 As New DataSet
        'ds2.Tables.Add(dtGraduateAudit.Copy)
        'Return ds2
    End Function

    Public Function GetResultsByEnrollment(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal campusID As String = "") As DataTable
        ' New Optional Parameter add termCond and classCond by Vijay Ramteke on May, 06 2009
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim ds As DataSet

        Dim myPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        Dim transcriptType As String = String.Empty
        If Not myPortalAdvAppSettings.AppSettings("TranscriptType", campusID) Is Nothing Then
            transcriptType = myPortalAdvAppSettings.AppSettings("TranscriptType", campusID)
        End If
        If stuEnrollId <> "" Then

            If transcriptType.ToLower = "traditional_numeric" Then
                'Code Commented By Vijay Ramteke on May, 06 2009
                'ds = dbTranscript.GetGraduateAuditByEnrollmentForNumeric(stuEnrollId)
                'Code Commented By Vijay Ramteke on May, 06 2009
                'Code Added By Vijay Ramteke on May, 06 2009
                ds = dbTranscript.GetGraduateAuditByEnrollmentForNumeric(stuEnrollId, , termCond, classCond)
                'Code Added By Vijay Ramteke on May, 06 2009
            ElseIf dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                'Code Commented By Vijay Ramteke on May, 06 2009
                'ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
                'Code Commented By Vijay Ramteke on May, 06 2009
                'Code Added By Vijay Ramteke on May, 06 2009
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId, , termCond, classCond)
                'Code Added By Vijay Ramteke on May, 06 2009
            Else
                'Code Commented By Vijay Ramteke on May, 06 2009
                'ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
                'Code Commented By Vijay Ramteke on May, 06 2009
                'Code Added By Vijay Ramteke on May, 06 2009
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId, , termCond, classCond, campusID)
                'Code Added By Vijay Ramteke on May, 06 2009

            End If
            'Code Commented By Vijay Ramteke on May, 06 2009
            'dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours)
            'Code Commented By Vijay Ramteke on May, 06 2009
            'Code Added By Vijay Ramteke on May, 06 2009
            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , termCond, classCond, campusID)
            'Code Added By Vijay Ramteke on May, 06 2009

            dtResults = ds.Tables("Results")
            '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
            If transcriptType.ToLower = "traditional_numeric" Then
                GetAverageScore()
            Else
                GetAverageGpa()
            End If

            ''''''
            SetProperties()
        End If

        Return dtResults
    End Function
    '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
    Private Sub GetAverageScore()
        Dim dcSa As New DataColumn
        dcSa.DataType = Type.GetType("System.Decimal")
        dcSa.ColumnName = "AvgScoreNew"
        Dim dcGa As New DataColumn
        dcGa.DataType = Type.GetType("System.Decimal")
        dcGa.ColumnName = "AvgGPANew"
        Try


            Dim dr() As DataRow
            dr = dtGraduateAudit.Select("Score is Not null")

            Dim scoreAvg As Decimal = 0.0
            'Dim GPAAvg As Decimal = 0.0
            Dim i As Integer = 0
            For Each dr1 In dr
                i += 1
                scoreAvg += DirectCast(dr1("Score"), Decimal)
            Next

            dcSa.DefaultValue = If(i > 0, (scoreAvg / i), 0)

            dcGa.DefaultValue = 0
            dtResults.Columns.Add(dcSa)
            dtResults.Columns.Add(dcGa)

        Catch ex As Exception
            dcSa.DefaultValue = 0
            dcGa.DefaultValue = 0
            dtResults.Columns.Add(dcSa)
            dtResults.Columns.Add(dcGa)
        End Try


    End Sub
    '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
    Private Sub GetAverageGpa()
        Dim dcSa As New DataColumn
        dcSa.DataType = Type.GetType("System.Decimal")
        dcSa.ColumnName = "AvgScoreNew"
        Dim dcGa As New DataColumn
        dcGa.DataType = Type.GetType("System.Decimal")
        dcGa.ColumnName = "AvgGPANew"
        ' Try


        Dim dr() As DataRow
        dr = dtGraduateAudit.Select("GPA is Not null")

        'Dim ScoreAvg As Decimal = 0.0
        Dim gpaAvg As Decimal = 0.0
        Dim i As Integer = 0
        For Each dr1 In dr
            i += 1
            gpaAvg += DirectCast(dr1("GPA"), Decimal)
        Next
        dcSa.DefaultValue = 0
        dcGa.DefaultValue = If(i = 0, 0.0, gpaAvg / i)
        'dcGA.DefaultValue = GPAAvg / i

        dtResults.Columns.Add(dcSa)
        dtResults.Columns.Add(dcGa)

    End Sub

    Public Function HasStudentMetAllRequirements(campusid As String, Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal gradesFormat As String = "Letter", Optional ByRef reason As String = "") As Boolean
        Dim result As Boolean
        Dim dbreq As New RequirementsDB

        result = False

        If gradesFormat.ToUpper = "LETTER" Then
            result = HasStudentMetAllRequirementsForLetterGrades(campusid, stuEnrollId, gradeReps, includeHours, reason)
        ElseIf gradesFormat.ToUpper = "NUMERIC" Then
            result = HasStudentMetAllRequirementsForNumericGrades(campusid, stuEnrollId, gradeReps, includeHours, reason)
        End If
        ''Added by Saraswathi Lakshmanan to find if the student has met all the graduation Requirements
        If result = True Then
            result = HasStudentMetAlltheGraduationRequirements(stuEnrollId, reason)
        End If

        Return result

    End Function

    ''Added by Saraswathi lakshmanan on Sept 30  2010
    ''For Document tracking to find if the student has met all the graduation requirements
    'Public Function HasStudentMetAlltheGraduationRequirements(ByVal stuEnrollId As String, Optional ByRef reason As String = "") As Boolean
    '    Dim strEnrollStatus As String = ""

    '    Dim strTestStatus As String = CheckIfRequiredTest_ForGraduation(stuEnrollId)
    '    Dim strDocStatus As String = CheckRequiredDocuments_ForGraduation(stuEnrollId)
    '    Dim strReqsMetStatus As String = (New AdReqsFacade).CheckIfReqGroupMeetsConditions_Graduation_SP(stuEnrollId)

    '    Dim strTestStatusEnrl As String = CheckIfRequiredTest_ForEnrollment(stuEnrollId)
    '    ''   Dim strDocStatusEnrl As String = CheckRequiredDocuments_ForEnrollment(stuEnrollId)
    '    Dim strDocStatusEnrl As String = CheckandListRequiredDocuments_ForEnrollment(stuEnrollId)
    '    Dim strReqsMetStatusEnrl As String = (New AdReqsFacade).CheckIfReqGroupMeetsConditions_Enrollment_SP(stuEnrollId)

    '    If strTestStatus <> "Eligible" Then
    '        reason = reason + "Student hasn't satisfied the graduation test requirement;"
    '    End If
    '    If strDocStatus <> "Eligible" Then
    '        reason = reason + "Student hasn't satisfied the graduation document requirement;"
    '    End If
    '    If strReqsMetStatus <> "Eligible" Then
    '        reason = reason + "Student hasn't satisfied the requirement groups requirement for graduation;"
    '    End If
    '    If strTestStatusEnrl <> "Eligible" Then
    '        reason = reason + "Student hasn't satisfied the enrollment test requirement;"
    '    End If
    '    If strDocStatusEnrl = "" Then
    '        reason = reason + strDocStatusEnrl
    '    End If
    '    If strReqsMetStatusEnrl <> "Eligible" Then
    '        reason = reason + "Student hasn't satisfied the requirement groups requirement for enrollment;"
    '    End If

    '    If strTestStatus = "Eligible" And strDocStatus = "Eligible" And strReqsMetStatus = "Eligible" And strTestStatusEnrl = "Eligible" And strDocStatusEnrl = "Eligible" And strReqsMetStatusEnrl = "Eligible" Then
    '        strEnrollStatus = "Eligible"
    '    Else
    '        strEnrollStatus = "Not Eligible"
    '    End If
    '    Return IIf(strEnrollStatus = "Eligible", True, False)

    'End Function


    Public Function HasStudentMetAlltheGraduationRequirements(ByVal stuEnrollId As String, Optional ByRef reason As String = "") As Boolean
        Dim strEnrollStatus As String
        Dim errmessage As String
        Dim strTestStatus As String = CheckandListRequiredtest_ForGraduation(stuEnrollId)
        Dim strDocStatus As String = CheckandListRequiredDocuments_ForGraduation(stuEnrollId)
        Dim strReqsMetStatus As String = CheckandListGradreqGroups(stuEnrollId)

        Dim strTestStatusEnrl As String = CheckandListRequiredTest_ForEnrollment(stuEnrollId)
        ''   Dim strDocStatusEnrl As String = CheckRequiredDocuments_ForEnrollment(stuEnrollId)
        Dim strDocStatusEnrl As String = CheckandListRequiredDocuments_ForEnrollment(stuEnrollId)
        Dim strReqsMetStatusEnrl As String = CheckandListEnrollreqGroups(stuEnrollId)

        'If strTestStatus <> "" Then
        '    reason = reason + strTestStatus
        'End If
        'If strDocStatus <> "" Then
        '    reason = reason + strDocStatus
        'End If
        'If strReqsMetStatus <> "" Then
        '    reason = reason + strReqsMetStatus
        'End If
        'If strTestStatusEnrl <> "" Then
        '    reason = reason + strTestStatusEnrl
        'End If
        'If strDocStatusEnrl <> "" Then
        '    reason = reason + strDocStatusEnrl
        'End If
        'If strReqsMetStatusEnrl <> "" Then
        '    reason = reason + strReqsMetStatusEnrl
        'End If

        errmessage = strReqsMetStatusEnrl + strTestStatusEnrl + strDocStatusEnrl + strReqsMetStatus + strTestStatus + strDocStatus
        If strTestStatus = "" And strDocStatus = "" And strReqsMetStatus = "" And strTestStatusEnrl = "" And strDocStatusEnrl = "" And strReqsMetStatusEnrl = "" Then
            strEnrollStatus = "Eligible"
        Else
            reason = "The student hasn't met the following requirements:; " + vbCrLf + errmessage
            strEnrollStatus = "Not Eligible"
        End If
        Return IIf(strEnrollStatus = "Eligible", True, False)

    End Function

    ''Added by Saraswathi lakshmanan on Sept 22 2010 For document tracking
    ''This function helps to find if the required test dcuments are passed by the student
    ''For use in post payments
    Public Function CheckIfRequiredTest_ForGraduation(ByVal StuEnrollId As String) As String
        Dim intStudentHasTaken As Integer
        Dim StudentFacade As New StudentsAccountsFacade
        intStudentHasTaken = StudentFacade.CheckIfStudentHasPassedRequiredTestWithProgramVersion_Graduation(StuEnrollId)

        If intStudentHasTaken >= 1 Then
            Return "Not Eligible"
        Else
            Return "Eligible"
        End If

    End Function

    Public Function CheckIfRequiredTest_ForEnrollment(ByVal StuEnrollId As String) As String
        Dim intStudentHasTaken As Integer
        Dim StudentFacade As New StudentsAccountsFacade
        intStudentHasTaken = StudentFacade.CheckIfStudentHasPassedRequiredTestWithProgramVersion_Enrollment(StuEnrollId)

        If intStudentHasTaken >= 1 Then
            Return "Not Eligible"
        Else
            Return "Eligible"
        End If

    End Function
    Public Function CheckRequiredDocuments_ForGraduation(ByVal StuEnrollID As String) As String
        Dim intStudentHasTaken As Integer
        Dim StudentFacade As New StudentsAccountsFacade

        intStudentHasTaken = StudentFacade.CheckAllApprovedDocumentsWithPrgVersion_Graduation(StuEnrollID)

        If intStudentHasTaken >= 1 Then
            Return "Not Eligible"
        Else
            Return "Eligible"
        End If
    End Function
    Public Function CheckRequiredDocuments_ForEnrollment(ByVal StuEnrollID As String) As String
        Dim intStudentHasTaken As Integer
        Dim studentFacade As New StudentsAccountsFacade

        intStudentHasTaken = studentFacade.CheckAllApprovedDocumentsWithPrgVersion_Enrollment(StuEnrollID)

        If intStudentHasTaken >= 1 Then
            Return "Not Eligible"
        Else
            Return "Eligible"
        End If
    End Function

    Public Function CheckandListRequiredDocuments_ForEnrollment(ByVal StuEnrollID As String) As String
        Dim ds As DataSet
        Dim Studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = Studentdb.CheckandListEnrollmentDocumentRequirements_Sp(StuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    strdocs.Append(row("Descrip") + ";")
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If

        'If intStudentHasTaken >= 1 Then
        '    Return "Not Eligible"
        'Else
        '    Return "Eligible"
        'End If
    End Function
    Public Function HasStudentMetAllRequirementsForLetterGrades(campusid As String, Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByRef reason As String = "") As Boolean
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim ds As New DataSet

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId, , , , campusid)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusid)
            dtResults = ds.Tables("Results")
            'Else
            '    If dtGraduateAudit.Rows.Count = 0 Then
            '        Dim ds As DataSet = (New MyPortalTranscriptDB).GetGraduateAuditByEnrollment(stuEnrollId)
            '        dtGraduateAudit = BuildGraduateAuditDT(ds)
            '    End If

            SetProperties()
        End If

        Dim result As Boolean
        ''Added by saraswathi Lakshmanan on May 06 2009
        ''To get the transfer grades, with credits earrned not checked.
        Dim dsgradesysdetailids As DataSet
        dsgradesysdetailids = dbTranscript.GetGradesSysDetailId()
        'Dim arraylistGradeSysIds As New ArrayList
        'Dim i As Integer
        'For i = 0 To dsgradesysdetailids.Tables(0).Rows.Count - 1
        '    arraylistGradeSysIds.Add(ds.Tables(0).Rows(i)(0).ToString)
        'Next i
        Dim objCredits As Object
        ''  Dim ObjHours As Object

        If dsgradesysdetailids.Tables(0).Rows.Count > 0 Then

            Dim filtercond As String = ""
            If dsgradesysdetailids.Tables(0).Rows.Count > 1 Then
                For i = 0 To dsgradesysdetailids.Tables(0).Rows.Count - 1
                    filtercond = filtercond + " GrdSysDetailId='" + dsgradesysdetailids.Tables(0).Rows(i)(0).ToString + "'  OR"
                Next
                filtercond = filtercond.Remove(filtercond.Length - 2, 2)
            Else
                filtercond = " GrdSysDetailId='" + dsgradesysdetailids.Tables(0).Rows(0)(0).ToString + "' "
            End If
            objCredits = dtGraduateAudit.Compute("SUM(Credits)", filtercond)
            ''   ObjHours = dtGraduateAudit.Compute("SUM(Hours)", filtercond)

        End If




        If dtGraduateAudit.Rows.Count > 0 Then
            Dim firstRow As DataRow
            firstRow = dtGraduateAudit.Rows(0)

            If firstRow("IsContinuingEd") Then
                'Comment line below if want to prevent from graduating CE students if she/he has scheduled courses
                result = True

                'Uncomment code below if want to prevent from graduating CE students if she/he has scheduled courses
                Dim rows() As DataRow = dtGraduateAudit.Select("CreditsRemaining > 0 OR HoursRemaining > 0")
                If rows.GetLength(0) = 0 Then
                    If (firstRow("IsPass") = False) Then
                        result = False
                        reason = reason + "Failed the course;"
                    Else
                        If MyPortalAdvAppSettings.AppSettings("CheckCreditsForContiningEd").ToLower = "yes" Then
                            Dim totalCredits As Decimal = dbTranscript.getCredits(firstRow("PrgVerId"))
                            If firstRow("CreditsEarned") < totalCredits Then
                                result = False
                                reason = reason + "Credits are not satisfied;"
                            Else
                                result = True
                            End If
                        Else
                            result = True
                        End If

                    End If

                Else
                    result = False
                    reason = reason + "Credits and Hours are remaining;"
                End If

            Else
                ''Added by saraswathi lakshmanan on may 06 2009
                ''if it is a transferGrade, and Credits earned is not checked. Allow the student to graduate, If the student is transfered from a different campus, the student will not earn the credit. the credits will be awarded to them.
                ''For mantis case 15845 and 11918
                If dsgradesysdetailids.Tables(0).Rows.Count > 0 Then
                    If Not IsDBNull(objCredits) Then
                        If (firstRow("CreditsRemaining") = Convert.ToDecimal(objCredits)) Then
                            result = True
                        Else
                            result = (firstRow("CreditsRemaining") <= 0 And firstRow("HoursRemaining") <= 0)
                            If result = False Then
                                reason = reason + "Credits or Hours are remaining;"
                            End If
                        End If
                    Else
                        result = (firstRow("CreditsRemaining") <= 0 And firstRow("HoursRemaining") <= 0)
                        If result = False Then
                            reason = reason + "Credits or Hours are remaining;"
                        End If
                    End If
                Else
                    result = (firstRow("CreditsRemaining") <= 0 And firstRow("HoursRemaining") <= 0)
                    If result = False Then
                        reason = reason + "Credits or Hours are remaining;"
                    End If
                End If
            End If
        Else
            'added by Theresa G on 5/12/2009
            'now we added the config entry CheckCreditsForContiningEd to treat it a regular course or continuing ed
            'if set to yes,it has to treat like a regular program version and the student has to fullfill the requirements

            If MyPortalAdvAppSettings.AppSettings("CheckCreditsForContiningEd").ToLower = "yes" Then
                result = False
            Else
                'There is no rows on the Graduate Audit table => no Program Version Definition
                'Continuing Education?
                'Assume all requirements have been met
                result = True
            End If


        End If
        Dim IsClockHourProgram As Boolean = False
        IsClockHourProgram = (New StuEnrollFacade).IsStudentProgramClockHourType(stuEnrollId)
        If MyPortalAdvAppSettings.AppSettings("TrackSapAttendance", campusid).ToLower = "byday" Then
            Dim gradHrsResult As Boolean = True

            If IsClockHourProgram = True Then
                gradHrsResult = (New StuEnrollDB).HasStudentMetGraduationHoursRequirement(stuEnrollId)
                If gradHrsResult = False Then
                    reason = reason + "Student has not satisfied the graduation hours requirements;"
                End If
            End If

            If result = True And gradHrsResult = True Then
                Return True
            Else

                Return False

            End If
        ElseIf MyPortalAdvAppSettings.AppSettings("TimeClockClassSection", campusid).ToLower = "yes" Then
            Dim gradHrsResult As Boolean = True
            If IsClockHourProgram = True Then
                gradHrsResult = (New StuEnrollDB).HasStudentMetGraduationHoursRequirementByClass(stuEnrollId, campusid)
            End If
            If result = True And gradHrsResult = True Then
                Return True
            Else
                reason = reason + "Student has not satisfied the graduation hours requirements;"
                Return False

            End If
        End If

        Return result




    End Function

    Public Function HasStudentMetAllRequirementsForNumericGrades(campusId As String, ByVal stuEnrollId As String, ByVal gradeReps As String, ByVal includeHours As Boolean, Optional ByRef reason As String = "") As Boolean
        Dim result As Boolean
        Dim stuPR As New StuProgressReportDB
        Dim dtWU As New DataTable
        Dim dtMCR As New DataTable
        Dim facPRO As New StuProgressReportObject

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        result = False

        'Get the WorkUnitResults info
        dtWU = (stuPR.GetWorkUnitResults(Nothing, stuEnrollId).Copy)

        'Get the ModuleCoursesResults info
        dtMCR = stuPR.GetModulesCoursesResults(Nothing, stuEnrollId).Copy

        'Update dtMCR to include the Completed and Credits Earned for each course
        facPRO.UpdateCompletedAndCreditsEarned(dtMCR, dtWU, "", Nothing)

        Dim rows() As DataRow = dtMCR.Select("Completed = false")

        If rows.Length = 0 And dtMCR.Rows.Count > 0 Then
            result = True
        End If
        If result = False Then
            reason = reason + "Student has failed the credit requirements;"
        End If
        Dim IsClockHourProgram As Boolean = False
        IsClockHourProgram = (New StuEnrollFacade).IsStudentProgramClockHourType(stuEnrollId)
        If MyPortalAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToLower = "byday" Then
            Dim gradHrsResult As Boolean = True

            If IsClockHourProgram = True Then
                gradHrsResult = (New StuEnrollDB).HasStudentMetGraduationHoursRequirement(stuEnrollId)
                If gradHrsResult = False Then
                    reason = reason + "Student has not satisfied the graduation hours requirements;"
                End If
            End If

            If result = True And gradHrsResult = True Then
                Return True
            Else
                Return False
            End If
        ElseIf MyPortalAdvAppSettings.AppSettings("TimeClockClassSection", campusId).ToLower = "yes" Then
            Dim gradHrsResult As Boolean = True
            If IsClockHourProgram = True Then
                gradHrsResult = (New StuEnrollDB).HasStudentMetGraduationHoursRequirementByClass(stuEnrollId, campusId)
            End If
            If result = True And gradHrsResult = True Then
                Return True
            Else
                If gradHrsResult = False Then
                    reason = reason + "Student has not satisfied the graduation hours requirements;"
                End If
                '  reason = reason + "Student has not satisfied the graduation hours requirements;"
                Return False

            End If
        End If
        Return result

    End Function



    Public Function HasStudentMetRequirement(ByVal reqId As String, Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As Boolean
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim ds As DataSet

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusId)
            dtResults = ds.Tables("Results")
            'Else
            '    If dtGraduateAudit.Rows.Count = 0 Then
            '        Dim ds As DataSet = (New MyPortalTranscriptDB).GetGraduateAuditByEnrollment(stuEnrollId)
            '        dtGraduateAudit = BuildGraduateAuditDT(ds)
            '    End If

            SetProperties()
        End If

        Dim result As Boolean

        If dtGraduateAudit.Rows.Count > 0 Then
            Dim rows() As DataRow
            rows = dtGraduateAudit.Select("ReqId='" & reqId & "'")

            If rows.GetLength(0) > 0 Then
                Dim firstRow As DataRow
                firstRow = dtGraduateAudit.Rows(0)
                result = (firstRow("CreditsRemaining") <= 0 And firstRow("HoursRemaining") <= 0)
            End If
        End If

        Return result
    End Function

    Public Function GetTotalClasses(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As Integer
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim ds As New DataSet

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusId)
            dtResults = ds.Tables("Results")
            'Else
            '    If dtGraduateAudit.Rows.Count = 0 Then
            '        Dim ds As DataSet = (New MyPortalTranscriptDB).GetGraduateAuditByEnrollment(stuEnrollId)
            '        dtGraduateAudit = BuildGraduateAuditDT(ds)
            '    End If

            SetProperties()
        End If

        'Return dtResults.Rows.Count

        Return TotalClasses
    End Function

    Public Function GetCreditsEarned(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As Decimal
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim ds As New DataSet

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusId)
            dtResults = ds.Tables("Results")
            'Else
            '    If dtGraduateAudit.Rows.Count = 0 Then
            '        Dim ds As DataSet = (New MyPortalTranscriptDB).GetGraduateAuditByEnrollment(stuEnrollId)
            '        dtGraduateAudit = BuildGraduateAuditDT(ds)
            '    End If

            SetProperties()
        End If

        'Dim result As Decimal
        'If dtGraduateAudit.Rows.Count > 0 Then
        '    Dim firstRow As DataRow
        '    firstRow = dtGraduateAudit.Rows(0)

        '    result = firstRow("CreditsEarned")
        'End If
        'Return result

        Return EarnedCredits
    End Function

    Public Function GetCreditsAttempted(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As Decimal
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim ds As New DataSet

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusId)
            dtResults = ds.Tables("Results")
            'Else
            '    If dtGraduateAudit.Rows.Count = 0 Then
            '        Dim ds As DataSet = (New MyPortalTranscriptDB).GetGraduateAuditByEnrollment(stuEnrollId)
            '        dtGraduateAudit = BuildGraduateAuditDT(ds)
            '    End If

            SetProperties()
        End If

        'Dim result As Decimal
        'If dtGraduateAudit.Rows.Count > 0 Then
        '    Dim firstRow As DataRow
        '    firstRow = dtGraduateAudit.Rows(0)

        '    result = firstRow("CreditsAttempted")
        'End If
        'Return result

        Return AttemptedCredits
    End Function

    Public Function GetGPA(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal termEndDate As DateTime = #1/1/1001#, Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As Decimal
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim ds As New DataSet

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId, termEndDate, , , campusId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId, termEndDate, , , campusId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, termEndDate, , , campusId)
            dtResults = ds.Tables("Results")
            'Else
            '    If dtGraduateAudit.Rows.Count = 0 Then
            '        Dim ds As DataSet = (New MyPortalTranscriptDB).GetGraduateAuditByEnrollment(stuEnrollId)
            '        dtGraduateAudit = BuildGraduateAuditDT(ds)
            '    End If

            SetProperties()
        End If

        'Dim result As Decimal
        'If dtGraduateAudit.Rows.Count > 0 Then
        '    Dim firstRow As DataRow
        '    firstRow = dtGraduateAudit.Rows(0)

        '    result = firstRow("GPA")
        'End If
        'Return result

        Return GPA
    End Function

    Public Function GetHoursCompleted(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As Decimal
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim ds As New DataSet

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusId)
            dtResults = ds.Tables("Results")
            'Else
            '    If dtGraduateAudit.Rows.Count = 0 Then
            '        Dim ds As DataSet = (New MyPortalTranscriptDB).GetGraduateAuditByEnrollment(stuEnrollId)
            '        dtGraduateAudit = BuildGraduateAuditDT(ds)
            '    End If

            SetProperties()
        End If

        'Dim result As Decimal
        'If dtGraduateAudit.Rows.Count > 0 Then
        '    Dim firstRow As DataRow
        '    firstRow = dtGraduateAudit.Rows(0)

        '    result = firstRow("HoursCompleted")
        'End If
        'Return result

        Return CompletedHours
    End Function

    Public Function GetTermProgressByEnrollment(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal campusId As String = "") As DataTable 'DataSet 
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim ds As New DataSet

        If stuEnrollId <> "" Then

            If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
            Else
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
            End If

            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , , , campusId)
            dtResults = ds.Tables("Results")
            'Else
            '    If dtGraduateAudit.Rows.Count = 0 Then
            '        Dim ds As DataSet = (New MyPortalTranscriptDB).GetGraduateAuditByEnrollment(stuEnrollId)
            '        dtGraduateAudit = BuildGraduateAuditDT(ds)
            '    End If

            SetProperties()
        End If
        '
        '
        '
        Dim termCtr As Integer = 1
        Dim arrRows() As DataRow
        Dim dr As DataRow
        Dim dtGradAudit As DataTable = dtGraduateAudit.Copy
        Dim dtTermProgress As New DataTable

        '   structure of the TermProgress table
        With dtTermProgress
            .Columns.Add("TermId", Type.GetType("System.Guid"))
            .Columns.Add("TermDescrip", Type.GetType("System.String"))
            .Columns.Add("StartDate", Type.GetType("System.DateTime"))
            .Columns.Add("EndDate", Type.GetType("System.DateTime"))
            .Columns.Add("TakenCourses", Type.GetType("System.Int32"))
            .Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
            .Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
            .Columns.Add("TermGPA", Type.GetType("System.Decimal"))
            .Columns.Add("GPA", Type.GetType("System.Decimal"))
        End With
        '
        '
        '   if the GradAudit exit function and return an empty TermProgress table
        If dtGradAudit.Rows.Count = 0 Then
            Return dtTermProgress
        End If
        '
        '
        '
        '********************************************************************************
        '**** Massage GraduateAudit and Results table to produce TermProgress table. ****
        '********************************************************************************
        '
        '   add DISTINCT Terms into TermProgress table
        If dtGradAudit.Rows.Count > 0 And dtResults.Rows.Count > 0 Then

            For Each dr In dtResults.Rows
                If Not dr.IsNull("TermId") Then
                    If Not IsValueAlreadyInTable(dr("TermId").ToString, "TermId", dtTermProgress) Then
                        '   add row into TermProgress table
                        Dim row As DataRow
                        row = dtTermProgress.NewRow
                        row("TermId") = dr("TermId")
                        row("TermDescrip") = dr("TermDescrip")
                        row("StartDate") = dr("StartDate")
                        row("EndDate") = dr("EndDate")
                        dtTermProgress.Rows.Add(row)
                        '
                        '   add columns corresponding to distinct Terms into temporary copy of GraduateAudit table
                        dtGradAudit.Columns.Add("EarnedCR" & termCtr, Type.GetType("System.Decimal"))
                        dtGradAudit.Columns.Add("AttemptedCR" & termCtr, Type.GetType("System.Decimal"))
                        termCtr += 1
                    End If
                End If
            Next
        End If
        '
        '
        '   CreditsEarned and CreditsAttempted for each course on every particular term
        '   TermGPA and Cummulative GPA for every term
        termCtr = 0
        Dim cummTermGPA As Decimal
        'Dim cummGPA As Decimal
        'Dim courseCtr As Integer
        Dim termCourseCtr As Integer
        Dim oneTerm As String
        Dim cummTerms As New StringBuilder

        For Each dr In dtTermProgress.Rows
            termCtr += 1
            cummTermGPA = 0
            termCourseCtr = 0
            arrRows = dtResults.Select("TermId='" & dr("TermId").ToString & "'")

            If arrRows.GetLength(0) > 0 Then

                For Each resultDR As DataRow In arrRows
                    Dim reqRow() As DataRow = dtGradAudit.Select("ReqId='" & resultDR("ReqId").ToString & "'")

                    If reqRow.GetLength(0) > 0 Then
                        Dim gradAuditDR As DataRow = reqRow(0)
                        If resultDR("IsCreditsEarned") Then gradAuditDR("EarnedCR" & termCtr.ToString) = resultDR("Credits")
                        If resultDR("IsCreditsAttempted") Then gradAuditDR("AttemptedCR" & termCtr.ToString) = resultDR("Credits")
                        'If resultDR("IsInGPA") Then
                        '    cummTermGPA += resultDR("GPA")
                        '    termCourseCtr += 1
                        'End If
                    End If
                Next

                dr("TakenCourses") = arrRows.GetLength(0)

                ''   simple average of the course grades of a particular term
                'If termCourseCtr > 0 Then
                '    dr("TermGPA") = System.Math.Round(cummTermGPA / termCourseCtr, 2)
                'Else
                '    dr("TermGPA") = 0
                'End If

                ''   cummulative GPA as of current term
                'cummGPA += cummTermGPA
                'courseCtr += termCourseCtr
                'If courseCtr > 0 Then
                '    dr("GPA") = System.Math.Round(cummGPA / courseCtr, 2)
                'Else
                '    dr("GPA") = 0
                'End If

                '   Compute TermGPA and cummulative GPA
                '   Select rows from dtGradAudit for current term and compute term GPA.
                oneTerm = "(EarnedCR" & termCtr.ToString & " IS NOT NULL AND AttemptedCR" & termCtr.ToString & " IS NOT NULL)"
                arrRows = dtGradAudit.Select(oneTerm)
                dr("TermGPA") = ComputeGPA(arrRows)

                '   Compute cummulative GPA
                '   Select rows from dtGradAudit for current and previous terms and compute cummulative GPA.
                cummTerms.Append(oneTerm)
                arrRows = dtGradAudit.Select(cummTerms.ToString)
                dr("GPA") = ComputeGPA(arrRows)

                cummTerms.Append(" OR ")

            End If
        Next
        '
        '
        '   Nested Course Groups
        '   Credits Earned and Credits Attempted for Course Groups on every particular term
        Dim maxLevel As Integer = dtGradAudit.Compute("MAX(Level)", "")
        Dim temp1 As Decimal
        Dim temp2 As Decimal

        '   find out if there are Course Groups in the Program Version definition
        Dim arrReqGrps() As DataRow = dtGradAudit.Select("ReqTypeId=2")

        If arrReqGrps.GetLength(0) > 0 Then
            termCtr = 0

            For Each row As DataRow In dtTermProgress.Rows
                termCtr += 1

                '   iterate (decrementing) through levels of nested Course Groups
                Do While maxLevel > 0

                    For Each dr In arrReqGrps

                        '   find out if there are children of level = maxLevel for current Course Group
                        arrRows = dtGradAudit.Select("ParentId='" & dr("ReqId").ToString & "' AND Level=" & maxLevel & " AND (IsCreditsEarned = 1 OR ReqTypeId = 2)")

                        If arrRows.GetLength(0) > 0 Then

                            For Each gradAuditDR As DataRow In arrRows
                                If Not gradAuditDR.IsNull("EarnedCR" & termCtr.ToString) Then
                                    temp1 += gradAuditDR("EarnedCR" & termCtr.ToString)
                                End If
                                If Not gradAuditDR.IsNull("AttemptedCR" & termCtr.ToString) Then
                                    temp2 += gradAuditDR("AttemptedCR" & termCtr.ToString)
                                End If
                            Next

                            '   update CreditsEarned for the current Course Group in the particular term
                            If temp1 < dr("CreditsEarned") Then
                                dr("EarnedCR" & termCtr.ToString) = temp1
                            Else
                                '   CreditsEarned capping
                                dr("EarnedCR" & termCtr.ToString) = dr("CreditsEarned")
                            End If

                            '   update CreditsAttempted for the current Course Group in the particular term
                            If temp2 < dr("CreditsAttempted") Then
                                dr("AttemptedCR" & termCtr.ToString) = temp2
                            Else
                                '   CreditsAttempted capping
                                dr("AttemptedCR" & termCtr.ToString) = dr("CreditsAttempted")
                            End If
                        End If

                    Next

                    maxLevel -= 1
                Loop
            Next
        End If
        '
        '
        '   CreditsEarned and CreditsAttempted for every particular term at Level = 0
        termCtr = 0
        Dim cumEarnedCR As Decimal
        Dim cumAttemptedCR As Decimal
        Dim excess As Decimal

        For Each row As DataRow In dtTermProgress.Rows
            termCtr += 1
            arrRows = dtGradAudit.Select("Level=0")

            If arrRows.GetLength(0) > 0 Then
                temp1 = 0
                temp2 = 0

                For Each gradAuditDR As DataRow In arrRows
                    If Not gradAuditDR.IsNull("EarnedCR" & termCtr.ToString) Then
                        temp1 += gradAuditDR("EarnedCR" & termCtr.ToString)
                    End If
                    If Not gradAuditDR.IsNull("AttemptedCR" & termCtr.ToString) Then
                        temp2 += gradAuditDR("AttemptedCR" & termCtr.ToString)
                    End If
                Next

                cumEarnedCR += temp1
                cumAttemptedCR += temp2

                '   update CreditsEarned for particular term
                If cumEarnedCR <= dtGradAudit.Rows(0)("CreditsEarned") Then
                    dtGradAudit.Rows(0)("EarnedCR" & termCtr.ToString) = temp1
                Else
                    '   CreditsEarned capping
                    excess = cumEarnedCR - dtGradAudit.Rows(0)("CreditsEarned")
                    If temp1 > excess Then
                        dtGradAudit.Rows(0)("EarnedCR" & termCtr.ToString) = temp1 - excess
                    Else
                        '   this case should never occur
                        dtGradAudit.Rows(0)("EarnedCR" & termCtr.ToString) = 0
                    End If
                End If

                '   update CreditsAttempted for particular term
                If cumAttemptedCR <= dtGradAudit.Rows(0)("CreditsAttempted") Then
                    dtGradAudit.Rows(0)("AttemptedCR" & termCtr.ToString) = temp2
                Else
                    '   CreditsAttempted capping
                    excess = cumAttemptedCR - dtGradAudit.Rows(0)("CreditsAttempted")
                    If temp1 > excess Then
                        dtGradAudit.Rows(0)("AttemptedCR" & termCtr.ToString) = temp2 - excess
                    Else
                        '   this case should never occur
                        dtGradAudit.Rows(0)("AttemptedCR" & termCtr.ToString) = 0
                    End If
                End If
            End If
        Next
        '
        '
        '   populate TermProgress table with data on first row of dtGradAudit table
        '   first row contains CreditsEarned and CreditsAttempted for every particular term
        termCtr = 0
        For Each row As DataRow In dtTermProgress.Rows
            termCtr += 1
            row("CreditsEarned") = dtGradAudit.Rows(0)("EarnedCR" & termCtr.ToString)
            row("CreditsAttempted") = dtGradAudit.Rows(0)("AttemptedCR" & termCtr.ToString)
        Next

        Return dtTermProgress
        'Dim ds2 As New DataSet
        'ds2.Tables.Add(dtGradAudit)
        'Return ds2
    End Function

    ' Added Newly By Uvarajan
    Public Function GetStudentsByEnrollment(ByVal stuId As String, Optional ByVal strWhere As String = "", Optional ByVal stuEnrollId As String = "") As DataTable
        'Dim dbTranscript As New MyPortalTranscriptDB
        Dim objTrans As New TransactionsDB
        Dim dtSummary As DataTable
        Dim ds2 As New DataSet
        Dim ds As New DataSet


        If stuEnrollId <> "" Then

            'ds = objTrans.GetMultipleStudentLedger(stuId, stuEnrollId, Guid.Empty.ToString, strWhere)
            ds2 = objTrans.GetMultipleStudentLedger(stuId, stuEnrollId, Guid.Empty.ToString, strWhere)

            For Each dr As DataRow In ds2.Tables(0).Rows
                If dr("RecordType") < 0 Then
                    dr.Delete()
                End If
            Next
            ds2.AcceptChanges()
            ds.Tables.Add(ds2.Tables(0).Copy)

            'dtGraduateAudit = BuildGraduateAuditDTStudent(ds)
            'dtResults = ds.Tables("MultipleStudentLedger")

            dtSummary = BuildGraduateAuditDTStudent(ds)


        End If

        'Return dtResults

        Return dtSummary
    End Function

    ' Added By uvarajan

    Private Function BuildGraduateAuditDTStudent(ByVal ds As DataSet)

        Dim dt As New DataTable("MultipleStudentLedger")
        dt = ds.Tables(0)
        dt.TableName = ("MultipleStudentLedger")

        Return dt

    End Function


    Public Function GetTermProgressFromResults(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal campusID As String = "") As DataTable 'DataSet 
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim br As New PortalRegisterStudentsBR
        Dim ds As New DataSet

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        If stuEnrollId <> "" Then
            If MyPortalAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric" Then
                ' Code Commented by vijay Ramteke on May, 06 2009
                'ds = dbTranscript.GetGraduateAuditByEnrollmentForNumeric(stuEnrollId)
                ' Code Commented by vijay Ramteke on May, 06 2009
                ' Code Added by vijay Ramteke on May, 06 2009
                ds = dbTranscript.GetGraduateAuditByEnrollmentForNumeric(stuEnrollId, , termCond, classCond)
                ' Code Added by vijay Ramteke on May, 06 2009
            ElseIf dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                ' Code Commented by vijay Ramteke on May, 06 2009
                'ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId)
                ' Code Commented by vijay Ramteke on May, 06 2009
                ' Code Added by vijay Ramteke on May, 06 2009
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollment(stuEnrollId, , termCond, classCond)
                ' Code Added by vijay Ramteke on May, 06 2009
            Else
                ' Code Commented by vijay Ramteke on May, 06 2009
                'ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId)
                ' Code Commented by vijay Ramteke on May, 06 2009
                ' Code Added by vijay Ramteke on May, 06 2009
                ds = dbTranscript.GetGraduateAuditByEnrollment(stuEnrollId, , termCond, classCond)
                ' Code Added by vijay Ramteke on May, 06 2009
            End If
            ' Code Commented by vijay Ramteke on May, 06 2009
            'dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours)
            ' Code Commented by vijay Ramteke on May, 06 2009
            ' Code Added by vijay Ramteke on May, 06 2009
            dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours, , termCond, classCond, campusID)
            ' Code Added by vijay Ramteke on May, 06 2009
            dtResults = ds.Tables("Results")

            SetProperties()
        End If
        '
        '
        '
        Dim termCtr As Integer = 1
        Dim arrRows() As DataRow
        Dim dr As DataRow
        Dim dtTermProgress As New DataTable
        Dim dtCummProgress As New DataTable
        '
        '   structure of the TermProgress table
        With dtTermProgress
            .Columns.Add("TermId", Type.GetType("System.Guid"))
            .Columns.Add("TermDescrip", Type.GetType("System.String"))
            .Columns.Add("StartDate", Type.GetType("System.DateTime"))
            .Columns.Add("EndDate", Type.GetType("System.DateTime"))
            .Columns.Add("TakenCourses", Type.GetType("System.Int32"))
            .Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
            .Columns.Add("FACreditsEarned", Type.GetType("System.Decimal"))
            .Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
            .Columns.Add("TermGPA", Type.GetType("System.Decimal"))
            .Columns.Add("GPA", Type.GetType("System.Decimal"))
            .Columns.Add("Score", Type.GetType("System.Decimal"))
        End With
        '
        '
        '   if the GradAudit is empty, return an empty TermProgress table
        If dtGraduateAudit.Rows.Count = 0 Then
            Return dtTermProgress
        End If
        '
        '
        '
        '   structure of the CummProgress table
        With dtCummProgress
            .Columns.Add("ReqId", Type.GetType("System.String"))
            .Columns.Add("ReqTypeId", Type.GetType("System.String"))
            .Columns.Add("Credits", Type.GetType("System.Decimal"))
            .Columns.Add("IsCreditsEarned", Type.GetType("System.Boolean"))
            .Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
            .Columns.Add("IsCreditsAttempted", Type.GetType("System.Boolean"))
            .Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
            .Columns.Add("GPA", Type.GetType("System.Decimal"))
            .Columns.Add("Score", Type.GetType("System.Decimal"))
        End With
        '
        '
        '********************************************************************************
        '**** Massage GraduateAudit and Results table to produce TermProgress table. ****
        '********************************************************************************
        '
        '   add DISTINCT Terms into TermProgress table
        If dtGraduateAudit.Rows.Count > 0 And dtResults.Rows.Count > 0 Then
            Dim drs() As DataRow = dtResults.Select("", "StartDate")

            ' For Each dr In dtResults.Rows
            For Each dr In drs
                If Not dr.IsNull("TermId") Then
                    If Not IsValueAlreadyInTable(dr("TermId").ToString, "TermId", dtTermProgress) Then
                        '   add row into TermProgress table
                        Dim row As DataRow
                        row = dtTermProgress.NewRow
                        row("TermId") = dr("TermId")
                        row("TermDescrip") = dr("TermDescrip")
                        row("StartDate") = dr("StartDate")
                        row("EndDate") = dr("EndDate")
                        dtTermProgress.Rows.Add(row)
                    End If
                End If
            Next
        End If
        '
        '
        '   CreditsEarned and CreditsAttempted for each course on every particular term
        '   TermGPA and Cummulative GPA for every term
        termCtr = 0
        Dim cummTermGPA As Decimal
        'Dim cummGPA As Decimal
        'Dim courseCtr As Integer
        Dim termCourseCtr As Integer
        Dim oneTerm As String
        Dim cummTerms As New StringBuilder
        Dim equivReqid As String
        'Dim rrows() As DataRow
        Dim tmpRows() As DataRow

        For Each dr In dtTermProgress.Rows
            termCtr += 1
            cummTermGPA = 0
            termCourseCtr = 0
            arrRows = dtResults.Select("TermId='" & dr("TermId").ToString & "'")

            If arrRows.GetLength(0) > 0 Then

                dr("TakenCourses") = arrRows.GetLength(0)
                dr("CreditsEarned") = 0
                dr("FACreditsEarned") = 0
                dr("CreditsAttempted") = 0
                If (MyPortalAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric") Then
                    For Each resultDR As DataRow In arrRows

                        'Verify that course belong to program version definition:
                        '       Look for course in the Graduate Audit table.
                        tmpRows = dtGraduateAudit.Select("ReqId='" & resultDR("ReqId").ToString & "'")

                        If tmpRows.GetLength(0) > 0 Then
                            Dim IsPass As Boolean = br.GrdOverRide(tmpRows(0)("PrgVerId").ToString(), resultDR("reqId").ToString, resultDR("Grade").ToString)
                            'Course belongs to program version definition
                            '
                            '
                            '--------------------------------------------------------------------------
                            'Troy:10/27/2006:The following block of code was commented out because the
                            'results for a term should not have anything to do with previous terms. It
                            'is the cummulative GPA calculation which has to worry about course
                            'repetitions etc. 
                            '--------------------------------------------------------------------------
                            'First, check if course was previously taken
                            'rrows = dtCummProgress.Select("ReqId='" & resultDR("ReqId").ToString & "'")

                            'If rrows.GetLength(0) = 0 Then
                            '    'Account for Credits Earned only ONCE
                            '    If resultDR("IsCreditsEarned") Then
                            '        dr("CreditsEarned") += resultDR("Credits")
                            '    End If
                            'End If
                            '---------------------------------------------------------------------------
                            ''Account for Credits Attempted ALWAYS, regardless of repetitions
                            'If Not IsDBNull(resultDR("IsCreditsAttempted")) Then
                            '    If resultDR("IsCreditsAttempted") Then
                            '        dr("CreditsAttempted") += resultDR("Credits")
                            '    End If

                            'End If

                            If Not IsDBNull(resultDR("IsCreditsAttempted")) Then
                                If resultDR("IsCreditsAttempted") = True Or resultDR("IsCreditsAttempted") = 1 Then
                                    If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                        Dim boolIsCourseALab As Boolean = False
                                        boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal = 0.0
                                            Try
                                                decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(stuEnrollId, MyPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                            Catch ex As Exception
                                                decCredits = 0
                                            End Try
                                            dr("CreditsAttempted") += decCredits
                                        Else
                                            dr("CreditsAttempted") += 0
                                        End If
                                    Else
                                        dr("CreditsAttempted") += resultDR("Credits")
                                    End If
                                Else
                                    dr("CreditsAttempted") += 0.0
                                End If
                            End If

                            'If Not IsDBNull(resultDR("IsCreditsEarned")) Then
                            '    If resultDR("IsCreditsEarned") And IsPass Then
                            '        ''Code added by Saraswathi on April 6 2009
                            '        '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                            '        ''' ''Call the function to add credits earned
                            '        Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDR("TestId").ToString())
                            '        If isHrsComp = True Then
                            '            dr("CreditsEarned") += resultDR("Credits")
                            '        End If

                            '    End If
                            'End If

                            If IsPass = True Then

                                ' ''Code added by Saraswathi on April 6 2009
                                ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                ' ''' ''Call the function to add credits earned
                                Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDR("TestId").ToString())
                                If isHrsComp = True Then
                                    If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                        Dim boolIsCourseALab As Boolean = False
                                        boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal = 0.0
                                            Try
                                                decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(stuEnrollId, MyPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                            Catch ex As Exception
                                                decCredits = 0
                                            End Try
                                            dr("CreditsEarned") += decCredits
                                            dr("FACreditsEarned") += resultDR("FinAidCredits")
                                        Else
                                            dr("CreditsEarned") += 0
                                            dr("FACreditsEarned") += 0
                                        End If
                                    Else
                                        ''Added by saraswathi lakshmanan on May 06 2009
                                        ''Earn credits only if, credits earned is checked.
                                        If resultDR("IsCreditsEarned").ToString = True Then
                                            dr("CreditsEarned") += resultDR("Credits")
                                            dr("FACreditsEarned") += resultDR("FinAidCredits")
                                        Else
                                            dr("CreditsEarned") += 0
                                            dr("FACreditsEarned") += 0
                                        End If
                                    End If
                                Else
                                    If (MyPortalAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                        If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                            Dim boolIsCourseALab As Boolean = False
                                            boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal = 0.0
                                                Try
                                                    decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(stuEnrollId, MyPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                dr("CreditsEarned") += decCredits
                                                dr("FACreditsEarned") += resultDR("FinAidCredits")
                                            Else
                                                dr("CreditsEarned") += 0
                                                dr("FACreditsEarned") += 0
                                            End If
                                        Else
                                            ''Added by saraswathi lakshmanan on May 06 2009
                                            ''Earn credits only if, credits earned is checked.
                                            If resultDR("IsCreditsEarned").ToString = True Then
                                                dr("CreditsEarned") += resultDR("Credits")
                                                dr("FACreditsEarned") += resultDR("FinAidCredits")
                                            Else
                                                dr("CreditsEarned") += 0
                                                dr("FACreditsEarned") += 0
                                            End If
                                        End If
                                    Else
                                        dr("CreditsEarned") += 0.0
                                        dr("FACreditsEarned") += 0
                                    End If
                                End If

                                'dr("Credits") = resultDR("Credits")
                                'dr("Hours") = resultDR("Hours")
                            Else
                                If (MyPortalAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                    ''Added by saraswathi lakshmanan on May 06 2009
                                    ''Earn credits only if, credits earned is checked.
                                    If resultDR("IsCreditsEarned").ToString = True Then
                                        dr("CreditsEarned") += resultDR("Credits")
                                        dr("FACreditsEarned") += resultDR("FinAidCredits")
                                    Else
                                        dr("CreditsEarned") += 0
                                        dr("FACreditsEarned") += 0
                                    End If
                                Else
                                    dr("CreditsEarned") += 0.0
                                    dr("FACreditsEarned") += 0
                                End If
                                If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                    Dim boolIsCourseALab As Boolean = False
                                    boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                    If boolIsCourseALab = True Then
                                        Dim decCredits As Decimal = 0.0
                                        Try
                                            decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(stuEnrollId, MyPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                        Catch ex As Exception
                                            decCredits = 0
                                        End Try
                                        dr("CreditsEarned") += decCredits
                                        dr("FACreditsEarned") += resultDR("FinAidCredits")
                                    Else
                                        dr("CreditsEarned") += 0
                                        dr("FACreditsEarned") += 0
                                    End If




                                End If
                            End If
                        Else
                            'Course does not belong to program version definition
                            'Adjust number of courses taken on this term
                            dr("TakenCourses") -= 1
                        End If
                    Next

                    '   Compute TermGPA and cummulative GPA
                    '   Select rows from dtResults for current term and compute term GPA.
                    oneTerm = "(TermId='" & dr("TermId").ToString & "' AND IsInGPA=True AND IsCreditsAttempted=True)"
                    arrRows = dtResults.Select(oneTerm)
                    dr("TermGPA") = ComputeGPA(arrRows, True)

                    '   Compute cummulative GPA
                    AddCoursesToCummProgress(dtCummProgress, arrRows, gradeReps)

                    dr("GPA") = ComputeCumGPA(dtCummProgress)
                Else
                    For Each resultDR As DataRow In arrRows

                        'Verify that course belong to program version definition:
                        '       Look for course in the Graduate Audit table.
                        tmpRows = dtGraduateAudit.Select("ReqId='" & resultDR("ReqId").ToString & "'")

                        If tmpRows.GetLength(0) > 0 Then
                            Dim IsPass As Boolean = br.GrdOverRide(tmpRows(0)("PrgVerId").ToString(), resultDR("reqId").ToString, resultDR("Grade").ToString)
                            'Course belongs to program version definition
                            '
                            '
                            '--------------------------------------------------------------------------
                            'Troy:10/27/2006:The following block of code was commented out because the
                            'results for a term should not have anything to do with previous terms. It
                            'is the cummulative GPA calculation which has to worry about course
                            'repetitions etc. 
                            '--------------------------------------------------------------------------
                            'First, check if course was previously taken
                            'rrows = dtCummProgress.Select("ReqId='" & resultDR("ReqId").ToString & "'")

                            'If rrows.GetLength(0) = 0 Then
                            '    'Account for Credits Earned only ONCE
                            '    If resultDR("IsCreditsEarned") Then
                            '        dr("CreditsEarned") += resultDR("Credits")
                            '    End If
                            'End If
                            '---------------------------------------------------------------------------
                            ''Account for Credits Attempted ALWAYS, regardless of repetitions
                            'If Not resultDR("IsCreditsAttempted") Is System.DBNull.Value Then
                            '    'code modified by balaji on 04/06/2009 mantis:15802
                            '    'modification starts here
                            '    If resultDR("IsCreditsAttempted") = True Or resultDR("IsCreditsAttempted") = 1 Then
                            '        dr("CreditsAttempted") += resultDR("Credits")
                            '    Else
                            '        dr("CreditsAttempted") += 0
                            '    End If
                            '    ''modification ends here
                            'End If

                            If Not IsDBNull(resultDR("IsCreditsAttempted")) Then
                                If resultDR("IsCreditsAttempted") = True Or resultDR("IsCreditsAttempted") = 1 Then
                                    If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                        Dim boolIsCourseALab As Boolean = False
                                        boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal = 0.0
                                            Try
                                                decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(stuEnrollId, MyPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                            Catch ex As Exception
                                                decCredits = 0
                                            End Try
                                            dr("CreditsAttempted") += decCredits
                                        Else
                                            dr("CreditsAttempted") += 0
                                        End If
                                    Else
                                        dr("CreditsAttempted") += resultDR("Credits")
                                    End If
                                Else
                                    dr("CreditsAttempted") += 0.0
                                End If
                            Else
                                If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                    Dim boolIsCourseALab As Boolean = False
                                    boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                    If boolIsCourseALab = True Then
                                        Dim decCredits As Decimal = 0.0
                                        Try
                                            decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(stuEnrollId, MyPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                        Catch ex As Exception
                                            decCredits = 0
                                        End Try
                                        dr("CreditsAttempted") += decCredits
                                    Else
                                        dr("CreditsAttempted") += 0
                                    End If
                                Else
                                    dr("CreditsAttempted") += resultDR("Credits")
                                End If
                            End If


                            'If Not resultDR("IsCreditsEarned") Is System.DBNull.Value And IsPass Then

                            '    ''Code added by Saraswathi on April 6 2009
                            '    '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                            '    ''' ''Call the function to add credits earned
                            '    Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDR("TestId").ToString())
                            '    If isHrsComp = True Then
                            '        dr("CreditsEarned") += resultDR("Credits")
                            '    End If


                            '    ''  dr("CreditsEarned") += resultDR("Credits")
                            'End If

                            If IsPass = True Then

                                ' ''Code added by Saraswathi on April 6 2009
                                ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                ' ''' ''Call the function to add credits earned
                                Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDR("TestId").ToString())
                                If isHrsComp = True Then
                                    If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                        Dim boolIsCourseALab As Boolean = False
                                        boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal = 0.0
                                            Try
                                                decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(stuEnrollId, MyPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                            Catch ex As Exception
                                                decCredits = 0
                                            End Try
                                            dr("CreditsEarned") += decCredits
                                            dr("FACreditsEarned") += resultDR("FinAidCredits")
                                        Else
                                            dr("CreditsEarned") += 0
                                            dr("FACreditsEarned") += 0
                                        End If
                                    Else
                                        ''Added by saraswathi lakshmanan on May 06 2009
                                        ''Earn credits only if, credits earned is checked.
                                        If resultDR("IsCreditsEarned").ToString = True Then
                                            dr("CreditsEarned") += resultDR("Credits")
                                            dr("FACreditsEarned") += resultDR("FinAidCredits")
                                        Else
                                            dr("CreditsEarned") += 0
                                            dr("FACreditsEarned") += 0
                                        End If

                                    End If
                                Else
                                    If (MyPortalAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                        If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                            Dim boolIsCourseALab As Boolean = False
                                            boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal = 0.0
                                                Try
                                                    decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(stuEnrollId, MyPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                dr("CreditsEarned") += decCredits
                                                dr("FACreditsEarned") += resultDR("FinAidCredits")
                                            Else
                                                dr("CreditsEarned") += 0
                                                dr("FACreditsEarned") += 0
                                            End If
                                        Else
                                            ''Added by saraswathi lakshmanan on May 06 2009
                                            ''Earn credits only if, credits earned is checked.
                                            If resultDR("IsCreditsEarned").ToString = True Then
                                                dr("CreditsEarned") += resultDR("Credits")
                                                dr("FACreditsEarned") += resultDR("FinAidCredits")
                                            Else
                                                dr("CreditsEarned") += 0
                                                dr("FACreditsEarned") += 0
                                            End If
                                        End If
                                    Else
                                        dr("CreditsEarned") += 0.0
                                        dr("FACreditsEarned") += 0
                                    End If
                                End If

                                'dr("Credits") = resultDR("Credits")
                                'dr("Hours") = resultDR("Hours")
                            Else
                                If Not resultDR("GrdSysDetailId") Is DBNull.Value And (MyPortalAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then

                                    ''Added by saraswathi lakshmanan on May 06 2009
                                    ''Earn credits only if, credits earned is checked.
                                    If resultDR("IsCreditsEarned").ToString = True Then
                                        dr("CreditsEarned") += resultDR("Credits")
                                        dr("FACreditsEarned") += resultDR("FinAidCredits")
                                    Else
                                        dr("CreditsEarned") += 0
                                        dr("FACreditsEarned") += 0
                                    End If
                                Else
                                    dr("CreditsEarned") += 0.0
                                    dr("FACreditsEarned") += 0
                                End If
                                If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                    Dim boolIsCourseALab As Boolean = False
                                    boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                    If boolIsCourseALab = True Then
                                        Dim decCredits As Decimal = 0.0
                                        Try
                                            decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(stuEnrollId, MyPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                        Catch ex As Exception
                                            decCredits = 0
                                        End Try
                                        dr("CreditsEarned") += decCredits
                                        dr("FACreditsEarned") += resultDR("FinAidCredits")
                                    Else
                                        dr("CreditsEarned") += 0
                                        dr("FACreditsEarned") += 0
                                    End If
                                End If
                            End If
                        Else
                            'Course does not belong to program version definition
                            'Adjust number of courses taken on this term
                            equivReqid = GetEquivalentReqId(resultDR("ReqId").ToString).ToString
                            If equivReqid <> "" Then
                                'tmpRows = dtGraduateAudit.Select("ReqId='" & equivReqid.ToString & "'")
                                tmpRows = dtGraduateAudit.Select("ReqId='" & resultDR("ReqId").ToString & "'")
                                If tmpRows.GetLength(0) > 0 Then
                                    'Dim IsPass As Boolean = br.GrdOverRide(tmpRows(0)("PrgVerId").ToString(), equivReqid.ToString, resultDR("Grade").ToString)
                                    Dim IsPass As Boolean = br.GrdOverRide(tmpRows(0)("PrgVerId").ToString(), resultDR("ReqId").ToString, resultDR("Grade").ToString)
                                    'code modified by balaji on 04/06/2009 mantis:15802
                                    'modification starts here
                                    If Not IsDBNull(resultDR("IsCreditsAttempted")) Then
                                        If resultDR("IsCreditsAttempted") = True Or resultDR("IsCreditsAttempted") = 1 Then
                                            If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                                Dim boolIsCourseALab As Boolean = False
                                                boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                                If boolIsCourseALab = True Then
                                                    Dim decCredits As Decimal = 0.0
                                                    Try
                                                        decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(stuEnrollId, MyPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                                    Catch ex As Exception
                                                        decCredits = 0
                                                    End Try
                                                    dr("CreditsAttempted") += decCredits
                                                Else
                                                    dr("CreditsAttempted") += 0
                                                End If
                                            Else
                                                dr("CreditsAttempted") += resultDR("Credits")
                                            End If
                                        Else
                                            dr("CreditsAttempted") += 0.0
                                        End If
                                    End If
                                    'modification ends here

                                    'If resultDR("IsCreditsEarned") And IsPass Then
                                    '    ''Code added by Saraswathi on April 6 2009
                                    '    '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                    '    ''' ''Call the function to add credits earned
                                    '    Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDR("TestId").ToString())
                                    '    If isHrsComp = True Then
                                    '        dr("CreditsEarned") += resultDR("Credits")
                                    '    End If


                                    '    ''  dr("CreditsEarned") += resultDR("Credits")
                                    'End If
                                    If IsPass = True Then

                                        ''Code added by Saraswathi on April 6 2009
                                        ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                        ' ''' ''Call the function to add credits earned
                                        Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDR("TestId").ToString())
                                        If isHrsComp = True Then
                                            If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                                Dim boolIsCourseALab As Boolean = False
                                                boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                                If boolIsCourseALab = True Then
                                                    Dim decCredits As Decimal = 0.0
                                                    Try
                                                        decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(stuEnrollId, MyPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                                    Catch ex As Exception
                                                        decCredits = 0
                                                    End Try
                                                    dr("CreditsEarned") += decCredits
                                                    dr("FACreditsEarned") += resultDR("FinAidCredits")
                                                Else
                                                    dr("CreditsEarned") += 0
                                                    dr("FACreditsEarned") += 0
                                                End If
                                            Else
                                                ''Added by saraswathi lakshmanan on May 06 2009
                                                ''Earn credits only if, credits earned is checked.
                                                If resultDR("IsCreditsEarned").ToString = True Then
                                                    dr("CreditsEarned") += resultDR("Credits")
                                                    dr("FACreditsEarned") += resultDR("FinAidCredits")
                                                Else
                                                    dr("CreditsEarned") += 0
                                                    dr("FACreditsEarned") += 0
                                                End If
                                            End If
                                        Else
                                            If (MyPortalAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                                If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                                    Dim boolIsCourseALab As Boolean = False
                                                    boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                                    If boolIsCourseALab = True Then
                                                        Dim decCredits As Decimal = 0.0
                                                        Try
                                                            decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(stuEnrollId, MyPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                                        Catch ex As Exception
                                                            decCredits = 0
                                                        End Try
                                                        dr("CreditsEarned") += decCredits
                                                        dr("FACreditsEarned") += resultDR("FinAidCredits")
                                                    Else
                                                        dr("CreditsEarned") += 0
                                                        dr("FACreditsEarned") += 0
                                                    End If
                                                Else
                                                    ''Added by saraswathi lakshmanan on May 06 2009
                                                    ''Earn credits only if, credits earned is checked.
                                                    If resultDR("IsCreditsEarned").ToString = True Then
                                                        dr("CreditsEarned") += resultDR("Credits")
                                                        dr("FACreditsEarned") += resultDR("FinAidCredits")
                                                    Else
                                                        dr("CreditsEarned") += 0
                                                        dr("FACreditsEarned") += 0
                                                    End If
                                                End If
                                            Else
                                                dr("CreditsEarned") += 0.0
                                                dr("FACreditsEarned") += 0
                                            End If
                                        End If

                                        'dr("Credits") = resultDR("Credits")
                                        'dr("Hours") = resultDR("Hours")
                                    Else
                                        If Not resultDR("GrdSysDetailId") Is DBNull.Value And (MyPortalAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                            ''Added by saraswathi lakshmanan on May 06 2009
                                            ''Earn credits only if, credits earned is checked.
                                            If resultDR("IsCreditsEarned").ToString = True Then
                                                dr("CreditsEarned") += resultDR("Credits")
                                                dr("FACreditsEarned") += resultDR("FinAidCredits")
                                            Else
                                                dr("CreditsEarned") += 0
                                                dr("FACreditsEarned") += 0
                                            End If
                                        Else
                                            dr("CreditsEarned") += 0.0
                                            dr("FACreditsEarned") += 0
                                        End If
                                        If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                            Dim boolIsCourseALab As Boolean = False
                                            boolIsCourseALab = (New PortalTransferGradeDB).IsCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal = 0.0
                                                Try
                                                    decCredits = (New MyPortalTranscriptDb).ComputeWithCreditsPerService(stuEnrollId, MyPortalAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                                Catch ex As Exception
                                                    decCredits = 0
                                                End Try
                                                dr("CreditsEarned") += decCredits
                                                dr("FACreditsEarned") += resultDR("FinAidCredits")
                                            Else
                                                dr("CreditsEarned") += 0
                                                dr("FACreditsEarned") += 0
                                            End If
                                        End If
                                    End If
                                Else
                                    dr("TakenCourses") -= 1
                                End If
                            Else
                                dr("TakenCourses") -= 1
                            End If

                        End If
                    Next

                    '   Compute TermGPA and cummulative GPA
                    '   Select rows from dtResults for current term and compute term GPA.
                    oneTerm = "(TermId='" & dr("TermId").ToString & "' AND IsInGPA=True AND IsCreditsAttempted=True)"
                    arrRows = dtResults.Select(oneTerm)
                    dr("TermGPA") = ComputeGPA(arrRows, True)

                    '   Compute cummulative GPA
                    AddCoursesToCummProgress(dtCummProgress, arrRows, gradeReps)

                    dr("GPA") = ComputeGPA(dtCummProgress)
                End If


            End If
        Next

        Return dtTermProgress
        'Dim ds2 As New DataSet
        'ds2.Tables.Add(dtGradAudit)
        'Return ds2
    End Function


    Public Function GetTermProgressFromResults_SP(Optional ByVal stuEnrollId As String = "", Optional ByVal gradeReps As String = "", Optional ByVal includeHours As Boolean = True, Optional ByVal termCond As String = "", Optional ByVal classCond As String = "", Optional ByVal campusID As String = "") As DataSet
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim br As New PortalRegisterStudentsBR
        Dim ds As New DataSet
        Dim rtnDs As New DataSet

        Dim MyPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            MyPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            MyPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        If stuEnrollId <> "" Then
            If MyPortalAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric" Then
                'Code Commented By Vijay Ramteke on May, 06 2009
                'ds = dbTranscript.GetGraduateAuditByEnrollmentForNumericForCourseEquivalent(StuEnrollId, termEndDate)
                'Code Commented By Vijay Ramteke on May, 06 2009
                'Code Added By Vijay Ramteke on May, 06 2009
                ds = dbTranscript.GetGraduateAuditByEnrollmentForNumericForCourseEquivalent_SP(stuEnrollId, , termCond, classCond)
                'Code Added By Vijay Ramteke on May, 06 2009
            ElseIf dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
                'Code Commented By Vijay Ramteke on May, 06 2009
                'ds = dbTranscript.GetGraduateAuditByCEProgEnrollmentForCourseEquivalent(StuEnrollId, termEndDate)
                'Code Commented By Vijay Ramteke on May, 06 2009
                'Code Added By Vijay Ramteke on May, 06 2009
                ds = dbTranscript.GetGraduateAuditByCEProgEnrollmentForCourseEquivalent_SP(stuEnrollId, , termCond, classCond)
                'Code Added By Vijay Ramteke on May, 06 2009

            Else
                'Code Commented By Vijay Ramteke on May, 06 2009
                'ds = dbTranscript.GetGraduateAuditByEnrollmentForCourseEquivalent(StuEnrollId, termEndDate)
                'Code Commented By Vijay Ramteke on May, 06 2009
                'Code Added By Vijay Ramteke on May, 06 2009
                ds = dbTranscript.GetGraduateAuditByEnrollmentForCourseEquivalent_SP(stuEnrollId, , termCond, classCond)
                'Code Added By Vijay Ramteke on May, 06 2009

            End If
            ' Code Commented by vijay Ramteke on May, 06 2009
            'dtGraduateAudit = BuildGraduateAuditDT(stuEnrollId, gradeReps, includeHours)
            ' Code Commented by vijay Ramteke on May, 06 2009
            ' Code Added by vijay Ramteke on May, 06 2009
            For Each drRow As DataRow In ds.Tables("Results").Rows
                drRow("IsCourseALab") = isCourseALabWorkOrLabHourCourse(drRow("ReqId").ToString)
                If drRow("IsCourseALab") Then
                    drRow("decCredits") = dbTranscript.ComputeWithCreditsPerService_SP(stuEnrollId, drRow("ReqId").ToString)
                End If
            Next
            ds.AcceptChanges()
            dtGraduateAudit = BuildGraduateAuditDT_SP(ds, gradeReps, includeHours)
            ' Code Added by vijay Ramteke on May, 06 2009
            'dtResults = ds.Tables("Results")
            dtResults = dbTranscript.GetDTResultsLetter(stuEnrollId, termCond, classCond)

            '''' Code changes by kamalesh Ahuja on 17 dec 2010 to resolve issue id DE1394
            If MyPortalAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric" Then
                GetAverageScore()
            Else
                GetAverageGpa()
            End If

            ''''''

            SetProperties()
        End If

        rtnDs.Tables.Add(dtResults.Copy())
        '
        '
        '
        Dim termCtr As Integer = 1
        Dim arrRows() As DataRow
        Dim dr As DataRow
        Dim dtTermProgress As New DataTable
        Dim dtCummProgress As New DataTable
        '
        '   structure of the TermProgress table
        With dtTermProgress
            .Columns.Add("TermId", Type.GetType("System.Guid"))
            .Columns.Add("TermDescrip", Type.GetType("System.String"))
            .Columns.Add("StartDate", Type.GetType("System.DateTime"))
            .Columns.Add("EndDate", Type.GetType("System.DateTime"))
            .Columns.Add("TakenCourses", Type.GetType("System.Int32"))
            .Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
            .Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
            .Columns.Add("TermGPA", Type.GetType("System.Decimal"))
            .Columns.Add("GPA", Type.GetType("System.Decimal"))
            .Columns.Add("Score", Type.GetType("System.Decimal"))
        End With
        '
        '
        '   if the GradAudit is empty, return an empty TermProgress table
        If dtGraduateAudit.Rows.Count = 0 Then
            'Return dtTermProgress
            Return rtnDs
        End If
        '
        '
        '
        '   structure of the CummProgress table
        With dtCummProgress
            .Columns.Add("ReqId", Type.GetType("System.String"))
            .Columns.Add("ReqTypeId", Type.GetType("System.String"))
            .Columns.Add("Credits", Type.GetType("System.Decimal"))
            .Columns.Add("IsCreditsEarned", Type.GetType("System.Boolean"))
            .Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
            .Columns.Add("IsCreditsAttempted", Type.GetType("System.Boolean"))
            .Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
            .Columns.Add("GPA", Type.GetType("System.Decimal"))
            .Columns.Add("Score", Type.GetType("System.Decimal"))
        End With
        '
        '
        '********************************************************************************
        '**** Massage GraduateAudit and Results table to produce TermProgress table. ****
        '********************************************************************************
        '
        '   add DISTINCT Terms into TermProgress table
        If dtGraduateAudit.Rows.Count > 0 And dtResults.Rows.Count > 0 Then
            Dim drs() As DataRow = dtResults.Select("", "StartDate")

            ' For Each dr In dtResults.Rows
            For Each dr In drs
                If Not dr.IsNull("TermId") Then
                    If Not IsValueAlreadyInTable(dr("TermId").ToString, "TermId", dtTermProgress) Then
                        '   add row into TermProgress table
                        Dim row As DataRow
                        row = dtTermProgress.NewRow
                        row("TermId") = dr("TermId")
                        row("TermDescrip") = dr("TermDescrip")
                        row("StartDate") = dr("StartDate")
                        row("EndDate") = dr("EndDate")
                        dtTermProgress.Rows.Add(row)
                    End If
                End If
            Next
        End If
        '
        '
        '   CreditsEarned and CreditsAttempted for each course on every particular term
        '   TermGPA and Cummulative GPA for every term
        termCtr = 0
        Dim cummTermGPA As Decimal
        'Dim cummGPA As Decimal
        'Dim courseCtr As Integer
        Dim termCourseCtr As Integer
        Dim oneTerm As String
        Dim cummTerms As New StringBuilder
        Dim equivReqid As String
        'Dim rrows() As DataRow
        Dim tmpRows() As DataRow

        For Each dr In dtTermProgress.Rows
            termCtr += 1
            cummTermGPA = 0
            termCourseCtr = 0
            arrRows = dtResults.Select("TermId='" & dr("TermId").ToString & "'")

            If arrRows.GetLength(0) > 0 Then

                dr("TakenCourses") = arrRows.GetLength(0)
                dr("CreditsEarned") = 0
                dr("CreditsAttempted") = 0
                If (MyPortalAdvAppSettings.AppSettings("TranscriptType", campusID).ToLower = "traditional_numeric") Then
                    For Each resultDr As DataRow In arrRows

                        'Verify that course belong to program version definition:
                        '       Look for course in the Graduate Audit table.
                        tmpRows = dtGraduateAudit.Select("ReqId='" & resultDr("ReqId").ToString & "'")

                        If tmpRows.GetLength(0) > 0 Then
                            Dim IsPass As Boolean = br.GrdOverRide_SP(tmpRows(0)("PrgVerId").ToString(), resultDr("reqId").ToString, resultDr("Grade").ToString)
                            'Dim IsPass As Boolean = tmpRows(0)("IsPass")
                            If Not IsDBNull(resultDr("IsCreditsAttempted")) Then
                                If resultDr("IsCreditsAttempted") = True Or resultDr("IsCreditsAttempted") = 1 Then
                                    If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                        Dim boolIsCourseALab As Boolean = False
                                        'boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                        boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal = 0.0
                                            decCredits = tmpRows(0)("decCredits")
                                            'Try
                                            '    decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService_SP(stuEnrollId, resultDR("ReqId").ToString)
                                            'Catch ex As System.Exception
                                            '    decCredits = 0
                                            'End Try
                                            dr("CreditsAttempted") += decCredits
                                        Else
                                            dr("CreditsAttempted") += 0
                                        End If
                                    Else
                                        dr("CreditsAttempted") += resultDr("Credits")
                                    End If
                                Else
                                    dr("CreditsAttempted") += 0.0
                                End If
                            End If


                            If IsPass = True Then

                                ''Code added by Saraswathi on April 6 2009
                                ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                ' ''' ''Call the function to add credits earned
                                Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDr("TestId").ToString())
                                If isHrsComp = True Then
                                    If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                        Dim boolIsCourseALab As Boolean = False
                                        'boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                        boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal = 0.0
                                            decCredits = tmpRows(0)("decCredits")
                                            'Try
                                            '    decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(stuEnrollId, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                            'Catch ex As System.Exception
                                            '    decCredits = 0
                                            'End Try
                                            dr("CreditsEarned") += decCredits
                                        Else
                                            dr("CreditsEarned") += 0
                                        End If
                                    Else
                                        ''Added by saraswathi lakshmanan on May 06 2009
                                        ''Earn credits only if, credits earned is checked.
                                        If resultDr("IsCreditsEarned").ToString = True Then
                                            dr("CreditsEarned") += resultDr("Credits")
                                        Else
                                            dr("CreditsEarned") += 0
                                        End If
                                    End If
                                Else
                                    If (MyPortalAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                        If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                            Dim boolIsCourseALab As Boolean = False
                                            'boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                            boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal = 0.0
                                                decCredits = tmpRows(0)("decCredits")
                                                'Try
                                                '    decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(stuEnrollId, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                                'Catch ex As System.Exception
                                                '    decCredits = 0
                                                'End Try
                                                dr("CreditsEarned") += decCredits
                                            Else
                                                dr("CreditsEarned") += 0
                                            End If
                                        Else
                                            ''Added by saraswathi lakshmanan on May 06 2009
                                            ''Earn credits only if, credits earned is checked.
                                            If resultDr("IsCreditsEarned").ToString = True Then
                                                dr("CreditsEarned") += resultDr("Credits")
                                            Else
                                                dr("CreditsEarned") += 0
                                            End If
                                        End If
                                    Else
                                        dr("CreditsEarned") += 0.0
                                    End If
                                End If

                                'dr("Credits") = resultDR("Credits")
                                'dr("Hours") = resultDR("Hours")
                            Else
                                If (MyPortalAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                    ''Added by saraswathi lakshmanan on May 06 2009
                                    ''Earn credits only if, credits earned is checked.
                                    If resultDr("IsCreditsEarned").ToString = True Then
                                        dr("CreditsEarned") += resultDr("Credits")
                                    Else
                                        dr("CreditsEarned") += 0
                                    End If
                                Else
                                    dr("CreditsEarned") += 0.0
                                End If
                                If resultDr("GrdSysDetailId") Is DBNull.Value Then
                                    Dim boolIsCourseALab As Boolean
                                    ' boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                    boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                    If boolIsCourseALab = True Then
                                        Dim decCredits As Decimal
                                        decCredits = tmpRows(0)("decCredits")
                                        'Try
                                        '    decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(stuEnrollId, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                        'Catch ex As System.Exception
                                        '    decCredits = 0
                                        'End Try
                                        dr("CreditsEarned") += decCredits
                                    Else
                                        dr("CreditsEarned") += 0
                                    End If
                                End If
                            End If
                        Else
                            'Course does not belong to program version definition
                            'Adjust number of courses taken on this term
                            dr("TakenCourses") -= 1
                        End If
                    Next

                    '   Compute TermGPA and cummulative GPA
                    '   Select rows from dtResults for current term and compute term GPA.
                    oneTerm = "(TermId='" & dr("TermId").ToString & "' AND IsInGPA=True AND IsCreditsAttempted=True)"
                    arrRows = dtResults.Select(oneTerm)
                    dr("TermGPA") = ComputeGPA(arrRows, True)

                    '   Compute cummulative GPA
                    AddCoursesToCummProgress(dtCummProgress, arrRows, gradeReps)

                    dr("GPA") = ComputeCumGPA(dtCummProgress)
                Else
                    For Each resultDR As DataRow In arrRows

                        'Verify that course belong to program version definition:
                        '       Look for course in the Graduate Audit table.
                        tmpRows = dtGraduateAudit.Select("ReqId='" & resultDR("ReqId").ToString & "'")

                        If tmpRows.GetLength(0) > 0 Then
                            Dim IsPass As Boolean = br.GrdOverRide_SP(tmpRows(0)("PrgVerId").ToString(), resultDR("reqId").ToString, resultDR("Grade").ToString)
                            'Dim IsPass As Boolean = tmpRows(0)("IsPass")

                            If IsPass = True Then

                                ''Code added by Saraswathi on April 6 2009
                                ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                ' ''' ''Call the function to add credits earned
                                Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDR("TestId").ToString())
                                If isHrsComp = True Then
                                    If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                        Dim boolIsCourseALab As Boolean = False
                                        ' boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                        boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                        If boolIsCourseALab = True Then
                                            Dim decCredits As Decimal = 0.0
                                            decCredits = tmpRows(0)("decCredits")
                                            'Try
                                            '    decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(stuEnrollId, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                            'Catch ex As System.Exception
                                            '    decCredits = 0
                                            'End Try
                                            dr("CreditsEarned") += decCredits
                                        Else
                                            dr("CreditsEarned") += 0
                                        End If
                                    Else
                                        ''Added by saraswathi lakshmanan on May 06 2009
                                        ''Earn credits only if, credits earned is checked.
                                        If resultDR("IsCreditsEarned").ToString = True Then
                                            dr("CreditsEarned") += resultDR("Credits")
                                        Else
                                            dr("CreditsEarned") += 0
                                        End If

                                    End If
                                Else
                                    If (MyPortalAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                        If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                            Dim boolIsCourseALab As Boolean = False
                                            'boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                            boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal = 0.0
                                                decCredits = tmpRows(0)("decCredits")
                                                'Try
                                                '    decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(stuEnrollId, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                                'Catch ex As System.Exception
                                                '    decCredits = 0
                                                'End Try
                                                dr("CreditsEarned") += decCredits
                                            Else
                                                dr("CreditsEarned") += 0
                                            End If
                                        Else
                                            ''Added by saraswathi lakshmanan on May 06 2009
                                            ''Earn credits only if, credits earned is checked.
                                            If resultDR("IsCreditsEarned").ToString = True Then
                                                dr("CreditsEarned") += resultDR("Credits")
                                            Else
                                                dr("CreditsEarned") += 0
                                            End If
                                        End If
                                    Else
                                        dr("CreditsEarned") += 0.0
                                    End If
                                End If

                                'dr("Credits") = resultDR("Credits")
                                'dr("Hours") = resultDR("Hours")
                            Else
                                If Not resultDR("GrdSysDetailId") Is DBNull.Value And (MyPortalAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then

                                    ''Added by saraswathi lakshmanan on May 06 2009
                                    ''Earn credits only if, credits earned is checked.
                                    If resultDR("IsCreditsEarned").ToString = True Then
                                        dr("CreditsEarned") += resultDR("Credits")
                                    Else
                                        dr("CreditsEarned") += 0
                                    End If
                                Else
                                    dr("CreditsEarned") += 0.0
                                End If
                                If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                    Dim boolIsCourseALab As Boolean = False
                                    'boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                    boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                    If boolIsCourseALab = True Then
                                        Dim decCredits As Decimal = 0.0
                                        decCredits = tmpRows(0)("decCredits")
                                        'Try
                                        '    decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(stuEnrollId, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                        'Catch ex As System.Exception
                                        '    decCredits = 0
                                        'End Try
                                        dr("CreditsEarned") += decCredits
                                    Else
                                        dr("CreditsEarned") += 0
                                    End If
                                End If
                            End If
                        Else
                            'Course does not belong to program version definition
                            'Adjust number of courses taken on this term
                            equivReqid = GetEquivalentReqId(resultDR("ReqId").ToString).ToString
                            If equivReqid <> "" Then
                                'tmpRows = dtGraduateAudit.Select("ReqId='" & equivReqid.ToString & "'")
                                tmpRows = dtGraduateAudit.Select("ReqId='" & resultDR("ReqId").ToString & "'")
                                If tmpRows.GetLength(0) > 0 Then
                                    'Dim IsPass As Boolean = br.GrdOverRide(tmpRows(0)("PrgVerId").ToString(), equivReqid.ToString, resultDR("Grade").ToString)
                                    Dim IsPass As Boolean = br.GrdOverRide_SP(tmpRows(0)("PrgVerId").ToString(), resultDR("ReqId").ToString, resultDR("Grade").ToString)
                                    'Dim IsPass As Boolean = tmpRows(0)("IsPass")
                                    'code modified by balaji on 04/06/2009 mantis:15802
                                    'modification starts here
                                    If Not IsDBNull(resultDR("IsCreditsAttempted")) Then
                                        If resultDR("IsCreditsAttempted") = True Or resultDR("IsCreditsAttempted") = 1 Then
                                            If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                                Dim boolIsCourseALab As Boolean = False
                                                ' boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                                boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                                If boolIsCourseALab = True Then
                                                    Dim decCredits As Decimal = 0.0
                                                    decCredits = tmpRows(0)("decCredits")
                                                    'Try
                                                    '    decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(stuEnrollId, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                                    'Catch ex As System.Exception
                                                    '    decCredits = 0
                                                    'End Try
                                                    dr("CreditsAttempted") += decCredits
                                                Else
                                                    dr("CreditsAttempted") += 0
                                                End If
                                            Else
                                                dr("CreditsAttempted") += resultDR("Credits")
                                            End If
                                        Else
                                            dr("CreditsAttempted") += 0.0
                                        End If
                                    End If

                                    If IsPass = True Then

                                        ''Code added by Saraswathi on April 6 2009
                                        ' '''need to modify here april 3 2008 --Saraswathi Lakshmanan
                                        ' ''' ''Call the function to add credits earned
                                        Dim isHrsComp As Boolean = dbTranscript.IsCourseCombinationandPass(stuEnrollId, resultDR("TestId").ToString())
                                        If isHrsComp = True Then
                                            If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                                Dim boolIsCourseALab As Boolean = False
                                                'boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                                boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                                If boolIsCourseALab = True Then
                                                    Dim decCredits As Decimal = 0.0
                                                    decCredits = tmpRows(0)("decCredits")
                                                    'Try
                                                    '    decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(stuEnrollId, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                                    'Catch ex As System.Exception
                                                    '    decCredits = 0
                                                    'End Try
                                                    dr("CreditsEarned") += decCredits
                                                Else
                                                    dr("CreditsEarned") += 0
                                                End If
                                            Else
                                                ''Added by saraswathi lakshmanan on May 06 2009
                                                ''Earn credits only if, credits earned is checked.
                                                If resultDR("IsCreditsEarned").ToString = True Then
                                                    dr("CreditsEarned") += resultDR("Credits")
                                                Else
                                                    dr("CreditsEarned") += 0
                                                End If
                                            End If
                                        Else
                                            If (MyPortalAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                                If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                                    Dim boolIsCourseALab As Boolean = False
                                                    ' boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                                    boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                                    If boolIsCourseALab = True Then
                                                        Dim decCredits As Decimal = 0.0
                                                        decCredits = tmpRows(0)("decCredits")
                                                        'Try
                                                        '    decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(stuEnrollId, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                                        'Catch ex As System.Exception
                                                        '    decCredits = 0
                                                        'End Try
                                                        dr("CreditsEarned") += decCredits
                                                    Else
                                                        dr("CreditsEarned") += 0
                                                    End If
                                                Else
                                                    ''Added by saraswathi lakshmanan on May 06 2009
                                                    ''Earn credits only if, credits earned is checked.
                                                    If resultDR("IsCreditsEarned").ToString = True Then
                                                        dr("CreditsEarned") += resultDR("Credits")
                                                    Else
                                                        dr("CreditsEarned") += 0
                                                    End If
                                                End If
                                            Else
                                                dr("CreditsEarned") += 0.0
                                            End If
                                        End If

                                        'dr("Credits") = resultDR("Credits")
                                        'dr("Hours") = resultDR("Hours")
                                    Else
                                        If Not resultDR("GrdSysDetailId") Is DBNull.Value And (MyPortalAdvAppSettings.AppSettings("IncludeHoursForFailingGrade").ToLower = "true") Then
                                            ''Added by saraswathi lakshmanan on May 06 2009
                                            ''Earn credits only if, credits earned is checked.
                                            If resultDR("IsCreditsEarned").ToString = True Then
                                                dr("CreditsEarned") += resultDR("Credits")
                                            Else
                                                dr("CreditsEarned") += 0
                                            End If
                                        Else
                                            dr("CreditsEarned") += 0.0
                                        End If
                                        If resultDR("GrdSysDetailId") Is DBNull.Value Then
                                            Dim boolIsCourseALab As Boolean = False
                                            ' boolIsCourseALab = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse(resultDR("ReqId").ToString)
                                            boolIsCourseALab = tmpRows(0)("IsCourseALab")
                                            If boolIsCourseALab = True Then
                                                Dim decCredits As Decimal = 0.0
                                                decCredits = tmpRows(0)("decCredits")
                                                'Try
                                                '    decCredits = (New MyPortalTranscriptDB).ComputeWithCreditsPerService(stuEnrollId, SingletonAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower, resultDR("ReqId").ToString)
                                                'Catch ex As System.Exception
                                                '    decCredits = 0
                                                'End Try
                                                dr("CreditsEarned") += decCredits
                                            Else
                                                dr("CreditsEarned") += 0
                                            End If
                                        End If
                                    End If
                                Else
                                    dr("TakenCourses") -= 1
                                End If
                            Else
                                dr("TakenCourses") -= 1
                            End If

                        End If
                    Next

                    '   Compute TermGPA and cummulative GPA
                    '   Select rows from dtResults for current term and compute term GPA.
                    oneTerm = "(TermId='" & dr("TermId").ToString & "' AND IsInGPA=True AND IsCreditsAttempted=True)"
                    arrRows = dtResults.Select(oneTerm)
                    dr("TermGPA") = ComputeGPA(arrRows, True)

                    '   Compute cummulative GPA
                    AddCoursesToCummProgress(dtCummProgress, arrRows, gradeReps)

                    dr("GPA") = ComputeGPA(dtCummProgress)
                End If


            End If
        Next
        rtnDs.Tables.Add(dtTermProgress)
        Return rtnDs
        'Dim ds2 As New DataSet
        'ds2.Tables.Add(dtGradAudit)
        'Return ds2
    End Function

#End Region

    Public Function RemoveDuplicateRows(ByVal dTable As DataTable, ByVal colName As String) As DataTable
        Dim hTable As New Hashtable
        Dim duplicateList As New ArrayList
        For Each drow As DataRow In dTable.Rows
            If hTable.Contains(drow(colName)) Then
                duplicateList.Add(drow)
            Else
                hTable.Add(drow(colName), String.Empty)
            End If
        Next

        For Each drow As DataRow In duplicateList
            dTable.Rows.Remove(drow)
        Next
        Return dTable
    End Function
    Public Function GetEquivalentCourseDescrip(ByVal Reqid As String, ByVal stuEnrollId As String)
        Return (New MyPortalTranscriptDb).GetEquivalentCourseDescrip(Reqid, stuEnrollId)
    End Function
    Public Function GetEquivalentReqId(ByVal Reqid As String) As String
        Return (New MyPortalTranscriptDb).GetEquivalentReqId(Reqid)
    End Function
    Public Function GetEquivalentReqId_SP(ByVal Reqid As String) As String
        Return (New MyPortalTranscriptDb).GetEquivalentReqId_SP(Reqid)
    End Function
    ' Added for Multile Student Attendance
    Public Function GetStudentHours(ByVal stuEnrollid, ByVal Otherwhere)
        Dim dbTranscript As New MyPortalTranscriptDb
        Dim ds As New DataSet
        Dim dtGraduateAudit As DataTable
        ds = dbTranscript.GetHours(stuEnrollid, Otherwhere)
        dtGraduateAudit = BuildStudentAttendance(ds)
        Return dtGraduateAudit

    End Function

    ' Added  for Multiple Student Attendance
    Public Function BuildStudentAttendance(ByVal ds As DataSet) As DataTable
        Dim dt As New DataTable("MultiStudentAttend")
        dt = ds.Tables(0)
        dt.TableName = "MultiStudentAttend"
        Return dt
    End Function
    Public Function isCourseALabWorkOrLabHourCourseCombination(ByVal ReqId As String) As Boolean
        Return (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourseCombination(ReqId)
    End Function
    Public Function isCourseALabWorkOrLabHourCourseCombination_SP(ByVal ReqId As String) As Boolean
        Return (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourseCombination_SP(ReqId)
    End Function
    Public Function isCourseALabWorkOrLabHourCourse(ByVal ReqId As String) As Boolean
        Return (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourse_SP(ReqId)
    End Function
    Public Function ComputeWithCreditsPerService(ByVal StuEnrollId As String, ByVal addcreditsbyservice As String, ByVal CourseId As String) As Decimal
        Return (New MyPortalTranscriptDb).ComputeWithCreditsPerService(StuEnrollId, addcreditsbyservice, CourseId)
    End Function
    Public Function GetEquivalentReqIdDS(ByVal reqid As String) As DataSet
        Return (New MyPortalTranscriptDb).GetEquivalentReqIdDS(reqid)
    End Function

    Public Function GetEquivalentReqIdDS_SP(ByVal reqid As String) As DataTable
        Return (New MyPortalTranscriptDb).GetEquivalentReqIdDS_SP(reqid)
    End Function

    Public Function CheckandListRequiredDocuments_ForGraduation(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListGradDocumentRequirements_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    strdocs.Append(row("Descrip") + ";")
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function

    Public Function CheckandListRequiredTest_ForEnrollment(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListEnrollmentTestRequirements_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    strdocs.Append(row("Descrip") + ";")
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function

    Public Function CheckandListRequiredtest_ForGraduation(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListGradTestRequirements_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    strdocs.Append(row("Descrip") + ";")
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If

    End Function
    Public Function CheckandListEnrollreqGroups(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListEnrollReqGroups_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    If row("Descrip") = "School Level Requirements" Then
                        strdocs.Append("-")
                    Else
                        strdocs.Append(row("Descrip") + ";")
                    End If
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If

    End Function
    Public Function CheckandListGradreqGroups(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListGradReqGroups_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    If row("Descrip") = "School Level Requirements" Then
                        strdocs.Append("-")
                    Else
                        strdocs.Append(row("Descrip") + ";")
                    End If

                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If

        'If intStudentHasTaken >= 1 Then
        '    Return "Not Eligible"
        'Else
        '    Return "Eligible"
        'End If
    End Function
    ''--------------------------------------------------------------

    Public Function CheckandListRequiredDocuments_ForFinAid(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListFinAidDocumentRequirements_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    strdocs.Append(row("Descrip") + ";")
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If

        'If intStudentHasTaken >= 1 Then
        '    Return "Not Eligible"
        'Else
        '    Return "Eligible"
        'End If
    End Function

    Public Function CheckandListRequiredtest_ForFinAid(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListFinAidTestRequirements_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    strdocs.Append(row("Descrip") + ";")
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If

        'If intStudentHasTaken >= 1 Then
        '    Return "Not Eligible"
        'Else
        '    Return "Eligible"
        'End If
    End Function
    Public Function CheckandListFinAidreqGroups(ByVal stuEnrollID As String) As String
        Dim ds As DataSet
        Dim studentdb As New RequirementsDB
        Dim strdocs As New StringBuilder
        Dim row As DataRow
        ds = studentdb.CheckandListFinAidReqGroups_Sp(stuEnrollID)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    If row("Descrip") = "School Level Requirements" Then
                        strdocs.Append("-")
                    Else
                        strdocs.Append(row("Descrip") + ";")
                    End If
                Next
                Return strdocs.ToString
            Else
                Return ""
            End If
        Else
            Return ""
        End If

        'If intStudentHasTaken >= 1 Then
        '    Return "Not Eligible"
        'Else
        '    Return "Eligible"
        'End If
    End Function
    Private Function GetCreditsEarned(ByVal dtWU As DataTable, ByVal score As Object) As Boolean
        Dim wuSatisfied As Boolean = True
        Dim i As Integer

        'Check the work units that have been atempted.
        'arrWU = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "' AND Score >= 0")
        If dtWU.Rows.Count > 0 Then
            If IsDBNull(score) Then
                For i = 0 To dtWU.Rows.Count - 1

                    If dtWU.Rows(i)("Required") = True Or dtWU.Rows(i)("MustPass") = True Then
                        'If dtWU.Rows(i)("Required") = True Then
                        If IsDBNull(dtWU.Rows(i)("Score")) Then
                            wuSatisfied = False
                            Exit For
                        End If
                    End If

                Next

            ElseIf score >= 0 Then
                For i = 0 To dtWU.Rows.Count - 1
                    If IsDBNull(dtWU.Rows(i)("score")) Then
                        If dtWU.Rows(i)("Required") = True Or dtWU.Rows(i)("MustPass") = True Then
                            wuSatisfied = False
                            Exit For
                        Else
                            wuSatisfied = True
                        End If
                    ElseIf dtWU.Rows(i)("score") < dtWU.Rows(i)("MinResult") Then
                        'First check if it is required
                        'wuSatisfied = False
                        'Exit For
                        If dtWU.Rows(i)("Required") = True And dtWU.Rows(i)("MustPass") = True Then
                            wuSatisfied = False
                            Exit For
                        End If
                    End If
                Next


            End If
        End If
        If wuSatisfied Then
            'If the score is empty we can say right away that the course has been completed
            If (score.ToString = "" Or IsDBNull(score)) Then
                Return True
            Else
                'Check if the score is a passing score
                If ScoreIsAPass(score) Then
                    Return True
                Else
                    Return False
                End If
            End If
        End If
        Return wuSatisfied



    End Function
    Private Function ScoreIsAPass(ByVal csrScore As Decimal) As Boolean
        Dim db As New StuProgressReportDB
        Dim minPassScore As Decimal

        minPassScore = db.GetMinPassingScore

        If csrScore >= minPassScore Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function GetGraduateAuditEnrollmentForCourseEquivalent(stuEnrollId As String, gradeReps As String, includeHours As Boolean, enrollment As List(Of String)) As List(Of GraduateAuditGroupDto)

        If (stuEnrollId = Guid.Empty.ToString()) Then
            'This is the All Case
            'For each here take the resumen value and condense in one record.
            Dim listHeaders As New List(Of GraduateAuditGroupDto)
            Dim listMaters As New List(Of GraduateAuditGroupDto)
            For Each enrollId As String In enrollment
                If (enrollId = Guid.Empty.ToString()) Then
                    Continue For
                End If
                Dim listEnroll As List(Of GraduateAuditGroupDto) = GetGraduateAuditByEnrollmentForCourseEquivalent(enrollId, gradeReps, includeHours)
                For Each groupDto As GraduateAuditGroupDto In listEnroll
                    If (IsNothing(groupDto.Level)) Then
                        listHeaders.Add(groupDto)
                    Else
                        Dim isNotthere As Boolean = True
                        For Each mater As GraduateAuditGroupDto In listMaters
                            If (mater.ReqId = groupDto.ReqId) Then
                                isNotthere = False
                                Exit For
                            End If
                        Next
                        If isNotthere = True Then
                            listMaters.Add(groupDto)
                        End If
                    End If
                Next
            Next
            ' Unify Header in one.
            Dim header As GraduateAuditGroupDto = GraduateAuditGroupDto.Factory()
            header.Descrip = "All Enrollments"
            For Each dto As GraduateAuditGroupDto In listHeaders
                header.Credits += dto.Credits
                header.CreditsEarned += dto.CreditsEarned
                header.CreditsRemaining += dto.CreditsRemaining
                header.Hours += dto.Hours
                header.HoursCompleted += dto.HoursCompleted
                header.HoursRemaining += dto.HoursRemaining
                header.Score += dto.Score
                header.Level = Nothing
            Next
            'Integrate in lisMaters
            listMaters.Insert(0, header)
            Return listMaters
        Else
            Dim listEnroll As List(Of GraduateAuditGroupDto) = GetGraduateAuditByEnrollmentForCourseEquivalent(stuEnrollId, gradeReps, includeHours)
            Return listEnroll
        End If
    End Function

    Public Shared Function GetHeaderTitles(ByVal auditInfo As GraduateAuditInfo) As List(Of String)
        'Construct the Header Title for the Summary table
        Dim headerTitles As List(Of String) = New List(Of String)()
        Dim score As String = If(auditInfo.StrGradesFormat.ToLower = "letter", "Grade", "Score")
        If auditInfo.StuEnrollmentId <> Guid.Empty.ToString() Then
            headerTitles.Add("Description")
            headerTitles.Add("Equivalent Course")
            headerTitles.Add("Credits")
            headerTitles.Add("Hours")
            headerTitles.Add(score)
            headerTitles.Add("Credits Earned")
            headerTitles.Add("Credits Remaining")
            headerTitles.Add("Hours Completed")
            headerTitles.Add("Hours Remaining")
        Else
            headerTitles.Add("Description")
            headerTitles.Add("Equivalent Course")
            headerTitles.Add("Total Credits")
            headerTitles.Add("Total Hours")
            headerTitles.Add(score)
            headerTitles.Add("Total Credits Earned")
            headerTitles.Add("Total Credits Remaining")
            headerTitles.Add("Total Hours Completed")
            headerTitles.Add("Total Hours Remaining")
        End If
        Return headerTitles
    End Function

    Public Shared Function GetHeaderData(ByVal dto As List(Of GraduateAuditGroupDto), ByVal objCredits As Decimal, ByVal objHours As Decimal) As GraduateAuditGroupDto
        Dim headerData As GraduateAuditGroupDto

        For Each groupDto As GraduateAuditGroupDto In From groupDto1 In dto Where groupDto1.Level Is Nothing
            headerData = groupDto
            Exit For
        Next
        If headerData Is Nothing Then
            Return headerData
        End If

        If headerData.CreditsEarned Is Nothing Then
            headerData.CreditsEarned = 0.0
        Else
            headerData.CreditsEarned = Math.Round(CType(headerData.CreditsEarned, Decimal), 2)
        End If
        If headerData.IsContinuingEd Then
            If headerData.CreditsRemaining Is Nothing Then
                headerData.CreditsRemaining = 0.0
            Else
                headerData.CreditsRemaining = Math.Round(CType(headerData.CreditsRemaining, Decimal), 2)
                headerData.CreditsRemaining = headerData.CreditsRemaining - objCredits
            End If
            headerData.HoursRemaining = headerData.HoursRemaining - objHours
        End If

        Return headerData
    End Function

    Public Shared Function CreateReportList(ByVal auditInfo As GraduateAuditInfo, ByRef headerTitles As IList(Of String), ByRef headerData As GraduateAuditGroupDto, ByRef detailDataList As IList(Of GraduateAuditGroupDto)) As List(Of GraduateAuditGroupDto)
        Dim datalist As List(Of String) = (From dto As ListItem In auditInfo.ItemCollection Select dto.Value).ToList()
        Dim dt As List(Of GraduateAuditGroupDto) = (New PortalGraduateAuditFacade).GetGraduateAuditEnrollmentForCourseEquivalent(auditInfo.StuEnrollmentId, auditInfo.GradeReps, auditInfo.IncludeHours, datalist)
        If dt.Count > 0 Then
            Dim transcript As New PortalTranscriptFacade
            Dim dsgradesysdetailids As List(Of String) = transcript.GetGradesSysDetailId()
            Dim objCredits As Decimal
            Dim objHours As Decimal
            If dsgradesysdetailids.Count > 0 Then
                objCredits += dt.Sum(Function(dto) (From detail In dsgradesysdetailids Where dto.GrdSysDetailId = detail).Sum(Function(detail) dto.Credits))
                objHours += dt.Sum(Function(dto) (From detail In dsgradesysdetailids Where dto.GrdSysDetailId = detail).Sum(Function(detail) dto.Hours))
            End If
            headerTitles = GetHeaderTitles(auditInfo) 'Create Headers
            headerData = GetHeaderData(dt, objCredits, objHours) 'Create Header Data
            detailDataList = GetDetailData(dt, auditInfo)
        End If
        Return dt
    End Function

    Private Shared Function GetDetailData(ByVal dt As List(Of GraduateAuditGroupDto), ByVal auditInfo As GraduateAuditInfo) As IList(Of GraduateAuditGroupDto)
        Dim resultlist As IList(Of GraduateAuditGroupDto) = New List(Of GraduateAuditGroupDto)
        For Each dr As GraduateAuditGroupDto In dt
            If IsNothing(dr.Level) Then
                Continue For
            End If
            If IsNothing(dr.Grade) Then
                dr.EquivCourseDesc = String.Empty
            End If

            dr.Credits = CType(IIf(IsNothing(dr.Credits), 0, dr.Credits), Decimal) 'Credits
            dr.Hours = CType(IIf(IsNothing(dr.Hours), 0, dr.Hours), Decimal) 'Hours

            If auditInfo.StrGradesFormat.ToLower = "letter" Then
                dr.Grade = CType(IIf(IsNothing(dr.Grade), "", dr.Grade), String)
            Else
                If dr.IsTransferGrade Then
                    dr.Grade = CType(IIf(IsNothing(dr.Grade), "", dr.Grade), String)
                Else
                    dr.Grade = CType(IIf(IsNothing(dr.Score), "", dr.Score), String)
                End If

            End If
            ''New Code Added By Vijay Ramteke For Rally Id DE1393 On December 22, 2010 
            Dim boolIsCourseALab As Boolean
            boolIsCourseALab = (New PortalGraduateAuditFacade).isCourseALabWorkOrLabHourCourse(dr.ReqId.ToString)
            If auditInfo.TranscripType.ToLower = "traditional_numeric" Then
                'check to make sure if the course is a lab work or course work

                If boolIsCourseALab = True Then
                    Dim decCredits As Decimal
                    Dim boolisCourseACombination As Boolean = CType((New PortalGraduateAuditFacade).isCourseALabWorkOrLabHourCourseCombination(dr.ReqId).ToString, Boolean)
                    If boolisCourseACombination = False Then
                        Try
                            decCredits = CType((New PortalGraduateAuditFacade).ComputeWithCreditsPerService(auditInfo.StuEnrollmentId, auditInfo.Addcreditsbyservice.ToLower, dr.ReqId).ToString, Decimal)
                        Catch ex As Exception
                            decCredits = 0
                        End Try
                        dr.CreditsEarned = Math.Round(decCredits, 2)
                    End If
                End If
            Else
                If boolIsCourseALab = True Then
                    Dim decCredits As Decimal
                    Dim boolisCourseACombination As Boolean = (New PortalGraduateAuditFacade).isCourseALabWorkOrLabHourCourseCombination(dr.ReqId.ToString)
                    If boolisCourseACombination = False Then
                        Try
                            decCredits = (New PortalGraduateAuditFacade).ComputeWithCreditsPerService(auditInfo.StuEnrollmentId, auditInfo.Addcreditsbyservice.ToLower, dr.ReqId.ToString)
                        Catch ex As Exception
                            decCredits = 0
                        End Try
                        dr.CreditsEarned = Math.Round(decCredits, 2)
                    End If
                End If
            End If

            dr.CreditsEarned = CType(IIf(IsNothing(dr.CreditsEarned), 0, dr.CreditsEarned), Decimal) 'Credits Earned
            dr.CreditsRemaining = CType(IIf(IsNothing(dr.CreditsRemaining), 0, dr.CreditsRemaining), Decimal) ' Credit Remaining

            Dim isTransferGrade As Boolean = False
            If auditInfo.Dsgradesysdetailids.Count > 0 Then
                Dim gradeSysDetailId As String
                gradeSysDetailId = dr.GrdSysDetailId.ToString
                Dim i As Integer
                isTransferGrade = False
                For i = 0 To auditInfo.Dsgradesysdetailids.Count - 1
                    If gradeSysDetailId = auditInfo.Dsgradesysdetailids(i) Then
                        isTransferGrade = True
                        Exit For
                    End If
                Next

            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim tempCreditRemained As Decimal

            If auditInfo.TranscripType.ToLower = "traditional_numeric" Then
                'Credits Remaining 
                Dim parcial As Decimal = dr.Credits - dr.CreditsEarned
                tempCreditRemained = parcial
                'If Not dr.CreditsEarned Is Nothing Then lbl7.Text = CType(parcial, String)
                'Dim boolIsCourseALab As Boolean = False
                'boolIsCourseALab = (New PortalGraduateAuditFacade).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                If boolIsCourseALab = True Then

                    'If Not IsNothing(dr.CreditsEarned) Then
                    If dr.CreditsEarned >= dr.Credits Then
                        tempCreditRemained = 0 ' For clinic service course if Credits Earned is equal to credits then set Credits Remaining to 0
                    Else
                        tempCreditRemained = parcial 'CType(IIf(IsNothing(dr.CreditsRemaining), "0", dr.Credits.ToString - dr.CreditsEarned.ToString), String)
                    End If
                    'End If
                End If
            Else

                'Dim boolIsCourseALab As Boolean = False
                'boolIsCourseALab = (New PortalGraduateAuditFacade).isCourseALabWorkOrLabHourCourse(dr("ReqId").ToString)
                'If Not IsNothing(dr.CreditsEarned) Then lbl7.Text = CType(IIf(IsNothing(dr.CreditsRemaining), "0", dr.Credits.ToString - dr.CreditsEarned.ToString), String)
                Dim parcial As Decimal = dr.Credits - dr.CreditsEarned
                If isTransferGrade = True Then
                    tempCreditRemained = 0
                    'lbl7.Text = CType(IIf(IsNothing(dr.CreditsRemaining), "0", "0.00"), String)
                Else
                    tempCreditRemained = dr.CreditsRemaining
                    'lbl7.Text = CType(IIf(IsNothing(dr.CreditsRemaining), "0", dr.CreditsRemaining.ToString()), String)
                End If
                If boolIsCourseALab = True Then
                    'If Not IsNothing(dr.CreditsEarned) Then
                    If dr.CreditsEarned >= dr.Credits Then
                        tempCreditRemained = 0 'CType(IIf(IsNothing(dr.CreditsRemaining), "0", 0), String) ' For clinic service course if Credits Earned is equal to credits then set Credits Remaining to 0
                    ElseIf dr.CreditsEarned < dr.Credits And auditInfo.Addcreditsbyservice.ToLower() = "yes" Then
                        tempCreditRemained = parcial ' dr.Credits - dr.CreditsEarned), String)
                    End If
                    'End If
                End If

                If boolIsCourseALab = True And auditInfo.Addcreditsbyservice.ToLower() = "no" Then
                    'If Not IsNothing(dr.CreditsEarned) Then
                    If dr.CreditsEarned >= dr.Credits Then
                        tempCreditRemained = 0 ' For clinic service course if Credits Earned is equal to credits then set Credits Remaining to 0
                    End If
                    'End If
                End If
            End If
            dr.CreditsRemaining = tempCreditRemained
            Dim tempHoursCompleted As Decimal
            Dim tempHoursRemaining As Decimal

            'Hours Completed and 'Hours Remaining
            If boolIsCourseALab = True Then
                Dim boolIsClinicCompletlySatisfied As Boolean = (New PortalExamsFacade).isClinicCourseCompletlySatisfied(auditInfo.StuEnrollmentId, 500, dr.ReqId.ToString)
                Dim boolRemaining As Boolean = CType((New PortalExamsFacade).IsRemainingLabCount(auditInfo.StuEnrollmentId, 500, dr.ReqId).ToString, Boolean)
                Dim boolIsCoursePartOfCombination As String = (New PortalExamsFacade).isCourseALabWorkOrLabHourCourseCombination(dr.ReqId).ToString
                Dim boolIsCombinationSatisfied As Boolean = False
                Dim boolFinalGradePosted As Boolean = False
                Dim intFinalGradePosted As Integer
                If boolIsCoursePartOfCombination = True Then
                    boolIsCombinationSatisfied = (New PortalExamsFacade).isClinicCourseCompletlySatisfiedCombination(auditInfo.StuEnrollmentId, 500, dr.ReqId.ToString)
                    intFinalGradePosted = (New PortalTransferGradeFacade).CheckIfFinalGradePostedForCombination(auditInfo.StuEnrollmentId, dr.ReqId.ToString)
                    If intFinalGradePosted >= 1 Then
                        boolFinalGradePosted = True
                    Else
                        boolFinalGradePosted = False
                    End If
                End If


                If boolRemaining = False Then
                    If isTransferGrade = True Then
                        tempHoursCompleted = dr.Hours '.ToString() '(dr.IsNull("Hours"), "0", "0.00")
                        tempHoursRemaining = 0
                    Else
                        tempHoursCompleted = dr.Hours ' IIf(dr.IsNull("Hours"), "0", dr("Hours").ToString)
                        tempHoursRemaining = 0
                    End If
                Else
                    If isTransferGrade = True Then
                        tempHoursCompleted = 0 'CType(IIf(IsNothing(dr.HoursCompleted), "0", "0.00"), String)
                        tempHoursRemaining = 0
                    Else
                        tempHoursCompleted = CType(IIf(IsNothing(dr.HoursCompleted), 0, dr.HoursCompleted), Decimal)
                        tempHoursRemaining = CType(IIf(IsNothing(dr.HoursRemaining), 0, dr.HoursRemaining), Decimal)
                    End If
                End If
                If boolIsClinicCompletlySatisfied = False Then
                    tempHoursCompleted = "0" 'IIf(dr.IsNull("Hours"), "0", "0.00")
                    tempHoursRemaining = CType(IIf(IsNothing(dr.HoursRemaining), 0, dr.Hours), Decimal)

                End If
                If boolIsClinicCompletlySatisfied = True And boolIsCoursePartOfCombination = True And boolIsCombinationSatisfied = False Then
                    tempHoursCompleted = "0" ' IIf(dr.IsNull("Hours"), "0", "0.00")
                    tempHoursRemaining = CType(IIf(IsNothing(dr.HoursRemaining), 0, dr.Hours), Decimal)
                End If
                If boolIsClinicCompletlySatisfied = True And boolIsCoursePartOfCombination = True And boolIsCombinationSatisfied = True And boolFinalGradePosted = False Then
                    tempHoursCompleted = "0" 'IIf(dr.IsNull("Hours"), "0", "0.00")
                    tempHoursRemaining = CType(IIf(IsNothing(dr.HoursRemaining), 0, dr.Hours), Decimal)
                ElseIf boolIsClinicCompletlySatisfied = True And boolIsCoursePartOfCombination = True And boolIsCombinationSatisfied = False And boolFinalGradePosted = True Then
                    tempHoursCompleted = CType(IIf(IsNothing(dr.Hours), 0, dr.Hours), String)
                    tempHoursRemaining = 0
                ElseIf boolIsClinicCompletlySatisfied = True And boolIsCoursePartOfCombination = True And boolIsCombinationSatisfied = True And boolFinalGradePosted = True Then
                    If Not IsNothing(dr.HoursRemaining) Then
                        If dr.HoursRemaining > 0 Then
                            tempHoursCompleted = 0 'dr.Hours'.ToString() 'IIf(dr.IsNull("Hours"), "0", "0.00")
                            tempHoursRemaining = CType(IIf(IsNothing(dr.HoursRemaining), 0, dr.Hours), Decimal)
                        Else
                            tempHoursCompleted = IIf(IsNothing(dr.Hours), 0, dr.Hours)
                            tempHoursRemaining = 0 'CType(IIf(IsNothing(dr.HoursRemaining), "0", "0.00"), String)
                        End If

                    End If

                End If
            Else
                If isTransferGrade = True Then
                    tempHoursCompleted = 0 ' CType(IIf(IsNothing(dr.HoursCompleted), "0", "0.00"), String)
                    tempHoursRemaining = 0 'CType(IIf(IsNothing(dr.HoursRemaining), "0", "0.00"), String)
                Else
                    tempHoursCompleted = CType(IIf(IsNothing(dr.HoursCompleted), 0, dr.HoursCompleted), Decimal)
                    tempHoursRemaining = CType(IIf(IsNothing(dr.HoursRemaining), 0, dr.HoursRemaining), Decimal)
                End If
            End If
            dr.HoursCompleted = tempHoursCompleted
            dr.HoursRemaining = tempHoursRemaining
            resultlist.Add(dr)
        Next
        Return resultlist
    End Function

    ''' <summary>
    ''' Calculate the average of a determinate colum in a table.
    ''' The ds must have a datatable named Result.
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <param name="fieldToCalculate"></param>
    ''' <param name="reqId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function CalculateAverage(ByRef ds As DataSet, fieldToCalculate As String, ByVal reqId As String) As Decimal
        Dim score As Decimal = 0
        If ds.Tables("Results").Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables("Results").Rows
                If row.Item("ReqId").ToString() = reqId AndAlso IsNothing(row.Item(fieldToCalculate)) = False AndAlso String.IsNullOrEmpty((row.Item(fieldToCalculate).ToString())) = False Then
                    Dim sco As Decimal
                    If Decimal.TryParse(row.Item(fieldToCalculate).ToString(), sco) Then
                        score += sco
                    End If
                End If
            Next
            score = score / ds.Tables("Results").Rows.Count
        End If
        Return score
    End Function

End Class
