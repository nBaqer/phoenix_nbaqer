Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.AdvantageV1.BusinessFacade.FIC_C3CommonObject

Public Class FIC_C3DetailObject

	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "FIC_C3Detail"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = FIC_C3DetailObjectDB.GetReportDatasetRaw(RptParamInfo)

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add("SSN", GetType(String))
			.Add("Name", GetType(String))
			.Add("Ind_Male_Appl", GetType(String))
			.Add("Ind_Male_Admit", GetType(String))
			.Add("Ind_Male_FT", GetType(String))
			.Add("Ind_Male_PT", GetType(String))
			.Add("Ind_Female_Appl", GetType(String))
			.Add("Ind_Female_Admit", GetType(String))
			.Add("Ind_Female_FT", GetType(String))
			.Add("Ind_Female_PT", GetType(String))
		End With

		Dim drLead As DataRow
		Dim drRpt As DataRow
		Dim IndPrefix As String
		Dim RowPtr As Integer

		' pre-process raw data to obtain prepared raw data for processing, create DataView to 
        '	use in processing
        If dsRaw.Tables(TblNameEnrollments).Rows.Count <= 0 Then
            Return dsRpt
        End If
        Dim dvStuEnrollInfo As New DataView(PreProcessRawData(dsRaw))

        ' loop thru Leads in raw dataset and process each
        For Each drLead In dsRaw.Tables(TblNameLeads).Rows
            drRpt = dsRpt.Tables(MainTableName).NewRow

            ' determine gender of current Lead, set appropriate prefix to use when referring to 
            '	indicator columns in report DataRow
            If drLead("GenderDescrip").ToString.ToLower <> "women" Then
                IndPrefix = "Ind_Male_"
            Else
                IndPrefix = "Ind_Female_"
            End If

            ' set name of current Lead, truncating to appropriate report lengths
            drRpt("Name") = Left(drLead("LastName").ToString, LastNameSize) & ", " & _
                Left(drLead("FirstName").ToString, FirstNameSize) & " " & _
                Left(drLead("MiddleName").ToString, MiddleNameSize)

            ' indicate Lead is an applicant 
            drRpt(IndPrefix & "Appl") = "X"

            ' if a record in Enrollment info contains current LeadId, get report info from that record
            dvStuEnrollInfo.Sort = "LeadId"
            RowPtr = dvStuEnrollInfo.Find(drLead("LeadId"))
            If RowPtr <> -1 Then
                ' record Lead's SSN
                drRpt("SSN") = SetSSN(dvStuEnrollInfo(RowPtr)("SSN").ToString)
                ' indicate Lead was admitted 
                drRpt(IndPrefix & "Admit") = "X"
                ' indicate Lead as either Full- or Part-time
                If dvStuEnrollInfo(RowPtr)("AttendTypeDescrip") = AttendTypeFullTime Then
                    drRpt(IndPrefix & "FT") = "X"
                Else
                    drRpt(IndPrefix & "PT") = "X"
                End If

                ' if no record in Enrollment info contains current LeadId, see if current Lead has an SSN
            ElseIf drLead("SSN").ToString <> "" Then
                ' if current Lead has an SSN, and a record in Enrollment info contains current Lead's SSN, 
                '	get report info from that record 
                dvStuEnrollInfo.Sort = "SSN"
                RowPtr = dvStuEnrollInfo.Find(drLead("SSN"))
                If RowPtr <> -1 Then
                    ' record Lead's SSN
                    drRpt("SSN") = SetSSN(dvStuEnrollInfo(RowPtr)("SSN").ToString)
                    ' indicate Lead was admitted 
                    drRpt(IndPrefix & "Admit") = "X"
                    ' indicate Lead as either Full- or Part-time
                    If dvStuEnrollInfo(RowPtr)("AttendTypeDescrip") = AttendTypeFullTime Then
                        drRpt(IndPrefix & "FT") = "X"
                    Else
                        drRpt(IndPrefix & "PT") = "X"
                    End If
                    ' current Lead has an SSN, but no record in Enrollment info contains current Lead's SSN,
                    '	so just record SSN
                Else
                    drRpt("SSN") = SetSSN(drLead("SSN").ToString)
                End If

                ' no record in Enrollment info contains current LeadId, and current Lead doesn't have an SSN,
                '	so call function to record default blank indicator for SSN
            Else
                drRpt("SSN") = SetSSN(drLead("SSN").ToString)

            End If

            dsRpt.Tables(MainTableName).Rows.Add(drRpt)
        Next

        ' return the Report DataSet
        Return dsRpt

    End Function

End Class
