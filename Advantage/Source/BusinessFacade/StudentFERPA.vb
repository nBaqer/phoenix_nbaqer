﻿Public Class StudentFERPA
    Public Function GetFERPACategories(ByVal showActiveOnly As String, ByVal campusid As String) As DataTable

        '   Instantiate DAL component
        With New StudentFERPADB

            '   get the dataset with all BankCodes
            Return .GetFERPAcategories(showActiveOnly, campusid)

        End With

    End Function
    Public Function GetFERPAEntities(ByVal showActiveOnly As String, ByVal campusid As String) As DataTable

        '   Instantiate DAL component
        With New StudentFERPADB

            '   get the dataset with all BankCodes
            Return .GetFERPAEntities(showActiveOnly, campusid)

        End With

    End Function
    Public Function GetFERPAInfo(ByVal ferpaId As String) As FERPAInfo

        '   Instantiate DAL component
        With New StudentFERPADB

            '   get the BankInfo
            Return .GetFERPAInfo(ferpaId)

        End With

    End Function
    Public Function GetFERPAEntityInfo(ByVal ferpaId As String) As FERPAInfo

        '   Instantiate DAL component
        With New StudentFERPADB

            '   get the BankInfo
            Return .GetFERPAEntityInfo(ferpaId)

        End With

    End Function
    
    Public Function UpdateFERPAInfo(ByVal ferpa As FERPAInfo, ByVal FERPAPages As String()) As String

        '   Instantiate DAL component
        With New StudentFERPADB

            '   If it is a new account do an insert. If not, do an update
            If Not (ferpa.IsInDB = True) Then
                '   return integer with insert results
                Return .AddFERPAInfo(ferpa, FERPAPages)
            Else
                '   return integer with update results
                Return .UpdateFERPAInfo(ferpa, FERPAPages)
            End If

        End With

    End Function
    Public Function UpdateFERPAEntityInfo(ByVal ferpa As FERPAInfo) As String

        '   Instantiate DAL component
        With New StudentFERPADB

            '   If it is a new account do an insert. If not, do an update
            If Not (ferpa.IsInDB = True) Then
                '   return integer with insert results
                Return .AddFERPAEntityInfo(ferpa)
            Else
                '   return integer with update results
                Return .UpdateFERPAEntityInfo(ferpa)
            End If

        End With

    End Function
    Public Function UpdateFERPAPolicyInfo(ByVal ferpa As FERPAPolicyInfo) As String

        '   Instantiate DAL component
        With New StudentFERPADB

            '   If it is a new account do an insert. If not, do an update
            
            Return .AddFERPAPolicyInfo(ferpa)
          

        End With

    End Function
    Public Function DeleteFERPAInfo(ByVal ferpaID As String) As String

        '   Instantiate DAL component
        With New StudentFERPADB


            Return .DeleteFERPAInfo(ferpaID)

        End With

    End Function
    Public Function DeleteFERPAEntityInfo(ByVal ferpaID As String) As String

        '   Instantiate DAL component
        With New StudentFERPADB


            Return .DeleteFERPAEntityInfo(ferpaID)

        End With

    End Function
    Public Function GetFERPAPages(ByVal FERPACategoryID As String) As DataTable

        '   Instantiate DAL component
        With New StudentFERPADB

            '   get the dataset with all BankCodes
            Return .GetFERPAPages(FERPACategoryID)

        End With

    End Function
    Public Function GetStudentPagesByModulePageName() As DataTable
        With New StudentFERPADB

            '   get the dataset with all BankCodes
            Return .GetStudentPagesByModulePageName()

        End With

    End Function
    Public Function GetFERPAPolicy(ByVal FERPAEntityID As String, ByVal StudentId As String) As DataTable

        '   Instantiate DAL component
        With New StudentFERPADB

            '   get the dataset with all BankCodes
            Return .GetFERPAPolicy(FERPAEntityID, StudentId)

        End With

    End Function
    Public Function GetFERPAPermission(ByVal resourceId As Integer, ByVal StudentId As String) As DataTable
        With New StudentFERPADB

            '   get the dataset with all BankCodes
            Return .GetFERPAPermission(resourceId, StudentId)

        End With
    End Function
    Public Function HasFERPAPermission(ByVal resourceId As Integer, ByVal StudentId As String) As Boolean
        Dim rtn As Boolean = False
        With New StudentFERPADB
            If .IsStudentPage(resourceId).Rows.Count > 0 Then
                If .GetFERPAPermission(resourceId, StudentId).Rows.Count > 0 Then
                    Return True
                End If
            End If

        End With
        Return rtn
    End Function
    Public Function HasGivenFERPAPermission(ByVal StudentId As String) As Boolean
        Dim rtn As Boolean = False

        Try

            With New StudentFERPADB
                Dim table as DataTable = .GetFERPAPermission(0, StudentId)
                If table is Nothing OrElse table.Rows.Count = 0 Then
                    Return rtn
                End If
                'If .GetFERPAPermission(0, StudentId).Rows.Count > 0 Then
                    Return True
                'End If

            End With
        Catch ex As Exception
            Return rtn
        End Try



    End Function

End Class
