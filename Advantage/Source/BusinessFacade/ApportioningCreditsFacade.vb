﻿Imports FAME.AdvantageV1.DataAccess
Imports FAME.AdvantageV1.Common
Public Class ApportioningCreditsFacade

    Public Shared Function GetFASAP(ByVal CampusGroupId As Guid, ByVal ProgramVersionId As Guid, ByVal EnrollmentStatusId As Guid, ByVal AsOfDate As DateTime) As Integer
        Dim ds As New DataSet
        ds = ApportioningCreditsDB.GetFASSAP(CampusGroupId, ProgramVersionId, EnrollmentStatusId, AsOfDate)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return 1
        Else
            Return 0
        End If
    End Function
    Public Shared Function GetApportioningCreditsCount(ByVal CampusGroupId As Guid, ByVal ProgramVersionId As Guid, ByVal EnrollmentStatusId As Guid, ByVal AsOfDate As DateTime) As Integer
        Dim ds As New DataSet
        ds = ApportioningCreditsDB.GetApportioningCreditsCount(CampusGroupId, ProgramVersionId, EnrollmentStatusId, AsOfDate)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Return 1
        Else
            Return 0
        End If
    End Function
End Class
