
Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllADetailObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SFE_AllADetail"

	Private dsRaw As New DataSet
	Private dtRptRaw As New DataTable
	Private dsRpt As New DataSet

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SFE_AllADetailObjectDB.GetReportDatasetRaw(RptParamInfo)

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		' pre-process raw data, first process student enrollment info, then process with raw student 
		'	data to obtain prepared raw data for processing
		dtRptRaw = SFE_AllACommonObject.PreProcessRawData(dsRaw.Tables(TblNameStudents), IPEDSFacade.GetProcessedEnrollInfo(dsRaw))

		' table names for different parts of report data
		Dim TableNamePart1 As String = MainTableName & "Part1"
		Dim TableNamePart2 As String = MainTableName & "Part2"
		Dim TableNamePart3 As String = MainTableName & "Part3"
		Dim TableNamePart4 As String = MainTableName & "Part4"

		' get IPEDS Gender info
		Dim dtGenders As DataTable = IPEDSFacade.GetIPEDSGenderInfo

		' get IPEDS Ethnic info
		Dim dtEthCodes As DataTable = IPEDSFacade.GetIPEDSEthnicInfo


		' process data for report Parts 1 and 2

		' create table to hold final report data for part 1
		With dsRpt.Tables.Add(TableNamePart1).Columns
			.Add(RptStuIdentifierColName, GetType(System.String))
			.Add("Name", GetType(System.String))
			.Add("GenderDescrip", GetType(System.String))
			.Add("EthCodeDescrip", GetType(System.String))
			.Add("Ind_FirstTime", GetType(System.String))
			.Add("Ind_OtherDegCert", GetType(System.String))
			.Add("Ind_NonDegCert", GetType(System.String))
		End With

		' create table to hold final report data for part 2
		With dsRpt.Tables.Add(TableNamePart2).Columns
			.Add(RptStuIdentifierColName, GetType(System.String))
			.Add("Name", GetType(System.String))
			.Add("GenderDescrip", GetType(System.String))
			.Add("EthCodeDescrip", GetType(System.String))
			.Add("Ind_FirstTime", GetType(System.String))
			.Add("Ind_OtherDegCert", GetType(System.String))
			.Add("Ind_NonDegCert", GetType(System.String))
		End With

		' loop thru genders, and ethnic groups for each gender, and process matching records in raw dataset
		For Each drGenders As DataRow In dtGenders.Rows
			For Each drEthCodes As DataRow In dtEthCodes.Rows
				ProcessRowsParts1And2(drGenders("GenderDescrip"), drEthCodes("EthCodeDescrip"), _
									  ProgTypeUndergraduate, AttendTypeFullTime, dsRpt.Tables(TableNamePart1))
				ProcessRowsParts1And2(drGenders("GenderDescrip"), drEthCodes("EthCodeDescrip"), _
									  ProgTypeUndergraduate, AttendTypePartTime, dsRpt.Tables(TableNamePart2))
			Next
		Next


		' process data for report parts 3 and 4

		' create table to hold final report data for part 3
		With dsRpt.Tables.Add(TableNamePart3).Columns
			.Add(RptStuIdentifierColName, GetType(System.String))
			.Add("Name", GetType(System.String))
			.Add("GenderDescrip", GetType(System.String))
			.Add("EthCodeDescrip", GetType(System.String))
			.Add("Ind_FullTime", GetType(System.String))
			.Add("Ind_PartTime", GetType(System.String))
		End With

		' create table to hold final report data for part 4
		With dsRpt.Tables.Add(TableNamePart4).Columns
			.Add(RptStuIdentifierColName, GetType(System.String))
			.Add("Name", GetType(System.String))
			.Add("GenderDescrip", GetType(System.String))
			.Add("EthCodeDescrip", GetType(System.String))
			.Add("Ind_FullTime", GetType(System.String))
			.Add("Ind_PartTime", GetType(System.String))
		End With

		' loop thru genders, and ethnic groups for each gender, and process matching records in raw dataset
		For Each drGenders As DataRow In dtGenders.Rows
			For Each drEthCodes As DataRow In dtEthCodes.Rows
				ProcessRowsParts3And4(drGenders("GenderDescrip"), drEthCodes("EthCodeDescrip"), _
									  ProgTypeGraduate, dsRpt.Tables(TableNamePart3))
				ProcessRowsParts3And4(drGenders("GenderDescrip"), drEthCodes("EthCodeDescrip"), _
									  ProgTypeFirstProf, dsRpt.Tables(TableNamePart4))
			Next
		Next

		' return report dataset
		Return dsRpt

	End Function

	Private Sub ProcessRowsParts1And2(ByVal GenderDescrip As String, ByVal EthCodeDescrip As String, ByVal ProgTypeDescrip As String, ByVal AttendTypeDescrip As String, ByRef dtRpt As DataTable)
	' Create data for report parts 1 and 2

		' create dataview from students in raw dataset, filtered according to the 
		'	passed in gender, ethnic code, program type, and attendance type
		Dim dvRptRaw As New DataView(dtRptRaw)
		dvRptRaw.RowFilter = "GenderDescrip LIKE '" & GenderDescrip & "' AND " & _
							 "EthCodeDescrip LIKE '" & EthCodeDescrip & "' AND " & _
							 "ProgTypeDescrip LIKE '" & ProgTypeDescrip & "' AND " & _
							 "AttendTypeDescrip LIKE '" & AttendTypeDescrip & "'"

		Dim i As Integer
		Dim drRpt As DataRow

		' if dataview has records, loop thru records and process raw data to create report records
		If dvRptRaw.Count > 0 Then
			For i = 0 To dvRptRaw.Count - 1
				drRpt = dtRpt.NewRow

				' set Student Id and name 
				IPEDSFacade.SetStudentInfo(dvRptRaw(i), drRpt)

				' get descriptions for gender and ethnic ids
				drRpt("GenderDescrip") = GenderDescrip
				drRpt("EthCodeDescrip") = EthCodeDescrip

				' set indicators as appropriate
				drRpt("Ind_FirstTime") = DBNull.Value
				drRpt("Ind_OtherDegCert") = DBNull.Value
				drRpt("Ind_NonDegCert") = DBNull.Value
				Select Case dvRptRaw(i)("DegCertSeekingDescrip").ToString
					Case DegCertSeekingFirstTime
						drRpt("Ind_FirstTime") = "X"
					Case DegCertSeekingOtherDegree
						drRpt("Ind_OtherDegCert") = "X"
					Case DegCertSeekingNonDegree
						drRpt("Ind_NonDegCert") = "X"
				End Select

				' add new report row to report table
				dtRpt.Rows.Add(drRpt)
			Next

		' if dataview has no records, create record with descriptions for gender and ethnic code, 
		'	NULL data otherwise
		Else
			drRpt = dtRpt.NewRow

			drRpt(RptStuIdentifierColName) = DBNull.Value
			drRpt("Name") = DBNull.Value
			drRpt("GenderDescrip") = GenderDescrip
			drRpt("EthCodeDescrip") = EthCodeDescrip
			drRpt("Ind_FirstTime") = DBNull.Value
			drRpt("Ind_OtherDegCert") = DBNull.Value
			drRpt("Ind_NonDegCert") = DBNull.Value

			' add empty report row to report table
			dtRpt.Rows.Add(drRpt)

		End If

	End Sub

	Private Sub ProcessRowsParts3And4(ByVal GenderDescrip As String, ByVal EthCodeDescrip As String, ByVal ProgTypeDescrip As String, ByRef dtRpt As DataTable)
	' create data for report parts 3 and 4

		' create dataview from students in raw dataset, filtered according to the 
		'	passed in gender, ethnic code and program type
		Dim dvRptRaw As New DataView(dtRptRaw)
		dvRptRaw.RowFilter = "GenderDescrip LIKE '" & GenderDescrip & "' AND " & _
							 "EthCodeDescrip LIKE '" & EthCodeDescrip & "' AND " & _
							 "ProgTypeDescrip LIKE '" & ProgTypeDescrip & "'"
		Dim i As Integer
		Dim drRpt As DataRow

		' if dataview has records, loop thru records and process raw data to create report records
		If dvRptRaw.Count > 0 Then
			For i = 0 To dvRptRaw.Count - 1
				drRpt = dtRpt.NewRow

				' set Student Id and name 
				IPEDSFacade.SetStudentInfo(dvRptRaw(i), drRpt)

				' get descriptions for gender and ethnic ids
				drRpt("GenderDescrip") = GenderDescrip
				drRpt("EthCodeDescrip") = EthCodeDescrip

				' set indicators as appropriate
				drRpt("Ind_FullTime") = DBNull.Value
				drRpt("Ind_PartTime") = DBNull.Value
				Select Case dvRptRaw(i)("AttendTypeDescrip").ToString
					Case AttendTypeFullTime
						drRpt("Ind_FullTime") = "X"
					Case AttendTypePartTime
						drRpt("Ind_PartTime") = "X"
				End Select

				' add new report row to report table
				dtRpt.Rows.Add(drRpt)
			Next

		' if dataview has no records, create record with descriptions for gender and ethnic code, 
		'	NULL data otherwise
		Else
			drRpt = dtRpt.NewRow

			drRpt(RptStuIdentifierColName) = DBNull.Value
			drRpt("Name") = DBNull.Value
			drRpt("GenderDescrip") = GenderDescrip
			drRpt("EthCodeDescrip") = EthCodeDescrip
			drRpt("Ind_FullTime") = DBNull.Value
			drRpt("Ind_PartTime") = DBNull.Value

			' add empty report row to report table
			dtRpt.Rows.Add(drRpt)

		End If

	End Sub

End Class