Public Class GradingPolicyFactory

    Public Shared Function CreateGradingPolicyObj(ByVal grdPolicy As GradingPolicyEnum) As GradingPolicyStrategyPattern
        Select Case grdPolicy
            Case GradingPolicyEnum.DropLowest
                Return New DropLowest
            Case GradingPolicyEnum.Best
                Return New Best
            Case GradingPolicyEnum.Latest
                Return New Latest
        End Select
    End Function

End Class
