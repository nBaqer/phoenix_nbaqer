﻿Public Class LeadTransactionsFacade

    Public Function PostLeadTransactions(ByVal PostLeadPaymentInfo As PostLeadPaymentInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New LeadTransactionsDB

            '   return integer with insert results
            Return .PostLeadTransactions(PostLeadPaymentInfo, user)

        End With

    End Function

    'returns only trans codes attached to systrancode 18 - applicant fee
    Public Function GetApplicantChargeDescriptions(ByVal showActiveOnly As String, ByVal campusid As Guid) As DataSet

        '   Instantiate DAL component
        With New LeadTransactionsDB

            '   get the dataset with all BankCodes
            Return .GetApplicantChargeDescriptions(showActiveOnly, campusid)

        End With

    End Function


End Class
