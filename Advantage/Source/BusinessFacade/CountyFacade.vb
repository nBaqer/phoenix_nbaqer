' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' CountyFacade.vb
'
' CountyFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class CountyFacade
    Public Function GetAllCounty() As DataSet

        '   Instantiate DAL component
        With New CountyDB

            '   get the dataset with all States
            Return .GetAllCounty()

        End With

    End Function
    Public Function GetAllCitizenShip() As DataSet
        With New CountyDB
            Return .GetAllCitizenShip()

        End With
    End Function
    Public Function GetDefaultCountry() As String
        With New CountyDB
            Return .GetDefaultCountry
        End With
    End Function
End Class
