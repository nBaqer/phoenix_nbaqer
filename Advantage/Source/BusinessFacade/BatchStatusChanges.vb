Public Class BatchChangeFromFutureStart
    Private ReadOnly mToStatuses As New ArrayList

    Public Sub New()
        MyBase.New()

        ' mToStatuses.Add(StateEnum.CurrentlyAttending)
        mToStatuses.Add(StateEnum.NoStart)
        mToStatuses.Add(StateEnum.FutureStart)
    End Sub
    Public ReadOnly Property ToStatuses()
        Get
            Return mToStatuses
        End Get
    End Property
End Class
Public Class BatchChangeFromLeaveOfAbsence
    Private m_ToStatuses As New ArrayList

    Public Sub New()
        MyBase.New()

        m_ToStatuses.Add(StateEnum.CurrentlyAttending)
    End Sub
    Public ReadOnly Property ToStatuses()
        Get
            Return m_ToStatuses
        End Get
    End Property
End Class
Public Class BatchChangeFromCurrentlyAttending
    Private m_ToStatuses As New ArrayList

    Public Sub New()
        MyBase.New()

        m_ToStatuses.Add(StateEnum.Graduated)
        m_ToStatuses.Add(StateEnum.Externship)
        m_ToStatuses.Add(StateEnum.CurrentlyAttending)
        m_ToStatuses.Add(StateEnum.FutureStart)
        m_ToStatuses.Add(StateEnum.NoStart)
        'm_ToStatuses.Add(StateEnum.TransferOut)
    End Sub
    Public ReadOnly Property ToStatuses()
        Get
            Return m_ToStatuses
        End Get
    End Property
End Class
Public Class BatchChangeFromDropped
    Private m_ToStatuses As New ArrayList

    Public Sub New()
        MyBase.New()

        'm_ToStatuses.Add(StateEnum.CurrentlyAttending)
        m_ToStatuses.Add(StateEnum.FutureStart)
        'm_ToStatuses.Add(StateEnum.Dropped)

        'm_ToStatuses.Add(StateEnum.ReEntry)
    End Sub
    Public ReadOnly Property ToStatuses()
        Get
            Return m_ToStatuses
        End Get
    End Property
End Class
Public Class BatchChangeFromTransferred
    Private m_ToStatuses As New ArrayList

    Public Sub New()
        MyBase.New()

        m_ToStatuses.Add(StateEnum.CurrentlyAttending)
        m_ToStatuses.Add(StateEnum.FutureStart)
        m_ToStatuses.Add(StateEnum.TransferOut)
        'm_ToStatuses.Add(StateEnum.ReEntry)
    End Sub
    Public ReadOnly Property ToStatuses()
        Get
            Return m_ToStatuses
        End Get
    End Property
End Class

''' <summary>
''' Define the status possible to jump from NoStart
''' </summary>
''' <remarks></remarks>
Public Class BatchChangeFromNoStart
    Private ReadOnly mToStatuses As New ArrayList

    Public Sub New()
        MyBase.New()

        mToStatuses.Add(StateEnum.FutureStart)
        mToStatuses.Add(StateEnum.NoStart)
        ' m_ToStatuses.Add(StateEnum.CurrentlyAttending)
    End Sub
    Public ReadOnly Property ToStatuses()
        Get
            Return mToStatuses
        End Get
    End Property
End Class

Public Class BatchChangeFromSuspended
    Private m_ToStatuses As New ArrayList

    Public Sub New()
        MyBase.New()

        m_ToStatuses.Add(StateEnum.CurrentlyAttending)
    End Sub
    Public ReadOnly Property ToStatuses()
        Get
            Return m_ToStatuses
        End Get
    End Property
End Class

Public Class BatchChangeFromGraduated
    Private m_ToStatuses As New ArrayList

    Public Sub New()
        MyBase.New()

        'm_ToStatuses.Add(StateEnum.CurrentlyAttending)
        m_ToStatuses.Add(StateEnum.Graduated)
    End Sub
    Public ReadOnly Property ToStatuses()
        Get
            Return m_ToStatuses
        End Get
    End Property
End Class

Public Class BatchChangeFromExternship
    Private m_ToStatuses As New ArrayList
    Public Sub New()
        MyBase.New()
        m_ToStatuses.Add(StateEnum.Graduated)
        m_ToStatuses.Add(StateEnum.CurrentlyAttending)
        'm_ToStatuses.Add(StateEnum.Suspended) 'Mantis 16814
    End Sub
    Public Sub New(ByVal user As String)
        MyBase.New()
        m_ToStatuses.Add(StateEnum.Graduated)
        m_ToStatuses.Add(StateEnum.CurrentlyAttending)

        'm_ToStatuses.Add(StateEnum.Suspended) 'Mantis 16814
        If user = "sa" Then
            m_ToStatuses.Add(StateEnum.LeaveOfAbsence)
        End If
    End Sub
    Public ReadOnly Property ToStatuses()
        Get
            Return m_ToStatuses
        End Get
    End Property
End Class

Public Class BatchChangeFromAcademicProbation
    Private m_ToStatuses As New ArrayList
    Public Sub New()
        MyBase.New()
        m_ToStatuses.Add(StateEnum.CurrentlyAttending)
    End Sub
    Public Sub New(ByVal user As String)
        MyBase.New()
        m_ToStatuses.Add(StateEnum.CurrentlyAttending)
    End Sub
    Public ReadOnly Property ToStatuses()
        Get
            Return m_ToStatuses
        End Get
    End Property
End Class
