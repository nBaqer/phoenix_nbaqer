Imports FAME.Advantage.Common

Public Class AttendanceHistorySingle

    Inherits BaseReportFacade

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim AttendanceHIst As New AttendanceHistorySingleVb
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by report.
        ds = BuildReportSource(AttendanceHIst.GetAttendanceHistory(rptParamInfo), AttendanceHIst.StudentIdentifier, rptParamInfo)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function
#End Region

#Region "Private Methods"
    Private Function CreateStudentDetailsTable() As DataTable

        Dim dt As New DataTable("StudentDetails")

        dt.Columns.Add(New DataColumn("StuEnrollId", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StudentIdentifier", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StudentIDandName", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("LDA", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StatusDescrip", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("RecordDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("LDADate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonAbs", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonMkUp", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("TueSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("TueAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("TueAbs", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("TueMkUp", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WedSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WedAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WedAbs", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WedMkUp", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("ThurSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("ThurAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("ThurAbs", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("ThurMkUp", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("FriSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("FriAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("FriAbs", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("FriMkUp", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SatSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SatAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SatAbs", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SatMkUp", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SunSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SunAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SunAbs", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SunMkUp", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WkSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WkAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WkAbs", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WkMkUp", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonComm", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("TueComm", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WedComm", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("ThurComm", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("FriComm", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SatComm", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SunComm", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("TueDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WedDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("ThurDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("FriDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SatDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SunDate", System.Type.GetType("System.String")))

        dt.Columns.Add(New DataColumn("PriorTotSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("PriorTotAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("PriorTotAbs", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("PriorTotMkUp", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonthTotSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonthTotAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonthTotAbs", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonthTotMkUp", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("GrandTotSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("GrandTotAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("GrandTotAbs", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("GrandTotMkUp", System.Type.GetType("System.String")))
        ''Added by Sarswathi to find the type Days or Hours
        dt.Columns.Add(New DataColumn("UnitTypeID", System.Type.GetType("System.String")))

        dt.Columns.Add(New DataColumn("TrackSAPAttendance", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SuppressDate", System.Type.GetType("System.String")))

        dt.Columns.Add(New DataColumn("MonTardy", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("TueTardy", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("WedTardy", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("ThurTardy", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("FriTardy", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SatTardy", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("SunTardy", System.Type.GetType("System.String")))
        ''Added by Dennis D to include Class Section in header area
        dt.Columns.Add(New DataColumn("ClassSection", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("InstructionType", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("ActualClassSection", System.Type.GetType("System.String")))


        Return dt

    End Function
    Private Function CreateStudentMonthlyTotsTable() As DataTable
        Dim dt As New DataTable("StudentMonthlyTots")

        dt.Columns.Add(New DataColumn("StuEnrollId", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StudentIdentifier", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StudentIDandName", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("LDA", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("StatusDescrip", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("RecordDate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("LDADate", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("PriorTotSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("PriorTotAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("PriorTotAbs", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("PriorTotMkUp", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonthTotSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonthTotAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonthTotAbs", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("MonthTotMkUp", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("GrandTotSch", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("GrandTotAct", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("GrandTotAbs", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("GrandTotMkUp", System.Type.GetType("System.String")))
        ''Added by Dennis D to include in Monthly Totals
        dt.Columns.Add(New DataColumn("ClassSection", System.Type.GetType("System.String")))
        Return dt


    End Function

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String, ByVal rptParamInfo As Common.ReportParamInfo) As DataSet

        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String
        Dim oldCmpGrp As String = ""
        Dim oldCampus As String = ""

        'Get the mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        Dim dsfinal As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try

            Dim drpunch As DataRow()
            Dim dtStudentDetails As DataTable
            Dim dtStudentMonthlyTots As DataTable
            Dim drStudentOverAllTardyDetails As DataRow()
            Dim drStudentTardyDetails As DataRow()

            If ds.Tables.Count > 0 Then
                dtStudentDetails = CreateStudentDetailsTable()
                dtStudentMonthlyTots = CreateStudentMonthlyTotsTable()

                Dim FilterOtherParam As String
                Dim strarray As String()


                FilterOtherParam = rptParamInfo.FilterOtherString
                strarray = FilterOtherParam.Split("=")
                Dim StartDay As Double = 0
                Dim EndDay As Double
                StartDay = CDbl(strarray(1).Substring(0, 1))
                If StartDay = 7 Then
                    StartDay = 0
                End If
                EndDay = (StartDay + 6) Mod 7
                Dim date1 As String
                'date1 = FilterOtherParam.Substring(FilterOtherParam.IndexOf("(") + 1, (FilterOtherParam.IndexOf(")") - FilterOtherParam.IndexOf("(")) - 1)
                date1 = FilterOtherParam.Substring(FilterOtherParam.LastIndexOf("(") + 1, (FilterOtherParam.LastIndexOf(")") - FilterOtherParam.LastIndexOf("(")) - 1)

                Dim FromDate As Date = "10/01/2008"
                Dim ToDate As Date = "11/30/2008"
                Dim ProcessDate As Date
                FromDate = CDate(date1.Substring(0, (date1.IndexOf("A")) - 1))
                ToDate = CDate(date1.Substring(date1.IndexOf("D") + 1, (date1.Length - date1.IndexOf("D") - 1)))
                FromDate = New Date(FromDate.Year, FromDate.Month, 1)
                FromDate = Format(FromDate, "MM/dd/yyyy")
                ToDate = ToDate.AddMonths(1)
                ToDate = New Date(ToDate.Year, ToDate.Month, 1)
                ToDate = ToDate.AddDays(-1)
                ToDate = Format(ToDate, "MM/dd/yyyy")

                Dim i As Integer
                Dim row As DataRow
                Dim stuEnrollid As String
                Dim endofmonth As Date
                Dim Startofmonth As Date
                Dim drstudent As DataRow()
                Dim stuname As String
                Dim MonSch As Decimal
                Dim TueSch As Decimal
                Dim WedSch As Decimal
                Dim ThurSch As Decimal
                Dim FriSch As Decimal
                Dim SatSch As Decimal
                Dim SunSch As Decimal
                Dim MonAct As Decimal
                Dim TueAct As Decimal
                Dim WedAct As Decimal
                Dim ThurAct As Decimal
                Dim FriAct As Decimal
                Dim SatAct As Decimal
                Dim SunAct As Decimal
                Dim MonAbs As Decimal
                Dim TueAbs As Decimal
                Dim WedAbs As Decimal
                Dim ThurAbs As Decimal
                Dim FriAbs As Decimal
                Dim SatAbs As Decimal
                Dim SunAbs As Decimal
                Dim MonMkUp As Decimal
                Dim TueMkUp As Decimal
                Dim WedMkUp As Decimal
                Dim ThurMkUp As Decimal
                Dim FriMkUp As Decimal
                Dim SatMkUp As Decimal
                Dim SunMkUp As Decimal
                Dim SchTot As Decimal
                Dim ActTot As Decimal
                Dim AbsTot As Decimal
                Dim MkUpTot As Decimal
                Dim StuIdentifier As String
                Dim atttype As String
                Dim attUnitType As String


                Dim strprocessDate As String
                Dim strClassSection As String
                Dim strClassSectionMeetingId As String
                Dim TrackSAPAttendance As String


                If MyAdvAppSettings.AppSettings("TrackSapAttendance", rptParamInfo.CampusId).ToString.ToLower = "byday" Then
                    TrackSAPAttendance = "byday"
                Else
                    TrackSAPAttendance = "byclass"
                End If



                If MyAdvAppSettings.AppSettings("TrackSapAttendance", rptParamInfo.CampusId).ToString.ToLower = "byday" Then



                    For i = 0 To ds.Tables("StudentsList").Rows.Count - 1
                        ProcessDate = FromDate
                        strprocessDate = Format(ProcessDate, "MM/dd/yyyy")

                        stuEnrollid = ds.Tables("StudentsList").Rows(i)("StuEnrollId").ToString
                        atttype = ds.Tables("StudentsList").Rows(i)("UnitTypeId").ToString

                        If atttype = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.PresentAbsentGuid Then
                            attUnitType = "Days"
                        ElseIf atttype = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.MinutesGuid Then
                            attUnitType = "Min"
                        Else
                            If MyAdvAppSettings.AppSettings("TrackSapAttendance", rptParamInfo.CampusId).ToString.ToLower = "byday" Then
                                attUnitType = "Min"
                            Else
                                attUnitType = "Hrs"
                            End If
                        End If

                        drStudentOverAllTardyDetails = ds.Tables("StudentTardyDetails").Select("StuEnrollid='" + stuEnrollid + "'", "RecordDate asc")
                        If drStudentOverAllTardyDetails.Length > 0 Then
                            Dim TardyMakingAbsenceCount As Integer
                            TardyMakingAbsenceCount = drStudentOverAllTardyDetails(0)("TardiesMakingAbsence")
                            If TardyMakingAbsenceCount <= drStudentOverAllTardyDetails.Length Then
                                For itemCount As Integer = TardyMakingAbsenceCount To drStudentOverAllTardyDetails.Length Step TardyMakingAbsenceCount
                                    drStudentOverAllTardyDetails(itemCount - 1)("MarkAbsent") = 1
                                Next
                            End If
                        End If



                        While (ProcessDate <= ToDate)
                            Startofmonth = New Date(ProcessDate.Year, ProcessDate.Month, 1)

                            If StartDay = ProcessDate.DayOfWeek Or Startofmonth = ProcessDate Then
                                row = dtStudentDetails.NewRow()
                            End If



                            drpunch = ds.Tables("StudentDetails").Select("StuEnrollid='" + stuEnrollid + "' and RecordDate='" + strprocessDate + "'")


                            drStudentTardyDetails = ds.Tables("StudentTardyDetails").Select("StuEnrollid='" + stuEnrollid + "' and RecordDate='" + strprocessDate + "'")

                            drstudent = ds.Tables("StudentDetails").Select("StuEnrollId='" + stuEnrollid + "'")
                            If drstudent.Length > 0 Then
                                row("StuEnrollid") = stuEnrollid
                                ''Added by Sarswathi to find the unit type Hrs or Days
                                row("UnitTypeID") = attUnitType
                                row("TrackSAPAttendance") = TrackSAPAttendance
                                Try
                                    row("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                                Catch ex As System.Exception
                                    row("SuppressDate") = "no"
                                End Try

                                If Not drstudent(0)("StudentIdentifier") Is System.DBNull.Value Then
                                    StuIdentifier = "#" + drstudent(0)("StudentIdentifier")
                                End If

                                ''Apply mask to SSN.
                                If StudentIdentifier = "SSN" Then
                                    If Not (drstudent(0)("StudentIdentifier") Is System.DBNull.Value) Then
                                        If drstudent(0)("StudentIdentifier") <> "" Then
                                            Dim temp As String = drstudent(0)("StudentIdentifier")
                                            StuIdentifier = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                        End If
                                    End If
                                End If

                                stuname = drstudent(0)("LastName")
                                If Not (drstudent(0)("FirstName") Is System.DBNull.Value) Then
                                    If drstudent(0)("FirstName") <> "" Then
                                        stuname &= ", " & drstudent(0)("FirstName")
                                    End If
                                End If
                                If Not (drstudent(0)("MiddleName") Is System.DBNull.Value) Then
                                    If drstudent(0)("MiddleName") <> "" Then
                                        stuname &= " " & drstudent(0)("MiddleName") & "."
                                    End If
                                End If
                                row("StudentIdentifier") = StuIdentifier
                                row("StudentName") = stuname
                                row("StudentIDandName") = StuIdentifier + " " + stuname
                                row("StatusDescrip") = drstudent(0)("StatusCodeDescrip")
                                row("LDADate") = drstudent(0)("LDA")
                                row("LDA") = Format(drstudent(0)("LDA"), "MM/dd/yyyy")
                            Else
                                Exit For
                            End If


                            If ProcessDate.DayOfWeek = DayOfWeek.Monday Then
                                row("MonDate") = Format(ProcessDate, "MM/dd/yyyy")
                                If drpunch.Length = 1 Then

                                    If drStudentTardyDetails.Length > 0 Then
                                        If drStudentTardyDetails(0)("MarkAbsent") = 1 Then

                                            row("MonSch") = drpunch(0)("SchedHours")
                                            row("MonAct") = 0
                                            row("MonAbs") = drpunch(0)("SchedHours")
                                            row("MonMkUp") = 0
                                        Else
                                            row("MonSch") = drpunch(0)("SchedHours")
                                            row("MonAct") = drpunch(0)("ActualHours")
                                            row("MonAbs") = drpunch(0)("AbsentHours")
                                            row("MonMkUp") = drpunch(0)("MakeUpHours")

                                        End If
                                    Else
                                        row("MonSch") = drpunch(0)("SchedHours")
                                        row("MonAct") = drpunch(0)("ActualHours")
                                        row("MonAbs") = drpunch(0)("AbsentHours")
                                        row("MonMkUp") = drpunch(0)("MakeUpHours")

                                    End If

                                    row("MonTardy") = drpunch(0)("isTardy")
                                End If
                                Dim comment = GetComment(ProcessDate, ds.Tables(1), ds.Tables(2), ds.Tables(3), stuEnrollid)
                                Dim actualString = row("MonAct").ToString()
                                Dim loasList = ds.Tables(1).AsEnumerable
                                Dim actualForDay As Decimal = If(Not String.IsNullOrEmpty(actualString), Decimal.Parse(actualString), 0)

                                ValidateIfActuallyOff(ProcessDate, actualForDay, comment, loasList)

                                row("MonComm") = comment

                            ElseIf ProcessDate.DayOfWeek = DayOfWeek.Tuesday Then
                                row("TueDate") = Format(ProcessDate, "MM/dd/yyyy")
                                If drpunch.Length = 1 Then

                                    If drStudentTardyDetails.Length > 0 Then
                                        If drStudentTardyDetails(0)("MarkAbsent") = 1 Then

                                            row("TueSch") = drpunch(0)("SchedHours")
                                            row("TueAct") = 0
                                            row("TueAbs") = drpunch(0)("SchedHours")
                                            row("TueMkUp") = 0
                                        Else

                                            row("TueSch") = drpunch(0)("SchedHours")
                                            row("TueAct") = drpunch(0)("ActualHours")
                                            row("TueAbs") = drpunch(0)("AbsentHours")
                                            row("TueMkUp") = drpunch(0)("MakeUpHours")

                                        End If
                                    Else

                                        row("TueSch") = drpunch(0)("SchedHours")
                                        row("TueAct") = drpunch(0)("ActualHours")
                                        row("TueAbs") = drpunch(0)("AbsentHours")
                                        row("TueMkUp") = drpunch(0)("MakeUpHours")

                                    End If
                                    row("TueTardy") = drpunch(0)("isTardy")

                                End If

                                Dim comment = GetComment(ProcessDate, ds.Tables(1), ds.Tables(2), ds.Tables(3), stuEnrollid)
                                Dim actualString = row("TueAct").ToString()
                                Dim loasList = ds.Tables(1).AsEnumerable
                                Dim actualForDay As Decimal = If(Not String.IsNullOrEmpty(actualString), Decimal.Parse(actualString), 0)

                                ValidateIfActuallyOff(ProcessDate, actualForDay, comment, loasList)
                                row("TueComm") = comment

                            ElseIf ProcessDate.DayOfWeek = DayOfWeek.Wednesday Then
                                row("WedDate") = Format(ProcessDate, "MM/dd/yyyy")
                                If drpunch.Length = 1 Then
                                    If drStudentTardyDetails.Length > 0 Then
                                        If drStudentTardyDetails(0)("MarkAbsent") = 1 Then
                                            row("WedSch") = drpunch(0)("SchedHours")
                                            row("WedAct") = 0
                                            row("WedAbs") = drpunch(0)("SchedHours")
                                            row("WedMkUp") = 0
                                        Else
                                            row("WedSch") = drpunch(0)("SchedHours")
                                            row("WedAct") = drpunch(0)("ActualHours")
                                            row("WedAbs") = drpunch(0)("AbsentHours")
                                            row("WedMkUp") = drpunch(0)("MakeUpHours")
                                        End If
                                    Else
                                        row("WedSch") = drpunch(0)("SchedHours")
                                        row("WedAct") = drpunch(0)("ActualHours")
                                        row("WedAbs") = drpunch(0)("AbsentHours")
                                        row("WedMkUp") = drpunch(0)("MakeUpHours")
                                    End If

                                    row("WedTardy") = drpunch(0)("isTardy")
                                End If

                                Dim comment = GetComment(ProcessDate, ds.Tables(1), ds.Tables(2), ds.Tables(3), stuEnrollid)
                                Dim actualString = row("WedAct").ToString()
                                Dim loasList = ds.Tables(1).AsEnumerable
                                Dim actualForDay As Decimal = If(Not String.IsNullOrEmpty(actualString), Decimal.Parse(actualString), 0)

                                ValidateIfActuallyOff(ProcessDate, actualForDay, comment, loasList)
                                row("WedComm") = comment


                            ElseIf ProcessDate.DayOfWeek = DayOfWeek.Thursday Then
                                row("ThurDate") = Format(ProcessDate, "MM/dd/yyyy")
                                If drpunch.Length = 1 Then
                                    If drStudentTardyDetails.Length > 0 Then
                                        If drStudentTardyDetails(0)("MarkAbsent") = 1 Then
                                            row("ThurSch") = drpunch(0)("SchedHours")
                                            row("ThurAct") = 0
                                            row("ThurAbs") = drpunch(0)("SchedHours")
                                            row("ThurMkUp") = 0
                                        Else
                                            row("ThurSch") = drpunch(0)("SchedHours")
                                            row("ThurAct") = drpunch(0)("ActualHours")
                                            row("ThurAbs") = drpunch(0)("AbsentHours")
                                            row("ThurMkUp") = drpunch(0)("MakeUpHours")
                                        End If
                                    Else
                                        row("ThurSch") = drpunch(0)("SchedHours")
                                        row("ThurAct") = drpunch(0)("ActualHours")
                                        row("ThurAbs") = drpunch(0)("AbsentHours")
                                        row("ThurMkUp") = drpunch(0)("MakeUpHours")
                                    End If

                                    row("ThurTardy") = drpunch(0)("isTardy")
                                End If

                                Dim comment = GetComment(ProcessDate, ds.Tables(1), ds.Tables(2), ds.Tables(3), stuEnrollid)
                                Dim actualString = row("ThurAct").ToString()
                                Dim loasList = ds.Tables(1).AsEnumerable
                                Dim actualForDay As Decimal = If(Not String.IsNullOrEmpty(actualString), Decimal.Parse(actualString), 0)

                                ValidateIfActuallyOff(ProcessDate, actualForDay, comment, loasList)
                                row("ThurComm") = comment

                            ElseIf ProcessDate.DayOfWeek = DayOfWeek.Friday Then
                                row("FriDate") = Format(ProcessDate, "MM/dd/yyyy")
                                If drpunch.Length = 1 Then
                                    If drStudentTardyDetails.Length > 0 Then
                                        If drStudentTardyDetails(0)("MarkAbsent") = 1 Then
                                            row("FriSch") = drpunch(0)("SchedHours")
                                            row("FriAct") = 0
                                            row("FriAbs") = drpunch(0)("SchedHours")
                                            row("FriMkUp") = 0
                                        Else
                                            row("FriSch") = drpunch(0)("SchedHours")
                                            row("FriAct") = drpunch(0)("ActualHours")
                                            row("FriAbs") = drpunch(0)("AbsentHours")
                                            row("FriMkUp") = drpunch(0)("MakeUpHours")
                                        End If
                                    Else
                                        row("FriSch") = drpunch(0)("SchedHours")
                                        row("FriAct") = drpunch(0)("ActualHours")
                                        row("FriAbs") = drpunch(0)("AbsentHours")
                                        row("FriMkUp") = drpunch(0)("MakeUpHours")
                                    End If

                                    row("FriTardy") = drpunch(0)("isTardy")
                                End If

                                Dim comment = GetComment(ProcessDate, ds.Tables(1), ds.Tables(2), ds.Tables(3), stuEnrollid)
                                Dim actualString = row("FriAct").ToString()
                                Dim loasList = ds.Tables(1).AsEnumerable
                                Dim actualForDay As Decimal = If(Not String.IsNullOrEmpty(actualString), Decimal.Parse(actualString), 0)

                                ValidateIfActuallyOff(ProcessDate, actualForDay, comment, loasList)
                                row("FriComm") = comment

                            ElseIf ProcessDate.DayOfWeek = DayOfWeek.Saturday Then
                                row("SatDate") = Format(ProcessDate, "MM/dd/yyyy")
                                If drpunch.Length = 1 Then
                                    If drStudentTardyDetails.Length > 0 Then
                                        If drStudentTardyDetails(0)("MarkAbsent") = 1 Then
                                            row("SatSch") = drpunch(0)("SchedHours")
                                            row("SatAct") = 0
                                            row("SatAbs") = drpunch(0)("SchedHours")
                                            row("SatMkUp") = 0
                                        Else
                                            row("SatSch") = drpunch(0)("SchedHours")
                                            row("SatAct") = drpunch(0)("ActualHours")
                                            row("SatAbs") = drpunch(0)("AbsentHours")
                                            row("SatMkUp") = drpunch(0)("MakeUpHours")
                                        End If

                                    Else
                                        row("SatSch") = drpunch(0)("SchedHours")
                                        row("SatAct") = drpunch(0)("ActualHours")
                                        row("SatAbs") = drpunch(0)("AbsentHours")
                                        row("SatMkUp") = drpunch(0)("MakeUpHours")
                                    End If

                                    row("SatTardy") = drpunch(0)("isTardy")
                                End If

                                Dim comment = GetComment(ProcessDate, ds.Tables(1), ds.Tables(2), ds.Tables(3), stuEnrollid)
                                Dim actualString = row("SatAct").ToString()
                                Dim loasList = ds.Tables(1).AsEnumerable
                                Dim actualForDay As Decimal = If(Not String.IsNullOrEmpty(actualString), Decimal.Parse(actualString), 0)

                                ValidateIfActuallyOff(ProcessDate, actualForDay, comment, loasList)
                                row("SatComm") = comment

                            ElseIf ProcessDate.DayOfWeek = DayOfWeek.Sunday Then
                                row("SunDate") = Format(ProcessDate, "MM/dd/yyyy")
                                If drpunch.Length = 1 Then
                                    If drStudentTardyDetails.Length > 0 Then
                                        If drStudentTardyDetails(0)("MarkAbsent") = 1 Then
                                            row("SunSch") = drpunch(0)("SchedHours")
                                            row("SunAct") = 0
                                            row("SunAbs") = drpunch(0)("SchedHours")
                                            row("SunMkUp") = 0
                                        Else
                                            row("SunSch") = drpunch(0)("SchedHours")
                                            row("SunAct") = drpunch(0)("ActualHours")
                                            row("SunAbs") = drpunch(0)("AbsentHours")
                                            row("SunMkUp") = drpunch(0)("MakeUpHours")
                                        End If

                                    Else
                                        row("SunSch") = drpunch(0)("SchedHours")
                                        row("SunAct") = drpunch(0)("ActualHours")
                                        row("SunAbs") = drpunch(0)("AbsentHours")
                                        row("SunMkUp") = drpunch(0)("MakeUpHours")
                                    End If

                                    row("SunTardy") = drpunch(0)("isTardy")
                                End If

                                Dim comment = GetComment(ProcessDate, ds.Tables(1), ds.Tables(2), ds.Tables(3), stuEnrollid)
                                Dim actualString = row("SunAct").ToString()
                                Dim loasList = ds.Tables(1).AsEnumerable
                                Dim actualForDay As Decimal = If(Not String.IsNullOrEmpty(actualString), Decimal.Parse(actualString), 0)

                                ValidateIfActuallyOff(ProcessDate, actualForDay, comment, loasList)
                                row("SunComm") = comment

                            End If

                            Dim nextMonth = (ProcessDate.AddMonths(1))
                            endofmonth = New Date(nextMonth.Year, nextMonth.Month, 1)
                            endofmonth = endofmonth.AddDays(-1)

                            If EndDay = ProcessDate.DayOfWeek Or endofmonth = ProcessDate Then
                                row("RecordDate") = Format(Startofmonth, "MM/dd/yyyy")

                                If row("MonSch") Is System.DBNull.Value Then
                                    MonSch = 0
                                Else
                                    MonSch = CDec(row("MonSch"))
                                End If
                                If row("TueSch") Is System.DBNull.Value Then
                                    TueSch = 0
                                Else
                                    TueSch = CDec(row("TueSch"))
                                End If

                                If row("WedSch") Is System.DBNull.Value Then
                                    WedSch = 0
                                Else
                                    WedSch = CDec(row("WedSch"))
                                End If
                                If row("ThurSch") Is System.DBNull.Value Then
                                    ThurSch = 0
                                Else
                                    ThurSch = CDec(row("ThurSch"))
                                End If

                                If row("FriSch") Is System.DBNull.Value Then
                                    FriSch = 0
                                Else
                                    FriSch = CDec(row("FriSch"))
                                End If
                                If row("SatSch") Is System.DBNull.Value Then
                                    SatSch = 0
                                Else
                                    SatSch = CDec(row("SatSch"))
                                End If
                                If row("SunSch") Is System.DBNull.Value Then
                                    SunSch = 0
                                Else
                                    SunSch = CDec(row("SunSch"))
                                End If

                                If row("MonAct") Is System.DBNull.Value Then
                                    MonAct = 0
                                Else
                                    MonAct = CDec(row("MonAct"))
                                End If
                                If row("TueAct") Is System.DBNull.Value Then
                                    TueAct = 0
                                Else
                                    TueAct = CDec(row("TueAct"))
                                End If

                                If row("WedAct") Is System.DBNull.Value Then
                                    WedAct = 0
                                Else
                                    WedAct = CDec(row("WedAct"))
                                End If
                                If row("ThurAct") Is System.DBNull.Value Then
                                    ThurAct = 0
                                Else
                                    ThurAct = CDec(row("ThurAct"))
                                End If

                                If row("FriAct") Is System.DBNull.Value Then
                                    FriAct = 0
                                Else
                                    FriAct = CDec(row("FriAct"))
                                End If
                                If row("SatAct") Is System.DBNull.Value Then
                                    SatAct = 0
                                Else
                                    SatAct = CDec(row("SatAct"))
                                End If
                                If row("SunAct") Is System.DBNull.Value Then
                                    SunAct = 0
                                Else
                                    SunAct = CDec(row("SunAct"))
                                End If
                                If row("MonAbs") Is System.DBNull.Value Then
                                    MonAbs = 0
                                Else
                                    MonAbs = CDec(row("MonAbs"))
                                End If
                                If row("TueAbs") Is System.DBNull.Value Then
                                    TueAbs = 0
                                Else
                                    TueAbs = CDec(row("TueAbs"))
                                End If

                                If row("WedAbs") Is System.DBNull.Value Then
                                    WedAbs = 0
                                Else
                                    WedAbs = CDec(row("WedAbs"))
                                End If
                                If row("ThurAbs") Is System.DBNull.Value Then
                                    ThurAbs = 0
                                Else
                                    ThurAbs = CDec(row("ThurAbs"))
                                End If

                                If row("FriAbs") Is System.DBNull.Value Then
                                    FriAbs = 0
                                Else
                                    FriAbs = CDec(row("FriAbs"))
                                End If
                                If row("SatAbs") Is System.DBNull.Value Then
                                    SatAbs = 0
                                Else
                                    SatAbs = CDec(row("SatAbs"))
                                End If
                                If row("SunAbs") Is System.DBNull.Value Then
                                    SunAbs = 0
                                Else
                                    SunAbs = CDec(row("SunAbs"))
                                End If
                                If row("MonMkUp") Is System.DBNull.Value Then
                                    MonMkUp = 0
                                Else
                                    MonMkUp = CDec(row("MonMkUp"))
                                End If
                                If row("TueMkUp") Is System.DBNull.Value Then
                                    TueMkUp = 0
                                Else
                                    TueMkUp = CDec(row("TueMkUp"))
                                End If

                                If row("WedMkUp") Is System.DBNull.Value Then
                                    WedMkUp = 0
                                Else
                                    WedMkUp = CDec(row("WedMkUp"))
                                End If
                                If row("ThurMkUp") Is System.DBNull.Value Then
                                    ThurMkUp = 0
                                Else
                                    ThurMkUp = CDec(row("ThurMkUp"))
                                End If

                                If row("FriMkUp") Is System.DBNull.Value Then
                                    FriMkUp = 0
                                Else
                                    FriMkUp = CDec(row("FriMkUp"))
                                End If
                                If row("SatMkUp") Is System.DBNull.Value Then
                                    SatMkUp = 0
                                Else
                                    SatMkUp = CDec(row("SatMkUp"))
                                End If
                                If row("SunMkUp") Is System.DBNull.Value Then
                                    SunMkUp = 0
                                Else
                                    SunMkUp = CDec(row("SunMkUp"))
                                End If
                                SchTot = MonSch + TueSch + WedSch + ThurSch + FriSch + SatSch + SunSch
                                ActTot = MonAct + TueAct + WedAct + ThurAct + FriAct + SatAct + SunAct
                                AbsTot = MonAbs + TueAbs + WedAbs + ThurAbs + FriAbs + SatAbs + SunAbs
                                MkUpTot = MonMkUp + TueMkUp + WedMkUp + ThurMkUp + FriMkUp + SatMkUp + SunMkUp

                                If SchTot = 0 Then
                                    row("WkSch") = "0.00"
                                Else
                                    row("WkSch") = SchTot
                                End If
                                If ActTot = 0 Then
                                    row("WkAct") = "0.00"
                                Else
                                    row("WkAct") = ActTot
                                End If
                                If AbsTot = 0 Then
                                    row("WkAbs") = "0.00"
                                Else
                                    row("WkAbs") = AbsTot
                                End If
                                If MkUpTot = 0 Then
                                    row("WkMkUp") = "0.00"
                                Else
                                    row("WkMkUp") = MkUpTot
                                End If
                                dtStudentDetails.Rows.Add(row)
                            End If
                            dtStudentDetails.AcceptChanges()
                            ProcessDate = ProcessDate.AddDays(1)
                            strprocessDate = Format(ProcessDate, "MM/dd/yyyy")


                        End While

                    Next


                    dsfinal.Tables.Add(dtStudentDetails)
                    Dim drstudMonth As DataRow()
                    Dim drPriorMonthDetail As DataRow()
                    Dim drstuddet As DataRow()
                    Dim OldprTotSch As Decimal = 0
                    Dim OldprTotAbs As Decimal = 0
                    Dim OldprTotAct As Decimal = 0
                    Dim OldprTotMkup As Decimal = 0


                    For i = 0 To ds.Tables("StudentsList").Rows.Count - 1
                        stuEnrollid = ds.Tables("StudentsList").Rows(i)("StuEnrollId").ToString
                        ProcessDate = Format(FromDate, "MM/dd/yyyy")
                        strprocessDate = Format(ProcessDate, "MM/dd/yyyy")
                        While (ProcessDate <= ToDate)
                            row = dtStudentMonthlyTots.NewRow()
                            row("StuEnrollId") = stuEnrollid
                            row("RecordDate") = ProcessDate
                            drstuddet = dsfinal.Tables("StudentDetails").Select("StuEnrollid='" + stuEnrollid + "'")
                            row("StudentName") = drstuddet(0)("StudentName")
                            row("StudentIdentifier") = drstuddet(0)("StudentIdentifier")
                            row("StudentIDandName") = drstuddet(0)("StudentIDandName")
                            row("StatusDescrip") = drstuddet(0)("StatusDescrip")
                            row("LDADate") = drstuddet(0)("LDADate")
                            row("LDA") = Format(drstuddet(0)("LDADate"), "MM/dd/yyyy")

                            If ProcessDate = FromDate Then
                                drPriorMonthDetail = ds.Tables("StudentMonthTots").Select("StuEnrollid='" + stuEnrollid + "'")
                                If drPriorMonthDetail.Length > 0 Then
                                    row("PriorTotSch") = drPriorMonthDetail(0)("SchedHours").ToString
                                    row("PriorTotAct") = drPriorMonthDetail(0)("ActualHours").ToString
                                    row("PriorTotAbs") = drPriorMonthDetail(0)("AbsentHours").ToString
                                    row("PriorTotMkUp") = drPriorMonthDetail(0)("MakeUpHours").ToString
                                Else
                                    row("PriorTotSch") = "0.00"
                                    row("PriorTotAct") = "0.00"
                                    row("PriorTotAbs") = "0.00"
                                    row("PriorTotMkUp") = "0.00"
                                End If
                            Else

                                row("PriorTotSch") = OldprTotSch.ToString
                                row("PriorTotAct") = OldprTotAct.ToString
                                row("PriorTotAbs") = OldprTotAbs.ToString
                                row("PriorTotMkUp") = OldprTotMkup.ToString

                            End If

                            drstudMonth = dsfinal.Tables("StudentDetails").Select("StuEnrollid='" + stuEnrollid + "' and RecordDate='" + strprocessDate + "'")
                            row("MonthTotSch") = getMonthTotalSch(drstudMonth).ToString
                            row("MonthTotAct") = getMonthTotalAct(drstudMonth).ToString
                            row("MonthTotAbs") = getMonthTotalAbs(drstudMonth).ToString
                            row("MonthTotMkUp") = getMonthTotalMkUp(drstudMonth).ToString

                            row("GrandTotSch") = CDec(row("PriorTotSch")) + CDec(row("MonthTotSch"))
                            row("GrandTotAct") = CDec(row("PriorTotAct")) + CDec(row("MonthTotAct"))
                            row("GrandTotAbs") = CDec(row("PriorTotAbs")) + CDec(row("MonthTotAbs"))
                            row("GrandTotMkUp") = CDec(row("PriorTotMkUp")) + CDec(row("MonthTotMkUp"))

                            OldprTotSch = CDec(row("GrandTotSch"))
                            OldprTotAbs = CDec(row("GrandTotAbs"))
                            OldprTotAct = CDec(row("GrandTotAct"))
                            OldprTotMkup = CDec(row("GrandTotMkUp"))

                            ProcessDate = ProcessDate.AddMonths(1)
                            strprocessDate = Format(ProcessDate, "MM/dd/yyyy")
                            dtStudentMonthlyTots.Rows.Add(row)
                        End While
                        dtStudentMonthlyTots.AcceptChanges()
                    Next i
                    dsfinal.Tables.Add(dtStudentMonthlyTots)
                    Dim drStud1 As DataRow()
                    Dim j As Integer

                    If dsfinal.Tables("StudentMonthlyTots").Rows.Count > 0 Then

                        For i = 0 To dsfinal.Tables("StudentMonthlyTots").Rows.Count - 1
                            stuEnrollid = dsfinal.Tables("StudentMonthlyTots").Rows(i)("StuEnrollId").ToString
                            ProcessDate = dsfinal.Tables("StudentMonthlyTots").Rows(i)("RecordDate")
                            strprocessDate = Format(ProcessDate, "MM/dd/yyyy")

                            drStud1 = dsfinal.Tables("StudentDetails").Select("StuEnrollid='" + stuEnrollid + "' and RecordDate='" + strprocessDate + "'")

                            For j = 0 To drStud1.Length - 1
                                drStud1(j)("PriorTotSch") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("PriorTotSch")
                                drStud1(j)("PriorTotAct") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("PriorTotAct")
                                drStud1(j)("PriorTotAbs") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("PriorTotAbs")
                                drStud1(j)("PriorTotMkUp") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("PriorTotMkUp")
                                drStud1(j)("MonthTotSch") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("MonthTotSch")
                                drStud1(j)("MonthTotAct") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("MonthTotAct")
                                drStud1(j)("MonthTotAbs") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("MonthTotAbs")
                                drStud1(j)("MonthTotMkUp") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("MonthTotMkUp")
                                drStud1(j)("GrandTotSch") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("GrandTotSch")
                                drStud1(j)("GrandTotAct") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("GrandTotAct")
                                drStud1(j)("GrandTotAbs") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("GrandTotAbs")
                                drStud1(j)("GrandTotMkUp") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("GrandTotMkUp")
                            Next j

                        Next
                    End If
                    dsfinal.AcceptChanges()



                Else

                    'This is for Class Sections. Used when "TrackSapAttendance" = "byclass"

                    'Here, we're getting all the different class sections from the returned in the "StudentDetails" dataset.
                    Dim b, k, m As Integer

                    Dim dtClsSecSectionandMeetingPeriods As New DataTable
                    dtClsSecSectionandMeetingPeriods.Columns.Add("ClassSection")
                    dtClsSecSectionandMeetingPeriods.Columns.Add("ClsSectMeetingId")
                    dtClsSecSectionandMeetingPeriods.Columns.Add("StartTime")
                    dtClsSecSectionandMeetingPeriods.Columns.Add("EndTime")

                    Dim dtClassSections As New DataTable
                    Dim ClsSectRow As DataRow
                    dtClassSections.Columns.Add("ClassSection")
                    dtClassSections.Columns.Add("ClsSectMeetingId")
                    For b = 0 To ds.Tables("StudentDetails").Rows.Count - 1
                        If dtClassSections.Rows.Count = 0 Then
                            dtClassSections.Rows.Add(ds.Tables("StudentDetails").Rows(b).Item("ClassSection"), ds.Tables("StudentDetails").Rows(b).Item("ClsSectMeetingId"))

                            dtClsSecSectionandMeetingPeriods.Rows.Add(ds.Tables("StudentDetails").Rows(b).Item("ClassSection"), ds.Tables("StudentDetails").Rows(b).Item("ClsSectMeetingId"), ds.Tables("StudentDetails").Rows(b).Item("StartTime"), ds.Tables("StudentDetails").Rows(b).Item("EndTime"))

                            'ClsSectRow = ds.Tables("StudentDetails").Rows(b).Item("ClassSection")
                            'dtClassSections.Rows.Add(ClsSectRow)
                        Else
                            Dim blnExists As Boolean
                            blnExists = False
                            For k = 0 To dtClassSections.Rows.Count - 1
                                If dtClassSections.Rows(k).Item("ClassSection") = ds.Tables("StudentDetails").Rows(b).Item("ClassSection") Then
                                    blnExists = True
                                End If
                            Next
                            If blnExists = False Then
                                'Add the Class Section
                                dtClassSections.Rows.Add(ds.Tables("StudentDetails").Rows(b).Item("ClassSection"), ds.Tables("StudentDetails").Rows(b).Item("ClsSectMeetingId"))
                            End If
                            '''Added to get all the ClsSection Meetings and their periods

                            Dim blnMeetingExists As Boolean
                            blnMeetingExists = False
                            For k = 0 To dtClsSecSectionandMeetingPeriods.Rows.Count - 1
                                If dtClsSecSectionandMeetingPeriods.Rows(k).Item("ClsSectMeetingId").ToString = ds.Tables("StudentDetails").Rows(b).Item("ClsSectMeetingId").ToString Then
                                    blnMeetingExists = True
                                End If
                            Next
                            If blnMeetingExists = False Then
                                'Add the Class Section
                                dtClsSecSectionandMeetingPeriods.Rows.Add(ds.Tables("StudentDetails").Rows(b).Item("ClassSection"), ds.Tables("StudentDetails").Rows(b).Item("ClsSectMeetingId"), ds.Tables("StudentDetails").Rows(b).Item("StartTime"), ds.Tables("StudentDetails").Rows(b).Item("EndTime"))
                            End If

                        End If
                    Next





                    'Here, we're going to loop by class section and build out the data for each class section.
                    For m = 0 To dtClassSections.Rows.Count - 1
                        strClassSection = dtClassSections.Rows(m).Item("ClassSection")
                        strClassSectionMeetingId = dtClassSections.Rows(m).Item("ClsSectMeetingId")


                        For i = 0 To ds.Tables("StudentsList").Rows.Count - 1
                            ProcessDate = FromDate
                            strprocessDate = Format(ProcessDate, "MM/dd/yyyy")

                            While (ProcessDate <= ToDate)
                                Startofmonth = New Date(ProcessDate.Year, ProcessDate.Month, 1)

                                If StartDay = ProcessDate.DayOfWeek Or Startofmonth = ProcessDate Then
                                    row = dtStudentDetails.NewRow()
                                End If

                                stuEnrollid = ds.Tables("StudentsList").Rows(i)("StuEnrollId").ToString
                                'CampusID = ds.Tables("StudentsList").Rows(i)("CampusID").ToString
                                atttype = ds.Tables("StudentsList").Rows(i)("UnitTypeId").ToString

                                If atttype = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.PresentAbsentGuid Then
                                    attUnitType = "Days"
                                ElseIf atttype = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.MinutesGuid Then
                                    attUnitType = "Min"
                                Else
                                    If MyAdvAppSettings.AppSettings("TrackSapAttendance", rptParamInfo.CampusId).ToString.ToLower = "byday" Then
                                        attUnitType = "Min"
                                    Else
                                        attUnitType = "Hrs"
                                    End If
                                End If

                                'Grab the puch for the class section in the current loop.
                                drpunch = ds.Tables("StudentDetails").Select("StuEnrollid='" + stuEnrollid + "' and RecordDate='" + strprocessDate + "' and ClassSection= '" + strClassSection + "'")

                                'drStudentOverAllTardyDetails = ds.Tables("StudentTardyDetails").Select("StuEnrollid='" + stuEnrollid + "'")
                                drStudentOverAllTardyDetails = ds.Tables("StudentTardyDetails").Select("StuEnrollid='" + stuEnrollid + "' and ClsSectMeetingId= '" + strClassSectionMeetingId + "'")

                                If drStudentOverAllTardyDetails.Length > 0 Then
                                    Dim TardyMakingAbsenceCount As Integer
                                    TardyMakingAbsenceCount = drStudentOverAllTardyDetails(0)("TardiesMakingAbsence")
                                    If TardyMakingAbsenceCount <= drStudentOverAllTardyDetails.Length Then
                                        For itemCount As Integer = TardyMakingAbsenceCount To drStudentOverAllTardyDetails.Length Step TardyMakingAbsenceCount
                                            drStudentOverAllTardyDetails(itemCount - 1)("MarkAbsent") = 1
                                        Next
                                    End If
                                End If

                                drStudentTardyDetails = ds.Tables("StudentTardyDetails").Select("StuEnrollid='" + stuEnrollid + "' and RecordDate='" + strprocessDate + "'  and ClsSectMeetingId= '" + strClassSectionMeetingId + "'")

                                drstudent = ds.Tables("StudentDetails").Select("StuEnrollId='" + stuEnrollid + "'")

                                If drstudent.Length > 0 Then
                                    row("StuEnrollid") = stuEnrollid
                                    ''Added by Sarswathi to find the unit type Hrs or Days
                                    row("UnitTypeID") = attUnitType
                                    row("TrackSAPAttendance") = TrackSAPAttendance

                                    Try
                                        row("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                                    Catch ex As System.Exception
                                        row("SuppressDate") = "no"
                                    End Try

                                    If Not drstudent(0)("StudentIdentifier") Is System.DBNull.Value Then
                                        StuIdentifier = drstudent(0)("StudentIdentifier")
                                    End If

                                    ''Apply mask to SSN.
                                    If StudentIdentifier = "SSN" Then
                                        If Not (drstudent(0)("StudentIdentifier") Is System.DBNull.Value) Then
                                            If drstudent(0)("StudentIdentifier") <> "" Then
                                                Dim temp As String = drstudent(0)("StudentIdentifier")
                                                StudentIdentifier = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                            End If
                                        End If
                                    End If

                                    row("StudentIdentifier") = StudentIdentifier
                                    stuname = drstudent(0)("LastName")

                                    If Not (drstudent(0)("FirstName") Is System.DBNull.Value) Then
                                        If drstudent(0)("FirstName") <> "" Then
                                            stuname &= ", " & drstudent(0)("FirstName")
                                        End If
                                    End If
                                    If Not (drstudent(0)("MiddleName") Is System.DBNull.Value) Then
                                        If drstudent(0)("MiddleName") <> "" Then
                                            stuname &= " " & drstudent(0)("MiddleName") & "."
                                        End If
                                    End If

                                    row("StudentName") = stuname
                                    row("StatusDescrip") = drstudent(0)("StatusCodeDescrip")
                                    row("LDADate") = drstudent(0)("LDA")
                                    row("LDA") = Format(drstudent(0)("LDA"), "MM/dd/yyyy")
                                    'Add the class section to record
                                    row("ClassSection") = strClassSection
                                    row("StudentIDandName") = "# " + StuIdentifier + " " + stuname
                                    'Get Instruction Type and Actual Class Section 
                                    Dim intlen As Integer = Len(strClassSection)
                                    Dim intBreak As Integer
                                    Dim strLetter, strInstructionType, strActualClassSection As String
                                    Dim blnIsAtHyphen As Boolean

                                    intBreak = InStr(strClassSection, "%")

                                    strActualClassSection = Trim(Mid(strClassSection, 1, intBreak - 1))
                                    strInstructionType = Trim(Mid(strClassSection, intBreak + 1, intlen))

                                    row("ActualClassSection") = strActualClassSection
                                    ' row("InstructionType") = strInstructionType

                                    If ds.Tables("InstructionTypeCount").Rows.Count > 0 Then
                                        If ds.Tables("InstructionTypeCount").Rows(0)(0) = 1 Then
                                            row("InstructionType") = ""
                                        Else
                                            row("InstructionType") = strInstructionType
                                        End If
                                    Else
                                        row("InstructionType") = strInstructionType
                                    End If


                                Else
                                    Exit For
                                End If

                                Dim StartTime As String
                                Dim EndTime As String
                                Dim StrHoliday As String = ""
                                Dim StrComment As String = ""

                                If ProcessDate.DayOfWeek = DayOfWeek.Monday Then
                                    row("MonDate") = Format(ProcessDate, "MM/dd/yyyy")
                                    If drpunch.Length > 0 Then
                                        row("MonSch") = 0
                                        row("MonAct") = 0
                                        row("MonAbs") = 0
                                        row("MonMkUp") = 0
                                        StrHoliday = ""
                                        For cnt As Integer = 0 To drpunch.Length - 1
                                            ''Find if this day is holiday
                                            ''Added while doing partial holidays for each class meetings
                                            'StartTime = drpunch(cnt)("StartTime")
                                            'EndTime = drpunch(cnt)("EndTime")


                                            If drStudentTardyDetails.Length > 0 Then
                                                If drStudentTardyDetails(0)("MarkAbsent") = 1 Then

                                                    row("MonSch") = row("MonSch") + drpunch(cnt)("SchedHours")
                                                    row("MonAbs") = row("MonAbs") + drpunch(cnt)("SchedHours")
                                                    row("MonAct") = row("MonAct") + 0
                                                    If drpunch(cnt)("ActualHours") > drpunch(cnt)("SchedHours") Then
                                                        row("MonMkUp") = row("MonMkUp") + drpunch(cnt)("ActualHours") - drpunch(cnt)("SchedHours")
                                                    Else
                                                        row("MonMkUp") = row("MonMkUp") + 0
                                                    End If

                                                Else
                                                    row("MonSch") = row("MonSch") + drpunch(cnt)("SchedHours")
                                                    row("MonAct") = row("MonAct") + drpunch(cnt)("ActualHours")
                                                    row("MonAbs") = row("MonAbs") + drpunch(cnt)("AbsentHours")
                                                    row("MonMkUp") = row("MonMkUp") + drpunch(cnt)("MakeUpHours")

                                                End If
                                            Else
                                                row("MonSch") = row("MonSch") + drpunch(cnt)("SchedHours")
                                                row("MonAct") = row("MonAct") + drpunch(cnt)("ActualHours")
                                                row("MonAbs") = row("MonAbs") + drpunch(cnt)("AbsentHours")
                                                row("MonMkUp") = row("MonMkUp") + drpunch(cnt)("MakeUpHours")
                                            End If

                                            row("MonTardy") = drpunch(cnt)("isTardy")
                                        Next
                                    End If

                                    If IsPartialHoliday(ProcessDate, ds.Tables(3), dtClsSecSectionandMeetingPeriods, strClassSection) Then
                                        StrHoliday = "Off"
                                    Else
                                        StrHoliday = ""
                                    End If

                                    StrComment = GetCommentbyClass(ProcessDate, ds.Tables(1), ds.Tables(2), stuEnrollid)
                                    If StrHoliday.Trim <> String.Empty Then
                                        row("MonComm") = "Off " + StrComment
                                    Else
                                        row("MonComm") = StrComment
                                    End If


                                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Tuesday Then
                                    row("TueDate") = Format(ProcessDate, "MM/dd/yyyy")
                                    If drpunch.Length > 0 Then
                                        row("TueSch") = 0
                                        row("TueAct") = 0
                                        row("TueAbs") = 0
                                        row("TueMkUp") = 0
                                        StrHoliday = ""
                                        For cnt As Integer = 0 To drpunch.Length - 1

                                            If drStudentTardyDetails.Length > 0 Then
                                                If drStudentTardyDetails(0)("MarkAbsent") = 1 Then

                                                    row("TueSch") = drpunch(cnt)("SchedHours")
                                                    row("TueAct") = 0
                                                    row("TueAbs") = drpunch(cnt)("SchedHours")

                                                    If drpunch(cnt)("ActualHours") > drpunch(cnt)("SchedHours") Then
                                                        row("TueMkUp") = drpunch(cnt)("ActualHours") - drpunch(cnt)("SchedHours")
                                                    Else
                                                        row("TueMkUp") = 0
                                                    End If

                                                Else

                                                    row("TueSch") = drpunch(cnt)("SchedHours")
                                                    row("TueAct") = drpunch(cnt)("ActualHours")
                                                    row("TueAbs") = drpunch(cnt)("AbsentHours")
                                                    row("TueMkUp") = drpunch(cnt)("MakeUpHours")

                                                End If
                                            Else

                                                row("TueSch") = row("TueSch") + drpunch(cnt)("SchedHours")
                                                row("TueAct") = row("TueAct") + drpunch(cnt)("ActualHours")
                                                row("TueAbs") = row("TueAbs") + drpunch(cnt)("AbsentHours")
                                                row("TueMkUp") = row("TueMkUp") + drpunch(cnt)("MakeUpHours")

                                            End If
                                            row("TueTardy") = drpunch(cnt)("isTardy")
                                        Next
                                    End If
                                    If IsPartialHoliday(ProcessDate, ds.Tables(3), dtClsSecSectionandMeetingPeriods, strClassSection) Then
                                        StrHoliday = "Off"
                                    Else
                                        StrHoliday = ""
                                    End If

                                    StrComment = GetCommentbyClass(ProcessDate, ds.Tables(1), ds.Tables(2), stuEnrollid)
                                    If StrHoliday.Trim <> String.Empty Then
                                        row("TueComm") = "Off " + StrComment
                                    Else
                                        row("TueComm") = StrComment
                                    End If


                                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Wednesday Then
                                    row("WedDate") = Format(ProcessDate, "MM/dd/yyyy")
                                    If drpunch.Length > 0 Then
                                        row("WedSch") = 0
                                        row("WedAct") = 0
                                        row("WedAbs") = 0
                                        row("WedMkUp") = 0
                                        StrHoliday = ""
                                        For cnt As Integer = 0 To drpunch.Length - 1

                                            If drStudentTardyDetails.Length > 0 Then
                                                If drStudentTardyDetails(0)("MarkAbsent") = 1 Then
                                                    row("WedSch") = drpunch(cnt)("SchedHours")
                                                    row("WedAbs") = drpunch(cnt)("SchedHours")
                                                    row("WedAct") = 0
                                                    If drpunch(cnt)("ActualHours") > drpunch(cnt)("SchedHours") Then
                                                        row("WedMkUp") = drpunch(cnt)("ActualHours") - drpunch(cnt)("SchedHours")
                                                    Else
                                                        row("WedMkUp") = 0
                                                    End If

                                                Else
                                                    row("WedSch") = drpunch(cnt)("SchedHours")
                                                    row("WedAct") = drpunch(cnt)("ActualHours")
                                                    row("WedAbs") = drpunch(cnt)("AbsentHours")
                                                    row("WedMkUp") = drpunch(cnt)("MakeUpHours")
                                                End If
                                            Else
                                                row("WedSch") = row("WedSch") + drpunch(cnt)("SchedHours")
                                                row("WedAct") = row("WedAct") + drpunch(cnt)("ActualHours")
                                                row("WedAbs") = row("WedAbs") + drpunch(cnt)("AbsentHours")
                                                row("WedMkUp") = row("WedMkUp") + drpunch(cnt)("MakeUpHours")
                                            End If

                                            row("WedTardy") = drpunch(cnt)("isTardy")
                                        Next
                                    End If
                                    If IsPartialHoliday(ProcessDate, ds.Tables(3), dtClsSecSectionandMeetingPeriods, strClassSection) Then
                                        StrHoliday = "Off"
                                    Else
                                        StrHoliday = ""
                                    End If
                                    StrComment = GetCommentbyClass(ProcessDate, ds.Tables(1), ds.Tables(2), stuEnrollid)
                                    If StrHoliday.Trim <> String.Empty Then
                                        row("WedComm") = "Off " + StrComment
                                    Else
                                        row("WedComm") = StrComment
                                    End If


                                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Thursday Then
                                    row("ThurDate") = Format(ProcessDate, "MM/dd/yyyy")
                                    If drpunch.Length > 0 Then
                                        row("ThurSch") = 0
                                        row("ThurAct") = 0
                                        row("ThurAbs") = 0
                                        row("ThurMkUp") = 0
                                        StrHoliday = ""
                                        For cnt As Integer = 0 To drpunch.Length - 1

                                            If drStudentTardyDetails.Length > 0 Then
                                                If drStudentTardyDetails(0)("MarkAbsent") = 1 Then
                                                    row("ThurSch") = drpunch(cnt)("SchedHours")
                                                    row("ThurAbs") = drpunch(cnt)("SchedHours")
                                                    row("ThurAct") = 0
                                                    If drpunch(cnt)("ActualHours") > drpunch(cnt)("SchedHours") Then
                                                        row("ThurMkUp") = drpunch(cnt)("ActualHours") - drpunch(cnt)("SchedHours")
                                                    Else
                                                        row("ThurMkUp") = 0
                                                    End If
                                                Else
                                                    row("ThurSch") = drpunch(cnt)("SchedHours")
                                                    row("ThurAct") = drpunch(cnt)("ActualHours")
                                                    row("ThurAbs") = drpunch(cnt)("AbsentHours")
                                                    row("ThurMkUp") = drpunch(cnt)("MakeUpHours")
                                                End If
                                            Else
                                                row("ThurSch") = row("ThurSch") + drpunch(cnt)("SchedHours")
                                                row("ThurAct") = row("ThurAct") + drpunch(cnt)("ActualHours")
                                                row("ThurAbs") = row("ThurAbs") + drpunch(cnt)("AbsentHours")
                                                row("ThurMkUp") = row("ThurMkUp") + drpunch(cnt)("MakeUpHours")
                                            End If

                                            row("ThurTardy") = drpunch(cnt)("isTardy")
                                        Next
                                    End If
                                    If IsPartialHoliday(ProcessDate, ds.Tables(3), dtClsSecSectionandMeetingPeriods, strClassSection) Then
                                        StrHoliday = "Off"
                                    Else
                                        StrHoliday = ""
                                    End If
                                    StrComment = GetCommentbyClass(ProcessDate, ds.Tables(1), ds.Tables(2), stuEnrollid)
                                    If StrHoliday.Trim <> String.Empty Then
                                        row("ThurComm") = "Off " + StrComment
                                    Else
                                        row("ThurComm") = StrComment
                                    End If

                                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Friday Then
                                    row("FriDate") = Format(ProcessDate, "MM/dd/yyyy")
                                    If drpunch.Length > 0 Then
                                        row("FriSch") = 0
                                        row("FriAct") = 0
                                        row("FriAbs") = 0
                                        row("FriMkUp") = 0
                                        StrHoliday = ""
                                        For cnt As Integer = 0 To drpunch.Length - 1

                                            If drStudentTardyDetails.Length > 0 Then
                                                If drStudentTardyDetails(0)("MarkAbsent") = 1 Then
                                                    row("FriSch") = drpunch(cnt)("SchedHours")
                                                    row("FriAbs") = drpunch(cnt)("SchedHours")
                                                    row("FriAct") = 0
                                                    If drpunch(cnt)("ActualHours") > drpunch(cnt)("SchedHours") Then
                                                        row("FriMkUp") = drpunch(cnt)("ActualHours") - drpunch(cnt)("SchedHours")
                                                    Else
                                                        row("FriMkUp") = 0
                                                    End If
                                                Else
                                                    row("FriSch") = drpunch(cnt)("SchedHours")
                                                    row("FriAct") = drpunch(cnt)("ActualHours")
                                                    row("FriAbs") = drpunch(cnt)("AbsentHours")
                                                    row("FriMkUp") = drpunch(cnt)("MakeUpHours")
                                                End If
                                            Else
                                                row("FriSch") = row("FriSch") + drpunch(cnt)("SchedHours")
                                                row("FriAct") = row("FriAct") + drpunch(cnt)("ActualHours")
                                                row("FriAbs") = row("FriAbs") + drpunch(cnt)("AbsentHours")
                                                row("FriMkUp") = row("FriMkUp") + drpunch(cnt)("MakeUpHours")
                                            End If

                                            row("FriTardy") = drpunch(cnt)("isTardy")
                                        Next
                                    End If
                                    If IsPartialHoliday(ProcessDate, ds.Tables(3), dtClsSecSectionandMeetingPeriods, strClassSection) Then
                                        StrHoliday = "Off"
                                    Else
                                        StrHoliday = ""
                                    End If
                                    StrComment = GetCommentbyClass(ProcessDate, ds.Tables(1), ds.Tables(2), stuEnrollid)
                                    If StrHoliday.Trim <> String.Empty Then
                                        row("FriComm") = "Off " + StrComment
                                    Else
                                        row("FriComm") = StrComment
                                    End If


                                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Saturday Then
                                    row("SatDate") = Format(ProcessDate, "MM/dd/yyyy")
                                    If drpunch.Length > 0 Then
                                        row("SatSch") = 0
                                        row("SatAct") = 0
                                        row("SatAbs") = 0
                                        row("SatMkUp") = 0
                                        StrHoliday = ""
                                        For cnt As Integer = 0 To drpunch.Length - 1

                                            If drStudentTardyDetails.Length > 0 Then
                                                If drStudentTardyDetails(0)("MarkAbsent") = 1 Then
                                                    row("SatSch") = drpunch(cnt)("SchedHours")
                                                    row("SatAbs") = drpunch(cnt)("SchedHours")
                                                    row("SatAct") = 0
                                                    If drpunch(cnt)("ActualHours") > drpunch(cnt)("SchedHours") Then
                                                        row("SatMkUp") = drpunch(cnt)("ActualHours") - drpunch(cnt)("SchedHours")
                                                    Else
                                                        row("SatMkUp") = 0
                                                    End If
                                                Else
                                                    row("SatSch") = drpunch(cnt)("SchedHours")
                                                    row("SatAct") = drpunch(cnt)("ActualHours")
                                                    row("SatAbs") = drpunch(cnt)("AbsentHours")
                                                    row("SatMkUp") = drpunch(cnt)("MakeUpHours")
                                                End If

                                            Else
                                                row("SatSch") = row("SatSch") + drpunch(cnt)("SchedHours")
                                                row("SatAct") = row("SatAct") + drpunch(cnt)("ActualHours")
                                                row("SatAbs") = row("SatAbs") + drpunch(cnt)("AbsentHours")
                                                row("SatMkUp") = row("SatMkUp") + drpunch(cnt)("MakeUpHours")
                                            End If

                                            row("SatTardy") = drpunch(cnt)("isTardy")
                                        Next
                                    End If
                                    If IsPartialHoliday(ProcessDate, ds.Tables(3), dtClsSecSectionandMeetingPeriods, strClassSection) Then
                                        StrHoliday = "Off"
                                    Else
                                        StrHoliday = ""
                                    End If
                                    StrComment = GetCommentbyClass(ProcessDate, ds.Tables(1), ds.Tables(2), stuEnrollid)
                                    If StrHoliday.Trim <> String.Empty Then
                                        row("SatComm") = "Off " + StrComment
                                    Else
                                        row("SatComm") = StrComment
                                    End If


                                ElseIf ProcessDate.DayOfWeek = DayOfWeek.Sunday Then
                                    row("SunDate") = Format(ProcessDate, "MM/dd/yyyy")
                                    If drpunch.Length > 0 Then
                                        row("SunSch") = 0
                                        row("SunAct") = 0
                                        row("SunAbs") = 0
                                        row("SunMkUp") = 0
                                        StrHoliday = ""
                                        For cnt As Integer = 0 To drpunch.Length - 1

                                            If drStudentTardyDetails.Length > 0 Then
                                                If drStudentTardyDetails(0)("MarkAbsent") = 1 Then
                                                    row("SunSch") = drpunch(cnt)("SchedHours")
                                                    row("SunAbs") = drpunch(cnt)("SchedHours")
                                                    row("SunAct") = 0
                                                    If drpunch(cnt)("ActualHours") > drpunch(cnt)("SchedHours") Then
                                                        row("SunMkUp") = drpunch(cnt)("ActualHours") - drpunch(cnt)("SchedHours")
                                                    Else
                                                        row("SunMkUp") = 0
                                                    End If
                                                Else
                                                    row("SunSch") = drpunch(cnt)("SchedHours")
                                                    row("SunAct") = drpunch(cnt)("ActualHours")
                                                    row("SunAbs") = drpunch(cnt)("AbsentHours")
                                                    row("SunMkUp") = drpunch(cnt)("MakeUpHours")
                                                End If

                                            Else
                                                row("SunSch") = row("SunSch") + drpunch(cnt)("SchedHours")
                                                row("SunAct") = row("SunAct") + drpunch(cnt)("ActualHours")
                                                row("SunAbs") = row("SunAbs") + drpunch(cnt)("AbsentHours")
                                                row("SunMkUp") = row("SunMkUp") + drpunch(cnt)("MakeUpHours")
                                            End If

                                            row("SunTardy") = drpunch(cnt)("isTardy")
                                        Next
                                    End If
                                    If IsPartialHoliday(ProcessDate, ds.Tables(3), dtClsSecSectionandMeetingPeriods, strClassSection) Then
                                        StrHoliday = "Off"
                                    Else
                                        StrHoliday = ""
                                    End If
                                    StrComment = GetCommentbyClass(ProcessDate, ds.Tables(1), ds.Tables(2), stuEnrollid)
                                    If StrHoliday.Trim <> String.Empty Then
                                        row("SunComm") = "Off " + StrComment
                                    Else
                                        row("SunComm") = StrComment
                                    End If

                                End If

                                Dim nextMonth = (ProcessDate.AddMonths(1))
                                endofmonth = New Date(nextMonth.Year, nextMonth.Month, 1)
                                endofmonth = endofmonth.AddDays(-1)

                                If EndDay = ProcessDate.DayOfWeek Or endofmonth = ProcessDate Then
                                    row("RecordDate") = Format(Startofmonth, "MM/dd/yyyy")

                                    If row("MonSch") Is System.DBNull.Value Then
                                        MonSch = 0
                                    Else
                                        MonSch = CDec(row("MonSch"))
                                    End If
                                    If row("TueSch") Is System.DBNull.Value Then
                                        TueSch = 0
                                    Else
                                        TueSch = CDec(row("TueSch"))
                                    End If

                                    If row("WedSch") Is System.DBNull.Value Then
                                        WedSch = 0
                                    Else
                                        WedSch = CDec(row("WedSch"))
                                    End If
                                    If row("ThurSch") Is System.DBNull.Value Then
                                        ThurSch = 0
                                    Else
                                        ThurSch = CDec(row("ThurSch"))
                                    End If

                                    If row("FriSch") Is System.DBNull.Value Then
                                        FriSch = 0
                                    Else
                                        FriSch = CDec(row("FriSch"))
                                    End If
                                    If row("SatSch") Is System.DBNull.Value Then
                                        SatSch = 0
                                    Else
                                        SatSch = CDec(row("SatSch"))
                                    End If
                                    If row("SunSch") Is System.DBNull.Value Then
                                        SunSch = 0
                                    Else
                                        SunSch = CDec(row("SunSch"))
                                    End If

                                    If row("MonAct") Is System.DBNull.Value Then
                                        MonAct = 0
                                    Else
                                        MonAct = CDec(row("MonAct"))
                                    End If
                                    If row("TueAct") Is System.DBNull.Value Then
                                        TueAct = 0
                                    Else
                                        TueAct = CDec(row("TueAct"))
                                    End If

                                    If row("WedAct") Is System.DBNull.Value Then
                                        WedAct = 0
                                    Else
                                        WedAct = CDec(row("WedAct"))
                                    End If
                                    If row("ThurAct") Is System.DBNull.Value Then
                                        ThurAct = 0
                                    Else
                                        ThurAct = CDec(row("ThurAct"))
                                    End If

                                    If row("FriAct") Is System.DBNull.Value Then
                                        FriAct = 0
                                    Else
                                        FriAct = CDec(row("FriAct"))
                                    End If
                                    If row("SatAct") Is System.DBNull.Value Then
                                        SatAct = 0
                                    Else
                                        SatAct = CDec(row("SatAct"))
                                    End If
                                    If row("SunAct") Is System.DBNull.Value Then
                                        SunAct = 0
                                    Else
                                        SunAct = CDec(row("SunAct"))
                                    End If
                                    If row("MonAbs") Is System.DBNull.Value Then
                                        MonAbs = 0
                                    Else
                                        MonAbs = CDec(row("MonAbs"))
                                    End If
                                    If row("TueAbs") Is System.DBNull.Value Then
                                        TueAbs = 0
                                    Else
                                        TueAbs = CDec(row("TueAbs"))
                                    End If

                                    If row("WedAbs") Is System.DBNull.Value Then
                                        WedAbs = 0
                                    Else
                                        WedAbs = CDec(row("WedAbs"))
                                    End If
                                    If row("ThurAbs") Is System.DBNull.Value Then
                                        ThurAbs = 0
                                    Else
                                        ThurAbs = CDec(row("ThurAbs"))
                                    End If

                                    If row("FriAbs") Is System.DBNull.Value Then
                                        FriAbs = 0
                                    Else
                                        FriAbs = CDec(row("FriAbs"))
                                    End If
                                    If row("SatAbs") Is System.DBNull.Value Then
                                        SatAbs = 0
                                    Else
                                        SatAbs = CDec(row("SatAbs"))
                                    End If
                                    If row("SunAbs") Is System.DBNull.Value Then
                                        SunAbs = 0
                                    Else
                                        SunAbs = CDec(row("SunAbs"))
                                    End If
                                    If row("MonMkUp") Is System.DBNull.Value Then
                                        MonMkUp = 0
                                    Else
                                        MonMkUp = CDec(row("MonMkUp"))
                                    End If
                                    If row("TueMkUp") Is System.DBNull.Value Then
                                        TueMkUp = 0
                                    Else
                                        TueMkUp = CDec(row("TueMkUp"))
                                    End If

                                    If row("WedMkUp") Is System.DBNull.Value Then
                                        WedMkUp = 0
                                    Else
                                        WedMkUp = CDec(row("WedMkUp"))
                                    End If
                                    If row("ThurMkUp") Is System.DBNull.Value Then
                                        ThurMkUp = 0
                                    Else
                                        ThurMkUp = CDec(row("ThurMkUp"))
                                    End If

                                    If row("FriMkUp") Is System.DBNull.Value Then
                                        FriMkUp = 0
                                    Else
                                        FriMkUp = CDec(row("FriMkUp"))
                                    End If
                                    If row("SatMkUp") Is System.DBNull.Value Then
                                        SatMkUp = 0
                                    Else
                                        SatMkUp = CDec(row("SatMkUp"))
                                    End If
                                    If row("SunMkUp") Is System.DBNull.Value Then
                                        SunMkUp = 0
                                    Else
                                        SunMkUp = CDec(row("SunMkUp"))
                                    End If

                                    'SchTot = Math.Round((MonSch / 60), 2) + Math.Round((TueSch / 60), 2) + Math.Round((WedSch / 60), 2) + Math.Round((ThurSch / 60), 2) +
                                    '    Math.Round((FriSch / 60), 2) + Math.Round((SatSch / 60), 2) + Math.Round((SunSch / 60), 2)
                                    'ActTot = Math.Round((MonAct / 60), 2) + Math.Round((TueAct / 60), 2) + Math.Round((WedAct / 60), 2) + Math.Round((ThurAct / 60), 2) +
                                    '    Math.Round((FriAct / 60), 2) + Math.Round((SatAct / 60), 2) + Math.Round((SunAct / 60), 2)
                                    'AbsTot = Math.Round((MonAbs / 60), 2) + Math.Round((TueAbs / 60), 2) + Math.Round((WedAbs / 60), 2) + Math.Round((ThurAbs / 60), 2) +
                                    '    Math.Round((FriAbs / 60), 2) + Math.Round((SatAbs / 60), 2) + Math.Round((SunAbs / 60), 2)
                                    'MkUpTot = Math.Round((MonMkUp / 60), 2) + Math.Round((TueMkUp / 60), 2) + Math.Round((WedMkUp / 60), 2) + Math.Round((ThurMkUp / 60), 2) +
                                    '    Math.Round((FriMkUp / 60), 2) + Math.Round((SatMkUp / 60), 2) + Math.Round((SunMkUp / 60), 2)
                                    ''modified on Aug 15 2012
                                    ''DE8243:  QA: Attendance history and weekly attendance reports not printing correct data for Present/Absent ByClass schools.
                                    If row("UnitTypeID") = "Days" Then
                                        SchTot = MonSch + TueSch + WedSch + ThurSch + FriSch + SatSch + SunSch
                                        ActTot = MonAct + TueAct + WedAct + ThurAct + FriAct + SatAct + SunAct
                                        AbsTot = MonAbs + TueAbs + WedAbs + ThurAbs + FriAbs + SatAbs + SunAbs
                                        MkUpTot = MonMkUp + TueMkUp + WedMkUp + ThurMkUp + FriMkUp + SatMkUp + SunMkUp


                                    Else
                                        SchTot = Math.Round((MonSch / 60), 2) + Math.Round((TueSch / 60), 2) + Math.Round((WedSch / 60), 2) + Math.Round((ThurSch / 60), 2) +
                                 Math.Round((FriSch / 60), 2) + Math.Round((SatSch / 60), 2) + Math.Round((SunSch / 60), 2)
                                        ActTot = Math.Round((MonAct / 60), 2) + Math.Round((TueAct / 60), 2) + Math.Round((WedAct / 60), 2) + Math.Round((ThurAct / 60), 2) +
                                            Math.Round((FriAct / 60), 2) + Math.Round((SatAct / 60), 2) + Math.Round((SunAct / 60), 2)
                                        AbsTot = Math.Round((MonAbs / 60), 2) + Math.Round((TueAbs / 60), 2) + Math.Round((WedAbs / 60), 2) + Math.Round((ThurAbs / 60), 2) +
                                            Math.Round((FriAbs / 60), 2) + Math.Round((SatAbs / 60), 2) + Math.Round((SunAbs / 60), 2)
                                        MkUpTot = Math.Round((MonMkUp / 60), 2) + Math.Round((TueMkUp / 60), 2) + Math.Round((WedMkUp / 60), 2) + Math.Round((ThurMkUp / 60), 2) +
                                            Math.Round((FriMkUp / 60), 2) + Math.Round((SatMkUp / 60), 2) + Math.Round((SunMkUp / 60), 2)

                                    End If






                                    If SchTot = 0 Then
                                        row("WkSch") = "0.00"
                                    Else
                                        row("WkSch") = SchTot
                                    End If
                                    If ActTot = 0 Then
                                        row("WkAct") = "0.00"
                                    Else
                                        row("WkAct") = ActTot
                                    End If
                                    If AbsTot = 0 Then
                                        row("WkAbs") = "0.00"
                                    Else
                                        row("WkAbs") = AbsTot
                                    End If
                                    If MkUpTot = 0 Then
                                        row("WkMkUp") = "0.00"
                                    Else
                                        row("WkMkUp") = MkUpTot
                                    End If
                                    dtStudentDetails.Rows.Add(row)
                                End If

                                dtStudentDetails.AcceptChanges()
                                ProcessDate = ProcessDate.AddDays(1)
                                strprocessDate = Format(ProcessDate, "MM/dd/yyyy")

                            End While

                        Next


                    Next  'This next is for class section



                    dsfinal.Tables.Add(dtStudentDetails)

                    Dim drstudMonth As DataRow()
                    Dim drPriorMonthDetail As DataRow()
                    Dim drstuddet As DataRow()
                    Dim OldprTotSch As Decimal = 0
                    Dim OldprTotAbs As Decimal = 0
                    Dim OldprTotAct As Decimal = 0
                    Dim OldprTotMkup As Decimal = 0



                    'Here, we're getting all the different class sections from the returned in the "StudentDetails" dataset.
                    Dim b1, k1, m1 As Integer
                    Dim dtClassSections1 As New DataTable
                    dtClassSections1.Columns.Add("ClassSection")
                    dtClassSections1.Columns.Add("ClsSectMeetingId")

                    For b1 = 0 To ds.Tables("StudentDetails").Rows.Count - 1
                        If dtClassSections1.Rows.Count = 0 Then
                            dtClassSections1.Rows.Add(ds.Tables("StudentDetails").Rows(b1).Item("ClassSection"), ds.Tables("StudentDetails").Rows(b1).Item("ClsSectMeetingId"))

                        Else
                            Dim blnExists As Boolean
                            blnExists = False
                            For k1 = 0 To dtClassSections1.Rows.Count - 1
                                If dtClassSections1.Rows(k1).Item("ClassSection") = ds.Tables("StudentDetails").Rows(b1).Item("ClassSection") Then
                                    blnExists = True
                                End If
                            Next
                            If blnExists = False Then
                                'Add the Class Section
                                dtClassSections1.Rows.Add(ds.Tables("StudentDetails").Rows(b1).Item("ClassSection"), ds.Tables("StudentDetails").Rows(b1).Item("ClsSectMeetingId"))

                            End If
                        End If
                    Next
                    Dim priorDataRow As AttendancePercentage

                    'Here, we're going to loop by class section and build out the data for each class section.
                    For m1 = 0 To dtClassSections1.Rows.Count - 1
                        strClassSection = dtClassSections1.Rows(m1).Item("ClassSection")
                        strClassSectionMeetingId = dtClassSections1.Rows(m1).Item("ClsSectMeetingId")


                        For i = 0 To ds.Tables("StudentsList").Rows.Count - 1
                            stuEnrollid = ds.Tables("StudentsList").Rows(i)("StuEnrollId").ToString
                            ProcessDate = Format(FromDate, "MM/dd/yyyy")
                            strprocessDate = Format(ProcessDate, "MM/dd/yyyy")
                            While (ProcessDate <= ToDate)
                                row = dtStudentMonthlyTots.NewRow()
                                row("StuEnrollId") = stuEnrollid
                                row("RecordDate") = ProcessDate

                                drstuddet = dsfinal.Tables("StudentDetails").Select("StuEnrollid='" + stuEnrollid + "'")

                                row("StudentName") = drstuddet(0)("StudentName")
                                row("StudentIdentifier") = drstuddet(0)("StudentIdentifier")
                                row("StatusDescrip") = drstuddet(0)("StatusDescrip")
                                row("LDADate") = drstuddet(0)("LDADate")


                                If ProcessDate = FromDate Then
                                    'Set Prior Months totals
                                    drPriorMonthDetail = ds.Tables("StudentMonthTots").Select("StuEnrollid='" + stuEnrollid + "' and ClassSection='" + strClassSection + "'")

                                    If drPriorMonthDetail.Length > 0 Then
                                        priorDataRow = ProcessPriorData(drPriorMonthDetail)
                                        'row("PriorTotSch") = Math.Round(CDec(drPriorMonthDetail(0)("SchedHours").ToString) / 60, 2)
                                        'row("PriorTotAct") = Math.Round(CDec(drPriorMonthDetail(0)("ActualHours").ToString) / 60, 2)
                                        'row("PriorTotAbs") = Math.Round(CDec(drPriorMonthDetail(0)("AbsentHours").ToString) / 60, 2)
                                        'row("PriorTotMkUp") = Math.Round(CDec(drPriorMonthDetail(0)("MakeUpHours").ToString) / 60, 2)

                                        If attUnitType = "Days" Then
                                            row("PriorTotSch") = Math.Round(CDec(priorDataRow.totalScheduled), 2)
                                            row("PriorTotAct") = Math.Round(CDec(priorDataRow.totalPresent), 2)
                                            row("PriorTotAbs") = Math.Round(CDec(priorDataRow.totalAbsent), 2)
                                            row("PriorTotMkUp") = Math.Round(CDec(priorDataRow.totalMakeUp), 2)

                                        Else
                                            row("PriorTotSch") = Math.Round(CDec(priorDataRow.totalScheduled) / 60, 2)
                                            row("PriorTotAct") = Math.Round(CDec(priorDataRow.totalPresent) / 60, 2)
                                            row("PriorTotAbs") = Math.Round(CDec(priorDataRow.totalAbsent) / 60, 2)
                                            row("PriorTotMkUp") = Math.Round(CDec(priorDataRow.totalMakeUp) / 60, 2)

                                        End If

                                    Else
                                        row("PriorTotSch") = "0.00"
                                        row("PriorTotAct") = "0.00"
                                        row("PriorTotAbs") = "0.00"
                                        row("PriorTotMkUp") = "0.00"
                                    End If
                                Else

                                    row("PriorTotSch") = OldprTotSch.ToString
                                    row("PriorTotAct") = OldprTotAct.ToString
                                    row("PriorTotAbs") = OldprTotAbs.ToString
                                    row("PriorTotMkUp") = OldprTotMkup.ToString

                                End If
                                'Set Current Months Totals
                                drstudMonth = dsfinal.Tables("StudentDetails").Select("StuEnrollid='" + stuEnrollid + "' and RecordDate='" + strprocessDate + "' and ClassSection='" + strClassSection + "'")

                                row("MonthTotSch") = getMonthTotalSch(drstudMonth).ToString
                                row("MonthTotAct") = getMonthTotalAct(drstudMonth).ToString
                                row("MonthTotAbs") = getMonthTotalAbs(drstudMonth).ToString
                                row("MonthTotMkUp") = getMonthTotalMkUp(drstudMonth).ToString

                                'Set Grand Totals
                                row("GrandTotSch") = CDec(row("PriorTotSch")) + CDec(row("MonthTotSch"))
                                row("GrandTotAct") = CDec(row("PriorTotAct")) + CDec(row("MonthTotAct"))
                                row("GrandTotAbs") = CDec(row("PriorTotAbs")) + CDec(row("MonthTotAbs"))
                                row("GrandTotMkUp") = CDec(row("PriorTotMkUp")) + CDec(row("MonthTotMkUp"))

                                OldprTotSch = CDec(row("GrandTotSch"))
                                OldprTotAbs = CDec(row("GrandTotAbs"))
                                OldprTotAct = CDec(row("GrandTotAct"))
                                OldprTotMkup = CDec(row("GrandTotMkUp"))

                                ProcessDate = ProcessDate.AddMonths(1)
                                strprocessDate = Format(ProcessDate, "MM/dd/yyyy")

                                row("ClassSection") = strClassSection

                                dtStudentMonthlyTots.Rows.Add(row)
                            End While
                            dtStudentMonthlyTots.AcceptChanges()
                        Next i


                    Next

                    dsfinal.Tables.Add(dtStudentMonthlyTots)




                    Dim drStud1 As DataRow()
                    Dim j As Integer

                    'Here, we're getting all the different class sections from the returned in the "StudentDetails" dataset.
                    Dim b2, k2, m2 As Integer
                    Dim dtClassSections2 As New DataTable
                    dtClassSections2.Columns.Add("ClassSection")

                    For b2 = 0 To ds.Tables("StudentDetails").Rows.Count - 1
                        If dtClassSections2.Rows.Count = 0 Then
                            dtClassSections2.Rows.Add(ds.Tables("StudentDetails").Rows(b2).Item("ClassSection"))
                        Else
                            Dim blnExists As Boolean
                            blnExists = False
                            For k2 = 0 To dtClassSections2.Rows.Count - 1
                                If dtClassSections2.Rows(k2).Item("ClassSection") = ds.Tables("StudentDetails").Rows(b2).Item("ClassSection") Then
                                    blnExists = True
                                End If
                            Next
                            If blnExists = False Then
                                'Add the Class Section
                                dtClassSections2.Rows.Add(ds.Tables("StudentDetails").Rows(b2).Item("ClassSection"))
                            End If
                        End If
                    Next


                    'Here, we're going to loop by class section and build out the data for each class section.
                    For m2 = 0 To dtClassSections1.Rows.Count - 1
                        strClassSection = dtClassSections2.Rows(m2).Item("ClassSection")

                        If dsfinal.Tables("StudentMonthlyTots").Rows.Count > 0 Then

                            For i = 0 To dsfinal.Tables("StudentMonthlyTots").Rows.Count - 1

                                If dsfinal.Tables("StudentMonthlyTots").Rows(i)("ClassSection") = strClassSection Then

                                    stuEnrollid = dsfinal.Tables("StudentMonthlyTots").Rows(i)("StuEnrollId").ToString
                                    ProcessDate = dsfinal.Tables("StudentMonthlyTots").Rows(i)("RecordDate")
                                    strprocessDate = Format(ProcessDate, "MM/dd/yyyy")

                                    drStud1 = dsfinal.Tables("StudentDetails").Select("StuEnrollid='" + stuEnrollid + "' and RecordDate='" + strprocessDate + "' and ClassSection='" + strClassSection + "'")

                                    For j = 0 To drStud1.Length - 1
                                        drStud1(j)("PriorTotSch") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("PriorTotSch")
                                        drStud1(j)("PriorTotAct") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("PriorTotAct")
                                        drStud1(j)("PriorTotAbs") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("PriorTotAbs")
                                        drStud1(j)("PriorTotMkUp") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("PriorTotMkUp")
                                        drStud1(j)("MonthTotSch") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("MonthTotSch")
                                        drStud1(j)("MonthTotAct") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("MonthTotAct")
                                        drStud1(j)("MonthTotAbs") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("MonthTotAbs")
                                        drStud1(j)("MonthTotMkUp") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("MonthTotMkUp")
                                        drStud1(j)("GrandTotSch") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("GrandTotSch")
                                        drStud1(j)("GrandTotAct") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("GrandTotAct")
                                        drStud1(j)("GrandTotAbs") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("GrandTotAbs")
                                        drStud1(j)("GrandTotMkUp") = dsfinal.Tables("StudentMonthlyTots").Rows(i)("GrandTotMkUp")
                                    Next j
                                End If
                            Next
                        End If
                    Next

                    dsfinal.AcceptChanges()


                End If
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try


        dsfinal.AcceptChanges()
        Return dsfinal
    End Function


    Private Function ValidateIfActuallyOff(ByVal ProcessDate As DateTime, ByVal actualForDay As Decimal, ByRef comment As String, loaList As IEnumerable(Of DataRow))
        If Not String.IsNullOrEmpty(comment) Then

            'student had actual hours for this day, ignore loa, holiday, or susepended comments
            If (actualForDay > 0) Then
                comment = String.Empty
            Else
                'If the comment for this day specifies student was on LOA
                If comment.Contains("LOA") Then
                    'Get the last 
                    Dim loaReturnDate As Date? = loaList.Where(Function(x) ProcessDate >= x.Field(Of DateTime)("StartDate") AndAlso ProcessDate <= x.Field(Of DateTime)("EndDate")) _
                    .Select(Of DateTime)(Function(dates) dates.Field(Of DateTime)("LOAReturnDate")).OrderByDescending(Of DateTime)(Function(x) x).FirstOrDefault()

                    If loaReturnDate IsNot Nothing Then
                        If (loaReturnDate = ProcessDate) Then
                            comment = String.Empty
                        End If
                    End If
                End If

            End If
        End If

    End Function
    Private Function getMonthTotalSch(ByVal dr As DataRow()) As Decimal
        Dim tot As Decimal = 0
        Dim i As Integer

        For i = 0 To dr.Length - 1
            tot += CDec(dr(i)("WkSch"))
        Next
        Return tot
    End Function
    Private Function getMonthTotalAbs(ByVal dr As DataRow()) As Decimal
        Dim tot As Decimal = 0
        Dim i As Integer

        For i = 0 To dr.Length - 1
            tot += CDec(dr(i)("WkAbs"))
        Next
        Return tot
    End Function
    Private Function getMonthTotalAct(ByVal dr As DataRow()) As Decimal
        Dim tot As Decimal = 0
        Dim i As Integer
        For i = 0 To dr.Length - 1
            tot += CDec(dr(i)("WkAct"))
        Next
        Return tot
    End Function
    Private Function getMonthTotalMkUp(ByVal dr As DataRow()) As Decimal
        Dim tot As Decimal = 0
        Dim i As Integer
        For i = 0 To dr.Length - 1
            tot += CDec(dr(i)("WkMkUp"))
        Next
        Return tot
    End Function
    'Private Function GetTotal(ByVal Mon As DataColumn, ByVal Tue As String, ByVal Wed As String, ByVal Thur As String, ByVal Fri As String, ByVal Sat As String, ByVal sun As String) As Decimal
    '    Dim MonSch As Decimal
    '    Dim TueSch As Decimal
    '    Dim WedSch As Decimal
    '    Dim ThurSch As Decimal
    '    Dim FriSch As Decimal
    '    Dim SatSch As Decimal
    '    Dim SunSch As Decimal
    '    Dim MonAct As Decimal
    '    Dim TueAct As Decimal

    '    If Mon Is System.DBNull.Value Then
    '        MonSch = 0
    '    Else
    '        MonSch = CDec(Mon)
    '    End If
    '    If Tue Is System.DBNull.Value Then
    '        TueSch = 0
    '    Else
    '        TueSch = CDec(Tue)
    '    End If

    '    If Wed Is System.DBNull.Value Then
    '        WedSch = 0
    '    Else
    '        WedSch = CDec(Wed)
    '    End If
    '    If Thur Is System.DBNull.Value Then
    '        ThurSch = 0
    '    Else
    '        ThurSch = CDec(Thur)
    '    End If

    '    If Fri Is System.DBNull.Value Then
    '        FriSch = 0
    '    Else
    '        FriSch = CDec(Fri)
    '    End If
    '    If Sat Is System.DBNull.Value Then
    '        SatSch = 0
    '    Else
    '        SatSch = CDec(Sat)
    '    End If
    '    If sun Is System.DBNull.Value Then
    '        SunSch = 0
    '    Else
    '        SunSch = CDec(sun)
    '    End If
    '    Return (MonSch + TueSch + WedSch + ThurSch + FriSch + SatSch + SunSch)
    'End Function
    Private Function GetComment(ByVal FromDate As Date, ByVal dtLOA As DataTable, ByVal dtSusp As DataTable, ByVal dtHol As DataTable, ByVal stuEnrollid As String) As String
        Dim Comm As String = ""
        Dim i As Integer
        For i = 0 To dtHol.Rows.Count - 1
            If FromDate >= dtHol.Rows(i)("StartDate") And FromDate <= dtHol.Rows(i)("EndDate") Then
                Comm = Comm + "Off "
                Exit For
            End If

        Next
        For i = 0 To dtLOA.Rows.Count - 1
            If dtLOA.Rows(i)("StuEnrollId").ToString = stuEnrollid Then
                If FromDate >= dtLOA.Rows(i)("StartDate") And FromDate <= dtLOA.Rows(i)("EndDate") Then
                    Comm = Comm + "LOA "
                    Exit For
                End If
            End If
        Next
        For i = 0 To dtSusp.Rows.Count - 1
            If dtSusp.Rows(i)("StuEnrollId").ToString = stuEnrollid Then
                If FromDate >= dtSusp.Rows(i)("StartDate") And FromDate <= dtSusp.Rows(i)("EndDate") Then
                    Comm = Comm + "Susp "
                    Exit For
                End If
            End If
        Next

        Return Comm
    End Function

    Private Function GetCommentbyClass(ByVal FromDate As Date, ByVal dtLOA As DataTable, ByVal dtSusp As DataTable, ByVal stuEnrollid As String) As String
        Dim Comm As String = ""
        Dim i As Integer
        For i = 0 To dtLOA.Rows.Count - 1
            If dtLOA.Rows(i)("StuEnrollId").ToString = stuEnrollid Then
                If FromDate >= dtLOA.Rows(i)("StartDate") And FromDate <= dtLOA.Rows(i)("EndDate") Then
                    Comm = Comm + "LOA "
                    Exit For
                End If
            End If
        Next
        For i = 0 To dtSusp.Rows.Count - 1
            If dtSusp.Rows(i)("StuEnrollId").ToString = stuEnrollid Then
                If FromDate >= dtSusp.Rows(i)("StartDate") And FromDate <= dtSusp.Rows(i)("EndDate") Then
                    Comm = Comm + "Susp "
                    Exit For
                End If
            End If
        Next

        Return Comm
    End Function

    Private Function IsPartialHoliday(ByVal CurDate As Date, ByVal dtHol As DataTable, ByVal dtClsSectMeetingperiods As DataTable, ByVal ClsSection As String) As Boolean
        Dim i As Integer
        For i = 0 To dtHol.Rows.Count - 1
            If CurDate >= dtHol.Rows(i)("StartDate") And CurDate <= dtHol.Rows(i)("EndDate") Then
                If dtHol.Rows(i)("AllDay") = False Then

                    Dim dr As DataRow()
                    Dim StartTime As String
                    Dim EndTime As String
                    dr = dtClsSectMeetingperiods.Select("ClassSection='" + ClsSection + "'")
                    If dr.Length >= 1 Then
                        For Each item In dr
                            StartTime = item("StartTime").ToString
                            EndTime = item("EndTime").ToString

                            If (StartTime >= Format(dtHol.Rows(i)("StartTime"), "HH:mm:ss") And StartTime <= Format(dtHol.Rows(i)("EndTime"), "HH:mm:ss")) Or (EndTime >= Format(dtHol.Rows(i)("StartTime"), "HH:mm:ss") And EndTime <= Format(dtHol.Rows(i)("EndTime"), "HH:mm:ss")) Then
                                Return True
                            ElseIf (StartTime <= Format(dtHol.Rows(i)("StartTime"), "HH:mm:ss") And EndTime >= Format(dtHol.Rows(i)("EndTime"), "HH:mm:ss")) Then
                                Return True
                            ElseIf (StartTime >= Format(dtHol.Rows(i)("StartTime"), "HH:mm:ss") And EndTime <= Format(dtHol.Rows(i)("EndTime"), "HH:mm:ss")) Then
                                Return True
                            End If
                        Next

                    End If


                Else
                    Return True

                End If

            End If

        Next
        Return False
    End Function
    Private Function ProcessPriorData(drPriorMonthDetail As DataRow()) As AttendancePercentage
        Dim totalPresent As Integer = 0
        Dim totalAbsent As Integer = 0
        Dim totalTardies As Integer = 0
        Dim totalExcused As Integer = 0
        Dim totalScheduled As Decimal = 0.0
        Dim totalAttended As Decimal = 0.0
        Dim totalPresentAdjusted As Decimal
        Dim totalAbsenceAdjusted As Decimal
        Dim totalTardiesAdjusted As Decimal
        Dim totalExcusedAdjusted As Decimal
        Dim ccounter As Integer
        Dim absences As Integer
        Dim minusPresent As Integer
        Dim minusTardies As Integer
        Dim totalmakeUp As Integer = 0
        'If doesPrgVerTrackAttendance Then

        'Attendance is tracked at the program version level
        Dim totalScheduledMinutes As Integer = 0
        Dim totalExcusedMinutes As Integer = 0
        Dim totalAbsentMinutes As Integer = 0
        Dim totalTardiesMinutes As Integer = 0
        Dim totalPresentMinutes As Integer = 0
        Dim tardiesMakingOneAbsence As Integer = 0
        For Each row As DataRow In drPriorMonthDetail
            Dim actual As Integer = CType(row("Actual"), Integer)
            Dim tardy As Boolean = CType(row("Tardy"), Boolean)
            tardiesMakingOneAbsence = CType(row("TardiesMakingAbsence"), Integer)
            Dim meetingDuration As Integer = 0





            'calculate duration of the meeting
            meetingDuration = CType(row("Scheduled"), Integer)
            totalScheduled += meetingDuration

            If meetingDuration >= actual And Not tardy Then
                totalAbsent += meetingDuration - actual
                totalPresent += actual
            ElseIf meetingDuration < actual And Not tardy Then
                totalPresent += actual
                totalmakeUp += actual - meetingDuration
            Else
                'totalPresent += actual
                If tardy Then
                    If meetingDuration > actual Then

                        ccounter += 1
                        If ccounter = tardiesMakingOneAbsence Then
                            absences += meetingDuration
                            minusPresent += actual
                            minusTardies += meetingDuration - actual
                            ccounter = 0
                            totalAbsent += meetingDuration
                        Else
                            absences += meetingDuration - actual
                            totalAbsent += meetingDuration - actual
                            totalPresent += actual
                        End If
                        totalTardies += meetingDuration - actual
                    Else
                        ccounter += 1
                        If ccounter = tardiesMakingOneAbsence Then
                            absences += meetingDuration
                            minusPresent += meetingDuration
                            minusTardies += actual - meetingDuration
                            ccounter = 0
                            totalAbsent += meetingDuration
                            'totalPresent += actual - meetingDuration
                        Else
                            absences += actual - meetingDuration
                            totalPresent += actual
                        End If
                        'totalTardies += actual - meetingDuration
                        totalmakeUp += actual - meetingDuration
                    End If

                End If

            End If

        Next

        Dim attInfo As New AttendancePercentage()
        attInfo.totalPresent = totalPresent
        attInfo.totalAbsent = totalAbsent
        attInfo.totalScheduled = totalScheduled
        attInfo.totalMakeUp = totalmakeUp

        'End If
        Return attInfo


    End Function

    Structure AttendancePercentage
        Dim totalPresent As Integer
        Dim totalAbsent As Integer
        Dim totalScheduled As Integer
        Dim totalMakeUp As Integer

    End Structure


#End Region



End Class
