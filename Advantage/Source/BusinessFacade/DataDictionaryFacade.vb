Public Class DataDictionaryFacade
    Public Function GetModulesListing() As DataTable
        Dim objDataDictDB As New DataDictionaryDB

        Return objDataDictDB.GetModulesListing
    End Function

    Public Function GetTablesForModule(ByVal modulePrefix As String) As DataTable
        Dim objdatadict As New DataDictionaryDB

        Return objdatadict.GetTablesForModule(modulePrefix)
    End Function

    Public Function GetFieldsForTableExclID(ByVal tableID As Integer) As DataTable
        Dim objdatadict As New DataDictionaryDB

        Return objdatadict.GetFieldsForTableExclID(tableID)
    End Function

    Public Function GetFieldProperties(ByVal fldID As Integer) As DataTable
        Dim objdatadict As New DataDictionaryDB

        Return objdatadict.GetFieldProperties(fldID)
    End Function

    Public Sub UpdateFieldProperties(ByVal fldId As Integer, ByVal fldReq As Boolean, ByVal fldCaption As String, ByVal captionIsInDB As String)
        Dim objDataDict As New DataDictionaryDB

        objDataDict.UpdateFieldProperties(fldId, fldReq, fldCaption, captionIsInDB)
    End Sub

    Public Function GetFields() As DataTable
        Dim objdatadict As New DataDictionaryDB

        Return objdatadict.GetFields
    End Function

    Public Function IsFieldDataDictionaryRequired(ByVal fldId As Integer) As Boolean
        Dim objdatadict As New DataDictionaryDB

        Return objdatadict.IsFieldDataDictionaryRequired(fldId)

    End Function
End Class
