Public Class LeadZipSummaryObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim obj As New LeadMasterDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(obj.GetLeadZipSummary(rptParamInfo))
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim oldCmpGrp As String
        Dim oldCampus As String
        Dim zipMask As String

        'Get the mask for the zip code
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        Try
            If ds.Tables.Count = 2 Then
                For Each dr As DataRow In ds.Tables(0).Rows

                    dr("ZipCount") = ds.Tables(0).Rows.Count

                    'Apply mask on Zip Code
                    If Not dr.IsNull("ForeignZip") Then
                        If  Not String.IsNullOrEmpty(dr("Zip").ToString()) And dr("ForeignZip") = False Then
                            'Domestic Zip
                            dr("Zip") = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                        End If
                    Else
                        'Domestic Zip
                        If Not String.IsNullOrEmpty(dr("Zip").ToString()) Then
                            dr("Zip") = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                        End If
                    End If

                    'Initialize columns
                    dr("Jan") = 0
                    dr("Feb") = 0
                    dr("Mar") = 0
                    dr("Apr") = 0
                    dr("May") = 0
                    dr("Jun") = 0
                    dr("Jul") = 0
                    dr("Aug") = 0
                    dr("Sep") = 0
                    dr("Oct") = 0
                    dr("Nov") = 0
                    dr("Dec") = 0

                    'Get campus group and campus
                    If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpDescrip") Then
                        oldCmpGrp = dr("CampGrpDescrip")
                    End If
                    If oldCmpGrp = dr("CampGrpDescrip") And oldCampus = "" Then
                        oldCampus = dr("CampDescrip")
                    ElseIf oldCmpGrp = dr("CampGrpDescrip") And oldCampus <> dr("CampDescrip") Then
                        oldCampus = dr("CampDescrip")
                    End If

                    Dim Rows() As DataRow = ds.Tables(1).Select("CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                    "CampusId='" & dr("CampusId").ToString & "' AND " & _
                                                                    "Zip='" & dr("Zip").ToString & "'")
                    For Each row As DataRow In Rows
                        If Not row.IsNull("InquiryDate") Then
                            Select Case Date.Parse(row("InquiryDate")).Month
                                Case 1
                                    dr("Jan") += row("CountByInquiryDate")
                                Case 2
                                    dr("Feb") += row("CountByInquiryDate")
                                Case 3
                                    dr("Mar") += row("CountByInquiryDate")
                                Case 4
                                    dr("Apr") += row("CountByInquiryDate")
                                Case 5
                                    dr("May") += row("CountByInquiryDate")
                                Case 6
                                    dr("Jun") += row("CountByInquiryDate")
                                Case 7
                                    dr("Jul") += row("CountByInquiryDate")
                                Case 8
                                    dr("Aug") += row("CountByInquiryDate")
                                Case 9
                                    dr("Sep") += row("CountByInquiryDate")
                                Case 10
                                    dr("Oct") += row("CountByInquiryDate")
                                Case 11
                                    dr("Nov") += row("CountByInquiryDate")
                                Case 12
                                    dr("Dec") += row("CountByInquiryDate")
                            End Select
                        End If
                    Next
                Next

                ds.AcceptChanges()

                'Compute Totals
                ''''Dim dtCG As DataTable = ds.Tables("CampusGroupTotals")
                ''''Dim dt As DataTable = ds.Tables("CampusTotals")
                ''''Dim rowCG As DataRow
                ''''Dim rowC As DataRow

                ''''For Each dr As DataRow In ds.Tables(0).Rows
                ''''    'Get campus group and campus
                ''''    If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpDescrip") Then
                ''''        'Compute totals for campus group
                ''''        'rowCG = dtCG.NewRow
                ''''        'rowCG("CampusGrpDescrip") = dr("CampGrpDescrip")
                ''''        'rowCG("CampusGrpTotal") = ds.Tables(0).Compute("SUM(Inquiries)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                ''''        'rowCG("CGJan") = ds.Tables(0).Compute("SUM(Jan)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                ''''        'rowCG("CGFeb") = ds.Tables(0).Compute("SUM(Feb)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                ''''        'rowCG("CGMar") = ds.Tables(0).Compute("SUM(Mar)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                ''''        'rowCG("CGApr") = ds.Tables(0).Compute("SUM(Apr)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                ''''        'rowCG("CGMay") = ds.Tables(0).Compute("SUM(May)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                ''''        'rowCG("CGJun") = ds.Tables(0).Compute("SUM(Jun)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                ''''        'rowCG("CGJul") = ds.Tables(0).Compute("SUM(Jul)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                ''''        'rowCG("CGAug") = ds.Tables(0).Compute("SUM(Aug)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                ''''        'rowCG("CGSep") = ds.Tables(0).Compute("SUM(Sep)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                ''''        'rowCG("CGOct") = ds.Tables(0).Compute("SUM(Oct)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                ''''        'rowCG("CGNov") = ds.Tables(0).Compute("SUM(Nov)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                ''''        'rowCG("CGDec") = ds.Tables(0).Compute("SUM(Dec)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                ''''        'dtCG.Rows.Add(rowCG)
                ''''        oldCmpGrp = dr("CampGrpDescrip")
                ''''    End If

                ''''    If oldCmpGrp = dr("CampGrpDescrip") And oldCampus = "" Then
                ''''        'Compute totals for campus
                ''''        'rowC = dt.NewRow
                ''''        'rowC("CampusGrpDescrip") = oldCmpGrp
                ''''        'rowC("CampusDescrip") = dr("CampDescrip")
                ''''        'rowC("CampusGrpTotal") = ds.Tables(0).Compute("SUM(Inquiries)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CJan") = ds.Tables(0).Compute("SUM(Jan)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CFeb") = ds.Tables(0).Compute("SUM(Feb)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CMar") = ds.Tables(0).Compute("SUM(Mar)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CApr") = ds.Tables(0).Compute("SUM(Apr)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CMay") = ds.Tables(0).Compute("SUM(May)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CJun") = ds.Tables(0).Compute("SUM(Jun)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CJul") = ds.Tables(0).Compute("SUM(Jul)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CAug") = ds.Tables(0).Compute("SUM(Aug)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CGSep") = ds.Tables(0).Compute("SUM(Sep)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("COct") = ds.Tables(0).Compute("SUM(Oct)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CNov") = ds.Tables(0).Compute("SUM(Nov)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CDec") = ds.Tables(0).Compute("SUM(Dec)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'dt.Rows.Add(rowC)
                ''''        oldCampus = dr("CampDescrip")
                ''''        '
                ''''    ElseIf oldCmpGrp = dr("CampGrpDescrip") And oldCampus <> dr("CampDescrip") Then
                ''''        'Compute totals for campus
                ''''        'rowC = dt.NewRow
                ''''        'rowC("CampusGrpDescrip") = oldCmpGrp
                ''''        'rowC("CampusDescrip") = dr("CampDescrip")
                ''''        'rowC("CampusGrpTotal") = ds.Tables(0).Compute("SUM(Inquiries)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CJan") = ds.Tables(0).Compute("SUM(Jan)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CFeb") = ds.Tables(0).Compute("SUM(Feb)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CMar") = ds.Tables(0).Compute("SUM(Mar)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CApr") = ds.Tables(0).Compute("SUM(Apr)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CMay") = ds.Tables(0).Compute("SUM(May)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CJun") = ds.Tables(0).Compute("SUM(Jun)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CJul") = ds.Tables(0).Compute("SUM(Jul)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CAug") = ds.Tables(0).Compute("SUM(Aug)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CSep") = ds.Tables(0).Compute("SUM(Sep)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("COct") = ds.Tables(0).Compute("SUM(Oct)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CNov") = ds.Tables(0).Compute("SUM(Nov)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'rowC("CDec") = ds.Tables(0).Compute("SUM(Dec)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                ''''        '                                                "CampusId='" & dr("CampusId").ToString & "'")
                ''''        'dt.Rows.Add(rowC)
                ''''        oldCampus = dr("CampDescrip")
                ''''    End If
                ''''Next
            End If
            ds.AcceptChanges()

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region
End Class
