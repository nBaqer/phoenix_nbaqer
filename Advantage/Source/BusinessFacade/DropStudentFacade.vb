Public Class DropStudentFacade
    Public Function PopulateDataGrid(ByVal student As String, ByVal term As String, ByVal campusId As String) As DataSet

        Dim db As New DropStudentDB
        Dim ds As DataSet

        ds = db.GetDataGridInfo(student, term, campusId)
        Return ds

    End Function

    Public Function GetDropGrades(ByVal ClsSect As String) As DataSet

        '   Instantiate DAL component
        With New DropStudentDB

            '   get the dataset with all TuitionCategories
            Return .GetDropGradeSystemDetails(ClsSect)

        End With

    End Function
    Public Function GetDropAndFailGrades(ByVal ClsSect As String) As DataSet

        '   Instantiate DAL component
        With New DropStudentDB

            '   get the dataset with all TuitionCategories
            Return .GetDropAndFailGrades(ClsSect)

        End With

    End Function
    'Public Function DeleteScheduledCourses(ByVal FinalGrdObj As FinalGradeInfo, ByVal user As String)
    '    Dim DB As New DropStudentDB

    '    DB.DeleteScheduledCourses(FinalGrdObj, user)
    'End Function

    'Public Function UpdateFinalGrade(ByVal finalGrdObj As FinalGradeInfo, ByVal user As String)
    '    Dim db As New DropStudentDB
    '    db.UpdateFinalGrade(finalGrdObj, user)
    'End Function

    Public Function GetTerms() As DataSet

        '   Instantiate DAL component
        Dim DB As New DropStudentDB

        '   get the dataset with all degrees
        Return DB.GetCurrentTerms()

    End Function
    Public Function GetTerms(ByVal stuEnrollId As String, Optional ByVal campusId As String = "") As DataSet

        '   Instantiate DAL component
        Dim DB As New DropStudentDB

        '   get the dataset with all degrees
        Return DB.GetCurrentTerms(stuEnrollId, campusId)

    End Function
    Public Function GetAllTerms(ByVal stuEnrollId As String, Optional ByVal campusId As String = "") As DataSet

        '   Instantiate DAL component
        Dim DB As New DropStudentDB

        '   get the dataset with all degrees
        Return DB.GetAllTerms(stuEnrollId, campusId)

    End Function

    Public Shared Sub UpdateFinalGrade(ByVal pocoDropCourses As IList(Of PocoDropCourse))
        Dim db As New DropStudentDB
        db.UpdateFinalGrade(pocoDropCourses)
    End Sub
End Class
