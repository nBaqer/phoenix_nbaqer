

Public Class RefundsFacade

    Public Function GetAppliedPaymentsDS(ByVal stuEnrollId As String) As DataSet
        '   get the dataset with all AppliedPayments
        Return (New RefundsDB).GetAppliedPaymentsDS(stuEnrollId)
    End Function
    Public Function UpdateAppliedPaymentsDS(ByVal ds As DataSet) As String

        '   get the dataset with all Lenders
        Return (New RefundsDB).UpdateAppliedPaymentsDS(ds)

    End Function


    Public Function GetTransCodesofTypeRefunds(ByVal campusId As String) As DataSet
        Return (New RefundsDB).GetTransCodesofTypeRefundsByCampus(campusId)
    End Function
End Class
