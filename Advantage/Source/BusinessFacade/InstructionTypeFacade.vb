﻿Public Class InstructionTypeFacade

    '08/25/2011 JRobinson Clock Hour Project - Add new methods to support Instruction Type

#Region "Instruction Type"


    Public Function RemoveDefaultFromInstructionTypes(ByVal InstructionTypeId As String) As String

        Dim DB As New InstructionTypeDB
        Return DB.RemoveDefaultFromInstructionTypes(InstructionTypeId)

    End Function
    Public Function CheckInstructionTypeDup(ByVal InstructionTypeId As String, ByVal InstructionTypeCode As String, ByVal InstructionTypeDescrip As String) As String

        Dim DB As New InstructionTypeDB
        Return DB.CheckInstructionTypeDup(InstructionTypeId, InstructionTypeCode, InstructionTypeDescrip)

    End Function

    Public Function GetInstructionTypesByStatus(ByVal Status As String) As DataTable

        '   get the dataset with all Instruction Types
        Return (New InstructionTypeDB).GetInstructionTypesByStatus(Status)

    End Function
    Public Function GetAllInstructionTypeIds() As DataSet

        '   get the dataset with all Clock IDs
        Return (New InstructionTypeDB).GetAllInstructionTypeIds()

    End Function


    Public Function GetInstructionTypeInfo(ByVal InstructionTypeId As String) As InstructionTypeInfo

        '   Instantiate DAL component
        With New InstructionTypeDB

            '   get the InstructionTypeInfo
            Return .GetInstructionTypeInfo(InstructionTypeId)

        End With

    End Function

    Public Function UpdateInstructionTypeInfo(ByVal InstructionTypeInfo As InstructionTypeInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New InstructionTypeDB

            '   If it is a new account do an insert. If not, do an update
            If Not (InstructionTypeInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddInstructionTypeInfo(InstructionTypeInfo, user)
            Else
                '   return integer with update results
                Return .UpdateInstructionTypeInfo(InstructionTypeInfo, user)
            End If

        End With

    End Function
    Public Function DeleteInstructionTypeInfo(ByVal InstructionTypeId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New InstructionTypeDB

            '   delete TimeClockSpecialCodeInfo and return integer result
            Return .DeleteInstructionTypeInfo(InstructionTypeId, modDate)

        End With

    End Function

    Public Function GetDefaultInstructionTypeId() As DataSet

        '   get the dataset with default InstructionTypeId
        Return (New InstructionTypeDB).GetDefaultInstructionTypeId()

    End Function


#End Region

End Class
