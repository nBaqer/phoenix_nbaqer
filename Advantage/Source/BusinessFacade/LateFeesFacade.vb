Public Class LateFeesFacade
    Public Function GetAllLateFees() As DataSet
        '   get the dataset with all LateFees
        Return (New LateFeesDB).GetAllLateFees()

    End Function
    Public Function GetLateFeesInfo(ByVal LateFeesId As String) As LateFeesInfo

        '   get the LateFeesInfo
        Return (New LateFeesDB).GetLateFeesInfo(LateFeesId)

    End Function
    Public Function UpdateLateFeesInfo(ByVal LateFeesInfo As LateFeesInfo, ByVal user As String) As String


        '   If it is a new account do an insert. If not, do an update
        If Not (LateFeesInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New LateFeesDB).AddLateFeesInfo(LateFeesInfo, user)
        Else
            '   return integer with update results
            Return (New LateFeesDB).UpdateLateFeesInfo(LateFeesInfo, user)
        End If

    End Function
    Public Function DeleteLateFeesInfo(ByVal LateFeesId As String, ByVal modDate As DateTime) As String

        '   delete LateFeesInfo ans return string result
        Return (New LateFeesDB).DeleteLateFeesInfo(LateFeesId, modDate)

    End Function
    Public Function GetCurrentLateFeesInfo(ByVal campusId As String) As LateFeesInfo

        '   get the LateFeesInfo
        Return (New LateFeesDB).GetCurrentLateFeesInfo(campusId)

    End Function

End Class
