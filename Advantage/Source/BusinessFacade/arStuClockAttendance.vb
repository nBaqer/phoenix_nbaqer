

Public Class arStuClockAttendance
    Inherits BaseReportFacade


#Region "Public Methods"
    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim DayAttedance As New arStuClockAttendanceDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by report.

        ds = BuildReportSource(DayAttedance.GetDayAttendance(rptParamInfo, rptParamInfo.CampusId), DayAttedance.StudentIdentifier, rptParamInfo.ResId, rptParamInfo.FilterOtherString.Trim)

        'ds = ClsSectAttedance.GetclassSectionAttendance(rptParamInfo)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String, ByVal resid As Integer, ByVal Otherwhere As String) As DataSet
        ' Dim stuName As String
        Dim row As DataRow
        Dim temp As String
        Dim arStuClockAttendance As New arStuClockAttendanceDB
        Dim ScheduledHoursCalculations As New DataTable
        Dim facInputMasks As New InputMasksFacade
        Dim dtEnroll As DataTable = ds.Tables("StudentClockAttendance")
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count > 0 Then
                For Each row In dtEnroll.Rows
                    'Apply mask to SSN.
                    If StudentIdentifier = "SSN" Then
                        If Not row.IsNull("StudentIdentifier") Then
                            'If row("StudentIdentifier") <> "" Then
                            '    row("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, row("StudentIdentifier"))
                            'End If
                            If row("StudentIdentifier").ToString.Length >= 1 Then
                                temp = row("StudentIdentifier")
                                row("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
                            Else
                                row("StudentIdentifier") = ""
                            End If
                        End If
                    End If
                Next
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

End Class
