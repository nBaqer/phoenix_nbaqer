' ===============================================================================
' FAME AdvantageV1.BusinessFacade
'
' AcademicAdvisorsFacade.vb
'
' AcademicAdvisors and FinancialAidAdvisors Services Interface (Facade). 
'
' This class will be used to invoke the Data Access Logic for Academic Advisors and Financial Aid Advisors.
' An AdvisorTypeId param is passed to all methods of this class. This param is used to call either the 
' AcademicAdvisors methods or the FinancialAidAdvisors methods on AcademicAdvisorsDB.
'
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class AcademicAdvisorsFacade
    Public Function GetAcademicAdvisorsPerCampus(ByVal campusId As String, ByVal AdvisorTypeId As Integer) As DataSet
        Dim objDB As New AcademicAdvisorsDB

        If AdvisorTypeId = 1 Then
            '   return Dataset
            Return objDB.GetAcademicAdvisorsPerCampus(campusId)
        Else
            '   return Dataset
            Return objDB.GetFinancialAidAdvisorsPerCampus(campusId)
        End If

    End Function
    Public Function GetStudentsAssignedToASpecificAcademicAdvisor(ByVal AdvisorId As String, ByVal AdvisorTypeId As Integer, ByVal CampusId As String) As DataSet

        If AdvisorTypeId = 1 Then
            '   return Dataset
            Return (New AcademicAdvisorsDB).GetStudentsAssignedToASpecificAcademicAdvisor(AdvisorId, CampusId)
        Else
            '   return Dataset
            Return (New AcademicAdvisorsDB).GetStudentsAssignedToASpecificFinancialAidAdvisor(AdvisorId)
        End If


    End Function
    Public Function GetStudentsNotAssignedToAnyAcademicAdvisor(ByVal campusId As String, ByVal AdvisorTypeId As Integer) As DataSet

        If AdvisorTypeId = 1 Then
            '   return Dataset
            Return (New AcademicAdvisorsDB).GetStudentsNotAssignedToAnyAcademicAdvisor(campusId)
        Else
            '   return Dataset
            Return (New AcademicAdvisorsDB).GetStudentsNotAssignedToAnyFinancialAidAdvisor(campusId)
        End If

    End Function
    Public Function UpdateListOfStudentsAssignedToAnAcademicAdvisor(ByVal campusId As String, ByVal AdvisorId As String, ByVal AdvisorTypeId As Integer, ByVal user As String, ByVal selectedStudents() As String) As String

        If AdvisorTypeId = 1 Then
            Return (New AcademicAdvisorsDB).UpdateListOfStudentsAssignedToAnAcademicAdvisor(campusId, AdvisorId, user, selectedStudents)
        Else
            Return (New AcademicAdvisorsDB).UpdateListOfStudentsAssignedToAFinancialAidAdvisor(campusId, AdvisorId, user, selectedStudents)
        End If
    End Function

End Class

