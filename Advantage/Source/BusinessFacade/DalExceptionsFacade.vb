Public Class DalExceptionsFacade
    Public Function GetReferencedTablesByValue(ByVal tableName As String, ByVal value As String) As String
        '   get string with all referenced tables
        Return (New DALExceptions).GetReferencedTablesByValue(tableName, value)
    End Function
End Class
