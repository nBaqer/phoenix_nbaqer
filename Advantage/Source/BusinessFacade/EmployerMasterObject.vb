Public Class EmployerMasterObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim empList As New EmployerMasterDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(empList.GetEmployerList(rptParamInfo))
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    ' Copy data from dataset to datatable.
    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim zipMask As String
        ''  Dim strName As String
        Dim streetAddress As String
        Dim cityStateZip As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        Try
            ds.Tables(0).TableName = "EmployerMasterDetail"
            ds.Tables(0).Columns.Add(New DataColumn("FullAddress", System.Type.GetType("System.String")))
            ds.Tables(0).Columns.Add(New DataColumn("EmployerCount", System.Type.GetType("System.Int32")))

            For Each dr As DataRow In ds.Tables(0).Rows
                dr("EmployerCount") = ds.Tables(0).Rows.Count
                '
                'Address1 allows NULL.
                If Not (dr("Address1") Is System.DBNull.Value) Then
                    If dr("Address1") <> "" Then
                        streetAddress = dr("Address1")
                    End If
                Else
                    streetAddress = ""
                End If
                'Address2 allows NULL.
                If Not (dr("Address2") Is System.DBNull.Value) Then
                    If dr("Address2") <> "" Then
                        streetAddress &= " " & dr("Address2")
                    End If
                End If
                dr("FullAddress") = streetAddress
                'City allows NULL.
                If Not (dr("City") Is System.DBNull.Value) Then
                    If dr("City") <> "" Then
                        cityStateZip = dr("City")
                    End If
                Else
                    cityStateZip = ""
                End If
                'State allows NULL.
                If Not (dr("StateDescrip") Is System.DBNull.Value) Then
                    If Not (dr("ForeignZip") Is System.DBNull.Value) Then
                        If dr("StateDescrip") <> "" And dr("ForeignZip") = False Then
                            'Domestic State
                            If cityStateZip <> "" Then
                                cityStateZip &= ", " & dr("StateDescrip")
                            Else
                                cityStateZip = dr("StateDescrip")
                            End If
                        End If
                    Else
                        'Domestic State
                        If dr("StateDescrip") <> "" Then
                            If cityStateZip <> "" Then
                                cityStateZip &= ", " & dr("StateDescrip")
                            Else
                                cityStateZip = dr("StateDescrip")
                            End If
                        End If
                    End If
                End If
                'StateOther allows NULL.
                If Not (dr("OtherState") Is System.DBNull.Value) Then
                    If Not (dr("ForeignZip") Is System.DBNull.Value) Then
                        If dr("OtherState") <> "" And dr("ForeignZip") = True Then
                            'International State
                            If cityStateZip <> "" Then
                                cityStateZip &= ", " & dr("OtherState")
                            Else
                                cityStateZip = dr("OtherState")
                            End If
                        End If
                    End If
                End If
                '
                'Zip allows NULL.
                If Not (dr("Zip") Is System.DBNull.Value) Then
                    If Not (dr("ForeignZip") Is System.DBNull.Value) Then
                        If dr("Zip") <> "" And dr("ForeignZip") = False Then
                            'Domestic Zip
                            If cityStateZip <> "" Then
                                cityStateZip &= " " & facInputMasks.ApplyMask(zipMask, dr("Zip"))
                            Else
                                cityStateZip = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                            End If
                        Else
                            'International Zip
                            If dr("Zip") <> "" Then
                                If cityStateZip <> "" Then
                                    cityStateZip &= " " & dr("Zip")
                                Else
                                    cityStateZip = dr("Zip")
                                End If
                            End If
                        End If
                    Else
                        'Domestic Zip
                        If dr("Zip") <> "" Then
                            If cityStateZip <> "" Then
                                cityStateZip &= " " & facInputMasks.ApplyMask(zipMask, dr("Zip"))
                            Else
                                cityStateZip = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                            End If
                        End If
                    End If
                End If
                '
                If cityStateZip <> "" Then
                    If dr("FullAddress") <> "" Then
                        dr("FullAddress") &= " " & cityStateZip
                    Else
                        dr("FullAddress") = cityStateZip
                    End If
                End If
                '
                'Apply mask.
                If Not (dr("Phone") Is System.DBNull.Value) Then
                    If Not (dr("ForeignPhone") Is System.DBNull.Value) Then
                        If dr("Phone") <> "" And dr("ForeignPhone") = False Then
                            'Domestic Phone
                            dr("Phone") = facInputMasks.ApplyMask(strMask, dr("Phone"))
                        Else
                            'International Phone
                            'dr("Phone") = dr("Phone")
                        End If
                    Else
                        'Domestic Phone
                        If dr("Phone") <> "" Then
                            dr("Phone") = facInputMasks.ApplyMask(strMask, dr("Phone"))
                        End If
                    End If
                End If
                '
                'Apply mask.
                If Not (dr("Fax") Is System.DBNull.Value) Then
                    If Not (dr("ForeignFax") Is System.DBNull.Value) Then
                        If dr("Fax") <> "" And dr("ForeignFax") = False Then
                            'Domestic Fax
                            dr("Fax") = facInputMasks.ApplyMask(strMask, dr("Fax"))
                        Else
                            'International Fax
                            'dr("Fax") = dr("Fax") 
                        End If
                    Else
                        If dr("Fax") <> "" Then
                            'Domestic Fax
                            dr("Fax") = facInputMasks.ApplyMask(strMask, dr("Fax"))
                        End If
                    End If
                End If
            Next

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region


End Class
