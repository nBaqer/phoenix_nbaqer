Public Class StuLowGPAObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim obj As New StuTranscriptDB
        Dim ds As DataSet
        'Get list of enrollments for ProgramVersion.
        'Massage dataset to produce one with the columns and data expected by the StuLowGPA report. 
        'The StuLowGPA report, whatever its frontend is, will only need to display the data in the dataset.         
        ds = BuildReportSource(obj.GetStuLowGPA(paramInfo), obj.StudentIdentifier, obj.MinimumGPA, obj.GradeCourseRepetitionsMethod, obj.IncludeHoursForFailedGrade, paramInfo.CampusId)
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String, ByVal minimumGPA As String, ByVal gradeCourseRepetitionsMethod As String, ByVal includeHoursForFailedGrade As Boolean, campusId As String) As DataSet
        Dim Transcript As New TranscriptFacade
        Dim StuEnrollId As String
        Dim stuName As String
        Dim facInputMasks As New InputMasksFacade
        Dim fac As New GraduateAuditFacade
        Dim transcriptUtil As New FAME.AdvantageV1.Common.Utilities
        Dim temp As String
        Dim gpa As Decimal

        'Get the mask for SSN
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count = 1 Then

                Dim dtStudents As DataTable = ds.Tables("StudentsWithLowGPA")

                For Each row As DataRow In dtStudents.Rows
                    row("PrgVerIdStr") = row("PrgVerId").ToString()

                    'Set up student name as: "LastName, FirstName MI."
                    stuName = row("LastName")
                    If Not row.IsNull("FirstName") Then
                        If row("FirstName") <> "" Then
                            stuName &= ", " & row("FirstName")
                        End If
                    End If
                    If Not row.IsNull("MiddleName") Then
                        If row("MiddleName") <> "" Then
                            stuName &= " " & row("MiddleName") & "."
                        End If
                    End If
                    row("StudentName") = stuName

                    'Apply mask to SSN.
                    If StudentIdentifier = "SSN" Then
                        If Not row.IsNull("StudentIdentifier") Then
                            If row("StudentIdentifier") <> "" Then
                                'row("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, row("StudentIdentifier"))
                                If row("StudentIdentifier").ToString.Length >= 1 Then
                                    temp = row("StudentIdentifier")
                                    row("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                Else
                                    row("StudentIdentifier") = ""
                                End If
                            End If
                        End If
                    End If

                    'Get program student is enrolled into.
                    StuEnrollId = row("StuEnrollId").ToString

                    '************************************************************************
                    'For every enrollment, get results and summary info.
                    '
                    gpa = fac.GetGPA(StuEnrollId, gradeCourseRepetitionsMethod, , includeHoursForFailedGrade, campusId)

                    If fac.GetTotalClasses > 0 Then
                        If gpa <= Decimal.Parse(minimumGPA) Then
                            row("GPA") = gpa
                        Else
                            'mark row for deletion because GPA is greater than minimum required.
                            row.Delete()
                        End If
                    Else
                        'mark row for deletion there is no GPA.
                        row.Delete()
                    End If
                    If fac.GradeInGPA = False Then
                        row.Delete()
                    End If
                Next

                'commit all changes.
                ds.AcceptChanges()

                For Each row As DataRow In dtStudents.Rows
                    row("StudentCount") = dtStudents.Rows.Count
                Next

            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region


End Class
