Public Class CopyClsSectFacade
    Public Function InitialLoadPage() As DataSet
        Dim ds As New DataSet
        'Dim olddt As DataTable
        'Dim newdt As DataTable
        Dim db As New CopyClsSectDB

        ds = db.GetDropDownList()

        'Add the concatenated FullName column to the datatable before sending it to the web page
        ds = ConcatenateEmployeeLastFirstName(ds)
        Return ds
    End Function
    Public Function GetSDateEDate(ByVal Term As String) As DataSet
        Dim ds As New DataSet
        'Dim olddt As DataTable
        'Dim newdt As DataTable
        Dim db As New CopyClsSectDB

        ds = db.GetStartEndDates(Term)

        Return ds
    End Function

    Public Function ConcatenateEmployeeLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("EmpId")}
        End With
        If ds.Tables("InstructorDT").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                row("FullName") = row("LastName") & ", " & row("FirstName")
            Next
        End If
        Return ds
    End Function
    Public Function PopulateDataGrid(ByVal Term As String, ByVal Term2 As String, ByVal CampusId As String) As DataSet

        Dim db As New CopyClsSectDB
        Dim ds As New DataSet

        ds = db.GetDataGridInfo(Term, Term2, CampusId)

        Return ds

    End Function

    Public Function ValidateClsSectMtgs(ByVal ClsSectMtgArrList As ArrayList) As String
        Dim ClsSectMeeting As ClsSectMeetingInfo
        Dim validation As New DALExceptions
        Dim msg As String = ""
        Dim ErrMsgObj As New ErrMsgInfo
        Dim ArrList As New ArrayList

        For Each ClsSectMeeting In ClsSectMtgArrList
            If (IsObjectNotEmpty(ClsSectMeeting)) Then

                'Check for conflicts
                msg = validation.TimeCollisions(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.Day.ToString, ClsSectMeeting.STimeDescrip, ClsSectMeeting.ETimeDescrip, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)

                'If msg <> "" Then ' ERROR MSG
                '    'Return msg
                '    ErrMsgObj.ErrMsg = msg
                '    ArrList.Add(ErrMsgObj)
                'Else
                '    ErrMsgObj.ErrMsg = msg
                '    ArrList.Add(ErrMsgObj)
                'End If

            End If
        Next
        Return msg
    End Function

    Private Function IsObjectNotEmpty(ByVal ClsSectMeeting As ClsSectMeetingInfo) As Boolean

        Dim obj As Boolean = False

        If Left(ClsSectMeeting.Day.ToString, 6) <> "000000" Then
            obj = True
        End If
        If Left(ClsSectMeeting.Room.ToString, 6) <> "000000" Then
            obj = True
        End If
        If Left(ClsSectMeeting.StartTime.ToString, 6) <> "000000" Then
            obj = True
        End If '
        If Left(ClsSectMeeting.EndTime.ToString, 6) <> "000000" Then
            obj = True
        End If '
        Return obj
    End Function

    Public Function GetClsSectMtgs(ByVal ClsSectId As Guid) As DataSet
        Dim db As New ClassSectionDB
        Dim ClsSectArrayList As New ArrayList

        Dim ds As DataSet
        'Dim ds1 As DataSet
        ds = db.GetClsSectMtgs(ClsSectId)
        Return ds
    End Function

    Public Function ConvertClsSectMtgsToArrList(ByVal ds As DataSet, ByVal InstructorId As String, ByVal ClsSectionId As Guid, ByVal SDate As String, ByVal EDate As String) As ArrayList

        'Dim count As Integer
        '' Dim strControl As String
        Dim ClsSectArrayList As New ArrayList
        Dim dt As DataTable
        Dim row As DataRow
        dt = ds.Tables("ClsSectMtgs")
        If ds.Tables.Count > 1 Then
            'Error - it returned more than one record
        Else
            'Loop thru the controls, find the appropriate dropdown(Day/Room/TimeInterval)
            'and set the object properties based on the ddl's that are set.
            For Each row In dt.Rows
                Dim ClsSectMtgObject As New ClsSectMeetingInfo
                ClsSectMtgObject.ClsSectId = ClsSectionId
                If Not row("WorkDaysId") Is System.DBNull.Value Then ClsSectMtgObject.Day = row("WorkDaysId")
                ClsSectMtgObject.Room = row("RoomId")
                If (Not row("TimeIntervalId") Is System.DBNull.Value) And row("TimeIntervalId").ToString <> "00000000-0000-0000-0000-000000000000" Then
                    ClsSectMtgObject.StartTime = row("TimeIntervalId")
                End If
                If (Not row("EndIntervalId") Is System.DBNull.Value) And row("EndIntervalId").ToString <> "00000000-0000-0000-0000-000000000000" Then
                    ClsSectMtgObject.EndTime = row("EndIntervalId")
                End If
                'If Not row("EndIntervalId") Is System.DBNull.Value Then ClsSectMtgObject.EndTime = row("EndIntervalId")
                ClsSectMtgObject.ClsSectMtgId = row("ClsSectMeetingId")
                ClsSectMtgObject.InstructorId = InstructorId
                If Not row("TimeIntervalId") Is System.DBNull.Value Then ClsSectMtgObject.STimeDescrip = row("TimeIntervalId").ToString
                If Not row("TimeIntervalId") Is System.DBNull.Value Then ClsSectMtgObject.ETimeDescrip = row("EndIntervalId").ToString
                ClsSectMtgObject.StartDate = CDate(SDate)
                ClsSectMtgObject.EndDate = CDate(EDate)
                'If Not row("StartDate") Is System.DBNull.Value Then ClsSectMtgObject.StartDate = row("StartDate")
                'If Not row("EndDate") Is System.DBNull.Value Then ClsSectMtgObject.EndDate = row("EndDate")
                If Not row("PeriodId") Is System.DBNull.Value Then ClsSectMtgObject.PeriodId = row("PeriodId")
                If Not row("AltPeriodId") Is System.DBNull.Value Then ClsSectMtgObject.AltPeriodId = row("AltPeriodId")
                ' 10/25/2011 Janet Robinson - Clock Hours Project Added new field
                If row("InstructionTypeId").ToString <> "" Then
                    ClsSectMtgObject.InstructionTypeId = row("InstructionTypeId")
                End If

                'Add the object to the array list
                ClsSectArrayList.Add(ClsSectMtgObject)
            Next
        End If

        Return ClsSectArrayList
    End Function

    Public Function AddClsSectMeeting(ByVal ClsSectMtgArrList As ArrayList, ByVal user As String, ByVal chkIgnore As Boolean, ByVal ClsSectId As Guid, ByVal modDate As DateTime) As ArrayList
        Dim db As New ClassSectionDB
        Dim ClsSectMeeting As ClsSectMeetingInfo
        Dim obj As New ClsSectMtgIdObjInfo
        Dim ErrMsgObj As New ErrMsgInfo
        Dim ArrList As New ArrayList
        Dim count As Integer = 0
        Dim validation As New DALExceptions
        Dim msg As String
        ''    Dim deleteMsg As String


        For Each ClsSectMeeting In ClsSectMtgArrList
            count = count + 1
            'if object is not empty
            'then add each single record
            If (IsObjectNotEmpty(ClsSectMeeting)) Then

                'Check for conflicts
                If (chkIgnore = False) Then
                    If ClsSectMeeting.Day.ToString <> "00000000-0000-0000-0000-000000000000" Then
                        msg = validation.TimeCollisions(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.Day.ToString, ClsSectMeeting.STimeDescrip, ClsSectMeeting.ETimeDescrip, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)
                    End If
                End If

                If msg <> "" Then ' ERROR MSG
                    'Return msg
                    'deleteMsg = db.DeleteClassSection(ClsSectId, modDate)
                    ErrMsgObj.ErrMsg = msg
                    ArrList.Add(ErrMsgObj)
                Else
                    'Generate a new guid for the insert
                    ClsSectMeeting.ClsSectMtgId = System.Guid.NewGuid
                    'Add the single record(one ClsSectMeeting object) to the database
                    db.AddSingleClsSectMeeting(ClsSectMeeting, user)

                    'Add this new guid to an arraylist, so it can be used later on
                    'for update purposes.
                    obj.ClsSectMtgId = ClsSectMeeting.ClsSectMtgId
                    obj.count = count
                    obj.txtboxname = "txtClsSectMeetingId"
                    ArrList.Add(obj)
                End If

            End If

        Next
        Return ArrList

    End Function

    Public Function AddClassSection(ByVal ClassSection As ClassSectionInfo, ByVal user As String) As ClsSectionResultInfo
        Dim db As New ClassSectionDB
        Return db.AddClassSection(ClassSection, user)
    End Function
End Class
