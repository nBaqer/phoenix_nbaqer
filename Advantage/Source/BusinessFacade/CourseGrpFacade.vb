Public Class CourseGrpFacade
    Public Function GetAllCourseGrps() As DataSet

        '   Instantiate DAL component
        Dim DB As New CourseGrpsDB

        '   get the dataset with all degrees
        Return DB.GetCourseGrps()

    End Function
    Public Function GetAllStatuses() As DataSet

        '   Instantiate DAL component
        Dim reqTypesDB As New ReqTypesDB

        '   get the dataset with all degrees
        Return reqTypesDB.GetAllStatuses()

    End Function

    Public Function GetAllCampGrps() As DataSet

        '   Instantiate DAL component
        Dim reqTypesDB As New ReqTypesDB

        '   get the dataset with all degrees
        Return reqTypesDB.GetAllCampGrps()

    End Function
    Public Function GetCourseGrpDetails(ByVal CourseGrpId As String) As CourseGrpInfo
        With New CourseGrpsDB
            Return .GetCourseGrpDetails(CourseGrpId)
        End With
    End Function

    Public Function InsertCourseGrp(ByVal CourseGrp As CourseGrpInfo, ByVal user As String) As String
        Dim db As New CourseGrpsDB

        Return db.InsertCourseGrp(CourseGrp, user)
    End Function
    Public Function DeleteCourseGrp(ByVal CourseGrp As String, ByVal modDate As DateTime) As String
        Dim DB As New CourseGrpsDB

        Return DB.DeleteCourseGrp(CourseGrp, modDate)
    End Function

    Public Function UpdateCourseGrp(ByVal CourseGrp As CourseGrpInfo, ByVal user As String) As String
        Dim DB As New CourseGrpsDB

        Return DB.UpdateCourseGrp(CourseGrp, user)
    End Function

    Public Function GetCourseGrpName(ByVal courseGrpId As String) As String
        Dim DB As New CourseGrpsDB

        Return DB.GetCourseGrpName(courseGrpId)
    End Function
End Class
