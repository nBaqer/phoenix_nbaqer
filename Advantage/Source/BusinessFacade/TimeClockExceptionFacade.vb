Public Class TimeClockExceptionFacade
    Inherits BaseReportFacade

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim TimClockExcept As New TimeClockExceptionVb
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by report.
        ds = BuildReportSource(TimClockExcept.GetTimeClockExceptions(rptParamInfo), TimClockExcept.StudentIdentifier)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function
#End Region

#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim stuName As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String
        Dim oldCmpGrp As String = ""
        Dim oldCampus As String = ""
        Dim dt As DataTable
        Dim dr2 As DataRow
        'Get the mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'Use table to store Total Amounts per Campus Groups and Campuses.
                    dt = ds.Tables(0)
                    For Each dr As DataRow In ds.Tables(0).Rows

                        'If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpDescrip") Then
                        '    'Get a row everytime there is a Campus Group.
                        '    dr2 = dt.NewRow
                        '    dr2("CampGrpDescrip") = dr("CampGrpDescrip")
                        '    dr2("CampGrpTotal") = ds.Tables(0).Compute("SUM(TransAmount)", "CampGrpDescrip='" & dr("CampGrpDescrip") & "'")
                        '    oldCmpGrp = dr("CampGrpDescrip")
                        '    oldCampus = ""
                        'End If
                        'If oldCmpGrp = dr("CampGrpDescrip") And oldCampus = "" Then
                        '    'Add the row created on the above code into dt table.
                        '    dr2("CampDescrip") = dr("CampDescrip")
                        '    dr2("CampusTotal") = ds.Tables(0).Compute("SUM(TransAmount)", "CampGrpDescrip='" & dr("CampGrpDescrip") & "' AND CampDescrip='" & dr("CampDescrip") & "'")
                        '    dt.Rows.Add(dr2)
                        '    oldCampus = dr("CampDescrip")
                        '    '
                        'ElseIf oldCmpGrp = dr("CampGrpDescrip") And oldCampus <> dr("CampDescrip") Then
                        '    'For every different Campus, get a new row and add it to dt table.
                        '    'This section of the code will be used when a Campus Group has several Campuses.
                        '    dr2 = dt.NewRow
                        '    dr2("CampGrpDescrip") = oldCmpGrp
                        '    dr2("CampGrpTotal") = ds.Tables(0).Compute("SUM(TransAmount)", "CampGrpDescrip='" & oldCmpGrp & "'")
                        '    dr2("CampDescrip") = dr("CampDescrip")
                        '    dr2("CampusTotal") = ds.Tables(0).Compute("SUM(TransAmount)", "CampGrpDescrip='" & oldCmpGrp & "' AND CampDescrip='" & dr("CampDescrip") & "'")
                        '    dt.Rows.Add(dr2)
                        '    oldCampus = dr("CampDescrip")
                        'End If
                        '
                        'Set up student name as: "LastName, FirstName MI."
                        'stuName = dr("LastName")
                        'If Not (dr("FirstName") Is System.DBNull.Value) Then
                        '    If dr("FirstName") <> "" Then
                        '        stuName &= ", " & dr("FirstName")
                        '    End If
                        'End If
                        'If Not (dr("MiddleName") Is System.DBNull.Value) Then
                        '    If dr("MiddleName") <> "" Then
                        '        stuName &= " " & dr("MiddleName") & "."
                        '    End If
                        'End If
                        'dr("StudentName") = stuName

                        'Apply mask to SSN.
                        If StudentIdentifier = "SSN" Then
                            If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                                If dr("StudentIdentifier") <> "" Then
                                    Dim temp As String = dr("StudentIdentifier")
                                    dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                End If
                            End If
                        End If

                        If Not (dr("BadgeNumber") Is System.DBNull.Value) Then
                            If dr("BadgeNumber") <> "" Then
                                dr("BadgeId") = dr("BadgeNumber")
                            End If
                        End If


                    Next

                End If
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try
        ds.AcceptChanges()
        Return ds
    End Function

#End Region



End Class
