Imports FAME.AdvantageV1.DataAccess
Public Class AdmissionRepWTDObject
    Inherits BaseReportFacade

    Private Enum LeadStatus
        IsNull = 0
        NewLead = 1
        IScheduled = 2
        Interviewed = 3
        Enrolled = 4
        Others = 5
        ''These Statuses added By Saraswathi lakshmanan
        ApplicationReceived = 6
        ApplicationNotAccepted = 7
        DeadLead = 8
        WillEnrollinFuture = 9
    End Enum

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim objDB As New AdmissionRepPerformanceDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(objDB.GetWeekToDateMTDRepPerformance(rptParamInfo))
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        Dim arrRows() As DataRow
        Dim arrTotStudents() As DataRow
        Dim arrNoStartStudents() As DataRow

        Try
            If ds.Tables.Count = 14 Then
                Dim dtWTDDetail As DataTable = ds.Tables("WTDDetail")
                Dim dtPerformance As DataTable = ds.Tables("Perfomance")

                Dim dtSource As DataTable = ds.Tables("SourceCategory")
                Dim dtProgram As DataTable = ds.Tables("Program")

                Dim dtCampusTtl As DataTable = ds.Tables("CampusTotalsPerf")
                Dim dtCampGrpTtl As DataTable = ds.Tables("CampusGroupTotalsPerf")

                Dim dtCampusTtlProg As DataTable = ds.Tables("CampusTotalsProg")
                Dim dtCampGrpTtlProg As DataTable = ds.Tables("CampusGroupTotalsProg")
                Dim dtCampusTtlSource As DataTable = ds.Tables("CampusTotalsSource")
                Dim dtCampGrpTtlSource As DataTable = ds.Tables("CampusGroupTotalsSource")

                Dim dtStudents As DataTable = ds.Tables("StudentsCount")
                Dim dtNoStartStudents As DataTable = ds.Tables("NoStartCount")

                Dim dtCampusDetails As DataTable = ds.Tables("CampusDetails")

                Dim noofStudents As Integer = 0
                Dim noofNoStarts As Integer = 0
                Dim NoOfStarts As Integer = 0


                For Each dr As DataRow In dtPerformance.Rows
                    dr("AdRepCount") = dtPerformance.Rows.Count
                    'fields used to link tables on report
                    dr("CampGrpIdStr") = dr("CampGrpId").ToString
                    dr("CampusIdStr") = dr("CampusId").ToString
                    dr("AdmissionsRepStr") = dr("AdmissionsRep").ToString

                    '   Week To Date Performance (WTD)
                    '   get all leads that belong to one Admission Rep from WTDDetail table
                    arrRows = dtWTDDetail.Select("CampGrpId = '" & dr("CampGrpId").ToString & "' " & _
                                                "AND CampusId = '" & dr("CampusId").ToString & "' " & _
                                                "AND AdmissionsRep = '" & dr("AdmissionsRep").ToString & "'")

                    If arrRows.GetLength(0) > 0 Then
                        'Compute status changes taking into account reversals.
                        ComputeChanges_Revised(dr, arrRows, True)
                        'Compute Percentages
                        ComputePercentage_revised(dr, True)
                    End If

                    '   get all the students in the arStuEnrollments table with the expected startDate between the given date range
                    arrTotStudents = dtStudents.Select("CampGrpId = '" & dr("CampGrpId").ToString & "' " & _
                                                "AND CampusId = '" & dr("CampusId").ToString & "' " & _
                                                "AND AdmissionsRep = '" & dr("AdmissionsRep").ToString & "'")

                    If arrTotStudents.GetLength(0) > 0 Then
                        noofStudents = arrTotStudents.Length
                    Else
                        noofStudents = 0
                    End If
                    '   get the list of NoStart students in the arStuEnrollments table with the expected startDate between the given date range
                    arrNoStartStudents = dtNoStartStudents.Select("CampGrpId = '" & dr("CampGrpId").ToString & "' " & _
                                                "AND CampusId = '" & dr("CampusId").ToString & "' " & _
                                                "AND AdmissionsRep = '" & dr("AdmissionsRep").ToString & "'")

                    If arrNoStartStudents.GetLength(0) > 0 Then
                        noofNoStarts = arrNoStartStudents.Length
                    Else
                        noofNoStarts = 0
                    End If

                    NoOfStarts = noofStudents - noofNoStarts
                    dr("StartsCount") = NoOfStarts
                    If arrRows.GetLength(0) > 0 Then
                        'Compute status changes taking into account reversals.
                        'ComputeChanges_Revised(dr, arrRows, True)
                        'Compute Percentages
                        ComputePercentage_revised(dr, True)
                    End If
                Next

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                For Each dr As DataRow In dtSource.Rows
                    dr("SourceCount") = dtSource.Rows.Count
                    'fields used to link tables on report
                    dr("CampGrpIdStr") = dr("CampGrpId").ToString
                    dr("CampusIdStr") = dr("CampusId").ToString
                    dr("SourceDescrip") = dr("SourceCatagoryDescrip").ToString

                    '   Week To Date Performance (WTD)
                    '   get all leads that belong to one Admission Rep from WTDDetail table
                    arrRows = dtWTDDetail.Select("CampGrpId = '" & dr("CampGrpId").ToString & "' " & _
                                                "AND CampusId = '" & dr("CampusId").ToString & "' " & _
                                                "AND SourceCategoryID = '" & dr("SourceCategoryID").ToString & "'")

                    If arrRows.GetLength(0) > 0 Then
                        'Compute status changes taking into account reversals.
                        ComputeChanges_Revised(dr, arrRows, True)
                        'Compute Percentages
                        ComputePercentage_revised(dr, True)
                    End If

                    '   get all the students in the arStuEnrollments table with the expected startDate between the given date range
                    arrTotStudents = dtStudents.Select("CampGrpId = '" & dr("CampGrpId").ToString & "' " & _
                                                "AND CampusId = '" & dr("CampusId").ToString & "' " & _
                                                 "AND SourceCategoryID = '" & dr("SourceCategoryID").ToString & "'")

                    If arrTotStudents.GetLength(0) > 0 Then
                        noofStudents = arrTotStudents.Length
                    Else
                        noofStudents = 0
                    End If
                    '   get the list of NoStart students in the arStuEnrollments table with the expected startDate between the given date range
                    arrNoStartStudents = dtNoStartStudents.Select("CampGrpId = '" & dr("CampGrpId").ToString & "' " & _
                                                "AND CampusId = '" & dr("CampusId").ToString & "' " & _
                                                 "AND SourceCategoryID = '" & dr("SourceCategoryID").ToString & "'")

                    If arrNoStartStudents.GetLength(0) > 0 Then
                        noofNoStarts = arrNoStartStudents.Length
                    Else
                        noofNoStarts = 0
                    End If

                    NoOfStarts = noofStudents - noofNoStarts
                    dr("StartsCount") = NoOfStarts
                    If arrRows.GetLength(0) > 0 Then
                        'Compute status changes taking into account reversals.
                        'ComputeChanges_Revised(dr, arrRows, True)
                        'Compute Percentages
                        ComputePercentage_revised(dr, True)
                    End If
                Next
                ''if the Source category is NUll in the DtWTDTable. we will not get those count . since we a re grouping only based on the source.
                ''So get all the null sourceCategories from dtWTDtable and insert a row into the dtsource table


                '   Week To Date Performance (WTD)
                '   get all leads who donot have SourceCategoryId defined in the adleads table

                For Each drCamp As DataRow In dtCampusDetails.Rows
                    arrRows = dtWTDDetail.Select("CampGrpId = '" & drCamp("CampGrpId").ToString & "' " & _
                                                "AND CampusId = '" & drCamp("CampusId").ToString & "' " & _
                                                "AND SourceCategoryID is Null ")

                    If arrRows.GetLength(0) > 0 Then
                        Dim dr As DataRow
                        dr = dtSource.NewRow
                        dr("SourceCount") = dtSource.Rows.Count
                        'fields used to link tables on report
                        dr("SourceCategoryID") = System.Guid.NewGuid
                        dr("CampGrpIdStr") = drCamp("CampGrpId").ToString
                        dr("CampusIdStr") = drCamp("CampusId").ToString
                        dr("SourceDescrip") = "No Source Specified"
                        dr("CampGrpID") = drCamp("CampGrpId").ToString
                        dr("CampusID") = drCamp("CampusId").ToString
                        dr("SourceCatagoryDescrip") = "No Source Specified"
                        dtSource.Rows.Add(dr)
                        'Compute status changes taking into account reversals.
                        ComputeChanges_Revised(dr, arrRows, True)
                        'Compute Percentages
                        ComputePercentage_revised(dr, True)
                    End If
                Next

                For Each dr As DataRow In dtProgram.Rows
                    dr("ProgramCount") = dtProgram.Rows.Count
                    'fields used to link tables on report
                    dr("CampGrpIdStr") = dr("CampGrpId").ToString
                    dr("CampusIdStr") = dr("CampusId").ToString
                    dr("ProgramDescrip") = dr("ProgDescrip").ToString

                    '   Week To Date Performance (WTD)
                    '   get all leads that belong to one Admission Rep from WTDDetail table
                    arrRows = dtWTDDetail.Select("CampGrpId = '" & dr("CampGrpId").ToString & "' " & _
                                                "AND CampusId = '" & dr("CampusId").ToString & "' " & _
                                                "AND ProgramID = '" & dr("ProgramID").ToString & "'")

                    If arrRows.GetLength(0) > 0 Then
                        'Compute status changes taking into account reversals.
                        ComputeChanges_Revised(dr, arrRows, True)
                        'Compute Percentages
                        ComputePercentage_revised(dr, True)
                    End If
                    '   get all the students in the arStuEnrollments table with the expected startDate between the given date range
                    arrTotStudents = dtStudents.Select("CampGrpId = '" & dr("CampGrpId").ToString & "' " & _
                                                "AND CampusId = '" & dr("CampusId").ToString & "' " & _
                                                 "AND ProgramID = '" & dr("ProgramID").ToString & "'")


                    If arrTotStudents.GetLength(0) > 0 Then
                        noofStudents = arrTotStudents.Length
                    Else
                        noofStudents = 0
                    End If
                    '   get the list of NoStart students in the arStuEnrollments table with the expected startDate between the given date range
                    arrNoStartStudents = dtNoStartStudents.Select("CampGrpId = '" & dr("CampGrpId").ToString & "' " & _
                                                "AND CampusId = '" & dr("CampusId").ToString & "' " & _
                                                  "AND ProgramID = '" & dr("ProgramID").ToString & "'")


                    If arrNoStartStudents.GetLength(0) > 0 Then
                        noofNoStarts = arrNoStartStudents.Length
                    Else
                        noofNoStarts = 0
                    End If

                    NoOfStarts = noofStudents - noofNoStarts
                    dr("StartsCount") = NoOfStarts
                    If arrRows.GetLength(0) > 0 Then
                        'Compute status changes taking into account reversals.
                        'ComputeChanges_Revised(dr, arrRows, True)
                        'Compute Percentages
                        ComputePercentage_revised(dr, True)
                    End If

                Next

                ''Similarly if the Program is NUll in the DtWTDTable. we will not get those count . since we are grouping only based on the Program.
                ''So get all the null Program from dtWTDtable and insert a row into the dtprogram table


                '   Week To Date Performance (WTD)
                '   get all leads who donot have ProgramId defined in the adleads table

                For Each drCamp As DataRow In dtCampusDetails.Rows
                    arrRows = dtWTDDetail.Select("CampGrpId = '" & drCamp("CampGrpId").ToString & "' " & _
                                                "AND CampusId = '" & drCamp("CampusId").ToString & "' " & _
                                                "AND ProgramID is Null ")

                    If arrRows.GetLength(0) > 0 Then
                        Dim dr As DataRow
                        dr = dtProgram.NewRow
                        dr("ProgramCount") = dtProgram.Rows.Count
                        'fields used to link tables on report
                        dr("ProgramID") = System.Guid.NewGuid
                        dr("CampGrpIdStr") = drCamp("CampGrpId").ToString
                        dr("CampusIdStr") = drCamp("CampusId").ToString
                        dr("ProgramDescrip") = "No Program Specified"
                        dr("CampGrpID") = drCamp("CampGrpId").ToString
                        dr("CampusID") = drCamp("CampusId").ToString
                        dr("ProgDescrip") = "No Program Specified"
                        dtProgram.Rows.Add(dr)
                        'Compute status changes taking into account reversals.
                        ComputeChanges_Revised(dr, arrRows, True)
                        'Compute Percentages
                        ComputePercentage_revised(dr, True)
                    End If
                Next

                'The 

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Fill campus group and campus total tables
                ComputeCampusGroupTotals_revised(dtPerformance, dtCampusTtl, dtCampGrpTtl)
                'Fill campus group and campus total tables
                ComputeCampusGroupTotals_revised(dtProgram, dtCampusTtlProg, dtCampGrpTtlProg)
                'Fill campus group and campus total tables
                ComputeCampusGroupTotals_revised(dtSource, dtCampusTtlSource, dtCampGrpTtlSource)

                Dim i As Integer
                For i = dtCampusDetails.Rows.Count - 1 To 0 Step -1
                    ''Remove the Campus if it dosen't have any details for that campus
                    Dim foundRows() As Data.DataRow
                    foundRows = dtWTDDetail.Select("CampusID='" & dtCampusDetails.Rows(i)("CampusID").ToString & "'")
                    If foundRows.Length = 0 Then
                        ''Delete the Campus from the campus Details table
                        dtCampusDetails.Rows(i).Delete()
                    End If
                Next
                dtCampusDetails.AcceptChanges()
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

    Private Function ValExistsInDT(ByVal dt As DataTable, ByVal columnName As String, ByVal value As String) As Boolean
        Dim rows() As DataRow

        rows = dt.Select(columnName & " = " & value)
        If rows.GetLength(0) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub ComputeChanges(ByVal dr As DataRow, ByVal arrRows() As DataRow, ByVal isWTD As Boolean)
        Dim matrix(6, 6) As Integer
        Dim i, j As Integer

        'Initialize matrix with zeros
        For i = 0 To 6
            For j = 0 To 6
                matrix(i, j) = 0
            Next
        Next

        Dim skip As Boolean

        If arrRows.GetLength(0) > 0 Then
            For Each row As DataRow In arrRows
                skip = False
                If row.IsNull("SysOrigStatusId") Then
                    i = LeadStatus.IsNull
                Else
                    If row("SysOrigStatusId") = row("SysNewStatusId") Then
                        skip = True
                    End If
                End If
                'ignore rows with original status equal to new status, because there was no change.
                If Not skip Then
                    Select Case CType(row("SysNewStatusId"), Integer)
                        Case 1
                            j = LeadStatus.NewLead
                            If Not row.IsNull("SysOrigStatusId") Then
                                Select Case CType(row("SysOrigStatusId"), Integer)
                                    Case 4
                                        i = LeadStatus.IScheduled
                                    Case 5
                                        i = LeadStatus.Interviewed
                                    Case 6
                                        i = LeadStatus.Enrolled
                                    Case Else
                                        i = LeadStatus.Others
                                End Select
                            End If

                        Case 4
                            j = LeadStatus.IScheduled
                            If Not row.IsNull("SysOrigStatusId") Then
                                Select Case CType(row("SysOrigStatusId"), Integer)
                                    Case 1
                                        i = LeadStatus.NewLead
                                    Case 5
                                        i = LeadStatus.Interviewed
                                    Case 6
                                        i = LeadStatus.Enrolled
                                    Case Else
                                        i = LeadStatus.Others
                                End Select
                            End If

                        Case 5
                            j = LeadStatus.Interviewed
                            If Not row.IsNull("SysOrigStatusId") Then
                                Select Case CType(row("SysOrigStatusId"), Integer)
                                    Case 1
                                        i = LeadStatus.NewLead
                                    Case 4
                                        i = LeadStatus.IScheduled
                                    Case 6
                                        i = LeadStatus.Enrolled
                                    Case Else
                                        i = LeadStatus.Others
                                End Select
                            End If

                        Case 6
                            j = LeadStatus.Enrolled
                            If Not row.IsNull("SysOrigStatusId") Then
                                Select Case CType(row("SysOrigStatusId"), Integer)
                                    Case 1
                                        i = LeadStatus.NewLead
                                    Case 4
                                        i = LeadStatus.IScheduled
                                    Case 5
                                        i = LeadStatus.Interviewed
                                    Case Else
                                        i = LeadStatus.Others
                                End Select
                            End If

                        Case Else
                            j = LeadStatus.Others
                            If Not row.IsNull("SysOrigStatusId") Then
                                Select Case CType(row("SysOrigStatusId"), Integer)
                                    Case 1
                                        i = LeadStatus.NewLead
                                    Case 4
                                        i = LeadStatus.IScheduled
                                    Case 5
                                        i = LeadStatus.Interviewed
                                    Case Else
                                        i = LeadStatus.Others
                                End Select
                            End If
                    End Select
                    '
                    If row.IsNull("SysOrigStatusId") Then
                        'When SysOrigStatusId is NULL, IsReversal is also NULL
                        matrix(i, j) += 1
                    Else
                        If row.IsNull("IsReversal") Then
                            matrix(j, i) -= 1
                        Else
                            matrix(i, j) += 1
                        End If
                    End If
                End If
            Next

            If isWTD Then
                'Initialize columns
                dr("WNL") = 0
                dr("WIScheduled") = 0
                dr("WInterviewed") = 0
                dr("WOthers") = 0
                dr("WEnrolled") = 0

                'Compute totals
                For k As Integer = 0 To 5
                    dr("WNL") += matrix(k, LeadStatus.NewLead)
                    dr("WIScheduled") += matrix(k, LeadStatus.IScheduled)
                    dr("WInterviewed") += matrix(k, LeadStatus.Interviewed)
                    dr("WOthers") += matrix(k, LeadStatus.Others)
                    dr("WEnrolled") += matrix(k, LeadStatus.Enrolled)
                Next
            Else
                '
                'Initialize columns
                dr("NL") = 0
                dr("IScheduled") = 0
                dr("Interviewed") = 0
                dr("Others") = 0
                dr("Enrolled") = 0

                'Compute totals
                For k As Integer = 0 To 5
                    dr("NL") += matrix(k, LeadStatus.NewLead)
                    dr("IScheduled") += matrix(k, LeadStatus.IScheduled)
                    dr("Interviewed") += matrix(k, LeadStatus.Interviewed)
                    dr("Others") += matrix(k, LeadStatus.Others)
                    dr("Enrolled") += matrix(k, LeadStatus.Enrolled)
                Next
            End If
        End If
    End Sub


    Private Sub ComputePercentage(ByVal dr As DataRow, ByVal isWTD As Boolean)
        If isWTD Then
            If dr("WNL") <> 0 And dr("WIScheduled") <> 0 Then
                dr("WNLToIS") = Convert.ToInt32(dr("WIScheduled") * 100 / dr("WNL")) & "%"
            End If
            If dr("WIScheduled") <> 0 And dr("WInterviewed") <> 0 Then
                dr("WISToI") = Convert.ToInt32(dr("WInterviewed") * 100 / dr("WIScheduled")) & "%"
            End If
            If dr("WInterviewed") <> 0 And dr("WEnrolled") <> 0 Then
                dr("WIToE") = Convert.ToInt32(dr("WEnrolled") * 100 / dr("WInterviewed")) & "%"
            End If
            If dr("WNL") <> 0 And dr("WEnrolled") <> 0 Then
                dr("WNLToE") = Convert.ToInt32(dr("WEnrolled") * 100 / dr("WNL")) & "%"
            End If
        Else
            '
            If dr("NL") <> 0 And dr("IScheduled") <> 0 Then
                dr("NLToIS") = Convert.ToInt32(dr("IScheduled") * 100 / dr("NL")) & "%"
            End If
            If dr("IScheduled") <> 0 And dr("Interviewed") <> 0 Then
                dr("ISToI") = Convert.ToInt32(dr("Interviewed") * 100 / dr("IScheduled")) & "%"
            End If
            If dr("Interviewed") <> 0 And dr("Enrolled") <> 0 Then
                dr("IToE") = Convert.ToInt32(dr("Enrolled") * 100 / dr("Interviewed")) & "%"
            End If
            If dr("NL") <> 0 And dr("Enrolled") <> 0 Then
                dr("NLToE") = Convert.ToInt32(dr("Enrolled") * 100 / dr("NL")) & "%"
            End If
        End If
    End Sub

    Private Sub ComputePercentage(ByVal dr As DataRow)
        If Not dr.IsNull("WNL") And Not dr.IsNull("WIScheduled") Then
            If dr("WNL") <> 0 And dr("WIScheduled") <> 0 Then
                dr("WNLToIS") = Convert.ToInt32(dr("WIScheduled") * 100 / dr("WNL")) & "%"
            End If
        End If
        If Not dr.IsNull("WIScheduled") And Not dr.IsNull("WInterviewed") Then
            If dr("WIScheduled") <> 0 And dr("WInterviewed") <> 0 Then
                dr("WISToI") = Convert.ToInt32(dr("WInterviewed") * 100 / dr("WIScheduled")) & "%"
            End If
        End If
        If Not dr.IsNull("WInterviewed") And Not dr.IsNull("WEnrolled") Then
            If dr("WInterviewed") <> 0 And dr("WEnrolled") <> 0 Then
                dr("WIToE") = Convert.ToInt32(dr("WEnrolled") * 100 / dr("WInterviewed")) & "%"
            End If
        End If
        If Not dr.IsNull("WNL") And Not dr.IsNull("WEnrolled") Then
            If dr("WNL") <> 0 And dr("WEnrolled") <> 0 Then
                dr("WNLToE") = Convert.ToInt32(dr("WEnrolled") * 100 / dr("WNL")) & "%"
            End If
        End If
        If Not dr.IsNull("NL") And Not dr.IsNull("IScheduled") Then
            If dr("NL") <> 0 And dr("IScheduled") <> 0 Then
                dr("NLToIS") = Convert.ToInt32(dr("IScheduled") * 100 / dr("NL")) & "%"
            End If
        End If
        If Not dr.IsNull("IScheduled") And Not dr.IsNull("Interviewed") Then
            If dr("IScheduled") <> 0 And dr("Interviewed") <> 0 Then
                dr("ISToI") = Convert.ToInt32(dr("Interviewed") * 100 / dr("IScheduled")) & "%"
            End If
        End If
        If Not dr.IsNull("Interviewed") And Not dr.IsNull("Enrolled") Then
            If dr("Interviewed") <> 0 And dr("Enrolled") <> 0 Then
                dr("IToE") = Convert.ToInt32(dr("Enrolled") * 100 / dr("Interviewed")) & "%"
            End If
        End If
        If Not dr.IsNull("NL") And Not dr.IsNull("Enrolled") Then
            If dr("NL") <> 0 And dr("Enrolled") <> 0 Then
                dr("NLToE") = Convert.ToInt32(dr("Enrolled") * 100 / dr("NL")) & "%"
            End If
        End If
    End Sub

    Private Sub ComputeCampusGroupTotals(ByVal dtPerformance As DataTable, ByVal dtCampusTtl As DataTable, ByVal dtCampGrpTtl As DataTable)
        Dim rowC As DataRow
        Dim rowCG As DataRow
        Dim oldCmpGrp As String
        Dim oldCampus As String

        'Loop thru Performance table to compute totals by campus group and campus
        For Each dr As DataRow In dtPerformance.Rows
            If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpId").ToString Then
                'Compute totals for campus group
                rowCG = dtCampGrpTtl.NewRow
                rowCG("CampGrpIdStr") = dr("CampGrpId").ToString
                rowCG("WNL") = dtPerformance.Compute("SUM(WNL)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("WIScheduled") = dtPerformance.Compute("SUM(WIScheduled)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("WInterviewed") = dtPerformance.Compute("SUM(WInterviewed)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("WOthers") = dtPerformance.Compute("SUM(WOthers)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("WEnrolled") = dtPerformance.Compute("SUM(WEnrolled)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("NL") = dtPerformance.Compute("SUM(NL)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("IScheduled") = dtPerformance.Compute("SUM(IScheduled)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("Interviewed") = dtPerformance.Compute("SUM(Interviewed)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("Others") = dtPerformance.Compute("SUM(Others)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("Enrolled") = dtPerformance.Compute("SUM(Enrolled)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                dtCampGrpTtl.Rows.Add(rowCG)
                'Compute Percentages
                ComputePercentage(rowCG)
                oldCmpGrp = dr("CampGrpId").ToString
                oldCampus = ""
            End If

            If oldCmpGrp = dr("CampGrpId").ToString And oldCampus = "" Then
                'Compute totals for campus
                rowC = dtCampusTtl.NewRow
                rowC("CampGrpIdStr") = oldCmpGrp
                rowC("CampusIdStr") = dr("CampusId").ToString
                rowC("WNL") = dtPerformance.Compute("SUM(WNL)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WIScheduled") = dtPerformance.Compute("SUM(WIScheduled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WInterviewed") = dtPerformance.Compute("SUM(WInterviewed)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WOthers") = dtPerformance.Compute("SUM(WOthers)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WEnrolled") = dtPerformance.Compute("SUM(WEnrolled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("NL") = dtPerformance.Compute("SUM(NL)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("IScheduled") = dtPerformance.Compute("SUM(IScheduled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("Interviewed") = dtPerformance.Compute("SUM(Interviewed)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("Others") = dtPerformance.Compute("SUM(Others)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("Enrolled") = dtPerformance.Compute("SUM(Enrolled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                dtCampusTtl.Rows.Add(rowC)
                'Compute Percentages
                ComputePercentage(rowC)
                oldCampus = dr("CampusId").ToString
                '
            ElseIf oldCmpGrp = dr("CampGrpId").ToString And oldCampus <> dr("CampusId").ToString Then
                'Compute totals for campus
                rowC = dtCampusTtl.NewRow
                rowC("CampGrpIdStr") = oldCmpGrp
                rowC("CampusIdStr") = dr("CampusId").ToString
                rowC("WNL") = dtPerformance.Compute("SUM(WNL)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WIScheduled") = dtPerformance.Compute("SUM(WIScheduled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WInterviewed") = dtPerformance.Compute("SUM(WInterviewed)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WOthers") = dtPerformance.Compute("SUM(WOthers)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WEnrolled") = dtPerformance.Compute("SUM(WEnrolled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")

                rowC("NL") = dtPerformance.Compute("SUM(NL)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("IScheduled") = dtPerformance.Compute("SUM(IScheduled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("Interviewed") = dtPerformance.Compute("SUM(Interviewed)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("Others") = dtPerformance.Compute("SUM(Others)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("Enrolled") = dtPerformance.Compute("SUM(Enrolled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                dtCampusTtl.Rows.Add(rowC)
                'Compute Percentages
                ComputePercentage(rowC)
                oldCampus = dr("CampusId").ToString
            End If
        Next
    End Sub
    ''Added by Saraswathi lakshmanan  on Oct 13 2009
    '' To compute the numbers in Lead Statuses
    Private Sub ComputeChanges_Revised(ByVal dr As DataRow, ByVal arrRows() As DataRow, ByVal isWTD As Boolean)
        Dim matrix(20, 20) As Integer
        Dim i, j As Integer

        'Initialize matrix with zeros
        For i = 0 To 20
            For j = 0 To 20
                matrix(i, j) = 0
            Next
        Next

        Dim skip As Boolean

        If arrRows.GetLength(0) > 0 Then
            For Each row As DataRow In arrRows
                skip = False
                If row.IsNull("SysOrigStatusId") Then
                    i = LeadStatus.IsNull
                Else
                    If row("SysOrigStatusId") = row("SysNewStatusId") Then
                        skip = True
                    End If
                End If
                'ignore rows with original status equal to new status, because there was no change.
                If Not skip Then
                    Select Case CType(row("SysNewStatusId"), Integer)
                        Case 1
                            j = LeadStatus.NewLead
                            If Not row.IsNull("SysOrigStatusId") Then
                                Select Case CType(row("SysOrigStatusId"), Integer)
                                    Case 4
                                        i = LeadStatus.IScheduled
                                    Case 5
                                        i = LeadStatus.Interviewed
                                    Case 6
                                        i = LeadStatus.Enrolled
                                        ''Added by Saraswathi to show the counts for all the statuses
                                    Case 2
                                        i = LeadStatus.ApplicationReceived
                                    Case 3
                                        i = LeadStatus.ApplicationNotAccepted
                                    Case 17
                                        i = LeadStatus.DeadLead
                                    Case 18
                                        i = LeadStatus.WillEnrollinFuture

                                    Case Else
                                        i = LeadStatus.Others
                                End Select
                            End If

                        Case 4
                            j = LeadStatus.IScheduled
                            If Not row.IsNull("SysOrigStatusId") Then
                                Select Case CType(row("SysOrigStatusId"), Integer)
                                    Case 1
                                        i = LeadStatus.NewLead
                                    Case 5
                                        i = LeadStatus.Interviewed
                                    Case 6
                                        i = LeadStatus.Enrolled

                                        ''Added by Saraswathi to show the counts for all the statuses
                                    Case 2
                                        i = LeadStatus.ApplicationReceived
                                    Case 3
                                        i = LeadStatus.ApplicationNotAccepted
                                    Case 17
                                        i = LeadStatus.DeadLead
                                    Case 18
                                        i = LeadStatus.WillEnrollinFuture

                                    Case Else
                                        i = LeadStatus.Others
                                End Select
                            End If

                        Case 5
                            j = LeadStatus.Interviewed
                            If Not row.IsNull("SysOrigStatusId") Then
                                Select Case CType(row("SysOrigStatusId"), Integer)
                                    Case 1
                                        i = LeadStatus.NewLead
                                    Case 4
                                        i = LeadStatus.IScheduled
                                    Case 6
                                        i = LeadStatus.Enrolled

                                        ''Added by Saraswathi to show the counts for all the statuses
                                    Case 2
                                        i = LeadStatus.ApplicationReceived
                                    Case 3
                                        i = LeadStatus.ApplicationNotAccepted
                                    Case 17
                                        i = LeadStatus.DeadLead
                                    Case 18
                                        i = LeadStatus.WillEnrollinFuture

                                    Case Else
                                        i = LeadStatus.Others
                                End Select
                            End If

                        Case 6
                            j = LeadStatus.Enrolled
                            If Not row.IsNull("SysOrigStatusId") Then
                                Select Case CType(row("SysOrigStatusId"), Integer)
                                    Case 1
                                        i = LeadStatus.NewLead
                                    Case 4
                                        i = LeadStatus.IScheduled
                                    Case 5
                                        i = LeadStatus.Interviewed

                                        ''Added by Saraswathi to show the counts for all the statuses
                                    Case 2
                                        i = LeadStatus.ApplicationReceived
                                    Case 3
                                        i = LeadStatus.ApplicationNotAccepted
                                    Case 17
                                        i = LeadStatus.DeadLead
                                    Case 18
                                        i = LeadStatus.WillEnrollinFuture

                                    Case Else
                                        i = LeadStatus.Others
                                End Select
                            End If

                        Case 2
                            j = LeadStatus.ApplicationReceived
                            If Not row.IsNull("SysOrigStatusId") Then
                                Select Case CType(row("SysOrigStatusId"), Integer)
                                    Case 1
                                        i = LeadStatus.NewLead
                                    Case 4
                                        i = LeadStatus.IScheduled
                                    Case 5
                                        i = LeadStatus.Interviewed

                                        ''Added by Saraswathi to show the counts for all the statuses
                                    Case 6
                                        i = LeadStatus.Enrolled
                                    Case 3
                                        i = LeadStatus.ApplicationNotAccepted
                                    Case 17
                                        i = LeadStatus.DeadLead
                                    Case 18
                                        i = LeadStatus.WillEnrollinFuture

                                    Case Else
                                        i = LeadStatus.Others
                                End Select
                            End If

                        Case 3
                            j = LeadStatus.ApplicationNotAccepted
                            If Not row.IsNull("SysOrigStatusId") Then
                                Select Case CType(row("SysOrigStatusId"), Integer)
                                    Case 1
                                        i = LeadStatus.NewLead
                                    Case 4
                                        i = LeadStatus.IScheduled
                                    Case 5
                                        i = LeadStatus.Interviewed

                                        ''Added by Saraswathi to show the counts for all the statuses
                                    Case 6
                                        i = LeadStatus.Enrolled
                                    Case 2
                                        i = LeadStatus.ApplicationReceived
                                    Case 17
                                        i = LeadStatus.DeadLead
                                    Case 18
                                        i = LeadStatus.WillEnrollinFuture

                                    Case Else
                                        i = LeadStatus.Others
                                End Select
                            End If

                        Case 17
                            j = LeadStatus.DeadLead
                            If Not row.IsNull("SysOrigStatusId") Then
                                Select Case CType(row("SysOrigStatusId"), Integer)
                                    Case 1
                                        i = LeadStatus.NewLead
                                    Case 4
                                        i = LeadStatus.IScheduled
                                    Case 5
                                        i = LeadStatus.Interviewed

                                        ''Added by Saraswathi to show the counts for all the statuses
                                    Case 6
                                        i = LeadStatus.Enrolled
                                    Case 3
                                        i = LeadStatus.ApplicationNotAccepted
                                    Case 2
                                        i = LeadStatus.ApplicationReceived
                                    Case 18
                                        i = LeadStatus.WillEnrollinFuture

                                    Case Else
                                        i = LeadStatus.Others
                                End Select
                            End If
                        Case 18
                            j = LeadStatus.WillEnrollinFuture
                            If Not row.IsNull("SysOrigStatusId") Then
                                Select Case CType(row("SysOrigStatusId"), Integer)
                                    Case 1
                                        i = LeadStatus.NewLead
                                    Case 4
                                        i = LeadStatus.IScheduled
                                    Case 5
                                        i = LeadStatus.Interviewed

                                        ''Added by Saraswathi to show the counts for all the statuses
                                    Case 6
                                        i = LeadStatus.Enrolled
                                    Case 3
                                        i = LeadStatus.ApplicationNotAccepted
                                    Case 17
                                        i = LeadStatus.DeadLead
                                    Case 2
                                        i = LeadStatus.ApplicationReceived

                                    Case Else
                                        i = LeadStatus.Others
                                End Select
                            End If
                        Case Else
                            j = LeadStatus.Others
                            If Not row.IsNull("SysOrigStatusId") Then
                                Select Case CType(row("SysOrigStatusId"), Integer)
                                    Case 1
                                        i = LeadStatus.NewLead
                                    Case 4
                                        i = LeadStatus.IScheduled
                                    Case 5
                                        i = LeadStatus.Interviewed

                                        ''Added by Saraswathi to show the counts for all the statuses
                                    Case 2
                                        i = LeadStatus.ApplicationReceived
                                    Case 3
                                        i = LeadStatus.ApplicationNotAccepted
                                    Case 17
                                        i = LeadStatus.DeadLead
                                    Case 18
                                        i = LeadStatus.WillEnrollinFuture

                                    Case Else
                                        i = LeadStatus.Others
                                End Select
                            End If
                    End Select
                    '
                    If row.IsNull("SysOrigStatusId") Then
                        'When SysOrigStatusId is NULL, IsReversal is also NULL
                        matrix(i, j) += 1
                    Else
                        If row.IsNull("IsReversal") Then
                            matrix(j, i) -= 1
                        Else
                            matrix(i, j) += 1
                        End If
                    End If
                End If
            Next

            If isWTD Then
                'Initialize columns
                dr("WNL") = 0
                dr("WIScheduled") = 0
                dr("WInterviewed") = 0
                dr("WOthers") = 0
                dr("WEnrolled") = 0

                dr("WApplReceived") = 0
                dr("WApplNotAccepted") = 0
                dr("WDeadLead") = 0
                dr("WEnrollinFuture") = 0
                dr("StartsCount") = 0

                'Compute totals
                For k As Integer = 0 To 20
                    dr("WNL") += matrix(k, LeadStatus.NewLead)
                    dr("WIScheduled") += matrix(k, LeadStatus.IScheduled)
                    dr("WInterviewed") += matrix(k, LeadStatus.Interviewed)
                    dr("WOthers") += matrix(k, LeadStatus.Others)
                    dr("WEnrolled") += matrix(k, LeadStatus.Enrolled)

                    dr("WApplReceived") += matrix(k, LeadStatus.ApplicationReceived)
                    dr("WApplNotAccepted") += matrix(k, LeadStatus.ApplicationNotAccepted)
                    dr("WDeadLead") += matrix(k, LeadStatus.DeadLead)
                    dr("WEnrollinFuture") += matrix(k, LeadStatus.WillEnrollinFuture)


                Next
                'Else
                '    '
                '    'Initialize columns
                '    dr("NL") = 0
                '    dr("IScheduled") = 0
                '    dr("Interviewed") = 0
                '    dr("Others") = 0
                '    dr("Enrolled") = 0

                '    'Compute totals
                '    For k As Integer = 0 To 20
                '        dr("NL") += matrix(k, LeadStatus.NewLead)
                '        dr("IScheduled") += matrix(k, LeadStatus.IScheduled)
                '        dr("Interviewed") += matrix(k, LeadStatus.Interviewed)
                '        dr("Others") += matrix(k, LeadStatus.Others)
                '        dr("Enrolled") += matrix(k, LeadStatus.Enrolled)
                '    Next
            End If
        End If
    End Sub


    Private Sub ComputeCampusGroupTotals_revised(ByVal dtPerformance As DataTable, ByVal dtCampusTtl As DataTable, ByVal dtCampGrpTtl As DataTable)
        Dim rowC As DataRow
        Dim rowCG As DataRow
        Dim oldCmpGrp As String
        Dim oldCampus As String

        'Loop thru Performance table to compute totals by campus group and campus
        For Each dr As DataRow In dtPerformance.Rows
            If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpId").ToString Then
                'Compute totals for campus group
                rowCG = dtCampGrpTtl.NewRow
                rowCG("CampGrpIdStr") = dr("CampGrpId").ToString
                rowCG("WNL") = dtPerformance.Compute("SUM(WNL)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("WIScheduled") = dtPerformance.Compute("SUM(WIScheduled)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("WInterviewed") = dtPerformance.Compute("SUM(WInterviewed)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("WOthers") = dtPerformance.Compute("SUM(WOthers)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("WEnrolled") = dtPerformance.Compute("SUM(WEnrolled)", "CampGrpId='" & dr("CampGrpId").ToString & "'")


                rowCG("WApplReceived") = dtPerformance.Compute("SUM(WApplReceived)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("WApplNotAccepted") = dtPerformance.Compute("SUM(WApplNotAccepted)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("WDeadLead") = dtPerformance.Compute("SUM(WDeadLead)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("WEnrollinFuture") = dtPerformance.Compute("SUM(WEnrollinFuture)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                rowCG("StartsCount") = dtPerformance.Compute("SUM(StartsCount)", "CampGrpId='" & dr("CampGrpId").ToString & "'")


                dtCampGrpTtl.Rows.Add(rowCG)
                'Compute Percentages
                ComputePercentage_revised(rowCG)
                oldCmpGrp = dr("CampGrpId").ToString
                oldCampus = ""
            End If

            If oldCmpGrp = dr("CampGrpId").ToString And oldCampus = "" Then
                'Compute totals for campus
                rowC = dtCampusTtl.NewRow
                rowC("CampGrpIdStr") = oldCmpGrp
                rowC("CampusIdStr") = dr("CampusId").ToString
                rowC("WNL") = dtPerformance.Compute("SUM(WNL)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WIScheduled") = dtPerformance.Compute("SUM(WIScheduled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WInterviewed") = dtPerformance.Compute("SUM(WInterviewed)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WOthers") = dtPerformance.Compute("SUM(WOthers)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WEnrolled") = dtPerformance.Compute("SUM(WEnrolled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")


                rowC("WApplReceived") = dtPerformance.Compute("SUM(WApplReceived)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WApplNotAccepted") = dtPerformance.Compute("SUM(WApplNotAccepted)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WDeadLead") = dtPerformance.Compute("SUM(WDeadLead)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WEnrollinFuture") = dtPerformance.Compute("SUM(WEnrollinFuture)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("StartsCount") = dtPerformance.Compute("SUM(StartsCount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                              "CampusId='" & dr("CampusId").ToString & "'")




                dtCampusTtl.Rows.Add(rowC)
                'Compute Percentages
                ComputePercentage_revised(rowC)
                oldCampus = dr("CampusId").ToString
                '
            ElseIf oldCmpGrp = dr("CampGrpId").ToString And oldCampus <> dr("CampusId").ToString Then
                'Compute totals for campus
                rowC = dtCampusTtl.NewRow
                rowC("CampGrpIdStr") = oldCmpGrp
                rowC("CampusIdStr") = dr("CampusId").ToString
                rowC("WNL") = dtPerformance.Compute("SUM(WNL)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WIScheduled") = dtPerformance.Compute("SUM(WIScheduled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WInterviewed") = dtPerformance.Compute("SUM(WInterviewed)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WOthers") = dtPerformance.Compute("SUM(WOthers)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WEnrolled") = dtPerformance.Compute("SUM(WEnrolled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WApplReceived") = dtPerformance.Compute("SUM(WApplReceived)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                              "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WApplNotAccepted") = dtPerformance.Compute("SUM(WApplNotAccepted)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WDeadLead") = dtPerformance.Compute("SUM(WDeadLead)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("WEnrollinFuture") = dtPerformance.Compute("SUM(WEnrollinFuture)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                "CampusId='" & dr("CampusId").ToString & "'")
                rowC("StartsCount") = dtPerformance.Compute("SUM(StartsCount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                               "CampusId='" & dr("CampusId").ToString & "'")


                dtCampusTtl.Rows.Add(rowC)
                'Compute Percentages
                ComputePercentage_revised(rowC)
                oldCampus = dr("CampusId").ToString
            End If
        Next
    End Sub
    Private Sub ComputePercentage_revised(ByVal dr As DataRow)
        If Not dr.IsNull("WNL") And Not dr.IsNull("WIScheduled") Then
            If dr("WNL") <> 0 And dr("WIScheduled") <> 0 Then
                dr("WNLToIS") = Convert.ToInt32(dr("WIScheduled") * 100 / dr("WNL")) & "%"
            End If
        End If
        If Not dr.IsNull("WIScheduled") And Not dr.IsNull("WInterviewed") Then
            If dr("WIScheduled") <> 0 And dr("WInterviewed") <> 0 Then
                dr("WISToI") = Convert.ToInt32(dr("WInterviewed") * 100 / dr("WIScheduled")) & "%"
            End If
        End If
        If Not dr.IsNull("WInterviewed") And Not dr.IsNull("WEnrolled") Then
            If dr("WInterviewed") <> 0 And dr("WEnrolled") <> 0 Then
                dr("WIToE") = Convert.ToInt32(dr("WEnrolled") * 100 / dr("WInterviewed")) & "%"
            End If
        End If
        If Not dr.IsNull("WNL") And Not dr.IsNull("WEnrolled") Then
            If dr("WNL") <> 0 And dr("WEnrolled") <> 0 Then
                dr("WNLToE") = Convert.ToInt32(dr("WEnrolled") * 100 / dr("WNL")) & "%"
            End If
        End If
        If Not dr.IsNull("StartsCount") And Not dr.IsNull("WEnrolled") Then
            If dr("StartsCount") <> 0 And dr("WEnrolled") <> 0 Then
                dr("EToS") = Convert.ToInt32(dr("StartsCount") * 100 / dr("WEnrolled")) & "%"
            End If
        End If
        If Not dr.IsNull("WNL") And Not dr.IsNull("StartsCount") Then
            If dr("StartsCount") <> 0 And dr("WNL") <> 0 Then
                dr("LToS") = Convert.ToInt32(dr("StartsCount") * 100 / dr("WNL")) & "%"
            End If
        End If
    End Sub
    Private Sub ComputePercentage_revised(ByVal dr As DataRow, ByVal isWTD As Boolean)
        If isWTD Then
            If dr("WNL") <> 0 And dr("WIScheduled") <> 0 Then
                dr("WNLToIS") = Convert.ToInt32(dr("WIScheduled") * 100 / dr("WNL")) & "%"
            End If
            If dr("WIScheduled") <> 0 And dr("WInterviewed") <> 0 Then
                dr("WISToI") = Convert.ToInt32(dr("WInterviewed") * 100 / dr("WIScheduled")) & "%"
            End If
            If dr("WInterviewed") <> 0 And dr("WEnrolled") <> 0 Then
                dr("WIToE") = Convert.ToInt32(dr("WEnrolled") * 100 / dr("WInterviewed")) & "%"
            End If
            If dr("WNL") <> 0 And dr("WEnrolled") <> 0 Then
                dr("WNLToE") = Convert.ToInt32(dr("WEnrolled") * 100 / dr("WNL")) & "%"
            End If
            If dr("StartsCount") <> 0 And dr("WEnrolled") <> 0 Then
                dr("EToS") = Convert.ToInt32(dr("StartsCount") * 100 / dr("WEnrolled")) & "%"
            End If
            If dr("StartsCount") <> 0 And dr("WNL") <> 0 Then
                dr("LToS") = Convert.ToInt32(dr("StartsCount") * 100 / dr("WNL")) & "%"
            End If
        End If
    End Sub
#End Region
End Class
