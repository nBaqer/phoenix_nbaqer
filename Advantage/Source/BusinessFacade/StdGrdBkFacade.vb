Imports FAME.Advantage.Common

Public Class StdGrdBkFacade
    Public Function GetAllTerms() As DataSet

        '   Instantiate DAL component
        Dim DB As New StdGrdBkDB

        '   get the dataset with all degrees
        Return DB.GetAllTerms()

    End Function
    Public Function GetClsSections(ByVal Term As String, ByVal Instr As String, ByVal CampusId As String) As DataSet

        '   Instantiate DAL component
        Dim db As New StdGrdBkDB
        Dim ds As New DataSet

        '   get the dataset with all degrees          
        ds = db.GetClsSects(Term, Instr, CampusId)

        Return ProcessClsSectInfo(ds)

    End Function
    Public Function GetClsSections(ByVal Term As String, ByVal Instr As String, ByVal CampusId As String, ByVal userName As String) As DataSet
        Dim ds As New DataSet
        ds = (New StdGrdBkDB).GetClsSects(Term, Instr, CampusId, userName)
        Return ProcessClsSectInfo(ds)
    End Function
    Public Function ProcessClsSectInfo(ByVal ds As DataSet) As DataSet
        Dim db As New GrdPostingsDB
        Dim tbl1 As New DataTable
        Dim tbl2 As New DataTable
        Dim row As DataRow


        tbl1 = ds.Tables("TermClsSects")
        'Make the ChildId column the primary key
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("ClsSectionId")}
        End With


        If ds.Tables("TermClsSects").Rows.Count > 0 Then
            'Load a datatable with ChildId, ChildTyp and ChildDescrip
            'Create a datable with a new column called ChildId, make this the primary key.

            'Add a new column called ChildDescrip to the datatable 
            Dim col9 As DataColumn = tbl1.Columns.Add("CourseSectDescrip", GetType(String))

            For Each row In tbl1.Rows

                row("CourseSectDescrip") = row("Descrip") & " - " & row("ClsSection")

            Next
        End If

        Return ds
    End Function
    Public Function PopulateDataList(ByVal clsSect As String) As DataSet

        Dim db As New StdGrdBkDB
        Dim ds As DataSet
        Dim unschedDateInfo As New UnscheduleClsInfo
        ds = db.PopulateStdDataList(clsSect)
        Return ConcatenateStudentLastFirstName(ds)
    End Function

    Public Function ConcatenateStudentLastFirstName(ByVal ds As DataSet, Optional ByVal campusId As String = "") As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        Dim db As New StdGrdBkDB
        Dim str As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        
        tbl = ds.Tables("ClsSectStds")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("a.StudentId")}
        End With
        If ds.Tables("ClsSectStds").Rows.Count > 0 Then
            'Add a new column called full-name to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                row("FullName") = row("LastName") & ", " & row("FirstName")
                If myAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower = "letter" Then
                    If (row.IsNull("GrdSysDetailId")) Then
                        'get the corresponding default grade 
                        str = db.GetDefaultGrade(row("StuEnrollId").ToString)
                        row("Grade") = str
                    End If
                Else
                    If myAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
                        If Not (row.IsNull("Grade")) Then
                            row("Grade") = Math.Round(row("Grade"), 0)
                        End If
                    End If
                End If
            Next
        End If
        Return ds
    End Function

    ''Optional Parameter isAcademicAdvisor 
    ''To allow the Academic advisor to post and modify grade

    Public Function PopulateDataGridOnItemCmd(ByVal StuEnrollId As String, ByVal ClsSectionId As String, ByVal FullName As String, ByVal Instr As String, ByVal userName As String, Optional ByVal isAcademicAdvisor As Boolean = False) As DataSet

        Dim db As New StdGrdBkDB
        Dim ds As DataSet

        ds = db.GetDataGridInfoOnItmCmd(StuEnrollId, ClsSectionId, Instr, userName, isAcademicAdvisor)
        Return BuildStuInfoDS(ds, StuEnrollId, FullName)

    End Function
    Public Function PopulateStdGrdBkGrid(ByVal StuEnrollId As String, ByVal ClsSectionId As String, ByVal FullName As String, ByVal Instr As String, ByVal userName As String) As DataSet

        Dim db As New StdGrdBkDB
        Dim ds As DataSet

        ds = db.GetDataGridInfoOnItmCmd(StuEnrollId, ClsSectionId, Instr, userName)
        Return BuildStuInfoDS(ds, StuEnrollId, FullName)

    End Function
    Public Function GetStdGrdBkPopupInfo(ByVal ClsSectId As String, ByVal StudentID As String) As String
        Dim ds As New DataSet
        With New StdGrdBkDB
            'get the dataset with all SkillGroups
            Return .GetStdGrdBkPopupInfo(ClsSectId, StudentID)
        End With
    End Function
    Public Function GetStdGrdBkPopupInfo(ByVal ClsSectId As String, ByVal StudentID As String, ByVal stuEnrollId As String) As String
        Return (New StdGrdBkDB).GetStdGrdBkPopupInfo(ClsSectId, StudentID, stuEnrollId)
    End Function
    Public Function BuildStuInfoDS(ByVal ds As DataSet, ByVal StuEnrollId As String, ByVal FullName As String) As DataSet
        Dim tbl1 As New DataTable
        Dim row As DataRow
        Dim tbl2 As New DataTable

        If ds.Tables.Count > 0 Then
            '
            tbl1 = ds.Tables("StudentGrdBk")
            Try
                With tbl1
                    .PrimaryKey = New DataColumn() {.Columns("InstrGrdBkWgtDetailId")}
                End With
            Catch ex As System.Exception
                Throw New System.Exception("Error found retrieving Grade Book Weightings. " & ex.Message)
            End Try
            '
            tbl2 = ds.Tables("GrdBkCriteria")
            Try
                With tbl2
                    .PrimaryKey = New DataColumn() {.Columns("InstrGrdBkWgtDetailId")}
                End With
            Catch ex As System.Exception
                Throw New System.Exception("Error found retrieving Grade Book Weightings. " & ex.Message)
            End Try
            '
            If tbl2.Rows.Count > 0 Then
                Dim col1 As DataColumn = tbl2.Columns.Add("StuEnrollId", GetType(String))
                Dim col2 As DataColumn = tbl2.Columns.Add("Score", GetType(String))
                Dim col4 As DataColumn = tbl2.Columns.Add("FullName", GetType(String))
                Dim col6 As DataColumn = tbl2.Columns.Add("Comments", GetType(String))
                Dim col7 As DataColumn = tbl2.Columns.Add("IsIncomplete", GetType(Boolean))
                Dim col8 As DataColumn = tbl2.Columns.Add("GrdBkResultId", GetType(String))
                Dim col9 As DataColumn = tbl2.Columns.Add("DateCompleted", GetType(string))

                For Each row In tbl2.Rows
                    If (tbl1.Rows.Count > 0) Then
                        Dim row2 As DataRow = tbl1.Rows.Find(row("InstrGrdBkWgtDetailId"))
                        If Not (row2 Is Nothing) Then
                            row("StuEnrollId") = row2("StuEnrollId")
                            row("Score") = row2("Score")
                            row("Comments") = row2("Comments")
                            row("Descrip") = row2("Descrip")
                            row("Comments") = row2("Comments")
                            row("FullName") = row2("LastName") & ", " & row2("FirstName")
                            row("IsIncomplete") = row2("IsIncomplete")
                            row("GrdBkResultId") = row2("GrdBkResultId")
                            row("DateCompleted") = row2("DateCompleted")
                        Else
                            row("StuEnrollId") = StuEnrollId
                            row("FullName") = FullName
                        End If
                    End If
                Next
            End If

        End If

        Return ds
    End Function
    Public Function ValidateScore(ByVal Score As Integer, ByVal StuEnrollId As String) As String

        Dim ds As New DataSet
        Dim db As New StdGrdBkDB
        Dim MinScore As String
        Dim MaxScore As String
        Dim errStr As String

        Dim MinMaxScore() As String = Split(db.GetGrdScaleScoreLimit(StuEnrollId))
        MaxScore = MinMaxScore(0)
        MinScore = MinMaxScore(1)

        If Score < MinScore Then
            errStr = "Score is lesser than the accepted score limit"
        ElseIf Score > MaxScore Then
            errStr = "Score is greater than the accepted score limit"
        End If


        Return errStr
    End Function

    Public Function InsertStudentGrade(ByVal StdGrdObj As StdGrdBkInfo, ByVal user As String) As String
        Return (New StdGrdBkDB).InsertStudentGrade(StdGrdObj, user)
    End Function
    Public Function DeleteStudentGrade(ByVal StdGrdObj As StdGrdBkInfo) As String
        Return (New StdGrdBkDB).DeleteStudentGrade(StdGrdObj)
    End Function
    Public Function UpdateStudentGrade(ByVal StdGrdObj As StdGrdBkInfo, ByVal user As String) As String
        Return (New StdGrdBkDB).UpdateStudentGrade(StdGrdObj, user)
    End Function

    Public Function UpdateIncompleteGrade(ByVal StdGrdObj As StdGrdBkInfo, ByVal user As String)
        Dim DB As New StdGrdBkDB

        DB.UpdateIncompleteGrade(StdGrdObj, user)
    End Function
    Public Function UpdateIncompleteGrade(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal isIncomplete As Boolean, ByVal user As String) As String
        Return (New StdGrdBkDB).UpdateIncompleteGrade(stuEnrollId, clsSectionId, isIncomplete, user)
    End Function
    Public Function GetGrdScaleSystemDetails(ByVal ClsSection As String, ByVal EmpId As String, ByVal termId As String) As DataSet

        '   Instantiate DAL component
        With New StdGrdBkDB
            Return .GetGradeScaleSystemDetails(ClsSection, EmpId, termId)

        End With

    End Function
    Public Function boolIsScorePosted(ByVal ClsSectId As String, ByVal StuEnrollId As String) As Boolean
        With New StdGrdBkDB
            Return .boolIsScorePosted(ClsSectId, StuEnrollId)
        End With
    End Function
    Public Function GetGrdScaleId(ByVal ClsSection As String) As String
        Dim db As New StdGrdBkDB
        Dim ds As New DataSet
        Dim row As DataRow
        Dim GrdScaleId As String
        '   Instantiate DAL component

        ds = db.GetGradeScaleId(ClsSection)

        For Each row In ds.Tables(0).Rows
            GrdScaleId = row("GrdScaleId").ToString
        Next
        Return GrdScaleId
    End Function
    Public Function GetGrdBkWgtDetailsAndResultsDS(ByVal clsSectionId As String, ByVal stuEnrollId As String) As DataSet
        Return (New StdGrdBkDB).GetGrdBkWgtDetailsAndResultsDS(clsSectionId, stuEnrollId)
    End Function
    Public Function GetGradeBookWeightingLevel(ByVal clsSectionId As String, ByVal stuEnrollId As String) As String
        Return (New StdGrdBkDB).GetGradeBookWeightingLevel(clsSectionId, stuEnrollId)
    End Function
    Public Function GetGradeBookWeightingLevel(ByVal clsSectionId As String) As String
        Return (New StdGrdBkDB).GetGradeBookWeightingLevel(clsSectionId)
    End Function
    Public Function GetGradeBookWeightingLevel_SP(ByVal clsSectionId As String) As String
        Return (New StdGrdBkDB).GetGradeBookWeightingLevel_SP(clsSectionId)
    End Function

    Public Function GetGradeBookComponentsAndEffectiveDatesDS() As DataSet
        Return (New StdGrdBkDB).GetGradeBookComponentsAndEffectiveDatesDS()
    End Function
    Public Function UpdateGradeBookComponentsandEffectiveDatesDS(ByVal ds As DataSet) As String
        Return (New StdGrdBkDB).UpdateGradeBookComponentsandEffectiveDatesDS(ds)
    End Function
    Public Function GetSysGradeBookComponents() As DataTable
        Return (New StdGrdBkDB).GetSysGradeBookComponents()
    End Function

    ''Added by Saraswathi Lakshmanan to fix issue 14296
    ''The class sections are populated the user and his roles
    ''Added on 26-Sept 2008

    Public Function GetClsSectionsbyUserandRoles(ByVal Term As String, ByVal Instr As String, ByVal CampusId As String, ByVal ShiftId As String, ByVal MinimumClassStartDate As Date, ByVal userName As String, ByVal isAcademicAdvisor As Boolean) As DataSet
        Dim ds As New DataSet
        ds = (New StdGrdBkDB).GetClsSectsbyUserandRoles(Term, Instr, CampusId, ShiftId, MinimumClassStartDate, userName, isAcademicAdvisor)
        Return ProcessClsSectInfo(ds)
    End Function

    public Function IsCourseALab(ByVal reqid As String, ByVal instrGrdBkWgtId As String) As Boolean
        return (New StdGrdBkDB).IsCourseALab(reqid,instrGrdBkWgtId)
    End Function

    Public Function GetClsSectionsbyUserandRoles(ByVal Term As String, ByVal Instr As String, ByVal CampusId As String, ByVal userName As String, ByVal isAcademicAdvisor As Boolean) As DataSet
        Dim ds As New DataSet
        ds = (New StdGrdBkDB).GetClsSectsbyUserandRoles(Term, Instr, CampusId, userName, isAcademicAdvisor)
        Return ProcessClsSectInfo(ds)
    End Function
    ''Added by Saraswathi Lakshmanan
    ''The class sections are populated based on the user and his roles and CohortStartDate
    ''Added on Oct- 3- 2008

    Public Function GetClsSectionsbyUserandRolesandCohortStDt(ByVal CohortStDate As String, ByVal Instr As String, ByVal CampusId As String, ByVal userName As String, ByVal isAcademicAdvisor As Boolean) As DataSet
        Dim ds As New DataSet
        ds = (New StdGrdBkDB).GetClsSectsbyUserandRolesandCohortStDt(CohortStDate, Instr, CampusId, userName, isAcademicAdvisor)
        Return ProcessClsSectInfo(ds)
    End Function
    ''Added by Saraswathi Lakshmanan
    ''The CohortStartDate is found 
    ''Added on Oct- 8- 2008
    Public Function GetCohortStartDate(ByVal UserName As String, ByVal Instr As String, ByVal isAcademicAdvisor As Boolean, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        ds = (New StdGrdBkDB).GetCohortStartDate(UserName, Instr, isAcademicAdvisor, CampusId)
        Return ds
    End Function
    ''Added by Saraswathi Lakshmanan
    ''The CohortStartDate is found based on the program version
    ''Added on Oct- 8- 2008
    Public Function GetCohortStartDatebyPrgVersion(ByVal PrgVersion As String) As DataSet
        Dim ds As New DataSet
        ds = (New StdGrdBkDB).GetCohortStartDatebyPrgVersion(PrgVersion)
        Return ds
    End Function
End Class
