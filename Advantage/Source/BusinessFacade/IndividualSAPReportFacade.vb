Imports FAME.Advantage.Common

Public Class IndividualSAPReportFacade
    Inherits BaseReportFacade
    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim GradebookResultsObj As New GradebookResultsDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by report.
        'ds = BuildReportSource(GradebookResultsObj.GradebookResults(rptParamInfo), GradebookResultsObj.StudentIdentifier, rptParamInfo.ResId, rptParamInfo.FilterOtherString.Trim)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        ds = BuildDataForReport(rptParamInfo)
        Return ds
    End Function
    Public Function BuildDataForReport(ByVal ParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim dtReturnResult As New DataTable
        Dim ds As New DataSet
        Dim facade As New GraduateAuditFacade
        Dim dtGetGPAandCGPA As New DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim gradeReps As String = MyAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod")
        Dim strIncludeHours As String = MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade")
        Dim includeHours As Boolean
        Dim stuEnrollId As String = ""
        Dim intRowCount As Integer
        Dim CGPA As Decimal = 0.0
        Dim GPA As Decimal = 0.0
        Dim intMaxIndexCount As Integer = 0
        'Code Added By Vijay Ramteke on May, 10 2009

        Dim dtTemp As New DataTable
        Dim dtMain As New DataTable
        Dim boolAssign As Boolean = False

        'Code Added By Vijay Ramteke on May, 10 2009

        If ParamInfo.FilterOther.Contains("arStuEnrollments.StuEnrollId") Then
            stuEnrollId = Mid(ParamInfo.FilterOther, InStr(ParamInfo.FilterOther, "'") + 1, 36)
        End If

        'stuEnrollId = Mid(ParamInfo.FilterOther, InStr(ParamInfo.FilterOther, "'") + 1, 36)
        If strIncludeHours.ToLower = "true" Then
            includeHours = True
        End If
        If stuEnrollId <> "" Then
            If strIncludeHours.ToLower = "true" Then
                includeHours = True
            End If
            dtGetGPAandCGPA = (New GraduateAuditFacade).GetTermProgressFromResults(stuEnrollId, gradeReps, includeHours)
            ds = (New AttendanceHistoryDB).GetInvidualSAPDetails(ParamInfo, dtGetGPAandCGPA)
            'Code Added By vijay Ramteke on May14,2009
            If Not ds Is Nothing Then
                dtTemp = ds.Tables(0)
                dtMain = dtTemp.Clone()
                dtMain.Clear()
                For Each drT As DataRow In dtTemp.Rows
                    dtMain.ImportRow(drT)
                Next
            End If
            Return ds
            'Code Added By vijay Ramteke on May14,2009
        Else
            If strIncludeHours.ToLower = "true" Then
                includeHours = True
            End If
            '   connect to the database
            Dim db As New DataAccess.DataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim DsStudEnrollMents As DataSet
            'build the sql query
            Dim sb As New Text.StringBuilder
            With sb
                .Append(" SELECT DISTINCT arStuEnrollments.StuEnrollId ")
                .Append(" FROM arStuEnrollments,syCampGrps,syCmpGrpCmps,syCampuses,arPrgVersions,arSAPChkResults ")
                .Append(" WHERE arSAPChkResults.StuEnrollId=arStuEnrollments.StuEnrollId AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId ")
                .Append(" AND syCampGrps.CampGrpId = syCmpGrpCmps.CampGrpId ")
                .Append(" AND syCampuses.CampusId = arStuEnrollments.CampusId ")
                .Append(" AND arPrgVersions.PrgVerId=arStuEnrollments.PrgVerId and arSAPChkResults.PreviewSapCheck = 0 ")
                .Append(" AND " & ParamInfo.FilterList & " ")
                If ParamInfo.FilterOther <> "" Then
                    .Append(" AND " & ParamInfo.FilterList & " ")
                End If

            End With
            DsStudEnrollMents = db.RunSQLDataSet(sb.ToString)
            Try
                Dim dtNewGetGPAandCGPA As DataTable
                Dim count As Boolean = False
                If DsStudEnrollMents.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In DsStudEnrollMents.Tables(0).Rows
                        stuEnrollId = dr("StuEnrollId").ToString
                        If strIncludeHours.ToLower = "true" Then
                            includeHours = True
                        End If
                        dtGetGPAandCGPA = (New GraduateAuditFacade).GetTermProgressFromResults(stuEnrollId, gradeReps, includeHours)
                        ds = (New AttendanceHistoryDB).GetInvidualSAPDetails(ParamInfo, dtGetGPAandCGPA, stuEnrollId)
                        'Code Added By Vijay Ramteke on May, 10 2009
                        If Not ds Is Nothing Then
                            If count = False Then
                                count = True
                                dtTemp = ds.Tables(0)
                                dtMain = dtTemp.Clone()
                                dtMain.Clear()
                                For Each drT As DataRow In dtTemp.Rows
                                    dtMain.ImportRow(drT)
                                Next
                            Else
                                For Each dr1 As DataRow In ds.Tables(0).Rows
                                    dtMain.ImportRow(dr1)
                                Next
                            End If
                        End If
                        'Code Added By Vijay Ramteke on May, 10 2009
                    Next
                End If
            Catch ex As Exception
            End Try
        End If
        '--
        'If dtGetGPAandCGPA.Rows.Count >= 1 Then
        '    intMaxIndexCount = dtGetGPAandCGPA.Rows.Count - 1
        '    CGPA = dtGetGPAandCGPA.Rows(intMaxIndexCount)("GPA")
        '    GPA = dtGetGPAandCGPA.Rows(intMaxIndexCount)("TermGPA")
        'End If
        'ds = (New AttendanceHistoryDB).GetInvidualSAPDetails(ParamInfo, GPA, CGPA)
        '--code commented by Priyanka on date 8th of May 2009
        'ds = (New AttendanceHistoryDB).GetInvidualSAPDetails(ParamInfo, dtGetGPAandCGPA)
        '--
        'Code Added By Vijay Ramteke on May, 10 2009
        If Not ds Is Nothing Then
            If ds.Tables.Count > 0 Then
                ds.Tables.RemoveAt(0)
                ds.Tables.Add(dtMain)
            End If
        Else
            If dtMain.Rows.Count > 0 Then
                ds = New DataSet()
                ds.Tables.Add(dtMain)
            End If
        End If
        'Code Added By Vijay Ramteke on May, 10 2009
        Return ds
    End Function
End Class
