Public Class CoursesFacade

    Public Function GetAllCourses(ByVal CampusId As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New CoursesDB
        Dim ds As New DataSet

        '   get the dataset with all degrees
        ds = DB.GetCourses(CampusId)
        ds = ConcatenateCourseCodeAndDescrip(ds, "Courses")
        Return ds
    End Function
    Public Function ConcatenateCourseCodeAndDescrip(ByVal ds As DataSet, ByVal tableName As String) As DataSet
        'Dim dt As DataTable = ds.Tables(tableName)

        With ds.Tables(0)
            '.PrimaryKey = New DataColumn() {.Columns("ReqId")}

            If .Rows.Count > 0 Then
                'Add a new column called CodeAndDescrip to the datatable 
                Dim col As DataColumn = .Columns.Add("CodeAndDescrip", Type.GetType("System.String"))

                '   column contains course code and description in this format: "(Code) Descrip"
                col.AllowDBNull = True
                col.Expression = "'(' + Code + ') ' + Descrip"
            End If
        End With

        Return ds
    End Function
    Public Function GetCourseDetails(ByVal CourseId As String) As CourseInfo
        With New CoursesDB
            Return .GetCourseDetails(CourseId)
        End With
    End Function

    Public Function InsertCourse(ByVal Course As CourseInfo, ByVal user As String) As String
        Dim db As New CoursesDB

        Return db.InsertCourse(Course, user)
    End Function

    Public Function DeleteCourse(ByVal Course As String, ByVal modDate As DateTime) As String
        Dim DB As New CoursesDB

        Return DB.DeleteCourse(Course, modDate)
    End Function

    Public Function UpdateCourse(ByVal Course As CourseInfo, ByVal user As String) As String
        Dim DB As New CoursesDB

        Return DB.UpdateCourse(Course, user)
    End Function

    Public Function DoesReqHasAttendanceRecords(ByVal reqId As String) As Boolean
        Dim DB As New CoursesDB

        Return DB.DoesReqHasAttendanceRecords(reqId)
    End Function

    Public Function CheckCourseWithProgramAttendanceType(ByVal reqId As String, ByVal attType As String) As Boolean
        Dim DB As New CoursesDB

        Return DB.CheckCourseWithProgramAttendanceType(reqId, attType)
    End Function
    Public Function GetCourses(ByVal CampusId As String, ByVal ParentReqId As String) As DataSet
        Dim DB As New CoursesDB
        Return DB.GetCourses(CampusId, ParentReqId)
    End Function
    Public Function AddCourseEquivalent(ByVal ParentReqId As String, ByVal EquivalentReqId() As String, ByVal user As String)
        Dim db As New CoursesDB
        Return db.AddCourseEquivalent(ParentReqId, EquivalentReqId, user)
    End Function
    Public Function DeleteCourseEquivalent(ByVal ParentReqId As String) As String
        Dim db As New CoursesDB
        Return db.DeleteCourseEquivalent(ParentReqId)
    End Function

    Public Function IsExternship(ByVal courseId As String) As Boolean
        Dim DB As New CoursesDB

        Return DB.IsExternship(courseId)

    End Function
    Public Function GetAllCourses_SP(ByVal showActiveOnly As Boolean, ByVal CampusId As String) As DataTable

        '   Instantiate DAL component


        Dim DB As New CoursesDB
        Return DB.GetAllCourses_SP(showActiveOnly, CampusId)

    End Function
    Public Function GetCoursesForTerm_SP(ByVal TermId As String, ByVal campusid As String, Optional ByVal showActiveOnly As Boolean = True) As DataSet

        Dim DB As New CoursesDB
        Return DB.GetCoursesForTerm_SP(TermId, campusid, showActiveOnly)

    End Function
    Public Function GetCoursesFromClassSectionsForTerm_SP(ByVal TermId As String) As DataTable

        Dim DB As New CoursesDB
        Return DB.GetCoursesFromClassSectionsForTerm_SP(TermId)

    End Function
    Public Function GetClassSections(ByVal StartDate As Date, ByVal EndDate As Date, ByVal CampusId As String, _
                                    ByVal InstructorId As String, ByVal CourseId As String, ByVal TermId As String) As DataSet
        Return (New CoursesDB).GetClassSections(StartDate, EndDate, CampusId, InstructorId, CourseId, TermId)

    End Function
    Public Function GetClassSectionMeetings(ByVal ClsSectionId As String) As DataSet
        Return (New CoursesDB).GetClassSectionMeetings(ClsSectionId)
    End Function
    Public Function GetClassSectionsAndMeetings(ByVal StartDate As Date, ByVal EndDate As Date, ByVal CampusId As String,
                                   ByVal InstructorId As String, ByVal CourseId As String, ByVal TermId As String) As DataSet
        Return (New CoursesDB).GetClassSectionsAndMeetings(StartDate, EndDate, CampusId, InstructorId, CourseId, TermId)
    End Function

    Public Function IsCourseActive(ByVal reqId As String) As Boolean
        Dim db As New CoursesDB
        Dim strStatus As String

        strStatus = db.GetCourseStatus(reqId)

        If strStatus = "Active" Then
            Return True
        Else
            Return False
        End If
    End Function
End Class
