Public Class StdAddressFacade
    'Public Function PopulateDataList(ByVal studentId As String, ByVal statusId As String) As DataSet

    '    Dim db As New StdAddressDB
    '    Dim ds As New DataSet
    '    ds = db.GetStudentAddresses(studentId, statusId)
    '    Return ds

    'End Function
    Public Function DoesDefaultAddExist(ByVal StudentId As String, ByVal StdAddressId As String) As Integer
        Dim ds As New DataSet
        Dim db As New StdAddressDB
        Dim count As Integer

        count = db.DoesDefaultAddExist(StudentId, StdAddressId)

        Return count
    End Function
    Public Function GetAllStudentAddresses(ByVal studentId As String) As DataSet

        Dim db As New StdAddressDB
        Dim ds As New DataSet
        ds = db.GetStudentAddresses(studentId)
        Return ds
    End Function

    Public Function GetStudentAddressInfo(ByVal StdAddressId As String) As StuAddressInfo

        '   Instantiate DAL component
        With New StdAddressDB

            '   get the BankInfo
            Return .GetStudentAddressInfo(StdAddressId)

        End With

    End Function
    Public Function GetDefaultStudentAddress(ByVal StudentId As String) As StuAddressInfo

        '   Instantiate DAL component
        With New StdAddressDB

            '   get the BankInfo
            Return .GetDefaultStudentAddress(StudentId)

        End With

    End Function
    Public Function UpdateStudentAddress(ByVal StudentAddressInfo As StuAddressInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New StdAddressDB

            '   If it is a new account do an insert. If not, do an update
            If Not (StudentAddressInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddStudentAddress(StudentAddressInfo, user)
            Else
                '   return integer with update results
                Return .UpdateStudentAddress(StudentAddressInfo, user)
            End If

        End With

    End Function
    Public Function DeleteStudentAddress(ByVal StdAddressId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New StdAddressDB

            '   delete BankInfo ans return integer result
            Return .DeleteStudentAddress(StdAddressId, modDate)

        End With

    End Function
End Class
