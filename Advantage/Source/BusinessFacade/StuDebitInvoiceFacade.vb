Public Class StuDebitInvoiceFacade
    Public Function GetStudentDebitInvoices(ByVal StartDate As String, ByVal CampusID As String, ByVal StatusId As String, ByVal ProgramId As String, ByVal ExpectDate As String) As DataSet
        Dim ds As New DataSet
        Dim stuName As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String

        ds = (New StuDebitInvoiceDB).GetStudentDebitInvoices(StartDate, CampusID, StatusId, ProgramId, ExpectDate)

        'Add new column
        ds.Tables(0).Columns.Add(New DataColumn("Student", System.Type.GetType("System.String")))

        'Get mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr As DataRow In ds.Tables(0).Rows
            stuName = dr("LastName")
            If Not dr.IsNull("FirstName") Then
                If dr("FirstName") <> "" Then
                    stuName &= ", " & dr("FirstName")
                End If
            End If
            If Not dr.IsNull("MiddleName") Then
                If dr("MiddleName") <> "" Then
                    stuName &= " " & dr("MiddleName") & "."
                End If
            End If
            dr("Student") = stuName

            'Apply mask to SSN.
            If Not dr.IsNull("SSN") Then
                If dr("SSN") <> "" Then
                    Dim temp As String = dr("SSN")
                    dr("SSN") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                End If
            End If
        Next

        Return ds

    End Function

    Public Function GetInvoiceDS(ByVal CampusId As String, ByVal StuEnrollIdList As String, ByVal RefDate As String) As DataSet
        Return (New StuDebitInvoiceDB).GetInvoiceDS(CampusId, StuEnrollIdList, RefDate)
    End Function
End Class
