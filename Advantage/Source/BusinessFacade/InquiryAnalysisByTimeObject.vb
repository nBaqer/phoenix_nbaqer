Public Class InquiryAnalysisByTimeObject
    Inherits BaseReportFacade

    Private dt As DataTable

    Public Sub New()
        MyBase.New()
        dt = New DataTable("InquiryAnalysisByTime")
        BuildResultTable()
    End Sub

    Public Overrides Function GetReportDataSet(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim inqByTime As New InquiryAnalysisByTime
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by the InquiryAnalysisByTime report.
        BuildReportSource(inqByTime.GetLeadsCreatedDate(paramInfo))
        ds.Tables.Add(dt)
        ' The InquiryAnalysisByTime report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

    Private Sub BuildResultTable()
        Dim dr As DataRow

        dt.Columns.Add(New DataColumn("TimePeriod", System.Type.GetType("System.String")))
        dt.Columns.Add(New DataColumn("NumOfInquiries", System.Type.GetType("System.Int32")))
        dt.Columns.Add(New DataColumn("PercOfTotal", System.Type.GetType("System.Decimal")))

        ' Add a row for every hour of the day.
        dr = dt.NewRow
        dr("TimePeriod") = "12:00AM to 12:59AM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "01:00AM to 01:59AM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "02:00AM to 02:59AM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "03:00AM to 03:59AM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "04:00AM to 04:59AM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "05:00AM to 05:59AM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "06:00AM to 06:59AM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "07:00AM to 07:59AM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "08:00AM to 08:59AM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "09:00AM to 09:59AM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "10:00AM to 10:59AM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "11:00AM to 11:59AM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "12:00PM to 12:59PM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "01:00PM to 01:59PM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "02:00PM to 02:59PM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "03:00PM to 03:59PM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "04:00PM to 04:59PM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "05:00PM to 05:59PM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "06:00PM to 06:59PM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "07:00PM to 07:59PM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "08:00PM to 08:59PM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "09:00PM to 09:59PM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "10:00PM to 10:59PM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("TimePeriod") = "11:00PM to 11:59PM"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
        ' Add last row of table.
        dr = dt.NewRow
        dr("TimePeriod") = "Total"
        dr("NumOfInquiries") = 0
        dr("PercOfTotal") = 0
        dt.Rows.Add(dr)
    End Sub

    Private Sub BuildReportSource(ByVal ds As DataSet)
        Dim d As DateTime
        'Dim strTime As String
        Dim dr As DataRow
        Dim intCounter As Integer = 0

        Try
            ' Update "NumOfInquiries" column for every SourceDate in dataset.
            For Each SourceRow As DataRow In ds.Tables(0).Rows
                ' Exclude rows with SourceDate = NULL.
                'If  SourceRow("SourceDate").GetType() Is GetType(DateTime) Then
                'd = DirectCast(SourceRow("SourceDate"), DateTime)
                d = CDate(SourceRow("SourceDate"))
                dr = GetRow(dt, d)
                dr.Item("NumOfInquiries") = CType(dr("NumOfInquiries"), Integer) + 1
                intCounter += 1
                'End If
            Next
            ' Update column "PercOfTotal" for every row in datatable.
            ComputePercentOfTotal(intCounter)
            ' Update last row of table.
            dr = dt.Rows(dt.Rows.Count - 1)
            dr("NumOfInquiries") = intCounter
            dr("PercOfTotal") = IIf(intCounter > 0, 100, 0)
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try
    End Sub

    Private Function GetRow(ByVal dtResultTable As DataTable, ByVal SourceDate As DateTime) As DataRow
        Dim boolRowFound As Boolean = False
        Dim dr As DataRow = Nothing
        Dim temp, temp2 As DateTime
        Dim i As Integer = 0
        Dim strSplit() As String
        Dim strTimePeriod As String

        ' Parse datatable to identify the time range where SourceDate falls.
        Do While i <= dtResultTable.Rows.Count And Not boolRowFound
            dr = dtResultTable.Rows(i)
            strTimePeriod = dr.Item("TimePeriod")
            strSplit = strTimePeriod.Split(" ")
            If strSplit.GetLength(0) = 3 Then
                temp = GetDate(SourceDate, strSplit(0))
                temp2 = GetDate(SourceDate, strSplit(2))
                If SourceDate >= temp And SourceDate <= temp2 Then
                    boolRowFound = True
                End If
            End If
            i += 1
        Loop
        Return dr
    End Function

    Private Function GetDate(ByVal SourceDate As DateTime, ByVal TimeString As String) As DateTime
        Dim mm As Integer = SourceDate.Month
        Dim dd As Integer = SourceDate.Day
        Dim yyyy As Integer = SourceDate.Year
        Dim hr As Integer
        Dim min As Integer
        Dim sec As Integer
        Dim strSplit() As String

        strSplit = TimeString.Split(":")
        If strSplit.GetLength(0) = 2 Then
            hr = Integer.Parse(strSplit(0))
            min = Integer.Parse(strSplit(1).Substring(0, 2))
            sec = min
            If hr = 12 And strSplit(1).EndsWith("AM") Then
                hr = 0
            ElseIf hr <> 12 And strSplit(1).EndsWith("PM") Then
                hr += 12
            End If
        End If
        '   year    (1 through 9999). 
        '   month   (1 through 12) 
        '   day     (1 through the number of days in month). 
        '   hours   (0 through 23). 
        '   minutes (0 through 59). 
        '   seconds (0 through 59). 
        Return New DateTime(yyyy, mm, dd, hr, min, sec)
    End Function

    Private Sub ComputePercentOfTotal(ByVal TotalOfInquiries As Integer)
        If TotalOfInquiries <> 0 Then
            For Each dr As DataRow In dt.Rows
                dr("PercOfTotal") = dr("NumOfInquiries") * 100 / TotalOfInquiries
            Next
        End If
    End Sub

End Class
