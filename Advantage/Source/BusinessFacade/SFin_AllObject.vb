
Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFin_AllObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SFin_All"

	Private dsRaw As New DataSet
	Private dtRptRaw As New DataTable
	Private dsRpt As New DataSet
	Private RptParamInfo As ReportParamInfoIPEDS

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SFin_AllObjectDB.GetReportDatasetRaw(RptParamInfo)

		' save passed-in report parameter info 
		Me.RptParamInfo = RptParamInfo

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		Dim drStudent As DataRow

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add(RptStuIdentifierColName, GetType(String))
			.Add("Name", GetType(String))
			.Add("Ind_MultEnroll", GetType(String))
			.Add("Tuition", GetType(Single))
			.Add("Fees", GetType(Single))
			.Add("PellDisb", GetType(Single))
			.Add("PellRefund", GetType(Single))
			.Add("FSEOGDisb", GetType(Single))
			.Add("FSEOGRefund", GetType(Single))
			.Add("SSIGDisb", GetType(Single))
			.Add("SSIGRefund", GetType(Single))
			.Add("ScholarshipsDisb", GetType(Single))
			.Add("ScholarshipsRefund", GetType(Single))
			.Add("FellowshipsDisb", GetType(Single))
			.Add("FellowshipsRefund", GetType(Single))
			.Add("TuitionWaiverDisb", GetType(Single))
			.Add("TuitionWaiverRefund", GetType(Single))
		End With

		For Each drStudent In dsRaw.Tables(TblNameStudents).Rows
			ProcessRows(drStudent)
		Next

		' return the Report DataSet
		Return dsRpt

	End Function

	Private Sub ProcessRows(ByVal drStudent As DataRow)
		Dim FilterString As String = "StudentId = '" & drStudent("StudentId").ToString & "'"
		Dim dvTuition As DataView
		Dim dvFees As DataView
		Dim dvAwards As DataView
		Dim dvRefunds As DataView
		Dim drRpt As DataRow
		Dim FundSourceDescrip As String
		Dim IncludeStudent As Boolean = False
		Dim ColDisb As String, ColRefund As String

		' create new report row
		drRpt = dsRpt.Tables(MainTableName).NewRow

		If RptParamInfo.ShowTuition Or RptParamInfo.ShowFees Then
			' compute student's total tuition, if requested by user
			If RptParamInfo.ShowTuition Then
				dvTuition = New DataView(dsRaw.Tables("TuitionFees"), _
										FilterString & " AND RptTransCodeDescrip LIKE 'Tuition'", _
										"", DataViewRowState.CurrentRows)
				' all tuition payments are entered as negative amounts, so use sum's absolute value
				drRpt("Tuition") = Math.Abs(CompSum(dvTuition, "TransAmount"))
				' include the student if has tuition payments
				If CSng(drRpt("Tuition")) > 0 Then
					IncludeStudent = True
				End If
			End If

			' compute student's total fees, if requested by user
			If RptParamInfo.ShowFees Then
				dvFees = New DataView(dsRaw.Tables("TuitionFees"), _
									FilterString & " AND RptTransCodeDescrip LIKE 'Fees'", _
									"", DataViewRowState.CurrentRows)
				' all fee payments are entered as negative amounts, so use sum's absolute value
				drRpt("Fees") = Math.Abs(CompSum(dvFees, "TransAmount"))
				' include the student if has fee payments (if not previously included)
				If Not IncludeStudent AndAlso CSng(drRpt("Fees")) > 0 Then
					IncludeStudent = True
				End If
			End If
		Else
			drRpt("Tuition") = 0
			drRpt("Fees") = 0
			IncludeStudent = False
		End If



		' compute student's total disbursements and refunds, for any fund sources requested by user
		If RptParamInfo.FilterFundSourceDescrips <> "" Then
			dvAwards = New DataView(dsRaw.Tables("Awards"))
			dvRefunds = New DataView(dsRaw.Tables("Refunds"))
			' loop thru fund sources requested by the user
			For Each FundSourceDescrip In RptParamInfo.FilterFundSourceDescrips.Split(",")
				' find and sum awards given to student from the fund source
				dvAwards.RowFilter = FilterString & " AND RptFundSourceDescrip LIKE '" & FundSourceDescrip & "'"
				ColDisb = FundSourceDescrip.Replace(" ", "") & "Disb"
				drRpt(ColDisb) = CompSum(dvAwards, "Amount")
				' find and sum refunds from student's account to the fund source
				dvRefunds.RowFilter = FilterString & " AND RefundFundSourceDescrip LIKE '" & FundSourceDescrip & "'"
				ColRefund = FundSourceDescrip.Replace(" ", "") & "Refund"
				drRpt(ColRefund) = CompSum(dvRefunds, "TransAmount")
				' include the student if has either disbursements and refunds (if not previously included)
				If Not IncludeStudent AndAlso (CSng(drRpt(ColDisb)) > 0 OrElse CSng(drRpt(ColRefund)) > 0) Then
					IncludeStudent = True
				End If
			Next
		End If

		' add report row to report dataset, if student should be included
		If IncludeStudent Then
			' set student identifier and name
			IPEDSFacade.SetStudentInfo(drStudent, drRpt)

			' set multiple enrollment indicator
			If CInt(drStudent("NumEnrollments")) > 1 Then
				drRpt("Ind_MultEnroll") = "*"
			End If

			dsRpt.Tables(MainTableName).Rows.Add(drRpt)
		End If

	End Sub

	Private Function CompSum(ByVal dv As DataView, ByVal SumCol As String) As Single
		Dim Sum As Single = 0
		Dim i As Integer

		For i = 0 To dv.Count - 1
			Sum += CSng(dv(i)(SumCol))
		Next

		Return Sum
	End Function

End Class
