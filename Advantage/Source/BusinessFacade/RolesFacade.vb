' ===============================================================================
' RolesFacade.vb
' Business Logic for managing Roles
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Data
Imports FAME.AdvantageV1.DataAccess
Imports FAME.AdvantageV1.Common


Public Class RolesFacade
    Public Shared Function GetRoles(ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataTable
        Return RolesDB.GetRoles(ShowActive, ShowInactive)
    End Function

    Public Shared Function GetExistingRoles() As DataTable
        Return RolesDB.GetExistingRoles()
    End Function

    Public Shared Function GetAdvantageRoles() As DataTable
        Return RolesDB.GetAdvantageRoles()
    End Function

    Public Shared Function GetRoleInfo(ByVal roleId As String) As RoleInfo
        Return RolesDB.GetRoleInfo(roleId)
    End Function

    Public Shared Function UpdateRoleInfo(ByVal roleInfo As RoleInfo, ByVal user As String) As String
        If Not (roleInfo.IsInDB = True) Then
            'return integer with insert results
            Return RolesDB.AddRoleInfo(roleInfo, user)
        Else
            'return integer with update results
            Return RolesDB.UpdateRoleInfo(roleInfo, user)
        End If
    End Function

    Public Shared Function DeleteRoleInfo(ByVal roleId As String, ByVal modDate As DateTime) As String
        Return RolesDB.DeleteRoleInfo(roleId, modDate)
    End Function

    Public Shared Function GetValidModules(ByVal roleId As String) As DataSet
        Return RolesDB.GetValidModules(roleId)
    End Function
    Public Shared Function GetAllModules() As DataSet
        Dim db As New ModulesDB
        Return db.GetAllModules()
    End Function
    Public Function GetAllResourcesModules() As DataSet
        Dim db As New ModulesDB
        Return db.GetAllResourcesModules()
    End Function
    Public Shared Function GetValidModulesForUser(ByVal UserId As String) As DataSet
        Return RolesDB.GetValidModulesForUser(UserId)
    End Function

    Public Shared Function AddValidModule(ByVal RoleId As String, ByVal ModuleId As String, ByVal user As String) As Boolean
        Return RolesDB.AddValidModule(RoleId, ModuleId, user)
    End Function

    Public Shared Function DeleteValidModules(ByVal roleId As String) As Boolean
        Return RolesDB.DeleteValidModules(roleId)
    End Function

    Public Shared Function GetUsersInRole(ByVal roleId As String) As DataSet
        Return RolesDB.GetUsersInRole(roleId)
    End Function
    Public Function CheckIfUserIsMappedToFrontDesk(ByVal UserId As String) As Boolean
        Return (New RolesDB).CheckIfUserIsMappedToFrontDesk(UserId)
    End Function
End Class
