Imports FAME.AdvantageV1.Common

Public MustInherit Class BaseReportFacadeIPEDS
	Public MustOverride Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
End Class
