﻿Imports System.Runtime.InteropServices

Public Class StudentPortalFacade
    Public Function GetPortalSetting(ByVal CampusId As String) As Boolean
        Dim obj As New StudentPortalDB
        Return obj.GetPortalSettingsByCampus(CampusId)
    End Function

    Public Sub UpdatePortalSettings(ByVal CampusId As String, _
                                    ByVal chkSourceCategoryAll As Boolean, ByVal SourceCategoryId As String, _
                                    ByVal chkSourceTypeAll As Boolean, ByVal SourceTypeId As String, _
                                    ByVal chkAdmRepAll As Boolean, ByVal AdmRepId As String, _
                                    ByVal chkLeadStatusAll As Boolean, ByVal LeadStatusId As String, _
                                    ByVal chkPhoneType1All As Boolean, ByVal PhoneType1Id As String, _
                                    ByVal chkPhoneType2All As Boolean, ByVal PhoneType2Id As String, _
                                    ByVal chkAddTypeAll As Boolean, ByVal AddTypeId As String, _
                                    ByVal chkGenderAll As Boolean, ByVal GenderId As String, _
                                    ByVal chkPortalCountryAll As Boolean, ByVal PortalCountryId As String, _
                                    ByVal chkPortalContactEmailAll As Boolean, ByVal PortalContactEmail As String, _
                                    ByVal chkEmailSubjectAll As Boolean, ByVal EmailSubject As String, _
                                    ByVal chkEmailBodyAll As Boolean, ByVal EmailBody As String)

        Dim PortalSettingsList As New StudentPortalDB

        PortalSettingsList.UpdatePortalInfoByCampus(CampusId, chkSourceCategoryAll, SourceCategoryId, chkSourceTypeAll, SourceTypeId,
                                                    chkAdmRepAll, AdmRepId, chkLeadStatusAll, LeadStatusId,
                                                    chkPhoneType1All, PhoneType1Id, chkPhoneType2All, PhoneType2Id,
                                                    chkAddTypeAll, AddTypeId, chkGenderAll, GenderId,
                                                    chkPortalCountryAll, PortalCountryId, chkPortalContactEmailAll, PortalContactEmail,
                                                    chkEmailSubjectAll, EmailSubject, chkEmailBodyAll, EmailBody)

    End Sub

    Public Function GetPortalValuesByCampus(ByVal CampusId As String) As DataSet
        Dim portalVaues As New StudentPortalDB
        Return portalVaues.GetPortalValuesByCampus(CampusId)
    End Function

End Class
