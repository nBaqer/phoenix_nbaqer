' ===============================================================================
' TMFacade.vb
' Business Logic for TM project
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' 6/14/06 - Performance enhancements to building a workflow tree
' 11/2/06 - (BEN) Added support for StartGap

Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Data
'Imports FAME.AdvantageV1.DataAccess
Imports FAME.AdvantageV1.DataAccess.TM
Imports FAME.AdvantageV1.Common.TM
Imports FAME.Advantage.Common

Namespace TM
#Region "Categories"
    Public Class CategoriesFacade
        Public Shared Function GetCategories(ByVal campusId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As Data.DataSet
            Return CategoriesDB.GetAll(campusId, ShowActive, ShowInactive)
        End Function

        Public Shared Function GetCategoryInfo(ByVal CategoryId As String) As CategoryInfo
            Return CategoriesDB.GetInfo(CategoryId)
        End Function

        Public Shared Function UpdateCategory(ByVal info As CategoryInfo, ByVal user As String) As Boolean
            If info.IsInDB = False Then
                Return CategoriesDB.Add(info, user)
            Else
                Return CategoriesDB.Update(info, user)
            End If
        End Function

        Public Shared Function DeleteCategory(ByVal CategoryId As String, ByVal modDate As DateTime) As String
            Return CategoriesDB.Delete(CategoryId, modDate)
        End Function
    End Class
#End Region

#Region "Permissions"
    Public Class PermissionsFacade
        Public Shared Function GetPermissions(ByVal UserId As String, ByVal CampGrpId As String) As Data.DataSet
            Return PermissionsDB.GetAll(UserId, CampGrpId)
        End Function

        Public Shared Function GetPermissionsInfo(ByVal SID As String) As PermissionsInfo
            Return PermissionsDB.GetInfo(SID)
        End Function

        Public Shared Function UpdatePermissions(ByVal info As PermissionsInfo, ByVal user As String) As Boolean
            If info.IsInDB = False Then
                Return PermissionsDB.Add(info, user)
            Else
                Return PermissionsDB.Update(info, user)
            End If
        End Function

        Public Shared Function DeletePermissions(ByVal SID As String) As Integer
            Return PermissionsDB.Delete(SID)
        End Function

        Public Shared Function DeleteUserPermissions(ByVal UserId As String) As Integer
            Return PermissionsDB.DeleteByUser(UserId)
        End Function


        Public Shared Function GetSystemUsers(ByVal CampGrpId As String, ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As Data.DataSet
            Return PermissionsDB.GetSystemUsers(CampGrpId, ShowActive, ShowInactive)
        End Function

        Public Shared Function GetSystemRoles() As Data.DataSet
            Return PermissionsDB.GetSystemRoles()
        End Function

        Public Shared Function GetCampusGroups() As Data.DataSet
            Return PermissionsDB.GetCampusGroups()
        End Function
        Public Shared Function GetCampusGroupsExceptAll() As Data.DataSet
            Return PermissionsDB.GetCampusGroupsExceptAll()
        End Function

        Public Shared Function GetAuthorizedAssignedToUsers(ByVal UserId As String, ByVal CampGrpId As String)
            Return PermissionsDB.GetAuthorizedAssignedToUsers(UserId, CampGrpId)
        End Function
    End Class
#End Region

#Region "Results"
    Public Class ResultsFacade
        Public Shared Function GetResults(ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Return ResultsDB.GetAll(ShowActive, ShowInactive)
        End Function

        Public Shared Function GetResultInfo(ByVal ResultId As String) As ResultInfo
            Return ResultsDB.GetInfo(ResultId)
        End Function

        Public Shared Function UpdateResult(ByVal info As ResultInfo, ByVal user As String) As Boolean
            If info.IsInDB = False Then
                Return ResultsDB.Add(info, user)
            Else
                Return ResultsDB.Update(info, user)
            End If
        End Function

        Public Shared Function DeleteResult(ByVal ResultId As String, ByVal modDate As DateTime) As String
            Return ResultsDB.Delete(ResultId, modDate)
        End Function

        ''' <summary>
        ''' Runs the external action associated with a result
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ExecuteAction(ByVal rInfo As ResultInfo, ByVal ReId As String) As Boolean
            Dim db As New DataAccess.DataAccess
            Dim sb As New StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            sb.Append("UPDATE " + rInfo.ResultActionTableName + vbCrLf)
            sb.Append(" SET " + rInfo.ResultActionColumnName + "='")
            sb.Append(rInfo.ResultActionValue + "'" + vbCrLf)
            sb.Append(" WHERE " + rInfo.ResultActionRowGuid + "='")
            sb.Append(ReId + "'")
            Try
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
            Catch ex As System.Exception
                If db.Connection.State = ConnectionState.Open Then
                    db.CloseConnection()
                End If
                Return False
            End Try
            Return True
        End Function

        ''' <summary>
        ''' Builds a sql query that out of a column and a value.
        ''' This query is used to execute some sort of action when a result is assigned
        ''' to a task.
        ''' This value is stored in tmResults.ExternalAction
        ''' TODO: This list of columns and column->table mappings needs to be defined
        ''' in an xml file
        ''' </summary>
        ''' <param name="column"></param>
        ''' <param name="value"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function BuildExternalAction(ByVal column As String, ByVal value As String) As String
            Dim table As String = ""
            Dim rowid As String = ""
            If column = "LeadStatus" Then
                table = "adLeads"
                rowid = "LeadId"
            End If
            Dim sb As New StringBuilder
            sb.Append("UPDATE " + table + vbCrLf)
            sb.Append("SET " + column + "='" + value + "'" + vbCrLf)
            sb.Append("WHERE " + rowid + " = '[VALUE]'")

            Return sb.ToString
        End Function
    End Class
#End Region

#Region "ResultTasks"
    Public Class ResultTasksFacade
        Public Shared Function GetNextTasks(ByVal ResultId As String) As DataSet
            Return ResultTasksDB.GetAll(ResultId)
        End Function

        Public Shared Function GetInfo(ByVal ResultTaskId As String) As ResultTaskInfo
            Return ResultTasksDB.GetInfo(ResultTaskId)
        End Function

        Public Shared Function UpdateNextTask(ByVal info As ResultTaskInfo, ByVal user As String) As Boolean
            If info.IsInDB = False Then
                Return ResultTasksDB.Add(info, user)
            Else
                Return ResultTasksDB.Update(info, user)
            End If
        End Function

        Public Shared Function Delete(ByVal ResultTaskId As String) As Boolean
            Return ResultTasksDB.Delete(ResultTaskId)
        End Function

        Public Shared Function DeleteAllByResultId(ByVal ResultId As String) As Boolean
            Return ResultTasksDB.DeleteAllByResultId(ResultId)
        End Function

        Public Shared Function GetExternalActionTables() As DataSet
            Dim xmlFileName As String = HttpContext.Current.Server.MapPath("TM/xml/ExternalActions.xml")
            Dim ds As New DataSet
            ds.ReadXml(xmlFileName, XmlReadMode.InferSchema)
            Return ds
        End Function

        Public Shared Function GetExternalActionColumns(ByVal ds As DataSet, ByVal table As String) As DataSet
            'Return ds.Tables(0).Select("table=" + table)
            Return ds
        End Function
    End Class
#End Region

#Region "TaskResults"
    Public Class TaskResultsFacade
        Public Shared Function GetTaskResults(ByVal TaskId As String) As DataSet
            Return TaskResultsDB.GetAll(TaskId)
        End Function

        Public Shared Function GetTaskResultInfo(ByVal TaskResultId As String) As TaskResultInfo
            Return TaskResultsDB.GetInfo(TaskResultId)
        End Function

        Public Shared Function UpdateTaskResult(ByVal info As TaskResultInfo, ByVal user As String) As Integer
            If info.IsInDB = False Then
                Return TaskResultsDB.Add(info, user)
            Else
                Return TaskResultsDB.Update(info, user)
            End If
        End Function

        Public Shared Function DeleteTaskResult(ByVal TaskResultId As String) As Integer
            Return TaskResultsDB.Delete(TaskResultId)
        End Function

        Public Shared Function DeleteTaskResultsForTask(ByVal TaskId As String) As Integer
            Return TaskResultsDB.DeleteResultsByTask(TaskId)
        End Function
    End Class
#End Region

#Region "Tasks"
    Public Class TasksFacade
        Public Shared Function GetTasks(ByVal CampGroupid As String, ByVal ModuleId As String, ByVal EntityId As String, _
                                            ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Return TasksDB.GetAll(CampGroupid, ModuleId, EntityId, ShowActive, ShowInactive)
        End Function
        Public Shared Function GetFilterTasks(ByVal CampGroupid As String, ByVal CategoryId As String, ByVal ModuleId As String, _
                                           ByVal ShowActive As Boolean, ByVal ShowInactive As Boolean) As DataSet
            Return TasksDB.GetFilter(CampGroupid, CategoryId, ModuleId, ShowActive, ShowInactive)
        End Function

        Public Shared Function GetTasksForUser(ByVal UserId As String, ByVal CampusId As String, ByVal ModuleId As String) As DataSet
            ' BEN - 10/27/06 - Add support for all campus group
            Return TasksDB.GetAllForUser(UserId, CampusId, ModuleId)
        End Function

        Public Shared Function GetTaskInfo(ByVal TaskId As String) As TaskInfo
            Return TasksDB.GetInfo(TaskId)
        End Function

        Public Shared Function UpdateTask(ByVal tInfo As TaskInfo, ByVal user As String) As Boolean
            If tInfo.IsInDB = False Then
                Return TasksDB.Add(tInfo, user)
            Else
                Return TasksDB.Update(tInfo, user)
            End If
        End Function

        Public Shared Function DeleteTask(ByVal TaskId As String, ByVal modDate As DateTime) As String
            Return TasksDB.Delete(TaskId, modDate)
        End Function

        ''' <summary>
        ''' Recursively builds the tree needed to draw the flowchart. Assumes no cycles!
        ''' </summary>        
        ''' <remarks></remarks>
        Protected Shared Sub BuildTree(ByVal level As Integer, ByRef totChildren As Integer, _
                ByVal maxLevels As Integer, ByVal info As TaskInfo, ByRef dsAllTasks As DataSet, _
                ByRef dsAllTaskResults As DataSet, ByRef dsAllResultTasks As DataSet)

            If level > maxLevels - 1 Then Return ' limit the number of levels
            If totChildren > 100 Then Return ' limit the number of children

            Dim taskid As String = info.TaskId
            If taskid = System.Guid.Empty.ToString() Then Return

            Dim resultId As String
            Dim resultDescrip As String
            Dim NextTasks() As DataRow
            Dim NextTaskInfo As TaskInfo

            'for each result get the next tasks and add their as children
            Dim Results() As DataRow = dsAllTaskResults.Tables(0).Select(String.Format("TaskId='{0}'", taskid)) 'TaskResultsDB.GetAll(taskid)
            For Each result As DataRow In Results
                resultDescrip = result.Item("Descrip")
                info.ChildrenResults.Add(resultDescrip)
                resultId = result.Item("ResultId").ToString
                NextTasks = dsAllResultTasks.Tables(0).Select(String.Format("ResultId='{0}'", resultId)) 'ResultTasksDB.GetAll(resultId)
                For Each nextTask As DataRow In NextTasks
                    totChildren += 1 ' keep track of the total number of children so far
                    taskid = nextTask.Item("TaskId").ToString
                    NextTaskInfo = _GetTaskFromDataSet(taskid, dsAllTasks)
                    info.Children.Add(NextTaskInfo)
                Next

                If NextTasks.Length = 0 Then
                    NextTaskInfo = New TaskInfo
                    NextTaskInfo.TaskId = System.Guid.Empty.ToString()
                    info.Children.Add(NextTaskInfo)
                End If
            Next

            'repeat for each child task that was just added            
            level = level + 1
            For Each child As TaskInfo In info.Children
                BuildTree(level, totChildren, maxLevels, child, dsAllTasks, dsAllTaskResults, dsAllResultTasks)
            Next
        End Sub

        Private Shared Function _GetTaskFromDataSet(ByVal taskid As String, ByVal ds As DataSet) As TaskInfo
            Dim dr() As DataRow = ds.Tables(0).Select(String.Format("TaskId='{0}'", taskid))
            If dr Is Nothing Or dr.Length = 0 Then
                Return Nothing
            End If

            Dim info As New TaskInfo
            info.Descrip = dr(0)("Descrip").ToString()
            info.TaskId = taskid
            Return info
        End Function

        Public Shared Function BuildTree(ByVal taskid As String, ByVal maxLevels As Integer) As TaskInfo
            ' Retrieve all the Tasks, TaskResult and ResultTasks in the system
            Dim dsAllTasks As DataSet = TasksFacade.GetTasks(Nothing, Nothing, Nothing, True, True)
            Dim dsAllTaskResults As DataSet = TaskResultsFacade.GetTaskResults(Nothing)
            Dim dsAllResultTasks As DataSet = ResultTasksFacade.GetNextTasks(Nothing)

            ' get the top level task
            Dim info As TaskInfo = _GetTaskFromDataSet(taskid, dsAllTasks)

            ' build the tree
            Dim totChildren As Integer = 0
            BuildTree(1, totChildren, maxLevels, info, dsAllTasks, dsAllTaskResults, dsAllResultTasks)

            ' return the top level task with its built up children
            Return info
        End Function
    End Class
#End Region

#Region "UserTasks"
    Public Class UserTasksFacade
        Public Shared Function GetUserTasks(ByVal CampusId As String, _
                                        ByVal AssignedById As String, _
                                        ByVal OwnerId As String, _
                                        ByVal ReId As String, _
                                        ByVal Priority As String, _
                                        ByVal Status As TaskStatus, _
                                        ByVal DueDateMin As String, _
                                        ByVal DueDateMax As String, Optional ByVal filterOthers As String = "", Optional ByVal OwnerIdForAssignedToMe As String = "") As DataSet

            Return UserTasksDB.GetAll(CampusId, AssignedById, OwnerId, ReId, Priority, Status, DueDateMin, DueDateMax, filterOthers, OwnerIdForAssignedToMe)
        End Function

        Public Shared Function GetWorkflows(ByVal CampusId As String, _
                                        ByVal AssignedById As String, _
                                        ByVal OwnerId As String, _
                                        ByVal ReId As String, _
                                        ByVal Priority As String, _
                                        ByVal Status As TaskStatus, _
                                        ByVal DueDateMin As String, _
                                        ByVal DueDateMax As String) As DataSet
            Return UserTasksDB.GetAllWorkflows(CampusId, OwnerId, ReId, Priority, Status, DueDateMin, DueDateMax)
        End Function

        Public Shared Function GetContactName(ByVal id As String) As String
            Return UserTasksDB.GetContactName(id)
        End Function

        Public Shared Function GetUserTaskInfo(ByVal UserTaskId As String) As UserTaskInfo
            Dim strMask As String
            Dim fac As New InputMasksFacade
            strMask = fac.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
            Return UserTasksDB.GetInfo(UserTaskId, strMask)
        End Function

        ''' <summary>
        ''' Adds or updates a UserTask given a UserTaskInfo object.
        ''' If the Status field in UserTaskInfo is set to "Completed",
        ''' then new tasks are created as defined in tmResultTasks
        ''' </summary>
        ''' <param name="info"></param>
        ''' <param name="user"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function UpdateUserTask(ByVal info As UserTaskInfo, ByVal user As String) As Boolean
            Dim res As Boolean = True
            If info.IsInDB = False Then
                Return UserTasksDB.Add(info, user)
                ' don't do any more if we are adding a record
            Else
                res = UserTasksDB.Update(info, user)
            End If

            ' check if the status has been set to completed and
            ' if it has, create the new tasks associated with the result
            If info.PrevStatus = info.Status Or info.Status <> TaskStatus.Completed Then Return res

            ' if we got here, then there are additional things we need to do
            ' because the status has been set to "Completed".
            ' First, get all the tasks that should be created as a result of
            ' setting the result.
            Dim dsTasks As DataSet = ResultTasksFacade.GetNextTasks(info.ResultId)
            For Each dr As DataRow In dsTasks.Tables(0).Rows
                ' create a new task assignment for each row in tmResultTasks
                Dim newTaskInfo As New UserTaskInfo
                newTaskInfo.TaskId = dr("TaskId").ToString()
                newTaskInfo.AssignedById = info.AssignedById

                ' 11/2/06 (BEN) Adjust the EndDate by what has been setup in SetupResults.aspx                
                Dim StartGap As Integer = CType(dr("StartGap"), Integer)
                Dim StartGapUnit As String = dr("StartGapUnit").ToString()
                If StartGapUnit = "min" Then
                    newTaskInfo.EndDate = Date.Now.AddMinutes(StartGap)
                ElseIf StartGapUnit = "hour" Then
                    newTaskInfo.EndDate = Date.Now.AddHours(StartGap)
                Else
                    newTaskInfo.EndDate = Date.Now.AddDays(StartGap)
                End If
                newTaskInfo.StartDate = newTaskInfo.EndDate

                newTaskInfo.OwnerId = info.OwnerId
                newTaskInfo.Priority = info.Priority
                newTaskInfo.ReId = info.ReId
                newTaskInfo.Status = TaskStatus.Pending
                newTaskInfo.PrevStatus = newTaskInfo.Status
                newTaskInfo.PrevUserTaskId = info.UserTaskId
                UserTasksFacade.UpdateUserTask(newTaskInfo, user)
            Next

            ExecuteAction(info)

            Return res
        End Function
        Private Shared Function GetTaskIDFromStatusCode(ByVal statuscode As String, Optional ByVal campusId As String = "", Optional ByVal UserId As String = "") As String()
            'Dim dtable As DataTable = TaskResultsDB.GetTaskIDFromStatusCode(statuscode)
            Dim dtable As DataTable = TaskResultsDB.GetTaskIDFromStatusCode(statuscode, campusId, UserId)
            If (dtable Is Nothing) Then
                Return Nothing
            Else
                Dim rtn(dtable.Rows.Count) As String
                'rtn = New String(dtable.Rows.Count)
                Dim i As Integer
                For i = 0 To dtable.Rows.Count - 1
                    rtn(i) = dtable.Rows(i)("TaskId").ToString
                Next
                Return rtn
            End If
        End Function
        Public Shared Function AddTaskFromStatusCode(ByVal statusCode As String, ByVal userInfo As UserTaskInfo) As Boolean
            Dim rtn As Boolean = False
            Dim taskIDS As String() = GetTaskIDFromStatusCode(statusCode, userInfo.CampGrpID, userInfo.ModUser)
            Dim ownerIDS As String()
            If (userInfo.OwnerId = "") Then
                ownerIDS = GetOwnerIDByRoles()
            Else
                Dim repIDS(1) As String
                repIDS(0) = userInfo.OwnerId
                ownerIDS = repIDS
                'Dim Days() As Integer = New Integer(6) {}

                'Dim taskIDS As String(1) = userInfo.OwnerId
            End If
            If (taskIDS Is Nothing) Then
                Return False
            Else
                Try
                    Dim i As Integer = 0
                    Dim j As Integer = 0
                    For i = 0 To taskIDS.Length - 1
                        If (taskIDS(i) <> Nothing) Then
                            If (ownerIDS Is Nothing) Then
                                Return False
                            Else
                                'returns all the owners where tasks where added
                                For j = 0 To ownerIDS.Length - 1
                                    If (ownerIDS(j) <> Nothing) Then
                                        userInfo.UserTaskId = Guid.NewGuid.ToString()
                                        userInfo.TaskId = taskIDS(i)
                                        userInfo.OwnerId = ownerIDS(j)
                                        If (userInfo.ReId = "") Then
                                            userInfo.ReId = GetStudentIDfromEnrollID(userInfo.ResultId)
                                        End If
                                        rtn = UserTasksDB.Add(userInfo, userInfo.ModUser)
                                    End If
                                Next
                            End If
                        End If
                    Next
                Catch ex As Exception
                    rtn = False
                End Try
                Return rtn
            End If


        End Function

        Private Shared Function GetOwnerIDByRoles() As String()
            Dim dtable As DataTable = TaskResultsDB.GetOwnerIDByRoles()
            If (dtable Is Nothing) Then
                Return Nothing
            Else
                Dim rtn(dtable.Rows.Count) As String

                'rtn = New String(dtable.Rows.Count)
                Dim i As Integer
                For i = 0 To dtable.Rows.Count - 1
                    rtn(i) = dtable.Rows(i)("UserId").ToString
                Next
                Return rtn
            End If
        End Function

        Private Shared Function GetStudentIDfromEnrollID(ByVal enrollId As String) As String
            Dim rtn As String = TaskResultsDB.GetStudentIDfromEnrollID(enrollId)
            Return rtn
        End Function

        ''' <summary>
        ''' Executes the other action associated with the given task being assigned
        ''' a result.  
        ''' </summary>
        ''' <param name="info"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Shared Function ExecuteAction(ByVal info As UserTaskInfo) As Boolean
            Try
                ' execute the external action            
                Dim rInfo As ResultInfo = ResultsFacade.GetResultInfo(info.ResultId)
                Return ResultsFacade.ExecuteAction(rInfo, info.ReId)
            Catch ex As System.Exception
            End Try
            Return False
        End Function

        Public Shared Function DeleteUserTaks(ByVal UserTaskId As String) As String
            Return UserTasksDB.Delete(UserTaskId)
        End Function
    End Class
#End Region

#Region "Workflow"
    ''' <summary>
    ''' BL for tmWorkflow
    ''' </summary>
    ''' <remarks></remarks>
    Public Class WorkflowFacade
        'Public Shared Function GetAll(byval cmpId as string) As Data.DataSet
        Public Shared Function GetAll() As Data.DataSet
            Return WorkflowDB.GetAll()
        End Function

        Public Shared Function GetWorkflow(ByRef oObj As WorkflowInfo) As Integer
            Return WorkflowDB.GetWorkflow(oObj)
        End Function

        Public Shared Function UpdateWorkflow(ByVal oObj As WorkflowInfo, ByVal user As String) As Integer
            If oObj.IsInDB = False Then
                Return WorkflowDB.Add(oObj, user)
            Else
                Return WorkflowDB.Update(oObj, user)
            End If
        End Function

        Public Shared Function Delete(ByVal oObj As WorkflowInfo) As Integer
            Return WorkflowDB.Delete(oObj.WorkflowId)
        End Function

        Public Shared Function Delete(ByVal strId As String) As Integer
            Return WorkflowDB.Delete(strId)
        End Function

    End Class
#End Region

#Region "Contact Search"
    Public Class ContactSearchFacade
        ' Search for a recipient for a message
        ' The important return is a GUID that represents the id of the recipient
        Public Shared Function RecipientSearch(ByVal RecipientType As String, ByVal Search As String, ByVal CampusId As String) As DataSet
            Select Case RecipientType
                Case "Student"
                    Return ContactSearchFacade.SearchStudent(Search, CampusId)
                Case "Lead"
                    Return ContactSearchFacade.SearchLead(Search, CampusId)
                Case "Lender"
                    Return ContactSearchFacade.SearchLender(Search, CampusId)
                Case "Employees"
                    Return ContactSearchFacade.SearchEmployees(Search, CampusId)
                Case "Employers"
                    Return ContactSearchFacade.SearchEmployers(Search, CampusId)
            End Select
            Return Nothing ' Bad param sent
        End Function

        Public Shared Function SearchStudent(ByVal search As String, ByVal CampusId As String) As DataSet
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("   TOP 50 S.StudentId RecipientId, " & vbCrLf)
            sb.Append("   (S.FirstName + ' ' + S.LastName) Name, " & vbCrLf)
            sb.Append("   S.SSN SSN, " & vbCrLf)
            sb.Append("   '' SecondIdDesc " & vbCrLf)
            sb.Append("FROM arStudent S " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            Dim strData As String = search.Trim
            Dim iPosition As Integer = strData.IndexOf(" ")
            If iPosition > 0 Then
                sb.Append("(S.LastName like '" + Right(strData, iPosition + 1) + "%' " & vbCrLf)
                sb.Append("OR S.FirstName like '" + Left(strData, iPosition) + "%') " & vbCrLf)
            Else
                sb.Append("(S.LastName like '" + strData + "%' " & vbCrLf)
                sb.Append("OR S.FirstName like '" + strData + "%') " & vbCrLf)
            End If

            ' restrict the search to this campusid
            If Not CampusId Is Nothing AndAlso CampusId <> "" Then
                sb.AppendFormat(" AND '{0}' in (select CampusId from arStuEnrollments SE where S.StudentId=SE.StudentId) {1}", CampusId, vbCrLf)
            End If

            sb.Append("ORDER BY S.LastName, S.FirstName" & vbCrLf)
            'Execute the query a
            Dim ds As DataSet = db.RunSQLDataSet(sb.ToString)
            TMCommon.ApplySSNMaskToDS(ds)
            Return ds
        End Function

        Public Shared Function SearchLead(ByVal search As String, ByVal CampusId As String) As DataSet
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build query to select students
            Dim sb As New StringBuilder
            Dim strData As String = search.Trim
            Dim iPosition As Integer = strData.IndexOf(" ", StringComparison.Ordinal)

            sb.Append("  SELECT TOP 50                                  ")
            sb.Append("         l.LeadId AS RecipientId                 ")
            sb.Append("        ,( FirstName + ' ' + LastName ) AS Name  ")
            sb.Append("        ,p.Phone                                 ")
            sb.Append("        ,m.EMail AS Email                        ")
            sb.Append("        ,SSN                                     ")
            sb.Append("        ,'' AS SecondIdDesc                      ")
            sb.Append("  FROM   adLeads l                               ")
            sb.Append("  LEFT JOIN ( SELECT *                           ")
            sb.Append("              FROM   adLeadPhone                 ")
            sb.Append("              WHERE  Position = 1                ")
            sb.Append("            ) p ON p.LeadId = l.LeadId           ")
            sb.Append("  LEFT JOIN ( SELECT *                           ")
            sb.Append("              FROM   dbo.AdLeadEmail             ")
            sb.Append("              WHERE  IsPreferred = 1             ")
            sb.Append("            ) m ON m.LeadId = l.LeadId           ")                                           
            sb.Append("WHERE ")
                                                           

            'sb.Append(" SELECT TOP 50 LeadID AS RecipientId, " & vbCrLf)
            'sb.Append("   (FirstName + ' ' + LastName) AS Name, " & vbCrLf)
            'sb.Append(" Phone,HomeEmail AS Email, " + vbCrLf)
            'sb.Append("   SSN, " & vbCrLf)
            'sb.Append("   '' AS SecondIdDesc ")
            'sb.Append("FROM adLeads" & vbCrLf)
 
            If Not campusId Is Nothing AndAlso campusId <> "" Then
                sb.Append("   CampusId = '" + campusId + "' AND " & vbCrLf)
            End If

            If iPosition > 0 Then
                sb.Append("(LastName like '" + Right(strData, iPosition + 1) + "%' " & vbCrLf)
                sb.Append("OR FirstName like '" + Left(strData, iPosition) + "%') " & vbCrLf)
            Else
                sb.Append("(LastName like '" + strData + "%' " & vbCrLf)
                sb.Append("OR FirstName like '" + strData + "%') " & vbCrLf)
            End If


            sb.Append("ORDER BY Name")
            Dim ds As DataSet = db.RunSQLDataSet(sb.ToString)
            ' apply the ssn mask
            TMCommon.ApplySSNMaskToDS(ds)
            Return ds
        End Function

        Public Shared Function SearchLender(ByVal search As String, ByVal CampusId As String) As DataSet
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            '   build query to select students
            Dim sb As New StringBuilder
            sb.Append("SELECT TOP 50 LenderID RecipientId, ")
            sb.Append("   LenderDescrip as Name, ")
            sb.Append("   '' SSN, " & vbCrLf)
            sb.Append("   '' SecondIdDesc " & vbCrLf)
            sb.Append("FROM faLenders" & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            sb.Append("   StatusId is not NULL " & vbCrLf)
            sb.Append("   AND LenderDescrip like '" + search.Trim + "%' " & vbCrLf)
            sb.Append("ORDER BY Name")

            'Execute the query a
            Return db.RunSQLDataSet(sb.ToString)
        End Function

        Public Shared Function SearchEmployees(ByVal search As String, ByVal CampusId As String) As DataSet
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("   E.EmpId RecipientId, " & vbCrLf)
            sb.Append("   (E.FirstName + ' ' + E.LastName) Name, " & vbCrLf)
            sb.Append("   E.SSN SSN, " & vbCrLf)
            sb.Append("   '' SecondIdDesc " & vbCrLf)
            sb.Append("FROM hrEmployees E " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            Dim strData As String = search.Trim
            Dim iPosition As Integer = strData.IndexOf(" ")
            If iPosition > 0 Then
                sb.Append("(E.LastName like '" + Right(strData, iPosition + 1) + "%' " & vbCrLf)
                sb.Append("OR E.FirstName like '" + Left(strData, iPosition) + "%') " & vbCrLf)
            Else
                sb.Append("(E.LastName like '" + strData + "%' " & vbCrLf)
                sb.Append("OR E.FirstName like '" + strData + "%') " & vbCrLf)
            End If
            sb.Append("ORDER BY E.LastName, E.FirstName" & vbCrLf)
            'Execute the query a
            Dim ds As DataSet = db.RunSQLDataSet(sb.ToString)
            TMCommon.ApplySSNMaskToDS(ds)
            Return ds
        End Function

        Public Shared Function SearchEmployers(ByVal search As String, ByVal CampusId As String) As DataSet
            Dim db As New DataAccess.DataAccess

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT " & vbCrLf)
            sb.Append("   EmployerId RecipientId, " & vbCrLf)
            sb.Append("   EmployerDescrip Name, " & vbCrLf)
            sb.Append("   '' SSN, " & vbCrLf)
            sb.Append("   '' SecondIdDesc " & vbCrLf)
            sb.Append("FROM plEmployers " & vbCrLf)
            sb.Append("WHERE " & vbCrLf)
            Dim strData As String = search.Trim
            Dim iPosition As Integer = strData.IndexOf(" ")
            If iPosition > 0 Then
                sb.Append("EmployerDescrip like '" + Right(strData, iPosition + 1) + "%' " & vbCrLf)
            Else
                sb.Append("EmployerDescrip like '" + strData + "%' " & vbCrLf)
            End If

            If Not CampusId Is Nothing AndAlso CampusId <> "" Then
                sb.AppendFormat(" AND CampGrpId in (select CampGrpId from syCmpGrpCmps where CampusId='{0}')", CampusId)
            End If
            sb.Append("ORDER BY EmployerDescrip" & vbCrLf)
            'Execute the query a
            Dim ds As DataSet = db.RunSQLDataSet(sb.ToString)
            Return ds
        End Function
    End Class
#End Region

#Region "Input Masks"
    Public Class InputMasksFacade

        Public Enum InputMaskItem
            SSN = 1
            Phone = 2
            Zip = 3
        End Enum

        Public Function GetInputMasks() As DataTable
            Dim dbInputMasks As New InputMasksDB

            Return dbInputMasks.GetInputMasks
        End Function

        Public Function GetInputMaskForItem(ByVal itemId As InputMaskItem) As String
            Dim dbInputMasks As New InputMasksDB

            Return dbInputMasks.GetInputMaskForItem(itemId)
        End Function

        Public Sub UpdateInputMask(ByVal inputMaskId As Integer, ByVal mask As String)
            Dim dbInputMasks As New InputMasksDB

            dbInputMasks.UpdateInputMask(inputMaskId, mask)
        End Sub

        Public Function UpdateInputMaskInfo(ByVal iMaskInfo As InputMaskInfo, ByVal user As String) As String
            Dim dbInputMasks As New InputMasksDB

            Return dbInputMasks.UpdateInputMaskInfo(iMaskInfo, user)
        End Function

        Public Function UpdateInputMaskDS(ByVal ds As DataSet) As String
            Dim dbInputMasks As New InputMasksDB

            Return dbInputMasks.UpdateInputMaskDS(ds)
        End Function

        Public Function ApplyMask(ByVal strMask As String, ByVal strVal As String) As String
            Dim arrMask As New ArrayList
            Dim arrVal As New ArrayList
            Dim chr As Char
            'Dim strMaskChars As String = "#&?AULH\"
            Dim strMaskChars As String = "#"
            Dim intCounter As Integer
            Dim intCounter2 As Integer
            Dim strCorrVal As String
            Dim strReturn As String = ""
            Dim strPrev As String = ""

            'Add each character in strMask to arrMask
            'Modified by Balaji on 2/18/2005 (If statement added)
            If Not strMask = "" Then
                For Each chr In strMask
                    arrMask.Add(chr.ToString)
                Next
            End If

            'Add each character in strVal to arrVal
            For Each chr In strVal
                arrVal.Add(chr)
            Next

            If arrVal.Count >= 1 Then
                'We need to loop through the arrMask ArrayList and see if each item is a
                'mask character. If it is, we can use intCounter to get the corresponding
                'value from the arrVal ArrayList. Note that we ignore the \ character unless
                'it was preceded by another \. This means that at the end if we have \\ it
                'should be replaced by a \ and if we have a \ then it should be replaced
                'with an empty space.
                For intCounter2 = 0 To arrMask.Count - 1
                    If arrMask(intCounter2).ToString() = "\" And strPrev <> "\" Then
                        'ignore
                    ElseIf strMaskChars.IndexOf(arrMask(intCounter2).ToString()) <> -1 Then
                        strCorrVal = arrVal(intCounter).ToString()
                        arrMask(intCounter2) = strCorrVal
                        intCounter += 1
                    End If
                    strPrev = arrMask(intCounter2).ToString()
                Next

                Dim strChars(arrMask.Count) As String
                arrMask.CopyTo(strChars)
                strReturn = String.Join("", strChars)


                If strReturn.IndexOf("\\") <> -1 Then
                    strReturn = strReturn.Replace("\\", "\")

                ElseIf strReturn.IndexOf("\") <> -1 Then
                    strReturn = strReturn.Replace("\", "")
                Else
                    Return strReturn
                End If

                Return strReturn
            Else
                Return ""
            End If
        End Function

        Public Function RemoveMask(ByVal strMask As String, ByVal strVal As String) As String
            Dim arrMask As New ArrayList
            Dim arrVal As New ArrayList
            Dim arrRemovedVals As New ArrayList
            Dim chr As Char
            'Dim strMaskChars As String = "#&?A"
            Dim strMaskChars As String = "#"
            Dim intCounter As Integer
            Dim strCorrVal As String

            'Add each character in strMask to arrMask
            For Each chr In strMask
                arrMask.Add(chr.ToString)
            Next

            'Add each character in strVal to arrVal
            For Each chr In strVal
                arrVal.Add(chr)
            Next

            'We need to loop through the arrMask ArrayList and see if each item is a
            'mask character. If it is, we can use intCounter to get the corresponding
            'value from the arrVal ArrayList. 
            For intCounter = 0 To arrMask.Count - 1
                If strMaskChars.IndexOf(arrMask(intCounter).ToString()) <> -1 Then
                    strCorrVal = arrVal(intCounter).ToString()
                    arrRemovedVals.Add(strCorrVal)
                End If

            Next
            Dim strChars(arrRemovedVals.Count) As String
            arrRemovedVals.CopyTo(strChars)
            Return String.Join("", strChars)
        End Function

        Public Function GetRegEx(ByVal strMask As String) As String

            Dim arrMask As New ArrayList
            Dim chr As Char
            'Dim strMaskChars As String = "#&?A"
            Dim strMaskChars As String = "#"
            Dim intCounter As Integer

            'Add each character in strMask to arrMask
            For Each chr In strMask
                arrMask.Add(chr.ToString)
            Next

            'We need to loop through the arrMask ArrayList and see if each item is a
            'mask character. If it is we will replace it with the equivalent
            'RegularExpressionValidator character. If it is not then it is a literal and we need
            'to escape it with a \. For example if we have (305)-789-9500 then the first character
            'should be recorded as \(.
            For intCounter = 0 To arrMask.Count - 1
                If strMaskChars.IndexOf(arrMask(intCounter).ToString()) <> -1 Then
                    arrMask(intCounter) = GetValidationChar(arrMask(intCounter).ToString())
                Else
                    arrMask(intCounter) = "\" & arrMask(intCounter)
                End If
            Next
            Dim strChars(arrMask.Count) As String
            arrMask.CopyTo(strChars)
            Return String.Join("", strChars)
        End Function

        Private Function GetValidationChar(ByVal chr As Char) As String

            Select Case chr
                Case "#"
                    Return "\d"
                    'Case "&"
                    'Return "\s"
                    'Case "?"
                    'Return "\D"
                    'Case "A"
                    'Return "\w"
            End Select
            Return Nothing
        End Function

        Public Function ValidateStringWithInputMask(ByVal strMask As String, ByVal strVal As String) As Boolean
            Dim objRegEx As Regex
            Dim regularExpression As String

            'If the mask and the string value are not the same length then we should return false.
            'This check is important because the IsMatch method of the Regex class only searches the
            'input string for a match. This means we could have a regular expression such as "\d{5}
            'but a string such "123456" will still return true because the pattern of five (5) 
            'consecutive digits is within the string.
            If strMask.Length <> strVal.Length Then
                Return False
            End If

            regularExpression = GetRegEx(strMask)
            objRegEx = New Regex(regularExpression)
            If objRegEx.IsMatch(strVal) Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function CanZipMaskBeChanged(ByVal newZipMask As String) As Boolean
            'The zip mask cannot be changed if there are existing records (none international) that have
            'a different number of digits from the proposed new mask. For example, if there are existing records
            'with 5 digits then we should not allow the mask to be changed to 10 digits.
            Dim dbInputMasks As New InputMasksDB
            Dim zipLenToCheck As Integer
            Dim numFound As Integer
            Dim numPoundSymbol As Integer

            'Find out how many # symbols are in the zip mask passed in
            numPoundSymbol = GetNumberOfOccurencesOfCharInString(newZipMask, "#")
            If numPoundSymbol = 5 Then
                zipLenToCheck = 10
            ElseIf numPoundSymbol = 10 Then
                zipLenToCheck = 5
            End If

            'We are going to check each table that uses the zip field to see if there are any possible conflict.
            'As soon as we find a table that has a potential problem we will return false

            'adLeads
            numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("adLeads", zipLenToCheck)
            If numFound > 0 Then Return False

            'adColleges
            'numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("adColleges", zipLenToCheck)
            'If numFound > 0 Then Return False

            'syInstitutions
            'numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("syInstitutions", zipLenToCheck)
            'If numFound > 0 Then Return False

            'arStudAddresses
            numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("arStudAddresses", zipLenToCheck)
            If numFound > 0 Then Return False

            'plEmployers
            numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("plEmployers", zipLenToCheck)
            If numFound > 0 Then Return False

            'plEmployerContact
            numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("plEmployerContact", zipLenToCheck)
            If numFound > 0 Then Return False

            'faLenders
            numFound = dbInputMasks.GetCountOfRecordsWithSpecifiedZipLength("faLenders", zipLenToCheck)
            If numFound > 0 Then Return False


            'If we get to this point then we can return true
            Return True

        End Function

        Public Function GetNumberOfOccurencesOfCharInString(ByVal stringToCheck As String, ByVal charToCheckFor As Char) As Integer
            Dim intCounter As Integer
            Dim arrCharsToCheck As New ArrayList
            Dim chr As Char
            Dim numFound As Integer

            For Each chr In stringToCheck
                arrCharsToCheck.Add(chr.ToString)
            Next

            numFound = 0
            For intCounter = 0 To arrCharsToCheck.Count - 1
                If arrCharsToCheck(intCounter) = charToCheckFor Then
                    numFound += 1
                End If
            Next

            Return numFound

        End Function
    End Class
#End Region

End Namespace
