Imports FAME.Advantage.Common
Public Class WeeklyAttendanceFacade
    Inherits BaseReportFacade

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim WeeklyAttendance As New WeeklyAttendanceDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by report.
        ' US2729 12/23/2011 Janet Robinson add method for single weekly attendance report
        If Not IsNothing(rptParamInfo.StudentId) Then
            ds = BuildReportSource(WeeklyAttendance.GetWeeklyAttendance_Single(rptParamInfo), WeeklyAttendance.StudentIdentifier, rptParamInfo)
        Else
            ds = BuildReportSource(WeeklyAttendance.GetWeeklyAttendance(rptParamInfo), WeeklyAttendance.StudentIdentifier, rptParamInfo)
        End If

        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function
#End Region

#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String, ByVal rptParamInfo As Common.ReportParamInfo) As DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String
        Dim oldCmpGrp As String = ""
        Dim oldCampus As String = ""
        Dim atttype As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim dt As DataTable
        'Get the mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'Use table to store Total Amounts per Campus Groups and Campuses.
                    dt = ds.Tables(0)
                    ''Added to find the attendance Unit Type
                    ds.Tables(0).Columns.Add(New DataColumn("UnitTypeID", System.Type.GetType("System.String")))

                    For Each dr As DataRow In ds.Tables(0).Rows


                        'Apply mask to SSN.
                        If StudentIdentifier = "SSN" Then
                            If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                                If dr("StudentIdentifier") <> "" Then
                                    Dim temp As String = dr("StudentIdentifier")
                                    dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                End If
                            End If
                        End If

                        atttype = dr("AttTypeID").ToString
                        If atttype = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.PresentAbsentGuid Then
                            dr("UnitTypeID") = "Days"
                        ElseIf atttype = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.MinutesGuid Then
                            dr("UnitTypeID") = "Min"
                        Else
                            If MyAdvAppSettings.AppSettings("TrackSapAttendance", rptParamInfo.CampusId).ToString.ToLower = "byday" Then
                                dr("UnitTypeID") = "Min"
                            Else
                                dr("UnitTypeID") = "Hrs"
                            End If
                        End If

                    Next
                    ds.Tables(0).Columns.Remove("AttTypeID")
                End If
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
