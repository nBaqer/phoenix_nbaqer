' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' FLFacade.vb
'
' 
' ===============================================================================
' Copyright (C) 2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
Imports System
Imports System.IO
Imports System.Net
Imports System.Data
Imports System.Data.OleDb
Imports FAME.AdvantageV1.DataAccess
Public Class FLFacade
    Public Function ProcessFLFile(ByRef clsflFile As FLFileInfo, ByRef myMsgCollection As FLCollectionMessageInfos, Optional ByVal SourceFolderLocation As String = "") As Boolean
        Dim strRecord As String = ""
        Dim intLoopCount As Integer = 0
        Dim intBytesRead As Integer = 0
        Dim Request As WebRequest
        Dim ResponseStream As Stream
        Dim Response As WebResponse
        Dim intOffSet As Integer
        Dim strSourcePath As String

        ProcessFLFile = False
        Try
            Request = WebRequest.Create(clsflFile.strFileName)
            Request.Headers.Add("Translate: f")
            Request.Credentials = CredentialCache.DefaultCredentials
            Response = CType(Request.GetResponse, WebResponse)
            ResponseStream = Response.GetResponseStream
            
            Dim uriSource As New Uri(clsflFile.strFileName)
            strSourcePath = uriSource.AbsolutePath
            
            Dim objIn As StreamReader
            If SourceFolderLocation = "" Then
                objIn = New StreamReader(strSourcePath, System.Text.Encoding.Default)
            Else
                objIn = New StreamReader(SourceFolderLocation, System.Text.Encoding.Default)
            End If
            Dim contents As String
            intLoopCount = 1
            Do
                Try
                    contents = objIn.ReadLine().Trim()
                Catch ex As Exception
                    contents = Nothing
                    Exit Do
                End Try
                Dim split As String() = System.Text.RegularExpressions.Regex.Split(contents, "\\s+", System.Text.RegularExpressions.RegexOptions.None)
                Dim s As String
                For Each s In split
                    strRecord = s
                    myMsgCollection.Add(clsflFile.FileId, clsflFile.strMsgType, strRecord, clsflFile.strFileName, intLoopCount)
                Next
                intLoopCount += 1
            Loop Until contents Is Nothing
            objIn.Close()
            'End If

            ResponseStream.Flush()
            ResponseStream.Close()
            Response.Close()
            ProcessFLFile = True
            'Catch e As ArgumentNullException
            '    Throw
            'Catch e As ArgumentOutOfRangeException
            '    Throw
            'Catch e As System.Exception
            '    Throw
        Catch ex As Exception

        End Try

    End Function
    Public Function ProcessFLMsgCollection(ByVal CxnString As String, ByRef myMsgCollection As FLCollectionMessageInfos, _
                                           Optional ByVal SourcePath As String = "", Optional ByVal TargetPath As String = "", _
                                           Optional ByVal FileName As String = "", Optional ByVal ExceptionPath As String = "", _
                                           Optional ByVal ExceptionGUID As String = "", _
                                           Optional ByVal SourceToTarget As String = "", _
                                           Optional ByVal Override As String = "", _
                                           Optional ByVal AwardCutOffDate As String = "", _
                                           Optional ByVal Datafromremotecomputer As String = "no", _
                                           Optional ByVal PrgVerId() As String = Nothing, _
                                           Optional ByVal SSN() As String = Nothing, _
                                           Optional ByVal FAID() As String = Nothing, _
                                           Optional ByVal Fund() As String = Nothing, _
                                           Optional ByVal decGrossAmount() As Decimal = Nothing, _
                                           Optional ByVal decLoanFees() As Decimal = Nothing, _
                                           Optional ByVal AwardYear() As String = Nothing, _
                                           Optional ByVal LoanBeginDate() As Date = Nothing, _
                                           Optional ByVal LoanEndDate() As Date = Nothing, _
                                           Optional ByVal HIDE() As String = Nothing,
                                           Optional ByVal ShowRemovedIsChecked As Boolean = False,
                                           Optional ByVal RECORDPOSITION() As String = Nothing) As String

        'Public Function ProcessFLMsgCollection(ByVal CxnString As String, ByRef myMsgCollection As FLCollectionMessageInfos, _
        '                                       Optional ByVal SourcePath As String = "", Optional ByVal TargetPath As String = "", _
        '                                       Optional ByVal FileName As String = "", Optional ByVal ExceptionPath As String = "", _
        '                                       Optional ByVal ExceptionGUID As String = "", _
        '                                       Optional ByVal SourceToTarget As String = "", _
        '                                       Optional ByVal Override As String = "", _
        '                                       Optional ByVal AwardCutOffDate As String = "", _
        '                                       Optional ByVal Datafromremotecomputer As String = "no", _
        '                                       Optional ByVal PrgVerId() As String = Nothing) As String
        Dim rowIndex As Integer
        Dim FLDX As New FLDataXfer
        Dim FLDA As New DataAccess.DataAccess
        Dim blResult As String
        Dim intStartIndex As Integer
        Dim strByPass As String
        Dim intDataExistsInException As Integer
        Dim db As DataAccess.DataAccess
        Dim rowCounterForException As Integer = 0


        'ProcessFLMsgCollection = False
        FLDA.OpenConnection()

        Dim groupTrans As OleDb.OleDbTransaction = FLDA.StartTransaction()
        'FLDA.ConnectionString = CxnString


        Try
            '' New Code Added By Vijay Ramteke On September 03, 2010
            If Not Override = "" And (InStr(FileName.ToUpper, "FAID") >= 1 Or InStr(FileName.ToUpper, "HEAD") >= 1) Then
                For rowIndex = 0 To PrgVerId.Count - 1 Step 1
                    Dim strType As String = myMsgCollection.Items(rowIndex).strMsgType
                    Dim strFileMessageType As String = "CLFA" & strType
                    Dim strDISBMessage As String = ""
                    Try
                        'If the school reprocesses the file, then look if there are record in
                        'syFameESPExceptionReport Table, if record exists then process only those
                        'records otherwise skip
                        If SourceToTarget = "exceptiontotarget" Then
                            'intDataExistsInException = FLDX.CheckDateExistsInExceptionReport(SSN(rowIndex), FAID(rowIndex), Fund(rowIndex), strFileMessageType)
                            intDataExistsInException = FLDX.RecordExistsInExceptionReport(strFileMessageType, myMsgCollection.Items(rowIndex).RecordPosition)
                            If intDataExistsInException = 0 Then
                                Exit Try
                            End If

                            ''Troy: 8/28/2011 We also need to exit the loop if we are not showing the hidden items and an item exists but is marked
                            ''to be hidden since that would not be in the grid.
                            If (intDataExistsInException > 0) And (ShowRemovedIsChecked = False) And (FLDX.RecordIsMarkedAsHidden(strFileMessageType, myMsgCollection.Items(rowIndex).RecordPosition)) Then
                                Exit Try
                            End If
                        End If

                        Dim strPrgVerId As String = ""
                        Dim strSSN As String = ""
                        Dim strFAID As String = ""
                        Dim strFund As String = ""
                        Dim GrossAmt As Decimal = 0
                        Dim LoanFees As Decimal = 0
                        Dim strAwardYear As String = ""
                        Dim dtLoanBeginDate, dtLoanEndDate As Date



                        If Not PrgVerId(rowIndex).ToString = "" Then
                            strPrgVerId = PrgVerId(rowIndex).ToString
                            strSSN = SSN(rowIndex).ToString
                            strFAID = FAID(rowIndex).ToString
                            strFund = Fund(rowIndex).ToString
                            GrossAmt = CType(decGrossAmount(rowIndex), Decimal)
                            strAwardYear = AwardYear(rowIndex).ToString
                            LoanFees = CType(decLoanFees(rowIndex), Decimal)
                            dtLoanBeginDate = LoanBeginDate(rowIndex)
                            dtLoanEndDate = LoanEndDate(rowIndex)
                        Else
                            strPrgVerId = ""
                            strSSN = SSN(rowIndex).ToString
                            strFAID = FAID(rowIndex).ToString
                            strFund = Fund(rowIndex).ToString
                            GrossAmt = CType(decGrossAmount(rowIndex), Decimal)
                            strAwardYear = AwardYear(rowIndex).ToString
                            LoanFees = CType(decLoanFees(rowIndex), Decimal)
                            dtLoanBeginDate = LoanBeginDate(rowIndex)
                            dtLoanEndDate = LoanEndDate(rowIndex)
                        End If

                        '' New Code Added By Vijay Ramteke On September 03, 2010
                        If InStr(FileName.ToUpper, "FAID") >= 1 Then
                            blResult = ReProcess_FAIDMessage(FLDX, FLDA, groupTrans, myMsgCollection.Items(rowIndex), strSSN, strFAID, strFund, GrossAmt, LoanFees, strAwardYear, FileName, ExceptionGUID, SourceToTarget, Override, strPrgVerId, dtLoanBeginDate, dtLoanEndDate)
                        ElseIf InStr(FileName.ToUpper, "HEAD") >= 1 Then
                            blResult = ReProcess_HEADMessage(FLDX, FLDA, groupTrans, myMsgCollection.Items(rowIndex), strSSN, strFAID, strFund, GrossAmt, LoanFees, strAwardYear, FileName, ExceptionGUID, SourceToTarget, Override, strPrgVerId, dtLoanBeginDate, dtLoanEndDate)
                        End If

                        If blResult = "exception" Then
                            strByPass = "exception"
                        End If
                    Catch ex As System.Exception
                        Return ex.Message
                    End Try
                Next
            Else
                For rowIndex = 0 To myMsgCollection.Count - 1 Step 1
                    If myMsgCollection.Items(rowIndex).strError = String.Empty And Not myMsgCollection.Items(rowIndex).blnIsInDB Then
                        Dim strType As String = myMsgCollection.Items(rowIndex).strMsgType
                        Dim strFileMessageType As String = "CLFA" & strType
                        Dim strDISBMessage As String = ""
                        Try
                            'If the school reprocesses the file, then look if there are record in
                            'syFameESPExceptionReport Table, if record exists then process only those
                            'records otherwise skip
                            If SourceToTarget = "exceptiontotarget" Then
                                'Exit Try if the record is not selected for reprocessing
                                If IsNothing(RECORDPOSITION) Then
                                    Exit Try
                                End If

                                If Not RECORDPOSITION Is Nothing And RECORDPOSITION.Length = 0 Then
                                    Exit Try
                                End If

                                If Not RECORDPOSITION Is Nothing And RECORDPOSITION.Length > 0 Then
                                    Array.Sort(RECORDPOSITION)
                                    Dim location As Integer
                                    location = Array.BinarySearch(RECORDPOSITION, myMsgCollection.Items(rowIndex).RecordPosition.ToString)
                                    If location < 0 Then
                                        Exit Try
                                    End If

                                    If myMsgCollection.Items(rowIndex).strMsgType = "DISB" Then
                                        strDISBMessage = FLDX.ReturnMessageForDISB(myMsgCollection.Items(rowIndex).strSSN, myMsgCollection.Items(rowIndex).strFAID, myMsgCollection.Items(rowIndex).strFund, strFileMessageType)
                                    End If
                                End If
                            End If

                            Dim strPrgVerId As String = ""

                            If Not PrgVerId Is Nothing Then
                                If PrgVerId.Length > 0 Then
                                    If SourceToTarget = "exceptiontotarget" Then
                                        'If we are reprocessing the number of records in the exceptions table for a file most likely will
                                        'be different from the number of records in the file. We cannot therefore use the rowIndex to find
                                        'the strPrgVerId in that case. 
                                        'We have to use the RECORDPOSITION array to find the index of the record that has the value of the
                                        'record position of the current record in the file.
                                        'Sort the array
                                        Array.Sort(RECORDPOSITION)
                                        Dim location As Integer
                                        location = Array.BinarySearch(RECORDPOSITION, myMsgCollection.Items(rowIndex).RecordPosition.ToString)
                                        If location >= 0 Then
                                            strPrgVerId = PrgVerId(location).ToString
                                        End If

                                    Else
                                        strPrgVerId = PrgVerId(rowIndex).ToString
                                    End If
                                End If
                                
                            End If

                            Select Case strType
                                Case "FAID"
                                    blResult = Process_FAIDMessage(FLDX, FLDA, groupTrans, myMsgCollection.Items(rowIndex), ExceptionGUID, SourceToTarget, Override, AwardCutOffDate, strPrgVerId)
                                Case "HEAD"
                                    blResult = Process_HEADMessage(FLDX, FLDA, groupTrans, myMsgCollection.Items(rowIndex), ExceptionGUID, SourceToTarget, Override, strPrgVerId)
                                Case "DISB"
                                    blResult = Process_DISBMessage(FLDX, FLDA, groupTrans, myMsgCollection.Items(rowIndex), ExceptionGUID, SourceToTarget, Override, strDISBMessage)
                                Case "CHNG"
                                    blResult = Process_CHNGMessage(FLDX, FLDA, groupTrans, myMsgCollection.Items(rowIndex), ExceptionGUID, SourceToTarget, Override)
                                Case "RCVD"
                                    ''blResult = Process_RCVDMessage(FLDX, FLDA, groupTrans, myMsgCollection.Items(rowIndex), ExceptionGUID, SourceToTarget, Override)
                                    blResult = Process_RCVDMessage_New(FLDX, FLDA, groupTrans, myMsgCollection.Items(rowIndex), ExceptionGUID, SourceToTarget, Override)
                                Case "REFU"
                                    blResult = Process_REFUMessage(FLDX, FLDA, groupTrans, myMsgCollection.Items(rowIndex), ExceptionGUID, SourceToTarget, Override)
                            End Select
                            'If Not blResult = "" And Not blResult = "exception" Then
                            '    'groupTrans.Rollback()
                            '    MoveFileToESPExceptionFolder(SourcePath, ExceptionPath, FileName)
                            '    Return blResult
                            '    Exit Function
                            'End If
                            If blResult = "exception" Then
                                strByPass = "exception"
                            End If
                        Catch ex As Exception
                            Return "ErrorStartingAtRecord:" + (rowIndex + 1).ToString

                            'If strType = "FAID" Then
                            '    Dim intStartIndex1 As Integer = InStrRev(myMsgCollection.Items(rowIndex).strFileNameSource, "/")
                            '    strFileNameOnly = myMsgCollection.Items(rowIndex).strFileNameSource.Substring(intStartIndex1)
                            '    Dim strExceptionMessage1 As String = FLDX.BuildExceptionReport( myMsgCollection.Items(rowIndex).strSSN, myMsgCollection.Items(rowIndex).strFAID, myMsgCollection.Items(rowIndex).strFund, myMsgCollection.Items(rowIndex), strFileNameOnly, myMsgCollection.Items(rowIndex).dateModDate, "Unable to create student award", ExceptionGUID)
                            '    Exit Try
                            'End If
                        End Try
                    End If
                Next
            End If

            ''New Code Added by Troy on 7/9/2011 to process the records that are marked to hide
            ''We want to update the records to hide them. 
            ''We will first update all records for the given filename to set hide to 0 if the user has checked
            'to see the removed items. This condition is important because we cannot set hide to 0 for everything
            'if the hidden are not being shown. The user might not have selected any of visible ones to hide but
            'there could be existing records that are hidden and should remain hidden.
            ''We will then update records with ExceptionReportIds to those in the HIDE array to 1.
            If ShowRemovedIsChecked Then
                FLDX.UpdateHiddenForFile(FileName, 0)
            End If

            If Not HIDE Is Nothing Then
                If HIDE.Length > 0 Then
                    'FLDX.UpdateHiddenForFile(FileName, 0)
                    For rowCount As Integer = 0 To HIDE.Length - 1
                        FLDX.UpdateHiddenByExceptionReportId(HIDE(rowCount), 1)
                    Next
                End If

            End If

            'ProcessFLMsgCollection = True        
            'If all the data are saved to database and if there are no issues
            If strByPass = "exception" And Not Trim(SourceToTarget) = "exceptiontotarget" Then
                'groupTrans.Rollback()

                Return "Please refer to FAME ESP Exception Report for details"
                Exit Function
            ElseIf strByPass = "exception" And Trim(SourceToTarget) = "exceptiontotarget" Then
                Return "Please refer to FAME ESP Exception Report for details"
                Exit Function
            End If
            If blResult = "" And Trim(SourceToTarget) = "sourcetotarget" Then
                MoveFileToESPArchiveFolder(SourcePath, TargetPath, FileName, Datafromremotecomputer)
                Return blResult
                Exit Function
            End If
            If blResult = "" And Trim(SourceToTarget) = "exceptiontotarget" Then
                'If we have records that are marked as hide for the file we cannot move it to the Archive folder.
                If FLDX.GetCountOfHiddenForFile(FileName) = 0 And Not IsNothing(RECORDPOSITION) Then
                    If RECORDPOSITION.Length > 0 Then
                        MoveFileToESPArchiveFolder(ExceptionPath, TargetPath, FileName, Datafromremotecomputer)
                        FLDX.DeleteFromExceptionReport(FileName)
                    End If
                End If
                Return blResult
                Exit Function
            End If
        Catch e As System.Exception
            'groupTrans.Rollback()
            'Return blResult
            If e.InnerException Is Nothing Then
                Return e.Message
            Else
                Return e.InnerException.Message.ToString
            End If

            'Throw
        Finally
            FLDA.CloseConnection()
        End Try
    End Function
    Private Sub RenameExistingFile(ByVal strTargetPathWithFile As String)
        Dim strRandomNumber As New Random
        Dim strTargetPathWithNewFile As String = strTargetPathWithFile + strRandomNumber.Next.ToString
        If File.Exists(strTargetPathWithFile) Then
            Dim fs As FileStream = New FileStream(strTargetPathWithFile, FileMode.Open)
            fs.Close()
            File.Move(strTargetPathWithFile, strTargetPathWithNewFile)
        End If
    End Sub
    Private Sub MoveFileToESPArchiveFolder(ByVal SourcePath As String, ByVal TargetPath As String, ByVal FileName As String, Optional ByVal Datafromremotecomputer As String = "no")
        Dim uriSource As New Uri(SourcePath.ToString.ToLower)
        Dim uriTarget As New Uri(TargetPath.ToString.ToLower)
        Dim strSourceFile As String
        Dim strTargetFile As String
        Dim strMessage As String = ""
        If Datafromremotecomputer = "yes" Then
            strSourceFile = SourcePath + "\" + FileName
            strTargetFile = TargetPath + "\" + FileName
        Else
            strSourceFile = uriSource.AbsolutePath + FileName
            strTargetFile = uriTarget.AbsolutePath + FileName
        End If
        'modified by balaji on 10/11/2007
        'based on input from patrick - files from Fame ESP will have an extension that is sequencial
        'in other words we won't have a case where there will be two HEAD Files (or any other file types)
        'with same extension, the extension will be .000,.001,.002
        'so when that is the case there is no need to rename the file.
        Try
            'RenameExistingFile(strTargetFile)
            'Directory.Move(strSourceFile, strTargetFile)
            File.Move(strSourceFile, strTargetFile)
        Catch e As System.Exception
        End Try
    End Sub
    Private Sub MoveFileToESPExceptionFolder(ByVal SourcePath As String, ByVal TargetPath As String, ByVal FileName As String, Optional ByVal DataFromRemoteComputer As String = "no")
        Dim uriSource As New Uri(SourcePath.ToString.ToLower)
        Dim uriTarget As New Uri(TargetPath.ToString.ToLower)
        Dim strSourceFile As String
        Dim strTargetFile As String
        Dim strMessage As String = ""
        If DataFromRemoteComputer = "yes" Then
            'Code commented out by Troy since the "\" character is being specified on the campus page
            'strSourceFile = SourcePath + "\" + FileName
            'strTargetFile = TargetPath + "\" + FileName
            strSourceFile = SourcePath + FileName
            strTargetFile = TargetPath + FileName
        Else
            strSourceFile = uriSource.AbsolutePath + FileName
            strTargetFile = uriTarget.AbsolutePath + FileName
        End If

        'modified by balaji on 10/11/2007
        'based on input from patrick - files from Fame ESP will have an extension that is sequencial
        'in other words we won't have a case where there will be two HEAD Files (or any other file types)
        'with same extension, the extension will be .000,.001,.002
        'so when that is the case there is no need to rename the file.
        Try
            'RenameExistingFile(strTargetFile)
            'Directory.Move(strSourceFile, strTargetFile)
            File.Move(strSourceFile, strTargetFile)
        Catch e As System.Exception
        End Try
    End Sub
    Public Function Process_FAIDMessage(ByRef FLDX As FLDataXfer, ByRef db As DataAccess.DataAccess, _
                                        ByRef groupTrans As OleDbTransaction, ByRef msgEntry As FLMessageInfo, _
                                        Optional ByVal ExceptionGUID As String = "", _
                                        Optional ByVal SourceToTarget As String = "", _
                                        Optional ByVal override As String = "", _
                                        Optional ByVal AwardCutOffDate As String = "", _
                                        Optional ByVal PrgVerId As String = "") As String
        Dim strStudentEnrollmentID As String = ""
        Dim strStudentAwardID As String = ""
        Dim strGetAward As String = ""
        Dim dr As DataRow
        Dim strStudentName As String = ""
        strStudentName = getStudentNameAndSSN(msgEntry.strSSN)

        Dim strExceptionMessage, strFileNameOnly, sMessage As String
        Dim intStartIndex As Integer = InStrRev(msgEntry.strFileNameSource, "/")
        strFileNameOnly = msgEntry.strFileNameSource.Substring(intStartIndex)

        If Not SourceToTarget = "exceptiontotarget" Then
            Dim strLoanBeginDate, strLoanEndDate As String
            If msgEntry.strFund = "02" Or msgEntry.strFund = "03" Or msgEntry.strFund = "04" Then
                strLoanBeginDate = "07/01/" + Mid(msgEntry.strAwdyr, 1, 4)
                strLoanEndDate = "06/30/" + Mid(msgEntry.strAwdyr, 6, 2)
            Else
                strLoanBeginDate = msgEntry.dateDate1
                strLoanEndDate = msgEntry.dateDate2
            End If

            Dim intCutOffDates As Integer
            Dim dtCuttOfDate As Date = CDate(AwardCutOffDate)
            Dim dtAwardStartDate As Date = CDate(strLoanBeginDate)
            intCutOffDates = DateDiff(DateInterval.Day, dtCuttOfDate, dtAwardStartDate)

            If intCutOffDates < 0 Then
                'If the Loan Begin Date falls before the award cut off date set in web.config,then ignore those records
                ' modified by balaji on 10/04/2007

                'Keep track of number of records ignored due to cut off date
                FLDX.UpdatePriorToCuttOffDateCount(ExceptionGUID)
                Return ""
                Exit Function
            End If
        End If

        If Trim(strStudentName) = "" Then
            msgEntry.Message = "Unable to find student with ssn:" & msgEntry.strSSN
            If SourceToTarget = "exceptiontotarget" Then
                FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
            Else
                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
            End If
            Return "exception"
            Exit Function
        End If
        Process_FAIDMessage = False
        Try
            Dim strType As String = msgEntry.strMsgType
            Dim strEnrollId As String = ""

            'DE7556:Unable to reprocess FAMELink exceptions. The If statement below was  If Not override = "" And SourceToTarget = "exceptiontotarget" Then. 
            'This created a problem when the user tried to reprocess a FAID record for a student with multiple enrollments. The code would go into the Else part
            'where no PrgVerId is being passed. As a result the code in FLDataXfer.vb GetStuEnrollIDbySSN would simply say that it could not create the award because the student
            'had multiple enrollments. I removed the Not.
            If override = "" And SourceToTarget = "exceptiontotarget" Then 'If user reprocess the faid award
                strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntry.strSSN, "FAID", PrgVerId, True)
            Else
                strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntry.strSSN, "FAID")
            End If

            If Not strEnrollId = "" And Len(strEnrollId) <> 36 Then
                msgEntry.Message = "Invalid StuEnrollId for student " & strStudentName & " : " & strEnrollId
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                Else
                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                End If
                Return "exception"
                Exit Function
            End If

            If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
                msgEntry.Message = strEnrollId
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                Else
                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                End If
                Return "exception"
                Exit Function
            End If
            If Not Trim(strEnrollId) = "" Then
                Try
                    ''If user chooses to override we need to replace Advantage data with Fame ESP Data 
                    ''based on FA_ID, Otherwise try creating a new student award
                    'If Not override = "" And SourceToTarget = "exceptiontotarget" Then
                    '    strGetAward = FLDX.Update_faStudentAwardsFromFAID(groupTrans, strStudentAwardID, strEnrollId, msgEntry, strStudentName, SourceToTarget, override)
                    '    If Not (strGetAward.Trim = "") Then
                    '        FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                    '        strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strGetAward, ExceptionGUID)
                    '        Return "exception"
                    '        Exit Function
                    '    Else
                    '        FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                    '        Dim intCountException As Integer = CheckDateExistsInExceptionReportByFileName(strFileNameOnly)
                    '        If intCountException >= 1 Then
                    '            Return "exception"
                    '        Else
                    '            Return ""
                    '        End If
                    '        Exit Function
                    '    End If
                    'End If

                    strGetAward = FLDX.Add_faStudentAwardsFromFAID(groupTrans, strStudentAwardID, strEnrollId, msgEntry, strStudentName, SourceToTarget)
                    If Not (strGetAward = "") Then
                        msgEntry.Message = strGetAward
                        If SourceToTarget = "exceptiontotarget" Then
                            FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                        Else
                            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                        End If
                        Return "exception"
                        Exit Function
                    Else
                        If SourceToTarget = "exceptiontotarget" Then
                            FLDX.DeleteExceptionRecord(db, strFileNameOnly, msgEntry)
                        End If
                        
                        Return ""

                        Exit Function
                    End If
                Catch ex As System.Exception
                    Return "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                    Exit Function
                End Try
            Else
                msgEntry.Message = "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                Else
                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                End If
                Return "exception"
                Exit Function
            End If
        Catch e As System.Exception
            Return "Unable to match financial aid data with student " & strStudentName & " enrollment records "
            Exit Function
        End Try
    End Function
    Public Function Process_HEADMessage(ByRef FLDX As FLDataXfer, ByRef db As DataAccess.DataAccess, _
                                        ByRef groupTrans As OleDbTransaction, ByRef msgEntry As FLMessageInfo, _
                                        Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", _
                                        Optional ByVal Override As String = "", _
                                        Optional ByVal PrgVerId As String = "",
                                        Optional ByVal msgFile As FLFileInfo = Nothing) As String
        Dim strStudentEnrollmentID As String = ""
        Dim strStudentAwardID As String = ""
        Dim strGetAward As String = ""
        Dim strStudentName As String = ""
        strStudentName = getStudentNameAndSSN(msgEntry.strSSN)

        Dim strExceptionMessage, strFileNameOnly1, sMessage As String
        Dim intStartIndex As Integer = InStrRev(msgEntry.strFileNameSource, "/")
        strFileNameOnly1 = msgEntry.strFileNameSource.Substring(intStartIndex)

        If Trim(strStudentName) = "" Then
            'sMessage = "Unable to find student with ssn:" & msgEntry.strSSN
            msgEntry.Message = "Unable to find student with ssn:" & msgEntry.strSSN
            If SourceToTarget = "exceptiontotarget" Then
                'FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly1)
                'Rather than deleting the record we should update the existing record with the latest message, moddate and moduser
                'We don't want to delete the record since it could now be marked as hidden. Also if we are dealing with invalid SSNs they
                'would not be deleted but the step that adds the record back would end up duplicating that record in the database.
                FLDX.UpdateExceptionRecord(db, strFileNameOnly1, msgEntry)
            Else
                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly1, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
            End If

            Return "exception"
            Exit Function
        End If

        Process_HEADMessage = False
        Try
            Dim strType As String = msgEntry.strMsgType
            Dim strEnrollId As String = ""

            If Not Override = "" And SourceToTarget = "exceptiontotarget" Then 'If user reprocess the faid award
                strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntry.strSSN, "HEAD", PrgVerId, True)
            Else
                ''New Code Added By Vijay Ramteke On August 24, 2010 For Mantis Id
                ''strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntry.strSSN, "HEAD")
                strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntry.strSSN, msgEntry, "HEAD")
                ''New Code Added By Vijay Ramteke On August 24, 2010 For Mantis Id
            End If

            If Not strEnrollId = "" And Len(strEnrollId) <> 36 Then
                msgEntry.Message = "Invalid StuEnrollId for student " & strStudentName & " : " & strEnrollId
                If SourceToTarget = "exceptiontotarget" Then
                    'FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly1)
                    FLDX.UpdateExceptionRecord(db, strFileNameOnly1, msgEntry)
                Else
                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly1, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                End If

                Return "exception"
                Exit Function
            End If


            If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
                msgEntry.Message = strEnrollId
                If SourceToTarget = "exceptiontotarget" Then
                    'FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly1)
                    FLDX.UpdateExceptionRecord(db, strFileNameOnly1, msgEntry)
                Else
                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly1, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                End If

                Return "exception"
                Exit Function
            End If
            If Not Trim(strEnrollId) = "" Then
                strGetAward = FLDX.Add_faStudentAwards(db, groupTrans, strStudentAwardID, strEnrollId, msgEntry, strStudentName, SourceToTarget)
                If Not (strGetAward = "") Then
                    'Return strGetAward
                    If SourceToTarget = "exceptiontotarget" Then
                        msgEntry.Message = strGetAward
                        FLDX.UpdateExceptionRecord(db, strFileNameOnly1, msgEntry)
                    Else
                        strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly1, msgEntry.dateModDate, strGetAward, ExceptionGUID, , , msgEntry.RecordPosition)
                    End If
                    Return "exception"
                    Exit Function
                Else
                    'If we are reprocessing and we get to this point it means that the record was now successfully processed so
                    'we can remove it from the exception table.
                    If SourceToTarget = "exceptiontotarget" Then
                        'FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly1)
                        FLDX.DeleteExceptionRecord(db, strFileNameOnly1, msgEntry)
                    End If
                    'Dim intCountException As Integer = CheckDateExistsInExceptionReportByFileName(strFileNameOnly1)
                    'If intCountException >= 1 Then
                    '    Return "exception"
                    'Else
                    '    Return ""
                    'End If
                    Return ""
                    Exit Function
                End If
            Else
                'Return "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                msgEntry.Message = "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.UpdateExceptionRecord(db, strFileNameOnly1, msgEntry)
                Else
                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly1, msgEntry.dateModDate, strGetAward, ExceptionGUID, , , msgEntry.RecordPosition)
                End If

                Return "exception"
                Exit Function
            End If
        Catch e As System.Exception
            Return "Unable to process HEAD message for student " & strStudentName
        End Try
    End Function
    Public Function Process_DISBMessage(ByRef FLDX As FLDataXfer, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDb.OleDbTransaction, ByRef msgEntry As FLMessageInfo, Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal override As String = "", Optional ByVal OverrideUpdateMessage As String = "") As String
        Dim strStudentEnrollmentID As String = ""
        Dim strStudentAwardID As String = ""
        Dim strAwardScheduleID As String = ""
        Dim strAwardMessage As String = ""
        Dim strAwardSchedule As String = ""
        Dim strStudentName As String = ""
        Dim strGetAwardNoFAID As String = ""
        strStudentName = getStudentNameAndSSN(msgEntry.strSSN)

        Dim strExceptionMessage, strFileNameOnly, sMessage As String
        Dim intStartIndex As Integer = InStrRev(msgEntry.strFileNameSource, "/")
        strFileNameOnly = msgEntry.strFileNameSource.Substring(intStartIndex)
        If Trim(strStudentName) = "" Then
            'sMessage = "Unable to find student with ssn:" & msgEntry.strSSN
            msgEntry.Message = "Unable to find student with ssn:" & msgEntry.strSSN
            If SourceToTarget = "exceptiontotarget" Then
                FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
            Else
                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
            End If

            Return "exception"
            Exit Function
        End If
        Try
            Dim strType As String = msgEntry.strMsgType
            Dim strEnrollId As String = ""
            Dim strReturnMessage As String = ""
            'strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntry.strSSN)
            strReturnMessage = FLDX.GetStudentAwardIDByFAID(db, strStudentAwardID, msgEntry.strFAID, strEnrollId, strStudentName, msgEntry.strFund) 'FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntry.strSSN)
            If Not Trim(strEnrollId) = "" Then
                strAwardMessage = FLDX.GetStudentAwardID(db, strStudentAwardID, strEnrollId, msgEntry.strFAID, strStudentName)
                If strAwardMessage = "" Then
                    strAwardSchedule = FLDX.Add_faAwardSchedule(db, groupTrans, strAwardScheduleID, strStudentAwardID, msgEntry, strStudentName, SourceToTarget, ExceptionGUID)
                    If Not strAwardSchedule = "" Then
                        msgEntry.Message = strAwardSchedule
                        Dim intPosition As Integer = 0
                        Dim intLookForColon As Integer = 0
                        Dim getAwardDate As String = ""
                        'Look for last colon in error message
                        intPosition = InStrRev(strAwardSchedule, ":")
                        If intPosition >= 1 Then
                            getAwardDate = Mid(strAwardSchedule, intPosition + 2)
                            getAwardDate = Replace(getAwardDate, ".", "")
                        End If

                        If SourceToTarget = "exceptiontotarget" Then
                            FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                        Else
                            'strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strAwardSchedule, ExceptionGUID, "", getAwardDate)
                            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , getAwardDate, msgEntry.RecordPosition)
                        End If

                        Return "exception"
                        Exit Function
                    Else
                        If SourceToTarget = "exceptiontotarget" Then
                            FLDX.DeleteExceptionRecord(db, strFileNameOnly, msgEntry)
                        End If
                        
                        Return ""

                        Exit Function
                    End If
                Else
                    'Get student award as there are no faid (manually added by school)
                    strGetAwardNoFAID = FLDX.GetStudentAwardIDRCVDForFAID(db, strStudentAwardID, strEnrollId, msgEntry.strFAID, strStudentName, msgEntry.strFund)
                    If Not (strGetAwardNoFAID = "") Then
                        'If there is no student award id found, then by pass the flow and store the record in the database
                        msgEntry.Message = strGetAwardNoFAID
                        If SourceToTarget = "exceptiontotarget" Then
                            FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                        Else
                            'strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strGetAwardNoFAID, ExceptionGUID)
                            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                        End If

                        Return "exception"
                        Exit Function
                    Else
                        strAwardSchedule = FLDX.Add_faAwardSchedule(db, groupTrans, strAwardScheduleID, strStudentAwardID, msgEntry, strStudentName)
                        If Not strAwardSchedule = "" Then
                            msgEntry.Message = strAwardSchedule
                            If SourceToTarget = "exceptiontotarget" Then
                                FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                            Else
                                'strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strAwardSchedule, ExceptionGUID)
                                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                            End If

                            Return "exception"
                            Exit Function
                        Else
                            If SourceToTarget = "exceptiontotarget" Then
                                FLDX.DeleteExceptionRecord(db, strFileNameOnly, msgEntry)
                            End If
                            
                            Return ""
                            Exit Function

                        End If
                    End If
                End If
            Else
                'Return "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                'sMessage = "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                msgEntry.Message = "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                Else
                    'strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, sMessage, ExceptionGUID)
                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                End If

                Return "exception"
                Exit Function
            End If
        Catch e As System.Exception
            Return "Unable to match financial aid data with student " & strStudentName & " enrollment records "
        End Try
    End Function
    Public Function Process_RCVDMessage(ByRef FLDX As FLDataXfer, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDb.OleDbTransaction, ByRef msgEntry As FLMessageInfo, Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal Override As String = "") As String
        Dim strStudentEnrollmentID As String = ""
        Dim strStudentAwardID As String = ""
        Dim strAwardScheduleID As String = ""
        Dim strTransactionID As String = ""
        Dim strPmtDisbRelID As String = ""
        Dim strAwardIdMessage As String = ""
        Dim strAwardScheduleMessage As String = ""
        Dim strTransactionMessage As String = ""
        Dim strPmtDisbRel As String = ""
        Dim saPaymentMessage As String = ""
        Dim strGetAwardNoFAID As String = ""


        ' Process_RCVDMessage = False
        Dim strStudentName As String = ""
        Dim strExceptionMessage, strFileNameOnly, sMessage As String
        Dim intStartIndex As Integer = InStrRev(msgEntry.strFileNameSource, "/")
        strFileNameOnly = msgEntry.strFileNameSource.Substring(intStartIndex)


        strStudentName = getStudentNameAndSSN(msgEntry.strSSN)
        If Trim(strStudentName) = "" Then
            'sMessage = "Unable to find student with ssn:" & msgEntry.strSSN
            msgEntry.Message = "Unable to find student with ssn:" & msgEntry.strSSN
            If SourceToTarget = "exceptiontotarget" Then
                FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
            Else
                'strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, sMessage, ExceptionGUID)
                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
            End If
            
            Return "exception"
            Exit Function

        End If

        Try
            Dim strType As String = msgEntry.strMsgType
            Dim strEnrollId As String = ""
            Dim strReturnMessage As String = ""

            strReturnMessage = FLDX.GetStudentAwardIDByFAID(db, strStudentAwardID, msgEntry.strFAID, strEnrollId, strStudentName, msgEntry.strFund) 'FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntry.strSSN)
            If Not (Trim(strEnrollId) = "") Then
                strAwardIdMessage = FLDX.GetStudentAwardID(db, strStudentAwardID, strEnrollId, msgEntry.strFAID, strStudentName)
                If Not (strAwardIdMessage = "") Then
                    'Get student award as there are no faid (manually added by school)
                    strGetAwardNoFAID = FLDX.GetStudentAwardIDRCVDForFAID(db, strStudentAwardID, strEnrollId, msgEntry.strFAID, strStudentName, msgEntry.strFund)
                    If Not (strGetAwardNoFAID = "") Then
                        'If there is no student award id found, then by pass the flow and store the record in the database
                        msgEntry.Message = strGetAwardNoFAID
                        If SourceToTarget = "exceptiontotarget" Then
                            FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                        Else
                            'strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strGetAwardNoFAID, ExceptionGUID)
                            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                        End If

                        Return "exception"
                        Exit Function
                    Else
                        'Check if Disbursement exists for the student award
                        Dim strDisbExists As String = ""
                        strDisbExists = FLDX.CheckIfDisbursementExists(strStudentAwardID, strStudentName)
                        If Not strDisbExists = "" Then
                            msgEntry.Message = strDisbExists
                            If SourceToTarget = "exceptiontotarget" Then
                                FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                            Else
                                'strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strDisbExists, ExceptionGUID)
                                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                            End If

                            Return "exception"
                            Exit Function
                        End If
                        'Check if payments have been posted for all disbursements for that award and amount
                        If Not SourceToTarget = "exceptiontotarget" Then
                            Dim strPaymentPosted As String = ""
                            strPaymentPosted = FLDX.CheckIfSchoolHasPostedAllPayments(strStudentAwardID, msgEntry, strStudentName)
                            If Not strPaymentPosted = "" Then
                                msgEntry.Message = strPaymentPosted
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                                Else
                                    'strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strPaymentPosted, ExceptionGUID)
                                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                                End If

                                Return "exception"
                                Exit Function
                            End If
                        End If

                        
                        strTransactionMessage = FLDX.Add_saTransaction(db, groupTrans, strTransactionID, strEnrollId, msgEntry, strStudentName, SourceToTarget)
                        If Not (strTransactionMessage = "") Then
                            msgEntry.Message = strTransactionMessage
                            If SourceToTarget = "exceptiontotarget" Then
                                FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                            Else
                                'strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strTransactionMessage, ExceptionGUID)
                                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                            End If

                            Return "exception"
                            Exit Function
                        Else
                            strPmtDisbRel = FLDX.Add_saPmtDisbRel(db, groupTrans, strPmtDisbRelID, strTransactionID, strStudentAwardID, msgEntry, strStudentName)
                            If Not (strPmtDisbRel = "") Then
                                'Return strPmtDisbRel
                                msgEntry.Message = strPmtDisbRel
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                                Else
                                    'strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strPmtDisbRel, ExceptionGUID)
                                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                                End If

                                Return "exception"
                                Exit Function
                            Else
                                saPaymentMessage = FLDX.Add_saPayments(db, groupTrans, strTransactionID, msgEntry, strStudentName, strStudentAwardID, strPmtDisbRelID)
                                If Not (saPaymentMessage = "") Then
                                    'Return saPaymentMessage
                                    msgEntry.Message = saPaymentMessage
                                    If SourceToTarget = "exceptiontotarget" Then
                                        FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                                    Else
                                        strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                                    End If

                                    Return "exception"
                                    Exit Function
                                Else
                                    If SourceToTarget = "exceptiontotarget" Then
                                        FLDX.DeleteExceptionRecord(db, strFileNameOnly, msgEntry)
                                    End If
                                    Process_RCVDMessage = True
                                    Return ""
                                    Exit Function
                                End If
                            End If
                        End If
                    End If
                Else
                    'Check if Disbursement exists for the student award
                    Dim strDisbExists As String = ""
                    strDisbExists = FLDX.CheckIfDisbursementExists(strStudentAwardID, strStudentName)
                    If Not strDisbExists = "" Then
                        msgEntry.Message = strDisbExists
                        If SourceToTarget = "exceptiontotarget" Then
                            FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                        Else
                            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                        End If

                        Return "exception"
                        Exit Function
                    End If
                    'Check if payments have been posted for all disbursements for that award and amount
                    If Not SourceToTarget = "exceptiontotarget" Then
                        Dim strPaymentPosted As String = ""
                        strPaymentPosted = FLDX.CheckIfSchoolHasPostedAllPayments(strStudentAwardID, msgEntry, strStudentName)
                        If Not strPaymentPosted = "" Then
                            msgEntry.Message = strPaymentPosted
                            If SourceToTarget = "exceptiontotarget" Then
                                FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                            Else
                                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                            End If
                            Return "exception"
                            Exit Function
                        End If
                    End If

                    strTransactionMessage = FLDX.Add_saTransaction(db, groupTrans, strTransactionID, strEnrollId, msgEntry, strStudentName, SourceToTarget)
                    If Not (strTransactionMessage = "") Then
                        msgEntry.Message = strTransactionMessage
                        If SourceToTarget = "exceptiontotarget" Then
                            FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                        Else
                            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                        End If
                        Return "exception"
                        Exit Function
                    Else
                        strPmtDisbRel = FLDX.Add_saPmtDisbRel(db, groupTrans, strPmtDisbRelID, strTransactionID, strStudentAwardID, msgEntry, strStudentName)
                        If Not (strPmtDisbRel = "") Then
                            'Return strPmtDisbRel
                            msgEntry.Message = strPmtDisbRel
                            If SourceToTarget = "exceptiontotarget" Then
                                FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                            Else
                                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                            End If
                            Return "exception"
                            Exit Function
                        Else
                            saPaymentMessage = FLDX.Add_saPayments(db, groupTrans, strTransactionID, msgEntry, strStudentName, strStudentAwardID)
                            If Not (saPaymentMessage = "") Then
                                msgEntry.Message = saPaymentMessage
                                'Return saPaymentMessage
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                                Else
                                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                                End If
                                Return "exception"
                                Exit Function
                            Else
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.DeleteExceptionRecord(db, strFileNameOnly, msgEntry)
                                End If
                                Process_RCVDMessage = True
                                Return ""

                                Exit Function
                            End If
                        End If
                    End If
                    'End If
                End If
            Else
                'Return "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                msgEntry.Message = "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                Else
                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                End If
                Return "exception"
                Exit Function
            End If
        Catch e As System.Exception
            Return e.Message
        End Try
    End Function
    Public Function Process_CHNGMessage(ByRef FLDX As FLDataXfer, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDbTransaction, ByRef msgEntry As FLMessageInfo, Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal Override As String = "") As String
        Dim strStudentEnrollmentID As String = ""
        Dim strStudentAwardID As String = ""
        Dim strAwardScheduleID As String = ""
        Dim strAwardIdMessage As String = ""
        Dim strAwardScheduleMessage As String = ""
        Dim strTransactionMessage As String = ""
        Dim strPmtDisbRel As String = ""
        Dim saPaymentMessage As String = ""
        Dim strUpdateMessage As String = ""
        Dim strInsertMessage As String = ""
        'Process_CHNGMessage = False
        Dim strStudentName As String = ""
        Dim strExceptionMessage, strFileNameOnly, sMessage As String
        Dim intStartIndex As Integer = InStrRev(msgEntry.strFileNameSource, "/")

        If intStartIndex = 0 Then
            intStartIndex = InStrRev(msgEntry.strFileNameSource, "\")
        End If

        strFileNameOnly = msgEntry.strFileNameSource.Substring(intStartIndex)

        strStudentName = getStudentNameAndSSN(msgEntry.strSSN)
        If Trim(strStudentName) = "" Then
            msgEntry.Message = "Unable to find student with ssn:" & msgEntry.strSSN
            If SourceToTarget = "exceptiontotarget" Then
                FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
            Else
                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
            End If
            Return "exception"
            Exit Function
        End If
        Try
            Dim strType As String = msgEntry.strMsgType
            Dim strEnrollId As String = ""
            strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntry.strSSN)
            If (Not Trim(strEnrollId) = "") Then
                strAwardIdMessage = FLDX.GetStudentAwardID(db, strStudentAwardID, strEnrollId, msgEntry.strFAID, strStudentName)
                If Not (strAwardIdMessage = "") Then
                    msgEntry.Message = strAwardIdMessage
                    If SourceToTarget = "exceptiontotarget" Then
                        FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                    Else
                        strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                    End If
                    Return "exception"
                    Exit Function
                Else
                    Select Case msgEntry.strChgCode
                        Case "C"
                            strUpdateMessage = FLDX.Update_faAwardSchedule(db, groupTrans, strStudentAwardID, msgEntry, strStudentName)
                            If Not (strUpdateMessage = "") Then
                                msgEntry.Message = strUpdateMessage
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                                Else
                                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                                End If
                                Return "exception"
                                Exit Function
                            Else
                                Process_CHNGMessage = True
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.DeleteExceptionRecord(db, strFileNameOnly, msgEntry)
                                End If
                                
                                Return ""

                                Exit Function
                            End If
                        Case "N"
                            strUpdateMessage = FLDX.Add_ChangefaAwardSchedule(db, groupTrans, strAwardScheduleID, strStudentAwardID, msgEntry, strStudentName)
                            If Not (strUpdateMessage = "") Then
                                msgEntry.Message = strUpdateMessage
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                                Else
                                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                                End If
                                Return "exception"
                                Exit Function
                            Else
                                Process_CHNGMessage = True
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.DeleteExceptionRecord(db, strFileNameOnly, msgEntry)
                                End If
                               
                                Return ""

                                Exit Function
                            End If
                        Case "D"
                            strUpdateMessage = FLDX.Delete_faAwardSchedule(db, groupTrans, strStudentAwardID, msgEntry, strStudentName)
                            If Not (strUpdateMessage = "") Then
                                msgEntry.Message = strUpdateMessage
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                                Else
                                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                                End If
                                Return "exception"
                                Exit Function
                            Else
                                Process_CHNGMessage = True
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.DeleteExceptionRecord(db, strFileNameOnly, msgEntry)
                                End If
                                
                                Return ""

                                Exit Function
                            End If
                    End Select
                End If
            Else
                msgEntry.Message = "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                Else
                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                End If
                Return "exception"
                Exit Function
            End If

            'Catch e As System.Exception
            '    Return e.Message
        Finally
        End Try
    End Function
    Public Function Process_REFUMessage(ByRef FLDX As FLDataXfer, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDbTransaction, ByRef msgEntry As FLMessageInfo, Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal Override As String = "") As String
        Dim strStudentEnrollmentID As String = ""
        Dim strStudentAwardID As String = ""
        Dim strAwardScheduleID As String = ""
        Dim strTransactionID As String = ""
        Dim strPmtDisbRelID As String = ""
        Dim strAwardIdMessage As String = ""
        Dim strAwardScheduleMessage As String = ""
        Dim strTransactionMessage As String = ""
        Dim strPmtDisbRel As String = ""
        Dim saPaymentMessage As String = ""
        Dim strUpdateMessage As String = ""
        Dim strInsertMessage As String = ""
        Dim strStudentName As String = ""
        Dim strGetAwardNoFAID As String = ""

        Dim strExceptionMessage, strFileNameOnly, sMessage As String
        Dim intStartIndex As Integer = InStrRev(msgEntry.strFileNameSource, "/")
        strFileNameOnly = msgEntry.strFileNameSource.Substring(intStartIndex)

        strStudentName = getStudentNameAndSSN(msgEntry.strSSN)
        If Trim(strStudentName) = "" Then
            msgEntry.Message = "Unable to find student with ssn:" & msgEntry.strSSN
            If SourceToTarget = "exceptiontotarget" Then
                FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
            Else
                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
            End If
            Return "exception"
            Exit Function
        End If
        Try
            Dim strType As String = msgEntry.strMsgType
            Dim strEnrollId As String = ""
            Dim strReturnMessage As String = ""
            strReturnMessage = FLDX.GetStudentAwardIDByFAID(db, strStudentAwardID, msgEntry.strFAID, strEnrollId, strStudentName, msgEntry.strFund) 'FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntry.strSSN) 
            If (Not Trim(strEnrollId) = "") Then
                strAwardIdMessage = FLDX.GetStudentAwardID(db, strStudentAwardID, strEnrollId, msgEntry.strFAID, strStudentName)
                If Not (strAwardIdMessage = "") Then
                    'Get student award as there are no faid (manually added by school)
                    strGetAwardNoFAID = FLDX.GetStudentAwardIDRCVDForFAID(db, strStudentAwardID, strEnrollId, msgEntry.strFAID, strStudentName, msgEntry.strFund)
                    If Not (strGetAwardNoFAID = "") Then
                        'If there is no student award id found, then by pass the flow and store the record in the database
                        msgEntry.Message = strGetAwardNoFAID
                        If SourceToTarget = "exceptiontotarget" Then
                            FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                        Else
                            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                        End If
                        Return "exception"
                        Exit Function
                    Else
                        strTransactionMessage = FLDX.Add_saTransaction(db, groupTrans, strTransactionID, strEnrollId, msgEntry, strStudentName)
                        If Not (strTransactionMessage = "") Then
                            msgEntry.Message = strTransactionMessage
                            If SourceToTarget = "exceptiontotarget" Then
                                FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                            Else
                                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                            End If
                            Return "exception"
                            Exit Function
                        Else
                            strUpdateMessage = FLDX.Add_saRefunds(db, groupTrans, strTransactionID, msgEntry, strStudentName)
                            If Not (strUpdateMessage = "") Then
                                msgEntry.Message = strUpdateMessage
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                                Else
                                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                                End If
                                Return "exception"
                                Exit Function
                            Else
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.DeleteExceptionRecord(db, strFileNameOnly, msgEntry)
                                End If
                                Return ""
                                Exit Function
                            End If
                           
                        End If
                    End If
                Else
                    
                    strTransactionMessage = FLDX.Add_saTransaction(db, groupTrans, strTransactionID, strEnrollId, msgEntry, strStudentName)
                    If Not (strTransactionMessage = "") Then
                        msgEntry.Message = strTransactionMessage
                        If SourceToTarget = "exceptiontotarget" Then
                            FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                        Else
                            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                        End If
                        Return "exception"
                        Exit Function
                    Else
                        strUpdateMessage = FLDX.Add_saRefunds(db, groupTrans, strTransactionID, msgEntry, strStudentName)
                        If Not (strUpdateMessage = "") Then
                            msgEntry.Message = strUpdateMessage
                            If SourceToTarget = "exceptiontotarget" Then
                                FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                            Else
                                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                            End If
                            Return "exception"
                            Exit Function
                        Else
                            If SourceToTarget = "exceptiontotarget" Then
                                FLDX.DeleteExceptionRecord(db, strFileNameOnly, msgEntry)
                            End If
                            
                            Return ""

                            Exit Function
                        End If

                        
                    End If
                    'End If
                End If
            Else
                msgEntry.Message = "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.UpdateExceptionRecord(db, strFileNameOnly, msgEntry)
                Else
                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, msgEntry.Message, ExceptionGUID, , , msgEntry.RecordPosition)
                End If
                Return "exception"
                Exit Function
            End If
        Catch e As System.Exception
            Return e.Message
        End Try
    End Function
    Public Function getStudentNameAndSSN(ByVal SSN As String) As String
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        Dim zipMask As String
        Dim strMaskSSN As String
        Dim dr As DataRow
        Dim strStudentName As String = (New FLDataXfer).GetStudentInfo(SSN)
        If InStr(strStudentName, "Student not found") >= 1 Then
            Return ""
            Exit Function
        End If
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        If SSN.ToString.Length >= 1 Then
            Return strStudentName & " (" + SSN & ")"
        Else
            Return ""
        End If
    End Function
    Public Function getExceptionReport(Optional ByVal ExceptionGUID As String = "") As DataSet
        Dim ds As New DataSet
        ds = (New FLDataXfer).getExceptionReport(ExceptionGUID)
        Return ds
    End Function
    'Public Function getExceptionReportAfterOverload(Optional ByVal ExceptionGUID As String = "", Optional ByVal FileName As String = "", Optional ByVal SSN As String = "", Optional ByVal Fund As String = "", Optional ByVal sortby As String = "") As DataSet
    '    Dim ds As New DataSet
    '    ds = (New FLDataXfer).getExceptionReportAfterOverload(ExceptionGUID, FileName, SSN, Fund, sortby)
    '    Return ds
    'End Function
    Public Function getExceptionReportAndSort(Optional ByVal SortBy As String = "") As DataSet
        Dim ds As New DataSet
        ds = (New FLDataXfer).getExceptionReportAndSort(SortBy)
        Return ds
    End Function
    Public Function getExceptionReportByFileName(Optional ByVal FileName As String = "", Optional ByVal sortby As String = "") As DataSet
        Dim ds As New DataSet
        ds = (New FLDataXfer).getExceptionReportByFileName(FileName, sortby)
        Return ds
    End Function
    Public Function getExceptionReport(ByVal SSN As String, ByVal MessageType As String, ByVal strFileName As String, Optional ByVal SortBy As String = "", Optional ByVal FundSource As String = "", Optional ByVal ShowRemoved As Integer = 0, Optional ByVal CampusId As String = "") As DataSet
        Dim ds As New DataSet
        ds = (New FLDataXfer).getExceptionReport(SSN, MessageType, strFileName, SortBy, FundSource, ShowRemoved, CampusId)
        Return ds
    End Function
    Public Sub UpdateExceptionCount(ByVal FileName As String, ByVal Success As Integer, ByVal Failed As Integer)
        Dim DataTransfer As New FLDataXfer
        DataTransfer.updateExceptionCount(FileName, Success, Failed)
    End Sub
    Public Function CountByAwardDate(ByVal ExceptionGUID As String) As Integer
        Dim DataTransfer As New FLDataXfer
        Dim intRecWithAwardDate As Integer
        intRecWithAwardDate = DataTransfer.CountByAwardDate(ExceptionGUID)
        Return intRecWithAwardDate
    End Function
    Public Function getFileNameWithStatus(ByVal FileName As String) As String
        Dim ds As New DataSet
        Dim dXfer As New FLDataXfer
        Dim strFileName As String
        ds = dXfer.getFileNameWithStatus(FileName)
        For Each dr As DataRow In ds.Tables(0).Rows
            strFileName = " (Last Imported on:" & CType(dr("ModDate"), Date).ToShortDateString & ") (records processed:" & CType(dr("Success"), Integer).ToString & ") (records failed:" & CType(dr("Failed"), Integer).ToString & ")"
        Next
        Return strFileName
    End Function
    Public Sub DeleteFromExceptionReport(ByVal FileName As String)
        Dim DataTransfer As New FLDataXfer
        DataTransfer.DeleteFromExceptionReport(FileName)
    End Sub
    Public Function CheckIfInitialImportSuccessful(ByVal FileName As String) As Integer
        Dim DataTransfer As New FLDataXfer
        Dim intImportSuccessful As Integer = DataTransfer.CheckIfInitialImportSuccessful(FileName)
        Return intImportSuccessful
    End Function
    Public Function CheckIfFileTypeExistsInException(ByVal FileType As String) As DataSet
        Dim ds As New DataSet
        ds = (New FLDataXfer).CheckIfFileTypeExistsInException(FileType)
        Return ds
    End Function
    Public Function getAllFilesNamesFromExceptionReport(Optional ByVal MessageType As String = "") As DataSet
        Dim ds As New DataSet
        ds = (New FLDataXfer).getAllFilesNamesFromExceptionReport(MessageType)
        Return ds
    End Function
    'Public Function CreateStudentAwards(ByVal SSN() As String, ByVal FAID() As String, ByVal Fund() As String, ByVal AwardYear() As String, ByVal LoanBeginDate() As String, ByVal LoanEndDate() As String, ByVal GrossAmount() As String, ByVal FileName As String)
    '    Dim strMessage As String
    '    strMessage = (New FLDataXfer).CreateStudentAwards(SSN, FAID, Fund, AwardYear, LoanBeginDate, LoanEndDate, GrossAmount, FileName)
    'End Function
    Public Function MarkEligibleRecordsToReprocess(ByVal FAID() As String, ByVal SSN() As String, ByVal FileName() As String, ByVal Fund() As String, ByVal selectedMessage() As String) As String
        Dim strMessage As String
        strMessage = (New FLDataXfer).MarkEligibleRecordsToReprocess(FAID, SSN, FileName, Fund, selectedMessage)
        Return strMessage
    End Function
    Public Function CheckDateExistsInExceptionReportByFileName(ByVal strMessageType As String) As Integer
        Dim intCount As Integer = 0
        intCount = (New FLDataXfer).CheckDateExistsInExceptionReportByFileName(strMessageType)
        Return intCount
    End Function
    Public Function MarkEligibleRecordsToDelete(ByVal FAID() As String, ByVal SSN() As String, ByVal FileName() As String, ByVal Fund() As String) As String
        Dim strMessage As String
        strMessage = (New FLDataXfer).MarkEligibleRecordsToDelete(FAID, SSN, FileName, Fund)
        Return strMessage
    End Function
    Public Function getFundSources() As DataSet
        Dim ds As New DataSet
        ds = (New FLDataXfer).getFundSources()
        Return ds
    End Function
    Public Function BuildArchiveReport(ByVal FileName As String, ByVal moduser As String) As String
        Dim strArchive As String = ""
        strArchive = (New FLDataXfer).BuildArchiveReport(FileName, moduser)
        Return strArchive
    End Function
    Public Function GetArchiveReportByFileName(ByVal FileName As String) As String
        Dim strArchive As String = ""
        strArchive = (New FLDataXfer).GetArchiveReportByFileName(FileName)
        Return strArchive
    End Function
    Public Function ProcessEdExpFile(ByRef clsflFile As FLFileInfo, ByRef myMsgCollection As FLCollectionMessageInfos, ByRef ds As DataSet, Optional ByVal SourceFolderLocation As String = "") As Boolean
        Dim strRecord As String = ""
        Dim strProcessType As String = ""
        Dim intLoopCount As Integer = 0
        Dim intBytesRead As Integer = 0
        Dim Request As WebRequest
        Dim ResponseStream As Stream
        Dim Response As WebResponse
        Dim myCredentials As NetworkCredential
        Dim intOffSet As Integer

        Dim dtMain = New DataTable("Main")
        Dim drMain As DataRow
        Dim dtChild1 As New DataTable("Child1")
        Dim drChild1 As DataRow
        Dim dtChild2 As New DataTable("Child2")
        Dim drChild2 As DataRow
        Dim urisource = New Uri(SourceFolderLocation)
        Dim strSourcePath As String = urisource.AbsolutePath
        Dim objIn As StreamReader

        ProcessEdExpFile = False
        Try
            If SourceFolderLocation.Contains("Pell") Then
                strProcessType = "Pell"
                dtMain.Columns.Add("ID", Type.GetType("System.Int32"))
                dtMain.Columns.Add("DataBaseIndicator", Type.GetType("System.String"))
                dtMain.Columns.Add("Filter", Type.GetType("System.String"))
                dtMain.Columns.Add("AwardAmtEY", Type.GetType("System.String"))
                dtMain.Columns.Add("AwardId", Type.GetType("System.String"))
                dtMain.Columns.Add("GrantType", Type.GetType("System.String"))
                dtMain.Columns.Add("PellAddDate", Type.GetType("System.String"))
                dtMain.Columns.Add("PellAddTime", Type.GetType("System.String"))
                dtMain.Columns.Add("PellOrgSSN", Type.GetType("System.String"))
                dtMain.Columns.Add("PellUpdateDate", Type.GetType("System.String"))
                dtMain.Columns.Add("PellUpdateTime", Type.GetType("System.String"))
                dtMain.Columns.Add("OriginationStatus", Type.GetType("System.String"))


                dtChild1.Columns.Add("ID", Type.GetType("System.Int32"))
                dtChild1.Columns.Add("DataBaseIndicator", Type.GetType("System.String"))
                dtChild1.Columns.Add("Filter", Type.GetType("System.String"))
                dtChild1.Columns.Add("AcceptedDisbAmt", Type.GetType("System.String"))
                dtChild1.Columns.Add("DisbDate", Type.GetType("System.String"))
                dtChild1.Columns.Add("DisbNum", Type.GetType("System.String"))
                dtChild1.Columns.Add("DisbRelInd", Type.GetType("System.String"))
                dtChild1.Columns.Add("DisbSeqNum", Type.GetType("System.String"))
                dtChild1.Columns.Add("SubmittedDisbAmt", Type.GetType("System.String"))
                dtChild1.Columns.Add("ActionStatusDisb", Type.GetType("System.String"))
            Else
                strProcessType = "DL"
                'For D
                dtMain.Columns.Add("ID", Type.GetType("System.Int32"))
                dtMain.Columns.Add("DataBaseIndicator", Type.GetType("System.String"))
                dtMain.Columns.Add("Filter", Type.GetType("System.String"))
                dtMain.Columns.Add("AddDate", Type.GetType("System.String"))
                dtMain.Columns.Add("AddTime", Type.GetType("System.String"))
                dtMain.Columns.Add("LoanAmtApproved", Type.GetType("System.String"))
                dtMain.Columns.Add("LoanFeePer", Type.GetType("System.String"))
                dtMain.Columns.Add("LoanId", Type.GetType("System.String"))
                dtMain.Columns.Add("LoanPeriodEndDate", Type.GetType("System.String"))
                dtMain.Columns.Add("LoanPeriodStartDate", Type.GetType("System.String"))
                dtMain.Columns.Add("LoanStatus", Type.GetType("System.String"))
                dtMain.Columns.Add("LoanType", Type.GetType("System.String"))
                dtMain.Columns.Add("MPNStatus", Type.GetType("System.String"))
                dtMain.Columns.Add("StuOrgSSN", Type.GetType("System.String"))
                dtMain.Columns.Add("UpdateDate", Type.GetType("System.String"))
                dtMain.Columns.Add("UpdateTime", Type.GetType("System.String"))

                'For N
                dtChild1.Columns.Add("ID", Type.GetType("System.Int32"))
                dtChild1.Columns.Add("DataBaseIndicator", Type.GetType("System.String"))
                dtChild1.Columns.Add("Filter", Type.GetType("System.String"))
                dtChild1.Columns.Add("AnticipatedDisbDate", Type.GetType("System.String"))
                dtChild1.Columns.Add("AnticipatedDisbFeeAmt", Type.GetType("System.String"))
                dtChild1.Columns.Add("AnticipatedDisbGrossAmt", Type.GetType("System.String"))
                dtChild1.Columns.Add("AnticipatedDisbIstRebateAmt", Type.GetType("System.String"))
                dtChild1.Columns.Add("AnticipatedDisbLoanId", Type.GetType("System.String"))
                dtChild1.Columns.Add("AnticipatedDisbNetAmt", Type.GetType("System.String"))
                dtChild1.Columns.Add("AnticipatedDisbNum", Type.GetType("System.String"))
                dtChild1.Columns.Add("DisbReleaseIndi", Type.GetType("System.String"))

                'For M
                dtChild2.Columns.Add("ID", Type.GetType("System.Int32"))
                dtChild2.Columns.Add("DataBaseIndicator", Type.GetType("System.String"))
                dtChild2.Columns.Add("Filter", Type.GetType("System.String"))
                dtChild2.Columns.Add("ActualDisbDate", Type.GetType("System.String"))
                dtChild2.Columns.Add("ActualDisbGrossAmt", Type.GetType("System.String"))
                dtChild2.Columns.Add("ActualInstRebateAmt", Type.GetType("System.String"))
                dtChild2.Columns.Add("ActualDisbLoanFeeAmt", Type.GetType("System.String"))
                dtChild2.Columns.Add("ActualDisbLoanAdjAmt", Type.GetType("System.String"))
                dtChild2.Columns.Add("ActualDisbNetAmt", Type.GetType("System.String"))
                dtChild2.Columns.Add("ActualDisbNum", Type.GetType("System.String"))
                dtChild2.Columns.Add("ActualDisbSeqNum", Type.GetType("System.String"))
                dtChild2.Columns.Add("ActualDisbStatus", Type.GetType("System.String"))
                dtChild2.Columns.Add("ActualDisbType", Type.GetType("System.String"))
                dtChild2.Columns.Add("LoanId", Type.GetType("System.String"))

            End If
            '' New Code

            Try
                Request = System.Net.FileWebRequest.Create(SourceFolderLocation)
                Request.Headers.Add("Translate: f")
                If clsflFile.datafromremotecomputer = True Then
                    myCredentials = New NetworkCredential("", clsflFile.networkuser, clsflFile.networkPassword)
                End If
                If clsflFile.datafromremotecomputer = True Then
                    Request.Credentials = myCredentials
                Else
                    Request.Credentials = CredentialCache.DefaultCredentials
                End If
                Response = CType(Request.GetResponse, WebResponse)
                ResponseStream = Response.GetResponseStream



                If SourceFolderLocation = "" Then
                    objIn = New StreamReader(ResponseStream, System.Text.Encoding.Default)
                Else
                    objIn = New StreamReader(ResponseStream, System.Text.Encoding.Default)
                End If
                Dim contents As String
                Dim ArrData() As String
                Dim chSplit As Char = ","
                Dim intRowID As Integer = 1
                'Dim intLoop As Integer = 1
                Do
                    Try
                        contents = objIn.ReadLine().Trim()
                    Catch ex As Exception
                        contents = Nothing
                        Exit Do
                    End Try
                    If contents.IndexOf(",") > -1 Then
                        ArrData = contents.Split(chSplit)
                    Else
                        ArrData = contents.Split(" ")
                    End If
                    ArrData = contents.Split(chSplit)
                    If strProcessType = "Pell" Then
                        'T - Pell, H - Teach
                        If (ArrData(0) = "T" Or ArrData(0) = "H") Then
                            drMain = dtMain.NewRow()
                            drMain("ID") = intRowID
                            drMain("DataBaseIndicator") = ArrData(0)
                            drMain("Filter") = ArrData(1)
                            drMain("AwardAmtEY") = ArrData(2)
                            drMain("AwardId") = ArrData(3)
                            drMain("GrantType") = ArrData(4)
                            drMain("PellAddDate") = ArrData(5)
                            drMain("PellAddTime") = ArrData(6)
                            drMain("PellOrgSSN") = ArrData(7)
                            drMain("PellUpdateDate") = ArrData(8)
                            drMain("PellUpdateTime") = ArrData(9)
                            drMain("OriginationStatus") = ArrData(10)
                            dtMain.Rows.Add(drMain)
                            intRowID += 1
                        Else
                            drChild1 = dtChild1.NewRow()
                            drChild1("ID") = intRowID - 1
                            drChild1("DataBaseIndicator") = ArrData(0)
                            drChild1("Filter") = ArrData(1)
                            drChild1("AcceptedDisbAmt") = ArrData(2)
                            drChild1("DisbDate") = ArrData(3)
                            drChild1("DisbNum") = ArrData(4)
                            drChild1("DisbRelInd") = ArrData(5)
                            drChild1("DisbSeqNum") = ArrData(6)
                            drChild1("SubmittedDisbAmt") = ArrData(7)
                            drChild1("ActionStatusDisb") = ArrData(8)
                            dtChild1.Rows.Add(drChild1)
                        End If
                    Else
                        ' Direct Loan
                        If (ArrData(0) = "D") Then
                            drMain = dtMain.NewRow()
                            drMain("ID") = intRowID
                            drMain("DataBaseIndicator") = ArrData(0)
                            drMain("Filter") = ArrData(1)
                            drMain("AddDate") = ArrData(2)
                            drMain("AddTime") = ArrData(3)
                            drMain("LoanAmtApproved") = ArrData(4)
                            drMain("LoanFeePer") = ArrData(5)
                            drMain("LoanId") = ArrData(6)
                            drMain("LoanPeriodEndDate") = ArrData(7)
                            drMain("LoanPeriodStartDate") = ArrData(8)
                            drMain("LoanStatus") = ArrData(9)
                            drMain("LoanType") = ArrData(10)
                            drMain("MPNStatus") = ArrData(11)
                            drMain("StuOrgSSN") = ArrData(12)
                            drMain("UpdateDate") = ArrData(13)
                            drMain("UpdateTime") = ArrData(14)
                            dtMain.Rows.Add(drMain)
                            intRowID += 1
                        ElseIf (ArrData(0) = "N") Then
                            If ArrData.Length > 1 Then
                                drChild1 = dtChild1.NewRow()
                                drChild1("ID") = intRowID - 1
                                drChild1("DataBaseIndicator") = ArrData(0)
                                drChild1("Filter") = ArrData(1)
                                drChild1("AnticipatedDisbDate") = ArrData(2)
                                drChild1("AnticipatedDisbFeeAmt") = ArrData(3)
                                drChild1("AnticipatedDisbGrossAmt") = ArrData(4)
                                drChild1("AnticipatedDisbIstRebateAmt") = ArrData(5)
                                drChild1("AnticipatedDisbLoanId") = ArrData(6)
                                drChild1("AnticipatedDisbNetAmt") = ArrData(7)
                                drChild1("AnticipatedDisbNum") = ArrData(8)
                                'drChild1("DisbReleaseIndi") = ArrData(9)
                                dtChild1.Rows.Add(drChild1)
                            End If
                        ElseIf (ArrData(0) = "M") Then
                            If ArrData.Length > 1 Then
                                drChild2 = dtChild2.NewRow()
                                drChild2("ID") = intRowID - 1
                                drChild2("DataBaseIndicator") = ArrData(0)
                                drChild2("Filter") = ArrData(1)
                                drChild2("ActualDisbDate") = ArrData(2)
                                drChild2("ActualDisbGrossAmt") = ArrData(3)
                                drChild2("ActualInstRebateAmt") = ArrData(4)
                                drChild2("ActualDisbLoanFeeAmt") = ArrData(5)
                                drChild2("ActualDisbLoanAdjAmt") = ArrData(6)
                                drChild2("ActualDisbNetAmt") = ArrData(7)
                                drChild2("ActualDisbNum") = ArrData(8)
                                drChild2("ActualDisbSeqNum") = ArrData(9)
                                drChild2("ActualDisbStatus") = ArrData(10)
                                drChild2("ActualDisbType") = ArrData(11)
                                drChild2("LoanId") = ArrData(12)
                                dtChild2.Rows.Add(drChild2)
                            End If
                        End If
                    End If

                    ArrData = Nothing
                Loop Until contents Is Nothing
                objIn.Close()
                'End If

                ResponseStream.Flush()
                ResponseStream.Close()
                Response.Close()

            Catch e As ArgumentNullException
                Throw
            Catch e As ArgumentOutOfRangeException
                Throw
            Catch e As System.Exception
                Throw
            End Try
            ProcessEdExpFile = ValidateDataSSN(dtMain, dtChild1, dtChild2, ds, strProcessType)

        Catch e As ArgumentNullException
            Throw
        Catch e As ArgumentOutOfRangeException
            Throw
        Catch e As System.Exception
            Throw
        End Try

    End Function
    Public Function ValidateDataSSN(ByRef dtMain As DataTable, ByRef dtChild1 As DataTable, ByRef dtChild2 As DataTable, ByRef ds As DataSet, ByVal ProcessType As String) As Boolean
        Dim strRecord As String = ""
        Dim intLoopCount As Integer = 0
        Dim intBytesRead As Integer = 0

        Dim drPATSData As DataRow
        Dim drPATSDisb As DataRow
        Dim drDirectLoan As DataRow
        Dim drDLSchDisb As DataRow
        Dim drDLActualDisb As DataRow
        Dim tempPATSRows() As DataRow
        Dim tempDLSchDisbRows() As DataRow
        Dim tempDLActualDisbRows() As DataRow
        Dim db As New DataAccess.DataAccess

        ValidateDataSSN = False
        Try
            If ProcessType = "Pell" Then
                For Each dr As DataRow In dtMain.Rows
                    Dim strSQL As String = "SELECT S.LastName, S.FirstName, S.SSN FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId Where S.SSN='" + dr("PELLORGSSN").ToString + "' AND SC.SysStatusId=9 "
                    Dim dsTemp As DataSet = db.RunSQLDataSet(strSQL, "StudentInfo")
                    If dsTemp.Tables("StudentInfo").Rows.Count > 0 Then
                        drPATSData = ds.Tables("PATSAward").NewRow()
                        drPATSData("LastName") = dsTemp.Tables("StudentInfo").Rows(0)("LastName")
                        drPATSData("FirstName") = dsTemp.Tables("StudentInfo").Rows(0)("FirstName")
                        drPATSData("SSN") = FormatSSN(dsTemp.Tables("StudentInfo").Rows(0)("SSN").ToString)
                        drPATSData("GrantType") = IIf(dr("GrantType") + "".ToString = "", "Pell", IIf(dr("GrantType") + "".ToString = "A", "ACG", ""))
                        drPATSData("AwardID") = dr("AwardId")
                        drPATSData("AddDate") = FormatDate(dr("PellAddDate").ToString)
                        drPATSData("AddTime") = FormatTime(dr("PellAddTime").ToString)
                        drPATSData("AwardAmount") = dr("AwardAmtEY")
                        ds.Tables("PATSAward").Rows.Add(drPATSData)
                        tempPATSRows = dtChild1.Select("ID = " + dr("ID").ToString)
                        For Each drS As DataRow In tempPATSRows
                            drPATSDisb = ds.Tables("PATSDisb").NewRow()
                            drPATSDisb("LastName") = dsTemp.Tables("StudentInfo").Rows(0)("LastName")
                            drPATSDisb("FirstName") = dsTemp.Tables("StudentInfo").Rows(0)("FirstName")
                            drPATSDisb("SSN") = FormatSSN(dsTemp.Tables("StudentInfo").Rows(0)("SSN").ToString)
                            drPATSDisb("AwardId") = dr("AwardId").ToString
                            drPATSDisb("DisbDate") = FormatDate(drS("DisbDate").ToString)
                            drPATSDisb("DisbRelInd") = drS("DisbRelInd")
                            drPATSDisb("ActionStatus") = drS("ActionStatusDisb")
                            drPATSDisb("DisbNum") = drS("DisbNum")
                            drPATSDisb("SubDisbAmt") = drS("SubmittedDisbAmt")
                            drPATSDisb("AccDisbAmt") = drS("AcceptedDisbAmt")
                            ds.Tables("PATSDisb").Rows.Add(drPATSDisb)
                        Next
                    End If
                Next
            Else
                'Direct Loan
                For Each dr As DataRow In dtMain.Rows
                    ''Dim strSQL As String = "SELECT S.LastName, S.FirstName, S.SSN FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId Where S.SSN='" + dr("PELLORGSSN").ToString + "' AND SC.SysStatusId=9 "
                    Dim strSQL As String = "SELECT S.LastName, S.FirstName, S.SSN FROM arStuEnrollments SE INNER JOIN arStudent S ON SE.StudentId = S.StudentId Where S.SSN='" + dr("StuOrgSSN").ToString + "'"
                    'LoadID join AwardID
                    Dim dsTemp As DataSet = db.RunSQLDataSet(strSQL, "StudentInfo")
                    If dsTemp.Tables("StudentInfo").Rows.Count > 0 Then
                        drDirectLoan = ds.Tables("DirectLoans").NewRow()
                        drDirectLoan("LastName") = dsTemp.Tables("StudentInfo").Rows(0)("LastName")
                        drDirectLoan("FirstName") = dsTemp.Tables("StudentInfo").Rows(0)("FirstName")
                        drDirectLoan("SSN") = dsTemp.Tables("StudentInfo").Rows(0)("SSN")
                        drDirectLoan("LoanType") = dr("LoanType")
                        drDirectLoan("AwardId") = ""
                        drDirectLoan("AddDate") = FormatDate(dr("AddDate").ToString)
                        drDirectLoan("AddTime") = FormatTime(dr("AddTime").ToString)
                        drDirectLoan("AwardAmount") = dr("LoanAmtApproved")
                        Dim LoanAmtApproved As Decimal
                        Dim FeesAmt As Decimal
                        Dim Fees As Decimal
                        Dim NetAmt As Decimal
                        Try
                            LoanAmtApproved = CType(dr("LoanAmtApproved").ToString, Decimal)
                        Catch ex As Exception
                            LoanAmtApproved = 0.0
                        End Try
                        Try
                            Fees = CType(dr("LoanFeePer").ToString, Decimal)
                        Catch ex As Exception
                            Fees = 0.0
                        End Try
                        Try
                            FeesAmt = (LoanAmtApproved * Fees) / 100
                        Catch ex As Exception
                            FeesAmt = 0.0
                        End Try
                        Try
                            NetAmt = LoanAmtApproved - FeesAmt
                        Catch ex As Exception
                            NetAmt = 0.0
                        End Try
                        drDirectLoan("Fees") = FeesAmt.ToString("0.00")
                        drDirectLoan("NetAmount") = NetAmt.ToString("0.00")
                        ds.Tables("DirectLoans").Rows.Add(drDirectLoan)

                        tempDLSchDisbRows = dtChild1.Select("ID = " + dr("ID").ToString)
                        tempDLActualDisbRows = dtChild2.Select("ID = " + dr("ID").ToString)
                        For Each drS As DataRow In tempDLSchDisbRows
                            drDLSchDisb = ds.Tables("DLSchDisb").NewRow()
                            drDLSchDisb("LastName") = dsTemp.Tables("StudentInfo").Rows(0)("LastName")
                            drDLSchDisb("FirstName") = dsTemp.Tables("StudentInfo").Rows(0)("FirstName")
                            drDLSchDisb("SSN") = FormatSSN(dsTemp.Tables("StudentInfo").Rows(0)("SSN").ToString)
                            drDLSchDisb("LoanId") = drS("AnticipatedDisbLoanId").ToString
                            drDLSchDisb("MPNStatus") = dr("MPNStatus").ToString
                            drDLSchDisb("DisbDate") = FormatDate(drS("AnticipatedDisbDate").ToString)
                            drDLSchDisb("DisbNum") = drS("AnticipatedDisbNum")
                            drDLSchDisb("GrossAmount") = drS("AnticipatedDisbGrossAmt")
                            drDLSchDisb("Fees") = drS("AnticipatedDisbFeeAmt")
                            drDLSchDisb("NetAmount") = drS("AnticipatedDisbNetAmt")
                            ds.Tables("DLSchDisb").Rows.Add(drDLSchDisb)
                        Next
                        For Each drS As DataRow In tempDLActualDisbRows
                            drDLActualDisb = ds.Tables("DLActualDisb").NewRow()
                            drDLActualDisb("LastName") = dsTemp.Tables("StudentInfo").Rows(0)("LastName")
                            drDLActualDisb("FirstName") = dsTemp.Tables("StudentInfo").Rows(0)("FirstName")
                            drDLActualDisb("SSN") = FormatSSN(dsTemp.Tables("StudentInfo").Rows(0)("SSN").ToString)
                            drDLActualDisb("LoanId") = drS("LoanId")
                            drDLActualDisb("DisbDate") = FormatDate(drS("ActualDisbDate").ToString)
                            drDLActualDisb("DisbStatus") = drS("ActualDisbStatus")
                            drDLActualDisb("DisbNum") = drS("ActualDisbNum")
                            drDLActualDisb("GrossAmount") = drS("ActualDisbGrossAmt")
                            drDLActualDisb("Fees") = drS("ActualDisbLoanFeeAmt")
                            drDLActualDisb("NetAmount") = drS("ActualDisbNetAmt")
                            ds.Tables("DLActualDisb").Rows.Add(drDLActualDisb)
                        Next
                    End If
                Next
            End If

            ValidateDataSSN = True
        Catch e As ArgumentNullException
            Throw
        Catch e As ArgumentOutOfRangeException
            Throw
        Catch e As System.Exception
            Throw
        End Try

    End Function
    Public Function FormatDate(ByRef DateToFormat As String) As String
        Dim strDD As String
        Dim strMM As String
        Dim strYYYY As String
        strYYYY = DateToFormat.Substring(0, 4)
        strMM = DateToFormat.Substring(4, 2)
        strDD = DateToFormat.Substring(6, 2)
        Return strMM + "/" + strDD + "/" + strYYYY
    End Function
    Public Function FormatTime(ByRef TimeToFormat As String) As String
        Dim strHH As String
        Dim strMM As String
        Dim strSS As String
        strHH = TimeToFormat.Substring(0, 2)
        strMM = TimeToFormat.Substring(2, 2)
        strSS = TimeToFormat.Substring(4, 2)
        Return strHH + ":" + strMM + ":" + strSS

    End Function
    Public Function FormatSSN(ByRef SSNToFormat As String) As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        Return facInputMasks.ApplyMask(strSSNMask, "*****" & SSNToFormat.Substring(5))
    End Function
    Public Function GetEnrollmentsDDLBySSN(ByVal SSN As String) As DataSet
        Return (New FLDataXfer).GetEnrollmentsDDLBySSN(SSN)
    End Function
    'Public Function ReProcess_HEADMessage(ByRef FLDX As FLDataXfer, ByRef db As DataAccess.DataAccess, _
    '                                 ByRef groupTrans As OleDbTransaction, ByRef msgEntry As FLMessageInfo, _
    '                                 ByVal strSSN As String, _
    '                                 ByVal strFAID As String, _
    '                                 ByVal strFund As String, _
    '                                 ByVal decGrossAmount As Decimal, _
    '                                 ByVal filename As String, _
    '                                 Optional ByVal ExceptionGUID As String = "", _
    '                                 Optional ByVal SourceToTarget As String = "", _
    '                                 Optional ByVal Override As String = "", _
    '                                 Optional ByVal PrgVerId As String = "") As String
    '    Dim strStudentEnrollmentID As String = ""
    '    Dim strStudentAwardID As String = ""
    '    Dim strGetAward As String = ""
    '    Dim strStudentName As String = ""
    '    strStudentName = getStudentNameAndSSN(strSSN)

    '    Dim strExceptionMessage, strFileNameOnly1, sMessage As String
    '    Dim intStartIndex As Integer = InStrRev(filename, "/")
    '    strFileNameOnly1 = filename.Substring(intStartIndex)

    '    If Trim(strStudentName) = "" Then
    '        sMessage = "Unable to find student with ssn:" & strSSN
    '        If SourceToTarget = "exceptiontotarget" Then
    '            FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
    '        End If
    '        strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, sMessage, ExceptionGUID)
    '        Return "exception"
    '        Exit Function
    '    End If
    '    Try
    '        Dim strType As String = "HEAD"
    '        Dim strEnrollId As String = ""
    '        If Not Override = "" And SourceToTarget = "exceptiontotarget" Then 'If user reprocess the faid award
    '            strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, strSSN, "HEAD", PrgVerId, True)
    '        Else
    '            strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, strSSN, "HEAD")
    '        End If
    '        If Not strEnrollId = "" And Len(strEnrollId) > 36 Then
    '            If SourceToTarget = "exceptiontotarget" Then
    '                FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
    '            End If
    '            strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, strEnrollId, ExceptionGUID)
    '            Return "exception"
    '            Exit Function
    '        End If


    '        If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
    '            If SourceToTarget = "exceptiontotarget" Then
    '                FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
    '            End If
    '            strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, strEnrollId, ExceptionGUID)
    '            Return "exception"
    '            Exit Function
    '        End If
    '        If Not Trim(strEnrollId) = "" Then
    '            'If user chooses to override we need to replace Advantage data with Fame ESP Data 
    '            'based on FA_ID, Otherwise try creating a new student award
    '            If Not Override = "" And SourceToTarget = "exceptiontotarget" Then
    '                strGetAward = FLDX.Update_faStudentAwards(db, groupTrans, strStudentAwardID, strEnrollId, msgEntry, strStudentName, SourceToTarget)
    '                If Not (strGetAward = "") Then
    '                    FLDX.DeleteRCVDFromExceptionReport(db, strSSN, strFAID, strFund, msgEntry, strFileNameOnly1)
    '                    strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, strGetAward, ExceptionGUID)
    '                    Return "exception"
    '                    Exit Function
    '                Else
    '                    FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
    '                    Dim intCountException As Integer = CheckDateExistsInExceptionReportByFileName(strFileNameOnly1)
    '                    If intCountException >= 1 Then
    '                        Return "exception"
    '                    Else
    '                        Return ""
    '                    End If
    '                    Exit Function
    '                End If
    '            End If
    '            strGetAward = FLDX.Add_faStudentAwards(db, groupTrans, strStudentAwardID, strEnrollId, msgEntry, strStudentName, SourceToTarget)
    '            If Not (strGetAward = "") Then
    '                'Return strGetAward
    '                If SourceToTarget = "exceptiontotarget" Then
    '                    FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
    '                End If
    '                strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, strGetAward, ExceptionGUID)
    '                Return "exception"
    '                Exit Function
    '            Else
    '                If SourceToTarget = "exceptiontotarget" Then
    '                    FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
    '                End If
    '                Dim intCountException As Integer = CheckDateExistsInExceptionReportByFileName(strFileNameOnly1)
    '                If intCountException >= 1 Then
    '                    Return "exception"
    '                Else
    '                    Return ""
    '                End If
    '                Exit Function
    '            End If
    '        Else
    '            'Return "Unable to match financial aid data with student " & strStudentName & " enrollment records "
    '            If SourceToTarget = "exceptiontotarget" Then
    '                FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)

    '            End If
    '            sMessage = "Unable to match financial aid data with student " & strStudentName & " enrollment records "
    '            strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, sMessage, ExceptionGUID)
    '            Return "exception"
    '            Exit Function
    '        End If
    '    Catch e As System.Exception
    '        Return "Unable to process HEAD message for student " & strStudentName
    '    End Try
    'End Function
    'Public Function ReProcess_FAIDMessage(ByRef FLDX As FLDataXfer, ByRef db As DataAccess.DataAccess, _
    '                                   ByRef groupTrans As OleDbTransaction, ByRef msgEntry As FLMessageInfo, _
    '                                   ByVal strSSN As String, _
    '                                     ByVal strFAID As String, _
    '                                     ByVal strFund As String, _
    '                                     ByVal decGrossAmount As Decimal, _
    '                                     ByVal filename As String, _
    '                                   Optional ByVal ExceptionGUID As String = "", _
    '                                   Optional ByVal SourceToTarget As String = "", _
    '                                   Optional ByVal override As String = "", _
    '                                   Optional ByVal AwardCutOffDate As String = "", _
    '                                   Optional ByVal PrgVerId As String = "") As String
    '    Dim strStudentEnrollmentID As String = ""
    '    Dim strStudentAwardID As String = ""
    '    Dim strGetAward As String = ""
    '    Dim dr As DataRow
    '    Dim strStudentName As String = ""
    '    strStudentName = getStudentNameAndSSN(strSSN)

    '    Dim strExceptionMessage, strFileNameOnly, sMessage As String
    '    Dim intStartIndex As Integer = InStrRev(filename, "/")
    '    strFileNameOnly = filename.Substring(intStartIndex)

    '    If Not SourceToTarget = "exceptiontotarget" Then
    '        Dim strLoanBeginDate, strLoanEndDate As String
    '        If strFund = "02" Or strFund = "03" Or strFund = "04" Then
    '            strLoanBeginDate = "07/01/" + Mid(strAwdyr, 1, 4)
    '            strLoanEndDate = "06/30/" + Mid(strAwdyr, 6, 2)
    '        Else
    '            strLoanBeginDate = dateDate1
    '            strLoanEndDate = dateDate2
    '        End If

    '        Dim intCutOffDates As Integer
    '        Dim dtCuttOfDate As Date = CDate(AwardCutOffDate)
    '        Dim dtAwardStartDate As Date = CDate(strLoanBeginDate)
    '        intCutOffDates = DateDiff(DateInterval.Day, dtCuttOfDate, dtAwardStartDate)

    '        If intCutOffDates < 0 Then
    '            'If the Loan Begin Date falls before the award cut off date set in web.config,then ignore those records
    '            ' modified by balaji on 10/04/2007

    '            'Keep track of number of records ignored due to cut off date
    '            FLDX.UpdatePriorToCuttOffDateCount(ExceptionGUID)
    '            Return ""
    '            Exit Function
    '        End If
    '    End If

    '    If Trim(strStudentName) = "" Then
    '        sMessage = "Unable to find student with ssn:" & strSSN
    '        If SourceToTarget = "exceptiontotarget" Then
    '            FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly)
    '        End If
    '        strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly, Date.Now.ToShortDateString, sMessage, ExceptionGUID)
    '        Return "exception"
    '        Exit Function
    '    End If
    '    Try
    '        Dim strType As String = "FAID"
    '        Dim strEnrollId As String = ""
    '        If Not override = "" And SourceToTarget = "exceptiontotarget" Then 'If user reprocess the faid award
    '            strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, strSSN, "FAID", PrgVerId, True)
    '        Else
    '            strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, strSSN, "FAID")
    '        End If

    '        If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
    '            If SourceToTarget = "exceptiontotarget" Then
    '                FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly)
    '            End If
    '            strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly, Date.Now.ToShortDateString, strEnrollId, ExceptionGUID)
    '            Return "exception"
    '            Exit Function
    '        End If
    '        If Not Trim(strEnrollId) = "" Then
    '            Try
    '                'If user chooses to override we need to replace Advantage data with Fame ESP Data 
    '                'based on FA_ID, Otherwise try creating a new student award
    '                If Not override = "" And SourceToTarget = "exceptiontotarget" Then
    '                    strGetAward = FLDX.Update_faStudentAwardsFromFAID(groupTrans, strStudentAwardID, strEnrollId, msgEntry, strStudentName, SourceToTarget, override)
    '                    If Not (strGetAward = "") Then
    '                        FLDX.DeleteRCVDFromExceptionReport(db, strSSN, strFAID, strFund, msgEntry, strFileNameOnly)
    '                        strExceptionMessage = FLDX.BuildExceptionReport(strSSN, strFAID, strFund, msgEntry, strFileNameOnly, Date.Now.ToShortDateString, strGetAward, ExceptionGUID)
    '                        Return "exception"
    '                        Exit Function
    '                    Else
    '                        FLDX.DeleteRCVDFromExceptionReport(db, strSSN, strFAID, strFund, msgEntry, strFileNameOnly)
    '                        Dim intCountException As Integer = CheckDateExistsInExceptionReportByFileName(strFileNameOnly)
    '                        If intCountException >= 1 Then
    '                            Return "exception"
    '                        Else
    '                            Return ""
    '                        End If
    '                        Exit Function
    '                    End If
    '                End If

    '                strGetAward = FLDX.Add_faStudentAwardsFromFAID(groupTrans, strStudentAwardID, strEnrollId, msgEntry, strStudentName, SourceToTarget)
    '                If Not (strGetAward = "") Then
    '                    If SourceToTarget = "exceptiontotarget" Then
    '                        FLDX.DeleteRCVDFromExceptionReport(db, strSSN, strFAID, strFund, msgEntry, strFileNameOnly)
    '                    End If
    '                    strExceptionMessage = FLDX.BuildExceptionReport(strSSN, strFAID, strFund, msgEntry, strFileNameOnly, Date.Now.ToShortDateString, strGetAward, ExceptionGUID)
    '                    Return "exception"
    '                    Exit Function
    '                Else
    '                    If SourceToTarget = "exceptiontotarget" Then
    '                        FLDX.DeleteRCVDFromExceptionReport(db, strSSN, strFAID, strFund, msgEntry, strFileNameOnly)
    '                    End If
    '                    Dim intCountException As Integer = CheckDateExistsInExceptionReportByFileName(strFileNameOnly)
    '                    If intCountException >= 1 Then
    '                        Return "exception"
    '                    Else
    '                        Return ""
    '                    End If
    '                    Exit Function
    '                End If
    '            Catch ex As System.Exception
    '                Return "Unable to match financial aid data with student " & strStudentName & " enrollment records "
    '                Exit Function
    '            End Try
    '        Else
    '            'Return "Unable to match financial aid data with student " & strStudentName & " enrollment records "
    '            'Exit Function
    '            sMessage = "Unable to match financial aid data with student " & strStudentName & " enrollment records "
    '            If SourceToTarget = "exceptiontotarget" Then
    '                FLDX.DeleteRCVDFromExceptionReport(db, strSSN, strFAID, strFund, msgEntry, strFileNameOnly)
    '            End If
    '            strExceptionMessage = FLDX.BuildExceptionReport(strSSN, strFAID, strFund, msgEntry, strFileNameOnly, Date.Now.ToShortDateString, sMessage, ExceptionGUID)
    '            Return "exception"
    '            Exit Function
    '        End If
    '    Catch e As System.Exception
    '        Return "Unable to match financial aid data with student " & strStudentName & " enrollment records "
    '        Exit Function
    '    End Try
    'End Function
    Public Function ReProcess_HEADMessage(ByRef FLDX As FLDataXfer, ByRef db As DataAccess.DataAccess, _
                                    ByRef groupTrans As OleDbTransaction, ByRef msgEntry As FLMessageInfo, _
                                    ByVal strSSN As String, _
                                    ByVal strFAID As String, _
                                    ByVal strFund As String, _
                                    ByVal decGrossAmount As Decimal, _
                                    ByVal decLoanFees As Decimal, _
                                    ByVal strAwardYear As String, _
                                    ByVal filename As String, _
                                    Optional ByVal ExceptionGUID As String = "", _
                                    Optional ByVal SourceToTarget As String = "", _
                                    Optional ByVal Override As String = "", _
                                    Optional ByVal PrgVerId As String = "", _
                                    Optional ByVal LoanBeginDate As Date = #1/1/1900#, _
                                    Optional ByVal LoanEndDate As Date = #1/1/1900#) As String
        Dim strStudentEnrollmentID As String = ""
        Dim strStudentAwardID As String = ""
        Dim strGetAward As String = ""
        Dim strStudentName As String = ""
        strStudentName = getStudentNameAndSSN(strSSN)

        Dim strExceptionMessage, strFileNameOnly1, sMessage As String
        Dim intStartIndex As Integer = InStrRev(filename, "/")
        strFileNameOnly1 = filename.Substring(intStartIndex)

        If Trim(strStudentName) = "" Then
            sMessage = "Unable to find student with ssn:" & strSSN
            If SourceToTarget = "exceptiontotarget" Then
                FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
            End If
            strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, sMessage, ExceptionGUID, , , decLoanFees, strAwardYear)
            Return "exception"
            Exit Function
        End If
        Try
            Dim strType As String = "HEAD"
            Dim strEnrollId As String = ""
            If Not Override = "" And SourceToTarget = "exceptiontotarget" Then 'If user reprocess the faid award
                strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, strSSN, "HEAD", PrgVerId, True)
            Else
                strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, strSSN, "HEAD")
            End If
            If Not strEnrollId = "" And Len(strEnrollId) > 36 Then
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
                End If
                strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, strEnrollId, ExceptionGUID, , , decLoanFees, strAwardYear)
                Return "exception"
                Exit Function
            End If
            If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
                End If
                strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, strEnrollId, ExceptionGUID, , , decLoanFees, strAwardYear)
                Return "exception"
                Exit Function
            End If
            If Not Trim(strEnrollId) = "" Then
                If Not Override = "" And SourceToTarget = "exceptiontotarget" Then
                    strGetAward = FLDX.Reprocess_Update_faStudentAwards(db, groupTrans, strStudentAwardID, strEnrollId, msgEntry, _
                                                                        strStudentName, SourceToTarget, _
                                                                        strSSN, strFAID, strFund, decGrossAmount, decLoanFees, strAwardYear, LoanBeginDate, LoanEndDate)
                    If Not (strGetAward = "") Then
                        FLDX.DeleteRCVDFromExceptionReport(db, strSSN, strFAID, strFund, msgEntry, strFileNameOnly1)
                        strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, strGetAward, ExceptionGUID, , , decLoanFees, strAwardYear)
                        Return "exception"
                        Exit Function
                    Else
                        FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
                        Dim intCountException As Integer = CheckDateExistsInExceptionReportByFileName(strFileNameOnly1)
                        If intCountException >= 1 Then
                            Return "exception"
                        Else
                            Return ""
                        End If
                        Exit Function
                    End If
                End If
            Else
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
                End If
                sMessage = "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, sMessage, ExceptionGUID, , , decLoanFees, strAwardYear)
                Return "exception"
                Exit Function
            End If
        Catch e As System.Exception
            Return "Unable to process HEAD message for student " & strStudentName
        End Try
    End Function
    Public Function ReProcess_FAIDMessage(ByRef FLDX As FLDataXfer, ByRef db As DataAccess.DataAccess, _
                                  ByRef groupTrans As OleDbTransaction, ByRef msgEntry As FLMessageInfo, _
                                  ByVal strSSN As String, _
                                  ByVal strFAID As String, _
                                  ByVal strFund As String, _
                                  ByVal decGrossAmount As Decimal, _
                                  ByVal decLoanFees As Decimal, _
                                  ByVal strAwardYear As String, _
                                  ByVal filename As String, _
                                  Optional ByVal ExceptionGUID As String = "", _
                                  Optional ByVal SourceToTarget As String = "", _
                                  Optional ByVal Override As String = "", _
                                  Optional ByVal PrgVerId As String = "", _
                                  Optional ByVal LoanBeginDate As Date = #1/1/1900#, _
                                  Optional ByVal LoanEndDate As Date = #1/1/1900#, _
                                  Optional ByVal AwardCutOffDate As String = "") As String
        Dim strStudentEnrollmentID As String = ""
        Dim strStudentAwardID As String = ""
        Dim strGetAward As String = ""
        Dim strStudentName As String = ""
        strStudentName = getStudentNameAndSSN(strSSN)

        Dim strExceptionMessage, strFileNameOnly1, sMessage As String
        Dim intStartIndex As Integer = InStrRev(filename, "/")
        strFileNameOnly1 = filename.Substring(intStartIndex)

        If Trim(strStudentName) = "" Then
            sMessage = "Unable to find student with ssn:" & strSSN
            If SourceToTarget = "exceptiontotarget" Then
                FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
            End If
            strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, sMessage, ExceptionGUID, , , decLoanFees, strAwardYear)
            Return "exception"
            Exit Function
        End If
        Try
            Dim strType As String = "FAID"
            Dim strEnrollId As String = ""
            If Not Override = "" And SourceToTarget = "exceptiontotarget" Then 'If user reprocess the faid award
                If Not PrgVerId = "" Then
                    strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, strSSN, "FAID", PrgVerId, True)
                Else
                    strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, strSSN, "FAID", True)
                End If
            Else
                strEnrollId = FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, strSSN, "FAID")
            End If
            If Not strEnrollId = "" And Len(strEnrollId) > 36 Then
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
                End If
                strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, strEnrollId, ExceptionGUID, , , decLoanFees, strAwardYear)
                Return "exception"
                Exit Function
            End If
            If Mid(Trim(strEnrollId), 1, 7) = "Student" Or Trim(strEnrollId) = "00000000-0000-0000-0000-000000000000" Then
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
                End If
                strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, strEnrollId, ExceptionGUID, , , decLoanFees, strAwardYear)
                Return "exception"
                Exit Function
            End If
            If Not Trim(strEnrollId) = "" Then
                If Not Override = "" And SourceToTarget = "exceptiontotarget" Then
                    strGetAward = FLDX.Reprocess_Update_faStudentAwards(db, groupTrans, strStudentAwardID, strEnrollId, msgEntry, _
                                                                        strStudentName, SourceToTarget, _
                                                                        strSSN, strFAID, strFund, decGrossAmount, decLoanFees, strAwardYear, LoanBeginDate, LoanEndDate)
                    If Not (strGetAward.Trim = "") Then
                        FLDX.DeleteRCVDFromExceptionReport(db, strSSN, strFAID, strFund, msgEntry, strFileNameOnly1)
                        strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, strGetAward, ExceptionGUID, , , decLoanFees, strAwardYear)
                        Return "exception"
                        Exit Function
                    Else
                        FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
                        Dim intCountException As Integer = CheckDateExistsInExceptionReportByFileName(strFileNameOnly1)
                        If intCountException >= 1 Then
                            Return "exception"
                        Else
                            Return ""
                        End If
                        Exit Function
                    End If
                End If
            Else
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.DeleteRCVDFromExceptionReport_Reprocess(db, strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1)
                End If
                sMessage = "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                strExceptionMessage = FLDX.BuildExceptionReport_Reprocess(strSSN, strFAID, strFund, decGrossAmount, strFileNameOnly1, Date.Now.ToShortDateString, sMessage, ExceptionGUID, , , decLoanFees, strAwardYear)
                Return "exception"
                Exit Function
            End If
        Catch e As System.Exception
            Return "Unable to process HEAD message for student " & strStudentName
        End Try
    End Function
    ''New Code Added By Vijay Ramteke On August 25, 2010 For Mantis Id 
    Public Function Process_RCVDMessage_New(ByRef FLDX As FLDataXfer, ByRef db As DataAccess.DataAccess, ByRef groupTrans As OleDb.OleDbTransaction, ByRef msgEntry As FLMessageInfo, Optional ByVal ExceptionGUID As String = "", Optional ByVal SourceToTarget As String = "", Optional ByVal Override As String = "") As String
        Dim strStudentEnrollmentID As String = ""
        Dim strStudentAwardID As String = ""
        Dim strAwardScheduleID As String = ""
        Dim strTransactionID As String = ""
        Dim strPmtDisbRelID As String = ""
        Dim strAwardIdMessage As String = ""
        Dim strAwardScheduleMessage As String = ""
        Dim strTransactionMessage As String = ""
        Dim strPmtDisbRel As String = ""
        Dim saPaymentMessage As String = ""
        Dim strGetAwardNoFAID As String = ""


        ''Process_RCVDMessage_New = False
        Dim strStudentName As String = ""
        Dim strExceptionMessage, strFileNameOnly, sMessage As String
        Dim intStartIndex As Integer = InStrRev(msgEntry.strFileNameSource, "\")
        strFileNameOnly = msgEntry.strFileNameSource.Substring(intStartIndex)


        strStudentName = getStudentNameAndSSN(msgEntry.strSSN)
        If Trim(strStudentName) = "" Then
            sMessage = "Unable to find student with ssn:" & msgEntry.strSSN
            If SourceToTarget = "exceptiontotarget" Then
                FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
            End If
            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, sMessage, ExceptionGUID, , , msgEntry.RecordPosition)
            Return "exception"
            Exit Function
        End If

        Try
            Dim strType As String = msgEntry.strMsgType
            Dim strEnrollId As String = ""
            Dim strReturnMessage As String = ""

            strReturnMessage = FLDX.GetStudentAwardIDByFAID(db, strStudentAwardID, msgEntry.strFAID, strEnrollId, strStudentName, msgEntry.strFund) 'FLDX.GetStuEnrollIDbySSN(db, strStudentEnrollmentID, msgEntry.strSSN)
            If Not (Trim(strEnrollId) = "") Then
                strAwardIdMessage = FLDX.GetStudentAwardID(db, strStudentAwardID, strEnrollId, msgEntry.strFAID, strStudentName)
                If Not (strAwardIdMessage = "") Then
                    'Get student award as there are no faid (manually added by school)
                    strGetAwardNoFAID = FLDX.GetStudentAwardIDRCVDForFAID(db, strStudentAwardID, strEnrollId, msgEntry.strFAID, strStudentName, msgEntry.strFund)
                    If Not (strGetAwardNoFAID = "") Then
                        'If there is no student award id found, then by pass the flow and store the record in the database
                        If SourceToTarget = "exceptiontotarget" Then
                            FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                        End If
                        strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strGetAwardNoFAID, ExceptionGUID, , , msgEntry.RecordPosition)
                        Return "exception"
                        Exit Function
                    Else
                        'Check if Disbursement exists for the student award
                        Dim strDisbExists As String = ""
                        strDisbExists = FLDX.CheckIfDisbursementExists(strStudentAwardID, strStudentName)
                        If Not strDisbExists = "" Then
                            If SourceToTarget = "exceptiontotarget" Then
                                FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                            End If
                            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strDisbExists, ExceptionGUID, , , msgEntry.RecordPosition)
                            Return "exception"
                            Exit Function
                        End If
                        'Check if payments have been posted for all disbursements for that award and amount
                        If Not SourceToTarget = "exceptiontotarget" Then
                            Dim strPaymentPosted As String = ""
                            strPaymentPosted = FLDX.CheckIfSchoolHasPostedAllPayments(strStudentAwardID, msgEntry, strStudentName)
                            If Not strPaymentPosted = "" Then
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                End If
                                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strPaymentPosted, ExceptionGUID, , , msgEntry.RecordPosition)
                                Return "exception"
                                Exit Function
                            End If
                        End If

                        'If user chooses to override we need to replace Advantage data with Fame ESP Data 
                        'based on FA_ID, Otherwise try creating a new student award
                        If Not Override = "" And SourceToTarget = "exceptiontotarget" Then
                            strTransactionMessage = FLDX.Update_saTransaction(db, groupTrans, strTransactionID, strEnrollId, msgEntry, strStudentName, SourceToTarget)
                            If Not (strTransactionMessage = "") Then
                                FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strTransactionMessage, ExceptionGUID, , , msgEntry.RecordPosition)
                                Return "exception"
                                Exit Function
                            Else
                                strPmtDisbRel = FLDX.Update_saPmtDisbRel(db, groupTrans, strPmtDisbRelID, strTransactionID, strStudentAwardID, msgEntry, strStudentName)
                                If Not (strPmtDisbRel = "") Then
                                    FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strPmtDisbRel, ExceptionGUID, , , msgEntry.RecordPosition)
                                    Return "exception"
                                    Exit Function
                                Else
                                    saPaymentMessage = FLDX.Update_saPayments(db, groupTrans, strTransactionID, msgEntry, strStudentName, strStudentAwardID, strPmtDisbRelID)
                                    If Not (saPaymentMessage = "") Then
                                        FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                        strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, saPaymentMessage, ExceptionGUID, , , msgEntry.RecordPosition)
                                        Return "exception"
                                        Exit Function
                                    Else
                                        FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                        Process_RCVDMessage_New = True
                                        Dim intCountException As Integer = CheckDateExistsInExceptionReportByFileName(strFileNameOnly)
                                        If intCountException >= 1 Then
                                            Return "exception"
                                        Else
                                            Return ""
                                        End If
                                        Exit Function
                                    End If
                                End If
                            End If
                        End If
                        '' New Code Added By Vijay Ramteke On August 25, 2010 For Mantis Id 

                        ''strTransactionMessage = FLDX.Add_saTransaction(db, groupTrans, strTransactionID, strEnrollId, msgEntry, strStudentName, SourceToTarget)
                        ''If Not (strTransactionMessage = "") Then
                        ''    If SourceToTarget = "exceptiontotarget" Then
                        ''        FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                        ''    End If
                        ''    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strTransactionMessage, ExceptionGUID)
                        ''    Return "exception"
                        ''    Exit Function
                        ''Else
                        ''    strPmtDisbRel = FLDX.Add_saPmtDisbRel(db, groupTrans, strPmtDisbRelID, strTransactionID, strStudentAwardID, msgEntry, strStudentName)
                        ''    If Not (strPmtDisbRel = "") Then
                        ''        'Return strPmtDisbRel
                        ''        If SourceToTarget = "exceptiontotarget" Then
                        ''            FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                        ''        End If
                        ''        strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strPmtDisbRel, ExceptionGUID)
                        ''        Return "exception"
                        ''        Exit Function
                        ''    Else
                        ''        saPaymentMessage = FLDX.Add_saPayments(db, groupTrans, strTransactionID, msgEntry, strStudentName, strStudentAwardID, strPmtDisbRelID)
                        ''        If Not (saPaymentMessage = "") Then
                        ''            'Return saPaymentMessage
                        ''            If SourceToTarget = "exceptiontotarget" Then
                        ''                FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                        ''            End If
                        ''            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, saPaymentMessage, ExceptionGUID)
                        ''            Return "exception"
                        ''            Exit Function
                        ''        Else
                        ''            If SourceToTarget = "exceptiontotarget" Then
                        ''                FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                        ''            End If
                        ''            Process_RCVDMessage_New = True
                        ''            Dim intCountException As Integer = CheckDateExistsInExceptionReportByFileName(strFileNameOnly)
                        ''            If intCountException >= 1 Then
                        ''                Return "exception"
                        ''            Else
                        ''                Return ""
                        ''            End If
                        ''            Exit Function
                        ''        End If
                        ''    End If
                        ''End If
                        Dim AwardScheduleId As String
                        AwardScheduleId = GetStudentAwardScheduleId(FLDX, db, msgEntry, strStudentAwardID)
                        If AwardScheduleId = "" Or AwardScheduleId = Guid.Empty.ToString Then
                            Dim strAwaedMessage = "Unable to apply payments  for student " & strStudentName & ", as no disbursement exists for this student award"
                            If SourceToTarget = "exceptiontotarget" Then
                                FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                            End If
                            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strTransactionMessage, ExceptionGUID, , , msgEntry.RecordPosition)
                            Return "exception"
                            Exit Function
                        Else
                            strTransactionMessage = FLDX.Add_saTransaction_New(db, groupTrans, strTransactionID, strEnrollId, msgEntry, strStudentName, SourceToTarget)
                            If Not (strTransactionMessage = "") Then
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                End If
                                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strTransactionMessage, ExceptionGUID, , , msgEntry.RecordPosition)
                                Return "exception"
                                Exit Function
                            Else
                                strPmtDisbRel = FLDX.Add_saPmtDisbRel_New(db, groupTrans, strPmtDisbRelID, strTransactionID, strStudentAwardID, AwardScheduleId, msgEntry, strStudentName)
                                If Not (strPmtDisbRel = "") Then
                                    'Return strPmtDisbRel
                                    If SourceToTarget = "exceptiontotarget" Then
                                        FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                    End If
                                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strPmtDisbRel, ExceptionGUID, , , msgEntry.RecordPosition)
                                    Return "exception"
                                    Exit Function
                                Else
                                    saPaymentMessage = FLDX.Add_saPayments_New(db, groupTrans, strTransactionID, msgEntry, strStudentName, strStudentAwardID, AwardScheduleId, strPmtDisbRelID)
                                    If Not (saPaymentMessage = "") Then
                                        'Return saPaymentMessage
                                        If SourceToTarget = "exceptiontotarget" Then
                                            FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                        End If
                                        strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, saPaymentMessage, ExceptionGUID, , , msgEntry.RecordPosition)
                                        Return "exception"
                                        Exit Function
                                    Else
                                        If SourceToTarget = "exceptiontotarget" Then
                                            FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                        End If
                                        Process_RCVDMessage_New = True
                                        Dim intCountException As Integer = CheckDateExistsInExceptionReportByFileName(strFileNameOnly)
                                        If intCountException >= 1 Then
                                            Return "exception"
                                        Else
                                            Return ""
                                        End If
                                        Exit Function
                                    End If
                                End If
                            End If
                        End If
                        '' New Code Added By Vijay Ramteke On August 25, 2010 For Mantis Id 
                    End If
                Else
                    'Check if Disbursement exists for the student award
                    Dim strDisbExists As String = ""
                    strDisbExists = FLDX.CheckIfDisbursementExists(strStudentAwardID, strStudentName)
                    If Not strDisbExists = "" Then
                        If SourceToTarget = "exceptiontotarget" Then
                            FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                        End If
                        strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strDisbExists, ExceptionGUID, , , msgEntry.RecordPosition)
                        Return "exception"
                        Exit Function
                    End If
                    'Check if payments have been posted for all disbursements for that award and amount
                    If Not SourceToTarget = "exceptiontotarget" Then
                        Dim strPaymentPosted As String = ""
                        strPaymentPosted = FLDX.CheckIfSchoolHasPostedAllPayments(strStudentAwardID, msgEntry, strStudentName)
                        If Not strPaymentPosted = "" Then
                            If SourceToTarget = "exceptiontotarget" Then
                                FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                            End If
                            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strPaymentPosted, ExceptionGUID, , , msgEntry.RecordPosition)
                            Return "exception"
                            Exit Function
                        End If
                    End If
                    'strAwardScheduleMessage = FLDX.GetAwardScheduleID(db, strAwardScheduleID, strStudentAwardID, strStudentName)
                    'If Not (strAwardScheduleMessage = "") Then
                    '    'Return strAwardScheduleMessage
                    '    Dim strExceptionMessage, strFileNameOnly As String
                    '    Dim intStartIndex As Integer = InStrRev(msgEntry.strFileNameSource, "/")
                    '    strFileNameOnly = msgEntry.strFileNameSource.Substring(intStartIndex)
                    '    strExceptionMessage = FLDX.BuildExceptionReport( msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strAwardScheduleMessage, ExceptionGUID)
                    '    Return "exception"
                    '    Exit Function
                    'Else
                    'If user chooses to override we need to replace Advantage data with Fame ESP Data 
                    'based on FA_ID, Otherwise try creating a new student award
                    If Not Override = "" And SourceToTarget = "exceptiontotarget" Then
                        strTransactionMessage = FLDX.Update_saTransaction(db, groupTrans, strTransactionID, strEnrollId, msgEntry, strStudentName, SourceToTarget)
                        If Not (strTransactionMessage = "") Then
                            FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strTransactionMessage, ExceptionGUID, , , msgEntry.RecordPosition)
                            Return "exception"
                            Exit Function
                        Else
                            strPmtDisbRel = FLDX.Update_saPmtDisbRel(db, groupTrans, strPmtDisbRelID, strTransactionID, strStudentAwardID, msgEntry, strStudentName)
                            If Not (strPmtDisbRel = "") Then
                                FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strPmtDisbRel, ExceptionGUID, , , msgEntry.RecordPosition)
                                Return "exception"
                                Exit Function
                            Else
                                saPaymentMessage = FLDX.Update_saPayments(db, groupTrans, strTransactionID, msgEntry, strStudentName, strStudentAwardID, strPmtDisbRelID)
                                If Not (saPaymentMessage = "") Then
                                    FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, saPaymentMessage, ExceptionGUID, , , msgEntry.RecordPosition)
                                    Return "exception"
                                    Exit Function
                                Else
                                    FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                    Process_RCVDMessage_New = True
                                    Dim intCountException As Integer = CheckDateExistsInExceptionReportByFileName(strFileNameOnly)
                                    If intCountException >= 1 Then
                                        Return "exception"
                                    Else
                                        Return ""
                                    End If
                                    Exit Function
                                End If
                            End If
                        End If
                    End If
                    '' New Code Added By Vijay Ramteke On August 25, 2010 For Mantis Id 

                    Dim AwardScheduleId As String
                    AwardScheduleId = GetStudentAwardScheduleId(FLDX, db, msgEntry, strStudentAwardID)
                    If AwardScheduleId = "" Or AwardScheduleId = Guid.Empty.ToString Then
                        Dim strAwaedMessage = "Unable to apply payments  for student " & strStudentName & ", as no disbursement exists for this student award"
                        If SourceToTarget = "exceptiontotarget" Then
                            FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                        End If
                        strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strTransactionMessage, ExceptionGUID, , , msgEntry.RecordPosition)
                        Return "exception"
                        Exit Function
                    Else
                        strTransactionMessage = FLDX.Add_saTransaction_New(db, groupTrans, strTransactionID, strEnrollId, msgEntry, strStudentName, SourceToTarget)
                        If Not (strTransactionMessage = "") Then
                            If SourceToTarget = "exceptiontotarget" Then
                                FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                            End If
                            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strTransactionMessage, ExceptionGUID, , , msgEntry.RecordPosition)
                            Return "exception"
                            Exit Function
                        Else
                            strPmtDisbRel = FLDX.Add_saPmtDisbRel_New(db, groupTrans, strPmtDisbRelID, strTransactionID, strStudentAwardID, AwardScheduleId, msgEntry, strStudentName)
                            If Not (strPmtDisbRel = "") Then
                                'Return strPmtDisbRel
                                If SourceToTarget = "exceptiontotarget" Then
                                    FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                End If
                                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strPmtDisbRel, ExceptionGUID, , , msgEntry.RecordPosition)
                                Return "exception"
                                Exit Function
                            Else
                                saPaymentMessage = FLDX.Add_saPayments_New(db, groupTrans, strTransactionID, msgEntry, strStudentName, strStudentAwardID, AwardScheduleId)
                                If Not (saPaymentMessage = "") Then
                                    'Return saPaymentMessage
                                    If SourceToTarget = "exceptiontotarget" Then
                                        FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                    End If
                                    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, saPaymentMessage, ExceptionGUID, , , msgEntry.RecordPosition)
                                    Return "exception"
                                    Exit Function
                                Else
                                    If SourceToTarget = "exceptiontotarget" Then
                                        FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                                    End If
                                    Process_RCVDMessage_New = True
                                    'Troy: 2/11/2013 Commented out the following to fix DE9085: FAMELink RCVD File is moved to the Exception Folder even though all the records were processed successfully
                                    'I am not sure why the check was being done. It seems unnecessary as if we get to this point it means that there was no exception. 
                                    'Since there are no exceptions I am just returning an empty string rather than doing the check.

                                    'Dim intCountException As Integer = CheckDateExistsInExceptionReportByFileName(strFileNameOnly)
                                    'If intCountException >= 1 Then
                                    '    Return "exception"
                                    'Else
                                    '    Return ""
                                    'End If
                                    Return ""
                                    Exit Function
                                End If
                            End If
                        End If
                    End If
                    ''strTransactionMessage = FLDX.Add_saTransaction(db, groupTrans, strTransactionID, strEnrollId, msgEntry, strStudentName, SourceToTarget)
                    ''If Not (strTransactionMessage = "") Then
                    ''    If SourceToTarget = "exceptiontotarget" Then
                    ''        FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                    ''    End If
                    ''    strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strTransactionMessage, ExceptionGUID)
                    ''    Return "exception"
                    ''    Exit Function
                    ''Else
                    ''    strPmtDisbRel = FLDX.Add_saPmtDisbRel(db, groupTrans, strPmtDisbRelID, strTransactionID, strStudentAwardID, msgEntry, strStudentName)
                    ''    If Not (strPmtDisbRel = "") Then
                    ''        'Return strPmtDisbRel
                    ''        If SourceToTarget = "exceptiontotarget" Then
                    ''            FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                    ''        End If
                    ''        strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, strPmtDisbRel, ExceptionGUID)
                    ''        Return "exception"
                    ''        Exit Function
                    ''    Else
                    ''        saPaymentMessage = FLDX.Add_saPayments(db, groupTrans, strTransactionID, msgEntry, strStudentName, strStudentAwardID)
                    ''        If Not (saPaymentMessage = "") Then
                    ''            'Return saPaymentMessage
                    ''            If SourceToTarget = "exceptiontotarget" Then
                    ''                FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                    ''            End If
                    ''            strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, saPaymentMessage, ExceptionGUID)
                    ''            Return "exception"
                    ''            Exit Function
                    ''        Else
                    ''            If SourceToTarget = "exceptiontotarget" Then
                    ''                FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                    ''            End If
                    ''            Process_RCVDMessage_New = True
                    ''            Dim intCountException As Integer = CheckDateExistsInExceptionReportByFileName(strFileNameOnly)
                    ''            If intCountException >= 1 Then
                    ''                Return "exception"
                    ''            Else
                    ''                Return ""
                    ''            End If
                    ''            Exit Function
                    ''        End If
                    ''    End If
                    ''End If

                    '' New Code Added By Vijay Ramteke On August 25, 2010 For Mantis Id
                    'End If
                End If
            Else
                'Return "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                sMessage = "Unable to match financial aid data with student " & strStudentName & " enrollment records "
                If SourceToTarget = "exceptiontotarget" Then
                    FLDX.DeleteRCVDFromExceptionReport(db, msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly)
                End If
                strExceptionMessage = FLDX.BuildExceptionReport(msgEntry.strSSN, msgEntry.strFAID, msgEntry.strFund, msgEntry, strFileNameOnly, msgEntry.dateModDate, sMessage, ExceptionGUID, , , msgEntry.RecordPosition)
                Return "exception"
                Exit Function
            End If
        Catch e As System.Exception
            Return e.Message
        End Try
    End Function
    Function GetStudentAwardScheduleId(ByRef FLDX As FLDataXfer, ByRef db As DataAccess.DataAccess, ByRef msgEntry As FLMessageInfo, ByVal StudentAwardId As String) As String
        Dim strStudentAwardScheduleId As String = ""
        '' Get Student AwardScheduleId For Amount and Expected Date Match

        '''' Code commented by Kamalesh Ahuja on 1 st september
        'strStudentAwardScheduleId = FLDX.GetStudentAwardScheduleIdByAmountAndDate(db, StudentAwardId, msgEntry.decAmount1, msgEntry.dateDate1)
        'If Not strStudentAwardScheduleId = "" and not strStudentAwardScheduleId=Guid.Empty.ToString Then
        '    '' Get the Match AwardSchedule For Amount And Expected Date
        '    Return strStudentAwardScheduleId
        'Else
        '''''''''''''''

        '' Get the Top 1 AwardSchedule
        strStudentAwardScheduleId = FLDX.GetStudentAwardScheduleId(db, StudentAwardId)
        Return strStudentAwardScheduleId
        '' Code Commented By Vijay Ramteke On August 26, 2010 As Pre Discussion With Troy
        '' '' Get Student AwardScheduleId For Date Match
        ''strStudentAwardScheduleId = FLDX.GetStudentAwardScheduleIdByDate(db, StudentAwardId, msgEntry.dateDate1)
        ''If Not strStudentAwardScheduleId = "" And Not strStudentAwardScheduleId = Guid.Empty.ToString Then
        ''    '' Get the Match AwardSchedule For Date
        ''    Return strStudentAwardScheduleId
        ''Else
        ''    '' Get Student AwardScheduleId For Amount Match
        ''    strStudentAwardScheduleId = FLDX.GetStudentAwardScheduleIdByAmount(db, StudentAwardId, msgEntry.decAmount1)
        ''    If Not strStudentAwardScheduleId = "" And Not strStudentAwardScheduleId = Guid.Empty.ToString Then
        ''        '' Get the Match AwardSchedule For Amount
        ''        Return strStudentAwardScheduleId
        ''    Else
        ''        '' Get the Top 1 AwardSchedule
        ''        strStudentAwardScheduleId = FLDX.GetStudentAwardScheduleId(db, StudentAwardId)
        ''        Return strStudentAwardScheduleId
        ''    End If
        ''End If
        '' Code Commented By Vijay Ramteke On August 26, 2010 As Pre Discussion With Troy

        '''' Code commented by Vijay Ramteke on 1 st september
        'End If
        '''''''''
        Return ""
    End Function
    ''New Code Added By Vijay Ramteke On August 25, 2010 For Mantis Id 
    '' New Code Added By Vijay Ramteke On September 03, 2010
    Public Function GetStuEnrollId(ByVal strSSN As String, ByVal dtAwardStartDate As DateTime, ByVal dtAwardEndDate As DateTime) As String
        Return (New FLDataXfer).GetStuEnrollId(strSSN, dtAwardStartDate, dtAwardEndDate)
    End Function
    '' New Code Added By Vijay Ramteke On September 03, 2010
    ''Added by Kamalesh Ahuja on May 27 2010 to resolve mantis issue id 18652
    Public Function IsRemoteServer(ByVal CampusId As String) As Boolean
        Return (New FLDataXfer).CheckIsRemoteServer(CampusId)
    End Function
    ''Added by Kamalesh Ahuja on May 27 2010 to resolve mantis issue id 18652
    Public Function GetSourceFilePath(ByVal CampusId As String, Optional ByRef SourcePath As String = "", Optional ByRef TargetPath As String = "", Optional ByRef ExceptionPath As String = "", Optional ByRef RemoteUserName As String = "", Optional ByRef RemotePasswd As String = "") As String
        Return (New FLDataXfer).GetFlSourcePath(CampusId, SourcePath, TargetPath, ExceptionPath, RemoteUserName, RemotePasswd)
    End Function

    Public Function GetFAMELinkFilesWithExceptions(ByVal CampusId As String) As DataSet
        Dim db As New FLDataXfer

        Return db.GetFAMELinkFilesWithExceptions(CampusId)

    End Function

    Public Function GetFLExceptionDetails(ByVal FileId As String, ByVal ShowHidden As Boolean) As DataSet
        Dim db As New FLDataXfer

        Return db.GetFLExceptionDetails(FileId, ShowHidden)

    End Function

    Public Function GetFAMELinkFileType(ByVal FileName As String) As String
        Dim fileType As String
        If FileName.ToUpper.Contains("FAID") Then
            fileType = "FAID"
        ElseIf FileName.ToUpper.Contains("HEAD") Then
            fileType = "HEAD"
        ElseIf FileName.ToUpper.Contains("DISB") Then
            fileType = "DISB"
        ElseIf FileName.ToUpper.Contains("CHNG") Then
            fileType = "CHNG"
        ElseIf FileName.ToUpper.Contains("RCVD") Then
            fileType = "RCVD"
        ElseIf FileName.ToUpper.Contains("REFU") Then
            fileType = "REFU"
        Else
            fileType = ""
        End If

        Return fileType
    End Function

    Public Function GetCountOfExceptions(Optional ByVal CampusId As String = "") As Integer
        Dim db As New FLDataXfer

        Return db.GetCountOfExceptions(CampusId)
    End Function

    Public Function GetAllCampuses() As DataSet
        Dim db As New FLDataXfer

        Return db.GetAllCampuses()

    End Function

    Public Function GetCountOfHiddenForFile(ByVal FileName As String) As Integer
        Dim db As New FLDataXfer

        Return db.GetCountOfHiddenForFile(FileName)

    End Function

    Public Function FileHasEnrollmentsForCampus(ByVal FileName As String, ByVal CampusId As String) As Boolean
        Dim db As New FLDataXfer

        Return db.FileHasEnrollmentsForCampus(FileName, CampusId)

    End Function

    Public Function FileHasEnrollmentsForNoCampuses(ByVal FileName As String) As Boolean
        Dim db As New FLDataXfer

        Return db.FileHasEnrollmentsForNoCampuses(FileName)

    End Function

End Class

