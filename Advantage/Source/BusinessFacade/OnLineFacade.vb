Public Class OnLineFacade
    Public Function GetAllOnLineStudents() As DataSet

        '   get the dataset with all OnLines
        Return (New OnLineDB).GetAllOnLineStudents()

    End Function
    Public Function GetOnLineInfoByOnLineStudentId(ByVal onLineStudentId As String) As OnLineInfo

        '   get the OnLineInfo
        Return (New OnLineDB).GetOnLineInfoByOnLineStudentId(onLineStudentId)

    End Function
    Public Function GetOnLineInfoByStudentId(ByVal studentId As String) As OnLineInfo

        '   get the OnLineInfo
        Return (New OnLineDB).GetOnLineInfoByStudentId(studentId)

    End Function
    Public Function GetOnLineInfoByStuEnrollId(ByVal stuEnrollId As String) As OnLineInfo

        '   get the OnLineInfo
        Return (New OnLineDB).GetOnLineInfoByStuEnrollId(stuEnrollId)

    End Function
    Public Function GetAllOnLineCoursesByStuEnrollId(ByVal stuEnrollId As String) As OnLineCourse()

        '   get the OnLineInfo
        Return (New OnLineDB).GetAllOnLineCoursesByStuEnrollId(stuEnrollId)

    End Function
    Public Function UpdateOnLineInfo(ByVal onLineInfo As OnLineInfo, ByVal user As String) As String

        '   If it is a new account do an insert. If not, do an update
        If Not (onLineInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New OnLineDB).AddOnLineInfo(onLineInfo, user)
        Else
            '   return integer with update results
            Return (New OnLineDB).UpdateOnLineInfo(onLineInfo, user)
        End If

    End Function
    'Public Function UpdateAllOnLineCoursesByStuEnrollId(ByVal stuEnrollId As String, ByVal onLineCourses As OnLineCourse()) As String

    '    '   return string with update results
    '    Return (New OnLineDB).UpdateAllOnLineCoursesByStuEnrollId(stuEnrollId, onLineCourses)

    'End Function
    Public Function DeleteOnLineInfo(ByVal stuEnrollId As String) As String

        '   delete OnLineInfo ans return string result
        Return (New OnLineDB).DeleteOnLineInfo(stuEnrollId)

    End Function
    Public Function IsOnLineClassSection(ByVal clsSectionId As String) As Boolean
        'return boolean
        Return (New OnLineDB).IsOnLineClassSection(clsSectionId)
    End Function
    Public Function IsEnrolledInAnyOnLineCourse(ByVal stuEnrollId As String) As Boolean
        'return boolean
        Return (New OnLineDB).IsEnrolledInAnyOnLineCourse(stuEnrollId)
    End Function
    Public Function IsOnLineStudent(ByVal studentId As String) As Boolean
        'return boolean
        Return (New OnLineDB).IsOnLineStudent(studentId)
    End Function
    Public Function UpdateOnLineStudentGradesWithOnLineData(ByVal results() As String, ByVal user As String) As String

        '   return string with update results
        Return (New OnLineDB).UpdateOnLineStudentGradesWithOnLineData(results, user)

    End Function
    Public Function UpdateOnLineStudentGradesWithAdvantageData(ByVal results() As String, ByVal user As String) As String

        '   return string with update results
        Return (New OnLineDB).UpdateOnLineStudentGradesWithAdvantageData(results, user)

    End Function
    Public Function GetAdvantageGrades(ByVal termGrades() As String, ByVal courseShortName As String) As DataSet
        '   get the Advantage Grades
        Return (New OnLineDB).GetAdvantageGrades(termGrades, courseShortName)
    End Function
    Public Function GetAdvantageCourseNameAndInstructor(ByVal courseShortName As String, ByVal termId As Integer) As String
        'get string with instructor name and course description
        Return (New OnLineDB).GetAdvantageCourseNameAndInstructor(courseShortName, termId)
    End Function
    Public Function GetAdvantageGradeScaleForCourse(ByVal courseShortName As String, ByVal termId As Integer) As DataSet
        'get Advantage Grade scale for course
        Return (New OnLineDB).GetAdvantageGradeScaleForCourse(courseShortName, termId)
    End Function
    Public Function SyncOnLineAttendance(ByVal onLineTermAttendance As OnLineTermAttendance, ByVal userId As String) As String
        'synchronize Attendance table
        Return (New OnLineDB).SyncOnLineAttendance(onLineTermAttendance, userId)
    End Function
    Public Function SynchAdvantageWithComCourse(ByVal termGrades() As String, ByVal termId As String) As String
        'synchronize atOnlineStudents table
        Return (New OnLineDB).SynchAdvantageWithComCourse(termGrades, termId)
    End Function

End Class
