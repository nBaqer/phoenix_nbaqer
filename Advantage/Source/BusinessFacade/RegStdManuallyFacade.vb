Imports FAME.AdvantageV1.BusinessRules
Imports FAME.AdvantageV1.DataAccess
Imports FAME.Advantage.Common

Public Class RegStdManuallyFacade
    'Public Function GetRemainingClassSections(ByVal stuEnrollId As String, ByVal campusId As String) As DataTable
    ''To fix mantis issue 17516: QA: "Bypass prerequisites" on register students manually is not showing all the class sections.
    Public Function GetRemainingClassSections(ByVal stuEnrollId As String, ByVal chkReqs As Boolean, ByVal campusId As String, ByVal chkAllCourses As Boolean, ByVal userId As String) As DataTable
        Dim ds As DataSet
        Dim ds2 As DataSet
        Dim tbl4 As New DataTable
        Dim dr As DataRow
        Dim row As DataRow
        Dim db As New RegStdsManuallyDB
        Dim facade As New RegFacade
        Dim errStr As String
        Dim attempted As Integer
        Dim iscoursecomplete As Boolean
        Dim regDB As New RegisterDB
        Dim isPass As Boolean
        Dim courseRetake As Boolean
        Dim br As New RegisterStudentsBR
        Dim stdAvailable As Boolean
        Dim metPrereqs As Boolean
        Dim hasOverride As Integer
        Dim startDate As String
        Dim examComponentsOnly As Boolean

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
      
        'Create new data table to add remaining class sections for student enrollment
        tbl4.TableName = "StudentClassSections"

        'Add a column called TblName and another called TblPK to the dtTables data table
        tbl4.Columns.Add("StuEnrollId")
        tbl4.Columns.Add("Code")
        tbl4.Columns.Add("Descrip")
        tbl4.Columns.Add("ClsSection")
        tbl4.Columns.Add("Shift")
        tbl4.Columns.Add("ClsSectionId")
        tbl4.Columns.Add("Credits")
        tbl4.Columns.Add("Hours")
        tbl4.Columns.Add("TermStartDate")
        tbl4.Columns.Add("TermDescrip")
        tbl4.Columns.Add("CampDescrip")
        tbl4.Columns.Add("ReqId")
        tbl4.Columns.Add("TermId")
        tbl4.Columns.Add("Instructor")

        Dim dbTranscript As New TranscriptDB

        If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
            ds = dbTranscript.GetRemainingCoursesForContinuingEdProgVersion_sp(stuEnrollId)
        Else
            ds = dbTranscript.GetRemainingCourses_sp(stuEnrollId)
        End If
        Dim i As Integer = 0

        For Each dr In ds.Tables(0).Rows
                i = i + 1
                startDate = dr("StartDate").ToString
                If startDate = "" Then startDate = dr("ExpStartDate").ToString
                If startDate <> "" Then
                    'ds2 = db.GetCourseClassSections(dr("ReqId").ToString, stuEnrollId, chkAllCourses, userId, campusId)
                    ds2 = db.GetCourseClassSectionsByClassSectionId(dr("ClsSectionId").ToString, stuEnrollId, chkAllCourses, userId, campusId)
                    If Not (ds2 Is Nothing) Then
                        Dim dtAttemptedClassTermStartDate As DateTime = Nothing
                        Dim dtAttemptedClassStartDate As DateTime = Nothing
                        For Each row In ds2.Tables(0).Rows

                            'bypass all the requirement conditiond
                            If chkReqs Then


                                Dim dr2 As DataRow = tbl4.NewRow

                                dr2("StuEnrollId") = dr("StuEnrollId")
                                dr2("Code") = dr("Code")
                                dr2("Descrip") = dr("Req")
                                dr2("Credits") = dr("Credits")
                                dr2("Hours") = dr("Hours")
                                dr2("ClsSection") = row("ClsSection")
                                dr2("ClsSectionId") = row("ClsSectionId")
                                dr2("Shift") = row("ShiftDescrip")
                                dr2("TermStartDate") = Convert.ToDateTime(row("TermStartDate")).ToShortDateString
                                dr2("TermDescrip") = row("TermDescrip")
                                dr2("CampDescrip") = row("CampDescrip")
                                dr2("ReqId") = dr("ReqId")
                                dr2("TermId") = row("TermId")
                                dr2("Instructor") = row("Instructor")
                                tbl4.Rows.Add(dr2)

                            Else

                                'If (dr("StartDate") < row("EndDate")) Then
                                If (startDate < row("EndDate")) Then

                                    stdAvailable = False

                                    'ExamComponentsOnly = regDB.IsCourseExamsOnly(dr("Code"), "exam")

                                    'Check if the student has attempted/is registered for this course for any other enrollment
                                    iscoursecomplete = IsCourseCompleted(stuEnrollId, row("ClsSectionId").ToString)
                                    ' iscoursecomplete = regDB.IsCourseComplete(stuEnrollId, row("ClsSectionId").ToString)
                                    attempted = regDB.HasStdAttemptedReq(stuEnrollId, row("ClsSectionId").ToString)

                                    isPass = br.DoesStdHavePassingGrd(stuEnrollId, dr("ReqId").ToString, dr("PrgVerId").ToString, row("ClsSectionId").ToString)
                                    If Not attempted >= 1 Then stdAvailable = True
                                    If isPass = False Then stdAvailable = True
                                    'Commented by Balaji on 6.28.2013 DE9768
                                    'If IsPass = True And ds.Tables(0).Rows(0).Item("AllowCompletedCourseRetake") = True Then StdAvailable = True
                                    'The above line was modified on 6.28.2013
                                    If isPass = True And dr("AllowCompletedCourseRetake") = True Then stdAvailable = True

                                    'Added by Balaji for defect DE9783
                                    'Prevent Retake of class section in prior terms
                                    If attempted >= 1 Then
                                        Dim dtAttemptedTermstartdate As DateTime
                                        Dim dtAttClassStartDate As DateTime
                                        Try
                                            dtAttemptedTermstartdate = regDB.GetAttemptedTermStartDate(stuEnrollId, row("ClsSectionId").ToString)
                                            If Not dtAttemptedTermstartdate = "#12:00:00 AM#" Then dtAttemptedClassTermStartDate = dtAttemptedTermstartdate
                                        Catch ex As Exception
                                            'dtAttemptedTermstartdate = Nothing
                                        End Try

                                        Try
                                            dtAttClassStartDate = regDB.GetAttemptedClassStartDate(stuEnrollId, row("ClsSectionId").ToString)
                                            If Not dtAttClassStartDate = "#12:00:00 AM#" Then dtAttemptedClassStartDate = dtAttClassStartDate
                                        Catch ex As Exception
                                            'dtAttClassStartDate = Nothing
                                        End Try

                                        'If Not dtAttemptedTermstartdate = Nothing Then
                                        If dr("AllowCompletedCourseRetake") = True And row("TermStartDate") >= dtAttemptedClassTermStartDate Then
                                            stdAvailable = True
                                            'If the class starts before an already attempted class then class should not be available for registration
                                            If row("ClassStartDate") < dtAttemptedClassStartDate Then stdAvailable = False
                                        ElseIf dr("AllowCompletedCourseRetake") = True And row("TermStartDate") < dtAttemptedClassTermStartDate Then
                                            stdAvailable = False
                                        End If

                                        If dr("AllowCompletedCourseRetake") = False And row("TermStartDate") <= dtAttemptedClassTermStartDate Then stdAvailable = False
                                        'End If
                                    End If

                                    'A student cannot be registered in another instance of an attempted course but cannot be registered in the same class
                                    'this if block prevents a student from getting registered in the same class again.
                                    If stdAvailable = True Then
                                        Dim hasStudentCompletedClass As Boolean
                                        hasStudentCompletedClass = regDB.HasStudentCompletedClass(stuEnrollId, row("ClsSectionId").ToString)
                                        If hasStudentCompletedClass = True Then
                                            stdAvailable = False
                                        End If
                                    End If
                                    
                                    If stdAvailable = True Then

                                        '****************************************************
                                        'Check if Student has met prereqs.
                                        '****************************************************


                                        metPrereqs = br.HasStdMetPrereqs(stuEnrollId, row("ClsSectionId").ToString, dr("PrgVerId").ToString, campusId, "False")
                                        'Check if student has override for any of the prereqs for this course
                                        hasOverride = regDB.DoesStdHaveOverride(stuEnrollId, row("ClsSectionId").ToString)

                                        If myAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                                            Dim intRowCount As Integer
                                            intRowCount = (New RegisterStudentsBR).IsCourseASHLevelCourse(dr("ReqId").ToString, "SH")
                                            If intRowCount >= 1 Then
                                                metPrereqs = True
                                            End If
                                        End If


                                        If metPrereqs = True Or hasOverride >= 1 Then  'Interested in Std...add to new datatable
                                            errStr = facade.ChkStudSchedConflict(stuEnrollId, row("ClsSectionId").ToString)

                                            If errStr = "" Then 'Conflict

                                                Dim dr2 As DataRow = tbl4.NewRow

                                                dr2("StuEnrollId") = dr("StuEnrollId")
                                                dr2("Code") = dr("Code")
                                                If iscoursecomplete = True Then
                                                    dr2("Descrip") = dr("Req") & "  (Retake)"
                                                Else
                                                    dr2("Descrip") = dr("Req")
                                                End If
                                                dr2("Credits") = dr("Credits")
                                                dr2("Hours") = dr("Hours")
                                                dr2("ClsSection") = row("ClsSection")
                                                dr2("ClsSectionId") = row("ClsSectionId")
                                                dr2("Shift") = row("ShiftDescrip")
                                                dr2("TermStartDate") = Convert.ToDateTime(row("TermStartDate")).ToShortDateString
                                                dr2("TermDescrip") = row("TermDescrip")
                                                dr2("CampDescrip") = row("CampDescrip")
                                                dr2("ReqId") = dr("ReqId")
                                                dr2("TermId") = row("TermId")
                                                dr2("Instructor") = row("Instructor")
                                                tbl4.Rows.Add(dr2)

                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        Next
                    End If
                End If
            Next

        Return tbl4
    End Function


    ''Campusid added to Function
    ''Modified on August 5 2009
    ''For mantis case 16753
    Public Function GetRemainingClassSections(ByVal stuEnrollId As String, ByVal termIds As String, ByVal chkReqs As Boolean, ByVal campusid As String, ByVal chkAllCourses As Boolean, ByVal userId As String) As DataTable
        Dim ds As DataSet
        Dim ds2 As DataSet
        Dim tbl4 As New DataTable
        Dim dr As DataRow
        Dim row As DataRow
        Dim db As New RegStdsManuallyDB
        Dim facade As New RegFacade
        Dim errStr As String
        Dim attempted As Integer
        Dim iscoursecomplete As Boolean
        Dim regDB As New RegisterDB
        Dim isPass As Boolean
        Dim br As New RegisterStudentsBR
        Dim stdAvailable As Boolean
        Dim metPrereqs As Boolean
        Dim hasOverride As Integer
        Dim startDate As String
        Dim examComponentsOnly As Boolean

        'Create new datatable to add remaining class sections for student enrollment
        tbl4.TableName = "StudentClassSections"

        'Add a column called TblName and another called TblPK to the dtTables datatable
        tbl4.Columns.Add("StuEnrollId")
        tbl4.Columns.Add("Code")
        tbl4.Columns.Add("Descrip")
        tbl4.Columns.Add("ClsSection")
        tbl4.Columns.Add("Shift")
        tbl4.Columns.Add("ClsSectionId")
        tbl4.Columns.Add("Credits")
        tbl4.Columns.Add("Hours")
        tbl4.Columns.Add("TermStartDate")
        tbl4.Columns.Add("TermDescrip")
        tbl4.Columns.Add("CampDescrip")
        tbl4.Columns.Add("ReqId")
        tbl4.Columns.Add("TermId")
        tbl4.Columns.Add("Instructor")

        Dim dbTranscript As New TranscriptDB

        If dbTranscript.IsContinuingEdPrgVersion(stuEnrollId) Then
            ds = dbTranscript.GetRemainingCoursesForContinuingEdProgVersion_sp(stuEnrollId)
        Else
            ds = dbTranscript.GetRemainingCourses_sp(stuEnrollId, termIds)
        End If

        ' Analysis if you bypass the requirements
        If chkReqs Then
            For Each dr In ds.Tables(0).Rows
                startDate = dr("StartDate").ToString
                If startDate = String.Empty Then startDate = dr("ExpStartDate").ToString
                If startDate <> String.Empty Then
                    'ds2 = db.GetCourseClassSections(dr("ReqId").ToString, stuEnrollId, chkAllCourses, userId, campus-id)
                    ds2 = db.GetCourseClassSectionsByClassSectionId(dr("ClsSectionId").ToString, stuEnrollId, chkAllCourses, userId, campusid, termIds)
                    If Not (ds2 Is Nothing) Then
                        For Each row In ds2.Tables(0).Rows
                            AddRowToDt(tbl4, dr, row, iscoursecomplete)
                        Next
                    End If
                End If
            Next
            Return tbl4 ' Finished function....
        End If
        ' Analysis of all requirements......
        Try
            For Each dr In ds.Tables(0).Rows
                startDate = dr("StartDate").ToString
                If startDate = String.Empty Then startDate = dr("ExpStartDate").ToString
                If startDate <> String.Empty Then
                    'Get the courses assigned to the student enrollment
                    'ds2 = db.GetCourseClassSections(dr("ReqId").ToString, stuEnrollId, chkAllCourses, userId, campus-id)
                    ds2 = db.GetCourseClassSectionsByClassSectionId(dr("ClsSectionId").ToString, stuEnrollId, chkAllCourses, userId, campusid, termIds)
                    If (ds2 Is Nothing) Then
                        Continue For ' No curse assigned returned
                    End If

                    Dim dtAttemptedClassTermStartDate As DateTime = Nothing
                    Dim dtAttemptedClassStartDate As DateTime = Nothing
                    ' for each curse assigned to the Term
                    For Each row In ds2.Tables(0).Rows
                        ' The student should be enrolled before the course ended
                        If Not (startDate < row("EndDate")) Then
                            Continue For
                        End If

                        ' Set Flag not available the course.....
                        stdAvailable = False
                        ' Read Conditions from database
                        'iscoursecomplete = IsCourseCompleted(stuEnrollId, row("ClsSectionId").ToString)
                        ' Dim isDropped As Boolean = regDB.IsCourseDropped(stuEnrollId,  row("ClsSectionId").ToString)
                        'Check if the student has attempted/is registered for this course for any other enrollment
                        attempted = regDB.HasStdAttemptedReq(stuEnrollId, row("ClsSectionId").ToString)

                        If attempted < 1 Then stdAvailable = True
                      
                        'Prevent Retake of class section in prior terms
                        If attempted > 0 Then

                            'Analysis isPass or not
                            isPass = br.DoesStdHavePassingGrd(stuEnrollId, dr("ReqId").ToString, dr("PrgVerId").ToString, row("ClsSectionId").ToString)
                            If isPass = False Then stdAvailable = True
                            If isPass = True And dr("AllowCompletedCourseRetake") = True Then stdAvailable = True

                            'Get the beginning of the Term
                            Dim dtAttemptedTermstartdate As DateTime
                            Dim dtAttClassStartDate As DateTime

                            dtAttemptedTermstartdate = regDB.GetAttemptedTermStartDate(stuEnrollId, row("ClsSectionId").ToString)
                            If Not dtAttemptedTermstartdate = "#12:00:00 AM#" Then dtAttemptedClassTermStartDate = dtAttemptedTermstartdate

                            dtAttClassStartDate = regDB.GetAttemptedClassStartDate(stuEnrollId, row("ClsSectionId").ToString)
                            If Not dtAttClassStartDate = "#12:00:00 AM#" Then dtAttemptedClassStartDate = dtAttClassStartDate

                            If dr("AllowCompletedCourseRetake") = True And row("TermStartDate") >= dtAttemptedClassTermStartDate Then
                                stdAvailable = True
                                'If the class starts before an already attempted class then class should not be available for registration
                                If row("ClassStartDate") < dtAttemptedClassStartDate Then stdAvailable = False
                            ElseIf dr("AllowCompletedCourseRetake") = True And row("TermStartDate") < dtAttemptedClassTermStartDate Then
                                stdAvailable = False
                            End If
                        End If

                        'A student cannot be registered in another instance of an attempted course but cannot be registered in the same class
                        'this if block prevents a student from getting registered in the same class again.
                        If stdAvailable = True Then
                            Dim hasStudentCompletedClass As Boolean
                            hasStudentCompletedClass = regDB.HasStudentCompletedClass(stuEnrollId, row("ClsSectionId").ToString)
                            If hasStudentCompletedClass = True Then
                                stdAvailable = False
                            End If
                        End If

                        'If not attempted/registered, then get class sections for this course.
                        If stdAvailable = True Then

                            '****************************************************
                            'Check if Student has met prereqs.
                            '****************************************************
                            metPrereqs = br.HasStdMetPrereqs(stuEnrollId, row("ClsSectionId").ToString, dr("PrgVerId").ToString, campusid, "False")
                            'Check if student has override for any of the prereqs for this course
                            hasOverride = regDB.DoesStdHaveOverride(stuEnrollId, row("ClsSectionId").ToString)
                            'If MetPrereqs = False Then 'Not interested in Student

                            If metPrereqs = True Or hasOverride >= 1 Then  'Interested in Std...add to new datatable
                                errStr = facade.ChkStudSchedConflict(stuEnrollId, row("ClsSectionId").ToString)

                                If errStr = String.Empty Then 'Conflict
                                    'Add the course to result
                                    AddRowToDt(tbl4, dr, row, (attempted > 0))
                                End If
                            End If
                        End If
                    Next
                End If
                ' End If
            Next

        Catch ex As Exception

        End Try

        Return tbl4
    End Function

    Private Sub AddRowToDt(dt As DataTable, dr As DataRow, row As DataRow, isretake As Boolean)

        Dim dr2 As DataRow = dt.NewRow
        dr2("StuEnrollId") = dr("StuEnrollId")
        dr2("Code") = dr("Code")
        If isretake Then
            dr2("Descrip") = dr("Req") & "  (Retake)"
        Else
            dr2("Descrip") = dr("Req")
        End If
        dr2("Credits") = dr("Credits")
        dr2("Hours") = dr("Hours")
        dr2("ClsSection") = row("ClsSection")
        dr2("ClsSectionId") = row("ClsSectionId")
        dr2("Shift") = row("ShiftDescrip")
        dr2("TermStartDate") = Convert.ToDateTime(row("TermStartDate")).ToShortDateString
        dr2("TermDescrip") = row("TermDescrip")
        dr2("CampDescrip") = row("CampDescrip")
        dr2("ReqId") = dr("ReqId")
        dr2("TermId") = row("TermId")
        dr2("Instructor") = row("Instructor")
        dt.Rows.Add(dr2)
    End Sub

    ''' <summary>
    ''' Get the info about a class quorum
    ''' </summary>
    ''' <param name="clsSectId"></param>
    ''' <returns>return a ClassQuorumInfo class otherwise Nothing</returns>
    Public Shared Function GetClassQuorum(clsSectId As String) As ClassQuorumInfo
        Dim db As RegisterDB = New RegisterDB()
        Dim info As ClassQuorumInfo = db.GetClassQuorum(clsSectId)
        Return info
    End Function


    Public Function RegisterStdManually(ByVal finalGrdObj As FinalGradeInfo, ByVal user As String)
        Dim db As New TransferGradesDB
        db.UpdateTransferGrade(finalGrdObj, user)
    End Function

    Public Function IsCourseCompleted(ByVal stuEnrollId As String, ByVal clsSectionId As String)
        Dim regDB As New RegisterDB
        Dim bIsCourseCompleted As Boolean
        bIsCourseCompleted = regDB.IsCourseComplete(stuEnrollId, clsSectionId)
        Return bIsCourseCompleted
    End Function

    Public Function GetPeriodClassRoom(ByVal clsSectionId As String) As List(Of PeriodClassRoom)
        Dim db As New RegStdsManuallyDB()
        Dim periodClassRoomList As List(Of PeriodClassRoom)
        periodClassRoomList = db.GetPeriodClassRoom(clsSectionId)
        Return periodClassRoomList
    End Function

    Public Function IsEnrollmentInSchoolStatus(ByVal StuEnrollId As String) As Boolean
        Dim db As New RegStdsManuallyDB()

        Return db.IsEnrollmentInSchoolStatus(StuEnrollId)

    End Function


End Class
