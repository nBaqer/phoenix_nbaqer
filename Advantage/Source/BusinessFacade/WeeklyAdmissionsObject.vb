Imports FAME.AdvantageV1.DataAccess.TM

Public Class WeeklyAdmissionsObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim objDB As New AdmissionRepPerformanceDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by report.
        ds = BuildReportSource(objDB.GetAdmissionsWeeklyReport(rptParamInfo), objDB.CutOffDate)
        ' The report, whatever its frontend is, will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    ' Copy data from dataset to datatable.
    Private Function BuildReportSource(ByVal ds As DataSet, ByVal cutOffDate As DateTime) As DataSet
        Try
            If ds.Tables.Count = 9 Then
                Dim dtStatuses As DataTable = ds.Tables("LeadStatuses")
                ProcessLeadStatusesDT(dtStatuses)

                Dim dtPlacements As DataTable = ds.Tables("Placements")
                'Format LeadName and PlaceOfEmployment columns on the Placements datatable
                FormatPlacementsDT(dtPlacements)

                Dim dtCGC As DataTable = ds.Tables("CampGrpsCampuses")
                Dim dtAptt As DataTable = ds.Tables("Appointments")
                Dim dtApptRates As DataTable = ds.Tables("ApptRates")
                'Compute totals for the Appointments datatable per campus grp/campus
                PopulateAppointmentsDT(dtCGC, dtAptt, dtApptRates, cutOffDate)

                Dim dtAdRep As DataTable = ds.Tables("AdRepPerformance")
                'Remove rows belonging to Admissions Rep with WeeklyCount=0 and CummCount=0
                'Compute Starts for each Admissions Rep per campus grp/campus
                ProcessPerformanceDT(dtAdRep)

                Dim dtDocTest As DataTable = ds.Tables("DocumentsAndTests")
                Dim dtDocTestRates As DataTable = ds.Tables("DocTestRates")
                'Add a table with Document and Test rates per campus grp/campus
                PopulateDocTestRatesDT(dtDocTest, dtDocTestRates)
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

    Private Sub PopulateAppointmentsDT(ByVal dtCGC As DataTable, ByVal dtAppt As DataTable, ByVal dtRates As DataTable, ByVal cutOffDate As DateTime)
        Dim oldCampGrp As String = ""
        Dim oldCampus As String = ""
        Dim row As DataRow
        Dim dsUT As New DataSet
        Dim weeklyPending As Integer
        Dim ytdPending As Integer
        Dim weeklyCompleted As Integer
        Dim ytdCompleted As Integer

        For Each dr As DataRow In dtCGC.Rows
            If oldCampGrp = "" Or oldCampGrp <> dr("CampGrpId").ToString Then
                oldCampGrp = dr("CampGrpId").ToString
                oldCampus = ""
            End If
            If oldCampus = "" Or oldCampus <> dr("CampusId").ToString Then
                oldCampus = dr("CampusId").ToString
                '
                row = dtAppt.NewRow
                row("CampGrpId") = oldCampGrp
                row("CampusId") = oldCampus
                row("Descrip") = "Scheduled Appointments"
                row("ViewOrder") = 0
                'Get ALL appointments, regardless of their status
                dsUT = UserTasksDB.GetAll(oldCampus, "", "", "", "", Common.TM.TaskStatus.None, cutOffDate.AddDays(-6).ToString, cutOffDate.AddDays(1).ToString)
                If dsUT.Tables.Count > 0 Then
                    weeklyPending = dsUT.Tables(0).Compute("Count(TaskDescrip)", "ModuleName='Admissions'")
                    row("WeeklyCount") = weeklyPending
                End If
                'Get ALL appointments, regardless of their status
                dsUT = UserTasksDB.GetAll(oldCampus, "", "", "", "", Common.TM.TaskStatus.None, "01/01/" & cutOffDate.Year, cutOffDate.AddDays(1).ToString)
                If dsUT.Tables.Count > 0 Then
                    ytdPending = dsUT.Tables(0).Compute("Count(TaskDescrip)", "ModuleName='Admissions'")
                    row("CummCount") = ytdPending
                End If
                dtAppt.Rows.Add(row)
                '
                row = dtAppt.NewRow
                row("CampGrpId") = oldCampGrp
                row("CampusId") = oldCampus
                row("Descrip") = "Completed Appointments"
                row("ViewOrder") = 1
                'Get only COMPLETED appointments
                dsUT = UserTasksDB.GetAll(oldCampus, "", "", "", "", Common.TM.TaskStatus.Completed, cutOffDate.AddDays(-6).ToString, cutOffDate.AddDays(1).ToString)
                If dsUT.Tables.Count > 0 Then
                    weeklyCompleted = dsUT.Tables(0).Compute("Count(TaskDescrip)", "ModuleName='Admissions'")
                    row("WeeklyCount") = weeklyCompleted
                End If
                'Get only COMPLETED appointments
                dsUT = UserTasksDB.GetAll(oldCampus, "", "", "", "", Common.TM.TaskStatus.Completed, "01/01/" & cutOffDate.Year, cutOffDate.AddDays(1).ToString)
                If dsUT.Tables.Count > 0 Then
                    ytdCompleted = dsUT.Tables(0).Compute("Count(TaskDescrip)", "ModuleName='Admissions'")
                    row("CummCount") = ytdCompleted
                End If
                dtAppt.Rows.Add(row)

                row = dtRates.NewRow
                row("CampGrpId") = oldCampGrp
                row("CampusId") = oldCampus
                'Compute ratio: Weekly_Completed/Weekly_Pending * 100%
                If weeklyPending <> 0 Then
                    row("WeeklyCount") = Math.Round(weeklyCompleted / weeklyPending * 100, 0)
                Else
                    row("WeeklyCount") = 0
                End If
                'Compute ratio: YTD_Completed/YTD_Pending * 100%
                If ytdPending <> 0 Then
                    row("CummCount") = Math.Round(ytdCompleted / ytdPending * 100, 0)
                Else
                    row("CummCount") = 0
                End If
                dtRates.Rows.Add(row)
            End If
        Next
    End Sub

    Private Sub FormatPlacementsDT(ByVal dtPlacements As DataTable)
        Dim facInputMasks As New InputMasksFacade
        Dim zipMask As String
        Dim streetAddress As String = ""
        Dim cityStateZip As String = ""

        'Get the mask for zip
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        For Each dr As DataRow In dtPlacements.Rows
            'Only Lead's MiddleName allows NULL.
            If Not dr.IsNull("MiddleName") Then
                If dr("MiddleName") <> "" Then
                    dr("LeadName") = dr("LastName") & ", " & dr("FirstName") & " " & dr("MiddleName") & "."
                Else
                    dr("LeadName") = dr("LastName") & ", " & dr("FirstName")
                End If
            Else
                dr("LeadName") = dr("LastName") & ", " & dr("FirstName")
            End If
            '
            If Not dr.IsNull("Employer") Then
                dr("PlaceOfEmployment") = dr("Employer")
            Else
                dr("PlaceOfEmployment") = ""
            End If
            '
            streetAddress = ""
            cityStateZip = ""
            '
            'Massage data to set PlaceOfEmployment as "Employer (Employer's Address)"
            'Address1 allows NULL.
            If Not dr.IsNull("Address1") Then
                If dr("Address1") <> "" Then
                    streetAddress = dr("Address1")
                End If
            End If
            'Address2 allows NULL.
            If Not dr.IsNull("Address2") Then
                If dr("Address2") <> "" Then
                    streetAddress &= " " & dr("Address2")
                End If
            End If
            'City allows NULL.
            If Not dr.IsNull("City") Then
                If dr("City") <> "" Then
                    cityStateZip = dr("City")
                End If
            End If
            'State allows NULL.
            If Not dr.IsNull("State") Then
                If Not dr.IsNull("ForeignZip") Then
                    If dr("State") <> "" And dr("ForeignZip") = False Then
                        'Domestic State
                        If cityStateZip <> "" Then
                            cityStateZip &= ", " & dr("State")
                        Else
                            cityStateZip = dr("State")
                        End If
                    End If
                Else
                    'Domestic State
                    If dr("StateDescrip") <> "" Then
                        If cityStateZip <> "" Then
                            cityStateZip &= ", " & dr("StateDescrip")
                        Else
                            cityStateZip = dr("StateDescrip")
                        End If
                    End If
                End If
            End If
            'StateOther allows NULL.
            If Not dr.IsNull("OtherState") Then
                If Not (dr("ForeignZip") Is System.DBNull.Value) Then
                    If dr("OtherState") <> "" And dr("ForeignZip") = True Then
                        'International State
                        If cityStateZip <> "" Then
                            cityStateZip &= ", " & dr("OtherState")
                        Else
                            cityStateZip = dr("OtherState")
                        End If
                    End If
                End If
            End If
            'Zip allows NULL.
            If Not dr.IsNull("Zip") Then
                If Not dr.IsNull("ForeignZip") Then
                    If dr("Zip") <> "" And dr("ForeignZip") = False Then
                        'Domestic Zip
                        If cityStateZip <> "" Then
                            cityStateZip &= " " & facInputMasks.ApplyMask(zipMask, dr("Zip"))
                        Else
                            cityStateZip = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                        End If
                    Else
                        'International Zip
                        If dr("Zip") <> "" Then
                            If cityStateZip <> "" Then
                                cityStateZip &= " " & dr("Zip")
                            Else
                                cityStateZip &= dr("Zip")
                            End If
                        End If
                    End If
                Else
                    'Domestic Zip
                    If dr("Zip") <> "" Then
                        If cityStateZip <> "" Then
                            cityStateZip &= " " & facInputMasks.ApplyMask(zipMask, dr("Zip"))
                        Else
                            cityStateZip = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                        End If
                    End If
                End If
            End If
            '
            If streetAddress <> "" And cityStateZip <> "" Then
                dr("PlaceOfEmployment") &= " (" & streetAddress & " " & cityStateZip & ")"
            ElseIf streetAddress <> "" Then
                dr("PlaceOfEmployment") &= " (" & streetAddress & ")"
            ElseIf cityStateZip <> "" Then
                dr("PlaceOfEmployment") &= " (" & cityStateZip & ")"
            End If
        Next
    End Sub

    Private Sub ProcessPerformanceDT(ByVal dtAdRep As DataTable)
        Dim oldCampGrp As String = ""
        Dim oldCampus As String = ""
        Dim oldAdRep As String = ""
        Dim weekly As Integer
        Dim ytd As Integer
        Dim r As DataRow
        Dim rows() As DataRow
        Dim dtCopy As DataTable = dtAdRep.Copy()

        'First validate that Admissions Rep has more that 0 in overall WeeklyCount and overall CummCount
        For Each dr As DataRow In dtCopy.Rows
            If oldCampGrp = "" Or oldCampGrp <> dr("CampGrpId").ToString Then
                oldCampGrp = dr("CampGrpId").ToString
                oldCampus = ""
                oldAdRep = ""
            End If
            If oldCampus = "" Or oldCampus <> dr("CampusId").ToString Then
                oldCampus = dr("CampusId").ToString
            End If
            If oldAdRep = "" Or oldAdRep <> dr("UserId").ToString Then
                oldAdRep = dr("UserId").ToString
                weekly = dtAdRep.Compute("SUM(WeeklyCount)", "CampGrpId='" & oldCampGrp & "' AND CampusId='" & oldCampus & "' AND UserId='" & oldAdRep & "'")
                ytd = dtAdRep.Compute("SUM(CummCount)", "CampGrpId='" & oldCampGrp & "' AND CampusId='" & oldCampus & "' AND UserId='" & oldAdRep & "'")
                If weekly = 0 And ytd = 0 Then
                    rows = dtAdRep.Select("CampGrpId='" & oldCampGrp & "' AND CampusId='" & oldCampus & "' AND UserId='" & oldAdRep & "'")
                    If rows.GetLength(0) > 0 Then
                        For Each r In rows
                            'Mark row for deletion
                            dtAdRep.Rows.Remove(r)
                        Next
                    End If
                End If
            End If
        Next

        'Commit changes
        dtAdRep.AcceptChanges()
        'Get a fresh copy of table
        dtCopy = dtAdRep.Copy()

        'Initialize variables
        oldCampGrp = ""
        oldCampus = ""
        oldAdRep = ""

        Dim row As DataRow
        Dim weeklyEnrolled As Integer = 0
        Dim ytdEnrolled As Integer = 0
        Dim weeklyCancelled As Integer = 0
        Dim ytdCancelled As Integer = 0

        For Each dr As DataRow In dtCopy.Rows
            If oldCampGrp = "" Or oldCampGrp <> dr("CampGrpId").ToString Then
                oldCampGrp = dr("CampGrpId").ToString
                oldCampus = ""
                oldAdRep = ""
            End If
            If oldCampus = "" Or oldCampus <> dr("CampusId").ToString Then
                oldCampus = dr("CampusId").ToString
            End If
            If oldAdRep = "" Or oldAdRep <> dr("UserId").ToString Then
                oldAdRep = dr("UserId").ToString
                '
                row = dtAdRep.NewRow
                row("CampGrpId") = oldCampGrp
                row("CampusId") = oldCampus
                row("UserId") = oldAdRep
                row("AdmissionsRep") = dr("AdmissionsRep")
                row("SysStatusId") = 999
                row("StatusCodeDescription") = "Starts"

                'Compute Starts = Enrolled - Cancelled
                rows = dtAdRep.Select("CampGrpId='" & oldCampGrp & "' AND CampusId='" & oldCampus & "' AND UserId='" & oldAdRep & "' AND (SysStatusId=6 OR SysStatusId=8)")
                If rows.GetLength(0) > 0 Then
                    For Each r In rows
                        Select Case r("SysStatusId").ToString
                            Case "6"
                                'Enrolled system status
                                weeklyEnrolled = r("WeeklyCount")
                                ytdEnrolled = r("CummCount")
                            Case "8"
                                'No Start system status
                                weeklyCancelled = r("WeeklyCount")
                                ytdCancelled = r("CummCount")
                        End Select
                    Next
                End If
                If (weeklyEnrolled > weeklyCancelled) Then
                    row("WeeklyCount") = weeklyEnrolled - weeklyCancelled
                Else
                    row("WeeklyCount") = 0
                End If
                If (ytdEnrolled > ytdCancelled) Then
                    row("CummCount") = ytdEnrolled - ytdCancelled
                Else
                    row("CummCount") = 0
                End If

                dtAdRep.Rows.Add(row)
            End If
        Next

    End Sub

    Private Sub PopulateDocTestRatesDT(ByVal dtDocTest As DataTable, ByVal dtRates As DataTable)
        Dim oldCampGrp As String = ""
        Dim oldCampus As String = ""
        Dim oldReqType As String = ""
        Dim row As DataRow
        Dim rows() As DataRow
        Dim numerator As Integer
        Dim denominator As Integer

        For Each dr As DataRow In dtDocTest.Rows
            If oldCampGrp = "" Or oldCampGrp <> dr("CampGrpId").ToString Then
                oldCampGrp = dr("CampGrpId").ToString
                oldCampus = ""
                oldReqType = ""
            End If
            If oldCampus = "" Or oldCampus <> dr("CampusId").ToString Then
                oldCampus = dr("CampusId").ToString
            End If
            If oldReqType = "" Or oldReqType <> dr("ReqType").ToString Then
                oldReqType = dr("ReqType").ToString
                '
                row = dtRates.NewRow
                row("CampGrpId") = oldCampGrp
                row("CampusId") = oldCampus
                row("ReqType") = oldReqType
                rows = dtDocTest.Select("CampGrpId='" & oldCampGrp & "' AND CampusId='" & oldCampus & "' AND ReqType='" & oldReqType & "'")
                If rows.GetLength(0) > 0 Then
                    'Received/Completed
                    numerator = dtDocTest.Compute("SUM(WeeklyCount)", "CampGrpId='" & oldCampGrp & "' AND CampusId='" & oldCampus & "' AND ReqType='" & oldReqType & "' AND ViewOrder=4")
                    'All
                    denominator = dtDocTest.Compute("SUM(WeeklyCount)", "CampGrpId='" & oldCampGrp & "' AND CampusId='" & oldCampus & "' AND ReqType='" & oldReqType & "' AND (ViewOrder=0 OR ViewOrder=4)")
                    If denominator <> 0 Then
                        row("WeeklyCount") = Math.Round(numerator / denominator * 100, 0)
                    Else
                        row("WeeklyCount") = 0
                    End If
                    'Received/Completed
                    numerator = dtDocTest.Compute("SUM(CummCount)", "CampGrpId='" & oldCampGrp & "' AND CampusId='" & oldCampus & "' AND ReqType='" & oldReqType & "' AND ViewOrder=4")
                    'All
                    denominator = dtDocTest.Compute("SUM(CummCount)", "CampGrpId='" & oldCampGrp & "' AND CampusId='" & oldCampus & "' AND ReqType='" & oldReqType & "' AND (ViewOrder=0 OR ViewOrder=4)")
                    If denominator <> 0 Then
                        row("CummCount") = Math.Round(numerator / denominator * 100, 0)
                    Else
                        row("CummCount") = 0
                    End If
                Else
                    row("WeeklyCount") = 0
                    row("CummCount") = 0
                End If
                dtRates.Rows.Add(row)
            End If
        Next
    End Sub

    Private Sub ProcessLeadStatusesDT(ByVal dtStatuses As DataTable)
        Dim oldCampGrp As String = ""
        Dim oldCampus As String = ""
        Dim oldStatus As String = ""
        Dim weeklyEnrolled As Integer = 0
        Dim ytdEnrolled As Integer = 0
        Dim weeklyCancelled As Integer = 0
        Dim ytdCancelled As Integer = 0
        Dim r As DataRow
        Dim row As DataRow
        Dim rows() As DataRow
        Dim dtCopy As DataTable = dtStatuses.Copy()

        For Each dr As DataRow In dtCopy.Rows
            If oldCampGrp = "" Or oldCampGrp <> dr("CampGrpId").ToString Then
                oldCampGrp = dr("CampGrpId").ToString
                oldCampus = ""
                oldStatus = ""
            End If
            If oldCampus = "" Or oldCampus <> dr("CampusId").ToString Then
                oldCampus = dr("CampusId").ToString
                '
                row = dtStatuses.NewRow
                row("CampGrpId") = oldCampGrp
                row("CampusId") = oldCampus
                row("SysStatusId") = 100000
                row("StatusCodeId") = System.Guid.Empty.ToString
                row("StatusCode") = "Starts"
                row("StatusCodeDescrip") = "Starts"

                'Compute Starts = Enrolled - Cancelled
                rows = dtStatuses.Select("CampGrpId='" & oldCampGrp & "' AND CampusId='" & oldCampus & "' AND (SysStatusId=6 OR SysStatusId=8)")
                If rows.GetLength(0) > 0 Then
                    For Each r In rows
                        Select Case r("SysStatusId").ToString
                            Case "6"
                                'Enrolled system status
                                weeklyEnrolled = r("WeeklyCount")
                                ytdEnrolled = r("CummCount")
                            Case "8"
                                'No Start system status
                                weeklyCancelled = r("WeeklyCount")
                                ytdCancelled = r("CummCount")
                        End Select
                    Next
                End If
                If (weeklyEnrolled > weeklyCancelled) Then
                    row("WeeklyCount") = weeklyEnrolled - weeklyCancelled
                Else
                    row("WeeklyCount") = 0
                End If
                If (ytdEnrolled > ytdCancelled) Then
                    row("CummCount") = ytdEnrolled - ytdCancelled
                Else
                    row("CummCount") = 0
                End If
                dtStatuses.Rows.Add(row)
            End If



        Next

    End Sub

#End Region

End Class
