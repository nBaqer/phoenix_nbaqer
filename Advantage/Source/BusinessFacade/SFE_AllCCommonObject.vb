Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllCCommonObject

	Public Const LastEdHS As String = "Schools"

	Public Shared Function PreProcessRawData(ByVal dtStudents As DataTable, ByVal dtStuEnrollInfo As DataTable) As DataTable
		' determine if this is for the detail report
		Dim IsDetailRpt As Boolean = dtStudents.Columns.Contains(RptStuIdentifierColName)

		' create data table to hold processed raw data
		Dim dtRptRaw As New DataTable
		With dtRptRaw.Columns
			If IsDetailRpt Then
				' if this is for the detail report, add columns for student identifier and name
				.Add(RptStuIdentifierColName, GetType(String))
				.Add("LastName", GetType(String))
				.Add("FirstName", GetType(String))
				.Add("MiddleName", GetType(String))
			End If
			.Add("StateDescrip", GetType(String))
			.Add("LastEdInstType", GetType(String))
			.Add("LastEdGradDate", GetType(DateTime))
		End With
		Dim drRptRaw As DataRow

		' create DataView of student enrollment info for use when processing, sorted by StudentId
		Dim dvStuEnrollInfo As New DataView(dtStuEnrollInfo, "", "StudentId", DataViewRowState.CurrentRows)
		Dim RowPtr As Integer

		For Each drStudent As DataRow In dtStudents.Rows
			' find student's record in enrollment info data
			RowPtr = dvStuEnrollInfo.Find(drStudent("StudentId"))

			' only include student if undergraduate, first-time
			If dvStuEnrollInfo(RowPtr)("ProgTypeDescrip").ToString <> ProgTypeUndergraduate OrElse _
			   Not dvStuEnrollInfo(RowPtr)("DegCertSeekingDescrip").ToString.StartsWith(DegCertSeekingFirstTime) Then
				Continue For
			End If

			drRptRaw = dtRptRaw.NewRow

			' if this is for the detail report, save student identifier and name
			If IsDetailRpt Then
				drRptRaw(RptStuIdentifierColName) = drStudent(RptStuIdentifierColName)
				drRptRaw("LastName") = drStudent("LastName")
				drRptRaw("FirstName") = drStudent("FirstName")
				drRptRaw("MiddleName") = drStudent("MiddleName")
			End If

			' determine the state to use for final report

			' if a state was entered in the student's original lead record, use that
			If drStudent("StateDescripAddress") IsNot DBNull.Value Then
				drRptRaw("StateDescrip") = drStudent("StateDescripAddress")

			' if a driver's license state was entered in the student's original 
			'	lead record, use that
			ElseIf drStudent("StateDescripDL") IsNot DBNull.Value Then
				drRptRaw("StateDescrip") = drStudent("StateDescripDL")

			' if no state or driver's license state was entered in the student's
			'	original lead record, and a partial address was entered, and the 
			'	address is not foreign, use "State Unknown"
			ElseIf drStudent("StateDescripAddress") Is DBNull.Value And _
				   drStudent("StateDescripDL") Is DBNull.Value And _
				   drStudent("Address1") IsNot DBNull.Value And _
				   Not CBool(drStudent("ForeignZip")) Then
				drRptRaw("StateDescrip") = StateUnknownDescrip

			' if no state or driver's license state was entered in the student's
			'	original lead record, and the address is foreign, use  
			'	"Foreign Country"
			ElseIf drStudent("StateDescripAddress") Is DBNull.Value And _
				   drStudent("StateDescripDL") Is DBNull.Value And _
				   CBool(drStudent("ForeignZip")) Then
				drRptRaw("StateDescrip") = StateForeignCountryDescrip

			' if no state or driver's license state was entered in the student's
			'	original lead record, no partial address was entered, and the 
			'	address is not foreign, use "Unreported"
			Else
			'drStudent("StateDescripAddress") Is DBNull.Value And _
			'drStudent("StateDescripDL") Is DBNull.Value And _
			'Not CBool(drStudent("ForeignZip")) And _
			'drStudent("Address1") Is DBNull.Value
				drRptRaw("StateDescrip") = StateResUnreportedDescrip
			End If

			' save Student's last previous education type and date
			drRptRaw("LastEdInstType") = drStudent("LastEdInstType")
			drRptRaw("LastEdGradDate") = drStudent("LastEdGradDate")

			' add processed raw DataRow to raw DataTable
			dtRptRaw.Rows.Add(drRptRaw)
		Next

		' return copy of processed raw data
		Return dtRptRaw.Copy

	End Function

End Class
