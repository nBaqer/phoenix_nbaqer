

Public Class TransferStudentsToAnotherProgramFacade
    Public Function GetCommonCoursesAndGrades(ByVal StudentId As String, ByVal SourcePrgVerId As String, ByVal TargetPrgVerId As String, Optional ByVal Campusid As String = "", Optional ByVal GetScheduledClasses As Boolean = False) As DataSet
        Return (New TransferStudentsToAnotherProgramDB).GetCommonCoursesAndGrades(StudentId, SourcePrgVerId, TargetPrgVerId, Campusid, GetScheduledClasses)
    End Function
    Public Function IsStudentAlreadyEnrolled(ByVal StudentId As String, ByVal PrgVerId As String) As Integer
        Return (New TransferStudentsToAnotherProgramDB).IsStudentAlreadyEnrolled(StudentId, PrgVerId)
    End Function
    Public Function GetStudentResults(ByVal LastName As String, ByVal FirstName As String, ByVal SSN As String, ByVal DOB As String, ByVal CampusId As String, ByVal PrgVerId As String) As DataSet
        Dim ds As DataSet
        ds = (New TransferStudentsToAnotherProgramDB).StudentSearchResults(LastName, FirstName, SSN, DOB, CampusId, PrgVerId)
        ds = ConcatenateStudentNames(ds)
        Return ds
    End Function
    Public Function ConcatenateStudentNames(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables(0)
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("StudentId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                row("FullName") = row("FirstName")
                If Not row("MiddleName").ToString.Length < 1 And Not row("MiddleName") Is DBNull.Value Then
                    row("FullName") &= " " & row("MiddleName")
                End If
                row("FullName") &= " " & row("LastName")
                If Not row("SSN").ToString.Length < 1 And Not row("SSN") Is DBNull.Value Then
                    row("FullName") &= " (" & row("SSN") & ")"
                End If
            Next
        End If
        Return ds
    End Function
    Public Function GetSourceCampusId(ByVal CampusId As String) As String
        Return (New TransferStudentsToAnotherProgramDB).GetSourceCampusId(CampusId)
    End Function
    Public Function GetAllCampusId() As DataSet
        Return (New TransferStudentsToAnotherProgramDB).GetAllCampusId()
    End Function
    Public Function GetTargetCampusId(ByVal UserId As String) As DataSet
        Return (New TransferStudentsToAnotherProgramDB).GetTargetCampusId(UserId)
    End Function
    Public Function GetProgram(ByVal CampusId As String, ByVal UserId As String) As DataSet
        Return (New TransferStudentsToAnotherProgramDB).GetProgram(CampusId, UserId)
    End Function
    Public Function GetSchedules(ByVal CampusId As String, ByVal UserId As String, Optional ByVal TargetPrgVerId As String = "", Optional ByVal SourcePrgVerId As String = "") As DataSet
        Return (New TransferStudentsToAnotherProgramDB).GetSchedules(CampusId, UserId, TargetPrgVerId, SourcePrgVerId)
    End Function
    Public Function GetTargetProgram(ByVal CampusId As String, ByVal UserId As String, Optional ByVal SourceProgramId As String = "") As DataSet
        Return (New TransferStudentsToAnotherProgramDB).GetTargetProgram(CampusId, UserId, SourceProgramId)
    End Function
    Public Function GetSourceProgramVersion(ByVal ProgramId As String, ByVal StudentId As String) As DataSet
        Return (New TransferStudentsToAnotherProgramDB).GetSourceProgramVersion(ProgramId, StudentId)
    End Function
    Public Function GetTargetProgramVersion(ByVal ProgramId As String, ByVal CampusId As String, ByVal SourcePrgVerId As String) As DataSet
        Return (New TransferStudentsToAnotherProgramDB).GetTargetProgramVersion(ProgramId, CampusId, SourcePrgVerId)
    End Function
    Public Function GetProgramVersion(ByVal CampusId As String) As DataSet
        Return (New TransferStudentsToAnotherProgramDB).GetProgramVersion(CampusId)
    End Function
    Public Function GetGradesByResultId(ByVal PrgVerId As String) As DataSet
        Return (New TransferStudentsToAnotherProgramDB).GetGradesByResultId(PrgVerId)
    End Function
    Public Function GetTermsByProgram(ByVal TargetProgramId As String, Optional ByVal CampusId As String = "") As DataSet
        Return (New TransferStudentsToAnotherProgramDB).GetTermsByProgram(TargetProgramId, CampusId)
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="stuEnrollId">The new enrollment ID (new Guid)</param>
    ''' <param name="studentID"></param>
    ''' <param name="campusId"></param>
    ''' <param name="prgVerId"></param>
    ''' <param name="user"></param>
    ''' <param name="sourcePrgVerId"></param>
    ''' <param name="enrollDate"></param>
    ''' <param name="expStartDate"></param>
    ''' <param name="expGradDate"></param>
    ''' <param name="enrollId"></param>
    ''' <param name="sourceCampusId"></param>
    ''' <param name="shiftId"></param>
    ''' <param name="transferHours"></param>
    ''' <returns></returns>
    Public Function InsertStudentEnrollment(stuEnrollId As String, studentID As String, campusId As String, prgVerId As String, user As String, sourcePrgVerId As String, Optional ByVal enrollDate As String = "", Optional ByVal expStartDate As String = "", Optional ByVal expGradDate As String = "", Optional ByVal enrollId As String = "", Optional ByVal sourceCampusId As String = "", Optional ByVal shiftId As String = "", Optional ByVal transferHours As Decimal = 0) As String
        Dim t = New TransferEnrollmentObj()
        'Test first if the student has a enrollment in the destination program id
        Dim studentEnrollment As New TransferStudentsToAnotherProgramDB
        Dim enroll As String = studentEnrollment.GetStudentEnrollmentByProgramVersion(studentID, prgVerId)
        If (String.IsNullOrEmpty(enroll) = False) Then
            Throw New ApplicationException("The Student can not be transfered, because already has a enrollment in the destination Program Version")
        End If

        ' Get the Guid code of the school for future start
        Dim db = New BatchStatusChangeDB
        t.SchoolGuidForFutureStart = db.GetSchoolDefinedStatusGuid(7, campusId)
        Dim campus As String = If(String.IsNullOrEmpty(sourceCampusId), campusId, sourceCampusId)
        t.SchoolGuidForTransferOut = db.GetSchoolDefinedStatusGuid(19, campus)
        t.OldStuEnrollId = db.GetActualEnrollmentByStudentIdAndProgramVersion(studentID, sourcePrgVerId)
        If (String.IsNullOrEmpty(t.OldStuEnrollId)) Then
            Throw New ApplicationException("The Student can not be transfered, because the actual enrollment is not active")
        End If

        t.StuEnrollId = stuEnrollId
        t.SchoolGuidCurrentState = db.GetActualEnrollmentSchoolStatusGuid(t.OldStuEnrollId)
        Dim sysStatus As Integer = db.GetSysStatus(t.SchoolGuidCurrentState)
        'If the enrollment being transferred from is currently attending then we want the new enrollment to be created with currently attending
        If sysStatus = 9 Then
            t.SchoolGuidForFutureStart = t.SchoolGuidCurrentState
        End If

        'Transfer Parameters to object
        t.StudentID = studentID
        t.LeadId = db.GetLeadIdByStudentId(studentID)
        t.PrgVerId = prgVerId
        t.CampusId = campusId
        '' DE 12167  -- The column ScheduleId was removed from arStuEnrollments table so we don't need this information
        ''t.ScheduleID = If(String.IsNullOrEmpty(scheduleID), Nothing, scheduleID)
        t.ShiftId = If(String.IsNullOrEmpty(shiftId), Nothing, shiftId)
        t.SourceCampusId = sourceCampusId
        t.SourcePrgVerId = sourcePrgVerId
        t.StudentID = studentID
        t.TransferHours = transferHours
        t.EnrollId = enrollId ' This is not related to StuEnroll. This is a number for id of the student and not for the system.
        t.User = user
        t.EnrollDate = enrollDate
        t.ExpStartDate = expStartDate
        t.ExpGradDate = expGradDate
        Dim result As String = studentEnrollment.InsertStudentEnrollment(t)
        Return result
    End Function

    Public Function TransferGrades(ByVal StuEnrollId As String, ByVal selectedCourse() As String, ByVal selectedGrade() As String, ByVal selectedTerm() As String, ByVal user As String, ByVal selectedResult() As String, ByVal selectedIsCourseCompleted() As Integer, Optional ByVal StudentId As String = "", Optional ByVal SourcePrgVerId As String = "", Optional ByVal TargetCampusId As String = "", Optional ByVal RemoveRemainingScheduledClasses As Boolean = False) As String      ' , Optional ByVal ScheduleID As String = ""
        Dim TransferGradeDB As New TransferStudentsToAnotherProgramDB
        Return TransferGradeDB.TransferGrades(StuEnrollId, selectedCourse, selectedGrade, selectedTerm, user, selectedResult, selectedIsCourseCompleted, StudentId, SourcePrgVerId, TargetCampusId, "", "", RemoveRemainingScheduledClasses)   '         , ScheduleID
    End Function
    Public Function GetTransferResult(ByVal StuEnrollId As String, ByVal StudentName As String) As String
        Dim TransferGradeDB As New TransferStudentsToAnotherProgramDB
        Return TransferGradeDB.GetTransferResult(StuEnrollId, StudentName)
    End Function
    Public Function GetStudentEnrollmentByProgramVersion(ByVal StudentId As String, ByVal PrgVerId As String) As String
        Dim TransferGradeDB As New TransferStudentsToAnotherProgramDB
        Return TransferGradeDB.GetStudentEnrollmentByProgramVersion(StudentId, PrgVerId)
    End Function
    Public Function IsCommonCourse(ByVal strTargetPrgVerId As String, ByVal adReqId As String) As Boolean
        Return (New TransferStudentsToAnotherProgramDB).IsCommonCourse(strTargetPrgVerId, adReqId)
    End Function
    Public Function GetProgramByProgramVersion(ByVal PrgVerId As String) As DataSet
        Dim TransferGradeDB As New TransferStudentsToAnotherProgramDB
        Return TransferGradeDB.GetProgramByProgramVersion(PrgVerId)
    End Function
    Public Function GetProgramByStuEnrollId(ByVal StuEnrollid As String) As DataSet
        Dim TransferGradeDB As New TransferStudentsToAnotherProgramDB
        Return TransferGradeDB.GetProgramByStuEnrollId(StuEnrollid)
    End Function
    Public Function GetDatesByEnrollment(ByVal StuEnrollId As String) As DataSet
        Dim TransferGradeDB As New TransferStudentsToAnotherProgramDB
        Return TransferGradeDB.GetDatesByEnrollment(StuEnrollId)
    End Function
    Public Function GetTransferResultDS(ByVal StuEnrollId As String) As DataSet
        Dim TransferGradeDB As New TransferStudentsToAnotherProgramDB
        Return TransferGradeDB.GetTransferResultDS(StuEnrollId)
    End Function

    Public Function IsProgramClockHourType(progId As String) As Boolean
        Dim db As New TransferStudentsToAnotherProgramDB
        Return db.IsProgramClockHourType(progId)
    End Function
End Class