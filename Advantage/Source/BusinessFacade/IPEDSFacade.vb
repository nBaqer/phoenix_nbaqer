
Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.Advantage.Common

Public Class IPEDSFacade
    ' Business facade layer functions used by IPEDS reports

    Public Shared Function GetSchoolRptTypes() As DataTable
        ' Get info on IPEDS School Reporting Types

        Return IPEDSDB.GetSchoolRptTypes
    End Function

    Public Shared Sub GetRptDatesRptType(ByVal SchoolRptType As String, ByVal Year As Integer, ByRef dateStart As Date, ByRef dateEnd As Date, ByRef dateRptEnd As Date)
        ' Utility function used by report parameter pages to set report/cohort start and end dates as per the 
        '	passed-in School Reporting Type and Cohort Year

        Select Case SchoolRptType
            Case SchoolProgramReporter
                dateEnd = CDate("10/31/" & Year)
                dateStart = CDate(dateEnd.Month - 2 & "/1/" & Year)

            Case SchoolAcademicYearReporter
                dateStart = CDate("9/1/" & Year)
                dateEnd = CDate("12/15/" & Year)
                If Not IsNothing(dateRptEnd) Then
                    dateRptEnd = CDate("10/15/" & Year)
                End If
        End Select
    End Sub

    Public Shared Function GetIPEDSCohortTypes() As DataTable

        Return IPEDSDB.GetIPEDSCohortTypes
    End Function

    Public Shared Function GetIPEDSEthnicInfo() As DataTable
        Return IPEDSDB.GetIPEDSEthnicInfo
    End Function

    Public Shared Function GetIPEDSGenderInfo() As DataTable
        Return IPEDSDB.GetIPEDSGenderInfo
    End Function

    Public Shared Function GetIPEDSAgeCatInfo() As DataTable
        Return IPEDSDB.GetIPEDSAgeCatInfo
    End Function

    Public Shared Function GetAllProgramsByType(ByVal ProgType As String) As DataTable
        Return IPEDSDB.GetAllProgramsByType(ProgType)
    End Function

    Public Shared Function GetAuditHistRecords(Optional ByVal TableNames As String = Nothing, _
                 Optional ByVal EventTypes As AuditHistEvents = Nothing, _
                 Optional ByVal EventDateFilter As String = Nothing, _
                 Optional ByVal RowIds As String = Nothing, _
                 Optional ByVal ColumnNames As String = Nothing, _
                 Optional ByVal OldValues As String = Nothing, _
                 Optional ByVal NewValues As String = Nothing) As DataTable

        Return IPEDSDB.GetAuditHistRecords(TableNames, EventTypes, EventDateFilter, RowIds, ColumnNames, OldValues, NewValues)
    End Function

    Public Shared Sub SetStudentInfo(ByVal StudentInfo As Object, ByRef drRpt As DataRow)
        Dim StudentIdentifier As String = StudentInfo(RptStuIdentifierColName).ToString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If MyAdvAppSettings.AppSettings("StudentIdentifier") = "SSN" Then
            ' if student identifier is set to SSN, apply input mask to db value
            Dim FacInputMasks As New InputMasksFacade
            Dim SSNMask As String = FacInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
            'drRpt(RptStuIdentifierColName) = FacInputMasks.ApplyMask(SSNMask, StudentIdentifier)
            Dim temp As String = StudentIdentifier
            drRpt(RptStuIdentifierColName) = FacInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
        Else
            ' otherwise just get db value, but limit to standard size
            drRpt(RptStuIdentifierColName) = Left(StudentIdentifier, StudentIdSize)
        End If

        ' Last/First/Middle names must be limited to standard number of characters
        drRpt("Name") = Left(StudentInfo("LastName").ToString, LastNameSize) & ", " & _
            Left(StudentInfo("FirstName").ToString, FirstNameSize) & " " & _
            Left(StudentInfo("MiddleName").ToString, MiddleNameSize)
    End Sub

    Public Shared Function GetIPEDSFldValues(ByVal FldName As String) As DataTable
        Return IPEDSDB.GetIPEDSFldValues(FldName)
    End Function

    Public Shared Function GetProcessedEnrollInfo(ByVal dsRaw As DataSet) As DataTable
        ' Used by reports needing to group, sort or filter students by Program Type / Attendance Type / 
        '	Degree-seeking status, to pre-process raw data.  Returns DataTable containing list of students 
        '	classified by Program Type, Attendance Type, and Degree-seeking status, using appropriate 
        '	precedances for multiple student enrollments.

        ' This routine assumes the passed-in DataSet contains tables "Students" and "Enrollments", 
        '	and assumes specific columns are present in each.

        ' create DataTable to hold pre-processed raw data
        Dim dtEnrollInfo As New DataTable
        With dtEnrollInfo.Columns
            .Add("StudentId", GetType(System.Guid))
            .Add("LeadId", GetType(System.Guid))
            .Add("ProgTypeDescrip", GetType(System.String))
            .Add("AttendTypeDescrip", GetType(System.String))
            .Add("DegCertSeekingDescrip", GetType(System.String))
        End With

        Dim drStudent As DataRow, drEnrollInfo As DataRow

        Dim dvEnrollments As New DataView(dsRaw.Tables(TblNameEnrollments), "", _
                  "StudentId", DataViewRowState.CurrentRows)
        Dim RowPtr As Integer

        For Each drStudent In dsRaw.Tables(TblNameStudents).Rows
            drEnrollInfo = dtEnrollInfo.NewRow

            drEnrollInfo("StudentId") = drStudent("StudentId")

            RowPtr = dvEnrollments.Find(drStudent("StudentId"))
            drEnrollInfo("LeadId") = dvEnrollments(RowPtr)("LeadId")

            ' classify student by Program Type, Attendance Type, and Degree-seeking status - 
            '	code below is structured to reflect proper precedances for multiple enrollments

            ' determine if student is a first professional, full time
            If ChkEnrollments(drStudent("StudentId").ToString, _
                  ProgTypeFirstProf, AttendTypeFullTime, "", dsRaw) Then
                drEnrollInfo("ProgTypeDescrip") = ProgTypeFirstProf
                drEnrollInfo("AttendTypeDescrip") = AttendTypeFullTime
                drEnrollInfo("DegCertSeekingDescrip") = DBNull.Value

                ' determine if student is a first professional, part time
            ElseIf ChkEnrollments(drStudent("StudentId").ToString, _
                  ProgTypeFirstProf, AttendTypePartTime, "", dsRaw) Then
                drEnrollInfo("ProgTypeDescrip") = ProgTypeFirstProf
                drEnrollInfo("AttendTypeDescrip") = AttendTypePartTime
                drEnrollInfo("DegCertSeekingDescrip") = DBNull.Value

                ' determine if student is a graduate student, full time
            ElseIf ChkEnrollments(drStudent("StudentId").ToString, _
                   ProgTypeGraduate, AttendTypeFullTime, "", dsRaw) Then
                drEnrollInfo("ProgTypeDescrip") = ProgTypeGraduate
                drEnrollInfo("AttendTypeDescrip") = AttendTypeFullTime
                drEnrollInfo("DegCertSeekingDescrip") = DBNull.Value

                ' determine if student is a graduate student, part time
            ElseIf ChkEnrollments(drStudent("StudentId").ToString, _
                   ProgTypeGraduate, AttendTypePartTime, "", dsRaw) Then
                drEnrollInfo("ProgTypeDescrip") = ProgTypeGraduate
                drEnrollInfo("AttendTypeDescrip") = AttendTypePartTime
                drEnrollInfo("DegCertSeekingDescrip") = DBNull.Value

                ' determine if student is undergraduate, full time, first-time
            ElseIf ChkEnrollments(drStudent("StudentId").ToString, _
                   ProgTypeUndergraduate, AttendTypeFullTime, _
                   DegCertSeekingFirstTime & "%", dsRaw) Then
                drEnrollInfo("ProgTypeDescrip") = ProgTypeUndergraduate
                drEnrollInfo("AttendTypeDescrip") = AttendTypeFullTime
                drEnrollInfo("DegCertSeekingDescrip") = DegCertSeekingFirstTime

                ' determine if student is undergraduate, full time, other degree/cert seeking
            ElseIf ChkEnrollments(drStudent("StudentId").ToString, _
                   ProgTypeUndergraduate, AttendTypeFullTime, _
                   DegCertSeekingOtherDegree, dsRaw) Then
                drEnrollInfo("ProgTypeDescrip") = ProgTypeUndergraduate
                drEnrollInfo("AttendTypeDescrip") = AttendTypeFullTime
                drEnrollInfo("DegCertSeekingDescrip") = DegCertSeekingOtherDegree

                ' determine if student is undergraduate, full time, non-degree/cert seeking
            ElseIf ChkEnrollments(drStudent("StudentId").ToString, _
                   ProgTypeUndergraduate, AttendTypeFullTime, _
                   DegCertSeekingNonDegree, dsRaw) Then
                drEnrollInfo("ProgTypeDescrip") = ProgTypeUndergraduate
                drEnrollInfo("AttendTypeDescrip") = AttendTypeFullTime
                drEnrollInfo("DegCertSeekingDescrip") = DegCertSeekingNonDegree

                ' determine if student is undergraduate, part time, first-time
            ElseIf ChkEnrollments(drStudent("StudentId").ToString, _
                   ProgTypeUndergraduate, AttendTypePartTime, _
                   DegCertSeekingFirstTime & "%", dsRaw) Then
                drEnrollInfo("ProgTypeDescrip") = ProgTypeUndergraduate
                drEnrollInfo("AttendTypeDescrip") = AttendTypePartTime
                drEnrollInfo("DegCertSeekingDescrip") = DegCertSeekingFirstTime

                ' determine if student is undergraduate, part time, other degree/cert seeking
            ElseIf ChkEnrollments(drStudent("StudentId").ToString, _
                   ProgTypeUndergraduate, AttendTypePartTime, _
                   DegCertSeekingOtherDegree, dsRaw) Then
                drEnrollInfo("ProgTypeDescrip") = ProgTypeUndergraduate
                drEnrollInfo("AttendTypeDescrip") = AttendTypePartTime
                drEnrollInfo("DegCertSeekingDescrip") = DegCertSeekingOtherDegree

                ' determine if student is undergraduate, part time, non-degree/cert seeking
            ElseIf ChkEnrollments(drStudent("StudentId").ToString, _
                   ProgTypeUndergraduate, AttendTypePartTime, _
                   DegCertSeekingNonDegree, dsRaw) Then
                drEnrollInfo("ProgTypeDescrip") = ProgTypeUndergraduate
                drEnrollInfo("AttendTypeDescrip") = AttendTypePartTime
                drEnrollInfo("DegCertSeekingDescrip") = DegCertSeekingNonDegree

            End If

            ' add row to pre-processed raw data
            dtEnrollInfo.Rows.Add(drEnrollInfo)
        Next

        ' return copy of pre-processed raw data
        Return dtEnrollInfo.Copy

    End Function

    Private Shared Function ChkEnrollments(ByVal StudentId As String, ByVal ProgTypeDescrip As String, ByVal AttendTypeDescrip As String, ByVal DegCertSeekingDescrip As String, ByVal dsRaw As DataSet) As Boolean
        ' Check student's records in list of enrollments for specific Program Type / Attendance Type / Degree-seeking status

        Dim FilterString As String

        ' first apply filters for specific student and passed-in Program Type
        FilterString = "StudentId = '" & StudentId & "' AND " & _
              "ProgTypeDescrip LIKE '" & ProgTypeDescrip & "'"

        ' apply filter for passed-in Attendance Type, if any
        If AttendTypeDescrip <> "" Then
            FilterString &= " AND AttendTypeDescrip LIKE '" & AttendTypeDescrip & "'"
        End If

        ' apply filter for passed-in Degree-seeking Status, if any
        If DegCertSeekingDescrip <> "" Then
            FilterString &= " AND DegCertSeekingDescrip LIKE '" & DegCertSeekingDescrip & "'"
        End If

        ' if the count of records matching the filter is > 0, the student has enrollments matching the
        '	passed in criteria
        Return CInt(dsRaw.Tables(TblNameEnrollments).Compute("Count(ProgTypeDescrip)", FilterString)) > 0
    End Function

    Public Shared Sub SetRptStuListParams(ByRef RptParamInfo As ReportParamInfoIPEDS)
        With RptParamInfo
            Select Case .ResourceId
                Case RptId_PartADetail, RptId_PartASummary, _
                  RptId_PartBDetail, RptId_PartBSummary, _
                  RptId_PartD
                    .StuList_InclNonDegCertSeeking = True
                    .StuList_InclPartTime = True
                    .StuList_InclTransferIn = True

                Case RptId_PartCDetail, RptId_PartCSummary
                    .StuList_InclPartTime = True
                    .StuList_InclTransferIn = True

                Case RptId_PartG
                    .StuList_InclPartTime = True
                    'Added by Balaji on 11/3/2006 to 
                Case 427, 428
                    .StuList_InclPartTime = True
                    ' future code to handle other reports
                Case 444, 445, 783   'US3158
                    .StuList_InclNonDegCertSeeking = True
                    .StuList_InclPartTime = True
                Case 459
                    .StuList_InclPartTime = True
                    .StuList_InclNonDegCertSeeking = False
            End Select
        End With
    End Sub

    Public Shared Function GetProgramDescripList(ByVal FilterProgramDescrips As String) As String()
        Dim ProgListSep As String = Chr(127).ToString
        Return FilterProgramDescrips.Replace(vbCrLf, ProgListSep).Split(ProgListSep)
    End Function
    'US3158
    Public Shared Function GetProgramMisingCIPCodebyProgAndCampus(ByVal CampusId As String,
                                                                  Optional ByVal ProgId As String = Nothing,
                                                                  Optional bCheckCIP As Boolean = True) As DataSet
        Return (New IPEDSDB).GetProgramMisingCIPCodebyProgAndCampus(CampusId, ProgId, bCheckCIP)
    End Function
End Class