Public Class DefRevenueSummaryObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim objDB As New StuAccountBalanceDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(objDB.GetDeferredRevenueSummary(rptParamInfo), objDB.StudentIdentifier)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim strName As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String

        'Get mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count = 4 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dtDefRevenue As DataTable = ds.Tables("DeferredRevenueSummary")
                    Dim dtCGTtls As DataTable = ds.Tables("CampusGroupTotals")
                    Dim dtCTtls As DataTable = ds.Tables("CampusTotals")
                    Dim dtPVTtls As DataTable = ds.Tables("PrgVerTotals")
                    Dim oldCmpGrp As String = ""
                    Dim oldCampus As String = ""
                    Dim oldPV As String = ""
                    Dim rr As DataRow
                    '
                    For Each dr As DataRow In dtDefRevenue.Rows
                        'Compute Total Amounts per Campus Groups and Campuses.
                        'CampGrpDescrip and CampDescrip columns cannot be NULL.
                        If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpId").ToString Then
                            'Get a row everytime there is a Campus Group.
                            rr = dtCGTtls.NewRow
                            oldCmpGrp = dr("CampGrpId").ToString
                            rr("CampGrpId") = oldCmpGrp
                            rr("PreviousCost") = dtDefRevenue.Compute("SUM(PreviousCost)", "CampGrpId='" & oldCmpGrp & "'")
                            rr("PreviousAccrualBal") = dtDefRevenue.Compute("SUM(PreviousAccrualBal)", "CampGrpId='" & oldCmpGrp & "'")
                            rr("NewCosts") = dtDefRevenue.Compute("SUM(NewCosts)", "CampGrpId='" & oldCmpGrp & "'")
                            rr("TotalCost") = dtDefRevenue.Compute("SUM(TotalCost)", "CampGrpId='" & oldCmpGrp & "'")
                            rr("CurrentAccrual") = dtDefRevenue.Compute("SUM(CurrentAccrual)", "CampGrpId='" & oldCmpGrp & "'")
                            rr("BalanceForward") = dtDefRevenue.Compute("SUM(BalanceForward)", "CampGrpId='" & oldCmpGrp & "'")
                            dtCGTtls.Rows.Add(rr)
                            oldCampus = ""
                            oldPV = ""
                        End If
                        If oldCampus = "" Or oldCampus <> dr("CampusId").ToString Then
                            'For every different Campus, get a new row and add it to dt table.
                            'This section of the code will be used when a Campus Group has several Campuses.
                            oldCampus = dr("CampusId").ToString
                            rr = dtCTtls.NewRow
                            rr("CampGrpId") = oldCmpGrp
                            rr("CampusId") = oldCampus
                            rr("PreviousCost") = dtDefRevenue.Compute("SUM(PreviousCost)", "CampGrpId='" & oldCmpGrp & "' AND CampusId='" & oldCampus & "'")
                            rr("PreviousAccrualBal") = dtDefRevenue.Compute("SUM(PreviousAccrualBal)", "CampGrpId='" & oldCmpGrp & "' AND CampusId='" & oldCampus & "'")
                            rr("NewCosts") = dtDefRevenue.Compute("SUM(NewCosts)", "CampGrpId='" & oldCmpGrp & "' AND CampusId='" & oldCampus & "'")
                            rr("TotalCost") = dtDefRevenue.Compute("SUM(TotalCost)", "CampGrpId='" & oldCmpGrp & "' AND CampusId='" & oldCampus & "'")
                            rr("CurrentAccrual") = dtDefRevenue.Compute("SUM(CurrentAccrual)", "CampGrpId='" & oldCmpGrp & "' AND CampusId='" & oldCampus & "'")
                            rr("BalanceForward") = dtDefRevenue.Compute("SUM(BalanceForward)", "CampGrpId='" & oldCmpGrp & "' AND CampusId='" & oldCampus & "'")
                            dtCTtls.Rows.Add(rr)
                        End If
                        If oldPV = "" Or oldPV <> dr("PrgVerId").ToString Then
                            oldPV = dr("PrgVerId").ToString
                            rr = dtPVTtls.NewRow
                            rr("CampGrpId") = oldCmpGrp
                            rr("CampusId") = oldCampus
                            rr("PrgVerId") = oldPV
                            rr("PreviousCost") = dtDefRevenue.Compute("SUM(PreviousCost)", "CampGrpId='" & oldCmpGrp & "' AND CampusId='" & oldCampus & "' AND PrgVerId='" & oldPV & "'")
                            rr("PreviousAccrualBal") = dtDefRevenue.Compute("SUM(PreviousAccrualBal)", "CampGrpId='" & oldCmpGrp & "' AND CampusId='" & oldCampus & "' AND PrgVerId='" & oldPV & "'")
                            rr("NewCosts") = dtDefRevenue.Compute("SUM(NewCosts)", "CampGrpId='" & oldCmpGrp & "' AND CampusId='" & oldCampus & "' AND PrgVerId='" & oldPV & "'")
                            rr("TotalCost") = dtDefRevenue.Compute("SUM(TotalCost)", "CampGrpId='" & oldCmpGrp & "' AND CampusId='" & oldCampus & "' AND PrgVerId='" & oldPV & "'")
                            rr("CurrentAccrual") = dtDefRevenue.Compute("SUM(CurrentAccrual)", "CampGrpId='" & oldCmpGrp & "' AND CampusId='" & oldCampus & "' AND PrgVerId='" & oldPV & "'")
                            rr("BalanceForward") = dtDefRevenue.Compute("SUM(BalanceForward)", "CampGrpId='" & oldCmpGrp & "' AND CampusId='" & oldCampus & "' AND PrgVerId='" & oldPV & "'")
                            dtPVTtls.Rows.Add(rr)
                        End If

                        'Set up student name as: "LastName, FirstName MI."
                        strName = ""
                        If Not (dr("LastName") Is System.DBNull.Value) Then
                            If dr("LastName") <> "" Then
                                strName = dr("LastName")
                            End If
                        End If
                        If Not (dr("FirstName") Is System.DBNull.Value) Then
                            If dr("FirstName") <> "" Then
                                If strName <> "" Then
                                    strName &= ", " & dr("FirstName")
                                Else
                                    strName &= dr("FirstName")
                                End If
                            End If
                        End If
                        If Not (dr("MiddleName") Is System.DBNull.Value) Then
                            If dr("MiddleName") <> "" Then
                                If strName <> "" Then
                                    strName &= " " & dr("MiddleName") & "."
                                Else
                                    strName &= dr("MiddleName") & "."
                                End If
                            End If
                        End If
                        dr("StudentName") = strName
                        '
                        'Apply mask to SSN.
                        If StudentIdentifier = "SSN" Then
                            If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                                If dr("StudentIdentifier") <> "" Then
                                    Dim temp As String = dr("StudentIdentifier")
                                    dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                End If
                            End If
                        End If
                        '
                        dr("RecCount") = dtDefRevenue.Rows.Count
                    Next
                End If
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
