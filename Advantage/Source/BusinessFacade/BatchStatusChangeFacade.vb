Imports FAME.AdvantageV1.BusinessFacade.StudentEnrollState

Public Class BatchStatusChangeFacade

    ''' <summary>
    ''' This is use only by Batch Status Change
    ''' </summary>
    ''' <param name="campusid"></param>
    ''' <param name="userName"></param>
    ''' <returns></returns>
    ''' <remarks>Transfer Out state is excluded (19)</remarks>
    Public Function GetActiveInactiveStatusesForBatchChanges(ByVal campusid As String, Optional ByVal userName As String = "") As DataSet
        Return (New BatchStatusChangeDB).GetActiveInactiveStatusesForBatchChanges(campusid, userName)
    End Function

    Public Function GetStatuses(ByVal statusCodeId As String, ByVal campusid As String, Optional ByVal user As String = "") As DataTable
        Dim obj As New BatchStatusChangeDB
        Dim enumType As StateEnum
        Dim arrToStatuses As ArrayList
        Dim strStatusList As String

        enumType = obj.GetAdvantageSystemStatus(statusCodeId, campusid)

        Select Case enumType
            Case StateEnum.FutureStart
                arrToStatuses = (New BatchChangeFromFutureStart).ToStatuses
            Case StateEnum.LeaveOfAbsence
                arrToStatuses = (New BatchChangeFromLeaveOfAbsence).ToStatuses
            Case StateEnum.CurrentlyAttending
                arrToStatuses = (New BatchChangeFromCurrentlyAttending).ToStatuses
            Case StateEnum.Dropped
                arrToStatuses = (New BatchChangeFromDropped).ToStatuses
            Case StateEnum.TransferOut
                arrToStatuses = (New BatchChangeFromDropped).ToStatuses
            Case StateEnum.NoStart
                arrToStatuses = (New BatchChangeFromNoStart).ToStatuses
            Case StateEnum.Suspended
                arrToStatuses = (New BatchChangeFromSuspended).ToStatuses
            Case StateEnum.Graduated
                arrToStatuses = (New BatchChangeFromGraduated).ToStatuses
            Case StateEnum.Externship
                Dim externshipinstance As New BatchChangeFromExternship(user)
                arrToStatuses = externshipinstance.ToStatuses
            Case StateEnum.AcademicProbation
                Dim boolIsUserDirectorofAcademics As Boolean
                boolIsUserDirectorofAcademics = (New LeadDB).IsDirectorofAcademic(user)
                'only director of academics can change the status from academic probation to currently attending
                If boolIsUserDirectorofAcademics = True Then
                    arrToStatuses = (New BatchChangeFromAcademicProbation).ToStatuses
                End If

        End Select

        For i As Integer = 0 To arrToStatuses.Count - 1
            strStatusList &= arrToStatuses.Item(i) & ","
        Next
        '   remove last comma
        strStatusList = strStatusList.Substring(0, strStatusList.Length - 1)

        '   pass comma separated list
        Return obj.GetStatuses(strStatusList, campusid, StatusCodeId)
    End Function
    Public Function GetStatuses_New(ByVal statusCodeId As String, ByVal campusid As String, Optional ByVal user As String = "") As DataTable
        Dim obj As New BatchStatusChangeDB
        Dim enumType As StateEnum
        Dim arrToStatuses As ArrayList
        Dim strStatusList As String

        enumType = obj.GetAdvantageSystemStatus(statusCodeId, campusid)

        Select Case enumType
            Case StateEnum.FutureStart
                arrToStatuses = (New BatchChangeFromFutureStart).ToStatuses
            Case StateEnum.LeaveOfAbsence
                arrToStatuses = (New BatchChangeFromLeaveOfAbsence).ToStatuses
            Case StateEnum.CurrentlyAttending
                arrToStatuses = (New BatchChangeFromCurrentlyAttending).ToStatuses
            Case StateEnum.Dropped
                arrToStatuses = (New BatchChangeFromDropped).ToStatuses
            Case StateEnum.TransferOut
                arrToStatuses = (New BatchChangeFromTransferred).ToStatuses
            Case StateEnum.NoStart
                arrToStatuses = (New BatchChangeFromNoStart).ToStatuses
            Case StateEnum.Suspended
                arrToStatuses = (New BatchChangeFromSuspended).ToStatuses
            Case StateEnum.Graduated
                arrToStatuses = (New BatchChangeFromGraduated).ToStatuses
            Case StateEnum.Externship
                Dim externshipinstance As New BatchChangeFromExternship(user)
                arrToStatuses = externshipinstance.ToStatuses
            Case StateEnum.AcademicProbation
                Dim boolIsUserDirectorofAcademics As Boolean
                boolIsUserDirectorofAcademics = (New LeadDB).IsDirectorofAcademic(user)
                'only director of academics can change the status from academic probation to currently attending
                If boolIsUserDirectorofAcademics = True Then
                    arrToStatuses = (New BatchChangeFromAcademicProbation).ToStatuses
                End If

        End Select

        For i As Integer = 0 To arrToStatuses.Count - 1
            strStatusList &= arrToStatuses.Item(i) & ","
        Next
        '   remove last comma
        strStatusList = strStatusList.Substring(0, strStatusList.Length - 1)

        '   pass comma separated list
        Return obj.GetStatuses(strStatusList, campusid, StatusCodeId)
    End Function

    Public Function GetStatuses(ByVal enumType As StateEnum, ByVal campusid As String) As DataTable
        Dim obj As New BatchStatusChangeDB
        'Dim enumType As StateEnum
        Dim arrToStatuses As ArrayList
        Dim strStatusList As String

        Select Case enumType
            Case StateEnum.FutureStart
                arrToStatuses = (New BatchChangeFromFutureStart).ToStatuses
            Case StateEnum.LeaveOfAbsence
                arrToStatuses = (New BatchChangeFromLeaveOfAbsence).ToStatuses
            Case StateEnum.CurrentlyAttending
                arrToStatuses = (New BatchChangeFromCurrentlyAttending).ToStatuses
            Case StateEnum.Dropped
                arrToStatuses = (New BatchChangeFromDropped).ToStatuses
            Case StateEnum.TransferOut
                arrToStatuses = (New BatchChangeFromDropped).ToStatuses
            Case StateEnum.NoStart
                arrToStatuses = (New BatchChangeFromNoStart).ToStatuses
            Case StateEnum.Suspended
                arrToStatuses = (New BatchChangeFromSuspended).ToStatuses
            Case StateEnum.Graduated
                arrToStatuses = (New BatchChangeFromGraduated).ToStatuses
                ' Case StateEnum.ReEntry
                '   arrToStatuses = (New BatchChangeFromCurrentlyAttending).ToStatuses
        End Select

        For i As Integer = 0 To arrToStatuses.Count - 1
            strStatusList &= arrToStatuses.Item(i) & ","
        Next
        '   remove last comma
        strStatusList = strStatusList.Substring(0, strStatusList.Length - 1)

        '   pass comma separated list
        Return obj.GetStatuses(strStatusList, campusid)
    End Function
    Public Function GetSysStatus(ByVal statusCodeId As String) As Integer
        Return (New BatchStatusChangeDB).GetSysStatus(statusCodeId)
    End Function
 
    Private Sub MassageStudentTable(ByRef dt As DataTable)
        'Massage datatable
        dt.TableName = "StudentEnrollments"
        dt.Columns.Add("LOAPeriod", Type.GetType("System.String"))
        dt.Columns.Add("SuspensionPeriod", Type.GetType("System.String"))
        Dim dc As DataColumn = dt.Columns.Add("StudentName", Type.GetType("System.String"))
        dc.AllowDBNull = True
        dc.Expression = "LastName + ', ' + FirstName"

        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String

        'Get mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr As DataRow In dt.Rows
            'Apply mask on SSN
            If Not dr.IsNull("SSN") Then
                If dr("SSN") <> "" Then
                    'dr("SSN") = facInputMasks.ApplyMask(strSSNMask, dr("SSN"))
                    Dim temp As String = dr("SSN")
                    dr("SSN") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                End If
            End If
            If Not dr.IsNull("LOAStartDate") And Not dr.IsNull("LOAEndDate") Then
                dr("LOAPeriod") = Convert.ToDateTime(dr("LOAStartDate")).ToShortDateString & "-" & Convert.ToDateTime(dr("LOAEndDate")).ToShortDateString
            End If
            If Not dr.IsNull("SusStartDate") And Not dr.IsNull("SusEndDate") Then
                dr("SuspensionPeriod") = Convert.ToDateTime(dr("SusStartDate")).ToShortDateString & "-" & Convert.ToDateTime(dr("SusEndDate")).ToShortDateString
            End If
        Next
    End Sub

   
    Public Function UpdateStuEnrollmentStatusInBatch(ByVal statusChangeList As List(Of StudentChangeHistoryObj)) As String

        Dim enrollState As IStudentEnrollmentState
        Dim result As String
        Dim objDB As New StatusChangeHistoryDB

        For Each change In statusChangeList
            'Fill Object of change 
            'Dim change = New StudentChangeHistoryObj()
            enrollState = StateFactory.CreateStateObj(change.OldSysStatusId)

            change.OrigStatusGuid = TerminateFacade.GetStudentStatusId(change.StuEnrollId)
            change.Lda = objDB.GetLdaDate(change.StuEnrollId)

            Try
                Select Case change.NewSysStatusId

                    Case StateEnum.NoStart
                        result = enrollState.ChangeStatusToNoStart(change)

                    Case StateEnum.Graduated
                        change.ExpGradDate = change.DateOfChange
                        result = enrollState.ChangeStatusToGraduated(change)

                    Case StateEnum.FutureStart
                        If (change.OldSysStatusId = 12 Or change.OldSysStatusId = 19) Then
                            change.DateOfReenrollment = change.DateOfChange
                            result = enrollState.ChangeStatusToFutureStartNew(change)
                        ElseIf change.OldSysStatusId = 9 Then
                            result = enrollState.ChangeStatusToFutureStart(change)
                        Else
                            change.DateDetermined = Nothing
                            change.StudentStatus = AdvantageCommonValues.ActiveGuid
                            change.DropReasonGuid = Nothing
                            change.Lda = Nothing
                            change.DateOfReenrollment = change.DateOfChange
                            result = enrollState.ChangeStatusToFutureStart(change)
                        End If


                    Case StateEnum.CurrentlyAttending           ' 9
                        change.DropReasonGuid = Nothing
                        change.DateOfReenrollment = change.DateOfChange
                        'change.ExpGradDate = changeDate(i)

                        result = enrollState.ChangeStatusToCurrAttending(change)
                        'result = enrollState.ChangeStatusToCurrAttending(arrStuEnrollId(i), user, statusCodeId, changeDate(i))

                    Case StateEnum.AcademicProbation            ' 20
                        change.DropReasonGuid = Nothing
                        change.DateOfReenrollment = change.DateOfChange
                        'change.ExpGradDate = changeDate(i)
                        result = enrollState.PlaceStudentOnProbation(change)
   
                    Case StateEnum.TransferOut
                        result = enrollState.ChangeStatusToTransferOut(change)
                        'result = enrollState.ChangeStatusToTransferOut(arrStuEnrollId(i), user, statusCodeId)

                    Case StateEnum.Externship
                        result = enrollState.ChangeStatusToExternship(change)

                    Case StateEnum.Suspended
                        result = enrollState.PlaceStudentOnSuspension(change)

                    Case StateEnum.LeaveOfAbsence
                        Dim suspensionstatusid As String = String.Empty
                        ' US2135 01/05/2012 Janet Robinson added RequestDate

                        'change.NewStatusId = suspensionstatusid

                        'change.LOARequestDate = requestDate
                        result = enrollState.PlaceStudentOnLOA(change)
                        'result = enrollState.PlaceStudentOnLOA(arrStuEnrollId(i), reason, startdate, endDate, suspensionstatusid, user, RequestDate)

                    Case StateEnum.Dropped

                        result = enrollState.DropStudent(change)
                    Case Else
                        Throw New ArgumentOutOfRangeException(change.NewSysStatusId, "New Status unknown")

                End Select
                'Exit FOR when an error occurs
                If Not (result = "") Then Exit For
            Catch ex As Exception
                result = String.Format("Error in Operation. The system report the following exception: {0}{1}", Environment.NewLine, ex.Message) ' 
            End Try

        Next

        Return result
    End Function

    Public Function GetLOAReasons(Optional ByVal strCampusId As String = "") As DataTable
        Return (New LeadDB).GetLOAReasons(strCampusId)
    End Function

    ''last-name, first-name and SSn is added to the filter list
    ''mantis case 07203
    Public Function GetStudentsDSbasedonNameandSSN(ByVal fromStatusCodeId As String, ByVal prgVerId As String, _
                                       ByVal startDate As String, ByVal campusId As String, ByVal lastname As String, ByVal firstname As String, ByVal ssn As String, byval studentGroups As String, Optional ByVal cohortStartDate As String = "", Optional ByVal userName As String = "") As DataTable
        Dim obj As New BatchStatusChangeDB
        Dim ds As DataTable
        ''Cohort Start Date added
        ds = obj.GetStudentsDSbasedonNameandSSN(fromStatusCodeId, prgVerId, startDate, campusId, lastname, firstname, ssn,studentGroups, cohortStartDate, userName)
        MassageStudentTable(ds)

        Return ds
    End Function

    ''' <summary>
    ''' Return true if the student has class or attendance in the enrollment
    ''' </summary>
    ''' <param name="enrollmentGuid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidateIfStudentEnrollmentHasAttendanceOrGrades(enrollmentGuid As String) As Boolean
        Dim db As New BatchStatusChangeDB
        Dim result As Boolean = db.ValidateIfStudentEnrollmentHasAttendanceOrGrades(enrollmentGuid)
        Return result
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="progVersionDescrip"></param>
    ''' <returns></returns>
    ''' <remarks>Used in BatchStatusChange.</remarks>
    Public Shared Function GetProgramIsClockHour(ByVal progVersionDescrip As String) As Boolean
        Dim result As Integer = (New BatchStatusChangeDB).GetProgramAcademicCalendar(progVersionDescrip)
        If (result = 5) Then ' 5 is clock hours in table. th table has not code then it is a fixed value.
            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="progVersionDescrip"></param>
    ''' <returns></returns>
    ''' <remarks>Used in BatchStatusChange</remarks>
    Public Shared Function GetHoursRequiredForProgramVersion(ByVal progVersionDescrip As String) As Decimal
        Dim result As Decimal = (New BatchStatusChangeDB).GetHoursRequiredForProgramVersion(progVersionDescrip)
        Return result
    End Function

    ''' <summary>
    ''' Get all attendance for a specific student enrollment ID
    ''' </summary>
    ''' <param name="stuEnrollmentId"></param>
    ''' <returns></returns>
    ''' <remarks>The data re in munutes in the Table. We need to converted to Hours</remarks>
    Public Shared Function GetStudentAttendanceForProgramVersion(ByVal stuEnrollmentId As String) As Integer
        ' Get regular attendance (minutes).....
        Dim result As Decimal = (New BatchStatusChangeDB).GetStudentAttendanceForProgramVersion(stuEnrollmentId)
        'Get Transfer our attendance if exists (minutes, un a datable under Actual column)..
        Dim dt As DataTable = (New ClsSectAttendanceDB).GetImportedAttendance_SP(stuEnrollmentId)
        Dim attTransfered As Decimal = 0
        If (Not (dt Is Nothing) AndAlso Not (dt.Rows Is Nothing) AndAlso dt.Rows.Count > 0) Then
            For Each row As DataRow In dt.Rows
                attTransfered += row("Actual")
            Next
        End If
        'Get transfer hours from off the enrollment if any is specified
        Dim enrollmentTransferHrs As Decimal = (New BatchStatusChangeDB).GetEnrollmentTransferHours(stuEnrollmentId)

        result = (result / 60) + (attTransfered / 60) + enrollmentTransferHrs
        Return result
    End Function
End Class
