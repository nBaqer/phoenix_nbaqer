Public Class LeadEntranceFacade
    Public Function GetControlCount(ByVal LeadId As String) As Integer
        Dim getDB As New LeadEntranceDB
        Return getDB.GetControlCount(LeadId)
    End Function
    'Public Function GetArrayCount(ByVal LeadId As String) As Integer
    '    Dim getDB As New LeadEntranceDB
    '    Return getDB.GetArrayCount(LeadId)
    'End Function
    'Public Function GetControlCountAppend(ByVal LeadId As String) As Integer
    '    Dim getDB As New LeadEntranceDB
    '    Return getDB.GetControlCountAppend(LeadId)
    'End Function
    Public Function GetLeadTestCount(ByVal LeadId As String, ByVal PrgVerId As String) As Integer
        Dim getDB As New LeadEntranceDB
        Return getDB.GetLeadTestCount(LeadId, PrgVerId)

    End Function
    Public Function getPrgVersionByStudent(ByVal StudentId As String) As Integer
        Dim getDB As New LeadEntranceDB
        Return getDB.getPrgVersionByStudent(StudentId)
    End Function
    Public Function GetRequiredTestCount(ByVal LeadId As String) As Integer
        Dim getDB As New LeadEntranceDB
        Return getDB.GetAllRequiredTestCount(LeadId)
    End Function
    Public Function GetRequiredRequirements(ByVal LeadId As String) As Integer
        Dim getDB As New LeadEntranceDB
        Return getDB.GetAllRequiredRequirements(LeadId)
    End Function
    'Public Function GetTestDetails(ByVal LeadId As String) As DataSet
    '    Dim getDB As New LeadEntranceDB
    '    Return getDB.GetTestDetails(LeadId)
    'End Function
    Public Function GetRequirementDetails(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        Dim getDB As New LeadEntranceDB
        Return getDB.GetRequirementDetails(LeadId, PrgVerId)
    End Function
    Public Function GetGridRequirementDetails(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        Dim getDB As New LeadEntranceDB
        Return getDB.GetGridRequirementDetails(LeadId, PrgVerId)
    End Function
    Public Function GetAllStandardRequirementDetails(ByVal LeadId As String) As DataSet
        Dim getDB As New LeadEntranceDB
        Return getDB.GetAllStandardRequirementDetails(LeadId)
    End Function
    Public Function GetAllStandardRequirementsNoPrgVersion(ByVal LeadId As String) As DataSet
        Dim getDB As New LeadEntranceDB
        Return getDB.GetAllStandardRequirementsNoPrgVersion(LeadId)
    End Function
    Public Function GetGridRequirementDetailsByEffectiveDates(ByVal LeadId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "") As DataSet
        'Dim getDb As New LeadEntranceDB
        Dim getDb As New RequirementsDB
        Return getDb.GetGridRequirementDetailsByEffectiveDates(LeadId, PrgVerId, CampusId)
    End Function
    Public Function GetStudentGridRequirementDetailsByEffectiveDates(ByVal StudentId As String, ByVal campusId As String) As DataSet
        'Dim getDb As New RequirementsDB
        'Return getDb.GetStudentGridRequirementDetailsByEffectiveDates(StudentId, campusId)
        Dim getDb As New RequirementsDB
        Return getDb.GetStudentGridRequirementDetailsByEffectiveDates_sp(StudentId, campusId)
    End Function
    Public Function GetAllStandardRequirementsByEffectiveDatesForOverride(ByVal LeadId As String) As DataSet
        Dim getDB As New LeadEntranceDB
        Return getDB.GetAllStandardRequirementsByEffectiveDatesForOverride(LeadId)
    End Function
    Public Function GetGridRequirementDetailsByEffectiveDatesForOverRide(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        Dim getDB As New LeadEntranceDB
        Return getDB.GetGridRequirementDetailsByEffectiveDatesForOverRide(LeadId, PrgVerId)
    End Function
    Public Function GetAllStandardRequirementsByEffectiveDates(ByVal LeadId As String, Optional ByVal CampusId As String = "") As DataSet
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.GetAllStandardRequirementsByEffectiveDates(LeadId, CampusId)
    End Function
    Public Function GetAllStudentStandardRequirementsByEffectiveDates(ByVal StudentId As String) As DataSet
        Dim getDB As New RequirementsDB
        Return getDB.GetAllStudentStandardRequirementsByEffectiveDates(StudentId)
    End Function
    Public Function GetAllStandardDocumentsByEffectiveDates(ByVal LeadId As String, ByVal CampusId As String) As DataSet
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        'Return getDB.GetAllStandardDocumentsByEffectiveDates(LeadId, CampusId)
        Return getDB.GetAllStandardDocumentsByEffectiveDates(LeadId, CampusId)
    End Function

    ''' <summary>
    ''' GetAllTerminationDocuments returns all the termination related document list
    ''' </summary>
    ''' <param name="LeadId"></param>
    ''' <param name="CampusId"></param>
    ''' <returns></returns>
    Public Function GetAllTerminationDocuments(ByVal ModuleId As Integer) As DataSet
        Dim getDB As New RequirementsDB 'LeadEntranceDB 
        Return getDB.GetAllTerminationDocuments(ModuleId)
    End Function

    Public Function GetAllExistingStandardDocumentsByEffectiveDates(ByVal LeadId As String, ByVal CampusId As String, ByVal LeadDocId As String) As DataSet
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.GetAllExistingStandardDocumentsByEffectiveDates(LeadId, CampusId, LeadDocId)
    End Function
    Public Sub InsertPrgVersionForLead(ByVal LeadId As String, ByVal PrgVerId As String)
        Dim db As New LeadEnrollmentDB
        db.InsertPrgVersionForLead(LeadId, PrgVerId)
    End Sub
    Public Sub UpdateExpectedDateForLead(ByVal LeadId As String, ByVal ExpectedStartDate As String)
        Dim db As New LeadEnrollmentDB
        db.UpdateExpectedDateForLead(LeadId, ExpectedStartDate)
    End Sub

    Public Function CheckIfLeadHasPassedRequiredTestNoProgramVersion(ByVal LeadId As String) As Integer
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.CheckIfLeadHasPassedRequiredTestNoProgramVersion(LeadId)
    End Function
    Public Function CheckIfLeadHasApprovedRequiredDocumentsNoProgramVersion(ByVal LeadId As String) As Integer
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.CheckIfLeadHasApprovedRequiredDocumentsNoProgramVersion(LeadId)
    End Function
    Public Function CheckIfLeadHasPassedRequiredTestWithProgramVersion(ByVal LeadId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "") As Integer
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.CheckIfLeadHasPassedRequiredTestWithProgramVersion(LeadId, PrgVerId, CampusId)
    End Function
    Public Function CheckAllApprovedDocumentsWithPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "") As Integer
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.CheckAllApprovedDocumentsWithPrgVersion(LeadId, PrgVerId, CampusId)
    End Function
    Public Function CheckAllApprovedDocumentsWithNoPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String) As Integer
        Dim getDB As New LeadEntranceDB
        Return getDB.CheckIfDocumentsApprovedNoProgramVersion(LeadId)
    End Function
    Public Function GetHasLeadMetRequirements(ByVal LeadId As String) As Boolean
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.GetHasLeadMetRequirements(LeadId)
    End Function
    Public Function GetAllStandardDocumentsByEffectiveDatesAndPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String, ByVal CampusId As String) As DataSet
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.GetAllStandardDocumentsByEffectiveDatesAndPrgVersion(LeadId, PrgVerId, CampusId)
    End Function

    Public Function GetAllExistingStandardDocumentsByEffectiveDatesAndPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String, ByVal LeadDocId As String, ByVal CampusId As String) As DataSet
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.GetAllExistingStandardDocumentsByEffectiveDatesAndPrgVersion(LeadId, PrgVerId, LeadDocId, CampusId)
    End Function
    Public Function GetAllStandardDocumentsByEffectiveDatesAndPrgVersionByStatus(ByVal LeadId As String, ByVal PrgVerId As String, ByVal CampusId As String) As DataSet
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.GetAllStandardDocumentsByEffectiveDatesAndPrgVersion(LeadId, PrgVerId, CampusId)
        'GetAllStandardDocumentsByEffectiveDatesAndPrgVersionByStatus(LeadId, PrgVerId)
    End Function
    Public Function getDocumentStatusByDocument(ByVal LeadId As String, ByVal DocumentId As String) As String
        Return (New LeadEntranceDB).getDocumentStatusByDocument(LeadId, DocumentId)
    End Function
    Public Function GetPendingDocsByStudentandPrgVersion(ByVal StudentId As String, ByVal campusId As String) As DataSet
        Dim getDB As New RequirementsDB
        Dim ds As New DataSet
        ds = getDB.GetPendingDocsByStudentandPrgVersion_SP(StudentId, campusId)

        'Added by Michelle R. Rodriguez, 07/20/2006
        'If there was an error in the DA layer, ds returns Nothing
        If Not (ds Is Nothing) Then
            If ds.Tables.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If Not dr.IsNull("ReqGrpDescrip") Then
                        If dr("ReqGrpDescrip").ToString <> "" Then
                            dr("ReqGrpDescrip") &= " (" & dr("NumRequired").ToString & ")"
                        End If
                    End If
                Next
            End If
        End If

        Return ds
    End Function
    Public Function GetPendingReqsByLeadandPrgVersion(ByVal LeadId As String) As DataSet
        Dim getDB As New LeadEntranceDB
        ' Return getDB.GetPendingReqsByLeadandPrgVersion(LeadId)
        Return getDB.GetPendingReqsByLeadandPrgVersion_SP(LeadId)
    End Function
    Public Function GetAllStandardDocumentsByEffectiveDatesByStatus(ByVal LeadId As String, ByVal CampusId As String) As DataSet
        'Dim getDb As New LeadEntranceDB
        'Return getDb.GetAllStandardDocumentsByEffectiveDatesByStatus(LeadId)

        Dim getDB As New RequirementsDB
        Return getDB.GetAllStandardDocumentsByEffectiveDates(LeadId, CampusId)
    End Function
    Public Function GetAllStandardDocumentsByStudentEnrollment(ByVal StudentId As String, ByVal ModuleId As Integer, ByVal CampusId As String, Optional ByVal StudentDocId As String = "") As DataSet
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.GetAllStandardDocumentsByPrgVersionEnrollments(StudentId, ModuleId, CampusId, StudentDocId)
    End Function
    Public Function GetAllExistingStandardDocumentsByStudentEnrollment(ByVal StudentId As String, ByVal ModuleId As Integer, ByVal StudentDocId As String, ByVal CampusId As String, Optional ByVal includeSubmittedDocumentsThatAreExpired As Boolean = False) As DataSet
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.GetAllExistingStandardDocumentsByPrgVersionEnrollments(StudentId, ModuleId, StudentDocId, CampusId, includeSubmittedDocumentsThatAreExpired)
    End Function
    Public Function GetAllStandardLeadTestCount(ByVal LeadId As String) As Integer
        Dim getDB As New LeadEntranceDB
        Return getDB.GetAllStandardLeadTestCount(LeadId)
    End Function
    Public Function GetDocumentsByPrgVersionLeadGroup(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        Dim getDB As New LeadEntranceDB
        Return getDB.GetDocumentsByPrgVersionLeadGroup(LeadId, PrgVerId)
    End Function
    Public Function GetPrgVersionByLead(ByVal LeadId As String) As String
        Dim getDB As New LeadEntranceDB
        Return getDB.getPrgVersionByLead(LeadId)
    End Function
    'Public Function GetScoreByTest(ByVal LeadId As String) As DataSet
    '    Dim getDB As New LeadEntranceDB
    '    Return getDB.GetScoreByTest(LeadId)
    'End Function
    Public Function GetLeadTestAndScores(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        Dim getDB As New LeadEntranceDB
        Return getDB.GetLeadTestAndScores(LeadId, PrgVerId)
    End Function
    Public Function GetStandardLeadTestAndScores(ByVal LeadId As String) As DataSet
        Dim getDB As New LeadEntranceDB
        Return getDB.GetStandardLeadTestAndScores(LeadId)
    End Function
    'Public Function GetScores(ByVal LeadId As String) As DataSet
    '    Dim getDB As New LeadEntranceDB
    '    Return getDB.GetScores(LeadId)
    'End Function
    'Public Function GetOverRideScores(ByVal LeadId As String) As DataSet
    '    Dim getDB As New LeadEntranceDB
    '    Return getDB.GetOverRideScores(LeadId)
    'End Function
    Public Function GetOverRideCount(ByVal LeadId As String) As Integer
        Dim getDB As New LeadEntranceDB
        Return getDB.GetOverRideCount(LeadId)
    End Function
    Public Function GetAllFailedLeadNames(ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        With New LeadEntranceDB
            ds = .GetAllFailedLeadNames(campusId)
        End With
        ds = ConcatenateFailedLeadLastFirstName(ds)
        Return ds
    End Function
    Public Function ConcatenateFailedLeadLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorStudent")
        With tbl
            .PrimaryKey = New DataColumn() { .Columns("LeadId")}
        End With
        If ds.Tables("InstructorStudent").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                row("FullName") = row("FirstName") & " " & row("LastName")
            Next
        End If
        Return ds
    End Function
    'Public Function GetTestDetailsAppend(ByVal LeadId As String) As DataSet
    '    Dim getDB As New LeadEntranceDB
    '    Return getDB.GetTestDetailsAppend(LeadId)
    'End Function
    Public Function EntranceTestOverRide(ByVal LeadId As String, ByVal selectedEntrTestId() As String, ByVal selectedOverRide() As String, ByVal User As String) As Integer
        Dim getDB As New LeadEntranceDB
        Return getDB.EntranceTestOverRide(LeadId, selectedEntrTestId, selectedOverRide, User)
    End Function
    Public Function InsertValues(ByVal LeadId As String, ByVal selectedEntrTestId() As String, ByVal selectedRequired() As String, ByVal selectedPass() As String, ByVal selectedTest() As String, ByVal selectedActual() As String, ByVal selectedMin() As String, ByVal selectedComments() As String, ByVal user As String) As Integer
        Dim getDB As New LeadEntranceDB
        Return getDB.InsertValues(LeadId, selectedEntrTestId, selectedRequired, selectedPass, selectedTest, selectedActual, selectedMin, selectedComments, user)
    End Function
    'Public Function GetLeadEntranceTestDetail(ByVal LeadId As String) As DataSet
    '    Dim getDB As New LeadEntranceDB
    '    Return getDB.GetLeadEntranceTestDetail(LeadId)
    'End Function
    Public Function GetTestCountByLead(ByVal LeadId As String) As Integer
        Dim getDB As New LeadEntranceDB
        Return getDB.GetTestCountByLead(LeadId)
    End Function
    Public Function CheckIfMandatoryGrpExists() As Integer
        Dim getDB As New RequirementsDB
        Return getDB.CheckIfMandatoryGrpExists()
    End Function
    Public Function DeleteRequirementGroup(ByVal ReqGrpId As String) As String
        Dim getDB As New RequirementsDB
        Return getDB.DeleteRequirementGroup(ReqGrpId)
    End Function

    Public Function GetProgramVersionRequerimentsStatus(ByVal prgVerId As String, ByVal leadId As String) As List(Of AdRequerimentsProgramVersion)
        Dim getDB As New RequirementsDB
        Dim list = getDB.GetProgramVersionRequerimentsStatus(prgVerId, leadId)
        Return list
    End Function
End Class
