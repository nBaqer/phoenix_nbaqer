Public Class StuAgingObject
    Inherits BaseReportFacade

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim stuAgingSummary As New StudentAgingDB
        Dim ds As DataSet
        ds = stuAgingSummary.GetStudentTransactions(rptParamInfo)
        ' Massage dataset to produce one with the columns and data expected by the AgingDetail report. 
        ' The Aging Detail report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return BuildReportSource(ds, stuAgingSummary.AgingDates, stuAgingSummary.StudentIdentifier)
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal ADates As AgingBalance, _
                                            ByVal StudentIdentifier As String) As DataSet
        Dim stuName As String
        Dim payment As Decimal
        Dim campusGrp As String
        Dim campus As String
        Dim prgVersion As String
        Dim sum As Decimal
        Dim rows() As DataRow
        Dim facInputMasks As New InputMasksFacade
        'Get the mask for SSN
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            'Get CHARGES and apply them into corresponding date-range column.
            rows = ds.Tables(0).Select("TransAmount > 0")
            If rows.GetLength(0) > 0 Then
                For Each dr As DataRow In rows
                    If Not (dr("TransAmount") Is System.DBNull.Value) Then
                        If dr("TransAmount") > 0 Then
                            'Transaction is a CHARGE.
                            Select Case ADates.GetPeriodRange(dr("TransDate"))
                                Case 1
                                    dr("CurrentBal") = dr("TransAmount")
                                Case 2
                                    dr("Bal31To60Day") = dr("TransAmount")
                                Case 3
                                    dr("Bal61To90Day") = dr("TransAmount")
                                Case 4
                                    dr("Bal91To120Day") = dr("TransAmount")
                                Case 5
                                    dr("BalOver120Day") = dr("TransAmount")
                            End Select
                        End If
                    End If
                Next
            End If
            '
            'Apply PAYMENTS to oldest charges.
            'If there is a remaining, apply it to the corresponding date-range column.
            Dim count As Integer = ds.Tables(0).Rows.Count
            For Each dr As DataRow In ds.Tables(0).Rows
                count -= 1
                If Not (dr("TransAmount") Is System.DBNull.Value) Then
                    
                If dr("TransAmount") < 0 Then
                        While (True)
                            'Transaction is a PAYMENT.
                            payment = dr("TransAmount")
                            'Apply payment where there are charges.
                            'Get all the charges in columns BalOver120Day.
                            sum = GetSumOfColumn(ds.Tables(0), "BalOver120Day", CreateFilter(dr))
                            If sum <> 0.0 And payment <> 0 Then
                                If (sum + payment) >= 0 Then
                                    'Apply all payment because is less or equal to Sum of BalOver120Day.
                                    dr("BalOver120Day") = payment
                                    payment = 0
                                    Exit While
                                Else
                                    'Payment is greater that Sum of BalOver120Day.
                                    dr("BalOver120Day") = -sum
                                    payment += sum
                                End If
                            End If
                            'Get all the charges in columns Bal91To120Day.
                            sum = GetSumOfColumn(ds.Tables(0), "Bal91To120Day", CreateFilter(dr))
                            If sum <> 0 And payment <> 0 Then
                                If (sum + payment) >= 0 Then
                                    'Apply all payment because is less or equal to Sum of Bal91To120Day.
                                    dr("Bal91To120Day") = payment
                                    payment = 0
                                    Exit While

                                Else
                                    'Payment is greater that Sum of Bal91To120Day.
                                    dr("Bal91To120Day") = -sum
                                    payment += sum
                                End If
                            End If
                            'Get all the charges in columns Bal61To90Day.
                            sum = GetSumOfColumn(ds.Tables(0), "Bal61To90Day", CreateFilter(dr))
                            If sum <> 0 And payment <> 0 Then
                                If (sum + payment) >= 0 Then
                                    'Apply all payment because is less or equal to Sum of Bal61To90Day.
                                    dr("Bal61To90Day") = payment
                                    payment = 0
                                    Exit While
                                Else
                                    'Payment is greater that Sum of Bal61To90Day.
                                    dr("Bal61To90Day") = -sum
                                    payment += sum
                                End If
                            End If
                            'Get all the charges in columns Bal31To60Day.
                            sum = GetSumOfColumn(ds.Tables(0), "Bal31To60Day", CreateFilter(dr))
                            If sum <> 0 And payment <> 0 Then
                                If (sum + payment) >= 0 Then
                                    'Apply all payment because is less or equal to Sum of Bal31To60Day.
                                    dr("Bal31To60Day") = payment
                                    payment = 0
                                    Exit While
                                Else
                                    'Payment is greater that Sum of Bal31To60Day.
                                    dr("Bal31To60Day") = -sum
                                    payment += sum
                                End If
                            End If
                            'Get all the charges in columns CurrentBal.
                            sum = GetSumOfColumn(ds.Tables(0), "CurrentBal", CreateFilter(dr))
                            If sum <> 0 And payment <> 0 Then
                                If (sum + payment) >= 0 Then
                                    'Apply all payment because is less or equal to Sum of CurrentBal.
                                    dr("CurrentBal") = payment
                                    payment = 0
                                    Exit While
                                Else
                                    'Payment is greater that Sum of CurrentBal.
                                    dr("CurrentBal") = -sum
                                    payment += sum
                                End If
                            End If
                            'Place remaining Payment into corresponding date-range column.
                            If payment <> 0 Then
                                Select Case ADates.GetPeriodRange(dr("TransDate"))
                                    Case 1
                                        If Not (dr("CurrentBal") Is System.DBNull.Value) Then
                                            dr("CurrentBal") += payment
                                        Else
                                            dr("CurrentBal") = payment
                                        End If
                                    Case 2
                                        If Not (dr("Bal31To60Day") Is System.DBNull.Value) Then
                                            dr("Bal31To60Day") += payment
                                        Else
                                            dr("Bal31To60Day") = payment
                                        End If
                                    Case 3
                                        If Not (dr("Bal61To90Day") Is System.DBNull.Value) Then
                                            dr("Bal61To90Day") += payment
                                        Else
                                            dr("Bal61To90Day") = payment
                                        End If
                                    Case 4
                                        If Not (dr("Bal91To120Day") Is System.DBNull.Value) Then
                                            dr("Bal91To120Day") += payment
                                        Else
                                            dr("Bal91To120Day") = payment
                                        End If
                                    Case 5
                                        If Not (dr("BalOver120Day") Is System.DBNull.Value) Then
                                            dr("BalOver120Day") += payment
                                        Else
                                            dr("BalOver120Day") = payment
                                        End If
                                End Select
                                payment = 0
                                Exit While
                            End If
                        End While
                    End If
                End If
                '
                'Set up student name as: "LastName, FirstName MI."
                stuName = dr("LastName")
                If Not (dr("FirstName") Is System.DBNull.Value) Then
                    If dr("FirstName") <> "" Then
                        stuName &= ", " & dr("FirstName")
                    End If
                End If
                If Not (dr("MiddleName") Is System.DBNull.Value) Then
                    If dr("MiddleName") <> "" Then
                        stuName &= " " & dr("MiddleName") & "."
                    End If
                End If
                dr("StudentName") = stuName
                '
                'Apply mask to SSN.
                If StudentIdentifier = "SSN" Then
                    If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                        If dr("StudentIdentifier") <> "" Then
                            Dim temp As String = dr("StudentIdentifier")
                            dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                        End If
                    End If
                End If
            Next

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function
    Private Function GetSumOfColumn(ByVal table As DataTable, ByVal columnName As String, ByVal filter As String) As Decimal
        Dim objSum As Object = table.Compute("Sum(" + columnName + ")", filter)
        If objSum Is System.DBNull.Value Then
            Return 0.0
        Else
            Return CType(objSum, Decimal)
        End If

    End Function
    Private Function CreateFilter(ByVal dr As DataRow) As String
        Return "StuEnrollId = '" & dr("StuEnrollId").ToString & "' AND " & _
                   "CampGrpId = '" & dr("CampGrpId").ToString & "' AND " & _
                   "CampusId = '" & dr("CampusId").ToString & "' AND " & _
                   "PrgVerId = '" & dr("PrgVerId").ToString & "'"
    End Function

#End Region

End Class
