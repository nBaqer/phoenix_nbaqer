Imports FAME.AdvantageV1.BusinessFacade.StudentEnrollState

Public Class StateFactory

    Public Shared Function CreateStateObj(ByVal enumStatus As StateEnum) As IStudentEnrollmentState

        Select Case enumStatus
            Case StateEnum.CurrentlyAttending
                Return New CurrentlyAttendingState
            Case StateEnum.Dropped
                Return New DroppedState
            Case StateEnum.Graduated
                Return New GraduatedState
            Case StateEnum.Suspended
                Return New SuspendedState
            Case StateEnum.LeaveOfAbsence
                Return New LOAState
            Case StateEnum.FutureStart
                Return New FutureStartState
            Case StateEnum.TransferOut
                Return New TransferedOutState
            Case StateEnum.NoStart
                Return New NoStartState
            Case StateEnum.AcademicProbation
                Return New AcademicProbationState
            Case StateEnum.Externship
                Return New ExternshipState
            Case StateEnum.DisciplinaryProbation
                Return New DisciplinaryProbation
            Case StateEnum.WarningProbation
                Return New WarningProbation

        End Select

    End Function
End Class
