Imports System.Data
Imports FAME.AdvantageV1.BusinessRules
Imports FAME.AdvantageV1.DataAccess
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common.Tables

Public Class RegFacade
    Public Shared CoreqClsSects As New DataSet

    Public Function PopulateDataList(ByVal term As String, ByVal shift As String, ByVal campusId As String, ByVal course As String, Optional ByVal allTerm As String = "") As DataSet

        Dim db As New RegisterDB
        Dim ds As DataSet
        Dim tbl1 As DataTable
        Dim tbl2 As DataTable
        Dim row As DataRow

        ds = db.PopulateClsSectDL(term, Shift, CampusId, Course, allTerm)

        tbl1 = ds.Tables("TermClsSects")
        tbl2 = ds.Tables("RemStdInfo")


        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("ClsSectionId")}
        End With

        With tbl2
            .PrimaryKey = New DataColumn() {.Columns("TestId")}
        End With
        'Add two extra columns to the original datatable.
        tbl1.Columns.Add("Assigned", GetType(String))
        tbl1.Columns.Add("Remaining", GetType(String))
        'Add a new column called ChildDescrip to the datatable 
        tbl1.Columns.Add("ChildDescripTyp", GetType(String))


        'Loop through the original datatable and for each entry, loop through the other datatable
        'and do a find for the ClsSectId. If u find it, then get info..and add it to the original dt.

        If tbl1.Rows.Count > 0 Then
            'Add a new column called full name to the datatable 
            For Each row In tbl1.Rows
                ' ClsSectId = row("ClsSectionId").ToString

                Dim row2 As DataRow = tbl2.Rows.Find(row("ClsSectionId"))
                If Not (row2 Is Nothing) Then
                    row("Assigned") = row2("Cnt")
                    row("Remaining") = row("MaxStud") - row("Assigned")
                Else
                    row("Assigned") = 0
                    row("Remaining") = row("MaxStud") - row("Assigned")

                End If

                row("ChildDescripTyp") = " (" + row("Code") + ") " + row("Class")
            Next
        End If
        Return ds
    End Function
    Public Function PopulateDataListNew(ByVal Progid As String, ByVal Term As String, ByVal Shift As String, ByVal CampusId As String, ByVal Course As String, Optional ByVal allTerm As String = "") As DataSet

        Dim db As New RegisterDB
         Dim tbl1 As DataTable
        Dim tbl2 As DataTable
        Dim row As DataRow


        Dim ds As DataSet = New DataSet()
        ds.Tables.Add(db.GetClsSections(Progid, Term, Shift, CampusId, Course, allTerm))
        ds.Tables.Add(db.GetStudentsRegisterPerClass(Term, Shift))
        tbl1 = ds.Tables("TermClsSects")
        tbl2 = ds.Tables("RemStdInfo")


        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("ClsSectionId")}
        End With

        With tbl2
            .PrimaryKey = New DataColumn() {.Columns("TestId")}
        End With
        'Add two extra columns to the original data-table.
        tbl1.Columns.Add("Assigned", GetType(String))
        tbl1.Columns.Add("Remaining", GetType(String))
        'Add a new column called ChildDescrip to the datatable 
        tbl1.Columns.Add("ChildDescripTyp", GetType(String))


        'Loop thru the original datatable and for each entry, loop thru the other datatable
        'and do a find for the ClsSectId. If u find it, then get info..and add it to the original dt.

        If tbl1.Rows.Count > 0 Then
            'Add a new column called full name to the data-table 
            For Each row In tbl1.Rows
                'Dim ClsSectId As String = row("ClsSectionId").ToString

                Dim row2 As DataRow = tbl2.Rows.Find(row("ClsSectionId"))
                If Not (row2 Is Nothing) Then
                    row("Assigned") = row2("Cnt")
                    row("Remaining") = row("MaxStud") - row("Assigned")
                Else
                    row("Assigned") = 0
                    row("Remaining") = row("MaxStud") - row("Assigned")

                End If

                row("ChildDescripTyp") = " (" + row("Code") + ") " + row("Class")
            Next
        End If
        Return ds
    End Function
    Public Function GetAllProgramsOfferedInCampus(ByVal campusId As String) As DataSet
        Dim db As New RegisterDB

        Return db.GetAllProgramsOfferedInCampus(campusId)

    End Function
    Public Function GetCurrentAndFutureTermsForProgram(ByVal progId As String, Optional ByVal campusid As String = "") As DataSet
        Dim db As New RegisterDB
        Return db.GetCurrentAndFutureTermsForProgram(progId, campusid)
    End Function
    Public Function GetCurrentAndFutureTermsForStudent(ByVal stuEnrollid As String, Optional ByVal campusid As String = "") As DataSet
        Dim db As New RegisterDB
        Return db.GetCurrentAndFutureTermsForStudent(stuEnrollid, campusid)
    End Function
    Public Function ChkStudSchedConflict(ByVal stuEnrollId As String, ByVal clsSectId As String) As String

        Dim BR As New RegisterStudentsBR
        Dim ErrStr As String

        ErrStr = BR.ChkStudSchedConflict(stuEnrollId, ClsSectId)

        Return ErrStr
    End Function

    'Public Function ChkStudSchedConflict_Sp(ByVal stuEnrollId As String, ByVal clsSectId As String) As String

    '    Dim br As New RegisterDB
    '    Dim errStr As String

    '    errStr = br.ChkStudSchedConflict_SP(stuEnrollId, ClsSectId)

    '    Return errStr
    'End Function
    Public Function ChkStudSchedConflictInAllEnrollments(ByVal stuEnrollId As String, ByVal clsSectId As String) As String

        Dim br As New RegisterStudentsBR
        Dim errStr As String

        errStr = br.ChkStudSchedConflictInAllEnrollments(stuEnrollId, ClsSectId)

        Return errStr
    End Function
    Public Function HasStudMetPrereqs(ByVal studentId As String, ByVal clsSectId As String, ByVal prgVerId As String, ByVal campusID As String) As Boolean

        Dim br As New RegisterStudentsBR
        Dim result As Boolean

        result = br.HasStdMetPrereqs(studentId, ClsSectId, PrgVerId, CampusID, "False")

        Return result
    End Function
    Public Function DoesReqHaveCoReqs(ByVal reqId As String) As Boolean

        Dim db As New RegisterDB
        Dim result As Boolean

        result = db.DoesReqHaveCoReqs(reqId)

        Return result
    End Function
    Public Function DoesStdHaveFinalGrade(ByVal StuEnrollId As String, ByVal ClsSectionId As String) As String
        Dim db As New RegisterDB
        Dim count As Integer

        count = db.DoesStdHaveFinalGrade(StuEnrollId, ClsSectionId)

        If count >= 1 Then
            Return "GradeExists"
        Else
            Return "NoGrdExists"
        End If

    End Function

    Public Function GetTransfersForStudent_SP(ByVal stuEnrollId As String, ByVal clsSectId As String) As Boolean

        Return New RegisterDB().GetTransfersForStudent_SP(stuEnrollId, clsSectId)

    End Function

    Public Function GetClsSectStudents(ByVal clsSectId As String, ByVal termId As String, ByVal campusId As String, ByVal boolPreReq As Boolean) As DataSet
        Dim ds As DataSet
        Dim olddt As DataTable
        Dim newdt As DataTable
        Dim db As New RegisterDB
        Dim tbl1 As DataTable
        Dim tbl2 As DataTable
        Dim row As DataRow
        Dim row2 As DataRow
        Dim dsFinal As DataSet
        Dim dsFinal2 As DataSet

        ds = db.GetAvailSelectStdsFrClsSect_SP(clsSectId, campusId)

        dsFinal = ProcessAvailStdsFrClsSect(ds, clsSectId, termId, boolPreReq, campusId)
        dsFinal2 = ProcessSelectedStdsFrClsSect(dsFinal, clsSectId, termId)

        tbl1 = dsFinal2.Tables("AvailStuds")
        tbl2 = dsFinal2.Tables("SelectedStuds")

        With dsFinal2.Tables("AvailStuds")
            .PrimaryKey = New DataColumn() {.Columns("StuEnrollId")}
        End With

        With dsFinal2.Tables("SelectedStuds")
            .PrimaryKey = New DataColumn() {.Columns("StuEnrollId")}
        End With

        tbl1.Columns.Add("FullName", GetType(String))
        If dsFinal2.Tables("AvailStuds").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            For Each row In tbl1.Rows
                row("FullName") = row("LastName") & ", " & row("FirstName") & " - " & row("ExpGradDate")
            Next
        End If

        tbl2.Columns.Add("FullName", GetType(String))
        If dsFinal2.Tables("SelectedStuds").Rows.Count > 0 Then
            For Each row2 In tbl2.Rows
                row2("FullName") = row2("LastName") & ", " & row2("FirstName") & " - " & row2("ExpGradDate")
            Next
        End If
        Return dsFinal2
    End Function

    Public Function AddStdToClsSect(ByVal storedGuid As String, ByVal stuEnrollId As String, ByVal user As String, ByVal campusId As String)
        Dim db As New RegisterDB

        db.AddStdToClsSect(storedGuid, stuEnrollId, user, campusId)

    End Function
    'Public Function DeleteStdFrmClsSect(ByVal storedGuid As String, ByVal stuEnrollIdList As List(Of String), ByVal user As String)
    '    Dim db As New RegisterDB
    '    db.DeleteStdFrmClsSect(storedGuid, stuEnrollIdList, user)
    'End Function

    ''Campus-id added to the Function
    Public Function ProcessAvailStdsFrClsSect(ByVal ds As DataSet, ByVal clsSectId As String, ByVal termId As String, ByVal boolPreReq As Boolean, ByVal campusid As String) As DataSet
        Dim student As String
        Dim attempted As Integer
        Dim stdAvailable As Boolean
        Dim req As String

        Dim isPass As Boolean
        Dim metPrereqs As Boolean
        Dim finalDS As New DataSet
        Dim dr As DataRow

        Dim row As DataRow
        Dim row2 As DataRow
        Dim row3 As DataRow
        Dim rowcount2 As Integer
        Dim tbl1 As DataTable
        Dim tempDt As DataTable
        Dim lName As String
        Dim fName As String
        Dim expGradDate As Date
        Dim prgVerId As String
        Dim anyCoreqs As Integer
        Dim ds1 As DataSet
        Dim coreqClsSect As String
        Dim coreqs As DataSet
        Dim attmptd As Integer
        Dim tbl3 As New DataTable
        Dim coReqId As String
        Dim stuEnrollment As String
        Dim tbl2 As DataTable
        Dim hasOverride As Integer
        Dim isCurrentlyRegistered As Integer
        Dim allowRetake As Boolean
        Dim studentId As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim gradesFormat As String = myAdvAppSettings.AppSettings("GradesFormat", campusid).ToLower
        Dim showCcr As Boolean
        If myAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToLower() = "yes" Then
            showCcr = True
        Else
            showCcr = False
        End If
        Dim checkConflicts As Boolean
        If myAdvAppSettings.AppSettings("TimeClockClassSection", campusid).ToString.ToLower = "yes" Then
            checkConflicts = True
        Else
            checkConflicts = False
        End If

        CoreqClsSects = Nothing

        tbl1 = ds.Tables("AvailStds")
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("StuEnrollId")}
        End With

        tbl2 = ds.Tables("SelectedStuds")
        With tbl2
            .PrimaryKey = New DataColumn() {.Columns("StuEnrollId")}
        End With

        tempDt = tbl1.Clone
        tempDt.TableName = "AvailStuds"
        ds.Tables.Add(tempDt)

        '****************************************************************************
        tbl3.TableName = "StdCoreqs"
        ds.Tables.Add(tbl3)

        tbl3.Columns.Add("StudentId")
        tbl3.Columns.Add("TestId")
        tbl3.Columns.Add("StuEnrollId")

        With tbl3
            .PrimaryKey = New DataColumn() {.Columns("StuEnrollId")}
        End With
        '****************************************************************************

        'Clean of duplicated the Available Student Table by copy only no duplicate records to cleanDt **********************************************
        If ds.Tables("AvailStds").Rows.Count > 0 Then
            Dim clean = ds.Tables("AvailStds").AsEnumerable().GroupBy(Function(x) x.Field(Of Guid)("StudentId")).[Select](Function(g) g.First())
            Dim cleanDt As DataTable = clean.CopyToDataTable()
            ds.Tables("AvailStds").Clear()
            ds.Tables("AvailStds").Merge(cleanDt)
        End If

        ' ******************************************************************************************************************************************



        For Each dr In ds.Tables("AvailStds").Rows
            'For Each dr In cleanDt.Rows
            Try
                Dim db As New RegisterDB
                Dim br As New RegisterStudentsBR
                stuEnrollment = dr("StuEnrollId").ToString
                req = dr("ReqId").ToString
                lName = dr("LastName")
                fName = dr("FirstName")
                expGradDate = dr("ExpGradDate")
                prgVerId = dr("PrgVerId").ToString
                studentId = dr("StudentId").ToString()
                allowRetake = dr("AllowCompletedCourseRetake")
                stdAvailable = True

                '******************************************************************************************
                'Check if student is currently registered for any class sections pertaining to this course.
                '******************************************************************************************
                isCurrentlyRegistered = db.IsStudentCurrentlyRegisteredInCourse_Sp(stuEnrollment, clsSectId)
                If isCurrentlyRegistered = 1 Then
                    'stdAvailable = False
                    Exit Try
                End If

                '****************************************************
                'Check for student schedule conflict.
                '****************************************************
                If checkConflicts Then
                    Dim dsScheduleConflicts As DataSet
                    dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsDuringRegistration(clsSectId, stuEnrollment)
                    If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
                        'stdAvailable = False
                        Exit Try
                    End If
                End If

                '****************************************************
                'Check if student has attempted course previously.
                '****************************************************
                attempted = db.HasStdAttemptedReq_Sp(stuEnrollment, clsSectId)
                If attempted >= 1 Then 'If attempted Course
                    Dim db1 As RegisterDB = New RegisterDB()
                    Dim isDropped As Boolean = db1.IsCourseDropped(stuEnrollment, clsSectId)
                    If Not isDropped Then

                        If gradesFormat = "numeric" Then
                            isPass = br.DoesStdHavePassingGrdForNumeric_sp(stuEnrollment, req, prgVerId, clsSectId)
                        Else
                            isPass = br.DoesStdHavePassingGrd(stuEnrollment, req, prgVerId, clsSectId)
                        End If
                        If isPass Then
                            If Not allowRetake Then
                                If Not showCcr Then
                                    'stdAvailable = False
                                    Exit Try
                                Else
                                    If HasStudentPassedSpeedTestsForCourse(stuEnrollment, req) Then
                                        'stdAvailable = False
                                        Exit Try
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If

                '****************************************************
                'Check if Student has met prereqs.
                '****************************************************
                If (boolPreReq = True) Then
                    metPrereqs = True
                Else
                    metPrereqs = br.HasStdMetPrereqs(stuEnrollment, clsSectId, prgVerId, campusid, "false")
                    If Not metPrereqs Then
                        If showCcr Then
                            Dim intRowCount As Integer
                            intRowCount = (New RegisterStudentsBR).IsCourseASHLevelCourse(dr("ReqId").ToString, "SH")
                            If intRowCount >= 1 Then
                                metPrereqs = True
                            End If
                        End If
                    End If
                End If

                hasOverride = db.DoesStdHaveOverride_Sp(stuEnrollment, clsSectId)

                If metPrereqs = True Or hasOverride >= 1 Then
                    anyCoreqs = db.DoesReqHaveCoReqs_Sp(req)
                    If anyCoreqs = 1 Then
                        coreqs = db.GetCourseCoreqs_Sp(req, termId)
                        For Each row3 In coreqs.Tables(0).Rows
                            coReqId = row3("PreCoReqId").ToString
                            attmptd = db.HasStdAttemptedReq_Sp(stuEnrollment, clsSectId, coReqId)

                            If attmptd = 1 Then 'If attempted Coreq
                                If gradesFormat = "numeric" Then
                                    isPass = db.DoesStdHavePassingGrdForNumeric_sp(stuEnrollment, coReqId, prgVerId, clsSectId)
                                    'IsPass = db.DoesStdHavePassingGrdForNumeric_sp(StuEnrollment, Req, PrgVerId, ClsSectId)
                                Else
                                    isPass = db.DoesStdHavePassingGrd_sp(stuEnrollment, coReqId, prgVerId, clsSectId)
                                    'IsPass = db.DoesStdHavePassingGrd_sp(StuEnrollment, Req, PrgVerId, ClsSectId)
                                End If
                                If isPass Then
                                    Exit For
                                End If
                            End If

                            If checkConflicts Then
                                ds1 = db.GetCoreqClsSects_SP(req, termId)
                                CoreqClsSects = ds1
                                For Each row2 In ds1.Tables(0).Rows
                                    coreqClsSect = row2("ClsSectionId").ToString
                                    Dim dsScheduleConflicts As DataSet
                                    dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsDuringRegistration(coreqClsSect, stuEnrollment)
                                    If Not dsScheduleConflicts Is Nothing AndAlso dsScheduleConflicts.Tables(0).Rows.Count >= 1 Then
                                        stdAvailable = False
                                    Else
                                        'Hence add student to the coreq DT.
                                        Dim dr1 As DataRow = tbl3.NewRow
                                        'dr1("StudentId") = Student
                                        dr1("StuEnrollId") = stuEnrollment
                                        dr1("TestId") = coreqClsSect
                                        tbl3.Rows.Add(dr1)
                                        Exit For 'Exit inner loop
                                    End If
                                Next
                                If stdAvailable = False Then
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Else
                    'stdAvailable = False
                    Exit Try
                End If

                If stdAvailable = True Then
                    row = tempDt.NewRow()
                    row("StuEnrollId") = stuEnrollment
                    row("ReqId") = req
                    row("LastName") = lName
                    row("FirstName") = fName
                    row("ExpGradDate") = expGradDate
                    row("StudentId") = studentId
                    tempDt.Rows.Add(row)
                End If



            Catch ex As Exception
                Debug.WriteLine(ex.Message)
                Throw
            End Try

        Next

        Return ds
    End Function

    Public Function ProcessSelectedStdsFrClsSect(ByVal ds As DataSet, ByVal clsSectId As String, ByVal termId As String) As DataSet
        Dim anyCoreqs As Integer
        Dim ds1 As DataSet
        Dim coreqClsSect As String
        Dim coreqs As DataSet
        Dim attmptd As Integer
        Dim tbl3 As New DataTable
        Dim db As New RegisterDB
        Dim dr As DataRow
        Dim row As DataRow
        Dim student As String
        Dim req As String
        Dim dt As DataTable
        Dim isReg As Integer
        Dim row3 As DataRow
        Dim prgVerId As String
        Dim isPass As Boolean
        Dim stdAvailable As Boolean
        Dim row2 As DataRow
        Dim errStr As String
        Dim br As New RegisterStudentsBR
        Dim stuEnrollment As String

        dt = ds.Tables("StdCoreqs")
        'Clean of duplicated the Available Student Table by copy only no duplicate records to cleanDt **********************************************
        If (ds.Tables("SelectedStuds").Rows.Count > 0) Then
            Dim clean = ds.Tables("SelectedStuds").AsEnumerable().GroupBy(Function(x) x.Field(Of Guid)("StudentId")).[Select](Function(g) g.First())
            Dim cleanDt As DataTable = clean.CopyToDataTable()
            ds.Tables("SelectedStuds").Clear()
            ds.Tables("SelectedStuds").Merge(cleanDt)
        End If
        ' ******************************************************************************************************************************************

        For Each dr In ds.Tables("SelectedStuds").Rows
            stuEnrollment = dr("StuEnrollId").ToString
            req = dr("ReqId").ToString
            anyCoreqs = db.DoesReqHaveCoReqs_Sp(req)
            If anyCoreqs = 1 Then
                'Get the Course CoReqs
                coreqs = db.GetCourseCoreqs_Sp(req, TermId)
                For Each row3 In coreqs.Tables(0).Rows

                    ds1 = db.GetCoreqClsSects_SP(req, TermId)
                    If Not (ds1 Is Nothing) Then
                        For Each row In ds1.Tables(0).Rows
                            coreqClsSect = row("ClsSectionId").ToString
                            isReg = db.IsStdRegFrClsSect_SP(stuEnrollment, coreqClsSect)
                            If isReg = 1 Then
                                Dim dr1 As DataRow = dt.NewRow
                                'dr1("StudentId") = Student
                                dr1("StuEnrollId") = stuEnrollment
                                dr1("TestId") = coreqClsSect
                                dt.Rows.Add(dr1)
                                Exit For
                            End If
                        Next
                    End If
                Next
            End If
        Next
        Return ds

    End Function

    Public Function GetAllTerms() As DataSet

        '   Instantiate DAL component
        Dim grdPostDB As New GrdPostingsDB

        '   get the dataset with all degrees
        Return grdPostDB.GetAllTerms()

    End Function
    Public Function GetCurrentAndFutureTerms(Optional ByVal campusId As String = "", Optional ByVal activeOnly As Boolean = False) As DataSet

        '   Instantiate DAL component
        Dim grdPostDB As New GrdPostingsDB

        '   get the dataset with all degrees
        Return grdPostDB.GetCurrentAndFutureTerms(campusId, activeOnly)
    End Function
    Public Function GetCurrentAndFutureTermsRegStds() As DataSet

        '   Instantiate DAL component
        Dim grdPostDB As New GrdPostingsDB

        '   get the dataset with all degrees
        Return grdPostDB.GetCurrentAndFutureTermsRegStds
    End Function

    Public Function GetTermsFromStartDate(ByVal stuEnrollId As String, Optional ByVal campusId As String = "") As DataSet
        '   get the dataset with terms from StartDate of the enrollment
        Return (New GrdPostingsDB).GetTermsFromStartDate(stuEnrollId, campusId)
    End Function

    Public Function GetAllStatuses() As DataSet

        '   Instantiate DAL component
        Dim db As New RegisterDB

        '   get the dataset with all degrees
        Return db.GetAllStatuses()

    End Function

    Public Function GetAllShifts() As DataSet
        Dim db As New ShiftsDB
        Dim ds As DataSet

        ds = db.GetAllShifts

        Return ds

    End Function

    ''The terms drop down list should show the terms related to the students prgverid and those mapped to all programs
    Public Function GetTermsFromStudentsProgramandStartDate(ByVal stuEnrollId As String, Optional ByVal campusId As String = "") As DataSet
        '   get the dataset with terms from StartDate of the enrollment
        Return (New GrdPostingsDB).GetTermsFromStudentsProgramandStartDate(stuEnrollId, campusId)
    End Function

    Public Function HasStudentPassedSpeedTestsForCourse(ByVal stuEnrollId As String, ByVal reqId As String) As Boolean
        Return (New RegisterDB).HasStudentPassedSpeedTestsForCourse_sp(stuEnrollId, reqId)
    End Function

    Public Function IsStudentHold(ByVal stuEnrollId As String) As String
        Return (New RegisterDB).IsStudentHold(stuEnrollId)
    End Function

    Public Function IsStudentTranscriptHold(ByVal stuEnrollId As String) As String
        Return (New RegisterDB).IsStudentTranscriptHold(stuEnrollId)
    End Function

    Public Function IsStudentEligibleForRegistration(ByVal stuEnrollId As String, ByVal clsSectionId As String) As String
        Return (New RegisterDB).IsStudentEligibleForRegistration(stuEnrollId, clsSectionId)
    End Function

    Public Function GetProgAcademicCalendar(ByVal stuEnrollId As String) As String

        Dim db As New RegisterDB
        Dim acadCal As String

        acadCal = db.GetProgAcademicCalendar(stuEnrollId)

        Return acadCal
    End Function
    Public Function DoesStdHaveOverride(ByVal stuEnrollId As String, ByVal prereqId As String) As Boolean

        Dim db As New RegisterDB
        Dim intOverRide As Integer

        intOverRide = db.DoesStdHaveOverride(stuEnrollId, prereqId)

        If intOverRide > 0 Then Return True

        Return False
    End Function

    Public Shared Function GetStudentIdFromEnrollment(enrollmentId) As String
        Dim db As New RegisterDB
        Dim result As String = db.GetStudentIdFromEnrollment(enrollmentId)
        Return result

    End Function

    Public Shared Function GetActiveEnrollmentsWithRequerimentId(studentId As String, reqid As String, Optional ByVal overridePostAcademics As Boolean = False) As IList(Of String)
        Dim db As New RegisterDB
        Dim result As List(Of String) = db.GetActiveEnrollmentsWithRequerimentId(studentId, reqid, overridePostAcademics)
        Return result
    End Function

    ''' <summary>
    ''' Get all enrollments id from the student with the Requirement given. All enrollments of the student level are gotten
    ''' (drop, externship transfer out etc)
    ''' </summary>
    ''' <param name="studentId"></param>
    ''' <param name="reqId"></param>
    ''' <returns>List of Enrollment Id as string list</returns>
    Public Shared Function GetAllStudentEnrollmentsWithRequerimentId(ByVal studentId As String, ByVal reqId As String) As List(Of String)
        Dim db As New RegisterDB
        Dim result As List(Of String) = db.GetAllStudentEnrollmentsWithRequerimentId(studentId, reqId)
        Return result
    End Function


    ''' <summary>
    ''' Return the list of Enrollments associated with a classId that can be used to enter or modify Registration Information
    '''  It is similar to below procedure, only considerer Future Start in the case of ClassId begin after the start day of the enrollment
    ''' </summary>
    ''' <param name="enrollId"></param>
    ''' <param name="classId"></param>
    ''' <returns></returns>
    Public Shared Function GetActiveEnrollmentsWithClassAssociatedForRegisterClass(ByVal enrollId As String, ByVal classId As String) As List(Of String)
        Dim db As New RegisterDB
        Dim result As List(Of String) = db.GetActiveEnrollmentsWithClassAssociatedForRegisterClass(enrollId, classId)
        Return result
    End Function


    ''' <summary>
    ''' Return the list of Enrollments associated with a classId that can be used to enter or modify Academic Information (Attendance & Score) 
    ''' </summary>
    ''' <param name="enrollId"></param>
    ''' <param name="classId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetActiveEnrollmentWithClassAssociated(ByVal enrollId As String, ByVal classId As String) As List(Of String)
        Dim db As New RegisterDB
        Dim result As List(Of String) = db.GetActiveEnrollmentWithClassAssociated(enrollId, classId)
        Return result
    End Function

  
    ''' <summary>
    ''' Check if a determinate enrollment is Already registered for a determinate Class
    ''' </summary>
    ''' <param name="stuEnrollId">Enrollment to be checked</param>
    ''' <param name="classId">Curse again is checked the enrollment as registered</param>
    ''' <returns>Already Registered if registered if not empty string</returns>
    ''' <remarks></remarks>
    Public Shared Function CheckIfEnrollmentIsAlreadyRegister(ByVal stuEnrollId As String, ByVal classId As String) As String
        Dim db As New RegisterDB
        Dim result As String = db.CheckIfEnrollmentIsAlreadyRegister(stuEnrollId, classId)
        Return result
    End Function

    Public Shared Function GetRegisterInformation(stuEnrollIdList As List(Of String), reqId As String) As List(Of ArResultsClass)
        Dim db As New RegisterDB
        Dim list As List(Of ArResultsClass) = db.GetRegisterInformation(stuEnrollIdList, reqId)
        Return list
    End Function


    Public Shared Sub RegisterClassInEnrollmentsOfStudents(ByVal registrationInfo As List(Of RegistrationClassInfo))
        Dim db As New RegisterDB
        db.RegisterClassInEnrollmentsOfStudents(registrationInfo)
    End Sub

    Public Shared Sub UnRegisterClassInEnrollmentsOfStudent(ByVal registrationInfo As List(Of RegistrationClassInfo))
        Dim db As New RegisterDB
        db.UnRegisterClassInEnrollmentsOfStudent(registrationInfo)
    End Sub

    Public Shared Function GetAllAcademicInformationForOneClass(registeredEnrollment As CorrectionRegInfo, arInfo As ArResultsClass) As CorrectionRegInfo
        Dim db As New RegisterDB
        Dim info = db.GetAllAcademicInformationForOneClass(registeredEnrollment, arInfo)
        Return info
    End Function


    Public Shared Sub InsertAllcademicInformationInDataBase(ByVal correctionRegInfos As IList(Of CorrectionRegInfo))
        Dim db As New RegisterDB
        'db.InsertAllcademicInformationInDataBase(correctionRegInfos)
        db.InsertAllcademicInformationInDataBaseInBulk(correctionRegInfos)
    End Sub


End Class
