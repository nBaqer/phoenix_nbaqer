' ===============================================================================
' StudentsFacade.vb
' Business Logic for interacting with Students
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports FAME.AdvantageV1.DataAccess.AR
Imports FAME.AdvantageV1.Common.AR

Namespace AR

#Region "Student Master"

    Public Class StudentMasterFacade
        Public Function UpdateStudentMaster(ByVal StudentInfo As StudentMasterInfo, ByVal user As String, ByVal strObjective As String) As String
            With New StudentMasterDB
                If (StudentInfo.IsInDB = False) Then
                    Return .AddStudent(StudentInfo, user, strObjective)
                Else
                    Return .UpdateStudent(StudentInfo, user, strObjective)
                End If
            End With
        End Function
        Public Function DeleteStudentMaster(ByVal studentid As String, ByVal modDate As DateTime) As String
            With New StudentMasterDB
                Return .DeleteStudent(studentid, modDate)
            End With
        End Function
        Public Function GetStudentInfo(ByVal StudentId As String) As StudentMasterInfo
            Dim GetStudent As New StudentMasterDB
            With GetStudent
                Return .GetStudentInfo(StudentId)
            End With
        End Function
        Public Function checkChildExists(ByVal EducationInstId As String) As Integer
            Dim getCheckChild As New StudentMasterDB
            With getCheckChild
                Return .checkChildExists(EducationInstId)
            End With
        End Function

        Public Function AddStudentIdFormat(ByVal strFormatType As String, ByVal strYearNumber As Integer, ByVal strMonthNumber As Integer, ByVal strDateNumber As Integer, ByVal strLNameNumber As Integer, ByVal strFNameNumber As Integer, ByVal strSeqNumber As Integer, ByVal strStartingSeqNumber As Integer, ByVal user As String) As Integer
            Dim getStudent As New StudentMasterDB
            With getStudent
                Return .AddStudentIdFormat(strFormatType, strYearNumber, strMonthNumber, strDateNumber, strLNameNumber, strFNameNumber, strSeqNumber, strStartingSeqNumber, user)
            End With
        End Function
        Public Function GetStudentFormat() As StudentMasterInfo
            Dim getStudent As New StudentMasterDB
            Return getStudent.GetStudentFormat()
        End Function
        Public Function GetDependencyType() As DataSet
            Dim getDDLData As New StudentMasterDB
            Return getDDLData.GetAllDependencyType()
        End Function
        Public Function GetGeographicType() As DataSet
            Dim getDDLData As New StudentMasterDB
            Return getDDLData.GetAllGeographicType()
        End Function
        Public Function GetAdminCriteria() As DataSet
            Dim getDDLData As New StudentMasterDB
            Return getDDLData.GetAllAdminCriteria()
        End Function
        Public Function GetHousing() As DataSet
            Dim getDDLData As New StudentMasterDB
            Return getDDLData.GetAllHousingType()
        End Function
    End Class

#End Region

#Region "Student Enrollments"
    Public Class StudentEnrollmentsFacade
        Public Shared Function GetEnrollmentStatusCodes() As DataSet
            Return StudentEnrollmentsDB.GetEnrollmentStatusCodes()
        End Function
    End Class
#End Region

#Region "Attendance"
    Public Class AttendanceFacade
        ''Optional parameter CohortStartDate added by Saraswathi

        Public Shared Function GetStudentSchedulesForAttendance( _
                                    ByVal filterProgId As String, _
                                    ByVal filterPrgVerId As String, _
                                    ByVal filterStuName As String, _
                                    ByVal filterStuStatus As String, _
                                    ByVal filterStartDate As String, _
                                    ByVal filterEndDate As String, _
                                    ByVal filterUseTimeClock As String, _
                                    ByVal filterBadgeNum As String, _
                                    Optional ByVal campusId As String = "", _
                                    Optional ByVal Studentgrpid As String = "", _
                                    Optional ByVal CohortStartDate As String = "", _
                                    Optional ByVal filteroutofschoolfilter As String = "") As DataSet
            Dim ds As New DataSet
            ds = AttendanceDB.GetStudentSchedulesForAttendance( _
                                    filterProgId, filterPrgVerId, filterStuName, _
                                    filterStuStatus, filterStartDate, filterEndDate, _
                                    filterUseTimeClock, filterBadgeNum, campusId, Studentgrpid, CohortStartDate, filteroutofschoolfilter)
            Return ds
        End Function
        ''Parameters made similar to get student schedule for attendance
        ''Modified by Saraswathi lakshmanan on april 12 2010
        ''To fix mantis case 18806 Ross Snow DAys
        Public Shared Function GetClockHourAttendance( _
                                    ByVal filterProgId As String, _
                                    ByVal filterPrgVerId As String, _
                                    ByVal filterStuName As String, _
                                    ByVal filterStuStatus As String, _
                                    ByVal filterStartDate As String, _
                                    ByVal filterEndDate As String, _
                                    ByVal filterUseTimeClock As String, _
                                      ByVal filterBadgeNum As String, _
                                    Optional ByVal campusId As String = "", _
                                    Optional ByVal Studentgrpid As String = "", _
                                    Optional ByVal CohortStartDate As String = "", _
                                    Optional ByVal filteroutofschoolfilter As String = "") As DataSet

            Dim ds As New DataSet
            ds = AttendanceDB.GetClockHourAttendance( _
                                    filterProgId, filterPrgVerId, filterStuName, _
                                    filterStuStatus, filterStartDate, filterEndDate, _
                                    filterUseTimeClock, filterBadgeNum, campusId, Studentgrpid, CohortStartDate, filteroutofschoolfilter)
            Return ds
        End Function
        Public Shared Function GetClockHourAttendance( _
                                 ByVal stuEnrollid As String, ByVal filterStartDate As String, ByVal filterEndDate As String) As DataSet

            Dim ds As New DataSet
            ds = AttendanceDB.GetClockHourAttendance(stuEnrollid, filterStartDate, filterEndDate)
            Return ds
        End Function
        Public Shared Function GetStatusCodes(ByVal CampusId As String, Optional ByVal strStatus As String = "Active", Optional ByVal StudentEnrollmentStatus As String = "") As DataSet
            Dim ds As New DataSet
            ds = (New AdReqsDB).GetStatusCodes(CampusId, strStatus, StudentEnrollmentStatus)
            Return ds
        End Function
        Public Shared Function DoesProgramVersionUseTimeClock(ByVal filterprgverId As String) As Boolean
            Dim boolProgramVersion As Boolean
            boolProgramVersion = AttendanceDB.DoesProgramVersionUseTimeClock(filterprgverId)
            Return boolProgramVersion
        End Function
        ''CampusID validation added for Ross Snow Days issue
        ''parameter added by Saraswathi lakshmanan on April 1 2010

        ''' <summary>
        ''' Converts a dataset to an array of ClockHourAttendanceInfo objects
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DSToClockHourAttendanceInfoArray(ByVal ds As DataSet) As ClockHourAttendanceInfo()
            Dim c As Integer = ds.Tables(0).Rows.Count
            Dim res(c - 1) As ClockHourAttendanceInfo

            For i As Integer = 0 To c - 1
                Try
                    res(i) = New ClockHourAttendanceInfo
                    Dim dr As DataRow = ds.Tables(0).Rows(i)
                    res(i).IsInDB = (dr.RowState = DataRowState.Modified)
                    res(i).StuEnrollId = dr("StuEnrollId").ToString()
                    res(i).ScheduleId = dr("ScheduleId").ToString()
                    res(i).Source = dr("Source").ToString
                    If Not DBNull.Value.Equals(dr("RecordDate")) Then
                        res(i).RecordDate = dr("RecordDate")
                    End If
                    If dr("ActualHours").ToString.Length > 0 Then
                        If dr("ActualHours") = "999.00" Then
                            res(i).ActualHours = "0.00"
                        ElseIf dr("ActualHours") = "9999.00" Then 'this code is set only when attendance is deleted
                            res(i).ActualHours = "9999.00"
                        Else
                            res(i).ActualHours = dr("ActualHours")
                        End If
                    Else
                        res(i).ActualHours = "999.00" 'if Actual Hours is nothing
                    End If
                    If Not DBNull.Value.Equals(dr("Tardy")) Then
                        res(i).Tardy = dr("Tardy")
                    End If
                    If Not DBNull.Value.Equals(dr("SchedHours")) Then
                        res(i).SchedHours = dr("SchedHours")
                    End If
                    If Not DBNull.Value.Equals(dr("UnitTypeDescrip")) Then
                        res(i).UnitTypeDescrip = dr("UnitTypeDescrip")
                    Else
                        res(i).UnitTypeDescrip = ""
                    End If
                    If Not DBNull.Value.Equals(dr("PostByException")) Then
                        res(i).PostByException = dr("PostByException")
                    Else
                        res(i).PostByException = "no"
                    End If
                Catch ex As System.Exception
                End Try
            Next
            Return res
        End Function
        Public Function StudentSummaryForm(ByVal StuEnrollId As String, ByVal AttUnitType As String) As ClockHourAttendanceInfo
            Dim stuDB As New AttendanceDB
            Return (stuDB).StudentSummaryForm(StuEnrollId, AttUnitType)
        End Function
        Public Sub UpdateScheduleByDateAndEnrollment(ByVal StuEnrollId As String, _
                                                          ByVal Source As String, _
                                                          ByVal PostedDate As Date, _
                                                          Optional ByVal ddlSDH0ChangedValue As String = "")
            Dim stuDB As New AttendanceDB
            stuDB.UpdateScheduleByDateAndEnrollment(StuEnrollId, Source, PostedDate, ddlSDH0ChangedValue)
        End Sub
        Public Function GetStudentSummaryDetails(ByVal StuEnrollId As String, Optional ByVal AttUnitType As String = "") As DataSet
            Dim stuDB As New AttendanceDB
            Return (stuDB).GetStudentSummaryDetails(StuEnrollId, AttUnitType)
        End Function
        Public Function GetScheduledHoursByWeek(ByVal StuEnrollId As String) As Decimal
            Dim stuDB As New AttendanceDB
            Return (stuDB).GetScheduledHoursByWeek(StuEnrollId)
        End Function
        Public Function GetScheduledHoursByWeek_SP(ByVal StuEnrollId As String) As Decimal
            Dim stuDB As New AttendanceDB
            Return (stuDB).GetScheduledHoursByWeek_SP(StuEnrollId)
        End Function
        Public Function GetAttendanceByEnrollment(ByVal StuEnrollId As String) As DataSet
            Dim stuDB As New AttendanceDB
            Return (stuDB).GetAttendanceByEnrollment(StuEnrollId)
        End Function
        Public Function GetLDAForDroppedStudent(ByVal StuEnrollId As String, Optional ByVal SysStatusId As Integer = 12) As String
            Dim stuDB As New AttendanceDB
            Return (stuDB).GetLDAForDroppedStudent(StuEnrollId, SysStatusId)
        End Function
        Public Function GetAttendanceSummary(ByVal StuEnrollId As String, Optional ByVal PrgVerId As String = "") As ClockHourAttendanceInfo
            Dim stuDB As New AttendanceDB
            Return (stuDB).GetAttendanceSummary(StuEnrollId, PrgVerId)
        End Function
        Public Function GetAttendanceSummaryForDateRange(ByVal StuEnrollId As String,ByVal startDate As Date, ByVal cutOffDate As Date, Optional campusid As String = "") As ClockHourAttendanceInfo
            Dim stuDB As New AttendanceDB
            Return (stuDB).GetAttendanceSummaryForDateRange(StuEnrollId,startDate, cutOffDate, campusid)
        End Function
        Public Function GetAttendanceSummaryForCutOffDate(ByVal StuEnrollId As String, ByVal cutOffDate As Date, Optional campusid As String = "") As ClockHourAttendanceInfo
            Dim stuDB As New AttendanceDB
            Return (stuDB).GetAttendanceSummaryForCutOffDate(StuEnrollId, cutOffDate, campusid)
        End Function
        Public Function GetAttendanceSummaryForCutOffDate_SP(ByVal StuEnrollId As String, ByVal cutOffDate As Date, ByVal unitTypeId As String, ByVal tardiesMakingAbsence As Integer) As ClockHourAttendanceInfo
            Dim stuDB As New AttendanceDB
            Return (stuDB).GetAttendanceSummaryForCutOffDate_SP(StuEnrollId, cutOffDate, unitTypeId, tardiesMakingAbsence)
        End Function

        Public Function GetSAPOffsetDate(ByVal StuEnrollId As String, ByVal CampusId As String, ByVal TriggerValue As Decimal, byval TriggerUnitType As integer, Optional byval IncludeExternship As boolean = False, Optional byval InludeTransferHours As Boolean = False)
             Dim stuDB As New AttendanceDB
            Return (stuDB).GetSAPOffsetDate(StuEnrollId, CampusId, TriggerValue, TriggerUnitType,IncludeExternship,InludeTransferHours)
        End Function
        ''Added by Saraswathi to find if the date given is a LOA date for a student
        ''NOv 17 2008
        Public Function IsStudentLOADate(ByVal StuEnrollId As String, ByVal HDate As Date) As Boolean
            Dim stuDB As New AttendanceDB
            Return (stuDB).IsStudentLOADate(StuEnrollId, HDate)
        End Function
        ''Added by Saraswathi to find if the date given is a Suspended date for a student
        ''NOv 17 2008
        Public Function IsStudentSuspendedDate(ByVal StuEnrollId As String, ByVal HDate As Date) As Boolean
            Dim stuDB As New AttendanceDB
            Return (stuDB).IsStudentSuspendedDate(StuEnrollId, HDate)
        End Function

         ''Added by Edwin To get student status history for all applicable enrollments
        ''2/19/2019
        Public Shared Function GetStudentStatusHistory(ByVal prgVerId As String, ByVal campusId As String ) As DataSet
            Dim stuDB As New AttendanceDB
            Dim ds As New DataSet
            ds = (stuDB).GetStudentStatusHistory(prgVerId, campusId)
            Return ds
        End Function
        Public Function IsHoliday(ByVal hDate As Date, ByVal CAmpusID As String) As Boolean
            Dim ds As DataSet
            ds = (New HolidaysDB).GetAllHolidays(CAmpusID)
            Dim holidayTable As System.Data.DataTable = ds.Tables(0)
            For i As Integer = 0 To holidayTable.Rows.Count - 1
                If hDate >= holidayTable.Rows(i)("HolidayStartDate") And hDate <= holidayTable.Rows(i)("HolidayEndDate") Then
                    Return True
                End If
            Next
            Return False
        End Function
        Public Shared Function SaveClockHourAttendanceArray(ByVal info() As ClockHourAttendanceInfo, ByVal user As String, ByVal CampusID As String) As String
            Return AttendanceDB.SaveClockHourAttendanceArray(info, user, CampusID)
        End Function
        Public Function PostClockAttendance(ByVal xmlRules As String) As String
            Dim db As New AttendanceDB
            db.PostClockAttendance(xmlRules)
            Return ""
        End Function
    End Class

#End Region
End Namespace