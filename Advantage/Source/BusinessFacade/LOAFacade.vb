Imports FAME.AdvantageV1.BusinessFacade.StudentEnrollState

Public Class LOAFacade
    Public Function PlaceStudentOnLOA(ByVal change As StudentChangeHistoryObj) As String
        Dim db As New LOADB
        Dim studentStatus As StateEnum
        Dim createObj As New StateFactory
        Dim batchdb As BatchStatusChangeDB = New BatchStatusChangeDB()

        'Retrieve current status of student
        studentStatus = db.GetStudentStatus(change.StuEnrollId)
        Dim stateObj As IStudentEnrollmentState = StateFactory.CreateStateObj(studentStatus)
        Dim result As String = stateObj.PlaceStudentOnLOA(change)
        Return result

    End Function
    ' US2135 01/05/2012 Janet Robinson added RequestDate
    Public Function PlaceStudentOnLOA(ByVal campusId As String, ByVal stuEnrollId As String, ByVal loaReasonId As String, ByVal startDate As String, ByVal endDate As String, ByVal loaStatus As String, ByVal user As String, ByVal requestDate As String) As String
        Dim db As New LOADB
        Dim studentStatus As StateEnum
        Dim createObj As New StateFactory


        'Retrieve current status of student
        studentStatus = db.GetStudentStatus(stuEnrollId)


        'Based on current status on student, create appropriate state object
        Dim stateObj As IStudentEnrollmentState = StateFactory.CreateStateObj(studentStatus)
        Dim change As StudentChangeHistoryObj = New StudentChangeHistoryObj()
        change.StuEnrollId = stuEnrollId
        change.CampusId = campusId
        change.NewStatusGuid = loaStatus
        change.DropReasonGuid = Nothing
        change.LoaReasonGuid = loaReasonId
        change.StartDate = startDate
        change.EndDate = endDate
        change.ModUser = user
        change.IsRevelsal = 0
        change.DateOfChange = startDate
        change.LOARequestDate = requestDate
        change.ModDate = DateTime.Now
        change.OrigStatusGuid = db.GetStudentStatusId(stuEnrollId).ToString()
        Dim result As String = stateObj.PlaceStudentOnLOA(change)
        Return result


        'Return stateObj.PlaceStudentOnLOA(StuEnrollId, LOAReasonId, StartDate, EndDate, LOAStatus, user, RequestDate)

    End Function

    Public Function GetLOAStatuses() As DataSet

        '   Instantiate DAL component
        Dim db As New LOADB
        Dim ds As DataSet

        '   get the dataset with all drop statuses          
        ds = db.GetLOAStatuses()
        Return ds

    End Function

    Public Function GetStudentStartDate(ByVal stuEnrollId As String) As String

        '   Instantiate DAL component
        Dim db As New LOADB

        Return db.GetStudentStartDate(stuEnrollId)

    End Function

    Public Function GetSuspensionStatuses() As DataSet

        '   Instantiate DAL component
        Dim db As New ProbationDB
        Dim ds As DataSet

        '   get the dataset with all drop statuses          
        ds = db.GetSuspensionStatuses()
        Return ds

    End Function
    '' Call fill status
    '' Parameters sysStatusList  String  "11" --> suspension
    ''            Optional ShowActiveOnly = True filter only active, Default false. If is Omited or false should not filter
    ''            Optional CampusId  Current Campus Id, Defalut Null, if is ommited, then get all status for the corp campus group.
    ''            .DataSource = facade.GetStatusList( "11", "Active", campusid) 
    Public Function GetStatusCodeForAGivenList(ByVal sysStatusIdList As String, Optional ByVal showActiveOnly As Boolean = False, Optional ByVal campusId As String = Nothing) As DataSet
        '   Instantiate DAL component
        Dim db As New ProbationDB
        Dim ds As DataSet

        '   get the dataset with all drop statuses          
        ds = db.GetStatusCodeForAGivenList(sysStatusIdList, showActiveOnly, campusId)
        Return ds
    End Function

    Public Function PlaceStudentOnSuspension(ByVal change As StudentChangeHistoryObj) As String
        Dim db As New LOADB
        Dim studentStatus As StateEnum
        Dim createObj As New StateFactory
        Dim statusDb As New BatchStatusChangeDB

        'Retrieve current status of student
        studentStatus = db.GetStudentStatus(change.StuEnrollId)

        'Based on current status on student, create appropriate state object
        Dim stateObj As IStudentEnrollmentState = StateFactory.CreateStateObj(studentStatus)

        change.OrigStatusGuid = statusDb.GetSchoolDefinedStatusGuid(studentStatus, change.CampusId)
        Return stateObj.PlaceStudentOnSuspension(change)
    End Function

    Public Function PlaceStudentOnSuspension(ByVal campusId As String, ByVal stuEnrollId As String, ByVal suspensionStatusId As String, ByVal reason As String, ByVal startDate As String, ByVal endDate As String, ByVal user As String) As String
        Dim db As New LOADB
        Dim studentStatus As StateEnum
        Dim createObj As New StateFactory
        Dim statusDb As New BatchStatusChangeDB

        'Retrieve current status of student
        studentStatus = db.GetStudentStatus(stuEnrollId)

        'Based on current status on student, create appropriate state object
        Dim stateObj As IStudentEnrollmentState = StateFactory.CreateStateObj(studentStatus)

        Dim change As StudentChangeHistoryObj = New StudentChangeHistoryObj()
        change.StuEnrollId = stuEnrollId
        change.NewStatusGuid = suspensionStatusId
        change.DropReasonGuid = Nothing
        change.Reason = reason
        change.StartDate = startDate
        change.EndDate = endDate
        change.ModUser = user
        change.IsRevelsal = 0
        change.DateOfChange = startDate
        change.CampusId = campusId
        change.ModDate = DateTime.Now
        change.OrigStatusGuid = statusDb.GetSchoolDefinedStatusGuid(studentStatus, campusId)
        Return stateObj.PlaceStudentOnSuspension(change)
    End Function

    Public Function GetStudentTutionEarningMethod_Sp(ByVal stuEnrollId As String) As Integer
        Return (New LOADB).GetStudentTutionEarningMethod_Sp(stuEnrollId)
    End Function

    Public Function AreStudentLOADatesOverlapping_GivenLOAID(ByVal stuEnrollId As String, ByVal startDate As Date, ByVal enddate As Date, ByVal lOaid As String) As String
        Return (New LOADB).AreStudentLOADatesOverlapping_GivenLOAID(stuEnrollId, startDate, enddate, lOaid)
    End Function

    Public Function StudentLOACount(ByVal stuEnrollId As String) As Integer
        Dim loaCnt As Integer
        Return (New LOADB).StudentLOACount(stuEnrollId)
        'Return LOACnt
    End Function
    Public Function CheckAttendanceInLDADates(ByVal stuEnrollId As String, startDate As DateTime, endDate As DateTime) As Boolean

        Return (New LOADB).CheckAttendanceInLDADates(stuEnrollId, startDate, endDate)

    End Function

    Public Function GetLoaReason(ByVal campusid As String) As DataSet

        '   Instantiate DAL component
        Dim db As New LOADB
        Dim ds As DataSet

        '   get the dataset with all drop statuses          
        ds = db.GetLoaReasons(campusid)

        Return ds
    End Function
End Class
