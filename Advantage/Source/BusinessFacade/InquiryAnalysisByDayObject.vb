
Public Class InquiryAnalysisByDayObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim inqByDay As New InquiryAnalysisByDayDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        'ds = inqByDay.GetLeadsByDay(rptParamInfo)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        'Return ds
        Return BuildReportSource(inqByDay.GetLeadsByDay(rptParamInfo))
    End Function

    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        If ds.Tables.Count > 0 Then
            Dim dt As DataTable = ds.Tables("InquiryByDay")
            For Each dr As DataRow In dt.Rows
                If dr.IsNull("SourceAdvDescrip") Then
                    dr("SourceAdvDescrip") = ""
                End If
            Next
        End If
        Return ds
    End Function

#End Region


End Class
