' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' StatesFacade.vb
'
' StatesFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class StatesFacade
    Public Function GetAllStates() As DataSet
        '   Instantiate DAL component
        With New StatesDB
            '   get the dataset with all States
            Return .GetAllStates()
        End With
    End Function
    Public Shared Function GetStatesIPEDS() As DataTable
        Dim dtStates As DataTable = StatesDB.GetStatesIPEDS()
        Dim dtStatesSorted As DataTable
        Dim drState As DataRow
        Dim i As Integer
        Dim rowsSorted() As DataRow

        With dtStates
            drState = .NewRow
            drState("StateDescrip") = IPEDSCommon.StateUnknownDescrip
            drState("FIPSCode") = IPEDSCommon.StateUnknownFIPSCode
            .Rows.Add(drState)

            drState = .NewRow
            drState("StateDescrip") = IPEDSCommon.StateForeignCountryDescrip
            drState("FIPSCode") = IPEDSCommon.StateForeignCountryFIPSCode
            .Rows.Add(drState)

            drState = .NewRow
            drState("StateDescrip") = IPEDSCommon.StateResUnreportedDescrip
            drState("FIPSCode") = IPEDSCommon.StateResUnreportedFIPSCode
            .Rows.Add(drState)

            .Columns.Add("SortGroup", GetType(Integer))
            For i = 0 To dtStates.Rows.Count - 1
                .Rows(i)("SortGroup") = IPEDSCommon.StateRptSortGroup(.Rows(i)("StateDescrip"))
            Next
        End With

        rowsSorted = dtStates.Select("", "SortGroup, StateDescrip")
        dtStatesSorted = dtStates.Copy
        dtStatesSorted.Rows.Clear()
        For i = 0 To rowsSorted.GetUpperBound(0)
            drState = dtStatesSorted.NewRow
            drState("StateId") = rowsSorted(i)("StateId")
            drState("StateDescrip") = rowsSorted(i)("StateDescrip")
            drState("FIPSCode") = rowsSorted(i)("FIPSCode")
            drState("SortGroup") = rowsSorted(i)("SortGroup")
            dtStatesSorted.Rows.Add(drState)
        Next

        Return dtStatesSorted

    End Function
End Class
