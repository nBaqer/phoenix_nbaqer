Imports System.data
Imports System.Data.OleDb
'Imports FAME.AdvantageV1.BusinessRules
Imports FAME.AdvantageV1.DataAccess
Public Class StatusCodeFacade
    Public Function InitialLoadPage() As DataSet
        Dim ds As DataSet
        'Dim da As OleDbDataAdapter
        'Dim olddt As DataTable
        'Dim newdt As DataTable
        Dim db As New StatusCodeDB

        ds = db.GetDropDownList()

        Return ds
    End Function
    Public Function PopulateDataList(ByVal StatusLevel As String, ByVal Status As String, ByVal CampusGroupId As String) As DataSet

        Dim db As New StatusCodeDB
        Dim ds As DataSet
        Dim da As New OleDbDataAdapter
        ds = db.GetStatusCodes(StatusLevel, Status, CampusGroupId)
        'ds = ConcatenateStudentLastFirstName(ds)
        Return ds
    End Function

    ''' <summary>
    ''' This filter the Disciplinary probation and Warning Probation status
    ''' Those status are in the table but they are not used in moment
    ''' </summary>
    ''' <param name="statusLevelId"></param>
    ''' <param name="includeProbation"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSysStatuses(ByVal statusLevelId As String, ByVal includeProbation As Boolean) As DataSet

        '   Instantiate DAL component
        Dim db As New StatusCodeDB
        Dim ds As DataSet

        '   get the dataset with all degrees          
        ds = db.GetSysStatusesByStatusLevel(statusLevelId, includeProbation)

        Return ds

    End Function
    Public Function GetStatusCodeDetails(statusCodeId As String) As StatusCodeInfo
        With New StatusCodeDB
            Dim result = .GetStatusCodeDetails(statusCodeId)
            ' If was used the status Warning or Academic probation as status change it to Currently Attendance
            If (result.SystemStatus = StatusCodeDB.eStatusCodes.WarningProbation Or result.SystemStatus = StatusCodeDB.eStatusCodes.DisciplinaryProbation) Then
                'Convert this to Current attendance
                result.SystemStatus = StatusCodeDB.eStatusCodes.CurrentlyAttending
                .UpdateStatusCode(result, "SUPPORT")
            End If
            Return result
        End With
    End Function


    Public Function InsertStatusCode(ByVal StatusCode As StatusCodeInfo, ByVal user As String) As String
        Dim db As New StatusCodeDB

        Return db.InsertStatusCode(StatusCode, user)
    End Function
    Public Function DeleteStatusCode(ByVal StatusCodeId As String) As String
        Dim DB As New StatusCodeDB

        Return DB.DeleteStatusCode(StatusCodeId)
    End Function

    Public Function UpdateStatusCode(ByVal StatusCode As StatusCodeInfo, ByVal user As String) As String
        Dim DB As New StatusCodeDB

        Return DB.UpdateStatusCode(StatusCode, user)
    End Function
    Public Function GetAllStatusCodes() As DataSet
        '   get All Status Codes
        Return (New StatusCodeDB).GetAllStatusCodes()

    End Function
    Public Function GetAllStatusCodesForLeads() As DataSet
        '   get All Status Codes
        Return (New StatusCodeDB).GetAllStatusCodesForLeads()

    End Function
    Public Function GetAllStatusCodesForStudents() As DataSet
        '   get All Status Codes
        Return (New StatusCodeDB).GetAllStatusCodesForStudents()

    End Function
    Public Function GetAllStatusCodesForStudents(ByVal campusId As String) As DataSet
        '   get All Status Codes
        Return (New StatusCodeDB).GetAllStatusCodesForStudents(campusId)

    End Function
    Public Function GetEnrollmentStatusCodes(ByVal campusId As String) As DataSet
        '   get All Status Codes
        Return (New StatusCodeDB).GetEnrollmentStatusCodes(campusId)

    End Function
    Public Function DoesDefaultLeadExistForAllCampusGroup(ByVal StatusLevel As Integer) As Boolean

        'return boolean
        Return (New StatusCodeDB).DoesDefaultLeadExistForAllCampusGroup(StatusLevel)

    End Function
    Public Function DoesDefaultLeadStatusExistForCampusGroup(ByVal StatusLevel As Integer, ByVal CampusGroup As String) As Boolean

        'return boolean
        Return (New StatusCodeDB).DoesDefaultLeadStatusExistForCampusGroup(StatusLevel, CampusGroup)

    End Function

    Public Function DoesDefaultStatusExistForCampusGroup(ByVal StatusLevel As Integer, ByVal SysStatus As Integer,ByVal CampusGroup As Guid) As Boolean
        
        Return (New StatusCodeDB).CheckIfDefaulExist(StatusLevel, SysStatus, CampusGroup)

    End Function
    Public Function GetDefaultLeadStatusCode() As String
        Return (New StatusCodeDB).GetDefaultLeadStatusCode()
    End Function
    Public Function GetStatusCodesByStatusAndDefault(ByVal StatusLevelId As String, ByVal StatusId As String, ByVal CampusGroupId As String) As DataSet
        Return (New StatusCodeDB).GetStatusCodesByStatusAndDefault(StatusLevelId, StatusId, CampusGroupId)
    End Function
    Public Function CheckIfDefaultStatusCodeExistForCampusGroup(ByVal CampGrpId As String) As String
        Return (New StatusCodeDB).CheckIfDefaultStatusCodeExistForCampusGroup(CampGrpId)
    End Function
    ''Added by saraswathi lakshmanan on June 10 2009
    ''To show the status of the students who are inschool Enrollment Status
    ''to fix iisue 14829
    Public Function GetAllStatusCodesForStudentsWithInSchoolEnrollment(ByVal campusId As String) As DataSet
        '   get All Status Codes
        Return (New StatusCodeDB).GetAllStatusCodesForStudentswithInSchoolEnrollment(campusId)

    End Function

    Public Function GetSystemStatusByStatusCode(ByVal statusCode As Guid) As DataSet

        '   Instantiate DAL component
        Dim db As New StatusCodeDB
        Dim ds As New DataSet

        ds = db.GetSystemStatusByStatusCode(statusCode)

        Return ds

    End Function
End Class
