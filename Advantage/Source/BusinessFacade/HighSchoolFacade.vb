Public Class HighSchoolFacade
    Public Function UpdateHighSchool(ByVal HsInfo As HighSchool, ByVal user As String) As String

        '   Instantiate DAL component
        With New HighSchoolDB

            '   If it is a new account do an insert. If not, do an update
            If Not (HsInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddHighSchoolInfo(HsInfo, user)
            Else
                '   return integer with update results
                Return .UpdateHighSchoolInfo(HsInfo, user)
            End If
        End With
    End Function
    Public Function GetHighSchoolInfo(ByVal HSId As String) As HighSchool
        '   Instantiate DAL component
        With New HighSchoolDB
            Return .GetHighSchoolInfo(HSId)
        End With
    End Function
    Public Function DeleteHighSchoolInfo(ByVal HSId As String, ByVal modDate As Date) As String
        '   Instantiate DAL component
        With New HighSchoolDB
            Return .DeleteHighSchoolInfo(HSId, modDate)
        End With
    End Function
    Public Function GetHighSchoolDataList(ByVal strSelectedIndex As Integer, Optional ByVal HsName As String="", Optional ByVal InstitutionType As Integer=0) As DataTable
        With New HighSchoolDB
            Return .getSchoolsList(strSelectedIndex,HsName,InstitutionType)
        End With
    End Function
    Public Function VerifyIfHighSchoolCanBeDeleted(ByVal HSId As String) As String
        Return (New HighSchoolDB).VerifyIfHighSchoolCanBeDeleted(HSId)
    End Function
End Class
