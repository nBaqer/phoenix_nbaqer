
Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.AdvantageV1.BusinessFacade.SGR_CommonObject

Public Class SGR_2YR3SummaryObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SGR_2YR3Summary"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet
	Private RptParamInfo As ReportParamInfoIPEDS

	Private DateFilter As String
	Private dvEnrollments As DataView

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SGR_2YR3SummaryObjectDB.GetReportDatasetRaw(RptParamInfo)

		' save passed-in report parameter info 
		Me.RptParamInfo = RptParamInfo

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

        Dim dtGenderInfo As DataTable = IPEDSFacade.GetIPEDSFldValues("Gender")         '"GenderDescrip"
        Dim dtEthnicInfo As DataTable = IPEDSFacade.GetIPEDSFldValues("Ethnic Code")    '"EthCodeDescrip"

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add("GenderDescrip", GetType(String))
			.Add("EthCodeDescrip", GetType(String))
			.Add("Sum_RevCohort", GetType(System.Int32))
			.Add("Sum_Comp", GetType(System.Int32))
			.Add("Sum_TransferOut", GetType(System.Int32))
			.Add("Sum_Excluded", GetType(System.Int32))
		End With

		' create student Enrollments DataView to be used when processing records
		dvEnrollments = New DataView(dsRaw.Tables(TblNameEnrollments))

		' set up date filter to be used when processing records
		DateFilter = EnrollDateFilter(RptParamInfo)

		' loop thru genders, and ethnic groups for each gender, and process
		'	matching records in raw dataset
		For Each drGenderInfo As DataRow In dtGenderInfo.Rows
			For Each drEthnicInfo As DataRow In dtEthnicInfo.Rows
				ProcessRows(drGenderInfo("AgencyDescrip"), drEthnicInfo("AgencyDescrip"))
			Next
		Next

		' return the Report DataSet
		Return dsRpt

	End Function

	Private Sub ProcessRows(ByVal GenderDescrip As String, ByVal EthCodeDescrip As String)
		Dim i As Integer, j As Integer
		Dim drRpt As DataRow
		Dim dvStudents As DataView
		Dim DateEnroll As Date, DateGrad As Date, Date150Pct As Date
		Dim Sum_RevCohort As Integer = 0
		Dim RevCohort As Boolean
		Dim Sum_Comp As Integer = 0
		Dim Sum_TransferOut As Integer = 0
		Dim Sum_Excluded As Integer = 0
		Dim StudentId As String
		Dim IsTransferOut As Boolean, IsExcluded As Boolean

		' create dataview from students in raw dataset, filtered according to the 
		'	passed in gender and ethnic descriptions
		dvStudents = New DataView(dsRaw.Tables(TblNameStudents), _
								  "GenderDescrip LIKE '" & GenderDescrip & "' AND EthCodeDescrip LIKE '" & EthCodeDescrip & "'", _
								  "", DataViewRowState.CurrentRows)

		' if we have matching students, loop thru students and sum relevant info
		If dvStudents.Count > 0 Then
			For i = 0 To dvStudents.Count - 1
				StudentId = dvStudents(i)("StudentId").ToString

				' assume student won't be counted in revised cohort
				RevCohort = False

				' determine if student is a "transfer out", add to sum and count in revised cohort if so
				IsTransferOut = TransferOut(StudentId, dvEnrollments)
				If IsTransferOut Then
					Sum_TransferOut += 1
					RevCohort = True
				End If

				' determine if student is an exclusion due to drop reason, add to sum 
				'	and count in revised cohort if so
				IsExcluded = ExcludedFromCohort(StudentId, dvEnrollments)
				If IsExcluded Then
					Sum_Excluded += 1
					RevCohort = True
				End If

				' if student is not a transfer out and is not excluded, determine if student completed 
				'	program within 150% of normal time to completion, add to sum and count in revised 
				'	cohort if so
				If (Not IsTransferOut And Not IsExcluded) AndAlso _
				   Completed150Pct(StudentId, dvEnrollments, "Weeks <= " & NumWeeks_2Yrs, DateFilter) Then
					Sum_Comp += 1
					RevCohort = True
				End If

				' count student in revised cohort if indicated
				If RevCohort Then
					Sum_RevCohort += 1
				End If
			Next
		End If

		' add data to report
		drRpt = dsRpt.Tables(MainTableName).NewRow

		' set report row Gender and Ethnic descriptions
		drRpt("GenderDescrip") = GenderDescrip
		drRpt("EthCodeDescrip") = EthCodeDescrip

		' enter sums computed above
		drRpt("Sum_RevCohort") = Sum_RevCohort
		drRpt("Sum_Comp") = Sum_Comp
		drRpt("Sum_TransferOut") = Sum_TransferOut
		drRpt("Sum_Excluded") = Sum_Excluded

		' add datarow to main report
		dsRpt.Tables(MainTableName).Rows.Add(drRpt)

	End Sub

End Class
