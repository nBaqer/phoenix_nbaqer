' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' EmployerFeeFacade.vb
'
' EmployerFeeFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class EmployerFeeFacade
    Public Function GetAllEmployerFee() As DataSet

        '   Instantiate DAL component
        With New EmployerFeeDB

            '   get the dataset with all States
            Return .GetAllEmployerFee()

        End With

    End Function
End Class
