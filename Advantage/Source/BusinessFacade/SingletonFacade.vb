Public Class SingletonFacade
    Public Function GetStudentSequence() As String
        Dim x As Integer
        Dim z As Singleton
        z = Singleton.GetSingletonObject()
        x = z.GetStudentSequence()
        Return x
    End Function
    Public Function GetStudentFormatSequence() As String
        Dim x As Integer
        Dim z As Singleton
        z = Singleton.GetSingletonObject()
        x = z.GetStudentFormatSequence()
        Return x
    End Function
    Public Function GetStudentSequenceNumber(ByVal intStartNumber As Integer) As String
        Dim x As Integer
        Dim z As Singleton
        z = Singleton.GetSingletonObject()
        x = z.GetStudentSequenceNumber(intStartNumber)
        Return x
    End Function
End Class
