﻿Imports FAME.Advantage.Common

Public Class ExpectedDisbursementObject
    Inherits BaseReportFacade
#Region "Private Data Members"
    Private MyAdvAppSettings As FAME.Advantage.Common.AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
#End Region
#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim expDisp As New ExpectedDisbursementDB
        Dim ds As DataSet
        ds = expDisp.GetExpectedDisbursement(rptParamInfo)

        Return BuildReportSource(ds)
    End Function
#End Region

#Region "Private Methods"

    Private Function GetAdvAppSettings() As FAME.Advantage.Common.AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        Dim facInputMasks As New InputMasksFacade
        'Get the mask for SSN
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        For Each dr As DataRow In ds.Tables(0).Rows
            ''Code Changed By Vijay Ramteke On October 01, 2010 Form Mantis Id 19800
            ''If dr("Voided").ToString = "False" Then
            If StudentIdentifier = "SSN" Then
                If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                    If dr("StudentIdentifier") <> "" Then
                        Dim temp As String = dr("StudentIdentifier")
                        dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                    End If
                End If
            End If
            'Else
            'dr.Delete()
            'End If
            ''Code Changed By Vijay Ramteke On October 01, 2010 Form Mantis Id 19800
        Next
        ds.AcceptChanges()

        Return ds
    End Function

#End Region
#Region "Public Properties"
    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region
End Class
