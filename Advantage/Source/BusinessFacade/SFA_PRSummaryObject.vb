Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.AdvantageV1.BusinessFacade.SFA_PRCommonObject

Public Class SFA_PRSummaryObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SFA_PRSummary"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet
	Private dvCustomPrefInfo As DataView
	Private dtStuInfoRaw As DataTable
	Private dvAwards As DataView

	Private Count_FedGrants As Integer = 0
	Private Sum_FedGrants As Single = 0
	Private Count_StateLocalGrants As Integer = 0
	Private Sum_StateLocalGrants As Single = 0
	Private Count_InstGrants As Integer = 0
	Private Sum_InstGrants As Single = 0
	Private Count_FedLoans As Integer = 0
	Private Sum_FedLoans As Single = 0

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' save the custom pref info from the report parameters into DataView, then set custom pref info to
		'	Nothing to avoid overhead of sending unnecessary data to DataAccess object
		dvCustomPrefInfo = New DataView(RptParamInfo.CustomPrefInfo.Copy)
		RptParamInfo.CustomPrefInfo = Nothing

		' get raw DataSet using passed-in parameters
		dsRaw = SFA_PRSummaryObjectDB.GetReportDatasetRaw(RptParamInfo)

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		dtStuInfoRaw = PreProcessRawData(dsRaw)

		Dim drRpt As DataRow
		Dim drStuInfoRaw As DataRow

		' create table to hold final report data 
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add("Count_FirstTime", GetType(Integer))
			.Add("Count_Enrolled", GetType(Integer))
			.Add("Count_FinAid", GetType(Integer))
			.Add("Count_FedGrants", GetType(Integer))
			.Add("Avg_FedGrants", GetType(Single))
			.Add("Count_StateLocalGrants", GetType(Integer))
			.Add("Avg_StateLocalGrants", GetType(Single))
			.Add("Count_InstGrants", GetType(Integer))
			.Add("Avg_InstGrants", GetType(Single))
			.Add("Count_FedLoans", GetType(Integer))
			.Add("Avg_FedLoans", GetType(Single))
		End With

		' create DataRow to hold report data
		drRpt = dsRpt.Tables(MainTableName).NewRow

		' process data for report Part 1
		With dtStuInfoRaw
			drRpt("Count_FirstTime") = .Compute("Count(DegCertSeekingDescrip)", _
												"DegCertSeekingDescrip LIKE '" & DegCertSeekingFirstTime & "%'")
			drRpt("Count_Enrolled") = .Rows.Count
			drRpt("Count_FinAid") = .Compute("Count(DegCertSeekingDescrip)", _
											 "DegCertSeekingDescrip LIKE '" & DegCertSeekingFirstTime & "%' AND FinAidCount > 0")
		End With


		' process data for report Part 2
		dvAwards = New DataView(dsRaw.Tables("Awards"))

		For Each drStuInfoRaw In dtStuInfoRaw.Rows
			ProcessRowsPart2(drStuInfoRaw)
		Next


		' record computed totals in report DataSet
		drRpt("Count_FedGrants") = Count_FedGrants
		If Count_FedGrants > 0 Then
			drRpt("Avg_FedGrants") = Sum_FedGrants / Count_FedGrants
		Else
			drRpt("Avg_FedGrants") = 0
		End If
		drRpt("Count_StateLocalGrants") = Count_StateLocalGrants
		If Count_StateLocalGrants > 0 Then
			drRpt("Avg_StateLocalGrants") = Sum_StateLocalGrants / Count_StateLocalGrants
		Else
			drRpt("Avg_StateLocalGrants") = 0
		End If
		drRpt("Count_InstGrants") = Count_InstGrants
		If Count_InstGrants > 0 Then
			drRpt("Avg_InstGrants") = Sum_InstGrants / Count_InstGrants
		Else
			drRpt("Avg_InstGrants") = 0
		End If
		drRpt("Count_FedLoans") = Count_FedLoans
		If Count_FedLoans > 0 Then
			drRpt("Avg_FedLoans") = Sum_FedLoans / Count_FedLoans
		Else
			drRpt("Avg_FedLoans") = 0
		End If

		' add data to report DataSet
		dsRpt.Tables(MainTableName).Rows.Add(drRpt)

		' return report DataSet
		Return dsRpt

	End Function

	Private Sub ProcessRowsPart2(ByVal drStuInfoRaw As DataRow)
		Dim StudentIdFilter As String = "StudentId = '" & drStuInfoRaw("StudentId").ToString & "'"
		Dim i As Integer
		Dim sb As New System.Text.StringBuilder


		' process Student's Federal Grants, if any
		dvAwards.RowFilter = StudentIdFilter & " AND TitleIV = 1 AND AwardTypeDescrip LIKE 'Grant'"
		If dvAwards.Count > 0 Then
			' if Student has Federal Grants, add to count and sum
			Count_FedGrants += 1
			Sum_FedGrants += CompAwardSum(dvAwards, "Amount")
		End If

		' process Student's Federal Loans, if any
		dvAwards.RowFilter = StudentIdFilter & " AND TitleIV = 1 AND AwardTypeDescrip LIKE 'Loan'"
		If dvAwards.Count > 0 Then
			' if Student has Federal Loans, add to count and sum
			Count_FedLoans += 1
			Sum_FedLoans += CompAwardSum(dvAwards, "Amount")
		End If

		' process Student's Fund Sources designated by user as "State/local government grants", if any
		dvCustomPrefInfo.RowFilter = "RptCatId = " & SFA_PRCommonObject.RptCat_StateLocalGrants
		If dvCustomPrefInfo.Count > 0 Then
			' if any Fund Sources are designated as "State/local government grants",
			'	build a list of those Fund Source ids
			With sb
				For i = 0 To dvCustomPrefInfo.Count - 1
					.Append("'" & LCase(dvCustomPrefInfo(i)("FundSourceId")) & "',")
				Next
				.Remove(.Length - 1, 1)	' get rid of extra "," at end
			End With
			' if Student has records from the raw data, which contain data for any of 
			'	the Fund Sources in the list just built, add to count and sum
			dvAwards.RowFilter = StudentIdFilter & " AND FundSourceId IN (" & sb.ToString & ")"
			If dvAwards.Count > 0 Then
				Count_StateLocalGrants += 1
				Sum_StateLocalGrants += CompAwardSum(dvAwards, "Amount")
			End If
		End If


		' process Student's Fund Sources designated by user as "Institutional grants", if any
		dvCustomPrefInfo.RowFilter = "RptCatId = " & SFA_PRCommonObject.RptCat_InstGrants
		If dvCustomPrefInfo.Count > 0 Then
			' if any Fund Sources are designated as "Institutional grants",
			'	build a list of those Fund Source ids
			With sb
				.Remove(0, .Length)
				For i = 0 To dvCustomPrefInfo.Count - 1
					.Append("'" & LCase(dvCustomPrefInfo(i)("FundSourceId")) & "',")
				Next
				.Remove(.Length - 1, 1)	' get rid of extra "," at end
			End With
			' if Student has records from the raw data, which contain data for any of 
			'	the Fund Sources in the list just built, add to count and sum
			dvAwards.RowFilter = StudentIdFilter & " AND FundSourceId IN (" & sb.ToString & ")"
			If dvAwards.Count > 0 Then
				Count_StateLocalGrants += 1
				Sum_StateLocalGrants += CompAwardSum(dvAwards, "Amount")
			End If
		End If

	End Sub

End Class
