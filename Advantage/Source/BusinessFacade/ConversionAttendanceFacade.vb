Public Class ConversionAttendanceFacade
    Public Function GetEnrollmentAttendance(ByVal stuEnrollId As String, ByVal startDate As String, ByVal endDate As String) As DataTable
        Dim db As New ConversionAttendanceDB
        Return db.GetEnrollmentAttendance(stuEnrollId, startDate, endDate)

    End Function

    Public Function GetEnrollmentTotalAttendanceHours(ByVal stuEnrollId As String, ByVal startDate As String, ByVal endDate As String) As Decimal
        Dim db As New ConversionAttendanceDB
        Return db.GetEnrollmentTotalAttendanceHours(stuEnrollId, startDate, endDate)
    End Function
End Class
