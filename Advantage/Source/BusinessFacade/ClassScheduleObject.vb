Imports FAME.Advantage.Common

Public Class ClassScheduleObject
    Inherits BaseReportFacade

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim clsSched As New ClassScheduleDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(clsSched.GetClassSchedule(rptParamInfo))
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region

#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            If ds.Tables.Count = 2 Then
                'Update link fields for CR.
                For Each dr As DataRow In ds.Tables(0).Rows
                    dr("ClsSectionIdStr") = dr("ClsSectionId").ToString
                Next
                For Each dr As DataRow In ds.Tables(1).Rows
                    dr("ClsSectionIdStr") = dr("ClsSectionId").ToString
                    Try
                        dr("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                    Catch ex As System.Exception
                        dr("SuppressDate") = "no"
                    End Try
                Next
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
