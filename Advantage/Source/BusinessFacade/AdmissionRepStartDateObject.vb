Public Class AdmissionRepStartDateObject
    Inherits BaseReportFacade

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim objDB As New AdmissionRepPerformanceDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(objDB.GetStartDateRepPerformance(rptParamInfo), rptParamInfo.FilterList)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal Filterlist As String) As DataSet
        Dim oldCmpGrp As String = ""
        Dim oldCampus As String = ""
        'Dim dt2 As DataTable
        Dim rowC As DataRow
        Dim rowCG As DataRow

        Try
            If ds.Tables.Count = 7 Then
                Dim dt As DataTable = ds.Tables("StartDateRepPerformance")
                Dim dtCampusTtl As DataTable = ds.Tables("CampusTotals")
                Dim dtCampGrpTtl As DataTable = ds.Tables("CampusGroupTotals")
                Dim dtPrgProjAmt As DataTable = ds.Tables("ProgProjectedAmount")
                Dim dtTermProjAmt As DataTable = ds.Tables("TermProjectedAmount")
                Dim dtCourseProjAmt As DataTable = ds.Tables("CourseProjectedAmount")
                Dim PrgProjAmount As Double = 0
                Dim TermProjAmount As Double = 0
                Dim CourseProjAmount As Double = 0

                Dim projAmount As Double = 0

                ''CAlculating the Projected AMount for each Term and ADmission REp
                For Each dr As DataRow In dt.Rows
                    Dim drPrgproj() As DataRow
                    Dim drTermproj() As DataRow
                    Dim drCourseproj() As DataRow
                    Dim prgprojamt As Double = 0
                    Dim Termprojamt As Double = 0
                    Dim Courseprojamt As Double = 0
                    If Filterlist.Contains("arTerm.TermId") Then

                        drPrgproj = dtPrgProjAmt.Select("TermDescrip='" & dr("TermDescrip").ToString & "' AND AdmissionsRep='" & dr("AdmissionsRep").ToString & "' AND CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                    "CampusId='" & dr("CampusId").ToString & "'")
                        drTermproj = dtTermProjAmt.Select("TermDescrip='" & dr("TermDescrip").ToString & "' AND AdmissionsRep='" & dr("AdmissionsRep").ToString & "' AND CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                "CampusId='" & dr("CampusId").ToString & "'")
                        drCourseproj = dtCourseProjAmt.Select("TermDescrip='" & dr("TermDescrip").ToString & "' AND AdmissionsRep='" & dr("AdmissionsRep").ToString & "' AND CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                "CampusId='" & dr("CampusId").ToString & "'")
                    Else

                        drPrgproj = dtPrgProjAmt.Select("AdmissionsRep='" & dr("AdmissionsRep").ToString & "' AND CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                    "CampusId='" & dr("CampusId").ToString & "'")
                        drTermproj = dtTermProjAmt.Select("AdmissionsRep='" & dr("AdmissionsRep").ToString & "' AND CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                "CampusId='" & dr("CampusId").ToString & "'")
                        drCourseproj = dtCourseProjAmt.Select("AdmissionsRep='" & dr("AdmissionsRep").ToString & "' AND CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                "CampusId='" & dr("CampusId").ToString & "'")
                    End If

                    If drPrgproj.Length = 1 Then
                        If Not drPrgproj(0)("TotTransAmount") Is DBNull.Value Then
                            prgprojamt = drPrgproj(0)("TotTransAmount")
                        End If

                    End If
                    If drTermproj.Length = 1 Then
                        If Not drTermproj(0)("TotTransAmount") Is DBNull.Value Then
                            Termprojamt = drTermproj(0)("TotTransAmount")
                        End If

                    End If
                    If drCourseproj.Length = 1 Then
                        If Not drCourseproj(0)("TotTransAmount") Is DBNull.Value Then
                            Courseprojamt = drCourseproj(0)("TotTransAmount")
                        End If

                    End If
                    dr("ProjAmount") = prgprojamt + Termprojamt + Courseprojamt
                Next

                dt.AcceptChanges()


                'Start Date Rep Performance
                For Each dr As DataRow In dt.Rows
                    PrgProjAmount = 0
                    TermProjAmount = 0
                    CourseProjAmount = 0

                   



                    'Compute Total Amounts per Campus Groups and Campuses.
                    If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpId").ToString Then
                        'Get a row everytime there is a Campus Group.
                        rowCG = dtCampGrpTtl.NewRow
                        rowCG("CampGrpIdStr") = dr("CampGrpId").ToString
                        rowCG("Enrolled") = 0
                        rowCG("Cancelled") = 0
                        rowCG("Starts") = 0
                        rowCG("Enrolled") = dt.Compute("SUM(Enrolled)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                        rowCG("Cancelled") = dt.Compute("SUM(Cancelled)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                        If rowCG("Enrolled") > rowCG("Cancelled") Then
                            rowCG("Starts") = rowCG("Enrolled") - rowCG("Cancelled")
                        End If
                        If Not rowCG.IsNull("Enrolled") Then
                            If rowCG("Enrolled") <> 0 And rowCG("Starts") <> 0 Then
                                rowCG("StartRate") = Convert.ToInt32(rowCG("Starts") * 100 / rowCG("Enrolled")) & "%"
                            End If
                        End If

                        rowCG("ProjAmount") = dt.Compute("SUM(ProjAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                        'PrgProjAmount = 0
                        'TermProjAmount = 0
                        'CourseProjAmount = 0
                        'If Not dtPrgProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "'") Is DBNull.Value Then
                        '    PrgProjAmount = dtPrgProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                        'End If
                        'If Not dtTermProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "'") Is DBNull.Value Then
                        '    TermProjAmount = dtTermProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                        'End If
                        'If Not dtCourseProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "'") Is DBNull.Value Then
                        '    CourseProjAmount = dtCourseProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                        'End If

                        'rowCG("ProjAmount") = PrgProjAmount + TermProjAmount + CourseProjAmount

                        dtCampGrpTtl.Rows.Add(rowCG)
                        oldCmpGrp = dr("CampGrpId").ToString
                        oldCampus = ""

                    End If

                    If oldCmpGrp = dr("CampGrpId").ToString And oldCampus = "" Then
                        'Compute totals for campus
                        rowC = dtCampusTtl.NewRow
                        rowC("CampGrpIdStr") = oldCmpGrp
                        rowC("CampusIdStr") = dr("CampusId").ToString
                        rowC("Enrolled") = 0
                        rowC("Cancelled") = 0
                        rowC("Starts") = 0
                        rowC("Enrolled") = dt.Compute("SUM(Enrolled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                        "CampusId='" & dr("CampusId").ToString & "'")
                        rowC("Cancelled") = dt.Compute("SUM(Cancelled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                        "CampusId='" & dr("CampusId").ToString & "'")
                        If rowC("Enrolled") > rowC("Cancelled") Then
                            rowC("Starts") = rowC("Enrolled") - rowC("Cancelled")
                        End If
                        If Not rowC.IsNull("Enrolled") Then
                            If rowC("Enrolled") <> 0 And rowC("Starts") <> 0 Then
                                rowC("StartRate") = Convert.ToInt32(rowC("Starts") * 100 / rowC("Enrolled")) & "%"
                            End If
                        End If
                        'PrgProjAmount = 0
                        'TermProjAmount = 0
                        'CourseProjAmount = 0
                        'If Not dtPrgProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                        '                     "CampusId='" & dr("CampusId").ToString & "'") Is DBNull.Value Then

                        '    PrgProjAmount = dtPrgProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                        '                                  "CampusId='" & dr("CampusId").ToString & "'")
                        'End If
                        'If Not dtTermProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                        '                              "CampusId='" & dr("CampusId").ToString & "'") Is DBNull.Value Then
                        '    TermProjAmount = dtTermProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                        '                                  "CampusId='" & dr("CampusId").ToString & "'")
                        'End If
                        'If Not dtCourseProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                        '                              "CampusId='" & dr("CampusId").ToString & "'") Is DBNull.Value Then
                        '    CourseProjAmount = dtCourseProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                        '                                  "CampusId='" & dr("CampusId").ToString & "'")
                        'End If

                        'rowC("ProjAmount") = PrgProjAmount + TermProjAmount + CourseProjAmount

                        'rowC("ProjAmount") = dtPrgProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                        '                               "CampusId='" & dr("CampusId").ToString & "'")

                        rowC("ProjAmount") = dt.Compute("SUM(ProjAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                        "CampusId='" & dr("CampusId").ToString & "'")

                        dtCampusTtl.Rows.Add(rowC)
                        oldCampus = dr("CampusId").ToString
                        '
                    ElseIf oldCmpGrp = dr("CampGrpId").ToString And oldCampus <> dr("CampusId").ToString Then
                        'Compute totals for campus
                        rowC = dtCampusTtl.NewRow
                        rowC("CampGrpIdStr") = oldCmpGrp
                        rowC("Enrolled") = 0
                        rowC("Cancelled") = 0
                        rowC("Starts") = 0
                        rowC("CampusIdStr") = dr("CampusId").ToString
                        rowC("Enrolled") = dt.Compute("SUM(Enrolled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                        "CampusId='" & dr("CampusId").ToString & "'")
                        rowC("Cancelled") = dt.Compute("SUM(Cancelled)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                        "CampusId='" & dr("CampusId").ToString & "'")
                        If rowC("Enrolled") > rowC("Cancelled") Then
                            rowC("Starts") = rowC("Enrolled") - rowC("Cancelled")
                        End If
                        If Not rowC.IsNull("Enrolled") Then
                            If rowC("Enrolled") <> 0 And rowC("Starts") <> 0 Then
                                rowC("StartRate") = Convert.ToInt32(rowC("Starts") * 100 / rowC("Enrolled")) & "%"
                            End If
                        End If
                        'PrgProjAmount = 0
                        'TermProjAmount = 0
                        'CourseProjAmount = 0
                        'If Not dtPrgProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                        '                       "CampusId='" & dr("CampusId").ToString & "'") Is DBNull.Value Then

                        '    PrgProjAmount = dtPrgProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                        '                                  "CampusId='" & dr("CampusId").ToString & "'")
                        'End If
                        'If Not dtTermProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                        '                              "CampusId='" & dr("CampusId").ToString & "'") Is DBNull.Value Then
                        '    TermProjAmount = dtTermProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                        '                                  "CampusId='" & dr("CampusId").ToString & "'")
                        'End If
                        'If Not dtCourseProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                        '                              "CampusId='" & dr("CampusId").ToString & "'") Is DBNull.Value Then
                        '    CourseProjAmount = dtCourseProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                        '                                  "CampusId='" & dr("CampusId").ToString & "'")
                        'End If

                        'rowC("ProjAmount") = PrgProjAmount + TermProjAmount + CourseProjAmount

                        'rowC("ProjAmount") = dtPrgProjAmt.Compute("SUM(TotTransAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                        '                        "CampusId='" & dr("CampusId").ToString & "'")
                        rowC("ProjAmount") = dt.Compute("SUM(ProjAmount)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                       "CampusId='" & dr("CampusId").ToString & "'")


                        dtCampusTtl.Rows.Add(rowC)
                        oldCampus = dr("CampusId").ToString
                    End If

                    'fields used to link tables on report
                    dr("CampGrpIdStr") = dr("CampGrpId").ToString
                    dr("CampusIdStr") = dr("CampusId").ToString

                    'Admission Rep counter
                    dr("AdRepCount") = dt.Rows.Count
                    'Dim drPrgproj() As DataRow
                    'Dim drTermproj() As DataRow
                    'Dim drCourseproj() As DataRow
                    'Dim prgprojamt As Double = 0
                    'Dim Termprojamt As Double = 0
                    'Dim Courseprojamt As Double = 0
                    'If Filterlist.Contains("arTerm.TermId") Then

                    '    drPrgproj = dtPrgProjAmt.Select("TermDescrip='" & dr("TermDescrip").ToString & "' AND AdmissionsRep='" & dr("AdmissionsRep").ToString & "' AND CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                    '                                "CampusId='" & dr("CampusId").ToString & "'")
                    '    drTermproj = dtTermProjAmt.Select("TermDescrip='" & dr("TermDescrip").ToString & "' AND AdmissionsRep='" & dr("AdmissionsRep").ToString & "' AND CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                    '                            "CampusId='" & dr("CampusId").ToString & "'")
                    '    drCourseproj = dtCourseProjAmt.Select("TermDescrip='" & dr("TermDescrip").ToString & "' AND AdmissionsRep='" & dr("AdmissionsRep").ToString & "' AND CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                    '                            "CampusId='" & dr("CampusId").ToString & "'")
                    'Else

                    '    drPrgproj = dtPrgProjAmt.Select("AdmissionsRep='" & dr("AdmissionsRep").ToString & "' AND CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                    '                                "CampusId='" & dr("CampusId").ToString & "'")
                    '    drTermproj = dtTermProjAmt.Select("AdmissionsRep='" & dr("AdmissionsRep").ToString & "' AND CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                    '                            "CampusId='" & dr("CampusId").ToString & "'")
                    '    drCourseproj = dtCourseProjAmt.Select("AdmissionsRep='" & dr("AdmissionsRep").ToString & "' AND CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                    '                            "CampusId='" & dr("CampusId").ToString & "'")
                    'End If

                    'If drPrgproj.Length = 1 Then
                    '    If Not drPrgproj(0)("TotTransAmount") Is DBNull.Value Then
                    '        prgprojamt = drPrgproj(0)("TotTransAmount")
                    '    End If

                    'End If
                    'If drTermproj.Length = 1 Then
                    '    If Not drTermproj(0)("TotTransAmount") Is DBNull.Value Then
                    '        Termprojamt = drTermproj(0)("TotTransAmount")
                    '    End If

                    'End If
                    'If drCourseproj.Length = 1 Then
                    '    If Not drCourseproj(0)("TotTransAmount") Is DBNull.Value Then
                    '        Courseprojamt = drCourseproj(0)("TotTransAmount")
                    '    End If

                    'End If
                    'dr("ProjAmount") = prgprojamt + Termprojamt + Courseprojamt

                    ' ''Compute StartRate for Admission Rep
                    ''If Not dr.IsNull("Enrolled") Then
                    ''    If dr("Enrolled") <> 0 And dr("Starts") <> 0 Then
                    ''        dr("StartRate") = Convert.ToInt32(dr("Starts") * 100 / dr("Enrolled")) & "%"
                    ''    End If
                    ''End If

                    If Not dr.IsNull("StartRate") Then
                        If dr("StartRate") = 0 Then
                            dr("StartRate") = System.DBNull.Value
                        End If
                    End If
                Next
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region


End Class
