Public Class OverridePreReqFacade

    Public Function GetAvailSelCoursesForEnrollment(ByVal StuEnrollId As String) As DataSet
        Dim ds As New DataSet

        ds = (New OverridePreReqDB).GetAvailSelCoursesForEnrollment(StuEnrollId)

        'We need to concatenate the course code and description in the Available Courses table
        ds.Tables("AvailCourses").Columns.Add("CodeDescrip", Type.GetType("System.String"))
        For Each dr As DataRow In ds.Tables("AvailCourses").Rows
            dr("CodeDescrip") = "(" & dr("Code") & ") " + dr("Descrip")
        Next

        'We need to concatenate the course code and description in the Selected Courses table
        ds.Tables("SelCourses").Columns.Add("CodeDescrip", Type.GetType("System.String"))
        For Each dr As DataRow In ds.Tables("SelCourses").Rows
            dr("CodeDescrip") = "(" & dr("Code") & ") " + dr("Descrip")
        Next

        Return ds
    End Function
    Public Function AddPreReqOverride(ByVal StuEnrollId As String, ByVal ReqId As String, ByVal user As String) As String
        Return (New OverridePreReqDB).AddPreReqOverride(StuEnrollId, ReqId, user)
    End Function
    Public Function DeletePreReqOverride(ByVal StuEnrollId As String, ByVal ReqId As String) As String
        Return (New OverridePreReqDB).DeletePreReqOverride(StuEnrollId, ReqId)
    End Function
End Class
