Public Class DocumentManagementFacade
    Public Function GetAllDocumentManagement() As DataSet
        '   Instantiate DAL component
        With New DocumentManagementDB
            '   get the dataset with all States
            Return .GetAllDocuments()
        End With
    End Function
    Public Function ConcatenateStudentLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("StudentId")}
        End With
        'Dim strFullName As String
        If ds.Tables("InstructorDT").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                ' DE7626 
                'row("FullName") = row("FirstName") & " " & row("LastName") & " " & row("MiddleName")
                row("FullName") = row("FirstName") & " " & row("MiddleName") & " " & row("LastName")
            Next
        End If
        Return ds
    End Function
    Public Function ConcatenateStudentLastFirstNameById(ByVal ds As DataSet) As String
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT1")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("StudentId")}
        End With
        Dim strFullName As String
        If ds.Tables("InstructorDT1").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                ' DE7626 
                'strFullName = row("FirstName") & " " & row("LastName") & " " & row("MiddleName")
                strFullName = row("FirstName") & " " & row("MiddleName") & " " & row("LastName")
            Next
        End If
        Return strFullName
    End Function
    Public Function GetStudentNames() As DataSet
        Dim ds As New DataSet
        With New DocumentManagementDB
            ds = .GetAllStudentsNames()
        End With
        ds = ConcatenateStudentLastFirstName(ds)
        Return ds
    End Function
    Public Function GetStudentNamesById(ByVal StudentId As String) As String
        Dim ds As New DataSet
        Dim strFullName As String
        With New DocumentManagementDB
            ds = .GetAllStudentsNamesById(StudentId)
        End With
        strFullName = ConcatenateStudentLastFirstNameById(ds)
        Return strFullName
    End Function
    Public Function InsertDocument(ByVal strFileName As String, ByVal strFileExtension As String, ByVal User As String, ByVal strDocumentType As String, ByVal strStudentId As String, ByVal strDocumentId As String, ByVal strModuleId As String, ByVal strDisplayName As String) As Integer
        With New DocumentManagementDB
            Return .InsertDocument(strFileName, strFileExtension, User, strDocumentType, strStudentId, strDocumentId, strModuleId, strDisplayName)
        End With
    End Function
    Public Function InsertDocumentbyID(ByVal strFileName As String, ByVal strFileExtension As String, ByVal User As String, ByVal strDocumentType As String, ByVal strStudentId As String, ByVal strDocumentId As String, ByVal strModuleId As String, ByVal strDisplayName As String) As String
        With New DocumentManagementDB
            Return .InsertDocumentbyID(strFileName, strFileExtension, User, strDocumentType, strStudentId, strDocumentId, strModuleId, strDisplayName)
        End With
    End Function
    Public Function UpdateDocument(ByVal FileID As String, ByVal strFileName As String, ByVal strFileExtension As String, ByVal User As String, ByVal strDocumentType As String, ByVal strStudentId As String, ByVal strDocumentId As String, ByVal strModuleId As String, ByVal strDisplayName As String) As Integer
        With New DocumentManagementDB
            Return .UpdateDocument(FileID, strFileName, strFileExtension, User, strDocumentType, strStudentId, strDocumentId, strModuleId, strDisplayName)
        End With
    End Function
    Public Function GetDocumentExtension(ByVal strFileName As String, ByVal strDocumentType As String) As String
        With New DocumentManagementDB
            Return .GetDocumentExtension(strFileName, strDocumentType)
        End With
    End Function
    Public Function GetStudentPhotoFileName(ByVal StudentId As String, ByVal strDocumentType As String) As String
        With New DocumentManagementDB
            Return .GetStudentPhotoFileName(StudentId, strDocumentType)
        End With
    End Function
    Public Function GetLeadPhotoFileName(ByVal StudentId As String, ByVal strDocumentType As String) As String
        With New DocumentManagementDB
            Return .GetLeadPhotoFileName(StudentId, strDocumentType)
        End With
    End Function
    Public Function DeleteDocument(ByVal strFileName As String, ByVal ModuleId As Integer, Optional ByVal DocumentId As String = "") As String
        With New DocumentManagementDB
            Return .DeleteDocument(strFileName, ModuleId, DocumentId)
        End With
    End Function

    Public Function DeleteDocumentbyID(ByVal FileID As String, Optional ByVal IsDelete As Boolean = False,Optional ByVal User As String=" ") As String
        With New DocumentManagementDB
            Return .DeleteDocumentbyID(FileId, IsDelete,User)
        End With
    End Function
    Public Function GetAllDocumentsByStudentDocType(ByVal FileName As String, ByVal DocumentType As String,Optional ByVal isSupportUser As Boolean = False) As DataSet
        With New DocumentManagementDB
            Return .GetAllDocumentsByStudentDocType(FileName, DocumentType,isSupportUser)
        End With
    End Function

    Public Function GetAllDocumentsByStudentIdandDocumentID(ByVal StudentID As String, ByVal DocumentID As String, Optional ByVal isSupportUser As Boolean=False) As DataSet
        With New DocumentManagementDB
            Return .GetAllDocumentsByStudentIdandDocumentID(StudentID, DocumentID,isSupportUser)
        End With
    End Function
    Public Function GetAllDocumentsByIdentifier(ByVal StudentDocId As String) As DataSet
        With New DocumentManagementDB
            Return .GetAllDocumentsByIdentifier(StudentDocId)
        End With
    End Function
    Public Function GetLeadPhotoDocumentType(ByVal LeadId As String, ByVal strDocumentType As String, ByVal Entity As String) As String
        With New DocumentManagementDB
            Return .GetLeadPhotoDocumentType(LeadId, strDocumentType, Entity)
        End With
    End Function

    Public Function IsStudentDocumentAlreadyCreated(ByVal StudentID As String, ByVal DocumentID As String) As Boolean
        With New DocumentManagementDB
            Return .IsStudentDocumentAlreadyCreated(StudentID, DocumentID)
        End With
    End Function
    Public Function IsLeadDocumentAlreadyCreated(ByVal LeadID As String, ByVal DocumentID As String) As Boolean
        With New DocumentManagementDB
            Return .IsLeadDocumentAlreadyCreated(LeadID, DocumentID)
        End With
    End Function
End Class
