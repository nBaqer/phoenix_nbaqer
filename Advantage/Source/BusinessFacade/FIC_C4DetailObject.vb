Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.AdvantageV1.BusinessFacade.FIC_C4CommonObject

Public Class FIC_C4DetailObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "FIC_C4Detail"

	' table names for different parts of report data
	Private TableNamePart1 As String = MainTableName & "Part1"
	Private TableNamePart2 As String = MainTableName & "Part2"
	Private TableNamePart3 As String = MainTableName & "Part3"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet
	Private dtScoresRawProc As DataTable

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = FIC_C4DetailObjectDB.GetReportDatasetRaw(RptParamInfo)

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		Dim drRpt As DataRow

		' process data for report part 1

		' create table to hold final report data for part 1
		With dsRpt.Tables.Add(TableNamePart1).Columns
			.Add(RptStuIdentifierColName, GetType(System.String))
			.Add("Name", GetType(System.String))
		End With

		' process raw student records for report part 1
		For Each drStudent As DataRow In dsRaw.Tables(TblNameStudents).Rows
			drRpt = dsRpt.Tables(TableNamePart1).NewRow
			' set student identifier and name
			IPEDSFacade.SetStudentInfo(drStudent, drRpt)
			' add row to report DataSet
			dsRpt.Tables(TableNamePart1).Rows.Add(drRpt)
		Next


		' process data for report part 2

		' create table to hold final report data for part 2
		With dsRpt.Tables.Add(TableNamePart2).Columns
			.Add("TestDescrip", GetType(String))
			.Add("Rank", GetType(System.Int32))
			.Add(RptStuIdentifierColName, GetType(String))
			.Add("Name", GetType(String))
			.Add("Score", GetType(Single))
			.Add("Percentile", GetType(String))
		End With

		' pre-process raw scores for processing, then loop thru IPEDS entrance tests, 
		'	and process scores for each
		dtScoresRawProc = PreProcessScores(dsRaw.Tables(TblNameStudents), dsRaw.Tables(TblNameScores))
		For Each drTest As DataRow In IPEDSFacade.GetIPEDSFldValues("TestDescrip").Rows
			ProcessRowsPart2(drTest("AgencyDescrip"))
		Next


		' process data for report part 3

		' create table to hold final report data for part 3
		With dsRpt.Tables.Add(TableNamePart3).Columns
			.Add("TotalEnrolled", GetType(System.Int32))
			.Add("TotalSAT", GetType(System.Int32))
			.Add("PctSAT", GetType(System.Int32))
			.Add("TotalACT", GetType(System.Int32))
			.Add("PctACT", GetType(System.Int32))
		End With

		' compute summary information based data in report DataSet
		drRpt = dsRpt.Tables(TableNamePart3).NewRow
		' total enrolled equals number of students in report part 1
		drRpt("TotalEnrolled") = dsRpt.Tables(TableNamePart1).Rows.Count
		' total with any SAT score equals number of students with SAT 1 Verbal - can do this since report SAT listing
		'	only inludes students with both SAT Verbal and Math scores
		drRpt("TotalSAT") = dsRpt.Tables(TableNamePart2).Compute("Count(Name)", "TestDescrip LIKE '" & EntrTestSAT1Verbal & "'")
		' compute percentage of students with any SAT score
		drRpt("PctSAT") = Math.Floor(((drRpt("TotalSAT") / drRpt("TotalEnrolled")) * 100) + 0.5)
		' total with any ACT score equals number of students with ACT Composite - can do this since report ACT listing 
		'	only inlcudes students with ACT Composite, English and Math scores
		drRpt("TotalACT") = dsRpt.Tables(TableNamePart2).Compute("Count(Name)", "TestDescrip LIKE '" & EntrTestACTComp & "'")
		' compute percentage of students with any ACT score
		drRpt("PctACT") = Math.Floor(((drRpt("TotalACT") / drRpt("TotalEnrolled")) * 100) + 0.5)
		' add single summary row to report DataSet
		dsRpt.Tables(TableNamePart3).Rows.Add(drRpt)

		' return the report DataSet
		Return dsRpt

	End Function

	Private Sub ProcessRowsPart2(ByVal TestDescrip As String)
	' Process Scores records for report part 2, for passed-in entrance test

		' create DataView of processed raw Scores records, filtered by passed-in entrance test
		Dim dvScores As New DataView(dtScoresRawProc, "TestDescrip LIKE '" & TestDescrip & "'", _
									 "", DataViewRowState.CurrentRows)
		Dim i As Integer
		Dim drRpt As DataRow

		Dim drStudent As DataRow
		Dim RankPct25 As Integer, RankPct75 As Integer


		' if there are any records matching passed-in entrance test, process records
		If dvScores.Count > 0 Then
			' find ranks for the 25th and 75th percentile scores - can do this with simple percentage
			'	multiplication since the Scores records are already sorted
			If dvScores.Count > 1 Then
				RankPct25 = Math.Floor((dvScores.Count * 0.25) + 0.5) + 1
				RankPct75 = Math.Floor((dvScores.Count * 0.75) + 0.5) + 1
			Else
				' if only have one record, 25th and 75th percentiles are same
				RankPct25 = 1
				RankPct75 = 1
			End If

			For i = 0 To dvScores.Count - 1
				drRpt = dsRpt.Tables(TableNamePart2).NewRow

				drRpt("TestDescrip") = TestDescrip

				' set the rank - can set by loop counter, since records are already sorted by score
				drRpt("Rank") = i + 1
				' indicate if the current rank is equal to either or both of the computed percentile rank
				If CInt(drRpt("Rank")) = RankPct25 Then
					drRpt("Percentile") = "25th Percentile"
				End If
				If CInt(drRpt("Rank")) = RankPct75 Then
					If drRpt("Percentile").ToString = "" Then
						drRpt("Percentile") = "75th Percentile"
					Else
						drRpt("Percentile") &= vbCrLf & "75th Percentile"
					End If
				End If

				' find the related student info in Students table, and set student identifier and name
				drStudent = dsRaw.Tables(TblNameStudents).Rows.Find(dvScores(i)("StudentId"))
				IPEDSFacade.SetStudentInfo(drStudent, drRpt)

				' record the score
				drRpt("Score") = dvScores(i)("Score")

				' add row to report DataSet
				dsRpt.Tables(TableNamePart2).Rows.Add(drRpt)
			Next

		' if no records with passed-in entrance test, create record with entrance test description, 
		'	NULL data otherwise
		Else
			drRpt = dsRpt.Tables(TableNamePart2).NewRow
			drRpt("TestDescrip") = TestDescrip
			dsRpt.Tables(TableNamePart2).Rows.Add(drRpt)

		End If

	End Sub

End Class
