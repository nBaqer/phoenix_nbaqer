' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' StudentAccountsFacade.vb
'
' StudentsAccountsFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================


Public Class StudentsAccountsFacade
    Public Function GetAllBanks(ByVal showActiveOnly As String) As DataSet

        '   Instantiate DAL component
        With New BanksDB

            '   get the dataset with all BankCodes
            Return .GetAllBanks(showActiveOnly)

        End With

    End Function
    Public Function GetAllPaymentDescriptions(ByVal showActiveOnly As String, ByVal campusid As Guid) As DataSet

        '   Instantiate DAL component
        With New PmtsDescriptionDB

            '   get the dataset with all BankCodes
            Return .GetAllPaymentDescriptions(showActiveOnly, campusid)

        End With

    End Function
    ''Added by Saraswathi lakshmanan to fix mantis 15222
    ''To get all the charge Descriptions
    Public Function GetAllChargeDescriptions(ByVal showActiveOnly As String, ByVal campusid As Guid) As DataSet

        '   Instantiate DAL component
        With New PmtsDescriptionDB

            '   get the dataset with all BankCodes
            Return .GetAllChargeDescriptions_sp(showActiveOnly, campusid)

        End With

    End Function

    ''Added by Saraswathi lakshmanan to fix mantis 15222
    ''To get all the charge Descriptions
    Public Function GetAllPaymentDescriptions_new(ByVal showActiveOnly As String, ByVal campusid As Guid) As DataSet

        '   Instantiate DAL component
        With New PmtsDescriptionDB

            '   get the dataset with all BankCodes
            Return .GetAllPaymentDescriptions_sp(showActiveOnly, campusid)

        End With

    End Function

    Public Function GetBankInfo(ByVal bankId As String) As BankInfo

        '   Instantiate DAL component
        With New BanksDB

            '   get the BankInfo
            Return .GetBankInfo(bankId)

        End With

    End Function
    Public Function GetPaymentInfo(ByVal bankId As String) As BankInfo

        '   Instantiate DAL component
        With New PmtsDescriptionDB

            '   get the BankInfo
            Return .GetPaymentInfo(bankId)

        End With

    End Function
    Public Function UpdateBankInfo(ByVal bankInfo As BankInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New BanksDB

            '   If it is a new account do an insert. If not, do an update
            If Not (bankInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddBankInfo(bankInfo, user)
            Else
                '   return integer with update results
                Return .UpdateBankInfo(bankInfo, user)
            End If

        End With

    End Function
    Public Function UpdatePaymentInfo(ByVal bankInfo As BankInfo) As String

        '   Instantiate DAL component
        With New PmtsDescriptionDB

            '   If it is a new account do an insert. If not, do an update
            If Not (bankInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddPaymentInfo(bankInfo)
            Else
                '   return integer with update results
                Return .UpdatePaymentInfo(bankInfo)
            End If

        End With

    End Function
    Public Function DeleteBankInfo(ByVal bankId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New BanksDB

            '   delete BankInfo ans return integer result
            Return .DeleteBankInfo(bankId, modDate)

        End With

    End Function
    Public Function DeletePaymentInfo(ByVal bankId As String) As String

        '   Instantiate DAL component
        With New PmtsDescriptionDB

            '   delete BankInfo ans return integer result
            Return .DeletePaymentInfo(bankId)

        End With

    End Function
    Public Function GetAllBankAccounts(ByVal showActiveOnly As Boolean) As DataSet

        '   Instantiate DAL component
        With New BanksDB

            '   get a dataset with all BankAccounts
            Return .GetAllBankAccounts(showActiveOnly)

        End With

    End Function
    Public Function GetBankAccounts(ByVal bankId As String, ByVal showActiveOnly As String) As DataSet

        '   Instantiate DAL component
        With New BanksDB

            '   get a dataset with all BankAccounts
            Return .GetBankAccounts(bankId, showActiveOnly)

        End With

    End Function
    Public Function GetBankAcctInfo(ByVal bankAcctId As String) As BankAcctInfo

        '   Instantiate DAL component
        With New BanksDB

            '   get the BankInfo
            Return .GetBankAcctInfo(bankAcctId)

        End With

    End Function
    Public Function UpdateBankAcctInfo(ByVal bankAcctInfo As BankAcctInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New BanksDB

            '   If it is a new account do an insert. If not, do an update
            If Not (bankAcctInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddBankAccountInfo(bankAcctInfo, user)
            Else
                '   return integer with update results
                Return .UpdateBankAccountInfo(bankAcctInfo, user)
            End If

        End With

    End Function
    Public Function DeleteBankAcctInfo(ByVal bankAcctId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New BanksDB

            '   delete BankInfo ans return integer result
            Return .DeleteBankAcctInfo(bankAcctId, modDate)

        End With

    End Function
    Public Function GetAllCreditCardTypes(ByVal showActiveOnly As String) As DataSet

        '   Instantiate DAL component
        With New CreditCardTypesDB

            '   get a dataset with all CreditCardTypes
            Return .GetAllCreditCardTypes(showActiveOnly)

        End With

    End Function
    Public Function GetAllPaidBys(ByVal showActiveOnly As String) As DataSet

        '   Instantiate DAL component
        With New PaidBysDB

            '   get the dataset with all PaidBys
            Return .GetAllPaidBys(showActiveOnly)

        End With

    End Function
    Public Function GetAdmissionDepositInfo(ByVal admissionDepositId As String) As AdmissionDepositInfo

        '   Instantiate DAL component
        With New BanksDB

            '   get the BankInfo
            Return .GetAdmissionDepositInfo(admissionDepositId)

        End With

    End Function
    Public Function UpdateAdmissionDepositInfo(ByVal admissionDepositInfo As AdmissionDepositInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New BanksDB

            '   If it is a new account do an insert. If not, do an update
            If Not (admissionDepositInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddAdmissionDepositInfo(admissionDepositInfo, user)
            Else
                '   return integer with update results
                Return .UpdateAdmissionDepositInfo(admissionDepositInfo, user)
            End If

        End With

    End Function
    Public Function DeleteAdmissionDepositInfo(ByVal AdmissionDepositId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New BanksDB

            '   delete BankInfo ans return integer result
            Return .DeleteAdmissionDepositInfo(AdmissionDepositId, modDate)

        End With

    End Function
    Public Function GetCreditCardTypeInfo(ByVal creditCardTypeId As String) As CreditCardTypeInfo

        '   Instantiate DAL component
        With New CreditCardTypesDB

            '   get the BankInfo
            Return .GetCreditCardTypeInfo(creditCardTypeId)

        End With

    End Function
    Public Function UpdateCreditCardTypeInfo(ByVal creditCardTypeInfo As CreditCardTypeInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New CreditCardTypesDB

            '   If it is a new account do an insert. If not, do an update
            If Not (creditCardTypeInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddCreditCardTypeInfo(creditCardTypeInfo, user)
            Else
                '   return integer with update results
                Return .UpdateCreditCardTypeInfo(creditCardTypeInfo, user)
            End If

        End With

    End Function
    Public Function DeleteCreditCardTypeInfo(ByVal CreditCardTypeId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New CreditCardTypesDB

            '   delete BankInfo ans return integer result
            Return .DeleteCreditCardTypeInfo(CreditCardTypeId, modDate)

        End With

    End Function
    Public Function GetPaidByInfo(ByVal creditCardTypeId As String) As PaidByInfo

        '   Instantiate DAL component
        With New PaidBysDB

            '   get the BankInfo
            Return .GetPaidByInfo(creditCardTypeId)

        End With

    End Function
    Public Function UpdatePaidByInfo(ByVal creditCardTypeInfo As PaidByInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New PaidBysDB

            '   If it is a new account do an insert. If not, do an update
            If Not (creditCardTypeInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddPaidByInfo(creditCardTypeInfo, user)
            Else
                '   return integer with update results
                Return .UpdatePaidByInfo(creditCardTypeInfo, user)
            End If

        End With

    End Function
    Public Function DeletePaidByInfo(ByVal PaidById As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New PaidBysDB

            '   delete BankInfo ans return integer result
            Return .DeletePaidByInfo(PaidById, modDate)

        End With

    End Function
    Public Function GetAllAcademicYears() As DataSet

        '   get the dataset with all AcademicYears
        Return (New AcademicYearsDB).GetAllAcademicYears()

    End Function
    Public Function GetAcademicYearInfo(ByVal AcademicYearId As String) As AcademicYearInfo

        '   Instantiate DAL component
        With New AcademicYearsDB

            '   get the BankInfo
            Return .GetAcademicYearInfo(AcademicYearId)

        End With

    End Function
    Public Function UpdateAcademicYearInfo(ByVal AcademicYearInfo As AcademicYearInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New AcademicYearsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (AcademicYearInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddAcademicYearInfo(AcademicYearInfo, user)
            Else
                '   return integer with update results
                Return .UpdateAcademicYearInfo(AcademicYearInfo, user)
            End If

        End With

    End Function
    Public Function DeleteAcademicYearInfo(ByVal AcademicYearId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New AcademicYearsDB

            '   delete BankInfo ans return integer result
            Return .DeleteAcademicYearInfo(AcademicYearId, modDate)

        End With

    End Function
    Public Function GetAllTerms(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "") As DataSet

        '   Instantiate DAL component
        With New TermsDB

            '   get the dataset with all BankCodes
            Return .GetAllTerms(showActiveOnly, campusId)

        End With

    End Function
    Public Function GetAllTerms(ByVal feeType As AdvantageCommonValues.TuitionFeeTypes, ByVal campusId As String) As DataSet

        '   Instantiate DAL component
        With New TermsDB

            '   get the dataset with all BankCodes
            Return .GetAllTerms(feeType, campusId)

        End With

    End Function
    Public Function GetAllCohorts(ByVal feeType As AdvantageCommonValues.TuitionFeeTypes, ByVal campusId As String) As DataSet

        '   Instantiate DAL component
        With New TermsDB

            '   get the dataset with all BankCodes
            Return .GetAllCohorts(feeType, campusId)

        End With

    End Function
    Public Function GetTermInfo(ByVal TermId As String) As TermInfo

        '   Instantiate DAL component
        With New TermsDB

            '   get the BankInfo
            Return .GetTermInfo(TermId)

        End With

    End Function
    Public Function UpdateTermInfo(ByVal TermInfo As TermInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New TermsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (TermInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddTermInfo(TermInfo, user)
            Else
                '   return integer with update results
                Return .UpdateTermInfo(TermInfo, user)
            End If

        End With

    End Function
    Public Function DeleteTermInfo(ByVal TermId As String) As String

        '   Instantiate DAL component
        With New TermsDB

            '   delete BankInfo ans return integer result
            Return .DeleteTermInfo(TermId)

        End With

    End Function
    Public Function GetAllTransCodes(ByVal showActiveOnly As Boolean) As DataSet

        '   Instantiate DAL component
        With New TransCodesDB

            '   get the dataset with all BankCodes
            Return .GetAllTransCodes(showActiveOnly)

        End With

    End Function
    Public Function GetAllTransCodes(ByVal sysTransCodeId As Integer, ByVal showActiveOnly As Boolean) As DataSet

        '   Instantiate DAL component
        With New TransCodesDB

            '   get the dataset with all BankCodes
            Return .GetAllTransCodes(sysTransCodeId, showActiveOnly)

        End With

    End Function
    Public Function GetTransCodeInfo(ByVal TransCodeId As String) As TransCodeInfo

        '   Instantiate DAL component
        With New TransCodesDB

            '   get the BankInfo
            Return .GetTransCodeInfo(TransCodeId)

        End With

    End Function
    Public Function UpdateTransCodeInfo(ByVal TransCodeInfo As TransCodeInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New TransCodesDB

            '   If it is a new account do an insert. If not, do an update
            If Not (TransCodeInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddTransCodeInfo(TransCodeInfo, user)
            Else
                '   return integer with update results
                Return .UpdateTransCodeInfo(TransCodeInfo, user)
            End If

        End With

    End Function
    Public Function DeleteTransCodeInfo(ByVal TransCodeId As String) As String

        '   Instantiate DAL component
        With New TransCodesDB

            '   delete BankInfo ans return integer result
            Return .DeleteTransCodeInfo(TransCodeId)

        End With

    End Function
    Public Function GetPostChargeInfo(ByVal PostChargeId As String) As PostChargeInfo

        '   Instantiate DAL component
        With New TransactionsDB

            '   get the BankInfo
            Return .GetPostChargeInfo(PostChargeId)

        End With

    End Function
    Public Function UpdatePostChargeInfo(ByVal PostChargeInfo As PostChargeInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New TransactionsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (PostChargeInfo.IsInDB = True) Then
                ' if it is a reversal 
                If PostChargeInfo.IsReversal Then Return .AddReversedPostChargeInfo(PostChargeInfo, user)
                '   return integer with insert results
                Return .AddPostChargeInfo(PostChargeInfo, user)
            Else
                '   return integer with update results
                Return .UpdatePostChargeInfo(PostChargeInfo, user)
            End If

        End With

    End Function
    Public Function DeletePostChargeInfo(ByVal PostChargeId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New TransactionsDB

            '   delete BankInfo ans return integer result
            Return .DeletePostChargeInfo(PostChargeId, modDate)

        End With

    End Function
    Public Function GetAllEnrollmentsPerStudent(ByVal studentId As String) As DataSet

        '   Instantiate DAL component
        With New StudentsDB

            '   get a dataset with all Enrollments per Student
            Return .GetAllEnrollmentsPerStudent(studentId)

        End With

    End Function
    Public Function GetAllStudents(ByVal showActiveOnly As Boolean) As DataSet

        '   Instantiate DAL component
        With New StudentsDB

            '   get the dataset with all Students
            Return .GetAllStudents(showActiveOnly)

        End With

    End Function
    Public Function GetPostPaymentInfo(ByVal postPaymentId As String) As PostPaymentInfo

        '   Instantiate DAL component
        With New TransactionsDB

            '   get the BankInfo
            '  Return .GetPostPaymentInfo(postPaymentId)
            ''Modified by Saraswathi lakshmanan on April 13 2010
            ''Converted to stored procedures
            Return .GetPostPaymentInfo_sp(postPaymentId)

        End With

    End Function
    Public Function UpdatePostPaymentInfo(ByVal postPaymentInfo As PostPaymentInfo, ByVal user As String, ByVal ScheduledDisb As ArrayList, Optional ByVal appPmtColl As ArrayList = Nothing) As String

        '   Instantiate DAL component
        With New TransactionsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (postPaymentInfo.IsInDB = True) Then
                '   if it is a reversal then update properly the DB
                If postPaymentInfo.IsReversal Then Return .AddReversedPostPaymentInfo(postPaymentInfo, user)
                '   return integer with insert results
                Return .AddPostPaymentInfo(postPaymentInfo, user, ScheduledDisb, appPmtColl)
            Else
                '   return integer with update results
                Return .UpdatePostPaymentInfo(postPaymentInfo, user)
            End If

        End With

    End Function
    Public Function DeletePostPaymentInfo(ByVal postPaymentId As String, ByVal modDate As DateTime, ByVal modDate1 As DateTime) As String

        '   Instantiate DAL component
        With New TransactionsDB

            '   delete BankInfo ans return integer result
            Return .DeletePostPaymentInfo(postPaymentId, modDate, modDate1)

        End With

    End Function
    Public Function GetAllFundSources(ByVal showActiveOnly As String, Optional ByVal titleIV As Boolean = False) As DataSet

        '   Instantiate DAL component
        With New FundSourcesDB

            '   get a dataset with all FundSources
            Return .GetAllFundSources(showActiveOnly, titleIV)

        End With

    End Function
    Public Function GetAllFundSourcesX(ByVal showActiveOnly As String, ByVal campusId As String, Optional ByVal  nontitleIvOnly As boolean = false) As DataSet

        '   Instantiate DAL component
        With New FundSourcesDB
            '   get a dataset with all FundSources
            Return .GetAllFundSourcesByCampusX(showActiveOnly, campusId, nontitleIvOnly)
        End With

    End Function
    Public Function GetAvailableAwardsForStudent(ByVal stuEnrollId As String, ByVal CutoffDate As String) As DataSet

        '   Instantiate DAL component
        With New StudentAwardsDB
            ''Converted to stored Procedure by Saraswathi lakshmanan on April 15 2010
            '   get a dataset with all FundSources
            ''New Code Addded By Vijay Ramteke On June 09, 2010 For Mantis Id 18950
            'Return .GetAvailableAwardsForStudent_Sp(stuEnrollId)
            Return .GetAvailableAwardsForStudent_Sp(stuEnrollId, CutoffDate)
            ''New Code Addded By Vijay Ramteke On June 09, 2010 For Mantis Id 18950
        End With

    End Function
    Public Function GetAllAwardTypes() As DataSet

        '   Instantiate DAL component
        With New FundSourcesDB

            '   get the dataset with all BankCodes
            Return .GetAwardTypes()

        End With

    End Function
    Public Function GetAdvantageFundSources() As DataSet

        '   Instantiate DAL component
        With New FundSourcesDB

            '   get the dataset with all BankCodes
            Return .GetAdvantageFundSources()

        End With

    End Function
    Public Function GetFundSourceInfo(ByVal FundSourceId As String) As FundSourceInfo

        '   Instantiate DAL component
        With New FundSourcesDB

            '   get the BankInfo
            Return .GetFundSourceInfo(FundSourceId)

        End With

    End Function
    Public Function UpdateFundSourceInfo(ByVal FundSourceInfo As FundSourceInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New FundSourcesDB

            '   If it is a new account do an insert. If not, do an update
            If Not (FundSourceInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddFundSourceInfo(FundSourceInfo, user)
            Else
                '   return integer with update results
                Return .UpdateFundSourceInfo(FundSourceInfo, user)
            End If

        End With
    End Function
    Public Function DeleteFundSourceInfo(ByVal FundSourceId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New FundSourcesDB

            '   delete BankInfo ans return integer result
            Return .DeleteFundSourceInfo(FundSourceId, modDate)
        End With

    End Function
    Public Function GetAllTermsPerStudent(ByVal studentId As String) As DataSet

        '   Instantiate DAL component
        With New StudentsDB

            '   get a dataset with all Terms per Student
            Return .GetAllTermsPerStudent(studentId)

        End With

    End Function
    Public Function GetAllTermsForStudentEnrollment(ByVal stuEnrollid As String, ByVal campusid As String)
        With New StudentsDB

            '   get a dataset with all Terms per Student
            Return .GetAllTermsForStudentEnrollment(stuEnrollid, campusid)

        End With
    End Function


    Public Function GetStudentLedger(ByVal studentId As String, ByVal enrollmentId As String, ByVal termId As String) As DataSet

        '   get a dataset with all Enrollments per Student
        Dim ds As DataSet

        ds = (New TransactionsDB).GetStudentLedger(studentId, enrollmentId, termId)

        ''DocumentId is added to the student ledger
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1

                    If Not ds.Tables(0).Rows(i)("CheckNumber") Is DBNull.Value Then
                        Select Case (ds.Tables(0).Rows(i)("PaymentTypeId").ToString)
                            Case PaymentType.Check
                                ds.Tables(0).Rows(i)("CheckNumber") = "Check Number: " + ds.Tables(0).Rows(i)("CheckNumber").ToString
                            Case PaymentType.Credit_Card
                                ds.Tables(0).Rows(i)("CheckNumber") = "C/C Authorization: " + ds.Tables(0).Rows(i)("CheckNumber").ToString
                            Case PaymentType.EFT
                                ds.Tables(0).Rows(i)("CheckNumber") = "EFT Number: " + ds.Tables(0).Rows(i)("CheckNumber").ToString
                            Case PaymentType.Money_Order
                                ds.Tables(0).Rows(i)("CheckNumber") = "Money Order Number: " + ds.Tables(0).Rows(i)("CheckNumber").ToString
                            Case PaymentType.Non_Cash
                                ds.Tables(0).Rows(i)("CheckNumber") = "Non Cash Reference #: " + ds.Tables(0).Rows(i)("CheckNumber").ToString
                        End Select
                    End If
                Next
            End If
        End If
        ds.AcceptChanges()
        Return ds


    End Function
    Public Function GetAvailableDeposits(ByVal startDate As Date?, ByVal endDate As Date?, ByVal bankAcct As String, ByVal campusId As String, bDeposited As Boolean) As DataSet

        '   Instantiate DAL component
        With New TransactionsDB

            '   get a dataset with all Available Deposits
            Return .GetAvailableDeposits(startDate, endDate, bankAcct, campusId, bDeposited)

        End With

    End Function
    Public Function UpdateSelectedDeposits(ByVal bankAcctId As String, ByVal user As String, ByVal selectedDeposits() As String) As String

        '   Instantiate DAL component
        With New TransactionsDB

            '   return integer with update results
            Return .UpdateSelectedDeposits(bankAcctId, user, selectedDeposits)

        End With

    End Function

    Public Function UpdateSelectedLeadDeposits(ByVal user As String, ByVal selectedDeposits() As String) As String

        '   Instantiate DAL component
        With New TransactionsDB

            '   return integer with update results
            Return .UpdateSelectedLeadDeposits(user, selectedDeposits)

        End With

    End Function
    Public Function GetMinAndMaxDatesFromTransactions(ByVal campusId As String) As Date()

        '   Instantiate DAL component
        With New TransactionsDB

            '   return date Array
            Return .GetMinAndMaxDatesFromTransactions(campusId)

        End With

    End Function
    Public Function GetMinAndMaxDatesFromStudentAndLeadTransactions(ByVal campusId As String) As Date()

        '   Instantiate DAL component
        With New TransactionsDB

            '   return date Array
            Return .GetMinAndMaxDatesFromStudentAndLeadTransactions(campusId)

        End With

    End Function
    Public Function GetMinAndMaxDatesFromGLDistributions() As Date()

        '   Instantiate DAL component
        With New TransactionsDB

            '   return date Array
            Return .GetMinAndMaxDatesFromGLDistributions()

        End With

    End Function
    Public Function GetPostRefundInfo(ByVal postRefundId As String) As PostRefundInfo

        '   Instantiate DAL component
        With New TransactionsDB

            '   get the BankInfo
            Return .GetPostRefundInfo(postRefundId)

        End With

    End Function
    Public Function UpdatePostRefundInfo(ByVal postRefundInfo As PostRefundInfo, ByVal postedRefunds As String, ByVal user As String) As String

        '   Instantiate DAL component
        With New TransactionsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (postRefundInfo.IsInDB = True) Then
                '   if it is a reversal... update database properly
                If postRefundInfo.IsReversal Then Return .AddReversedPostRefundInfo(postRefundInfo, user)
                '   return integer with insert results
                Return .AddPostRefundInfo(postRefundInfo, postedRefunds, user)
            Else
                '   return integer with update results
                Return .UpdatePostRefundInfo(postRefundInfo, user)
            End If

        End With

    End Function
    ''Function Added by Kamalesh Ahuja on 15 Jan 2010 to Resolve Mantis Issue Id 12038 and 08425
    Public Function UpdatePostRefundInfoNew(ByVal postRefundInfo As PostRefundInfo, ByVal postedRefunds As String, ByVal user As String) As String

        '   Instantiate DAL component
        With New TransactionsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (postRefundInfo.IsInDB = True) Then
                '   if it is a reversal... update database properly
                If postRefundInfo.IsReversal Then Return .AddReversedPostRefundInfo(postRefundInfo, user)
                '   return integer with insert results
                Return .AddPostRefundInfoNew(postRefundInfo, postedRefunds, user)
            Else
                '   return integer with update results
                Return .UpdatePostRefundInfo(postRefundInfo, user)
            End If

        End With

    End Function

    Public Function DeletePostRefundInfo(ByVal postRefundId As String, ByVal modDate As DateTime, ByVal modDate1 As DateTime) As String

        '   Instantiate DAL component
        With New TransactionsDB

            '   delete BankInfo ans return integer result
            Return .DeletePostRefundInfo(postRefundId, modDate, modDate1)

        End With

    End Function
    'Public Function SaveXmlDocumentTest(ByVal guid As String, ByVal xmlAsVarChar As String) As String

    '    '   Instantiate DAL component
    '    With New TransactionsDB

    '        '   save string as Xml Document return integer result
    '        Return .SaveXmlDocumentTest(guid, xmlAsVarChar)

    '    End With

    'End Function
    'Public Function GetXmlDocumentTest(ByVal guid As String) As System.Xml.XmlDocument

    '    '   Instantiate DAL component
    '    With New TransactionsDB

    '        '   save string as Xml Document return integer result
    '        Return .GetXmlDocumentTest(guid)

    '    End With

    'End Function
    ''Modified by Saraswathi On july 20 2009
    ''For mantis case 16590
    ''Include Voids is added to the search list
    ''Include voids modified to Voids list having three options shoow only voids, do not show void and show all
    ''Modified on august 18 2009 by Saraswathi lakshmanan

    Public Function SearchTransactions(ByVal campusId As String, ByVal transCodeId As String, ByVal transDateFrom As Date, ByVal transDateTo As Date, ByVal transAmountFrom As Decimal, ByVal transAmountTo As Decimal, ByVal description As String, ByVal reference As String, ByVal termId As String, ByVal academicYearId As String, ByVal charges As Boolean, ByVal payments As Boolean, ByVal debitAdjustments As Boolean, ByVal creditAdjustments As Boolean, ByVal VoidList As Integer) As DataSet

        '   Instantiate DAL component
        With New TransactionsDB

            '   get a dataset with all Enrollments per Student
            Return .SearchTransactions(campusId, transCodeId, transDateFrom, transDateTo, transAmountFrom, transAmountTo, description, reference, termId, academicYearId, charges, payments, debitAdjustments, creditAdjustments, VoidList)

        End With

    End Function
    Public Function GetGLDistributionsDS(ByVal TransactionId As String) As DataSet

        '   Instantiate DAL component
        With New TransactionsDB

            '   get the GLDistributions Dataset
            Return .GetGLDistributionsDS(TransactionId)

        End With

    End Function
    Public Function UpdateGLDistributionsDS(ByVal ds As DataSet) As String

        '   Instantiate DAL component
        With New TransactionsDB

            '   update GLDistributions Dataset
            Return .UpdateGLDistributionsDS(ds)

        End With

    End Function
    Public Function GetCommonTransactionInfo(ByVal CommonTransactionId As String) As CommonTransactionInfo

        '   Instantiate DAL component
        With New TransactionsDB

            '   get the BankInfo
            Return .GetCommonTransactionInfo(CommonTransactionId)

        End With

    End Function
    Public Function UpdateCommonTransactionInfo(ByVal commonTransactionInfo As CommonTransactionInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New TransactionsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (commonTransactionInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddCommonTransactionInfo(commonTransactionInfo, user)
            Else
                '   return integer with update results
                Return .UpdateCommonTransactionInfo(commonTransactionInfo, user)
            End If

        End With

    End Function
    Public Function DeleteCommonTransactionInfo(ByVal commonTransactionId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New TransactionsDB

            '   delete BankInfo ans return integer result
            Return .DeleteCommonTransactionInfo(commonTransactionId, modDate)

        End With

    End Function
    Public Function GetAllGLAccounts(ByVal showActiveOnly As Boolean) As DataSet

        '   Instantiate DAL component
        With New GLAccountsDB

            '   get the dataset with all GLAccounts
            Return .GetAllGLAccounts(showActiveOnly)

        End With

    End Function
    Public Function GetTuitionEarningsInfo(ByVal TuitionEarningId As String) As TuitionEarningsInfo

        '   Instantiate DAL component
        With New TuitionEarningsDB

            '   get the TuitionEarningsInfo
            Return .GetTuitionEarningsInfo(TuitionEarningId)

        End With

    End Function
    Public Function UpdateTuitionEarningsInfo(ByVal TuitionEarningsInfo As TuitionEarningsInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New TuitionEarningsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (TuitionEarningsInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddTuitionEarningsInfo(TuitionEarningsInfo, user)
            Else
                '   return integer with update results
                Return .UpdateTuitionEarningsInfo(TuitionEarningsInfo, user)
            End If

        End With

    End Function
    Public Function DeleteTuitionEarningsInfo(ByVal TuitionEarningId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New TuitionEarningsDB

            '   delete TuitionEarningsInfo ans return integer result
            Return .DeleteTuitionEarningsInfo(TuitionEarningId, modDate)

        End With

    End Function
    Public Function GetAllTuitionEarnings() As DataSet

        '   Instantiate DAL component
        With New TuitionEarningsDB

            '   get the dataset with all TuitionEarnings
            Return .GetAllTuitionEarnings()

        End With

    End Function
    Public Function GetTuitionEarningsDS() As DataSet

        '   Instantiate DAL component
        With New TuitionEarningsDB

            '   get the TuitionEarningsPercentageRanges Dataset
            Return .GetTuitionEarningsDS()

        End With

    End Function
    Public Function UpdateTuitionEarningsDS(ByVal ds As DataSet) As String

        '   Instantiate DAL component
        With New TuitionEarningsDB

            '   get the TuitionEarningsPercentageRanges Dataset
            Return .UpdateTuitionEarningsDS(ds)

        End With

    End Function
    Public Function GetAllTuitionCategories(ByVal showActiveOnly As String) As DataSet

        '   Instantiate DAL component
        With New TuitionCategoriesDB

            '   get the dataset with all TuitionCategories
            Return .GetAllTuitionCategories(showActiveOnly)

        End With

    End Function
    Public Function GetTuitionCategoryInfo(ByVal TuitionCategoryId As String) As TuitionCategoryInfo

        '   Instantiate DAL component
        With New TuitionCategoriesDB

            '   get the TuitionCategoryInfo
            Return .GetTuitionCategoryInfo(TuitionCategoryId)

        End With

    End Function
    Public Function UpdateTuitionCategoryInfo(ByVal TuitionCategoryInfo As TuitionCategoryInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New TuitionCategoriesDB

            '   If it is a new account do an insert. If not, do an update
            If Not (TuitionCategoryInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddTuitionCategoryInfo(TuitionCategoryInfo, user)
            Else
                '   return string with update results
                Return .UpdateTuitionCategoryInfo(TuitionCategoryInfo, user)
            End If

        End With

    End Function
    Public Function DeleteTuitionCategoryInfo(ByVal TuitionCategoryId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New TuitionCategoriesDB

            '   delete TuitionCategoryInfo ans return string result
            Return .DeleteTuitionCategoryInfo(TuitionCategoryId, modDate)

        End With

    End Function
    Public Function GetAllBillingMethods(ByVal showActiveOnly As String) As DataSet

        '   Instantiate DAL component
        With New BillingMethodsDB

            '   get the dataset with all BillingMethods
            Return .GetAllBillingMethods(showActiveOnly)

        End With

    End Function
    Public Function GetBillingMethodInfo(ByVal BillingMethodId As String) As BillingMethodInfo

        '   Instantiate DAL component
        With New BillingMethodsDB

            '   get the BillingMethodInfo
            Return .GetBillingMethodInfo(BillingMethodId)

        End With

    End Function
    Public Function BillingMethodCheckForChargedPaymentPeriods(ByVal BillingMethodId As String) As Boolean
        '   Instantiate DAL component
        Dim DB As New BillingMethodsDB

        '   get the dataset with all degrees
        Return DB.BillingMethodCheckForChargedPaymentPeriods(BillingMethodId)
    End Function
    Public Function UpdateBillingMethodInfo(ByVal BillingMethodInfo As BillingMethodInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New BillingMethodsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (BillingMethodInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddBillingMethodInfo(BillingMethodInfo, user)
            Else
                '   return integer with update results
                Return .UpdateBillingMethodInfo(BillingMethodInfo, user)
            End If

        End With

    End Function
    Public Function DeleteBillingMethodInfo(ByVal BillingMethodId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New BillingMethodsDB

            '   delete BillingMethodInfo ans return string result
            Return .DeleteBillingMethodInfo(BillingMethodId, modDate)

        End With

    End Function
    Public Function GetIncrementInfo(ByVal BillingMethodId As String) As IncrementInfo

        '   Instantiate DAL component
        With New BillingMethodsDB

            '   get the BillingMethodInfo
            Return .GetIncrementInfo(BillingMethodId)

        End With

    End Function
    Public Function UpdateIncrementInfo(ByVal IncrementInfo As IncrementInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New BillingMethodsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (IncrementInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddIncrementInfo(IncrementInfo, user)
            Else
                '   return integer with update results
                Return .UpdateIncrementInfo(IncrementInfo, user)
            End If

        End With

    End Function
    Public Function DeleteIncrementInfo(ByVal BillingMethodId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New BillingMethodsDB

            '   delete BillingMethodInfo ans return string result
            Return .DeleteIncrementInfo(BillingMethodId, modDate)

        End With

    End Function
    Public Function GetPmtPeriods(ByVal IncrementId As String) As DataSet

        '   Instantiate DAL component
        With New BillingMethodsDB

            '   get the BillingMethodInfo
            Return .GetPmtPeriods(IncrementId)

        End With

    End Function
    Public Function UpdatePmtPeriodInfo(ByVal IsInDB As Boolean, ByVal PmtPeriodInfo As PmtPeriodInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New BillingMethodsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (IsInDB = True) Then
                '   return integer with insert results
                Return .AddPmtPeriodInfo(PmtPeriodInfo, user)
            Else
                '   return integer with update results
                Return .UpdatePmtPeriodInfo(PmtPeriodInfo, user)
            End If

        End With
    End Function
    Public Function UpdatePmtPeriodsDS(ByVal ds As DataSet) As String

        '   Instantiate DAL component
        With New BillingMethodsDB

            '   delete PmtPeriodsIncrementsInfo ans return string result
            Return .UpdatePmtPeriodsDS(ds)

        End With

    End Function
    Public Function DeletePmtPeriodInfo(ByVal PmtPeriodId As String) As String

        '   Instantiate DAL component
        With New BillingMethodsDB

            '   delete PmtPeriodsIncrementsInfo ans return string result
            Return .DeletePmtPeriodInfo(PmtPeriodId)

        End With

    End Function
    Public Function DeletePmtPeriodsForAIncrementId(ByVal IncrementId As String) As String

        '   Instantiate DAL component
        With New BillingMethodsDB

            '   delete PmtPeriods For A IncrementId ans return string result
            Return .DeletePmtPeriodsForAIncrementId(IncrementId)

        End With

    End Function

    Public Function CheckIfPrgVersionsHaveABillingMethodId(ByVal BillingMethodId As String) As Boolean
        With New BillingMethodsDB
            Return .CheckIfPrgVersionsHaveABillingMethodId(BillingMethodId)
        End With

    End Function

    Public Function GetAllPeriodicFees(ByVal showActiveOnly As Boolean) As DataSet
        '   get the dataset with all PeriodicFees
        Return (New PeriodicFeesDB).GetAllPeriodicFees(showActiveOnly)
    End Function
    Public Function GetAllPrgVersionFees(ByVal showActiveOnly As Boolean) As DataSet
        '   get the dataset with all PrgVersionFees
        Return (New PrgVersionFeesDB).GetAllPrgVersionFees(showActiveOnly)
    End Function
    'Public Function GetAllCourseFees(ByVal showActiveOnly As Boolean) As DataSet
    '    '   get the dataset with all CourseFees
    '    Return (New CourseFeesDB).GetAllCourseFees(showActiveOnly)
    'End Function
    Public Function GetPeriodicFeeInfo(ByVal PeriodicFeeId As String) As PeriodicFeeInfo

        '   get the PeriodicFeeInfo
        Return (New PeriodicFeesDB).GetPeriodicFeeInfo(PeriodicFeeId)

    End Function
    Public Function GetPrgVersionFeeInfo(ByVal PrgVersionFeeId As String) As PrgVersionFeeInfo

        '   get the PrgVersionFeeInfo
        Return (New PrgVersionFeesDB).GetPrgVersionFeeInfo(PrgVersionFeeId)

    End Function
    Public Function GetCourseFeeInfo(ByVal CourseFeeId As String) As CourseFeeInfo

        '   get the CourseFeeInfo
        Return (New CourseFeesDB).GetCourseFeeInfo(CourseFeeId)

    End Function
    Public Function UpdatePeriodicFeeInfo(ByVal PeriodicFeeInfo As PeriodicFeeInfo, ByVal user As String, ByVal terms() As String) As String
        '   If it is a new account do an insert. If not, do an update
        If Not (PeriodicFeeInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New PeriodicFeesDB).AddPeriodicFeeInfo(PeriodicFeeInfo, user, terms)
        Else
            '   return integer with update results
            Return (New PeriodicFeesDB).UpdatePeriodicFeeInfo(PeriodicFeeInfo, user, terms)
        End If
    End Function
    Public Function UpdatePrgVersionFeeInfo(ByVal PrgVersionFeeInfo As PrgVersionFeeInfo, ByVal user As String) As String
        '   If it is a new account do an insert. If not, do an update
        If Not (PrgVersionFeeInfo.IsInDB = True) Then
            '   return string with insert results
            Return (New PrgVersionFeesDB).AddPrgVersionFeeInfo(PrgVersionFeeInfo, user)
        Else
            '   return string with update results
            Return (New PrgVersionFeesDB).UpdatePrgVersionFeeInfo(PrgVersionFeeInfo, user)
        End If
    End Function
    Public Function UpdateCourseFeeInfo(ByVal CourseFeeInfo As CourseFeeInfo, ByVal user As String) As String
        '   If it is a new account do an insert. If not, do an update
        If Not (CourseFeeInfo.IsInDB = True) Then
            '   return string with insert results
            Return (New CourseFeesDB).AddCourseFeeInfo(CourseFeeInfo, user)
        Else
            '   return string with update results
            Return (New CourseFeesDB).UpdateCourseFeeInfo(CourseFeeInfo, user)
        End If
    End Function
    Public Function DeletePeriodicFeeInfo(ByVal PeriodicFeeId As String, ByVal modDate As DateTime) As String

        '   delete PeriodicFeeInfo ans return string result
        Return (New PeriodicFeesDB).DeletePeriodicFeeInfo(PeriodicFeeId, modDate)

    End Function
    Public Function DeletePrgVersionFeeInfo(ByVal PrgVersionFeeId As String, ByVal modDate As DateTime) As String

        '   delete PrgVersionFeeInfo ans return string result
        Return (New PrgVersionFeesDB).DeletePrgVersionFeeInfo(PrgVersionFeeId, modDate)

    End Function
    Public Function DeleteCourseFeeInfo(ByVal CourseFeeId As String, ByVal modDate As DateTime) As String

        '   delete CourseFeeInfo ans return string result
        Return (New CourseFeesDB).DeleteCourseFeeInfo(CourseFeeId, modDate)

    End Function
    Public Function GetAllProgramVersions(ByVal showActiveOnly As Boolean) As DataSet

        '   Instantiate DAL component
        With New StudentsDB

            '   get a dataset with all Enrollments per Student
            Return .GetAllProgramVersions(True)

        End With

    End Function
    Public Function GetAllRateSchedules(ByVal showActiveOnly As Boolean) As DataSet

        '   Instantiate DAL component
        With New RateSchedulesDB

            '   get a dataset with all RateSchedules
            Return .GetAllRateSchedules()

        End With

    End Function
    Public Function GetRateSchedulesDS() As DataSet

        '   Instantiate DAL component
        With New RateSchedulesDB

            '   get the RateSchedulesPercentageRanges Dataset
            Return .GetRateSchedulesDS()

        End With

    End Function
    Public Function UpdateRateSchedulesDS(ByVal ds As DataSet) As String

        '   Instantiate DAL component
        With New RateSchedulesDB

            '   get the RateSchedules Dataset
            Return .UpdateRateSchedulesDS(ds)

        End With

    End Function
    Public Function GetAllCourses(ByVal showActiveOnly As Boolean) As DataSet

        '   Instantiate DAL component
        With New CoursesDB

            '   get a dataset with all courses
            Return .GetAllCourses(True)

        End With

    End Function
    Public Function GetAllCoursesNotCourseGroups(ByVal showActiveOnly As Boolean, Optional ByVal campusId As String = "") As DataSet

        '   Instantiate DAL component
        With New CoursesDB

            '   get a dataset with all courses
            Return .GetAllCoursesNotCourseGroups(True, campusId)

        End With

    End Function
    Public Function GetTransCodesDS(ByVal showActiveOnly As String) As DataSet

        '   Instantiate DAL component
        With New TransCodesDB

            '   get the TransCodes Dataset
            Return .GetTransCodesDS(showActiveOnly)

        End With

    End Function
    Public Function UpdateTransCodesDS(ByVal ds As DataSet) As String

        '   Instantiate DAL component
        With New TransCodesDB

            '   get the TransCodes Dataset
            Return .UpdateTransCodesDS(ds)

        End With

    End Function

    Public Function SearchGLDistributions(ByVal campGrpId As String, ByVal transDateFrom As Date, ByVal transDateTo As Date, ByVal summary As Boolean) As IList(Of GeneralLedgerInfo)

        '   Instantiate DAL component
        With New TransactionsDB
            ' If summary Then
            '   get a dataset with summary GL Distributions
            '  Return .SearchGLDistributions_new(campGrpId, transDateFrom, transDateTo)
            '   Else
            '   get a dataset with all GL Distributions
            'Dim ds = .SearchGLDistributionsDetails(campGrpId, transDateFrom, transDateTo)
            Dim glList As IList(Of GeneralLedgerInfo) = .SearchGlDistributionsDetails(campGrpId, transDateFrom, transDateTo)
            Dim leadGlList As IList(Of GeneralLedgerInfo) = .SearchGlForLead(campGrpId, transDateFrom, transDateTo)

            For Each info As GeneralLedgerInfo In leadGlList
                glList.Add(info)
            Next
            ' Order the list by Date
            glList = glList.OrderBy(Function(x) x.CreateDate).ToList()

            If summary Then
                'Create a summary .....

                Dim query = glList.GroupBy(Function(c) c.TransactionDescrip)



                Dim glsummary As IList(Of GeneralLedgerInfo) = New List(Of GeneralLedgerInfo)

                For Each info As GeneralLedgerInfo In glList

                    Dim info1 = info
                    Dim item = glsummary.SingleOrDefault(Function(x) x.GlAccount.Trim() = info1.GlAccount.Trim() And x.TransactionDescrip.Trim() = info1.TransactionDescrip.Trim())

                    If IsNothing(item) Then
                        Dim led As GeneralLedgerInfo = New GeneralLedgerInfo()
                        led.GlAccountId = info.GlAccountId
                        led.GlAccount = info.GlAccount
                        led.TransactionDescrip = info.TransactionDescrip
                        led.Transaction = info.Transaction
                        led.Credit = If(IsNothing(info.Credit), 0, info.Credit)
                        led.Debit = If(IsNothing(info.Debit), 0, info.Debit)
                        glsummary.Add(led)

                    Else
                        Dim item1 = glsummary.IndexOf(item)
                        glsummary(item1).Credit += If(IsNothing(info.Credit), 0, info.Credit)
                        glsummary(item1).Debit += If(IsNothing(info.Debit), 0, info.Debit)
                    End If
                Next

                'For Each info As GeneralLedgerInfo In glList

                '    Dim info1 = info
                '    If glsummary.Any(Function(x) x.GlAccount = info1.GlAccount) Then
                '        Continue For
                '    End If
                '    Dim led As GeneralLedgerInfo = New GeneralLedgerInfo()
                '    led.GlAccountId = info.GlAccountId
                '    led.GlAccount = info.GlAccount
                '    led.TransactionDescrip = info.TransactionDescrip
                '    led.Credit = 0
                '    led.Debit = 0
                '    For Each fo As GeneralLedgerInfo In glList
                '        If led.GlAccount = fo.GlAccount Then
                '            led.Debit += If(IsNothing(fo.Debit), 0, fo.Debit)
                '            led.Credit += If(IsNothing(fo.Credit), 0, fo.Credit)
                '            led.Transaction = String.Empty
                '            led.CreateDate = Nothing
                '            led.EndDate = Nothing
                '        End If
                '    Next

                '    glsummary.Add(led)
                'Next




                'For Each info As GeneralLedgerInfo In glList

                '    Dim info1 = info
                '    If glsummary.Any(Function(x) x.GlAccountId = info1.GlAccountId) Then
                '        Continue For
                '    End If
                '    Dim led As GeneralLedgerInfo = New GeneralLedgerInfo()
                '    led.GlAccountId = info.GlAccountId
                '    led.GlAccount = info.GlAccount
                '    led.TransactionDescrip = info.TransactionDescrip
                '    led.Credit = 0
                '    led.Debit = 0
                '    For Each fo As GeneralLedgerInfo In glList
                '        If led.GlAccountId = fo.GlAccountId Then
                '            led.Debit += If(IsNothing(fo.Debit), 0, fo.Debit)
                '            led.Credit += If(IsNothing(fo.Credit), 0, fo.Credit)
                '            led.Transaction = String.Empty
                '            led.CreateDate = Nothing
                '            led.EndDate = Nothing
                '        End If
                '    Next

                '    glsummary.Add(led)
                'Next



                If glsummary.Count > 0 Then
                    glsummary(0).CreateDate = transDateFrom
                    glsummary(0).EndDate = transDateTo
                End If
                    Return glsummary


                Else
                    Return glList
                End If





        End With
    End Function

    Public Function GetAllDeferredRevenuesByTransCode(ByVal reportDate As Date) As DataSet
        '   Instantiate DAL component
        With New TuitionEarningsDB

            '   get a dataset with AllDeferredRevenuesByTransCode
            Return .GetAllDeferredRevenuesByTransCode(reportDate)

        End With
    End Function
    'Public Function GetAllDeferredRevenuesByTransCodeDBI(ByVal reportDate As Date, ByVal campusId As String) As DataTable
    '    '   Instantiate DAL component
    '    With New TuitionEarningsDB

    '        '   get a dataset with AllDeferredRevenuesByTransCode
    '        Return .GetAllDeferredRevenuesByTransCodeDBI(reportDate, campusId)

    '    End With
    'End Function
    ''Modified by Saraswathi lakshmanan on June 29 2009
    ''The return type is chenged from datatable to dataset
    ''Modified by Saraswathi lakshmanan on June 29 2009
    ''The return type is chenged from datatable to dataset
    ''Optional parameter added by Saraswathi lakshmanan on June 30 2010
    ''To run the deferred revenue starting from given transaction Date
    Public Function GetAllDeferredRevenuesByTransCodeDBI(ByVal reportDate As Date, ByVal campusId As String, Optional ByVal TransDate As String = "") As DataSet
        '   Instantiate DAL component
        With New TuitionEarningsDB

            '   get a dataset with AllDeferredRevenuesByTransCode
            Return .GetAllDeferredRevenuesByTransCodeDBI(reportDate, campusId, TransDate)

        End With
    End Function

    Public Function GetDeferredRevenueDetailsByTransCode(ByVal transCodeId As String, ByVal reportDate As Date) As DataSet
        '   Instantiate DAL component
        With New TuitionEarningsDB

            '   get a dataset with AllDeferredRevenuesByTransCode
            Return .GetDeferredRevenueDetailsByTransCode(transCodeId, reportDate)

        End With
    End Function
    ''Optional Parameter added by Saraswathi Lakshmanan on June 30 20109
    ''To fix deferred revenue for Coyne
    Public Function GetDeferredRevenueDetailsByTransCodeDBI(ByVal transCodeId As String, ByVal reportDate As Date, ByVal campusId As String, Optional ByVal TransDate As String = "") As DataTable
        '   Instantiate DAL component
        With New TuitionEarningsDB

            '   get a datatable with DeferredRevenueDetailsByTransCode
            Return .GetDeferredRevenueDetailsByTransCodeDBI(transCodeId, reportDate, campusId, TransDate)

        End With
    End Function

    Public Function GetDeferredRevenueDetailsForTransCode(ByVal transCodeId As String, ByVal reportDate As Date, ByVal campusId As String, ByVal studentIdentifier As String, _
                                                          ByVal mode As Integer, ByVal user As String, ByVal transCutOffDate As String) As DataTable
        Dim teDB As New TuitionEarningsDB

        Return teDB.GetDeferredRevenueDetailsForTransCode(transCodeId, reportDate, campusId, studentIdentifier, mode, user, transCutOffDate)
    End Function


    Public Function GetDeferredRevenueDetailsByEnrollment(ByVal stuEnrollId As String, ByVal transCodeId As String) As DataSet
        '   Instantiate DAL component
        With New TuitionEarningsDB

            '   get a dataset with AllDeferredRevenuesByTransCode
            Return .GetDeferredRevenueDetailsByEnrollment(stuEnrollId, transCodeId)

        End With
    End Function
    Public Function GetDeferredRevenueTotalsByEnrollment(ByVal stuEnrollId As String) As DataSet
        '   Instantiate DAL component
        With New TuitionEarningsDB

            '   get a dataset with AllDeferredRevenuesByTransCode
            Return .GetDeferredRevenueTotalsByEnrollment(stuEnrollId)

        End With
    End Function
    Public Function PostDeferredRevenues(ByVal reportDate As Date, ByVal user As String) As String

        '   Instantiate DAL component
        With New TuitionEarningsDB

            '   return integer with insert results
            Return .PostDeferredRevenues(reportDate, user)

        End With

    End Function
    Public Function PostDeferredRevenuesDBI(ByVal reportDate As Date, ByVal user As String, ByVal campusId As String, Optional ByVal TransDate As String = "") As DataSet

        '   Instantiate DAL component
        With New TuitionEarningsDB

            '   return integer with insert results
            Return .PostDeferredRevenuesDBI(reportDate, user, campusId, TransDate)

        End With

    End Function

    Public Function GetDeferredRevenueResults(ByVal reportDate As Date, ByVal campusId As String, ByVal studentIdentifierType As String, ByVal mode As Integer, ByVal user As String, Optional ByVal CutOffTransDate As Date = Nothing) As DataSet
        Dim db As New TuitionEarningsDB

        If (IsNothing(CutOffTransDate) Or CutOffTransDate = Date.MinValue Or CutOffTransDate = "#12:00:00 AM#") Then
            Return db.GetDeferredRevenueResults(reportDate, campusId, studentIdentifierType, mode, user)
        Else
            Return db.GetDeferredRevenueResults(reportDate, campusId, studentIdentifierType, mode, user, CutOffTransDate)
        End If

    End Function


    ''Modified by saraswathi lakshmanan on may 19 2009
    ''The deferred revenue dates are fetched based on the campus id
    Public Function GetEndOfMonthDatesForPostingDeferredRevenueCampus(ByVal CampusID As String) As DataTable

        '   Instantiate DAL component
        With New TuitionEarningsDB
            '   return integer with insert results
            Return .GetEndOfMonthDatesForPostingDeferredRevenueCampus(CampusID)
        End With

    End Function

    ''Modified by saraswathi lakshmanan on may 19 2009
    ''The deferred revenue dates are fetched based on the campus id
    Public Function GetEndOfMonthDatesForPostingDeferredRevenue() As DataTable

        '   Instantiate DAL component
        With New TuitionEarningsDB
            '   return integer with insert results
            Return .GetEndOfMonthDatesForPostingDeferredRevenue()
        End With

    End Function
    'Public Function GetAllTransactionTypes(ByVal showActiveOnly As Boolean) As DataSet

    '    '   Instantiate DAL component
    '    With New TransactionTypesDB

    '        '   get the dataset with all TransactionTypes
    '        Return .GetAllTransactionTypes(showActiveOnly)

    '    End With

    'End Function

    'Public Function GetAllTransactionTypesXmlDoc(ByVal showActiveOnly As Boolean) As XmlDocument

    '    '   Instantiate DAL component
    '    With New TransactionTypesDB

    '        '   get the dataset with all TransactionTypes
    '        Return .GetAllTransactionTypesXmlDoc(showActiveOnly)

    '    End With

    'End Function
    'Public Function GetTransactionTypeInfo(ByVal TransactionTypeId As String) As TransactionTypeInfo

    '    '   Instantiate DAL component
    '    With New TransactionTypesDB

    '        '   get the TransactionTypeInfo
    '        Return .GetTransactionTypeInfo(TransactionTypeId)

    '    End With

    'End Function
    'Public Function UpdateTransactionTypeInfo(ByVal TransactionTypeInfo As TransactionTypeInfo, ByVal user As String) As String

    '    '   Instantiate DAL component
    '    With New TransactionTypesDB

    '        '   If it is a new account do an insert. If not, do an update
    '        If Not (TransactionTypeInfo.IsInDB = True) Then
    '            '   return integer with insert results
    '            Return .AddTransactionTypeInfo(TransactionTypeInfo, user)
    '        Else
    '            '   update TransactionTypeInfo and return string result
    '            Return .UpdateTransactionTypeInfo(TransactionTypeInfo, user)
    '        End If

    '    End With

    'End Function
    'Public Function DeleteTransactionTypeInfo(ByVal TransactionTypeId As String) As String

    '    '   Instantiate DAL component
    '    With New TransactionTypesDB

    '        '   delete TransactionTypeInfo ans return string result
    '        Return .DeleteTransactionTypeInfo(TransactionTypeId)

    '    End With

    'End Function
    Public Function GetAllBatchPayments(ByVal isPosted As Boolean, ByVal dateFrom As Date, ByVal dateTo As Date) As DataSet

        '   Instantiate DAL component
        With New BatchPaymentsDB

            '   get the dataset with all BatchPayments
            Return .GetAllBatchPayments(isPosted, dateFrom, dateTo)

        End With

    End Function
    Public Function GetBatchPaymentInfo(ByVal BatchPaymentId As String) As BatchPaymentInfo

        '   Instantiate DAL component
        With New BatchPaymentsDB

            '   get the BatchPaymentInfo
            Return .GetBatchPaymentInfo(BatchPaymentId)

        End With

    End Function
    Public Function UpdateBatchPaymentInfo(ByVal BatchPaymentInfo As BatchPaymentInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New BatchPaymentsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (BatchPaymentInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddBatchPaymentInfo(BatchPaymentInfo, user)
            Else
                '   return string with update results
                Return .UpdateBatchPaymentInfo(BatchPaymentInfo, user)
            End If

        End With

    End Function
    Public Function DeleteBatchPaymentInfo(ByVal BatchPaymentId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New BatchPaymentsDB

            '   delete BatchPaymentInfo ans return string result
            Return .DeleteBatchPaymentInfo(BatchPaymentId, modDate)

        End With

    End Function
    Public Function GetMinAndMaxDatesFromBatchPayments() As Date()

        '   Instantiate DAL component
        With New BatchPaymentsDB

            '   return date Array
            Return .GetMinAndMaxDatesFromBatchPayments()

        End With

    End Function
    Public Function GetBatchPaymentsDS(ByVal isPosted As Boolean, ByVal dateFrom As Date, ByVal dateTo As Date) As DataSet

        '   Instantiate DAL component
        With New BatchPaymentsDB

            '   get the TransCodes Dataset
            Return .GetBatchPaymentsDS(isPosted, dateFrom, dateTo)

        End With

    End Function
    Public Function UpdateBatchPaymentsDS(ByVal ds As DataSet) As String

        '   Instantiate DAL component
        With New BatchPaymentsDB

            '   update GLDistributions Dataset
            Return .UpdateBatchPaymentsDS(ds)

        End With

    End Function
    Public Function GetTransCodesByCourse(ByVal courseId As String) As DataSet
        '   get the dataset with all TransCodes
        Return (New CourseFeesDB).GetTransCodesByCourse(courseId)
    End Function
    Public Function GetTransCodesByProgramVersion(ByVal courseId As String) As DataSet
        '   get the dataset with all TransCodes
        Return (New PrgVersionFeesDB).GetTransCodesByProgramVersion(courseId)
    End Function
    Public Function GetTransCodesByTerm(ByVal termId As String) As DataSet
        '   get the dataset with all TransCodes
        Return (New PeriodicFeesDB).GetTransCodesByTerm(termId)
    End Function
    Public Function GetFutureTermsInWhichToApplyThisTransCodeId(ByVal termId As String, ByVal transCodeId As String, ByVal CampusID As String, ByVal selApplyToThesePrograms As String, Optional ByVal PrgVerId As String = "") As DataSet
        '   get the dataset with all Terms
        Return (New PeriodicFeesDB).GetFutureTermsInWhichToApplyThisTransCodeId(termId, transCodeId, CampusID, selApplyToThesePrograms, PrgVerId)
    End Function
    Public Function ApplyFeesByCourse(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String
        '   return string with update results
        Return (New TransactionsDB).ApplyFeesByCourse(stuEnrollId, clsSectionId, user, campusId, postFeesDate)
    End Function
    Public Function DeleteAppliedFeesByCourse(ByVal stuEnrollId As String, ByVal clsSectionId As String, ByVal user As String, ByVal campusId As String) As String
        '   return string with update results
        Return (New TransactionsDB).DeleteAppliedFeesByCourse(stuEnrollId, clsSectionId, user, campusId)
    End Function
    Public Function ApplyFeesByCourse(ByVal termId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String
        '   return string with update results
        Return (New TransactionsDB).ApplyFeesByCourse(termId, user, campusId, postFeesDate)
    End Function
    Public Function ApplyFeesByCourseForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String
        '   return string with update results
        Return (New TransactionsDB).ApplyFeesByCourseForCohortStart(termId, user, campusId, postFeesDate)
    End Function

    Public Function DeleteAppliedFeesByCourse(ByVal termId As String, ByVal user As String, ByVal campusId As String) As String
        '   return string with update results
        Return (New TransactionsDB).DeleteAppliedFeesByCourse(termId, user, campusId)
    End Function
    Public Function ApplyFeesByProgramVersion(ByVal stuEnrollId As String, ByVal tuitionCategoryId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String
        '   return string with update results
        Return (New TransactionsDB).ApplyFeesByProgramVersion(stuEnrollId, tuitionCategoryId, user, campusId, postFeesDate)
    End Function
    Public Function DeleteAppliedFeesByProgramVersion(ByVal stuEnrollId As String, ByVal tuitionCategoryId As String, ByVal user As String, ByVal campusId As String) As String
        '   return string with update results
        Return (New TransactionsDB).DeleteAppliedFeesByProgramVersion(stuEnrollId, tuitionCategoryId, user, campusId)
    End Function
    Public Function ApplyFeesByProgramVersion(ByVal termId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String
        '   return string with update results
        Return (New TransactionsDB).ApplyFeesByProgramVersion(termId, user, campusId, postFeesDate)
    End Function
    Public Function ApplyFeesByProgramVersionForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String
        '   return string with update results
        Return (New TransactionsDB).ApplyFeesByProgramVersionForCohortStart(termId, user, campusId, postFeesDate)
    End Function

    Public Function DeleteAppliedFeesByProgramVersion(ByVal termId As String, ByVal user As String, ByVal campusId As String) As String
        '   return string with update results
        Return (New TransactionsDB).DeleteAppliedFeesByProgramVersion(termId, user, campusId)
    End Function
    Public Function ApplyFeesByTerm(ByVal stuEnrollId As String, ByVal termId As String, ByVal tuitionCategoryId As String, ByVal user As String, ByVal campusId As String) As String
        '   return string with update results
        Return (New TransactionsDB).ApplyFeesByTerm(stuEnrollId, termId, tuitionCategoryId, user, campusId)
    End Function
    Public Function ApplyFeesByTerm(ByVal termId As String, ByVal tuitionCategoryId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String
        '   return string with update results
        Return (New TransactionsDB).ApplyFeesByTerm(termId, tuitionCategoryId, user, campusId, postFeesDate)
    End Function
    Public Function DeleteAppliedFeesByTerm(ByVal stuEnrollId As String, ByVal termId As String, ByVal tuitionCategoryId As String, ByVal user As String, ByVal campusId As String) As String
        '   return string with update results
        Return (New TransactionsDB).DeleteAppliedFeesByTerm(stuEnrollId, termId, tuitionCategoryId, user, campusId)
    End Function
    Public Function DeleteAppliedFeesByTerm(ByVal termId As String, ByVal tuitionCategoryId As String, ByVal user As String, ByVal campusId As String) As String
        '   return string with update results
        Return (New TransactionsDB).DeleteAppliedFeesByTerm(termId, tuitionCategoryId, user, campusId)
    End Function
    Public Function GetFeesToBePostedByTermDS(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        Dim dsProcessed As New DataSet
        Dim dsFinal As New DataSet

        ds = (New TransactionsDB).GetFeesToBePostedByTermDS(termId, user, campusId)
        'Remove duplicate charges for the same course for the same student.
        'dsProcessed = ProcesFeesToBePosted(ds)

        'For Term Fees we only want to return the summaries by enrollment and FeeId/transcodeid
        'dsFinal = GetSummaryFeesToBePosted(dsProcessed)

        Return ds
    End Function

    Private Function ProcesFeesToBePosted(ByVal ds As DataSet) As DataSet
        Dim ds1 As DataSet = ds.Clone

        'Add a field to act as a unique identifier so we can search better
        ds1.Tables(0).Columns.Add("PK", Type.GetType("System.Int32"))
        ds1.Tables(0).Columns("PK").AutoIncrement = True


        For Each dr As DataRow In ds.Tables(0).Rows
            ds1.Tables(0).ImportRow(dr)
        Next

        Dim ds2 As DataSet = ds1.Clone
        For Each dr As DataRow In ds1.Tables(0).Rows
            ds2.Tables(0).ImportRow(dr)
        Next

        Dim dsReturn As DataSet = ds1.Clone

        'Loop through the rows in ds1
        For Each dr As DataRow In ds1.Tables(0).Rows
            'Items with descrip of Term are term level fees and should be returned without any checks ie. term fees with unit of term
            If dr("Descrip") = "Term" Then
                dsReturn.Tables(0).ImportRow(dr)
            Else
                'For other rows we will only add them if there are no other rows for the same student, descrip/course, transaction code
                'and with a lower credential.If there is a tie then win is earliest start date followed by weeks in program then by pv name.
                Dim foundRows() As DataRow
                Dim filter As String

                filter = "PK <> " & dr("PK") & " AND StudentId='" & dr("StudentId").ToString & "' AND TransCodeId='" & dr("TransCodeId").ToString & "' AND Descrip='" & dr("Descrip") & "'"
                foundRows = ds2.Tables(0).Select(filter)

                If foundRows.Length = 0 Then
                    dsReturn.Tables(0).ImportRow(dr)
                Else
                    'Add the row if it has a lower credential level
                    If dr("CredentialLevel") < foundRows(0)("CredentialLevel") Then
                        dsReturn.Tables(0).ImportRow(dr)
                    ElseIf dr("CredentialLevel") = foundRows(0)("CredentialLevel") Then
                        'Add the row if it has an earlier start date
                        If dr("StartDate") < foundRows(0)("StartDate") Then
                            dsReturn.Tables(0).ImportRow(dr)
                        ElseIf dr("StartDate") = foundRows(0)("StartDate") Then
                            'Add the row if it has less weeks in the program
                            If dr("Weeks") < foundRows(0)("Weeks") Then
                                dsReturn.Tables(0).ImportRow(dr)
                            ElseIf dr("Weeks") = foundRows(0)("Weeks") Then
                                'Add the row if program version descrip is alphabetically less 
                                If dr("PrgVerDescrip") < foundRows(0)("PrgVerDescrip") Then
                                    dsReturn.Tables(0).ImportRow(dr)
                                End If
                            End If
                        End If
                    End If
                End If
            End If




        Next

        Return dsReturn
    End Function

    Private Function GetSummaryFeesToBePosted(ByVal ds As DataSet) As DataSet
        Dim dsReturn As DataSet = ds.Clone
        

        'Loop through each row in ds. If the enrollemnt and feeid does not exists in dsReturn then add record with summary for the transamount
        For Each dr As DataRow In ds.Tables(0).Rows
            Dim foundRows() As DataRow
            Dim filter As String
            filter = "StuEnrollId ='" & dr("StuEnrollId").ToString & "' AND FeeId = '" & dr("FeeId").ToString & "'"

            If dsReturn.Tables(0).Rows.Count = 0 Then
                'Add row
                dsReturn.Tables(0).ImportRow(dr)
            Else
                foundRows = dsReturn.Tables(0).Select(filter)
                If foundRows.Length = 0 Then
                    dsReturn.Tables(0).ImportRow(dr)
                End If
            End If
        Next

        'At this point dsReturn has the rows we want to return but we need the summaries for the TransAmount field
        For Each dr As DataRow In dsReturn.Tables(0).Rows
            Dim sumObject As Object
            Dim filter As String

            filter = "StuEnrollId ='" & dr("StuEnrollId").ToString & "' AND FeeId = '" & dr("FeeId").ToString & "'"

            sumObject = ds.Tables(0).Compute("Sum(TransAmount)", filter)

            dr("TransAmount") = Decimal.Parse(sumObject.ToString)
            dr("Descrip") = "Term"

        Next

        Return dsReturn
    End Function

    Public Function GetFeesToBePostedByTermDSForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet
        '   return string with update results
        Return (New TransactionsDB).GetFeesToBePostedByTermDSForCohortStart(termId, user, campusId)
    End Function

    Public Function GetFeesToBePostedByCourseDS(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        Dim dsProcessed As New DataSet

        ds = (New TransactionsDB).GetFeesToBePostedByCourseDS(termId, user, campusId)
        'Remove duplicate charges for the same course for the same student.
        'dsProcessed = ProcesFeesToBePosted(ds)
        Return ds
    End Function
    Public Function GetFeesToBePostedByCourseDSForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet
        '   return string with update results
        Return (New TransactionsDB).GetFeesToBePostedByCourseDSForCohortStart(termId, user, campusId)
    End Function

    Public Function GetFeesToBePostedByProgramVersionDS(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet
        '   return string with update results
        Return (New TransactionsDB).GetFeesToBePostedByProgramVersionDS(termId, user, campusId)
    End Function
    Public Function GetFeesToBePostedByProgramVersionDSForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet
        '   return string with update results
        Return (New TransactionsDB).GetFeesToBePostedByProgramVersionDSForCohortStart(termId, user, campusId)
    End Function
    Public Function ApplyFeesByTerm(ByVal termId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String
        '   return string with update results
        Return (New TransactionsDB).ApplyFeesByTerm(termId, user, campusId, postFeesDate)
    End Function

    Public Function ApplyFeesByTermForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String, ByVal postFeesDate As Date) As String
        '   return string with update results
        Return (New TransactionsDB).ApplyFeesByTermForCohortStart(termId, user, campusId, postFeesDate)
    End Function
    Public Function DeleteAppliedFeesByTerm(ByVal termId As String, ByVal user As String, ByVal campusId As String) As String
        '   return string with update results
        Return (New TransactionsDB).DeleteAppliedFeesByTerm(termId, user, campusId)
    End Function
    Public Function GetAwardScheduledDisb(ByVal stuEnrollId As String, ByVal fundSourceId As String, ByVal CutoffDate As String) As DataSet
        '   return dataset
        '  Return (New TransactionsDB).GetAwardScheduledDisb(stuEnrollId, fundSourceId)
        ''Changed to stored procedure on April 15 2010

        'New Code Added By Vijay Ramteke On June 09, 2010 For Mantis Id 18950
        'Return (New TransactionsDB).GetAwardScheduledDisb_Sp(stuEnrollId, fundSourceId)
        Return (New TransactionsDB).GetAwardScheduledDisb_Sp(stuEnrollId, fundSourceId, CutoffDate)
        'New Code Added By Vijay Ramteke On June 09, 2010 For Mantis Id 18950
    End Function
    Public Function GetPmtPlanScheduledDisb(ByVal stuEnrollId As String) As DataSet
        '   return dataset
        '  Return (New TransactionsDB).GetPmtPlanScheduledDisb(stuEnrollId)
        ''Converted to stored Procedure
        ''Added by Saraswathi lakshmanan on April 16 2010
        Return (New TransactionsDB).GetPmtPlanScheduledDisb_sp(stuEnrollId)
    End Function

    Public Function GetProjectedAmountByEnrollment(ByVal stuEnrollId As String) As Decimal
        '   return get the dataset with all BankCodes
        Return (New TransactionsDB).GetProjectedAmountByEnrollment(stuEnrollId)
    End Function
    Public Function GetProjectedAmountForStudent(ByVal studentId As String) As Decimal
        '   return get the dataset with all BankCodes
        Return (New TransactionsDB).GetProjectedAmountForStudent(studentId)
    End Function
    Public Function GetPaymentDisbursements(ByVal transactionId As String) As DataSet
        '   return dataset
        Return (New TransactionsDB).GetPaymentDisbursements(transactionId)
    End Function
    Public Function HasDisbursements(ByVal stuEnrollId As String) As DataTable
        '   return dataset
        '' Return (New TransactionsDB).HasDisbursements(stuEnrollId)
        ''Modified to stored Procerdure by SAraswathi Lakshmanan on April 13 2010
        Return (New TransactionsDB).HasDisbursements_sp(stuEnrollId)
    End Function
    Public Function IsThisTermBeingUsed(ByVal termId As String) As Boolean
        'return boolean value
        Return (New TermsDB).IsThisTermBeingUsed(termId)
    End Function
    Public Function CreateTimeIntervalsTable(ByVal startHour As DateTime, ByVal endHour As DateTime, ByVal timeSpan As Integer, ByVal deleteAllExistingRecords As Boolean, ByVal user As String) As String
        'return string
        Return (New TimeIntervalsDB).CreateTimeIntervalsTable(startHour, endHour, timeSpan, deleteAllExistingRecords, user)
    End Function
    Public Function GetAllTimeIntervals() As DataSet
        '   get the dataset with all TimeIntervals
        Return (New TimeIntervalsDB).GetAllTimeIntervals()
    End Function
    Public Function GetTimeIntervalInfo(ByVal TimeIntervalId As String) As TimeIntervalInfo
        '   get the TimeIntervalInfo
        Return (New TimeIntervalsDB).GetTimeIntervalInfo(TimeIntervalId)
    End Function
    Public Function UpdateTimeIntervalInfo(ByVal TimeIntervalInfo As TimeIntervalInfo, ByVal user As String) As String
        '   If it is a new account do an insert. If not, do an update
        If Not (TimeIntervalInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New TimeIntervalsDB).AddTimeIntervalInfo(TimeIntervalInfo, user)
        Else
            '   return integer with update results
            Return (New TimeIntervalsDB).UpdateTimeIntervalInfo(TimeIntervalInfo, user)
        End If
    End Function
    Public Function DeleteTimeIntervalInfo(ByVal TimeIntervalId As String, ByVal modDate As DateTime) As String
        '   delete TimeIntervalInfo ans return string result
        Return (New TimeIntervalsDB).DeleteTimeIntervalInfo(TimeIntervalId, modDate)
    End Function

    Public Function DeleteTimeIntervalInfoMulti(ByVal TimeIntervalDescrip As String) As String
        '   delete TimeIntervalInfo ans return string result
        Return (New TimeIntervalsDB).DeleteTimeIntervalInfoMulti(TimeIntervalDescrip)
    End Function

    Public Function GetChargesWithBalanceGreaterThanZero(ByVal stuEnrollId As String) As DataTable
        '' Return (New StuAccountBalanceDB).GetChargesWithBalanceGreaterThanZero(stuEnrollId)
        ''Modified by Saraswathi lakshmanan on April 13 2010
        ''Converted to stored Procedure
        Return (New StuAccountBalanceDB).GetChargesWithBalanceGreaterThanZero_sp(stuEnrollId)
    End Function

    Public Function GetDeferredRevenueDates() As DataTable
        Return (New TuitionEarningsDB).GetDeferredRevenueDates()
    End Function
    Public Function VoidTransaction(ByVal transactionId As String, ByVal userId As String) As String
        Return (New TransactionsDB).VoidTransaction(transactionId, userId)
    End Function
    Public Function ApplyLateFees(ByVal lateFees As String(), ByVal transCodeId As String, ByVal postFeesDate As Date, ByVal campusId As String, ByVal user As String) As String
        '   return string with update results
        Return (New TransactionsDB).ApplyLateFees(lateFees, transCodeId, postFeesDate, campusId, user)
    End Function
    Public Function GetDateOfLastLateFeesPosting(ByVal campusId As String) As Date
        '   return date Array
        Return (New TransactionsDB).GetDateOfLastLateFeesPosting(campusId)
    End Function
    Public Function GetPostedFeesByTermDS(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet
        '   return string with update results
        Return (New TransactionsDB).GetPostedFeesByTermDS(termId, user, campusId)
    End Function
    Public Function GetPostedFeesByTermDSForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet
        '   return string with update results
        Return (New TransactionsDB).GetPostedFeesByTermDSForCohortStart(termId, user, campusId)
    End Function

    Public Function GetPostedFeesByCourseDS(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet
        '   return string with update results
        Return (New TransactionsDB).GetPostedFeesByCourseDS(termId, user, campusId)
    End Function
    Public Function GetPostedFeesByCourseDSForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet
        '   return string with update results
        Return (New TransactionsDB).GetPostedFeesByCourseDSForCohortStart(termId, user, campusId)
    End Function

    Public Function GetPostedFeesByProgramVersionDS(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet
        '   return string with update results
        Return (New TransactionsDB).GetPostedFeesByProgramVersionDS(termId, user, campusId)
    End Function
    Public Function GetPostedFeesByProgramVersionDSForCohortStart(ByVal termId As String, ByVal user As String, ByVal campusId As String) As DataSet
        '   return string with update results
        Return (New TransactionsDB).GetPostedFeesByProgramVersionDSForCohortStart(termId, user, campusId)
    End Function
    Public Function GetTermsPerStudentEnrollment(ByVal stuEnrollId As String) As DataSet

        '   Instantiate DAL component
        With New StudentsDB

            '   get a dataset with all Terms per Student
            Return .GetTermsPerStudentEnrollment(stuEnrollId)

        End With

    End Function

    Public Function GetFeeLevelId(ByVal stuEnrollId As String, ByVal transCodeId As String) As Integer
        With New StudentDB
            Return .GetFeeLevelId(stuEnrollId, transCodeId)
        End With
    End Function
    'Function added by Saraswathi Lakshmanan to find the Student Ledger Balance
    'October 8-2008
    Public Function GetStudentLedgerBalance(ByVal stuEnrollId As String) As Double
        With New StudentDB
            ''COnverted to stored Procedure
            Return .GetStudentLedgerBalance_sp(stuEnrollId)
        End With
    End Function
    ''To show the default charge tpe for the selected transcode for a given student.
    Public Function GetDefaultChargesForTransCodeandStudent(ByVal StuEnrollId As String, ByVal TransCode As String) As DataSet
        With New StudentDB
            Return .GetDefaultChargesForTransCodeandStudent_Sp(StuEnrollId, TransCode)
        End With
    End Function
    ''To get the period detilas like academic year 1, academic year 2, 1-20 credits payment period etc
    ''This is used in Post charges page
    Public Function GetPeriodDetailsforStudentEnrollment(ByVal StuEnrollId As String, ByVal defPeriod As Integer) As DataSet
        Dim dsDefPeriod As DataSet

        With New StudentDB
            dsDefPeriod = .GetChargePeriodSequences(StuEnrollId, defPeriod)
        End With

        dsDefPeriod.Tables(0).Columns.Add(New DataColumn("Value", Type.GetType("System.String")))
        For Each chargeSeqRow As DataRow In dsDefPeriod.Tables(0).Rows
            If defPeriod = 4 Then
                chargeSeqRow("Value") = "AcademicYear - " + chargeSeqRow("StartValue").ToString
            Else
                chargeSeqRow("Value") = chargeSeqRow("StartValue").ToString + " - " + chargeSeqRow("EndValue").ToString
            End If

        Next
        dsDefPeriod.AcceptChanges()
        Return dsDefPeriod

    End Function

    Public Function GetManualPostingCutoffDate(ByVal AwardTypeId As String) As String
        With New FundSourcesDB
            Return .GetManualPostingCutoffDate(AwardTypeId)
        End With
    End Function

    Public Function GetAvailableAwardsForStudent_NoAid(ByVal stuEnrollId As String, ByVal CutoffDate As String) As DataSet
        With New StudentAwardsDB
            Return .GetAvailableAwardsForStudent_Sp_NoAid(stuEnrollId, CutoffDate)
        End With

    End Function
    Public Function GetAwardScheduledDisb_NoAid(ByVal stuEnrollId As String, ByVal fundSourceId As String, ByVal CutoffDate As String) As DataSet
        Return (New TransactionsDB).GetAwardScheduledDisb_Sp_NoAid(stuEnrollId, fundSourceId, CutoffDate)
    End Function
    'New Code Added By Vijay Ramteke On June 09, 2010 For Mantis Id 18950

    ''Added by Saraswathi lakshmanan to fix mantis 15222
    ''To get all the ADjustment Descriptions
    Public Function GetAllAdjustmentsDescriptions(ByVal showActiveOnly As String) As DataSet

        '   Instantiate DAL component
        With New PmtsDescriptionDB

            '   get the dataset with all BankCodes
            Return .GetAllAdjustmentsDescriptions_SP(showActiveOnly)

        End With

    End Function
    '' Code Added by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports
    Public Function GetAvailableAwardsForStudent_NoAidNew(ByVal CutoffDate As String, ByVal CampusId As Guid) As DataSet
        With New StudentAwardsDB
            Return .GetAvailableAwardsForStudent_Sp_NoAidNew(CutoffDate, CampusId)
        End With

    End Function
    ''
    '' Code Added by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports
    Public Function GetAvailableAwardsForStudentPP(ByVal CutoffDate As String, ByVal CampusId As Guid) As DataSet

        '   Instantiate DAL component
        With New StudentAwardsDB
            ''Converted to stored Procedure by Saraswathi lakshmanan on April 15 2010
            '   get a dataset with all FundSources

            'Return .GetAvailableAwardsForStudent_Sp(stuEnrollId)
            Return .GetAvailableAwardsForStudent_SpPP(CutoffDate, CampusId)

        End With

    End Function
    '' Code Added by Kamalesh Ahuja on July 21 2010 as per discussion with Troy For Revenue Ratio Reports
    Public Function GetAvailableAwardsForStudentNew(ByVal CutoffDate As String, ByVal CampusId As Guid) As DataSet

        '   Instantiate DAL component
        With New StudentAwardsDB
            ''Converted to stored Procedure by Saraswathi lakshmanan on April 15 2010
            '   get a dataset with all FundSources

            'Return .GetAvailableAwardsForStudent_Sp(stuEnrollId)
            Return .GetAvailableAwardsForStudent_SpNew(CutoffDate, CampusId)

        End With

    End Function
    ''Added by Saraswathi lakshmanan on Sept 22 2010
    'For document Tracking
    ''Function to check if the finaid requirements for Test are met.

    Public Function CheckIfStudentHasPassedRequiredTestWithProgramVersion_FinAid(ByVal StuEnrollId As String) As Integer
        Dim getDB As New RequirementsDB
        Return getDB.HasStudentMetFinAidTestRequirements_Sp(StuEnrollId)
    End Function


    Public Function CheckAllApprovedDocumentsWithPrgVersion_FinAid_Student(ByVal StuEnrollId As String) As Integer
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.HasStudentMetFinAidDocumentRequirements_Sp(StuEnrollId)

    End Function

    ''Added by Saraswathi lakshmanan on Sept 22 2010
    'For document Tracking
    ''Function to check if the finaid requirements for Test are met.

    Public Function CheckIfStudentHasPassedRequiredTestWithProgramVersion_Graduation(ByVal StuEnrollId As String) As Integer
        Dim getDB As New RequirementsDB
        Return getDB.HasStudentMetGraduationTestRequirements_Sp(StuEnrollId)
    End Function
    Public Function CheckIfStudentHasPassedRequiredTestWithProgramVersion_Enrollment(ByVal StuEnrollId As String) As Integer
        Dim getDB As New RequirementsDB
        Return getDB.HasStudentMetEnrollmentTestRequirements_Sp(StuEnrollId)
    End Function

    Public Function CheckAllApprovedDocumentsWithPrgVersion_Graduation(ByVal StuEnrollId As String) As Integer
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.HasStudentMetGraduationDocumentRequirements_Sp(StuEnrollId)

    End Function
    Public Function CheckAllApprovedDocumentsWithPrgVersion_Enrollment(ByVal StuEnrollId As String) As Integer
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.HasStudentMetEnrollmentDocumentRequirements_Sp(StuEnrollId)

    End Function
    Public Function PostTermFees(ByVal xmlRules As String) As String
        Dim db As New TransactionsDB
        db.PostTermFees(xmlRules)
        Return ""

    End Function
    Public Function CheckTransDuplicatePayment(postPaymentInfo As PostPaymentInfo) As Boolean
        Dim db As New TransactionsDB
        Return db.CheckTransDuplicatePayment(postPaymentInfo)


    End Function
    Public Function CheckTransDuplicateCharge(postChargeInfo As PostChargeInfo) As Boolean
        Dim db As New TransactionsDB
        Return db.CheckTransDuplicateCharge(postChargeInfo)


    End Function
    Public Function CheckTransDuplicateRefund(PostRefundInfo As PostRefundInfo) As Boolean
        Dim db As New TransactionsDB
        Return db.CheckTransDuplicateRefund(PostRefundInfo)


    End Function
End Class



