Public Class PlacementFacade
    Public Function GetAllSkillGroups() As DataSet
        With New PlacementDB
            Return .GetAllSkillGroups()
        End With
    End Function
    Public Function GetAllSkillGroups(ByVal CampusId As String, ByVal LeadId As String) As DataSet
        With New PlacementDB
            Return .GetAllSkillGroups(CampusId, LeadId)
        End With
    End Function
    Public Function GetHowPlaced() As DataSet

        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetHowPlaced()
        End With

    End Function
    Public Function GetExitInterview(ByVal ExitInterviewId As String) As PlacementInfo

        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetExitInterviewDetails(ExitInterviewId)
        End With

    End Function
    Public Function GetStudentName(ByVal StudentId As String) As DataSet
        With New PlacementDB
            Return .GetStudentName(StudentId)
        End With
    End Function
    Public Function GetResumeStudentPhone(ByVal StudentId As String) As DataSet
        Dim ds As New DataSet
        With New PlacementDB
            ds = .GetResumeStudentPhone(StudentId)
        End With
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim zipMask As String
        Dim dr As DataRow

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        Try
            For Each dr In ds.Tables(0).Rows
                If dr("Phone").ToString.Length >= 1 And dr("ForeignPhone") = 0 Then
                    dr("Phone") = facInputMasks.ApplyMask(strMask, dr("Phone"))
                End If
            Next
        Catch ex As Exception
            For Each dr In ds.Tables(0).Rows
                dr("Phone") = ""
            Next
        End Try
        Return ds
    End Function
    Public Function intCheckAddressExists(ByVal StudentId As String) As Integer
        With New PlacementDB
            Return .intCheckAddressExists(StudentId)
        End With
    End Function
    Public Function intCheckPhoneExists(ByVal StudentId As String) As Integer
        With New PlacementDB
            Return .intCheckPhoneExists(StudentId)
        End With
    End Function
    Public Function GetResumeStudentAddress(ByVal StudentId As String) As DataSet
        Dim ds As New DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            ds = .GetResumeStudentAddress(StudentId)
        End With

        Return ds
    End Function
    Public Function GetLeadAddress(ByVal LeadId As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetAllLeadAddress(LeadId)
        End With
    End Function
    Public Function GetStudentAddress(ByVal StudentId As String) As DataSet
        'Instantiate DAL component
        Dim ds As New DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim zipMask As String
        Dim dr As DataRow

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        With New PlacementDB
            ds = .GetAllStudentAddress(StudentId)
        End With
        For Each dr In ds.Tables(0).Rows
            dr("HomePhone") = facInputMasks.ApplyMask(strMask, dr("HomePhone"))
        Next
        Return ds
    End Function
    Public Function GetAllStudentAddressPhone(ByVal StudentId As String) As String
        With New PlacementDB
            Return .GetAllStudentAddressPhone(StudentId)

        End With
    End Function

    Public Function GetStudentSkills(ByVal StudentId As String) As DataSet

        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetAllStudentSkills(StudentId)
        End With

    End Function
    Public Function GetCollegeNames(ByVal StudentId As String, ByVal InstType As String) As DataSet

        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetCollegeNames(StudentId, InstType)
        End With

    End Function
    Public Function GetCollegeNamesByID(ByVal InstType As String, ByVal StudentID As String) As DataSet

        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetCollegeNamesByID(InstType, StudentID)
        End With
    End Function
    Public Function GetLeadEmployerNames(ByVal LeadId As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetLeadEmployerNames(LeadId)
        End With
    End Function
    Public Function GetEmployerNames(ByVal StudentId As String, ByVal showActiveOnly As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetEmployerNames(StudentId, showActiveOnly)
        End With
    End Function
    Public Function GetEmployerNamesByEnrollment(ByVal StudentId As String, ByVal EnrollmentId As String) As DataSet

        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetEmployerNamesByEnrollment(StudentId, EnrollmentId)
        End With

    End Function
    Public Function GetEmployerByPlacementId(ByVal placementid As String) As PlacementInfo

        With New PlacementDB
            Return .GetPlacementDetails(placementid)
        End With
    End Function
    Public Function GetStudentInfo(ByVal StudentId As String, ByVal CollegeId As String) As PlacementInfo

        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetStudentInfo(StudentId, CollegeId)
        End With

    End Function
    Public Function GetEducationInstAddress(ByVal EducationInstId As String, ByVal EducationInstType As String) As PlacementInfo

        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetEducationInstAddress(EducationInstId, EducationInstType)
        End With

    End Function
    Public Function GetPlacementDetails(ByVal PlacementId As String) As PlacementInfo
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetPlacementDetails(PlacementId)
        End With
    End Function
    Public Function GetEmploymentInfo(ByVal StudentId As String) As PlacementInfo

        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetEmploymentInfo(StudentId)
        End With

    End Function
    Public Function GetLeadEmploymentInfo(ByVal LeadId As String) As PlacementInfo

        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetLeadEmploymentInfo(LeadId)
        End With

    End Function
    Public Function GetAllDegrees() As DataSet

        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all Degrees
            Return .GetAllDegrees()
        End With

    End Function
    Public Function GetAllFieldStudy() As DataSet

        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all Degrees
            Return .GetAllFieldStudy()
        End With

    End Function
    Public Function GetAllSalaryType() As DataSet

        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all Degrees
            Return .GetAllSalaryType()
        End With

    End Function
    Public Function GetAllExtracurriculars() As DataSet

        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all Extracurriculars
            Return .GetAllExtracurriculars()
        End With

    End Function
    Public Function GetAllSkillsByGroup(ByVal SkillGrpId As String, ByVal StudentId As String) As DataSet

        With New PlacementDB
            'get the dataset with all PossibleSkills Based on Groups Available
            Return .GetAllSkillsByGroup(SkillGrpId, StudentId)
        End With
    End Function
    Public Function GetAllLeadSkillsByGroup(ByVal SkillGrpId As String, ByVal LeadId As String) As DataSet
        With New PlacementDB
            'get the dataset with all PossibleSkills Based on Groups Available
            Return .GetAllLeadSkillsByGroup(SkillGrpId, LeadId)
        End With
    End Function
    Public Function GetLeadSkillsByGroup(ByVal LeadId As String, ByVal CampusId As String) As DataSet
        With New PlacementDB
            'get the dataset with all PossibleSkills Based on Groups Available
            Return .GetLeadSkillsByGroup(LeadId, CampusId)
        End With
    End Function
    Public Function GetAllLeadSkillsByGroup(ByVal LeadId As String, ByVal CampusId As String, ByVal SkillGrpId As String) As DataSet
        With New PlacementDB
            'get the dataset with all PossibleSkills Based on Groups Available
            Return .GetAllLeadSkillsByGroup(LeadId, CampusId, SkillGrpId)
        End With
    End Function
    Public Function GetAllExtracurricularGroups() As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all Extracurricular Groups
            Return .GetAllExtracurricularGroups()
        End With
    End Function
    Public Function GetAllExtracurricularGroups(ByVal LeadId As String, ByVal CampusId As String) As DataSet
        Return (New PlacementDB).GetAllExtracurricularGroups(LeadId, CampusId)
    End Function
    'GetAllExtraCurricularsByGroup
    Public Function GetAllExtraCurricularsByGroup(ByVal ExtracurricularGrpId As String, ByVal StudentId As String) As DataSet

        With New PlacementDB
            'get the dataset with all Possible Extracurriculars Based on Groups Available
            Return .GetAllExtraCurricularsByGroup(ExtracurricularGrpId, StudentId)
        End With
    End Function
    Public Function GetAllLeadExtraCurricularsByGroup(ByVal ExtracurricularGrpId As String, ByVal LeadId As String) As DataSet
        With New PlacementDB
            Return .GetAllLeadExtraCurricularsByGroup(ExtracurricularGrpId, LeadId)
        End With
    End Function
    Public Function GetAllLeadExtraCurricularsByGroup(ByVal ExtracurricularGrpId As String, ByVal LeadId As String, ByVal CampusId As String) As DataSet
        With New PlacementDB
            Return .GetAllLeadExtraCurricularsByGroup(ExtracurricularGrpId, LeadId, CampusId)
        End With
    End Function
    Public Function GetStudentExtracurricularsByGroup(ByVal ExtracurricularGrpId As String, ByVal studentId As String) As DataSet

        With New PlacementDB
            'Get The Dataset of Student Skill By Group
            Return .GetStudentExtracurricularsByGroup(ExtracurricularGrpId, studentId)
        End With
    End Function
    Public Function GetLeadExtracurricularsByGroup(ByVal ExtracurricularGrpId As String, ByVal LeadId As String) As DataSet

        With New PlacementDB
            'Get The Dataset of Student Skill By Group
            Return .GetLeadExtracurricularsByGroup(ExtracurricularGrpId, LeadId)
        End With
    End Function
    Public Function GetLeadExtracurricularsByGroup(ByVal ExtracurricularGrpId As String, ByVal LeadId As String, ByVal campusId As String) As DataSet

        With New PlacementDB
            'Get The Dataset of Student Skill By Group
            Return .GetLeadExtracurricularsByGroup(ExtracurricularGrpId, LeadId, campusId)
        End With
    End Function
    Public Function GetStudentSkillsByGroup(ByVal SkillGrpId As String, ByVal studentId As String) As DataSet

        With New PlacementDB
            'Get The Dataset of Student Skill By Group
            Return .GetStudentSkillByGroup(SkillGrpId, studentId)
        End With
    End Function
    Public Function GetLeadSkillByGroup(ByVal SkillGrpId As String, ByVal LeadId As String) As DataSet
        With New PlacementDB
            'Get The Dataset of Student Skill By Group
            Return .GetLeadSkillByGroup(SkillGrpId, LeadId)
        End With
    End Function
    Public Function UpdateStudentExtracurriculars(ByVal StudentId As String, ByVal ExtracurricularGrpId As String, ByVal user As String, ByVal selectedCertifications() As String) As Integer

        '   Instantiate DAL component
        Dim StudentExtracurriculars As New PlacementDB

        'Update The Skills Based on StudentId and Skill GroupId
        Return StudentExtracurriculars.UpdateStudentExtracurriculars(StudentId, ExtracurricularGrpId, user, selectedCertifications)

    End Function
    Public Function UpdateLeadExtracurriculars(ByVal LeadId As String, ByVal ExtracurricularGrpId As String, ByVal user As String, ByVal selectedDegrees() As String)
        '   Instantiate DAL component
        Dim LeadExtracurriculars As New PlacementDB

        'Update The Skills Based on StudentId and Skill GroupId
        Return LeadExtracurriculars.UpdateLeadExtracurriculars(LeadId, ExtracurricularGrpId, user, selectedDegrees)

    End Function
    Public Function UpdateStudentSkills(ByVal StudentId As String, ByVal SkillGrpId As String, ByVal user As String, ByVal selectedCertifications() As String) As String

        '   Instantiate DAL component
        Dim StudentSkills As New PlacementDB

        'Update The Skills Based on StudentId and Skill GroupId
        Return StudentSkills.UpdateStudentSkills(StudentId, SkillGrpId, user, selectedCertifications)

    End Function
    Public Function UpdateLeadSkills(ByVal LeadId As String, ByVal SkillGrpId As String, ByVal user As String, ByVal selectedDegrees() As String)
        '   Instantiate DAL component
        Dim LeadSkills As New PlacementDB

        'Update The Skills Based on StudentId and Skill GroupId
        Return LeadSkills.UpdateLeadSkills(LeadId, SkillGrpId, user, selectedDegrees)
    End Function
    Public Function UpdateExtracurriculur(ByVal ExtracurricularInfo As PlacementInfo, ByVal user As String) As Integer

        With New PlacementDB
            If (ExtracurricularInfo.IsInDb = False) Then
                Return .AddExtracurricularInfo(ExtracurricularInfo, user)
            Else
                Return .UpdateExtracurricularInfo(ExtracurricularInfo, user)
            End If
        End With
    End Function
    Public Function GetStartDate(ByVal StudentId As String, ByVal PrgVerId As String) As String
        With New PlacementDB
            Return .GetStartDate(StudentId, PrgVerId)
        End With
    End Function
    Public Function UpdateStudentPlHistory(ByVal StudentPlInfo As PlacementInfo, ByVal user As String) As String
        With New PlacementDB
            If (StudentPlInfo.IsInDb = False) Then
                Return .AddStudentPlHistory(StudentPlInfo, user)
            Else
                Return .UpdateStudentPlHistory(StudentPlInfo, user)
            End If
        End With
    End Function
    Public Function GetAllExtracurricularDescription(ByVal showActiveOnly As Boolean) As DataSet

        '   Instantiate DAL component
        With New PlacementDB

            '   get the dataset with all BankCodes
            Return .GetAllExtracurricularDesc(showActiveOnly)

        End With
    End Function
    Public Function GetExtracurricularDetails(ByVal Extracurricularid As String) As PlacementInfo

        With New PlacementDB
            Return .GetExtracurricularDetails(Extracurricularid)
        End With
    End Function
    Public Function CheckStudentEducation(ByVal EducationInstId As String, ByVal SchoolType As String) As String
        With New PlacementDB
            Return .CheckStudentEducation(EducationInstId, SchoolType)
        End With
    End Function
    'Public Function GetStudentInfo(ByVal StudentId As String, ByVal EducationInstType As String) As PlacementInfo

    '    With New PlacementDB
    '        Return .GetStudentInfo(StudentId, EducationInstType)
    '    End With
    'End Function

    Public Function DeleteExtracurricular(ByVal ExtracurricularId As String) As Integer

        '   Instantiate DAL component
        With New PlacementDB

            '   delete EmployerInfo ans return integer result
            Return .DeleteExtracurriculur(ExtracurricularId)

        End With

    End Function
    Public Function GetAllSchools(ByVal SchoolType As String, ByVal campusid As String, Optional ByVal LeadId As String = "") As DataSet
        With New PlacementDB
            Return .GetAllSchools(SchoolType, campusid, LeadId)
        End With
    End Function
    Public Function GetAllSchoolsForStudent(ByVal SchoolType As String, ByVal campusid As String, Optional ByVal StudentId As String = "") As DataSet
        With New PlacementDB
            Return .GetAllSchoolsForStudent(SchoolType, campusid, StudentId)
        End With
    End Function
    Public Function GetAllSchools(ByVal SchoolType As String) As DataSet
        '   Instantiate DAL component
        With New PlacementDB
            'delete EmployerInfo ans return integer result
            Return .GetAllSchools(SchoolType)
        End With
    End Function
    Public Function GetAllSchoolsNotActive(ByVal SchoolType As String) As DataSet
        '   Instantiate DAL component
        With New PlacementDB
            'delete EmployerInfo ans return integer result
            Return .GetAllSchoolsNotActive(SchoolType)
        End With
    End Function
    Public Function GetAllStudentSchools(ByVal SchoolType As String) As DataSet
        '   Instantiate DAL component
        With New PlacementDB
            'delete EmployerInfo ans return integer result
            Return .GetAllStudentSchools(SchoolType)
        End With
    End Function
    '
    Public Function GetAddressBySchool(ByVal CollegeId As String, ByVal SchoolType As String) As PlacementInfo
        '   Instantiate DAL component
        With New PlacementDB
            'delete EmployerInfo ans return integer result
            Return .GetAddressBySchool(CollegeId, SchoolType)
        End With
    End Function
    Public Function GetAddressByEmployer(ByVal EmployerId As String) As PlacementInfo
        '   Instantiate DAL component
        With New PlacementDB
            'delete EmployerInfo ans return integer result
            Return .GetAddressByEmployer(EmployerId)
        End With
    End Function
    'Public Function UpdateStudentEducation(ByVal EducationInfo As PlacementInfo, ByVal user As String, ByVal Major As String, ByVal campusId As String) As String

    '    With New PlacementDB
    '        If (EducationInfo.IsInDb = False) Then
    '            Return .AddStudentEducation(EducationInfo, user, campusId)
    '        Else
    '            Return .UpdateStudentEducation(EducationInfo, user, Major)
    '        End If
    '    End With
    'End Function
    Public Function UpdateStudentEmployment(ByVal EducationInfo As PlacementInfo, ByVal user As String, ByVal PkValue As String) As String

        With New PlacementDB
            If (EducationInfo.IsInDb = False) Then
                Return .AddStudentEmployment(EducationInfo, user)
            Else
                Return .UpdateStudentEmployment(EducationInfo, user, PkValue)
            End If
        End With
    End Function
    Public Function UpdateLeadEmployment(educationInfo As PlacementInfo, user As String, pkValue As String) As String

        With New PlacementDB
            If (educationInfo.IsInDb = False) Then
                Return .AddLeadEmployment(educationInfo, user)
            Else
                Return .UpdateLeadEmployment(educationInfo, user, pkValue)
            End If
        End With
    End Function

    Public Function UpdateStudentExitInterview(ByVal EducationInfo As PlacementInfo, ByVal user As String) As String
        With New PlacementDB
            If (EducationInfo.IsInDb = False) Then
                Return .AddStudentExitInterview(EducationInfo, user)
            Else
                Return .UpdateStudentExitInterview(EducationInfo, user)
            End If
        End With
    End Function
    Public Function GetAllInterviewScheduleByStudent(ByVal StudentId As String)
        With New PlacementDB
            Return .GetAllInterviewScheduleByStudent(StudentId)
        End With
    End Function
    'Public Function UpdateStudentEduExtracurriculars(ByVal StudentId As String, ByVal EducationInstId As String, ByVal user As String, ByVal selectedCertifications() As String) As Integer

    '    '   Instantiate DAL component
    '    Dim StudentEduExtracurriculars As New PlacementDB

    '    'Update The Skills Based on StudentId and Skill GroupId
    '    Return StudentEduExtracurriculars.UpdateEduStudentExtracurriculurs(StudentId, EducationInstId, user, selectedCertifications)

    'End Function
    'Public Function GetStudentExtracurr(ByVal StudentId As String, ByVal CollegeId As String) As DataSet
    '    With New PlacementDB
    '        'get the dataset with all Possible Extracurriculars Based on Groups Available
    '        Return .GetStudentExtracurr(StudentId, CollegeId)
    '    End With
    'End Function
    Public Function GetStudentEducationInstID(ByVal PrimaryId As String) As String
        With New PlacementDB
            'get the dataset with all Possible Extracurriculars Based on Groups Available
            Return .GetEducationID(PrimaryId)
        End With
    End Function
    Public Function GetLeadEducationInstID(ByVal PrimaryId As String) As String
        With New PlacementDB
            'get the dataset with all Possible Extracurriculars Based on Groups Available
            Return .GetLeadEducationID(PrimaryId)
        End With
    End Function
    'Public Function GetValidExtracurr(ByVal StudentId As String, ByVal CollegeId As String) As Integer
    '    With New PlacementDB
    '        'get the dataset with all Possible Extracurriculars Based on Groups Available
    '        Return .GetValidExtracurr(StudentId, CollegeId)
    '    End With
    'End Function
    'Public Function DeleteStudentEducation(ByVal StuEducationId As String) As String
    '    With New PlacementDB
    '        'get the dataset with all Possible Extracurriculars Based on Groups Available
    '        Return .DeleteStudentEducation(StuEducationId)
    '    End With
    'End Function
    Public Function DeleteLeadEducation(ByVal LeadEducationId As String, ByVal ModDate As DateTime) As String
        With New PlacementDB
            'get the dataset with all Possible Extracurriculars Based on Groups Available
            Return .DeleteLeadEducation(LeadEducationId, ModDate)
        End With
    End Function
    Public Function DeletePlacementInfo(ByVal PlacementId As String) As String
        With New PlacementDB
            'get the dataset with all Possible Extracurriculars Based on Groups Available
            Return .DeletePlacementInfo(PlacementId)
        End With
    End Function
    Public Function DeleteStudentEmployment(ByVal StudentId As String) As String
        With New PlacementDB
            'get the dataset with all Possible Extracurriculars Based on Groups Available
            Return .DeleteStudentEmployment(StudentId)
        End With
    End Function
    Public Function DeleteLeadEmployment(ByVal LeadId As String, ByVal moddate As DateTime) As String
        With New PlacementDB
            'get the dataset with all Possible Extracurriculars Based on Groups Available
            Return .DeleteLeadEmployment(LeadId, moddate)
        End With
    End Function
    Public Function GetAllEmployers() As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetAllEmployers()
        End With
    End Function
    Public Function GetAllJobStatus() As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetAllJobStatus
        End With
    End Function
    Public Function GetAllJobTitles() As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetAllJobTitles
        End With
    End Function
    Public Function GetAllExtraCurrResume(ByVal StudentId As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetAllExtraCurrResume(StudentId)
        End With
    End Function

    Public Function GetAllJobTitlesByEmployer(ByVal Employer As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetAllJobTitlesByEmployer(Employer)
        End With
    End Function
    Public Function SkillGroup(ByVal StudentId As String) As String
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .SkillGroup(StudentId)
        End With
    End Function
    Public Function Experience(ByVal StudentId As String) As String
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .Experience(StudentId)
        End With
    End Function
    Public Function StudentExperience(ByVal StudentId As String) As String
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .StudentExperience(StudentId)
        End With
    End Function
    Public Function StudentEmploymentHistory(ByVal StudentId As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetStudentEmploymentHistory(StudentId)
        End With
    End Function
    Public Function GetEmploymentDates(ByVal StudentId As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetEmploymentDates(StudentId)
        End With
    End Function
    Public Function GetLeadEmploymentDates(ByVal LeadId As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetLeadEmploymentDates(LeadId)
        End With
    End Function
    Public Function GetSchoolStuEmpHistory(ByVal StudentId As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetSchoolStuEmpHistory(StudentId)
        End With
    End Function
    Public Function GetStudentCollegeHistory(ByVal StudentId As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetStudentCollegeHistory(StudentId)
        End With
    End Function
    Public Function GetResumeSkills(ByVal StudentId As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetResumeSkills(StudentId)
        End With
    End Function

    Public Function ResumeEducationHistory(ByVal LeadId As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .ResumeEducationHistory(LeadId)
        End With
    End Function
    Public Function ResumeStudentEducationHistory(ByVal StudentId As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .ResumeStudentEducationHistory(StudentId)
        End With
    End Function
    Public Function GetLeadCollegeHistory(ByVal LeadId As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetLeadCollegeHistory(LeadId)
        End With
    End Function
    Public Function GetStudentSchoolHistory(ByVal StudentId As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetStudentSchoolHistory(StudentId)
        End With
    End Function
    Public Function GetLeadSchoolHistory(ByVal LeadId As String) As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetLeadSchoolHistory(LeadId)
        End With
    End Function
    Public Function GetStudentExtracurriculars(ByVal StudentId As String) As String
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .ExtracurricularGroup(StudentId)
        End With
    End Function
    Public Function ConcatenateCodeTitleDescrip(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("EmployerJobId")}
        End With
        If ds.Tables("InstructorDT").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("CodeDescrip", GetType(String))
            For Each row In tbl.Rows
                row("CodeDescrip") = row("TitleCode") & " - " & row("EmployerJobTitle")
            Next
        End If
        Return ds
    End Function
    Public Function GetJobTitleByEmployer(ByVal EmployerId As String) As DataSet
        'Instantiate DAL component
        Dim ds As New DataSet
        With New PlacementDB
            'get the dataset with all SkillGroups
            ds = .GetJobTitleByEmployer(EmployerId)
        End With
        ds = ConcatenateCodeTitleDescrip(ds)
        Return ds
    End Function
    Public Function GetAllInterview() As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetAllInterview()
        End With
    End Function
    Public Function GetAllSchoolStatus() As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetAllSchoolStatus()
        End With
    End Function
    Public Function GetAllTransportation() As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetAllTransportation()
        End With
    End Function
    Public Function GetAllAssistance() As DataSet
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetAllAssistance()
        End With
    End Function
    Public Function GetNameByStudentId(ByVal StudentId As String) As PlacementInfo
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetNameByStudentId(StudentId)
        End With
    End Function
    Public Function GetEnrollmentDescrip(ByVal EnrollmentId As String) As PlacementInfo
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .GetEnrollmentDescrip(EnrollmentId)
        End With
    End Function
    Public Function DeleteExitInterview(ByVal ExitInterviewId As String) As String
        'Instantiate DAL component
        With New PlacementDB
            'get the dataset with all SkillGroups
            Return .DeleteExitInterview(ExitInterviewId)
        End With
    End Function
    'Public Function GetCountExitInterView(ByVal EnrollmentId As String, ByVal StudentId As String, ByVal Availabledate As String) As Integer
    '    'Instantiate DAL component
    '    With New PlacementDB
    '        'get the dataset with all SkillGroups
    '        Return .GetCountExitInterView(EnrollmentId, StudentId, Availabledate)
    '    End With
    'End Function

    Public Function GetIneligibilityReasons() As DataSet
        'Instantiate DAL component
        With New PlacementDB
            Return .GetIneligibilityReasons()
        End With
    End Function

End Class
