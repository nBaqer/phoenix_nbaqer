Public Class DailyInquiryByMonthObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim leadList As New InquiryAnalysisByDayDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(leadList.GetDailyInquiryByMonth(rptParamInfo))
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    ' Copy data from dataset to datatable.
    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim strName As String = ""
        Dim zipMask As String

        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        Try
            For Each dr As DataRow In ds.Tables(0).Rows
                dr("LeadCount") = ds.Tables(0).Rows.Count
                If Not dr.IsNull("InquiryDate") Then
                    dr("Year") = Date.Parse(dr("InquiryDate")).Year
                    dr("Month") = Date.Parse(dr("InquiryDate")).Month
                    dr("MonthLabel") = Date.Parse(dr("InquiryDate")).ToString("MMMM yyyy")
                End If

                strName = ""

                'Only Lead's MiddleName allows NULL.
                If Not dr.IsNull("MiddleName") Then
                    If dr("MiddleName") <> "" Then
                        dr("LeadName") = dr("LastName") & ", " & dr("FirstName") & _
                                            " " & dr("MiddleName") & "."
                    Else
                        dr("LeadName") = dr("LastName") & ", " & dr("FirstName")
                    End If
                Else
                    dr("LeadName") = dr("LastName") & ", " & dr("FirstName")
                End If

                'Zip allows NULL.
                If Not dr.IsNull("Zip") Then
                    If Not dr.IsNull("ForeignZip") Then
                        If dr("Zip") <> "" And dr("ForeignZip") = False Then
                            'Domestic Zip
                            dr("Zip") = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                        End If
                    Else
                        'Domestic Zip
                        If dr("Zip") <> "" Then
                            dr("Zip") = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                        End If
                    End If
                End If
            Next

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
