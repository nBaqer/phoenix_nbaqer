Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SGR_CommonObject

	Public Const NumWeeks_2Yrs As Integer = 104
	Public Const NumWeeks_4Yrs As Integer = 208
	Public Const NumWeeks_5Yrs As Integer = 260

	Public Shared Function EnrollDateFilter(ByVal RptParamInfo As ReportParamInfoIPEDS) As String
		Dim Dte As Date

		With RptParamInfo
			Select Case .CohortType
				Case CohortFullYear
					Dte = .CohortEndDate
				Case CohortFall
					Dte = .RptEndDate
			End Select

			Return "EnrollDate <= '" & FmtRptDateParam(Dte) & "'"
		End With
	End Function

	Public Shared Function Completed150Pct(ByVal StudentId As String, ByVal dvEnrollments As DataView, ByVal WeeksFilter As String, ByVal DateFilter As String) As Boolean
		Dim DateEnroll As Date, DateGrad As Date, Date150Pct As Date
		Dim i As Integer

		dvEnrollments.RowFilter = "StudentId = '" & StudentId & "' AND " & _
								  "SysStatusDescrip LIKE 'Graduated' AND " & _
								  WeeksFilter & " AND " & DateFilter

		For i = 0 To dvEnrollments.Count - 1
			DateEnroll = CDate(dvEnrollments(i)("EnrollDate"))
			DateGrad = CDate(dvEnrollments(i)("ExpGradDate"))
			Date150Pct = DateEnroll.AddDays((dvEnrollments(i)("Weeks") * 7) * 1.5)
			If Date.Compare(DateGrad, Date150Pct) < 0 Then
				Return True
			End If
		Next

		Return False

	End Function

	Public Shared Function TransferOut(ByVal StudentId As String, ByVal dvEnrollments As DataView) As Boolean
		With dvEnrollments
			.RowFilter = "StudentId = '" & StudentId & "' AND SysStatusDescrip LIKE 'Transfer Out'"
			Return .Count > 0
		End With
	End Function

	Public Shared Function ExcludedFromCohort(ByVal StudentId As String, ByVal dvEnrollments As DataView) As Boolean
		With dvEnrollments
			.RowFilter = "StudentId = '" & StudentId & "' AND " & _
						 "SysStatusDescrip LIKE 'Dropped' AND " & _
						 "DropReasonDescrip IN (" & DropReasonsExclude & ")"
			Return .Count > 0
		End With
	End Function


	Public Shared Function StillEnrolled(ByVal StudentId As String, ByVal dvEnrollments As DataView, ByVal Weeks As Integer, ByVal RptParamInfo As ReportParamInfoIPEDS) As Boolean
		Dim DateFilter As Date = CDate("8/31/" & RptParamInfo.CohortYear + (GradRates4YrYearsPrior - 1))

		With dvEnrollments
			.RowFilter = "StudentId = '" & StudentId & "' AND " & _
						 "SysStatusDescrip IN ('Enrolled','Future Start','Currently Attending') AND " & _
						 "EnrollDate <= '" & FmtRptDateParam(DateFilter) & "' AND " & _
						 "Weeks >= " & Weeks
			Return .Count > 0
		End With
	End Function

	Public Shared Function AttainedBachelors(ByVal StudentId As String, ByVal dvEnrollments As DataView, ByVal InNumYears As Integer, ByVal DateFilter As String) As Boolean
		Dim DateEnroll As Date, DateGrad As Date
		Dim i As Integer
		Dim InNumDays As Double = InNumYears * 365

		dvEnrollments.RowFilter = "StudentId = '" & StudentId & "' AND " & _
								  "SysStatusDescrip LIKE 'Graduated' AND " & _
								  "Weeks >= " & NumWeeks_4Yrs & " AND " & DateFilter

		For i = 0 To dvEnrollments.Count - 1
			DateEnroll = CDate(dvEnrollments(i)("EnrollDate"))
			DateGrad = CDate(dvEnrollments(i)("ExpGradDate"))
			If DateGrad.Subtract(DateEnroll).TotalDays <= InNumDays Then
				Return True
			End If
		Next

		Return False

	End Function

End Class
