
Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllASummaryObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SFE_AllASummary"

	Private dsRaw As New DataSet
	Private dtRptRaw As New DataTable
	Private dsRpt As New DataSet

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SFE_AllASummaryObjectDB.GetReportDatasetRaw(RptParamInfo)

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		' pre-process raw data, first process student enrollment info, then process with raw student 
		'	data to obtain prepared raw data for processing
		dtRptRaw = SFE_AllACommonObject.PreProcessRawData(dsRaw.Tables(TblNameStudents), IPEDSFacade.GetProcessedEnrollInfo(dsRaw))

		' table names for different parts of report data
		Dim TableNamePart1 As String = MainTableName & "Part1"
		Dim TableNamePart2 As String = MainTableName & "Part2"
		Dim TableNamePart3 As String = MainTableName & "Part3"
		Dim TableNamePart4 As String = MainTableName & "Part4"

		' get IPEDS Gender info
		Dim dtGenders As DataTable = IPEDSFacade.GetIPEDSGenderInfo

		' get IPEDS Ethnic info
		Dim dtEthCodes As DataTable = IPEDSFacade.GetIPEDSEthnicInfo


		' process data for report Parts 1 and 2

		' create table to hold final report data for part 1
		With dsRpt.Tables.Add(TableNamePart1).Columns
			.Add("GenderDescrip", GetType(System.String))
			.Add("EthCodeDescrip", GetType(System.String))
			.Add("Count_FirstTime", GetType(Integer))
			.Add("Count_OtherDegCert", GetType(Integer))
			.Add("Count_NonDegCert", GetType(Integer))
		End With

		' create table to hold final report data for part 2
		With dsRpt.Tables.Add(TableNamePart2).Columns
			.Add("GenderDescrip", GetType(System.String))
			.Add("EthCodeDescrip", GetType(System.String))
			.Add("Count_FirstTime", GetType(Integer))
			.Add("Count_OtherDegCert", GetType(Integer))
			.Add("Count_NonDegCert", GetType(Integer))
		End With

		' loop thru genders, and ethnic groups for each gender, and process matching records in raw dataset
		For Each drGenders As DataRow In dtGenders.Rows
			For Each drEthCodes As DataRow In dtEthCodes.Rows
				ProcessRowsParts1And2(drGenders("GenderDescrip"), drEthCodes("EthCodeDescrip"), _
									  ProgTypeUndergraduate, AttendTypeFullTime, dsRpt.Tables(TableNamePart1))
				ProcessRowsParts1And2(drGenders("GenderDescrip"), drEthCodes("EthCodeDescrip"), _
									  ProgTypeUndergraduate, AttendTypePartTime, dsRpt.Tables(TableNamePart2))
			Next
		Next


		' process data for report parts 3 and 4

		' create table to hold final report data for part 3
		With dsRpt.Tables.Add(TableNamePart3).Columns
			.Add("GenderDescrip", GetType(System.String))
			.Add("EthCodeDescrip", GetType(System.String))
			.Add("Count_FullTime", GetType(Integer))
			.Add("Count_PartTime", GetType(Integer))
		End With

		' create table to hold final report data for part 4
		With dsRpt.Tables.Add(TableNamePart4).Columns
			.Add("GenderDescrip", GetType(System.String))
			.Add("EthCodeDescrip", GetType(System.String))
			.Add("Count_FullTime", GetType(Integer))
			.Add("Count_PartTime", GetType(Integer))
		End With

		' loop thru genders, and ethnic groups for each gender, and process matching records in raw dataset
		For Each drGenders As DataRow In dtGenders.Rows
			For Each drEthCodes As DataRow In dtEthCodes.Rows
				ProcessRowsParts3And4(drGenders("GenderDescrip"), drEthCodes("EthCodeDescrip"), _
									  ProgTypeGraduate, dsRpt.Tables(TableNamePart3))
				ProcessRowsParts3And4(drGenders("GenderDescrip"), drEthCodes("EthCodeDescrip"), _
									  ProgTypeFirstProf, dsRpt.Tables(TableNamePart4))
			Next
		Next

		' return report dataset
		Return dsRpt

	End Function

	Private Sub ProcessRowsParts1And2(ByVal GenderDescrip As String, ByVal EthCodeDescrip As String, ByVal ProgTypeDescrip As String, ByVal AttendTypeDescrip As String, ByRef dtRpt As DataTable)
	' Create data for report parts 1 and 2

		' create filter according to passed in gender, ethnic code, program type, and attendance type
		Dim FilterString As String = "GenderDescrip LIKE '" & GenderDescrip & "' AND " & _
									 "EthCodeDescrip LIKE '" & EthCodeDescrip & "' AND " & _
									 "ProgTypeDescrip LIKE '" & ProgTypeDescrip & "' AND " & _
									 "AttendTypeDescrip LIKE '" & AttendTypeDescrip & "'"

		' create new summary report row and add to report DataSet
		Dim drRpt As DataRow = dtRpt.NewRow

		' set Gender and Ethnic Code descriptions
		drRpt("GenderDescrip") = GenderDescrip
		drRpt("EthCodeDescrip") = EthCodeDescrip

		' set counters for each Degree-seeking status, using filters from passed-in info
		With dtRptRaw
			drRpt("Count_FirstTime") = .Compute("COUNT(GenderDescrip)", FilterString & " AND DegCertSeekingDescrip LIKE '" & DegCertSeekingFirstTime & "'")
			drRpt("Count_OtherDegCert") = .Compute("COUNT(GenderDescrip)", FilterString & " AND DegCertSeekingDescrip LIKE '" & DegCertSeekingOtherDegree & "'")
			drRpt("Count_NonDegCert") = .Compute("COUNT(GenderDescrip)", FilterString & " AND DegCertSeekingDescrip LIKE '" & DegCertSeekingNonDegree & "'")
		End With

		dtRpt.Rows.Add(drRpt)
	End Sub

	Private Sub ProcessRowsParts3And4(ByVal GenderDescrip As String, ByVal EthCodeDescrip As String, ByVal ProgTypeDescrip As String, ByRef dtRpt As DataTable)
	' create data for report parts 3 and 4

		' create filter according to passed in gender, ethnic code, and program type
		Dim FilterString As String = "GenderDescrip LIKE '" & GenderDescrip & "' AND " & _
									 "EthCodeDescrip LIKE '" & EthCodeDescrip & "' AND " & _
									 "ProgTypeDescrip LIKE '" & ProgTypeDescrip & "'"

		' create new summary report row and add to report DataSet
		Dim drRpt As DataRow = dtRpt.NewRow

		' set Gender and Ethnic Code descriptions
		drRpt("GenderDescrip") = GenderDescrip
		drRpt("EthCodeDescrip") = EthCodeDescrip

		' set counters for each Attendance Type, using filters from passed-in info
		With dtRptRaw
			drRpt("Count_FullTime") = .Compute("COUNT(GenderDescrip)", FilterString & " AND AttendTypeDescrip LIKE '" & AttendTypeFullTime & "'")
			drRpt("Count_PartTime") = .Compute("COUNT(GenderDescrip)", FilterString & " AND AttendTypeDescrip LIKE '" & AttendTypePartTime & "'")
		End With

		dtRpt.Rows.Add(drRpt)
	End Sub
End Class