Public Class QuickLeadFacade
    Public Function GetAllRequiredStudentFields() As DataSet
        'Instantiate DAL component
        Dim ds As New DataSet
        With New QuickLead
            'get the dataset with all SkillGroups
            ds = .GetAllRequiredStudentFields()
        End With
        ds = ConcatenateDocFieldsAndRequired(ds)
        Return ds
    End Function
    Public Function ConcatenateDocFieldsAndRequired(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables(0)
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("ResDefId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows

                If Not row("FldName") Is System.DBNull.Value Then
                    If row("Required") = 1 Or row("Required") = True Then
                        row("FullName") = row("FldName") & "-" & "Required"
                    Else
                        row("FullName") = row("FldName")
                    End If
                Else
                    row("FullName") = ""
                End If
            Next
        End If
        Return ds
    End Function
    Public Function ConcatenateExistingFieldsAndRequired(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables(0)
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("FldId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows

                If Not row("LeadField") Is System.DBNull.Value Then
                    'Begin US8216 - Maintenance\Quick Leads - changing the name of campusId , AssignedDate and admincriteriaid to Lead Assigned to , Adm Rep Assigned and Admin Criteria respectively
                    If row("LeadField") = "CampusId" Then
                        row("LeadField") = "Lead Assigned to"
                    End If
                    If row("LeadField") = "AssignedDate" Then
                        row("LeadField") = "Adm Rep Assigned"
                    End If
                    If row("LeadField") = "admincriteriaid" Then
                        row("LeadField") = "Admin Criteria"
                    End If
                    If row("LeadField") = "Children" Then
                        row("LeadField") = "Dependants"
                    End If
                    'End US8216 - Maintenance\Quick Leads - changing the name of campusId , AssignedDate and admincriteriaid to Lead Assigned to , Adm Rep Assigned and Admin Criteria respectively
                    If row("Required") = 1 Or row("Required") = True Then
                        row("FullName") = row("LeadField") & "-" & "Required"
                    Else
                        row("FullName") = row("LeadField")
                    End If
                Else
                    row("FullName") = ""
                End If
            Next
        End If
        Return ds
    End Function
    Public Function GetAllNotRequiredStudentFields() As DataSet
        'Instantiate DAL component
        With New QuickLead
            'get the dataset with all SkillGroups
            Return .GetAllNotRequiredStudentFields()
        End With
    End Function
    Public Function GetAllStudentField() As DataSet
        'Instantiate DAL component
        With New QuickLead
            'get the dataset with all SkillGroups
            Return .GetAllStudentField()
        End With
    End Function
    'Public Function GetAllFieldCount() As Integer
    '    'Instantiate DAL component
    '    With New QuickLead
    '        'get the dataset with all SkillGroups
    '        Return .GetAllFieldCount()
    '    End With
    'End Function
    Public Function GetLeadCount() As Integer
        'Instantiate DAL component
        With New QuickLead
            'get the dataset with all SkillGroups
            Return .GetLeadCount()
        End With
    End Function
    Public Function GetAllFieldCount() As Integer
        'Instantiate DAL component
        With New QuickLead
            'get the dataset with all SkillGroups
            Return .GetAllFieldCount()
        End With
    End Function
    Public Function GetAllLeadFields() As DataSet
        'Instantiate DAL component
        Dim ds As New DataSet
        With New QuickLead
            'get the dataset with all SkillGroups
            ds = .GetAllLeadFields()
        End With
        ds = ConcatenateExistingFieldsAndRequired(ds)
        Return ds
    End Function
    Public Function GetAllNotRequiredLeadFields() As DataSet
        'Instantiate DAL component
        With New QuickLead
            'get the dataset with all SkillGroups
            Return .GetAllNotRequiredLeadFields()
        End With
    End Function
    Public Function UpdateModules(ByVal user As String, ByVal selectedDegrees() As String, ByVal Selectedtext() As String) As DataSet
        'Instantiate DAL component
        With New QuickLead
            'get the dataset with all SkillGroups
            Return .UpdateModules(user, selectedDegrees, Selectedtext)
        End With
    End Function
End Class
