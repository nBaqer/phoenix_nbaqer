Public Class StudentSearchFacade

    ''Modified by Saraswathi Lakshmanan on 29 Sept 2008
    ''Cohort StartDate added as optional Parameter

    Public Function StudentSearchResults(ByVal strStudentID As String, ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strStudentStatus As String, ByVal strEnrollmentID As String, ByVal strProgramID As String, ByVal strCampus As String, ByVal strStudentNumber As String, ByVal StrStatus As String, Optional ByVal leadGrpId As String = "", Optional ByVal CohortStartDate As String = "") As DataSet
        Dim ds As DataSet
        With New StudentSearchDB
            ds = .StudentSearchResults(strStudentID, strLastName, strFirstName, strSSN, strStudentStatus, strEnrollmentID, strProgramID, strCampus, strStudentNumber, StrStatus, leadGrpId, CohortStartDate)
        End With

        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        Dim zipMask As String
        Dim dr As DataRow

        'Get the mask for phone numbers and zip
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds

    End Function
    Public Function GetQuickStudentContactInfoDS(ByVal search As String, ByVal campusId As String) As DataSet
        With New StudentSearchDB
            Return .GetQuickStudentContactInfoDS(search, campusId)
        End With
    End Function
    'Public Function LeadSearchResults(ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strDOB As String, ByVal strStatus As String, ByVal strPhone As String, ByVal userId As String, ByVal campusId As String) As DataSet
    '    Dim ds As New DataSet
    '    With New StudentSearchDB
    '        ds = .LeadSearchResults(strLastName, strFirstName, strSSN, strDOB, strStatus, strPhone, userId, campusId)
    '    End With
    '    Dim facInputMasks As New InputMasksFacade
    '    Dim SSNMask As String
    '    Dim zipMask As String
    '    Dim dr As DataRow

    '    'Get the mask for phone numbers and zip
    '    SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

    '    ' Try
    '    For Each dr In ds.Tables(0).Rows
    '        If dr("SSN").ToString.Length >= 1 Then
    '            Dim temp As String = dr("SSN")
    '            dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
    '        Else
    '            dr("SSN") = ""
    '        End If
    '    Next
    '    Return ds
    'End Function
    '' Code modified by kamalesh Ahuja on 11 June 2010 to Resolve mantis issue id 18411
    Public Function LeadSearchResults(ByVal strLastName As String, ByVal strFirstName As String, ByVal strSSN As String, ByVal strDOB As String, ByVal strStatus As String, ByVal strPhone As String, ByVal userId As String, ByVal campusId As String, ByVal LeadCampuses As String, Optional ByVal HasEditOtherPerm As Boolean = False, Optional ByVal AdmissionRep As String = "") As DataSet
        Dim ds As New DataSet
        With New StudentSearchDB
            ds = .LeadSearchResults(strLastName, strFirstName, strSSN, strDOB, strStatus, strPhone, userId, campusId, LeadCampuses, HasEditOtherPerm, AdmissionRep)
        End With
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        Dim zipMask As String
        Dim dr As DataRow

        'Get the mask for phone numbers and zip
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        ' Try
        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                Dim temp As String = dr("SSN")
                If temp.Length > 5 Then
                    dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
                Else
                    dr("SSN") = String.Empty
                End If
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds
    End Function
    Public Function GetStudentNameByID(ByVal strStudentID As String) As String
        With New StudentSearchDB
            Return .GetStudentNameByID(strStudentID)
        End With
    End Function
    Public Function GetStudentNameByTransactionId(ByVal transactionID As String) As String
        With New StudentSearchDB
            Return .GetStudentNameByTransactionId(transactionID)
        End With
    End Function
    Public Function GetLeadDOB() As DataSet
        With New StudentSearchDB
            Return .GetAllLeadDOB()
        End With
    End Function
    Public Function GetLeadNameByID(ByVal strLeadID As String) As String
        With New StudentSearchDB
            Return .GetLeadNameByID(strLeadID)
        End With
    End Function
    Public Function GetAllStudentsForEmail(ByVal statusId As String, ByVal campGrpId As String, ByVal programId As String) As DataSet
        'Instantiate DAL component
        With New StudentSearchDB
            'return Dataset
            Return .GetAllStudentsForEmail(statusId, campGrpId, programId)
        End With
    End Function
    'Public Function GetAllEnrollmentsUsedByStudents() As DataSet
    '    'Instantiate DAL component
    '    With New StudentSearchDB
    '        'return Dataset
    '        Return .GetAllEnrollmentsUsedByStudents()
    '    End With
    'End Function
    Public Function GetAllEnrollmentsUsedByStudents(Optional ByVal campusId As String = "") As DataSet
        'Instantiate DAL component
        With New StudentSearchDB
            'return Dataset
            Return .GetAllEnrollmentsUsedByStudents(campusId)
        End With
    End Function
    Public Function GetAllEnrollmentsUsedByLeads() As DataSet
        'Instantiate DAL component
        'return Dataset
        Return (New LeadDB).GetAllEnrollmentsUsedByLeads
    End Function
    'Public Function GetAllStudentEmailsByStudentGroup(ByVal groupsIdList(,) As String) As DataSet
    '    'Instantiate DAL component
    '    With New StudentSearchDB
    '        'Return Dataset
    '        Return .GetAllStudentEmailsByStudentGroup(groupsIdList)
    '    End With
    'End Function
    'Public Function GetAllGroupsUsedByStudents() As DataSet
    '    'Instantiate DAL component
    '    With New StudentSearchDB
    '        'return Dataset
    '        Return .GetAllGroupsUsedByStudents()
    '    End With
    'End Function
    Public Function GetStudentSearchDSWithStudentNumber(ByVal lastName As String, ByVal firstName As String, ByVal ssn As String, ByVal enrollment As String, ByVal statusId As String, ByVal prgVerId As String, ByVal campusId As String, ByVal studentNumber As String) As DataSet
        Dim ds As New DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        Dim dr As DataRow
        With New StudentSearchDB
            ds = .GetStudentSearchDSWithStudentNumber(lastName, firstName, ssn, enrollment, statusId, prgVerId, campusId, studentNumber)
        End With
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds
    End Function
    Public Function GetStudentSearchDS(ByVal lastName As String, ByVal firstName As String, ByVal ssn As String, ByVal enrollment As String, ByVal statusId As String, ByVal prgVerId As String, ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        Dim dr As DataRow
        With New StudentSearchDB
            ds = .GetStudentSearchDS(lastName, firstName, ssn, enrollment, statusId, prgVerId, campusId)
        End With
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds
    End Function
    Public Function GetStudentDataForHeaderInfo(ByVal studentId As String, ByVal campusid As String) As StudentDataForHeaderInfo
        ' return StudentDataForHeaderInfo
        Return (New StudentSearchDB).GetStudentDataForHeaderInfo(studentId, campusid)
    End Function
    Public Function getStudentTerm(ByVal stuEnrollId As String, ByVal campusid As String) As DataTable
        'Instantiate DAL component
        With New StudentSearchDB
            'return Dataset
            Return .getStudentTerm(stuEnrollId, campusid)
        End With
    End Function
    Public Function getAllStudentTerm(ByVal stuEnrollId As String, ByVal campusid As String) As DataTable
        'Instantiate DAL component
        With New StudentSearchDB
            'return Dataset
            Return .getAllStudentTerm(stuEnrollId, campusid)
        End With
    End Function
    Public Function getStudentAllTerm(ByVal studentId As String, ByVal campusid As String) As DataTable
        'Instantiate DAL component
        With New StudentSearchDB
            'return Dataset
            Return .getStudentAllTerm(studentId, campusid)
        End With
    End Function
    ''Added by saraswathi Lakshmanan on June 10 2009 to fix issue 14829
    ''To show the inschool enrollments to nonSa users
    Public Function GetStudentSearchDSWithStudentNumberforCurrentEnrollment(ByVal lastName As String, ByVal firstName As String, ByVal ssn As String, ByVal enrollment As String, ByVal statusId As String, ByVal prgVerId As String, ByVal campusId As String, ByVal studentNumber As String) As DataSet
        Dim ds As New DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        Dim dr As DataRow
        Dim dsInschool As New DataSet
        Dim dsOutofSchool As New DataSet

        With New StudentSearchDB
            ds = .GetStudentSearchDSWithStudentNumberforcurrentEnrollment(lastName, firstName, ssn, enrollment, statusId, prgVerId, campusId, studentNumber)

            ''  dsOutofSchool = .GetStudentSearchDSWithStudentNumberforPastEnrollment(lastName, firstName, ssn, enrollment, statusId, prgVerId, campusId, studentNumber)

        End With
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        ''   ds = JoinTwoDatasets(dsInschool, dsOutofSchool)
        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds
    End Function
    ''Added by Saraswathi lakshmanan on June 11 2009
    ''To get the InschoolEnrollments Only
    Public Function GetStudentSearchDSForInSchoolEnrollment(ByVal lastName As String, ByVal firstName As String, ByVal ssn As String, ByVal enrollment As String, ByVal statusId As String, ByVal prgVerId As String, ByVal campusId As String) As DataSet

        ''Modified By Saraswarthi lakshmanan on August 11 2009
        Dim dsInschool As New DataSet
        Dim dsoutofSchool As New DataSet

        Dim ds As New DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        Dim dr As DataRow
        With New StudentSearchDB
            ds = .GetStudentSearchDSForInSchoolEnrollment(lastName, firstName, ssn, enrollment, statusId, prgVerId, campusId)
            ''  dsoutofSchool = .GetStudentSearchDSForOutofSchoolEnrollment(lastName, firstName, ssn, enrollment, statusId, prgVerId, campusId)
        End With
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        ''   ds = JoinTwoDatasets(dsInschool, dsoutofSchool)
        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds
    End Function

    'Public Function JoinTwoDatasets(ByVal dsInSchool As DataSet, ByVal dsoutofSchool As DataSet) As DataSet
    '    If dsInSchool.Tables.Count > 0 Then
    '        If dsoutofSchool.Tables.Count > 0 Then
    '            Dim ItableCount As Integer
    '            For ItableCount = 0 To dsInSchool.Tables.Count - 1
    '                If dsInSchool.Tables(ItableCount).Columns.Contains("StudentID") = True Then
    '                    Dim dr As DataRow
    '                    For Each dr In dsInSchool.Tables("Students").Rows
    '                        ''If InSchool Status is available then remove the records of out of school status from the second dataset i.e. Outof School
    '                        If dsoutofSchool.Tables("Students").TableName = "Students" Then
    '                            If dsoutofSchool.Tables("Students").Rows.Find(dr("StudentID")) IsNot Nothing Then
    '                                Dim drStudentStudentID As DataRow
    '                                drStudentStudentID = dsoutofSchool.Tables("Students").Rows.Find(dr("StudentID"))
    '                                'If drStudentStudentID IsNot Nothing Then
    '                                '    Dim drStudentEnrStudentID() As DataRow
    '                                '    drStudentEnrStudentID = dsoutofSchool.Tables("StudentEnrollments").Select("StudentID='" + drStudentStudentID("StudentID").ToString + "'")
    '                                '    Dim Istudenenrollmentrowcount As Integer
    '                                '    For Istudenenrollmentrowcount = 0 To drStudentEnrStudentID.Length - 1
    '                                '        Dim drEnrlStuEnroll() As DataRow
    '                                '        drEnrlStuEnroll = dsoutofSchool.Tables("Enrollments").Select("PrgVerID='" + drStudentEnrStudentID(Istudenenrollmentrowcount)("PrgVerID").ToString + "'")
    '                                '        ''Delete the Enrollment Tables Program Versions
    '                                '        Dim iEnrollmentRowCount As Integer
    '                                '        For iEnrollmentRowCount = 0 To drEnrlStuEnroll.Length - 1
    '                                '            dsoutofSchool.Tables("Enrollments").Rows.Remove(drEnrlStuEnroll(iEnrollmentRowCount))
    '                                '        Next
    '                                '    Next



    '                                'End If



    '                                dsoutofSchool.Tables("StudentEnrollMents").Rows.Remove(drStudentStudentID)
    '                                'dsoutofSchool.Relations("StudentsStudentEnrollments").ChildTable.Rows.Remove(dsoutofSchool.Tables("StudentEnrollments").Select("StudentID='" + dr("StudentID").ToString+"'")
    '                            End If
    '                        End If
    '                    Next
    '                    dsoutofSchool.AcceptChanges()

    '                    For Each droutofSchool As DataRow In dsoutofSchool.Tables("Students").Rows
    '                        If droutofSchool IsNot Nothing Then
    '                            Dim drStudentEnrStudentID() As DataRow
    '                            drStudentEnrStudentID = dsoutofSchool.Tables("StudentEnrollments").Select("StudentID='" + drStudentStudentID("StudentID").ToString + "'")
    '                            Dim Istudenenrollmentrowcount As Integer
    '                            For Istudenenrollmentrowcount = 0 To drStudentEnrStudentID.Length - 1
    '                                Dim drEnrlStuEnroll() As DataRow
    '                                drEnrlStuEnroll = dsoutofSchool.Tables("Enrollments").Select("PrgVerID='" + drStudentEnrStudentID(Istudenenrollmentrowcount)("PrgVerID").ToString + "'")
    '                                ''Delete the Enrollment Tables Program Versions
    '                                Dim iEnrollmentRowCount As Integer
    '                                For iEnrollmentRowCount = 0 To drEnrlStuEnroll.Length - 1
    '                                    dsoutofSchool.Tables("Enrollments").Rows.Remove(drEnrlStuEnroll(iEnrollmentRowCount))
    '                                Next
    '                            Next



    '                        End If

    '                        dsInSchool.Tables("Students").ImportRow(droutofSchool)
    ' dsInSchool.Relations("StudentStudentEnrollments").ChildTable.Rows.Add(dsInSchool.Tables("Students").

    '                    Next




    '                    ''If Student in out of School Status is not available in the inschool Status dataset then get only the recent most from the out of School Status and remove the others
    '                    'For Each droutofSchool As DataRow In dsoutofSchool.Tables(ItableCount).Rows
    '                    '    If dsoutofSchool.Tables(ItableCount).TableName = "Students" Then
    '                    '        If dsInSchool.Tables(ItableCount).Rows.Find(droutofSchool("StudentID")) Is Nothing Then
    '                    '            Dim DataRowCollectOutofSchool() As DataRow
    '                    '            If dsoutofSchool.Tables(ItableCount).Columns.Contains("StartDate") = True Then
    '                    '                DataRowCollectOutofSchool = dsoutofSchool.Tables(ItableCount).Select("StudentID='" + droutofSchool("StudentID").ToString + "'", "StartDate desc")

    '                    '            Else
    '                    '                DataRowCollectOutofSchool = dsoutofSchool.Tables(ItableCount).Select("StudentID='" + droutofSchool("StudentID").ToString + "' ")
    '                    '            End If
    '                    '            If DataRowCollectOutofSchool.Length > 0 Then
    '                    '                Dim datarowCollectionEnrollments() As DataRow
    '                    '                dsInSchool.Tables(ItableCount).ImportRow(DataRowCollectOutofSchool(0))
    '                    '                dsInSchool.AcceptChanges()
    '                    '                datarowCollectionEnrollments = dsoutofSchool.Tables("StudentEnrollments").Select("StudentID='" + droutofSchool("StudentID").ToString + "'", "StartDate desc")
    '                    '                If datarowCollectionEnrollments.Length > 0 Then
    '                    '                    Dim datarowCollectionprgversion() As DataRow
    '                    '                    datarowCollectionprgversion = dsoutofSchool.Tables("Enrollments").Select("PrgVerID='" + datarowCollectionEnrollments("PrgVerID").ToString + "'", "StartDate desc")


    '                    '                    'dsInSchool.Tables("StudentEnrollments").ImportRow(datarowCollectionEnrollments(0))
    '                    '                End If

    '                    '            End If
    '                    '        End If
    '                    '    End If

    '                    'Next
    '                    dsInSchool.AcceptChanges()
    '                End If
    '            Next
    '        End If
    '    End If
    '    Return dsInSchool
    'End Function

    ''Added by Saraswathi Laksshmanan 
    ''To find if the user is a system administrator
    ''for mantis issue 17126
    Public Function GetIsRoleSystemAdministarator(ByVal UserId As String) As Boolean
        Dim stusearchDB As New StudentSearchDB
        Return stusearchDB.GetIsRoleSystemAdministarator(UserId)
    End Function
    Public Function GetStudentSearchDSWithStudentNumberMRU(ByVal dsMRU As DataSet, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        Dim dr As DataRow
        With New StudentSearchDB
            ds = .GetStudentSearchDSWithStudentNumberMRU(dsMRU, CampusId)
        End With
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds
    End Function
    Public Function GetStudentSearchDSWithStudentNumberforCurrentEnrollmentMRU(ByVal dsMRU As DataSet, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        Dim dr As DataRow
        Dim dsInschool As New DataSet
        Dim dsOutofSchool As New DataSet

        With New StudentSearchDB
            ds = .GetStudentSearchDSWithStudentNumberforcurrentEnrollmentMRU(dsMRU, CampusId)
        End With
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds
    End Function
    Public Function GetStudentSearchDSMRU(ByVal dsMRU As DataSet, ByVal campusId As String) As DataSet
        Dim ds As DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim ssnMask As String
        Dim dr As DataRow
        With New StudentSearchDB
            ds = .GetStudentSearchDSMRU(dsMRU, CampusId)
        End With
        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(ssnMask, "*****" & temp.Substring(5))
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds

    End Function
    Public Function GetStudentSearchDSForInSchoolEnrollmentMRU(ByVal dsMRU As DataSet, ByVal CampusId As String) As DataSet

        ''Modified By Saraswarthi lakshmanan on August 11 2009
        Dim dsInschool As New DataSet
        Dim dsoutofSchool As New DataSet

        Dim ds As New DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        Dim dr As DataRow
        With New StudentSearchDB
            ds = .GetStudentSearchDSForInSchoolEnrollmentMRU(dsMRU, CampusId)
            ''  dsoutofSchool = .GetStudentSearchDSForOutofSchoolEnrollment(lastName, firstName, ssn, enrollment, statusId, prgVerId, campusId)
        End With
        SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        ''   ds = JoinTwoDatasets(dsInschool, dsoutofSchool)
        For Each dr In ds.Tables(0).Rows
            If dr("SSN").ToString.Length >= 1 Then
                Dim temp As String = dr("SSN")
                dr("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
                'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
            Else
                dr("SSN") = ""
            End If
        Next
        Return ds
    End Function

    ''' <summary>
    ''' Get all enrollment per Student
    ''' </summary>
    ''' <param name="studentId">The student Id</param>
    ''' <returns>The list of Enrollment per Student, plus a All Enrollment record.</returns>
    ''' <remarks>
    ''' EnrollmentItemsDto return with always StuEnrollId as Guid.empty
    ''' </remarks>
    Public Shared Function GetAllEnrollmentsUsedByStudentsList(ByVal studentId As String) As Object
        Dim list As List(Of EnrollmentItemsDto) = New List(Of EnrollmentItemsDto)()
        Dim facade As StudentSearchFacade = New StudentSearchFacade()
        ' Get data from DB
        Dim ds As DataSet = facade.GetAllEnrollmentsUsedByStudents(studentId)
        Dim dt As DataTable = ds.Tables(0)
        For Each row As DataRow In dt.Rows
            Dim dto As New EnrollmentItemsDto()
            dto.PrgVerDescrip = row("PrgVerDescrip").ToString()
            dto.PrgVerId = row("PrgVerId").ToString()
            dto.StuEnrollId = Guid.Empty.ToString()
            list.Add(dto)
        Next

        'Add All Enrollments
        If list.Count > 1 Then
            Dim dto As New EnrollmentItemsDto()
            dto.StuEnrollId = Guid.Empty.ToString()
            dto.PrgVerId = Guid.Empty.ToString()
            dto.PrgVerDescrip = "All Enrollments"
            list.Add(dto)
        End If

        Return list
    End Function

End Class
