Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.AdvantageV1.BusinessFacade.FIC_C3CommonObject

Public Class FIC_C3SummaryObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "FIC_C3Summary"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = FIC_C3SummaryObjectDB.GetReportDatasetRaw(RptParamInfo)

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add("Count_Male_Appl", GetType(System.Int32))
			.Add("Count_Male_Admit", GetType(System.Int32))
			.Add("Count_Male_FT", GetType(System.Int32))
			.Add("Count_Male_PT", GetType(System.Int32))
			.Add("Count_Female_Appl", GetType(System.Int32))
			.Add("Count_Female_Admit", GetType(System.Int32))
			.Add("Count_Female_FT", GetType(System.Int32))
			.Add("Count_Female_PT", GetType(System.Int32))
		End With

		Dim drLead As DataRow
		Dim drRpt As DataRow
		Dim ColPrefix As String
		Dim RowPtr As Integer

		' pre-process raw data to obtain prepared raw data for processing, create DataView to 
		'	use in processing
		Dim dvStuEnrollInfo As New DataView(PreProcessRawData(dsRaw))

		drRpt = dsRpt.Tables(MainTableName).NewRow
		drRpt("Count_Male_Appl") = 0
		drRpt("Count_Male_Admit") = 0
		drRpt("Count_Male_FT") = 0
		drRpt("Count_Male_PT") = 0
		drRpt("Count_Female_Appl") = 0
		drRpt("Count_Female_Admit") = 0
		drRpt("Count_Female_FT") = 0
		drRpt("Count_Female_PT") = 0

		' loop thru Leads in raw dataset and process each
		For Each drLead In dsRaw.Tables(TblNameLeads).Rows
			' determine gender of current Lead, set appropriate prefix to use when referring to 
			'	counting columns in report DataRow
			If drLead("GenderDescrip").ToString.ToLower <> "women" Then
				ColPrefix = "Count_Male_"
			Else
				ColPrefix = "Count_Female_"
			End If

			' count Lead is an applicant 
			drRpt(ColPrefix & "Appl") += 1

			' if a record in Enrollment info contains current LeadId, get report info from that record
			dvStuEnrollInfo.Sort = "LeadId"
			RowPtr = dvStuEnrollInfo.Find(drLead("LeadId"))
			If RowPtr <> -1 Then
				' count Lead as admitted 
				drRpt(ColPrefix & "Admit") += 1
				' count Lead as either Full- or Part-time
				If dvStuEnrollInfo(RowPtr)("AttendTypeDescrip") = AttendTypeFullTime Then
					drRpt(ColPrefix & "FT") += 1
				Else
					drRpt(ColPrefix & "PT") += 1
				End If

			' if no record in Enrollment info contains current LeadId, see if current Lead has an SSN
			ElseIf drLead("SSN").ToString <> "" Then
				' if current Lead has an SSN, and a record in Enrollment info contains current Lead's SSN, 
				'	get report info from that record 
				dvStuEnrollInfo.Sort = "SSN"
				RowPtr = dvStuEnrollInfo.Find(drLead("SSN"))
				If RowPtr <> -1 Then
					' count Lead as admitted 
					drRpt(ColPrefix & "Admit") += 1
					' count Lead as either Full- or Part-time
					If dvStuEnrollInfo(RowPtr)("AttendTypeDescrip") = AttendTypeFullTime Then
						drRpt(ColPrefix & "FT") += 1
					Else
						drRpt(ColPrefix & "PT") += 1
					End If
				End If

			End If

		Next

		dsRpt.Tables(MainTableName).Rows.Add(drRpt)

		' return the Report DataSet
		Return dsRpt

	End Function

End Class
