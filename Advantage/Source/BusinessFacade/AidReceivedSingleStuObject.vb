Public Class AidReceivedSingleStuObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim aidRec As New AidReceivedDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(aidRec.GetAidReceivedBySingleStudent(rptParamInfo))
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region

#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet

        Try
            If ds.Tables.Count = 1 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If Not (dr("AwardStartDate") Is System.DBNull.Value) And Not (dr("AwardEndDate") Is System.DBNull.Value) Then
                            If dr("AwardStartDate").ToString <> "" And dr("AwardEndDate").ToString <> "" Then
                                dr("DatePeriod") = dr("AwardStartDate").ToShortDateString & " - " & dr("AwardEndDate").ToShortDateString
                            End If
                        End If
                    Next

                End If
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region


End Class
