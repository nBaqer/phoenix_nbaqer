Public Class MissingItemsFacade

#Region "Private Members"

    Private Enum SysDocStatus
        Approved = 1
        NotApproved = 2
    End Enum

#End Region

    Public Function GetStudentListByAdReqs(ByVal campusId As String, ByVal prgVerId As String, _
                                            ByVal studentGrpId As String, _
                                            ByVal shiftId As String, _
                                            ByVal statusCodeId As String) As DataTable
        Dim objDB As New AdReqsDB
        Dim facInputMasks As New InputMasksFacade
        Dim stuName As String
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Dim dt As DataTable = objDB.GetStudentListByAdReqs(campusId, prgVerId, studentGrpId, shiftId, statusCodeId)

        'Add calculated column: '(PrgVerCode) PrgVerDescrip'
        Dim dc As DataColumn = dt.Columns.Add("PrgVerCodeDescrip", Type.GetType("System.String"))
        dc.AllowDBNull = True
        dc.Expression = "'(' + PrgVerCode + ')' + PrgVerDescrip"

        'Massage table
        For Each dr As DataRow In dt.Rows
            'Set up student name as: "LastName, FirstName MI."
            stuName = dr("LastName")
            If Not dr.IsNull("FirstName") Then
                If dr("FirstName") <> "" Then
                    stuName &= ", " & dr("FirstName")
                End If
            End If
            If Not dr.IsNull("MiddleName") Then
                If dr("MiddleName") <> "" Then
                    stuName &= " " & dr("MiddleName") & "."
                End If
            End If
            dr("StudentName") = stuName

            'Apply mask to SSN.
            If Not dr.IsNull("SSN") Then
                If dr("SSN") <> "" Then
                    'dr("SSN") = facInputMasks.ApplyMask(strSSNMask, dr("SSN"))
                    Dim temp As String = dr("SSN")
                    dr("SSN") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                End If
            End If
        Next

        Return dt
    End Function

    Public Function GetRequirementsByProgramVersionAndMandatories(ByVal campusId As String, ByVal prgVerId As String, ByVal studentGrpId As String) As DataTable
        Return (New AdReqsDB).GetRequirementsByProgramVersionAndMandatories(campusId, prgVerId, studentGrpId)
    End Function

    Public Function GetReqGroupsByProgramVersion(ByVal campusId As String, ByVal prgVerId As String, ByVal studentGrpId As String) As DataTable
        Return (New AdReqsDB).GetReqGroupsByProgramVersion(campusId, prgVerId, studentGrpId)
    End Function

    Public Function GetStudentRequirements(ByVal campusId As String, ByVal prgVerId As String, _
                                            ByVal studentGrpId As String, _
                                            ByVal shiftId As String, _
                                            ByVal statusCodeId As String) As DataTable
        Return (New AdReqsDB).GetStudentRequirements(campusId, prgVerId, studentGrpId, shiftId, statusCodeId)
    End Function
    Public Function GetStudentTestRequirements(ByVal campusId As String, ByVal prgVerId As String, _
                                        ByVal studentGrpId As String, _
                                        ByVal shiftId As String, _
                                        ByVal statusCodeId As String) As DataTable
        Return (New AdReqsDB).GetStudentTestRequirements(campusId, prgVerId, studentGrpId, shiftId, statusCodeId)
    End Function

    Public Function GetDropDownLists(ByVal campusId As String) As DataSet
        Return (New AdReqsDB).GetMissingItemsDropDownLists(campusId)
    End Function

    Public Function AreReqGroupsMet(ByVal stuEnrollId As String, ByVal reqGrpId As String) As Boolean
        Return (New AdReqsDB).AreReqGroupsMet(stuEnrollId, reqGrpId)
    End Function

    Public Function GetReqGroupsList(ByVal dt As DataTable) As String
        Dim strList As String = ""
        For Each dr As DataRow In dt.Rows
            If Not dr.IsNull("ReqGrpId") Then
                strList &= "'" & dr("ReqGrpId").ToString & "',"
            End If
        Next
        If strList <> "" Then
            strList = strList.Remove(strList.Length - 1, 1)
        Else
            strList &= "'" & System.Guid.Empty.ToString & "'"
        End If
        Return strList
    End Function
    Public Function GetList(ByVal dt As DataTable, ByVal lookupFld As String) As String
        Dim strList As String = ""

        If lookupFld <> "" Then
            For Each dr As DataRow In dt.Rows
                If Not dr.IsNull(lookupFld) Then
                    strList &= "'" & dr(lookupFld).ToString & "',"
                End If
            Next
            If strList <> "" Then
                strList = strList.Remove(strList.Length - 1, 1)
            End If
        End If

        Return strList
    End Function

    Public Function GetAttemptedReqsFromReqGroupByStudentId(ByVal campusId As String, ByVal progId As String, _
                                            ByVal studentGrpId As String, _
                                            ByVal shiftId As String, _
                                            ByVal statusCodeId As String, _
                                            ByVal dtReqGrps As DataTable) As DataTable
        Dim objDB As New AdReqsDB
        Dim dt As New DataTable
        Dim dtTemp As DataTable
        Dim dtTtl As DataTable
        Dim rr As DataRow
        Dim rr2 As DataRow

        'Create new table to hold the total number of attempted ReqGrpId's requirements, documents And tests
        With dt
            .TableName = "ResReqGrps"
            .Columns.Add("StudentId", GetType(System.String))
            .Columns.Add("ReqGrpId", GetType(System.String))
            .Columns.Add("Descrip", GetType(System.String))
            .Columns.Add("NumReqs", GetType(System.Int32))
            .Columns.Add("DocTestFromReqGrpAttempted", GetType(System.Int32))
        End With


        If dtReqGrps.Rows.Count > 0 Then

            'Iterate for every ReqGrpId
            For Each dr As DataRow In dtReqGrps.Rows

                'Retrieve students that have attempted requirements belonging to a particular ReqGrpId
                dtTemp = objDB.GetAttemptedReqsFromReqGroupByStudentId(campusId, progId, studentGrpId, shiftId, statusCodeId, dr("ReqGrpId").ToString)

                If dtTemp.Rows.Count > 0 Then
                    'Retrieve the number of Documents or Test attempted
                    dtTtl = objDB.GetDocTestFromReqGrpAttempted(studentGrpId, dr("ReqGrpId").ToString, GetList(dtTemp, "StudentId"))

                    For Each row As DataRow In dtTemp.Rows
                        'Create a row
                        rr = dt.NewRow
                        rr("StudentId") = row("StudentId").ToString
                        rr("ReqGrpId") = dr("ReqGrpId").ToString
                        rr("Descrip") = row("Descrip")
                        rr("NumReqs") = row("NumReqs")

                        If dtTtl.Rows.Count > 0 Then
                            'Total number of attempted ReqGrpId's requirements (Documents And Tests)
                            rr2 = dtTtl.Rows.Find(row("StudentId").ToString)
                            If Not (rr2 Is Nothing) Then
                                rr("DocTestFromReqGrpAttempted") = rr2("ReqsFromRGAttempted")
                            Else
                                'Student has not attempted any of the requirements of this ReqGrpId
                                rr("DocTestFromReqGrpAttempted") = 0
                            End If
                        Else
                            rr("DocTestFromReqGrpAttempted") = 0
                        End If

                        dt.Rows.Add(rr)
                    Next
                End If


            Next

        End If

        Return dt
    End Function

End Class
