
Imports System.Web
Imports System.Data
Imports FAME.Advantage.Common

Public Class MRUFacade

    Private m_Context As HttpContext
    Private m_DataSet As DataSet


    Public Function LoadMRU(ByVal strType As String, ByVal userId As String, Optional ByVal campusId As String = Nothing, Optional ByVal studentGrpId As String = "", Optional ByVal restrictSearchByStudentGroup As String = "False") As DataSet
        Dim dbMRU As New MRUDB
        Dim ds As New DataSet

        ds = dbMRU.LoadMRU(strType, userId, campusId, studentGrpId, restrictSearchByStudentGroup)
        'If we are dealing with students and employees we will need to
        'concatenate the first and last names.

        Return ds
    End Function

    Public Function LoadAndUpdateMRU(ByVal strType As String, ByVal strID As String, ByVal userId As String, Optional ByVal campusId As String = Nothing, Optional ByVal studentGrpId As String = "", Optional ByVal restrictSearchByStudentGroup As String = "False") As DataSet
        Dim objMRU As New MRUDB
        Dim dRow As DataRow
        Dim blnFound As Boolean

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        m_DataSet = LoadMRU(strType, userId, campusId, studentGrpId, restrictSearchByStudentGroup)

        'Check to see if the id passed in is in the datatable. Remember that
        'this id could be for an employer, student or prospect etc.
        For Each dRow In m_DataSet.Tables("MRUList").Rows
            If dRow("ChildId").ToString().ToUpper = strID.ToUpper Then
                blnFound = True
                Exit For
            End If
        Next

        'When a matching row is found it means that the type is already in the
        'list and so we don't need to do anything. If no matching row is found
        'then we first need to determine how many items are in the list. If less
        'than the configured amount we can simply insert the item into the database and update 
        'the dataset. However, if we are at the limit for items in the list we need to
        'first remove the item with the lowest counter value and then insert the 
        'new item. We also have to make certain that the dataset is synchronized
        'with the database.
        If blnFound = False Then
            Try
                If m_DataSet.Tables("MRUList").Rows.Count >= CInt(MyAdvAppSettings.AppSettings("MRULimit")) Then
                    'Delete the row with lowest counter from the database. 
                    objMRU.DeleteLRUItem(strType, userId, HttpContext.Current.Request.Params("cmpid"))
                End If
                'We always need to do an insert if no matching row was found.
                objMRU.AddMRUItem(strType, strID, userId, HttpContext.Current.Request.Params("cmpid"))

                'Reload the dataset with updated rows from database
                m_DataSet = LoadMRU(strType, userId, HttpContext.Current.Request.Params("cmpid"))
            Catch ex As System.Exception
                If ex.InnerException Is Nothing Then
                    Throw New System.Exception(ex.Message.ToString)
                Else
                    Throw New System.Exception(ex.InnerException.ToString)
                End If

            End Try
        End If

        Return m_DataSet
    End Function

    Public Function DoesResourceHasMRU(ByVal resourceID As Integer) As Boolean
        Dim dbMRU As New MRUDB

        Return dbMRU.DoesResourceHasMRU(resourceID)
    End Function

    Public Function GetResourceMRUTypeId(ByVal resourceID As Integer) As Integer
        Dim dbMRU As New MRUDB

        Return dbMRU.GetResourceMRUTypeId(resourceID)
    End Function
    ' US3054 4/17/2012 Janet Robinson switch MRU return to match left MRU bar for Students
    Public Function LoadMRUForStudentSearch(ByVal userId As String, ByVal campusId As String) As DataSet
        Dim dbMRU As New MRUDB
        Dim ds As DataSet

        ds = dbMRU.LoadMRUForStudentSearch(userId, campusId)

        Return ds
    End Function


    Public Shared Function GetShadowLead(studentGuidStr As String) As Guid
        Dim dbMRU As New MRUDB
        Dim slead = dbMRU.GetShadowLead(studentGuidStr)
        Return slead
    End Function
End Class
