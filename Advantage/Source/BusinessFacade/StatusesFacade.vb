' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' StatusesFacade.vb
'
' StatusesFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================

Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class StatusesFacade
	Public Function GetAllStatuses() As DataSet

		'   Instantiate DAL component
		Dim statusesDB As New StatusesDB

		'   get the dataset with all Statuses
		Return statusesDB.GetAllStatuses()

	End Function
	Public Function GetAllPrograms() As DataSet

		'   Instantiate DAL component
		Dim statusesDB As New StatusesDB

		'   get the dataset with all Statuses
		Return statusesDB.GetAllPrograms()

	End Function
    'Public Function GetAllProgramVersions() As DataSet
    '	'   Instantiate DAL component
    '	Dim statusesDB As New StatusesDB
    '	'   get the dataset with all Statuses
    '	Return statusesDB.GetAllPrgVerDescrip
    'End Function
    Public Function GetAllProgramVersions(Optional ByVal campusId As String = "") As DataSet
        '   Instantiate DAL component
        Dim statusesDB As New StatusesDB
        '   get the dataset with all Statuses
        Return statusesDB.GetAllPrgVerDescrip(campusId)
    End Function

	Public Function GetAllSysDocStatuses() As DataSet

		'   Instantiate DAL component
		Dim statusesDB As New StatusesDB

		'   get the dataset with all Statuses
		Return statusesDB.GetAllSysDocStatuses()

	End Function
	Public Function GetAllDataTypes() As DataSet

		'   Instantiate DAL component
		Dim statusesDB As New StatusesDB

		'   get the dataset with all Statuses
		Return statusesDB.GetAllDataTypes()

	End Function

	Public Shared Function GetStatesIPEDS() As DataTable
		Dim dtStates As DataTable = StatesDB.GetStatesIPEDS()
		Dim dtStatesSorted As DataTable
		Dim drState As DataRow
		Dim i As Integer
		Dim rowsSorted() As DataRow

		With dtStates
			drState = .NewRow
			drState("StateDescrip") = StateUnknownDescrip
			drState("FIPSCode") = StateUnknownFIPSCode
			.Rows.Add(drState)

			drState = .NewRow
			drState("StateDescrip") = StateForeignCountryDescrip
			drState("FIPSCode") = StateForeignCountryFIPSCode
			.Rows.Add(drState)

			drState = .NewRow
			drState("StateDescrip") = StateResUnreportedDescrip
			drState("FIPSCode") = StateResUnreportedFIPSCode
			.Rows.Add(drState)

			.Columns.Add("SortGroup", GetType(Integer))
			For i = 0 To dtStates.Rows.Count - 1
				.Rows(i)("SortGroup") = StateRptSortGroup(.Rows(i)("StateDescrip"))
			Next
		End With

		rowsSorted = dtStates.Select("", "SortGroup, StateDescrip")
		dtStatesSorted = dtStates.Copy
		dtStatesSorted.Rows.Clear()
		For i = 0 To rowsSorted.GetUpperBound(0)
			drState = dtStatesSorted.NewRow
			drState("StateId") = rowsSorted(i)("StateId")
			drState("StateDescrip") = rowsSorted(i)("StateDescrip")
			drState("FIPSCode") = rowsSorted(i)("FIPSCode")
			drState("SortGroup") = rowsSorted(i)("SortGroup")
			dtStatesSorted.Rows.Add(drState)
		Next

		Return dtStatesSorted

	End Function

End Class
