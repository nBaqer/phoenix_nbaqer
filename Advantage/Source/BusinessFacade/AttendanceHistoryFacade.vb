Public Class AttendanceHistoryFacade
    Inherits BaseReportFacade

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim GradebookResultsObj As New GradebookResultsDB
        Dim ds As DataSet
        ' Massage dataset to produce one with the columns and data expected by report.
        'ds = BuildReportSource(GradebookResultsObj.GradebookResults(rptParamInfo), GradebookResultsObj.StudentIdentifier, rptParamInfo.ResId, rptParamInfo.FilterOtherString.Trim)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        ds = BuildDataForReport(rptParamInfo)
        Return ds
    End Function
    Public Function BuildDataForReport(ByVal ParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim dtReturnResult As New DataTable
        Dim ds As DataSet
        ds = (New AttendanceHistoryDB).GetAttendanceSummary(ParamInfo)
        'ds = (New AttendanceHistoryDB).GetCompletedAndExternshipSummary(ParamInfo)
        Return ds
    End Function
    Public Function GetAttendanceDate(ByVal StuEnrollId As String, ByVal StartDate As String) As DataTable
        Dim dt As DataTable
        dt = (New AttendanceHistoryDB).GetAttendanceDates(StuEnrollId, StartDate)
        Return dt
    End Function
    Public Function GetCommentsByDate(ByVal StuEnrollId As String, ByVal dtDate As String) As String
        Dim strComments As String
        strComments = (New AttendanceHistoryDB).GetCommentsByDate(StuEnrollId, dtDate)
        Return strComments
    End Function
    Public Function UpdateComments(ByVal StuEnrollId As String, ByVal RecordDate As DateTime, ByVal Comments As String) As String
        Dim strComments As String
        strComments = (New AttendanceHistoryDB).UpdateComments(StuEnrollId, RecordDate, Comments)
        Return strComments
    End Function
    Public Function GetStudentNames() As DataTable
        Dim dt As DataTable
        dt = (New AttendanceHistoryDB).GetStudentNames()
        Return dt
    End Function
    Public Function GetAttendanceDateByStudent(ByVal StuEnrollId As String) As DataTable
        Dim dt As DataTable
        dt = (New AttendanceHistoryDB).GetAttendanceDateByStudent(StuEnrollId)
        Return dt
    End Function
    Public Function GetExternshipAttendance(ByVal StuEnrollId As String) As DataTable
        Dim dt As DataTable
        dt = (New AttendanceHistoryDB).GetExternshipAttendanceDays(StuEnrollId)
        Return dt
    End Function
    Public Function getPrograms(ByVal progid As String) As DataSet
        Return (New AttendanceHistoryDB).getPrograms(progid)
    End Function
    '' New Code Added By Vijay Ramteke On October 22, 2010 For Rally Id DE1007
    Public Function GetStudentNames(ByVal ProgId As String, ByVal PrgVerId As String, ByVal StuName As String, ByVal StuStatus As String, ByVal StartDate As String, ByVal EndDate As String, ByVal UseTimeClock As String, ByVal BadgeNum As String, Optional ByVal campusId As String = "", Optional ByVal Studentgrpid As String = "", Optional ByVal CohortStartDate As String = "", Optional ByVal outofschoolfilter As String = "") As DataTable
        Dim dt As New DataTable
        dt = (New AttendanceHistoryDB).GetStudentNames(ProgId, PrgVerId, StuName, StuStatus, StartDate, EndDate, UseTimeClock, BadgeNum, campusId, Studentgrpid, CohortStartDate, outofschoolfilter)
        Return dt
    End Function
    '' New Code Added By Vijay Ramteke On October 22, 2010 For Rally Id DE1007
End Class
