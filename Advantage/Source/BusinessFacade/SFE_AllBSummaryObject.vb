
Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllBSummaryObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SFE_AllBSummary"

	Private dsRaw As New DataSet
	Private dtRptRaw As New DataTable
	Private dsRpt As New DataSet
    Private dteAgeAsOf As DateTime

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SFE_AllBSummaryObjectDB.GetReportDataSetRaw(RptParamInfo)

        ' set date on which to calculate students' ages
        dteAgeAsOf = RptParamInfo.RptEndDate

        ' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		' pre-process raw data, first process student enrollment info, then process with raw student 
		'	data to obtain prepared raw data for processing
        dtRptRaw = SFE_AllBCommonObject.PreProcessRawData(dsRaw.Tables(TblNameStudents), IPEDSFacade.GetProcessedEnrollInfo(dsRaw), dteAgeAsOf)

		' get IPEDS Age Categories
		Dim dtAgeCatInfo As DataTable = IPEDSFacade.GetIPEDSAgeCatInfo
		Dim drAgeCatInfo As DataRow

		' create tables to hold final report data
		For i As Integer = 1 To SFE_AllBCommonObject.NumRptParts
			With dsRpt.Tables.Add(MainTableName & "Part" & i).Columns
				.Add("AgeDescrip", GetType(System.String))
				.Add("Count_Male", GetType(System.Int32))
				.Add("Count_Female", GetType(System.Int32))
			End With
		Next

		' for each Program and Attendance type, loop thru age categories, and process rows for each age category

		' process age categories for full time undergrads
		For Each drAgeCatInfo In dtAgeCatInfo.Rows
			ProcessRows(drAgeCatInfo, ProgTypeUndergraduate, AttendTypeFullTime, dsRpt.Tables(MainTableName & "Part1"))
		Next

		' process age categories for part time undergrads
		For Each drAgeCatInfo In dtAgeCatInfo.Rows
			ProcessRows(drAgeCatInfo, ProgTypeUndergraduate, AttendTypePartTime, dsRpt.Tables(MainTableName & "Part2"))
		Next

		' process age categories for full time graduates
		For Each drAgeCatInfo In dtAgeCatInfo.Rows
			ProcessRows(drAgeCatInfo, ProgTypeGraduate, AttendTypeFullTime, dsRpt.Tables(MainTableName & "Part3"))
		Next

		' process age categories for part time graduates
		For Each drAgeCatInfo In dtAgeCatInfo.Rows
			ProcessRows(drAgeCatInfo, ProgTypeGraduate, AttendTypePartTime, dsRpt.Tables(MainTableName & "Part4"))
		Next

		' process age categories for full time first professionals
		For Each drAgeCatInfo In dtAgeCatInfo.Rows
			ProcessRows(drAgeCatInfo, ProgTypeFirstProf, AttendTypeFullTime, dsRpt.Tables(MainTableName & "Part5"))
		Next

		' process age categories for part time first professionals
		For Each drAgeCatInfo In dtAgeCatInfo.Rows
			ProcessRows(drAgeCatInfo, ProgTypeFirstProf, AttendTypePartTime, dsRpt.Tables(MainTableName & "Part6"))
		Next

		' return report dataset
		Return dsRpt

	End Function

	Private Sub ProcessRows(ByVal drAgeCatInfo As DataRow, ByVal ProgTypeDescrip As String, ByVal AttendTypeDescrip As String, ByRef dtRpt As DataTable)
		Dim i As Integer
		Dim drRpt As DataRow
		Dim AgeRangeLower As Integer = 0, AgeRangeUpper As Integer = 0
		Dim FilterString As String

		' set filter according to passed-in age category info
		If Not (drAgeCatInfo("AgeCatRangeLower") Is DBNull.Value) Then
			AgeRangeLower = CInt(drAgeCatInfo("AgeCatRangeLower"))
		End If
		If Not (drAgeCatInfo("AgeCatRangeUpper") Is DBNull.Value) Then
			AgeRangeUpper = CInt(drAgeCatInfo("AgeCatRangeUpper"))
		End If

		If AgeRangeLower <> 0 And AgeRangeUpper <> 0 Then
			FilterString = "Age >= " & AgeRangeLower & " AND Age <= " & AgeRangeUpper
		ElseIf AgeRangeLower = 0 Then
			FilterString = "Age <= " & AgeRangeUpper
		Else ' AgeRangeUpper = 0
			FilterString = "Age >= " & AgeRangeLower
		End If

		' add passed-in Program Type and Attendance Type to filter
		FilterString &= "AND ProgTypeDescrip LIKE '" & ProgTypeDescrip & "' AND " & _
						"AttendTypeDescrip LIKE '" & AttendTypeDescrip & "'"

		' create new summary report row and add to report DataSet
		drRpt = dtRpt.NewRow

		' set age category description
		drRpt("AgeDescrip") = drAgeCatInfo("AgeCatDescrip").ToString

		' set counters for each Gender, using filters from passed-in info
		With dtRptRaw
			drRpt("Count_Male") = .Compute("COUNT(Age)", FilterString & " AND GenderDescrip LIKE 'Men'")
			drRpt("Count_Female") = .Compute("COUNT(Age)", FilterString & " AND GenderDescrip LIKE 'Women'")
		End With

		dtRpt.Rows.Add(drRpt)
	End Sub

End Class



