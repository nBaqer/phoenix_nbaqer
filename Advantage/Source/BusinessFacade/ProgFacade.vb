Imports System.data
Imports FAME.AdvantageV1.DataAccess


Public Class ProgFacade

#Region "Private variables and objects"

#End Region

    Public Function ProcessCoursesNotAssgd(ByVal TblSelect As String, ByVal ID As String, ByVal CampusId As String) As DataSet

        Dim PrgDB As New ProgDB
        Dim ds As New DataSet
        Dim row As DataRow
        Dim Req As String
        Dim tbl As New DataTable


        ds = PrgDB.GetCoursesNotAssigned(TblSelect, ID, CampusId)

        tbl = ds.Tables(0)
        'Make the ChildId column the primary key
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("ReqId")}
        End With

        If tbl.Rows.Count > 0 Then
            'Load a datatable with ChildId, ChildTyp and ChildDescrip
            'Create a datable with a new column called ChildId, make this the primary key.

            'Add a new column called ChildDescrip to the datatable 
            Dim col10 As DataColumn = tbl.Columns.Add("ChildDescripTyp", GetType(String))

            For Each row In tbl.Rows

                Dim row2 As DataRow = tbl.Rows.Find(row("ReqId"))

                Req = row2("ReqId").ToString


                row("ChildDescripTyp") = row("Descrip") + " (" + row("Code") + " )"

            Next
        End If

        Return ds

    End Function
    Public Function ProcessCoursesNotAssgd(ByVal TblSelect As String, ByVal ID As String, ByVal TrackAttendance As Boolean, ByVal CampusId As String) As DataSet

        Dim PrgDB As New ProgDB
        Dim ds As New DataSet
        Dim row As DataRow
        Dim Req As String
        Dim tbl As New DataTable


        ds = PrgDB.GetCoursesNotAssigned(TblSelect, ID, TrackAttendance, CampusId)

        tbl = ds.Tables(0)
        'Make the ChildId column the primary key
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("ReqId")}
        End With

        If tbl.Rows.Count > 0 Then
            'Load a datatable with ChildId, ChildTyp and ChildDescrip
            'Create a datable with a new column called ChildId, make this the primary key.

            'Add a new column called ChildDescrip to the datatable 
            Dim col10 As DataColumn = tbl.Columns.Add("ChildDescripTyp", GetType(String))

            For Each row In tbl.Rows

                Dim row2 As DataRow = tbl.Rows.Find(row("ReqId"))

                Req = row2("ReqId").ToString


                row("ChildDescripTyp") = row("Descrip") + " (" + row("Code") + " )"

            Next
        End If

        Return ds

    End Function

    Public Function ProcessCourseGrpsNotAssgd(ByVal TblSelect As String, ByVal ID As String, ByVal CampusId As String) As DataSet

        Dim PrgDB As New ProgDB
        Dim ds As New DataSet

        ds = PrgDB.GetCourseGrpsNotAssigned(TblSelect, ID, CampusId)

        Return ds

    End Function

    Public Function GetCourseCourseGrpsAssigned(ByVal TblSelect As String, ByVal ID As String) As DataSet
        Dim PrgDB As New ProgDB
        Dim loadedDS As New DataSet

        loadedDS = PrgDB.LoadChildDS(TblSelect, ID)

        Return loadedDS
    End Function

    Public Function GetCoursesForProgVersion(ByVal prgVerId As String) As DataTable
        Dim PrgDB As New ProgDB

        Return PrgDB.GetCoursesForProgVersion(prgVerId)

    End Function

    Public Sub UpdatePrgVersionTermNo(ByVal termNo As Integer, ByVal reqId As String, ByVal prgverid As String)
        Dim PrgDB As New ProgDB
        PrgDB.UpdatePrgVersionTermNo(termNo, reqId, prgverid)
    End Sub

    Public Function ProcessHrsCrdtsDescrips(ByVal ds As DataSet) As DataSet
        'return tbl3 for Presentation layer to bind to list box
        Dim tbl1 As New DataTable
        Dim tbl2 As New DataTable
        Dim tbl3 As New DataTable
        Dim tbl4 As New DataTable
        Dim row As DataRow
        Dim selHours As Decimal
        Dim selCreds As Decimal
        Dim txtChildHrs As String
        Dim txtChildCrds As String
        Dim hcRow As DataRow
        Dim Req As String

        tbl1 = ds.Tables("Courses")
        'Make the ChildId column the primary key
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("ReqId")}
        End With

        tbl2 = ds.Tables("CourseGrps")
        'Make the ChildId column the primary key
        With tbl2
            .PrimaryKey = New DataColumn() {.Columns("ReqId")}
        End With

        tbl3 = ds.Tables("FldsSelected")
        'Make the ChildId column the primary key
        With tbl3
            .PrimaryKey = New DataColumn() {.Columns("ReqId")}
        End With

        tbl4.TableName = "ChildHrsCrds"

        'Add a column called TblName and another called TblPK to the dtTables datatable
        Dim col1 As DataColumn = tbl4.Columns.Add("ChildHrs")
        Dim col2 As DataColumn = tbl4.Columns.Add("ChildCrds")


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If ds.Tables("FldsSelected").Rows.Count > 0 Then
            'Load a datatable with ChildId, ChildTyp and ChildDescrip
            'Create a datable with a new column called ChildId, make this the primary key.

            'Add a new column called ChildDescrip to the datatable 
            Dim col10 As DataColumn = tbl3.Columns.Add("ChildDescripTyp", GetType(String))

            For Each row In tbl3.Rows

                If (row("ReqTypeId") = 1) Then 'COURSE
                    Dim row2 As DataRow = tbl1.Rows.Find(row("ReqId"))

                    Req = row2("ReqId").ToString
                    'Populate the hours and credits for the courses
                    'selected.
                    If Not row2.IsNull("Hours") Then
                        selHours += row2("Hours")
                        txtChildHrs = selHours.ToString
                    End If
                    If Not row2.IsNull("Credits") Then
                        selCreds += row2("Credits")
                        txtChildCrds = selCreds.ToString
                    End If

                    If (row("IsRequired") = True) Then
                        row("ChildDescripTyp") = row("Descrip") + " (" + row("Code") + " )" + " - R"
                    Else
                        row("ChildDescripTyp") = row("Descrip") + " (" + row("Code") + " )" + " - E"
                    End If

                ElseIf (row("ReqTypeId") = 2) Then 'COURSEGRP
                    Dim row3 As DataRow = tbl2.Rows.Find(row("ReqId"))
                    row("ChildDescripTyp") = row("Descrip")

                    'Populate the hours and credits for the coursegrps
                    'selected.
                    selHours += row3("Hours")
                    selCreds += row3("Credits")
                    txtChildHrs = selHours.ToString
                    txtChildCrds = selCreds.ToString
                End If

            Next

            Dim dr As DataRow = tbl4.NewRow
            dr("ChildHrs") = txtChildHrs
            dr("ChildCrds") = txtChildCrds
            tbl4.Rows.Add(dr)

        End If

        ds.Tables.Add(tbl4)
        Return ds
    End Function
    Public Function ProcessDescription(ByVal ds As DataSet) As DataSet
        Dim tbl1 As New DataTable
        Dim tbl2 As New DataTable
        Dim tbl3 As New DataTable
        Dim tbl4 As New DataTable
        Dim row As DataRow
        Dim selHours As Integer
        Dim selCreds As Integer
        Dim txtChildHrs As String
        Dim txtChildCrds As String
        Dim hcRow As DataRow

        tbl3 = ds.Tables("FldsSelected")
        'Make the ChildId column the primary key
        With tbl3
            .PrimaryKey = New DataColumn() {.Columns("ReqId")}
        End With


        If ds.Tables("FldsSelected").Rows.Count > 0 Then
            'Load a datatable with ChildId, ChildTyp and ChildDescrip
            'Create a datable with a new column called ChildId, make this the primary key.

            'Add a new column called ChildDescrip to the datatable 
            Dim col9 As DataColumn = tbl3.Columns.Add("ChildDescrip", GetType(String))

            'Add a new column called ChildDescrip to the datatable 
            Dim col10 As DataColumn = tbl3.Columns.Add("ChildDescripTyp", GetType(String))

            For Each row In tbl3.Rows

                If (row("ReqTypeId") = 1) Then 'COURSE
                    Dim row2 As DataRow = tbl3.Rows.Find(row("ReqId"))


                    If (row("IsRequired") = True) Then
                        row("ChildDescripTyp") = row("Descrip") + " - R"
                    Else
                        row("ChildDescripTyp") = row("Descrip") + " - E"
                    End If

                ElseIf (row("ReqTypeId") = 2) Then 'COURSEGRP
                    Dim row3 As DataRow = tbl3.Rows.Find(row("ReqId"))
                    row("ChildDescripTyp") = row("Descrip")

                End If

            Next

        End If
        Return ds
    End Function
    Public Function GetGrdSysDetails(ByVal ID As String) As DataSet

        '   Instantiate DAL component
        Dim db As New ProgDB

        '   get the dataset with all degrees
        Return db.GetGrdSysDetails(ID)

    End Function

    Public Function InsertCourseCourseGrp(ByVal TblSelect As String, ByVal ChildInfoObj As ChildInfo, ByVal ID As String, ByVal user As String) As String
        Dim PrgDB As New ProgDB
        Dim sModDate As String

        sModDate = PrgDB.InsertChildToDB(TblSelect, ChildInfoObj, ID, user)
        Return sModDate
    End Function

    Public Function DeleteCourseCourseGrp(ByVal TblSelect As String, ByVal ChildInfoObj As ChildInfo, ByVal ID As String) As String
        Dim PrgDB As New ProgDB

        Return PrgDB.DeleteChildFrmDB(TblSelect, ChildInfoObj, ID)

    End Function
    Public Function UpdateCourseCourseGrp(ByVal TblSelect As String, ByVal ChildInfoObj As ChildInfo, ByVal ID As String, ByVal user As String)
        Dim PrgDB As New ProgDB

        PrgDB.UpdateChildInDB(TblSelect, ChildInfoObj, ID, user)
    End Function

    Public Function ValidateHrsCreds(ByVal PrgVerHrs As Decimal, ByVal PrgVerCreds As Decimal, ByVal ChildHrs As Decimal, ByVal ChildCreds As Decimal, ByVal ds As DataSet) As ProgBL.ValResult

        Dim BL As New ProgBL
        Dim ValEnum As New ProgBL.ValResult
        Dim enumvar As Integer

        enumvar = BL.ValHrsCreds(PrgVerHrs, PrgVerCreds, ChildHrs, ChildCreds, ds)
        If enumvar = ValEnum.Success Then
            Return ProgBL.ValResult.Success
        ElseIf enumvar = ValEnum.FailHrsCrdtsNtMet Then
            Return ProgBL.ValResult.FailHrsCrdtsNtMet
        ElseIf enumvar = ValEnum.FailReqHrsCrdsOver Then
            Return ProgBL.ValResult.FailReqHrsCrdsOver
        End If

    End Function

    Public Function GetMinGradeReqd(ByVal PrgVerId As String) As String

        Dim PrgDB As New ProgDB
        Dim dt As New DataTable
        Dim mingrade As String
        Dim row As DataRow

        dt = PrgDB.GetMinGradeReqd(PrgVerId)
        For Each row In dt.Rows
            mingrade = row("Grade")
        Next


        Return mingrade

    End Function
    Public Function GetMinGradeReqdDT(ByVal PrgVerId As String) As DataTable

        Dim PrgDB As New ProgDB
        Dim dt As New DataTable
        Dim mingrade As String
        Dim row As DataRow

        dt = PrgDB.GetMinGradeReqd(PrgVerId)

        Return dt

    End Function
    Public Function DoesPrgVerHaveStdsReg(ByVal PrgVerId As String) As Integer

        Dim db As New ProgDB
        Dim Result As Integer

        Result = db.AreStudentsRegFrPrgVer(PrgVerId)

        Return Result
    End Function
    Public Function DoesPrgVerHaveStdsEnrolled(ByVal PrgVerId As String) As Integer

        Dim db As New ProgDB
        Dim Result As Integer

        Result = db.AreStudentsEnrolledInPrgVer(PrgVerId)

        Return Result
    End Function
    Public Function PrgVersionsWithDifferingUnits(ByVal ProgId As String, ByVal hours As Decimal, ByVal credits As Decimal, ByVal terms As Integer, ByVal weeks As Integer) As Integer
        Dim db As New ProgDB
        Dim result As Integer

        result = db.PrgVersionsWithDifferingUnits(ProgId, hours, credits, terms, weeks)
        Return result
    End Function
    Public Function IsCourseHaveStudents(ByVal ReqId As String, ByVal ProgVerId As String) As Boolean
        Dim db As New ProgDB
        'If (db.IsCourseHaveStudents(ReqId, ProgVerId) > 0) Then
        ''modified by Saraswathi lakshmanan on june 18 2010
        ''Th stored procedure checks for the courses and the coursegroups for a given reqID.
        If (db.IsCourseHaveStudents_SP(ReqId, ProgVerId) > 0) Then
            Return True
        Else
            Return False
        End If
    End Function
    ''Added by Saraswathi lakshmanan on may 05 2010 for payment periods

    Public Function GetDefaultChargesForProgVersion(ByVal prgVerId As String) As DataTable
        Dim PrgDB As New ProgDB

        Return PrgDB.GetDefaultChargesForProgVersion_sp(prgVerId)

    End Function

    Public Function GetAllSysTransCodes(ByVal showActive As String) As DataTable
        Dim PrgDB As New ProgDB

        Return PrgDB.GetAllSysTransCodes_SP(showActive)

    End Function

    Public Function GetAllFeeLevels() As DataTable
        Dim PrgDB As New ProgDB

        Return PrgDB.GetAllFeeLevels_SP()

    End Function
    Public Function UpdateDefaultChargesforProgramVersion(ByVal dtDefaultCharge As DataTable, ByVal UserName As String)
        Dim PrgDB As New ProgDB
        PrgDB.UpdateDefaultChargesforProgramVersion(dtDefaultCharge, UserName)
    End Function
End Class
