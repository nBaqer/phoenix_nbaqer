Public Class LeadZipDetailObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim obj As New LeadMasterDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(obj.GetLeadZipDetail(rptParamInfo))
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    ' Copy data from dataset to datatable.
    Private Function BuildReportSource(ByVal ds As DataSet) As DataSet
        Dim facInputMasks As New InputMasksFacade
        Dim zipMask As String
        Dim strName As String = ""

        'Get the mask for phone numbers and zip
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        Try
            If ds.Tables.Count = 2 Then

                Dim zipCount As Integer = ds.Tables(1).Rows.Count
                Dim ttlInquiries As Integer

                If ds.Tables(1).Rows.Count > 0 Then
                    Dim obj As Object = ds.Tables(1).Compute("SUM(Inquiries)", "")
                    If Not (obj Is System.DBNull.Value) Then
                        ttlInquiries = obj
                    End If
                End If

                For Each dr As DataRow In ds.Tables(0).Rows
                    dr("ZipCount") = zipCount       ' ds.Tables(1).Rows.Count
                    strName = ""
                    'Only Lead's MiddleName allows NULL.
                    If Not (dr("MiddleName") Is System.DBNull.Value) Then
                        If dr("MiddleName") <> "" Then
                            dr("LeadName") = dr("LastName") & ", " & dr("FirstName") & _
                                                " " & dr("MiddleName") & "."
                        Else
                            dr("LeadName") = dr("LastName") & ", " & dr("FirstName")
                        End If
                    Else
                        dr("LeadName") = dr("LastName") & ", " & dr("FirstName")
                    End If                '

                    If Not (dr("ForeignZip") Is System.DBNull.Value) Then
                        If dr("Zip") <> "" And dr("ForeignZip") = False Then
                            'Domestic Zip
                            dr("Zip") = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                        End If
                    Else
                        'Domestic Zip
                        If dr("Zip") <> "" Then
                            dr("Zip") = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                        End If
                    End If
                Next

                For Each dr As DataRow In ds.Tables(1).Rows
                    'Compute Percentage for every zip code.
                    If Not (dr("Inquiries") Is System.DBNull.Value) Then
                        If ttlInquiries <> 0 Then
                            dr("Percentage") = Integer.Parse(dr("Inquiries")) * 100 / ttlInquiries
                        End If
                    End If

                    If Not (dr("ForeignZip") Is System.DBNull.Value) Then
                        If dr("Zip") <> "" And dr("ForeignZip") = False Then
                            'Domestic Zip
                            dr("Zip") = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                        End If
                    Else
                        'Domestic Zip
                        If dr("Zip") <> "" Then
                            dr("Zip") = facInputMasks.ApplyMask(zipMask, dr("Zip"))
                        End If
                    End If
                Next
            End If


        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
