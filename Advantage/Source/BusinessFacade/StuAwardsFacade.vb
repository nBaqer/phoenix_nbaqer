

Public Class StuAwardsFacade

    Public Function GetAllLenders(ByVal strStatus As String, ByVal campusId As String) As DataSet

        '   get the dataset with all Lenders
        Return (New LendersDB).GetAllLenders(strStatus, campusId)

    End Function
    Public Function GetLendersOnly(ByVal strStatus As String) As DataSet

        '   get the dataset with all Lenders
        Return (New LendersDB).GetLendersOnly(strStatus)

    End Function
    Public Function GetServicersOnly(ByVal strStatus As String) As DataSet

        '   get the dataset with all Lenders
        Return (New LendersDB).GetServicersOnly(strStatus)

    End Function
    Public Function GetGuarantorsOnly(ByVal strStatus As String) As DataSet

        '   get the dataset with all Lenders
        Return (New LendersDB).GetGuarantorsOnly(strStatus)

    End Function
    Public Function GetLenderInfo(ByVal LenderId As String) As LenderInfo

        '   get the LenderInfo
        Return (New LendersDB).GetLenderInfo(LenderId)

    End Function
    Public Function UpdateLenderInfo(ByVal LenderInfo As LenderInfo, ByVal user As String) As ResultInfo

        '   If it is a new account do an insert. If not, do an update
        If Not (LenderInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New LendersDB).AddLenderInfo(LenderInfo, user)
        Else
            '   return integer with update results
            Return (New LendersDB).UpdateLenderInfo(LenderInfo, user)
        End If

    End Function
    Public Function DeleteLenderInfo(ByVal LenderId As String, ByVal modDate As DateTime) As String

        '   delete LenderInfo ans return string result
        Return (New LendersDB).DeleteLenderInfo(LenderId, modDate)

    End Function
    Public Function GetAllStudentAwards(ByVal stuEnrollId As String, Optional ByVal FundSourceId As String = "") As DataSet
        '   get the dataset with all StudentAwards
        Dim ds As New DataSet
        ds = (New StudentAwardsDB).GetAllStudentAwards(stuEnrollId, FundSourceId)
        'For Each dr As DataRow In ds.Tables(0).Rows
        '    If CType(dr("AwardStartDate"), Date).ToShortDateString = "12/30/1899" Then
        '        dr("AwardStartDate") = ""
        '    End If
        '    If CType(dr("AwardEndDate"), Date).ToShortDateString = "12/30/1899" Then
        '        dr("AwardEndDate") = ""
        '    End If
        'Next
        Return ds
    End Function
    ''
    ''Function Added by Kamalesh Ahuja on 31 Aug 2010 to Resolve Mantis Issue Id 19665
    Public Function GetAllStudentAwardsNew(ByVal stuEnrollId As String) As DataSet
        '   get the dataset with all StudentAwards
        Dim ds As New DataSet
        ds = (New StudentAwardsDB).GetAllStudentAwardsNew(stuEnrollId)
        'For Each dr As DataRow In ds.Tables(0).Rows
        '    If CType(dr("AwardStartDate"), Date).ToShortDateString = "12/30/1899" Then
        '        dr("AwardStartDate") = ""
        '    End If
        '    If CType(dr("AwardEndDate"), Date).ToShortDateString = "12/30/1899" Then
        '        dr("AwardEndDate") = ""
        '    End If
        'Next
        Return ds
    End Function
    ''
    Public Function GetStudentAwardScheduleForRefunds(ByVal stuAwardId As String) As DataSet
        '   get the dataset with all AwardSchedules
        Dim ds As New DataSet
        ds = (New StudentAwardsDB).GetStudentAwardScheduleForRefunds(stuAwardId)

        Return ds
    End Function

    Public Function GetStudentAwardSchedule(ByVal stuAwardId As String) As DataSet
        '   get the dataset with all AwardSchedules
        Dim ds As New DataSet
        ds = (New StudentAwardsDB).GetStudentAwardSchedule(stuAwardId)

        Return ds
    End Function


    Public Function GetAwardsPerStudent(ByVal stuEnrollId As String) As DataSet

        '   get the dataset with all StudentAwards
        Return (New StudentAwardsDB).GetAwardsPerStudent(stuEnrollId)

    End Function
    'Public Function GetStuAwardInfo(ByVal StudentAwardId As String) As StuAwardInfo

    '    '   get the StuAwardInfo
    '    Return (New StudentAwardsDB).GetStuAwardInfo(StudentAwardId)

    'End Function
    Public Function UpdateStuAwardInfo(ByVal StuAwardInfo As StuAwardInfo, ByVal user As String) As ResultInfo

        '   If it is a new account do an insert. If not, do an update
        If Not (StuAwardInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New StudentAwardsDB).AddStuAwardInfo(StuAwardInfo, user)
        Else
            '   return integer with update results
            Return (New StudentAwardsDB).UpdateStuAwardInfo(StuAwardInfo, user)
        End If

    End Function
    Public Function DeleteStuAwardInfo(ByVal StudentAwardId As String, ByVal modDate As DateTime) As String

        '   delete StuAwardInfo ans return string result
        Return (New StudentAwardsDB).DeleteStuAwardInfo(StudentAwardId, modDate)

    End Function
    Public Function GetStuAwardsDS(Optional ByVal studentId As String = "") As DataSet
        Dim ds As New DataSet
        '   get StudentAwards dataset
        ds = (New StudentAwardsDB).GetStuAwardsDS(studentId)
        'For Each dr As DataRow In ds.Tables("StudentAwards").Rows
        '    If CType(dr("AwardStartDate"), Date).ToShortDateString = "12/30/1899" Then
        '        dr("AwardStartDate") = ""
        '    End If
        '    If CType(dr("AwardEndDate"), Date).ToShortDateString = "12/30/1899" Then
        '        dr("AwardEndDate") = ""
        '    End If
        'Next
        Return ds
    End Function
    Public Function UpdateStuAwardsDS(ByVal ds As DataSet, Optional ByVal StudentAwardId As String = "", Optional ByVal StudentId As String = "") As String

        '   get StudentAwards dataset
        Return (New StudentAwardsDB).UpdateStuAwardsDS(ds, StudentAwardId, StudentId)

    End Function
    'Public Function GetAllAwardTypes(ByVal strStatus As String) As DataSet

    '    '   get the dataset with all Award Types
    '    Return (New StudentAwardsDB).GetAllAwardTypes(strStatus)

    'End Function
    'Public Function GetAllAwardYears() As DataSet

    '    '   get the dataset with all Award Years
    '    Return (New StudentAwardsDB).GetAllAwardYears
    '    '    Return (New StudentAwardsDB).GetAllAwardYears
    'End Function
    'End Function
    Public Function GetAwardTimeIntervals() As DataSet

        '   get the dataset with all Award Time Intervals
        Return (New StudentAwardsDB).GetAwardTimeIntervals

    End Function
    Public Function GetTopTerms(ByVal StartDate As DateTime, ByVal EndDate As DateTime, ByVal DisbNumber As Integer) As DataTable

        '   get datatable with top n terms, including current one
        Return (New StudentAwardsDB).GetTopTerms(StartDate, EndDate, DisbNumber)

    End Function
    Public Function GetAllPaymentPlans(ByVal stuEnrollId As String) As DataSet

        '   get the dataset with all PaymentPlans
        Return (New StudentAwardsDB).GetAllPaymentPlans(stuEnrollId)

    End Function
    Public Function GetPaymentPlanInfo(ByVal PaymentPlanId As String) As PaymentPlanInfo

        '   get the PaymentPlanInfo
        Return (New StudentAwardsDB).GetPaymentPlanInfo(PaymentPlanId)

    End Function
    Public Function UpdatePaymentPlanInfo(ByVal PaymentPlanInfo As PaymentPlanInfo, ByVal user As String) As ResultInfo

        '   If it is a new account do an insert. If not, do an update
        If Not (PaymentPlanInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New StudentAwardsDB).AddPaymentPlanInfo(PaymentPlanInfo, user)
        Else
            '   return integer with update results
            Return (New StudentAwardsDB).UpdatePaymentPlanInfo(PaymentPlanInfo, user)
        End If

    End Function
    Public Function DeletePaymentPlanInfo(ByVal PaymentPlanId As String, ByVal modDate As DateTime) As String

        '   delete PaymentPlanInfo ans return string result
        Return (New StudentAwardsDB).DeletePaymentPlanInfo(PaymentPlanId, modDate)

    End Function
    Public Function GetPaymentPlansDS(ByVal paymentPlanId As String, ByVal voided As Boolean?) As DataSet

        '   get PaymentPlans dataset
        Return (New StudentAwardsDB).GetPaymentPlansDS(paymentPlanId, voided)

    End Function
    Public Function UpdatePaymentPlansDS(ByVal ds As DataSet) As String

        '   get PaymentPlans dataset
        Return (New StudentAwardsDB).UpdatePaymentPlansDS(ds)

    End Function
    Public Function CheckIfDataExistsInPmtDisbRel(ByVal strStudentAwardId As String) As String
        Return (New StudentAwardsDB).CheckIfDataExistsInPmtDisbRel(strStudentAwardId)
    End Function
    Public Function HasPaymentPlanPostedPayments(ByVal paymentPlanId As Guid) As Boolean
        Return (New StudentAwardsDB).HasPaymentPlanPostedPayments(paymentPlanId)
    End Function
End Class
