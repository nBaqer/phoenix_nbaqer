' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' StudentAccountsFacade.vb
'
' StudentsAccountsFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class StudentContactsFacade
    Public Function GetAllStudentContacts(ByVal studentId As String) As DataSet

        '   get the dataset with all StudentContacts
        Return (New StudentContactsDB).GetAllStudentContacts(studentId)

    End Function
    Public Function GetStudentContactInfo(ByVal StudentContactId As String) As StudentContactInfo

        '   get the StudentContactInfo
            Return (New StudentContactsDB).GetStudentContactInfo(StudentContactId)

    End Function
    Public Function UpdateStudentContactInfo(ByVal StudentContactInfo As StudentContactInfo, ByVal user As String) As String

        '   If it is a new account do an insert. If not, do an update
        If Not (StudentContactInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New StudentContactsDB).AddStudentContactInfo(StudentContactInfo, user)
        Else
            '   return integer with update results
            Return (New StudentContactsDB).UpdateStudentContactInfo(StudentContactInfo, user)
        End If

    End Function
    Public Function DeleteStudentContactInfo(ByVal StudentContactId As String, ByVal modDate As DateTime) As String

        '   delete StudentContactInfo ans return string result
        Return (New StudentContactsDB).DeleteStudentContactInfo(StudentContactId, modDate)

    End Function
    Public Function GetAllStudentContactAddresses(ByVal studentContactId As String) As DataSet

        '   get the dataset with all StudentContactAddresses
        Return (New StudentContactsDB).GetAllStudentContactAddresses(studentContactId)

    End Function
    Public Function GetStudentContactAddressInfo(ByVal StudentContactAddressId As String) As StudentContactAddressInfo

        '   get the StudentContactAddressInfo
        Return (New StudentContactsDB).GetStudentContactAddressInfo(StudentContactAddressId)

    End Function
    Public Function UpdateStudentContactAddressInfo(ByVal StudentContactAddressInfo As StudentContactAddressInfo, ByVal user As String) As String

        '   If it is a new account do an insert. If not, do an update
        If Not (StudentContactAddressInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New StudentContactsDB).AddStudentContactAddressInfo(StudentContactAddressInfo, user)
        Else
            '   return integer with update results
            Return (New StudentContactsDB).UpdateStudentContactAddressInfo(StudentContactAddressInfo, user)
        End If

    End Function
    Public Function DeleteStudentContactAddressInfo(ByVal StudentContactAddressId As String, ByVal modDate As DateTime) As String

        '   delete StudentContactAddressInfo ans return string result
        Return (New StudentContactsDB).DeleteStudentContactAddressInfo(StudentContactAddressId, modDate)

    End Function
    Public Function GetAllStudentContactPhones(ByVal studentId As String) As DataSet
        Dim ds As New DataSet
        ds = (New StudentContactsDB).GetAllStudentContactPhones(studentId)
        Dim facInputMasks As New InputMasksFacade
        Dim PhoneMask As String
        Dim zipMask As String
        Dim dr As DataRow

        'Get the mask for phone numbers and zip
        PhoneMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

        For Each dr In ds.Tables(0).Rows
            If dr("Phone").ToString.Length >= 1 And dr("ForeignPhone") = 0 Then
                dr("Phone") = facInputMasks.ApplyMask(PhoneMask, dr("Phone"))
                'Else
                'dr("Phone") = dr("Phone")
            End If
        Next

        Return ds

    End Function
    Public Function GetStudentContactPhoneInfo(ByVal StudentContactPhoneId As String) As StudentContactPhoneInfo

        '   get the StudentContactPhoneInfo
        Return (New StudentContactsDB).GetStudentContactPhoneInfo(StudentContactPhoneId)

    End Function
    Public Function UpdateStudentContactPhoneInfo(ByVal StudentContactPhoneInfo As StudentContactPhoneInfo, ByVal user As String) As String

        '   If it is a new account do an insert. If not, do an update
        If Not (StudentContactPhoneInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New StudentContactsDB).AddStudentContactPhoneInfo(StudentContactPhoneInfo, user)
        Else
            '   return integer with update results
            Return (New StudentContactsDB).UpdateStudentContactPhoneInfo(StudentContactPhoneInfo, user)
        End If

    End Function
    Public Function DeleteStudentContactPhoneInfo(ByVal StudentContactPhoneId As String, ByVal modDate As DateTime) As String

        '   delete StudentContactPhoneInfo ans return string result
        Return (New StudentContactsDB).DeleteStudentContactPhoneInfo(StudentContactPhoneId, modDate)

    End Function
End Class
