Public Class AdReqsFacade

    Public Function GetReqGroupsDS() As DataSet

        '   get LeadGrpReqGrp dataset
        Return (New AdReqsDB).GetReqGroupsDS()

    End Function
    Public Function CheckIfReqGroupMeetsConditionsNoPrgVersion(ByVal LeadId As String) As String
        With New RequirementsDB
            Return .CheckIfReqGroupMeetsConditionsNoPrgVersion(LeadId)
        End With
    End Function
    Public Function GetRequirementsAndEffectiveDatesDS() As DataSet

        Dim ds As DataSet
        '   get LeadGrpReqGrp dataset
        ds = (New AdReqsDB).GetRequirementsAndEffectiveDatesDS()

        Dim dr As DataRow
        For Each dr In ds.Tables("Requirements").Rows
            Try
                If dr("MandatoryRequirement") = 1 Then
                    dr("MandatoryRequirement") = True
                Else
                    dr("MandatoryRequirement") = False
                End If
            Catch ex As Exception
                dr("MandatoryRequirement") = False
            End Try
        Next
        Return ds

    End Function
    Public Function GetReqDefByLeadGrp(ByVal ReqGrpId As String, ByVal LeadGrpId As String) As AdReqGrpInfo
        Return (New AdReqsDB).GetReqDefByLeadGrp(ReqGrpId, LeadGrpId)
    End Function
    Public Function GetsReqsByLeadGrp(ByVal LeadGrpId As Object, ByVal ReqGrpId As String, ByVal strCurrentDate As Date, Optional ByVal campusid As String = "") As DataSet
        Dim ds As DataSet
        ds = (New AdReqsDB).GetsReqsByLeadGrp(LeadGrpId, ReqGrpId, strCurrentDate, campusid)
        For Each dr As DataRow In ds.Tables(0).Rows
            If dr.IsNull("Required") OrElse Not (Convert.ToBoolean(dr("Required"))) Then
                dr("Required") = False
            End If
        Next
        Return ds
    End Function
    Public Function GetsMandatoryRequirementsByLeadGrp(ByVal strCurrentDate As Date) As DataSet
        Return (New AdReqsDB).GetsMandatoryRequirementsByLeadGrp(strCurrentDate)
    End Function
    Public Function InsertReqGrpDef(ByVal ReqGrpId As String, ByVal minNumReqs As Integer, ByVal LeadGrpId As String, ByVal selectedRequirement() As String, ByVal User As String, ByVal selectedMandatory() As Boolean, ByVal StatusId As String) As String
        Return (New AdReqsDB).InsertReqGrpDef(ReqGrpId, minNumReqs, LeadGrpId, selectedRequirement, User, selectedMandatory, StatusId)
    End Function
    Public Function UpdateReqGrpDef(ByVal ReqGrpId As String, ByVal minNumReqs As Integer, ByVal LeadGrpId As String, ByVal selectedRequirement() As String, ByVal User As String, ByVal selectedMandatory() As Boolean, ByVal StatusId As String) As String
        Return (New AdReqsDB).UpdateReqGrpDef(ReqGrpId, minNumReqs, LeadGrpId, selectedRequirement, User, selectedMandatory, StatusId)
    End Function
    Public Function UpdateReqGroupsDS(ByVal ds As DataSet) As String

        '   get StudentAwards dataset
        Return (New AdReqsDB).UpdateReqGroupsDS(ds)
    End Function
    Public Function GetRequirementGroupByLeadGroups(ByVal ReqGrpId As String) As DataSet
        Return (New AdReqsDB).GetRequirementGroupByLeadGroups(ReqGrpId)
    End Function
    Public Function GetRequirementGroupByLeadGroups(ByVal ReqGrpId As String, ByVal Status As String) As DataSet
        Return (New AdReqsDB).GetRequirementGroupByLeadGroups(ReqGrpId, Status)
    End Function
    Public Function UpdateRequirementsandEffectiveDatesDS(ByVal ds As DataSet) As String

        '   get StudentAwards dataset
        Return (New AdReqsDB).UpdateRequirementsandEffectiveDatesDS(ds)

    End Function
    Public Function GetAllLeadGroupsForEnrollment(ByVal CampusId As String, Optional ByVal StuEnrollId As String = "") As DataSet
        Return (New AdReqsDB).GetAllLeadGroupsForEnrollment(CampusId, StuEnrollId)
    End Function
    Public Function GetAllLeadGroups(ByVal CampusId As String, Optional ByVal StatusId As String = "Active") As DataSet

        '   get LeadGrpReqGrp dataset
        Return (New AdReqsDB).GetAllLeadGroups(CampusId, StatusId)

    End Function
    Public Function GetAllLeadGroupsForExistingLeads(ByVal CampusId As String, Optional ByVal LeadId As String = "") As DataSet
        Return (New AdReqsDB).GetAllLeadGroupsForExistingLeads(CampusId, LeadId)
    End Function
    Public Function GetAllLeadGroups() As DataSet

        '   get LeadGrpReqGrp dataset
        Return (New AdReqsDB).GetAllLeadGroups()

    End Function
    Public Function UpdateLeadGroupsAndStudentEnrollments(ByVal LeadId As String, ByVal user As String, ByVal selectedLeadGroups() As String, ByVal StuEnrollId As String) As Integer

        '   Instantiate DAL component
        Dim reqsDB As New RequirementsDB

        '   return query results
        Return reqsDB.UpdateLeadGroupsAndStudentEnrollments(LeadId, user, selectedLeadGroups, StuEnrollId)

    End Function
    Public Function UpdateLeadGroupsAndStudent(ByVal LeadId As String, ByVal user As String, ByVal selectedLeadGroups() As String, ByVal StuEnrollId As String) As Integer

    End Function
    Public Function UpdateLeadGroups(ByVal LeadId As String, ByVal user As String, ByVal selectedLeadGroups() As String) As Integer

        '   Instantiate DAL component
        Dim reqsDB As New AdReqsDB

        '   return query results
        Return reqsDB.UpdateLeadGroups(LeadId, user, selectedLeadGroups)

    End Function
    Public Function GetLeadgroupsselected(ByVal LeadId As String) As DataSet

        '   Instantiate DAL component
        Dim leadDb As New AdReqsDB

        '   return Dataset
        Return leadDb.GetLeadgroupsSelected(LeadId)

    End Function
    Public Function GetLeadgroupsSelectedByStudent(ByVal studentId As String) As DataSet
        '   Instantiate DAL component
        Dim leadDb As New AdReqsDB

        '   return Dataset
        Return leadDb.GetLeadgroupsSelectedByStudent(studentId)

    End Function
    Public Function GetLeadgroupsSelectedByStudentEnrollment(ByVal StuEnrollId As String) As DataSet
        Dim leadDb As New RequirementsDB
        Return leadDb.GetLeadgroupsSelectedByStudentEnrollment(StuEnrollId)
    End Function
    Public Function GetLeadGroupCountByStudentEnrollment(ByVal StuEnrollId As String) As Integer
        Dim leadDb As New RequirementsDB
        Return leadDb.GetLeadGroupCountByStudentEnrollment(StuEnrollId)
    End Function
    Public Function GetLeadGroupCount(ByVal LeadId As String) As Integer
        Dim leadDb As New AdReqsDB
        Return leadDb.GetLeadGroupCount(LeadId)
    End Function
    Public Function GetLeadGroupCountByStudent(ByVal StudentId As String) As Integer
        Dim leadDb As New AdReqsDB
        Return leadDb.GetLeadGroupCountByStudent(StudentId)
    End Function
    Public Function AdmReqSummary(ByVal LeadId As String, ByVal PrgVerId As String) As String
        'Instantiate DAL component
        With New AdReqsDB
            'get the dataset with all SkillGroups
            Return .ReqByGrp(LeadId, PrgVerId)
        End With
    End Function
    Public Function StandardAdmReqSummary(ByVal LeadId As String) As String
        'Instantiate DAL component
        With New AdReqsDB
            'get the dataset with all SkillGroups
            Return .StandardReqByGrp(LeadId)
        End With
    End Function
    Public Function StandardReqByGrpAndEffectiveDates(ByVal LeadId As String, ByVal strEnrolldate As String) As String
        With New RequirementsDB
            Return .StandardReqByGrpAndEffectiveDates(LeadId, strEnrolldate)
        End With
    End Function
    Public Function GetAllDocumentStudentNames(ByVal campusId As String, Optional ByVal StudentIdentifier As String = "SSN") As DataSet
        Dim ds As New DataSet
        With New RequirementsDB 'LeadDB
            ds = .GetAllStudentNamesAndIdentifier(campusId, StudentIdentifier)
        End With
        ds = ConcatenateStudentLastFirstName(ds, StudentIdentifier)
        Return ds
    End Function
    Public Function ConcatenateStudentLastFirstName(ByVal ds As DataSet, Optional ByVal StudentIdentifier As String = "SSN") As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorStudent")
        'With tbl
        '    .PrimaryKey = New DataColumn() {.Columns("StudentId")}
        'End With
        If ds.Tables("InstructorStudent").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                If StudentIdentifier.ToLower() = "SSN".ToLower() Then
                    If Not row("SSN") Is DBNull.Value Then
                        If Len(row("SSN")) >= 1 Then
                            row("SSN") = "*****" & Mid(row("SSN"), 6, 4)
                            row("FullName") = row("FirstName") & " " & row("LastName") & " - (" & row("SSN") & ")"
                        Else
                            row("FullName") = row("FirstName") & " " & row("LastName")
                        End If
                    Else
                        row("FullName") = row("FirstName") & " " & row("LastName")
                    End If 
                Else If StudentIdentifier.ToLower() = "StudentId".ToLower() Then
                        If Not row("StudentNumber") Is DBNull.Value Then
                            If Len(row("StudentNumber")) >= 1 Then
                                'row("StudentNumber") = "*****" & Mid(row("SSN"), 6, 4)
                                row("FullName") = row("FirstName") & " " & row("LastName") & " - (" & row("StudentNumber") & ")"
                            Else
                                row("FullName") = row("FirstName") & " " & row("LastName")
                            End If
                        Else
                            row("FullName") = row("FirstName") & " " & row("LastName")
                        End If 
                Else If StudentIdentifier.ToLower() = "EnrollmentId".ToLower() Then
                    If Not row("EnrollmentId") Is DBNull.Value Then
                        If Len(row("EnrollmentId")) >= 1 Then
                            row("FullName") = row("FirstName") & " " & row("LastName") & " - (" & row("EnrollmentId") & ")"
                        Else
                            row("FullName") = row("FirstName") & " " & row("LastName")
                        End If
                    Else
                        row("FullName") = row("FirstName") & " " & row("LastName")
                    End If 
                End If 
            Next
        End If
        Return ds
    End Function
    Public Function ReqByGrpAndEffectiveDates(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date) As String
        'With New AdReqsDB
        With New RequirementsDB
            Return .ReqByGrpandEffectiveDates(LeadId, PrgVerId, strEnrollDate)
        End With
    End Function
    Public Function CheckIfReqGroupMeetsConditions(ByVal LeadId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "") As String
        With New RequirementsDB
            Return .CheckIfReqGroupMeetsConditions(LeadId, PrgVerId, CampusId)
        End With
    End Function

    Public Function GetLeadGroups(ByVal adReqId As String) As DataSet
        Dim ds As DataSet
        '   get Lead Grp assigned and not assigned to adReqId
        ds = (New AdReqsDB).GetLeadGroups(adReqId)

        'Concatenate the Lead Group description and the isRequired of the selected Lead Group
        Dim dc As DataColumn = ds.Tables("SelLeadGrps").Columns.Add("Descrip", Type.GetType("System.String"))
        dc.AllowDBNull = True
        dc.Expression = "IIF(IsRequired = True, LeadGrpDescrip + ' ( R )', LeadGrpDescrip)"

        Return ds
    End Function
    Public Function UpdateAdRequirementInfo(ByVal adReqInfo As AdRequirementInfo, ByVal user As String) As String

        '   If it is a new account do an insert. If not, do an update
        If Not (adReqInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New AdReqsDB).AddAdRequirementInfo(adReqInfo, user)
        Else
            '   return integer with update results
            Return (New AdReqsDB).UpdateAdRequirementInfo(adReqInfo, user)
        End If

    End Function
    Public Function DeleteAdRequirement(ByVal adReqId As String, ByVal modDate As DateTime) As String
        Return (New AdReqsDB).DeleteAdRequirement(adReqId, modDate)
    End Function
    Public Function InsertReqLeadGroups(ByVal adReqLeadGrpId As AdReqLeadGrpInfo) As String
        Return (New AdReqsDB).InsertReqLeadGroups(adReqLeadGrpId)
    End Function
    Public Function UpdateReqLeadGroups(ByVal adReqLeadGrpId As AdReqLeadGrpInfo) As String
        Return (New AdReqsDB).UpdateReqLeadGroups(adReqLeadGrpId)
    End Function
    Public Sub DeleteReqLeadGroups(ByVal adReqId As String, ByVal leadGrpId As String)
        Dim obj As New AdReqsDB
        obj.DeleteReqLeadGroups(adReqId, leadGrpId)
    End Sub
    Public Sub DeleteReqLeadGroups(ByVal adReqId As String)
        Dim obj As New AdReqsDB
        obj.DeleteReqLeadGroups(adReqId)
    End Sub
    Public Function ValidateMandatoryReqGroup(ByVal ReqGrpId As String) As String
        Return (New AdReqsDB).ValidateMandatoryReqGroup(ReqGrpId)
    End Function
    Public Function DeleteReqGrpDef(ByVal ReqGrpId As String) As String
        Return (New AdReqsDB).DeleteReqGrpDef(ReqGrpId)
    End Function
    Public Function GetRequirementGroupsByLead(ByVal LeadId As String, ByVal strEnrollDate As Date) As DataSet
        Return (New RequirementsDB).GetRequirementGroupsByLead(LeadId, strEnrollDate)
    End Function
    Public Function GetRequirementGroupsByLeadAndPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date) As DataSet
        Return (New RequirementsDB).GetRequirementGroupsByLeadAndPrgVersion(LeadId, PrgVerId, strEnrollDate)
    End Function
    Public Function GetEntranceTestsByGroupAndLead(ByVal LeadId As String, ByVal strEnrollDate As Date, ByVal ReqGrpId As String) As DataSet
        Return (New RequirementsDB).GetEntranceTestsByGroupAndLead(LeadId, strEnrollDate, ReqGrpId)
    End Function
    Public Function GetDocumentsByGroupAndLead(ByVal LeadId As String, ByVal strEnrollDate As Date, ByVal ReqGrpId As String) As DataSet
        Return (New RequirementsDB).GetDocumentsByGroupAndLead(LeadId, strEnrollDate, ReqGrpId)
    End Function
    Public Function GetDocumentsByGroupAndLeadAndPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date, ByVal ReqGrpId As String) As DataSet
        Return (New RequirementsDB).GetDocumentsByGroupAndLeadAndPrgVersion(LeadId, PrgVerId, strEnrollDate, ReqGrpId)
    End Function
    Public Function GetEntranceTestsByGroupAndLeadAndPrgVersion(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date, ByVal ReqGrpId As String) As DataSet
        Return (New RequirementsDB).GetEntranceTestsByGroupAndLeadAndPrgVersion(LeadId, PrgVerId, strEnrollDate, ReqGrpId)
    End Function
    Public Function GetAdmissionRequirements(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date) As String
        Return (New RequirementsDB).GetAdmissionRequirements(LeadId, PrgVerId, strEnrollDate)
    End Function
    Public Function GetSchoolLevelAdmissionRequirements(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date, ByVal CampusId As String) As DataSet
        Dim ds As DataSet
        Dim intOverride As Integer
        Dim mScore As Integer
        ds = (New RequirementsDB).GetSchoolLevelAdmissionRequirements(LeadId, PrgVerId, strEnrollDate, CampusId)

        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables(2)
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("adReqId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))

            For Each row In tbl.Rows
                If row("override") Is DBNull.Value Then
                    intOverride = 0
                Else
                    intOverride = CType(row("override"), Integer)
                End If
                If row("adReqTypeId") = 1 Then
                    If Not row("ActualScore") Is DBNull.Value Then
                        If row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) >= CDbl(row("MinScore")) And intOverride = 0 Then
                            row("FullName") = row("Descrip") & "(Required) (Pass)"
                        ElseIf row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) >= CDbl(row("MinScore")) And intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Pass) (Override)"
                        ElseIf row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) < CDbl(row("MinScore")) And intOverride = 0 Then
                            row("FullName") = row("Descrip") & "(Required) (Fail)"
                        ElseIf row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) < CDbl(row("MinScore")) And intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Fail) (Override)"
                        Else
                            If intOverride = 1 Then
                                row("FullName") = row("Descrip") & "(Required) (Override)"
                            Else
                                row("FullName") = row("Descrip") & "(Required)"
                            End If
                        End If
                    Else
                        If intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Override)"
                        Else
                            row("FullName") = row("Descrip") & "(Required)"
                        End If
                    End If
                Else
                    If Not row("DocStatusDescrip") Is DBNull.Value Then
                        If row("DocStatusDescrip").ToString.Length >= 1 And intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (" & row("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row("DocStatusDescrip").ToString.Length >= 1 And intOverride = 0 Then
                            row("FullName") = row("Descrip") & "(Required) (" & row("DocStatusDescrip") & ")"
                        Else
                            If intOverride = 1 Then
                                row("FullName") = row("Descrip") & "(Required) (Override)"
                            Else
                                row("FullName") = row("Descrip") & "(Required)"
                            End If
                        End If
                    Else
                        If intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Override)"
                        Else
                            row("FullName") = row("Descrip") & "(Required)"
                        End If
                    End If
                End If
            Next
        End If

        Dim tbl1 As DataTable
        Dim row1 As DataRow
        tbl1 = ds.Tables(3)
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("adReqId"), .Columns("LeadGrpId")}
        End With
        If ds.Tables(3).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col1 As DataColumn = tbl1.Columns.Add("FullName", GetType(String))
            For Each row1 In tbl1.Rows
                If row1("override") Is DBNull.Value Then
                    intOverride = 0
                Else
                    intOverride = CType(row1("override"), Integer)
                End If
                If row1("adReqTypeId") = 1 Then
                    '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                    ''If row1.IsNull("minScore") Or row1("MinScore") = "" Then
                    If row1.IsNull("minScore") Or row1("MinScore") = 0 Then
                        '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                        mScore = 0
                    Else
                        mScore = CDbl(row1("MinScore"))
                    End If
                    If Not row1("ActualScore") Is DBNull.Value Then
                        If row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 0 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 0 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Fail)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Fail) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 0 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Fail)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 0 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Fail) (Override)"
                        Else
                            If row1("Required") = 1 And intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = 1 And intOverride = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            row1("FullName") = row1("Descrip")
                        End If
                    End If
                Else
                    If Not row1("DocStatusDescrip") Is DBNull.Value Then
                        If row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & "(Required) (" & row1("DocStatusDescrip") & ")"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & "(Required) (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 0 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 0 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        Else
                            If row1("Required") = 1 And intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = 1 And intOverride = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                ElseIf intOverride = 0 Then
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            If intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Override)"
                            Else
                                row1("FullName") = row1("Descrip")
                            End If
                        End If
                    End If
                End If
            Next
        End If
        Return ds
    End Function
    Public Function GetRequirementsByLeadGroup(ByVal LeadId As String, ByVal PrgVerId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal CampusId As String) As DataSet
        Return (New RequirementsDB).GetRequirementsByLeadGroup(LeadId, PrgVerId, EnrollDate, LeadGrpId, CampusId)
    End Function
    Public Function GetRequirementsByLeadGroupAndReqGroup(ByVal LeadId As String, ByVal PrgVerId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal ReqGrpId As String, ByVal CampusId As String) As DataSet
        Dim ds As DataSet
        ds = (New RequirementsDB).GetRequirementsByLeadGroupAndReqGroup(LeadId, PrgVerId, EnrollDate, LeadGrpId, ReqGrpId, CampusId)

        Dim tbl1 As DataTable
        Dim row1 As DataRow
        Dim intOverride As Integer
        Dim mScore As Integer = 0
        tbl1 = ds.Tables(0)
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("adReqId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            'Add a new column called full name to the data-table 
            Dim col1 As DataColumn = tbl1.Columns.Add("FullName", GetType(String))
            For Each row1 In tbl1.Rows
                If row1("override") Is DBNull.Value Then
                    intOverride = 0
                Else
                    intOverride = CType(row1("override"), Integer)
                End If
                If row1("adReqTypeId") = 1 Then
                    '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                    ''If row1.IsNull("minScore") Or row1("MinScore") = "" Then
                    If row1.IsNull("minScore") Or row1("MinScore") = 0 Then
                        '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                        mScore = 0
                    Else
                        mScore = CDbl(row1("MinScore"))
                    End If
                    If Not row1("ActualScore") Is DBNull.Value Then
                        If row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = True And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = True And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = False And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = False And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = True And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Fail) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = True And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Fail)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = False And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Fail) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = False And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Fail)"
                        Else
                            If row1("Required") = True And intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = True And intOverride = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = True And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = True And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            If intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Override)"
                            Else
                                row1("FullName") = row1("Descrip")
                            End If
                        End If
                    End If
                Else
                    If Not row1("DocStatusDescrip") Is DBNull.Value Then
                        If row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = True And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = True And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (" & row1("DocStatusDescrip") & ")"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = False And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = False And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")"
                        Else
                            If row1("Required") = True And intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = True And intOverride = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        Try
                            If row1("Required") Is DBNull.Value Then
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                                Exit Try
                            End If
                            If row1("Required") = True And intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = True And intOverride = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                End If
            Next
        End If
        Return ds
    End Function
    Public Function GetRequirementsAssignedToLeadReqGroupNoPrgVersion(ByVal LeadId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal CampusId As String) As DataSet
        Dim ds As DataSet
        ds = (New RequirementsDB).GetRequirementsAssignedToLeadReqGroupNoPrgVersion(LeadId, EnrollDate, LeadGrpId, CampusId)
        Dim tbl1 As DataTable
        Dim row1 As DataRow
        Dim intOverRide As Integer
        Dim mScore As Integer
        tbl1 = ds.Tables(0)
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("adReqId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col1 As DataColumn = tbl1.Columns.Add("FullName", GetType(String))
            For Each row1 In tbl1.Rows
                If row1("override") Is DBNull.Value Then
                    intOverRide = 0
                Else
                    intOverRide = CType(row1("override"), Integer)
                End If
                If row1("adReqTypeId") = 1 Then
                    '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                    ''If row1.IsNull("minScore") Or row1("MinScore") = "" Then
                    If row1.IsNull("minScore") Or row1("MinScore") = 0 Then
                        '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                        mScore = 0
                    Else
                        mScore = CDbl(row1("MinScore"))
                    End If
                    If Not row1("ActualScore") Is DBNull.Value Then
                        If row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = False And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = False And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Pass) (Override)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Fail)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Fail) (Override)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = False And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Fail)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = False And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Fail) (Override)"
                        Else
                            If row1("Required") = True And intOverRide = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            ElseIf row1("Required") = True And intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            Else
                                If intOverRide = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            If intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Override)"
                            Else
                                row1("FullName") = row1("Descrip")
                            End If
                        End If
                    End If
                Else
                    If Not row1("DocStatusDescrip") Is DBNull.Value Then
                        If row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (" & row1("DocStatusDescrip") & ")"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = False And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = False And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")"
                        Else
                            If row1("Required") = True And intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = True And intOverRide = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverRide = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            If intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Override)"
                            Else
                                row1("FullName") = row1("Descrip")
                            End If
                        End If
                    End If
                End If
            Next
        End If
        Return ds
    End Function
    Public Function GetRequirementsAssignedToLeadReqGroup(ByVal LeadId As String, ByVal PrgVerId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal CampusId As String) As DataSet
        Dim ds As DataSet
        ds = (New RequirementsDB).GetRequirementsAssignedToLeadReqGroup(LeadId, PrgVerId, EnrollDate, LeadGrpId, CampusId)
        Dim tbl1 As DataTable
        Dim row1 As DataRow
        Dim intOverRide As Integer
        Dim mScore As Integer
        tbl1 = ds.Tables(0)
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("adReqId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col1 As DataColumn = tbl1.Columns.Add("FullName", GetType(String))
            For Each row1 In tbl1.Rows
                If row1("override") Is DBNull.Value Then
                    intOverRide = 0
                Else
                    intOverRide = CType(row1("override"), Integer)
                End If

                If row1("adReqTypeId") = 1 Then
                    '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                    ''If row1.IsNull("minScore") Or row1("MinScore") = "" Then
                    If row1.IsNull("minScore") Or row1("MinScore") = 0 Then
                        '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                        mScore = 0
                    Else
                        mScore = CDbl(row1("MinScore"))
                    End If
                    If Not row1("ActualScore") Is DBNull.Value Then
                        If row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = False And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = False And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Pass) (Override)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Fail)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Fail) (Override)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = False And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Fail)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = False And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Fail) (Override)"
                        Else
                            If row1("Required") = True And intOverRide = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            ElseIf row1("Required") = True And intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            Else
                                If intOverRide = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            If intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Override)"
                            Else
                                row1("FullName") = row1("Descrip")
                            End If
                        End If
                    End If
                Else
                    If Not row1("DocStatusDescrip") Is DBNull.Value Then
                        If row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (" & row1("DocStatusDescrip") & ")"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = False And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = False And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")"
                        Else
                            If row1("Required") = True And intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = True And intOverRide = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverRide = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            If intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Override)"
                            Else
                                row1("FullName") = row1("Descrip")
                            End If
                        End If
                    End If
                End If
            Next
        End If
        Return ds
    End Function
    Public Function GetSchoolLevelAdmissionRequirementsNoProgramVersion(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date, ByVal CampusId As String) As DataSet
        Dim ds As DataSet
        ds = (New RequirementsDB).GetSchoolLevelAdmissionRequirementsWithNoProgramVersion(LeadId, PrgVerId, strEnrollDate, CampusId)

        Dim tbl As DataTable
        Dim intOverride As Integer
        Dim mScore As Integer
        Dim row As DataRow
        tbl = ds.Tables(2)
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("adReqId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))

            For Each row In tbl.Rows
                If row("override") Is DBNull.Value Then
                    intOverride = 0
                Else
                    intOverride = CType(row("override"), Integer)
                End If
                If row("adReqTypeId") = 1 Then
                    If Not row("ActualScore") Is DBNull.Value Then
                        If row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) >= CDbl(row("MinScore")) And intOverride = 0 Then
                            row("FullName") = row("Descrip") & "(Required) (Pass)"
                        ElseIf row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) >= CDbl(row("MinScore")) And intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Pass) (Override)"
                        ElseIf row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) < CDbl(row("MinScore")) And intOverride = 0 Then
                            row("FullName") = row("Descrip") & "(Required) (Fail)"
                        ElseIf row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) < CDbl(row("MinScore")) And intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Fail) (Override)"
                        Else
                            If intOverride = 1 Then
                                row("FullName") = row("Descrip") & "(Required) (Override)"
                            Else
                                row("FullName") = row("Descrip") & "(Required)"
                            End If
                        End If
                    Else
                        If intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Override)"
                        Else
                            row("FullName") = row("Descrip") & "(Required)"
                        End If
                    End If
                Else
                    If Not row("DocStatusDescrip") Is DBNull.Value Then
                        If row("DocStatusDescrip").ToString.Length >= 1 And intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (" & row("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row("DocStatusDescrip").ToString.Length >= 1 And intOverride = 0 Then
                            row("FullName") = row("Descrip") & "(Required) (" & row("DocStatusDescrip") & ")"
                        Else
                            If intOverride = 1 Then
                                row("FullName") = row("Descrip") & "(Required) (Override)"
                            Else
                                row("FullName") = row("Descrip") & "(Required)"
                            End If
                        End If
                    Else
                        If intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Override)"
                        Else
                            row("FullName") = row("Descrip") & "(Required)"
                        End If
                    End If
                End If
            Next
        End If

        Dim tbl1 As DataTable
        Dim row1 As DataRow
        tbl1 = ds.Tables(3)
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("adReqId"), .Columns("LeadGrpId")}
        End With
        If ds.Tables(3).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col1 As DataColumn = tbl1.Columns.Add("FullName", GetType(String))
            For Each row1 In tbl1.Rows
                If row1("override") Is DBNull.Value Then
                    intOverride = 0
                Else
                    intOverride = CType(row1("override"), Integer)
                End If
                If row1("adReqTypeId") = 1 Then
                    '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                    ''If row1.IsNull("minScore") Or row1("MinScore") = "" Then
                    If row1.IsNull("minScore") Or row1("MinScore") = 0 Then
                        '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                        mScore = 0
                    Else
                        mScore = CDbl(row1("MinScore"))
                    End If
                    If Not row1("ActualScore") Is DBNull.Value Then
                        If row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 0 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 0 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Fail)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Fail) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 0 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Fail)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 0 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Fail) (Override)"
                        Else
                            If row1("Required") = 1 And intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = 1 And intOverride = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            row1("FullName") = row1("Descrip")
                        End If
                    End If
                Else
                    If Not row1("DocStatusDescrip") Is DBNull.Value Then
                        If row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & "(Required) (" & row1("DocStatusDescrip") & ")"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & "(Required) (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 0 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 0 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        Else
                            If row1("Required") = 1 And intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = 1 And intOverride = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                ElseIf intOverride = 0 Then
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            If intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Override)"
                            Else
                                row1("FullName") = row1("Descrip")
                            End If
                        End If
                    End If
                End If
            Next
        End If
        Return ds
    End Function
    Public Function GetReqsToLeadApplicants(ByVal LeadId As String, ByVal PrgVerId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal CampusId As String) As DataSet
        Dim ds As DataSet
        ds = (New RequirementsDB).GetRequirementsAssignedToLeadReqGroupNoProgramVersion(LeadId, PrgVerId, EnrollDate, LeadGrpId, CampusId)
        Return ds
    End Function
    Public Function GetStudentList(ByVal ProgId As String, _
                                   ByVal StudentGroup As String, _
                                   Optional ByVal StatusCodeId As String = "", _
                                   Optional ByVal ShiftId As String = "", _
                                   Optional ByVal CampusId As String = "", _
                                   Optional ByVal StudentId As String = "", Optional ByVal reqType As String = "") As DataTable
        Dim dt As DataTable
        dt = (New MissingItemsDB).GetStudentList(ProgId, StudentGroup, StatusCodeId, ShiftId, CampusId, StudentId, reqType)
        dt = ConcatenateNamesInStudentList(dt)
        Return dt
    End Function
    Public Function GetStudentsWithMissingRequirementsAndRequirementGroup(Optional ByVal CampusId As String = "", Optional ByVal ProgId As String = "", Optional ByVal ShiftId As String = "", Optional ByVal EnrollmentStatus As String = "", Optional ByVal StudentGroup As String = "", Optional ByVal reqtype As String = "") As DataTable
        Dim dt As DataTable
        'dt = (New MissingItemsDB).GetStudentsWithMissingRequirementsAndRequirementGroup(CampusId, ProgId, ShiftId, EnrollmentStatus, StudentGroup, reqtype)
        'Return dt

        dt = (New MissingItemsDB).GetStudentsWithMissingRequirementsAndRequirementGroup_Sp(CampusId, ProgId, ShiftId, EnrollmentStatus, StudentGroup, reqtype)
        Return dt
    End Function
    Public Function ConcatenateNamesInStudentList(ByVal dt As DataTable) As DataTable
        Dim row As DataRow
        Dim facInputMasks As New InputMasksFacade
        Dim SSNMask As String
        'With dt
        '    .PrimaryKey = New DataColumn() {.Columns("StudentId")}
        'End With
        If dt.Rows.Count > 0 Then
            SSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = dt.Columns.Add("FullName", GetType(String))
            For Each row In dt.Rows
                row("FullName") = row("FirstName") & " " & row("LastName")
                If row("SSN").ToString.Length >= 1 Then
                    Dim temp As String = row("SSN")
                    row("SSN") = facInputMasks.ApplyMask(SSNMask, "*****" & temp.Substring(5))
                    'dr("SSN") = facInputMasks.ApplyMask(SSNMask, dr("SSN"))
                Else
                    row("SSN") = ""
                End If
            Next
        End If
        Return dt
    End Function
    ''modified by saraswathi Lakshmanan to fix rally case DE5003 Name: QA: Requirement type should show only the requirements tied to the requirement type and not the other missing items.
    ''feb 4 2011

    Public Function GetRequirements(ByVal StudentId As String, Optional ByVal campusid As String = "", Optional ByVal PrgVerId As String = "", Optional ByVal ShiftId As String = "", Optional ByVal EnrollmentStatus As String = "", Optional ByVal reqtype As String = "") As DataTable
        Dim dt As DataTable
        ' dt = (New MissingItemsDB).GetRequirements(StudentId, campusid, PrgVerId, ShiftId, EnrollmentStatus, reqtype)
        dt = (New MissingItemsDB).GetRequirements_Sp(StudentId, campusid, PrgVerId, ShiftId, EnrollmentStatus, reqtype)
        Return dt
    End Function
    Public Function GetRequirementsGroupsByStudent(ByVal StudentId As String, Optional ByVal CampusId As String = "", Optional ByVal PrgVerId As String = "", Optional ByVal ShiftId As String = "", Optional ByVal EnrollmentStatus As String = "", Optional ByVal reqtype As String = "") As DataTable
        Dim dt As New DataTable
        '' dt = (New MissingItemsDB).GetRequirementsGroupsByStudent(StudentId, CampusId, PrgVerId, ShiftId, EnrollmentStatus, reqtype)
        dt = (New MissingItemsDB).GetRequirementsGroupsByStudent_SP(StudentId, CampusId, PrgVerId, ShiftId, EnrollmentStatus, reqtype)
        Return dt
    End Function
    Public Function GetRequirementsByReqGroupAndStudent(ByVal ReqGrpId As String, ByVal StudentId As String, Optional ByVal CampusId As String = "", Optional ByVal ReqType As String = "") As DataTable
        Dim dt As DataTable
        ' dt = (New MissingItemsDB).GetRequirementsByReqGroupAndStudent(ReqGrpId, StudentId, CampusId, ReqType)
        dt = (New MissingItemsDB).GetRequirementsByReqGroupAndStudent_SP(ReqGrpId, StudentId, CampusId, ReqType)
        Return dt
    End Function
    Public Function GetAllOutofSchoolStatus(ByVal CampusId As String, Optional ByVal strStatus As String = "Active") As DataSet
        Dim ds As DataSet
        ds = (New AdReqsDB).GetAllOutofSchoolStatus(CampusId, strStatus)
        Return ds
    End Function
    Public Function GetAllInSchoolStatus(ByVal CampusId As String, Optional ByVal strStatus As String = "Active") As DataSet
        Dim ds As DataSet
        ds = (New AdReqsDB).GetAllInSchoolStatus(CampusId, strStatus)
        Return ds
    End Function
    Public Function GetStatusCodes(ByVal CampusId As String, Optional ByVal strStatus As String = "Active", Optional ByVal StudentEnrollmentStatus As String = "") As DataSet
        Dim ds As DataSet
        ds = (New AdReqsDB).GetStatusCodes(CampusId, strStatus, StudentEnrollmentStatus)
        Return ds
    End Function


    Public Function CheckIfReqGroupMeetsConditions_FinAid_SP(ByVal StuEnrollId As String) As String
        With New RequirementsDB
            Return .CheckIfReqGroupMeetsConditions_FinAid_SP(StuEnrollId)
        End With
    End Function
    Public Function CheckIfReqGroupMeetsConditions_Graduation_SP(ByVal StuEnrollId As String) As String
        With New RequirementsDB
            Return .CheckIfReqGroupMeetsConditions_Graduation_SP(StuEnrollId)
        End With
    End Function
    Public Function CheckIfReqGroupMeetsConditions_Enrollment_SP(ByVal StuEnrollId As String) As String
        With New RequirementsDB
            Return .CheckIfReqGroupMeetsConditions_Enrollment_SP(StuEnrollId)
        End With
    End Function
    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function CheckIfReqGroupMeetsConditionsForStudent(ByVal StudentId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "", Optional ByVal SelectedLeadGroups As String = Nothing) As String
        With New RequirementsDB
            Return .CheckIfReqGroupMeetsConditionsForStudent(StudentId, PrgVerId, CampusId, SelectedLeadGroups)
        End With
    End Function
    ''
    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function GetRequirementsByLeadGroupForStudent(ByVal StudentId As String, ByVal PrgVerId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal CampusId As String, ByVal SelectedLeadGroups As String) As DataSet
        Return (New RequirementsDB).GetRequirementsByLeadGroupForStudent(StudentId, PrgVerId, EnrollDate, LeadGrpId, CampusId, SelectedLeadGroups)
    End Function

    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function GetRequirementsByLeadGroupAndReqGroupForStudent(ByVal StudentId As String, ByVal PrgVerId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal ReqGrpId As String, ByVal CampusId As String, ByVal SelectedLeadGroups As String) As DataSet
        Dim ds As DataSet
        ds = (New RequirementsDB).GetRequirementsByLeadGroupAndReqGroupForStudent(StudentId, PrgVerId, EnrollDate, LeadGrpId, ReqGrpId, CampusId, SelectedLeadGroups)

        Dim tbl1 As DataTable
        Dim row1 As DataRow
        Dim intOverride As Integer
        Dim mScore As Integer
        tbl1 = ds.Tables(0)
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("adReqId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col1 As DataColumn = tbl1.Columns.Add("FullName", GetType(String))
            For Each row1 In tbl1.Rows
                If row1("override") Is DBNull.Value Then
                    intOverride = 0
                Else
                    intOverride = CType(row1("override"), Integer)
                End If
                If row1("adReqTypeId") = 1 Then
                    '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                    ''If row1.IsNull("minScore") Or row1("MinScore") = "" Then
                    If row1.IsNull("minScore") Or row1("MinScore") = 0 Then
                        '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                        mScore = 0
                    Else
                        mScore = CDbl(row1("MinScore"))
                    End If
                    If Not row1("ActualScore") Is DBNull.Value Then
                        If row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = True And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = True And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = False And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = False And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = True And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Fail) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = True And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Fail)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = False And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Fail) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = False And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Fail)"
                        Else
                            If row1("Required") = True And intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = True And intOverride = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = True And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = True And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            If intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Override)"
                            Else
                                row1("FullName") = row1("Descrip")
                            End If
                        End If
                    End If
                Else
                    If Not row1("DocStatusDescrip") Is DBNull.Value Then
                        If row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = True And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = True And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (" & row1("DocStatusDescrip") & ")"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = False And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = False And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")"
                        Else
                            If row1("Required") = True And intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = True And intOverride = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        Try
                            If row1("Required") Is DBNull.Value Then
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                                Exit Try
                            End If
                            If row1("Required") = True And intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = True And intOverride = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                End If
            Next
        End If
        Return ds
    End Function
    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function GetRequirementsAssignedToLeadReqGroupNoPrgVersionforStudent(ByVal StudentId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal CampusId As String, ByVal SelectedLeadGroups As String) As DataSet
        Dim ds As New DataSet
        ds = (New RequirementsDB).GetRequirementsAssignedToLeadReqGroupNoPrgVersionForStudent(StudentId, EnrollDate, LeadGrpId, CampusId, SelectedLeadGroups)
        Dim tbl1 As DataTable
        Dim row1 As DataRow
        Dim intOverRide As Integer
        Dim mScore As Integer
        tbl1 = ds.Tables(0)
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("adReqId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col1 As DataColumn = tbl1.Columns.Add("FullName", GetType(String))
            For Each row1 In tbl1.Rows
                If row1("override") Is DBNull.Value Then
                    intOverRide = 0
                Else
                    intOverRide = CType(row1("override"), Integer)
                End If
                If row1("adReqTypeId") = 1 Then
                    '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                    ''If row1.IsNull("minScore") Or row1("MinScore") = "" Then
                    If row1.IsNull("minScore") Or row1("MinScore") = 0 Then
                        '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                        mScore = 0
                    Else
                        mScore = CDbl(row1("MinScore"))
                    End If
                    If Not row1("ActualScore") Is DBNull.Value Then
                        If row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = False And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = False And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Pass) (Override)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Fail)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Fail) (Override)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = False And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Fail)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = False And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Fail) (Override)"
                        Else
                            If row1("Required") = True And intOverRide = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            ElseIf row1("Required") = True And intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            Else
                                If intOverRide = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            If intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Override)"
                            Else
                                row1("FullName") = row1("Descrip")
                            End If
                        End If
                    End If
                Else
                    If Not row1("DocStatusDescrip") Is DBNull.Value Then
                        If row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (" & row1("DocStatusDescrip") & ")"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = False And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = False And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")"
                        Else
                            If row1("Required") = True And intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = True And intOverRide = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverRide = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            If intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Override)"
                            Else
                                row1("FullName") = row1("Descrip")
                            End If
                        End If
                    End If
                End If
            Next
        End If
        Return ds
    End Function
    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function GetRequirementsAssignedToLeadReqGroupForStudent(ByVal StudentId As String, ByVal PrgVerId As String, ByVal EnrollDate As Date, ByVal LeadGrpId As String, ByVal CampusId As String, ByVal SelectedLeadGroups As String) As DataSet
        Dim ds As DataSet
        ds = (New RequirementsDB).GetRequirementsAssignedToLeadReqGroupForStudent(StudentId, PrgVerId, EnrollDate, LeadGrpId, CampusId, SelectedLeadGroups)
        Dim tbl1 As DataTable
        Dim row1 As DataRow
        Dim intOverRide As Integer
        Dim mScore As Integer = 0
        tbl1 = ds.Tables(0)
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("adReqId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col1 As DataColumn = tbl1.Columns.Add("FullName", GetType(String))
            For Each row1 In tbl1.Rows
                If row1("override") Is DBNull.Value Then
                    intOverRide = 0
                Else
                    intOverRide = CType(row1("override"), Integer)
                End If

                If row1("adReqTypeId") = 1 Then
                    '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                    ''If row1.IsNull("minScore") Or row1("MinScore") = "" Then
                    If row1.IsNull("minScore") Or row1("MinScore") = 0 Then
                        '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                        mScore = 0
                    Else
                        mScore = CDbl(row1("MinScore"))
                    End If
                    If Not row1("ActualScore") Is DBNull.Value Then
                        If row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = False And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = False And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Pass) (Override)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Fail)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Fail) (Override)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = False And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Fail)"
                        ElseIf Not row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = False And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Fail) (Override)"
                        Else
                            If row1("Required") = True And intOverRide = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            ElseIf row1("Required") = True And intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            Else
                                If intOverRide = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            If intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Override)"
                            Else
                                row1("FullName") = row1("Descrip")
                            End If
                        End If
                    End If
                Else
                    If Not row1("DocStatusDescrip") Is DBNull.Value Then
                        If row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required) (" & row1("DocStatusDescrip") & ")"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = False And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = False And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")"
                        Else
                            If row1("Required") = True And intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = True And intOverRide = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverRide = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = True And intOverRide = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = True And intOverRide = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            If intOverRide = 1 Then
                                row1("FullName") = row1("Descrip") & " (Override)"
                            Else
                                row1("FullName") = row1("Descrip")
                            End If
                        End If
                    End If
                End If
            Next
        End If
        Return ds
    End Function

    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function GetSchoolLevelAdmissionRequirementsNoProgramVersionForStudent(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollDate As Date, ByVal CampusId As String, ByVal SelectedLeadGroups As String) As DataSet
        Dim ds As New DataSet
        ds = (New RequirementsDB).GetSchoolLevelAdmissionRequirementsWithNoProgramVersionForStudent(LeadId, PrgVerId, strEnrollDate, CampusId, SelectedLeadGroups)

        Dim tbl As DataTable
        Dim intOverride As Integer
        Dim mScore As Integer = 0
        Dim row As DataRow
        tbl = ds.Tables(2)
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("adReqId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))

            For Each row In tbl.Rows
                If row("override") Is DBNull.Value Then
                    intOverride = 0
                Else
                    intOverride = CType(row("override"), Integer)
                End If
                If row("adReqTypeId") = 1 Then
                    If Not row("ActualScore") Is DBNull.Value Then
                        If row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) >= CDbl(row("MinScore")) And intOverride = 0 Then
                            row("FullName") = row("Descrip") & "(Required) (Pass)"
                        ElseIf row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) >= CDbl(row("MinScore")) And intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Pass) (Override)"
                        ElseIf row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) < CDbl(row("MinScore")) And intOverride = 0 Then
                            row("FullName") = row("Descrip") & "(Required) (Fail)"
                        ElseIf row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) < CDbl(row("MinScore")) And intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Fail) (Override)"
                        Else
                            If intOverride = 1 Then
                                row("FullName") = row("Descrip") & "(Required) (Override)"
                            Else
                                row("FullName") = row("Descrip") & "(Required)"
                            End If
                        End If
                    Else
                        If intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Override)"
                        Else
                            row("FullName") = row("Descrip") & "(Required)"
                        End If
                    End If
                Else
                    If Not row("DocStatusDescrip") Is DBNull.Value Then
                        If row("DocStatusDescrip").ToString.Length >= 1 And intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (" & row("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row("DocStatusDescrip").ToString.Length >= 1 And intOverride = 0 Then
                            row("FullName") = row("Descrip") & "(Required) (" & row("DocStatusDescrip") & ")"
                        Else
                            If intOverride = 1 Then
                                row("FullName") = row("Descrip") & "(Required) (Override)"
                            Else
                                row("FullName") = row("Descrip") & "(Required)"
                            End If
                        End If
                    Else
                        If intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Override)"
                        Else
                            row("FullName") = row("Descrip") & "(Required)"
                        End If
                    End If
                End If
            Next
        End If

        Dim tbl1 As DataTable
        Dim row1 As DataRow
        tbl1 = ds.Tables(3)
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("adReqId"), .Columns("LeadGrpId")}
        End With
        If ds.Tables(3).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col1 As DataColumn = tbl1.Columns.Add("FullName", GetType(String))
            For Each row1 In tbl1.Rows
                If row1("override") Is DBNull.Value Then
                    intOverride = 0
                Else
                    intOverride = CType(row1("override"), Integer)
                End If
                If row1("adReqTypeId") = 1 Then
                    '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                    ''If row1.IsNull("minScore") Or row1("MinScore") = "" Then
                    If row1.IsNull("minScore") Or row1("MinScore") = 0 Then
                        '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                        mScore = 0
                    Else
                        mScore = CDbl(row1("MinScore"))
                    End If
                    If Not row1("ActualScore") Is DBNull.Value Then
                        If row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 0 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 0 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Fail)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Fail) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 0 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Fail)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 0 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Fail) (Override)"
                        Else
                            If row1("Required") = 1 And intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = 1 And intOverride = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            row1("FullName") = row1("Descrip")
                        End If
                    End If
                Else
                    If Not row1("DocStatusDescrip") Is DBNull.Value Then
                        If row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & "(Required) (" & row1("DocStatusDescrip") & ")"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & "(Required) (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 0 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 0 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        Else
                            If row1("Required") = 1 And intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = 1 And intOverride = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                ElseIf intOverride = 0 Then
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            If intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Override)"
                            Else
                                row1("FullName") = row1("Descrip")
                            End If
                        End If
                    End If
                End If
            Next
        End If
        Return ds
    End Function
    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function CheckIfStudentHasPassedRequiredTestWithProgramVersion(ByVal StudentId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "", Optional ByVal SelectedLeadGroups As String = Nothing) As Integer
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.CheckIfStudentHasPassedRequiredTestWithProgramVersion(StudentId, PrgVerId, CampusId, SelectedLeadGroups)
    End Function

    Public Function CheckAllApprovedDocumentsWithPrgVersionForStudent(ByVal StudentId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "", Optional ByVal SelectedLeadGroups As String = Nothing) As Integer
        Dim getDB As New RequirementsDB 'LeadEntranceDB
        Return getDB.CheckAllApprovedDocumentsWithPrgVersionforStudent(StudentId, PrgVerId, CampusId, SelectedLeadGroups)
    End Function
    ''
    '' New code added by kamalesh Ahuja on December 30 for Rally Issue id DE 1276
    Public Function GetSchoolLevelAdmissionRequirementsForStudent(ByVal studentId As String, ByVal prgVerId As String, ByVal strEnrollDate As Date, ByVal campusId As String, ByVal selectedLeadGroup As String) As DataSet
        Dim ds As DataSet
        Dim intOverride As Integer
        Dim mScore As Integer
        ds = (New RequirementsDB).GetSchoolLevelAdmissionRequirementsForStudent(studentId, prgVerId, strEnrollDate, campusId, selectedLeadGroup)

        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables(2)
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("adReqId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            'Add a new column called full name to the data table 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))

            For Each row In tbl.Rows
                If row("override") Is DBNull.Value Then
                    intOverride = 0
                Else
                    intOverride = CType(row("override"), Integer)
                End If
                If row("adReqTypeId") = 1 Then
                    If Not row("ActualScore") Is DBNull.Value Then
                        If row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) >= CDbl(row("MinScore")) And intOverride = 0 Then
                            row("FullName") = row("Descrip") & "(Required) (Pass)"
                        ElseIf row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) >= CDbl(row("MinScore")) And intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Pass) (Override)"
                        ElseIf row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) < CDbl(row("MinScore")) And intOverride = 0 Then
                            row("FullName") = row("Descrip") & "(Required) (Fail)"
                        ElseIf row("ActualScore").ToString.Length >= 1 And row("TestTaken").ToString.Length >= 1 And CDbl(row("ActualScore")) < CDbl(row("MinScore")) And intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Fail) (Override)"
                        Else
                            If intOverride = 1 Then
                                row("FullName") = row("Descrip") & "(Required) (Override)"
                            Else
                                row("FullName") = row("Descrip") & "(Required)"
                            End If
                        End If
                    Else
                        If intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Override)"
                        Else
                            row("FullName") = row("Descrip") & "(Required)"
                        End If
                    End If
                Else
                    If Not row("DocStatusDescrip") Is DBNull.Value Then
                        If row("DocStatusDescrip").ToString.Length >= 1 And intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (" & row("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row("DocStatusDescrip").ToString.Length >= 1 And intOverride = 0 Then
                            row("FullName") = row("Descrip") & "(Required) (" & row("DocStatusDescrip") & ")"
                        Else
                            If intOverride = 1 Then
                                row("FullName") = row("Descrip") & "(Required) (Override)"
                            Else
                                row("FullName") = row("Descrip") & "(Required)"
                            End If
                        End If
                    Else
                        If intOverride = 1 Then
                            row("FullName") = row("Descrip") & "(Required) (Override)"
                        Else
                            row("FullName") = row("Descrip") & "(Required)"
                        End If
                    End If
                End If
            Next
        End If

        Dim tbl1 As DataTable
        Dim row1 As DataRow
        tbl1 = ds.Tables(3)
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("adReqId"), .Columns("LeadGrpId")}
        End With
        If ds.Tables(3).Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col1 As DataColumn = tbl1.Columns.Add("FullName", GetType(String))
            For Each row1 In tbl1.Rows
                If row1("override") Is DBNull.Value Then
                    intOverride = 0
                Else
                    intOverride = CType(row1("override"), Integer)
                End If
                If row1("adReqTypeId") = 1 Then
                    '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                    ''If row1.IsNull("minScore") Or row1("MinScore") = "" Then
                    If row1.IsNull("minScore") Or row1("MinScore") = 0 Then
                        '' New Code Added By Vijay Ramteke On November 02, 2010 For Rally Id DE1192
                        mScore = 0
                    Else
                        mScore = CDbl(row1("MinScore"))
                    End If
                    If Not row1("ActualScore") Is DBNull.Value Then
                        If row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 0 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Pass)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) >= mScore And row1("Required") = 0 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Pass) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Fail)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & "(Required) (Fail) (Override)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 0 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Fail)"
                        ElseIf row1("ActualScore").ToString.Length >= 1 And row1("TestTaken").ToString.Length >= 1 And CDbl(row1("ActualScore")) < mScore And row1("Required") = 0 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Fail) (Override)"
                        Else
                            If row1("Required") = 1 And intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = 1 And intOverride = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                Else
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            row1("FullName") = row1("Descrip")
                        End If
                    End If
                Else
                    If Not row1("DocStatusDescrip") Is DBNull.Value Then
                        If row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & "(Required) (" & row1("DocStatusDescrip") & ")"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & "(Required) (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 0 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")"
                        ElseIf row1("DocStatusDescrip").ToString.Length >= 1 And row1("Required") = 0 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (" & row1("DocStatusDescrip") & ")" & " (Override)"
                        Else
                            If row1("Required") = 1 And intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Required) (Override)"
                            ElseIf row1("Required") = 1 And intOverride = 0 Then
                                row1("FullName") = row1("Descrip") & " (Required)"
                            Else
                                If intOverride = 1 Then
                                    row1("FullName") = row1("Descrip") & " (Override)"
                                ElseIf intOverride = 0 Then
                                    row1("FullName") = row1("Descrip")
                                End If
                            End If
                        End If
                    Else
                        If row1("Required") = 1 And intOverride = 1 Then
                            row1("FullName") = row1("Descrip") & " (Required) (Override)"
                        ElseIf row1("Required") = 1 And intOverride = 0 Then
                            row1("FullName") = row1("Descrip") & " (Required)"
                        Else
                            If intOverride = 1 Then
                                row1("FullName") = row1("Descrip") & " (Override)"
                            Else
                                row1("FullName") = row1("Descrip")
                            End If
                        End If
                    End If
                End If
            Next
        End If
        Return ds
    End Function
End Class

