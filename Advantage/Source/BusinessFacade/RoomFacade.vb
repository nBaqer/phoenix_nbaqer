Public Class RoomFacade
    Public Function GetRooms(ByVal BldgId As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New RoomDB
        '   get the dataset with all degrees
        Return DB.GetRooms(BldgId)

    End Function
    Public Function GetBldgRooms(ByVal BldgId As String)
        With New RoomDB
            Return .GetBldgRooms(BldgId)
        End With
    End Function
End Class
