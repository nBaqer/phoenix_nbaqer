' ===============================================================================
' CRMissingItems.vb
' Retrieves the data to support the Missing Items report
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' Notes: 

Imports System.Text
Imports FAME.Advantage.Common

Public Class CRMissingItems
    Inherits BaseReportFacade

    'Public Function GetReportDataSet_old(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
    '    ' get the sql string to bring in all missing requirements
    '    Dim sb As New StringBuilder()
    '    Dim sbMissingItems As String
    '    If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
    '        sbMissingItems = (New MissingItemsDB).GetStudentsListForMissingItemsReport(rptParamInfo.CampusId, rptParamInfo.FilterList)
    '    Else
    '        sbMissingItems = (New MissingItemsDB).GetStudentsListForMissingItemsReport(rptParamInfo.CampusId, "")
    '    End If
    '    sb.Append(sbMissingItems)
    '    ''TypeofRequirementPostion
    '    'Dim strTypeofRequirementPosition As Integer
    '    'strTypeofRequirementPosition = InStr(rptParamInfo.FilterList, "SyTypeofRequirement.TypeOfReq")

    '    ' sb.Append(sbMissingItems)
    '    If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
    '        Dim strReq As String = "AND SyTypeofRequirement.TypeOfReq = '" + rptParamInfo.FilterList.Substring(InStr(rptParamInfo.FilterList, "AND SyTypeofRequirement.TypeOfReq = '") - 1 + Len("AND SyTypeofRequirement.TypeOfReq = '")).Substring(0, rptParamInfo.FilterList.Substring(InStr(rptParamInfo.FilterList, "AND SyTypeofRequirement.TypeOfReq = '") - 1 + Len("AND SyTypeofRequirement.TypeOfReq = '")).IndexOf("'")) + "'"

    '        Dim strFilter As String = rptParamInfo.FilterList.Replace(strReq, "")
    '        sb.AppendFormat("   AND {0} {1}", strFilter, vbCrLf)

    '        'sb.AppendFormat("   AND {0} {1}", Mid(rptParamInfo.FilterList, 1, strTypeofRequirementPosition - 5), vbCrLf)





    '    End If
    '    sb.Append("order by syCampGrps.CampGrpId,arStuEnrollments.CampusID, LastName, FirstName ")
    '    'sb.Append("( " + vbCrLf)
    '    'sb.Append(GetReqsSQL())
    '    '' add any filters from the report ui
    '    'If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
    '    '    sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterList, vbCrLf)
    '    'End If

    '    'sb.Append(vbCrLf + "union " + vbCrLf + vbCrLf)

    '    'sb.Append(GetReqsForGroupSQL())
    '    '' add any filters from the report ui
    '    'rptParamInfo.FilterList = Replace(rptParamInfo.FilterList, "syCampGrps.CampGrpId in ", "CampGrpId in")
    '    'rptParamInfo.FilterList = Replace(rptParamInfo.FilterList, "adLeadGroups.LeadGrpId", "LeadGrpId")
    '    'If InStr(rptParamInfo.FilterList, "ShiftId") >= 1 Then
    '    '    rptParamInfo.FilterList = Replace(rptParamInfo.FilterList, "arStuEnrollments.ShiftId", "ShiftId")
    '    'End If
    '    'If InStr(rptParamInfo.FilterList, "StatusCodeId") >= 1 Then
    '    '    rptParamInfo.FilterList = Replace(rptParamInfo.FilterList, "arStuEnrollments.StatusCodeId", "StatusCodeId")
    '    'End If
    '    'If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
    '    '    sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterList, vbCrLf)
    '    'End If
    '    'sb.Append(") " + vbCrLf)
    '    'sb.Append("order by CampGrpId, CampusId, LastName, FirstName " + vbCrLf)


    '    Dim db As New FAME.AdvantageV1.DataAccess.DataAccess
    '    db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
    '    Dim ds As New DataSet
    '    If sbMissingItems <> "" Then

    '        ds = db.RunParamSQLDataSet(sb.ToString())
    '        ds.DataSetName = "dsMissingItems"
    '        ds.Tables(0).TableName = "MissingItems"
    '    End If



    '    Return ds
    'End Function

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet


        Dim ds As New DataSet
        If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
            Dim strReq As String = "AND SyTypeofRequirement.TypeOfReq = '" + rptParamInfo.FilterList.Substring(InStr(rptParamInfo.FilterList, "AND SyTypeofRequirement.TypeOfReq = '") - 1 + Len("AND SyTypeofRequirement.TypeOfReq = '")).Substring(0, rptParamInfo.FilterList.Substring(InStr(rptParamInfo.FilterList, "AND SyTypeofRequirement.TypeOfReq = '") - 1 + Len("AND SyTypeofRequirement.TypeOfReq = '")).IndexOf("'")) + "'"


            Dim strFilter As String = rptParamInfo.FilterList.Replace(strReq, "")
    
            Dim StatusCodeid As String = ""
            Dim CampGrpID As String = ""
            Dim LeadGrpID As String = ""
            Dim ShiftID As String = ""

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If



            If strFilter.ToLower.Contains("t1.campgrpid in ") Then
                If strFilter.Substring(strFilter.ToLower.IndexOf("t1.campgrpid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    CampGrpID = strFilter.Substring(strFilter.ToLower.IndexOf("t1.campgrpid in ("), strFilter.Substring(strFilter.ToLower.IndexOf("t1.campgrpid in (")).ToLower.IndexOf(" and ")).ToLower.Replace("t1.campgrpid in", "").ToLower.Replace(")", "").ToLower.Replace("(", "")
                Else
                    CampGrpID = strFilter.Substring(strFilter.ToLower.IndexOf("t1.campgrpid in (")).ToLower.Replace("t1.campgrpid in", "").ToLower.Replace(")", "").ToLower.Replace("(", "")
                End If
            End If

            If strFilter.ToLower.Contains("arstuenrollments.statuscodeid in ") Then
                If strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.statuscodeid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    StatusCodeid = strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.statuscodeid in ("), strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.statuscodeid in (")).ToLower.IndexOf(" and ")).ToLower.Replace("arstuenrollments.statuscodeid in", "").ToLower.Replace(")", "").ToLower.Replace("(", "")
                Else
                    StatusCodeid = strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.statuscodeid in (")).ToLower.Replace("arstuenrollments.statuscodeid in", "").Replace(")", "").ToLower.Replace("(", "")
                End If

            End If
            If strFilter.ToLower.Contains("arstuenrollments.statuscodeid = ") Then
                Dim i = strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ")
                If strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    StatusCodeid = strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.statuscodeid = "), strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.IndexOf(" and ")).ToLower.Replace("arstuenrollments.statuscodeid =", "").ToLower
                Else
                    StatusCodeid = strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.statuscodeid = ")).ToLower.Replace("arstuenrollments.statuscodeid = ", "").ToLower()
                End If

            End If

            If strFilter.ToLower.Contains("adleadgroups.leadgrpid in ") Then
                If strFilter.Substring(strFilter.ToLower.IndexOf("adleadgroups.leadgrpid in")).ToLower.IndexOf(" and ") >= 0 Then
                    LeadGrpID = strFilter.Substring(strFilter.ToLower.IndexOf("adleadgroups.leadgrpid in ("), strFilter.Substring(strFilter.ToLower.IndexOf("adleadgroups.leadgrpid in (")).ToLower.IndexOf(" and ")).ToLower.Replace("adleadgroups.leadgrpid in", "").ToLower.Replace(")", "").ToLower.Replace("(", "")
                Else
                    LeadGrpID = strFilter.Substring(strFilter.ToLower.IndexOf("adleadgroups.leadgrpid in (")).ToLower.Replace("adleadgroups.leadgrpid in", "").ToLower.Replace(")", "").ToLower.Replace("(", "")
                End If
            End If

            If strFilter.ToLower.Contains("adleadgroups.leadgrpid = ") Then
                If strFilter.Substring(strFilter.ToLower.IndexOf("adleadgroups.leadgrpid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    LeadGrpID = strFilter.Substring(strFilter.ToLower.IndexOf("adleadgroups.leadgrpid = "), strFilter.Substring(strFilter.ToLower.IndexOf("adleadgroups.leadgrpid = ")).ToLower.IndexOf(" and ")).ToLower.Replace("adleadgroups.leadgrpid = ", "").ToLower
                Else
                    LeadGrpID = strFilter.Substring(strFilter.ToLower.IndexOf("adleadgroups.leadgrpid = ")).ToLower.Replace("adleadgroups.leadgrpid = ", "").ToLower()
                End If
            End If


            If strFilter.ToLower.Contains("arstuenrollments.shiftid in ") Then
                If strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.shiftid in ")).ToLower.IndexOf(" and ") >= 0 Then
                    ShiftID = strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.shiftid in ("), strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.shiftid in (")).ToLower.IndexOf(" and ")).ToLower.Replace("arstuenrollments.shiftid in", "").ToLower.Replace(")", "").ToLower.Replace("(", "")
                Else
                    ShiftID = strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.shiftid in (")).ToLower.Replace("arstuenrollments.shiftid in", "").ToLower.Replace(")", "").Replace("(", "")
                End If
            End If


            If strFilter.ToLower.Contains("arstuenrollments.shiftid = ") Then
                If strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.shiftid = ")).ToLower.IndexOf(" and ") >= 0 Then
                    ShiftID = strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.shiftid = "), strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.shiftid = ")).ToLower.IndexOf(" and ")).ToLower.Replace("arstuenrollments.shiftid = ", "").ToLower
                Else
                    ShiftID = strFilter.Substring(strFilter.ToLower.IndexOf("arstuenrollments.shiftid = ")).ToLower.Replace("arstuenrollments.shiftid = ", "").ToLower()
                End If
            End If
            Dim db As New SQLDataAccess
            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        
            If rptParamInfo.CampusId = String.Empty Then
                db.AddParameter("@CampusID", DBNull.Value, SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            Else
                db.AddParameter("@CampusID", New Guid(rptParamInfo.CampusId), SqlDbType.UniqueIdentifier, , ParameterDirection.Input)
            End If

            If CampGrpID = String.Empty Then
                db.AddParameter("@CampGrpID", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            Else
                CampGrpID = CampGrpID.Replace(" ", "").Replace("'", "")
                db.AddParameter("@CampGrpID", CampGrpID, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            End If
            If ShiftID = String.Empty Then
                db.AddParameter("@ShiftID", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            Else
                ShiftID = ShiftID.Replace(" ", "").Replace("'", "")
                db.AddParameter("@ShiftID", ShiftID, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            End If
            If StatusCodeid = String.Empty Then
                db.AddParameter("@StatusCodeID", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            Else
                StatusCodeid = StatusCodeid.Replace(" ", "").Replace("'", "")
                db.AddParameter("@StatusCodeID", StatusCodeid, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            End If

            If LeadGrpID = String.Empty Then
                db.AddParameter("@LeadGrpID", DBNull.Value, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            Else
                LeadGrpID = LeadGrpID.Replace(" ", "").Replace("'", "")
                db.AddParameter("@LeadGrpID", LeadGrpID, SqlDbType.VarChar, 8000, ParameterDirection.Input)
            End If




            'If strReq.ToLower.Contains("enrollment") Then
            ds = db.RunParamSQLDataSet_SP("dbo.USP_GetStudentsWithMissingItemsReport_Enroll")
            'ElseIf strReq.ToLower.Contains("graduation") Then
            '    ds = db.RunParamSQLDataSet_SP("dbo.USP_GetStudentsWithMissingItemsReport_Grad")
            'ElseIf strReq.ToLower.Contains("financial aid") Then
            '    ds = db.RunParamSQLDataSet_SP("dbo.USP_GetStudentsWithMissingItemsReport_FinAid")
            'End If


            ds.DataSetName = "dsMissingItems"
            If ds.Tables.Count >= 1 Then
                ds.Tables(0).TableName = "MissingItems"
            End If



        End If


        Return ds
    End Function




#Region "Helper"
#Region "SQL Helpers"

    ''' <summary>
    ''' returns sql for Mandatory Requirements (Documents and Tests)
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function GetReqsSQL() As String
        Dim sb As New StringBuilder()
        sb.Append("select distinct" + vbCrLf)
        sb.Append("     syCampGrps.CampGrpId as CampGrpId, " + vbCrLf)
        sb.Append("     syCampGrps.CampGrpDescrip, " + vbCrLf)
        sb.Append("     arStuEnrollments.CampusId as CampusId, " + vbCrLf)
        sb.Append("     (select CampDescrip from syCampuses where syCampuses.CampusId=arStuEnrollments.CampusId) as CampusDescrip, " + vbCrLf)
        sb.Append("     arStuEnrollments.StuEnrollId, " + vbCrLf)
        sb.Append("     arStudent.StudentId, " + vbCrLf)
        sb.Append("     arStudent.LastName as LastName, " + vbCrLf)
        sb.Append("     arStudent.FirstName as FirstName, " + vbCrLf)
        sb.Append("     arStudent.SSN, " + vbCrLf)
        sb.Append("     arStuEnrollments.PrgVerId, " + vbCrLf)
        sb.Append("     arPrgVersions.PrgVerDescrip, " + vbCrLf)
        sb.Append("     arStuEnrollments.StatusCodeId, " + vbCrLf)
        sb.Append("     syStatusCodes.StatusCodeDescrip, " + vbCrLf)
        sb.Append("     arStuEnrollments.ShiftId, " + vbCrLf)
        sb.Append("     (SELECT ShiftDescrip FROM arShifts WHERE ShiftId=arStuEnrollments.ShiftId) AS ShiftDescrip, " + vbCrLf)
        sb.Append("     arStuEnrollments.EdLvlId, " + vbCrLf)
        sb.Append("     (SELECT EdLvlDescrip FROM adEdLvls WHERE EdLvlId=arStuEnrollments.EdLvlId) AS EdLvlDescrip, " + vbCrLf)
        sb.Append("     adReqs.adReqId, " + vbCrLf)
        sb.Append("     adReqs.Descrip AS ReqDescrip, " + vbCrLf)
        sb.Append("     RT.Descrip AS ReqType, " + vbCrLf)
        sb.Append("     case " + vbCrLf)
        sb.Append("         when (select count(*) from adLeadByLeadGroups LL, adReqLeadGroups L " + vbCrLf)
        sb.Append("                 where LL.StuEnrollId=arStuEnrollments.StuEnrollId " + vbCrLf)
        sb.Append("                 and L.LeadGrpId=LL.LeadGrpId and L.IsRequired=1) > 0 then 1 " + vbCrLf)
        sb.Append("         when T.MandatoryRequirement > 0 then 1 " + vbCrLf)
        sb.Append("         else 0 " + vbCrLf)
        sb.Append("     end as IsRequired, " + vbCrLf)
        sb.Append("     case " + vbCrLf)
        sb.Append("         when (select count(*) from adLeadEntranceTest LED " + vbCrLf)
        sb.Append("             where LED.EntrTestId=adReqs.adReqId and LED.LeadId=arStuEnrollments.LeadId) > 0 then 1 " + vbCrLf)
        sb.Append("         when (select count(*) from arStudent S, plStudentDocs D, syDocStatuses DS, sySysDocStatuses SDS " + vbCrLf)
        sb.Append("             where S.StudentId=D.StudentId and S.StudentId=arStuEnrollments.StudentId and D.DocumentId=adReqs.adReqId " + vbCrLf)
        sb.Append("                 and D.DocStatusId=DS.DocStatusId and DS.SysDocStatusId=SDS.SysDocStatusId) >0 then 1 " + vbCrLf)
        sb.Append("         else 0 " + vbCrLf)
        sb.Append("     end as IsFullfilled " + vbCrLf)
        sb.Append("from " + vbCrLf)
        sb.Append("     arStudent, " + vbCrLf)
        sb.Append("     arStuEnrollments, " + vbCrLf)
        sb.Append("     arPrgVersions, " + vbCrLf)
        sb.Append("     syStatusCodes, " + vbCrLf)
        sb.Append("     syCampGrps, " + vbCrLf)
        sb.Append("     adReqs, " + vbCrLf)
        sb.Append("     adReqsEffectiveDates T, " + vbCrLf)
        sb.Append("     adReqTypes RT, " + vbCrLf)
        sb.Append("     syStatuses S, " + vbCrLf)
        sb.Append("     adLeadByLeadGroups, " + vbCrLf)
        sb.Append("     adLeadGroups " + vbCrLf)
        sb.Append("where " + vbCrLf)
        sb.Append("     arStudent.StudentId = arStuEnrollments.StudentId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.StatusCodeId = syStatusCodes.StatusCodeId " + vbCrLf)
        sb.Append("     AND adLeadGroups.LeadGrpId = adLeadByLeadGroups.LeadGrpId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.StuEnrollId = adLeadByLeadGroups.StuEnrollId " + vbCrLf)
        sb.Append("     AND arPrgVersions.CampGrpId in (select distinct t1.CampGrpId from syCmpGrpCmps t1, syCmpGrpCmps t2 where t1.CampusId=t2.CampusId and t2.CampGrpId=syCampGrps.CampGrpId) " + vbCrLf)
        sb.Append("     AND adReqs.CampGrpId in (select distinct t1.CampGrpId from syCmpGrpCmps t1, syCmpGrpCmps t2 where t1.CampusId=t2.CampusId and t2.CampGrpId=syCampGrps.CampGrpId) " + vbCrLf)
        sb.Append("     AND adReqs.adReqId = T.adReqId " + vbCrLf)
        sb.Append("     AND adReqs.adReqTypeId = RT.adReqTypeId " + vbCrLf)
        sb.Append("     AND adReqs.StatusId = S.StatusId AND S.Status='Active' " + vbCrLf)
        sb.Append("     AND T.StartDate <= getdate() AND (T.EndDate IS NULL OR T.EndDate >= getdate()) " + vbCrLf)
        sb.Append("     AND ( " + vbCrLf)
        sb.Append("         -- include if it is a global requirement " + vbCrLf)
        sb.Append("         T.MandatoryRequirement = 1 " + vbCrLf)
        sb.Append("         or " + vbCrLf)
        sb.Append("         -- include if part of the student group requirements " + vbCrLf)
        sb.Append("         (select count(*) from adLeadByLeadGroups LL, adReqLeadGroups L " + vbCrLf)
        sb.Append("          where T.adReqId=adReqs.adReqId and T.adReqEffectiveDateId=L.adReqEffectiveDateId and " + vbCrLf)
        sb.Append("                 LL.StuEnrollId=arStuEnrollments.StuEnrollId and L.LeadGrpId=LL.LeadGrpId and L.IsRequired=1) > 0 " + vbCrLf)
        sb.Append("         or " + vbCrLf)
        sb.Append("         -- include if part of the program version requirements " + vbCrLf)
        sb.Append("         (select count(*) from adPrgVerTestDetails PVT where PVT.adReqId=adReqs.adReqId " + vbCrLf)
        sb.Append("             and PVT.PrgVerId in (select PV.PrgVerId from arPrograms P, arPrgVersions PV where PV.ProgId=P.ProgId " + vbCrLf)
        sb.Append("                     and P.ProgId=arPrgVersions.ProgId)) > 0 " + vbCrLf)
        sb.Append("         ) " + vbCrLf)
        sb.Append("     -- include only reqs that have not been fullfilled " + vbCrLf)
        sb.Append("     AND (case " + vbCrLf)
        sb.Append("         when (select count(*) from adLeadEntranceTest LED " + vbCrLf)
        sb.Append("             where LED.EntrTestId=adReqs.adReqId and LED.LeadId=arStuEnrollments.LeadId) > 0 then 1 " + vbCrLf)
        sb.Append("         when (select count(*) from arStudent S, plStudentDocs D, syDocStatuses DS, sySysDocStatuses SDS " + vbCrLf)
        sb.Append("             where S.StudentId=D.StudentId and S.StudentId=arStuEnrollments.StudentId and D.DocumentId=adReqs.adReqId " + vbCrLf)
        sb.Append("                 and D.DocStatusId=DS.DocStatusId and DS.SysDocStatusId=SDS.SysDocStatusId) > 0 then 1 " + vbCrLf)
        sb.Append("         else 0 " + vbCrLf)
        sb.Append("         end) = 0 " + vbCrLf)
        Return sb.ToString()
    End Function
    Protected Function GetReqsForGroupSQL() As String
        Dim sb As New StringBuilder()
        'sb.Append("select distinct " + vbCrLf)
        'sb.Append("     syCampGrps.CampGrpId, " + vbCrLf)
        'sb.Append("     syCampGrps.CampGrpDescrip, " + vbCrLf)
        'sb.Append("     arStuEnrollments.CampusId, " + vbCrLf)
        'sb.Append("     (select CampDescrip from syCampuses where syCampuses.CampusId=arStuEnrollments.CampusId) as CampusDescrip, " + vbCrLf)
        'sb.Append("     arStuEnrollments.StuEnrollId, " + vbCrLf)
        'sb.Append("     arStudent.StudentId, " + vbCrLf)
        'sb.Append("     arStudent.LastName, " + vbCrLf)
        'sb.Append("     arStudent.FirstName, " + vbCrLf)
        'sb.Append("     arStudent.SSN, " + vbCrLf)
        'sb.Append("     arStuEnrollments.PrgVerId, " + vbCrLf)
        'sb.Append("     arPrgVersions.PrgVerDescrip, " + vbCrLf)
        'sb.Append("     arStuEnrollments.StatusCodeId, " + vbCrLf)
        'sb.Append("     syStatusCodes.StatusCodeDescrip, " + vbCrLf)
        'sb.Append("     arStuEnrollments.ShiftId, " + vbCrLf)
        'sb.Append("     (SELECT ShiftDescrip FROM arShifts WHERE ShiftId=arStuEnrollments.ShiftId) AS ShiftDescrip, " + vbCrLf)
        'sb.Append("     arStuEnrollments.EdLvlId, " + vbCrLf)
        'sb.Append("     (SELECT EdLvlDescrip FROM adEdLvls WHERE EdLvlId=arStuEnrollments.EdLvlId) AS EdLvlDescrip, " + vbCrLf)
        'sb.Append("     T.ReqGrpId as adReqId, " + vbCrLf)
        'sb.Append("     RG.Descrip AS ReqDescrip, " + vbCrLf)
        'sb.Append("     'Group' AS ReqType, " + vbCrLf)
        'sb.Append("     RG.IsMandatoryReqGrp as IsRequired,	" + vbCrLf)
        'sb.Append("     (case when " + vbCrLf)
        'sb.Append("         (select count(*) " + vbCrLf)
        'sb.Append("             from adReqGrpDef RGD, adReqs R, adReqsEffectiveDates RED, plStudentDocs SD " + vbCrLf)
        'sb.Append("             where RGD.adReqId=R.adReqId and RGD.adReqId=RED.adReqId " + vbCrLf)
        'sb.Append("                 and (R.adReqTypeId=3 or R.adReqTypeId=1) and RGD.adReqId=SD.DocumentId " + vbCrLf)
        'sb.Append("                 and RED.StartDate <= getdate() and (RED.EndDate IS NULL OR RED.EndDate >= getdate()) " + vbCrLf)
        'sb.Append("                 and SD.DocStatusId IN (SELECT DocStatusId FROM syDocStatuses D,sySysDocStatuses S WHERE D.SysDocStatusId=S.SysDocStatusId AND S.DocStatusDescrip='Approved') " + vbCrLf)
        'sb.Append("                 and SD.StudentId=arStudent.StudentId " + vbCrLf)
        'sb.Append("                 and RGD.ReqGrpid=T.ReqGrpId " + vbCrLf)
        'sb.Append("                 and RGD.LeadGrpId=L.LeadGrpId) >= L.NumReqs " + vbCrLf)
        'sb.Append("      then 1 else 0 end) as IsFullfilled	" + vbCrLf)
        'sb.Append("from " + vbCrLf)
        'sb.Append("     arStudent, " + vbCrLf)
        'sb.Append("     arStuEnrollments, " + vbCrLf)
        'sb.Append("     arPrgVersions, " + vbCrLf)
        'sb.Append("     syStatusCodes, " + vbCrLf)
        'sb.Append("     syCampGrps, " + vbCrLf)
        'sb.Append("     adReqGroups RG, " + vbCrLf)
        'sb.Append("     adPrgVerTestDetails T, " + vbCrLf)
        'sb.Append("     adLeadGrpReqGroups L, " + vbCrLf)
        'sb.Append("     adLeadByLeadGroups, " + vbCrLf)
        'sb.Append("     adLeadGroups " + vbCrLf)
        'sb.Append("where " + vbCrLf)
        'sb.Append("     arStudent.StudentId = arStuEnrollments.StudentId " + vbCrLf)
        'sb.Append("     AND arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId " + vbCrLf)
        'sb.Append("     AND arStuEnrollments.StatusCodeId = syStatusCodes.StatusCodeId " + vbCrLf)
        'sb.Append("     AND adLeadGroups.LeadGrpId = adLeadByLeadGroups.LeadGrpId " + vbCrLf)
        'sb.Append("     AND arStuEnrollments.StuEnrollId = adLeadByLeadGroups.StuEnrollId " + vbCrLf)
        'sb.Append("     AND arPrgVersions.CampGrpId in (select distinct t1.CampGrpId from syCmpGrpCmps t1, syCmpGrpCmps t2 where t1.CampusId=t2.CampusId and t2.CampGrpId=syCampGrps.CampGrpId) " + vbCrLf)
        'sb.Append("     AND RG.StatusId in (select S.StatusId from syStatuses S where S.Status='Active') " + vbCrLf)
        'sb.Append("     AND RG.ReqGrpId = T.ReqGrpid " + vbCrLf)
        'sb.Append("     AND T.PrgVerId in (select PV.PrgVerId from arPrograms P, arPrgVersions PV where PV.ProgId=P.ProgId and P.ProgId=arPrgVersions.ProgId) " + vbCrLf)
        'sb.Append("     AND RG.ReqGrpId = L.ReqGrpId " + vbCrLf)
        'sb.Append("     AND L.LeadGrpId in (select distinct LeadGrpId from adLeadByLeadGroups  LL where LL.StuEnrollId=arStuEnrollments.StuEnrollid) " + vbCrLf)
        'sb.Append("     -- Filter only reqs that are incomplete " + vbCrLf)
        'sb.Append("     AND (case when " + vbCrLf)
        'sb.Append("         (select count(*) " + vbCrLf)
        'sb.Append("             from adReqGrpDef RGD, adReqs R, adReqsEffectiveDates RED, plStudentDocs SD " + vbCrLf)
        'sb.Append("             where RGD.adReqId=R.adReqId and RGD.adReqId=RED.adReqId " + vbCrLf)
        'sb.Append("                 and (R.adReqTypeId=3 or R.adReqTypeId=1) and RGD.adReqId=SD.DocumentId " + vbCrLf)
        'sb.Append("                 and RED.StartDate <= getdate() and (RED.EndDate IS NULL OR RED.EndDate >= getdate()) " + vbCrLf)
        'sb.Append("                 and SD.DocStatusId IN (SELECT DocStatusId FROM syDocStatuses D,sySysDocStatuses S WHERE D.SysDocStatusId=S.SysDocStatusId AND S.DocStatusDescrip='Approved') " + vbCrLf)
        'sb.Append("                 and SD.StudentId=arStudent.StudentId " + vbCrLf)
        'sb.Append("                 and RGD.ReqGrpid=T.ReqGrpId " + vbCrLf)
        'sb.Append("                 and RGD.LeadGrpId=L.LeadGrpId) >= L.NumReqs " + vbCrLf)
        'sb.Append("         then 1 else 0 end) = 0 " + vbCrLf)

        Dim strCurrentDate As String = Date.Now.ToShortDateString.ToString
        With sb
            '.Append(" select ReqGrpId as adReqId,Descrip as ReqDescrip,  ")
            '.Append(" FirstName,LastName,SSN,PrgVerId,PrgVerDescrip,StatusCodeId,StatusCodeDescrip,ShiftId,ShiftDescrip, ")
            '.Append(" EdLvlId,EdLvlDescrip,CampusId,CampDescrip,CampGrpId,CampGrpDescrip,IsRequired, ")
            '.Append(" 'Group' as ReqType,0 as IsFullFilled ")
            .Append(" select CampGrpId,CampGrpDescrip,CampusId,CampDescrip as CampusDescrip,NULL as StuEnrollId, ")
            .Append(" StudentId,LastName,FirstName,SSN,PrgVerId,PrgVerDescrip,StatusCodeId, ")
            .Append(" StatusCodeDescrip,ShiftId,ShiftDescrip,  EdLvlId,EdLvlDescrip, ")
            .Append(" ReqGrpId as adReqId,Descrip as ReqDescrip, 'Group' as ReqType, ")
            .Append(" IsRequired,0 as IsFullFilled  ")
            .Append(" from ")
            .Append(" (     ")
            'TestDocumentAttempted removed from the count query by Balaji on 4/23/2007
            .Append(" select ReqGrpId,R5.Descrip,R5.NumReqs,IsNull(R5.TestPassed,0)+IsNULL(R5.DocsApproved,0)+IsNULL(R5.TestOverriden,0)+IsNULL(R5.DocumentOverriden,0) as AttemptedReqs,  ")
            .Append(" StudentId,FirstName,LastName,SSN,PrgVerId,PrgVerDescrip,StatusCodeId,StatusCodeDescrip,ShiftId,ShiftDescrip, ")
            .Append(" EdLvlId,EdLvlDescrip,CampusId,CampDescrip,CampGrpId,CampGrpDescrip,IsRequired, ")
            .Append(" Case when LeadGrpId is NULL then '00000000-0000-0000-0000-000000000000' else LeadGrpId end as LeadGrpId ")
            .Append(" from ")
            '*********************************************************************************************************************************************
            ' This Query gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version 
            '*********************************************************************************************************************************************
            .Append(" (Select  Distinct t1.ReqGrpId,t2.Descrip,t2.CampGrpId,t3.NumReqs as Numreqs,  ")
            .Append(" t6.StudentId as StudentId,t6.FirstName as FirstName,t6.LastName as LastName,t6.SSN as SSN, ")
            .Append(" t5.PrgVerId,(select Distinct PrgVerDescrip from arPrgVersions where PrgVerId=t5.PrgVerId) as PrgVerDescrip, ")
            .Append(" t5.StatusCodeId,(select Distinct StatusCodeDescrip from syStatusCodes where StatusCodeId=t5.StatusCodeId) as StatusCodeDescrip, ")
            .Append(" t5.ShiftId,(select Distinct ShiftDescrip from arShifts where ShiftId=t5.ShiftId) as ShiftDescrip, ")
            .Append(" t5.EdLvlId,(SELECT Distinct EdLvlDescrip FROM adEdLvls WHERE EdLvlId=t5.EdLvlId) AS EdLvlDescrip, ")
            .Append(" t5.CampusId,(select Distinct CampDescrip from syCampuses where CampusId=t5.CampusId) as CampDescrip, ")
            .Append(" t7.CampGrpDescrip,t2.IsMandatoryReqGrp as IsRequired, ")
            ''''''''''''' This Query gives the number of test passed by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId ")
            .Append("  and A4.LeadGrpId in 	(select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId=t6.StudentId) ")
            .Append("  and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=1) ")
            .Append(" R1,adLeadEntranceTest R2,arStudent R3 WHERE ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.StudentId = R3.StudentId and R3.StudentId=t6.StudentId and R1.CurrentDate >= R1.StartDate and Len(R2.TestTaken) >=4 and R2.Pass=1  ")
            .Append("  and R2.EntrTestId not in (select Distinct EntrTestId from adEntrTestOverRide t1,arStudent t2 where t1.StudentId=t2.StudentId and OverRide=1 and t2.StudentId=t6.StudentId) ")
            .Append(" and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestPassed, ")
            '''''''''''' This Query gives the number of overriden test
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId ")
            .Append(" and A4.LeadGrpId in (select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId=t6.StudentId) ")
            .Append(" and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=1) ")
            .Append(" R1,adEntrTestOverride R2,arStudent R3 WHERE ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.StudentId = R3.StudentId and R3.StudentId=t6.StudentId and R2.Override=1 and R1.CurrentDate >= R1.StartDate   ")
            .Append(" and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as TestOverriden, ")
            '''''''''''' This Query gives the number of overriden documents
            .Append(" (select Count(*) from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId ")
            .Append(" and A4.LeadGrpId in 	(select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId=t6.StudentId) ")
            .Append(" and ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=3) ")
            .Append(" R1,adEntrTestOverride R2,arStudent R3 WHERE ")
            .Append(" R1.adReqId = R2.EntrTestId and R2.StudentId = R3.StudentId and R3.StudentId=t6.StudentId and R2.Override=1 and R1.CurrentDate >= R1.StartDate   ")
            .Append(" and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as DocumentOverriden, ")
            ''''''''''''' This Query gives the number of documents approved by the lead and grouped by lead group ''''''''
            .Append(" (select Count(*) from  ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId  ")
            .Append(" and A4.LeadGrpId in 	(select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId=t6.StudentId) ")
            .Append(" and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId=3) R1,plStudentDocs R2,syDocStatuses R3,arStudent R4 WHERE ")
            .Append(" R1.adReqId = R2.DocumentId and R2.StudentId=R4.StudentId and R4.StudentId=t6.StudentId  ")
            .Append("  and R2.DocumentId not in (select Distinct EntrTestId from adEntrTestOverRide t1,arStudent t2 where t1.StudentId=t2.StudentId and OverRide=1 and t2.StudentId=t6.StudentId) ")
            .Append(" and R2.DocStatusId = R3.DocStatusId and R3.SysDocStatusId=1 and R1.CurrentDate >= R1.StartDate and (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL)) as DocsApproved, ")
            '''''''''''' This Query gives the number of test and documents attempted(no matter pass or fail,no matter approved/not approved) grouped by lead group ''''''''
            .Append(" (select Count(*)  from ")
            .Append(" (select Distinct A2.adReqId,'" & strCurrentDate & "' as CurrentDate,A3.StartDate,A3.EndDate,A4.LeadGrpId ")
            .Append(" from adReqGrpDef A1,adReqs A2,adReqsEffectiveDates A3,adReqLeadGroups A4 ")
            .Append(" where A1.adReqId = A2.adReqId and A2.adReqId = A3.adReqId and A3.adReqEffectiveDateId = A4.adReqEffectiveDateId ")
            .Append(" and A4.LeadGrpId in 	(select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId=t6.StudentId) ")
            .Append("  and ")
            .Append(" ReqGrpId = t1.ReqGrpId and A2.adreqTypeId in (1,3)) R1,adEntrTestOverRide R2,arStudent R3 ")
            .Append(" where R1.adReqId = R2.EntrTestId and R2.StudentId = R3.StudentId and R3.StudentId=t6.StudentId  and R1.CurrentDate >= R1.StartDate and R2.OverRide=1 and ")
            .Append(" (R1.CurrentDate <= R1.EndDate or R1.EndDate is NULL) Group BY LeadGrpId) as TestDocumentAttempted,t4.LeadGrpId ")
            .Append(" from adPrgVerTestDetails t1,adReqGroups t2,adLeadGrpReqGroups t3,adLeadGroups t4,arStuEnrollments t5,arStudent t6,syCampGrps t7  where ")
            .Append(" t1.ReqGrpId = t2.ReqGrpId and t2.ReqGrpId = t3.ReqGrpId and t3.LeadGrpId = t4.LeadGrpId and t1.PrgVerId=t5.PrgVerId and t5.StudentId=t6.StudentId and t2.CampGrpId=t7.CampGrpId ")
            .Append(" and t2.IsMandatoryReqGrp <> 1 ")
            .Append(" and t3.LeadGrpId in 	(select Distinct LeadGrpId from adLeadByLeadGroups t1,arStuEnrollments t2,arStudent t3 where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and t3.StudentId=t6.StudentId) ")
            .Append(" ) R5  ")
            'where LeadGrpId='" & LeadGrpId & "'
            'If Not strCampGrpId = "" Then
            '    .Append("  where R5.CampGrpId in 	('")
            '    .Append(strCampGrpId)
            '    .Append(") ")
            'End If
            .Append("    ) R8 ")
            .Append("   where AttemptedReqs < NumReqs ")
            '**************************************************************************************************************************************************************
            'End of the  Query that gives the Requirement Group and Number of Test Passed,Document Approved,Requirements Attempted by Lead and Program Version
            '**************************************************************************************************************************************************************
        End With

        Return sb.ToString()
    End Function
#End Region

#End Region

End Class
