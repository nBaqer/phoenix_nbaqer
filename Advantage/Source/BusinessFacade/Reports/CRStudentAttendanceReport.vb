' ===============================================================================
' CRStudentAttendanceReport.vb
' Retrieves the data to support the student attendance report
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' Notes: Fills up a DataSet suitable for use with Advantage Crystal Report architecture
' Return is for all enrollments.  That is, we do not filter on StuEnrollId

Imports System.Text
Imports FAME.Advantage.Common

Public Class CRStudentAttendanceReport
    Inherits BaseReportFacade

    Private ReadOnly myAdvAppSettings As  FAME.Advantage.Common.AdvAppSettings
#Region "Public Methods"

    Sub New()
        myAdvAppSettings = AdvAppSettings.GetAppSettings()

    End Sub

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim params() As String = rptParamInfo.FilterOther.Split(" ")
        Dim StudentId As String = params(2).Replace(";", "")
        ''New Code Added By Vijay Ramteke On Septembaer 09, 2010 From Mantis Id 13650
        Dim StuEnrollId As String = ""
        ''New Code Added By Vijay Ramteke On Septembaer 09, 2010 From Mantis Id 13650


        

        ' check if the StuEnrollId as been passed.  If it has, force it to the StudentId
        If params(0).ToLower = "arstuenrollments.stuenrollid" Then
            StuEnrollId = StudentId
            StudentId = "'" + GetStudentId(StudentId) + "'"
        End If

        ' create a datatable with the proper schema
        Dim dtRes As DataTable = CreateReportDataTableSchema() ' Final result will be held in dtRes.

        If StudentId <> "" Then
            ' Get all the class section and meeting days
            Dim usePeriods As Boolean = (Not MyAdvAppSettings.AppSettings("SchedulingMethod", rptParamInfo.CampusId) Is Nothing AndAlso MyAdvAppSettings.AppSettings("SchedulingMethod", rptParamInfo.CampusId).ToString <> "Traditional")
            Dim dtMeetDays As DataTable = GetClsSectionMeetingDays(StudentId, usePeriods, StuEnrollId)


            ' Get all the classes for this student
            'Dim clsSectionIds(dtMeetDays.Rows.Count - 1) As String
            'Dim clsSectionMeetingIds(dtMeetDays.Rows.Count - 1) As String
            Dim clsSectionMeetingIds() As String
            Dim b As New List(Of String)

            For i As Integer = 0 To dtMeetDays.Rows.Count - 1
                'Only add if the meeting is not there already
                If Not b.Contains(dtMeetDays.Rows(i)("ClsSectMeetingId").ToString()) Then
                    b.Add(dtMeetDays.Rows(i)("ClsSectMeetingId").ToString())
                End If
            Next

            clsSectionMeetingIds = b.ToArray

            If clsSectionMeetingIds.Length > 0 Then
                ' Get the data ranges for all classes belonging student
                'Dim dtDateRange As DataTable = GetClsSectionDateRange(clsSectionMeetingIds, StudentId, StuEnrollId)
                Dim dtDateRange As DataTable = GetClsSectionDateRange(dtMeetDays)

                ' Get the attendance record for a student and fill up dtRes with the proper data
                Dim dtAttendance As DataTable = GetStudentAttendance(rptParamInfo, StudentId, StuEnrollId)

                ' Now that we have all the right data, we can build up the
                ' final DataTable that gets returned to the report engine.  

                ' Build up dtRes with rows.  Each row will hold the proper meeting days of a class.
                If dtAttendance.Rows.Count > 0 Then
                    If usePeriods Then
                        CreateMeetingDaysPeriods(dtRes, dtMeetDays, dtDateRange, rptParamInfo.CampusId)
                    Else
                        CreateMeetingDays(dtRes, dtMeetDays, dtDateRange, rptParamInfo.CampusId)
                    End If

                    ' set all the attendance information in dtRes
                    'dtRes = GetUnscheduledAttendance(dtRes, dtAttendance)
                    FillAttendanceInfo(dtRes, dtAttendance)
                End If
            End If
        End If

        ' Return the resulting dataset
        Dim ds As New DataSet("dsStudentAttendance")
        ds.Tables.Add(dtRes.Copy())

        ''' Code modified by kamalesh ahuja on 15 Sept 2010 to sort by date for mantis id 13650
        'ds.Tables(0).TableName = "StudentAttendance"

        Dim dt As New DataTable
        Dim DV As New DataView
        DV = ds.Tables(0).DefaultView
        DV.Sort = "ClassStartDate"

        dt = DV.ToTable()

        ds.Tables.RemoveAt(0)
        ds.Tables.Add(dt)
        ds.AcceptChanges()
        ds.Tables(0).TableName = "StudentAttendance"
        ''''''''

        Return ds
    End Function
    Private Function GetUnscheduledAttendance(dtRes As DataTable, dtAttendance As DataTable) As DataTable
        For Each dr As DataRow In dtAttendance.Rows
            Dim clsSectionId As String = dr("ClsSectionId").ToString()
            Dim MeetDate As DateTime = dr("MeetDate")
            Dim drClass() As DataRow = dtRes.Select(String.Format("ClsSectionId='{0}' and MeetDate='{1}'", clsSectionId, MeetDate))
            If drClass.Length = 0 Then
                Dim str As String = String.Empty
            End If

        Next


        Return dtRes

    End Function

#End Region

#Region "Private Helper Methods"

#Region "Methods to fill up the resulting dataset"
    ''' <summary>
    ''' Adds rows to dtRes for all the meeting days for each class
    ''' </summary>
    ''' <param name="dtRes"></param>
    ''' <param name="dtMeetdays"></param>
    ''' <param name="dtDateRange"></param>
    ''' <remarks></remarks>
    Private Sub CreateMeetingDays(ByVal dtRes As DataTable, ByVal dtMeetdays As DataTable, ByVal dtDateRange As DataTable, Optional ByVal campusId As String = "")

     

        Dim usePeriods As Boolean = (Not MyAdvAppSettings.AppSettings("SchedulingMethod", campusId) Is Nothing AndAlso MyAdvAppSettings.AppSettings("SchedulingMethod", campusId).ToString <> "Traditional")
        Dim holidayFac As New HolidayFacade()

        For Each drClass As DataRow In dtDateRange.Rows
            Dim clsSectionId As String = drClass("ClsSectionId").ToString()
            Dim ClsSectMeetingId As String = drClass("ClsSectMeetingId").ToString()
            Dim startDate As DateTime = CType(drClass("StartDate"), DateTime)
            Dim rendDate As DateTime = CType(drClass("EndDate"), DateTime) ' unadjusted end date
            Dim endDate As DateTime = CType(drClass("EndDate"), DateTime)
            ' restrict the endDate to today's date
            If endDate > Date.Today Then endDate = Date.Today
            Dim drClsMeetDays() As DataRow = dtMeetdays.Select(String.Format("ClsSectMeetingId='{0}'", ClsSectMeetingId))
            Dim clsAttFac As New ClsSectAttendanceFacade()
            ' for this class, go from the start date to the end date and 
            ' add rows when the date is part of a meeting

            Dim arrDates As ArrayList = clsAttFac.GetMeetDatesForMeetingsArrayListFromRange(drClsMeetDays, clsSectionId, ClsSectMeetingId, startDate.ToString, endDate.ToString, campusId)

            Dim meetNum As Integer = 0
            For Each d As Date In arrDates
                'Dim bFound = False
                'For Each drDay As DataRow In drClsMeetDays
                '    If Not bFound AndAlso drDay("WorkDaysDescrip").ToString().ToLower() = d.ToString("ddd").ToLower() Then
                '        bFound = True
                Dim drDay As DataRow = drClsMeetDays(0)
                Dim drNew As DataRow = dtRes.NewRow()
                drNew("TermId") = drDay.Item("TermId")
                drNew("TermDescrip") = drDay.Item("TermDescrip")
                drNew("ClsSectionId") = clsSectionId
                drNew("ClsSectMeetingId") = ClsSectMeetingId
                drNew("ReqId") = drDay.Item("ReqId")
                drNew("ClassName") = drDay.Item("Descrip")
                drNew("InstructorId") = drDay.Item("InstructorId")
                drNew("InstructorName") = drDay.Item("FullName")
                drNew("ClassStartDate") = startDate
                drNew("ClassEndDate") = rendDate
                drNew("MeetDate") = d
                drNew("MeetNumber") = meetNum
                drNew("AttendanceType") = "PA" ' TODO
                drNew("NumTardiesMakingAbsent") = 0
                drNew("PrgVerDescrip") = drDay.Item("PrgVerDescrip")
                drNew("InstructionTypeID") = drDay("InstructionTypeID")
                drNew("ClsSectMeetingDesc") = drDay("InstructionTypeDescrip") & " - " & drDay("WorkDaysDescrip")
                drNew("TimeIntervalDescrip") = CDate(drDay("TimeIntervalDescrip")).TimeOfDay
                meetNum += 1
                dtRes.Rows.Add(drNew)
                ' End If
                'Next
            Next

            'Dim iDate As DateTime = startDate
            'Dim meetNum As Integer = 0
            'While iDate <= endDate
            '    For Each drDay As DataRow In drClsMeetDays
            '        If Not holidayFac.IsHoliday(iDate) AndAlso _
            '            drDay("WorkDaysDescrip").ToString().ToLower() = iDate.ToString("ddd").ToLower() Then
            '            Dim drNew As DataRow = dtRes.NewRow()
            '            drNew("TermId") = drDay.Item("TermId")
            '            drNew("TermDescrip") = drDay.Item("TermDescrip")
            '            drNew("ClsSectionId") = clsSectionId
            '            drNew("ReqId") = drDay.Item("ReqId")
            '            drNew("ClassName") = drDay.Item("Descrip")
            '            drNew("InstructorId") = drDay.Item("InstructorId")
            '            drNew("InstructorName") = drDay.Item("FullName")
            '            drNew("ClassStartDate") = startDate
            '            drNew("ClassEndDate") = rendDate
            '            drNew("MeetDate") = iDate
            '            drNew("MeetNumber") = meetNum
            '            drNew("AttendanceType") = "PA" ' TODO
            '            drNew("NumTardiesMakingAbsent") = 0
            '            drNew("PrgVerDescrip") = drDay.Item("PrgVerDescrip")
            '            meetNum += 1

            '            dtRes.Rows.Add(drNew)
            '        End If
            '    Next
            '    iDate = iDate.AddDays(1) ' go to the next day
            'End While
        Next
    End Sub
    ''' <summary>
    ''' Creates meeting days for scheduling methods that use periods
    ''' </summary>
    ''' <param name="dtRes"></param>
    ''' <param name="dtMeetdays"></param>
    ''' <param name="dtDateRange"></param>
    ''' <remarks></remarks>
    Private Sub CreateMeetingDaysPeriods(ByVal dtRes As DataTable, ByVal dtMeetdays As DataTable, ByVal dtDateRange As DataTable, Optional ByVal campusId As String = "")

       

        Dim usePeriods As Boolean = (Not MyAdvAppSettings.AppSettings("SchedulingMethod", campusId) Is Nothing AndAlso MyAdvAppSettings.AppSettings("SchedulingMethod", campusId).ToString <> "Traditional")
        Dim holidayFac As New HolidayFacade()
        Dim clsAttFac As New ClsSectAttendanceFacade()

        For Each drClass As DataRow In dtDateRange.Rows
            Dim stuEnrollId As String = drClass("StuEnrollId").ToString()
            Dim prgVerDescrip As String = drClass("PrgVerDescrip").ToString()
            Dim clsSectionId As String = drClass("ClsSectionId").ToString()
            Dim ClsSectMeetingId As String = drClass("ClsSectMeetingId").ToString()
            Dim startDate As DateTime = CType(drClass("StartDate"), DateTime)
            Dim rendDate As DateTime = CType(drClass("EndDate"), DateTime) ' unadjusted end date
            Dim endDate As DateTime = CType(drClass("EndDate"), DateTime)

            ' restrict the endDate to today's date
            If endDate > Date.Today Then endDate = Date.Today
            'Dim drClsMeetDays() As DataRow = dtMeetdays.Select(String.Format("ClsSectionId='{0}'", clsSectionId))
            Dim drClsMeetDays() As DataRow = dtMeetdays.Select(String.Format("ClsSectMeetingId='{0}' AND StuEnrollId='{1}'", ClsSectMeetingId, stuEnrollId))
            'Dim arrDates As ArrayList = clsAttFac.NewGetMeetDatesArrayListFromRange(clsSectionId, ClsSectMeetingId, startDate.ToString, endDate.ToString)
            'Dim arrDates As ArrayList = clsAttFac.GetMeetDatesArrayListFromRange_New(clsSectionId, startDate.ToString(), endDate.ToString())
            Dim arrDates As ArrayList = clsAttFac.GetMeetDatesForMeetingsArrayListFromRange(clsSectionId, ClsSectMeetingId, startDate.ToString, endDate.ToString, campusId)

            Dim meetNum As Integer = 0
            For Each d As Date In arrDates
                'Dim bFound = False
                'For Each drDay As DataRow In drClsMeetDays
                '    If Not bFound AndAlso drDay("WorkDaysDescrip").ToString().ToLower() = d.ToString("ddd").ToLower() Then
                '        bFound = True
                Dim drDay As DataRow = drClsMeetDays(0)
                Dim drNew As DataRow = dtRes.NewRow()
                drNew("TermId") = drDay.Item("TermId")
                drNew("TermDescrip") = drDay.Item("TermDescrip")
                drNew("ClsSectionId") = clsSectionId
                drNew("ClsSectMeetingId") = ClsSectMeetingId
                drNew("ReqId") = drDay.Item("ReqId")
                drNew("ClassName") = drDay.Item("Descrip")
                drNew("InstructorId") = drDay.Item("InstructorId")
                drNew("InstructorName") = drDay.Item("FullName")
                drNew("ClassStartDate") = startDate
                drNew("ClassEndDate") = rendDate
                drNew("MeetDate") = d
                drNew("MeetNumber") = meetNum
                drNew("AttendanceType") = "PA" ' TODO
                drNew("NumTardiesMakingAbsent") = 0
                drNew("PrgVerDescrip") = prgVerDescrip
                drNew("InstructionTypeID") = drDay("InstructionTypeID")
                drNew("ClsSectMeetingDesc") = drDay("InstructionTypeDescrip") & " - " & drDay("PeriodDescrip")
                drNew("TimeIntervalDescrip") = CDate(drDay("TimeIntervalDescrip")).TimeOfDay
                drNew("StuEnrollId") = stuEnrollId
                meetNum += 1
                dtRes.Rows.Add(drNew)
                ' End If
                'Next
            Next
        Next
    End Sub


    ''' <summary>
    ''' Fills dtRes with all the attedance information
    ''' </summary>
    ''' <param name="dtRes"></param>
    ''' <param name="dtAttendance"></param>
    ''' <remarks></remarks>
    Private Sub FillAttendanceInfo(ByVal dtRes As DataTable, ByVal dtAttendance As DataTable)
        Dim db As New ClsSectAttendanceDB
        Dim attFacade As New ClsSectAttendanceFacade
        Dim attPercent As Common.AttendancePercentageInfo
        Dim attType As String
        Dim PrgVerId As String
        Dim PrgVerDescrip As String

      

        For Each dr As DataRow In dtRes.Rows
            Dim clsSectionId As String = dr("ClsSectionId").ToString()
            Dim ClsSectMeetingId As String = dr("ClsSectMeetingId").ToString()
            Dim MeetDate As DateTime = dr("MeetDate")
            If MeetDate.TimeOfDay.Hours = 0 Then MeetDate = CDate(MeetDate & " " & dr("TimeIntervalDescrip").ToString)
            'Dim drClass() As DataRow = dtAttendance.Select(String.Format("ClsSectionId='{0}' and MeetDate='{1}' and ClsSectMeetingId='{2}'", clsSectionId, MeetDate, ClsSectMeetingId))
            Dim drClass() As DataRow = dtAttendance.Select(String.Format("ClsSectionId='{0}' and (MeetDate='{1}' or meetdate='{3}') and ClsSectMeetingId='{2}'", clsSectionId, MeetDate, ClsSectMeetingId, MeetDate.ToShortDateString))
            'Dim drClass() As DataRow = dtAttendance.Select(String.Format("ClsSectionId='{0}'", clsSectionId))
            If drClass.Length > 0 Then
                PrgVerId = drClass(0)("PrgVerId").ToString()
                PrgVerDescrip = drClass(0)("PrgVerDescrip")
                dr("StudentName") = drClass(0)("StudentName")
                'dr("PrgVerId") = drClass(0)("PrgVerId")
                'dr("PrgVerDescrip") = drClass(0)("PrgVerDescrip")
                dr("Actual") = drClass(0)("Actual")
                dr("Tardy") = drClass(0)("Tardy")
                dr("Excused") = drClass(0)("Excused")
                dr("Absent") = 0

                Try
                    dr("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                Catch ex As System.Exception
                    dr("SuppressDate") = "no"
                End Try
                ' Set the number of tardies making absent

                dr("NumTardiesMakingAbsent") = GetTardiesMakingAbsence(PrgVerId, clsSectionId)
                attType = db.GetPrgVersionAttendanceType(PrgVerId, True)
                If attType Is Nothing Then
                    dr("AttendanceType") = attFacade.GetClassSectionAttendanceType(clsSectionId).ToUpper()
                Else
                    dr("AttendanceType") = attType.ToUpper()
                End If
                ' make adjustments for "minutes" or "pa" program type
                If dr("AttendanceType") = "MINUTES" Then
                    Dim classDuration As Integer = 0 'GetScheduledMinutesForMeetDay(dtm)
                    attPercent = attFacade.GetPercentageAttendanceInfo(drClass(0)("StuEnrollId").ToString, Date.Today.ToString, clsSectionId)
                    dr("TotalPresent") = attPercent.TotalPresent
                    dr("TotalTardies") = attPercent.TotalTardies
                    dr("TotalAbsent") = attPercent.TotalAbsent
                    dr("TotalExcused") = attPercent.TotalExcused
                    dr("TotalPresentAdjusted") = attPercent.TotalPresentAdjusted
                    dr("TotalTardiesAdjusted") = attPercent.TotalTardiesAdjusted
                    dr("TotalAbsentAdjusted") = attPercent.TotalAbsentAdjusted
                    dr("TotalExcusedAdjusted") = attPercent.TotalExcusedAdjusted
                    dr("TotalScheduled") = attPercent.TotalScheduled
                    dr("TotalMakeUp") = attPercent.TotalMakeUp
                    If Integer.Parse(dr("Actual").ToString()) = 0 Then
                        dr("AttendanceStr") = CType(classDuration, String) ' absent ?                        
                    Else
                        dr("AttendanceStr") = dr("Actual").ToString()
                    End If
                ElseIf dr("AttendanceType") = "CLOCK HOURS" Then
                    Dim classDuration As Integer = 0 'GetScheduledMinutesForMeetDay(dtm)
                    attPercent = attFacade.GetPercentageAttendanceInfo(drClass(0)("StuEnrollId").ToString, Date.Today.ToString, clsSectionId)
                    dr("TotalPresent") = Math.Round(attPercent.TotalPresent / 60, 2)
                    dr("TotalTardies") = Math.Round(attPercent.TotalTardies / 60, 2)
                    dr("TotalAbsent") = Math.Round(attPercent.TotalAbsent / 60, 2)
                    dr("TotalExcused") = Math.Round(attPercent.TotalExcused / 60, 2)
                    dr("TotalPresentAdjusted") = Math.Round(attPercent.TotalPresentAdjusted / 60, 2)
                    dr("TotalTardiesAdjusted") = Math.Round(attPercent.TotalTardiesAdjusted / 60, 2)
                    dr("TotalAbsentAdjusted") = Math.Round(attPercent.TotalAbsentAdjusted / 60, 2)
                    dr("TotalExcusedAdjusted") = Math.Round(attPercent.TotalExcusedAdjusted / 60, 2)
                    dr("TotalScheduled") = Math.Round(attPercent.TotalScheduled / 60, 2)
                    dr("TotalMakeUp") = Math.Round(attPercent.TotalMakeUp / 60, 2)
                    If Integer.Parse(dr("Actual").ToString()) = 0 Then
                        dr("AttendanceStr") = CType(classDuration / 60, String) ' absent ?                        
                    Else
                        dr("AttendanceStr") = (Math.Round(dr("Actual") / 60, 2)).ToString()
                    End If
                Else
                    If dr("Actual") = 0 And dr("Excused") = 1 Then
                        dr("AttendanceStr") = "E"
                    ElseIf dr("Actual") = 0 And dr("Excused") <> 1 Then
                        dr("AttendanceStr") = "A"
                    ElseIf dr("Actual") = 1 And dr("Tardy") = 1 Then
                        dr("AttendanceStr") = "T"
                    ElseIf dr("Actual") = 1 And dr("Tardy") <> 1 Then
                        dr("AttendanceStr") = "P"
                    Else
                        dr("AttendanceStr") = " "
                    End If
                End If
            Else
                'Dim drClass1() As DataRow = dtAttendance.Select(String.Format("ClsSectionId='{0}' and ClsSectMeetingId='{1}'", clsSectionId, ClsSectMeetingId))
                Dim drClass1() As DataRow = dtAttendance.Select(String.Format("ClsSectionId='{0}'", clsSectionId))
                If drClass1.Length > 0 Then
                    PrgVerId = drClass1(0)("PrgVerId").ToString()
                    PrgVerDescrip = drClass1(0)("PrgVerDescrip")
                    'dr("PrgVerId") = drClass1(0)("PrgVerId")
                    'dr("PrgVerDescrip") = drClass1(0)("PrgVerDescrip")
                    attType = db.GetPrgVersionAttendanceType(PrgVerId, True)
                    If attType Is Nothing Then
                        dr("AttendanceType") = attFacade.GetClassSectionAttendanceType(clsSectionId).ToUpper()
                    Else
                        dr("AttendanceType") = attType.ToUpper()
                    End If

                    Try
                        dr("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                    Catch ex As System.Exception
                        dr("SuppressDate") = "no"
                    End Try
                    ' make adjustments for "minutes" or "pa" program type
                    If dr("AttendanceType") = "MINUTES" Then
                        Dim classDuration As Integer = 0 'GetScheduledMinutesForMeetDay(dtm)
                        attPercent = attFacade.GetPercentageAttendanceInfo(drClass1(0)("StuEnrollId").ToString, Date.Today.ToString, clsSectionId)
                        dr("TotalPresent") = attPercent.TotalPresent
                        dr("TotalTardies") = attPercent.TotalTardies
                        dr("TotalAbsent") = attPercent.TotalAbsent
                        dr("TotalExcused") = attPercent.TotalExcused
                        dr("TotalPresentAdjusted") = attPercent.TotalPresentAdjusted
                        dr("TotalTardiesAdjusted") = attPercent.TotalTardiesAdjusted
                        dr("TotalAbsentAdjusted") = attPercent.TotalAbsentAdjusted
                        dr("TotalExcusedAdjusted") = attPercent.TotalExcusedAdjusted
                        dr("TotalScheduled") = attPercent.TotalScheduled
                        dr("TotalMakeUp") = attPercent.TotalMakeUp
                    ElseIf dr("AttendanceType") = "CLOCK HOURS" Then
                        Dim classDuration As Integer = 0 'GetScheduledMinutesForMeetDay(dtm)
                        attPercent = attFacade.GetPercentageAttendanceInfo(drClass1(0)("StuEnrollId").ToString, Date.Today.ToString, clsSectionId)
                        dr("TotalPresent") = attPercent.TotalPresent / 60
                        dr("TotalTardies") = attPercent.TotalTardies / 60
                        dr("TotalAbsent") = attPercent.TotalAbsent / 60
                        dr("TotalExcused") = attPercent.TotalExcused / 60
                        dr("TotalPresentAdjusted") = attPercent.TotalPresentAdjusted / 60
                        dr("TotalTardiesAdjusted") = attPercent.TotalTardiesAdjusted / 60
                        dr("TotalAbsentAdjusted") = attPercent.TotalAbsentAdjusted / 60
                        dr("TotalExcusedAdjusted") = attPercent.TotalExcusedAdjusted / 60
                        dr("TotalScheduled") = attPercent.TotalScheduled / 60
                        dr("TotalMakeUp") = attPercent.TotalMakeUp / 60
                    End If
                Else
                    'If PrgVerId Is Nothing Then
                    '    dr("PrgVerId") = "00000000-0000-0000-0000-000000000000"
                    '    dr("PrgVerDescrip") = ""
                    'Else
                    '    dr("PrgVerId") = PrgVerId
                    '    dr("PrgVerDescrip") = PrgVerDescrip
                    'End If
                    attType = db.GetPrgVersionAttendanceType(PrgVerId, True)
                    If attType Is Nothing Then
                        dr("AttendanceType") = attFacade.GetClassSectionAttendanceType(clsSectionId).ToUpper()
                    Else
                        dr("AttendanceType") = attType.ToUpper()
                    End If
                    dr("PrgVerId") = "00000000-0000-0000-0000-000000000000"
                    dr("PrgVerDescrip") = dr("PrgVerDescrip")

                    Try
                        dr("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                    Catch ex As System.Exception
                        dr("SuppressDate") = "no"
                    End Try
                End If
            End If

        Next
    End Sub

    Public Function GetTardiesMakingAbsence(ByVal prgVerId As String, ByVal clsSectionId As String) As Integer
        Dim factor As Integer = 0
        Dim fac As New ClsSectAttendanceFacade

        '   Check if attendance is tracked at the Program Version level
        If fac.DoesPrgVersionTrackAttendance(prgVerId, True) Then
            '   Get number of tardies making an absence at the program version level
            factor = fac.GetTardiesMakingAbsenceForPrgVersion(prgVerId, True)
        Else
            '   Get number of tardies making an absence at the course level
            factor = fac.GetTardiesMakingAbsenceForCourse(clsSectionId, False)
        End If

        Return factor
    End Function
#End Region

#Region "Other helpers"
    ''' <summary>
    ''' Get a studentid from the stuenrollid
    ''' </summary>
    ''' <param name="StuEnrollId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetStudentId(ByVal StuEnrollId As String) As String
        Try
            Dim db As New FAME.AdvantageV1.DataAccess.DataAccess



            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Dim sb As New StringBuilder
            sb.Append("SELECT  " + vbCrLf)
            sb.Append("		distinct StudentId " + vbCrLf)
            sb.Append("FROM	arStuEnrollments SE " + vbCrLf)
            sb.Append("WHERE " + vbCrLf)
            sb.AppendFormat("       SE.StuEnrollId = {0} ", StuEnrollId)
            Return db.RunParamSQLScalar(sb.ToString()).ToString()
        Catch ex As System.Exception
            Return ""
        End Try
    End Function

    ''' <summary>
    ''' Return all the class sections and meeting times that a student has
    ''' </summary>
    ''' <param name="usePeriods"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' ''New Code Added Bu Vijay Ramteke On September 14, 2010 Form Mantis Id 13650
    Private Function GetClsSectionMeetingDays(ByVal StudentId As String, ByVal usePeriods As Boolean, Optional ByVal StuEnrollId As String = "") As DataTable
        Dim db As New FAME.AdvantageV1.DataAccess.DataAccess
        Dim sb As New StringBuilder


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        If usePeriods Then
            sb.Append("SELECT  " + vbCrLf)
            sb.Append("     Term.TermId, " + vbCrLf)
            sb.Append("     Term.TermDescrip, " + vbCrLf)
            sb.Append("     CS.ClsSectionId, " + vbCrLf)
            sb.Append("     CS.ReqId as ReqId, " + vbCrLf)
            sb.Append("     Req.Descrip, " + vbCrLf)
            sb.Append("     CS.InstructorId, " + vbCrLf)
            sb.Append("     (select FullName from syUsers U where U.UserId=CS.InstructorId) as FullName, " + vbCrLf)
            sb.Append("     CS.StartDate, " + vbCrLf)
            sb.Append("     CS.EndDate, " + vbCrLf)
            sb.Append("		PWD.WorkDayId, " + vbCrLf)
            sb.Append("		WD.WorkdaysDescrip, " + vbCrLf)
            sb.Append("		TI1.TimeIntervalDescrip, " + vbCrLf)
            sb.Append("		TI2.TimeIntervalDescrip As EndTime, " + vbCrLf)
            'sb.Append(" (SELECT PrgVerDescrip FROM arResults R2, arClassSections CS2, arStuEnrollments SE2,arPrgVersions PV WHERE ")
            'sb.Append(" SE2.StuEnrollId=R2.StuEnrollId AND SE2.StudentId=" + StudentId + " AND R2.TestId=CS2.ClsSectionId ")
            '''New Code Added Bu Vijay Ramteke On September 14, 2010 Form Mantis Id 13650
            'If Not StuEnrollId = "" Then
            '    sb.Append(" AND SE2.StuEnrollId=" + StuEnrollId + "")
            'End If
            '''New Code Added Bu Vijay Ramteke On September 14, 2010 Form Mantis Id 13650
            'sb.Append(" AND R2.TestId=CSM.ClsSectionId and SE2.PrgVerId=PV.PrgVerId) as PrgVerDescrip,  " + vbCrLf)
            sb.Append("     PV.PrgVerDescrip,RS.StuEnrollId,SE.PrgVerId, " + vbCrLf)
            sb.Append(" CSM.InstructionTypeID,CSM.ClsSectMeetingId, " + vbCrLf)
            sb.Append(" P.PeriodDescrip, ")
            sb.Append("( SELECT    IT.InstructionTypeDescrip  FROM      arInstructionType IT ")
            sb.Append(" WHERE IT.InstructionTypeId = CSM.InstructionTypeId   ) AS InstructionTypeDescrip, " + vbCrLf)
            sb.Append(" SE.StuEnrollId ")
            sb.Append(" FROM	" + vbCrLf)
            sb.Append("     arClassSections CS, " + vbCrLf)
            sb.Append("     arTerm Term, " + vbCrLf)
            sb.Append("     arReqs Req, " + vbCrLf)
            sb.Append("     arAttUnitType AttType, " + vbCrLf)
            sb.Append("     arClsSectMeetings CSM, " + vbCrLf)
            sb.Append("     syPeriods P, " + vbCrLf)
            sb.Append("     syPeriodsWorkDays PWD, " + vbCrLf)
            sb.Append("     plWorkdays WD, " + vbCrLf)
            sb.Append("     cmTimeInterval TI1, " + vbCrLf)
            sb.Append("     cmTimeInterval TI2, " + vbCrLf)
            sb.Append("     arResults RS, " + vbCrLf)
            sb.Append("     arStuEnrollments SE, " + vbCrLf)
            sb.Append("     arprgversions PV ")
            sb.Append("WHERE " + vbCrLf)
            sb.Append("     CS.ClsSectionId=CSM.ClsSectionid " + vbCrLf)
            sb.Append("     AND CS.TermId = Term.TermId " + vbCrLf)
            sb.Append("     AND CS.ReqId = Req.ReqId " + vbCrLf)
            sb.Append(" AND Req.UnitTypeId=AttType.UnitTypeId  AND LOWER(AttType.UnitTypeDescrip)<>'none' " + vbCrLf)
            sb.Append("		AND CSM.PeriodId=P.PeriodId " + vbCrLf)
            sb.Append("     AND P.PeriodId=PWD.PeriodId " + vbCrLf)
            sb.Append("     AND PWD.WorkDayId=WD.WorkDaysId " + vbCrLf)
            sb.Append("     AND P.StartTimeId = TI1.TimeIntervalId  " + vbCrLf)
            sb.Append("     AND P.EndTimeId = TI2.TimeIntervalId  " + vbCrLf)
            sb.Append("     AND CS.StartDate <= getdate() " + vbCrLf)
            sb.Append("     AND CS.ClsSectionId=RS.TestId " + vbCrLf)
            sb.Append("     AND RS.StuEnrollId=SE.StuEnrollId " + vbCrLf)
            sb.Append("     AND SE.PrgVerId=PV.PrgVerId " + vbCrLf)
            sb.Append("     AND SE.StudentId=" + StudentId + vbCrLf)
            ''New Code Added Bu Vijay Ramteke On September 14, 2010 Form Mantis Id 13650
            'If Not StuEnrollId = "" Then
            '    sb.AppendFormat("   AND CSM.ClsSectionId IN (SELECT R2.TestId FROM arResults R2, arClassSections CS2, arStuEnrollments SE2 WHERE SE2.StuEnrollId=R2.StuEnrollId and SE2.StudentId={0} and SE2.StuEnrollId={1}  AND R2.TestId=CS2.ClsSectionId) {2} ", StudentId, StuEnrollId, vbCrLf)
            'Else
            '    sb.AppendFormat("   AND CSM.ClsSectionId IN (SELECT R2.TestId FROM arResults R2, arClassSections CS2, arStuEnrollments SE2 WHERE SE2.StuEnrollId=R2.StuEnrollId and SE2.StudentId={0} AND R2.TestId=CS2.ClsSectionId) {1} ", StudentId, vbCrLf)
            'End If
            ''sb.AppendFormat("   AND CSM.ClsSectionId IN (SELECT R2.TestId FROM arResults R2, arClassSections CS2, arStuEnrollments SE2 WHERE SE2.StuEnrollId=R2.StuEnrollId and SE2.StudentId={0} AND R2.TestId=CS2.ClsSectionId) {1} ", StudentId, vbCrLf)
            ''New Code Added Bu Vijay Ramteke On September 14, 2010 Form Mantis Id 13650
            If Not StuEnrollId = "" Then
                'sb.AppendFormat("AND SE.StuEnrollId = {0} ", StuEnrollId)
                sb.Append(" AND RS.StuEnrollId = " + StuEnrollId + vbCrLf)
            Else
                'sb.AppendFormat("AND SE.StudentId = {0} ", StudentId)
            End If

            'sb.Append(vbCrLf + "UNION ALL " + vbCrLf + vbCrLf)

            'sb.Append("Select  " + vbCrLf)
            'sb.Append("     Term.TermId, " + vbCrLf)
            'sb.Append("     Term.TermDescrip, " + vbCrLf)
            'sb.Append("     CS.ClsSectionId, " + vbCrLf)
            'sb.Append("     CS.ReqId As ReqId, " + vbCrLf)
            'sb.Append("     Req.Descrip, " + vbCrLf)
            'sb.Append("     CS.InstructorId, " + vbCrLf)
            'sb.Append("(select FullName from syUsers U where U.UserId=CS.InstructorId) As FullName, " + vbCrLf)
            'sb.Append("     CS.StartDate, " + vbCrLf)
            'sb.Append("     CS.EndDate, " + vbCrLf)
            'sb.Append("		PWD.WorkDayId, " + vbCrLf)
            'sb.Append("		WD.WorkdaysDescrip, " + vbCrLf)
            'sb.Append("		TI1.TimeIntervalDescrip, " + vbCrLf)
            'sb.Append("		TI2.TimeIntervalDescrip As EndTime, " + vbCrLf)
            'sb.Append("(SELECT PrgVerDescrip FROM arResults R2, arClassSections CS2, arStuEnrollments SE2, arPrgVersions PV WHERE ")
            'sb.Append(" SE2.StuEnrollId=R2.StuEnrollId And SE2.StudentId=" + StudentId + " And R2.TestId=CS2.ClsSectionId ")
            '''New Code Added Bu Vijay Ramteke On September 14, 2010 Form Mantis Id 13650
            'If Not StuEnrollId = "" Then
            '    sb.Append(" And SE2.StuEnrollId=" + StuEnrollId + "")
            'End If
            '''New Code Added Bu Vijay Ramteke On September 14, 2010 Form Mantis Id 13650
            'sb.Append(" And R2.TestId=CSM.ClsSectionId And SE2.PrgVerId=PV.PrgVerId) As PrgVerDescrip, CSM.InstructionTypeID, CSM.ClsSectMeetingId, " + vbCrLf)
            'sb.Append(" P.PeriodDescrip, ")
            'sb.Append("( SELECT    IT.InstructionTypeDescrip  FROM      arInstructionType IT ")
            'sb.Append(" WHERE IT.InstructionTypeId = CSM.InstructionTypeId   ) As InstructionTypeDescrip ")
            'sb.Append(" FROM	" + vbCrLf)
            'sb.Append("     arClassSections CS, " + vbCrLf)
            'sb.Append("     arTerm Term, " + vbCrLf)
            'sb.Append("     arReqs Req, " + vbCrLf)
            'sb.Append(" arAttUnitType AttType, " + vbCrLf)
            'sb.Append("     arClsSectMeetings CSM, " + vbCrLf)
            'sb.Append("     syPeriods P, " + vbCrLf)
            'sb.Append("     syPeriodsWorkDays PWD, " + vbCrLf)
            'sb.Append("     plWorkdays WD, " + vbCrLf)
            'sb.Append("     cmTimeInterval TI1, " + vbCrLf)
            'sb.Append("     cmTimeInterval TI2 " + vbCrLf)
            'sb.Append("WHERE " + vbCrLf)
            'sb.Append("     CS.ClsSectionId=CSM.ClsSectionid " + vbCrLf)
            'sb.Append("     And CS.TermId = Term.TermId " + vbCrLf)
            'sb.Append("     And CS.ReqId = Req.ReqId " + vbCrLf)
            'sb.Append(" And Req.UnitTypeId=AttType.UnitTypeId  And LOWER(AttType.UnitTypeDescrip)<>'none' " + vbCrLf)
            'sb.Append("		AND CSM.AltPeriodId=P.PeriodId " + vbCrLf)
            'sb.Append("     AND P.PeriodId=PWD.PeriodId " + vbCrLf)
            'sb.Append("     AND PWD.WorkDayId=WD.WorkDaysId " + vbCrLf)
            'sb.Append("     AND P.StartTimeId = TI1.TimeIntervalId  " + vbCrLf)
            'sb.Append("     AND P.EndTimeId = TI2.TimeIntervalId  " + vbCrLf)
            'sb.Append("     AND CS.StartDate <= getdate() " + vbCrLf)

            '''New Code Added Bu Vijay Ramteke On September 14, 2010 Form Mantis Id 13650
            'If Not StuEnrollId = "" Then
            '    sb.AppendFormat("   AND CSM.ClsSectionId IN (SELECT R2.TestId FROM arResults R2, arClassSections CS2, arStuEnrollments SE2 WHERE SE2.StuEnrollId=R2.StuEnrollId and SE2.StudentId={0} and SE2.StuEnrollId={1}  AND R2.TestId=CS2.ClsSectionId) {2} ", StudentId, StuEnrollId, vbCrLf)
            'Else
            '    sb.AppendFormat("   AND CSM.ClsSectionId IN (SELECT R2.TestId FROM arResults R2, arClassSections CS2, arStuEnrollments SE2 WHERE SE2.StuEnrollId=R2.StuEnrollId and SE2.StudentId={0} AND R2.TestId=CS2.ClsSectionId) {1} ", StudentId, vbCrLf)
            'End If
            '''sb.AppendFormat("       AND CSM.ClsSectionId IN (SELECT R2.TestId FROM arResults R2, arClassSections CS2, arStuEnrollments SE2 WHERE SE2.StuEnrollId=R2.StuEnrollId AND SE2.StudentId={0} AND R2.TestId=CS2.ClsSectionId)  {1}", StudentId, vbCrLf)
            '''New Code Added Bu Vijay Ramteke On September 14, 2010 Form Mantis Id 13650

            sb.Append("ORDER BY Req.Descrip, CS.StartDate" + vbCrLf)
            'Else
            '    sb.Append("SELECT DISTINCT " + vbCrLf)
            '    sb.Append("     Term.TermId, " + vbCrLf)
            '    sb.Append("     Term.TermDescrip, " + vbCrLf)
            '    sb.Append("     CS.ClsSectionId, " + vbCrLf)
            '    sb.Append("     CS.ReqId, " + vbCrLf)
            '    sb.Append("     Req.Descrip, " + vbCrLf)
            '    sb.Append("     CS.InstructorId, " + vbCrLf)
            '    sb.Append("     (select FullName from syUsers U where U.UserId=CS.InstructorId) as FullName, " + vbCrLf)
            '    sb.Append("     CS.StartDate, " + vbCrLf)
            '    sb.Append("     CS.EndDate, " + vbCrLf)
            '    sb.Append("     CSM.WorkDaysId, " + vbCrLf)
            '    sb.Append("     WD.WorkDaysDescrip, " + vbCrLf)
            '    sb.Append("     TI.TimeIntervalDescrip, " + vbCrLf)
            '    sb.Append("     ETI.TimeIntervalDescrip As EndTime, " + vbCrLf)
            '    sb.Append(" (SELECT PrgVerDescrip FROM arResults R2, arClassSections CS2, arStuEnrollments SE2,arPrgVersions PV WHERE ")
            '    sb.Append(" SE2.StuEnrollId=R2.StuEnrollId AND SE2.StudentId=" + StudentId + " AND R2.TestId=CS2.ClsSectionId ")
            '    ''New Code Added Bu Vijay Ramteke On September 09, 2010 Form Mantis Id 13650
            '    If Not StuEnrollId = "" Then
            '        sb.Append(" AND SE2.StuEnrollId=" + StuEnrollId + "")
            '    End If
            '    ''New Code Added Bu Vijay Ramteke On September 09, 2010 Form Mantis Id 13650
            '    sb.Append(" AND R2.TestId=CSM.ClsSectionId and SE2.PrgVerId=PV.PrgVerId) as PrgVerDescrip,CSM.InstructionTypeID,CSM.ClsSectMeetingId, " + vbCrLf)
            '    sb.Append("( SELECT    IT.InstructionTypeDescrip  FROM      arInstructionType IT ")
            '    sb.Append(" WHERE IT.InstructionTypeId = CSM.InstructionTypeId   ) AS InstructionTypeDescrip ")
            '    sb.Append(" FROM " + vbCrLf)
            '    sb.Append("     arClsSectMeetings CSM, " + vbCrLf)
            '    sb.Append("     arClassSections CS, " + vbCrLf)
            '    sb.Append("     arTerm Term, " + vbCrLf)
            '    sb.Append("     arReqs Req, " + vbCrLf)
            '    sb.Append(" arAttUnitType AttType, " + vbCrLf)
            '    sb.Append("     plWorkDays WD, " + vbCrLf)
            '    sb.Append("     cmTimeInterval TI, " + vbCrLf)
            '    sb.Append("     cmTimeInterval ETI " + vbCrLf)
            '    sb.Append("WHERE " + vbCrLf)
            '    sb.Append("     CSM.ClsSectionId = CS.ClsSectionId " + vbCrLf)
            '    sb.Append("     AND CS.TermId = Term.TermId " + vbCrLf)
            '    sb.Append("     AND CS.ReqID = Req.ReqId " + vbCrLf)
            '    sb.Append(" AND Req.UnitTypeId=AttType.UnitTypeId  AND LOWER(AttType.UnitTypeDescrip)<>'none' " + vbCrLf)
            '    sb.Append("     AND CSM.WorkDaysId = WD.WorkDaysId " + vbCrLf)
            '    sb.Append("     AND CSM.TimeIntervalId = TI.TimeIntervalId " + vbCrLf)
            '    sb.Append("     AND CSM.EndIntervalId = ETI.TimeIntervalId " + vbCrLf)
            '    sb.Append("     AND EXISTS (SELECT * FROM arResults R2, arClassSections CS2, arStuEnrollments SE2 " + vbCrLf)
            '    ''New Code Added Bu Vijay Ramteke On September 14, 2010 Form Mantis Id 13650
            '    If Not StuEnrollId = "" Then
            '        sb.AppendFormat("   WHERE SE2.StuEnrollId=R2.StuEnrollId and SE2.StudentId={0} and SE2.StuEnrollId={1}  {2} ", StudentId, StuEnrollId, vbCrLf)
            '    Else
            '        sb.AppendFormat("   WHERE SE2.StuEnrollId=R2.StuEnrollId and SE2.StudentId={0} {1} ", StudentId, vbCrLf)
            '    End If
            '    ''sb.AppendFormat("         WHERE SE2.StuEnrollId=R2.StuEnrollId AND SE2.StudentId={0} {1} ", StudentId, vbCrLf)
            '    ''New Code Added Bu Vijay Ramteke On September 14, 2010 Form Mantis Id 13650

            '    sb.Append("             AND R2.TestId=CS2.ClsSectionId " + vbCrLf)
            '    sb.Append("             AND R2.TestId=CSM.ClsSectionId) " + vbCrLf)
            '    sb.Append("     AND CS.StartDate <= getdate() " + vbCrLf)
            '    sb.Append("ORDER BY Req.Descrip, CS.StartDate" + vbCrLf)
        End If

        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function

    ''' <summary>
    ''' Get the start and end date for all the class sections added
    ''' </summary>
    ''' <param name="clsSectionIds"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetClsSectionDateRange(ByVal clsSectionMeetingIds() As String, ByVal studentId As String, Optional ByVal stuEnrollId As String = "") As DataTable
        Dim db As New FAME.AdvantageV1.DataAccess.DataAccess
        Dim sb As New StringBuilder


        db.ConnectionString = myAdvAppSettings.AppSettings("ConString")

        sb.Append("SELECT CS.ClsSectionId, CS.StartDate, CS.EndDate,CS.ClsSectMeetingId,SE.StuEnrollId,SE.PrgVerId,PV.PrgVerDescrip " + vbCrLf)
        sb.Append("FROM arClsSectMeetings CS, arClassSections C, arResults RS, arStuEnrollments SE, arPrgVersions PV " + vbCrLf)
        sb.Append("WHERE CS.ClsSectionId=C.ClsSectionId " + vbCrLf)
        sb.Append("AND C.ClsSectionId=RS.TestId " + vbCrLf)
        sb.Append("AND RS.StuEnrollId=SE.StuEnrollId " + vbCrLf)
        sb.Append("AND SE.PrgVerId=PV.PrgVerId " + vbCrLf)
        'sb.Append("WHERE CS.ClsSectionId IN ('63B4359F-5A66-4209-9845-7FEAF99FBAA7') " + vbCrLf)

        If Not stuEnrollId = "" Then
            sb.AppendFormat("AND SE.StuEnrollId = {0} " + vbCrLf, stuEnrollId)
        Else
            sb.AppendFormat("AND SE.StudentId = {0} " + vbCrLf, studentId)
        End If

        If clsSectionMeetingIds.Length > 0 Then
            sb.Append("AND ClsSectMeetingId in (")
            For Each id As String In clsSectionMeetingIds
                sb.AppendFormat("'{0}',", id)
            Next
            sb.Remove(sb.Length - 1, 1) ' remove the last comma
            sb.Append(")" + vbCrLf)
        End If
        sb.Append(" order by CS.ClsSectionId,CS.ClsSectMeetingId ")
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function

    Public Function GetClsSectionDateRange(ByVal meetingDays As DataTable) As DataTable
        Return meetingDays.DefaultView.ToTable(True, {"StuEnrollId", "PrgVerDescrip", "ClsSectionId", "ClsSectMeetingId", "StartDate", "EndDate", "Descrip"})
    End Function
    ''' <summary>
    ''' Get the all student attendance records for the given student
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetStudentAttendance(ByVal rptParamInfo As Common.ReportParamInfo, ByVal StudentId As String, Optional ByVal StuEnrollId As String = "") As DataTable
        Dim db As New FAME.AdvantageV1.DataAccess.DataAccess


        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        sb.Append("SELECT " + vbCrLf)
        sb.Append("     (select S.LastName + ', ' + S.FirstName from arStudent S where S.StudentId=SE.StudentId) As StudentName, " + vbCrLf)
        sb.Append("     CSA.StuEnrollId, " + vbCrLf)
        sb.Append("     SE.PrgVerId, " + vbCrLf)
        sb.Append("     PV.PrgVerDescrip, " + vbCrLf)
        sb.Append("     CSA.ClsSectionId, " + vbCrLf)
        sb.Append("     CSA.ClsSectMeetingId, " + vbCrLf)
        sb.Append("     CSA.MeetDate, " + vbCrLf)
        sb.Append("     CSA.Actual, " + vbCrLf)
        sb.Append("     CSA.Tardy, " + vbCrLf)
        sb.Append("     CSA.Excused " + vbCrLf)
        sb.Append("FROM " + vbCrLf)
        sb.Append("     atClsSectAttendance CSA, arClassSections CS, arStuEnrollments SE, arPrgVersions PV, syCampGrps " + vbCrLf)
        sb.Append("WHERE  CSA.ClsSectionId = CS.ClsSectionId " + vbCrLf)
        sb.Append("     AND PV.CampGrpid=syCampGrps.CampGrpid " + vbCrLf)
        sb.Append("     AND CSA.StuEnrollId = SE.StuEnrollId " + vbCrLf)
        sb.Append("     AND SE.PrgVerId = PV.PrgVerId " + vbCrLf)
        sb.Append("  AND CSA.Actual <> 999 AND CSA.Actual <> 9999 ")
        sb.Append("       AND EXISTS (SELECT * FROM arResults R, arClassSections CS2 " + vbCrLf)
        sb.Append("                       WHERE R.StuEnrollId=SE.StuEnrollId " + vbCrLf)
        sb.Append("                       AND R.TestId=CS2.ClsSectionId " + vbCrLf)
        sb.Append("                       AND R.TestId=CSA.ClsSectionId) " + vbCrLf)
        ''New Code Added By Vijay Ramteke On September 09, 2010 For Mantis Id 13650
        If Not StuEnrollId = "" Then
            sb.AppendFormat("       AND SE.StudentId = {0} And SE.StuEnrollId= {1} AND CSA.MeetDate >= CS.StartDate {2}", StudentId, StuEnrollId, vbCrLf)
        Else
            sb.AppendFormat("       AND SE.StudentId = {0} AND CSA.MeetDate >= CS.StartDate {1}", StudentId, vbCrLf)
        End If
        ''New Code Added By Vijay Ramteke On September 09, 2010 For Mantis Id 13650
        ''sb.AppendFormat("       AND SE.StudentId = {0} AND CSA.MeetDate >= CS.StartDate {1}", StudentId, vbCrLf)

        sb.Append("       AND CSA.MeetDate <= '" & Date.Today.Add(New TimeSpan(1, 0, 0, 0)) & "' " + vbCrLf)
        ' add any filters from the report ui
        If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterList, vbCrLf)
        End If
        sb.Append(" Union ")
        sb.Append(" select Distinct t3.LastName + ', ' + t3.FirstName  As StudentName, " + vbCrLf)
        sb.Append(" t2.StuEnrollId,t2.PrgVerId,(select Distinct PrgVerDescrip from arPrgVersions where PrgVerId=t2.PrgVerId) as PrgVerDescrip, " + vbCrLf)
        sb.Append(" NULL as ClsSectionId,NULL AS  ClsSectMeetingId ,t1.RecordDate,case when (t1.ActualHours is NULL or t1.ActualHours=999.00) then '0.00' else t1.ActualHours end as ActualHours,  t1.IsTardy,NULL as Excused " + vbCrLf)
        sb.Append("  from arStudentClockAttendance t1,arStuEnrollments t2,arStudent t3 " + vbCrLf)
        sb.Append("  where t1.StuEnrollId=t2.StuEnrollId and t2.StudentId=t3.StudentId and  " + vbCrLf)
        ''New Code Added By Vijay Ramteke On September 09, 2010 For Mantis Id 13650
        If Not StuEnrollId = "" Then
            sb.AppendFormat("  t3.StudentId = {0} and t2.StuEnrollId = {1} {2}", StudentId, StuEnrollId, vbCrLf)
        Else
            sb.AppendFormat("  t3.StudentId = {0} {1}", StudentId, vbCrLf)
        End If
        ''sb.AppendFormat("  t3.StudentId = {0} ", StudentId, vbCrLf)
        ''New Code Added By Vijay Ramteke On September 09, 2010 For Mantis Id 13650

        'sb.Append("ORDER BY CSA.ClsSectionId, CSA.MeetDate" + vbCrLf)
        sb.Append(" ORDER BY PrgVerDescrip ")
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)
    End Function
    ''' <summary>
    ''' Creates a data table with the proper schema we are looking for
    ''' to return to the caller
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function CreateReportDataTableSchema() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("StudentName", Type.GetType("System.String"))        'arStudent.LastName & arStudent.FirstName
        dt.Columns.Add("PrgVerId", Type.GetType("System.Guid"))             'arStuEnrollments.PrgVerId
        dt.Columns.Add("PrgVerDescrip", Type.GetType("System.String"))      'arPrgVersions.PrgVerDescrip
        dt.Columns.Add("TermId", Type.GetType("System.Guid"))               'arTerm.TermId
        dt.Columns.Add("TermDescrip", Type.GetType("System.String"))        'arTerm.TermDescrip
        dt.Columns.Add("ClsSectionId", Type.GetType("System.Guid"))         'atClsSectAttendance.ClsSectionId
        dt.Columns.Add("ClsSectMeetingId", Type.GetType("System.Guid"))         'atClsSectAttendance.ClsSectionId
        dt.Columns.Add("ReqId", Type.GetType("System.String"))              'arClassSections.ReqId
        dt.Columns.Add("ClassName", Type.GetType("System.String"))          'arReqs.Descrip
        dt.Columns.Add("InstructorId", Type.GetType("System.Guid"))         'arClassSections.InstructorId
        dt.Columns.Add("InstructorName", Type.GetType("System.String"))     'syUsers.FullName
        dt.Columns.Add("ClassStartDate", Type.GetType("System.DateTime"))   'arClassSections.StartDate
        dt.Columns.Add("ClassEndDate", Type.GetType("System.DateTime"))     'arClassSections.EndDate
        dt.Columns.Add("MeetDate", Type.GetType("System.DateTime"))         'atClsSectAttendance.MeetDate
        dt.Columns.Add("Actual", Type.GetType("System.Int32"))              'atClsSectAttendance.Actual
        dt.Columns.Add("Tardy", Type.GetType("System.Int32"))               'atClsSectAttendance.Tardy
        dt.Columns.Add("Excused", Type.GetType("System.Int32"))             'atClsSectAttendance.Excused
        dt.Columns.Add("Absent", Type.GetType("System.Int32"))              'Absent - calculated
        dt.Columns.Add("MeetNumber", Type.GetType("System.Int32"))          'Meeting number for specific class
        dt.Columns.Add("AttendanceType", Type.GetType("System.String"))     'Either PA or MINUTES
        dt.Columns.Add("NumTardiesMakingAbsent", Type.GetType("System.Int32")) 'Number of tardies making absent
        dt.Columns.Add("AttendanceStr", Type.GetType("System.String"))      'Attendance string
        dt.Columns.Add("TotalPresent", Type.GetType("System.Decimal"))
        dt.Columns.Add("TotalTardies", Type.GetType("System.Decimal"))
        dt.Columns.Add("TotalAbsent", Type.GetType("System.Decimal"))
        dt.Columns.Add("TotalExcused", Type.GetType("System.Decimal"))
        dt.Columns.Add("TotalPresentAdjusted", Type.GetType("System.Decimal"))
        dt.Columns.Add("TotalTardiesAdjusted", Type.GetType("System.Decimal"))
        dt.Columns.Add("TotalAbsentAdjusted", Type.GetType("System.Decimal"))
        dt.Columns.Add("TotalExcusedAdjusted", Type.GetType("System.Decimal"))
        dt.Columns.Add("TotalScheduled", Type.GetType("System.Decimal"))
        dt.Columns.Add("TotalMakeUp", Type.GetType("System.Decimal"))
        dt.Columns.Add("SuppressDate", Type.GetType("System.String"))
        dt.Columns.Add("InstructionTypeID", Type.GetType("System.String"))
        dt.Columns.Add("ClsSectMeetingDesc", Type.GetType("System.String"))
        dt.Columns.Add("TimeIntervalDescrip", Type.GetType("System.String"))
        dt.Columns.Add("StuEnrollId", Type.GetType("System.Guid"))

        Return dt
    End Function
#End Region

#End Region
End Class
