' ===============================================================================
' CRSAPCheckResults.vb
' Retrieves the data to support the SAP Check Results report
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' Notes: 

Imports System.Text
Imports FAME.Advantage.Common

Public Class CRSapCheckResults
    Inherits BaseReportFacade

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim StartDate As String = ""
        Dim EndDate As String = ""
        Dim ds As DataSet = GetSAPResults(rptParamInfo)
        ds.DataSetName = "dsSAPCheckResults"
        ds.Tables(0).TableName = "SAPCheckResults"
        Return ds
    End Function


#Region "Helper"
    Protected Function GetSAPResults(ByVal rptParamInfo As Common.ReportParamInfo) As DataSet
        'connect to the database
        Dim db As New FAME.AdvantageV1.DataAccess.DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim strSuppressDate As String = "no"
        Try
            strSuppressDate = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
        Catch ex As System.Exception
            strSuppressDate = "no"
        End Try

        Dim sb As New StringBuilder
        sb.Append("select " + vbCrLf)
        sb.Append("     arStudent.StudentId, " + vbCrLf)
        sb.Append("     arSAPChkResults.Period, " + vbCrLf)
        sb.Append("     arSAPChkResults.ModDate, " + vbCrLf)
        sb.Append("     arSAPChkResults.GPA, " + vbCrLf)
        sb.Append("     arSAPChkResults.Comments, " + vbCrLf)
        sb.Append("     CASE arSAPChkResults.IsMakingSAP when 1 then 'Yes' else 'No' end as MakingSAP, " + vbCrLf)
        sb.Append("     arStudent.FirstName, " + vbCrLf)
        sb.Append("     arStudent.LastName, " + vbCrLf)
        sb.Append("     arPrgVersions.PrgVerId, " + vbCrLf)
        sb.Append("     arPrgVersions.PrgVerDescrip, " + vbCrLf)
        sb.Append("     arStuEnrollments.CampusId, " + vbCrLf)
        sb.Append("     (select CampDescrip from syCampuses C where C.CampusId = arStuEnrollments.CampusId) as CampDescrip, " + vbCrLf)
        sb.Append("     arPrgVersions.CampGrpId, " + vbCrLf)
        sb.Append("     syCampGrps.CampGrpDescrip, " + vbCrLf)
        sb.Append("'" & strSuppressDate & "'")
        sb.Append(" as SuppressDate ")
        sb.Append("from " + vbCrLf)
        sb.Append("     syCampGrps, " + vbCrLf)
        sb.Append("     arSAPChkResults, " + vbCrLf)
        sb.Append("     arStuEnrollments, " + vbCrLf)
        sb.Append("     arStudent, " + vbCrLf)
        sb.Append("     arPrgVersions, " + vbCrLf)
        sb.Append("     arSAPDetails, " + vbCrLf)
        sb.Append("     syCmpGrpCmps " + vbCrLf)
        sb.Append("where " + vbCrLf)
        'sb.Append("     syCampGrps.CampGrpId=arPrgVersions.CampGrpId " + vbCrLf)
        sb.Append("    syCmpGrpCmps.campusid=arStuEnrollments.campusid " + vbCrLf)
        sb.Append("   and syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId " + vbCrLf)
        sb.Append("     and arSAPChkResults.StuEnrollId = arStuEnrollments.StuEnrollId " + vbCrLf)
        sb.Append("     and arStuEnrollments.StudentId = arStudent.StudentId " + vbCrLf)
        sb.Append("     and arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId " + vbCrLf)
        sb.Append("     and arSAPChkResults.SAPDetailId = arSAPDetails.SAPDetailId  and arSAPChkResults.PreviewSapCheck = 0 " + vbCrLf)

        ' add the filter list

        If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterList, vbCrLf)
        End If
        If Not rptParamInfo.FilterOther Is Nothing AndAlso rptParamInfo.FilterOther <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterOther, vbCrLf)
        End If

        sb.Append("     order by arSAPChkResults.moddate desc ")
        'If Not rptParamInfo.OrderBy Is Nothing AndAlso rptParamInfo.OrderBy <> "" Then
        '    sb.AppendFormat("order by {0} {1}", rptParamInfo.OrderBy, vbCrLf)
        'End If

        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
#End Region
End Class
