' ===============================================================================
' CRClockHourAttendance.vb
' Retrieves the data to support the Clock Hour Attendance
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' Notes: 

Imports System.Text
Imports FAME.Advantage.Common


Public Class CRClockHourAttendance
    Inherits BaseReportFacade

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim ds As DataSet = GetClockAttendance(rptParamInfo)
        ds.DataSetName = "dsClockAttendance"
        ds.Tables(0).TableName = "Attendance"
        Return ds
    End Function

#Region "Helper"
    Protected Function GetClockAttendance(ByVal rptParamInfo As Common.ReportParamInfo) As DataSet
        'connect to the database
        Dim db As New FAME.AdvantageV1.DataAccess.DataAccess
        Dim RecordStartDate As Date
        Dim RecordEndDate As Date
        Dim NoDays As Long

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim rtn As String = getDateRange(rptParamInfo.FilterOtherString)
        If (rtn <> String.Empty) Then
            RecordStartDate = CDate(rtn.Trim)
            RecordEndDate = DateAdd(DateInterval.Day, 6, RecordStartDate)
            'NoDays = DateDiff(DateInterval.Day, RecordStartDate, RecordEndDate)
            NoDays = 6
        End If
        Dim strFilter As String
        Dim strRecordDate As String = "arStudentClockAttendance.recorddate BETWEEN '" + RecordStartDate & "' AND '" + RecordEndDate + "'"
        If Not rptParamInfo.FilterOther Is Nothing AndAlso rptParamInfo.FilterOther <> "" Then
            If (rptParamInfo.FilterOther.ToLower.IndexOf("arstuenrollments.startdate") <> "-1") Then
                strFilter = strRecordDate + " AND " + rptParamInfo.FilterOther.Substring(rptParamInfo.FilterOther.ToLower.IndexOf("arstuenrollments.startdate"))
                'strFilter = rptParamInfo.FilterOther.Substring(rptParamInfo.FilterOther.ToLower.IndexOf("arstuenrollments.startdate"))
            Else
                strFilter = strRecordDate
                'strFilter = ""
            End If
            rptParamInfo.FilterOther = strFilter
        End If
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        Dim dtMain As DataTable


        sb.Append("     select distinct arStuEnrollments.StuEnrollId,arStudent.FirstName,arStudent.LastName,  ")
        sb.Append(" syCampGrps.CampGrpDescrip,  " + vbCrLf)
        sb.Append("arStuEnrollments.CampusId,  " + vbCrLf)
        sb.Append("(select CampDescrip from syCampuses C where C.CampusId=arStuEnrollments.CampusId) as CampDesrip  " + vbCrLf)
        sb.Append(" from arStudentClockAttendance, " + vbCrLf)
        sb.Append("  arStuEnrollments, " + vbCrLf)
        sb.Append("     arStudent, " + vbCrLf)
        sb.Append("     arPrgVersions PV, " + vbCrLf)
        sb.Append("     arPrograms, " + vbCrLf)
        sb.Append("     syCampGrps, " + vbCrLf)
        sb.Append("     syCmpGrpCmps" + vbCrLf)
        sb.Append("     where" + vbCrLf)
        sb.Append("     arStudentClockAttendance.StuEnrollId=arStuEnrollments.StuEnrollId and" + vbCrLf)
        sb.Append("     arStudent.StudentId = arStuEnrollments.StudentId" + vbCrLf)
        sb.Append("     AND arStuEnrollments.PrgVerId = PV.PrgVerId " + vbCrLf)
        sb.Append("     AND PV.ProgId = arPrograms.ProgId " + vbCrLf)
        sb.Append("     and arStuEnrollments.CampusId=syCmpGrpCmps.CampusId " + vbCrLf)
        sb.Append("     AND syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId" + vbCrLf)
        ' add the filter list
        If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterList, vbCrLf)
        End If
        If Not rptParamInfo.FilterOther Is Nothing AndAlso rptParamInfo.FilterOther <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterOther, vbCrLf)
        End If
        sb.Append(vbCrLf + vbCrLf)
        If Not rptParamInfo.OrderBy Is Nothing AndAlso rptParamInfo.OrderBy <> "" Then
            sb.AppendFormat("order by {0} {1}", rptParamInfo.OrderBy, vbCrLf)
        End If
        Dim index As Integer
        Dim dr As DataRow
        Dim dt As New DataTable("dtNew")
        Dim dtClock As DataTable
        Dim i As Integer
        Dim j As Integer
        Dim strDate As Date

        dt.Columns.Add("CampGrpDescrip", Type.GetType("System.String"))
        dt.Columns.Add("CampDesrip", Type.GetType("System.String"))
        dt.Columns.Add("StuEnrollId", Type.GetType("System.String"))
        dt.Columns.Add("FirstName", Type.GetType("System.String"))
        dt.Columns.Add("LastName", Type.GetType("System.String"))
        dt.Columns.Add("StartDate", Type.GetType("System.String"))
        dt.Columns.Add("Day1", Type.GetType("System.String"))
        dt.Columns.Add("Day2", Type.GetType("System.String"))
        dt.Columns.Add("Day3", Type.GetType("System.String"))
        dt.Columns.Add("Day4", Type.GetType("System.String"))
        dt.Columns.Add("Day5", Type.GetType("System.String"))
        dt.Columns.Add("Day6", Type.GetType("System.String"))
        dt.Columns.Add("Day7", Type.GetType("System.String"))
        'For index = 0 To NoDays
        '    dt.Columns.Add("Day" + index, Type.GetType("System.String"))
        'Next

        dtMain = db.RunParamSQLDataSet(sb.ToString).Tables(0)
        If dtMain.Rows.Count > 0 Then
            'dr = dt.NewRow
            'dr("FirstName") = "Student"
            'dr("Day1") = RecordStartDate.DayOfWeek.ToString.Substring(0, 3)
            'dr("Day2") = DateAdd(DateInterval.Day, 1, RecordStartDate).DayOfWeek.ToString.Substring(0, 3)
            'dr("Day3") = DateAdd(DateInterval.Day, 2, RecordStartDate).DayOfWeek.ToString.Substring(0, 3)
            'dr("Day4") = DateAdd(DateInterval.Day, 3, RecordStartDate).DayOfWeek.ToString.Substring(0, 3)
            'dr("Day5") = DateAdd(DateInterval.Day, 4, RecordStartDate).DayOfWeek.ToString.Substring(0, 3)
            'dr("Day6") = DateAdd(DateInterval.Day, 5, RecordStartDate).DayOfWeek.ToString.Substring(0, 3)
            'dr("Day7") = DateAdd(DateInterval.Day, 6, RecordStartDate).DayOfWeek.ToString.Substring(0, 3)
            'dt.Rows.Add(dr)
            For index = 0 To dtMain.Rows.Count - 1
                dr = dt.NewRow

                'dtClock = getClockStudentAttendance(dtMain.Rows(index)("StuEnrollId").ToString, strRecordDate)
                dtClock = getClockStudentAttendance(dtMain.Rows(index)("StuEnrollId").ToString)
                If dtClock.Rows.Count > 0 Then
                    dr("CampGrpDescrip") = dtMain.Rows(index)("CampGrpDescrip").ToString
                    dr("CampDesrip") = dtMain.Rows(index)("CampDesrip").ToString
                    dr("StuEnrollId") = dtMain.Rows(index)("StuEnrollId").ToString
                    dr("FirstName") = dtMain.Rows(index)("FirstName")
                    dr("LastName") = dtMain.Rows(index)("LastName")
                    dr("StartDate") = RecordStartDate
                    For i = 0 To NoDays
                        dr("Day" + (i + 1).ToString) = "Absent"
                        strDate = DateAdd(DateInterval.Day, i, RecordStartDate)
                        For j = 0 To dtClock.Rows.Count - 1
                            If strDate = CDate(dtClock.Rows(j)("RecordDate")) Then
                                dr("Day" + (i + 1).ToString) = dtClock.Rows(j)("ActualHours").ToString
                                Exit For
                            End If
                        Next
                    Next

                End If


                dt.Rows.Add(dr)

            Next
        End If
        ds.Tables.Add(dt)
        Return ds
    End Function
    Private Function getDateRange(ByVal str As String) As String

        Dim strResult As String
        If (str.IndexOf("Record Start Date Equal To") <> "-1") Then
            strResult = str.Replace("Record Start Date Equal To", "").Trim
            If (strResult.IndexOf(" ") <> "-1") Then
                strResult = strResult.Substring(0, strResult.IndexOf(" "))
            End If
            Return strResult
        End If
        Return strResult
    End Function

    Private Function getClockStudentAttendance(ByVal stuEnrollID As String) As DataTable
        Dim db As New FAME.AdvantageV1.DataAccess.DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New StringBuilder
        sb.Append(" select RecordDate,ActualHours from arStudentClockAttendance where StuEnrollId='" & stuEnrollID & "'" + vbCrLf)
        'sb.Append(" AND " & strRecordDate + vbCrLf)
        Return db.RunParamSQLDataSet(sb.ToString).Tables(0)

    End Function

    Private Function getRecordDate(ByVal RecordSDate As Date, ByVal NoDays As Long, ByVal RecordDate As Date) As String
        Dim index As Integer
        Dim strDate As Date
        For index = 0 To NoDays
            strDate = DateAdd(DateInterval.Day, index, RecordSDate)
            If (strDate = RecordDate) Then
                Return RecordDate
            End If
        Next
        Return ""

    End Function
#End Region

End Class
