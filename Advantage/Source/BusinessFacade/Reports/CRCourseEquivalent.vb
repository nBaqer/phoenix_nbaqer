' ===============================================================================
' CDateRangeGrades.vb
' Retrieves the data to support the Grades by Date Range
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' Notes: 

Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports FAME.Advantage.Common


Public Class CRCourseEquivalent
    Inherits BaseReportFacade

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim ds As DataSet = GetCourses(rptParamInfo)
        ds.DataSetName = "dsEquivalentCourses"
        ds.Tables(0).TableName = "Courses"
        Return ds
    End Function

#Region "Helper"
    Protected Function GetCourses(ByVal rptParamInfo As Common.ReportParamInfo) As DataSet
        'connect to the database
        Dim db As New FAME.AdvantageV1.DataAccess.DataAccess
        Dim ds As New DataSet
        Dim dt As New DataTable

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        With sb
            .Append(" select arCourseEquivalent.Reqid,arReqs.Descrip as course, syCampGrps.CampGrpDescrip as CampGrpDescrip, ")
            .Append(" (select CampDescrip from syCampuses C where C.CampusId=syCmpGrpCmps.CampusId) as CampDesrip,arReqs.Code,arReqs.Credits,arReqs.Hours ")
            .Append(" from arCourseEquivalent, arPrgVersions, arProgVerDef, syCampGrps,syCmpGrpCmps,syCampuses,  arReqs ")
            .Append(" where arCourseEquivalent.ReqId = arProgVerDef.Reqid And arPrgVersions.PrgVerid = arProgVerDef.PrgVerid ")
            '.Append(" and syCampGrps.CampGrpId=arPrgVersions.CampGrpId ")
            .Append(" and arReqs.Reqid=arCourseEquivalent.Reqid and syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId ")
            .Append("  AND syCmpGrpCmps.CampusId=syCampuses.CampusId  ")
            If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
                .AppendFormat("   AND {0} {1}", rptParamInfo.FilterList, vbCrLf)
            End If
            .Append(" group by arCourseEquivalent.Reqid,arReqs.Descrip,syCampGrps.CampGrpDescrip,syCmpGrpCmps.CampusId,arReqs.Code,arReqs.Credits,arReqs.Hours  ")
        End With


        ds = db.RunParamSQLDataSet(sb.ToString)
        dt = ds.Tables(0)
        dt.Columns.Add("EquivCourses", Type.GetType("System.String"))
        For i As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(i)("EquivCourses") = GetEquivalentCourses(dt.Rows(i)("reqid").ToString)
        Next
        Return ds
    End Function
    Public Function GetEquivalentCourses(ByVal Reqid As String) As String
        ''     Dim da As OleDbDataAdapter
        Dim equivCourses As String = String.Empty


        '   connect to the database
        Dim db As New FAME.AdvantageV1.DataAccess.DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim sb As New System.Text.StringBuilder

        'Changed this function to include sub-query for StudentId & StuEnrollId to accomodate
        'situation when the Student did the course (and received a grade) in another enrollment.
        '#BN 05/06/05

        With sb
            .Append(" select arReqs.Reqid,'(' + Code + ') ' + Descrip  as Descrip  from arCourseEquivalent,arReqs where ")
            .Append(" arCourseEquivalent.EquivReqId= arReqs.Reqid and ")
            .Append(" arCourseEquivalent.Reqid= ? ")
        End With
        ' Add the student enrollid and ClsSectId to the parameter list
        db.AddParameter("@reqId", Reqid, db.OleDbDataType.OleDbString, , ParameterDirection.Input)

        ' Add the student enrollid and ClsSectId to the parameter list

        '   build the sql query

        '   Execute the query
        db.OpenConnection()

        'Execute the query
        Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)

        While dr.Read()
            If equivCourses <> "" Then
                equivCourses = equivCourses & "," & dr("Descrip")
            Else
                equivCourses = dr("Descrip")
            End If
        End While

        If Not dr.IsClosed Then dr.Close()
        If db.Connection.State=ConnectionState.Open Then db.CloseConnection()

        'Return the datatable in the dataset
        Return equivCourses

    End Function
#End Region

End Class
