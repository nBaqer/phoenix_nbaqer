' ===============================================================================
' CDateRangeGrades.vb
' Retrieves the data to support the Grades by Date Range
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' Notes: 

Imports System.Text
Imports FAME.Advantage.Common

Public Class CRDateRangeGrades
    Inherits BaseReportFacade

#Region "Private Data Members"

    'Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
    Private m_StudentIdentifier As String

#End Region

#Region "Public Properties"

    Public ReadOnly Property StudentIdentifier() As String
        Get
            Return m_StudentIdentifier
        End Get
    End Property

#End Region

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        m_StudentIdentifier = MyAdvAppSettings.AppSettings("StudentIdentifier")


        Dim ds As DataSet = GetGrades(rptParamInfo)
        ds.DataSetName = "dsGrades"
        ds.Tables(0).TableName = "Grades"
        ''Added on jan 2009 8th by Saraswathi
        ''To show the studentIdentifier and Class Code in the report
        ''For mantis issue 15109

        Dim stuName As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String
        Dim oldCmpGrp As String = ""
        Dim oldCampus As String = ""
        Dim dt As DataTable
        Dim dr2 As DataRow
        'Get the mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    dt = ds.Tables(0)
                    For Each dr As DataRow In ds.Tables(0).Rows
                        'Apply mask to SSN.
                        If StudentIdentifier = "SSN" Then
                            If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                                If dr("StudentIdentifier") <> "" Then
                                    Dim temp As String = dr("StudentIdentifier")
                                    dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                End If
                            End If
                        End If
                    Next

                End If
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try
        Return ds
    End Function

#Region "Helper"
    Protected Function GetGrades(ByVal rptParamInfo As Common.ReportParamInfo) As DataSet
        'connect to the database
        Dim db As New FAME.AdvantageV1.DataAccess.DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
        Dim strStudentId As String = ""
        If StudentIdentifier = "SSN" Then
            strStudentId = "arStudent.SSN AS StudentIdentifier,"
        ElseIf StudentIdentifier = "EnrollmentId" Then
            strStudentId = "arStuEnrollments.EnrollmentId AS StudentIdentifier,"
        ElseIf StudentIdentifier = "StudentId" Then
            strStudentId = "arStudent.StudentNumber AS StudentIdentifier,"
        End If

        Dim sb As New StringBuilder
        'sb.Append("(" + vbCrLf)
        'sb.Append("select distinct " + vbCrLf)
        'sb.Append("     PV.CampGrpId, " + vbCrLf)
        'sb.Append("     syCampGrps.CampGrpDescrip, " + vbCrLf)
        'sb.Append("     arStuEnrollments.CampusId, " + vbCrLf)
        'sb.Append("     (select CampDescrip from syCampuses C where C.CampusId=arStuEnrollments.CampusId) as CampDesrip, " + vbCrLf)
        'sb.Append("     arPrograms.ProgId, " + vbCrLf)
        'sb.Append("     arPrograms.ProgDescrip, " + vbCrLf)
        'sb.Append("     arStuEnrollments.StuEnrollId, " + vbCrLf)
        'sb.Append("     arStudent.FirstName, " + vbCrLf)
        'sb.Append("     arStudent.LastName, " + vbCrLf)
        'sb.Append("     REQ.Descrip as ClassName, " + vbCrLf)
        'sb.Append("     arGradeSystemDetails.Grade, " + vbCrLf)
        'sb.Append("     SC.StatusCodeDescrip, " + vbCrLf)
        'sb.Append("     arStuEnrollments.StartDate, " + vbCrLf)
        'sb.Append("     TG.ModDate, " + vbCrLf)
        'sb.Append("     arGradeSystemDetails.GrdSysDetailId " + vbCrLf)
        'If Not rptParamInfo.OrderBy Is Nothing AndAlso rptParamInfo.OrderBy <> "" Then
        '    sb.Append(" , " + rptParamInfo.OrderBy + vbCrLf)
        'End If
        'sb.Append("from " + vbCrLf)
        'sb.Append("     arStuEnrollments, " + vbCrLf)
        'sb.Append("     arStudent, " + vbCrLf)
        'sb.Append("     arTransferGrades TG, " + vbCrLf)
        'sb.Append("     arReqs REQ, " + vbCrLf)
        'sb.Append("     arPrgVersions PV, " + vbCrLf)
        'sb.Append("     arPrograms, " + vbCrLf)
        'sb.Append("     arGradeSystemDetails, " + vbCrLf)
        'sb.Append("     syStatusCodes SC, " + vbCrLf)
        'sb.Append("     syCampGrps, " + vbCrLf)
        'sb.Append("     arClassSectionTerms CST, " + vbCrLf)
        'sb.Append("     arClassSections " + vbCrLf)
        'sb.Append("where  " + vbCrLf)
        'sb.Append("     arStudent.StudentId = arStuEnrollments.StudentId " + vbCrLf)
        'sb.Append("     AND arStuEnrollments.StuEnrollId = TG.StuEnrollId " + vbCrLf)
        'sb.Append("     AND arStuEnrollments.PrgVerId = PV.PrgVerId " + vbCrLf)
        'sb.Append("     AND PV.ProgId = arPrograms.ProgId " + vbCrLf)
        'sb.Append("     AND TG.GrdSysDetailId = arGradeSystemDetails.GrdSysDetailId " + vbCrLf)
        'sb.Append("     AND TG.ReqId = Req.ReqId " + vbCrLf)
        'sb.Append("     AND TG.TermId = CST.TermId " + vbCrLf)
        'sb.Append("     and CST.ClsSectionId=arClassSections.ClsSectionId  " + vbCrLf)
        'sb.Append("     AND arStuEnrollments.StatusCodeId = SC.StatusCodeId " + vbCrLf)
        'sb.Append("     AND PV.CampGrpId = syCampGrps.CampGrpId " + vbCrLf)
        '' add the filter list
        'If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
        '    sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterList, vbCrLf)
        'End If
        'If Not rptParamInfo.FilterOther Is Nothing AndAlso rptParamInfo.FilterOther <> "" Then
        '    sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterOther, vbCrLf)
        'End If

        'sb.Append(vbCrLf + "union" + vbCrLf + vbCrLf)


        sb.Append("select " + vbCrLf)
        sb.Append("     PV.CampGrpId, " + vbCrLf)
        sb.Append("     syCampGrps.CampGrpDescrip, " + vbCrLf)
        sb.Append("     arStuEnrollments.CampusId, " + vbCrLf)
        sb.Append("     (select CampDescrip from syCampuses C where C.CampusId=arStuEnrollments.CampusId) as CampDesrip, " + vbCrLf)
        sb.Append("     arPrograms.ProgId, " + vbCrLf)
        sb.Append("     arPrograms.ProgDescrip, " + vbCrLf)
        sb.Append("     arStuEnrollments.StuEnrollId, " + vbCrLf)
        sb.Append("     arStudent.FirstName, " + vbCrLf)
        sb.Append("     arStudent.LastName, " + vbCrLf)
        ''ReqCode and StudentIdentifier Added on jan 8 2009 by Saraswathi
        ''As per the mantis 15109
        sb.Append("     REQ.Code as ClassCode, " + vbCrLf)
        sb.Append(strStudentId)
        sb.Append("     REQ.Descrip as ClassName, " + vbCrLf)
        sb.Append("     arGradeSystemDetails.Grade, " + vbCrLf)
        sb.Append("     SC.StatusCodeDescrip, " + vbCrLf)
        sb.Append("     arClassSections.StartDate, " + vbCrLf)
        sb.Append("     R.ModDate, " + vbCrLf)
        sb.Append("     arGradeSystemDetails.GrdSysDetailId  " + vbCrLf)
        If Not rptParamInfo.OrderBy Is Nothing AndAlso rptParamInfo.OrderBy <> "" Then
            sb.Append(" , " + rptParamInfo.OrderBy + vbCrLf)
        End If
        sb.Append(" from " + vbCrLf)
        sb.Append("     arStuEnrollments, " + vbCrLf)
        sb.Append("     arStudent, " + vbCrLf)
        sb.Append("     arResults R, " + vbCrLf)
        sb.Append("     arClassSections , " + vbCrLf)
        sb.Append("     arReqs REQ, " + vbCrLf)
        sb.Append("     arPrgVersions PV, " + vbCrLf)
        sb.Append("     arPrograms, " + vbCrLf)
        sb.Append("     arGradeSystemDetails, " + vbCrLf)
        sb.Append("     syStatusCodes SC, " + vbCrLf)
        sb.Append("     syCampGrps, " + vbCrLf)
        sb.Append("     syCmpGrpCmps, " + vbCrLf)
        sb.Append("     Sycampuses " + vbCrLf)
        sb.Append("where  " + vbCrLf)
        sb.Append("     arStudent.StudentId = arStuEnrollments.StudentId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.StuEnrollId = R.StuEnrollId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.PrgVerId = PV.PrgVerId " + vbCrLf)
        sb.Append("     AND PV.ProgId = arPrograms.ProgId " + vbCrLf)
        sb.Append("     AND R.GrdSysDetailId = arGradeSystemDetails.GrdSysDetailId " + vbCrLf)
        sb.Append("     AND R.TestId = arClassSections.ClsSectionId " + vbCrLf)
        sb.Append("     AND arClassSections.ReqId = REQ.ReqId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.StatusCodeId = SC.StatusCodeId " + vbCrLf)
        sb.Append("     and arStuEnrollments.CampusId=syCmpGrpCmps.CampusId " + vbCrLf)
        sb.Append("     and arStuEnrollments.CampusId=Sycampuses.CampusId " + vbCrLf)
        sb.Append("     AND syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId " + vbCrLf)
        ' add the filter list
        If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterList, vbCrLf)
        End If
        If Not rptParamInfo.FilterOther Is Nothing AndAlso rptParamInfo.FilterOther <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterOther, vbCrLf)
        End If

        sb.Append(" Union ")

        sb.Append("     select " + vbCrLf)
        sb.Append("     PV.CampGrpId, " + vbCrLf)
        sb.Append("     syCampGrps.CampGrpDescrip, " + vbCrLf)
        sb.Append("     arStuEnrollments.CampusId, " + vbCrLf)
        sb.Append("     (select CampDescrip from syCampuses C where C.CampusId=arStuEnrollments.CampusId) as CampDesrip, " + vbCrLf)
        sb.Append("     arPrograms.ProgId, " + vbCrLf)
        sb.Append("     arPrograms.ProgDescrip, " + vbCrLf)
        sb.Append("     arStuEnrollments.StuEnrollId, " + vbCrLf)
        sb.Append("     arStudent.FirstName, " + vbCrLf)
        sb.Append("     arStudent.LastName, " + vbCrLf)
        ''ReqCode and StudentIdentifier Added on jan 8 2009 by Saraswathi
        ''As per the mantis 15109
        sb.Append("     REQ.Code as ClassCode, " + vbCrLf)
        sb.Append(strStudentId)
        sb.Append("     REQ.Descrip as ClassName, " + vbCrLf)
        sb.Append("     arGradeSystemDetails.Grade, " + vbCrLf)
        sb.Append("     SC.StatusCodeDescrip, " + vbCrLf)
        sb.Append("     arTerm.StartDate, " + vbCrLf)
        sb.Append("     R.ModDate, " + vbCrLf)
        sb.Append("     arGradeSystemDetails.GrdSysDetailId " + vbCrLf)
        If Not rptParamInfo.OrderBy Is Nothing AndAlso rptParamInfo.OrderBy <> "" Then
            sb.Append(" , " + rptParamInfo.OrderBy + vbCrLf)
        End If
        sb.Append("     from " + vbCrLf)
        sb.Append("     arStuEnrollments, " + vbCrLf)
        sb.Append("     arStudent, " + vbCrLf)
        sb.Append("     arTransferGrades R, " + vbCrLf)
        ''Query Modified by Saraswathi on october 13th 2008
        ''The ClassSartDate is selected from arclassSection and
        '' CohortStartDate is selected from arstuenrollments
        ''The TermId and ReqId of arclasssection are matched against 
        ''arterm and arReqs
        sb.Append("     arClassSections ," + vbCrLf)

        sb.Append("     arTerm ," + vbCrLf)
        sb.Append("     arReqs REQ, " + vbCrLf)
        sb.Append("     arPrgVersions PV, " + vbCrLf)
        sb.Append("     arPrograms, " + vbCrLf)
        sb.Append("     arGradeSystemDetails, " + vbCrLf)
        sb.Append("     syStatusCodes SC, " + vbCrLf)
        sb.Append("     syCampGrps, " + vbCrLf)
        sb.Append("     syCmpGrpCmps, " + vbCrLf)
        sb.Append("     Sycampuses " + vbCrLf)
        sb.Append("     where " + vbCrLf)
        sb.Append("     arStudent.StudentId = arStuEnrollments.StudentId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.StuEnrollId = R.StuEnrollId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.PrgVerId = PV.PrgVerId " + vbCrLf)
        sb.Append("     AND PV.ProgId = arPrograms.ProgId " + vbCrLf)
        sb.Append("     AND R.GrdSysDetailId = arGradeSystemDetails.GrdSysDetailId " + vbCrLf)
        sb.Append("     and R.TermId=arTerm.TermId" + vbCrLf)
        sb.Append("     AND R.ReqId = REQ.ReqId " + vbCrLf)
        ''Query Modified
        sb.Append("     and arClassSections.TermId=arTerm.TermId" + vbCrLf)
        sb.Append("     AND arClassSections.ReqId =REQ.ReqId " + vbCrLf)

        sb.Append("     AND arStuEnrollments.StatusCodeId = SC.StatusCodeId " + vbCrLf)
        sb.Append("     and arStuEnrollments.CampusId=syCmpGrpCmps.CampusId " + vbCrLf)
        sb.Append("     and arStuEnrollments.CampusId=Sycampuses.CampusId " + vbCrLf)
        sb.Append("     AND syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId " + vbCrLf)

        If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterList, vbCrLf)
        End If
        If Not rptParamInfo.FilterOther Is Nothing AndAlso rptParamInfo.FilterOther <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterOther, vbCrLf)
        End If

        sb.Append(vbCrLf + vbCrLf)
        If Not rptParamInfo.OrderBy Is Nothing AndAlso rptParamInfo.OrderBy <> "" Then
            sb.AppendFormat("order by {0} {1}", rptParamInfo.OrderBy, vbCrLf)
        End If

        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
#End Region

    'Private Function GetAdvAppSettings() As AdvAppSettings
    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If
    'End Function

End Class
