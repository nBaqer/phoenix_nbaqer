' ===============================================================================
' CDateRangeGrades.vb
' Retrieves the data to support the Grades by Date Range
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' Notes: 

Imports System.Text
Imports FAME.Advantage.Common


Public Class CRCourseSequences
    Inherits BaseReportFacade

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim ds As DataSet = GetCourseSequences(rptParamInfo)
        ds.DataSetName = "dsCourseSequences"
        ds.Tables(0).TableName = "CourseSequences"
        Return ds
    End Function

#Region "Helper"
    Protected Function GetCourseSequences(ByVal rptParamInfo As Common.ReportParamInfo) As DataSet
        'connect to the database
        Dim db As New FAME.AdvantageV1.DataAccess.DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder


        sb.Append(" select  " + vbCrLf)
        If rptParamInfo.FilterListString.IndexOf("Campus Group Equal To (All)") <> -1 Then
            sb.Append("'All' as CampGrpDescrip,   " + vbCrLf)
        Else
            sb.Append(" syCampGrps.CampGrpDescrip,   " + vbCrLf)
        End If
        sb.Append(" syCampGrps.CampGrpDescrip,   " + vbCrLf)
        sb.Append(" syCmpGrpCmps.CampusId,   " + vbCrLf)
        sb.Append(" (select CampDescrip from syCampuses C where C.CampusId=syCmpGrpCmps.CampusId) as CampDesrip, " + vbCrLf)
        sb.Append(" PVD.TermNo, " + vbCrLf)
        sb.Append(" REQ.Descrip, " + vbCrLf)
        sb.Append(" REQ.Code," + vbCrLf)
        sb.Append(" REQ.Credits," + vbCrLf)
        sb.Append(" REQ.Hours," + vbCrLf)
        sb.Append(" arPrgVersions.PrgVerId, " + vbCrLf)
        sb.Append(" arPrgVersions.PrgVerDescrip " + vbCrLf)
        sb.Append(" from " + vbCrLf)
        sb.Append("  arReqs REQ,  " + vbCrLf)
        sb.Append(" arPrgVersions, " + vbCrLf)
        sb.Append(" arProgVerDef PVD, " + vbCrLf)
        sb.Append(" syCampGrps,  " + vbCrLf)
        sb.Append(" syCmpGrpCmps " + vbCrLf)
        sb.Append(" where " + vbCrLf)
        sb.Append(" arPrgVersions.PrgVerId = PVD.PrgVerId      AND " + vbCrLf)
        sb.Append(" PVD.ReqId = REQ.ReqId " + vbCrLf)
        sb.Append(" and arPrgVersions.CampGrpId=syCampGrps.CampGrpId " + vbCrLf)
        sb.Append(" AND syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId  " + vbCrLf)
        sb.Append(" and TermNo is not null  " + vbCrLf)
        If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
            If rptParamInfo.FilterListString.IndexOf("Campus Group Equal To (All)") <> -1 Then
                sb.Append(rptParamInfo.FilterList.Substring(rptParamInfo.FilterList.IndexOf("AND")))
            Else
                sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterList, vbCrLf)
            End If

        End If
        If Not rptParamInfo.FilterOther Is Nothing AndAlso rptParamInfo.FilterOther <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterOther, vbCrLf)
        End If
        sb.Append(vbCrLf + vbCrLf)
        sb.Append(" order by syCampGrps.CampGrpDescrip,CampDesrip, " + vbCrLf)
        sb.Append(" arPrgVersions.PrgVerDescrip, TermNo, REQ.Descrip  " + vbCrLf)

        ' add the filter list

        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
#End Region

End Class
