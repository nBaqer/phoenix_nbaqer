' ===============================================================================
' CDateRangeGrades.vb
' Retrieves the data to support the Grades by Date Range
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' Notes: 

Imports System.Text
Imports FAME.Advantage.Common


Public Class CRSummaryGrades
    Inherits BaseReportFacade

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim ds As DataSet = GetGrades(rptParamInfo)
        ds.DataSetName = "dsGradesSummary"
        ds.Tables(0).TableName = "Grades"
        Return ds
    End Function

#Region "Helper"
    Protected Function GetGrades(ByVal rptParamInfo As Common.ReportParamInfo) As DataSet
        'connect to the database
        Dim db As New FAME.AdvantageV1.DataAccess.DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder

        sb.Append(" select grade,sum(GradeCount)as count,CampGrpDescrip,CampusId,CampDesrip from ( " + vbCrLf)
        sb.Append("select " + vbCrLf)
        sb.Append(" arGradeSystemDetails.Grade as Grade, " + vbCrLf)
        sb.Append(" count(arGradeSystemDetails.Grade) as GradeCount, " + vbCrLf)
        sb.Append("syCampGrps.CampGrpDescrip as CampGrpDescrip ,  " + vbCrLf)
        sb.Append("arStuEnrollments.CampusId as CampusId,  " + vbCrLf)
        sb.Append("(select CampDescrip from syCampuses C where C.CampusId=arStuEnrollments.CampusId) as CampDesrip  " + vbCrLf)
        sb.Append("from " + vbCrLf)
        sb.Append("     arStuEnrollments, " + vbCrLf)
        sb.Append("     arStudent, " + vbCrLf)
        sb.Append("      (select  TestId,GrdSysDetailId,StuEnrollId from arResults group by TestId,GrdSysDetailId,StuEnrollId) R, " + vbCrLf)
        sb.Append("     arClassSections , " + vbCrLf)
        sb.Append("     arReqs REQ, " + vbCrLf)
        sb.Append("     arPrgVersions PV, " + vbCrLf)
        sb.Append("     arPrograms, " + vbCrLf)
        sb.Append("     arGradeSystemDetails, " + vbCrLf)
        sb.Append("     syStatusCodes SC, " + vbCrLf)
        sb.Append("     syCampGrps, " + vbCrLf)
        sb.Append("     syCmpGrpCmps, " + vbCrLf)
        sb.Append("     Sycampuses " + vbCrLf)
        sb.Append("where  " + vbCrLf)
        sb.Append("     arStudent.StudentId = arStuEnrollments.StudentId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.StuEnrollId = R.StuEnrollId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.PrgVerId = PV.PrgVerId " + vbCrLf)
        sb.Append("     AND PV.ProgId = arPrograms.ProgId " + vbCrLf)
        sb.Append("     AND R.GrdSysDetailId = arGradeSystemDetails.GrdSysDetailId " + vbCrLf)
        sb.Append("     AND R.TestId = arClassSections.ClsSectionId " + vbCrLf)
        sb.Append("     AND arClassSections.ReqId = REQ.ReqId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.StatusCodeId = SC.StatusCodeId " + vbCrLf)
        sb.Append("     and arStuEnrollments.CampusId=syCmpGrpCmps.CampusId " + vbCrLf)
        sb.Append("     and arStuEnrollments.CampusId=Sycampuses.CampusId " + vbCrLf)
        sb.Append("     AND syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId " + vbCrLf)
        ' add the filter list
        If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterList, vbCrLf)
        End If
        If Not rptParamInfo.FilterOther Is Nothing AndAlso rptParamInfo.FilterOther <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterOther, vbCrLf)
        End If
        sb.Append(vbCrLf + vbCrLf)
        sb.Append("    group by  arGradeSystemDetails.Grade,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId ")

        sb.Append(" Union All ")
        sb.Append("     select  " + vbCrLf)
        sb.Append("     arGradeSystemDetails.Grade as  grade,  " + vbCrLf)
        sb.Append("     count(arGradeSystemDetails.Grade) as GradeCount,  " + vbCrLf)
        sb.Append("     syCampGrps.CampGrpDescrip as CampGrpDescrip,   " + vbCrLf)
        sb.Append("     arStuEnrollments.CampusId as CampusId,   " + vbCrLf)
        sb.Append("     (select CampDescrip from syCampuses C where C.CampusId=arStuEnrollments.CampusId) as CampDesrip   " + vbCrLf)
        sb.Append("     from " + vbCrLf)
        sb.Append("     arStuEnrollments,  " + vbCrLf)
        ''Query Modified by Saraswathi on October 13 th 2008
        ''arClassSections included and compared against arReqs and arTerm
        sb.Append("     arClassSections,  " + vbCrLf)

        sb.Append("     arStudent,  " + vbCrLf)
        sb.Append("     arTransferGrades R,  " + vbCrLf)
        sb.Append("     arTerm , " + vbCrLf)
        sb.Append("     arReqs REQ,  " + vbCrLf)
        sb.Append("     arPrgVersions PV,  " + vbCrLf)
        sb.Append("     arPrograms,  " + vbCrLf)
        sb.Append("     arGradeSystemDetails,  " + vbCrLf)
        sb.Append("     syStatusCodes SC,  " + vbCrLf)
        sb.Append("     syCampGrps,  " + vbCrLf)
        sb.Append("     syCmpGrpCmps, " + vbCrLf)
        sb.Append("     Sycampuses " + vbCrLf)
        sb.Append("     where " + vbCrLf)
        sb.Append("     arStudent.StudentId = arStuEnrollments.StudentId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.StuEnrollId = R.StuEnrollId  " + vbCrLf)
        sb.Append("     AND arStuEnrollments.PrgVerId = PV.PrgVerId  " + vbCrLf)
        sb.Append("     AND PV.ProgId = arPrograms.ProgId  " + vbCrLf)
        sb.Append("     AND R.GrdSysDetailId = arGradeSystemDetails.GrdSysDetailId  " + vbCrLf)
        sb.Append("     and R.TermId=arTerm.TermId " + vbCrLf)
        sb.Append("     AND R.ReqId = REQ.ReqId  " + vbCrLf)
        ''Query Modified by Saraswathi on October 13 th 2008
        ''arClassSections included and compared against arReqs and arTerm
        sb.Append("     and arClassSections.TermId=arTerm.TermId " + vbCrLf)
        sb.Append("     AND REQ.ReqId =arClassSections.ReqId  " + vbCrLf)


        sb.Append("     AND arStuEnrollments.StatusCodeId = SC.StatusCodeId  " + vbCrLf)
        sb.Append("     and arStuEnrollments.CampusId=syCmpGrpCmps.CampusId  " + vbCrLf)
        sb.Append("     and arStuEnrollments.CampusId=Sycampuses.CampusId " + vbCrLf)
        sb.Append("     AND syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId ")
        If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterList, vbCrLf)
        End If
        If Not rptParamInfo.FilterOther Is Nothing AndAlso rptParamInfo.FilterOther <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterOther, vbCrLf)
        End If
        sb.Append(vbCrLf + vbCrLf)

        sb.Append("     group by  arGradeSystemDetails.Grade,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId ")
        sb.Append(" ) R    group by  Grade,CampGrpDescrip,CampusId,CampDesrip ")

        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
    Protected Function GetGrades1(ByVal rptParamInfo As Common.ReportParamInfo) As DataSet
        'connect to the database
        Dim db As New FAME.AdvantageV1.DataAccess.DataAccess

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

        Dim sb As New StringBuilder
        'sb.Append("(" + vbCrLf)
        'sb.Append("select distinct " + vbCrLf)
        'sb.Append("     PV.CampGrpId, " + vbCrLf)
        'sb.Append("     syCampGrps.CampGrpDescrip, " + vbCrLf)
        'sb.Append("     arStuEnrollments.CampusId, " + vbCrLf)
        'sb.Append("     (select CampDescrip from syCampuses C where C.CampusId=arStuEnrollments.CampusId) as CampDesrip, " + vbCrLf)
        'sb.Append("     arPrograms.ProgId, " + vbCrLf)
        'sb.Append("     arPrograms.ProgDescrip, " + vbCrLf)
        'sb.Append("     arStuEnrollments.StuEnrollId, " + vbCrLf)
        'sb.Append("     arStudent.FirstName, " + vbCrLf)
        'sb.Append("     arStudent.LastName, " + vbCrLf)
        'sb.Append("     REQ.Descrip as ClassName, " + vbCrLf)
        'sb.Append("     arGradeSystemDetails.Grade, " + vbCrLf)
        'sb.Append("     SC.StatusCodeDescrip, " + vbCrLf)
        'sb.Append("     arStuEnrollments.StartDate, " + vbCrLf)
        'sb.Append("     TG.ModDate, " + vbCrLf)
        'sb.Append("     arGradeSystemDetails.GrdSysDetailId " + vbCrLf)
        'If Not rptParamInfo.OrderBy Is Nothing AndAlso rptParamInfo.OrderBy <> "" Then
        '    sb.Append(" , " + rptParamInfo.OrderBy + vbCrLf)
        'End If
        'sb.Append("from " + vbCrLf)
        'sb.Append("     arStuEnrollments, " + vbCrLf)
        'sb.Append("     arStudent, " + vbCrLf)
        'sb.Append("     arTransferGrades TG, " + vbCrLf)
        'sb.Append("     arReqs REQ, " + vbCrLf)
        'sb.Append("     arPrgVersions PV, " + vbCrLf)
        'sb.Append("     arPrograms, " + vbCrLf)
        'sb.Append("     arGradeSystemDetails, " + vbCrLf)
        'sb.Append("     syStatusCodes SC, " + vbCrLf)
        'sb.Append("     syCampGrps, " + vbCrLf)
        'sb.Append("     arClassSectionTerms CST, " + vbCrLf)
        'sb.Append("     arClassSections " + vbCrLf)
        'sb.Append("where  " + vbCrLf)
        'sb.Append("     arStudent.StudentId = arStuEnrollments.StudentId " + vbCrLf)
        'sb.Append("     AND arStuEnrollments.StuEnrollId = TG.StuEnrollId " + vbCrLf)
        'sb.Append("     AND arStuEnrollments.PrgVerId = PV.PrgVerId " + vbCrLf)
        'sb.Append("     AND PV.ProgId = arPrograms.ProgId " + vbCrLf)
        'sb.Append("     AND TG.GrdSysDetailId = arGradeSystemDetails.GrdSysDetailId " + vbCrLf)
        'sb.Append("     AND TG.ReqId = Req.ReqId " + vbCrLf)
        'sb.Append("     AND TG.TermId = CST.TermId " + vbCrLf)
        'sb.Append("     and CST.ClsSectionId=arClassSections.ClsSectionId  " + vbCrLf)
        'sb.Append("     AND arStuEnrollments.StatusCodeId = SC.StatusCodeId " + vbCrLf)
        'sb.Append("     AND PV.CampGrpId = syCampGrps.CampGrpId " + vbCrLf)
        '' add the filter list
        'If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
        '    sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterList, vbCrLf)
        'End If
        'If Not rptParamInfo.FilterOther Is Nothing AndAlso rptParamInfo.FilterOther <> "" Then
        '    sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterOther, vbCrLf)
        'End If

        'sb.Append(vbCrLf + "union" + vbCrLf + vbCrLf)

        sb.Append("select " + vbCrLf)
        sb.Append(" arGradeSystemDetails.Grade, " + vbCrLf)
        sb.Append(" count(arGradeSystemDetails.Grade) as Count, " + vbCrLf)
        sb.Append("syCampGrps.CampGrpDescrip,  " + vbCrLf)
        sb.Append("arStuEnrollments.CampusId,  " + vbCrLf)
        sb.Append("(select CampDescrip from syCampuses C where C.CampusId=arStuEnrollments.CampusId) as CampDesrip  " + vbCrLf)
        If Not rptParamInfo.OrderBy Is Nothing AndAlso rptParamInfo.OrderBy <> "" Then
            sb.Append(" , " + rptParamInfo.OrderBy + vbCrLf)
        End If
        sb.Append("from " + vbCrLf)
        sb.Append("     arStuEnrollments, " + vbCrLf)
        sb.Append("     arStudent, " + vbCrLf)
        sb.Append("     arResults R, " + vbCrLf)
        sb.Append("     arClassSections , " + vbCrLf)
        sb.Append("     arReqs REQ, " + vbCrLf)
        sb.Append("     arPrgVersions PV, " + vbCrLf)
        sb.Append("     arPrograms, " + vbCrLf)
        sb.Append("     arGradeSystemDetails, " + vbCrLf)
        sb.Append("     syStatusCodes SC, " + vbCrLf)
        sb.Append("     syCampGrps, " + vbCrLf)
        sb.Append("     syCmpGrpCmps " + vbCrLf)
        sb.Append("where  " + vbCrLf)
        sb.Append("     arStudent.StudentId = arStuEnrollments.StudentId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.StuEnrollId = R.StuEnrollId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.PrgVerId = PV.PrgVerId " + vbCrLf)
        sb.Append("     AND PV.ProgId = arPrograms.ProgId " + vbCrLf)
        sb.Append("     AND R.GrdSysDetailId = arGradeSystemDetails.GrdSysDetailId " + vbCrLf)
        sb.Append("     AND R.TestId = arClassSections.ClsSectionId " + vbCrLf)
        sb.Append("     AND arClassSections.ReqId = REQ.ReqId " + vbCrLf)
        sb.Append("     AND arStuEnrollments.StatusCodeId = SC.StatusCodeId " + vbCrLf)
        sb.Append("     and arStuEnrollments.CampusId=syCmpGrpCmps.CampusId " + vbCrLf)
        sb.Append("     AND syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId " + vbCrLf)
        ' add the filter list
        If Not rptParamInfo.FilterList Is Nothing AndAlso rptParamInfo.FilterList <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterList, vbCrLf)
        End If
        If Not rptParamInfo.FilterOther Is Nothing AndAlso rptParamInfo.FilterOther <> "" Then
            sb.AppendFormat("   AND {0} {1}", rptParamInfo.FilterOther, vbCrLf)
        End If
        sb.Append(vbCrLf + vbCrLf)
        If Not rptParamInfo.OrderBy Is Nothing AndAlso rptParamInfo.OrderBy <> "" Then
            sb.AppendFormat("order by {0} {1}", rptParamInfo.OrderBy, vbCrLf)
        End If
        sb.Append("    group by  arGradeSystemDetails.Grade,syCampGrps.CampGrpDescrip,arStuEnrollments.CampusId ")
        Return db.RunParamSQLDataSet(sb.ToString)
    End Function
#End Region

End Class
