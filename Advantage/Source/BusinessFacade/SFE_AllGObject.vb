
Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllGObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SFE_AllG"

	Private dsRaw As New DataSet
	Private dtRptRaw As New DataTable
	Private dsRpt As New DataSet
	Private RptParamInfo As ReportParamInfoIPEDS

	Private dvEnrollments As DataView
	Private drRawStudent As DataRow
	Private dvAuditHistEnrollments As DataView
	Private dvAuditHistStudents As DataView
	Private EnrollAsOfDate As String
	Private DateFilterCompleted As String
	Private DateFilterActiveStart As String, DateFilterActiveEnd As String

	Public Overrides Function GetReportDataSet(ByVal RptParaminfo As ReportParamInfoIPEDS) As System.Data.DataSet
		'get raw DataSet using passed-in parameters
		dsRaw = SFE_AllGObjectDB.GetReportDatasetRaw(RptParaminfo)

		' save passed-in report parameter info 
		Me.RptParamInfo = RptParaminfo

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	'Private Function BuildReportDataSet(ByVal dsRaw As DataSet, ByVal RptParaminfo As ReportParamInfoIPEDS) As DataSet
	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		'Dim dsRpt As New DataSet
		Dim drRpt As DataRow
		Dim StudentIsExcluded As Boolean, StudentIsComplete As Boolean, StudentIsEnrolled As Boolean
		Dim SummaryTableName As String = MainTableName & "Summary"
		Dim IdsFullTime As String = PutDBQuotesList(SFE_AllGObjectDB.GetIdsFullTime)
		Dim IdsPartTime As String = PutDBQuotesList(SFE_AllGObjectDB.GetIdsPartTime)

		dvAuditHistStudents = New DataView(dsRaw.Tables("AuditHistStudents"))
		dvAuditHistEnrollments = New DataView(dsRaw.Tables("AuditHistEnrollments"))
		dvEnrollments = New DataView(dsRaw.Tables(TblNameEnrollments))
		EnrollAsOfDate = FmtRptDateDisplay(RptParaminfo.RptEndDate.AddYears(-1))

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add(RptStuIdentifierColName, GetType(System.String))
			.Add("Name", GetType(System.String))
			.Add("Ind_FullTime", GetType(String))
			.Add("Ind_FTExclFromCohort", GetType(String))
			.Add("Ind_FTCompleted", GetType(String))
			.Add("Ind_FTEnrolled", GetType(String))
			.Add("Ind_PartTime", GetType(String))
			.Add("Ind_PTExclFromCohort", GetType(String))
			.Add("Ind_PTCompleted", GetType(String))
			.Add("Ind_PTEnrolled", GetType(String))
		End With

		' loop thru student records, setting indicators for each student
        For Each drRawStudent In dsRaw.Tables(TblNameStudents).Rows
            drRpt = dsRpt.Tables(MainTableName).NewRow

            IPEDSFacade.SetStudentInfo(drRawStudent, drRpt)

            ' Determine if student was Full Time as of Report End Date - 1 year
            If IdsFullTime.Length > 1 Then drRpt("Ind_FullTime") = IIf(DetermineFullorPartTime(IdsFullTime), "X", DBNull.Value)

            ' Determine if student was Part Time as of Report End Date - 1 year
            If IdsPartTime.Length > 1 Then drRpt("Ind_PartTime") = IIf(DetermineFullorPartTime(IdsPartTime), "X", DBNull.Value)

            ' only add student to report if was enrolled full- or part-time as of Report End Date - 1 year
            If drRpt("Ind_FullTime").Equals("X") Or drRpt("Ind_PartTime").Equals("X") Then
                ' determine if student should be excluded from Cohort
                StudentIsExcluded = DetermineExcluded()

                ' set up dates to be used in filtering records to determine student's present
                '	status, with regards to "Completed" and "Re-enrolled or still enrolled", 
                '	according to School Reporting Type
                Select Case RptParamInfo.SchoolRptType
                    Case SchoolProgramReporter
                        DateFilterCompleted = FmtRptDateDisplay(RptParamInfo.CohortStartDate)
                        DateFilterActiveStart = FmtRptDateDisplay(RptParamInfo.CohortStartDate)
                        DateFilterActiveEnd = FmtRptDateDisplay(RptParamInfo.CohortEndDate)
                    Case SchoolAcademicYearReporter
                        DateFilterCompleted = FmtRptDateDisplay(RptParamInfo.RptEndDate)
                        DateFilterActiveStart = EnrollAsOfDate
                        DateFilterActiveEnd = FmtRptDateDisplay(RptParamInfo.RptEndDate)
                End Select

                ' determine if student present status is "Completed"
                StudentIsComplete = DetermineComplete()

                ' determine is present status is "Re-enrolled or still enrolled"
                StudentIsEnrolled = DetermineEnrolled()

                ' set the appropriate indicators for this student, according to what was 
                '	determined for this student, above
                If drRpt("Ind_FullTime").Equals("X") Then
                    drRpt("Ind_FTExclFromCohort") = IIf(StudentIsExcluded, "X", DBNull.Value)
                    drRpt("Ind_FTCompleted") = IIf(StudentIsComplete, "X", DBNull.Value)
                    drRpt("Ind_FTEnrolled") = IIf(StudentIsEnrolled, "X", DBNull.Value)
                    drRpt("Ind_PTExclFromCohort") = DBNull.Value
                    drRpt("Ind_PTCompleted") = DBNull.Value
                    drRpt("Ind_PTEnrolled") = DBNull.Value
                Else
                    drRpt("Ind_FTExclFromCohort") = DBNull.Value
                    drRpt("Ind_FTCompleted") = DBNull.Value
                    drRpt("Ind_FTEnrolled") = DBNull.Value
                    drRpt("Ind_PTExclFromCohort") = IIf(StudentIsExcluded, "X", DBNull.Value)
                    drRpt("Ind_PTCompleted") = IIf(StudentIsComplete, "X", DBNull.Value)
                    drRpt("Ind_PTEnrolled") = IIf(StudentIsEnrolled, "X", DBNull.Value)
                End If

                ' add new row to report
                dsRpt.Tables(MainTableName).Rows.Add(drRpt)
            End If
        Next

		' calculate report summary information
		With dsRpt
			' create DataTable to hold summary info
			With .Tables.Add(SummaryTableName).Columns
				.Add("FTTotalCohort", GetType(System.Int32))
				.Add("FTExclFromCohort", GetType(System.Int32))
				.Add("FTCompleted", GetType(System.Int32))
				.Add("FTEnrolled", GetType(System.Int32))
				.Add("FTPercent", GetType(System.Single))
				.Add("PTTotalCohort", GetType(System.Int32))
				.Add("PTExclFromCohort", GetType(System.Int32))
				.Add("PTCompleted", GetType(System.Int32))
				.Add("PTEnrolled", GetType(System.Int32))
				.Add("PTPercent", GetType(System.Single))
			End With

			' calculate values for summary based on report data, computed above
			drRpt = .Tables(SummaryTableName).NewRow
			With .Tables(MainTableName)
				drRpt("FTTotalCohort") = .Compute("Count(Ind_FullTime)", "")
				drRpt("FTExclFromCohort") = .Compute("Count(Ind_FTExclFromCohort)", "")
				drRpt("FTCompleted") = .Compute("Count(Ind_FTCompleted)", "")
				drRpt("FTEnrolled") = .Compute("Count(Ind_FTEnrolled)", "")
				drRpt("FTPercent") = ((drRpt("FTCompleted") + drRpt("FTEnrolled")) / _
									  (drRpt("FTTotalCohort") - drRpt("FTExclFromCohort"))) _
									 * 100
				drRpt("PTTotalCohort") = .Compute("Count(Ind_FullTime)", "")
				drRpt("PTExclFromCohort") = .Compute("Count(Ind_PTExclFromCohort)", "")
				drRpt("PTCompleted") = .Compute("Count(Ind_PTCompleted)", "")
				drRpt("PTEnrolled") = .Compute("Count(Ind_PTEnrolled)", "")

				drRpt("PTPercent") = ((drRpt("PTCompleted") + drRpt("PTEnrolled")) / _
									  (drRpt("PTTotalCohort") - drRpt("PTExclFromCohort"))) _
									 * 100
			End With

			' add summary row to table
			.Tables(SummaryTableName).Rows.Add(drRpt)
		End With


		' return the Report DataSet
		Return dsRpt

	End Function

	Private Function DetermineFullorPartTime(ByVal FullOrPartTimeIds As String) As Boolean
		Dim i As Integer
		Dim EnrollmentList As String
		Dim sb As New System.Text.StringBuilder

		dvEnrollments.RowFilter = "StudentId = '" & drRawStudent("StudentId").ToString & "'"

		If dvEnrollments.Count = 0 Then
			Return False
		End If

		For i = 0 To dvEnrollments.Count - 1
            sb.Append("Rowid = '" & dvEnrollments(i)("StuEnrollId").ToString & "' OR ")
		Next
        'EnrollmentList = sb.ToString.TrimEnd(",".ToCharArray)
        EnrollmentList = sb.ToString.TrimEnd(" OR ".ToCharArray)

        'dvAuditHistEnrollments.RowFilter = "EventDate <= '" & EnrollAsOfDate & "' AND " & _
        '								   "RowId IN (" & EnrollmentList & ") AND " & _
        '								   "Event LIKE 'I' AND " & _
        '								   "ColumnName LIKE 'AttendTypeId' AND " & _
        '								   "NewValue IN (" & FullOrPartTimeIds & ")"
        dvAuditHistEnrollments.RowFilter = "EventDate <= '" & EnrollAsOfDate & "' AND " & _
                                            EnrollmentList & " AND " & _
                                            "Event LIKE 'I' AND " & _
                                            "ColumnName LIKE 'AttendTypeId' AND " & _
                                            "NewValue IN (" & FullOrPartTimeIds & ")"

		Return dvAuditHistEnrollments.Count > 0
	End Function

	Private Function DetermineExcluded() As Boolean
	' determine if student should be excluded from Cohort

		dvEnrollments.RowFilter = "StudentId = '" & drRawStudent("StudentId").ToString & "' AND " & _
								  "EnrollDate <= '" & EnrollAsOfDate & "' AND " & _
								  "NOT (SysStatusDescrip LIKE 'Dropped')"
		If dvEnrollments.Count > 0 Then
			' if student has at least one enrollment that does NOT have status 
			'	of "Dropped", then student is NOT excluded from Cohort
			Return False
		Else
			' if all of a student's enrollments have status "Dropped", then
			'	if AT LEAST ONE enrollment was dropped for a valid reason, 
			'	student is excluded from Cohort
			dvEnrollments.RowFilter = "StudentId = '" & drRawStudent("StudentId").ToString & "' AND " & _
									  "EnrollDate <= '" & EnrollAsOfDate & "' AND " & _
									  "DropReasonDescrip IN (" & DropReasonsExclude & ")"
			Return dvEnrollments.Count > 0
		End If

	End Function

	Private Function DetermineComplete() As Boolean
	' determine if student present status is "Completed"

		Dim i As Integer

		' get list of Enrollments for this student, where Enrollment Start Date is between
		'	 the Report End Date - 1 year and the ending date as established above
		dvEnrollments.RowFilter = "StudentId = '" & drRawStudent("StudentId").ToString & "' AND " & _
								  "EnrollDate > '" & EnrollAsOfDate & "' AND " & _
								  "EnrollDate <= '" & DateFilterCompleted & "'"

		' if we found no enrollments above, student is NOT "Completed"
		If dvEnrollments.Count > 0 Then
			Return False
		End If

		' otherwise, loop thru Student's Enrollments
		For i = 0 To dvEnrollments.Count - 1
			' look for records for enrollment in the list of Enrollment audit history records,
			'	where the Event Date is on or prior to the ending date established above, and the
			'	status was changed to one which maps to "Graduated" in sySysStatuses
			dvAuditHistEnrollments.RowFilter = "Event = 'U' AND " & _
											   "EventDate <= '" & DateFilterCompleted & "' AND " & _
											   "RowId = '" & dvEnrollments(i)("StuEnrollId").ToString & "' AND " & _
											   "ColumnName LIKE 'StatusCodeId' AND " & _
											   "NewValue IN (" & SFE_AllGObjectDB.GetIdsGraduated & ")"
			' If we don't find any audit history records for the current enrollment, we know that
			'	the student did NOT complete this enrollment, and thus the student is NOT 
			'	considered "Completed"
			If dvAuditHistEnrollments.Count = 0 Then
				Return False
			End If
		Next

		' if looped thru all Student's Enrollments, and found audit history records for
		'	ALL enrollments, then student IS "Completed"
		Return True

	End Function

	Private Function DetermineEnrolled() As Boolean
	' determine is present status is "Re-enrolled or still enrolled"

		' look for Student audit history records where the student was set to "Active" within
		'	the date range established above
		dvAuditHistStudents.RowFilter = "EventDate > '" & DateFilterActiveStart & "' AND " & _
										"EventDate <= '" & DateFilterActiveEnd & "' AND " & _
										"RowId = '" & drRawStudent("StudentId").ToString & "' AND " & _
										"NewValue = '" & AdvantageCommonValues.ActiveGuid & "'"

		' if the student was NOT set to "Active" within the date range, the student remains 
		'	NOT "Re-enrolled or still enrolled"
		If dvAuditHistStudents.Count = 0 Then
			Return False
		End If

		' otherwise look for Student audit history records where the student was 
		'	set to "Inactive" within the same date range established above
		dvAuditHistStudents.RowFilter = "EventDate > '" & DateFilterActiveStart & "' AND " & _
										"EventDate <= '" & DateFilterActiveEnd & "' AND " & _
										"RowId = '" & drRawStudent("StudentId").ToString & "' AND " & _
										"NewValue = '" & AdvantageCommonValues.InactiveGuid & "'"
		' if the student was NOT set to "Inactive" during the same date range, 
		'	the student IS "Re-enrolled or still enrolled", otherwise the student 
		'	IS NOT "Re-enrolled or still enrolled"
		Return dvAuditHistStudents.Count = 0

	End Function

End Class