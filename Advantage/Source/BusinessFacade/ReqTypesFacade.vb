Public Class ReqTypesFacade
    Public Function GetAllStatuses() As DataSet

        '   Instantiate DAL component
        Dim reqTypesDB As New reqTypesDB

        '   get the dataset with all degrees
        Return reqTypesDB.GetAllStatuses()

    End Function

    Public Function GetAllCampGrps() As DataSet

        '   Instantiate DAL component
        Dim reqTypesDB As New reqTypesDB

        '   get the dataset with all degrees
        Return reqTypesDB.GetAllCampGrps()

    End Function

    Public Function GetChildTypes() As DataSet

        '   Instantiate DAL component
        Dim reqTypesDB As New reqTypesDB

        '   get the dataset with all degrees
        Return reqTypesDB.GetChildTypes()

    End Function

    Public Function AddRequirementType(ByVal RequirementType As RequirementTypeInfo) As String
        Dim db As New ReqTypesDB

        Return db.AddReqType(RequirementType)
    End Function
    Public Function UpdateRequirementType(ByVal RequirementType As RequirementTypeInfo) As String
        Dim db As New ReqTypesDB
        Return db.UpdateReqType(RequirementType)
    End Function
    Public Sub DeleteRequirementType(ByVal ReqTypeId As Integer)
        Dim db As New ReqTypesDB
        db.DeleteReqType(ReqTypeId)
    End Sub

    Public Function GetReqTypeId() As Integer

        '   Instantiate DAL component
        Dim reqTypesDB As New reqTypesDB

        '   get the dataset with all degrees
        Return reqTypesDB.GetReqTypId()

    End Function

    Public Function GetReqTypInfoOnItemCmd(ByVal ReqTypeId As Integer) As RequirementTypeInfo

        '   Instantiate DAL component
        Dim reqTypesDB As New reqTypesDB
        Dim ds As DataSet
        Dim ReqTypInfo As RequirementTypeInfo

        '   get the dataset with all degrees
        ds = reqTypesDB.GetReqTypeInfo(ReqTypeId)

        'Fill Requirement Type Object to send back to UI.
        ReqTypInfo = FillReqTypInfoObject(ds)

        Return ReqTypInfo
    End Function

    Public Function FillReqTypInfoObject(ByVal ds As DataSet) As RequirementTypeInfo
        Dim ReqTypObject As New RequirementTypeInfo
        Dim dt As DataTable
        Dim row As DataRow
        dt = ds.Tables(0)
        If ds.Tables.Count > 1 Then
            'Error - it returned more than one record
        Else
            For Each row In dt.Rows
                ReqTypObject.ReqTypeId = row("ReqTypeId")
                ReqTypObject.Code = row("Code")
                ReqTypObject.Descrip = row("Descrip")
                ReqTypObject.StatusId = row("StatusId")
                ReqTypObject.CampGrpId = row("CampGrpId")
                ReqTypObject.TrkCount = row("TrkCount")
                ReqTypObject.TrkGrade = row("TrkGrade")
                If row("IsGroup").ToString <> "" Then
                    ReqTypObject.IsGroup = row("IsGroup")
                End If

                If row("ChildType").ToString <> "" Then
                    ReqTypObject.ChildType = row("ChildType")
                Else
                    ReqTypObject.ChildType = Nothing
                End If
                If row("TrkHours").ToString <> "" Then
                    ReqTypObject.TrkHours = row("TrkHours")
                End If
                If row("TrkGrade").ToString <> "" Then
                    ReqTypObject.TrkGrade = row("TrkGrade")
                End If
                If row("TrkCount").ToString <> "" Then
                    ReqTypObject.TrkCount = row("TrkCount")
                End If

            Next
        End If
        Return ReqTypObject
    End Function


End Class
