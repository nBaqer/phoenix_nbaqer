Imports FAME.Advantage.Common

Public Class StuClassScheduleObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim stuClsSched As New StuClassScheduleDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(stuClsSched.GetClassSchedule(rptParamInfo), stuClsSched.StudentIdentifier)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim strName As String
        Dim facInputMasks As New InputMasksFacade
        'Get the mask for SSN
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    dr("TestIdStr") = dr("TestId").ToString
                    'Set up student name as: "LastName, FirstName MI."
                    strName = ""
                    If Not (dr("LastName") Is System.DBNull.Value) Then
                        If dr("LastName") <> "" Then
                            strName = dr("LastName")
                        End If
                    End If
                    If Not (dr("FirstName") Is System.DBNull.Value) Then
                        If dr("FirstName") <> "" Then
                            If strName <> "" Then
                                strName &= ", " & dr("FirstName")
                            Else
                                strName &= dr("FirstName")
                            End If
                        End If
                    End If
                    If Not (dr("MiddleName") Is System.DBNull.Value) Then
                        If dr("MiddleName") <> "" Then
                            If strName <> "" Then
                                strName &= " " & dr("MiddleName") & "."
                            Else
                                strName &= dr("MiddleName") & "."
                            End If
                        End If
                    End If
                    dr("StudentName") = strName

                    'Apply mask to SSN.
                    If StudentIdentifier = "SSN" Then
                        If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                            If dr("StudentIdentifier") <> "" Then
                                Dim temp As String = dr("StudentIdentifier")
                                dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                            End If
                        End If
                    End If

                    dr("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower

                Next
                '
                For Each dr As DataRow In ds.Tables(1).Rows
                    'This needs to be done, in order to link tables in report. 
                    'CR.NET does not handle GUID fields.
                    dr("ClsSectionStr") = dr("ClsSectionId").ToString
                Next
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
