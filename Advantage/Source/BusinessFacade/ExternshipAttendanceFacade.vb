Public Class ExternshipAttendanceFacade
    Inherits BaseReportFacade
    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim GradebookResultsObj As New GradebookResultsDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by report.
        'ds = BuildReportSource(GradebookResultsObj.GradebookResults(rptParamInfo), GradebookResultsObj.StudentIdentifier, rptParamInfo.ResId, rptParamInfo.FilterOtherString.Trim)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        ds = BuildDataForReport(rptParamInfo)
        Return ds
    End Function
    Public Function BuildDataForReport(ByVal ParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim dtReturnResult As New DataTable
        Dim ds As New DataSet
        ds = (New AttendanceHistoryDB).GetExternshipSummary(ParamInfo)
        Return ds
    End Function
    Public Function GetExternshipAttendanceDS(ByVal stuEnrollId As String) As DataSet
        '   get the dataset with all ExternshipAttendance
        Return (New ExternshipAttendanceDB).GetExternshipAttendanceDS(stuEnrollId)
    End Function
    Public Function UpdateExternshipAttendanceDS(ByVal ds As DataSet, ByVal user As String) As String
        '   update dataset and return result in a string
        Return (New ExternshipAttendanceDB).UpdateExternshipAttendanceDS(ds, user)
    End Function
    Public Function GetRequiredHoursForExternships(ByVal stuEnrollId As String) As String
        '   get scalar
        Return (New ExternshipAttendanceDB).GetRequiredHoursForExternships(stuEnrollId)
    End Function
    Public Function GetCompletedHoursForExternships(ByVal stuEnrollId As String) As String
        '   get scalar
        Return (New ExternshipAttendanceDB).GetCompletedHoursForExternships(stuEnrollId)
    End Function
    Public Function UpdateStudentEnrollmentStatusWithExternshipStatus(ByVal StuEnrollId As String, ByVal campusId As String, ByVal statusCodeId As String) As String
        Dim refDb As New ExternshipAttendanceDB
        Return (refDb.UpdateStudentEnrollmentStatusWithExternshipStatus(StuEnrollId, campusId, statusCodeId))
    End Function
    Public Function GetExternshipStatusCode(ByVal campusId As String) As String
        Return (New ExternshipAttendanceDB).GetExternshipStatusCode(campusId)
    End Function
    Public Function GetExternshipStatusDescrip(ByVal campusId As String, ByVal statusCodeId As String) As String
        Return (New ExternshipAttendanceDB).GetExternshipStatusDescrip(campusId, statusCodeId)
    End Function
    Public Sub SetIsCourseCompleted(ByVal StuEnrollid As String)
        Dim externDB As New ExternshipAttendanceDB
        externDB.SetIsCourseCompleted(StuEnrollid)
    End Sub
End Class
