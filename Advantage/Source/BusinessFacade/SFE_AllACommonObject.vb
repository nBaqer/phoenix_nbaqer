Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllACommonObject

	Public Shared Function PreProcessRawData(ByVal dtStudents As DataTable, ByVal dtStuEnrollInfo As DataTable) As DataTable

		' determine if this is for the detail report
		Dim IsDetailRpt As Boolean = dtStudents.Columns.Contains(RptStuIdentifierColName)

		' create data table to hold processed raw data
		Dim dtRptRaw As New DataTable
		With dtRptRaw.Columns
			If IsDetailRpt Then
				' if this is for the detail report, add columns for student identifier and name
				.Add(RptStuIdentifierColName, GetType(String))
				.Add("LastName", GetType(String))
				.Add("FirstName", GetType(String))
				.Add("MiddleName", GetType(String))
			End If
			.Add("GenderDescrip", GetType(String))
			.Add("EthCodeDescrip", GetType(String))
			.Add("ProgTypeDescrip", GetType(String))
			.Add("AttendTypeDescrip", GetType(String))
			.Add("DegCertSeekingDescrip", GetType(String))
		End With
		Dim drRptRaw As DataRow

		' create DataView of student enrollment info for use when processing
		Dim dvStuEnrollInfo As New DataView(dtStuEnrollInfo, "", "StudentId", DataViewRowState.CurrentRows)
		Dim RowPtr As Integer

		For Each drStudent As DataRow In dtStudents.Rows
			drRptRaw = dtRptRaw.NewRow

			' save basic student information
			If IsDetailRpt Then
				' if this is for the detail report, save student identifier and name
				drRptRaw(RptStuIdentifierColName) = drStudent("StudentIdentifier")
				drRptRaw("LastName") = drStudent("LastName")
				drRptRaw("FirstName") = drStudent("FirstName")
				drRptRaw("MiddleName") = drStudent("MiddleName")
			End If

			' save gender and ethnic code descriptions
			drRptRaw("GenderDescrip") = drStudent("GenderDescrip")
			drRptRaw("EthCodeDescrip") = drStudent("EthCodeDescrip")

			' find student's record in enrollment info data, and copy to processed raw data
			RowPtr = dvStuEnrollInfo.Find(drStudent("StudentId"))
			drRptRaw("ProgTypeDescrip") = dvStuEnrollInfo(RowPtr)("ProgTypeDescrip")
			drRptRaw("AttendTypeDescrip") = dvStuEnrollInfo(RowPtr)("AttendTypeDescrip")
			drRptRaw("DegCertSeekingDescrip") = dvStuEnrollInfo(RowPtr)("DegCertSeekingDescrip")

			' add processed raw DataRow to raw DataTable
			dtRptRaw.Rows.Add(drRptRaw)
		Next

		' return copy of processed raw data
		Return dtRptRaw.Copy

	End Function

End Class
