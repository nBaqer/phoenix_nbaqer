

Public Class ApplyPaymentsFacade
    Public Function GetPaymentsToApply(ByVal campusId As String) As DataSet
        Return (New ApplyPaymentsDB).GetPaymentsToApply(campusId)
    End Function
    Public Function GetAvailableCharges(ByVal transactionId As String) As DataTable
        Return (New ApplyPaymentsDB).GetAvailableCharges(transactionId)
    End Function
    Public Function SaveNewAppliedPayments(ByVal appliedPaymentsCollection As ArrayList) As String
        Return (New ApplyPaymentsDB).SaveNewAppliedPayments(appliedPaymentsCollection)
    End Function
End Class
