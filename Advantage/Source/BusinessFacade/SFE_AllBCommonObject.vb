Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllBCommonObject

	Public Const NumRptParts = 6

    Public Shared Function PreProcessRawData(ByVal dtStudents As DataTable, ByVal dtStuEnrollInfo As DataTable, ByVal dteAgeAsOf As DateTime) As DataTable
        ' determine if this is for the detail report
        Dim IsDetailRpt As Boolean = dtStudents.Columns.Contains(RptStuIdentifierColName)

        ' create data table to hold processed raw data
        Dim dtRptRaw As New DataTable
        With dtRptRaw.Columns
            If IsDetailRpt Then
                ' if this is for the detail report, add columns for student identifier and name
                .Add(RptStuIdentifierColName, GetType(System.String))
                .Add("LastName", GetType(System.String))
                .Add("FirstName", GetType(System.String))
                .Add("MiddleName", GetType(System.String))
            End If
            .Add("Age", GetType(Integer))
            .Add("GenderDescrip", GetType(System.String))
            .Add("ProgTypeDescrip", GetType(System.String))
            .Add("AttendTypeDescrip", GetType(System.String))
        End With
        Dim drRptRaw As DataRow

        ' create DataView of student enrollment info for use when processing, sorted by StudentId
        Dim dvStuEnrollInfo As New DataView(dtStuEnrollInfo, "", "StudentId", DataViewRowState.CurrentRows)
        Dim RowPtr As Integer

        For Each drStudent As DataRow In dtStudents.Rows
            drRptRaw = dtRptRaw.NewRow

            ' save basic student information
            If IsDetailRpt Then
                ' if this is for the detail report, save student identifier and name
                drRptRaw(RptStuIdentifierColName) = drStudent(RptStuIdentifierColName)
                drRptRaw("LastName") = drStudent("LastName")
                drRptRaw("FirstName") = drStudent("FirstName")
                drRptRaw("MiddleName") = drStudent("MiddleName")
            End If

            ' compute student's age from DOB
            'drRptRaw("Age") = Math.Floor(Date.Now.Date.Subtract(CDate(drStudent("DOB"))).Days / 365)
            drRptRaw("Age") = Math.Floor(dteAgeAsOf.Date.Subtract(CDate(drStudent("DOB"))).Days / 365)

            ' save gender description
            drRptRaw("GenderDescrip") = drStudent("GenderDescrip")

            ' find student's record in enrollment info data, and copy to processed raw data
            RowPtr = dvStuEnrollInfo.Find(drStudent("StudentId"))
            drRptRaw("ProgTypeDescrip") = dvStuEnrollInfo(RowPtr)("ProgTypeDescrip")
            drRptRaw("AttendTypeDescrip") = dvStuEnrollInfo(RowPtr)("AttendTypeDescrip")

            ' add processed raw DataRow to raw DataTable
            dtRptRaw.Rows.Add(drRptRaw)
        Next

        ' return copy of processed raw data
        Return dtRptRaw.Copy

    End Function

End Class
