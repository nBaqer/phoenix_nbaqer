Public Class AdmissionRepYTDObject
    Inherits BaseReportFacade

    Private Enum LeadStatus
        IsNull = 0
        NewLead = 1
        IScheduled = 2
        Interviewed = 3
        Enrolled = 4
        Others = 5
        ''These Statuses added By Saraswathi lakshmanan
        ApplicationReceived = 6
        ApplicationNotAccepted = 7
        DeadLead = 8
        WillEnrollinFuture = 9
    End Enum

    Private dtStatusCode As DataTable

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim objDB As New AdmissionRepPerformanceDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(objDB.GetYearToDateRepPerformance(rptParamInfo), rptParamInfo)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal paramInfo As Common.ReportParamInfo) As DataSet
        Dim arrRows() As DataRow
        Dim year As String = ""
        Dim startDate As String = ""
        Dim endDate As String = ""
        Dim adRep As String = ""
        Dim arrList As ArrayList
        Dim row As DataRow
        Dim dr As DataRow
        Dim ctrAdRep As Integer
        Dim dsRpt As New DataSet




        Try
            If ds.Tables.Count = 5 Then
                Dim dtDetail As DataTable = ds.Tables("YTDDetail")
                Dim dtTemp As DataTable = ds.Tables("YTDTemp")
                Dim dtTempCampusTtl As DataTable = ds.Tables("YTDCampusTotals")
                Dim dtTempCampGrpTtl As DataTable = ds.Tables("YTDCampusGroupTotals")
                dtStatusCode = ds.Tables("StatusCodeDescrip")
                '
                Dim idx As Integer
                Dim idx2 As Integer
                Dim endDate1 As String = ""

                If paramInfo.FilterOther <> "" Then
                    'Parse FilterOther in order to get End Date
                    idx = paramInfo.FilterOther.IndexOf("BETWEEN")
                    If idx <> -1 Then
                        idx2 = paramInfo.FilterOther.IndexOf("AM")
                        If idx2 <> -1 Then
                            Enddate1 = paramInfo.FilterOther.Substring(idx + 9, idx2 + 2 - (idx + 8))
                        Else
                            Enddate1 = paramInfo.FilterOther.Substring(idx + 8)
                        End If
                        Dim idx3 As Integer
                        idx3 = Enddate1.IndexOf(" ")
                        If idx3 > 0 Then
                            endDate1 = endDate1.Substring(0, idx3).Trim
                        End If
                    End If
                End If
                Dim myCulture As New System.Globalization.CultureInfo("en-US", True)
                Dim dtEndDate As Date = Date.Parse(endDate1, myCulture, Globalization.DateTimeStyles.NoCurrentDateDefault)
                Dim ToDate As Date
                Dim FromDate As Date

                ToDate = dtEndDate.AddMonths(1)
                ToDate = New Date(ToDate.Year, ToDate.Month, 1)
                ToDate = ToDate.AddDays(-1)

                FromDate = dtEndDate.AddMonths(-11)
                FromDate = New Date(FromDate.Year, FromDate.Month, 1)



                For Each dr In dtDetail.Rows
                    If adRep <> dr("AdmissionsRep").ToString Then
                        ctrAdRep += 1
                        'year = Date.Parse(dtDetail.Rows(0)("ModDate")).Year
                        'year = Date.Parse(FromDate).Year
                        startDate = FromDate

                        For m As Integer = 1 To 12
                            If m = 1 Then
                                ' startDate = startDate - NZ 11/19 assignment has no effect
                            Else
                                startDate = Date.Parse(FromDate).AddMonths(m - 1)
                            End If
                            endDate = Date.Parse(FromDate).AddMonths(m)

                            'If m < 12 Then
                            '    endDate = (m + 1) & "/01/" & year
                            'Else
                            '    endDate = "01/01/" & (year + 1)
                            'End If

                            '   get all leads that belong to one Admission Rep from WTDDetail table
                            arrRows = dtDetail.Select("CampGrpId = '" & dr("CampGrpId").ToString & "' " & _
                                                        "AND CampusId = '" & dr("CampusId").ToString & "' " & _
                                                        "AND AdmissionsRep = '" & dr("AdmissionsRep").ToString & "' " & _
                                                        "AND ModDate >= '" & startDate & "' AND ModDate < '" & endDate & "'")

                            If arrRows.GetLength(0) > 0 Then
                                'Compute status changes taking into account reversals.
                                'arrList = ComputeChanges_revised(arrRows, m - 1)
                                arrList = ComputeChanges_revised(arrRows, Date.Parse(startDate).Month - 1)
                                For i As Integer = 0 To arrList.Count - 1
                                    row = dtTemp.NewRow
                                    row("CampGrpIdStr") = dr("CampGrpId").ToString
                                    row("CampGrpDescrip") = dr("CampGrpDescrip")
                                    row("CampusIdStr") = dr("CampusId").ToString
                                    row("CampDescrip") = dr("CampDescrip")
                                    row("AdmissionsRepStr") = dr("AdmissionsRep").ToString
                                    row("FullName") = dr("FullName")
                                    row("LeadStatusDescrip") = ""
                                    Select Case i
                                        Case 0
                                            row("LeadStatus") = LeadStatus.NewLead
                                            If dtStatusCode.Rows.Count > 0 Then
                                                If Not dtStatusCode.Rows(0).IsNull("NewLead") Then
                                                    row("LeadStatusDescrip") = dtStatusCode.Rows(0)("NewLead")
                                                    row("LeadStatusDescrip") &= " "
                                                End If
                                            End If
                                            row("LeadStatusDescrip") &= "(NL)"
                                        Case 1
                                            row("LeadStatus") = LeadStatus.IScheduled
                                            If dtStatusCode.Rows.Count > 0 Then
                                                If Not dtStatusCode.Rows(0).IsNull("IntScheduled") Then
                                                    row("LeadStatusDescrip") = dtStatusCode.Rows(0)("IntScheduled")
                                                    row("LeadStatusDescrip") &= " "
                                                End If
                                            End If
                                            row("LeadStatusDescrip") &= "(IS)"
                                        Case 2
                                            row("LeadStatus") = LeadStatus.Interviewed
                                            If dtStatusCode.Rows.Count > 0 Then
                                                If Not dtStatusCode.Rows(0).IsNull("Interviewed") Then
                                                    row("LeadStatusDescrip") = dtStatusCode.Rows(0)("Interviewed")
                                                    row("LeadStatusDescrip") &= " "
                                                End If
                                            End If
                                            row("LeadStatusDescrip") &= "(I)"
                                        Case 3
                                            'Special case: this is a catch-all bucket
                                            row("LeadStatus") = LeadStatus.Others
                                            row("LeadStatusDescrip") = "Others"
                                        Case 4
                                            row("LeadStatus") = LeadStatus.Enrolled
                                            If dtStatusCode.Rows.Count > 0 Then
                                                If Not dtStatusCode.Rows(0).IsNull("Enrolled") Then
                                                    row("LeadStatusDescrip") = dtStatusCode.Rows(0)("Enrolled")
                                                    row("LeadStatusDescrip") &= " "
                                                End If
                                            End If
                                            row("LeadStatusDescrip") &= "(E)"
                                            ''Added by Saraswathi lakshmanan on Nov 12 2009
                                        Case 5
                                            row("LeadStatus") = LeadStatus.ApplicationReceived
                                            If dtStatusCode.Rows.Count > 0 Then
                                                If Not dtStatusCode.Rows(0).IsNull("ApplicationReceived") Then
                                                    row("LeadStatusDescrip") = dtStatusCode.Rows(0)("ApplicationReceived")
                                                    row("LeadStatusDescrip") &= " "
                                                End If
                                            End If
                                            row("LeadStatusDescrip") &= "(AR)"
                                        Case 6
                                            row("LeadStatus") = LeadStatus.ApplicationNotAccepted
                                            If dtStatusCode.Rows.Count > 0 Then
                                                If Not dtStatusCode.Rows(0).IsNull("ApplicationNotAccepted") Then
                                                    row("LeadStatusDescrip") = dtStatusCode.Rows(0)("ApplicationNotAccepted")
                                                    row("LeadStatusDescrip") &= " "
                                                End If
                                            End If
                                            row("LeadStatusDescrip") &= "(ANA)"

                                        Case 7
                                            row("LeadStatus") = LeadStatus.DeadLead
                                            If dtStatusCode.Rows.Count > 0 Then
                                                If Not dtStatusCode.Rows(0).IsNull("DeadLead") Then
                                                    row("LeadStatusDescrip") = dtStatusCode.Rows(0)("DeadLead")
                                                    row("LeadStatusDescrip") &= " "
                                                End If
                                            End If
                                            row("LeadStatusDescrip") &= "(DL)"

                                        Case 8
                                            row("LeadStatus") = LeadStatus.WillEnrollinFuture
                                            If dtStatusCode.Rows.Count > 0 Then
                                                If Not dtStatusCode.Rows(0).IsNull("WillEnrollinFuture") Then
                                                    row("LeadStatusDescrip") = dtStatusCode.Rows(0)("WillEnrollinFuture")
                                                    row("LeadStatusDescrip") &= " "
                                                End If
                                            End If
                                            row("LeadStatusDescrip") &= "(WEIF)"

                                    End Select
                                    row("Count") = arrList(i)
                                    ' row("Month") = m
                                    row("Month") = Date.Parse(startDate).Month
                                    dtTemp.Rows.Add(row)
                                Next
                            End If
                        Next
                        adRep = dr("AdmissionsRep").ToString
                    End If
                Next

                'transform dtTemp into a table that can be used by the report
                dsRpt = TransformDSforRpt_Revised(dtTemp, ctrAdRep)

                'add campus total table
                Dim dtCampusTtl As DataTable = dtTempCampusTtl.Copy
                ComputeCampusTotals_Revised(dsRpt.Tables(0), dtCampusTtl)
                dsRpt.Tables.Add(dtCampusTtl)

                'add campus group total table
                Dim dtCampGrpTtl As DataTable = dtTempCampGrpTtl.Copy
                ComputeCampusGroupTotals_Revised(dsRpt.Tables(0), dtCampGrpTtl)
                dsRpt.Tables.Add(dtCampGrpTtl)

                'Change LeadStatusOrCriteria, so the Enrolled status is listed after Others.
                For Each dr In dsRpt.Tables("YTDPerformance").Rows
                    If dr("LeadStatusOrCriteria") = 4 Then
                        dr("LeadStatusOrCriteria") *= 10
                    End If
                Next

                'Change LeadStatusOrCriteria, so the Enrolled status is listed after Others.
                AlterViewOrder(dsRpt.Tables("YTDPerformance"))
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return dsRpt
    End Function

    Private Function ComputeChanges(ByVal arrRows() As DataRow, ByVal refMonth As Integer) As ArrayList
        Dim matrix(12, 6, 6) As Integer
        Dim i, j, k As Integer
        Dim arrList As New ArrayList

        'Initialize matrix with zeros
        For k = 0 To 12
            For i = 0 To 6
                For j = 0 To 6
                    matrix(k, i, j) = 0
                Next
            Next
        Next

        Dim skip As Boolean

        If arrRows.GetLength(0) > 0 Then
            For Each row As DataRow In arrRows
                skip = False
                If row.IsNull("SysOrigStatusId") Then
                    i = LeadStatus.IsNull
                Else
                    If row("SysOrigStatusId") = row("SysNewStatusId") Then
                        skip = True
                    End If
                End If
                'ignore rows with original status equal to new status, because there was no change.
                If Not skip Then
                    If Not row.IsNull("SysNewStatusId") Then
                        Select Case Integer.Parse(row("SysNewStatusId"))
                            Case 1
                                j = LeadStatus.NewLead
                                If Not row.IsNull("SysOrigStatusId") Then
                                    Select Case Integer.Parse(row("SysOrigStatusId"))
                                        Case 4
                                            i = LeadStatus.IScheduled
                                        Case 5
                                            i = LeadStatus.Interviewed
                                        Case 6
                                            i = LeadStatus.Enrolled
                                        Case Else
                                            i = LeadStatus.Others
                                    End Select
                                End If

                            Case 4
                                j = LeadStatus.IScheduled
                                If Not row.IsNull("SysOrigStatusId") Then
                                    Select Case Integer.Parse(row("SysOrigStatusId"))
                                        Case 1
                                            i = LeadStatus.NewLead
                                        Case 5
                                            i = LeadStatus.Interviewed
                                        Case 6
                                            i = LeadStatus.Enrolled
                                        Case Else
                                            i = LeadStatus.Others
                                    End Select
                                End If

                            Case 5
                                j = LeadStatus.Interviewed
                                If Not row.IsNull("SysOrigStatusId") Then
                                    Select Case Integer.Parse(row("SysOrigStatusId"))
                                        Case 1
                                            i = LeadStatus.NewLead
                                        Case 4
                                            i = LeadStatus.IScheduled
                                        Case 6
                                            i = LeadStatus.Enrolled
                                        Case Else
                                            i = LeadStatus.Others
                                    End Select
                                End If

                            Case 6
                                j = LeadStatus.Enrolled
                                If Not row.IsNull("SysOrigStatusId") Then
                                    Select Case Integer.Parse(row("SysOrigStatusId"))
                                        Case 1
                                            i = LeadStatus.NewLead
                                        Case 4
                                            i = LeadStatus.IScheduled
                                        Case 5
                                            i = LeadStatus.Interviewed
                                        Case Else
                                            i = LeadStatus.Others
                                    End Select
                                End If

                            Case Else
                                j = LeadStatus.Others
                                If Not row.IsNull("SysOrigStatusId") Then
                                    Select Case Integer.Parse(row("SysOrigStatusId"))
                                        Case 1
                                            i = LeadStatus.NewLead
                                        Case 4
                                            i = LeadStatus.IScheduled
                                        Case 5
                                            i = LeadStatus.Interviewed
                                        Case Else
                                            i = LeadStatus.Others
                                    End Select
                                End If
                        End Select
                    End If

                    If row.IsNull("SysOrigStatusId") Then
                        'When SysOrigStatusId is NULL, IsReversal is also NULL
                        matrix(refMonth, i, j) += 1
                    Else
                        If row.IsNull("IsReversal") Then
                            matrix(refMonth, j, i) -= 1
                        Else
                            matrix(refMonth, i, j) += 1
                        End If
                    End If
                End If
            Next

            Dim ctrNL As Integer = 0
            Dim ctrIS As Integer = 0
            Dim ctrI As Integer = 0
            Dim ctrO As Integer = 0
            Dim ctrE As Integer = 0

            'Compute totals
            For k = 0 To 5
                ctrNL += matrix(refMonth, k, LeadStatus.NewLead)
                ctrIS += matrix(refMonth, k, LeadStatus.IScheduled)
                ctrI += matrix(refMonth, k, LeadStatus.Interviewed)
                ctrO += matrix(refMonth, k, LeadStatus.Others)
                ctrE += matrix(refMonth, k, LeadStatus.Enrolled)
            Next

            arrList.Add(ctrNL)
            arrList.Add(ctrIS)
            arrList.Add(ctrI)
            arrList.Add(ctrO)
            arrList.Add(ctrE)
        End If

        Return arrList
    End Function

    Private Sub ComputePercentage(ByVal row As DataRow, ByVal dr As DataRow, ByVal rw As DataRow)
        If Not row.IsNull("Jan") And Not dr.IsNull("Jan") Then
            If Not row("Jan") <> "0" And Not dr("Jan") <> "0" Then
                rw("Jan") = Convert.ToInt32(Integer.Parse(row("Jan")) * 100 / Integer.Parse(dr("Jan"))) & "%"
            End If
        End If
        If Not row.IsNull("Feb") And Not dr.IsNull("Feb") Then
            If row("Feb") <> "0" And dr("Feb") <> "0" Then
                rw("Feb") = Convert.ToInt32(Integer.Parse(row("Feb")) * 100 / Integer.Parse(dr("Feb"))) & "%"
            End If
        End If
        If Not row.IsNull("Mar") And Not dr.IsNull("Mar") Then
            If row("Mar") <> "0" And dr("Mar") <> "0" Then
                rw("Mar") = Convert.ToInt32(Integer.Parse(row("Mar")) * 100 / Integer.Parse(dr("Mar"))) & "%"
            End If
        End If
        If Not row.IsNull("Apr") And Not dr.IsNull("Apr") Then
            If row("Apr") <> "0" And dr("Apr") <> "0" Then
                rw("Apr") = Convert.ToInt32(Integer.Parse(row("Apr")) * 100 / Integer.Parse(dr("Apr"))) & "%"
            End If
        End If
        If Not row.IsNull("May") And Not dr.IsNull("May") Then
            If row("May") <> "0" And dr("May") <> "0" Then
                rw("May") = Convert.ToInt32(Integer.Parse(row("May")) * 100 / Integer.Parse(dr("May"))) & "%"
            End If
        End If
        If Not row.IsNull("Jun") And Not dr.IsNull("Jun") Then
            If row("Jun") <> "0" And dr("Jun") <> "0" Then
                rw("Jun") = Convert.ToInt32(Integer.Parse(row("Jun")) * 100 / Integer.Parse(dr("Jun"))) & "%"
            End If
        End If
        If Not row.IsNull("Jul") And Not dr.IsNull("Jul") Then
            If row("Jul") <> "0" And dr("Jul") <> "0" Then
                rw("Jul") = Convert.ToInt32(Integer.Parse(row("Jul")) * 100 / Integer.Parse(dr("Jul"))) & "%"
            End If
        End If
        If Not row.IsNull("Aug") And Not dr.IsNull("Aug") Then
            If row("Aug") <> "0" And dr("Aug") <> "0" Then
                rw("Aug") = Convert.ToInt32(Integer.Parse(row("Aug")) * 100 / Integer.Parse(dr("Aug"))) & "%"
            End If
        End If
        If Not row.IsNull("Sep") And Not dr.IsNull("Sep") Then
            If row("Sep") <> "0" And dr("Sep") <> "0" Then
                rw("Sep") = Convert.ToInt32(Integer.Parse(row("Sep")) * 100 / Integer.Parse(dr("Sep"))) & "%"
            End If
        End If
        If Not row.IsNull("Oct") And Not dr.IsNull("Oct") Then
            If row("Oct") <> "0" And dr("Oct") <> "0" Then
                rw("Oct") = Convert.ToInt32(Integer.Parse(row("Oct")) * 100 / Integer.Parse(dr("Oct"))) & "%"
            End If
        End If
        If Not row.IsNull("Nov") And Not dr.IsNull("Nov") Then
            If row("Nov") <> "0" And dr("Nov") <> "0" Then
                rw("Nov") = Convert.ToInt32(Integer.Parse(row("Nov")) * 100 / Integer.Parse(dr("Nov"))) & "%"
            End If
        End If
        If Not row.IsNull("Dec") And Not dr.IsNull("Dec") Then
            If row("Dec") <> "0" And dr("Dec") <> "0" Then
                rw("Dec") = Convert.ToInt32(Integer.Parse(row("Dec")) * 100 / Integer.Parse(dr("Dec"))) & "%"
            End If
        End If
        If Not row.IsNull("Total") And Not dr.IsNull("Total") Then
            If row("Total") <> "0" And dr("Total") <> "0" Then
                rw("Total") = Convert.ToInt32(Integer.Parse(row("Total")) * 100 / Integer.Parse(dr("Total"))) & "%"
            End If
        End If
    End Sub

    Private Function TransformDSforRpt(ByVal dtYTDTemp As DataTable, ByVal AdRepCount As Integer) As DataSet
        Dim row As DataRow
        Dim rw As DataRow
        Dim arrRows() As DataRow
        Dim dtYTD As New DataTable("YTDPerformance")
        Dim ds As New DataSet
        Dim iJan, iFeb, iMar, iApr, iMay, iJun, iJul, iAug, iSep, iOct, iNov, iDec As Integer


        dtYTD.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("CampusIdStr", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("AdmissionsRepStr", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("FullName", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("LeadStatusOrCriteria", System.Type.GetType("System.Int32")))
        dtYTD.Columns.Add(New DataColumn("Descrip", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Jan", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Feb", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Mar", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Apr", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("May", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Jun", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Jul", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Aug", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Sep", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Oct", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Nov", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Dec", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Total", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("AdRepCount", System.Type.GetType("System.Int32")))

        'Loop thru YTDTemp table to produce table expected by report
        For Each dr As DataRow In dtYTDTemp.Rows

            If Not ValExistsInDT(dtYTD, "AdmissionsRepStr", dr("AdmissionsRepStr").ToString) Then

                'Loop thru every LeadStatus
                For iLeadStatus As Integer = 1 To 5

                    arrRows = dtYTDTemp.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                "AND AdmissionsRepStr = '" & dr("AdmissionsRepStr") & "'" & _
                                "AND LeadStatus = " & iLeadStatus, _
                                "CampGrpIdStr,CampusIdStr,AdmissionsRepStr,LeadStatus,Month")

                    If arrRows.GetLength(0) > 0 Then
                        For Each row In arrRows
                            If Not row.IsNull("Month") Then
                                Select Case Integer.Parse(row("Month"))
                                    Case 1
                                        iJan += row("Count")
                                    Case 2
                                        iFeb += row("Count")
                                    Case 3
                                        iMar += row("Count")
                                    Case 4
                                        iApr += row("Count")
                                    Case 5
                                        iMay += row("Count")
                                    Case 6
                                        iJun += row("Count")
                                    Case 7
                                        iJul += row("Count")
                                    Case 8
                                        iAug += row("Count")
                                    Case 9
                                        iSep += row("Count")
                                    Case 10
                                        iOct += row("Count")
                                    Case 11
                                        iNov += row("Count")
                                    Case 12
                                        iDec += row("Count")
                                End Select
                            End If
                        Next
                    End If

                    rw = dtYTD.NewRow
                    rw("AdRepCount") = AdRepCount
                    rw("CampGrpIdStr") = dr("CampGrpIdStr")
                    rw("CampGrpDescrip") = dr("CampGrpDescrip")
                    rw("CampusIdStr") = dr("CampusIdStr")
                    rw("CampDescrip") = dr("CampDescrip")
                    rw("AdmissionsRepStr") = dr("AdmissionsRepStr")
                    rw("FullName") = dr("FullName")

                    Select Case iLeadStatus
                        Case 1
                            rw("LeadStatusOrCriteria") = LeadStatus.NewLead
                            If dtStatusCode.Rows.Count > 0 Then
                                If Not dtStatusCode.Rows(0).IsNull("NewLead") Then
                                    rw("Descrip") = dtStatusCode.Rows(0)("NewLead")
                                    rw("Descrip") &= " "
                                End If
                            End If
                            rw("Descrip") &= "(NL)"

                        Case 2
                            rw("LeadStatusOrCriteria") = LeadStatus.IScheduled
                            If dtStatusCode.Rows.Count > 0 Then
                                If Not dtStatusCode.Rows(0).IsNull("IntScheduled") Then
                                    rw("Descrip") = dtStatusCode.Rows(0)("IntScheduled")
                                    rw("Descrip") &= " "
                                End If
                            End If
                            rw("Descrip") &= "(IS)"

                        Case 3
                            rw("LeadStatusOrCriteria") = LeadStatus.Interviewed
                            If dtStatusCode.Rows.Count > 0 Then
                                If Not dtStatusCode.Rows(0).IsNull("Interviewed") Then
                                    rw("Descrip") = dtStatusCode.Rows(0)("Interviewed")
                                    rw("Descrip") &= " "
                                End If
                            End If
                            rw("Descrip") &= "(I)"

                        Case 4
                            rw("LeadStatusOrCriteria") = LeadStatus.Enrolled
                            If dtStatusCode.Rows.Count > 0 Then
                                If Not dtStatusCode.Rows(0).IsNull("Enrolled") Then
                                    rw("Descrip") = dtStatusCode.Rows(0)("Enrolled")
                                    rw("Descrip") &= " "
                                End If
                            End If
                            rw("Descrip") &= "(E)"

                        Case 5
                            rw("LeadStatusOrCriteria") = LeadStatus.Others
                            rw("Descrip") &= "Others"
                      
                    End Select

                    If iJan <> 0 Then rw("Jan") = iJan
                    If iFeb <> 0 Then rw("Feb") = iFeb
                    If iMar <> 0 Then rw("Mar") = iMar
                    If iApr <> 0 Then rw("Apr") = iApr
                    If iMay <> 0 Then rw("May") = iMay
                    If iJun <> 0 Then rw("Jun") = iJun
                    If iJul <> 0 Then rw("Jul") = iJul
                    If iAug <> 0 Then rw("Aug") = iAug
                    If iSep <> 0 Then rw("Sep") = iSep
                    If iOct <> 0 Then rw("Oct") = iOct
                    If iNov <> 0 Then rw("Nov") = iNov
                    If iDec <> 0 Then rw("Dec") = iDec
                    If (iJan + iFeb + iMar + iApr + iMay + iJun + iJul + iAug + iSep + iOct + iNov + iDec) <> 0 Then
                        rw("Total") = iJan + iFeb + iMar + iApr + iMay + iJun + iJul + iAug + iSep + iOct + iNov + iDec
                    End If
                    dtYTD.Rows.Add(rw)
                    iJan = 0
                    iFeb = 0
                    iMar = 0
                    iApr = 0
                    iMay = 0
                    iJun = 0
                    iJul = 0
                    iAug = 0
                    iSep = 0
                    iOct = 0
                    iNov = 0
                    iDec = 0
                Next
            End If
        Next

        Dim arrRw() As DataRow
        Dim arrRw2() As DataRow
        Dim arrRw3() As DataRow
        Dim i As Integer
        Dim dt As DataTable = dtYTD.Copy

        'Loop thru YTDPerc table to produce table expected by report
        For Each dr As DataRow In dt.Rows

            i = dtYTD.Compute("COUNT(CampGrpIdStr)", _
                                    "CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                    "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                    "AND LeadStatusOrCriteria = " & (LeadStatus.Enrolled + 1))

            If i = 0 Then
                arrRows = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.NewLead, _
                                        "CampGrpIdStr,CampusIdStr,AdmissionsRepStr")

                arrRw = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.IScheduled, _
                                        "CampGrpIdStr,CampusIdStr,AdmissionsRepStr")

                arrRw2 = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.Interviewed, _
                                        "CampGrpIdStr,CampusIdStr,AdmissionsRepStr")

                arrRw3 = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.Enrolled, _
                                        "CampGrpIdStr,CampusIdStr,AdmissionsRepStr")

                ' NL to IS
                If arrRows.GetLength(0) > 0 And arrRows.GetLength(0) = arrRw.GetLength(0) Then
                    i = 0
                    For Each row In arrRows
                        rw = dtYTD.NewRow
                        rw("AdRepCount") = AdRepCount
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("AdmissionsRepStr") = row("AdmissionsRepStr")
                        rw("FullName") = row("FullName")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 1
                        rw("Descrip") = "NL / IS"
                        ComputePercentage(arrRw(i), row, rw)
                        dtYTD.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'IS to I
                If arrRw2.GetLength(0) > 0 And arrRw2.GetLength(0) = arrRw.GetLength(0) Then
                    i = 0
                    For Each row In arrRw
                        rw = dtYTD.NewRow
                        rw("AdRepCount") = AdRepCount
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("AdmissionsRepStr") = row("AdmissionsRepStr")
                        rw("FullName") = row("FullName")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 2
                        rw("Descrip") = "IS / I"
                        ComputePercentage(arrRw2(i), row, rw)
                        dtYTD.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'I to E
                If arrRw.GetLength(0) > 0 And arrRw2.GetLength(0) = arrRw3.GetLength(0) Then
                    i = 0
                    For Each row In arrRw
                        rw = dtYTD.NewRow
                        rw("AdRepCount") = AdRepCount
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("AdmissionsRepStr") = row("AdmissionsRepStr")
                        rw("FullName") = row("FullName")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 3
                        rw("Descrip") = "I / E"
                        ComputePercentage(arrRw3(i), row, rw)
                        dtYTD.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'NL to E
                If arrRows.GetLength(0) > 0 And arrRows.GetLength(0) = arrRw3.GetLength(0) Then
                    i = 0
                    For Each row In arrRows
                        rw = dtYTD.NewRow
                        rw("AdRepCount") = AdRepCount
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("AdmissionsRepStr") = row("AdmissionsRepStr")
                        rw("FullName") = row("FullName")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 4
                        rw("Descrip") = "NL / E"
                        ComputePercentage(arrRw3(i), row, rw)
                        dtYTD.Rows.Add(rw)
                        i += 1
                    Next
                End If
            End If
        Next

        ds.Tables.Add(dtYTD)

        Return ds
    End Function

    Private Function TransformDSforRpt_Revised(ByVal dtYTDTemp As DataTable, ByVal AdRepCount As Integer) As DataSet
        Dim row As DataRow
        Dim rw As DataRow
        Dim arrRows() As DataRow
        Dim dtYTD As New DataTable("YTDPerformance")
        Dim ds As New DataSet
        Dim iJan, iFeb, iMar, iApr, iMay, iJun, iJul, iAug, iSep, iOct, iNov, iDec As Integer


        dtYTD.Columns.Add(New DataColumn("CampGrpIdStr", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("CampGrpDescrip", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("CampusIdStr", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("CampDescrip", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("AdmissionsRepStr", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("FullName", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("LeadStatusOrCriteria", System.Type.GetType("System.Int32")))
        dtYTD.Columns.Add(New DataColumn("Descrip", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Jan", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Feb", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Mar", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Apr", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("May", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Jun", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Jul", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Aug", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Sep", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Oct", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Nov", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Dec", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("Total", System.Type.GetType("System.String")))
        dtYTD.Columns.Add(New DataColumn("AdRepCount", System.Type.GetType("System.Int32")))

        'Loop thru YTDTemp table to produce table expected by report
        For Each dr As DataRow In dtYTDTemp.Rows

            If Not ValExistsInDT(dtYTD, "AdmissionsRepStr", dr("AdmissionsRepStr").ToString) Then

                'Loop thru every LeadStatus
                For iLeadStatus As Integer = 1 To 9

                    arrRows = dtYTDTemp.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                "AND AdmissionsRepStr = '" & dr("AdmissionsRepStr") & "'" & _
                                "AND LeadStatus = " & iLeadStatus, _
                                "CampGrpIdStr,CampusIdStr,AdmissionsRepStr,LeadStatus,Month")

                    If arrRows.GetLength(0) > 0 Then
                        For Each row In arrRows
                            If Not row.IsNull("Month") Then
                                Select Case Integer.Parse(row("Month"))
                                    Case 1
                                        iJan += row("Count")
                                    Case 2
                                        iFeb += row("Count")
                                    Case 3
                                        iMar += row("Count")
                                    Case 4
                                        iApr += row("Count")
                                    Case 5
                                        iMay += row("Count")
                                    Case 6
                                        iJun += row("Count")
                                    Case 7
                                        iJul += row("Count")
                                    Case 8
                                        iAug += row("Count")
                                    Case 9
                                        iSep += row("Count")
                                    Case 10
                                        iOct += row("Count")
                                    Case 11
                                        iNov += row("Count")
                                    Case 12
                                        iDec += row("Count")
                                End Select
                            End If
                        Next
                    End If

                    rw = dtYTD.NewRow
                    rw("AdRepCount") = AdRepCount
                    rw("CampGrpIdStr") = dr("CampGrpIdStr")
                    rw("CampGrpDescrip") = dr("CampGrpDescrip")
                    rw("CampusIdStr") = dr("CampusIdStr")
                    rw("CampDescrip") = dr("CampDescrip")
                    rw("AdmissionsRepStr") = dr("AdmissionsRepStr")
                    rw("FullName") = dr("FullName")

                    Select Case iLeadStatus
                        Case 1
                            rw("LeadStatusOrCriteria") = LeadStatus.NewLead
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("NewLead") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("NewLead")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(NL)"
                            rw("Descrip") &= "New Lead"
                        Case 2
                            rw("LeadStatusOrCriteria") = LeadStatus.IScheduled
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("IntScheduled") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("IntScheduled")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(IS)"
                            rw("Descrip") &= "Interview Scheduled"
                        Case 3
                            rw("LeadStatusOrCriteria") = LeadStatus.Interviewed
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("Interviewed") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("Interviewed")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(I)"
                            rw("Descrip") &= "Interviewed"
                        Case 4
                            rw("LeadStatusOrCriteria") = LeadStatus.Enrolled
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("Enrolled") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("Enrolled")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(E)"
                            rw("Descrip") &= "Enrolled"
                        Case 5
                            rw("LeadStatusOrCriteria") = LeadStatus.Others
                            rw("Descrip") &= "Others"
                        Case 6
                            rw("LeadStatusOrCriteria") = LeadStatus.ApplicationReceived
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("ApplicationReceived") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("ApplicationReceived")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(AR)"
                            rw("Descrip") &= "Application Received"
                        Case 7
                            rw("LeadStatusOrCriteria") = LeadStatus.ApplicationNotAccepted
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("ApplicationNotAccepted") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("ApplicationNotAccepted")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(ANA)"
                            rw("Descrip") &= "Application Not Accepted"
                        Case 8
                            rw("LeadStatusOrCriteria") = LeadStatus.DeadLead
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("DeadLead") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("DeadLead")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(DL)"
                            rw("Descrip") &= "Dead Lead"
                        Case 9
                            rw("LeadStatusOrCriteria") = LeadStatus.WillEnrollinFuture
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("WillEnrollinFuture") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("WillEnrollinFuture")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(WEIF)"
                            rw("Descrip") &= "Will Enroll in the Future"
                    End Select

                    If iJan <> 0 Then rw("Jan") = iJan
                    If iFeb <> 0 Then rw("Feb") = iFeb
                    If iMar <> 0 Then rw("Mar") = iMar
                    If iApr <> 0 Then rw("Apr") = iApr
                    If iMay <> 0 Then rw("May") = iMay
                    If iJun <> 0 Then rw("Jun") = iJun
                    If iJul <> 0 Then rw("Jul") = iJul
                    If iAug <> 0 Then rw("Aug") = iAug
                    If iSep <> 0 Then rw("Sep") = iSep
                    If iOct <> 0 Then rw("Oct") = iOct
                    If iNov <> 0 Then rw("Nov") = iNov
                    If iDec <> 0 Then rw("Dec") = iDec
                    If (iJan + iFeb + iMar + iApr + iMay + iJun + iJul + iAug + iSep + iOct + iNov + iDec) <> 0 Then
                        rw("Total") = iJan + iFeb + iMar + iApr + iMay + iJun + iJul + iAug + iSep + iOct + iNov + iDec
                    End If
                    dtYTD.Rows.Add(rw)
                    iJan = 0
                    iFeb = 0
                    iMar = 0
                    iApr = 0
                    iMay = 0
                    iJun = 0
                    iJul = 0
                    iAug = 0
                    iSep = 0
                    iOct = 0
                    iNov = 0
                    iDec = 0
                Next
            End If
        Next

        Dim arrRw() As DataRow
        Dim arrRw2() As DataRow
        Dim arrRw3() As DataRow
        Dim i As Integer
        Dim dt As DataTable = dtYTD.Copy

        'Loop thru YTDPerc table to produce table expected by report
        For Each dr As DataRow In dt.Rows

            i = dtYTD.Compute("COUNT(CampGrpIdStr)", _
                                    "CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                    "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                    "AND LeadStatusOrCriteria = " & (LeadStatus.Enrolled + 1))

            If i = 0 Then
                arrRows = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.NewLead, _
                                        "CampGrpIdStr,CampusIdStr,AdmissionsRepStr")

                arrRw = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.IScheduled, _
                                        "CampGrpIdStr,CampusIdStr,AdmissionsRepStr")

                arrRw2 = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.Interviewed, _
                                        "CampGrpIdStr,CampusIdStr,AdmissionsRepStr")

                arrRw3 = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.Enrolled, _
                                        "CampGrpIdStr,CampusIdStr,AdmissionsRepStr")

                ' NL to IS
                If arrRows.GetLength(0) > 0 And arrRows.GetLength(0) = arrRw.GetLength(0) Then
                    i = 0
                    For Each row In arrRows
                        rw = dtYTD.NewRow
                        rw("AdRepCount") = AdRepCount
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("AdmissionsRepStr") = row("AdmissionsRepStr")
                        rw("FullName") = row("FullName")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 1
                        rw("Descrip") = "NL / IS"
                        ComputePercentage(arrRw(i), row, rw)
                        dtYTD.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'IS to I
                If arrRw2.GetLength(0) > 0 And arrRw2.GetLength(0) = arrRw.GetLength(0) Then
                    i = 0
                    For Each row In arrRw
                        rw = dtYTD.NewRow
                        rw("AdRepCount") = AdRepCount
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("AdmissionsRepStr") = row("AdmissionsRepStr")
                        rw("FullName") = row("FullName")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 2
                        rw("Descrip") = "IS / I"
                        ComputePercentage(arrRw2(i), row, rw)
                        dtYTD.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'I to E
                If arrRw.GetLength(0) > 0 And arrRw2.GetLength(0) = arrRw3.GetLength(0) Then
                    i = 0
                    For Each row In arrRw
                        rw = dtYTD.NewRow
                        rw("AdRepCount") = AdRepCount
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("AdmissionsRepStr") = row("AdmissionsRepStr")
                        rw("FullName") = row("FullName")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 3
                        rw("Descrip") = "I / E"
                        ComputePercentage(arrRw3(i), row, rw)
                        dtYTD.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'NL to E
                If arrRows.GetLength(0) > 0 And arrRows.GetLength(0) = arrRw3.GetLength(0) Then
                    i = 0
                    For Each row In arrRows
                        rw = dtYTD.NewRow
                        rw("AdRepCount") = AdRepCount
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("AdmissionsRepStr") = row("AdmissionsRepStr")
                        rw("FullName") = row("FullName")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 4
                        rw("Descrip") = "NL / E"
                        ComputePercentage(arrRw3(i), row, rw)
                        dtYTD.Rows.Add(rw)
                        i += 1
                    Next
                End If
            End If
        Next

        ds.Tables.Add(dtYTD)

        Return ds
    End Function

    Private Function ValExistsInDT(ByVal dt As DataTable, ByVal columnName As String, ByVal value As String) As Boolean
        Dim rows() As DataRow

        rows = dt.Select(columnName & " = '" & value & "'")
        If rows.GetLength(0) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub ComputeCampusTotals(ByVal dtYTD As DataTable, ByVal dtCampusTotals As DataTable)
        Dim arrRows() As DataRow
        Dim arrRw() As DataRow
        Dim arrRw2() As DataRow
        Dim arrRw3() As DataRow
        Dim i, iLeadStatus As Integer
        Dim row As DataRow
        Dim rw As DataRow
        Dim iJan, iFeb, iMar, iApr, iMay, iJun, iJul, iAug, iSep, iOct, iNov, iDec, iTotal As Integer

        'Loop thru YTDPerformance table to compute totals by campus
        For Each dr As DataRow In dtYTD.Rows
            If dtCampusTotals.Rows.Count = 0 Then
                i = 0
            Else
                i = dtCampusTotals.Compute("COUNT(CampGrpIdStr)", _
                                        "CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "'")
            End If
            If i = 0 Then
                For iLeadStatus = 1 To 5
                    arrRows = dtYTD.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                            "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                            "AND LeadStatusOrCriteria = " & iLeadStatus, _
                                            "CampGrpIdStr,CampusIdStr,AdmissionsRepStr")
                    If arrRows.GetLength(0) > 0 Then
                        For Each row In arrRows
                            If Not row.IsNull("Jan") Then iJan += row("Jan")
                            If Not row.IsNull("Feb") Then iFeb += row("Feb")
                            If Not row.IsNull("Mar") Then iMar += row("Mar")
                            If Not row.IsNull("Apr") Then iApr += row("Apr")
                            If Not row.IsNull("May") Then iMay += row("May")
                            If Not row.IsNull("Jun") Then iJun += row("Jun")
                            If Not row.IsNull("Jul") Then iJul += row("Jul")
                            If Not row.IsNull("Aug") Then iAug += row("Aug")
                            If Not row.IsNull("Sep") Then iSep += row("Sep")
                            If Not row.IsNull("Oct") Then iOct += row("Oct")
                            If Not row.IsNull("Nov") Then iNov += row("Nov")
                            If Not row.IsNull("Dec") Then iDec += row("Dec")
                            If Not row.IsNull("Total") Then iTotal += row("Total")
                        Next
                    End If

                    rw = dtCampusTotals.NewRow
                    rw("CampGrpIdStr") = dr("CampGrpIdStr")
                    rw("CampGrpDescrip") = dr("CampGrpDescrip")
                    rw("CampusIdStr") = dr("CampusIdStr")
                    rw("CampDescrip") = dr("CampDescrip")

                    Select Case iLeadStatus
                        Case 1
                            rw("LeadStatusOrCriteria") = LeadStatus.NewLead
                            If dtStatusCode.Rows.Count > 0 Then
                                If Not dtStatusCode.Rows(0).IsNull("NewLead") Then
                                    rw("Descrip") = dtStatusCode.Rows(0)("NewLead")
                                    rw("Descrip") &= " "
                                End If
                            End If
                            rw("Descrip") &= "(NL)"

                        Case 2
                            rw("LeadStatusOrCriteria") = LeadStatus.IScheduled
                            If dtStatusCode.Rows.Count > 0 Then
                                If Not dtStatusCode.Rows(0).IsNull("IntScheduled") Then
                                    rw("Descrip") = dtStatusCode.Rows(0)("IntScheduled")
                                    rw("Descrip") &= " "
                                End If
                            End If
                            rw("Descrip") &= "(IS)"

                        Case 3
                            rw("LeadStatusOrCriteria") = LeadStatus.Interviewed
                            If dtStatusCode.Rows.Count > 0 Then
                                If Not dtStatusCode.Rows(0).IsNull("Interviewed") Then
                                    rw("Descrip") = dtStatusCode.Rows(0)("Interviewed")
                                    rw("Descrip") &= " "
                                End If
                            End If
                            rw("Descrip") &= "(I)"

                        Case 4
                            rw("LeadStatusOrCriteria") = LeadStatus.Enrolled
                            If dtStatusCode.Rows.Count > 0 Then
                                If Not dtStatusCode.Rows(0).IsNull("Enrolled") Then
                                    rw("Descrip") = dtStatusCode.Rows(0)("Enrolled")
                                    rw("Descrip") &= " "
                                End If
                            End If
                            rw("Descrip") &= "(E)"

                        Case 5
                            rw("LeadStatusOrCriteria") = LeadStatus.Others
                            rw("Descrip") &= "Others"
                    End Select

                    If iJan <> 0 Then rw("Jan") = iJan
                    If iFeb <> 0 Then rw("Feb") = iFeb
                    If iMar <> 0 Then rw("Mar") = iMar
                    If iApr <> 0 Then rw("Apr") = iApr
                    If iMay <> 0 Then rw("May") = iMay
                    If iJun <> 0 Then rw("Jun") = iJun
                    If iJul <> 0 Then rw("Jul") = iJul
                    If iAug <> 0 Then rw("Aug") = iAug
                    If iSep <> 0 Then rw("Sep") = iSep
                    If iOct <> 0 Then rw("Oct") = iOct
                    If iNov <> 0 Then rw("Nov") = iNov
                    If iDec <> 0 Then rw("Dec") = iDec
                    If iTotal <> 0 Then rw("Total") = iTotal
                    dtCampusTotals.Rows.Add(rw)

                    iJan = 0
                    iFeb = 0
                    iMar = 0
                    iApr = 0
                    iMay = 0
                    iJun = 0
                    iJul = 0
                    iAug = 0
                    iSep = 0
                    iOct = 0
                    iNov = 0
                    iDec = 0
                    iTotal = 0
                Next
            End If
        Next

        Dim dt As DataTable = dtCampusTotals.Copy
        For Each dr As DataRow In dt.Rows

            i = dtCampusTotals.Compute("COUNT(CampGrpIdStr)", _
                                    "CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                    "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                    "AND LeadStatusOrCriteria = " & (LeadStatus.Enrolled + 1))

            If i = 0 Then
                arrRows = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.NewLead, _
                                        "CampGrpIdStr,CampusIdStr")

                arrRw = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.IScheduled, _
                                        "CampGrpIdStr,CampusIdStr")

                arrRw2 = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.Interviewed, _
                                        "CampGrpIdStr,CampusIdStr")

                arrRw3 = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.Enrolled, _
                                        "CampGrpIdStr,CampusIdStr")

                ' NL to IS
                If arrRows.GetLength(0) > 0 And arrRows.GetLength(0) = arrRw.GetLength(0) Then
                    i = 0
                    For Each row In arrRows
                        rw = dtCampusTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 1
                        rw("Descrip") = "NL / IS"
                        ComputePercentage(arrRw(i), row, rw)
                        dtCampusTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'IS to I
                If arrRw2.GetLength(0) > 0 And arrRw2.GetLength(0) = arrRw.GetLength(0) Then
                    i = 0
                    For Each row In arrRw
                        rw = dtCampusTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 2
                        rw("Descrip") = "IS / I"
                        ComputePercentage(arrRw2(i), row, rw)
                        dtCampusTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'I to E
                If arrRw.GetLength(0) > 0 And arrRw2.GetLength(0) = arrRw3.GetLength(0) Then
                    i = 0
                    For Each row In arrRw
                        rw = dtCampusTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 3
                        rw("Descrip") = "I / E"
                        ComputePercentage(arrRw3(i), row, rw)
                        dtCampusTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'NL to E
                If arrRows.GetLength(0) > 0 And arrRows.GetLength(0) = arrRw3.GetLength(0) Then
                    i = 0
                    For Each row In arrRows
                        rw = dtCampusTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 4
                        rw("Descrip") = "NL / E"
                        ComputePercentage(arrRw3(i), row, rw)
                        dtCampusTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
            End If
        Next

        'Change LeadStatusOrCriteria, so the Enrolled status is listed after Others.
        AlterViewOrder(dtCampusTotals)
    End Sub

    Private Sub ComputeCampusTotals_Revised(ByVal dtYTD As DataTable, ByVal dtCampusTotals As DataTable)
        Dim arrRows() As DataRow
        Dim arrRw() As DataRow
        Dim arrRw2() As DataRow
        Dim arrRw3() As DataRow
        Dim i, iLeadStatus As Integer
        Dim row As DataRow
        Dim rw As DataRow
        Dim iJan, iFeb, iMar, iApr, iMay, iJun, iJul, iAug, iSep, iOct, iNov, iDec, iTotal As Integer

        'Loop thru YTDPerformance table to compute totals by campus
        For Each dr As DataRow In dtYTD.Rows
            If dtCampusTotals.Rows.Count = 0 Then
                i = 0
            Else
                i = dtCampusTotals.Compute("COUNT(CampGrpIdStr)", _
                                        "CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "'")
            End If
            If i = 0 Then
                For iLeadStatus = 1 To 9
                    arrRows = dtYTD.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                            "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                            "AND LeadStatusOrCriteria = " & iLeadStatus, _
                                            "CampGrpIdStr,CampusIdStr,AdmissionsRepStr")
                    If arrRows.GetLength(0) > 0 Then
                        For Each row In arrRows
                            If Not row.IsNull("Jan") Then iJan += row("Jan")
                            If Not row.IsNull("Feb") Then iFeb += row("Feb")
                            If Not row.IsNull("Mar") Then iMar += row("Mar")
                            If Not row.IsNull("Apr") Then iApr += row("Apr")
                            If Not row.IsNull("May") Then iMay += row("May")
                            If Not row.IsNull("Jun") Then iJun += row("Jun")
                            If Not row.IsNull("Jul") Then iJul += row("Jul")
                            If Not row.IsNull("Aug") Then iAug += row("Aug")
                            If Not row.IsNull("Sep") Then iSep += row("Sep")
                            If Not row.IsNull("Oct") Then iOct += row("Oct")
                            If Not row.IsNull("Nov") Then iNov += row("Nov")
                            If Not row.IsNull("Dec") Then iDec += row("Dec")
                            If Not row.IsNull("Total") Then iTotal += row("Total")
                        Next
                    End If

                    rw = dtCampusTotals.NewRow
                    rw("CampGrpIdStr") = dr("CampGrpIdStr")
                    rw("CampGrpDescrip") = dr("CampGrpDescrip")
                    rw("CampusIdStr") = dr("CampusIdStr")
                    rw("CampDescrip") = dr("CampDescrip")

                    Select Case iLeadStatus
                        Case 1
                            rw("LeadStatusOrCriteria") = LeadStatus.NewLead
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("NewLead") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("NewLead")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(NL)"
                            rw("Descrip") &= "New Lead"
                        Case 2
                            rw("LeadStatusOrCriteria") = LeadStatus.IScheduled
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("IntScheduled") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("IntScheduled")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(IS)"
                            rw("Descrip") &= "Interview Scheduled"
                        Case 3
                            rw("LeadStatusOrCriteria") = LeadStatus.Interviewed
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("Interviewed") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("Interviewed")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(I)"
                            rw("Descrip") &= "Interviewed"
                        Case 4
                            rw("LeadStatusOrCriteria") = LeadStatus.Enrolled
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("Enrolled") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("Enrolled")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(E)"
                            rw("Descrip") &= "Enrolled"
                        Case 5
                            rw("LeadStatusOrCriteria") = LeadStatus.Others
                            rw("Descrip") &= "Others"
                        Case 6
                            rw("LeadStatusOrCriteria") = LeadStatus.ApplicationReceived
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("ApplicationReceived") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("ApplicationReceived")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(AR)"
                            rw("Descrip") &= "Application Received"
                        Case 7
                            rw("LeadStatusOrCriteria") = LeadStatus.ApplicationNotAccepted
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("ApplicationNotAccepted") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("ApplicationNotAccepted")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(ANA)"
                            rw("Descrip") &= "Application Not Accepted"
                        Case 8
                            rw("LeadStatusOrCriteria") = LeadStatus.DeadLead
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("DeadLead") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("DeadLead")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(DL)"
                            rw("Descrip") &= "Dead Lead"
                        Case 9
                            rw("LeadStatusOrCriteria") = LeadStatus.WillEnrollinFuture
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("WillEnrollinFuture") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("WillEnrollinFuture")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(WEIF)"
                            rw("Descrip") &= "Will Enroll in the Future"
                    End Select

                    If iJan <> 0 Then rw("Jan") = iJan
                    If iFeb <> 0 Then rw("Feb") = iFeb
                    If iMar <> 0 Then rw("Mar") = iMar
                    If iApr <> 0 Then rw("Apr") = iApr
                    If iMay <> 0 Then rw("May") = iMay
                    If iJun <> 0 Then rw("Jun") = iJun
                    If iJul <> 0 Then rw("Jul") = iJul
                    If iAug <> 0 Then rw("Aug") = iAug
                    If iSep <> 0 Then rw("Sep") = iSep
                    If iOct <> 0 Then rw("Oct") = iOct
                    If iNov <> 0 Then rw("Nov") = iNov
                    If iDec <> 0 Then rw("Dec") = iDec
                    If iTotal <> 0 Then rw("Total") = iTotal
                    dtCampusTotals.Rows.Add(rw)

                    iJan = 0
                    iFeb = 0
                    iMar = 0
                    iApr = 0
                    iMay = 0
                    iJun = 0
                    iJul = 0
                    iAug = 0
                    iSep = 0
                    iOct = 0
                    iNov = 0
                    iDec = 0
                    iTotal = 0
                Next
            End If
        Next

        Dim dt As DataTable = dtCampusTotals.Copy
        For Each dr As DataRow In dt.Rows

            i = dtCampusTotals.Compute("COUNT(CampGrpIdStr)", _
                                    "CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                    "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                    "AND LeadStatusOrCriteria = " & (LeadStatus.Enrolled + 1))

            If i = 0 Then
                arrRows = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.NewLead, _
                                        "CampGrpIdStr,CampusIdStr")

                arrRw = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.IScheduled, _
                                        "CampGrpIdStr,CampusIdStr")

                arrRw2 = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.Interviewed, _
                                        "CampGrpIdStr,CampusIdStr")

                arrRw3 = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND CampusIdStr = '" & dr("CampusIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.Enrolled, _
                                        "CampGrpIdStr,CampusIdStr")

                ' NL to IS
                If arrRows.GetLength(0) > 0 And arrRows.GetLength(0) = arrRw.GetLength(0) Then
                    i = 0
                    For Each row In arrRows
                        rw = dtCampusTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 1
                        rw("Descrip") = "NL / IS"
                        ComputePercentage(arrRw(i), row, rw)
                        dtCampusTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'IS to I
                If arrRw2.GetLength(0) > 0 And arrRw2.GetLength(0) = arrRw.GetLength(0) Then
                    i = 0
                    For Each row In arrRw
                        rw = dtCampusTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 2
                        rw("Descrip") = "IS / I"
                        ComputePercentage(arrRw2(i), row, rw)
                        dtCampusTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'I to E
                If arrRw.GetLength(0) > 0 And arrRw2.GetLength(0) = arrRw3.GetLength(0) Then
                    i = 0
                    For Each row In arrRw
                        rw = dtCampusTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 3
                        rw("Descrip") = "I / E"
                        ComputePercentage(arrRw3(i), row, rw)
                        dtCampusTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'NL to E
                If arrRows.GetLength(0) > 0 And arrRows.GetLength(0) = arrRw3.GetLength(0) Then
                    i = 0
                    For Each row In arrRows
                        rw = dtCampusTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("CampusIdStr") = row("CampusIdStr")
                        rw("CampDescrip") = row("CampDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 4
                        rw("Descrip") = "NL / E"
                        ComputePercentage(arrRw3(i), row, rw)
                        dtCampusTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
            End If
        Next

        'Change LeadStatusOrCriteria, so the Enrolled status is listed after Others.
        AlterViewOrder(dtCampusTotals)
    End Sub


    Private Sub ComputeCampusGroupTotals(ByVal dtYTD As DataTable, ByVal dtCGTotals As DataTable)
        Dim arrRows() As DataRow
        Dim arrRw() As DataRow
        Dim arrRw2() As DataRow
        Dim arrRw3() As DataRow
        Dim i, iLeadStatus As Integer
        Dim row As DataRow
        Dim rw As DataRow
        Dim iJan, iFeb, iMar, iApr, iMay, iJun, iJul, iAug, iSep, iOct, iNov, iDec, iTotal As Integer

        'Loop thru YTDPerformance table to compute totals by campus
        For Each dr As DataRow In dtYTD.Rows
            If dtCGTotals.Rows.Count = 0 Then
                i = 0
            Else
                i = dtCGTotals.Compute("COUNT(CampGrpIdStr)", _
                                        "CampGrpIdStr = '" & dr("CampGrpIdStr") & "'")
            End If
            If i = 0 Then
                For iLeadStatus = 1 To 5
                    arrRows = dtYTD.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                            "AND LeadStatusOrCriteria = " & iLeadStatus, _
                                            "CampGrpIdStr,CampusIdStr,AdmissionsRepStr")
                    If arrRows.GetLength(0) > 0 Then
                        For Each row In arrRows
                            If Not row.IsNull("Jan") Then iJan += row("Jan")
                            If Not row.IsNull("Feb") Then iFeb += row("Feb")
                            If Not row.IsNull("Mar") Then iMar += row("Mar")
                            If Not row.IsNull("Apr") Then iApr += row("Apr")
                            If Not row.IsNull("May") Then iMay += row("May")
                            If Not row.IsNull("Jun") Then iJun += row("Jun")
                            If Not row.IsNull("Jul") Then iJul += row("Jul")
                            If Not row.IsNull("Aug") Then iAug += row("Aug")
                            If Not row.IsNull("Sep") Then iSep += row("Sep")
                            If Not row.IsNull("Oct") Then iOct += row("Oct")
                            If Not row.IsNull("Nov") Then iNov += row("Nov")
                            If Not row.IsNull("Dec") Then iDec += row("Dec")
                            If Not row.IsNull("Total") Then iTotal += row("Total")
                        Next
                    End If

                    rw = dtCGTotals.NewRow
                    rw("CampGrpIdStr") = dr("CampGrpIdStr")
                    rw("CampGrpDescrip") = dr("CampGrpDescrip")

                    Select Case iLeadStatus
                        Case 1
                            rw("LeadStatusOrCriteria") = LeadStatus.NewLead
                            If dtStatusCode.Rows.Count > 0 Then
                                If Not dtStatusCode.Rows(0).IsNull("NewLead") Then
                                    rw("Descrip") = dtStatusCode.Rows(0)("NewLead")
                                    rw("Descrip") &= " "
                                End If
                            End If
                            rw("Descrip") &= "(NL)"

                        Case 2
                            rw("LeadStatusOrCriteria") = LeadStatus.IScheduled
                            If dtStatusCode.Rows.Count > 0 Then
                                If Not dtStatusCode.Rows(0).IsNull("IntScheduled") Then
                                    rw("Descrip") = dtStatusCode.Rows(0)("IntScheduled")
                                    rw("Descrip") &= " "
                                End If
                            End If
                            rw("Descrip") &= "(IS)"

                        Case 3
                            rw("LeadStatusOrCriteria") = LeadStatus.Interviewed
                            If dtStatusCode.Rows.Count > 0 Then
                                If Not dtStatusCode.Rows(0).IsNull("Interviewed") Then
                                    rw("Descrip") = dtStatusCode.Rows(0)("Interviewed")
                                    rw("Descrip") &= " "
                                End If
                            End If
                            rw("Descrip") &= "(I)"

                        Case 4
                            rw("LeadStatusOrCriteria") = LeadStatus.Enrolled
                            If dtStatusCode.Rows.Count > 0 Then
                                If Not dtStatusCode.Rows(0).IsNull("Enrolled") Then
                                    rw("Descrip") = dtStatusCode.Rows(0)("Enrolled")
                                    rw("Descrip") &= " "
                                End If
                            End If
                            rw("Descrip") &= "(E)"

                        Case 5
                            rw("LeadStatusOrCriteria") = LeadStatus.Others
                            rw("Descrip") &= "Others"
                    End Select

                    If iJan <> 0 Then rw("Jan") = iJan
                    If iFeb <> 0 Then rw("Feb") = iFeb
                    If iMar <> 0 Then rw("Mar") = iMar
                    If iApr <> 0 Then rw("Apr") = iApr
                    If iMay <> 0 Then rw("May") = iMay
                    If iJun <> 0 Then rw("Jun") = iJun
                    If iJul <> 0 Then rw("Jul") = iJul
                    If iAug <> 0 Then rw("Aug") = iAug
                    If iSep <> 0 Then rw("Sep") = iSep
                    If iOct <> 0 Then rw("Oct") = iOct
                    If iNov <> 0 Then rw("Nov") = iNov
                    If iDec <> 0 Then rw("Dec") = iDec
                    If iTotal <> 0 Then rw("Total") = iTotal
                    dtCGTotals.Rows.Add(rw)

                    iJan = 0
                    iFeb = 0
                    iMar = 0
                    iApr = 0
                    iMay = 0
                    iJun = 0
                    iJul = 0
                    iAug = 0
                    iSep = 0
                    iOct = 0
                    iNov = 0
                    iDec = 0
                    iTotal = 0
                Next
            End If
        Next

        Dim dt As DataTable = dtCGTotals.Copy
        For Each dr As DataRow In dt.Rows

            i = dtCGTotals.Compute("COUNT(CampGrpIdStr)", _
                                    "CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                    "AND LeadStatusOrCriteria = " & (LeadStatus.Enrolled + 1))

            If i = 0 Then
                arrRows = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.NewLead, _
                                        "CampGrpIdStr")

                arrRw = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.IScheduled, _
                                        "CampGrpIdStr")

                arrRw2 = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.Interviewed, _
                                        "CampGrpIdStr")

                arrRw3 = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.Enrolled, _
                                        "CampGrpIdStr")

                ' NL to IS
                If arrRows.GetLength(0) > 0 And arrRows.GetLength(0) = arrRw.GetLength(0) Then
                    i = 0
                    For Each row In arrRows
                        rw = dtCGTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 1
                        rw("Descrip") = "NL / IS"
                        ComputePercentage(arrRw(i), row, rw)
                        dtCGTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'IS to I
                If arrRw2.GetLength(0) > 0 And arrRw2.GetLength(0) = arrRw.GetLength(0) Then
                    i = 0
                    For Each row In arrRw
                        rw = dtCGTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 2
                        rw("Descrip") = "IS / I"
                        ComputePercentage(arrRw2(i), row, rw)
                        dtCGTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'I to E
                If arrRw.GetLength(0) > 0 And arrRw2.GetLength(0) = arrRw3.GetLength(0) Then
                    i = 0
                    For Each row In arrRw
                        rw = dtCGTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 3
                        rw("Descrip") = "I / E"
                        ComputePercentage(arrRw3(i), row, rw)
                        dtCGTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'NL to E
                If arrRows.GetLength(0) > 0 And arrRows.GetLength(0) = arrRw3.GetLength(0) Then
                    i = 0
                    For Each row In arrRows
                        rw = dtCGTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 4
                        rw("Descrip") = "NL / E"
                        ComputePercentage(arrRw3(i), row, rw)
                        dtCGTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
            End If
        Next

        'Change LeadStatusOrCriteria, so the Enrolled status is listed after Others.
        AlterViewOrder(dtCGTotals)
    End Sub

    Private Sub ComputeCampusGroupTotals_Revised(ByVal dtYTD As DataTable, ByVal dtCGTotals As DataTable)
        Dim arrRows() As DataRow
        Dim arrRw() As DataRow
        Dim arrRw2() As DataRow
        Dim arrRw3() As DataRow
        Dim i, iLeadStatus As Integer
        Dim row As DataRow
        Dim rw As DataRow
        Dim iJan, iFeb, iMar, iApr, iMay, iJun, iJul, iAug, iSep, iOct, iNov, iDec, iTotal As Integer

        'Loop thru YTDPerformance table to compute totals by campus
        For Each dr As DataRow In dtYTD.Rows
            If dtCGTotals.Rows.Count = 0 Then
                i = 0
            Else
                i = dtCGTotals.Compute("COUNT(CampGrpIdStr)", _
                                        "CampGrpIdStr = '" & dr("CampGrpIdStr") & "'")
            End If
            If i = 0 Then
                For iLeadStatus = 1 To 9
                    arrRows = dtYTD.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                            "AND LeadStatusOrCriteria = " & iLeadStatus, _
                                            "CampGrpIdStr,CampusIdStr,AdmissionsRepStr")
                    If arrRows.GetLength(0) > 0 Then
                        For Each row In arrRows
                            If Not row.IsNull("Jan") Then iJan += row("Jan")
                            If Not row.IsNull("Feb") Then iFeb += row("Feb")
                            If Not row.IsNull("Mar") Then iMar += row("Mar")
                            If Not row.IsNull("Apr") Then iApr += row("Apr")
                            If Not row.IsNull("May") Then iMay += row("May")
                            If Not row.IsNull("Jun") Then iJun += row("Jun")
                            If Not row.IsNull("Jul") Then iJul += row("Jul")
                            If Not row.IsNull("Aug") Then iAug += row("Aug")
                            If Not row.IsNull("Sep") Then iSep += row("Sep")
                            If Not row.IsNull("Oct") Then iOct += row("Oct")
                            If Not row.IsNull("Nov") Then iNov += row("Nov")
                            If Not row.IsNull("Dec") Then iDec += row("Dec")
                            If Not row.IsNull("Total") Then iTotal += row("Total")
                        Next
                    End If

                    rw = dtCGTotals.NewRow
                    rw("CampGrpIdStr") = dr("CampGrpIdStr")
                    rw("CampGrpDescrip") = dr("CampGrpDescrip")

                    Select Case iLeadStatus
                        Case 1
                            rw("LeadStatusOrCriteria") = LeadStatus.NewLead
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("NewLead") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("NewLead")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(NL)"
                            rw("Descrip") &= "New Lead"
                        Case 2
                            rw("LeadStatusOrCriteria") = LeadStatus.IScheduled
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("IntScheduled") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("IntScheduled")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(IS)"
                            rw("Descrip") &= "Interview Scheduled"
                        Case 3
                            rw("LeadStatusOrCriteria") = LeadStatus.Interviewed
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("Interviewed") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("Interviewed")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(I)"
                            rw("Descrip") &= "Interviewed"
                        Case 4
                            rw("LeadStatusOrCriteria") = LeadStatus.Enrolled
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("Enrolled") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("Enrolled")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(E)"
                            rw("Descrip") &= "Enrolled"
                        Case 6
                            rw("LeadStatusOrCriteria") = LeadStatus.ApplicationReceived
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("ApplicationReceived") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("ApplicationReceived")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(AR)"
                            rw("Descrip") &= "Application Received"
                        Case 7
                            rw("LeadStatusOrCriteria") = LeadStatus.ApplicationNotAccepted
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("ApplicationNotAccepted") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("ApplicationNotAccepted")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(ANA)"
                            rw("Descrip") &= "Application Not Accepted"
                        Case 8
                            rw("LeadStatusOrCriteria") = LeadStatus.DeadLead
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("DeadLead") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("DeadLead")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(DL)"
                            rw("Descrip") &= "Dead Lead"
                        Case 9
                            rw("LeadStatusOrCriteria") = LeadStatus.WillEnrollinFuture
                            'If dtStatusCode.Rows.Count > 0 Then
                            '    If Not dtStatusCode.Rows(0).IsNull("WillEnrollinFuture") Then
                            '        rw("Descrip") = dtStatusCode.Rows(0)("WillEnrollinFuture")
                            '        rw("Descrip") &= " "
                            '    End If
                            'End If
                            'rw("Descrip") &= "(WEIF)"
                            rw("Descrip") &= "Will Enroll in the Future"
                        Case 5
                            rw("LeadStatusOrCriteria") = LeadStatus.Others
                            rw("Descrip") &= "Others"
                    End Select

                    If iJan <> 0 Then rw("Jan") = iJan
                    If iFeb <> 0 Then rw("Feb") = iFeb
                    If iMar <> 0 Then rw("Mar") = iMar
                    If iApr <> 0 Then rw("Apr") = iApr
                    If iMay <> 0 Then rw("May") = iMay
                    If iJun <> 0 Then rw("Jun") = iJun
                    If iJul <> 0 Then rw("Jul") = iJul
                    If iAug <> 0 Then rw("Aug") = iAug
                    If iSep <> 0 Then rw("Sep") = iSep
                    If iOct <> 0 Then rw("Oct") = iOct
                    If iNov <> 0 Then rw("Nov") = iNov
                    If iDec <> 0 Then rw("Dec") = iDec
                    If iTotal <> 0 Then rw("Total") = iTotal
                    dtCGTotals.Rows.Add(rw)

                    iJan = 0
                    iFeb = 0
                    iMar = 0
                    iApr = 0
                    iMay = 0
                    iJun = 0
                    iJul = 0
                    iAug = 0
                    iSep = 0
                    iOct = 0
                    iNov = 0
                    iDec = 0
                    iTotal = 0
                Next
            End If
        Next

        Dim dt As DataTable = dtCGTotals.Copy
        For Each dr As DataRow In dt.Rows

            i = dtCGTotals.Compute("COUNT(CampGrpIdStr)", _
                                    "CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                    "AND LeadStatusOrCriteria = " & (LeadStatus.Enrolled + 1))

            If i = 0 Then
                arrRows = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.NewLead, _
                                        "CampGrpIdStr")

                arrRw = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.IScheduled, _
                                        "CampGrpIdStr")

                arrRw2 = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.Interviewed, _
                                        "CampGrpIdStr")

                arrRw3 = dt.Select("CampGrpIdStr = '" & dr("CampGrpIdStr") & "' " & _
                                        "AND LeadStatusOrCriteria = " & LeadStatus.Enrolled, _
                                        "CampGrpIdStr")

                ' NL to IS
                If arrRows.GetLength(0) > 0 And arrRows.GetLength(0) = arrRw.GetLength(0) Then
                    i = 0
                    For Each row In arrRows
                        rw = dtCGTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 1
                        rw("Descrip") = "NL / IS"
                        ComputePercentage(arrRw(i), row, rw)
                        dtCGTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'IS to I
                If arrRw2.GetLength(0) > 0 And arrRw2.GetLength(0) = arrRw.GetLength(0) Then
                    i = 0
                    For Each row In arrRw
                        rw = dtCGTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 2
                        rw("Descrip") = "IS / I"
                        ComputePercentage(arrRw2(i), row, rw)
                        dtCGTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'I to E
                If arrRw.GetLength(0) > 0 And arrRw2.GetLength(0) = arrRw3.GetLength(0) Then
                    i = 0
                    For Each row In arrRw
                        rw = dtCGTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 3
                        rw("Descrip") = "I / E"
                        ComputePercentage(arrRw3(i), row, rw)
                        dtCGTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
                'NL to E
                If arrRows.GetLength(0) > 0 And arrRows.GetLength(0) = arrRw3.GetLength(0) Then
                    i = 0
                    For Each row In arrRows
                        rw = dtCGTotals.NewRow
                        rw("CampGrpIdStr") = row("CampGrpIdStr")
                        rw("CampGrpDescrip") = row("CampGrpDescrip")
                        rw("LeadStatusOrCriteria") = LeadStatus.Enrolled + 4
                        rw("Descrip") = "NL / E"
                        ComputePercentage(arrRw3(i), row, rw)
                        dtCGTotals.Rows.Add(rw)
                        i += 1
                    Next
                End If
            End If
        Next

        'Change LeadStatusOrCriteria, so the Enrolled status is listed after Others.
        AlterViewOrder(dtCGTotals)
    End Sub

    Private Sub AlterViewOrder(ByVal dt As DataTable)
        'Change LeadStatusOrCriteria, so the Enrolled status is listed after Others.
        For Each dr As DataRow In dt.Rows
            If dr("LeadStatusOrCriteria") = 4 Then
                dr("LeadStatusOrCriteria") *= 10
            End If
        Next
    End Sub



    Private Function ComputeChanges_revised(ByVal arrRows() As DataRow, ByVal refMonth As Integer) As ArrayList
        Dim matrix(40, 20, 20) As Integer
        Dim i, j, k As Integer
        Dim arrList As New ArrayList

        'Initialize matrix with zeros
        For k = 0 To 40
            For i = 0 To 20
                For j = 0 To 20
                    matrix(k, i, j) = 0
                Next
            Next
        Next

        Dim skip As Boolean

        If arrRows.GetLength(0) > 0 Then
            For Each row As DataRow In arrRows
                skip = False
                If row.IsNull("SysOrigStatusId") Then
                    i = LeadStatus.IsNull
                Else
                    If row("SysOrigStatusId") = row("SysNewStatusId") Then
                        skip = True
                    End If
                End If
                'ignore rows with original status equal to new status, because there was no change.
                If Not skip Then
                    If Not row.IsNull("SysNewStatusId") Then
                        Select Case Integer.Parse(row("SysNewStatusId"))
                            Case 1
                                j = LeadStatus.NewLead
                                If Not row.IsNull("SysOrigStatusId") Then
                                    Select Case Integer.Parse(row("SysOrigStatusId"))
                                        Case 4
                                            i = LeadStatus.IScheduled
                                        Case 5
                                            i = LeadStatus.Interviewed
                                        Case 6
                                            i = LeadStatus.Enrolled
                                            ''Added by Saraswathi to show the counts for all the statuses
                                        Case 2
                                            i = LeadStatus.ApplicationReceived
                                        Case 3
                                            i = LeadStatus.ApplicationNotAccepted
                                        Case 17
                                            i = LeadStatus.DeadLead
                                        Case 18
                                            i = LeadStatus.WillEnrollinFuture

                                        Case Else
                                            i = LeadStatus.Others
                                    End Select
                                End If

                            Case 4
                                j = LeadStatus.IScheduled
                                If Not row.IsNull("SysOrigStatusId") Then
                                    Select Case Integer.Parse(row("SysOrigStatusId"))
                                        Case 1
                                            i = LeadStatus.NewLead
                                        Case 5
                                            i = LeadStatus.Interviewed
                                        Case 6
                                            i = LeadStatus.Enrolled
                                            ''Added by Saraswathi to show the counts for all the statuses
                                        Case 2
                                            i = LeadStatus.ApplicationReceived
                                        Case 3
                                            i = LeadStatus.ApplicationNotAccepted
                                        Case 17
                                            i = LeadStatus.DeadLead
                                        Case 18
                                            i = LeadStatus.WillEnrollinFuture
                                        Case Else
                                            i = LeadStatus.Others
                                    End Select
                                End If

                            Case 5
                                j = LeadStatus.Interviewed
                                If Not row.IsNull("SysOrigStatusId") Then
                                    Select Case Integer.Parse(row("SysOrigStatusId"))
                                        Case 1
                                            i = LeadStatus.NewLead
                                        Case 4
                                            i = LeadStatus.IScheduled
                                        Case 6
                                            i = LeadStatus.Enrolled
                                            ''Added by Saraswathi to show the counts for all the statuses
                                        Case 2
                                            i = LeadStatus.ApplicationReceived
                                        Case 3
                                            i = LeadStatus.ApplicationNotAccepted
                                        Case 17
                                            i = LeadStatus.DeadLead
                                        Case 18
                                            i = LeadStatus.WillEnrollinFuture
                                        Case Else
                                            i = LeadStatus.Others
                                    End Select
                                End If

                            Case 6
                                j = LeadStatus.Enrolled
                                If Not row.IsNull("SysOrigStatusId") Then
                                    Select Case Integer.Parse(row("SysOrigStatusId"))
                                        Case 1
                                            i = LeadStatus.NewLead
                                        Case 4
                                            i = LeadStatus.IScheduled
                                        Case 5
                                            i = LeadStatus.Interviewed
                                            ''Added by Saraswathi to show the counts for all the statuses
                                        Case 2
                                            i = LeadStatus.ApplicationReceived
                                        Case 3
                                            i = LeadStatus.ApplicationNotAccepted
                                        Case 17
                                            i = LeadStatus.DeadLead
                                        Case 18
                                            i = LeadStatus.WillEnrollinFuture
                                        Case Else
                                            i = LeadStatus.Others
                                    End Select
                                End If

                            Case 2
                                j = LeadStatus.ApplicationReceived
                                If Not row.IsNull("SysOrigStatusId") Then
                                    Select Case Integer.Parse(row("SysOrigStatusId"))
                                        Case 1
                                            i = LeadStatus.NewLead
                                        Case 4
                                            i = LeadStatus.IScheduled
                                        Case 5
                                            i = LeadStatus.Interviewed

                                            ''Added by Saraswathi to show the counts for all the statuses
                                        Case 6
                                            i = LeadStatus.Enrolled
                                        Case 3
                                            i = LeadStatus.ApplicationNotAccepted
                                        Case 17
                                            i = LeadStatus.DeadLead
                                        Case 18
                                            i = LeadStatus.WillEnrollinFuture

                                        Case Else
                                            i = LeadStatus.Others
                                    End Select
                                End If

                            Case 3
                                j = LeadStatus.ApplicationNotAccepted
                                If Not row.IsNull("SysOrigStatusId") Then
                                    Select Case Integer.Parse(row("SysOrigStatusId"))
                                        Case 1
                                            i = LeadStatus.NewLead
                                        Case 4
                                            i = LeadStatus.IScheduled
                                        Case 5
                                            i = LeadStatus.Interviewed

                                            ''Added by Saraswathi to show the counts for all the statuses
                                        Case 6
                                            i = LeadStatus.Enrolled
                                        Case 2
                                            i = LeadStatus.ApplicationReceived
                                        Case 17
                                            i = LeadStatus.DeadLead
                                        Case 18
                                            i = LeadStatus.WillEnrollinFuture

                                        Case Else
                                            i = LeadStatus.Others
                                    End Select
                                End If

                            Case 17
                                j = LeadStatus.DeadLead
                                If Not row.IsNull("SysOrigStatusId") Then
                                    Select Case Integer.Parse(row("SysOrigStatusId"))
                                        Case 1
                                            i = LeadStatus.NewLead
                                        Case 4
                                            i = LeadStatus.IScheduled
                                        Case 5
                                            i = LeadStatus.Interviewed

                                            ''Added by Saraswathi to show the counts for all the statuses
                                        Case 6
                                            i = LeadStatus.Enrolled
                                        Case 3
                                            i = LeadStatus.ApplicationNotAccepted
                                        Case 2
                                            i = LeadStatus.ApplicationReceived
                                        Case 18
                                            i = LeadStatus.WillEnrollinFuture

                                        Case Else
                                            i = LeadStatus.Others
                                    End Select
                                End If
                            Case 18
                                j = LeadStatus.WillEnrollinFuture
                                If Not row.IsNull("SysOrigStatusId") Then
                                    Select Case Integer.Parse(row("SysOrigStatusId"))
                                        Case 1
                                            i = LeadStatus.NewLead
                                        Case 4
                                            i = LeadStatus.IScheduled
                                        Case 5
                                            i = LeadStatus.Interviewed

                                            ''Added by Saraswathi to show the counts for all the statuses
                                        Case 6
                                            i = LeadStatus.Enrolled
                                        Case 3
                                            i = LeadStatus.ApplicationNotAccepted
                                        Case 17
                                            i = LeadStatus.DeadLead
                                        Case 2
                                            i = LeadStatus.ApplicationReceived

                                        Case Else
                                            i = LeadStatus.Others
                                    End Select
                                End If
                            Case Else
                                j = LeadStatus.Others
                                If Not row.IsNull("SysOrigStatusId") Then
                                    Select Case Integer.Parse(row("SysOrigStatusId"))
                                        Case 1
                                            i = LeadStatus.NewLead
                                        Case 4
                                            i = LeadStatus.IScheduled
                                        Case 5
                                            i = LeadStatus.Interviewed
                                            ''Added by Saraswathi to show the counts for all the statuses
                                        Case 2
                                            i = LeadStatus.ApplicationReceived
                                        Case 3
                                            i = LeadStatus.ApplicationNotAccepted
                                        Case 17
                                            i = LeadStatus.DeadLead
                                        Case 18
                                            i = LeadStatus.WillEnrollinFuture
                                        Case Else
                                            i = LeadStatus.Others
                                    End Select
                                End If
                        End Select
                    End If

                    If row.IsNull("SysOrigStatusId") Then
                        'When SysOrigStatusId is NULL, IsReversal is also NULL
                        matrix(refMonth, i, j) += 1
                    Else
                        If row.IsNull("IsReversal") Then
                            matrix(refMonth, j, i) -= 1
                        Else
                            matrix(refMonth, i, j) += 1
                        End If
                    End If
                End If
            Next

            Dim ctrNL As Integer = 0
            Dim ctrIS As Integer = 0
            Dim ctrI As Integer = 0
            Dim ctrO As Integer = 0
            Dim ctrE As Integer = 0
            Dim ctrAplR As Integer = 0
            Dim ctrAplNA As Integer = 0
            Dim ctrDL As Integer = 0
            Dim ctrWEiF As Integer = 0



            'Compute totals
            For k = 0 To 20
                ctrNL += matrix(refMonth, k, LeadStatus.NewLead)
                ctrIS += matrix(refMonth, k, LeadStatus.IScheduled)
                ctrI += matrix(refMonth, k, LeadStatus.Interviewed)
                ctrO += matrix(refMonth, k, LeadStatus.Others)
                ctrE += matrix(refMonth, k, LeadStatus.Enrolled)

                ctrAplR += matrix(refMonth, k, LeadStatus.ApplicationReceived)
                ctrAplNA += matrix(refMonth, k, LeadStatus.ApplicationNotAccepted)
                ctrDL += matrix(refMonth, k, LeadStatus.DeadLead)
                ctrWEiF += matrix(refMonth, k, LeadStatus.WillEnrollinFuture)
            Next

            arrList.Add(ctrNL)
            arrList.Add(ctrIS)
            arrList.Add(ctrI)
            arrList.Add(ctrO)
            arrList.Add(ctrE)

            arrList.Add(ctrAplR)
            arrList.Add(ctrAplNA)
            arrList.Add(ctrDL)
            arrList.Add(ctrWEiF)

        End If

        Return arrList
    End Function


#End Region

End Class
