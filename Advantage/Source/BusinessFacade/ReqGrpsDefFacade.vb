Public Class ReqGrpsDefFacade
    Public Function GetAvailSelectedCourses(ByVal CourseId As String) As DataSet

        '   Instantiate DAL component
        Dim DB As New ReqGrpsDefDB
        Dim ds As New DataSet
        Dim tbl1 As New DataTable
        Dim tbl2 As New DataTable

        '   get the dataset with all degrees          
        ds = DB.GetAvailSelectedCourses(CourseId)

        tbl1 = ds.Tables("AvailCourses")
        tbl2 = ds.Tables("SelectedCourses")

        'Make the ChildId column the primary key
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("adReqId")}
        End With
        With tbl2
            .PrimaryKey = New DataColumn() {.Columns("adReqId")}
        End With

        Return ds

    End Function
    Public Function GetMandatoryReqs() As DataSet

        '   Instantiate DAL component
        Dim DB As New ReqGrpsDefDB
        Dim ds As New DataSet
        Dim tbl1 As New DataTable
        Dim tbl2 As New DataTable

        '   get the dataset with all degrees          
        ds = DB.GetMandatoryReqs()

        Return ds

    End Function
    Public Function GetReqLeadGrps(ByVal ReqId As String) As DataSet
        Dim DB As New ReqGrpsDefDB


        Return DB.GetReqLeadGroups(ReqId)


    End Function
    Public Function GetReqsForLeadGrps(ByVal LeadGrpId As String) As DataSet
        Dim db As New ReqGrpsDefDB

        Return db.GetReqsForLeadGrps(LeadGrpId)
    End Function
    Public Function AssignReqsToReqGrp(ByVal ChildInfoObj As ChildInfo, ByVal ID As String, ByVal user As String)
        Dim PrgDB As New ReqGrpsDefDB

        PrgDB.AssignReqsToReqGrp(ChildInfoObj, ID, user)
    End Function

    Public Function DeleteReqsFrmReqGrp(ByVal ChildInfoObj As ChildInfo, ByVal ID As String) As String
        Dim PrgDB As New ReqGrpsDefDB

        Return PrgDB.DeleteReqsFrmReqGrp(ChildInfoObj, ID)

    End Function
    Public Function UpdateReqGrpDef(ByVal ChildInfoObj As ChildInfo, ByVal ID As String, ByVal user As String)
        Dim PrgDB As New ReqGrpsDefDB

        PrgDB.UpdateReqGrpDef(ChildInfoObj, ID, user)
    End Function
End Class
