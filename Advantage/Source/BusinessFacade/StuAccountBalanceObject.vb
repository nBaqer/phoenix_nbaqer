Public Class StuAccountBalanceObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim stuAccountBal As New StuAccountBalanceDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by the StudentAccountBalance report.
        If rptParamInfo.ResId = 275 Then
            ds = BuildReportSource(stuAccountBal.GetStuAccountBalance(rptParamInfo), stuAccountBal.StudentIdentifier)
        ElseIf rptParamInfo.ResId = 516 Then
            ds = BuildReportSource(stuAccountBal.GetStuAccountBalance(rptParamInfo, True), stuAccountBal.StudentIdentifier)
        ElseIf rptParamInfo.ResId = 513 Then
            ds = BuildReportSource(stuAccountBal.GetStuAccountBalanceMultipleEnrollments(rptParamInfo), stuAccountBal.StudentIdentifier)
        End If
        ' The StudentAccountBalance report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim stuName As String
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim strSSNMask As String

        'Get the mask for phone number and SSN
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count = 3 Then

                Dim dtStuAccBal As DataTable = ds.Tables("StuAccountBalance")
                Dim dtCampusTtl As DataTable = ds.Tables("CampusTotals")
                Dim dtCampGrpTtl As DataTable = ds.Tables("CampusGroupTotals")
                Dim rowC As DataRow
                Dim rowCG As DataRow
                Dim oldCmpGrp As String
                Dim oldCampus As String

                Dim i As Integer
                For i = 0 To dtStuAccBal.Rows.Count - 1
                    Dim dr As DataRow = dtStuAccBal.Rows(i)
                    'For Each dr As DataRow In dtStuAccBal.Rows

                    Try


                        dr("AccountCount") = dtStuAccBal.Rows.Count

                        'Compute Totals
                        'Get campus group and campus
                        If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpDescrip") Then
                            'Compute totals for campus group
                            rowCG = dtCampGrpTtl.NewRow
                            rowCG("CampGrpDescrip") = dr("CampGrpDescrip")
                            rowCG("CGBalance") = dtStuAccBal.Compute("SUM(Balance)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                            dtCampGrpTtl.Rows.Add(rowCG)
                            oldCmpGrp = dr("CampGrpDescrip")
                            oldCampus = ""
                        End If

                        If oldCmpGrp = dr("CampGrpDescrip") And oldCampus = "" Then
                            'Compute totals for campus
                            rowC = dtCampusTtl.NewRow
                            rowC("CampGrpDescrip") = oldCmpGrp
                            rowC("CampusDescrip") = dr("CampDescrip")
                            rowC("CBalance") = dtStuAccBal.Compute("SUM(Balance)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                            "CampusId='" & dr("CampusId").ToString & "'")
                            dtCampusTtl.Rows.Add(rowC)
                            oldCampus = dr("CampDescrip")
                            '
                        ElseIf oldCmpGrp = dr("CampGrpDescrip") And oldCampus <> dr("CampDescrip") Then
                            'Compute totals for campus
                            rowC = dtCampusTtl.NewRow
                            rowC("CampGrpDescrip") = oldCmpGrp
                            rowC("CampusDescrip") = dr("CampDescrip")
                            rowC("CBalance") = dtStuAccBal.Compute("SUM(Balance)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                            "CampusId='" & dr("CampusId").ToString & "'")
                            dtCampusTtl.Rows.Add(rowC)
                            oldCampus = dr("CampDescrip")
                        End If

                        'Set up student name as: "LastName, FirstName MI."
                        stuName = dr("LastName")
                        If Not dr.IsNull("FirstName") Then
                            If dr("FirstName") <> "" Then
                                stuName &= ", " & dr("FirstName")
                            End If
                        End If
                        If Not dr.IsNull("MiddleName") Then
                            If dr("MiddleName") <> "" Then
                                stuName &= " " & dr("MiddleName") & "."
                            End If
                        End If
                        dr("StudentName") = stuName
                        '
                        'Apply mask to Phone.
                        If Not dr.IsNull("Phone") Then
                            If Not dr.IsNull("ForeignPhone") Then
                                If dr("Phone") <> "" And dr("ForeignPhone") = False Then
                                    'Domestic Phone
                                    If (dr("Phone").ToString().Length > 9) Then
                                        dr("Phone") = facInputMasks.ApplyMask(strMask, dr("Phone"))

                                    End If
                                End If
                            Else
                                If dr("Phone") <> "" Then
                                    'Domestic Phone
                                    If (dr("Phone").ToString().Length > 9) Then
                                        dr("Phone") = facInputMasks.ApplyMask(strMask, dr("Phone"))
                                    End If
                                End If
                                End If
                        End If
                        '
                        'Apply mask to SSN.
                        If StudentIdentifier = "SSN" Then
                            If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                                If dr("StudentIdentifier") <> "" Then
                                    Dim temp As String = dr("StudentIdentifier")
                                    dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        Dim s As Integer = i
                    End Try
                Next
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region

End Class
