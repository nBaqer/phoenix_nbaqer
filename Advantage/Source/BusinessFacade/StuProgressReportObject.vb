Imports FAME.AdvantageV1.BusinessFacade.AR
Imports FAME.AdvantageV1.Common.AR
Imports FAME.Advantage.Common

Public Class StuProgressReportObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim ds As  DataSet
        Dim stuPr As New StuProgressReportDB

        ' Massage dataset to produce one with the columns and data expected by the StudentTranscript report. 
        ds = BuildReportSource(stuPr.GetEnrollment(paramInfo), stuPr.StudentIdentifier, stuPr.ShowProgramOnTranscript, paramInfo.CampusId, paramInfo)
        ' The StudentTranscript report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds

    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal EnrollDataSet As DataSet, ByVal StudentIdentifier As String, ByVal showProg As Boolean, Optional ByVal campusId As String = "", Optional ByVal paraminfo As ReportParamInfo = Nothing) As DataSet


        Try
            If EnrollDataSet.Tables.Count > 0 Then

                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                Dim m_studentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
                Dim facInputMasks As New InputMasksFacade
                Dim strSSNMask As String
                strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
                Dim dtGeneralInfo As DataTable = EnrollDataSet.Tables("GeneralInfo")
                Dim dt As DataTable = EnrollDataSet.Tables("SummaryInfo")
                Dim corpInfo As CorporateInfo = GetCorpInfo(campusId)
                Dim streetAddress As String = ""
                Dim FullAddress As String = ""
                Dim cityStateZip As String = ""
                Dim zipMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
                With corpInfo
                    streetAddress = .Address1
                    streetAddress &= " " & .Address2
                    FullAddress = streetAddress
                    cityStateZip = .City
                    If .State <> "" Then
                        If cityStateZip <> "" Then
                            cityStateZip &= ", " & .State
                        Else
                            cityStateZip = .State
                        End If
                    End If
                    If .Zip <> "" Then
                        If cityStateZip <> "" Then
                            cityStateZip &= " " & facInputMasks.ApplyMask(zipMask, .Zip)
                        Else
                            cityStateZip = facInputMasks.ApplyMask(zipMask, .Zip)
                        End If
                    End If
                    If cityStateZip <> "" Then
                        If FullAddress <> "" Then
                            FullAddress &= Chr(10) & cityStateZip
                        Else
                            FullAddress = cityStateZip
                        End If
                    End If
                End With

                For Each iRow As DataRow In dt.Rows
                    iRow("CorporateName") = corpInfo.CorporateName
                    iRow("FullAddress") = FullAddress
                    If corpInfo.Phone <> "" Then
                        iRow("Phone") = "Phone: " & corpInfo.Phone
                    End If
                    If corpInfo.Fax <> "" Then
                        iRow("Fax") = "Fax: " & corpInfo.Fax
                    End If
                    iRow("Website") = corpInfo.Website
                    If StudentIdentifier = "SSN" Then
                        If Not (iRow("StudentNumber") Is System.DBNull.Value) Then
                            If iRow("StudentNumber") <> "" Then
                                Dim temp As String = iRow("StudentNumber")
                                iRow("StudentNumber") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                            End If
                        End If
                    End If
                Next

                Dim stuPR As New StuProgressReportDB
                Dim br As New AttendancePercentageBR
                Dim dtWU As New DataTable
                dtWU.Columns.Add(New DataColumn("ReqId", System.Type.GetType("System.String")))
                dtWU.Columns.Add(New DataColumn("TermId", System.Type.GetType("System.String")))
                dtWU.Columns.Add(New DataColumn("Descrip", System.Type.GetType("System.String")))
                dtWU.Columns.Add(New DataColumn("Score", System.Type.GetType("System.Decimal")))
                dtWU.Columns.Add(New DataColumn("MinResult", System.Type.GetType("System.Decimal")))
                dtWU.Columns.Add(New DataColumn("Required", System.Type.GetType("System.Boolean")))
                dtWU.Columns.Add(New DataColumn("Remaining", System.Type.GetType("System.Decimal")))
                dtWU.Columns.Add(New DataColumn("MustPass", System.Type.GetType("System.Boolean")))
                dtWU.Columns.Add(New DataColumn("WorkUnitType", System.Type.GetType("System.Int32")))
                dtWU.Columns.Add(New DataColumn("WorkUnitDescrip", System.Type.GetType("System.String")))
                dtWU.Columns.Add(New DataColumn("stuEnrollid", System.Type.GetType("System.String")))
                Dim ds As DataSet
                Dim ScheduledHours As Decimal = 0.0
                Dim CompletedHours As Decimal = 0.0
                Dim Attendance As Decimal = 0.0
                Dim GradesFormat As String = MyAdvAppSettings.AppSettings("GradesFormat", campusId).ToLower()
                'Get the data for GeneralInfo including the student groups
                Dim dtGI As New DataTable

                Dim dr As DataRow
                Dim studentGrps As String
                Dim attInfo As New ClockHourAttendanceInfo
                Dim attInfo_nonRoss As Common.AttendancePercentageInfo
                dtGI = stuPR.GetGeneralInfo(paraminfo)
                studentGrps = GetStudentGroups(paraminfo)
                Dim strEnrollId As String = GetStuEnrollId(paraminfo)
                'Dim cutOffDate As Date = Date.Parse(stuPR.GetCutOffDate(paraminfo))
                Dim cutOffDate As Date = Date.Parse(paraminfo.CutoffDate)
                Dim arrWU() As DataRow
                Dim rw As DataRow
                Dim attendanceType As String = "minutes"
                'If SingletonAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                If MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToLower = "byclass" Then
                    ScheduledHours = 0.0
                    CompletedHours = 0.0
                    Attendance = 0.0
                    Dim attTypeObj As Object = (New ClsSectAttendanceDB).GetPrgVersionAttendanceType(strEnrollId, False)
                    If Not attTypeObj Is Nothing Then attendanceType = attTypeObj.ToString.ToLower()
                    If MyAdvAppSettings.AppSettings("DisplayAttendanceUnitForProgressReportByClass").ToLower = "hours" And attendanceType = "present absent" Then

                        attInfo_nonRoss = br.GetPercentageAttendanceInfoForReport(strEnrollId, cutOffDate)
                        ScheduledHours = (attInfo_nonRoss.TotalPresent + attInfo_nonRoss.TotalAbsent + attInfo_nonRoss.TotalExcused + attInfo_nonRoss.TotalTardies) / 60
                        CompletedHours = (attInfo_nonRoss.TotalPresent + attInfo_nonRoss.TotalTardies) / 60
                        If ScheduledHours > 0 Then
                            Attendance = Math.Round(CompletedHours * 100 / ScheduledHours, 2)
                        End If
                        attInfo.TotalHoursPresent = CompletedHours
                        attInfo.TotalHoursSched = ScheduledHours
                        attInfo.TotalHoursAbsent = ScheduledHours - CompletedHours
                    Else
                        attInfo_nonRoss = br.GetPercentageAttendanceInfo(strEnrollId, cutOffDate)
                        If attInfo_nonRoss.TardiesMakingAbsence = 0 Then
                            ScheduledHours = attInfo_nonRoss.TotalPresent + attInfo_nonRoss.TotalAbsent + attInfo_nonRoss.TotalExcused + attInfo_nonRoss.TotalTardies
                            CompletedHours = attInfo_nonRoss.TotalPresent + attInfo_nonRoss.TotalTardies
                            If ScheduledHours > 0 Then Attendance = CompletedHours / ScheduledHours * 100
                        Else
                            ScheduledHours = attInfo_nonRoss.TotalPresentAdjusted + attInfo_nonRoss.TotalAbsentAdjusted + attInfo_nonRoss.TotalTardiesAdjusted
                            CompletedHours = attInfo_nonRoss.TotalPresentAdjusted + attInfo_nonRoss.TotalTardiesAdjusted
                            Attendance = attInfo_nonRoss.PercentageAttendance
                        End If



                        If attendanceType = "minutes" Then
                            If CompletedHours > 0 Then attInfo.TotalHoursPresent = CompletedHours / 60
                            If ScheduledHours > 0 Then attInfo.TotalHoursSched = ScheduledHours / 60
                            attInfo.TotalHoursAbsent = attInfo.TotalHoursSched - attInfo.TotalHoursPresent

                        Else
                            'ScheduledHours = attInfo_nonRoss.TotalPresentAdjusted + attInfo_nonRoss.TotalAbsentAdjusted + attInfo_nonRoss.TotalTardiesAdjusted
                            'CompletedHours = attInfo_nonRoss.TotalPresentAdjusted + attInfo_nonRoss.TotalTardiesAdjusted
                            attInfo.TotalHoursPresent = CompletedHours
                            attInfo.TotalHoursSched = ScheduledHours
                            attInfo.TotalHoursAbsent = ScheduledHours - CompletedHours
                        End If



                    End If
                Else

                    attInfo = (New AttendanceFacade).GetAttendanceSummaryForCutOffDate(strEnrollId, cutOffDate)
                    Attendance = attInfo.AttendancePercentage
                End If

                dr = dtGeneralInfo.NewRow
                dr("StuEnrollId") = dtGI.Rows(0)("StuEnrollId")
                dr("StartDate") = dtGI.Rows(0)("StartDate")
                If MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToLower = "byclass" Then
                    attInfo.LDA = (New AttendanceDB).GetLDAForTermCutOffDate(strEnrollId, cutOffDate)
                End If

                If attInfo.LDA = Date.MinValue Then
                    If dt.Rows(0).IsNull("LDA") Then
                        dr("LastDateAttended") = "1/1/1900"
                    Else
                        If dt.Rows(0)("LDA") > cutOffDate Then
                            dr("LastDateAttended") = "1/1/1900"
                        Else
                            dr("LastDateAttended") = dt.Rows(0)("LDA")
                        End If

                    End If
                Else
                    dr("LastDateAttended") = attInfo.LDA
                End If



                dr("ExpectedGradDate") = dtGI.Rows(0)("ExpGradDate")
                dr("TotalCost") = dtGI.Rows(0)("TotalCost")
                dr("StudentGroups") = studentGrps
                dr("CurrentBalance") = dtGI.Rows(0)("CurrentBalance")
                'Attendance info 
                dr("TotalDaysAttended") = attInfo.TotalHoursPresent
                dr("ScheduledDays") = attInfo.TotalHoursSched
                dr("DaysAbsent") = attInfo.TotalHoursAbsent
                dr("MakeupDays") = attInfo.TotalMakeUpHours
                If attInfo.TotalHoursSched = 0 Then
                    dr("WeeklySchedHours") = (New AttendanceFacade).GetScheduledHoursByWeek(strEnrollId)
                Else
                    dr("WeeklySchedHours") = attInfo.TotalSchedHoursByWeek
                End If

                dr("AttendanceType") = MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToLower.ToString()
                dr("Attendance") = Attendance
                dr("RossType") = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower.ToString()
                Dim strArr() As String
                strArr = paraminfo.FilterList.ToLower().Split(",")
                dr("ShowWorkUnits") = strArr(0)
                If strArr(0) = "false" And strArr(4) = String.Empty Then
                    dr("ShowGroupWorkUnits") = "false"
                End If

                dr("ShowStudentsignature") = strArr(1)
                dr("ShowSchoolsignature") = strArr(2)
                dr("ShowTermOrModule") = strArr(3)
                dr("GradeType") = GradesFormat
                dr("ShowCurrentBalance") = paraminfo.ShowCurrentBalance.ToString.ToLower
                dr("ShowTotalCost") = paraminfo.ShowTotalCost.ToString.ToLower
                If attendanceType = "minutes" Then
                    dr("ByClassAttendanceType") = "hours"
                Else
                    dr("ByClassAttendanceType") = MyAdvAppSettings.AppSettings("DisplayAttendanceUnitForProgressReportByClass").ToLower.ToString()
                End If
                'If (New ProgVerDB).IsClockHourSchool Then
                '    dr("ClockHourSchool") = "yes"
                'Else
                '    dr("ClockHourSchool") = "no"
                'End If
                If (New ProgVerDB).IsClockHourProgramVersion(strEnrollId) Then
                    dr("ClockHourSchool") = "yes"
                Else
                    dr("ClockHourSchool") = "no"
                End If


                'Add the row
                dtGeneralInfo.Rows.Add(dr)

                'Get the WorkUnitResults info

                dtWU = (stuPR.GetWorkUnitResultsForProgressDisplay(paraminfo).Copy)



                'Get the ModuleCoursesResults info
                Dim dtMCR As New DataTable

                dtMCR = stuPR.GetModulesCoursesResults(paraminfo).Copy

                'Update dtMCR to include the Completed and Credits Earned for each course
                If GradesFormat = "numeric" Then
                    UpdateCompletedAndCreditsEarned(dtMCR, dtWU, campusId, paraminfo.CutoffDate)
                Else
                    UpdateCompletedAndCreditsEarnedForLetterGrades(dtMCR, dtWU, campusId, paraminfo.CutoffDate)
                End If

                If strArr(0) = "false" Then
                    'dtWU = (stuPR.GetWorkUnitResults(paraminfo).Clone)
                    If strArr(4) <> String.Empty Then

                        Dim strGroupFilter As String = strArr(4).Replace("|", ",")
                        If strGroupFilter.EndsWith(",") Then strGroupFilter = strGroupFilter.Substring(0, strGroupFilter.Length - 1)

                        ' arrWU = (stuPR.GetWorkUnitResults(paraminfo).Copy).Select("WorkUnitType in (" & strGroupFilter & ")")
                        'GetWorkUnitResultsForProgressDisplay
                        'arrWU = (stuPR.GetWorkUnitResultsForProgressDisplay(paraminfo).Copy).Select("WorkUnitType in (" & strGroupFilter & ")")
                        Dim dtWUcloneTable As DataTable
                        dtWUcloneTable = dtWU.Clone()

                        arrWU = dtWU.Select("WorkUnitType in (" & strGroupFilter & ")")
                        For i As Integer = 0 To arrWU.Length - 1

                            rw = dtWUcloneTable.NewRow
                            rw("ReqId") = arrWU(i)("ReqId")
                            rw("TermId") = arrWU(i)("TermId")
                            rw("Descrip") = arrWU(i)("Descrip")
                            rw("Score") = arrWU(i)("Score")
                            rw("MinResult") = arrWU(i)("MinResult")
                            rw("Required") = arrWU(i)("Required")
                            rw("MustPass") = arrWU(i)("MustPass")
                            rw("Remaining") = arrWU(i)("Remaining")
                            rw("WorkUnitType") = arrWU(i)("WorkUnitType")
                            rw("WorkUnitDescrip") = arrWU(i)("WorkUnitDescrip")
                            rw("RossType") = MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower.ToString
                            dtWUcloneTable.Rows.Add(rw)
                        Next
                        EnrollDataSet.Tables.Add(dtWUcloneTable)
                        EnrollDataSet.Tables(2).TableName = "WorkUnitResults"
                    Else
                        EnrollDataSet.Tables.Add(dtWU)
                        EnrollDataSet.Tables(2).TableName = "WorkUnitResults"
                    End If

                Else
                    EnrollDataSet.Tables.Add(dtWU)
                    EnrollDataSet.Tables(2).TableName = "WorkUnitResults"
                End If


                'Add the ModuleCoursesResults info to EnrollDataSet
                EnrollDataSet.Tables.Add(dtMCR)
                EnrollDataSet.Tables(3).TableName = "ModuleCoursesResults"



            End If
        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return EnrollDataSet

    End Function

    Public Function UpdateCompletedAndCreditsEarned(ByRef dtMCR As DataTable, ByVal dtWU As DataTable, ByVal campusId As String, ByVal cutoffDate As DateTime)
        'For each row in the courses results table (dtMCR) we will check the
        'work unit results table (dtWU) to see if the student satisfied the
        'work unit for that term/module. Note that for schools like Ross we are 
        'using the same approach of Freedom to record the classes as well as 
        'work units with null results at the time of enrollment. This means that
        'dtWU should always contain all the relevant work units for a term/module.
        'If they have not been attempted they will simply have a null value in 
        'the score field.This is the same approach that we are taking for the courses
        'as well. They will have a null score in the arTransferGrades table or 
        'the arResults table.
        Dim wuSatisfied As Boolean
        Dim csrCompleted As Boolean
        Dim csrDescrip As String
        Dim wuDescrip As String
        Dim drMCR As DataRow
        Dim arrWU() As DataRow
        Dim arrWUAll() As DataRow
        Dim arrWUForLab() As DataRow
        Dim i As Integer
        Dim numWUAttempted As Integer
        Dim numWUNotAttempted As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim GradesFormat As String = MyAdvAppSettings.AppSettings("GradesFormat", CampusId).ToLower()
        'Add fields for Completed and CreditsEarned to dtMCR
        dtMCR.Columns.Add("Completed", Type.GetType("System.Boolean"))
        dtMCR.Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
        dtMCR.Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
        dtMCR.Columns.Add("FinAidCreditsEarned", Type.GetType("System.Decimal"))
        dtMCR.Columns.Add("TermGPA", Type.GetType("System.Decimal"))
        dtMCR.Columns.Add("CUMGPA", Type.GetType("System.Decimal"))
        dtMCR.Columns.Add("CurrentScore", Type.GetType("System.Decimal"))
        dtMCR.Columns.Add("CurrentGrade", Type.GetType("System.String"))
        'dtMCR.Columns.Add("Grade", Type.GetType("System.String"))


        For Each drMCR In dtMCR.Rows
            'Set the defaults for the new columns added
            drMCR("Completed") = False
            drMCR("CreditsEarned") = 0.0
            drMCR("CreditsAttempted") = 0.0
            drMCR("FinAidCreditsEarned") = 0.0
            csrDescrip = drMCR("Descrip")
            drMCR("CurrentScore") = drMCR("Score")
            drMCR("CurrentGrade") = drMCR("Grade")
            Dim boolIsCourseALab As Boolean = False
            boolIsCourseALab = (New TransferGradeDB).isCourseALabWorkOrLabHourCourse(drMCR("ReqId").ToString)
            'First: If there is no score for the course itself it normally means that the student
            'has not yet attempted the course.However, there are some courses such as MA110 Keyboarding where the course will have a null score
            'because the work units are only required to be attempted but not passed and so they are assign weights of 0. This means that the best
            'approach to take is to start with the work unit themselves. For credits attempted, if there is at least 1 work unit with a score then
            'it means that the course has been attempted. The course will be considered complete if all the work units have been satisfied and
            'the course score is >= min score.

            'Start out assuming that all the work units have been satisfied
            wuSatisfied = True
            numWUAttempted = 0
            numWUNotAttempted = 0
            csrCompleted = False
            'Check the work units that have been atempted.
            If GradesFormat = "numeric" Then
                arrWU = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "' AND Score >= 0")
                arrWUAll = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "'")
                arrWUForLab = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "' and workunitType in(500,503,544,533)")
                If arrWU.Length > 0 And arrWUAll.Length = arrWUForLab.Length Then
                    'wuSatisfied = ScoreIsAPass(arrWUForLab)
                    'If boolIsCourseALab = True Then
                    '    If arrWUAll.Length = arrWUForLab.Length Then
                    '        drMCR("CreditsAttempted") = drMCR("Credits")
                    '    Else
                    '        If Not drMCR.IsNull("IsCreditsAttempted") Then
                    '            If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
                    '        End If
                    '    End If
                    'ElseIf Not drMCR.IsNull("IsCreditsAttempted") Then
                    '    If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
                    'End If
                    drMCR("CreditsAttempted") = drMCR("Credits")
                    If MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                        csrCompleted = ScoreIsAPassNonRoss(arrWUForLab)
                    Else
                        csrCompleted = ScoreIsAPass(arrWUForLab)
                        If csrCompleted = False Then wuSatisfied = False
                    End If


                Else
                    If arrWU.Length > 0 Then
                        'If boolIsCourseALab = True Then
                        '    If arrWUAll.Length = arrWUForLab.Length Then
                        '        drMCR("CreditsAttempted") = drMCR("Credits")
                        '    Else
                        '        If Not drMCR.IsNull("IsCreditsAttempted") Then
                        '            If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
                        '        End If
                        '    End If
                        'ElseIf Not drMCR.IsNull("IsCreditsAttempted") Then
                        '    If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
                        'End If
                        drMCR("CreditsAttempted") = drMCR("Credits")

                        For i = 0 To arrWU.Length - 1
                            'If there are any work units where the score is less than the min result and 
                            'the student must pass it then they have not been satisfied.
                            Dim wuRequired As String
                            Dim wuMustPass As String
                            wuDescrip = arrWU(i)("Descrip")
                            If arrWU(i)("score") < arrWU(i)("MinResult") Then
                                'First check if it is required
                                wuRequired = arrWU(i)("Required").ToString
                                wuMustPass = arrWU(i)("MustPass")
                                If arrWU(i)("Required") = True And arrWU(i)("MustPass") = True Then
                                    wuSatisfied = False
                                    Exit For
                                End If

                            End If
                        Next
                    Else
                        'Set indicator that there were no attempted work units
                        numWUAttempted = 1

                    End If



                    'Check the work units that have not been attempted yet.
                    arrWU = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "' AND Score IS NULL")
                    If arrWU.Length > 0 Then
                        'We need to check each row to see if the work unit is first required. If it is
                        'required and the student has not yet attempted it then we can just say the
                        'Set indicator that there were no not attempted work units
                        numWUNotAttempted = 1
                        'student has not satisfied it.
                        For iRow As Integer = 0 To arrWU.Length - 1
                            If arrWU(iRow)("Required") = True Then
                                wuSatisfied = False
                                Exit For
                            End If

                        Next



                    End If

                    'If arrWU.Length > 0 Then
                    '    'We need to check each row to see if the work unit is first required. If it is
                    '    'required and the student has not yet attempted it then we can just say the
                    '    'student has not satisfied it.
                    '    For iRow As Integer = 0 To arrWU.Length - 1
                    '        If arrWU(iRow)("Required") = True Then
                    '            wuSatisfied = False
                    '            Exit For
                    '        End If

                    '    Next
                    'Else
                    '    'Set indicator that there were no not attempted work units
                    '    numWUNotAttempted = 1

                    'End If

                    'We have to check for an exception where the work units might not have been registered for the stuent
                    'when he/she was enrolled. When that is the case we would incorrectly end up with wuSatisfied being left
                    'as true
                    If numWUAttempted = 1 And numWUNotAttempted = 1 Then
                        wuSatisfied = False
                    End If
                End If
            End If
            If Not drMCR.IsNull("Score") And MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                wuSatisfied = True
                drMCR("CurrentScore") = drMCR("Score")
                drMCR("CreditsAttempted") = drMCR("Credits")
                'If boolIsCourseALab = True Then
                '    If arrWUAll.Length = arrWUForLab.Length Then
                '        drMCR("CreditsAttempted") = drMCR("Credits")
                '    Else
                '        If Not drMCR.IsNull("IsCreditsAttempted") Then
                '            If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
                '        End If
                '    End If
                'ElseIf Not drMCR.IsNull("IsCreditsAttempted") Then
                '    If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
                'End If
            End If


            'At this point if the work units have not been satisfied there is no need to do anything as the default settings above will take care of it.
            'However, if they have been satisfied we now have to look at whether the student has a passing score. Remember that there are some courses
            'where the passing score is a 0 and so the course might not have a score recorded on it but if all the work units have been satisfied then it
            'means that the course has been completed.

            'Assume that the course has not been completed

            Dim rtn As TransferGBWInfo
            If MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                If drMCR.IsNull("Score") And Not drMCR("clsSectionId").ToString = "00000000-0000-0000-0000-000000000000" Then

                    rtn = GetTransferGradesScore(drMCR("TermId").ToString, "CourseLevel", drMCR("clsSectionId").ToString, drMCR("StuEnrollId").ToString, campusId)
                    If Not rtn Is Nothing Then
                        If rtn.Score = -1 Then
                            drMCR("CurrentScore") = DBNull.Value
                        Else
                            drMCR("CurrentScore") = rtn.Score
                        End If
                    End If
                End If

                'drMCR("Score") = DBNull.Value
            End If
            If wuSatisfied Then
                'If the score is empty we can say right away that the course has been completed
                If (drMCR("Score").ToString = "" Or drMCR.IsNull("Score")) And drMCR("IsAttendanceOnly") = True Then
                    'If (drMCR("Score").ToString = "" Or drMCR.IsNull("Score")) Then
                    csrCompleted = True
                Else
                    'Check if the score is a passing score
                    If Not drMCR.IsNull("Score") Then
                        If ScoreIsAPass(drMCR("Score"), drMCR("clsSectionId").ToString) Then
                            csrCompleted = True
                        Else
                            csrCompleted = False
                        End If
                    End If
                End If
            End If
            'At this point if the course is completed we can set the relevant fields
            'If wuSatisfied Then
            '    drMCR("CreditsAttempted") = drMCR("Credits")
            'End If
            '
            '**********************************************************************************************************************************
            'the below code was added to calculate the credits depending upon the hours attempted,not just the course credits
            'this is done aonly if the addcreditsbyservice = 'yes' eg. Hair Dynamics 
            'if the course completed is false,then it has to calculate the credits attempted and earned using the add credits by service 
            'set in grade book weights for this course.This is applicable only for course having lab work or lab hours
            'mantis issue 16300
            'by Theresa G on 5/20/2009
            '**********************************************************************************************************************************



            Dim addcreditsbyservice As String = MyAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower
            If csrCompleted = False And addcreditsbyservice = "yes" Then
                If boolIsCourseALab = True Then
                    Dim decCredits As Decimal = 0.0
                    Try
                        decCredits = (New TranscriptDB).ComputeWithCreditsPerService(drMCR("StuEnrollId").ToString, addcreditsbyservice, drMCR("ReqId").ToString)
                    Catch ex As System.Exception
                        decCredits = 0
                    End Try
                    drMCR("CreditsAttempted") = decCredits
                    drMCR("CreditsEarned") = decCredits
                End If

            End If

            If csrCompleted = True Then
                If MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                    If ((New TranscriptDB).IsCourseCombinationandPass(drMCR("StuEnrollId").ToString, drMCR("clsSectionId").ToString)) Then
                        If boolIsCourseALab = True Then
                            If arrWU.Length > 0 And arrWUAll.Length = arrWUForLab.Length Then
                                drMCR("CreditsAttempted") = drMCR("Credits")
                                drMCR("CreditsEarned") = drMCR("Credits")
                                drMCR("FinAidCreditsEarned") = drMCR("FinAidCredits")
                            Else
                                'If Not drMCR.IsNull("IsCreditsAttempted") Then
                                '    If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
                                'End If
                                drMCR("CreditsAttempted") = drMCR("Credits")
                                If Not drMCR.IsNull("IsCreditsEarned") Then
                                    If drMCR("IsCreditsEarned") Then drMCR("CreditsEarned") = drMCR("Credits")
                                    If drMCR("IsCreditsEarned") Then drMCR("FinAidCreditsEarned") = drMCR("FinAidCredits")
                                End If
                            End If
                        Else
                            'If Not drMCR.IsNull("IsCreditsAttempted") Then
                            '    If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
                            'End If
                            drMCR("CreditsAttempted") = drMCR("Credits")
                            If Not drMCR.IsNull("IsCreditsEarned") Then
                                If drMCR("IsCreditsEarned") Then drMCR("CreditsEarned") = drMCR("Credits")
                                If drMCR("IsCreditsEarned") Then drMCR("FinAidCreditsEarned") = drMCR("FinAidCredits")
                            End If
                        End If

                        drMCR("Completed") = True
                    Else

                        drMCR("Completed") = True
                    End If
                Else
                    If boolIsCourseALab = True Then
                        If arrWU.Length > 0 And arrWUAll.Length = arrWUForLab.Length Then
                            drMCR("CreditsAttempted") = drMCR("Credits")
                            drMCR("CreditsEarned") = drMCR("Credits")
                            drMCR("FinAidCreditsEarned") = drMCR("FinAidCredits")
                        Else
                            'If Not drMCR.IsNull("IsCreditsAttempted") Then
                            '    If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
                            'End If
                            drMCR("CreditsAttempted") = drMCR("Credits")
                            If Not drMCR.IsNull("IsCreditsEarned") Then
                                If drMCR("IsCreditsEarned") Then drMCR("CreditsEarned") = drMCR("Credits")
                                If drMCR("IsCreditsEarned") Then drMCR("FinAidCreditsEarned") = drMCR("FinAidCredits")
                            End If
                        End If
                    Else
                        'If Not drMCR.IsNull("IsCreditsAttempted") Then
                        '    If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
                        'End If
                        drMCR("CreditsAttempted") = drMCR("Credits")
                        If Not drMCR.IsNull("IsCreditsEarned") Then
                            If drMCR("IsCreditsEarned") Then drMCR("CreditsEarned") = drMCR("Credits")
                            If drMCR("IsCreditsEarned") Then drMCR("FinAidCreditsEarned") = drMCR("FinAidCredits")
                        End If
                    End If
                    drMCR("Completed") = True

                End If


            End If
            If MyAdvAppSettings.AppSettings("GradeRounding").ToLower() = "yes" Then
                If Not drMCR("Score") Is System.DBNull.Value Then
                    drMCR("Score") = Math.Round(CType(drMCR("Score"), Decimal)).ToString
                End If
                If Not drMCR("CurrentScore") Is System.DBNull.Value Then
                    drMCR("CurrentScore") = Math.Round(CType(drMCR("CurrentScore"), Decimal)).ToString
                End If
            End If

            'added by Theresa G on 6/4/09 for mantis 
            If drMCR("clsSectionId").ToString = "00000000-0000-0000-0000-000000000000" Then
                If Not drMCR.IsNull("IsCreditsAttempted") Then
                    If drMCR("IsCreditsAttempted") Then
                        drMCR("CreditsAttempted") = drMCR("Credits")
                    Else
                        drMCR("CreditsAttempted") = 0
                    End If

                End If
            End If




        Next



    End Function
    Private Function getCUMGPA(ByVal dtTermProgress As DataTable, ByVal cutoffDate As DateTime) As Decimal()
        Dim rtnGPA(3) As Decimal
        Dim maxDate As Date
        Dim credits As Decimal = 0.0
        Dim FAcredits As Decimal = 0.0
        If cutoffDate = #1/1/1900# Then
            If dtTermProgress.Rows.Count > 0 Then
                maxDate = dtTermProgress.Rows(0)("StartDate")
                For i As Integer = 0 To dtTermProgress.Rows.Count - 1
                    If maxDate < dtTermProgress.Rows(i)("StartDate") Then
                        maxDate = dtTermProgress.Rows(i)("StartDate")
                        credits = credits + dtTermProgress.Rows(i)("CreditsEarned")
                        FAcredits = FAcredits + dtTermProgress.Rows(i)("FACreditsEarned")
                    End If
                Next

                For i As Integer = 0 To dtTermProgress.Rows.Count - 1
                    If dtTermProgress.Rows(i)("StartDate") = maxDate Then
                        rtnGPA(0) = dtTermProgress.Rows(i)("GPA")
                        rtnGPA(1) = credits
                        rtnGPA(2) = FAcredits
                        Return rtnGPA
                    End If
                Next
            End If
        Else
            If dtTermProgress.Rows.Count > 0 Then

                For i As Integer = 0 To dtTermProgress.Rows.Count - 1
                    If dtTermProgress.Rows(i)("StartDate") <= cutoffDate Then
                        maxDate = dtTermProgress.Rows(i)("StartDate")
                        credits = credits + dtTermProgress.Rows(i)("CreditsEarned")
                        FAcredits = FAcredits + dtTermProgress.Rows(i)("FACreditsEarned")
                    End If
                Next
                For i As Integer = 0 To dtTermProgress.Rows.Count - 1
                    If dtTermProgress.Rows(i)("StartDate") = maxDate Then
                        rtnGPA(0) = dtTermProgress.Rows(i)("GPA")
                        rtnGPA(1) = credits
                        rtnGPA(2) = FAcredits
                        Return rtnGPA
                    End If
                Next
            End If
        End If
        Return rtnGPA
    End Function



    Public Function UpdateCompletedAndCreditsEarnedForLetterGrades(ByRef dtMCR As DataTable, ByVal dtWU As DataTable, ByVal campusId As String, Optional ByVal cutoffDate As DateTime = #1/1/1900#)
        'For each row in the courses results table (dtMCR) we will check the
        'work unit results table (dtWU) to see if the student satisfied the
        'work unit for that term/module. Note that for schools like Ross we are 
        'using the same approach of Freedom to record the classes as well as 
        'work units with null results at the time of enrollment. This means that
        'dtWU should always contain all the relevant work units for a term/module.
        'If they have not been attempted they will simply have a null value in 
        'the score field.This is the same approach that we are taking for the courses
        'as well. They will have a null score in the arTransferGrades table or 
        'the arResults table.
        Dim wuSatisfied As Boolean
        Dim csrCompleted As Boolean
        Dim csrDescrip As String
        Dim wuDescrip As String
        Dim drMCR As DataRow
        Dim arrWU() As DataRow
        Dim arrWUScore() As DataRow
        Dim arrWUAll() As DataRow
        Dim arrWUForLab() As DataRow
        Dim arrTerm() As DataRow
        Dim cumGPA() As Decimal
        Dim OverallCreditsEarned As Decimal
        Dim i As Integer
        Dim numWUAttempted As Integer
        Dim numWUNotAttempted As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim GradesFormat As String = MyAdvAppSettings.AppSettings("GradesFormat", CampusId).ToLower()

        'Add fields for Completed and CreditsEarned to dtMCR
        dtMCR.Columns.Add("Completed", Type.GetType("System.Boolean"))
        dtMCR.Columns.Add("CreditsEarned", Type.GetType("System.Decimal"))
        dtMCR.Columns.Add("CreditsAttempted", Type.GetType("System.Decimal"))
        dtMCR.Columns.Add("FinAidCreditsEarned", Type.GetType("System.Decimal"))
        dtMCR.Columns.Add("TermGPA", Type.GetType("System.Decimal"))
        dtMCR.Columns.Add("CUMGPA", Type.GetType("System.Decimal"))
        dtMCR.Columns.Add("OverallCreditsEarned", Type.GetType("System.Decimal"))
        dtMCR.Columns.Add("OverallFACreditsEarned", Type.GetType("System.Decimal"))
        dtMCR.Columns.Add("CurrentScore", Type.GetType("System.Decimal"))
        dtMCR.Columns.Add("CurrentGrade", Type.GetType("System.String"))

        Dim dt As New DataTable
        Dim gradeReps As String = MyAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod")
        Dim strIncludeHours As String = MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade")
        Dim includeHours As Boolean
        If strIncludeHours.ToLower = "true" Then
            includeHours = True
        End If

        'dt = (New GraduateAuditFacade).GetTermProgressByEnrollment(stuEnrollId, gradeReps)
        If dtMCR.Rows.Count > 0 Then
            dt = (New GraduateAuditFacade).GetTermProgressFromResults(dtMCR.Rows(0)("StuEnrollId").ToString, gradeReps, includeHours)
            cumGPA = getCUMGPA(dt, cutoffDate)

        End If

        For Each drMCR In dtMCR.Rows
            'Set the defaults for the new columns added

            'get the termGPA

            Dim boolIsCourseALab As Boolean = False
            boolIsCourseALab = (New TransferGradeDB).isCourseALabWorkOrLabHourCourse(drMCR("ReqId").ToString)
            arrTerm = dt.Select("TermId='" & drMCR("TermId").ToString & "'")
            If arrTerm.Length > 0 Then
                drMCR("TermGPA") = arrTerm(0)("TermGPA")

            End If
            drMCR("CUMGPA") = cumGPA(0)
            drMCR("OverallCreditsEarned") = cumGPA(1)
            drMCR("OverallFACreditsEarned") = cumGPA(2)


            drMCR("Completed") = False
            drMCR("CreditsEarned") = 0.0
            drMCR("CreditsAttempted") = 0.0
            drMCR("FinAidCreditsEarned") = 0.0
            drMCR("CurrentScore") = drMCR("Score")
            drMCR("CurrentGrade") = drMCR("Grade")
            csrDescrip = drMCR("Descrip")

            'First: If there is no score for the course itself it normally means that the student
            'has not yet attempted the course.However, there are some courses such as MA110 Keyboarding where the course will have a null score
            'because the work units are only required to be attempted but not passed and so they are assign weights of 0. This means that the best
            'approach to take is to start with the work unit themselves. For credits attempted, if there is at least 1 work unit with a score then
            'it means that the course has been attempted. The course will be considered complete if all the work units have been satisfied and
            'the course score is >= min score.

            'Start out assuming that all the work units have been satisfied
            wuSatisfied = True
            numWUAttempted = 0
            numWUNotAttempted = 0

            'to check whether the student has attempted or not
            arrWU = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "'")
            arrWUScore = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "' and score is not null ")
            arrWUAll = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "'")
            arrWUForLab = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "' and workunitType in(500,503,544,533)")
            If arrWU.Length > 0 And arrWUScore.Length > 0 Then
                numWUAttempted = 1
            Else
                If Not drMCR.IsNull("Grade") Then
                    numWUAttempted = 1
                End If
            End If
            If numWUAttempted = 1 Then
                drMCR("CreditsAttempted") = drMCR("Credits")
            End If

            'If boolIsCourseALab = True And numWUAttempted = 1 Then
            '    If arrWU.Length > 0 And arrWUAll.Length = arrWUForLab.Length Then
            '        drMCR("CreditsAttempted") = drMCR("Credits")
            '    Else
            '        If Not drMCR.IsNull("IsCreditsAttempted") Then
            '            If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
            '        End If

            '    End If

            'ElseIf Not drMCR.IsNull("IsCreditsAttempted") Then
            '        If numWUAttempted = 1 And drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
            'End If
            'At this point if the work units have not been satisfied there is no need to do anything as the default settings above will take care of it.
            'However, if they have been satisfied we now have to look at whether the student has a passing score. Remember that there are some courses
            'where the passing score is a 0 and so the course might not have a score recorded on it but if all the work units have been satisfied then it
            'means that the course has been completed.

            'Assume that the course has not been completed
            csrCompleted = False
            Dim rtn As TransferGBWInfo
            If arrWU.Length > 0 And arrWUAll.Length = arrWUForLab.Length Then
                If MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then

                    ''this if condition added to resolve mantis 18245
                    If AllArePracticalExams(arrWUForLab) Then
                        If Not drMCR.IsNull("Grade") Then
                            If drMCR("IsPass") Then
                                csrCompleted = True
                            End If

                        Else
                            If MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                                If drMCR.IsNull("Score") And Not drMCR("clsSectionId").ToString = "00000000-0000-0000-0000-000000000000" Then

                                    ' rtn = GetTransferGradesScore(drMCR("TermId").ToString, "CourseLevel", drMCR("clsSectionId").ToString, drMCR("StuEnrollId").ToString, campusId)
                                    rtn = GetTransferGradesScore(drMCR("TermId").ToString, "CourseLevel", drMCR("clsSectionId").ToString, drMCR("StuEnrollId").ToString, campusId)
                                    If Not rtn Is Nothing Then


                                        If rtn.Score = -1 Then
                                            drMCR("Score") = DBNull.Value
                                        Else
                                            drMCR("CurrentScore") = rtn.Score
                                            If rtn.Grade <> "" Then
                                                drMCR("CurrentGrade") = rtn.Grade
                                            End If
                                        End If
                                    End If
                                End If

                                'drMCR("Score") = DBNull.Value
                            End If
                            If wuSatisfied Then
                                'If the score is empty we can say right away that the course has been completed
                                If (drMCR("Score").ToString = "" Or drMCR.IsNull("Score")) And MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "true" Then
                                    csrCompleted = True
                                Else
                                    'Check if the score is a passing score
                                    If Not drMCR.IsNull("Score") Then
                                        If ScoreIsAPass(drMCR("Score"), drMCR("clsSectionId").ToString) Then
                                            csrCompleted = True
                                        Else
                                            csrCompleted = False
                                        End If

                                    End If


                                End If
                            End If
                        End If
                    Else
                        csrCompleted = ScoreIsAPassNonRoss(arrWUForLab)
                    End If

                Else
                    csrCompleted = ScoreIsAPass(arrWUForLab)
                End If


            Else
                If Not drMCR.IsNull("Grade") Then
                    If drMCR("IsPass") Then
                        csrCompleted = True
                    End If

                Else
                    If MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                        If drMCR.IsNull("Score") And Not drMCR("clsSectionId").ToString = "00000000-0000-0000-0000-000000000000" Then

                            ' rtn = GetTransferGradesScore(drMCR("TermId").ToString, "CourseLevel", drMCR("clsSectionId").ToString, drMCR("StuEnrollId").ToString, campusId)
                            rtn = GetTransferGradesScore(drMCR("TermId").ToString, "CourseLevel", drMCR("clsSectionId").ToString, drMCR("StuEnrollId").ToString, campusId)
                            If Not rtn Is Nothing Then


                                If rtn.Score = -1 Then
                                    drMCR("Score") = DBNull.Value
                                Else
                                    drMCR("CurrentScore") = rtn.Score
                                    If rtn.Grade <> "" Then
                                        drMCR("CurrentGrade") = rtn.Grade
                                    End If
                                End If
                            End If
                        End If

                        'drMCR("Score") = DBNull.Value
                    End If
                    If wuSatisfied Then
                        'If the score is empty we can say right away that the course has been completed
                        If (drMCR("Score").ToString = "" Or drMCR.IsNull("Score")) And MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "true" Then
                            csrCompleted = True
                        Else
                            'Check if the score is a passing score
                            If Not drMCR.IsNull("Score") Then
                                If ScoreIsAPass(drMCR("Score"), drMCR("clsSectionId").ToString) Then
                                    csrCompleted = True
                                Else
                                    csrCompleted = False
                                End If

                            End If


                        End If
                    End If
                End If
            End If
            '**********************************************************************************************************************************
            'the below code was added to calculate the credits depending upon the hours attempted,not just the course credits
            'this is done aonly if the addcreditsbyservice = 'yes' eg. Hair Dynamics 
            'if the course completed is false,then it has to calculate the credits attempted and earned using the add credits by service 
            'set in grade book weights for this course.This is applicable only for course having lab work or lab hours
            'mantis issue 16300
            'by Theresa G on 5/20/2009
            '**********************************************************************************************************************************



            Dim addcreditsbyservice As String = MyAdvAppSettings.AppSettings("addcreditsbyservice").ToString.ToLower
            If csrCompleted = False And addcreditsbyservice = "yes" Then

                If boolIsCourseALab = True Then
                    Dim decCredits As Decimal = 0.0
                    Try
                        decCredits = (New TranscriptDB).ComputeWithCreditsPerService(drMCR("StuEnrollId").ToString, addcreditsbyservice, drMCR("ReqId").ToString)
                    Catch ex As System.Exception
                        decCredits = 0
                    End Try
                    drMCR("CreditsAttempted") = decCredits
                    drMCR("CreditsEarned") = decCredits
                End If

            End If


            'At this point if the course is completed we can set the relevant fields
            If csrCompleted = True Then
                If MyAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToLower = "false" Then
                    If ((New TranscriptDB).IsCourseCombinationandPass(drMCR("StuEnrollId").ToString, drMCR("clsSectionId").ToString)) Then
                        If boolIsCourseALab = True Then
                            If arrWU.Length > 0 And arrWUAll.Length = arrWUForLab.Length Then
                                drMCR("CreditsAttempted") = drMCR("Credits")
                                drMCR("CreditsEarned") = drMCR("Credits")
                                drMCR("FinAidCreditsEarned") = drMCR("FinAidCredits")
                            Else
                                drMCR("CreditsAttempted") = drMCR("Credits")
                                'If Not drMCR.IsNull("IsCreditsAttempted") Then
                                '    If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
                                'End If
                                If Not drMCR.IsNull("IsCreditsEarned") Then
                                    If drMCR("IsCreditsEarned") Then drMCR("CreditsEarned") = drMCR("Credits")
                                    If drMCR("IsCreditsEarned") Then drMCR("FinAidCreditsEarned") = drMCR("FinAidCredits")
                                End If
                            End If

                        Else
                            'If Not drMCR.IsNull("IsCreditsAttempted") Then
                            '    If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
                            'End If
                            drMCR("CreditsAttempted") = drMCR("Credits")
                            If Not drMCR.IsNull("IsCreditsEarned") Then
                                If drMCR("IsCreditsEarned") Then drMCR("CreditsEarned") = drMCR("Credits")
                                If drMCR("IsCreditsEarned") Then drMCR("FinAidCreditsEarned") = drMCR("FinAidCredits")
                            End If
                        End If

                        drMCR("Completed") = True

                    End If
                Else
                    If boolIsCourseALab = True Then
                        If arrWU.Length > 0 And arrWUAll.Length = arrWUForLab.Length Then
                            drMCR("CreditsAttempted") = drMCR("Credits")
                            drMCR("CreditsEarned") = drMCR("Credits")
                            drMCR("FinAidCreditsEarned") = drMCR("FinAidCredits")
                        Else
                            'If Not drMCR.IsNull("IsCreditsAttempted") Then
                            '    If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
                            'End If
                            drMCR("CreditsAttempted") = drMCR("Credits")
                            If Not drMCR.IsNull("IsCreditsEarned") Then
                                If drMCR("IsCreditsEarned") Then drMCR("CreditsEarned") = drMCR("Credits")
                                If drMCR("IsCreditsEarned") Then drMCR("FinAidCreditsEarned") = drMCR("FinAidCredits")
                            End If
                        End If
                    Else
                        'If Not drMCR.IsNull("IsCreditsAttempted") Then
                        '    If drMCR("IsCreditsAttempted") Then drMCR("CreditsAttempted") = drMCR("Credits")
                        'End If
                        drMCR("CreditsAttempted") = drMCR("Credits")
                        If Not drMCR.IsNull("IsCreditsEarned") Then
                            If drMCR("IsCreditsEarned") Then drMCR("CreditsEarned") = drMCR("Credits")
                            If drMCR("IsCreditsEarned") Then drMCR("FinAidCreditsEarned") = drMCR("FinAidCredits")
                        End If
                    End If

                    drMCR("Completed") = True

                End If

            End If
            'added by Theresa G on 6/4/09 for mantis 
            If drMCR("clsSectionId").ToString = "00000000-0000-0000-0000-000000000000" Then
                If Not drMCR.IsNull("IsCreditsAttempted") Then
                    If drMCR("IsCreditsAttempted") Then
                        drMCR("CreditsAttempted") = drMCR("Credits")
                    Else
                        drMCR("CreditsAttempted") = 0
                    End If

                End If
            End If

        Next



    End Function




    Public Function ScoreIsAPass(ByVal csrScore As Decimal, ByVal clsSectionId As String) As Boolean
        Dim db As New StuProgressReportDB
        Dim minPassScore As Decimal

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If clsSectionId = "00000000-0000-0000-0000-000000000000" Then
            minPassScore = db.GetMinPassingScore
        Else
            minPassScore = db.GetMinPassingScore(clsSectionId)
        End If

        If MyAdvAppSettings.AppSettings("GradeRounding").ToLower = "yes" Then
            csrScore = Math.Round(csrScore)
        End If
        If csrScore >= minPassScore Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Function ScoreIsAPass(ByVal iRows As DataRow()) As Boolean
        Dim rtn As Boolean = True

        For Each irow As DataRow In iRows

            If Not irow.IsNull("remaining") Then
                If irow("WorkUnitType") = 533 Then
                    If irow("Required") = True And irow("Remaining") > 0 And irow("mustPass") = True Then
                        Return False
                    End If
                Else
                    If irow("Required") = True And irow("Remaining") > 0 Then
                        Return False
                    End If
                End If


            End If


        Next
        Return True
    End Function
    Public Function ScoreIsAPassNonRoss(ByVal iRows As DataRow()) As Boolean
        Dim rtn As Boolean = True

        For Each irow As DataRow In iRows
            If Not irow.IsNull("remaining") Then
                If irow("Remaining") > 0 Then
                    Return False
                End If
            End If
        Next
        Return True
    End Function
    Public Function GetMinPassingScore() As Decimal
        Dim db As New StuProgressReportDB
        Dim minPassScore As Decimal

        minPassScore = db.GetMinPassingScore

        Return minPassScore

    End Function

    Public Function GetWeightsForCourse(ByVal Reqid As String) As Integer
        Return (New StuProgressReportDB).GetWeightsForCourse(Reqid)
    End Function
    Public Function GetMinPassingScore_SP() As Decimal
        Dim db As New StuProgressReportDB
        Dim minPassScore As Decimal

        minPassScore = db.GetMinPassingScore_Sp

        Return minPassScore

    End Function

    Private Function GetStudentGroups(ByVal paramInfo As ReportParamInfo) As String
        Dim db As New StuProgressReportDB
        Dim dt As New DataTable
        Dim sGroups As String

        sGroups = ""
        dt = db.GetStudentGroups(paramInfo)

        If dt.Rows.Count = 0 Then
            sGroups = ""
        Else
            'Create a comma separated list of the student groups
            For Each dr As DataRow In dt.Rows
                sGroups = sGroups & ", " & Trim(dr("Descrip"))
            Next
            'Remove the first comma and space
            sGroups = sGroups.Substring(2)
        End If

        Return sGroups

    End Function

    Private Function AreWorkUnitsSatisfied(ByVal dtWU As DataTable) As Boolean
        Dim drMCR As DataRow
        Dim arrWU() As DataRow
        Dim wuSatisfied As Boolean
        Dim csrDescrip As String
        Dim wuDescrip As String
        Dim i As Integer

        'Start out assuming the work units have been satisfied
        wuSatisfied = True

        'Check the work units that have not been attempted yet.
        arrWU = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "' AND Score IS NULL")
        If arrWU.Length > 0 Then
            'We need to check each row to see if the work unit is first required. If it is
            'required and the student has not yet attempted it then we can just say the
            'student has not yet satisfied the work unit.
            For iRow As Integer = 0 To arrWU.Length - 1
                If arrWU(iRow)("Required") = True Then
                    wuSatisfied = False
                    Exit For
                End If

            Next

        End If

        'Check the work units that have been atempted.
        arrWU = dtWU.Select("TermId = '" & drMCR("TermId").ToString & "' AND ReqId = '" & drMCR("ReqId").ToString & "' AND Score >= 0")
        If arrWU.Length > 0 Then

            For i = 0 To arrWU.Length - 1
                'If there are any work units where the score is less than the min result and 
                'the student must pass it we can say the work unit has not been satisfied.
                Dim wuRequired As String
                Dim wuMustPass As String
                wuDescrip = arrWU(i)("Descrip")
                If arrWU(i)("score") < arrWU(i)("MinResult") Then
                    'First check if it is required
                    wuRequired = arrWU(i)("Required").ToString
                    wuMustPass = arrWU(i)("MustPass")
                    If arrWU(i)("Required") = True And arrWU(i)("MustPass") = True Then
                        wuSatisfied = False
                        Exit For
                    End If

                End If
            Next

        End If

        Return wuSatisfied

    End Function

    Private Function GetStuEnrollId(ByVal rptparaminfo As ReportParamInfo) As String
        'The FilterOther property has the enrollment and term start cutoff 
        'The format is like this 'arStuEnrollments.StuEnrollId='xxxx' AND arTerm.StartDate='xxxx'
        Dim strArr() As String
        Dim strItem As String

        strArr = rptparaminfo.FilterOther.ToString.Split("AND")
        strArr = strArr(0).Split("=")
        strItem = Trim(Replace(strArr(1), "'", ""))
        Return XmlConvert.ToGuid(strItem).ToString
    End Function

    Private Function GetTransferGradesScore(ByVal TermId As String, ByVal GBWLevel As String, _
                                        ByVal ClsSectionId As String, ByVal stuEnrollid As String, ByVal CampusId As String) As TransferGBWInfo
        Dim isIncomplete, boolSU, boolPF, thereWasAnError As Boolean
        Dim x, intMinVal, IntMaxVal, intPass As Integer
        Dim intFinalScore As Decimal = 0.0
        Dim intWeight As Decimal
        Dim intClsSectionId, intWeightTakenCount As Integer
        Dim intTotalWeightsForClsSection As Decimal = 0.0
        Dim accumWgt As Decimal = 0.0
        Dim ttlWgt As Decimal = 0.0
        Dim strStuEnrollId, strFirstName, strLastName As String
        Dim strGradeSystemId As String = System.DBNull.Value.ToString
        Dim strGrdSysDetailId As String = System.DBNull.Value.ToString
        Dim errorMessage As String = ""
        Dim strClsSectionMessage As String = ""
        Dim dtStudents As DataTable
        Dim dtGrdScaleDtls As DataTable
        Dim dtGBWResults As DataTable
        Dim gradedClsSection As New ArrayList
        Dim isNotGradedClsSection As New ArrayList
        Dim updateResults As New ArrayList
        Dim transferGBW As TransferGBWInfo
        Dim objDB As New TransferGradeDB

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim GradesFormat As String = MyAdvAppSettings.AppSettings("GradesFormat", CampusId)

        thereWasAnError = False
        transferGBW = New TransferGBWInfo
        'Get Grad Scale Details for class section
        dtGrdScaleDtls = objDB.GetGradeScaleDetails(TermId, ClsSectionId)
        If dtGrdScaleDtls.Rows.Count > 0 Then
            strGradeSystemId = dtGrdScaleDtls.Rows(0)("GrdSystemId").ToString
        End If

        'Get list of students registered on the class section
        'dtStudents = objDB.GetStudentsInClassSection(ClsSectionId.GetValue(x).ToString)
        'For Each dr As DataRow In dtStudents.Rows
        strStuEnrollId = stuEnrollid

        isIncomplete = False
        ' strFirstName = dr("FirstName")
        ' strLastName = dr("LastName")
        'If dr.IsNull("IsInComplete") Then
        '    isIncomplete = False
        'Else
        '    isIncomplete = dr("IsInComplete")
        'End If
        'Get GrdSysDetailId for the IsIncomplete grade (IsInComplete=1)
        If isIncomplete Then
            strGrdSysDetailId = objDB.GetIncompleteGrade(strGradeSystemId)
        Else
            strGrdSysDetailId = System.DBNull.Value.ToString
        End If

        'Count the number of GBW result records
        intWeightTakenCount = objDB.GradeWeightsTaken(strStuEnrollId, ClsSectionId)
        If intWeightTakenCount = 0 Then Return Nothing


        'If intWeightTakenCount = 0 Then
        '    transferGBW.Score = -1
        '    Return transferGBW
        'End If

        'Check if the student has taken total weights defined on classsection
        'If totalweights is 2 and if student has taken 1 then consider that student 
        'is not completly graded.
        Dim facade As New StdGrdBkFacade
        GBWLevel = facade.GetGradeBookWeightingLevel(ClsSectionId)
        If intWeightTakenCount >= 1 Then
            Try
                If GBWLevel.ToLower = "instructorlevel" Then
                    intTotalWeightsForClsSection = objDB.GetWeightsByClassSectionStudentEnrollment(TermId, ClsSectionId)
                Else
                    intTotalWeightsForClsSection = objDB.GetTotalWeightsAtCourseLevel(TermId, CampusId, ClsSectionId)
                End If
            Catch ex As System.Exception
                intTotalWeightsForClsSection = 0
            End Try
            If GBWLevel.ToLower = "courselevel" And MyAdvAppSettings.AppSettings("EnforceNumberOfGradingComponents").ToLower = "no" Then
                ' intWeightTakenCount = intWeightTakenCount - NZ 11/19 assignment has no effect
            Else
                'If intWeightTakenCount < intTotalWeightsForClsSection And intWeightTakenCount > 1 Then
                '    intWeightTakenCount = -1
                'End If
            End If
        End If

        'If intWeightTakenCount < intWeightCount Then
        'If the Student has not been graded get the student name for error message
        If intWeightTakenCount > 0 Then
            '    'Check If The Grade Is InComplete
            '    Try
            '        If Not isIncomplete Then
            '            errorMessage &= "     " & strFirstName & " " & strLastName & vbLf
            '            thereWasAnError = True
            '        End If
            '    Catch ex As System.Exception
            '        isIncomplete = False
            '        errorMessage &= "     " & strFirstName & " " & strLastName & vbLf
            '        thereWasAnError = True
            '    End Try
            'Else
            '    If GBWLevel.ToLower = "instructorlevel" Then
            '        'Instructor Level
            '        'Compute score based on the GBW defined by the instructor
            '        dtGBWResults = objDB.GetGBWResultsForInstructorLevel(strStuEnrollId, ClsSectionId)
            '        If dtGBWResults.Rows.Count > 0 Then
            '            intWeight = dtGBWResults.Rows(0)("TotalWeight")
            '            If intWeight <> 0 Then
            '                intFinalScore = Math.Round((dtGBWResults.Rows(0)("FinalScore") / intWeight) * 100, 2)
            '            Else
            '                intFinalScore = 0.0
            '            End If
            '        End If
            '    Else
            'Course Level
            'Compute score based on the Grading Policies
            If GBWLevel.ToLower = "instructorlevel" Then
                dtGBWResults = objDB.GetGBWResultsForInstructorLevel(strStuEnrollId, ClsSectionId)
            Else
                dtGBWResults = objDB.GetGBWResultsForCourseLevel(strStuEnrollId, ClsSectionId)
            End If


            Dim oldGBWDetailId As String = ""
            Dim weight As Decimal
            Dim grdPolicyId As Integer
            Dim param As Integer
            Dim gbwUtil As GrdBkWgtUtility
            Dim arrRows() As DataRow

            'Modified by balaji on 04/30/2007 to fix Mantis 11012
            'the total weight and accumWgt needs to be reset for every student otherwise 
            'its keeps adding those weights for all students registered in that
            'class and the final score is miscalculated.
            ttlWgt = 0
            accumWgt = 0
            intFinalScore = 0
            If GBWLevel.ToLower = "instructorlevel" Then
                If dtGBWResults.Rows.Count > 0 Then
                    intWeight = dtGBWResults.Rows(0)("TotalWeight")
                    If intWeight <> 0 Then
                        intFinalScore = Math.Round((dtGBWResults.Rows(0)("FinalScore") / intWeight) * 100, 2)
                    Else
                        intFinalScore = 0.0
                    End If
                End If
            Else


                For Each row As DataRow In dtGBWResults.Rows
                    If oldGBWDetailId = "" Or oldGBWDetailId <> row("InstrGrdBkWgtDetailId").ToString Then
                        oldGBWDetailId = row("InstrGrdBkWgtDetailId").ToString
                        If row.IsNull("Weight") Then
                            weight = 0
                        Else
                            weight = row("Weight")
                        End If

                        If row.IsNull("GrdPolicyId") Then
                            grdPolicyId = 0
                        Else
                            grdPolicyId = row("GrdPolicyId")
                        End If
                        If row.IsNull("Parameter") Then
                            param = 0
                        Else
                            param = row("Parameter")
                        End If

                        'Get results for Grading Component Type
                        arrRows = dtGBWResults.Select("InstrGrdBkWgtDetailId='" & oldGBWDetailId & "'")
                        '
                        If arrRows.GetLength(0) > 0 Then
                            '                        ttlWgt += weight
                            'Compute score using Strategy pattern
                            If Not (grdPolicyId = 0) Then
                                gbwUtil = New GrdBkWgtUtility(arrRows, weight, grdPolicyId, param)
                            Else
                                gbwUtil = New GrdBkWgtUtility(arrRows, weight)
                            End If
                            If arrRows(0)("Score") Is System.DBNull.Value And gbwUtil.Weight = 0 Then
                            Else
                                ttlWgt += weight
                            End If
                            'Use gbwUtil.Weight property
                            accumWgt += gbwUtil.Weight
                            'intFinalScore += gbwUtil.Weight / weight
                        End If
                    End If
                Next

                If ttlWgt > 0 Then
                    intFinalScore = System.Math.Round((accumWgt / ttlWgt) * 100, 2)
                    'intFinalScore = System.Math.Round((intFinalScore) * 100, 2)
                    intWeight = ttlWgt
                End If
            End If
        End If

        'Check If course has been marked as Pass/Fail
        boolPF = objDB.IsCoursePF(ClsSectionId)

        'Get Grad Scale Details for the final score
        'If GradesFormat has been set to numeric all we need is the score and grade is not
        'required in transcript.
        If MyAdvAppSettings.AppSettings("GradeRounding").ToLower() = "yes" Then
            intFinalScore = Math.Round(intFinalScore)
        End If
        If GradesFormat.ToString.ToLower = "numeric" Then
            For Each dr1 As DataRow In dtGrdScaleDtls.Rows
                intMinVal = dr1("MinVal")
                IntMaxVal = dr1("MaxVal")
                intPass = dr1("Pass")
                If Math.Floor(intFinalScore) >= intMinVal And Math.Floor(intFinalScore) <= IntMaxVal Then
                    If intPass = 1 And boolPF Then
                        'If Grade falls under Pass Category and If Course Marked as Pass/Fail
                        strGrdSysDetailId = objDB.GetPassGrade(strGradeSystemId)
                    ElseIf intPass = 0 And boolPF Then
                        'If Grade falls under Fail Category and If Course Marked as Pass/Fail
                        strGrdSysDetailId = objDB.GetFailGrade(strGradeSystemId)
                    Else
                        strGrdSysDetailId = Guid.Empty.ToString
                    End If
                End If
            Next
        Else
            For Each dr1 As DataRow In dtGrdScaleDtls.Rows
                intMinVal = dr1("MinVal")
                IntMaxVal = dr1("MaxVal")
                intPass = dr1("Pass")
                If Math.Floor(intFinalScore) >= intMinVal And Math.Floor(intFinalScore) <= IntMaxVal Then
                    strGrdSysDetailId = dr1("GrdSysDetailId").ToString
                    strGradeSystemId = dr1("GrdSystemId").ToString
                    Exit For
                End If
            Next

            'Check If Course Has Been Marked as Satisfactory/Unsatisfactory
            boolSU = objDB.IsCourseSU(ClsSectionId)


            If Not isIncomplete Then
                If intPass = 1 And boolSU Then
                    'If Grade falls under Pass Category and If Course Marked as 
                    'Satisfactory/UnSatisfactory
                    strGrdSysDetailId = objDB.GetPassSatisfactoryGrade(strGradeSystemId)

                ElseIf intPass = 0 And boolSU Then
                    'If Grade falls under Fail Category and If Course Marked as 
                    'Satisfactory/UnSatisfactory
                    strGrdSysDetailId = objDB.GetNoPassUnSatisfactoryGrade(strGradeSystemId)
                End If

                If intPass = 1 And boolPF Then
                    'If Grade falls under Pass Category and If Course Marked as 
                    'Pass/Fail
                    strGrdSysDetailId = objDB.GetPassGrade(strGradeSystemId)
                ElseIf intPass = 0 And boolPF Then
                    'If Grade falls under Fail Category and If Course Marked as 
                    'Pass/Fail
                    strGrdSysDetailId = objDB.GetFailGrade(strGradeSystemId)
                End If
            End If
        End If
        ' End If

        'Construct TransferGBWInfo object

        With transferGBW
            .ClsSectionId = ClsSectionId
            .StuEnrollId = strStuEnrollId
            If GradesFormat.ToString.ToLower = "numeric" Then
                .Score = intFinalScore
            End If
            .GrdSysDetailId = strGrdSysDetailId
            .Pass = intPass
            If .GrdSysDetailId <> "" Then
                .Grade = (New GradesDB).GetGradesInfo(.GrdSysDetailId).Grade
            Else
                .Grade = ""
            End If

        End With

        'Return intFinalScore.ToString()
        Return transferGBW





    End Function

#End Region
    Private Function GetCorpInfo(ByVal CampusId As String) As CorporateInfo
        Dim corpInfo As  CorporateInfo
        corpInfo = (New CampusGroupsFacade).GetCorporateInfoFromCampusId(CampusId)
        Return corpInfo
    End Function
    Private Function AllArePracticalExams(ByVal drs As DataRow()) As Boolean
        Dim rtn As Boolean = True
        For i = 0 To drs.Length - 1
            If drs(i)("workunitType") <> 533 Then
                Return False
            End If
        Next
        Return rtn
    End Function
End Class
