Imports FAME.Advantage.Common

Public Class TransferGradeFacade
    Public Function GetAllClassSectionsByTerm(ByVal termId As String, ByVal campusId As String) As DataSet
        With New TransferGradeDB
            Return .GetAllClassSectionBytermId(termId, campusId)
        End With
    End Function

    Public Function TransferGradeByTermClassSection(ByVal termId As String, ByVal clsSectionId() As String, ByVal clsSectionDescrip() As String) As String
        With New TransferGradeDB
            Return .TransferGradeByTermClassSection(termId, clsSectionId, clsSectionDescrip)
        End With
    End Function


    Public Function GetGradedBytermId(ByVal termId As String, ByVal campusId As String) As DataSet
        Dim ds As DataSet
        Dim transferDB As New TransferGradeDB
        ds = transferDB.GetGradedBytermId(termId, campusId)
        Return ds
    End Function


    Public Function GetGradedBytermIdCourseLevel(ByVal termId As String, ByVal campusId As String) As DataSet
        Dim ds As DataSet
        Dim transferDB As New TransferGradeDB
        ds = transferDB.GetGradedBytermIdCourseLevel(termId, campusId)
        Return ds
    End Function

    Public Function TransferGradeByTermClassSectionCourseLevel(ByVal termId As String, ByVal clsSectionId() As String, ByVal clsSectionDescrip() As String, ByVal campusId As String) As String
        With New TransferGradeDB
            Return .TransferGradeByTermClassSectionCourseLevel(termId, clsSectionId, clsSectionDescrip, campusId)
        End With
    End Function
    Public Function CheckIfFinalGradePostedForCombination(ByVal stuEnrollid As String, ByVal reqId As String) As Integer
        With New TransferGradeDB
            Return .CheckIfFinalGradePostedForCombination(stuEnrollid, reqId)
        End With
    End Function
    'Public Function TransferGrades(ByVal termId As String, ByVal GBWLevel As String, _
    '                                ByVal clsSectionId() As String, ByVal clsSectionDescrip() As String, _
    '                                ByVal campusId As String, ByVal user As String, Optional ByVal GradesFormat As String = "", Optional ByVal RegentConfig As String = "") As String
    Public Function TransferGrades(ByVal termId As String, ByVal GBWLevel As String, _
                                    ByVal clsSectionId() As String, ByVal clsSectionDescrip() As String, _
                                    ByVal campusId As String, ByVal user As String, Optional ByVal GradesFormat As String = "", Optional selectedCourseCode() As String = Nothing) As String

        Dim isIncomplete, boolSU, boolPF, thereWasAnError As Boolean
        Dim x, intPass As Integer
        Dim intMinVal, intMaxVal As Decimal
        Dim intFinalScore As Decimal = 0.0
        Dim intWeight As Decimal
        Dim intclsSectionId, intWeightTakenCount As Integer
        Dim intTotalWeightsForClsSection As Decimal = 0.0
        Dim accumWgt As Decimal
        Dim ttlWgt As Decimal
        Dim strstuEnrollid, strFirstName, strLastName As String
        Dim strPrgVerCode As String
        Dim strPrgVerDescrip As String
        Dim strGradeSystemId As String = DBNull.Value.ToString
        Dim strGrdSysDetailId As String
        Dim errorMessage As String = ""
        Dim inCompleteErrorMessage As String = ""
        Dim strClsSectionMessage As StringBuilder = New StringBuilder()
        Dim strClsSectionIncompleteMessage As StringBuilder = New StringBuilder()
        Dim strReturnMessage As StringBuilder = New StringBuilder()
        Dim dtStudents As DataTable
        Dim dtGrdScaleDtls As DataTable
        Dim dtGbwResults As DataTable
        Dim gradedClsSection As New ArrayList
        Dim isNotGradedClsSection As New ArrayList
        Dim updateResults As New ArrayList
        Dim transferGBW As TransferGBWInfo
        Dim objDB As New TransferGradeDB
        Dim EnforceNumberOfGradingComponents As String = "yes"
        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        intclsSectionId = clsSectionId.Length
        While x < intclsSectionId
            thereWasAnError = False
            'Get Grad Scale Details for class section
            dtGrdScaleDtls = objDB.GetGradeScaleDetails(termId, clsSectionId.GetValue(x).ToString)
            If dtGrdScaleDtls.Rows.Count > 0 Then
                strGradeSystemId = dtGrdScaleDtls.Rows(0)("GrdSystemId").ToString
            End If

            'Get list of students registered on the class section
            If myAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
                dtStudents = objDB.GetStudentsInClassSectionForCourseLevel(clsSectionId.GetValue(x).ToString)
            Else
                dtStudents = objDB.GetStudentsInClassSectionForInstructorLevel(clsSectionId.GetValue(x).ToString)
            End If

            For Each dr As DataRow In dtStudents.Rows
                thereWasAnError = False
                strstuEnrollid = dr("stuEnrollid").ToString
                strFirstName = dr("FirstName")
                strLastName = dr("LastName")
                strPrgVerCode = dr("PrgVerCode")
                strPrgVerDescrip = dr("PrgVerDescrip")
                If dr.IsNull("IsInComplete") Then
                    isIncomplete = False
                Else
                    isIncomplete = dr("IsInComplete")
                End If

                'Get GrdSysDetailId for the IsIncomplete grade (IsInComplete=1)
                If isIncomplete Then
                    strGrdSysDetailId = objDB.GetIncompleteGrade(strGradeSystemId)
                Else
                    strGrdSysDetailId = DBNull.Value.ToString
                End If

                'Count the number of GBW result records

                'Dim strCourseId As String = (New ExamsFacade).GetreqId(termId, clsSectionId.GetValue(x).ToString)
                Dim boolDoesClassHaveClinicServices1 As Boolean
                boolDoesClassHaveClinicServices1 = (New TransferGradeDB).isCourseALabWorkOrLabHourCourseByClass(termId, clsSectionId.GetValue(x).ToString)
                If boolDoesClassHaveClinicServices1 = True Then
                    intWeightTakenCount = objDB.GradeWeightsTakenForClinics(strstuEnrollid, clsSectionId.GetValue(x).ToString)
                Else
                    intWeightTakenCount = objDB.GradeWeightsTaken(strstuEnrollid, clsSectionId.GetValue(x).ToString)
                End If


                'Check if the student has taken total weights defined on classsection
                'If totalweights is 2 and if student has taken 1 then consider that student 
                'is not completly graded.
                Dim facade As New StdGrdBkFacade
                GBWLevel = facade.GetGradeBookWeightingLevel(clsSectionId.GetValue(x).ToString)
                Dim boolDoesClassHaveClinicServices As Boolean = False
                Dim boolIsCombinationSatisfied As Boolean = False
                Dim boolIsClinicCompletlySatisfied As Boolean = False
                If intWeightTakenCount >= 1 Then
                    Try
                        If GBWLevel.ToLower = "instructorlevel" Then
                            intTotalWeightsForClsSection = objDB.GetWeightsByClassSectionStudentEnrollment(termId, clsSectionId.GetValue(x).ToString)
                        Else
                            intTotalWeightsForClsSection = objDB.GetTotalWeightsAtCourseLevel(termId, campusId, clsSectionId.GetValue(x).ToString)
                        End If
                    Catch ex As System.Exception
                        intTotalWeightsForClsSection = 0
                    End Try

                    boolDoesClassHaveClinicServices = (New TransferGradeDB).isCourseALabWorkOrLabHourCourseByClass(termId, clsSectionId.GetValue(x).ToString)
                    'since externship and clinic doesn't work with instructorlevel
                    If dr("GradesComplete") = "Yes" And GBWLevel.ToLower = "courselevel" Then

                        'if its a combination check if all components are satisfied

                        If dr("ExtnCount") > 0 Then
                            boolDoesClassHaveClinicServices = True
                            boolIsClinicCompletlySatisfied = (New TransferGradeDB).IsStudentCompletedExternShipHours(strstuEnrollid, clsSectionId.GetValue(x).ToString)
                        Else
                            Dim boolDoesClassHaveClinicServices2 As Boolean = False
                            boolDoesClassHaveClinicServices2 = (New TransferGradeDB).isCourseALabWorkOrLabHourCourseByClass(termId, clsSectionId.GetValue(x).ToString)
                            If boolDoesClassHaveClinicServices2 = True Then
                                Dim strreqId As String = (New ExamsFacade).GetreqId(termId, clsSectionId.GetValue(x).ToString)
                                boolIsClinicCompletlySatisfied = (New ExamsFacade).isClinicCourseCompletlySatisfied(strstuEnrollid, 500, strreqId)
                                Dim boolIsCoursePartOfCombination As Boolean = (New ExamsFacade).isCourseALabWorkOrLabHourCourseCombination(strreqId)


                                If boolIsCoursePartOfCombination = True Then
                                    boolIsCombinationSatisfied = (New ExamsFacade).isClinicCourseCompletlySatisfiedCombination(strstuEnrollid, 500, strreqId)
                                End If
                                If boolIsClinicCompletlySatisfied = True And boolIsCoursePartOfCombination = True And boolIsCombinationSatisfied = True Then
                                    ' intWeightTakenCount = intWeightTakenCount- NZ 11/19 assignment has no effect
                                    boolIsClinicCompletlySatisfied = True
                                End If
                                If boolIsClinicCompletlySatisfied = True And boolIsCoursePartOfCombination = False Then
                                    boolIsClinicCompletlySatisfied = False
                                End If
                            End If
                        End If

                    End If

                End If

                If intWeightTakenCount <= 0 Then
                    'Check If The Grade Is InComplete
                    Try

                        boolDoesClassHaveClinicServices = (New TransferGradeDB).isCourseALabWorkOrLabHourCourseByClass(termId, clsSectionId.GetValue(x).ToString)
                        If boolDoesClassHaveClinicServices = True Then
                            Dim strreqId As String = (New ExamsFacade).GetreqId(termId, clsSectionId.GetValue(x).ToString)
                            boolIsClinicCompletlySatisfied = (New ExamsFacade).isClinicCourseCompletlySatisfied(strstuEnrollid, 500, strreqId)
                            Dim boolIsCoursePartOfCombination As Boolean = (New ExamsFacade).isCourseALabWorkOrLabHourCourseCombination(strreqId)
                            boolIsCombinationSatisfied = False

                            If boolIsCoursePartOfCombination = True Then
                                boolIsCombinationSatisfied = (New ExamsFacade).isClinicCourseCompletlySatisfiedCombination(strstuEnrollid, 500, strreqId)
                            End If
                            If boolIsClinicCompletlySatisfied = True And boolIsCoursePartOfCombination = True And boolIsCombinationSatisfied = True Then
                                Exit Try
                            End If
                        End If

                        Try
                            If Not isIncomplete Then
                                If intWeightTakenCount = 0 Then errorMessage &= "     " & strFirstName & " " & strLastName & " -- " & strPrgVerDescrip & vbLf
                                If intWeightTakenCount < 0 Then inCompleteErrorMessage &= "     " & strFirstName & " " & strLastName & " -- " & strPrgVerDescrip & vbLf
                                thereWasAnError = True
                            End If
                        Catch ex As Exception
                            inCompleteErrorMessage &= "     " & strFirstName & " " & strLastName & " -- " & strPrgVerDescrip & vbLf
                            thereWasAnError = True
                        End Try
                    Catch ex As Exception

                    End Try


                Else
                    If GBWLevel.ToLower = "instructorlevel" Then
                        'Instructor Level
                        'Compute score based on the GBW defined by the instructor
                        dtGbwResults = objDB.GetGBWResultsForInstructorLevel(strstuEnrollid, clsSectionId.GetValue(x).ToString)
                        If dtGbwResults.Rows.Count > 0 Then
                            intWeight = dtGbwResults.Rows(0)("TotalWeight")
                            If intWeight <> 0 Then
                                intFinalScore = Math.Round((dtGbwResults.Rows(0)("FinalScore") / intWeight) * 100, 2)
                                If myAdvAppSettings.AppSettings("GradeRounding").ToString.ToLower = "yes" Then
                                    intFinalScore = Math.Round(intFinalScore, 0, MidpointRounding.AwayFromZero)
                                End If
                            Else
                                intFinalScore = 0.0
                            End If
                        End If
                    Else
                        'Course Level
                        'Compute score based on the Grading Policies
                        'modified by Theresa G on sep 20 2010
                        '19745: The grade on the transcript tab shows different from what it posts on grade book page. 
                        dtGbwResults = objDB.GetGBWResultsForCourseLevel(strstuEnrollid, clsSectionId.GetValue(x).ToString, True)

                        Dim oldGbwDetailId As String = ""
                        Dim weight As Decimal
                        Dim grdPolicyId As Integer
                        Dim param As Integer
                        Dim gbwUtil As GrdBkWgtUtility
                        Dim arrRows() As DataRow
                        Dim intLoopFirst As Integer = 0

                        'Modified by balaji on 04/30/2007 to fix Mantis 11012
                        'the total weight and accumWgt needs to be reset for every student otherwise 
                        'its keeps adding those weights for all students registered in that
                        'class and the final score is miscalculated.
                        ttlWgt = 0
                        accumWgt = 0
                        For Each row As DataRow In dtGbwResults.Rows
                            intLoopFirst += 1
                            If oldGbwDetailId = "" Or oldGbwDetailId <> row("InstrGrdBkWgtDetailId").ToString Then
                                oldGbwDetailId = row("InstrGrdBkWgtDetailId").ToString
                                If row.IsNull("Weight") Then
                                    weight = 0
                                Else
                                    weight = row("Weight")
                                End If

                                If row.IsNull("GrdPolicyId") Then
                                    grdPolicyId = 0
                                Else
                                    grdPolicyId = row("GrdPolicyId")
                                End If
                                If row.IsNull("Parameter") Then
                                    param = 0
                                Else
                                    param = row("Parameter")
                                End If

                                'Get results for Grading Component Type
                                arrRows = dtGbwResults.Select("InstrGrdBkWgtDetailId='" & oldGbwDetailId & "'")
                                '
                                If arrRows.GetLength(0) > 0 Then
                                    ttlWgt += weight
                                    'Compute score using Strategy pattern
                                    If Not (grdPolicyId = 0) Then
                                        gbwUtil = New GrdBkWgtUtility(arrRows, weight, grdPolicyId, param)
                                    Else
                                        gbwUtil = New GrdBkWgtUtility(arrRows, weight)
                                    End If
                                    'Use gbwUtil.Weight property
                                    accumWgt += gbwUtil.Weight
                                    'If EnforceNumberOfGradingComponents = "no" Then
                                    '    If intLoopFirst = 1 Then gbwUtil.Score
                                    'End If
                                End If
                            End If
                            intLoopFirst += 1
                        Next

                        'commented by Theresa G on Sep 20 2010 to fix mantis
                        '19745: The grade on the transcript tab shows different from what it posts on grade book page. 
                        intFinalScore = System.Math.Round((accumWgt / ttlWgt) * 100, 2)
                        'Troy:3/1/2013 If the school is doing grade rounding then the rounding should take place at this point
                        'before we decide which letter grade the student should receive.
                        If myAdvAppSettings.AppSettings("GradeRounding").ToString.ToLower = "yes" Then
                            intFinalScore = Math.Round(intFinalScore, 0, MidpointRounding.AwayFromZero)
                        End If
                        intWeight = ttlWgt

                    End If

                    'Check If course has been marked as Pass/Fail
                    boolPF = objDB.IsCoursePF(clsSectionId.GetValue(x).ToString)

                    'Get Grad Scale Details for the final score
                    'If GradesFormat has been set to numeric all we need is the score and grade is not
                    'required in transcript.
                    Dim boolScoreIsInRange As Boolean = False
                    If Not isIncomplete Then
                        If GradesFormat.ToString.ToLower = "numeric" Then

                            For Each dr1 As DataRow In dtGrdScaleDtls.Rows
                                intMinVal = dr1("MinVal")
                                intMaxVal = dr1("MaxVal")
                                intPass = dr1("Pass")
                                If intFinalScore >= intMinVal And intFinalScore <= (intMaxVal + 0.99) Then
                                    If intPass = 1 And boolPF Then
                                        'If Grade falls under Pass Category and If Course Marked as Pass/Fail
                                        strGrdSysDetailId = objDB.GetPassGrade(strGradeSystemId)
                                        boolScoreIsInRange = True
                                    ElseIf intPass = 0 And boolPF Then
                                        'If Grade falls under Fail Category and If Course Marked as Pass/Fail
                                        strGrdSysDetailId = objDB.GetFailGrade(strGradeSystemId)
                                        boolScoreIsInRange = True
                                    Else
                                        Try
                                            strGrdSysDetailId = dr1("GrdSysDetailId").ToString
                                        Catch ex As System.Exception
                                            strGrdSysDetailId = Guid.Empty.ToString
                                        End Try
                                        boolScoreIsInRange = True
                                    End If
                                End If
                            Next

                        Else
                            For Each dr1 As DataRow In dtGrdScaleDtls.Rows
                                intMinVal = dr1("MinVal")
                                intMaxVal = dr1("MaxVal")
                                intPass = dr1("Pass")
                                If intFinalScore >= intMinVal And intFinalScore <= (intMaxVal + 0.99) Then
                                    strGrdSysDetailId = dr1("GrdSysDetailId").ToString
                                    strGradeSystemId = dr1("GrdSystemId").ToString
                                    boolScoreIsInRange = True
                                    Exit For
                                End If
                            Next

                            'Check If Course Has Been Marked as Satisfactory/Unsatisfactory
                            boolSU = objDB.IsCourseSU(clsSectionId.GetValue(x).ToString)



                            If intPass = 1 And boolSU Then
                                'If Grade falls under Pass Category and If Course Marked as 
                                'Satisfactory/UnSatisfactory
                                strGrdSysDetailId = objDB.GetPassSatisfactoryGrade(strGradeSystemId)

                            ElseIf intPass = 0 And boolSU Then
                                'If Grade falls under Fail Category and If Course Marked as 
                                'Satisfactory/UnSatisfactory
                                strGrdSysDetailId = objDB.GetNoPassUnSatisfactoryGrade(strGradeSystemId)
                            End If

                            If intPass = 1 And boolPF Then
                                'If Grade falls under Pass Category and If Course Marked as 
                                'Pass/Fail
                                strGrdSysDetailId = objDB.GetPassGrade(strGradeSystemId)
                            ElseIf intPass = 0 And boolPF Then
                                'If Grade falls under Fail Category and If Course Marked as 
                                'Pass/Fail
                                strGrdSysDetailId = objDB.GetFailGrade(strGradeSystemId)
                            End If
                        End If
                    End If
                End If


                If dr("GradesComplete") = "No" And thereWasAnError = False Then
                    inCompleteErrorMessage &= "     " & strFirstName & " " & strLastName & " -- " & strPrgVerDescrip & vbLf
                End If
                'Construct TransferGBWInfo object

                transferGBW = New TransferGBWInfo
                With transferGBW
                    .clsSectionId = clsSectionId.GetValue(x).ToString
                    .stuEnrollid = strstuEnrollid
                    If GradesFormat.ToString.ToLower = "numeric" Then
                        .Score = intFinalScore
                    End If
                    .GrdSysDetailId = strGrdSysDetailId
                End With
                'Insert into arrayList for batch update at the end
                'Added by Balaji on 12/29/2009 to fix issue 18281
                'Update score only when the student is graded
                If thereWasAnError = False And dr("GradesComplete") = "Yes" Then
                    If boolDoesClassHaveClinicServices = False Then
                        updateResults.Add(transferGBW)
                    Else
                        If boolIsClinicCompletlySatisfied Then
                            updateResults.Add(transferGBW)
                        Else
                            inCompleteErrorMessage &= "     " & strFirstName & " " & strLastName & " -- " & strPrgVerDescrip & vbLf
                        End If
                    End If
                End If


            Next 'End of iteration on student table

            If thereWasAnError Then
                'There was some error
                'Insert into arrayList for batch update at the end (SET arClassSections.IsGraded=0)
                isNotGradedClsSection.Add(clsSectionId.GetValue(x).ToString)
                'Update arClassSections set IsGraded = 0 where clsSectionId = ? and termId =? 
            Else
                'There was NO error
                'Insert into arrayList for batch update at the end (SET arClassSections.IsGraded=1)
                gradedClsSection.Add(clsSectionId.GetValue(x).ToString)
                'Update arClassSections set IsGraded = 1 where clsSectionId = ? and termId =?
            End If

            Try
                If errorMessage.Length >= 1 Then
                    strClsSectionMessage.Append(vbLf & "<strong>" & selectedCourseCode.GetValue(x) & " - " & clsSectionDescrip.GetValue(x).ToString & "</strong>" & vbLf)
                    strClsSectionMessage.Append(errorMessage)
                    strClsSectionMessage.Append(vbLf)
                    errorMessage = ""
                End If
                If inCompleteErrorMessage.Length >= 1 Then
                    strClsSectionIncompleteMessage.Append(vbLf & "<strong>" & selectedCourseCode.GetValue(x) & " - " & clsSectionDescrip.GetValue(x).ToString() & "</strong>" & vbLf)
                    strClsSectionIncompleteMessage.Append(inCompleteErrorMessage)
                    strClsSectionIncompleteMessage.Append(vbLf)
                    inCompleteErrorMessage = ""
                End If
            Catch ex As System.Exception
                'Do Nothing
            End Try

            'Increment value of x
            x = x + 1
        End While 'End of iteration on the classSections collection


        'Do updates within a transaction
        If updateResults.Count > 0 Or isNotGradedClsSection.Count > 0 Or gradedClsSection.Count > 0 Then
            'Update the arResults and the arClassSections tables
            strReturnMessage.Append(objDB.TransferGBW(termId, updateResults, isNotGradedClsSection, gradedClsSection, user, GradesFormat))
        End If
        If strReturnMessage.ToString() = "" Then
            Try
                If strClsSectionMessage.Length >= 1 Then
                    strReturnMessage.Append("The following students do not have components graded for this course:" & vbLf)
                    strReturnMessage.Append(strClsSectionMessage)
                End If
                If strClsSectionIncompleteMessage.Length >= 1 Then
                    strReturnMessage.Append(vbLf & "The following students are missing grade components for this course:" & vbLf)
                    strReturnMessage.Append(strClsSectionIncompleteMessage)
                Else
                    '' Grades were not transferred successfully"
                End If
            Catch ex As System.Exception
                'Do Nothing
            End Try
        End If
        Return strReturnMessage.ToString()
    End Function

End Class
