Public Class LeadStatusChangeFacade
    Public Function GetLeadStatusChangesDS() As DataSet

        '   get LeadStatusChangesDS dataset
        Return (New LeadStatusChangesDB).GetLeadStatusChangesDS()

    End Function
    Public Function UpdateLeadStatusChangesDS(ByVal ds As DataSet) As String

        '   get LeadStatusChangesDS dataset
        Return (New LeadStatusChangesDB).UpdateLeadStatusChangesDS(ds)

    End Function
    Public Function GetLeadStatusForCampGroup(ByVal campGrpId As String) As DataSet

        '   get LeadStatusChangesDS dataset
        Return (New LeadStatusChangesDB).GetLeadStatusForCampGroup(campGrpId)

    End Function
    Public Function GetNewLeadStatuses(ByVal campusId As String, ByVal userId As String) As DataSet

        '   get dataset with lead statuses that map to New Lead system status.
        Return (New LeadStatusChangesDB).GetNewLeadStatuses(campusId, userId)

    End Function
    Public Function GetAvailLeadStatuses(ByVal leadStatusId As String, ByVal campusId As String, ByVal userId As String) As DataSet
        If leadStatusId = "" Then
            '   get dataset with lead statuses that map to New Lead system status.
            Return (New LeadStatusChangesDB).GetNewLeadStatuses(campusId, userId)

        Else
            '   get dataset with current lead status and those statuses that can be changed into.
            Return (New LeadStatusChangesDB).GetAvailLeadStatuses(leadStatusId, campusId, userId)
        End If
    End Function
    Public Function GetExistingLeadStatuses(ByVal campusId As String, ByVal userId As String, ByVal currentstatusid As String, Optional ByVal LeadId As String = "") As DataSet
        Return (New LeadStatusChangesDB).GetExistingLeadStatuses(campusId, userId, currentstatusid, LeadId)
    End Function
    Public Function GetLeadStatusMappedToEnrolledStatus(ByVal userId As String, ByVal campusid As String) As DataSet
        Return (New LeadStatusChangesDB).GetLeadStatusMappedToEnrolledStatus(userId, campusid)
    End Function
    Public Function GetLeadStatusMappedToUnEnrolledStatus(ByVal userId As String, ByVal campusid As String) As DataSet
        Return (New LeadStatusChangesDB).GetLeadStatusMappedToUnEnrolledStatus(userId, campusid)
    End Function
    Public Function GetSearchAvailLeadStatuses(ByVal leadStatusId As String, ByVal campusId As String, ByVal userId As String) As DataSet

        If leadStatusId = "" Then
            '   get dataset with lead statuses that map to New Lead system status.
            Return (New LeadStatusChangesDB).GetNewSearchLeadStatuses(campusid, userId)

        Else
            '   get dataset with current lead status and those statuses that can be changed into.
            Return (New LeadStatusChangesDB).GetAvailLeadStatuses(leadStatusId, campusid, userId)
        End If

    End Function
    Public Function InsertLeadStatusChange(ByVal statusChanged As LeadStatusesInfo, ByVal user As String) As String

        'If (statusChanged.IsInDB = False) Then
        '   get dataset with current lead status and those statuses that can be changed into.
        Return (New LeadStatusChangesDB).InsertLeadStatusChange(statusChanged, user)
        'Else
        '    '   get dataset with current lead status and those statuses that can be changed into.
        '    Return (New LeadStatusChangesDB).UpdateLeadStatusChange(statusChanged, user)
        'End If

    End Function
    Public Function DeleteLeadStatusChange(ByVal leadId As String) As String
        Dim objDB As New LeadStatusChangesDB

        Return (New LeadStatusChangesDB).DeleteLeadStatusChange(leadId)

    End Function
    Public Function GetLeadStatus(ByVal leadId As String) As String
        Return (New LeadStatusChangesDB).GetLeadStatus(leadId)
    End Function
    Public Function CheckIfLeadCanBeEnrolledByStatus(ByVal userId As String, ByVal campusid As String, ByVal CurrentStatusId As String) As Integer
        Return (New LeadStatusChangesDB).CheckIfLeadCanBeEnrolledByStatus(userId, campusid, CurrentStatusId)
    End Function
End Class
