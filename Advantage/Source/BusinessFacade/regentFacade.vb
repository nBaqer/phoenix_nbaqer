Public Class regentFacade
    Public Function InsertAwardCode(ByVal AwardCode As String, ByVal AwardCodeId As String, ByVal moduser As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode.InsertAwardCode(AwardCode, AwardCodeId, moduser))
    End Function
    Public Function getAwardCode() As DataTable
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode.getAwardCode())
    End Function
    Public Function InsertAwardSubCode(ByVal AwardCode As String, ByVal AwardCodeId As String, ByVal awardsubcodeid As String, ByVal moduser As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode.InsertAwardSubCode(AwardCode, AwardCodeId, awardsubcodeid, moduser))
    End Function
    Public Function getAwardSubCode() As DataTable
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).getAwardSubCode
    End Function
    Public Function updateRegentTerm(ByVal TermId As String, ByVal regentTermId As String, ByVal moduser As String, ByVal StartTerm As String, ByVal EndTerm As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).updateRegentTerm(TermId, regentTermId, moduser, StartTerm, EndTerm)
    End Function
    Public Function updateRegentActivityCode(ByVal ExtracurrId As String, ByVal ActivityCode As String, ByVal moduser As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).updateRegentActivityCode(ExtracurrId, ActivityCode, moduser)
    End Function
    Public Function getregentTerm(ByVal TermId As String) As DataTable
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).getregentTerm(TermId)
    End Function
    Public Function getregentActivityCode(ByVal ExtraCurrId As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).getregentActivityCode(ExtraCurrId)
    End Function
    Public Function updateRegentCurriculumCode(ByVal PrgVerId As String, ByVal ActivityCode As String, ByVal moduser As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).updateRegentCurriculumCode(PrgVerId, ActivityCode, moduser)
    End Function
    Public Function updateRegentLender(ByVal LenderId As String, ByVal regentLenderId As String, ByVal moduser As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).updateRegentLender(LenderId, regentLenderId, moduser)
    End Function
    Public Function getRegentLender(ByVal LenderId As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).getregentLender(LenderId)
    End Function
    Public Function updateMaintenanceData(ByVal tblName As String, ByVal PKID As String, _
                                            ByVal Value2 As String, _
                                            ByVal fldvalue1 As String, _
                                            ByVal fldvalue2 As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).updateMaintenanceData(tblName, PKID, Value2, fldvalue1, fldvalue2)
    End Function
    Public Function getregentDegreeCode(ByVal DegreeId As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).getregentDegreeCode(DegreeId)
    End Function
    Public Function getregentCurriculum(ByVal PrgVerId As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).getregentCurriculum(PrgVerId)
    End Function
    Public Function updateRegentDegreeCode(ByVal DegreeId As String, ByVal regentDegreeCode As String, ByVal moduser As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).updateRegentDegreeCode(DegreeId, regentDegreeCode, moduser)
    End Function
    Public Function getregentWithdrawalCode(ByVal StatusCodeId As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).getregentWithdrawalCode(StatusCodeId)
    End Function
    Public Function updateRegentWithdrawalCode(ByVal StatusCodeId As String, ByVal regentWithdrawalCode As String, ByVal moduser As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).updateRegentWithdrawalCode(StatusCodeId, regentWithdrawalCode, moduser)
    End Function
    Public Function getAllMaintenanceDataByPrimaryKey(ByVal tblname As String, ByVal fldValue As String, ByVal pKValue As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).getAllMaintenanceDataByPrimaryKey(tblname, fldValue, pKValue)
    End Function
    Public Function getAllAddressCode(ByVal tblname As String, ByVal fldValue As String, ByVal pKValue As String) As String
        Dim InsAwardCode As New regentDB
        Return (InsAwardCode).getAllAddressCode(tblname, fldValue, pKValue)
    End Function
    Public Function ImportDISBDataFromRegent(ByVal UploadedFile As String, ByVal CampusId As String, ByVal User As String) As String
        Dim InsAwardCode As New regDisbursements
        Return (InsAwardCode).ImportDISBDataFromRegent(UploadedFile, CampusId, User)
    End Function
    Public Function ReturnExceptionDS(ByVal filename As String) As DataSet
        Dim InsAwardCode As New regDisbursements
        Return (InsAwardCode).ReturnExceptionDS(filename)
    End Function
End Class
