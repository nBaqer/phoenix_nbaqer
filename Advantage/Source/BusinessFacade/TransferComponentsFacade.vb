Public Class TransferComponentsFacade

    Public Function GetPartiallyCompletedCourses(ByVal StuEnrollId As String) As DataSet
        Return (New TransferComponentsDB).GetPartiallyCompletedCourses(StuEnrollId)
    End Function

    Public Function GetStudentsWithPartiallyCompletedCourses(ByVal ReqId As String, ByVal NewClsSectionId As String, TermId As String) As DataSet
        Dim ds As New DataSet
        Dim dsScheduleConflicts As New DataSet
        ds = (New TransferComponentsDB).GetStudentsWithPartiallyCompletedCourses(ReqId, TermId)
        If NewClsSectionId <> "" Then
            For Each dr As DataRow In ds.Tables(0).Rows
                If dr("ClsSectionId").ToString <> NewClsSectionId AndAlso _
                    Not (New AdStudentGroupsFacade).IsStudentOnRegistrationHold(dr("StuEnrollId").ToString) Then
                    dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsDuringRegistration(NewClsSectionId, dr("StuEnrollId").ToString)
                    If dsScheduleConflicts Is Nothing OrElse dsScheduleConflicts.Tables(0).Rows.Count < 1 Then
                        dr("ValidStudent") = 1
                    End If
                End If
            Next
        End If
        Return ds
    End Function

    Public Function GetCompletedComponents(ByVal ResultId As String) As DataSet
        Return (New TransferComponentsDB).GetCompletedComponents(ResultId)
    End Function

    Public Function GetTermsForPartiallyCompletedCourse(ByVal StuEnrollId As String) As DataTable
        Dim ds As New DataSet, dt As New DataTable
        Dim dtDistinctTerms As New DataTable
        ds = (New TransferComponentsDB).GetPartiallyCompletedCourses(StuEnrollId)
        dt = ds.Tables(0)
        dtDistinctTerms = dt.DefaultView.ToTable(True, "TermId", "TermDescrip")
        Return dtDistinctTerms
    End Function

    Public Function GetAvailableClassesForPartialTransferByStudent(ByVal ResultId As String) As DataTable
        Dim ds As New DataSet, dt As New DataTable
        Dim dsScheduleConflicts As New DataSet
        ds = (New TransferComponentsDB).GetAvailableClassesForPartialTransferByStudent(ResultId)
        dt = ds.Tables(0).Clone
        For Each dr As DataRow In ds.Tables(0).Rows
            dsScheduleConflicts = (New ClassSectionFacade).CheckScheduleConflictsDuringRegistration(dr("ClsSectionId").ToString, dr("StuEnrollId").ToString)
            If dsScheduleConflicts Is Nothing OrElse dsScheduleConflicts.Tables(0).Rows.Count < 1 Then
                dt.ImportRow(dr)
            End If
        Next
        Return dt
    End Function

    Public Function GetAvailableClassesForPartialTransferByClass(ByVal ReqId As String, ByVal CampusId As String) As DataTable
        Dim ds As New DataSet, dt As New DataTable
        ds = (New TransferComponentsDB).GetAvailableClassesForPartialTransferByClass(ReqId, CampusId)
        dt = ds.Tables(0)
        Return dt
    End Function
    Public Function GetScheduleConflictsTransferPartialForStudent(ByVal ClassList As String) As String
        Dim dt As New DataTable
        Dim rtn As String = String.Empty
        Dim dsScheduleConflicts As New DataSet
        dt = (New TransferComponentsDB).GetScheduleConflictsTransferPartialForStudent(ClassList)

        For Each dr As DataRow In dt.Rows
            If rtn = String.Empty Then
                rtn = dr("CourseDescrip") & "-" & dr("ClsSection")
            Else
                rtn = rtn & " , " & dr("CourseDescrip") & "-" & dr("ClsSection")
            End If

        Next
        Return rtn
    End Function
End Class

