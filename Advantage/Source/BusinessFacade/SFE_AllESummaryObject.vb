
Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllESummaryObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SFE_AllESummary"

	Private dsRaw As New DataSet
	Private dtRptRaw As New DataTable
	Private dsRpt As New DataSet

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SFE_AllESummaryObjectDB.GetReportDatasetRaw(RptParamInfo)

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		' pre-process raw data, first process student enrollment info, then process with raw student 
		'	data to obtain prepared raw data for processing
		dtRptRaw = SFE_AllECommonObject.PreProcessRawData(dsRaw.Tables(TblNameStudents), IPEDSFacade.GetProcessedEnrollInfo(dsRaw))

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add("GenderDescrip", GetType(String))
			.Add("EthCodeDescrip", GetType(String))
			.Add("Count_Undergrad", GetType(System.Int32))
			.Add("Count_Grad", GetType(System.Int32))
			.Add("Count_FirstProf", GetType(System.Int32))
		End With

		' get IPEDS Gender and Ethnic Codes, for use in processing
		Dim dtGenders As DataTable = IPEDSFacade.GetIPEDSGenderInfo
		Dim dtEthCodes As DataTable = IPEDSFacade.GetIPEDSEthnicInfo

		' loop thru genders, and ethnic groups for each gender, and process matching records in raw dataset
		For Each drGenderInfo As DataRow In dtGenders.Rows
			For Each drEthnicInfo As DataRow In dtEthCodes.Rows
				ProcessRows(drGenderInfo("GenderDescrip"), drEthnicInfo("EthCodeDescrip"))
			Next
		Next

		' return report dataset
		Return dsRpt

	End Function

	Private Sub ProcessRows(ByVal GenderDescrip As String, ByVal EthCodeDescrip As String)
		' set filter according to passed-in Gender and Ethnic Code
		Dim FilterString As String = "GenderDescrip LIKE '" & GenderDescrip & "' AND " & _
									 "EthCodeDescrip LIKE '" & EthCodeDescrip & "'"

		' create new summary report row and add to report DataSet
		Dim drRpt As DataRow = dsRpt.Tables(MainTableName).NewRow

		' set descriptions for Gender and Ethnic Code
		drRpt("GenderDescrip") = GenderDescrip
		drRpt("EthCodeDescrip") = EthCodeDescrip

		' set counters for each type of program, using filters of Gender/Ethnic Code and Program Type
		With dtRptRaw
			drRpt("Count_Undergrad") = .Compute("COUNT(GenderDescrip)", FilterString & " AND ProgTypeDescrip LIKE '" & ProgTypeUndergraduate & "'")
			drRpt("Count_Grad") = .Compute("COUNT(GenderDescrip)", FilterString & " AND ProgTypeDescrip LIKE '" & ProgTypeGraduate & "'")
			drRpt("Count_FirstProf") = .Compute("COUNT(GenderDescrip)", FilterString & " AND ProgTypeDescrip LIKE '" & ProgTypeFirstProf & "'")
		End With

		dsRpt.Tables(MainTableName).Rows.Add(drRpt)
	End Sub

End Class
