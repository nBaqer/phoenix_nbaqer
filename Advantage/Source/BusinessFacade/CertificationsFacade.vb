' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' CertificationsFacade.vb
'
' CertificationsFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class CertificationsFacade
    Public Function GetAllCertifications() As DataSet
        '   get the dataset with all degrees
        Return (New CertificationsDB).GetAllCertifications()
    End Function
End Class

