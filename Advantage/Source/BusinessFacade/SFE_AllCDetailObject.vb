
Imports FAME.AdvantageV1.Common.IPEDSCommon

Public Class SFE_AllCDetailObject
Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "SFE_AllCDetail"

	Private dsRaw As New DataSet
	Private dtRptRaw As New DataTable
	Private dsRpt As New DataSet
	Private RptParamInfo As ReportParamInfoIPEDS

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = SFE_AllCDetailObjectDB.GetReportDataSetRaw(RptParamInfo)

		' save passed-in parameter info for use in processing
		Me.RptParamInfo = RptParamInfo

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		Dim drState As DataRow

		' pre-process raw data, first process student enrollment info, then process with raw student 
		'	data to obtain prepared raw data for processing
		dtRptRaw = SFE_AllCCommonObject.PreProcessRawData(dsRaw.Tables(TblNameStudents), IPEDSFacade.GetProcessedEnrollInfo(dsRaw))

		' create table to hold final report data
		With dsRpt.Tables.Add(MainTableName).Columns
			.Add(RptStuIdentifierColName, GetType(String))
			.Add("Name", GetType(String))
			.Add("StateDescrip", GetType(String))
			.Add("Ind_FirstTime", GetType(String))
			.Add("Ind_Grad", GetType(String))
		End With

		' get IPEDS States list, for use in processing
		Dim dtStates As DataTable = StatesFacade.GetStatesIPEDS

		' loop thru IPEDS states, and process matching records in raw dataset
		For Each drState In dtStates.Rows
			ProcessRows(drState)
		Next

		' return report dataset
		Return dsRpt

	End Function

	Private Sub ProcessRows(ByVal drState As DataRow)
		Dim i As Integer
		Dim drRpt As DataRow
		Dim DateLastYear As Date = RptParamInfo.RptEndDate.AddYears(-1)

		' create dataview from students in raw dataset, filtered according to the 
		'	passed in state description
		Dim dvRptRaw As New DataView(dtRptRaw, "StateDescrip LIKE '" & drState("StateDescrip") & "'", _
									 "", DataViewRowState.CurrentRows)

		' if student dataview has records, loop thru records and process raw data 
		'	to create report records
		If dvRptRaw.Count > 0 Then
			For i = 0 To dvRptRaw.Count - 1
				drRpt = dsRpt.Tables(MainTableName).NewRow

				' set Student Id and name
				IPEDSFacade.SetStudentInfo(dvRptRaw(i), drRpt)

				' record the state description
				drRpt("StateDescrip") = drState("StateDescrip")

				' set indicators as appropriate
				drRpt("Ind_FirstTime") = "X"
				drRpt("Ind_Grad") = DBNull.Value
				If dvRptRaw(i)("LastEdInstType").ToString = SFE_AllCCommonObject.LastEdHS AndAlso _
				   Date.Compare(CDate(dvRptRaw(i)("LastEdGradDate")), DateLastYear) < 0 Then
					drRpt("Ind_Grad") = "X"
				End If

				' add new report row to report table
				dsRpt.Tables(MainTableName).Rows.Add(drRpt)
			Next

		' if student dataview has no records, create record with description for
		'	state, NULL data otherwise
		Else
			drRpt = dsRpt.Tables(MainTableName).NewRow

			drRpt(RptStuIdentifierColName) = DBNull.Value
			drRpt("Name") = DBNull.Value
			drRpt("StateDescrip") = drState("StateDescrip")
			drRpt("Ind_FirstTime") = DBNull.Value
			drRpt("Ind_Grad") = DBNull.Value

			' add empty report row to report table
			dsRpt.Tables(MainTableName).Rows.Add(drRpt)

		End If

	End Sub

End Class