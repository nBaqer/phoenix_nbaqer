Public Class StudentAwardFacade
    Public Sub AddStudentAward(ByVal StudentAwardInfo As StudentAwardInfo, ByVal Details As DataTable)
        Dim StudentAwardDB As New StudentAwardDB
        StudentAwardDB.AddStudentAward(StudentAwardInfo, Details)
    End Sub
    Public Sub UpdateStudentAward(ByVal StudentAwardInfo As StudentAwardInfo, ByVal Details As DataTable)
        Dim StudentAwardDB As New StudentAwardDB
        StudentAwardDB.UpdateStudentAward(StudentAwardInfo, Details)
    End Sub
    Public Sub DeleteStudentAward(ByVal StudentAwardId As Guid)
        Dim StudentAwardDB As New StudentAwardDB
        StudentAwardDB.DeleteStudentAward(StudentAwardId)
    End Sub
    Public Sub AddStudentAwardSchedule(ByVal StudentAwardScheduleInfo As StudentAwardScheduleInfo)
        Dim StudentAwardDB As New StudentAwardDB
        StudentAwardDB.AddStudentAwardSchedule(StudentAwardScheduleInfo)
    End Sub
    Public Sub UpdateStudentAwardSchedule(ByVal StudentAwardScheduleInfo As StudentAwardScheduleInfo)
        Dim StudentAwardDB As New StudentAwardDB
        StudentAwardDB.UpdateStudentAwardSchedule(StudentAwardScheduleInfo)
    End Sub
    Public Sub DeleteStudentAwardSchedule(ByVal AwardScheduleId As Guid)
        Dim StudentAwardDB As New StudentAwardDB
        StudentAwardDB.DeleteStudentAwardSchedule(AwardScheduleId)
    End Sub
    Public Function GetStudentAwardSchedule(ByVal StudentAwardId As Guid) As DataSet
        Dim StudentAwardDB As New StudentAwardDB
        Dim ds As New DataSet
        ds = StudentAwardDB.GetStudentAwardSchedule(StudentAwardId)
        Return ds
    End Function
    Public Function GetAwardDetails(ByVal StudentAwardId As Guid) As DataSet
        Dim StudentAwardDB As New StudentAwardDB
        Dim ds As New DataSet
        ds = StudentAwardDB.GetAwardDetails(StudentAwardId)
        Return ds
    End Function
    'Public Function PopulateDropDown() As DataSet
    '    Dim StudentAwardDB As New StudentAwardDB
    '    Dim ds As New DataSet
    '    ds = StudentAwardDB.PopulateDropDown
    '    Return ds
    'End Function
    'Public Function PopulateDataList(ByVal StudentId As Guid) As DataSet
    '    Dim StudentAwardDB As New StudentAwardDB
    '    Dim ds As New DataSet
    '    ds = StudentAwardDB.PopulateDataList(StudentId)
    '    Return ds
    'End Function
    Public Function GetCalculatedFields(ByVal StudentAwardId As Guid) As Double()
        Dim StudentAwardDB As New StudentAwardDB
        Dim Totals() As Double
        Totals = StudentAwardDB.GetCalculatedFields(StudentAwardId)
        Return Totals
    End Function
    Public Function GetLenderDropDowns() As DataSet
        Dim StudentAwardDB As New StudentAwardDB
        Dim ds As New DataSet
        ds = StudentAwardDB.GetLenderDropDowns
        Return ds
    End Function
    'Public Function GetAllAwardTypes(ByVal showActiveOnly As Boolean) As DataSet

    '    '   Instantiate DAL component
    '    With New StudentAwardDB

    '        '   get a dataset with all FundSources
    '        Return .GetAllAwardTypes(showActiveOnly)

    '    End With

    'End Function
    Public Function GetAllAwardsPerStudent(ByVal stuEnrollId As String) As DataSet

        '   Instantiate DAL component
        With New StudentAwardDB

            ''Modified to stored Procedure on April 15 2010
            '   get a dataset with all awards
            Return .GetAllAwardsPerStudent_Sp(stuEnrollId)

        End With

    End Function
    Public Function GetLateFeesToBePostedDS(ByVal cutoffDate As Date, ByVal gracePeriod As Integer, ByVal user As String, ByVal campusid As String, Optional ByVal effectiveDate As String = "1/1/1900") As DataSet
        '   return string with update results
        Return (New StudentAwardsDB).GetLateFeesToBePostedDS(cutoffDate, gracePeriod, user, campusid, effectiveDate)
    End Function
End Class
