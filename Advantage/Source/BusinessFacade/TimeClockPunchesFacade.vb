Imports FAME.Advantage.Common

Public Class TimeClockPunchesFacade
    Inherits BaseReportFacade

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim TimClockPunches As New TimeClockPunchReport
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by report.
        ds = BuildReportSource(TimClockPunches.GetTimeClockpunchesWeekly(rptParamInfo), TimClockPunches.StudentIdentifier)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function
#End Region

#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim stuName As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String
        Dim oldCmpGrp As String = ""
        Dim oldCampus As String = ""
        Dim dt As DataTable
        Dim dtWeekPunches As DataTable
        Dim dr2 As DataRow

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Get the mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Dim dspunch As New DataSet
        Try
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'Use table to store Total Amounts per Campus Groups and Campuses.
                    dt = ds.Tables(0)

                    '''''''''''''''''''''''''''''''

                    For Each dr As DataRow In ds.Tables(0).Rows
                        'Apply mask to SSN.
                        If StudentIdentifier = "SSN" Then
                            If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                                If dr("StudentIdentifier") <> "" Then
                                    Dim temp As String = dr("StudentIdentifier")
                                    dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                                End If
                            End If
                        End If

                        If Not (dr("BadgeNumber") Is System.DBNull.Value) Then
                            If dr("BadgeNumber") <> "" Then
                                dr("BadgeId") = dr("BadgeNumber")
                            End If
                        End If

                    Next

                    '''''''''''''''''''''''''''''''''''



                    dt.Columns.Add(New DataColumn("RowNo", System.Type.GetType("System.Int32")))

                    Dim i As Integer
                    Dim j As Integer
                    Dim maxrowno As Integer = 0

                    For i = 0 To ds.Tables(2).Rows.Count - 1
                        Dim drInPunchesForaDate() As DataRow = ds.Tables(0).Select("StuEnrollid='" + ds.Tables(2).Rows(i)("StuEnrollId").ToString + "'and RecordDate='" + ds.Tables(2).Rows(i)("RecordDate").ToString + "' and PunchIn is not null")
                        For j = 0 To drInPunchesForaDate.Length - 1
                            drInPunchesForaDate(j)("RowNo") = j + 1
                            maxrowno = FindMaxRowNo(maxrowno, j + 1)
                        Next
                    Next i

                    For i = 0 To ds.Tables(2).Rows.Count - 1
                        Dim drOutPunchesForaDate() As DataRow = ds.Tables(0).Select("StuEnrollid='" + ds.Tables(2).Rows(i)("StuEnrollId").ToString + "' and RecordDate='" + ds.Tables(2).Rows(i)("RecordDate").ToString + "' and PunchOut is not null")
                        For j = 0 To drOutPunchesForaDate.Length - 1
                            drOutPunchesForaDate(j)("RowNo") = j + 1
                            maxrowno = FindMaxRowNo(maxrowno, j + 1)
                        Next
                    Next i
                    ds.AcceptChanges()
                    dtWeekPunches = NewTabletoStorePunchesForaWeek()
                    Dim row As DataRow
                    Dim StuEnrollIdCount As Integer
                    For StuEnrollIdCount = 0 To ds.Tables(1).Rows.Count - 1
                        For i = 0 To maxrowno - 1

                            Dim drPunchesforWeek() As DataRow = ds.Tables(0).Select("StuEnrollid='" + ds.Tables(1).Rows(StuEnrollIdCount)("StuEnrollId").ToString + "' and RowNo=" + (i + 1).ToString)
                            If drPunchesforWeek.Length > 0 Then
                                row = dtWeekPunches.NewRow()
                                row("StuEnrollId") = drPunchesforWeek(0)("StuEnrollId").ToString
                                row("StudentIdentifier") = drPunchesforWeek(0)("StudentIdentifier").ToString
                                row("StudentName") = drPunchesforWeek(0)("StudentName").ToString
                                row("BadgeId") = drPunchesforWeek(0)("BadgeId").ToString
                                Try
                                    row("SuppressDate") = MyAdvAppSettings.AppSettings("SuppressDateInTranscriptFooter").ToLower
                                Catch ex As System.Exception
                                    row("SuppressDate") = "no"
                                End Try

                                For j = 0 To drPunchesforWeek.Length - 1

                                    If Not drPunchesforWeek(j)("RecordDate") Is System.DBNull.Value Then
                                        If CDate(drPunchesforWeek(j)("RecordDate")).DayOfWeek.ToString = "Monday" Then
                                            row("MonDate") = drPunchesforWeek(j)("RecordDate")
                                            If row("MonIn") Is DBNull.Value Then
                                                row("MonIn") = drPunchesforWeek(j)("PunchIn")
                                            End If
                                            If row("MonOut") Is DBNull.Value Then
                                                row("MonOut") = drPunchesforWeek(j)("PunchOut")
                                            End If
                                            row("MonSch") = drPunchesforWeek(j)("SchedHours")
                                            row("MonAct") = drPunchesforWeek(j)("ActualHours")

                                        ElseIf CDate(drPunchesforWeek(j)("RecordDate")).DayOfWeek.ToString = "Tuesday" Then
                                            row("TueDate") = drPunchesforWeek(j)("RecordDate")
                                            If row("TueIn") Is DBNull.Value Then
                                                row("TueIn") = drPunchesforWeek(j)("PunchIn")
                                            End If
                                            If row("TueOut") Is DBNull.Value Then
                                                row("TueOut") = drPunchesforWeek(j)("PunchOut")
                                            End If
                                            row("TueSch") = drPunchesforWeek(j)("SchedHours")
                                            row("TueAct") = drPunchesforWeek(j)("ActualHours")
                                        ElseIf CDate(drPunchesforWeek(j)("RecordDate")).DayOfWeek.ToString = "Wednesday" Then
                                            row("WedDate") = drPunchesforWeek(j)("RecordDate")
                                            If row("WedIn") Is DBNull.Value Then
                                                row("WedIn") = drPunchesforWeek(j)("PunchIn")
                                            End If
                                            If row("WedOut") Is DBNull.Value Then
                                                row("WedOut") = drPunchesforWeek(j)("PunchOut")
                                            End If
                                            row("WedSch") = drPunchesforWeek(j)("SchedHours")
                                            row("WedAct") = drPunchesforWeek(j)("ActualHours")
                                        ElseIf CDate(drPunchesforWeek(j)("RecordDate")).DayOfWeek.ToString = "Thursday" Then
                                            row("ThurDate") = drPunchesforWeek(j)("RecordDate")
                                            If row("ThurIn") Is DBNull.Value Then
                                                row("ThurIn") = drPunchesforWeek(j)("PunchIn")
                                            End If
                                            If row("ThurOut") Is DBNull.Value Then
                                                row("ThurOut") = drPunchesforWeek(j)("PunchOut")
                                            End If
                                            row("ThurSch") = drPunchesforWeek(j)("SchedHours")
                                            row("ThurAct") = drPunchesforWeek(j)("ActualHours")
                                        ElseIf CDate(drPunchesforWeek(j)("RecordDate")).DayOfWeek.ToString = "Friday" Then
                                            row("FriDate") = drPunchesforWeek(j)("RecordDate")
                                            If row("FriIn") Is DBNull.Value Then
                                                row("FriIn") = drPunchesforWeek(j)("PunchIn")
                                            End If
                                            If row("FriOut") Is DBNull.Value Then
                                                row("FriOut") = drPunchesforWeek(j)("PunchOut")
                                            End If
                                            row("FriSch") = drPunchesforWeek(j)("SchedHours")
                                            row("FriAct") = drPunchesforWeek(j)("ActualHours")
                                        ElseIf CDate(drPunchesforWeek(j)("RecordDate")).DayOfWeek.ToString = "Saturday" Then
                                            row("SatDate") = drPunchesforWeek(j)("RecordDate")
                                            If row("SatIn") Is DBNull.Value Then
                                                row("SatIn") = drPunchesforWeek(j)("PunchIn")
                                            End If
                                            If row("SatOut") Is DBNull.Value Then
                                                row("SatOut") = drPunchesforWeek(j)("PunchOut")
                                            End If
                                            row("SatSch") = drPunchesforWeek(j)("SchedHours")
                                            row("SatAct") = drPunchesforWeek(j)("ActualHours")
                                        ElseIf CDate(drPunchesforWeek(j)("RecordDate")).DayOfWeek.ToString = "Sunday" Then
                                            row("SunDate") = drPunchesforWeek(j)("RecordDate")
                                            If row("SunIn") Is DBNull.Value Then
                                                row("SunIn") = drPunchesforWeek(j)("PunchIn")
                                            End If
                                            If row("SunOut") Is DBNull.Value Then
                                                row("SunOut") = drPunchesforWeek(j)("PunchOut")
                                            End If
                                            row("SunSch") = drPunchesforWeek(j)("SchedHours")
                                            row("SunAct") = drPunchesforWeek(j)("ActualHours")
                                        End If

                                    End If
                                Next
                                dtWeekPunches.Rows.Add(row)
                            End If


                        Next
                    Next
                    ''Adding the Actual and Scheduled hours to the last row
                    '' ''

                    ''Dim rowno As Integer = dtWeekPunches.Rows.Count - 1
                    ''If rowno > 0 Then
                    ''    dtWeekPunches.Rows(rowno)("MonAct") = dtWeekPunches.Rows(0)("MonAct")
                    ''    dtWeekPunches.Rows(rowno)("MonSch") = dtWeekPunches.Rows(0)("MonSch")
                    ''    dtWeekPunches.Rows(rowno)("TueAct") = dtWeekPunches.Rows(0)("TueAct")
                    ''    dtWeekPunches.Rows(rowno)("TueSch") = dtWeekPunches.Rows(0)("TueSch")
                    ''    dtWeekPunches.Rows(rowno)("WedAct") = dtWeekPunches.Rows(0)("WedAct")
                    ''    dtWeekPunches.Rows(rowno)("WedSch") = dtWeekPunches.Rows(0)("WedSch")
                    ''    dtWeekPunches.Rows(rowno)("ThurAct") = dtWeekPunches.Rows(0)("ThurAct")
                    ''    dtWeekPunches.Rows(rowno)("ThurSch") = dtWeekPunches.Rows(0)("ThurSch")
                    ''    dtWeekPunches.Rows(rowno)("FriAct") = dtWeekPunches.Rows(0)("FriAct")
                    ''    dtWeekPunches.Rows(rowno)("FriSch") = dtWeekPunches.Rows(0)("FriSch")
                    ''    dtWeekPunches.Rows(rowno)("SatAct") = dtWeekPunches.Rows(0)("SatAct")
                    ''    dtWeekPunches.Rows(rowno)("SatSch") = dtWeekPunches.Rows(0)("SatSch")
                    ''    dtWeekPunches.Rows(rowno)("SunAct") = dtWeekPunches.Rows(0)("SunAct")
                    ''    dtWeekPunches.Rows(rowno)("SunSch") = dtWeekPunches.Rows(0)("SunSch")
                    ''End If

                    dspunch.Tables.Add(dtWeekPunches)
                    dspunch.Merge(ds.Tables(1))
                    dspunch.Tables(1).Columns.Add(New DataColumn("MakeUpHours", System.Type.GetType("System.String")))
                    dspunch.Tables(1).Columns.Add(New DataColumn("AbsentHours", System.Type.GetType("System.String")))

                    For i = 0 To dspunch.Tables(1).Rows.Count - 1


                        If Not dspunch.Tables(1).Rows(i)("ActualHours") Is System.DBNull.Value Then
                            If CDec(dspunch.Tables(1).Rows(i)("ActualHours")) = 0.0 Then
                                dspunch.Tables(1).Rows(i)("AbsentHours") = dspunch.Tables(1).Rows(i)("SchedHours")
                                dspunch.Tables(1).Rows(i)("MakeUpHours") = "0.00"
                            ElseIf CDec(dspunch.Tables(1).Rows(i)("ActualHours")) = 9999.0 Then
                                dspunch.Tables(1).Rows(i)("ActualHours") = "0.00"
                                dspunch.Tables(1).Rows(i)("AbsentHours") = "0.00"
                                dspunch.Tables(1).Rows(i)("MakeUpHours") = "0.00"
                            ElseIf CDec(dspunch.Tables(1).Rows(i)("ActualHours")) = 999.0 Then
                                dspunch.Tables(1).Rows(i)("ActualHours") = "0.00"
                                dspunch.Tables(1).Rows(i)("AbsentHours") = "0.00"
                                dspunch.Tables(1).Rows(i)("MakeUpHours") = "0.00"
                            Else
                                If CDec(dspunch.Tables(1).Rows(i)("SchedHours")) > CDec(dspunch.Tables(1).Rows(i)("ActualHours")) Then
                                    dspunch.Tables(1).Rows(i)("AbsentHours") = CDec(dspunch.Tables(1).Rows(i)("SchedHours")) - CDec(dspunch.Tables(1).Rows(i)("ActualHours"))
                                    dspunch.Tables(1).Rows(i)("MakeUpHours") = "0.00"

                                ElseIf CDec(dspunch.Tables(1).Rows(i)("ActualHours")) > CDec(dspunch.Tables(1).Rows(i)("SchedHours")) Then
                                    dspunch.Tables(1).Rows(i)("AbsentHours") = "0.00"
                                    dspunch.Tables(1).Rows(i)("MakeUpHours") = CDec(dspunch.Tables(1).Rows(i)("ActualHours")) - CDec(dspunch.Tables(1).Rows(i)("SchedHours"))
                                ElseIf CDec(dspunch.Tables(1).Rows(i)("ActualHours")) = CDec(dspunch.Tables(1).Rows(i)("SchedHours")) Then
                                    dspunch.Tables(1).Rows(i)("AbsentHours") = "0.00"
                                    dspunch.Tables(1).Rows(i)("MakeUpHours") = "0.00"
                                End If
                            End If
                        End If
                    Next i
                    dspunch.AcceptChanges()

                End If
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try


        Return dspunch
    End Function
    Private Function FindMaxRowNo(ByVal maxrowno As Integer, ByVal rowno As Integer) As Integer
        If maxrowno > rowno Then
            Return maxrowno
        Else
            Return rowno
        End If

    End Function
    'Private Sub AddRowtoPunchesTable(ByVal dtTimeClockPunch As DataTable, ByVal stuEnrollId As String, ByVal StuName As String, ByVal BadgeId As String)
    '    Dim row As DataRow
    '    row = dtTimeClockPunch.NewRow()
    '    row("StuEnrollId") = stuEnrollId
    '    row("StudentName") = StuName
    '    row("BadgeId") = BadgeId
    '    dtTimeClockPunch.Rows.Add(row)
    'End Sub
    Private Function NewTabletoStorePunchesForaWeek() As DataTable
        Dim dtTimeClockPunch As New DataTable("dtTimeClockPunches")
        dtTimeClockPunch.Columns.Add(New DataColumn("StuEnrollId", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("StudentIdentifier", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("StudentName", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("BadgeId", System.Type.GetType("System.String")))


        dtTimeClockPunch.Columns.Add(New DataColumn("MonDate", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("MonIn", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("MonOut", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("MonSch", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("MonAct", System.Type.GetType("System.String")))

        dtTimeClockPunch.Columns.Add(New DataColumn("TueDate", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("TueIn", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("TueOut", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("TueSch", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("TueAct", System.Type.GetType("System.String")))

        dtTimeClockPunch.Columns.Add(New DataColumn("WedDate", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("WedIn", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("WedOut", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("WedSch", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("WedAct", System.Type.GetType("System.String")))

        dtTimeClockPunch.Columns.Add(New DataColumn("ThurDate", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("ThurIn", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("ThurOut", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("ThurSch", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("ThurAct", System.Type.GetType("System.String")))

        dtTimeClockPunch.Columns.Add(New DataColumn("FriDate", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("FriIn", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("FriOut", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("FriSch", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("FriAct", System.Type.GetType("System.String")))

        dtTimeClockPunch.Columns.Add(New DataColumn("SatDate", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("SatIn", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("SatOut", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("SatSch", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("SatAct", System.Type.GetType("System.String")))

        dtTimeClockPunch.Columns.Add(New DataColumn("SunDate", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("SunIn", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("SunOut", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("SunSch", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("SunAct", System.Type.GetType("System.String")))
        dtTimeClockPunch.Columns.Add(New DataColumn("SuppressDate", System.Type.GetType("System.String")))


        Return dtTimeClockPunch

    End Function
#End Region

End Class
