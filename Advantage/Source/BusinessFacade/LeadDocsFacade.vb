Public Class LeadDocsFacade
    Public Function GetDocsLeadByLeadId(ByVal LeadId As String, ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        With New LeadDocsDB
            ds = .GetDocsLeadByLeadId(LeadId, campusId)
        End With
        ds = ConcatenateDocLeadsLastFirstName(ds)
        Return ds
    End Function
    Public Function GetAllDocumentStatus() As DataSet
        With New LeadDocsDB
            Return .GetAllDocumentStatus()
        End With
    End Function
    Public Function ConcatenateDocLeadsLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("LeadDT5")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("LeadDocId")}
        End With
        If ds.Tables("LeadDT5").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                ' row("FullName") = row("FirstName") & " " & row("LastName") & " " & row("MiddleName") & " - " & row("DocumentDescription")

                If Not row("DocumentStatus") Is System.DBNull.Value And Not row("DocumentDescription") Is System.DBNull.Value Then
                    If Not row("DocumentStatus") = "" Then
                        row("FullName") = row("DocumentDescription") & " (" & row("DocumentStatus") & ")"
                    Else
                        row("FullName") = row("DocumentDescription")
                    End If
                Else
                    row("FullName") = ""
                End If
            Next
        End If
        Return ds
    End Function
    Public Function GetLeadNamesById(ByVal LeadId As String) As String
        Dim ds As New DataSet
        Dim strFullName As String
        With New LeadDocsDB
            ds = .GetAllLeadsNamesById(LeadId)
        End With
        strFullName = ConcatenateLeadLastFirstNameById(ds)
        Return strFullName
    End Function
    Public Function ConcatenateLeadLastFirstNameById(ByVal ds As DataSet) As String
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT1")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("LeadId")}
        End With
        Dim strFullName As String
        If ds.Tables("InstructorDT1").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                ' DE7626 
                'strFullName = row("FirstName") & " " & row("LastName") & " " & row("MiddleName")
                strFullName = row("FirstName") & " " & row("MiddleName") & " " & row("LastName")
            Next
        End If
        Return strFullName
    End Function
    Public Function GetDocumentExtension(ByVal strFileName As String, ByVal strDocumentType As String) As String
        With New LeadDocsDB
            Return .GetDocumentExtension(strFileName, strDocumentType)
        End With
    End Function
    Public Function GetStudentDocs(ByVal LeadDocId As String, ByVal LeadId As String) As StudentDocsInfo
        With New LeadDocsDB
            Return .GetLeadDetails(LeadDocId, LeadId)
        End With
    End Function
    'Public Function GetModuleId(ByVal DocumentId As String) As Integer
    '    With New LeadDocsDB
    '        Return .GetModuleId(DocumentId)
    '    End With
    'End Function
    Public Function GetEnrollmentCountByLeadId(ByVal leadId As String) As Integer
        With New LeadDocsDB
            Return .GetStudentId(leadId)
        End With
    End Function
    Public Function DeleteLeadDocs(ByVal LeadDocId As String, ByVal DocumentId As String, ByVal LeadId As String) As Integer
        With New LeadDocsDB
            'get the dataset with all Possible Extracurriculars Based on Groups Available
            Return .DeleteLeadDocs(LeadDocId, DocumentId, LeadId)
        End With
    End Function
    Public Function GetAllDocsStudentByStatus(ByVal DocumentStatusId As String, ByVal LeadId As String, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        With New LeadDocsDB
            ds = .GetAllDocsStudentByStatus(DocumentStatusId, LeadId, CampusId)
        End With
        ds = ConcatenateDocLeadLastFirstName(ds)
        Return ds
    End Function
    Public Function InsertDocument(ByVal strFileName As String, ByVal strFileExtension As String, ByVal User As String, ByVal strDocumentType As String, ByVal strStudentId As String, ByVal strDocumentId As String, ByVal strDisplayName As String) As Integer
        With New LeadDocsDB
            Return .InsertDocument(strFileName, strFileExtension, User, strDocumentType, strStudentId, strDocumentId, strDisplayName)
        End With
    End Function
    Public Function InsertDocumentbyID(ByVal strFileName As String, ByVal strFileExtension As String, ByVal User As String, ByVal strDocumentType As String, ByVal strStudentId As String, ByVal strDocumentId As String, ByVal strDisplayName As String) As String
        With New LeadDocsDB
            Return .InsertDocumentbyID(strFileName, strFileExtension, User, strDocumentType, strStudentId, strDocumentId, strDisplayName)
        End With
    End Function
    Public Function UpdateDocument(ByVal FileID As String, ByVal strFileName As String, ByVal strFileExtension As String, ByVal User As String, ByVal strDocumentType As String, ByVal strStudentId As String, ByVal strDocumentId As String, ByVal strDisplayName As String) As Integer
        With New LeadDocsDB
            Return .UpdateDocument(FileID, strFileName, strFileExtension, User, strDocumentType, strStudentId, strDocumentId, strDisplayName)
        End With
    End Function
    Public Function DeleteDocumentbyID(ByVal FileID As String) As String
        With New LeadDocsDB
            Return .DeleteDocumentbyID(FileID)
        End With
    End Function
    'Public Function GetAllDocuments1() As DataSet
    '    'Instantiate DAL component
    '    With New LeadDocsDB
    '        'get the dataset with all SkillGroups
    '        Return .GetAllDocuments1()
    '    End With
    'End Function
    Public Function GetAllDocsByLeadGroupandStudent(ByVal ModuleId As String, ByVal StudentId As String) As DataSet
        With New LeadDocsDB
            Return .GetAllDocsByLeadGroupandStudent(ModuleId, StudentId)
        End With
    End Function
    Public Function GetAllDocumentsByStudent(ByVal campusId As String) As DataSet
        With New LeadDocsDB
            Return .GetAllDocumentsByStudent(campusId)
        End With
    End Function

    ''' <summary>
    ''' The GetDocumentTypes method call the LeadDocsDB.GetDocumentTypes with campusId and returns the dataset of DocumentTypes
    ''' </summary>
    ''' <param name="campusId"></param>
    ''' <returns></returns>
    Public Function GetDocumentTypes(ByVal campusId As String) As DataSet
        With New LeadDocsDB
            Return .GetDocumentTypes(campusId)
        End With
    End Function

    Public Function GetDocumentsByProgramVersionAndStatus(ByVal LeadId As String, ByVal PrgVerId As String) As DataSet
        With New LeadDB
            Return .GetDocumentsByProgramVersionAndStatus(LeadId, PrgVerId)
        End With
    End Function
    Public Function GetAllDocumentsByLeadGroup(ByVal PrgVerId As String, ByVal LeadId As String) As DataSet
        With New LeadDocsDB
            Return .GetAllDocumentsByLeadGroup(PrgVerId, LeadId)
        End With
    End Function
    Public Function GetAllStandardDocumentsByLeadGroupAndStatus(ByVal LeadId As String) As DataSet
        With New LeadDocsDB
            Return .GetAllStandardDocumentsByLeadGroupAndStatus(LeadId)
        End With
    End Function
    Public Function GetAllStandardDocumentsByLeadGroup(ByVal LeadId As String) As DataSet
        With New LeadDocsDB
            Return .GetAllStandardDocumentsByLeadGroup(LeadId)
        End With
    End Function
    Public Function GetAllDocumentsByStudentAndDocType(ByVal DocumentType As String) As DataSet
        With New LeadDocsDB
            Return .GetAllDocumentsByStudentAndDocType(DocumentType)
        End With
    End Function
    Public Function GetAllDocumentsByStudentDocType(ByVal FileName As String, ByVal DocumentType As String) As DataSet
        With New LeadDocsDB
            Return .GetAllDocumentsByStudentDocType(FileName, DocumentType)
        End With
    End Function
    Public Function DeleteDocument(ByVal strFileName As String, ByVal ModuleId As Integer) As String
        With New LeadDocsDB
            Return .DeleteDocument(strFileName)
        End With
    End Function
    Public Function ConcatenateDocLeadLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("StudentDT5")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("LeadDocId")}
        End With
        If ds.Tables("StudentDT5").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                If Not row("DocumentStatus") Is System.DBNull.Value And Not row("DocumentDescription") Is System.DBNull.Value Then
                    If Not row("DocumentStatus") = "" Then
                        row("FullName") = row("DocumentDescription") & " (" & row("DocumentStatus") & ")"
                    Else
                        row("FullName") = row("DocumentDescription")
                    End If
                Else
                    row("FullName") = ""
                End If
            Next
        End If
        Return ds
    End Function
    Public Function UpdateStudentDocs(ByVal StudentDocsInfo As StudentDocsInfo, ByVal user As String, ByVal StudentDocsId As String) As String
        With New LeadDocsDB
            If (StudentDocsInfo.IsInDb = False) Then
                Return .AddStudentDocs(StudentDocsInfo, user)
            Else
                Return .UpdateStudentDocs(StudentDocsInfo, user, StudentDocsId)
            End If
        End With
    End Function
    Public Function CheckIfDocHasBeenApproved(ByVal DocStatusId As String) As Integer
        With New LeadDocsDB
            Return .CheckIfDocHasBeenApproved(DocStatusId)
        End With
    End Function

    Public Function GetAllDocumentsByLeadIdandDocumentID(ByVal LeadID As String, ByVal DocumentID As String) As DataSet
        With New LeadDocsDB
            Return .GetAllDocumentsByLeadIdandDocumentID(LeadID, DocumentID)
        End With
    End Function
End Class
