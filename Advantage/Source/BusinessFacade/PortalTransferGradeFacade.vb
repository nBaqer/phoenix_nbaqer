Imports FAME.Advantage.Common

Public Class PortalTransferGradeFacade
    Public Function GetAllClassSectionsByTerm(ByVal TermId As String, ByVal CampusId As String) As DataSet
        With New PortalTransferGradeDB
            Return .GetAllClassSectionByTermId(TermId, CampusId)
        End With
    End Function
    Public Function TransferGradeByTermClassSection(ByVal TermId As String, ByVal ClsSectionId() As String, ByVal clsSectionDescrip() As String) As String
        With New PortalTransferGradeDB
            Return .TransferGradeByTermClassSection(TermId, ClsSectionId, clsSectionDescrip)
        End With
    End Function
    Public Function GetGradedByTermId(ByVal TermId As String, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        Dim TransferDB As New PortalTransferGradeDB
        'Dim dr As DataRow
        'Dim strStatus As String
        'Dim intInComplete As Integer
        'Dim intTotalWeights, intActualWeights As Integer
        ds = TransferDB.GetGradedByTermId(TermId, CampusId)
        'For Each dr In ds.Tables(0).Rows
        '    If dr("Grades") Is System.DBNull.Value Then
        '        dr("Grades") = "No"
        '    End If
        'Next
        Return ds
    End Function
    Public Function GetGradedByTermIdCourseLevel(ByVal TermId As String, ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        Dim TransferDB As New PortalTransferGradeDB
        '  Dim dr As DataRow
        'Dim strStatus As String
        'Dim intInComplete As Integer
        'Dim intTotalWeights, intActualWeights As Integer
        ds = TransferDB.GetGradedByTermIdCourseLevel(TermId, CampusId)
        Return ds
    End Function
    Public Function TransferGradeByTermClassSectionCourseLevel(ByVal TermId As String, ByVal ClsSectionId() As String, ByVal clsSectionDescrip() As String, ByVal CampusId As String) As String
        With New PortalTransferGradeDB
            Return .TransferGradeByTermClassSectionCourseLevel(TermId, ClsSectionId, clsSectionDescrip, CampusId)
        End With
    End Function
    Public Function CheckIfFinalGradePostedForCombination(ByVal StuEnrollid As String, ByVal ReqId As String) As Integer
        With New PortalTransferGradeDB
            Return .CheckIfFinalGradePostedForCombination(StuEnrollid, ReqId)
        End With
    End Function
    'Public Function TransferGrades(ByVal TermId As String, ByVal GBWLevel As String, _
    '                                ByVal ClsSectionId() As String, ByVal clsSectionDescrip() As String, _
    '                                ByVal CampusId As String, ByVal user As String, Optional ByVal GradesFormat As String = "", Optional ByVal RegentConfig As String = "") As String
    Public Function TransferGrades(ByVal TermId As String, ByVal GBWLevel As String, _
                                    ByVal ClsSectionId() As String, ByVal clsSectionDescrip() As String, _
                                    ByVal CampusId As String, ByVal user As String, Optional ByVal GradesFormat As String = "", Optional selectedCourseCode() As String = Nothing) As String
        Dim isIncomplete, boolSU, boolPF, thereWasAnError As Boolean
        Dim x, intPass As Integer
        Dim intMinVal, intMaxVal As Decimal
        Dim intFinalScore As Decimal = 0.0
        Dim intWeight As Decimal
        Dim intClsSectionId, intWeightTakenCount As Integer
        Dim intTotalWeightsForClsSection As Decimal = 0.0
        Dim accumWgt As Decimal = 0.0
        Dim ttlWgt As Decimal = 0.0
        Dim strStuEnrollId, strFirstName, strLastName As String
        Dim strGradeSystemId As String = System.DBNull.Value.ToString
        Dim strGrdSysDetailId As String = System.DBNull.Value.ToString
        Dim errorMessage As String = ""
        Dim InCompleteErrorMessage As String
        Dim strClsSectionMessage As String = ""
        Dim strClsSectionIncompleteMessage As String = ""
        Dim dtStudents As DataTable
        Dim dtGrdScaleDtls As DataTable
        Dim dtGBWResults As DataTable
        Dim gradedClsSection As New ArrayList
        Dim isNotGradedClsSection As New ArrayList
        Dim updateResults As New ArrayList
        Dim transferGBW As TransferGBWInfo
        Dim objDB As New PortalTransferGradeDB
        Dim EnforceNumberOfGradingComponents As String = "yes"
        Dim myPortalAdvAppSettings As PortalAdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings) Is Nothing Then
            myPortalAdvAppSettings = DirectCast(HttpContext.Current.Session("PortalAdvAppSettings"), PortalAdvAppSettings)
        Else
            myPortalAdvAppSettings = New PortalAdvAppSettings
        End If

        intClsSectionId = ClsSectionId.Length
        While x < intClsSectionId
            thereWasAnError = False
            'Get Grad Scale Details for class section
            dtGrdScaleDtls = objDB.GetGradeScaleDetails(TermId, ClsSectionId.GetValue(x).ToString)
            If dtGrdScaleDtls.Rows.Count > 0 Then
                strGradeSystemId = dtGrdScaleDtls.Rows(0)("GrdSystemId").ToString
            End If

            'Get list of students registered on the class section
            If myPortalAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" Then
                dtStudents = objDB.GetStudentsInClassSectionForCourseLevel(ClsSectionId.GetValue(x).ToString)
            Else
                dtStudents = objDB.GetStudentsInClassSectionForInstructorLevel(ClsSectionId.GetValue(x).ToString)
            End If

            For Each dr As DataRow In dtStudents.Rows
                thereWasAnError = False
                strStuEnrollId = dr("StuEnrollId").ToString
                strFirstName = dr("FirstName")
                strLastName = dr("LastName")
                If dr.IsNull("IsInComplete") Then
                    isIncomplete = False
                Else
                    isIncomplete = dr("IsInComplete")
                End If

                'Get GrdSysDetailId for the IsIncomplete grade (IsInComplete=1)
                If isIncomplete Then
                    strGrdSysDetailId = objDB.GetIncompleteGrade(strGradeSystemId)
                Else
                    strGrdSysDetailId = System.DBNull.Value.ToString
                End If

                'Count the number of GBW result records

                Dim strCourseId As String = (New PortalExamsFacade).GetReqId(TermId, ClsSectionId.GetValue(x).ToString)
                Dim boolDoesClassHaveClinicServices1 As Boolean = False
                boolDoesClassHaveClinicServices1 = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourseByClass(TermId, ClsSectionId.GetValue(x).ToString)
                If boolDoesClassHaveClinicServices1 = True Then
                    intWeightTakenCount = objDB.GradeWeightsTakenForClinics(strStuEnrollId, ClsSectionId.GetValue(x).ToString)
                Else
                    intWeightTakenCount = objDB.GradeWeightsTaken(strStuEnrollId, ClsSectionId.GetValue(x).ToString)
                End If


                'Check if the student has taken total weights defined on classsection
                'If totalweights is 2 and if student has taken 1 then consider that student 
                'is not completly graded.
                Dim facade As New StdGrdBkFacade
                GBWLevel = facade.GetGradeBookWeightingLevel(ClsSectionId.GetValue(x).ToString)
                Dim boolDoesClassHaveClinicServices As Boolean = False
                Dim boolIsCombinationSatisfied As Boolean = False
                Dim boolIsClinicCompletlySatisfied As Boolean = False
                If intWeightTakenCount >= 1 Then
                    Try
                        If GBWLevel.ToLower = "instructorlevel" Then
                            intTotalWeightsForClsSection = objDB.GetWeightsByClassSectionStudentEnrollment(TermId, ClsSectionId.GetValue(x).ToString)
                        Else
                            intTotalWeightsForClsSection = objDB.GetTotalWeightsAtCourseLevel(TermId, CampusId, ClsSectionId.GetValue(x).ToString)
                        End If
                    Catch ex As System.Exception
                        intTotalWeightsForClsSection = 0
                    End Try

                    boolDoesClassHaveClinicServices = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourseByClass(TermId, ClsSectionId.GetValue(x).ToString)
                    'since externship and clinic doesn't work with instructorlevel
                    If dr("GradesComplete") = "Yes" And GBWLevel.ToLower = "courselevel" Then

                        'if its a combination check if all components are satisfied

                        If dr("ExtnCount") > 0 Then
                            boolDoesClassHaveClinicServices = True
                            boolIsClinicCompletlySatisfied = (New PortalTransferGradeDB).IsStudentCompletedExternShipHours(strStuEnrollId, ClsSectionId.GetValue(x).ToString)
                        Else
                            Dim boolDoesClassHaveClinicServices2 As Boolean = False
                            boolDoesClassHaveClinicServices2 = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourseByClass(TermId, ClsSectionId.GetValue(x).ToString)
                            If boolDoesClassHaveClinicServices2 = True Then
                                Dim strReqId As String = (New PortalExamsFacade).GetReqId(TermId, ClsSectionId.GetValue(x).ToString)
                                boolIsClinicCompletlySatisfied = (New PortalExamsFacade).isClinicCourseCompletlySatisfied(strStuEnrollId, 500, strReqId)
                                Dim boolIsCoursePartOfCombination As Boolean = (New PortalExamsFacade).isCourseALabWorkOrLabHourCourseCombination(strReqId)


                                If boolIsCoursePartOfCombination = True Then
                                    boolIsCombinationSatisfied = (New PortalExamsFacade).isClinicCourseCompletlySatisfiedCombination(strStuEnrollId, 500, strReqId)
                                End If
                                If boolIsClinicCompletlySatisfied = True And boolIsCoursePartOfCombination = True And boolIsCombinationSatisfied = True Then
                                    ' intWeightTakenCount = intWeightTakenCount- NZ 11/19 assignment has no effect
                                    boolIsClinicCompletlySatisfied = True
                                End If
                                If boolIsClinicCompletlySatisfied = True And boolIsCoursePartOfCombination = False Then
                                    boolIsClinicCompletlySatisfied = False
                                    ' intWeightTakenCount = intWeightTakenCount - NZ 11/19 assignment has no effect
                                End If
                                'Else
                                '    intWeightTakenCount = -1
                            End If
                        End If

                    End If

                End If

                'If intWeightTakenCount < intWeightCount Then
                'If the Student has not been graded get the student name for error message
                'Dim boolDoesClassHaveClinicServices As Boolean = False
                If intWeightTakenCount <= 0 Then
                    'Check If The Grade Is InComplete
                    Try

                        boolDoesClassHaveClinicServices = (New PortalTransferGradeDB).isCourseALabWorkOrLabHourCourseByClass(TermId, ClsSectionId.GetValue(x).ToString)
                        If boolDoesClassHaveClinicServices = True Then
                            Dim strReqId As String = (New PortalExamsFacade).GetReqId(TermId, ClsSectionId.GetValue(x).ToString)
                            boolIsClinicCompletlySatisfied = (New PortalExamsFacade).isClinicCourseCompletlySatisfied(strStuEnrollId, 500, strReqId)
                            Dim boolIsCoursePartOfCombination As Boolean = (New PortalExamsFacade).isCourseALabWorkOrLabHourCourseCombination(strReqId)
                            boolIsCombinationSatisfied = False

                            If boolIsCoursePartOfCombination = True Then
                                boolIsCombinationSatisfied = (New PortalExamsFacade).isClinicCourseCompletlySatisfiedCombination(strStuEnrollId, 500, strReqId)
                            End If
                            If boolIsClinicCompletlySatisfied = True And boolIsCoursePartOfCombination = True And boolIsCombinationSatisfied = True Then
                                Exit Try
                            End If
                        End If

                        Try
                            If Not isIncomplete Then
                                If intWeightTakenCount = 0 Then errorMessage &= "     " & strFirstName & " " & strLastName & vbLf
                                If intWeightTakenCount < 0 Then InCompleteErrorMessage &= "     " & strFirstName & " " & strLastName & vbLf
                                thereWasAnError = True
                            End If
                        Catch ex As System.Exception
                            isIncomplete = False
                            InCompleteErrorMessage &= "     " & strFirstName & " " & strLastName & vbLf
                            thereWasAnError = True
                        End Try
                    Catch ex As Exception

                    End Try


                Else
                    If GBWLevel.ToLower = "instructorlevel" Then
                        'Instructor Level
                        'Compute score based on the GBW defined by the instructor
                        dtGBWResults = objDB.GetGBWResultsForInstructorLevel(strStuEnrollId, ClsSectionId.GetValue(x).ToString)
                        If dtGBWResults.Rows.Count > 0 Then
                            intWeight = dtGBWResults.Rows(0)("TotalWeight")
                            If intWeight <> 0 Then
                                intFinalScore = Math.Round((dtGBWResults.Rows(0)("FinalScore") / intWeight) * 100, 2)
                                If myPortalAdvAppSettings.AppSettings("GradeRounding").ToString.ToLower = "yes" Then
                                    intFinalScore = Math.Round(intFinalScore, 0, MidpointRounding.AwayFromZero)
                                End If
                            Else
                                intFinalScore = 0.0
                            End If
                        End If
                    Else
                        'Course Level
                        'Compute score based on the Grading Policies
                        'modified by Theresa G on sep 20 2010
                        '19745: The grade on the transcript tab shows different from what it posts on grade book page. 
                        dtGBWResults = objDB.GetGBWResultsForCourseLevel(strStuEnrollId, ClsSectionId.GetValue(x).ToString, True)

                        Dim oldGBWDetailId As String = ""
                        Dim weight As Decimal
                        Dim grdPolicyId As Integer
                        Dim param As Integer
                        Dim gbwUtil As GrdBkWgtUtility
                        Dim arrRows() As DataRow
                        Dim totCurrentScore As Decimal
                        Dim intLoopFirst As Integer = 0

                        'Modified by balaji on 04/30/2007 to fix Mantis 11012
                        'the total weight and accumWgt needs to be reset for every student otherwise 
                        'its keeps adding those weights for all students registered in that
                        'class and the final score is miscalculated.
                        ttlWgt = 0
                        accumWgt = 0
                        totCurrentScore = 0
                        For Each row As DataRow In dtGBWResults.Rows
                            intLoopFirst += 1
                            If oldGBWDetailId = "" Or oldGBWDetailId <> row("InstrGrdBkWgtDetailId").ToString Then
                                oldGBWDetailId = row("InstrGrdBkWgtDetailId").ToString
                                If row.IsNull("Weight") Then
                                    weight = 0
                                Else
                                    weight = row("Weight")
                                End If

                                If row.IsNull("GrdPolicyId") Then
                                    grdPolicyId = 0
                                Else
                                    grdPolicyId = row("GrdPolicyId")
                                End If
                                If row.IsNull("Parameter") Then
                                    param = 0
                                Else
                                    param = row("Parameter")
                                End If

                                'Get results for Grading Component Type
                                arrRows = dtGBWResults.Select("InstrGrdBkWgtDetailId='" & oldGBWDetailId & "'")
                                '
                                If arrRows.GetLength(0) > 0 Then
                                    ttlWgt += weight
                                    'Compute score using Strategy pattern
                                    If Not (grdPolicyId = 0) Then
                                        gbwUtil = New GrdBkWgtUtility(arrRows, weight, grdPolicyId, param)
                                    Else
                                        gbwUtil = New GrdBkWgtUtility(arrRows, weight)
                                    End If
                                    'Use gbwUtil.Weight property
                                    accumWgt += gbwUtil.Weight
                                    If EnforceNumberOfGradingComponents = "no" Then
                                        If intLoopFirst = 1 Then totCurrentScore += gbwUtil.Score
                                    End If
                                End If
                            End If
                            intLoopFirst += 1
                        Next

                        'commented by Theresa G on Sep 20 2010 to fix mantis
                        '19745: The grade on the transcript tab shows different from what it posts on grade book page. 
                        intFinalScore = System.Math.Round((accumWgt / ttlWgt) * 100, 2)
                        'Troy:3/1/2013 If the school is doing grade rounding then the rounding should take place at this point
                        'before we decide which letter grade the student should receive.
                        If myPortalAdvAppSettings.AppSettings("GradeRounding").ToString.ToLower = "yes" Then
                            intFinalScore = Math.Round(intFinalScore, 0, MidpointRounding.AwayFromZero)
                        End If
                        intWeight = ttlWgt
                        'If ttlWgt > 0 And AdvantageCommonValues.getEnforceNumberOfGradingComponents <> "no" Then
                        '    intFinalScore = System.Math.Round((accumWgt / ttlWgt) * 100, 2)
                        '    intWeight = ttlWgt
                        'ElseIf ttlWgt > 0 And AdvantageCommonValues.getEnforceNumberOfGradingComponents = "no" Then
                        '    intFinalScore = totCurrentScore
                        'End If
                    End If

                    'Check If course has been marked as Pass/Fail
                    boolPF = objDB.IsCoursePF(ClsSectionId.GetValue(x).ToString)

                    'Get Grad Scale Details for the final score
                    'If GradesFormat has been set to numeric all we need is the score and grade is not
                    'required in transcript.
                    Dim boolScoreIsInRange As Boolean = False
                    If Not isIncomplete Then
                        If GradesFormat.ToString.ToLower = "numeric" Then

                            For Each dr1 As DataRow In dtGrdScaleDtls.Rows
                                intMinVal = dr1("MinVal")
                                intMaxVal = dr1("MaxVal")
                                intPass = dr1("Pass")
                                If intFinalScore >= intMinVal And intFinalScore <= (intMaxVal + 0.99) Then
                                    If intPass = 1 And boolPF Then
                                        'If Grade falls under Pass Category and If Course Marked as Pass/Fail
                                        strGrdSysDetailId = objDB.GetPassGrade(strGradeSystemId)
                                        boolScoreIsInRange = True
                                    ElseIf intPass = 0 And boolPF Then
                                        'If Grade falls under Fail Category and If Course Marked as Pass/Fail
                                        strGrdSysDetailId = objDB.GetFailGrade(strGradeSystemId)
                                        boolScoreIsInRange = True
                                    Else
                                        Try
                                            strGrdSysDetailId = dr1("GrdSysDetailId").ToString
                                        Catch ex As System.Exception
                                            strGrdSysDetailId = Guid.Empty.ToString
                                        End Try
                                        boolScoreIsInRange = True
                                    End If
                                End If
                            Next
                            'Troy:3/1/2013 I commented out the following code as it is no longer necessary. 
                            'Since we are now adding 0.99 to the max value, the final score will always fall within one of the grade scale ranges.

                            'If boolScoreIsInRange = False Then
                            '    If SingletonAppSettings.AppSettings("GradeRounding").ToString.ToLower = "yes" Then
                            '        intFinalScore = Math.Round(intFinalScore, 0)
                            '    Else
                            '        intFinalScore = Math.Round(intFinalScore, 2)
                            '    End If
                            '    For Each dr1 As DataRow In dtGrdScaleDtls.Rows
                            '        intMinVal = dr1("MinVal")
                            '        IntMaxVal = dr1("MaxVal")
                            '        intPass = dr1("Pass")
                            '        If intFinalScore >= intMinVal And intFinalScore <= IntMaxVal Then
                            '            If intPass = 1 And boolPF Then
                            '                'If Grade falls under Pass Category and If Course Marked as Pass/Fail
                            '                strGrdSysDetailId = objDB.GetPassGrade(strGradeSystemId)
                            '                boolScoreIsInRange = True
                            '            ElseIf intPass = 0 And boolPF Then
                            '                'If Grade falls under Fail Category and If Course Marked as Pass/Fail
                            '                strGrdSysDetailId = objDB.GetFailGrade(strGradeSystemId)
                            '                boolScoreIsInRange = True
                            '            Else
                            '                Try
                            '                    strGrdSysDetailId = dr1("GrdSysDetailId").ToString
                            '                Catch ex As System.Exception
                            '                    strGrdSysDetailId = Guid.Empty.ToString
                            '                End Try
                            '                boolScoreIsInRange = True
                            '            End If
                            '        End If
                            '    Next
                            'End If
                        Else
                            For Each dr1 As DataRow In dtGrdScaleDtls.Rows
                                intMinVal = dr1("MinVal")
                                intMaxVal = dr1("MaxVal")
                                intPass = dr1("Pass")
                                If intFinalScore >= intMinVal And intFinalScore <= (intMaxVal + 0.99) Then
                                    strGrdSysDetailId = dr1("GrdSysDetailId").ToString
                                    strGradeSystemId = dr1("GrdSystemId").ToString
                                    boolScoreIsInRange = True
                                    Exit For
                                End If
                            Next

                            'Troy:3/1/2013 I commented out the following code as it is no longer necessary. 
                            'Since we are now adding 0.99 to the max value, the final score will always fall within one of the grade scale ranges.

                            'If boolScoreIsInRange = False Then
                            '    If SingletonAppSettings.AppSettings("GradeRounding").ToString.ToLower = "yes" Then
                            '        intFinalScore = Math.Round(intFinalScore, 0)
                            '    Else
                            '        intFinalScore = Math.Round(intFinalScore, 2)
                            '    End If
                            '    For Each dr1 As DataRow In dtGrdScaleDtls.Rows
                            '        intMinVal = dr1("MinVal")
                            '        IntMaxVal = dr1("MaxVal")
                            '        intPass = dr1("Pass")
                            '        If intFinalScore >= intMinVal And intFinalScore <= IntMaxVal Then
                            '            strGrdSysDetailId = dr1("GrdSysDetailId").ToString
                            '            strGradeSystemId = dr1("GrdSystemId").ToString
                            '            boolScoreIsInRange = True
                            '            Exit For
                            '        End If
                            '    Next
                            'End If

                            'Check If Course Has Been Marked as Satisfactory/Unsatisfactory
                            boolSU = objDB.IsCourseSU(ClsSectionId.GetValue(x).ToString)



                            If intPass = 1 And boolSU Then
                                'If Grade falls under Pass Category and If Course Marked as 
                                'Satisfactory/UnSatisfactory
                                strGrdSysDetailId = objDB.GetPassSatisfactoryGrade(strGradeSystemId)

                            ElseIf intPass = 0 And boolSU Then
                                'If Grade falls under Fail Category and If Course Marked as 
                                'Satisfactory/UnSatisfactory
                                strGrdSysDetailId = objDB.GetNoPassUnSatisfactoryGrade(strGradeSystemId)
                            End If

                            If intPass = 1 And boolPF Then
                                'If Grade falls under Pass Category and If Course Marked as 
                                'Pass/Fail
                                strGrdSysDetailId = objDB.GetPassGrade(strGradeSystemId)
                            ElseIf intPass = 0 And boolPF Then
                                'If Grade falls under Fail Category and If Course Marked as 
                                'Pass/Fail
                                strGrdSysDetailId = objDB.GetFailGrade(strGradeSystemId)
                            End If
                        End If
                    End If
                End If


                If dr("GradesComplete") = "No" And thereWasAnError = False Then
                    InCompleteErrorMessage &= "     " & strFirstName & " " & strLastName & vbLf
                End If
                'Construct TransferGBWInfo object

                transferGBW = New TransferGBWInfo
                With transferGBW
                    .ClsSectionId = ClsSectionId.GetValue(x).ToString
                    .StuEnrollId = strStuEnrollId
                    If GradesFormat.ToString.ToLower = "numeric" Then
                        .Score = intFinalScore
                    End If
                    .GrdSysDetailId = strGrdSysDetailId
                End With
                'Insert into arrayList for batch update at the end
                'Added by Balaji on 12/29/2009 to fix issue 18281
                'Update score only when the student is graded
                If thereWasAnError = False And dr("GradesComplete") = "Yes" Then
                    If boolDoesClassHaveClinicServices = False Then
                        updateResults.Add(transferGBW)
                    Else
                        If boolIsClinicCompletlySatisfied Then
                            updateResults.Add(transferGBW)
                        Else
                            InCompleteErrorMessage &= "     " & strFirstName & " " & strLastName & vbLf
                        End If
                    End If
                End If

                'Update arResults set GrdSysDetailId = ? where StuEnrollId = ? and TestId =? and GrdSysDetailId is Null
                'Dim decGPA As Decimal = 0.0
                'Dim decCredAtt As Decimal = 0.0
                'Dim decCredEarned As Decimal = 0.0
                'decGPA = (New GraduateAuditFacade).GetGPA(strStuEnrollId)
                'decCredAtt = (New GraduateAuditFacade).GetCreditsAttempted(strStuEnrollId)
                'decCredEarned = (New GraduateAuditFacade).GetCreditsEarned(strStuEnrollId)
                'If RegentConfig.ToString.Trim = "yes" Then
                '    Dim regDB As New regentDB
                '    Dim strStudentId As String = regDB.getStudentId(strStuEnrollId)
                '    Dim strFilename As String = AdvantageCommonValues.getStudentBatchFileName()
                '    regDB.AddAcademicXML(strStudentId, strFilename, "", decCredAtt, decCredEarned, decGPA)
                'End If
            Next 'End of iteration on student table

            If thereWasAnError Then
                'There was some error
                'Insert into arrayList for batch update at the end (SET arClassSections.IsGraded=0)
                isNotGradedClsSection.Add(ClsSectionId.GetValue(x).ToString)
                'Update arClassSections set IsGraded = 0 where ClsSectionId = ? and TermId =? 
            Else
                'There was NO error
                'Insert into arrayList for batch update at the end (SET arClassSections.IsGraded=1)
                gradedClsSection.Add(ClsSectionId.GetValue(x).ToString)
                'Update arClassSections set IsGraded = 1 where ClsSectionId = ? and TermId =?
            End If

            Try
                If errorMessage.Length >= 1 Then
                    strClsSectionMessage &= vbLf
                    strClsSectionMessage &= selectedCourseCode.GetValue(x) & " - " & clsSectionDescrip.GetValue(x).ToString & vbLf & vbLf
                    strClsSectionMessage &= errorMessage
                    errorMessage = ""
                End If
                If InCompleteErrorMessage.Length >= 1 Then
                    strClsSectionIncompleteMessage &= vbLf
                    strClsSectionIncompleteMessage &= selectedCourseCode.GetValue(x) & " - " & clsSectionDescrip.GetValue(x).ToString & vbLf & vbLf
                    strClsSectionIncompleteMessage &= InCompleteErrorMessage
                    InCompleteErrorMessage = ""
                End If
            Catch ex As System.Exception
                'Do Nothing
            End Try

            'Increment value of x
            x = x + 1
        End While 'End of iteration on the classSections collection

        Dim strReturnMessage As String = ""
        'Do updates within a transaction
        If updateResults.Count > 0 Or isNotGradedClsSection.Count > 0 Or gradedClsSection.Count > 0 Then
            'Update the arResults and the arClassSections tables
            strReturnMessage = objDB.TransferGBW(TermId, updateResults, isNotGradedClsSection, gradedClsSection, user, GradesFormat)
        End If
        If strReturnMessage = "" Then
            Try
                If strClsSectionMessage.Length >= 1 Then
                    strReturnMessage = "The following students have none of their components graded" & vbLf
                    strReturnMessage &= strClsSectionMessage
                End If
                If strClsSectionIncompleteMessage.Length >= 1 Then
                    strReturnMessage &= vbLf & "The following students have some of their components graded" & vbLf
                    strReturnMessage &= strClsSectionIncompleteMessage
                Else
                    '' Grades were not transferred successfully"
                End If
            Catch ex As System.Exception
                'Do Nothing
            End Try
        End If
        Return strReturnMessage
    End Function

End Class
