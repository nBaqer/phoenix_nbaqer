﻿Public Class LOAExpirationFacade
    Inherits BaseReportFacade
#Region "Public Methods"
    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim loaExp As New LOAExpirationDB
        Dim ds As DataSet
        ds = BuildReportDataset(loaExp.GetLOAExpiration(rptParamInfo), loaExp.StudentIdentifier)
        Return ds
    End Function
#End Region
#Region "Protected Methods"
    Protected Function BuildReportDataset(ByRef ds As DataSet, ByVal StudentIdentifier As String)
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        Dim temp As String
        For Each row As DataRow In ds.Tables("PendingLOAExpiration").Rows
            If StudentIdentifier = "SSN" Then
                If Not (row("StudentIdentifier") Is System.DBNull.Value) Then
                    If row("StudentIdentifier") <> "" Then
                        temp = row("StudentIdentifier")
                        row("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                    End If
                End If
            End If
        Next
        Return ds
    End Function
#End Region
End Class
