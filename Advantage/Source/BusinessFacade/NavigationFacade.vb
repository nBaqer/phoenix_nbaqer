' ===============================================================================
' FAME AdvantageV1.BusinessFacade
'
' NavigationFacade.vb
'
' Navigation Services Interface (Facade). 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Imports System.Xml
Public Class NavigationFacade

    Public Function GetNavigationTree(ByVal filter As String, ByVal schedulingMethods As Integer, campusid As String) As XmlDocument

        '   Instantiate DAL component
        With New NavigationNodesDB

            '   return XmlDocument
            Return .GetNavigationTree(filter, schedulingMethods, campusid)

        End With

    End Function
    Public Function GetAllModules() As DataSet
        With New ModulesDB
            Return .GetAllModules()
        End With
    End Function
    Public Function GetAllResourceModules() As DataSet
        With New ModulesDB
            Return .GetAllResourceModules()
        End With
    End Function
    Public Function GetAllResourcesForStudentTabs() As DataSet
        With New ModulesDB
            Return .GetAllResourcesForStudentTabs()
        End With
    End Function

    Public Function GetAllStudentModules() As DataSet
        With New ModulesDB
            Return .GetAllStudentModules()
        End With
    End Function
    Public Function GetAllStudentResources() As DataSet
        With New ModulesDB
            Return .GetAllStudentResources()
        End With
    End Function
    Public Function GetPagesByModulePageName(ByVal ResourceId As Integer, ByVal PageName As String) As DataSet
        With New NavigationNodesDB
            Return .GetPagesByModulePageName(ResourceId, PageName)
        End With
    End Function

    Public Function GetStudentPagesByModulePageName(ByVal ResourceId As Integer, ByVal PageName As String) As DataSet
        With New NavigationNodesDB
            Return .GetStudentPagesByModulePageName(ResourceId, PageName)
        End With
    End Function
    Public Function GetAllNodeDetails(ByVal ResourceId As String) As DataSet
        With New NavigationNodesDB
            Return .GetAllNodeDetails(ResourceId)
        End With
    End Function
    Public Function GetStudentNodeDetails(ByVal ResourceId As String) As DataSet
        With New NavigationNodesDB
            Return .GetStudentNodeDetails(ResourceId)
        End With
    End Function
    Public Function GetHierarchyIndex(ByVal ResourceId As String) As Integer
        With New NavigationNodesDB
            Return .GetHierarchyIndex(ResourceId)
        End With
    End Function
    Public Function GetAllParentNodes() As String
        With New NavigationNodesDB
            Return .GetAllParentNodes()
        End With
    End Function
    Public Function GetAllCommonTaskNodes() As String
        With New NavigationNodesDB
            Return .GetAllCommonTaskNodes()
        End With
    End Function
    Public Function InsertNode(ByVal NodeInfo As NavigationNodeInfo, ByVal User As String) As String
        With New NavigationNodesDB
            Return .InsertNode(NodeInfo, User)
        End With
    End Function
    Public Function InsertStudentNode(ByVal NodeInfo As NavigationNodeInfo, ByVal User As String) As String
        With New NavigationNodesDB
            Return .InsertStudentNode(NodeInfo, User)
        End With
    End Function
    Public Function GetNumberOfAdHocReportsUsedByModule(ByVal modul As String, ByVal campusId As String) As Integer
        With New NavigationNodesDB
            Return .GetNumberOfAdHocReportsUsedByModule(modul, campusId)
        End With
    End Function
    Public Function GetPagesByModulePageNameSP(ByVal ResourceId As Integer, ByVal PageName As String) As DataSet
        With New NavigationNodesDB
            Return .GetPagesByModulePageNameSP(ResourceId, PageName)
        End With
    End Function
    Public Function GetModulesByResource(ByVal ResourceId As Integer) As DataSet
        With New NavigationNodesDB
            Return .GetModulesByResource(ResourceId)
        End With
    End Function
    Public Sub InsertNodeSP(ByVal AD_ModuleId As Boolean, ByVal AR_ModuleId As Boolean, _
                               ByVal SA_ModuleId As Boolean, ByVal PL_ModuleId As Boolean, _
                               ByVal FA_ModuleId As Boolean, ByVal HR_ModuleId As Boolean, _
                               ByVal FAC_ModuleId As Boolean, ByVal SY_ModuleId As Boolean, _
                               ByVal MaintenanePageId As Integer, ByVal UserName As String)
        Dim NavNode As New NavigationNodesDB
        NavNode.InsertNodeSP(AD_ModuleId, AR_ModuleId, _
                               SA_ModuleId, PL_ModuleId, _
                               FA_ModuleId, HR_ModuleId, _
                               FAC_ModuleId, SY_ModuleId, _
                               MaintenanePageId, UserName)
    End Sub
End Class

