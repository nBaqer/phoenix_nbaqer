Imports System.data
Imports System.Data.OleDb
'Imports FAME.AdvantageV1.BusinessRules
Imports FAME.AdvantageV1.DataAccess
Public Class ClassSectionFacade
    Public Function InitialLoadPage(ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        'Dim da As OleDbDataAdapter
        'Dim olddt As DataTable
        'Dim newdt As DataTable
        Dim db As New ClassSectionDB

        ds = db.GetDropDownList(CampusId)

        '     ds = FormatDateField(ds)
        'Add the concatenated FullName column to the datatable before sending it to the web page
        'ds = ConcatenateEmployeeLastFirstName(ds)
        ds = ConcatenateTimeOnly(ds)

        'ds = ConcatenateCourseCodeAndDescrip(ds)
        'ds = ConcatenateCourseCodeAndDescrip(ds, "CourseDT")

        Return ds
    End Function
    Public Function InitialLoadPage(ByVal CampusId As String, ByVal prgVerId As String) As DataSet
        Dim ds As New DataSet
        'Dim da As OleDbDataAdapter
        'Dim olddt As DataTable
        'Dim newdt As DataTable
        Dim db As New ClassSectionDB

        ds = db.GetDropDownList(CampusId, prgVerId)

        '     ds = FormatDateField(ds)
        'Add the concatenated FullName column to the datatable before sending it to the web page
        'ds = ConcatenateEmployeeLastFirstName(ds)
        ds = ConcatenateTimeOnly(ds)

        'ds = ConcatenateCourseCodeAndDescrip(ds)
        'ds = ConcatenateCourseCodeAndDescrip(ds, "CourseDT")

        Return ds
    End Function
    Public Function ConcatenateEmployeeLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("EmpId")}
        End With
        If ds.Tables("InstructorDT").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                row("FullName") = row("LastName") & ", " & row("FirstName")
            Next
        End If
        Return ds
    End Function
    Public Function ConcatenateTimeOnly(ByVal ds As DataSet) As DataSet

        Dim tbl As New DataTable
        Dim row As DataRow
        Dim ListDate As DateTime
        Dim a As DateTime
        tbl = ds.Tables("TimeIntervalList")
        If ds.Tables("TimeIntervalList").Rows.Count > 0 Then
            'Add a new column called TimeOnly to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("TimeOnly", GetType(String))
            For Each row In tbl.Rows
                ListDate = row("TimeIntervalDescrip")
                row("TimeOnly") = ListDate.ToShortTimeString
                a = ListDate.ToLongTimeString()
            Next
        End If
        Return ds
    End Function
    Public Function ConcatenateCourseCodeAndDescrip(ByVal ds As DataSet, ByVal tableName As String) As DataSet
        Dim dt As DataTable = ds.Tables(tableName)

        With dt
            '.PrimaryKey = New DataColumn() {.Columns("ReqId")}

            If .Rows.Count > 0 Then
                'Add a new column called CodeAndDescrip to the datatable 
                Dim col As DataColumn = .Columns.Add("CodeAndDescrip", Type.GetType("System.String"))

                '   column contains course code and description in this format: "(Code) Descrip"
                col.AllowDBNull = True
                col.Expression = "'(' + Code + ') ' + Descrip"
            End If
        End With

        Return ds
    End Function
    Public Function FormatDateField(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        Dim time As DateTime
        tbl = ds.Tables("TimeIntervalList")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("TimeIntervalID")}
        End With
        If ds.Tables("TimeIntervalList").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("Time", GetType(String))

            For Each row In tbl.Rows
                time = row("TimeIntervalDescrip")
                time = Convert.ToString(time.ToString("t"))
                row("Time") = time
            Next
        End If
        Return ds
    End Function
    Public Function GetCampRooms(ByVal CampusId As String) As DataSet

        '   Instantiate DAL component
        Dim db As New ClassSectionDB

        '   get the dataset with all degrees
        Return db.GetCampusRooms(CampusId)

    End Function
    Public Function GetDaysInPeriod(ByVal PeriodId As String) As DataSet
        Dim db As New ClassSectionDB

        Return db.GetDaysInPeriod(PeriodId)
    End Function

    Public Function GetMaxCapOnRoom(ByVal BldgId As String)
        With New ClassSectionDB
            Return .GetMaxCapOnRoom(BldgId)
        End With
    End Function
    Public Function GetStartEndDates(ByVal TermId As String) As String

        '   Instantiate DAL component
        Dim db As New ClassSectionDB
        Dim str As String
        '   get the dataset with all degrees
        str = db.GetStartEndDates(TermId)
        Return str
    End Function
    ''Optional parameter CohortStartDate added by Saraswathi
    Public Function PopulateDataList(ByVal Term As String, ByVal Course As String, ByVal Instructor As String, ByVal Shift As String, ByVal CampusId As String, Optional ByVal CohortStartDate As String = "") As DataSet
        Dim db As New ClassSectionDB
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter

        ds = db.PopulateClsSectDataList(Term, Course, Instructor, Shift, CampusId, , CohortStartDate)

        'ds = ConcatenateStudentLastFirstName(ds)

        ds = ConcatenateCourseCodeAndDescrip(ds, "ClassSection")

        Return ds
    End Function

    Public Function GetTermModuleSummary(ByVal prgVerId As String, ByVal status As String, ByVal campusId As String) As DataSet
        Dim db As New ClassSectionDB

        If prgVerId = "" Then
            Return db.GetTermModuleSummary(status, campusId)
        Else
            Return db.GetTermModuleSummary(prgVerId, status, campusId)
        End If

    End Function

    Public Function AddTermModule(ByVal termModuleInfo As TermModuleInfo, ByVal user As String) As ClsSectionResultInfo
        Dim db As New ClassSectionDB
        Dim db2 As New TermsDB
        Dim cInfo As New ClsSectionResultInfo

        db2.AddTermInfo(termModuleInfo.TermInfo, user)

        cInfo = db.AddClassSection(termModuleInfo.ClsSectionInfo, user)

        Return cInfo
    End Function

    Public Function UpdateTermModule(ByVal termModuleInfo As TermModuleInfo, ByVal user As String) As ClsSectionResultInfo
        Dim db As New ClassSectionDB
        Dim db2 As New TermsDB
        Dim cInfo As New ClsSectionResultInfo

        db2.UpdateTermInfo(termModuleInfo.TermInfo, user)

        cInfo = db.UpdateClassSection(termModuleInfo.ClsSectionInfo, user)

        Return cInfo
    End Function

    Public Function AddClassSection(ByVal ClassSection As ClassSectionInfo, ByVal user As String) As ClsSectionResultInfo
        Dim db As New ClassSectionDB

        Return db.AddClassSection(ClassSection, user)

    End Function
    Public Function UpdateClassSection(ByVal ClassSection As ClassSectionInfo, ByVal user As String) As ClsSectionResultInfo
        Dim db As New ClassSectionDB
        ''  Dim msg As String
        Return db.UpdateClassSection(ClassSection, user)
    End Function
    Public Function DeleteClassSection(ByVal ClsSectId As Guid, ByVal modDate As DateTime) As String
        Dim db As New ClassSectionDB
        Return db.DeleteClassSection(ClsSectId, modDate)
    End Function
    Public Function DeleteClassSection(ByVal ClsSectId As Guid, ByVal modDate As DateTime, ByVal termId As String) As String
        Dim db As New ClassSectionDB
        Return db.DeleteClassSection(ClsSectId, modDate, termId)
    End Function
    Public Function AddOverridenConflicts(ByVal clsSectionId As String, ByVal clsSectionList As ArrayList, ByVal arrDeleteList As ArrayList, ByVal user As String) As String
        Return (New ClassSectionDB).AddOverridenConflicts(clsSectionId, clsSectionList, arrDeleteList, user)
    End Function

    Public Sub DeleteTerm(ByVal termId As String)
        Dim db As New ClassSectionDB

        db.DeleteTerm(termId)

    End Sub

    Public Function AddClsSectMeeting(ByVal ClsSectMtgArrList As ArrayList, ByVal allowConflicts As Boolean, ByVal clsSectionId As String, ByVal user As String) As ArrayList
        Dim db As New ClassSectionDB
        Dim ClsSectMeeting As ClsSectMeetingInfo
        Dim ErrMsgObj As New ErrMsgInfo
        Dim ArrList As New ArrayList
        Dim count As Integer = 0
        Dim validation As New DALExceptions
        Dim msg As String
        ''  Dim dbmsg As String
        Dim resinfo As ClsSectMtgIdObjInfo


        For Each ClsSectMeeting In ClsSectMtgArrList
            count = count + 1
            'if object is not empty
            'then add each single record
            If (IsObjectNotEmpty(ClsSectMeeting)) Then

                'Added by BN to fix the problem where there was no instructor selected
                'but was still getting an instructor collision message.
                If (ClsSectMeeting.InstructorId = "") Then
                    ClsSectMeeting.InstructorId = Guid.Empty.ToString
                End If

                'Check for conflicts
                If Not allowConflicts Then
                    msg = validation.TimeCollisions(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.Day.ToString, ClsSectMeeting.STimeDescrip, ClsSectMeeting.ETimeDescrip, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)
                Else
                    msg = validation.TimeCollisionsByClsSectionId(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.Day.ToString, ClsSectMeeting.STimeDescrip, ClsSectMeeting.ETimeDescrip, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, clsSectionId)
                End If

                If msg <> "" Then ' ERROR MSG
                    'Return msg
                    ErrMsgObj.ErrMsg = msg
                    ArrList.Add(ErrMsgObj)
                Else
                    'Generate a new guid for the insert
                    ClsSectMeeting.ClsSectMtgId = System.Guid.NewGuid
                    'Add the single record(one ClsSectMeeting object) to the database
                    resinfo = db.AddSingleClsSectMeeting(ClsSectMeeting, user)

                    'Add this new guid to an arraylist, so it can be used later on
                    'for update purposes.
                    resinfo.count = count
                    resinfo.txtboxname = "txtClsSectMeetingId"

                    ArrList.Add(resinfo)
                End If

            End If

        Next
        Return ArrList

    End Function
    Public Function UpdateClsSectMeeting(ByVal ClsSectMtgArrList As ArrayList, ByVal allowConflicts As Boolean, ByVal clsSectionId As String, ByVal user As String) As ArrayList
        Dim db As New ClassSectionDB
        Dim ClsSectMeeting As ClsSectMeetingInfo
        Dim count As Integer = 0
        Dim ClsSectMtgIdArrLst As New ArrayList
        Dim msg As String
        Dim validation As New DALExceptions
        Dim ErrMsgObj As New ErrMsgInfo
        Dim ArrList As New ArrayList
        '' Dim dbmsg As String
        Dim obj As New ClsSectMtgIdObjInfo

        For Each ClsSectMeeting In ClsSectMtgArrList
            'Increment count 
            count = count + 1

            'if object is not empty
            'then add each single record
            If Left(ClsSectMeeting.ClsSectMtgId.ToString, 6) <> "000000" Then
                If Left(ClsSectMeeting.Day.ToString, 6) = "000000" Then
                    'Delete the meeting
                    db.DeleteOneClsSectMeeting(ClsSectMeeting.ClsSectMtgId)
                Else

                    'Added by BN to fix the problem where there was no instructor selected
                    'but was still getting an instructor collision message.
                    If (ClsSectMeeting.InstructorId = "") Then
                        ClsSectMeeting.InstructorId = Guid.Empty.ToString
                    End If


                    'Check for conflicts
                    If Not allowConflicts Then
                        'Commented by Balaji on Oct 28 2011 
                        'There is no need to check for collisions as its already being handled in code behind page
                        'msg = validation.TimeCollisions(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.Day.ToString, ClsSectMeeting.STimeDescrip, ClsSectMeeting.ETimeDescrip, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)
                    Else
                        msg = validation.TimeCollisionsByClsSectionId(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.Day.ToString, ClsSectMeeting.STimeDescrip, ClsSectMeeting.ETimeDescrip, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, clsSectionId)
                    End If


                    If msg <> "" Then ' ERROR MSG
                        'Return msg
                        ErrMsgObj.ErrMsg = msg
                        ArrList.Add(ErrMsgObj)
                        Return ArrList
                    Else

                        'If a clssection meeting id already exists, this is an update
                        'Add the single record(one ClsSectMeeting object) to the database
                        obj = db.UpdateSingleClsSectMtg(ClsSectMeeting, user)
                        obj.ClsSectMtgId = ClsSectMeeting.ClsSectMtgId
                        obj.count = count
                        obj.txtboxname = "txtClsSectMeetingId"

                        ArrList.Add(obj)
                    End If

                End If


            Else 'Fresh Insert
                If (IsObjectNotEmpty(ClsSectMeeting)) Then


                    'Added by BN to fix the problem where there was no instructor selected
                    'but was still getting an instructor collision message.
                    If (ClsSectMeeting.InstructorId = "") Then
                        ClsSectMeeting.InstructorId = Guid.Empty.ToString
                    End If


                    If Not allowConflicts Then
                        ' msg = validation.TimeCollisions(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.Day.ToString, ClsSectMeeting.STimeDescrip, ClsSectMeeting.ETimeDescrip, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)
                    Else
                        msg = validation.TimeCollisionsByClsSectionId(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.Day.ToString, ClsSectMeeting.STimeDescrip, ClsSectMeeting.ETimeDescrip, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, clsSectionId)
                    End If


                    If msg <> "" Then ' ERROR MSG
                        'Return msg
                        ErrMsgObj.ErrMsg = msg
                        ArrList.Add(ErrMsgObj)

                    Else

                        'Generate a new guid for the insert
                        ClsSectMeeting.ClsSectMtgId = System.Guid.NewGuid
                        obj = db.AddSingleClsSectMeeting(ClsSectMeeting, user)

                        'Add this new guid to an arraylist, so it can be used later on
                        'for update purposes.
                        obj.ClsSectMtgId = ClsSectMeeting.ClsSectMtgId
                        obj.count = count
                        obj.txtboxname = "txtClsSectMeetingId"

                        ArrList.Add(obj)
                    End If
                End If

            End If

        Next
        Return ArrList
    End Function
    Public Sub DeleteClsSectMeeting(ByVal ClsSectId As Guid)
        Dim db As New ClassSectionDB
        db.DeleteClsSectMeeting(ClsSectId)
    End Sub

    Private Function IsObjectNotEmpty(ByVal ClsSectMeeting As ClsSectMeetingInfo) As Boolean

        Dim obj As Boolean = False

        If Left(ClsSectMeeting.Day.ToString, 6) <> "000000" Then
            obj = True
        End If
        If Left(ClsSectMeeting.Room.ToString, 6) <> "000000" Then
            obj = True
        End If
        If Left(ClsSectMeeting.StartTime.ToString, 6) <> "000000" Then
            obj = True
        End If '
        If Left(ClsSectMeeting.EndTime.ToString, 6) <> "000000" Then
            obj = True
        End If '
        Return obj
    End Function
    Public Function GetOneClassSection(ByVal ClsSectId As Guid) As ClassSectionInfo
        Dim db As New ClassSectionDB
        Dim SingleClsSect As ClassSectionInfo
        Dim ds As DataSet
        ''  Dim ds1 As DataSet
        ds = db.GetSingleClsSect(ClsSectId)
        SingleClsSect = ConvertSingleClsSectToObject(ds)
        Return SingleClsSect
    End Function

    Public Function GetClassSection(ByVal ClsSectId As Guid) As DataSet

        Return (New ClassSectionDB).GetClassSection(ClsSectId)

    End Function

    Public Function GetOverridenConflicts(ByVal ClsSectId As String) As DataTable

        Return (New ClassSectionDB).GetOverridenConflicts(ClsSectId)

    End Function
    Public Function DeleteOverridenConflicts(ByVal ClsSectId As String)
        Dim obj As New ClassSectionDB
        obj.DeleteOverridenConflicts(ClsSectId)

    End Function
    Public Function DeleteOverridenConflicts(ByVal arrDeleteList As ArrayList) As String

        Dim obj As New ClassSectionDB
        Return obj.DeleteOverridenConflicts(arrDeleteList)

    End Function
    Public Function GetTermModuleDetails(ByVal termId As String) As TermModuleInfo
        Dim db As New ClassSectionDB
        Dim ds1 As New DataSet
        Dim ds2 As New DataSet
        Dim dr1, dr2 As DataRow
        Dim termObject As New TermInfo
        Dim ClassSectionObject As New ClassSectionInfo

        ds1 = db.GetSingleTerm(termId)
        ds2 = db.GetClsSectForTermModule(termId)

        dr1 = ds1.Tables(0).Rows(0)
        termObject.TermId = dr1("TermId").ToString
        termObject.StartDate = dr1("StartDate").ToShortDateString
        termObject.EndDate = dr1("EndDate").ToShortDateString
        termObject.StatusId = dr1("StatusId").ToString
        termObject.Description = dr1("TermDescrip").ToString
        termObject.Code = dr1("TermCode")
        termObject.IsModule = True

        dr2 = ds2.Tables(0).Rows(0)
        ClassSectionObject.ClsSectId = dr2("ClsSectionId")

        If dr2("InstructorId").ToString() <> "" Then
            ClassSectionObject.InstructorId = dr2("InstructorId")
        End If

        If dr2("ShiftId").ToString() <> "" Then
            ClassSectionObject.ShiftId = dr2("ShiftId")
        End If

        ClassSectionObject.CampusId = dr2("CampusId")
        ClassSectionObject.GrdScaleId = dr2("GrdScaleId")
        ClassSectionObject.Section = dr2("ClsSection")
        ClassSectionObject.StartDate = dr2("StartDate").ToShortDateString
        ClassSectionObject.EndDate = dr2("EndDate").ToShortDateString
        ClassSectionObject.MaxStud = dr2("MaxStud")
        ClassSectionObject.TermId = dr2("TermId")
        ClassSectionObject.CourseId = dr2("ReqId")
        ClassSectionObject.ModDate = dr2("ModDate")

        Return New TermModuleInfo(termObject, ClassSectionObject)

    End Function

    Public Function GetClsSectMtgs(ByVal ClsSectId As Guid) As ArrayList
        Dim db As New ClassSectionDB
        Dim ClsSectArrayList As New ArrayList

        Dim ds As DataSet
        '' Dim ds1 As DataSet
        ds = db.GetClsSectMtgs(ClsSectId)
        ClsSectArrayList = ConvertClsSectMtgsToArrList(ds)
        Return ClsSectArrayList
    End Function


    Public Function ConvertSingleClsSectToObject(ByVal ds As DataSet) As ClassSectionInfo
        Dim ClsSectObject As New ClassSectionInfo
        Dim dt, dt2 As DataTable
        Dim row As DataRow
        dt = ds.Tables("SingleClsSect")
        dt2 = ds.Tables("OverridenConflicts")
        'If ds.Tables.Count > 1 Then
        '    'Error - it returned more than one record
        'Else
        For Each row In dt.Rows
            ClsSectObject.ClsSectId = row("ClsSectionId")
            If row("TermId").ToString <> "" Then
                ClsSectObject.TermId = row("TermId")
            End If
            If row("ReqId").ToString <> "" Then
                ClsSectObject.CourseId = row("ReqId")
            End If
            If row("InstructorId").ToString <> "" Then
                ClsSectObject.InstructorId = row("InstructorId")
            End If
            If row("ShiftId").ToString <> "" Then
                ClsSectObject.ShiftId2 = row("ShiftId").ToString
            End If
            If row("CampusId").ToString <> "" Then
                ClsSectObject.CampusId = row("CampusId")
            End If
            If row("GrdScaleId").ToString <> "" Then
                ClsSectObject.GrdScaleId = row("GrdScaleId")
            End If
            ClsSectObject.Section = row("ClsSection")
            ClsSectObject.StartDate = row("StartDate")
            ClsSectObject.EndDate = row("EndDate")
            ClsSectObject.MaxStud = row("MaxStud")
            ClsSectObject.ModUser = row("ModUser")
            ClsSectObject.ModDate = row("ModDate")
            ClsSectObject.Term = row("Term")
            ClsSectObject.Course = row("Course")
            If row("Instructor").ToString <> "" Then
                ClsSectObject.Instructor = row("Instructor")
            End If
            If row("Shift").ToString <> "" Then
                ClsSectObject.Shift = row("Shift")
            End If
            ClsSectObject.Campus = row("Campus")
            ClsSectObject.GrdScale = row("GrdScale")

            If dt2.Rows.Count > 0 Then
                ClsSectObject.AllowOverrideConflicts = True
            End If

            If row("CohortStartDate").ToString <> "" Then
                ClsSectObject.CohortStartDate = row("CohortStartDate")
            End If
            If row("InstrGrdBkWgtId").ToString <> "" Then
                ClsSectObject.InstrGrdBkWgtId = row("InstrGrdBkWgtId")
            End If

            ' 09/12/2011 Janet Robinson - Clock Hours Project Added new fields
            Try
                ClsSectObject.AllowEarlyIn = row("AllowEarlyIn")
            Catch ex As Exception
                ClsSectObject.AllowEarlyIn = False
            End Try
            Try
                ClsSectObject.AllowLateOut = row("AllowLateOut")
            Catch ex As Exception
                ClsSectObject.AllowLateOut = False
            End Try
            Try
                ClsSectObject.AllowExtraHours = row("AllowExtraHours")
            Catch ex As Exception
                ClsSectObject.AllowExtraHours = False
            End Try
            Try
                ClsSectObject.CheckTardyIn = row("CheckTardyIn")
            Catch ex As Exception
                ClsSectObject.CheckTardyIn = False
            End Try
            Try
                ClsSectObject.MaxInBeforeTardy = row("MaxInBeforeTardy")
            Catch ex As Exception
            End Try
            Try
                ClsSectObject.AssignTardyInTime = row("AssignTardyInTime")
            Catch ex As Exception

            End Try

        Next
        'End If
        Return ClsSectObject
    End Function

    Public Function ConvertClsSectMtgsToArrList(ByVal ds As DataSet) As ArrayList

        'Dim count As Integer
        'Dim strControl As String
        Dim ClsSectArrayList As New ArrayList
        Dim dt As DataTable
        Dim row As DataRow
        dt = ds.Tables("ClsSectMtgs")
        If ds.Tables.Count > 1 Then
            'Error - it returned more than one record
        Else
            'Loop thru the controls, find the appropriate dropdown(Day/Room/TimeInterval)
            'and set the object properties based on the ddl's that are set.
            For Each row In dt.Rows
                Dim ClsSectMtgObject As New ClsSectMeetingInfo
                If row("WorkDaysId").ToString <> "" Then
                    ClsSectMtgObject.Day = row("WorkDaysId")
                End If
                If row("TimeIntervalId").ToString <> "" Then
                    ClsSectMtgObject.Room = row("RoomId")
                End If
                If row("TimeIntervalId").ToString <> "" Then
                    ClsSectMtgObject.StartTime = row("TimeIntervalId")
                End If

                If row("EndIntervalId").ToString <> "" Then
                    ClsSectMtgObject.EndTime = row("EndIntervalId")
                End If
                ClsSectMtgObject.ClsSectMtgId = row("ClsSectMeetingId")
                ClsSectMtgObject.ModDate = row("ModDate")
                ' 09/28/2011 Janet Robinson - Clock Hours Project Added new field
                If row("InstructionTypeId").ToString <> "" Then
                    ClsSectMtgObject.InstructionTypeId = row("InstructionTypeId")
                End If
                ClsSectMtgObject.AttUsedCnt = row("AttUsedCnt")

                'Add the object to the array list
                ClsSectArrayList.Add(ClsSectMtgObject)
            Next
        End If

        Return ClsSectArrayList
    End Function

    Public Function SendErrMsgToUI(ByVal msg As String) As String

    End Function
    Public Function DoResultsExistFrClsSect(ByVal ClsSect As String) As Integer
        Dim db As New ClassSectionDB

        Return db.DoesClsSectHaveGradesPosted(ClsSect)
    End Function
    Public Function StudentsRegisteredCount(ByVal ClsSect As String) As Integer
        Dim db As New ClassSectionDB

        Return db.StudentsRegisteredCount(ClsSect)
    End Function

    Public Function ValidateClassSectionDates(ByVal startDate As Date, ByVal endDate As Date, ByVal termId As String) As String
        Dim termSDate As Date
        Dim termEDate As Date
        Dim tDB As New TermsDB
        Dim tInfo As New TermInfo
        Dim errMsg As String = ""

        'Get the term start and end dates
        tInfo = tDB.GetTermInfo(termId)

        termSDate = tInfo.StartDate
        termEDate = tInfo.EndDate

        If startDate < termSDate Then
            errMsg &= "Class section start date must be on or after term start date " & termSDate.ToShortDateString & vbCr
        End If

        If endDate > termEDate Then
            errMsg &= "Class section end date must be on or before term end date " & termEDate.ToShortDateString & vbCr
        End If


        Return errMsg
    End Function

    Public Function GetCurrentAndFutureTerms() As DataSet

        '   Instantiate DAL component
        Dim GrdPostDB As New GrdPostingsDB

        '   get the dataset with all degrees
        Return GrdPostDB.GetCurrentAndFutureTerms
    End Function

    Public Function GetCourseForClassSection(ByVal clsSectionId As String) As DataTable
        Dim db As New ClassSectionDB
        Return db.GetCourseForClassSection(clsSectionId)

    End Function

    Public Function GetClassesForTermBasedOnCourse(ByVal reqId As String, ByVal termId As String) As DataTable
        Dim db As New ClassSectionDB
        Return db.GetClassesForTermBasedOnCourse(reqId, termId)
    End Function

    Public Function GetTermDates(ByVal termId As String) As DataTable
        Dim db As New ClassSectionDB
        Return db.GetTermDates(termId)
    End Function
    Public Function InsertAllClassSectionsBelongingToACourseGroup(ByVal termId As String, ByVal groups() As String, ByVal campusId As String, ByVal user As String) As String
        Return (New ClassSectionDB).InsertAllClassSectionsBelongingToACourseGroup(termId, groups, campusId, user)
    End Function

    ''Modified by Saraswathi lakshmanan on May 12 2010
    'For mantis case 18921

    Public Function InsertAllClassSectionsBelongingToACourseGroup_CAmpusID(ByVal termId As String, ByVal groups() As String, ByVal CAmpGrpID As String, ByVal user As String) As String
        Return (New ClassSectionDB).InsertAllClassSectionsBelongingToACourseGroup_CampusID(termId, groups, CAmpGrpID, user)
    End Function
    Public Function GetGroupsWithMissingClassSections(ByVal termId As String, Optional ByVal campusId As String = "") As DataTable
        'Return (New ClassSectionDB).GetGroupsWithMissingClassSections(termId, campusId)
        ''Modified by Saraswathi lakshmanan on May 12 2010
        Return (New ClassSectionDB).GetGroupsWithMissingClassSectionsForCampGrps_SP(termId, campusId)

    End Function

    'Public Function ValidateMaxStudents(ByVal ClsSectId As String) As String
    '    Dim MaxRegistered As String
    '    Dim termEDate As Date
    '    Dim tDB As New TermsDB
    '    Dim tInfo As New TermInfo
    '    Dim errMsg As String = ""

    '    'Get the term start and end dates
    '    tInfo = tDB.GetTermInfo(termId)

    '    MaxRegistered = tInfo.StartDate
    '    termEDate = tInfo.EndDate

    '    If startDate < termSDate Then
    '        errMsg &= "Class section start date must be on or after term start date " & termSDate.ToShortDateString & vbCr
    '    End If

    '    If endDate > termEDate Then
    '        errMsg &= "Class section end date must be on or before term end date " & termEDate.ToShortDateString & vbCr
    '    End If


    '    Return errMsg
    'End Function
    Public Function CheckScheduleConflictsWhileAddingClasses(ByVal ClsSectionId As String, _
                                                        ByVal ClsMeetingStartDate As Date,
                                                        ByVal ClsMeetingEndDate As Date, _
                                                        ByVal ClsMeetingRoomId As String, _
                                                        ByVal InstructorId As String, _
                                                        ByVal CurrentTermId As String, _
                                                        ByVal CurrentCourseId As String, _
                                                        ByVal CurrentPeriodId As String) As DataSet
        'Return (New ClassSectionDB).CheckScheduleConflictsWhileAddingClasses(ClsSectionId)
        Return (New ClassSectionDB).CheckScheduleConflictsWhileAddingClasses(ClsSectionId, ClsMeetingStartDate, ClsMeetingEndDate, _
                                                                            ClsMeetingRoomId, InstructorId, CurrentTermId, CurrentCourseId, CurrentPeriodId)

    End Function
    Public Function CheckScheduleConflictsWhileAddingClassesArray(ByVal ClsSectionId As String, _
                                                     ByVal ClsMeetingStartDate As String,
                                                     ByVal ClsMeetingEndDate As String, _
                                                     ByVal ClsMeetingRoomId As String, _
                                                     ByVal InstructorId As String, _
                                                     ByVal CurrentTermId As String, _
                                                     ByVal CurrentCourseId As String, _
                                                     ByVal CurrentPeriodId As String, _
                                                     ByVal CurrentCampusId As String) As DataSet
        'Return (New ClassSectionDB).CheckScheduleConflictsWhileAddingClasses(ClsSectionId)
        Return (New ClassSectionDB).CheckScheduleConflictsWhileAddingClassesArray(ClsSectionId, ClsMeetingStartDate, ClsMeetingEndDate, _
                                                                            ClsMeetingRoomId, InstructorId, CurrentTermId, CurrentCourseId, CurrentPeriodId, CurrentCampusId)

    End Function
    Public Function CheckScheduleConflictsDuringRegistration(ByVal ClsSectionId As String, ByVal StuEnrollId As String) As DataSet
        Return (New ClassSectionDB).CheckScheduleConflictsDuringRegistration(ClsSectionId, StuEnrollId)
    End Function
    Public Function GetStudentInfo(ByVal StuEnrollId As String) As DataSet
        Return (New ClassSectionDB).GetStudentInfo(StuEnrollId)
    End Function
    Public Function CheckScheduleConflictsWhileAddingClassesNoPeriods(ByVal ClsSectionId As String, _
                                                       ByVal ClsMeetingStartDate As String,
                                                       ByVal ClsMeetingEndDate As String, _
                                                       ByVal ClsMeetingRoomId As String, _
                                                       ByVal InstructorId As String, _
                                                       ByVal CurrentTermId As String, _
                                                       ByVal CurrentCourseId As String, _
                                                       ByVal WorkDayId As String, _
                                                       ByVal StartTimeId As String, _
                                                       ByVal EndTimeId As String, _
                                                       ByVal CurrentCampusId As String) As DataSet
        Return (New ClassSectionDB).CheckScheduleConflictsWhileAddingClassesNoPeriods(ClsSectionId, ClsMeetingStartDate, ClsMeetingEndDate, _
                                                                            ClsMeetingRoomId, InstructorId, CurrentTermId,
                                                                            CurrentCourseId, WorkDayId, StartTimeId, EndTimeId, CurrentCampusId)
    End Function
    Public Function CheckScheduleConflictsDuringCopy(ByVal ClsSectionId As String, ByVal TargetTermId As String) As DataSet
        Return (New ClassSectionDB).CheckScheduleConflictsDuringCopy(ClsSectionId, TargetTermId)
    End Function
    Public Function GetTimeInterval(ByVal PeriodId As String) As DataSet
        Return (New ClassSectionDB).GetTimeInterval(PeriodId)
    End Function
    Public Function GetWorkDaysOverlap(ByVal PeriodId1 As String, ByVal PeriodId2 As String) As Integer
        Return (New ClassSectionDB).GetWorkDaysOverlap(PeriodId1, PeriodId2)
    End Function
    Public Function CheckScheduleConflictsWhileCopyingToAnotherTerm(ByVal ClsSectionId As String, ByVal SourceTermId As String, ByVal TargetTermId As String, ByVal CampusId As String) As DataSet
        Return (New ClassSectionDB).CheckScheduleConflictsWhileCopyingToAnotherTerm(ClsSectionId, SourceTermId, TargetTermId, CampusId)
    End Function
    Public Function CheckScheduleConflictsForClassesInsideTerm(ByVal ClsSectionId As String, ByVal TargetTermId As String, ByVal CampusId As String) As DataSet
        Return (New ClassSectionDB).CheckScheduleConflictsForClassesInsideTerm(ClsSectionId, TargetTermId, CampusId)
    End Function
    Public Function CheckScheduleConflictsForClassesInsideTermIgnoreRooms(ByVal ClsSectionId As String, ByVal TargetTermId As String, ByVal CampusId As String) As DataSet
        Return (New ClassSectionDB).CheckScheduleConflictsForClassesInsideTermIgnoreRooms(ClsSectionId, TargetTermId, CampusId)
    End Function
    Public Function CheckScheduleConflictsWhileAddingClassesArrayWithInSameClass(ByVal ClsSectionId As String, _
                                                            ByVal ClsMeetingStartDate As String,
                                                            ByVal ClsMeetingEndDate As String, _
                                                            ByVal ClsMeetingRoomId As String, _
                                                            ByVal InstructorId As String, _
                                                            ByVal CurrentTermId As String, _
                                                            ByVal CurrentCourseId As String, _
                                                            ByVal CurrentPeriodId As String, _
                                                            ByVal CurrentCampusId As String) As DataSet
        Return (New ClassSectionDB).CheckScheduleConflictsWhileAddingClassesArrayWithInSameClass(ClsSectionId, ClsMeetingStartDate, ClsMeetingEndDate, _
                                                                            ClsMeetingRoomId, InstructorId, CurrentTermId, CurrentCourseId, CurrentPeriodId, CurrentCampusId)
    End Function
    Public Function CheckScheduleConflictsDuringRegistrationMan(ByVal ClsSectionId As String) As DataSet
        Return (New ClassSectionDB).CheckScheduleConflictsDuringRegistrationMan(ClsSectionId)
    End Function
End Class
