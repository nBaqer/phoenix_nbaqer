Public Class CampusProgVerFacade
    ''' <summary>
    ''' This method is to fetch the R2T4 Calculation Period Types.
    ''' </summary>
    ''' <returns>Returns the R2T4 Calculation Period Type Id and Description</returns>
    Public Function GetR2T4CalculationPeriodTypes() As DataSet
        Return (New CampusProgVerDB).GetR2T4CalculationPeriodTypes()
    End Function

    ''' <summary>
    ''' This method inserts campus program versions details.
    ''' </summary>
    ''' <param name="campusPrgVersion"></param>
    ''' <param name="campusId"></param>
    ''' <returns></returns>
    Public Function InsertIntoCampusPrgVerTable(ByVal campusPrgVersion As Dictionary(Of String , Object), ByVal campusId As String) As String
        Return (New CampusProgVerDB).InsertIntoCampusPrgVerTable(campusPrgVersion, campusId)
    End Function

    ''' <summary>
    ''' This method fetches the campus program version details.
    ''' </summary>
    ''' <param name="prgVerId"></param>
    ''' <param name="campusId"></param>
    ''' <returns>Returns the campus program version details.</returns>
    Public Function GetCampusPrgVersionData(ByVal prgVerId As String, ByVal campusId As String) As DataSet
        '   Instantiate DAL component
        Dim db As New CampusProgVerDB
        Return db.GetCampusPrgVersionData(prgVerId, campusId)
    End Function

    ''' <summary>
    ''' This method deletes the campus program versions details.
    ''' </summary>
    ''' <param name="prgVerId"></param>
    ''' <param name="campusId"></param>
    Public Sub DeleteCampusProgramVersion(ByVal prgVerId As String, ByVal campusId As String)
        Dim obj As New CampusProgVerDB
        obj.DeleteCampusProgramVersion(prgVerId, campusId)
    End Sub

    ''' <summary>
    ''' This method updates the campus program versions details.
    ''' </summary>
    ''' <param name="tblName"></param>
    ''' <param name="pkId"></param>
    ''' <param name="value"></param>
    ''' <param name="fldvalue1"></param>
    ''' <param name="campusId"></param>
    ''' <param name="modUser"></param>
    ''' <returns></returns>
    Public Function UpdateCampusPrgVersionDetails(ByVal tblName As String, ByVal pkId As String,
                                                  ByVal value As String,
                                                  ByVal fldvalue1 As String,
                                                  ByVal campusId As String,
                                                  ByVal modUser As String) As String
        Dim campusProgVerDb As New CampusProgVerDB
        Return (campusProgVerDb).UpdateCampusPrgVersionDetails(tblName, pkId, value, fldvalue1, campusId, modUser)
    End Function

    ''' <summary>
    ''' This method returns the values for Self Paced dropdown list. 
    ''' </summary>
    ''' <param name="tblname">The tblname is the table name where the self paced values stored.</param>
    ''' <param name="fldName">The fldName is the self paced column name</param>
    ''' <param name="pKName">The pKName is the primary key program version Id.</param>
    ''' <param name="pKValue">The pKValue is the primary key value</param>
    ''' <returns>Returns self paced value of the selected item for self paced dropdownlist</returns>
    Public Function GetSelfPacedValue(ByVal tblname As String, ByVal fldName As String, ByVal pKName As String, ByVal pKValue As String, ByVal campusId As String) As Integer
        Dim campusProgVerDb As New CampusProgVerDB
        Return (campusProgVerDb).GetSelfPacedValue(tblname, fldName, pKName, pKValue, campusId)
    End Function

    ''' <summary>
    ''' This method checkd if the campus program versions details already exists for the specified campus.
    ''' </summary>
    ''' <param name="prgVerId"></param>
    ''' <param name="campusId"></param>
    ''' <returns>Returns a boolean value whether the campus program version exists or not</returns>
    Public Function DoesCampusProgVerAlreadyExists(ByVal prgVerId As String, ByVal campusId As String) As Boolean
        'return boolean value
        Dim recCount As Integer = (New CampusProgVerDB).DoesCampusProgVerAlreadyExists(prgVerId, campusId)
        If recCount > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
End Class