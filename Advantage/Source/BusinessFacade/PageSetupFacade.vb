Public Class PageSetupFacade

    Public Function GetResourceDef(ByVal resourceId As Integer, ByVal languageName As String) As DataTable
        Dim dbPageSetup As New PageSetupDB

        Return dbPageSetup.GetResourceDef(resourceId, languageName)
    End Function
    Public Function GetResourceForQuickLeads() As DataTable
        Dim dbPageSetup As New PageSetupDB

        Return dbPageSetup.GetResourceForQuickLeads()
    End Function

    Public Function GetDataDictionaryReqFields() As DataTable
        Dim dbPageSetup As New PageSetupDB

        Return dbPageSetup.GetDataDictionaryReqFields
    End Function
    Public Function GetDataDictionaryReqFieldsForQuickLeads() As DataTable
        Dim dbPageSetup As New PageSetupDB
        Return dbPageSetup.GetDataDictionaryReqFieldsForQuickLeads
    End Function

    Public Function GetSchlRequiredFieldsForResource(ByVal resourceId As Integer) As DataTable
        Dim dbPageSetup As New PageSetupDB

        Return dbPageSetup.GetSchlRequiredFieldsForResource(resourceId)
    End Function
    Public Function GetSchlRequiredFieldsForResourceForQuickLeads(ByVal resourceId As Integer) As DataTable
        Dim dbPageSetup As New PageSetupDB
        Return dbPageSetup.GetSchlRequiredFieldsForResourceForQuickLeads(resourceId)
    End Function

    Public Function GetIPEDSFields() As DataTable
        Dim dbPageSetup As New PageSetupDB

        Return dbPageSetup.GetIPEDSFields
    End Function
    Public Function GetIPEDSFieldsForQuickLeads() As DataTable
        Dim dbPageSetup As New PageSetupDB
        Return dbPageSetup.GetIPEDSFieldsForQuickLeads
    End Function
End Class
