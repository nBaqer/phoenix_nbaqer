Imports FAME.AdvantageV1.Common.IPEDSCommon
Imports FAME.AdvantageV1.BusinessFacade.FIC_C4CommonObject

Public Class FIC_C4SummaryObject
	Inherits BaseReportFacadeIPEDS

	Public Const MainTableName As String = "FIC_C4Summary"

	Private dsRaw As New DataSet
	Private dsRpt As New DataSet
	Private dtScoresRawProc As DataTable

	Public Overrides Function GetReportDataSet(ByVal RptParamInfo As ReportParamInfoIPEDS) As System.Data.DataSet
		' get raw DataSet using passed-in parameters
		dsRaw = FIC_C4SummaryObjectDB.GetReportDatasetRaw(RptParamInfo)

		' build and return report DataSet
		Return BuildReportDataSet()
	End Function

	Private Function BuildReportDataSet() As DataSet
		' if empty raw dataset, return empty dataset
		If dsRaw.Tables.Count = 0 Then
			Return New DataSet
		End If

		Dim drRpt As DataRow

		With dsRpt.Tables.Add(MainTableName).Columns
			.Add("TotalSAT", GetType(System.Int32))
			.Add("PctSAT", GetType(System.Int32))
			.Add("TotalACT", GetType(System.Int32))
			.Add("PctACT", GetType(System.Int32))
			.Add("SAT1VerbalPct25", GetType(Single))
			.Add("SAT1VerbalPct75", GetType(Single))
			.Add("SAT1MathPct25", GetType(Single))
			.Add("SAT1MathPct75", GetType(Single))
			.Add("ACTCompPct25", GetType(Single))
			.Add("ACTCompPct75", GetType(Single))
			.Add("ACTEnglishPct25", GetType(Single))
			.Add("ACTEnglishPct75", GetType(Single))
			.Add("ACTMathPct25", GetType(Single))
			.Add("ACTMathPct75", GetType(Single))
		End With

		Dim TotalEnrolled As Integer = dsRaw.Tables(TblNameStudents).Rows.Count

		dtScoresRawProc = PreProcessScores(dsRaw.Tables(TblNameStudents), dsRaw.Tables(TblNameScores))

		drRpt = dsRpt.Tables(MainTableName).NewRow
		drRpt("TotalSAT") = dtScoresRawProc.Compute("Count(TestDescrip)", "TestDescrip LIKE '" & EntrTestSAT1Verbal & "'")
		drRpt("PctSAT") = Math.Floor(((drRpt("TotalSAT") / TotalEnrolled) * 100) + 0.5)
		drRpt("TotalACT") = dtScoresRawProc.Compute("Count(TestDescrip)", "TestDescrip LIKE '" & EntrTestACTComp & "'")
		drRpt("PctACT") = Math.Floor(((drRpt("TotalACT") / TotalEnrolled) * 100) + 0.5)

		GetPercentileScores(EntrTestSAT1Verbal, drRpt("SAT1VerbalPct25"), drRpt("SAT1VerbalPct75"))
		GetPercentileScores(EntrTestSAT1Math, drRpt("SAT1MathPct25"), drRpt("SAT1MathPct75"))
		GetPercentileScores(EntrTestACTComp, drRpt("ACTCompPct25"), drRpt("ACTCompPct75"))
		GetPercentileScores(EntrTestACTEnglish, drRpt("ACTEnglishPct25"), drRpt("ACTEnglishPct75"))
		GetPercentileScores(EntrTestACTMath, drRpt("ACTMathPct25"), drRpt("ACTMathPct75"))

		dsRpt.Tables(MainTableName).Rows.Add(drRpt)

		Return dsRpt

	End Function

	Private Sub GetPercentileScores(ByVal TestDescrip As String, ByRef ScorePct25 As Object, ByRef ScorePct75 As Object)
		Dim dvScores As New DataView(dtScoresRawProc, "TestDescrip LIKE '" & TestDescrip & "'", _
									 "", DataViewRowState.CurrentRows)

		If dvScores.Count > 0 Then
			Dim i As Integer
			Dim RankPct25 As Integer, RankPct75 As Integer

			If dvScores.Count > 1 Then
				RankPct25 = Math.Floor((dvScores.Count * 0.25) + 0.5)
				RankPct75 = Math.Floor((dvScores.Count * 0.75) + 0.5)
			Else
				RankPct25 = 0
				RankPct75 = 0
			End If

			i = 0
			While i < dvScores.Count OrElse IsNothing(ScorePct25) OrElse IsNothing(ScorePct75)
				If i = RankPct25 Then
					ScorePct25 = dvScores(i)("Score")
				End If
				If i = RankPct75 Then
					ScorePct75 = dvScores(i)("Score")
				End If
				i += 1
			End While
		End If

	End Sub

End Class
