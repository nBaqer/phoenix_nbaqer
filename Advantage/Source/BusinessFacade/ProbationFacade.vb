Imports FAME.AdvantageV1.BusinessFacade.StudentEnrollState

Public Class ProbationFacade


    Public Function GetProbationTypeFromStatusCode(ByVal statusCodeId) As Integer
        Dim db As New ProbationDB

        Return db.GetProbationTypeFromStatusCode(statusCodeId)
    End Function

    'Public Function PlaceStudentOnProbation(ByVal stuEnrollId As String, ByVal reason As String, ByVal startDate As String, ByVal endDate As String, ByVal probType As String, ByVal user As String) As String
    Public Function PlaceStudentOnProbation(change As StudentChangeHistoryObj) As String

        Dim db As New LOADB
        Dim studentStatus As StateEnum
        Dim createObj As New StateFactory

        'Retrieve current status of student
        studentStatus = db.GetStudentStatus(change.StuEnrollId)
        'Based on current status on student, create appropriate state object
        Dim stateObj As IStudentEnrollmentState = StateFactory.CreateStateObj(studentStatus)
        Dim result As String = stateObj.PlaceStudentOnProbation(change)
        Return result

        'Return stateObj.PlaceStudentOnProbation(StuEnrollId, Reason, StartDate, EndDate, ProbType, user)

    End Function
    Public Function GetCurrAttendingProbationStatus() As String

        '   Instantiate DAL component
        Dim db As New ProbationDB


        Return db.GetCurrAttendingProbationStatus()


    End Function

    'US8404
    'Public Function GetProbationStatuses(ByVal probationtype As Integer) As DataSet

    '    '   Instantiate DAL component
    '    Dim db As New ProbationDB
    '    Dim ds As DataSet

    '    '   get the dataset with all drop statuses          
    '    ds = db.GetProbationStatuses(probationtype)
    '    Return ds

    'End Function
    Public Function GetProbationStatuses() As DataSet

        '   Instantiate DAL component
        Dim db As New ProbationDB
        Dim ds As DataSet

        '   get the dataset with all drop statuses          
        ds = db.GetProbationStatuses()
        Return ds

    End Function
    Public Function GetWarningsOnly() As DataSet

        '   Instantiate DAL component
        Dim db As New ProbationDB
        Dim ds As DataSet

        '   get the dataset with all drop statuses          
        ds = db.GetWarningsOnly()
        Return ds

    End Function
    Public Function GetStudentWarnings(ByVal stuEnrollId As String) As DataSet

        '   Instantiate DAL component
        Dim db As New ProbationDB
        Dim ds As DataSet

        '   get the dataset with all drop statuses          
        ds = db.GetStudentWarnings(stuEnrollId)
        Return ds

    End Function
    Public Function GetStudentLOAs(ByVal stuEnrollId As String) As DataSet

        '   Instantiate DAL component
        Dim db As New ProbationDB
        Dim ds As DataSet

        '   get the dataset with all drop statuses          
        ds = db.GetStudentLOAs(stuEnrollId)
        Return ds

    End Function
    Public Function GetStudentProbations(ByVal stuEnrollId As String, Optional ByVal probationType As Integer = 0) As DataSet

        '   Instantiate DAL component
        Dim db As New ProbationDB
        Dim ds As DataSet

        '   get the dataset with all drop statuses          
        ds = db.GetStudentProbations(stuEnrollId, probationType)
        Return ds

    End Function
    Public Function GetStudentSuspensions(ByVal stuEnrollId As String) As DataSet

        '   Instantiate DAL component
        Dim db As New ProbationDB
        Dim ds As DataSet

        '   get the dataset with all drop statuses          
        ds = db.GetStudentSuspensions(stuEnrollId)
        Return ds

    End Function
    'Public Function UpdateLOAEndDate(ByVal EndDate As String, ByVal StudentLOAId As String, ByVal user As String) As String
    '    Dim DB As New LOADB

    '    Return DB.UpdateLOAEndDate(EndDate, StudentLOAId, user)
    'End Function
    Public Function UpdateLOADate(ByVal startDate As String, ByVal endDate As String, ByVal studentLOAId As String, ByVal user As String) As String
        Dim db As New LOADB

        Return db.UpdateLOADate(startDate, endDate, studentLOAId, user)
    End Function


    Public Function GetProbationTypes() As DataSet

        '   Instantiate DAL component
        Dim db As New ProbationDB
        Dim ds As DataSet

        '   get the dataset with all drop statuses          
        ds = db.GetProbationTypes()
        Return ds

    End Function
End Class
