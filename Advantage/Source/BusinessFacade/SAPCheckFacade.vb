Imports System.Data
Imports System.Web
Imports FAME.AdvantageV1.DataAccess
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessRules
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports FAME.AdvantageV1.BusinessFacade.StudentEnrollState

Public Class SAPCheckFacade
    Public Function GetSAPPolicyDetails(ByVal StudentArrList As ArrayList, ByVal user As String, ByVal campusId As String) As String

        '' Dim Student As SAPCheckInfo
        Dim db As New SAPCheckDB
        Dim ds As DataSet
        Dim ds1 As DataSet
        Dim ds2 As New DataSet
        Dim ds3 As DataSet
        Dim ds5 As DataSet
        Dim tbl1 As DataTable
        Dim row As DataRow
        Dim row3 As DataRow
        Dim sapDetail As DataRow
        '' Dim StuResult As DataRow
        Dim currRow As DataRow
        Dim sapId As String
        Dim trigUnit As String
        Dim credsAttmptd As Decimal
        Dim trigOffsetTyp As String
        Dim startDate As Date
        Dim trigValue As Decimal
        Dim trigOffSeq As Integer
        Dim QuantMinUnitTyp As String
        Dim QuantMinVal As Decimal
        Dim QualMinVal As Decimal
        Dim stuEnrollId As String
        Dim sapInfo As SAPInfo
        Dim credits As New SAPCHK.SAPCredits
        Dim days As New SAPCHK.SAPDays
        Dim weeks As New SAPCHK.SAPWeeks
        Dim months As New SAPCHK.SAPMonths
        'Dim ScheduledHours As New SAPCHK.SAPScheduledHours
        Dim actualHours As New SAPCHK.SAPActualHours
        Dim prgVerId As String
        Dim errStr As String = ""
        Dim ds4 As New DataSet
        Dim ds6 As DataSet
        'Dim row2 As DataRow
        'Dim TermCount As Integer
        Dim curRow As DataRow
        Dim curRow2 As DataRow
        Dim term As String
        Dim trigToFire As Integer
        Dim termSDate As Date
        Dim termEDate As Date
        Dim count As Integer
        Dim currDate As Date
        Dim offsetDate As Date
        Dim intStudentCount As Integer
        Dim x As Integer
        Dim iNormalProgLength As Decimal
        Dim iMaxTimeFrame As Decimal
        Dim sapTrigUnit As String
        Dim FallsWithinMTF As Boolean
        '    Dim iMTFTrigger As Integer
        Dim progVerDescrip As String
        Dim graduateAudit As New GraduateAuditFacade
        Dim examsBf As New ExamsFacade
        Dim str As String
        Dim errStr2 As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim gradeReps As String = myAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod")
        Dim strIncludeHours As String = myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade")
        Dim dtTermGPA As DataTable
        Dim includeHours As Boolean
        Dim br As New AttendancePercentageBR
        Dim periodsDB As New PeriodsDB
        Dim attInfo As AttendancePercentageInfo
        Dim attSummaryInfo As Common.AR.ClockHourAttendanceInfo
        Dim attType As String
        Dim dsStudentSAPResults As DataSet

        If strIncludeHours.ToLower = "true" Then
            includeHours = True
        End If

        intStudentCount = StudentArrList.Count

        'For Each Student In StudentArrList
        While x < intStudentCount
            sapInfo = New SAPInfo
            'StuEnrollId = Student.StudentId.ToString
            stuEnrollId = DirectCast(StudentArrList.Item(x), String)

            ds = db.GetPrgVerSAP(stuEnrollId)


            tbl1 = ds.Tables(0)
            'Make the ChildId column the primary key
            With tbl1
                .PrimaryKey = New DataColumn() { .Columns("PrgVerId")}
            End With


            If ds.Tables(0).Rows.Count > 0 Then
                'Retrieve the SAPId from the data-table and get the
                'Details for this SAP Policy.

                For Each row In tbl1.Rows 'Loop through each program version for each student
                    'Reset the CredsAttmptd variable. Added by Troy on 4/19/2006
                    credsAttmptd = 0
                    sapId = row("SAPId").ToString
                    prgVerId = row("PrgVerId").ToString
                    progVerDescrip = row("PrgVerDescrip")
                    attType = row("attendanceType").ToString.ToUpper()
                    sapInfo.attType = attType
                    sapTrigUnit = row("TrigUnitTypId")
                    sapInfo.TrackExternAttendance = row("TrackExternAttendance")
                    ' check also the student program is a clock hour
                    If row("IncludeTransferHours") And Not row("ClockHrProgram").ToString = "" Then
                        sapInfo.IncludeTransferHours = True
                    Else
                        sapInfo.IncludeTransferHours = False
                    End If

                    'Check if student falls within the Maximum Time Frame of the Program
                    If sapTrigUnit = 3 Then
                        iNormalProgLength = row("Credits")  'Troy on 4/19/2006. Should only be done for those programs that do have credits
                        iMaxTimeFrame = row("Credits") * 1.5 'Troy on 4/19/2006. Should only be done for those programs that do have credits
                        credsAttmptd = graduateAudit.GetCreditsAttemptedForSAP(stuEnrollId, gradeReps, includeHours, campusId)

                        'Fill the SAPInfo object to pass to BR later on.
                        sapInfo.CredsAttempted = credsAttmptd
                        sapInfo.CredsEarned = graduateAudit.EarnedCreditsForSAP + graduateAudit.CreditZeroWeight
                        sapInfo.PercCredsEarned = graduateAudit.PercentageEarnedCreditsForSAP
                        sapInfo.FinCredsEarned = graduateAudit.EarnedFinAidCredits
                        '**************************************************************************************
                        If credsAttmptd > iNormalProgLength And credsAttmptd < iMaxTimeFrame Then
                            FallsWithinMTF = True
                        ElseIf credsAttmptd > iMaxTimeFrame Then 'Fix for mantis # 7296
                            errStr = "Cannot run SAP Check since student's attempted credits exceeds MTF."
                        End If
                    End If


                    'Get the SAP Policy Details for the SAP Policy
                    'attached to this program version.
                    'Troy: 6/23/2016 DE12847 We need to execute all the increments for the student
                    'that have not been run as yet
                    ds1 = db.GetSAPPolicyDetails(sapId)

                    Dim aRows As DataRow()

                    'Get the trigger/increment to be fired for this student.
                    If sapTrigUnit <> 3 Then
                        ds5 = db.GetTrigToBeFired(stuEnrollId)

                        If ds5.Tables(0).Rows.Count > 0 Then
                            row3 = ds5.Tables(0).Rows(0)

                            'We want to get the sequences from the sap policy details that are greater than the
                            'last increment done
                            aRows = ds1.Tables(0).Select("Seq > " & row3("Period"))

                            'If Not (row3 Is Nothing) Then
                            '    trigToFire = row3("Period")
                            'End If
                        Else
                            'trigToFire = 0
                            'No sap check has been done for the student so we need all the increments
                            aRows = ds1.Tables(0).Select()
                        End If

                    Else
                        'Find the row that contains the trigger for Normal Program Length
                        Dim arows2 As DataRow() = ds1.Tables(0).Select("TrigValue =" & (iNormalProgLength * 100))
                        Try
                            trigToFire = arows2(0)("Seq") - 1
                        Catch ex As System.Exception
                            errStr2 = ("ErrStr2The SAP Check process has been halted since the program version- " & progVerDescrip & " does not have an increment set up for Normal Program Length- " & iNormalProgLength & " credits")
                            Return errStr2
                        End Try

                        If myAdvAppSettings.AppSettings("SAPUsesCreditRanges") = "True" Then
                            'We need the first value that is greater than or equal to the credits attempted
                            aRows = ds1.Tables(0).Select("TrigValue >= " & credsAttmptd * 100)
                            'trigToFire = aRows(0)("Seq") - 1
                        Else
                            'We need the first value that is less than or equal to the credits attempted
                            aRows = ds1.Tables(0).Select("TrigValue <= " & credsAttmptd * 100)
                            'In this case if there are no credits attempted the result set returned will be empty
                            If aRows.Length = 0 Then
                                errStr = "The student has not yet attempted any credits."
                                Exit For
                                'Else
                                '    'we need the last entry in the array
                                '    Dim len As Integer = aRows.Length
                                '    trigToFire = aRows(len - 1)("Seq") - 1
                            End If
                        End If

                    End If

                    'Troy: 6/23/2016 We need to execute the existing code for each increment
                    'that has not been run
                    For cntRows As Integer = 0 To aRows.Length - 1
                        Dim blnHasIncrementBeenRunForStudent As Boolean
                        Dim aRowsFound As DataRow()

                        'Set this variable to false. All the trigunits <> 3 will only return increments 
                        'that have not been run for the student. However, for trigunit = 3 we have to do
                        'check to see whether or not the increment was run for the student
                        blnHasIncrementBeenRunForStudent = False

                        trigToFire = aRows(cntRows)("Seq") - 1

                        If sapTrigUnit = 3 Then
                            dsStudentSAPResults = db.GetStudentSAPIncrementsAlreadyRun(stuEnrollId)

                            aRowsFound = dsStudentSAPResults.Tables(0).Select("Period = " & (trigToFire + 1))

                            If dsStudentSAPResults.Tables(0).Rows.Count = 0 Then
                                blnHasIncrementBeenRunForStudent = False
                            ElseIf aRowsFound.Length = 0 Then
                                blnHasIncrementBeenRunForStudent = False
                            Else
                                blnHasIncrementBeenRunForStudent = True
                            End If
                        End If


                        If blnHasIncrementBeenRunForStudent = False Then
                            'Get the Trigger Unit for this SAP Policy.
                            Try

                                If (ds1.Tables(0).Rows.Count > trigToFire) Then
                                    sapDetail = ds1.Tables(0).Rows(trigToFire)

                                    trigUnit = sapDetail("TrigUnitTypDescrip")
                                    trigOffsetTyp = sapDetail("TrigOffTypDescrip")

                                    'Get info for the current trigger
                                    trigValue = sapDetail("TrigValue")
                                    QuantMinUnitTyp = sapDetail("QuantMinTypDesc")
                                    If sapDetail.IsNull("QuantMinValue") Then
                                        sapInfo.OverallAttendance = False
                                    Else
                                        sapInfo.OverallAttendance = True
                                        QuantMinVal = sapDetail("QuantMinValue")
                                    End If
                                    QualMinVal = sapDetail("QualMinValue")
                                    If Not sapDetail.IsNull("TrigOffsetSeq") Then
                                        trigOffSeq = sapDetail("TrigOffsetSeq")
                                    End If
                                Else
                                    errStr = "No more SAP checks left for this student "
                                    Exit For
                                End If
                            Catch
                                errStr = "No more SAP checks left for this student "
                                Exit For
                            End Try

                            'Get the Term StartDate & EndDate related to this trigger.
                            If (trigOffsetTyp = "start of term") Or (trigOffsetTyp = "end of term") Or (trigOffsetTyp = "start date") Then
                                ds4 = db.GetStuTerms(stuEnrollId)
                                count = ds4.Tables(0).Rows.Count
                            End If
                            Select Case (trigOffsetTyp)
                                Case "start of term"
                                    If ((count >= trigOffSeq - 1) And trigOffSeq >= 2) Then
                                        curRow = ds4.Tables(0).Rows(trigOffSeq - 2)
                                        term = curRow("TermId").ToString
                                        termEDate = curRow("EndDate")
                                        termSDate = curRow("StartDate")
                                        If (trigUnit = "Days") Then
                                            If DateAdd(DateInterval.Day, trigValue / 100, termSDate) > Date.Now Then
                                                errStr = "It is not yet time to check this increment."
                                                Exit For
                                            End If
                                            sapInfo.OffsetDate = DateAdd(DateInterval.Day, trigValue / 100, termSDate)
                                        ElseIf (trigUnit = "Weeks") Then
                                            If DateAdd(DateInterval.WeekOfYear, trigValue / 100, termSDate) > Date.Now Then
                                                errStr = "It is not yet time to check this increment."
                                                Exit For
                                            End If
                                            sapInfo.OffsetDate = DateAdd(DateInterval.WeekOfYear, trigValue / 100, termSDate)
                                        End If

                                    Else
                                        errStr = "Results do not exist for Term " & trigOffSeq
                                        Exit For
                                    End If
                                    'Calculate Cumulative GPA using Centralized function before making
                                    'a call to the Business Rules Layer. We have to expose the GPA
                                    'property in the facade and pass it to BR since we cannot access
                                    'the facade in the BR. 04/26/06
                                    sapInfo.GPA = graduateAudit.GetGPA(stuEnrollId, gradeReps, termEDate, includeHours, campusId)

                                    If myAdvAppSettings.AppSettings("IncludeAllCreditsAttemptedinTotal").ToString.ToLower = "yes" Then
                                        sapInfo.CredsAttempted = graduateAudit.AllAttemptedCreditsForSAP
                                    Else
                                        sapInfo.CredsAttempted = graduateAudit.AttemptedCreditsForSAP
                                    End If

                                    sapInfo.CredsEarned = graduateAudit.EarnedCreditsForSAP + graduateAudit.CreditZeroWeight
                                    If sapInfo.CredsAttempted > 0 Then sapInfo.PercCredsEarned = System.Math.Round(sapInfo.CredsEarned / sapInfo.CredsAttempted * 100, 2)
                                    sapInfo.FinCredsEarned = graduateAudit.EarnedFinAidCredits

                                Case "end of term"
                                    If (count >= trigOffSeq) Then
                                        curRow = ds4.Tables(0).Rows(trigOffSeq - 1)
                                        term = curRow("TermId").ToString
                                        sapInfo.Term = term

                                        If Not (row("MinTermGPA") Is System.DBNull.Value) Then
                                            sapInfo.MinTermGPA = row("MinTermGPA").ToString
                                        End If
                                        dtTermGPA = graduateAudit.GetTermProgressFromResults(stuEnrollId, gradeReps, includeHours, , , campusId)
                                        Dim arows2 As DataRow() = dtTermGPA.Select("TermId ='" & term & "'")
                                        If (arows2.Length > 0) Then sapInfo.TermGPA = arows2(0)("TermGPA")


                                        If Not (row("TermGPAOver") Is System.DBNull.Value) Then
                                            sapInfo.TermGPAOver = row("TermGPAOver").ToString
                                        End If
                                        termEDate = curRow("EndDate")
                                        termSDate = curRow("StartDate")
                                        If (trigUnit = "Days") Then
                                            If DateAdd(DateInterval.Day, trigValue / 100, termEDate) > Date.Now Then
                                                errStr = "It is not yet time to check this increment."
                                                Exit For
                                            End If
                                            sapInfo.OffsetDate = DateAdd(DateInterval.Day, trigValue / 100, termEDate)
                                        ElseIf (trigUnit = "Weeks") Then
                                            'If DateAdd(DateInterval.WeekOfYear, TrigValue, TermEDate) > Date.Now Then
                                            '    ErrStr = "It is not yet time to check this increment."
                                            '    Exit For
                                            'End If
                                            If termEDate.AddDays((trigValue / 100) * 7) > Date.Now Then
                                                errStr = "It is not yet time to check this increment."
                                                Exit For
                                            End If
                                            'SAPInfo.OffsetDate = DateAdd(DateInterval.WeekOfYear, TrigValue, TermEDate)
                                            sapInfo.OffsetDate = termEDate.AddDays((trigValue / 100) * 7)
                                        ElseIf (trigUnit = "Months") Then
                                            If DateAdd(DateInterval.Month, trigValue / 100, termEDate) > Date.Now Then
                                                errStr = "It is not yet time to check this increment."
                                                Exit For
                                            End If
                                            sapInfo.OffsetDate = DateAdd(DateInterval.Month, trigValue / 100, termEDate)
                                        End If

                                    Else
                                        errStr = "Results do not exist for Term " & trigOffSeq
                                        Exit For
                                    End If
                                    'Calculate Cumulative GPA using Centralized function before making
                                    'a call to the Business Rules Layer. We have to expose the GPA
                                    'property in the facade and pass it to BR since we cannot access
                                    'the facade in the BR. 04/26/06
                                    sapInfo.GPA = graduateAudit.GetGPA(stuEnrollId, gradeReps, termEDate, includeHours, campusId)
                                    'SAPInfo.GPA = SAPInfo.TermGPA
                                    If myAdvAppSettings.AppSettings("IncludeAllCreditsAttemptedinTotal").ToString.ToLower = "yes" Then
                                        sapInfo.CredsAttempted = graduateAudit.AllAttemptedCreditsForSAP
                                    Else
                                        sapInfo.CredsAttempted = graduateAudit.AttemptedCreditsForSAP
                                    End If
                                    sapInfo.CredsEarned = graduateAudit.EarnedCreditsForSAP + graduateAudit.CreditZeroWeight
                                    'SAPInfo.PercCredsEarned = GraduateAudit.PercentageEarnedCredits
                                    If sapInfo.CredsAttempted > 0 Then sapInfo.PercCredsEarned = System.Math.Round(sapInfo.CredsEarned / sapInfo.CredsAttempted * 100, 2)
                                    sapInfo.FinCredsEarned = graduateAudit.EarnedFinAidCredits

                                Case "start date"
                                    'Get Student start date
                                    ds3 = db.GetStuStartDate(stuEnrollId)
                                    currRow = ds3.Tables(0).Rows(0)
                                    'Get the value of the StartDate Column in the table
                                    startDate = currRow("StartDate")

                                    'Get the offset date/credits attempted to trigger the sap check
                                    If (trigUnit = "Days") Then
                                        sapInfo.OffsetDate = DateAdd(DateInterval.Day, trigValue / 100, startDate)
                                        'SAPInfo.OffsetDate = "4/1/2009"
                                        offsetDate = DateAdd(DateInterval.Day, trigValue / 100, startDate)

                                        'Check if student has any LOA's and subtract that period of time accordingly.
                                        str = db.DoesStudentHaveLOA(stuEnrollId, startDate, offsetDate)

                                        Dim arrRange() As String
                                        Dim NoOfLOADays As Integer
                                        Dim LOAStart As Date
                                        Dim LOAEnd As Date
                                        arrRange = str.Split(",")
                                        If arrRange(0) <> "" Then
                                            LOAStart = CDate(arrRange(0))
                                        End If
                                        If arrRange(1) <> "" Then
                                            LOAEnd = CDate(arrRange(1))
                                        End If
                                        count = arrRange(2)

                                        If count > 0 Then
                                            NoOfLOADays = DateDiff(DateInterval.Day, LOAStart, LOAEnd)
                                            offsetDate = DateAdd(DateInterval.Day, NoOfLOADays, offsetDate)
                                        End If

                                    ElseIf (trigUnit = "Weeks") Then
                                        'SAPInfo.OffsetDate = DateAdd(DateInterval.WeekOfYear, TrigValue, StartDate)
                                        sapInfo.OffsetDate = startDate.AddDays((trigValue / 100) * 7)
                                        offsetDate = startDate.AddDays((trigValue / 100) * 7)
                                        'Check if student has any LOA's and subtract that period of time accordingly.
                                        str = db.DoesStudentHaveLOA(stuEnrollId, startDate, offsetDate)

                                        Dim arrRange() As String
                                        Dim NoOfLOADays As Integer
                                        Dim LOAStart As Date
                                        Dim LOAEnd As Date
                                        arrRange = str.Split(",")
                                        If arrRange(0) <> "" Then
                                            LOAStart = CDate(arrRange(0))
                                        End If
                                        If arrRange(1) <> "" Then
                                            LOAEnd = CDate(arrRange(1))
                                        End If
                                        count = arrRange(2)

                                        If count > 0 Then
                                            NoOfLOADays = DateDiff(DateInterval.Day, LOAStart, LOAEnd)
                                            offsetDate = DateAdd(DateInterval.Day, NoOfLOADays, offsetDate)
                                        End If
                                    ElseIf (trigUnit = "Months") Then
                                        sapInfo.OffsetDate = startDate.AddMonths(trigValue / 100)
                                        'SAPInfo.OffsetDate = "4/1/2009"
                                        offsetDate = startDate.AddMonths(trigValue / 100)

                                        'Check if student has any LOA's and subtract that period of time accordingly.
                                        str = db.DoesStudentHaveLOA(stuEnrollId, startDate, offsetDate)

                                        Dim arrRange() As String
                                        Dim NoOfLOADays As Integer
                                        Dim LOAStart As Date
                                        Dim LOAEnd As Date
                                        arrRange = str.Split(",")
                                        If arrRange(0) <> "" Then
                                            LOAStart = CDate(arrRange(0))
                                        End If
                                        If arrRange(1) <> "" Then
                                            LOAEnd = CDate(arrRange(1))
                                        End If
                                        count = arrRange(2)

                                        If count > 0 Then
                                            NoOfLOADays = DateDiff(DateInterval.Day, LOAStart, LOAEnd)
                                            offsetDate = DateAdd(DateInterval.Day, NoOfLOADays, offsetDate)
                                        End If
                                    ElseIf (trigUnit = "Credits Attempted") Then
                                        'Get credits attempted
                                        Dim dataset As New DataSet


                                        'Check if student meets trigger
                                        If myAdvAppSettings.AppSettings("SAPUsesCreditRanges") <> "True" Then
                                            'If we get to this point there is no need to check if student has met trigger

                                            'If (CredsAttmptd >= TrigValue) Then
                                            'Get offset date for credits
                                            Dim strDate As String = db.GetOffsetDateForCreditsPolicy(stuEnrollId)
                                            If strDate <> "" Then
                                                sapInfo.OffsetDate = strDate
                                            End If

                                            'Troy: 6/23/2016 We need to get the term that the student met the credits attempted trig value
                                            dtTermGPA = graduateAudit.GetTermProgressFromResults(stuEnrollId, gradeReps, includeHours, , , campusId)
                                            Dim arows2 As DataRow() = dtTermGPA.Select("CumCreditsAttempted >= " & (trigValue / 100) & "")
                                            termEDate = arows2(0)("EndDate")

                                            'Calculate Cumulative GPA using Centralized function before making
                                            'a call to the Business Rules Layer. We have to expose the GPA
                                            'property in the facade and pass it to BR since we cannot access
                                            'the facade in the BR. 05/02/06
                                            sapInfo.GPA = graduateAudit.GetGPA(stuEnrollId, gradeReps, termEDate, includeHours, campusId)
                                            If myAdvAppSettings.AppSettings("IncludeAllCreditsAttemptedinTotal").ToString.ToLower = "yes" Then
                                                sapInfo.CredsAttempted = graduateAudit.AllAttemptedCreditsForSAP
                                            Else
                                                sapInfo.CredsAttempted = graduateAudit.AttemptedCreditsForSAP
                                            End If
                                            sapInfo.CredsEarned = graduateAudit.EarnedCreditsForSAP + graduateAudit.CreditZeroWeight
                                            If sapInfo.CredsAttempted > 0 Then sapInfo.PercCredsEarned = System.Math.Round(sapInfo.CredsEarned / sapInfo.CredsAttempted * 100, 2)
                                            sapInfo.FinCredsEarned = graduateAudit.EarnedFinAidCredits
                                            Exit Select
                                            'Else
                                            'ErrStr = "Student has not met the trigger yet "
                                            'Exit For
                                            'End If
                                        Else
                                            'Get offset date for credits
                                            Dim strDate As String = db.GetOffsetDateForCreditsPolicy(stuEnrollId)
                                            If strDate <> "" Then
                                                sapInfo.OffsetDate = strDate
                                            End If

                                            'Calculate Cumulative GPA using Centralized function before making
                                            'a call to the Business Rules Layer. We have to expose the GPA
                                            'property in the facade and pass it to BR since we cannot access
                                            'the facade in the BR. 04/26/06
                                            sapInfo.GPA = graduateAudit.GetGPA(stuEnrollId, gradeReps, , includeHours, campusId)
                                            If myAdvAppSettings.AppSettings("IncludeAllCreditsAttemptedinTotal").ToString.ToLower = "yes" Then
                                                sapInfo.CredsAttempted = graduateAudit.AllAttemptedCreditsForSAP
                                            Else
                                                sapInfo.CredsAttempted = graduateAudit.AttemptedCreditsForSAP
                                            End If
                                            sapInfo.CredsEarned = graduateAudit.EarnedCreditsForSAP + graduateAudit.CreditZeroWeight
                                            If sapInfo.CredsAttempted > 0 Then sapInfo.PercCredsEarned = System.Math.Round(sapInfo.CredsEarned / sapInfo.CredsAttempted * 100, 2)
                                            sapInfo.FinCredsEarned = graduateAudit.EarnedFinAidCredits

                                            'If we are using ranges of credits there is no need to check if student has met trigger.
                                            Exit Select
                                        End If

                                    End If

                                    'Get the current date
                                    currDate = Date.Now

                                    'Check if student has met the trigger.
                                    If (currDate >= offsetDate) Then

                                    Else
                                        errStr = "Student has not met the trigger yet "
                                        Exit For
                                    End If

                                    'Get student terms that fall in the range of the start
                                    'date and the offset date.
                                    ds6 = db.GetStuTermsfromSDate(stuEnrollId, startDate, offsetDate, campusId)
                                    count = ds6.Tables(0).Rows.Count
                                    'If student does not have any terms that fall between start date & offset date
                                    'then no need to perform the SAP check
                                    If (count < 1 And sapTrigUnit <> "4" And sapTrigUnit <> "5" And trigOffsetTyp <> "start date") Then
                                        errStr = "Student has not met the trigger yet "
                                        Exit For
                                    ElseIf sapTrigUnit = "4" Or sapTrigUnit = "5" Then
                                        offsetDate = (New AttendanceFacade).GetSAPOffsetDate(stuEnrollId,campusId,(trigValue / 100), sapTrigUnit, sapInfo.TrackExternAttendance, sapInfo.IncludeTransferHours)
                                        SAPInfo.OffsetDate = offsetDate

                                        If myAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToLower = "byday" Then

                                            If attType = "CLOCK HOURS" Then
                                                attSummaryInfo = (New AttendanceFacade).GetAttendanceSummaryForCutOffDate(stuEnrollId, offsetDate, campusId)
                                                sapInfo.ActHours = attSummaryInfo.ActualDaysPresent
                                                sapInfo.ActHoursAdjusted = attSummaryInfo.TotalHoursPresent
                                                sapInfo.SchedHours = attSummaryInfo.TotalHoursSched
                                            Else
                                                If attType.ToString.Substring(0, 7).ToLower = "present" Then
                                                    attType = "pa"
                                                End If
                                                With (New FAME.AdvantageV1.DataAccess.AR.AttendanceDB).StudentSummaryFormForCutOffDate(stuEnrollId, attType, offsetDate)
                                                    sapInfo.ActHours = .TotalHoursPresent
                                                    sapInfo.SchedHours = .TotalHoursSched
                                                End With
                                            End If
                                        Else
                                            If myAdvAppSettings.AppSettings("UseImportedAttendance", campusId).ToLower = "true" And attType = "CLOCK HOURS" Then
                                                attInfo = br.GetPercentageAttendanceInfoForReportWithImportedAttendance(stuEnrollId, offsetDate)
                                                sapInfo.SchedHours = attInfo.TotalScheduled / 60
                                                sapInfo.ActHours = attInfo.TotalPresentAdjusted / 60
                                                sapInfo.ActHoursAdjusted = sapInfo.ActHours
                                            Else
                                                attInfo = br.GetPercentageAttendanceInfoForReport(stuEnrollId, offsetDate)
                                                sapInfo.SchedHours = attInfo.TotalScheduled / 60
                                                sapInfo.ActHours = attInfo.TotalPresentAdjusted / 60
                                                sapInfo.ActHoursAdjusted = sapInfo.ActHours
                                            End If
                                            If Not sapInfo.OverallAttendance Then
                                                sapInfo = PopulateSAPInfoDetails(sapInfo, sapDetail("Sapdetailid").ToString, stuEnrollId, offsetDate)
                                            End If


                                        End If


                                        If sapInfo.IncludeTransferHours = True Then
                                            sapInfo.ActHours += row("TransferHours")
                                            sapInfo.SchedHours += row("TransferHours")
                                            sapInfo.ActHoursAdjusted += row("TransferHours")
                                        End If

                                        If sapTrigUnit = "4" Then 'Actual Hours
                                            If sapDetail("TrigValue") / 100 > sapInfo.ActHours Then
                                                errStr = "Student has not met the trigger yet "
                                                Exit For
                                            End If
                                        ElseIf sapTrigUnit = "5" Then
                                            If sapDetail("TrigValue") / 100 > sapInfo.SchedHours Then
                                                errStr = "Student has not met the trigger yet "
                                                Exit For
                                            End If
                                        End If
                                        sapInfo.GPA = graduateAudit.GetGPA(stuEnrollId, , , , campusId)
                                    ElseIf trigOffsetTyp = "start date" Then

                                        'Calculate Cumulative GPA using Centralized function before making
                                        'a call to the Business Rules Layer. We have to expose the GPA
                                        'property in the facade and pass it to BR since we cannot access
                                        'the facade in the BR. 04/26/06
                                        sapInfo.GPA = graduateAudit.GetGPA(stuEnrollId, gradeReps, "1/1/1001", includeHours, campusId)
                                        If myAdvAppSettings.AppSettings("IncludeAllCreditsAttemptedinTotal").ToString.ToLower = "yes" Then
                                            sapInfo.CredsAttempted = graduateAudit.AllAttemptedCreditsForSAP
                                        Else
                                            sapInfo.CredsAttempted = graduateAudit.AttemptedCreditsForSAP
                                        End If
                                        sapInfo.CredsEarned = graduateAudit.EarnedCreditsForSAP + graduateAudit.CreditZeroWeight
                                        If sapInfo.CredsAttempted > 0 Then sapInfo.PercCredsEarned = System.Math.Round(sapInfo.CredsEarned / sapInfo.CredsAttempted * 100, 2)
                                        sapInfo.FinCredsEarned = graduateAudit.EarnedFinAidCredits
                                    Else

                                        curRow2 = ds6.Tables(0).Rows(count - 1)
                                        term = curRow2("TermId").ToString
                                        termEDate = curRow2("EndDate")
                                        termSDate = curRow2("StartDate")
                                        'Calculate Cumulative GPA using Centralized function before making
                                        'a call to the Business Rules Layer. We have to expose the GPA
                                        'property in the facade and pass it to BR since we cannot access
                                        'the facade in the BR. 04/26/06
                                        sapInfo.GPA = graduateAudit.GetGPA(stuEnrollId, gradeReps, offsetDate, includeHours, campusId)
                                        If myAdvAppSettings.AppSettings("IncludeAllCreditsAttemptedinTotal").ToString.ToLower = "yes" Then
                                            sapInfo.CredsAttempted = graduateAudit.AllAttemptedCreditsForSAP
                                        Else
                                            sapInfo.CredsAttempted = graduateAudit.AttemptedCreditsForSAP
                                        End If
                                        sapInfo.CredsEarned = graduateAudit.EarnedCreditsForSAP + graduateAudit.CreditZeroWeight
                                        If sapInfo.CredsAttempted > 0 Then sapInfo.PercCredsEarned = System.Math.Round(sapInfo.CredsEarned / sapInfo.CredsAttempted * 100, 2)
                                        sapInfo.FinCredsEarned = graduateAudit.EarnedFinAidCredits
                                    End If
                            End Select

                            'fill up the sap object
                            If sapTrigUnit = "4" Or sapTrigUnit = "5" Then
                                sapInfo.TrigValue = sapDetail("TrigValue") / 100
                            Else
                                sapInfo.TrigValue = sapDetail("TrigValue")
                            End If
                            sapInfo.TrigValue = sapDetail("TrigValue")
                            sapInfo.QuantMinUnitType = sapDetail("QuantMinTypDesc")
                            If Not sapDetail.IsNull("QuantMinValue") Then sapInfo.QuantMinValue = sapDetail("QuantMinValue")
                            sapInfo.QualMinValue = sapDetail("QualMinValue")
                            sapInfo.QuantMinUnitTypId = sapDetail("QuantMinUnitTypId")
                            If sapInfo.QuantMinUnitTypId = "3" And sapTrigUnit <> "4" And sapTrigUnit <> "5" Then
                                offsetDate = Date.Today
                                'Dim br As New AttendancePercentageBR
                                'Dim periodsDB As New PeriodsDB
                                'Dim attInfo As Common.AttendancePercentageInfo
                                'If attType = "CLOCK HOURS" Then
                                '    attSummaryInfo = (New AttendanceFacade).GetAttendanceSummaryForCutOffDate(StuEnrollId, OffsetDate)
                                '    SAPInfo.ActHours = attSummaryInfo.ActualDaysPresent
                                '    SAPInfo.ActHoursAdjusted = attSummaryInfo.TotalHoursPresent
                                '    SAPInfo.SchedHours = attSummaryInfo.TotalHoursSched
                                'Else
                                If myAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToLower = "byday" Then

                                    If attType = "CLOCK HOURS" Then
                                        attSummaryInfo = (New AttendanceFacade).GetAttendanceSummaryForCutOffDate(stuEnrollId, offsetDate)
                                        sapInfo.ActHours = attSummaryInfo.ActualDaysPresent
                                        sapInfo.ActHoursAdjusted = attSummaryInfo.TotalHoursPresent
                                        sapInfo.SchedHours = attSummaryInfo.TotalHoursSched
                                    Else


                                        If attType.ToString.Substring(0, 7).ToLower = "present" Then
                                            attType = "pa"
                                        End If
                                        With (New DataAccess.AR.AttendanceDB).StudentSummaryFormForCutOffDate(stuEnrollId, attType, offsetDate)
                                            sapInfo.ActHours = .TotalHoursPresent
                                            sapInfo.SchedHours = .TotalHoursSched
                                        End With
                                    End If
                                Else
                                    'attInfo = br.GetPercentageAttendanceInfo(StuEnrollId, OffsetDate)



                                    attInfo = br.GetPercentageAttendanceInfoForReport(stuEnrollId, offsetDate)
                                    sapInfo.SchedHours = (attInfo.TotalPresent + attInfo.TotalAbsent + attInfo.TotalExcused + attInfo.TotalTardies) / 60
                                    sapInfo.ActHours = (attInfo.TotalPresent + attInfo.TotalTardies) / 60
                                    sapInfo.ActHoursAdjusted = sapInfo.ActHours

                                End If
                                'End If

                                '

                                'SAPInfo.SchedHours = db.GetClockScheduledHours(StuEnrollId)
                                'SAPInfo.ActHours = db.GetClockActualHours(StuEnrollId)

                            End If


                            If Not sapDetail.IsNull("TrigOffsetSeq") Then
                                sapInfo.TrigOffsetSeq = sapDetail("TrigOffsetSeq")
                            End If
                            sapInfo.QualType = sapDetail("QualMinTypDesc")
                            'Commented out by Troy on 4/21/2006. It does not matter whether student is within MTF or not. 
                            'It is okay to have more than one SAP Check results for a particular increment. For example,
                            'it is possible to have more than one result for a student from NPL to MTF.
                            'If FallsWithinMTF = True Then
                            '    SAPInfo.Seq = iMTFTrigger
                            'Else
                            '    SAPInfo.Seq = SapDetail("Seq")
                            'End If
                            sapInfo.Seq = sapDetail("Seq")
                            sapInfo.TrigUnit = trigUnit
                            sapInfo.ConsequenceTypId = sapDetail("ConsequenceTypId")
                            sapInfo.MinCredsCompltd = sapDetail("MinCredsCompltd")
                            sapInfo.TrigOffsetTyp = sapDetail("TrigOffTypDescrip")
                            sapInfo.SAPDetailId = sapDetail("SAPDetailId")
                            sapInfo.MinAttendanceValue = sapDetail("MinAttendanceValue")
                            sapInfo.Period = trigToFire + 1
                            If myAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Then
                                sapInfo.OverallAverage = examsBf.GetOverallAverageByStudent(stuEnrollId, 501, 533)
                            Else
                                Dim dt As DataTable
                                dt = graduateAudit.GetResultsByEnrollment(stuEnrollId, gradeReps, strIncludeHours, , , campusId)
                                sapInfo.OverallAverage = getAverage(dt)
                            End If

                            If (trigOffsetTyp = "start of term") Or (trigOffsetTyp = "end of term") Then
                                sapInfo.TermEDate = termEDate
                                sapInfo.TermSDate = termSDate
                            End If
                            Select Case Trim(trigUnit)
                                Case "Credits Attempted"
                                    errStr = credits.IsMakingSAP(stuEnrollId, sapInfo, prgVerId, user, gradeReps, Nothing, campusId)

                                Case Is = "Days"
                                    errStr = days.IsMakingSAP(stuEnrollId, sapInfo, prgVerId, user, gradeReps, Nothing, campusId)

                                Case Is = "Weeks"
                                    If (currDate >= offsetDate) Then
                                        errStr = weeks.IsMakingSAP(stuEnrollId, sapInfo, prgVerId, user, gradeReps, Nothing, campusId)
                                    End If
                                Case Is = "Actual Hours"
                                    errStr = actualHours.IsMakingSAP(stuEnrollId, sapInfo, prgVerId, user, gradeReps, Nothing, campusId)
                                Case Is = "Scheduled Hours"
                                    errStr = actualHours.IsMakingSAP(stuEnrollId, sapInfo, prgVerId, user, gradeReps, Nothing, campusId)
                                Case Is = "Months"
                                    If attType = "CLOCK HOURS" Then
                                        attSummaryInfo = (New AttendanceFacade).GetAttendanceSummaryForCutOffDate(stuEnrollId, offsetDate)
                                        sapInfo.ActHours = attSummaryInfo.ActualDaysPresent
                                        sapInfo.ActHoursAdjusted = attSummaryInfo.TotalHoursPresent
                                        sapInfo.SchedHours = attSummaryInfo.TotalHoursSched
                                    End If
                                    errStr = months.IsMakingSAP(stuEnrollId, sapInfo, prgVerId, user, gradeReps)
                            End Select
                            If errStr.Length = 0 Then
                                'If the student is making SAP and is currently on academic probation then we need to change the status to
                                'currently attending.
                                If IsEnrollmentStatusAcademicProbation(stuEnrollId) Then

                                    'Dim sapChkDB As New SAPCheckDB
                                    'sapChkDB.ChangeEnrollmentStatusFromAcademicProbationToLastCurrentlyAttendingStatus(stuEnrollId, campusId)
                                    Dim batch As BatchStatusChangeDB = New BatchStatusChangeDB()
                                    Dim change As StudentChangeHistoryObj = New StudentChangeHistoryObj()
                                    Dim status As IStudentEnrollmentState = New AcademicProbationState()
                                    change.CampusId = campusId
                                    change.DateOfChange = DateTime.Now
                                    change.DropReasonGuid = Nothing
                                    change.IsRevelsal = 0
                                    change.Lda = Nothing
                                    change.ModDate = change.DateOfChange
                                    change.ModUser = user
                                    change.NewStatusGuid = batch.GetSchoolDefinedStatusGuid(9, campusId)
                                    change.NewSysStatusId = 9
                                    change.OldSysStatusId = 20
                                    change.OrigStatusGuid = batch.GetSchoolDefinedStatusGuid(20, campusId)
                                    change.ProbWarningTypeId = 1
                                    change.StuEnrollId = stuEnrollId
                                    change.StudentStatus = "Active"
                                    status.ChangeStatusToCurrAttending(change)
                                End If
                            End If

                        End If

                    Next 'End of Each Increment to be fired loop
                Next 'End of PrgVerSAP Loop 

            End If
            x = x + 1
            'Next 'End of Student Loop
        End While
        Return errStr
    End Function


    Public Function GetFASAPPolicyDetails(ByVal StudentArrList As ArrayList, ByVal user As String, ByVal campusId As String) As String

        '' Dim Student As SAPCheckInfo
        Dim db As New SAPCheckDB
        Dim ds As DataSet
        Dim ds1 As DataSet
        Dim ds2 As New DataSet
        Dim ds3 As DataSet
        Dim ds5 As DataSet
        Dim tbl1 As DataTable
        Dim row As DataRow
        Dim row3 As DataRow
        Dim SapDetail As DataRow
        '' Dim StuResult As DataRow
        Dim CurrRow As DataRow
        Dim SAPId As String
        Dim TrigUnit As String
        Dim CredsAttmptd As Decimal
        Dim TrigOffsetTyp As String
        Dim StartDate As Date
        Dim TrigValue As Decimal
        Dim TrigOffSeq As Integer
        Dim QuantMinUnitTyp As String
        Dim QuantMinVal As Decimal
        Dim QualMinVal As Decimal
        Dim StuEnrollId As String
        Dim SAPInfo As SAPInfo
        Dim Credits As New SAPCHK.SAPCredits
        Dim Days As New SAPCHK.SAPDays
        Dim Weeks As New SAPCHK.SAPWeeks
        Dim Months As New SAPCHK.SAPMonths
        'Dim ScheduledHours As New SAPCHK.SAPScheduledHours
        Dim ActualHours As New SAPCHK.SAPActualHours
        Dim PrgVerId As String
        Dim ErrStr As String
        Dim ds4 As New DataSet
        Dim ds6 As New DataSet
        'Dim row2 As DataRow
        'Dim TermCount As Integer
        Dim CurRow As DataRow
        Dim CurRow2 As DataRow
        Dim Term As String
        Dim TrigToFire As Integer
        Dim TermSDate As Date
        Dim TermEDate As Date
        Dim count As Integer
        Dim CurrDate As Date
        Dim OffsetDate As Date
        Dim intStudentCount As Integer
        Dim x As Integer
        Dim iNormalProgLength As Decimal
        Dim iMaxTimeFrame As Decimal
        Dim SAPTrigUnit As String
        Dim FallsWithinMTF As Boolean
        '    Dim iMTFTrigger As Integer
        Dim ProgVerDescrip As String
        Dim GraduateAudit As New GraduateAuditFacade
        Dim examsBF As New ExamsFacade
        'Dim OffsetDate_Credits As Date
        'Dim DoesStudentHaveLOA As Boolean
        Dim str As String
        Dim ErrStr2 As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        
        Dim gradeReps As String = myAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod")
        Dim strIncludeHours As String = myAdvAppSettings.AppSettings("IncludeHoursForFailingGrade")
        Dim dtTermGPA As DataTable
        Dim includeHours As Boolean
        Dim br As New AttendancePercentageBR
        Dim periodsDB As New PeriodsDB
        Dim attInfo As AttendancePercentageInfo
        Dim attSummaryInfo As Common.AR.ClockHourAttendanceInfo
        Dim attType As String

        If strIncludeHours.ToLower = "true" Then
            includeHours = True
        End If

        intStudentCount = StudentArrList.Count

        'For Each Student In StudentArrList
        While x < intStudentCount
            SAPInfo = New SAPInfo
            'StuEnrollId = Student.StudentId.ToString
            StuEnrollId = DirectCast(StudentArrList.Item(x), String)

            ds = db.GetPrgVerSAP(StuEnrollId)

            tbl1 = ds.Tables(0)
            'Make the ChildId column the primary key
            With tbl1
                .PrimaryKey = New DataColumn() {.Columns("PrgVerId")}
            End With


            If ds.Tables(0).Rows.Count > 0 Then
                'Retrieve the SAPId from the datatable and get the
                'Details for this SAP Policy.

                For Each row In tbl1.Rows 'Loop thru each program version for each student
                    'Reset the CredsAttmptd variable. Added by Troy on 4/19/2006
                    CredsAttmptd = 0
                    SAPId = row("SAPId").ToString
                    PrgVerId = row("PrgVerId").ToString
                    ProgVerDescrip = row("PrgVerDescrip")
                    attType = row("attendanceType").ToString.ToUpper()
                    SAPInfo.attType = attType
                    'iNormalProgLength = row("Credits") 'Commented out by Troy on 4/19/2006 since not all programs have credits stated
                    'Get the maximum time frame by multiplying the Normal Program Length * 1.5
                    'iMaxTimeFrame = row("Credits") * 1.5 'Commented out by Troy on 4/19/2006 since not all programs have credits stated
                    SAPTrigUnit = row("TrigUnitTypId")
                    SAPInfo.TrackExternAttendance = row("TrackExternAttendance")
                    If row("IncludeTransferHours") And Not row("ClockHrProgram").ToString = "" Then
                        SAPInfo.IncludeTransferHours = True
                    Else
                        SAPInfo.IncludeTransferHours = False
                    End If



                    'Check if student falls within the Maximum Time Frame of the Program
                    If SAPTrigUnit = 3 Then
                        iNormalProgLength = row("Credits")  'Troy on 4/19/2006. Should only be done for those programs that do have credits
                        iMaxTimeFrame = row("Credits") * 1.5 'Troy on 4/19/2006. Should only be done for those programs that do have credits
                        'Dim CreditsDS As DataSet
                        'Dim CreditsDR As DataRow
                        '**************************************************************************************
                        'The following code was commented out to use the centralized component for 
                        'computing Credits Attempted by BN 04/26/06
                        '**************************************************************************************

                        CredsAttmptd = GraduateAudit.GetCreditsAttempted(StuEnrollId, gradeReps, includeHours, campusId)
                        'Fill the SAPInfo object to pass to BR later on.
                        SAPInfo.CredsAttempted = CredsAttmptd
                        SAPInfo.CredsEarned = GraduateAudit.EarnedCredits + GraduateAudit.CreditZeroWeight
                        SAPInfo.PercCredsEarned = GraduateAudit.PercentageEarnedCredits
                        SAPInfo.FinCredsEarned = GraduateAudit.EarnedFinAidCredits
                        '**************************************************************************************
                        If CredsAttmptd > iNormalProgLength And CredsAttmptd < iMaxTimeFrame Then
                            FallsWithinMTF = True
                        ElseIf CredsAttmptd > iMaxTimeFrame Then 'Fix for mantis # 7296
                            ErrStr = "Cannot run SAP Check since student's attempted credits exceeds MTF."
                        End If
                    End If


                    'Get the SAP Policy Details for the SAP Policy
                    'attached to this program version.
                    ds1 = db.GetSAPPolicyDetails(SAPId)

                    'Get the trigger/increment to be fired for this student.
                    If SAPTrigUnit <> 3 Then
                        ds5 = db.GetTrigFASAPToBeFired(StuEnrollId)

                        If ds5.Tables(0).Rows.Count > 0 Then
                            row3 = ds5.Tables(0).Rows(0)

                            If Not (row3 Is Nothing) Then
                                TrigToFire = row3("Period")
                            End If
                        Else
                            TrigToFire = 0
                        End If

                    Else
                        'Find the row that contains the trigger for Normal Program Length
                        Dim arows2 As DataRow() = ds1.Tables(0).Select("TrigValue =" & (iNormalProgLength * 100))
                        Try
                            TrigToFire = arows2(0)("Seq") - 1
                        Catch ex As System.Exception
                            ErrStr2 = ("ErrStr2The SAP Check process has been halted since the program version- " & ProgVerDescrip & " does not have an increment set up for Normal Program Length- " & iNormalProgLength & " credits")
                            Return ErrStr2
                        End Try

                        If myAdvAppSettings.AppSettings("SAPUsesCreditRanges") = "True" Then
                            'We need the first value that is greater than or equal to the credits attempted
                            Dim aRows As DataRow() = ds1.Tables(0).Select("TrigValue >= " & CredsAttmptd * 100)
                            TrigToFire = aRows(0)("Seq") - 1
                        Else
                            'We need the first value that is less than or equal to the credits attempted
                            Dim aRows As DataRow() = ds1.Tables(0).Select("TrigValue <= " & CredsAttmptd * 100)
                            'In this case if there are no credits attempted the result set returned will be empty
                            If aRows.Length = 0 Then
                                ErrStr = "The student has not yet attempted any credits."
                                Exit For
                            Else
                                'we need the last entry in the array
                                Dim len As Integer = aRows.Length
                                TrigToFire = aRows(len - 1)("Seq") - 1
                            End If
                        End If

                    End If



                    'Get the Trigger Unit for this SAP Policy.
                    Try
                        SapDetail = ds1.Tables(0).Rows(TrigToFire)

                        TrigUnit = SapDetail("TrigUnitTypDescrip")
                        TrigOffsetTyp = SapDetail("TrigOffTypDescrip")

                        'Get info for the current trigger
                        TrigValue = SapDetail("TrigValue")
                        QuantMinUnitTyp = SapDetail("QuantMinTypDesc")
                        If SapDetail.IsNull("QuantMinValue") Then
                            SAPInfo.OverallAttendance = False
                        Else
                            SAPInfo.OverallAttendance = True
                            QuantMinVal = SapDetail("QuantMinValue")
                        End If
                        QualMinVal = SapDetail("QualMinValue")
                        If Not SapDetail.IsNull("TrigOffsetSeq") Then
                            TrigOffSeq = SapDetail("TrigOffsetSeq")
                        End If
                    Catch
                        ErrStr = "No more SAP checks left for this student "
                        Exit For
                    End Try

                    'Get the Term StartDate & EndDate related to this trigger.
                    If (TrigOffsetTyp = "start of term") Or (TrigOffsetTyp = "end of term") Then
                        ds4 = db.GetStuTerms(StuEnrollId)
                        count = ds4.Tables(0).Rows.Count
                    End If
                    Select Case (TrigOffsetTyp)
                        Case "start of term"
                            If ((count >= TrigOffSeq - 1) And TrigOffSeq >= 2) Then
                                CurRow = ds4.Tables(0).Rows(TrigOffSeq - 2)
                                Term = CurRow("TermId").ToString
                                TermEDate = CurRow("EndDate")
                                TermSDate = CurRow("StartDate")
                                If (TrigUnit = "Days") Then
                                    If DateAdd(DateInterval.Day, TrigValue / 100, TermSDate) > Date.Now Then
                                        ErrStr = "It is not yet time to check this increment."
                                        Exit For
                                    End If
                                    SAPInfo.OffsetDate = DateAdd(DateInterval.Day, TrigValue / 100, TermSDate)
                                ElseIf (TrigUnit = "Weeks") Then
                                    If DateAdd(DateInterval.WeekOfYear, TrigValue / 100, TermSDate) > Date.Now Then
                                        ErrStr = "It is not yet time to check this increment."
                                        Exit For
                                    End If
                                    SAPInfo.OffsetDate = DateAdd(DateInterval.WeekOfYear, TrigValue / 100, TermSDate)
                                End If

                            Else
                                ErrStr = "Results do not exist for Term " & TrigOffSeq
                                Exit For
                            End If
                            'Calculate Cumulative GPA using Centralized function before making
                            'a call to the Business Rules Layer. We have to expose the GPA
                            'property in the facade and pass it to BR since we cannot access
                            'the facade in the BR. 04/26/06
                            SAPInfo.GPA = GraduateAudit.GetGPA(StuEnrollId, gradeReps, TermEDate, includeHours, campusId)
                            SAPInfo.CredsAttempted = GraduateAudit.AllAttemptedCredits
                            SAPInfo.CredsEarned = GraduateAudit.EarnedCredits + GraduateAudit.CreditZeroWeight
                            If SAPInfo.CredsAttempted > 0 Then SAPInfo.PercCredsEarned = System.Math.Round(SAPInfo.CredsEarned / SAPInfo.CredsAttempted * 100, 2)
                            SAPInfo.FinCredsEarned = GraduateAudit.EarnedFinAidCredits

                        Case "end of term"
                            If (count >= TrigOffSeq) Then
                                CurRow = ds4.Tables(0).Rows(TrigOffSeq - 1)
                                Term = CurRow("TermId").ToString
                                SAPInfo.Term = Term

                                If Not (row("MinTermGPA") Is System.DBNull.Value) Then
                                    SAPInfo.MinTermGPA = row("MinTermGPA").ToString
                                End If
                                dtTermGPA = GraduateAudit.GetTermProgressFromResults(StuEnrollId, gradeReps, includeHours, , , campusId)
                                Dim arows2 As DataRow() = dtTermGPA.Select("TermId ='" & Term & "'")
                                If (arows2.Length > 0) Then SAPInfo.TermGPA = arows2(0)("TermGPA")


                                If Not (row("TermGPAOver") Is System.DBNull.Value) Then
                                    SAPInfo.TermGPAOver = row("TermGPAOver").ToString
                                End If
                                TermEDate = CurRow("EndDate")
                                TermSDate = CurRow("StartDate")
                                If (TrigUnit = "Days") Then
                                    If DateAdd(DateInterval.Day, TrigValue / 100, TermEDate) > Date.Now Then
                                        ErrStr = "It is not yet time to check this increment."
                                        Exit For
                                    End If
                                    SAPInfo.OffsetDate = DateAdd(DateInterval.Day, TrigValue / 100, TermEDate)
                                ElseIf (TrigUnit = "Weeks") Then
                                    'If DateAdd(DateInterval.WeekOfYear, TrigValue, TermEDate) > Date.Now Then
                                    '    ErrStr = "It is not yet time to check this increment."
                                    '    Exit For
                                    'End If
                                    If TermEDate.AddDays((TrigValue / 100) * 7) > Date.Now Then
                                        ErrStr = "It is not yet time to check this increment."
                                        Exit For
                                    End If
                                    'SAPInfo.OffsetDate = DateAdd(DateInterval.WeekOfYear, TrigValue, TermEDate)
                                    SAPInfo.OffsetDate = TermEDate.AddDays((TrigValue / 100) * 7)
                                ElseIf (TrigUnit = "Months") Then
                                    If DateAdd(DateInterval.Month, TrigValue / 100, TermEDate) > Date.Now Then
                                        ErrStr = "It is not yet time to check this increment."
                                        Exit For
                                    End If
                                    SAPInfo.OffsetDate = DateAdd(DateInterval.Month, TrigValue / 100, TermEDate)
                                End If

                            Else
                                ErrStr = "Results do not exist for Term " & TrigOffSeq
                                Exit For
                            End If
                            'Calculate Cumulative GPA using Centralized function before making
                            'a call to the Business Rules Layer. We have to expose the GPA
                            'property in the facade and pass it to BR since we cannot access
                            'the facade in the BR. 04/26/06
                            SAPInfo.GPA = GraduateAudit.GetGPA(StuEnrollId, gradeReps, TermEDate, includeHours, campusId)
                            SAPInfo.CredsAttempted = GraduateAudit.AllAttemptedCredits
                            SAPInfo.CredsEarned = GraduateAudit.EarnedCredits + GraduateAudit.CreditZeroWeight
                            If SAPInfo.CredsAttempted > 0 Then SAPInfo.PercCredsEarned = System.Math.Round(SAPInfo.CredsEarned / SAPInfo.CredsAttempted * 100, 2)
                            SAPInfo.FinCredsEarned = GraduateAudit.EarnedFinAidCredits

                        Case "start date"
                            'Get Student start date
                            ds3 = db.GetStuStartDate(StuEnrollId)
                            CurrRow = ds3.Tables(0).Rows(0)
                            'Get the value of the StartDate Column in the table
                            StartDate = CurrRow("StartDate")

                            'Get the offset date/credits attempted to trigger the sap check
                            If (TrigUnit = "Days") Then
                                SAPInfo.OffsetDate = DateAdd(DateInterval.Day, TrigValue / 100, StartDate)
                                'SAPInfo.OffsetDate = "4/1/2009"
                                OffsetDate = DateAdd(DateInterval.Day, TrigValue / 100, StartDate)

                                'Check if student has any LOA's and subtract that period of time accordingly.
                                str = db.DoesStudentHaveLOA(StuEnrollId, StartDate, OffsetDate)

                                Dim arrRange() As String
                                Dim NoOfLOADays As Integer
                                Dim LOAStart As Date
                                Dim LOAEnd As Date
                                arrRange = str.Split(",")
                                If arrRange(0) <> "" Then
                                    LOAStart = CDate(arrRange(0))
                                End If
                                If arrRange(1) <> "" Then
                                    LOAEnd = CDate(arrRange(1))
                                End If
                                count = arrRange(2)

                                If count > 0 Then
                                    NoOfLOADays = DateDiff(DateInterval.Day, LOAStart, LOAEnd)
                                    OffsetDate = DateAdd(DateInterval.Day, NoOfLOADays, OffsetDate)
                                End If

                            ElseIf (TrigUnit = "Weeks") Then
                                'SAPInfo.OffsetDate = DateAdd(DateInterval.WeekOfYear, TrigValue, StartDate)
                                SAPInfo.OffsetDate = StartDate.AddDays((TrigValue / 100) * 7)
                                OffsetDate = StartDate.AddDays((TrigValue / 100) * 7)
                                'Check if student has any LOA's and subtract that period of time accordingly.
                                str = db.DoesStudentHaveLOA(StuEnrollId, StartDate, OffsetDate)

                                Dim arrRange() As String
                                Dim NoOfLOADays As Integer
                                Dim LOAStart As Date
                                Dim LOAEnd As Date
                                arrRange = str.Split(",")
                                If arrRange(0) <> "" Then
                                    LOAStart = CDate(arrRange(0))
                                End If
                                If arrRange(1) <> "" Then
                                    LOAEnd = CDate(arrRange(1))
                                End If
                                count = arrRange(2)

                                If count > 0 Then
                                    NoOfLOADays = DateDiff(DateInterval.Day, LOAStart, LOAEnd)
                                    OffsetDate = DateAdd(DateInterval.Day, NoOfLOADays, OffsetDate)
                                End If
                            ElseIf (TrigUnit = "Months") Then
                                SAPInfo.OffsetDate = StartDate.AddMonths(TrigValue / 100)
                                'SAPInfo.OffsetDate = "4/1/2009"
                                OffsetDate = StartDate.AddMonths(TrigValue / 100)

                                'Check if student has any LOA's and subtract that period of time accordingly.
                                str = db.DoesStudentHaveLOA(StuEnrollId, StartDate, OffsetDate)

                                Dim arrRange() As String
                                Dim NoOfLOADays As Integer
                                Dim LOAStart As Date
                                Dim LOAEnd As Date
                                arrRange = str.Split(",")
                                If arrRange(0) <> "" Then
                                    LOAStart = CDate(arrRange(0))
                                End If
                                If arrRange(1) <> "" Then
                                    LOAEnd = CDate(arrRange(1))
                                End If
                                count = arrRange(2)

                                If count > 0 Then
                                    NoOfLOADays = DateDiff(DateInterval.Day, LOAStart, LOAEnd)
                                    OffsetDate = DateAdd(DateInterval.Day, NoOfLOADays, OffsetDate)
                                End If
                            ElseIf (TrigUnit = "Credits") Then
                                'Get credits attempted
                                Dim dataset As New DataSet

                                'Commented out by Troy on 4/19/2006 because the CredsAttmptd variable is already
                                'populated earlier on in the code
                                'Check if student meets trigger
                                If myAdvAppSettings.AppSettings("SAPUsesCreditRanges") <> "True" Then
                                    'If we get to this point there is no need to check if student has met trigger
                                    'Get offset date for credits
                                    Dim strDate As String = db.GetOffsetDateForCreditsPolicy(StuEnrollId)
                                    If strDate <> "" Then
                                        SAPInfo.OffsetDate = strDate
                                    End If
                                    'Calculate Cumulative GPA using Centralized function before making
                                    'a call to the Business Rules Layer. We have to expose the GPA
                                    'property in the facade and pass it to BR since we cannot access
                                    'the facade in the BR. 05/02/06
                                    SAPInfo.GPA = GraduateAudit.GetGPA()
                                    SAPInfo.CredsAttempted = GraduateAudit.AllAttemptedCredits
                                    SAPInfo.CredsEarned = GraduateAudit.EarnedCredits + GraduateAudit.CreditZeroWeight
                                    If SAPInfo.CredsAttempted > 0 Then SAPInfo.PercCredsEarned = System.Math.Round(SAPInfo.CredsEarned / SAPInfo.CredsAttempted * 100, 2)
                                    SAPInfo.FinCredsEarned = GraduateAudit.EarnedFinAidCredits
                                    Exit Select
                                    'Else
                                    'ErrStr = "Student has not met the trigger yet "
                                    'Exit For
                                    'End If
                                Else
                                    'Get offset date for credits
                                    Dim strDate As String = db.GetOffsetDateForCreditsPolicy(StuEnrollId)
                                    If strDate <> "" Then
                                        SAPInfo.OffsetDate = strDate
                                    End If

                                    'Calculate Cumulative GPA using Centralized function before making
                                    'a call to the Business Rules Layer. We have to expose the GPA
                                    'property in the facade and pass it to BR since we cannot access
                                    'the facade in the BR. 04/26/06
                                    SAPInfo.GPA = GraduateAudit.GetGPA(StuEnrollId, gradeReps, , includeHours, campusId)
                                    SAPInfo.CredsAttempted = GraduateAudit.AllAttemptedCredits
                                    SAPInfo.CredsEarned = GraduateAudit.EarnedCredits + GraduateAudit.CreditZeroWeight
                                    If SAPInfo.CredsAttempted > 0 Then SAPInfo.PercCredsEarned = System.Math.Round(SAPInfo.CredsEarned / SAPInfo.CredsAttempted * 100, 2)
                                    SAPInfo.FinCredsEarned = GraduateAudit.EarnedFinAidCredits

                                    'If we are using ranges of credits there is no need to check if student has met trigger.
                                    Exit Select
                                End If

                            End If

                            'Get the current date
                            CurrDate = Date.Now

                            'Check if student has met the trigger.
                            If (CurrDate >= OffsetDate) Then

                            Else
                                ErrStr = "Student has not met the trigger yet "
                                Exit For
                            End If

                            'Get student terms that fall in the range of the start
                            'date and the offset date.
                            ds6 = db.GetStuTermsfromSDate(StuEnrollId, StartDate, OffsetDate, campusId)
                            count = ds6.Tables(0).Rows.Count
                            'If student does not have any terms that fall between start date & offset date
                            'then no need to perform the SAP check
                            If (count < 1 And SAPTrigUnit <> "4" And SAPTrigUnit <> "5" And TrigOffsetTyp <> "start date") Then
                                ErrStr = "Student has not met the trigger yet "
                                Exit For
                            ElseIf SAPTrigUnit = "4" Or SAPTrigUnit = "5" Then
                                OffsetDate = Date.Today
                                'SAPInfo.OffsetDate = OffsetDate

                                If myAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToLower = "byday" Then

                                    If attType = "CLOCK HOURS" Then
                                        attSummaryInfo = (New AttendanceFacade).GetAttendanceSummaryForCutOffDate(StuEnrollId, OffsetDate, campusId)
                                        SAPInfo.ActHours = attSummaryInfo.ActualDaysPresent
                                        SAPInfo.ActHoursAdjusted = attSummaryInfo.TotalHoursPresent
                                        SAPInfo.SchedHours = attSummaryInfo.TotalHoursSched
                                    Else
                                        If attType.ToString.Substring(0, 7).ToLower = "present" Then
                                            attType = "pa"
                                        End If
                                        With (New FAME.AdvantageV1.DataAccess.AR.AttendanceDB).StudentSummaryFormForCutOffDate(StuEnrollId, attType, OffsetDate)
                                            SAPInfo.ActHours = .TotalHoursPresent
                                            SAPInfo.SchedHours = .TotalHoursSched
                                        End With
                                    End If
                                Else
                                    If myAdvAppSettings.AppSettings("UseImportedAttendance", campusId).ToLower = "true" And attType = "CLOCK HOURS" Then
                                        attInfo = br.GetPercentageAttendanceInfoForReportWithImportedAttendance(StuEnrollId, OffsetDate)
                                        SAPInfo.SchedHours = attInfo.TotalScheduled / 60
                                        SAPInfo.ActHours = attInfo.TotalPresentAdjusted / 60
                                        SAPInfo.ActHoursAdjusted = SAPInfo.ActHours
                                    Else
                                        attInfo = br.GetPercentageAttendanceInfoForReport(StuEnrollId, OffsetDate)
                                        SAPInfo.SchedHours = attInfo.TotalScheduled / 60
                                        SAPInfo.ActHours = attInfo.TotalPresentAdjusted / 60
                                        SAPInfo.ActHoursAdjusted = SAPInfo.ActHours
                                    End If
                                    If Not SAPInfo.OverallAttendance Then
                                        SAPInfo = PopulateSAPInfoDetails(SAPInfo, SapDetail("Sapdetailid").ToString, StuEnrollId, OffsetDate)
                                    End If


                                End If


                                If SAPInfo.IncludeTransferHours = True Then
                                    SAPInfo.ActHours += row("TransferHours")
                                    SAPInfo.SchedHours += row("TransferHours")
                                    SAPInfo.ActHoursAdjusted += row("TransferHours")
                                End If

                                If SAPTrigUnit = "4" Then 'Actual Hours
                                    If SapDetail("TrigValue") / 100 > SAPInfo.ActHours Then
                                        ErrStr = "Student has not met the trigger yet "
                                        Exit For
                                    End If
                                ElseIf SAPTrigUnit = "5" Then
                                    If SapDetail("TrigValue") / 100 > SAPInfo.SchedHours Then
                                        ErrStr = "Student has not met the trigger yet "
                                        Exit For
                                    End If
                                End If
                                SAPInfo.GPA = GraduateAudit.GetGPA(StuEnrollId, , , , campusId)
                            ElseIf TrigOffsetTyp = "start date" Then
                                SAPInfo.GPA = GraduateAudit.GetGPA(StuEnrollId, gradeReps, "1/1/1001", includeHours, campusId)
                                SAPInfo.CredsAttempted = GraduateAudit.AllAttemptedCredits
                                SAPInfo.CredsEarned = GraduateAudit.EarnedCredits + GraduateAudit.CreditZeroWeight
                                If SAPInfo.CredsAttempted > 0 Then SAPInfo.PercCredsEarned = System.Math.Round(SAPInfo.CredsEarned / SAPInfo.CredsAttempted * 100, 2)
                                SAPInfo.FinCredsEarned = GraduateAudit.EarnedFinAidCredits
                            Else

                                CurRow2 = ds6.Tables(0).Rows(count - 1)
                                Term = CurRow2("TermId").ToString
                                TermEDate = CurRow2("EndDate")
                                TermSDate = CurRow2("StartDate")
                                'Calculate Cumulative GPA using Centralized function before making
                                'a call to the Business Rules Layer. We have to expose the GPA
                                'property in the facade and pass it to BR since we cannot access
                                'the facade in the BR. 04/26/06
                                SAPInfo.GPA = GraduateAudit.GetGPA(StuEnrollId, gradeReps, OffsetDate, includeHours, campusId)
                                SAPInfo.CredsAttempted = GraduateAudit.AllAttemptedCredits
                                SAPInfo.CredsEarned = GraduateAudit.EarnedCredits + GraduateAudit.CreditZeroWeight
                                If SAPInfo.CredsAttempted > 0 Then SAPInfo.PercCredsEarned = System.Math.Round(SAPInfo.CredsEarned / SAPInfo.CredsAttempted * 100, 2)
                                SAPInfo.FinCredsEarned = GraduateAudit.EarnedFinAidCredits
                            End If
                    End Select

                    'fill up the sap object
                    If SAPTrigUnit = "4" Or SAPTrigUnit = "5" Then
                        SAPInfo.TrigValue = SapDetail("TrigValue") / 100
                    Else
                        SAPInfo.TrigValue = SapDetail("TrigValue")
                    End If
                    SAPInfo.TrigValue = SapDetail("TrigValue")
                    SAPInfo.QuantMinUnitType = SapDetail("QuantMinTypDesc")
                    If Not SapDetail.IsNull("QuantMinValue") Then SAPInfo.QuantMinValue = SapDetail("QuantMinValue")
                    SAPInfo.QualMinValue = SapDetail("QualMinValue")
                    SAPInfo.QuantMinUnitTypId = SapDetail("QuantMinUnitTypId")
                    If SAPInfo.QuantMinUnitTypId = "3" And SAPTrigUnit <> "4" And SAPTrigUnit <> "5" Then
                        OffsetDate = Date.Today

                        If myAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToLower = "byday" Then

                            If attType = "CLOCK HOURS" Then
                                attSummaryInfo = (New AttendanceFacade).GetAttendanceSummaryForCutOffDate(StuEnrollId, OffsetDate)
                                SAPInfo.ActHours = attSummaryInfo.ActualDaysPresent
                                SAPInfo.ActHoursAdjusted = attSummaryInfo.TotalHoursPresent
                                SAPInfo.SchedHours = attSummaryInfo.TotalHoursSched
                            Else


                                If attType.ToString.Substring(0, 7).ToLower = "present" Then
                                    attType = "pa"
                                End If
                                With (New FAME.AdvantageV1.DataAccess.AR.AttendanceDB).StudentSummaryFormForCutOffDate(StuEnrollId, attType, OffsetDate)
                                    SAPInfo.ActHours = .TotalHoursPresent
                                    SAPInfo.SchedHours = .TotalHoursSched
                                End With
                            End If
                        Else



                            attInfo = br.GetPercentageAttendanceInfoForReport(StuEnrollId, OffsetDate)
                            SAPInfo.SchedHours = (attInfo.TotalPresent + attInfo.TotalAbsent + attInfo.TotalExcused + attInfo.TotalTardies) / 60
                            SAPInfo.ActHours = (attInfo.TotalPresent + attInfo.TotalTardies) / 60
                            SAPInfo.ActHoursAdjusted = SAPInfo.ActHours


                        End If

                    End If


                    If Not SapDetail.IsNull("TrigOffsetSeq") Then
                        SAPInfo.TrigOffsetSeq = SapDetail("TrigOffsetSeq")
                    End If
                    SAPInfo.QualType = SapDetail("QualMinTypDesc")
                    'Commented out by Troy on 4/21/2006. It does not matter whether student is within MTF or not. 
                    'It is okay to have more than one SAP Check results for a particular increment. For example,
                    'it is possible to have more than one result for a student from NPL to MTF.

                    SAPInfo.Seq = SapDetail("Seq")
                    SAPInfo.TrigUnit = TrigUnit
                    SAPInfo.ConsequenceTypId = SapDetail("ConsequenceTypId")
                    SAPInfo.MinCredsCompltd = SapDetail("MinCredsCompltd")
                    SAPInfo.TrigOffsetTyp = SapDetail("TrigOffTypDescrip")
                    SAPInfo.SAPDetailId = SapDetail("SAPDetailId")
                    SAPInfo.MinAttendanceValue = SapDetail("MinAttendanceValue")
                    SAPInfo.Period = TrigToFire + 1
                    If myAdvAppSettings.AppSettings("ShowROSSOnlyTabsForStudent").ToString.ToLower = "true" Then
                        SAPInfo.OverallAverage = examsBF.GetOverallAverageByStudent(StuEnrollId, 501, 533)
                    Else
                        Dim dt As DataTable
                        dt = GraduateAudit.GetResultsByEnrollment(StuEnrollId, gradeReps, strIncludeHours, , , campusId)
                        SAPInfo.OverallAverage = getAverage(dt)
                    End If

                    If (TrigOffsetTyp = "start of term") Or (TrigOffsetTyp = "end of term") Then
                        SAPInfo.TermEDate = TermEDate
                        SAPInfo.TermSDate = TermSDate
                    End If
                    Select Case Trim(TrigUnit)
                        Case "Credits"
                            ErrStr = Credits.IsMakingFASAP(StuEnrollId, SAPInfo, PrgVerId, user, gradeReps, Nothing, campusId)

                        Case Is = "Days"
                            ErrStr = Days.IsMakingFASAP(StuEnrollId, SAPInfo, PrgVerId, user, gradeReps, Nothing, campusId)

                        Case Is = "Weeks"
                            If (CurrDate >= OffsetDate) Then
                                ErrStr = Weeks.IsMakingFASAP(StuEnrollId, SAPInfo, PrgVerId, user, gradeReps, Nothing, campusId)
                            End If
                        Case Is = "Actual Hours"
                            ErrStr = ActualHours.IsMakingFASAP(StuEnrollId, SAPInfo, PrgVerId, user, gradeReps, Nothing, campusId)
                        Case Is = "Scheduled Hours"
                            ErrStr = ActualHours.IsMakingFASAP(StuEnrollId, SAPInfo, PrgVerId, user, gradeReps, Nothing, campusId)
                        Case Is = "Months"
                            If attType = "CLOCK HOURS" Then
                                attSummaryInfo = (New AttendanceFacade).GetAttendanceSummaryForCutOffDate(StuEnrollId, OffsetDate)
                                SAPInfo.ActHours = attSummaryInfo.ActualDaysPresent
                                SAPInfo.ActHoursAdjusted = attSummaryInfo.TotalHoursPresent
                                SAPInfo.SchedHours = attSummaryInfo.TotalHoursSched
                            End If
                            ErrStr = Months.IsMakingFASAP(StuEnrollId, SAPInfo, PrgVerId, user, gradeReps)
                    End Select
                    If ErrStr.Length = 0 Then
                        'If the student is making SAP and is currently on academic probation then we need to change the status to
                        'currently attending.
                        If IsEnrollmentStatusAcademicProbation(StuEnrollId) Then

                            Dim batch As BatchStatusChangeDB = New BatchStatusChangeDB()
                            Dim change As StudentChangeHistoryObj = New StudentChangeHistoryObj()
                            Dim status As IStudentEnrollmentState = New AcademicProbationState()
                            change.CampusId = campusId
                            change.DateOfChange = DateTime.Now
                            change.DropReasonGuid = Nothing
                            change.IsRevelsal = 0
                            change.Lda = Nothing
                            change.ModDate = change.DateOfChange
                            change.ModUser = user
                            change.NewStatusGuid = batch.GetSchoolDefinedStatusGuid(9, campusId)
                            change.NewSysStatusId = 9
                            change.OldSysStatusId = 20
                            change.OrigStatusGuid = batch.GetSchoolDefinedStatusGuid(20, campusId)
                            change.ProbWarningTypeId = 1
                            change.StuEnrollId = StuEnrollId
                            change.StudentStatus = "Active"
                            status.ChangeStatusToCurrAttending(change)
                            'Dim sapChkDB As New SAPCheckDB
                            'sapChkDB.ChangeEnrollmentStatusFromAcademicProbationToLastCurrentlyAttendingStatus(StuEnrollId, campusId)

                        End If
                    End If


                Next 'End of PrgVerSAP Loop 

            End If
            x = x + 1

        End While
        Return ErrStr
    End Function
    Public Function GetTrigFASAPToBeFired(ByVal StuEnrollId As String) As String
        Dim ds1 As New DataSet
        ''  Dim Student As SAPCheckInfo
        Dim db As New SAPCheckDB

        ds1 = db.GetTrigFASAPToBeFired(StuEnrollId)
    End Function


    Public Function GetTrigToBeFired(ByVal StuEnrollId As String) As String
        Dim ds1 As New DataSet
        ''  Dim Student As SAPCheckInfo
        Dim db As New SAPCheckDB

        ds1 = db.GetTrigToBeFired(StuEnrollId)
    End Function
    'Private Sub DisplayErroessage(ByVal errorMessage As String)

    '    '   Display error in message box in the client
    '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    'End Sub
    Public Function IsEnrollmentStatusAcademicProbation(ByVal stuEnrollId As String) As Boolean
        Dim sapChkDB As New SAPCheckDB
        Dim probDB As New ProbationDB
        Dim probStatus As String
        Dim curStatus As String

        probStatus = probDB.GetAcademicProbationStatus()
        curStatus = sapChkDB.GetEnrollmentCurrentStatus(stuEnrollId)

        If probStatus = curStatus Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Function getAverage(ByVal dt As DataTable) As Decimal
        Dim rtn As Decimal = 0.0
        Dim sum As Decimal = 0.0
        Dim cnt As Integer = dt.Rows.Count
        If cnt = 0 Then Return 0.0
        For i As Integer = 0 To dt.Rows.Count - 1
            If Not dt.Rows(i).IsNull("Score") Then
                sum = sum + CType(dt.Rows(i)("Score"), Decimal)
            End If

        Next
        If sum > 0 Then
            rtn = Math.Round(sum / cnt, 2)
        End If
        Return rtn
    End Function
    Public Function PerformFASAPCheck(ByVal dtStudent As DataTable) As DataTable
        Dim db As New SAPCheckDB
        Dim msg As String
        If dtStudent.Rows.Count > 0 Then
            msg = db.PerformFASAPCheck(dtStudent)
            If (String.IsNullOrEmpty(msg) = False) Then
                Throw New ApplicationException(msg)
            End If
        
        End If
        Return dtStudent
    End Function
    Public Function PerformSAPCheck(ByVal dtStudent As DataTable) As DataTable
        Dim db As New SAPCheckDB
        Dim msg As String
        If dtStudent.Rows.Count > 0 Then
            msg = db.PerformSAPCheck(dtStudent)
            'If (String.IsNullOrEmpty(msg) = False) Then
            '    Throw New ApplicationException(msg)
            'End If
        End If
        Return dtStudent
    End Function
    Public Function CheckFASAPDetailsExists(ByVal prgVerId As String) As Boolean
        Return (New SAPCheckDB).CheckFASAPDetailsExists(prgVerId)
    End Function
    Public Function CheckSAPDetailsExists(ByVal prgVerId As String) As Boolean
        Return (New SAPCheckDB).CheckSAPDetailsExists(prgVerId)
    End Function
    Private Function PopulateSAPInfoDetails(sap As SAPInfo, sapDetailId As String, stueEnrollId As String, OffsetDate As Date) As SAPInfo
        Dim dtInstructionTypes As DataTable
        Dim SAPDB As New SAPCheckDB
        dtInstructionTypes = SAPDB.GetSAPPolicyInstuctionDetails(sapDetailId)
        Dim _QuantMinValueByInsTypes(dtInstructionTypes.Rows.Count) As QuantMinValueByInsType

        For i As Integer = 0 To dtInstructionTypes.Rows.Count - 1
            _QuantMinValueByInsTypes(i) = New QuantMinValueByInsType
            _QuantMinValueByInsTypes(i).InstructionTypeId = dtInstructionTypes.Rows(i)("InstructionTypeid")
            _QuantMinValueByInsTypes(i).QuantMinValue = dtInstructionTypes.Rows(i)("QuantMinValue")
            _QuantMinValueByInsTypes(i).SAPDetailId = dtInstructionTypes.Rows(i)("sapDetailId")
            _QuantMinValueByInsTypes(i).InstructionTypeDescrip = dtInstructionTypes.Rows(i)("InstructionTypeDescrip")
            _QuantMinValueByInsTypes(i).attDetailsInfo = (New AttendancePercentageBR).GetPercentageAttendanceInfoForReport_ByInsType(stueEnrollId, OffsetDate, dtInstructionTypes.Rows(i)("InstructionTypeid").ToString)
            sap.AddQuantMinValueByInsType(_QuantMinValueByInsTypes(i))
        Next
        Return sap
    End Function
    Public Function CheckForSAPDependency(ByVal SAPId As String) As Boolean
        Return (New SAPCheckDB).CheckForSAPDependency(SAPId)
    End Function
    Public Function CheckForFASAPDependency(ByVal SAPId As String) As Boolean
        Return (New SAPCheckDB).CheckForFASAPDependency(SAPId)
    End Function

End Class
