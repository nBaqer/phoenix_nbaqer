
Public Class AcreditationAgenciesFacade
    Public Function GetFieldsUsedByAcreditationAgency(ByVal acreditationAgencyId As Integer) As DataSet
        'return Dataset
        Return (New AcreditationAgenciesDB).GetFieldsUsedByAcreditationAgency(acreditationAgencyId)
    End Function
    Public Function GetDDLValuesDS(ByVal agencyId As Integer, ByVal agencyFieldId As Integer) As DataSet
        'return Dataset
        Return (New AcreditationAgenciesDB).GetDDLValuesDS(agencyId, agencyFieldId)
    End Function
    Public Function ValidateDDLValues(ByVal agencyId As Integer, ByVal agencyFieldId As Integer) As String
        'return string
        Return (New AcreditationAgenciesDB).ValidateDDLValues(agencyId, agencyFieldId)
    End Function
    'Public Function GetMappingOptionsByField(ByVal acreditationAgencyFieldId As Integer) As DataSet
    '    'return Dataset
    '    Return (New AcreditationAgenciesDB).GetMappingOptionsByField(acreditationAgencyFieldId)
    'End Function
    ' DE 1131 Janet Robinson 5/3/2011
    Public Function UpdateMappedValues(ByVal mappingOptionsDS As DataSet) As String
        'return Dataset
        Return (New AcreditationAgenciesDB).UpdateMappedValues(mappingOptionsDS)
    End Function
End Class

