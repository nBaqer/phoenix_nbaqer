Public Class CourseDropsObject
    Inherits BaseReportFacade


#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal paramInfo As ReportParamInfo) As DataSet
        Dim stuCourseDrops As New CourseDropsDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data expected by report.
        ds = BuildReportSource(stuCourseDrops.GetCourseDrops(paramInfo), stuCourseDrops.StudentIdentifier)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim stuName As String
        Dim facInputMasks As New InputMasksFacade

        'Get the mask for SSN
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            For Each dr As DataRow In ds.Tables(0).Rows
                dr("CourseDropCount") = ds.Tables(0).Rows.Count
                dr("TermIdStr") = dr("TermId").ToString

                'Set up student name as: "LastName, FirstName MI."
                stuName = dr("LastName")
                If Not (dr("FirstName") Is System.DBNull.Value) Then
                    If dr("FirstName") <> "" Then
                        stuName &= ", " & dr("FirstName")
                    End If
                End If
                If Not (dr("MiddleName") Is System.DBNull.Value) Then
                    If dr("MiddleName") <> "" Then
                        stuName &= " " & dr("MiddleName") & "."
                    End If
                End If
                dr("StudentName") = stuName
                '
                'Apply mask to SSN.
                If StudentIdentifier = "SSN" Then
                    If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                        If dr("StudentIdentifier") <> "" Then
                            Dim temp As String = dr("StudentIdentifier")
                            dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                        End If
                    End If
                End If
            Next

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region


End Class

