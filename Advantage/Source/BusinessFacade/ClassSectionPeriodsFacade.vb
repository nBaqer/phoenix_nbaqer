Imports System.data
Imports System.Data.OleDb
'Imports FAME.AdvantageV1.BusinessRules
Imports FAME.AdvantageV1.DataAccess
Public Class ClassSectionPeriodsFacade
    Public Function InitialLoadPage(ByVal CampusId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim olddt As DataTable
        Dim newdt As DataTable
        Dim db As New ClassSectionPeriodsDB

        ds = db.GetDropDownList(CampusId)

        '     ds = FormatDateField(ds)
        'Add the concatenated FullName column to the datatable before sending it to the web page
        'ds = ConcatenateEmployeeLastFirstName(ds)
        ds = ConcatenateTimeOnly(ds)

        'ds = ConcatenateCourseCodeAndDescrip(ds)
        'ds = ConcatenateCourseCodeAndDescrip(ds, "CourseDT")

        Return ds
    End Function
    Public Function InitialLoadPage(ByVal CampusId As String, ByVal prgVerId As String) As DataSet
        Dim ds As New DataSet
        Dim da As OleDbDataAdapter
        Dim olddt As DataTable
        Dim newdt As DataTable
        Dim db As New ClassSectionPeriodsDB

        ds = db.GetDropDownList(CampusId, prgVerId)

        '     ds = FormatDateField(ds)
        'Add the concatenated FullName column to the datatable before sending it to the web page
        'ds = ConcatenateEmployeeLastFirstName(ds)
        ds = ConcatenateTimeOnly(ds)

        'ds = ConcatenateCourseCodeAndDescrip(ds)
        'ds = ConcatenateCourseCodeAndDescrip(ds, "CourseDT")

        Return ds
    End Function
    Public Function ConcatenateEmployeeLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        tbl = ds.Tables("InstructorDT")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("EmpId")}
        End With
        If ds.Tables("InstructorDT").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("FullName", GetType(String))
            For Each row In tbl.Rows
                row("FullName") = row("LastName") & ", " & row("FirstName")
            Next
        End If
        Return ds
    End Function
    Public Function ConcatenateTimeOnly(ByVal ds As DataSet) As DataSet

        Dim tbl As New DataTable
        Dim row As DataRow
        Dim ListDate As DateTime
        Dim a As DateTime
        tbl = ds.Tables("TimeIntervalList")
        If ds.Tables("TimeIntervalList").Rows.Count > 0 Then
            'Add a new column called TimeOnly to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("TimeOnly", GetType(String))
            For Each row In tbl.Rows
                ListDate = row("TimeIntervalDescrip")
                row("TimeOnly") = ListDate.ToShortTimeString
                a = ListDate.ToLongTimeString()
            Next
        End If
        Return ds
    End Function
    Public Function ConcatenateCourseCodeAndDescrip(ByVal ds As DataSet, ByVal tableName As String) As DataSet
        Dim dt As DataTable = ds.Tables(tableName)

        With dt
            '.PrimaryKey = New DataColumn() {.Columns("ReqId")}

            If .Rows.Count > 0 Then
                'Add a new column called CodeAndDescrip to the datatable 
                Dim col As DataColumn = .Columns.Add("CodeAndDescrip", Type.GetType("System.String"))

                '   column contains course code and description in this format: "(Code) Descrip"
                col.AllowDBNull = True
                col.Expression = "'(' + Code + ') ' + Descrip"
            End If
        End With

        Return ds
    End Function
    Public Function FormatDateField(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow
        Dim time As DateTime
        tbl = ds.Tables("TimeIntervalList")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("TimeIntervalID")}
        End With
        If ds.Tables("TimeIntervalList").Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            Dim col As DataColumn = tbl.Columns.Add("Time", GetType(String))

            For Each row In tbl.Rows
                time = row("TimeIntervalDescrip")
                time = Convert.ToString(time.ToString("t"))
                row("Time") = time
            Next
        End If
        Return ds
    End Function
    Public Function GetCampRooms(ByVal CampusId As String) As DataSet

        '   Instantiate DAL component
        Dim db As New ClassSectionPeriodsDB

        '   get the dataset with all degrees
        Return db.GetCampusRooms(CampusId)

    End Function
    Public Function GetDaysInPeriod(ByVal PeriodId As String) As DataSet
        Dim db As New ClassSectionPeriodsDB

        Return db.GetDaysInPeriod(PeriodId)
    End Function

    Public Function GetMaxCapOnRoom(ByVal BldgId As String)
        With New ClassSectionPeriodsDB
            Return .GetMaxCapOnRoom(BldgId)
        End With
    End Function
    Public Function GetStartEndDates(ByVal TermId As String) As String

        '   Instantiate DAL component
        Dim db As New ClassSectionPeriodsDB
        Dim str As String
        '   get the dataset with all degrees
        str = db.GetStartEndDates(TermId)
        Return str
    End Function
    Public Function PopulateDataList(ByVal Term As String, ByVal Course As String, ByVal Instructor As String, ByVal Shift As String, ByVal CampusId As String) As DataSet
        Dim db As New ClassSectionPeriodsDB
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter

        ds = db.PopulateClsSectDataList(Term, Course, Instructor, Shift, CampusId)

        'ds = ConcatenateStudentLastFirstName(ds)

        ds = ConcatenateCourseCodeAndDescrip(ds, "ClassSection")

        Return ds
    End Function
    Public Function PopulateClsSectDataList_SP(ByVal Term As String, ByVal Course As String, ByVal Instructor As String, ByVal Shift As String,
                                               ByVal CampusId As String, Optional ByVal StartDate As Nullable(Of DateTime) = Nothing, _
                                               Optional ByVal EndDate As Nullable(Of DateTime) = Nothing) As DataTable
        Dim db As New ClassSectionPeriodsDB

        Return db.PopulateClsSectDataList_SP(Term, Course, Instructor, Shift, CampusId, StartDate, EndDate)






    End Function

    Public Function GetTermModuleSummary(ByVal prgVerId As String, ByVal status As String, ByVal campusId As String) As DataSet
        Dim db As New ClassSectionPeriodsDB

        If prgVerId = "" Then
            Return db.GetTermModuleSummary(status, campusId)
        Else
            Return db.GetTermModuleSummary(prgVerId, status, campusId)
        End If

    End Function

    Public Function AddTermModule(ByVal termModuleInfo As TermModuleInfo, ByVal user As String) As ClsSectionResultInfo
        Dim db As New ClassSectionPeriodsDB
        Dim db2 As New TermsDB
        Dim cInfo As New ClsSectionResultInfo

        db2.AddTermInfo(termModuleInfo.TermInfo, user)

        cInfo = db.AddClassSection(termModuleInfo.ClsSectionInfo, user)

        Return cInfo
    End Function

    Public Function UpdateTermModule(ByVal termModuleInfo As TermModuleInfo, ByVal user As String) As ClsSectionResultInfo
        Dim db As New ClassSectionPeriodsDB
        Dim db2 As New TermsDB
        Dim cInfo As New ClsSectionResultInfo

        db2.UpdateTermInfo(termModuleInfo.TermInfo, user)

        cInfo = db.UpdateClassSection(termModuleInfo.ClsSectionInfo, user)

        Return cInfo
    End Function

    Public Function AddClassSection(ByVal ClassSection As ClassSectionInfo, ByVal user As String) As ClsSectionResultInfo
        Dim db As New ClassSectionPeriodsDB

        Return db.AddClassSection(ClassSection, user)

    End Function
    Public Function UpdateClassSection(ByVal ClassSection As ClassSectionInfo, ByVal user As String) As ClsSectionResultInfo
        Dim db As New ClassSectionPeriodsDB
        Dim msg As String
        Return db.UpdateClassSection(ClassSection, user)
    End Function
    Public Function DeleteClassSection(ByVal ClsSectId As Guid, ByVal modDate As DateTime) As String
        Dim db As New ClassSectionPeriodsDB
        Return db.DeleteClassSection(ClsSectId, modDate)
    End Function
    Public Function DeleteClassSection(ByVal ClsSectId As Guid, ByVal modDate As DateTime, ByVal termId As String) As String
        Dim db As New ClassSectionPeriodsDB
        Return db.DeleteClassSection(ClsSectId, modDate, termId)
    End Function
    Public Function AddOverridenConflicts(ByVal clsSectionId As String, ByVal clsSectionList As ArrayList, ByVal arrDeleteList As ArrayList, ByVal user As String) As String
        Return (New ClassSectionPeriodsDB).AddOverridenConflicts(clsSectionId, clsSectionList, arrDeleteList, user)
    End Function

    Public Sub DeleteTerm(ByVal termId As String)
        Dim db As New ClassSectionPeriodsDB

        db.DeleteTerm(termId)

    End Sub

    Public Function AddClsSectMeeting(ByVal ClsSectMtgArrList As ArrayList, ByVal allowConflicts As Boolean, ByVal clsSectionId As String, ByVal user As String) As ArrayList
        Dim db As New ClassSectionPeriodsDB
        Dim ClsSectMeeting As ClsSectMeetingInfo
        Dim ErrMsgObj As New ErrMsgInfo
        Dim ArrList As New ArrayList
        Dim count As Integer = 0
        Dim validation As New DALExceptions
        Dim msg As String
        Dim dbmsg As String
        Dim resinfo As ClsSectMtgIdObjInfo


        For Each ClsSectMeeting In ClsSectMtgArrList
            count = count + 1
            'if object is not empty
            'then add each single record
            If (IsObjectNotEmpty(ClsSectMeeting)) Then
                ''Added by Saraswathi lakshmanan to check for the collisions.
                ''Added on August 14 2009
                ''To fix mantis case 14484
                'Added by BN to fix the problem where there was no instructor selected
                'but was still getting an instructor collision message.
                If (ClsSectMeeting.InstructorId = "") Then
                    ClsSectMeeting.InstructorId = Guid.Empty.ToString
                End If

                'Check for conflicts
                'commented by Theresa G on 12/21/09 since Galen is having conflict with class section in a different term
                'If Not allowConflicts Then
                '    msg = validation.TimeCollisionsforClsSectionwithPeriods(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.PeriodId.ToString, ClsSectMeeting.AltPeriodId.ToString, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)
                'Else
                '    msg = validation.TimeCollisionsByClsSectionIdforClassSecWPeriods(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.PeriodId.ToString, ClsSectMeeting.AltPeriodId.ToString, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, clsSectionId)
                'End If

                If msg <> "" Then ' ERROR MSG
                    'Return msg
                    ErrMsgObj.ErrMsg = msg
                    ArrList.Add(ErrMsgObj)
                Else

                    'Generate a new guid for the insert
                    ClsSectMeeting.ClsSectMtgId = System.Guid.NewGuid
                    'Add the single record(one ClsSectMeeting object) to the database\
                    If ClsSectMeeting.CmdType <> 3 Then
                        resinfo = db.AddSingleClsSectMeeting(ClsSectMeeting, user)



                        'Add this new guid to an arraylist, so it can be used later on
                        'for update purposes.
                        resinfo.count = count
                        resinfo.txtboxname = "txtClsSectMeetingId"

                        ArrList.Add(resinfo)
                    End If
                End If

            End If

        Next
        Return ArrList

    End Function
    Public Sub CopyClsSectMeeting(ByVal allowConflicts As Boolean, ByVal clsSectionId As String, ByVal user As String)
        Dim db As New ClassSectionPeriodsDB
        Dim ClsSectMeeting As ClsSectMeetingInfo
        Dim ErrMsgObj As New ErrMsgInfo
        Dim ArrList As New ArrayList
        Dim count As Integer = 0
        Dim validation As New DALExceptions
        Dim msg As String
        Dim dbmsg As String
        Dim resinfo As ClsSectMtgIdObjInfo
        Dim ClsSectMtgArrList As ArrayList
        ClsSectMtgArrList = GetClsSectMtgsWithPeriods(XmlConvert.ToGuid(clsSectionId))
        For Each ClsSectMeeting In ClsSectMtgArrList
            count = count + 1
            'if object is not empty
            'then add each single record
            If (IsObjectNotEmpty(ClsSectMeeting)) Then


                'Generate a new guid for the insert
                ClsSectMeeting.ClsSectMtgId = System.Guid.NewGuid
                'Add the single record(one ClsSectMeeting object) to the database
                resinfo = db.AddSingleClsSectMeeting(ClsSectMeeting, user)

                'Add this new guid to an arraylist, so it can be used later on
                'for update purposes.
                ' resinfo.count = count
                'resinfo.txtboxname = "txtClsSectMeetingId"

                'ArrList.Add(resinfo)


            End If

        Next


    End Sub
    Public Function UpdateClsSectMeeting(ByVal ClsSectMtgArrList As ArrayList, ByVal allowConflicts As Boolean, ByVal clsSectionId As String, ByVal user As String) As ArrayList
        Dim db As New ClassSectionPeriodsDB
        Dim ClsSectMeeting As ClsSectMeetingInfo
        Dim count As Integer = 0
        Dim ClsSectMtgIdArrLst As New ArrayList
        Dim msg As String
        Dim validation As New DALExceptions
        Dim ErrMsgObj As New ErrMsgInfo
        Dim ArrList As New ArrayList
        Dim dbmsg As String
        Dim obj As New ClsSectMtgIdObjInfo

        'For Each ClsSectMeeting In ClsSectMtgArrList
        '    'Increment count 
        '    count = count + 1

        '    'if object is not empty
        '    'then add each single record
        '    If Left(ClsSectMeeting.ClsSectMtgId.ToString, 6) <> "000000" Then
        '        If Left(ClsSectMeeting.PeriodId.ToString, 6) = "000000" Then
        '            'Delete the meeting
        '            db.DeleteOneClsSectMeeting(ClsSectMeeting.ClsSectMtgId)
        '        Else

        '            'If a clssection meeting id already exists, this is an update
        '            'Add the single record(one ClsSectMeeting object) to the database
        '            obj = db.UpdateSingleClsSectMtg(ClsSectMeeting, user)
        '            obj.ClsSectMtgId = ClsSectMeeting.ClsSectMtgId
        '            obj.count = count
        '            obj.txtboxname = "txtClsSectMeetingId"

        '            ArrList.Add(obj)
        '        End If

        '    Else 'Fresh Insert
        '        If (IsObjectNotEmpty(ClsSectMeeting)) Then
        '            'Generate a new guid for the insert
        '            ClsSectMeeting.ClsSectMtgId = System.Guid.NewGuid
        '            obj = db.AddSingleClsSectMeeting(ClsSectMeeting, user)

        '            'Add this new guid to an arraylist, so it can be used later on
        '            'for update purposes.
        '            obj.ClsSectMtgId = ClsSectMeeting.ClsSectMtgId
        '            obj.count = count
        '            obj.txtboxname = "txtClsSectMeetingId"

        '            ArrList.Add(obj)

        '        End If

        '    End If

        'Next
        'Return ArrList



        For Each ClsSectMeeting In ClsSectMtgArrList
            'Increment count 
            count = count + 1

            'if object is not empty
            'then add each single record
            If Left(ClsSectMeeting.ClsSectMtgId.ToString, 6) <> "000000" Then
                If Left(ClsSectMeeting.PeriodId.ToString, 6) = "000000" Then
                    'Delete the meeting
                    db.DeleteOneClsSectMeeting(ClsSectMeeting.ClsSectMtgId)
                Else

                    'Added by BN to fix the problem where there was no instructor selected
                    'but was still getting an instructor collision message.
                    If (ClsSectMeeting.InstructorId = "") Then
                        ClsSectMeeting.InstructorId = Guid.Empty.ToString
                    End If


                    'Check for conflicts
                    If Not allowConflicts Then
                        'msg = validation.TimeCollisionsforClsSectionwithPeriods(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.Day.ToString, ClsSectMeeting.STimeDescrip, ClsSectMeeting.ETimeDescrip, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)
                        msg = validation.TimeCollisionsforClsSectionwithPeriods(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.PeriodId.ToString, ClsSectMeeting.AltPeriodId.ToString, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)
                    Else
                        msg = validation.TimeCollisionsByClsSectionIdforClassSecWPeriods(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.PeriodId.ToString, ClsSectMeeting.AltPeriodId.ToString, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, clsSectionId)
                    End If


                    If msg <> "" Then ' ERROR MSG
                        'Return msg
                        ErrMsgObj.ErrMsg = msg
                        ArrList.Add(ErrMsgObj)
                        Return ArrList
                    Else

                        'If a clssection meeting id already exists, this is an update
                        'Add the single record(one ClsSectMeeting object) to the database
                        obj = db.UpdateSingleClsSectMtg(ClsSectMeeting, user)
                        obj.ClsSectMtgId = ClsSectMeeting.ClsSectMtgId
                        obj.count = count
                        obj.txtboxname = "txtClsSectMeetingId"

                        ArrList.Add(obj)
                    End If

                End If


            Else 'Fresh Insert
                If (IsObjectNotEmpty(ClsSectMeeting)) Then


                    'Added by BN to fix the problem where there was no instructor selected
                    'but was still getting an instructor collision message.
                    If (ClsSectMeeting.InstructorId = "") Then
                        ClsSectMeeting.InstructorId = Guid.Empty.ToString
                    End If


                    If Not allowConflicts Then
                        'msg = validation.TimeCollisionsforClsSectionwithPeriods(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.Day.ToString, ClsSectMeeting.STimeDescrip, ClsSectMeeting.ETimeDescrip, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)
                        msg = validation.TimeCollisionsforClsSectionwithPeriods(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.PeriodId.ToString, ClsSectMeeting.AltPeriodId.ToString, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)
                    Else
                        msg = validation.TimeCollisionsByClsSectionIdforClassSecWPeriods(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.PeriodId.ToString, ClsSectMeeting.AltPeriodId.ToString, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, clsSectionId)
                    End If


                    If msg <> "" Then ' ERROR MSG
                        'Return msg
                        ErrMsgObj.ErrMsg = msg
                        ArrList.Add(ErrMsgObj)

                    Else

                        'Generate a new guid for the insert
                        ClsSectMeeting.ClsSectMtgId = System.Guid.NewGuid
                        obj = db.AddSingleClsSectMeeting(ClsSectMeeting, user)

                        'Add this new guid to an arraylist, so it can be used later on
                        'for update purposes.
                        obj.ClsSectMtgId = ClsSectMeeting.ClsSectMtgId
                        obj.count = count
                        obj.txtboxname = "txtClsSectMeetingId"

                        ArrList.Add(obj)
                    End If
                End If

            End If

        Next
        Return ArrList
    End Function
    Public Function UpdateClsSectMeeting_SP(ByVal ClsSectMtgArrList As ArrayList, ByVal allowConflicts As Boolean, ByVal clsSectionId As String, ByVal user As String) As ArrayList

        Dim db As New ClassSectionPeriodsDB
        Dim ClsSectMeeting As ClsSectMeetingInfo
        Dim count As Integer = 0
        Dim ClsSectMtgIdArrLst As New ArrayList
        Dim msg As String
        Dim validation As New DALExceptions
        Dim ErrMsgObj As New ErrMsgInfo
        Dim ArrList As New ArrayList
        Dim dbmsg As String
        Dim obj As New ClsSectMtgIdObjInfo
        For Each ClsSectMeeting In ClsSectMtgArrList
            'Increment count 
            count = count + 1

            'if object is not empty
            'then add each single record
            If ClsSectMeeting.CmdType = 3 Then

                'Delete the meeting
                db.DeleteOneClsSectMeeting(ClsSectMeeting.ClsSectMtgId)

            ElseIf ClsSectMeeting.CmdType = 2 Then

                'Added by BN to fix the problem where there was no instructor selected
                'but was still getting an instructor collision message.
                If (ClsSectMeeting.InstructorId = "") Then
                    ClsSectMeeting.InstructorId = Guid.Empty.ToString
                End If


                'Check for conflicts
                'commented by Theresa G on 12/21/09 since Galen is having conflict with class section in a different term
                'If Not allowConflicts Then
                '    'msg = validation.TimeCollisionsforClsSectionwithPeriods(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.Day.ToString, ClsSectMeeting.STimeDescrip, ClsSectMeeting.ETimeDescrip, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)
                '    msg = validation.TimeCollisionsforClsSectionwithPeriods(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.PeriodId.ToString, ClsSectMeeting.AltPeriodId.ToString, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)
                'Else
                '    msg = validation.TimeCollisionsByClsSectionIdforClassSecWPeriods(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.PeriodId.ToString, ClsSectMeeting.AltPeriodId.ToString, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, clsSectionId)
                'End If


                If msg <> "" Then ' ERROR MSG
                    'Return msg
                    ErrMsgObj.ErrMsg = msg
                    ArrList.Add(ErrMsgObj)
                    Return ArrList
                Else

                    'If a clssection meeting id already exists, this is an update
                    'Add the single record(one ClsSectMeeting object) to the database
                    obj = db.UpdateSingleClsSectMtg(ClsSectMeeting, user)
                    obj.ClsSectMtgId = ClsSectMeeting.ClsSectMtgId
                    obj.count = count
                    obj.txtboxname = "txtClsSectMeetingId"

                    ArrList.Add(obj)
                End If




            ElseIf ClsSectMeeting.CmdType = 1 Then 'Fresh Insert
                If (IsObjectNotEmpty(ClsSectMeeting)) Then


                    'Added by BN to fix the problem where there was no instructor selected
                    'but was still getting an instructor collision message.
                    If (ClsSectMeeting.InstructorId = "") Then
                        ClsSectMeeting.InstructorId = Guid.Empty.ToString
                    End If

                    'commented by Theresa G on 12/21/09 since Galen is having conflict with class section in a different term
                    'If Not allowConflicts Then
                    '    'msg = validation.TimeCollisionsforClsSectionwithPeriods(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.Day.ToString, ClsSectMeeting.STimeDescrip, ClsSectMeeting.ETimeDescrip, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)
                    '    msg = validation.TimeCollisionsforClsSectionwithPeriods(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.PeriodId.ToString, ClsSectMeeting.AltPeriodId.ToString, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, ClsSectMeeting.ClsSectMtgId.ToString)
                    'Else
                    '    msg = validation.TimeCollisionsByClsSectionIdforClassSecWPeriods(ClsSectMeeting.InstructorId, ClsSectMeeting.Room.ToString, ClsSectMeeting.PeriodId.ToString, ClsSectMeeting.AltPeriodId.ToString, ClsSectMeeting.StartDate, ClsSectMeeting.EndDate, clsSectionId)
                    'End If


                    If msg <> "" Then ' ERROR MSG
                        'Return msg
                        ErrMsgObj.ErrMsg = msg
                        ArrList.Add(ErrMsgObj)

                    Else

                        'Generate a new guid for the insert
                        'ClsSectMeeting.ClsSectMtgId = System.Guid.NewGuid
                        obj = db.AddSingleClsSectMeeting(ClsSectMeeting, user)

                        'Add this new guid to an arraylist, so it can be used later on
                        'for update purposes.
                        obj.ClsSectMtgId = ClsSectMeeting.ClsSectMtgId
                        obj.count = count
                        obj.txtboxname = "txtClsSectMeetingId"

                        ArrList.Add(obj)
                    End If
                End If
            End If


        Next
        Return ArrList


    End Function
    Public Sub DeleteClsSectMeeting(ByVal ClsSectId As Guid)
        Dim db As New ClassSectionPeriodsDB
        db.DeleteClsSectMeeting(ClsSectId)
    End Sub

    Private Function IsObjectNotEmpty(ByVal ClsSectMeeting As ClsSectMeetingInfo) As Boolean

        Dim obj As Boolean = False

        If Left(ClsSectMeeting.PeriodId.ToString, 6) <> "000000" Then
            obj = True
        End If
        If Left(ClsSectMeeting.Room.ToString, 6) <> "000000" Then
            obj = True
        End If
        If (ClsSectMeeting.StartDate) <> Date.MinValue Then
            obj = True
        End If '
        If (ClsSectMeeting.EndDate) <> Date.MaxValue Then
            obj = True
        End If '
        Return obj
    End Function

    Public Function GetOneClassSection(ByVal ClsSectId As Guid) As ClassSectionInfo
        Dim db As New ClassSectionPeriodsDB
        Dim SingleClsSect As ClassSectionInfo
        Dim ds As DataSet
        Dim ds1 As DataSet
        ds = db.GetSingleClsSect(ClsSectId)
        SingleClsSect = ConvertSingleClsSectToObject(ds)
        Return SingleClsSect
    End Function

    Public Function GetClassSection(ByVal ClsSectId As Guid) As DataSet

        Return (New ClassSectionPeriodsDB).GetClassSection(ClsSectId)

    End Function

    Public Function GetOverridenConflicts(ByVal ClsSectId As String) As DataTable

        Return (New ClassSectionPeriodsDB).GetOverridenConflicts(ClsSectId)

    End Function
    Public Function DeleteOverridenConflicts(ByVal ClsSectId As String)
        Dim obj As New ClassSectionPeriodsDB
        obj.DeleteOverridenConflicts(ClsSectId)

    End Function
    Public Function DeleteOverridenConflicts(ByVal arrDeleteList As ArrayList) As String

        Dim obj As New ClassSectionPeriodsDB
        Return obj.DeleteOverridenConflicts(arrDeleteList)

    End Function
    Public Function GetTermModuleDetails(ByVal termId As String) As TermModuleInfo
        Dim db As New ClassSectionPeriodsDB
        Dim ds1 As New DataSet
        Dim ds2 As New DataSet
        Dim dr1, dr2 As DataRow
        Dim termObject As New TermInfo
        Dim ClassSectionObject As New ClassSectionInfo

        ds1 = db.GetSingleTerm(termId)
        ds2 = db.GetClsSectForTermModule(termId)

        dr1 = ds1.Tables(0).Rows(0)
        termObject.TermId = dr1("TermId").ToString
        termObject.StartDate = dr1("StartDate").ToShortDateString
        termObject.EndDate = dr1("EndDate").ToShortDateString
        termObject.StatusId = dr1("StatusId").ToString
        termObject.Description = dr1("TermDescrip").ToString
        termObject.Code = dr1("TermCode")
        termObject.IsModule = True

        dr2 = ds2.Tables(0).Rows(0)
        ClassSectionObject.ClsSectId = dr2("ClsSectionId")

        If dr2("InstructorId").ToString() <> "" Then
            ClassSectionObject.InstructorId = dr2("InstructorId")
        End If

        If dr2("ShiftId").ToString() <> "" Then
            ClassSectionObject.ShiftId = dr2("ShiftId")
        End If

        ClassSectionObject.CampusId = dr2("CampusId")
        ClassSectionObject.GrdScaleId = dr2("GrdScaleId")
        ClassSectionObject.Section = dr2("ClsSection")
        ClassSectionObject.StartDate = dr2("StartDate").ToShortDateString
        ClassSectionObject.EndDate = dr2("EndDate").ToShortDateString
        ClassSectionObject.MaxStud = dr2("MaxStud")
        ClassSectionObject.TermId = dr2("TermId")
        ClassSectionObject.CourseId = dr2("ReqId")
        ClassSectionObject.ModDate = dr2("ModDate")

        Return New TermModuleInfo(termObject, ClassSectionObject)

    End Function

    Public Function GetClsSectMtgs(ByVal ClsSectId As Guid) As ArrayList
        Dim db As New ClassSectionPeriodsDB
        Dim ClsSectArrayList As New ArrayList

        Dim ds As DataSet
        Dim ds1 As DataSet
        ds = db.GetClsSectMtgs(ClsSectId)
        ClsSectArrayList = ConvertClsSectMtgsToArrList(ds)
        Return ClsSectArrayList
    End Function

    Public Function GetClsSectMtgsWithPeriods(ByVal ClsSectId As Guid) As ArrayList
        Dim db As New ClassSectionPeriodsDB
        Dim ClsSectArrayList As New ArrayList

        Dim ds As DataSet
        Dim ds1 As DataSet
        ds = db.GetClsSectMtgsWithPeriods(ClsSectId)
        ClsSectArrayList = ConvertClsSectMtgsToArrList(ds)
        Return ClsSectArrayList
    End Function

    Public Function GetClsSectMtgsWithPeriods_SP(ByVal ClsSectId As Guid) As DataTable
        Dim db As New ClassSectionPeriodsDB
        Return db.GetClsSectMtgsWithPeriods_SP(ClsSectId)


    End Function


    Public Function ConvertSingleClsSectToObject(ByVal ds As DataSet) As ClassSectionInfo
        Dim ClsSectObject As New ClassSectionInfo
        Dim dt, dt2 As DataTable
        Dim row As DataRow
        dt = ds.Tables("SingleClsSect")
        dt2 = ds.Tables("OverridenConflicts")
        'If ds.Tables.Count > 1 Then
        '    'Error - it returned more than one record
        'Else
        For Each row In dt.Rows
            ClsSectObject.ClsSectId = row("ClsSectionId")
            If row("TermId").ToString <> "" Then
                ClsSectObject.TermId = row("TermId")
            End If
            If row("ReqId").ToString <> "" Then
                ClsSectObject.CourseId = row("ReqId")
            End If
            If row("InstructorId").ToString <> "" Then
                ClsSectObject.InstructorId = row("InstructorId")
            End If
            If row("InstructorId").ToString <> "" Then
                ClsSectObject.InstructorId2 = row("InstructorId").ToString
            End If
            If row("ShiftId").ToString <> "" Then
                ClsSectObject.ShiftId2 = row("ShiftId").ToString
            End If
            If row("CampusId").ToString <> "" Then
                ClsSectObject.CampusId = row("CampusId")
            End If
            If row("GrdScaleId").ToString <> "" Then
                ClsSectObject.GrdScaleId = row("GrdScaleId")
            End If
            ClsSectObject.Section = row("ClsSection")
            ClsSectObject.StartDate = row("StartDate")
            ClsSectObject.EndDate = row("EndDate")
            If row("StudentStartDate").ToString <> "" Then
                ClsSectObject.StudentStartDate = row("StudentStartDate")
            End If
            ClsSectObject.MaxStud = row("MaxStud")
            ClsSectObject.ModUser = row("ModUser")
            ClsSectObject.ModDate = row("ModDate")
            ClsSectObject.Term = row("Term")
            ClsSectObject.Course = row("Course")
            If row("Instructor").ToString <> "" Then
                ClsSectObject.Instructor = row("Instructor")
            End If
            If row("Shift").ToString <> "" Then
                ClsSectObject.Shift = row("Shift")
            End If
            ClsSectObject.Campus = row("Campus")
            ClsSectObject.GrdScale = row("GrdScale")
            If row("LeadGrpId").ToString <> "" Then
                ClsSectObject.LeadGrpId = row("LeadGrpId")
                ClsSectObject.LeadGrp = row("LeadGrp")
            End If

            If dt2.Rows.Count > 0 Then
                ClsSectObject.AllowOverrideConflicts = True
            End If
            If row("InstrGrdBkWgtId").ToString <> "" Then
                ClsSectObject.InstrGrdBkWgtId = row("InstrGrdBkWgtId")
            End If
            If row("InstrGrdBkWgtId").ToString <> "" Then
                ClsSectObject.InstrGrdBkWgtId = row("InstrGrdBkWgtId")
            End If

            ' 09/12/2011 Janet Robinson - Clock Hours Project Added new fields
            Try
                ClsSectObject.AllowEarlyIn = row("AllowEarlyIn")
            Catch ex As Exception
                ClsSectObject.AllowEarlyIn = False
            End Try
            Try
                ClsSectObject.AllowLateOut = row("AllowLateOut")
            Catch ex As Exception
                ClsSectObject.AllowLateOut = False
            End Try
            Try
                ClsSectObject.AllowExtraHours = row("AllowExtraHours")
            Catch ex As Exception
                ClsSectObject.AllowExtraHours = False
            End Try
            Try
                ClsSectObject.CheckTardyIn = row("CheckTardyIn")
            Catch ex As Exception
                ClsSectObject.CheckTardyIn = False
            End Try
            Try
                ClsSectObject.MaxInBeforeTardy = row("MaxInBeforeTardy")
            Catch ex As Exception

            End Try
            Try
                ClsSectObject.AssignTardyInTime = row("AssignTardyInTime")
            Catch ex As Exception

            End Try


        Next
        'End If
        Return ClsSectObject
    End Function

    Public Function ConvertClsSectMtgsToArrList(ByVal ds As DataSet) As ArrayList

        Dim count As Integer
        Dim strControl As String
        Dim ClsSectArrayList As New ArrayList
        Dim dt As DataTable
        Dim row As DataRow
        dt = ds.Tables("ClsSectMtgs")
        If ds.Tables.Count > 1 Then
            'Error - it returned more than one record
        Else
            'Loop thru the controls, find the appropriate dropdown(Day/Room/TimeInterval)
            'and set the object properties based on the ddl's that are set.
            For Each row In dt.Rows
                Dim ClsSectMtgObject As New ClsSectMeetingInfo
                If Not row("PeriodId") Is System.DBNull.Value Then ClsSectMtgObject.PeriodId = row("PeriodId")
                If Not row("PeriodDescrip") Is System.DBNull.Value Then ClsSectMtgObject.PeriodDescrip = row("PeriodDescrip")
                If Not row("AltPeriodId") Is System.DBNull.Value Then ClsSectMtgObject.AltPeriodId = row("AltPeriodId")
                If Not row("RoomDescrip") Is System.DBNull.Value Then ClsSectMtgObject.RoomDescrip = row("RoomDescrip")
                If Not row("PeriodDescrip") Is System.DBNull.Value Then ClsSectMtgObject.PeriodDescrip = row("PeriodDescrip")
                ClsSectMtgObject.Room = row("RoomId")
                If Not row("StartDate") Is System.DBNull.Value Then ClsSectMtgObject.StartDate = row("StartDate")
                If Not row("EndDate") Is System.DBNull.Value Then ClsSectMtgObject.EndDate = row("EndDate")
                ClsSectMtgObject.ClsSectMtgId = row("ClsSectMeetingId")
                ClsSectMtgObject.ModDate = row("ModDate")
                'Add the object to the array list
                ClsSectArrayList.Add(ClsSectMtgObject)
            Next
        End If

        Return ClsSectArrayList
    End Function

    Public Function SendErrMsgToUI(ByVal msg As String) As String

    End Function
    Public Function DoResultsExistFrClsSect(ByVal ClsSect As String) As Integer
        Dim db As New ClassSectionPeriodsDB

        Return db.DoesClsSectHaveGradesPosted(ClsSect)
    End Function

    Public Function ValidateClassSectionDates(ByVal startDate As Date, ByVal endDate As Date, ByVal termId As String, ByVal courseId As String) As String
        Dim termSDate As Date
        Dim termEDate As Date
        Dim tDB As New TermsDB
        Dim tInfo As New TermInfo
        Dim errMsg As String = ""
        Dim fac As New CoursesFacade

        'If the course passed in is an externship then we should not bother with the validation
        If fac.IsExternship(courseId) Then
            'No validation
        Else
            'Get the term start and end dates
            tInfo = tDB.GetTermInfo(termId)

            termSDate = tInfo.StartDate
            termEDate = tInfo.EndDate

            If startDate < termSDate Then
                errMsg &= "Class section start date must be on or after term start date " & termSDate.ToShortDateString & vbCr
            End If

            If endDate > termEDate Then
                errMsg &= "Class section end date must be on or before term end date " & termEDate.ToShortDateString & vbCr
            End If

        End If

        

        Return errMsg
    End Function

    Public Function GetCurrentAndFutureTerms() As DataSet

        '   Instantiate DAL component
        Dim GrdPostDB As New GrdPostingsDB

        '   get the dataset with all degrees
        Return GrdPostDB.GetCurrentAndFutureTerms
    End Function

    'Public Function ValidateMaxStudents(ByVal ClsSectId As String) As String
    '    Dim MaxRegistered As String
    '    Dim termEDate As Date
    '    Dim tDB As New TermsDB
    '    Dim tInfo As New TermInfo
    '    Dim errMsg As String = ""

    '    'Get the term start and end dates
    '    tInfo = tDB.GetTermInfo(termId)

    '    MaxRegistered = tInfo.StartDate
    '    termEDate = tInfo.EndDate

    '    If startDate < termSDate Then
    '        errMsg &= "Class section start date must be on or after term start date " & termSDate.ToShortDateString & vbCr
    '    End If

    '    If endDate > termEDate Then
    '        errMsg &= "Class section end date must be on or before term end date " & termEDate.ToShortDateString & vbCr
    '    End If


    '    Return errMsg
    'End Function
   

    Public Function GetAllPeriods_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable
        Dim db As New ClassSectionPeriodsDB
        Return db.GetAllPeriods_SP(showActiveOnly, campusId)

    End Function

    Public Function GetAllRooms_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable
        Dim db As New ClassSectionPeriodsDB
        Return db.GetAllRooms_SP(showActiveOnly, campusId)

    End Function

    Public Function GetInstructionTypes_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable
        Dim db As New ClassSectionPeriodsDB
        Return db.GetInstructionTypes_SP(showActiveOnly, campusId)

    End Function

    Public Function GetTimeClocks_SP(ByVal showActiveOnly As Boolean, ByVal campusId As String) As DataTable
        Dim db As New ClassSectionPeriodsDB
        Return db.GetTimeClocks_SP(showActiveOnly, campusId)

    End Function


End Class
