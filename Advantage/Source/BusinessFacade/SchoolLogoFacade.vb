
Imports FAME.Advantage.Common

Public Class SchoolLogoFacade
    Public Function ConnectionString() As String

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        Return myAdvAppSettings.AppSettings("ConnectionString")
    End Function
    Public Function InsertImage(ByVal images As Byte(), ByVal contentLength As Int64, ByVal imgFile As String, ByVal isOfficialUse As Boolean) As String
        Return (New SchoolLogoDB).InsertImage(images, contentLength, imgFile, isOfficialUse)
    End Function
    Public Function GetImage(Optional ByVal imgId As Integer = 0) As Byte()
        Return (New SchoolLogoDB).GetImage(imgId)
    End Function

Function InsertImage(imageStr As Byte(), contentLength As Int64, strImgFile As String, isOfficialUse As Boolean, logoCode As String) As String 
        'Get if the code of the image exists
        Dim service As SchoolLogoDB = New SchoolLogoDB
        Dim idImage As Int32 = service.GetImageIdByCode(logoCode)
        Dim isExists As Boolean = ( idImage >= 0)
        Dim result As String
    If (isExists) Then
       result =  (New SchoolLogoDB).UpdateImage(idImage, imageStr, contentLength, strImgFile, isOfficialUse, logoCode)
    Else 
        result =  (New SchoolLogoDB).InsertImage(imageStr, ContentLength, strImgFile, isOfficialUse, logoCode)
    End If
        Return result
 End Function 

End Class
