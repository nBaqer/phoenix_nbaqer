﻿Imports FAME.AdvantageV1.DataAccess.SuvaTable

Public Class SuvaTableFacade

    Public Shared Function GetMockItemsFromSuvaCatalogDB() As IList(Of SuvaCat)
        ' TODO: Implement this method
        'Add a 0 items
         Dim list As IList(Of SuvaCat) = new List(Of SuvaCat)
        Dim sel As SuvaCat = SuvaCat.Factory()
        sel.Description = "Select"
        sel.IdSuva = 0
        list.Add(sel)
        return list
    End Function

    ''' <summary>
    ''' Return the content of the arSuvaTable Catalog.
    ''' </summary>
    ''' <returns> Return a Iist of SuvaCat.</returns>
    ''' <remarks></remarks>
    Public Shared Function GetItemsFromSuvaCatalogDB() As IList(Of SuvaCat)

        ' Get info from database
        Dim db = New SuvaTableDB()
        Dim list As IList(Of SuvaCat) = db.GetAllFromSuvaCatalog()
        Return list
    End Function

End Class
