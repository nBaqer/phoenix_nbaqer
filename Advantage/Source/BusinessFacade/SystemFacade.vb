' ===============================================================================
' FAME.AdvantageV1.BusinessFacade
'
' SystemFacade.vb
'
' SystemFacade Service Interface. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class SystemFacade
    Public Function GetAllStudentRestrictionTypes(ByVal showActiveOnly As String) As DataSet

        '   Instantiate DAL component
        With New StudentRestrictionsDB

            '   get a dataset with all StudentRestrictions
            Return .GetAllStudentRestrictionTypes(showActiveOnly)

        End With

    End Function
    Public Function GetStudentRestrictionTypeInfo(ByVal StudentRestrictionId As String) As StudentRestrictionTypeInfo

        '   Instantiate DAL component
        With New StudentRestrictionsDB

            '   get the BankInfo
            Return .GetStudentRestrictionTypeInfo(StudentRestrictionId)

        End With

    End Function
    Public Function UpdateStudentRestrictionTypeInfo(ByVal StudentRestrictionTypeInfo As StudentRestrictionTypeInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New StudentRestrictionsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (StudentRestrictionTypeInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddStudentRestrictionTypeInfo(StudentRestrictionTypeInfo, user)
            Else
                '   return integer with update results
                Return .UpdateStudentRestrictionTypeInfo(StudentRestrictionTypeInfo, user)
            End If

        End With

    End Function
    Public Function DeleteStudentRestrictionTypeInfo(ByVal StudentRestrictionId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New StudentRestrictionsDB

            '   delete BankInfo ans return integer result
            Return .DeleteStudentRestrictionTypeInfo(StudentRestrictionId, modDate)

        End With

    End Function
    Public Function GetAllGrpStuRestrictions(ByVal showActiveOnly As Boolean) As DataSet

        '   Instantiate DAL component
        With New StudentRestrictionsDB

            '   get a dataset with all GrpStuRestrictions
            Return .GetAllGrpStuRestrictions(showActiveOnly)

        End With

    End Function
    'Public Function GetGrpStuRestrictionInfo(ByVal stuRestrictionId As String) As GrpStuRestrictionInfo

    '    '   Instantiate DAL component
    '    With New StudentRestrictionsDB

    '        '   get the 
    '        Return .GetGrpStuRestrictionInfo(stuRestrictionId)

    '    End With

    'End Function
    Public Function UpdateGrpStuRestrictionInfo(ByVal GrpStuRestrictionInfo As GrpStuRestrictionInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New StudentRestrictionsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (GrpStuRestrictionInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddGrpStuRestrictionInfo(GrpStuRestrictionInfo, user)
            Else
                '   return integer with update results
                Return .UpdateGrpStuRestrictionInfo(GrpStuRestrictionInfo, user)
            End If

        End With

    End Function
    Public Function DeleteGrpStuRestrictionInfo(ByVal GrpStuRestrictionId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New StudentRestrictionsDB

            '   delete BankInfo ans return integer result
            Return .DeleteGrpStuRestrictionInfo(GrpStuRestrictionId, modDate)

        End With

    End Function
    'Public Function GetAllSGroups(ByVal showActiveOnly As Boolean) As DataSet

    '    '   Instantiate DAL component
    '    With New SGroupsDB

    '        '   get a dataset with all StudentRestrictions
    '        Return .GetAllSGroups(showActiveOnly)

    '    End With

    'End Function
    Public Function GetAllRDFSiteSums(ByVal statusSelectIndex As Integer, ByVal moduleId As Integer) As DataSet

        '   Instantiate DAL component
        With New RDFSiteSumsDB

            '   get the dataset with all RDFSiteSums
            Return .GetAllRDFSiteSums(statusSelectIndex, moduleId)

        End With

    End Function
    Public Function GetRDFSiteSum(ByVal moduleId As Integer, ByVal campusId As String) As String

        '   Instantiate DAL component
        With New RDFSiteSumsDB

            '   get the dataset with all RDFSiteSums
            Return .GetRDFSiteSum(moduleId, campusId)

        End With

    End Function
    Public Function GetRDFSiteSumInfo(ByVal RDFSiteSumId As String) As RDFSiteSumInfo

        '   Instantiate DAL component
        With New RDFSiteSumsDB

            '   get the RDFSiteSumInfo
            Return .GetRDFSiteSumInfo(RDFSiteSumId)

        End With

    End Function
    Public Function UpdateRDFSiteSumInfo(ByVal RDFSiteSumInfo As RDFSiteSumInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New RDFSiteSumsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (RDFSiteSumInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddRDFSiteSumInfo(RDFSiteSumInfo, user)
            Else
                '   return integer with update results
                Return .UpdateRDFSiteSumInfo(RDFSiteSumInfo, user)
            End If

        End With

    End Function
    Public Function DeleteRDFSiteSumInfo(ByVal RDFSiteSumId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New RDFSiteSumsDB

            '   delete RDFSiteSumInfo ans return string result
            Return .DeleteRDFSiteSumInfo(RDFSiteSumId, modDate)

        End With

    End Function
    Public Function GetAllAnnouncements(ByVal statusSelectIndex As Integer, ByVal moduleName As String) As DataSet

        '   Instantiate DAL component
        With New AnnouncementsDB

            '   get the dataset with all Announcements
            Return .GetAllAnnouncements(statusSelectIndex, moduleName)

        End With

    End Function
    Public Function GetAnnouncementInfo(ByVal AnnouncementId As String) As AnnouncementInfo

        '   Instantiate DAL component
        With New AnnouncementsDB

            '   get the AnnouncementInfo
            Return .GetAnnouncementInfo(AnnouncementId)

        End With

    End Function
    Public Function UpdateAnnouncementInfo(ByVal AnnouncementInfo As AnnouncementInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New AnnouncementsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (AnnouncementInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddAnnouncementInfo(AnnouncementInfo, user)
            Else
                '   return integer with update results
                Return .UpdateAnnouncementInfo(AnnouncementInfo, user)
            End If

        End With

    End Function
    Public Function DeleteAnnouncementInfo(ByVal AnnouncementId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New AnnouncementsDB

            '   delete AnnouncementInfo ans return string result
            Return .DeleteAnnouncementInfo(AnnouncementId, modDate)

        End With

    End Function
    Public Function GetAllModules() As DataSet

        '   get a dataset with all StudentRestrictions
        Return (New ModulesDB).GetAllModules()

    End Function
End Class


