Imports System.data
Imports FAME.AdvantageV1.DataAccess
Public Class TermProgressFacade
    Public Function PopulateDataGridOnItemCmd(ByVal StuEnrollmentId As String) As DataTable

        Dim db As New TermProgressDB
        Dim ds As New DataSet
        Dim tbl1 As DataTable
        Dim tbl2 As DataTable
        Dim tbl3 As DataTable
        Dim tbl4 As DataTable
        Dim row As DataRow
        Dim grdDetail As DataRow
        Dim dr As DataRow
        Dim arr As DataRow
        Dim arr1 As DataRow
        Dim Req As String
        Dim Grade As String
        Dim BelongToPrgVer As Integer
        Dim StudentProgress As New StudentProgressInfo
        Dim TermCredsAttmptd As Integer
        Dim TermCredsEarned As Integer
        Dim Term As String
        Dim TotalCreds As Double
        Dim GrdWeight As Double
        Dim TotalCreds2 As Double
        Dim GrdWeight2 As Double

        ds = db.GetStudentProgress(StuEnrollmentId)

        tbl1 = ds.Tables("StdProgress")
        tbl2 = ds.Tables("StuResults")
        tbl3 = ds.Tables("CredsAttmptd")

        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("TermId")}
        End With

        With tbl2
            .PrimaryKey = New DataColumn() {.Columns("ReqId")}
        End With

        Dim col1 As DataColumn = tbl1.Columns.Add("CredsAttmptd", GetType(String))
        Dim col2 As DataColumn = tbl1.Columns.Add("CredsEarned", GetType(String))
        Dim col3 As DataColumn = tbl1.Columns.Add("CurrGPA", GetType(String))
        Dim col4 As DataColumn = tbl1.Columns.Add("CumGPA", GetType(String))

        'Dim col5 As DataColumn = tbl2.Columns.Add("IsInCredsAttmptd", GetType(Boolean))
        'Dim col6 As DataColumn = tbl2.Columns.Add("IsInCredsEarned", GetType(Boolean))
        'Dim col7 As DataColumn = tbl2.Columns.Add("IsInPrgVer", GetType(Boolean))
        'Dim col8 As DataColumn = tbl2.Columns.Add("IsInGPA", GetType(Boolean))
        'Dim col9 As DataColumn = tbl2.Columns.Add("GPA", GetType(Decimal))


        'If tbl2.Rows.Count > 0 Then
        '    'Add a new column called fullname to the datatable 
        '    For Each row In tbl2.Rows
        '        Req = row("ReqId").ToString
        '        Grade = row("Grade").ToString

        '        'Find out if this Requirement belongs to the ProgVersion we're dealing with.
        '        'BelongToPrgVer = DoesReqBelongToPrgVer(ProgVerId, Req)

        '        'If BelongToPrgVer = 1 Then
        '        '    row("IsInPrgVer") = True
        '        '    tbl4 = GetGrdSysDetails(row("Grade").ToString, ProgVerId)
        '        '    Dim row2 As DataRow = tbl4.Rows.Find(Grade)
        '        '    row("IsInCredsAttmptd") = row2("IsCreditsAttempted")
        '        '    row("IsInCredsEarned") = row2("IsCreditsEarned")
        '        '    row("IsInGPA") = row2("IsInGPA")
        '        '    row("GPA") = row2("GPA")

        '        'End If

        '        tbl4 = GetGrdSysDetails(row("Grade").ToString, ProgVerId)
        '        tbl4 = GetGrdSysDetails(row("GrdSysDetailId"))
        '        Dim row2 As DataRow = tbl4.Rows.Find(Grade)
        '        row("IsInCredsAttmptd") = row2("IsCreditsAttempted")
        '        row("IsInCredsEarned") = row2("IsCreditsEarned")
        '        row("IsInGPA") = row2("IsInGPA")
        '        row("GPA") = row2("GPA")


        '    Next
        'End If

        'Fill up the Student Progress table with the processed data.
        If tbl1.Rows.Count > 0 Then
            'Add a new column called fullname to the datatable 
            For Each dr In tbl1.Rows
                Term = dr("TermId").ToString
                Dim TermSDate As DateTime = dr("StartDate")

                'Get all the Results belonging to this Term in the Student Results table.
                Dim aRows As DataRow()
                Dim aRows1 As DataRow()

                TermCredsAttmptd = 0
                TermCredsEarned = 0
                TotalCreds = 0
                GrdWeight = 0
                TotalCreds2 = 0
                GrdWeight2 = 0

                'Find this Term in the results table and retrieve corresponding info.
                aRows = tbl2.Select("TermId ='" & dr("TermId").ToString & "'")

                For Each arr In aRows
                    If (arr("IsCreditsAttempted")) Then
                        TermCredsAttmptd += arr("Credits")
                    End If

                    If (arr("IsCreditsEarned")) Then
                        TermCredsEarned += arr("Credits")
                    End If

                    If (arr("IsInGPA")) Then
                        TotalCreds += arr("Credits")
                        GrdWeight += arr("GPA") * arr("Credits")
                    End If
                Next

                dr("CredsAttmptd") = TermCredsAttmptd
                dr("CredsEarned") = TermCredsEarned

                If GrdWeight > 0 And TotalCreds > 0 Then
                    dr("CurrGPA") = System.Math.Round((GrdWeight / TotalCreds), 2).ToString("##.00")
                End If

                'To calculate the Cumulative GPA
                Dim a As String = "StartDate <= " & "#" & TermSDate.ToShortDateString() & "#"
                aRows1 = tbl2.Select(a)


                For Each arr1 In aRows1
                    Dim reqstr As String = arr1("ReqId").ToString
                    'Dim isingpab As Boolean = arr1("IsInGPA")


                    If Not arr1.IsNull("IsInGPA") Then 'And (arr1("IsInGPA") = True) Then
                        TotalCreds2 += arr1("Credits")
                        If Not arr1.IsNull("GPA") Then
                            GrdWeight2 += arr1("GPA") * arr1("Credits")
                        End If
                    End If
                Next
                If GrdWeight2 > 0 And TotalCreds2 > 0 Then
                    dr("CumGPA") = System.Math.Round((GrdWeight2 / TotalCreds2), 2).ToString("##.00")
                End If
            Next
        End If



        Return tbl1

    End Function

    Public Function GetGrdSysDetails(ByVal GrdSysDetailId) As DataTable

        Dim db As New TermProgressDB
        Dim ds As New DataSet
        Dim Result As Boolean
        Dim tbl1 As New DataTable

        ds = db.GetGrdSysDetails(GrdSysDetailId)
        tbl1 = ds.Tables("GrdSysDetails")


        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("Grade")}
        End With

        Return tbl1
    End Function

    Public Function GetTermProgressInfo(ByVal stuEnrollmentId As String) As DataTable
        Dim db As New TermProgressDB
        Dim dtResults As DataTable
        Dim dtSummary As New DataTable
        Dim tbl1 As DataTable
        Dim tbl2 As DataTable
        Dim tbl3 As DataTable
        Dim tbl4 As DataTable
        Dim row As DataRow
        Dim grdDetail As DataRow
        Dim dr As DataRow
        Dim dr1 As DataRow
        Dim arr As DataRow
        Dim arr1 As DataRow
        Dim Req As String
        Dim Grade As String
        Dim BelongToPrgVer As Integer
        Dim StudentProgress As New StudentProgressInfo
        Dim TermCredsAttmptd As Double
        Dim TermCredsEarned As Double
        Dim Term As String
        Dim TotalCreds As Double
        Dim GrdWeight As Double
        Dim TotalCreds2 As Double
        Dim GrdWeight2 As Double

        dtResults = db.GetStudentResultsForClassesInEnrollment(stuEnrollmentId)

        'We will return dtSummary to the UI
        Dim col0 As DataColumn = dtSummary.Columns.Add("TermId", GetType(String))
        Dim col1 As DataColumn = dtSummary.Columns.Add("CredsAttmptd", GetType(String))
        Dim col2 As DataColumn = dtSummary.Columns.Add("CredsEarned", GetType(String))
        Dim col3 As DataColumn = dtSummary.Columns.Add("CurrGPA", GetType(String))
        Dim col4 As DataColumn = dtSummary.Columns.Add("CumGPA", GetType(String))
        Dim col5 As DataColumn = dtSummary.Columns.Add("Term", GetType(String))
        Dim col6 As DataColumn = dtSummary.Columns.Add("StartDate", GetType(String))

        'With dtSummary
        '.PrimaryKey = New DataColumn() {.Columns("TermId")}
        'End With

        'If dtResults is empty we can simply return dtSummary at this point.
        'Otherwise we need to populate it with the distinct terms that are in dtResults
        'and then loop through it and fill out the different fields.
        If dtResults.Rows.Count = 0 Then
            Return dtSummary
        Else
            'Get distinct terms that are in the dtResults datatable. Loop through dtResults
            'and add the TermId from it to dtSummary if it does not exist there already.
            For Each dr1 In dtResults.Rows
                If dtSummary.Rows.Count = 0 Or Not ValExistsInDT(dtSummary, dr1("TermId").ToString()) Then
                    Dim dr2 As DataRow = dtSummary.NewRow
                    dr2("TermId") = dr1("TermId").ToString
                    dr2("Term") = dr1("TermDescrip")
                    dr2("StartDate") = dr1("StartDate")
                    dtSummary.Rows.Add(dr2)
                End If
            Next

            'Loop through each row in dtSummary and get the computed values for the fields
            'based on the data in dtResults
            For Each dr In dtSummary.Rows
                Term = dr("TermId").ToString
                Dim TermSDate As DateTime = dr("StartDate")

                'Get all the Results belonging to this Term in the Student Results table.
                Dim aRows As DataRow()
                Dim aRows1 As DataRow()

                TermCredsAttmptd = 0
                TermCredsEarned = 0
                TotalCreds = 0
                GrdWeight = 0
                TotalCreds2 = 0
                GrdWeight2 = 0

                'Find this Term in the results table and retrieve corresponding info.
                aRows = dtResults.Select("TermId ='" & dr("TermId").ToString & "'")

                For Each arr In aRows
                    If (arr("IsCreditsAttempted")) Then
                        TermCredsAttmptd += arr("Credits")
                    End If

                    If (arr("IsCreditsEarned")) Then
                        TermCredsEarned += arr("Credits")
                    End If

                    If (arr("IsInGPA")) Then
                        TotalCreds += arr("Credits")
                        GrdWeight += arr("GPA") * arr("Credits")
                    End If
                Next

                dr("CredsAttmptd") = System.Math.Round(TermCredsAttmptd, 2).ToString("##.00")
                dr("CredsEarned") = System.Math.Round(TermCredsEarned, 2).ToString("##.00")

                If GrdWeight > 0 And TotalCreds > 0 Then
                    dr("CurrGPA") = System.Math.Round((GrdWeight / TotalCreds), 2).ToString("##.00")
                End If

                'To calculate the Cumulative GPA
                Dim a As String = "StartDate <= " & "#" & TermSDate.ToShortDateString() & "#"
                aRows1 = dtResults.Select(a)


                For Each arr1 In aRows1
                    Dim reqstr As String = arr1("ReqId").ToString

                    If Not arr1.IsNull("IsInGPA") Then
                        If arr1("IsInGPA") = 1 Or arr1("IsInGPA") = True Then
                            TotalCreds2 += arr1("Credits")
                            If Not arr1.IsNull("GPA") Then
                                GrdWeight2 += arr1("GPA") * arr1("Credits")
                            End If
                        End If
                    End If
                Next
                If GrdWeight2 > 0 And TotalCreds2 > 0 Then
                    dr("CumGPA") = System.Math.Round((GrdWeight2 / TotalCreds2), 2).ToString("##.00")
                End If
            Next

            Return dtSummary
        End If
    End Function
    Private Function ValExistsInDT(ByVal dt As DataTable, ByVal val As String) As Boolean
        Dim dr As DataRow() = dt.Select("TermId ='" & val & "'")
        If dr.Length <= 0 Then
            Return False
        Else
            Return True
        End If

    End Function
    Public Function IsThereAnyClsSectionForTerm(ByVal TermId As String, ByVal CampGrpId As String, Optional ByVal NewCampGrpId As String = "") As Integer
        Return (New TermsDB).IsThereAnyClsSectionForTerm(TermId, CampGrpId, NewCampGrpId)
    End Function
    Public Function IsThereAnyFERPACategory(ByVal FERPACategoryID As String, ByVal CampGrpId As String, Optional ByVal NewCampGrpId As String = "") As Integer
        Return (New TermsDB).IsThereAnyFERPACategory(FERPACategoryID, CampGrpId, NewCampGrpId)
    End Function
    Public Function IsThereAnyFERPAEntity(ByVal FERPAEntityID As String, ByVal CampGrpId As String, Optional ByVal NewCampGrpId As String = "") As Integer
        Return (New TermsDB).IsThereAnyFERPAEntity(FERPAEntityID, CampGrpId, NewCampGrpId)
    End Function
    Public Function ClsSectionForTerm(ByVal TermId As String, ByVal CampGrpId As String, ByVal NewCampGrpId As String) As DataSet
        Return (New TermsDB).ClsSectionForTerm(TermId, CampGrpId, NewCampGrpId)
    End Function
    ''Added by Saraswathi lakshmanan on May 132010
    ''To fix mantis issue 18921 DAta issues forRoss
    ''Function to find the min and maximum class Section dates for a term

    Public Function getMinAndMaxDatesForClassSections(ByVal TermId As String) As DataSet
        Return (New TermsDB).getMinAndMaxDatesForClassSections(TermId)
    End Function

    ''' <summary>
    ''' Return the list of records associated to enrollment list with Scores for the given ClassId
    ''' by components
    ''' </summary>
    ''' <param name="enrollmentIdToModifyList">List of enrollment Guids as string</param>
    ''' <param name="classId">Class Scored</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAllScoredRecordsForEnrollmentAndClassId(ByVal enrollmentIdToModifyList As List(Of String), ByVal classId As String, Optional ByVal isCourseLevel As Boolean = False) As List(Of GrdRecordsPoco)
        Dim db As TermsDB = New TermsDB()
        Dim result As List(Of GrdRecordsPoco) = db.GetAllScoredRecordsForEnrollmentAndClassId(enrollmentIdToModifyList, classId, isCourseLevel)
        Return result
    End Function

    ''' <summary>
    ''' Register the score. The operation are defined in the name of the list.
    ''' This procedure does this as a single transaction. scored all enrollments that have a determinate ClassId for one student.
    ''' </summary>
    ''' <param name="recordsToDeleteList">List of data to generate a delete operation</param>
    ''' <param name="recordsToUpdatePocos">List of data to generate a update </param>
    ''' <param name="recordsToInsertList">List of data to generate a insert operation</param>
    ''' <remarks></remarks>
    Public Shared Sub ExecuteScoreUpdates(ByVal recordsToDeleteList As List(Of GrdRecordsPoco), ByVal recordsToUpdatePocos As List(Of GrdRecordsPoco), ByVal recordsToInsertList As List(Of GrdRecordsPoco), ByVal enrollmentIdToModifyList As List(Of String), ByVal arRecordValues As GrdRecordsPoco)
        Dim db As TermsDB = New TermsDB()
        db.ExecuteScoreUpdates(recordsToDeleteList, recordsToUpdatePocos, recordsToInsertList, enrollmentIdToModifyList, arRecordValues)
    End Sub
End Class
