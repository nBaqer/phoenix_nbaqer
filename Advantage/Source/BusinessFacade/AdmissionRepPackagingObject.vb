Public Class AdmissionRepPackagingObject
    Inherits BaseReportFacade

#Region "Public Methods"

    Public Overrides Function GetReportDataSet(ByVal rptParamInfo As Common.ReportParamInfo) As System.Data.DataSet
        Dim objDB As New AdmissionRepPerformanceDB
        Dim ds As New DataSet
        ' Massage dataset to produce one with the columns and data 
        ' expected by report.
        ds = BuildReportSource(objDB.GetPackagingListByRep(rptParamInfo), objDB.StudentIdentifier)
        ' The report, whatever its frontend is, 
        ' will only need to display the data in the dataset. 
        Return ds
    End Function

#End Region


#Region "Private Methods"

    Private Function BuildReportSource(ByVal ds As DataSet, ByVal StudentIdentifier As String) As DataSet
        Dim strName As String
        Dim facInputMasks As New InputMasksFacade
        Dim strSSNMask As String
        Dim oldCmpGrp, oldCampus, oldAdRep As String
        ''  Dim ctrAdRep As Integer
        Dim rowC, rowCG, rowAR As DataRow
        '' Dim arrRows() As DataRow

        'Get mask for SSN
        strSSNMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        Try
            If ds.Tables.Count = 4 Then
                Dim dt As DataTable = ds.Tables("PackagingListByRep")
                Dim dtCampusTtl As DataTable = ds.Tables("CampusTotals")
                Dim dtCampGrpTtl As DataTable = ds.Tables("CampusGroupTotals")
                Dim dtAdRepTtl As DataTable = ds.Tables("AdmissionRepTotals")
                '
                'Packaging List By Rep
                For Each dr As DataRow In dt.Rows
                    'fields used to link tables on report
                    dr("CampGrpIdStr") = dr("CampGrpId").ToString
                    dr("CampusIdStr") = dr("CampusId").ToString

                    'Admission Rep counter
                    dr("StudentCount") = dt.Rows.Count

                    If Not dr.IsNull("TopAwardDate") And Not dr.IsNull("TopPayPlanDate") Then
                        If Date.Parse(dr("TopAwardDate").ToString) < Date.Parse(dr("TopPayPlanDate").ToString) Then
                            dr("FirstProjectedDate") = dr("TopAwardDate")
                            dr("FirstProjectedAmount") = dr("TopAwardAmount")
                        Else
                            dr("FirstProjectedDate") = dr("TopPayPlanDate")
                            dr("FirstProjectedAmount") = dr("TopPayPlanAmount")
                        End If

                    ElseIf Not dr.IsNull("TopAwardDate") Then
                        dr("FirstProjectedDate") = dr("TopAwardDate")
                        dr("FirstProjectedAmount") = dr("TopAwardAmount")

                    ElseIf Not dr.IsNull("TopPayPlanDate") Then
                        dr("FirstProjectedDate") = dr("TopPayPlanDate")
                        dr("FirstProjectedAmount") = dr("TopPayPlanAmount")
                    End If

                    If Not dr.IsNull("EnrollDate") And Not dr.IsNull("FirstProjectedDate") Then
                        dr("Days") = Date.Parse(dr("FirstProjectedDate")).Subtract(Date.Parse(dr("EnrollDate"))).Days
                    End If

                    'Set up student name as: "LastName, FirstName MI."
                    strName = ""
                    If Not (dr("LastName") Is System.DBNull.Value) Then
                        If dr("LastName") <> "" Then
                            strName = dr("LastName")
                        End If
                    End If
                    If Not (dr("FirstName") Is System.DBNull.Value) Then
                        If dr("FirstName") <> "" Then
                            If strName <> "" Then
                                strName &= ", " & dr("FirstName")
                            Else
                                strName &= dr("FirstName")
                            End If
                        End If
                    End If
                    If Not (dr("MiddleName") Is System.DBNull.Value) Then
                        If dr("MiddleName") <> "" Then
                            If strName <> "" Then
                                strName &= " " & dr("MiddleName") & "."
                            Else
                                strName &= dr("MiddleName") & "."
                            End If
                        End If
                    End If
                    dr("StudentName") = strName
                    '
                    'Apply mask to SSN.
                    If StudentIdentifier = "SSN" Then
                        If Not (dr("StudentIdentifier") Is System.DBNull.Value) Then
                            If dr("StudentIdentifier") <> "" Then
                                Dim temp As String = dr("StudentIdentifier")
                                dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                            End If
                        End If
                    End If

                    If dr.IsNull("AdmissionRep") Then dr("AdmissionRep") = " "
                Next


                'Compute Total Amounts per Campus Groups and Campuses.
                For Each dr As DataRow In dt.Rows
                    If oldCmpGrp = "" Or oldCmpGrp <> dr("CampGrpId").ToString Then
                        'Get a row everytime there is a Campus Group.
                        rowCG = dtCampGrpTtl.NewRow
                        rowCG("CampGrpIdStr") = dr("CampGrpId").ToString
                        rowCG("StudentTotal") = dt.Compute("COUNT(LastName)", "CampGrpId='" & dr("CampGrpId").ToString & "'")
                        rowCG("ProjectedStudents") = dt.Compute("COUNT(LastName)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND FirstProjectedDate IS NOT NULL")
                        If Not rowCG.IsNull("ProjectedStudents") Then
                            If rowCG("ProjectedStudents") <> 0 And rowCG("StudentTotal") <> 0 Then
                                rowCG("ProjectedRate") = Convert.ToInt32(rowCG("ProjectedStudents") * 100 / rowCG("StudentTotal")) & "%"
                            End If
                        End If
                        dtCampGrpTtl.Rows.Add(rowCG)
                        oldCmpGrp = dr("CampGrpId").ToString
                        oldCampus = ""
                        oldAdRep = ""
                    End If

                    If oldCmpGrp = dr("CampGrpId").ToString And oldCampus = "" Then
                        'Compute totals for campus
                        rowC = dtCampusTtl.NewRow
                        rowC("CampGrpIdStr") = oldCmpGrp
                        rowC("CampusIdStr") = dr("CampusId").ToString
                        rowC("StudentTotal") = dt.Compute("COUNT(LastName)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                        "CampusId='" & dr("CampusId").ToString & "'")
                        rowC("ProjectedStudents") = dt.Compute("COUNT(LastName)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                        "CampusId='" & dr("CampusId").ToString & "' " & _
                                                                        "AND FirstProjectedDate IS NOT NULL")
                        If Not rowC.IsNull("ProjectedStudents") Then
                            If rowC("ProjectedStudents") <> 0 And rowC("StudentTotal") <> 0 Then
                                rowC("ProjectedRate") = Convert.ToInt32(rowC("ProjectedStudents") * 100 / rowC("StudentTotal")) & "%"
                            End If
                        End If
                        dtCampusTtl.Rows.Add(rowC)
                        oldCampus = dr("CampusId").ToString
                        oldAdRep = ""
                        '
                    ElseIf oldCmpGrp = dr("CampGrpId").ToString And oldCampus <> dr("CampusId").ToString Then
                        'Compute totals for campus
                        rowC = dtCampusTtl.NewRow
                        rowC("CampGrpIdStr") = oldCmpGrp
                        rowC("CampusIdStr") = dr("CampusId").ToString
                        rowC("StudentTotal") = dt.Compute("COUNT(LastName)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                        "CampusId='" & dr("CampusId").ToString & "'")
                        rowC("ProjectedStudents") = dt.Compute("COUNT(LastName)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                        "CampusId='" & dr("CampusId").ToString & "' " & _
                                                                        "AND FirstProjectedDate IS NOT NULL")
                        If Not rowC.IsNull("ProjectedStudents") Then
                            If rowC("ProjectedStudents") <> 0 And rowC("StudentTotal") <> 0 Then
                                rowC("ProjectedRate") = Convert.ToInt32(rowC("ProjectedStudents") * 100 / rowC("StudentTotal")) & "%"
                            End If
                        End If
                        dtCampusTtl.Rows.Add(rowC)
                        oldCampus = dr("CampusId").ToString
                        oldAdRep = ""
                    End If

                    'If Not dr("AdmissionRep") = "" Then
                    If oldAdRep = "" Or oldAdRep <> dr("AdmissionRep") Then
                        'Compute totals for admission rep
                        rowAR = dtAdRepTtl.NewRow
                        rowAR("CampGrpIdStr") = oldCmpGrp
                        rowAR("CampusIdStr") = oldCampus
                        rowAR("AdmissionRep") = dr("AdmissionRep")
                        rowAR("StudentTotal") = dt.Compute("COUNT(LastName)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                        "CampusId='" & dr("CampusId").ToString & "' AND " & _
                                                                        "AdmissionRep='" & dr("AdmissionRep") & "'")
                        rowAR("ProjectedStudents") = dt.Compute("COUNT(LastName)", "CampGrpId='" & dr("CampGrpId").ToString & "' AND " & _
                                                                        "CampusId='" & dr("CampusId").ToString & "' " & _
                                                                        "AND FirstProjectedDate IS NOT NULL AND " & _
                                                                        "AdmissionRep='" & dr("AdmissionRep") & "'")
                        If Not rowAR.IsNull("ProjectedStudents") Then
                            If rowAR("ProjectedStudents") <> 0 And rowAR("StudentTotal") <> 0 Then
                                rowAR("ProjectedRate") = Convert.ToInt32(rowAR("ProjectedStudents") * 100 / rowAR("StudentTotal")) & "%"
                            End If
                        End If
                        dtAdRepTtl.Rows.Add(rowAR)
                        oldAdRep = dr("AdmissionRep")
                        'ctrAdRep += 1
                    End If
                Next
            End If

        Catch ex As System.Exception
            If ex.InnerException Is Nothing Then
                Throw New Exception("Error building report dataset - " & ex.Message)
            Else
                Throw New Exception("Error building report dataset - " & ex.InnerException.Message)
            End If
        End Try

        Return ds
    End Function

#End Region


End Class
