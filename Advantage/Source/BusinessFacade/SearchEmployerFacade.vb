Public Class SearchEmployerFacade
    Public Function GetAllCity() As DataSet
        '   connect to the database
        With New SearchEmployerDB
            Return .GetAllCity()
        End With
    End Function
    Public Function GetAllState() As DataSet
        '   connect to the database
        With New SearchEmployerDB
            Return .GetAllState()
        End With

    End Function
    Public Function GetAllZip() As DataSet
        '   connect to the database
        With New SearchEmployerDB
            Return .GetAllZip()
        End With
    End Function
    Public Function GetAllCounty() As DataSet
        '   connect to the database
        With New SearchEmployerDB
            Return .GetAllCounty()
        End With
    End Function
    Public Function GetEmployerSearch(ByVal strCode As String, ByVal strEmployerID As String, ByVal strEmployerName As String, ByVal strCity As String, ByVal strState As String, ByVal strZip As String, ByVal strCounty As String, ByVal strLocation As String, ByVal strStatus As String, ByVal strIndustry As String, ByVal strCampus As String) As DataSet
        With New SearchEmployerDB
            Return .GetAllEmployers(strCode, strEmployerID, strEmployerName, strCity, strState, strZip, strCounty, strLocation, strStatus, strIndustry, strCampus)
        End With
    End Function
End Class
