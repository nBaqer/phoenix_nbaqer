Public Class PostFinalGrdsFacade
    Public Function GetAllTerms() As DataSet

        '   Instantiate DAL component
        Dim db As New PostFinalGrdsDB

        '   get the dataset with all degrees
        Return db.GetAllTerms()

    End Function
    Public Function GetCurrentAndPastTerms(Optional ByVal campusId As String = "") As DataSet

        '   Instantiate DAL component
        Dim grdPostDB As New GrdPostingsDB

        '   get the dataset with all degrees
        Return grdPostDB.GetCurrentAndPastTerms("", campusId)
    End Function
    Public Function GetInstructors(ByVal term As String, ByVal campusId As String) As DataSet
        Dim db As New PostFinalGrdsDB
        Dim ds As DataSet

        '   get the dataset with all degrees          
        ds = db.GetInstructorsForTerm(Term, CampusId)

        Return ConcatenateEmployeeLastFirstName(ds)


    End Function

    Public Function ConcatenateEmployeeLastFirstName(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        'Dim row As DataRow
        tbl = ds.Tables("InstructorDT")
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("InstructorId")}
        End With
       
        Return ds
    End Function

    Public Function GetClsSections(ByVal term As String, ByVal instructor As String, ByVal campusId As String) As DataSet

        '   Instantiate DAL component
        Dim db As New PostFinalGrdsDB
        Dim ds As DataSet

        'Dim rowCount As Integer
        '   get the dataset with all degrees          
        ds = db.GetClsSects(term, Instructor, CampusId)
        'rowCount = ds.Tables("TermClsSects").Rows.Count


        ds.Tables("TermClsSects").AcceptChanges()

        ds = ConcatenateClassNameAndSectionNumber(ds)

        Return ds

    End Function

    Public Function ConcatenateClassNameAndSectionNumber(ByVal ds As DataSet) As DataSet
        Dim tbl As DataTable
        Dim row As DataRow

        tbl = ds.Tables(0)
        With tbl
            .PrimaryKey = New DataColumn() {.Columns("a.ClsSectionId")}
        End With
        If ds.Tables(0).Rows.Count > 0 Then
            'Add a new column called CourseSectDescrip to the datatable 
            tbl.Columns.Add("CourseSectDescrip", GetType(String))
            For Each row In tbl.Rows
                row("CourseSectDescrip") = row("Descrip") & " - " & row("ClsSection")
            Next
        End If
        Return ds
    End Function

    Public Function GetStdInfoFrDatagrid(ByVal clsSect As String, ByVal userId As String) As DataSet

        Dim db As New PostFinalGrdsDB
        Dim ds As DataSet

      ds = db.GetStudentInfoForFinalGrades(clsSect, userId)

        ' Eliminate duplicate studentId if exists-------------------------------------
        If ds.Tables(0).Rows.Count > 0 Then
            Dim clean = ds.Tables(0).AsEnumerable().GroupBy(Function(x) x.Field(Of Guid)("StudentId")).[Select](Function(g) g.First())
            Dim cleanDt As DataTable = clean.CopyToDataTable()
            ds.Tables(0).Clear()
            ds.Tables(0).Merge(cleanDt)
        End If

        '------------------------------------------------------------------------------


        Dim facInputMasks As New InputMasksFacade
        'Dim stuName As String
        Dim strSSNMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        For Each dr As DataRow In ds.Tables("ClsSectStds").Rows
            'Apply mask to SSN.
            If Not String.IsNullOrWhiteSpace(dr("StudentIdentifier").ToString()) Then
                If dr("IsSSNStudentIdentifier") = "1" Then
                    Dim temp As String = dr("StudentIdentifier")
                    dr("StudentIdentifier") = facInputMasks.ApplyMask(strSSNMask, "*****" & temp.Substring(5))
                End If
            End If
        Next

        Return ds 'Might not need this function for this page.

    End Function
    Public Function GetAllGrades(ByVal clsSect As String) As DataSet

        '   Instantiate DAL component
        With New PostFinalGrdsDB

            '   get the dataset with all TuitionCategories
            Return .GetGradeSystemDetails(clsSect)

        End With

    End Function
    Public Function GetContinuingEdGrades(ByVal prgVerId As String) As DataSet

        '   Instantiate DAL component
        With New PostFinalGrdsDB

            '   get the dataset with all TuitionCategories
            Return .GetContinuingEDGrades(prgVerId)

        End With

    End Function


    Public Function BuildStuInfoDS(ByVal ds As DataSet) As DataSet
        Dim tbl1 As DataTable
        Dim row As DataRow
        Dim tbl2 As DataTable


        tbl1 = ds.Tables("ClsSectStds")
        If (ds.Tables("ClsSectStdGrds").Rows.Count = 0) Then

        End If
        'Make the ChildId column the primary key
        With tbl1
            .PrimaryKey = New DataColumn() {.Columns("StudentId")}
        End With

        tbl2 = ds.Tables("ClsSectGrds")
        'Make the ChildId column the primary key
        With tbl2
            .PrimaryKey = New DataColumn() {.Columns("StudentId")}
        End With
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If ds.Tables("ClsSectStdGrds").Rows.Count > 0 Then
            'Load a datatable with ChildId, ChildTyp and ChildDescrip
            'Create a datatable with a new column called ChildId, make this the primary key.

            'Add a new column called ChildDescrip to the datatable 
            tbl1.Columns.Add("Score", GetType(String))
            tbl1.Columns.Add("Comments", GetType(String))

            For Each row In tbl1.Rows

                If (tbl2.Rows.Count > 0) Then
                    Dim row2 As DataRow = tbl2.Rows.Find(row("StudentId"))
                    If Not (row2 Is Nothing) Then
                        row("Score") = row2("Score")
                        row("Comments") = row2("Comments")
                    End If
                End If
            Next

        End If
        Return ds
    End Function

    'Public Function UpdateFinalGrade(ByVal FinalGrdObj As FinalGradeInfo, ByVal user As String, Optional ByVal scoreIsNull As Boolean = False) As String
    '    Dim db As New PostFinalGrdsDB

    '    Return db.UpdateFinalGrade(FinalGrdObj, user, scoreIsNull)
    'End Function

    ''' <summary>
    ''' Update all final grades for the enrollments in the parameters lists.
    ''' </summary>
    ''' <param name="listOfUpdateOperationsFormatNumeric">Update Enrollment class score in school Format Grades Numeric</param>
    ''' <param name="listOfUpdateOperationsFormatNotNumeric">Update enrollment class grade in school with format grade Not Numeric</param>
    Public Shared Sub UpdateFinalGradePerEnrollments(ByVal listOfUpdateOperationsFormatNumeric As List(Of GrdRecordsPoco), ByVal listOfUpdateOperationsFormatNotNumeric As List(Of GrdRecordsPoco))
        If (listOfUpdateOperationsFormatNotNumeric.Count = 0 And listOfUpdateOperationsFormatNumeric.Count = 0) Then
            Return
        End If
        Dim db As New PostFinalGrdsDB
        db.UpdateFinalGradePerEnrollments(listOfUpdateOperationsFormatNumeric, listOfUpdateOperationsFormatNotNumeric)
    End Sub

    ''show the CohortStart Date for Current and Past terms
    Public Function GetCohortStartDateforCurrentAndPastTerms(Optional ByVal campusId As String = "") As DataSet

        '   Instantiate DAL component
        Dim grdPostDB As New PostFinalGrdsDB

        '   get the dataset with all degrees
        Return grdPostDB.GetCohortStartDateforCurrentAndPastTerms("", campusId)
    End Function

    ''get the Class Section based on the CohortStartDate and Instructor
    Public Function GetClsSectionsbyCohortStartDate(ByVal cohortStartDate As String, ByVal instructor As String, ByVal campusId As String) As DataSet

        '   Instantiate DAL component
        Dim db As New PostFinalGrdsDB
        Dim ds As DataSet
        ' Dim ReqId As String
        'Dim i As Integer
        Dim rowCount As Integer
        '   get the dataset with all degrees          
        ds = db.GetClsSectsbyCohortStartDate(cohortStartDate, instructor, CampusId)
        rowCount = ds.Tables("TermClsSects").Rows.Count

        For i As Integer = 0 To rowCount - 1
            If Not db.GetTheoryHours(ds.Tables("TermClsSects").Rows(i)("ReqId").ToString) Then
                ds.Tables("TermClsSects").Rows(i).Delete()
            End If
        Next
        ds.Tables("TermClsSects").AcceptChanges()

        'count = DB.GetTheoryHours("35ABD9F7-68F8-4E64-B14C-284AEED4C3EE")
        ds = ConcatenateClassNameAndSectionNumber(ds)

        Return ds

    End Function
    ''find the Instructors based on the Cohort Start Date
    ''October 14 th 2008
    Public Function GetInstructorbyCohortStartDate(ByVal cohortStartDate As String, ByVal campusId As String) As DataSet
        Dim db As New PostFinalGrdsDB
        Dim ds As DataSet

        '   get the dataset with all degrees          
        ds = db.GetInstructorbyCohortStartDate(cohortStartDate, campusId)

        Return ConcatenateEmployeeLastFirstName(ds)

    End Function

End Class
