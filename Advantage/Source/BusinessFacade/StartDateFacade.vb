Public Class StartDateFacade

    Public Function GetGradeScales() As DataTable
        Dim db As New StartDateDB

        Return db.GetGradeScales

    End Function

    Public Function GetShifts() As DataTable
        Dim db As New StartDateDB

        Return db.GetShifts

    End Function

    Public Function GetRelatedStarts(ByVal termId As String, Optional ByVal shiftId As String = "") As DataTable
        Dim db As New StartDateDB

        Return db.GetRelatedStarts(termId, shiftId)

    End Function

    Public Function GetStartDateClassesAndMeetings(ByVal termId As String) As DataSet
        Dim db As New StartDateDB

        Return db.GetStartDateClassesAndMeetings(termId)

    End Function
    Public Function ValidateEndDate(ByVal termId As String) As String
        Dim db As New StartDateDB
        Return db.ValidateEndDate(termId)
        
    End Function

    Public Function GetTermStartDate(ByVal termId) As String
        Dim db As New StartDateDB

        Return db.GetTermStartDate(termId)

    End Function

    Public Function GetTermEndDate(ByVal termId) As String
        Dim db As New StartDateDB

        Return db.GetTermEndDate(termId)

    End Function

    Public Function ProcessStartDateClassesAndMeetings(ByVal dtEntries As DataTable, ByVal dsDBInfo As DataSet, ByVal termID As String, _
        ByVal modUser As String, ByVal campusId As String, ByVal grdScaleId As String, ByVal shiftID As String) As String
        Dim db As New StartDateDB

        Return db.ProcessStartDateClassesAndMeetings(dtEntries, dsDBInfo, termID, modUser, campusId, grdScaleId, shiftID)

    End Function

    Public Function GetOverlappingClassesForStartDate(ByVal termID As String, ByVal shift As String) As DataTable
        Dim db As New StartDateDB

        Return db.GetOverlappingClassesForStartDate(termID, shift)

    End Function

    Public Function AddClassesToStartDate(ByVal arrCS As ArrayList, ByVal termID As String, ByVal user As String) As String
        Dim db As New StartDateDB

        Return db.AddClassesToStartDate(arrCS, termID, user)

    End Function

    Public Function AreStuentsRegisteredForStart(ByVal termId As String) As Boolean
        Dim db As New StartDateDB

        Return db.AreStuentsRegisteredForStart(termId)

    End Function

    Public Function GetDatesIfNull(ByVal termId As String, ByVal clsSecId As String) As String()
        Dim db As New StartDateDB
        Return db.GetDatesIfNull(termId, clsSecId)
    End Function
End Class

