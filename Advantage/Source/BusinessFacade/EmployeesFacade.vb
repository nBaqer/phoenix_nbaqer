' ===============================================================================
' FAME AdvantageV1.BusinessFacade
'
' EmployeesFacade.vb
'
' Employees Services Interface (Facade). 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class EmployeesFacade

    Public Function GetAvailableRolesPerEmployee(ByVal empId As String) As DataSet

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return Dataset
        Return empDB.GetAvailableRolesPerEmployee(empId)

    End Function
    Public Function UpdateEmployeeRoles(ByVal empId As String, ByVal user As String, ByVal selectedRoles() As String) As String

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return query results
        Return empDB.UpdateEmployeeRoles(empId, user, selectedRoles)

    End Function
    Public Function GetDegreesPerEmployee(ByVal empId As String) As DataSet

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return Dataset
        Return empDB.GetDegreesPerEmployee(empId)

    End Function
    Public Function GetCertificationsPerEmployee(ByVal empId As String) As DataSet

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return Dataset
        Return empDB.GetCertificationsPerEmployee(empId)

    End Function
    Public Function UpdateEmployeeDegrees(ByVal empId As String, ByVal user As String, ByVal selectedDegrees() As String) As String

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return query results
        Return empDB.UpdateEmployeeDegrees(empId, user, selectedDegrees)

    End Function
    Public Function UpdateEmployeeCertifications(ByVal empId As String, ByVal user As String, ByVal selectedCertifications() As String) As String

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return query results
        Return empDB.UpdateEmployeeCertifications(empId, user, selectedCertifications)

    End Function
    Public Function GetAvailableAssignedActivitiesPerEmployee(ByVal empId As String) As DataSet

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return Dataset
        Return empDB.GetAvailableAssignedActivitiesPerEmployee(empId)

    End Function
    Public Function GetSelectedAssignedActivitiesPerEmployee(ByVal empId As String) As DataSet

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return Dataset
        Return empDB.GetSelectedAssignedActivitiesPerEmployee(empId)

    End Function
    Public Function UpdateEmployeeAssignedActivities(ByVal empId As String, ByVal user As String, ByVal selectedAssignedActivities() As String) As String

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return query results
        Return empDB.UpdateEmployeeAssignedActivities(empId, user, selectedAssignedActivities)

    End Function
    Public Function GetSelectedRolesPerEmployee(ByVal empId As String) As DataSet

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return dataset
        Return empDB.GetSelectedRolesPerEmployee(empId)

    End Function
    Public Function GetContactInfo(ByVal empId As String) As EmployeeContactInfo

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return dataset
        Return empDB.GetContactInfo(empId)

    End Function
    Public Function GetHRInfo(ByVal empId As String) As EmployeeHRInfo

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return dataset
        Return empDB.GetHRInfo(empId)

    End Function
    Public Function GetUserInfo(ByVal empId As String) As EmployeeUserInfo

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return dataset
        Return empDB.GetUserInfo(empId)

    End Function
    Public Function GetInfo(ByVal empId As String) As EmployeeInfo

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return dataset
        Return empDB.GetInfo(empId)

    End Function
    Public Function UpdateContactInfo(ByVal empContactInfoData As EmployeeContactInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New EmployeesDB

            '   If it is a new record do an insert. If not, do an update
            If Not (empContactInfoData.IsInDB = True) Then
                '   return string with insert results
                Return .AddContactInfo(empContactInfoData, user)
            Else
                '   return integer with update results
                Return .UpdateContactInfo(empContactInfoData, user)
            End If

        End With
    End Function
    Public Function UpdateHRInfo(ByVal empHRInfoData As EmployeeHRInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New EmployeesDB

            '   If it is a new record do an insert. If not, do an update
            If Not (empHRInfoData.IsInDB = True) Then
                '   return string with insert results
                Return .AddHRInfo(empHRInfoData, user)
            Else
                '   return integer with update results
                Return .UpdateHRInfo(empHRInfoData, user)
            End If

        End With
    End Function
    Public Function UpdateUserInfo(ByVal empUserInfoData As EmployeeUserInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New EmployeesDB

            '   If it is a new record do an insert. If not, do an update
            If Not (empUserInfoData.IsInDB = True) Then
                '   return string with insert results
                Return .AddUserInfo(empUserInfoData, user)
            Else
                '   return integer with update results
                Return .UpdateUserInfo(empUserInfoData, user)
            End If

        End With
    End Function
    Public Function UpdateInfo(ByVal empInfo As FAME.AdvantageV1.Common.EmployeeInfo, ByVal user As String) As String

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   If it is a new record do an insert. If not, do an update
        If Not (empInfo.IsInDB = True) Then
            '   return string with insert results
            Return empDB.AddEmployee(empInfo, user)
        Else
            '   return string with update results
            Return empDB.UpdateInfo(empInfo, user)
        End If

    End Function
    Public Function GetAllEmployeesForEmail(ByVal statusId As String) As DataSet

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return Dataset
        Return empDB.GetAllEmployeesForEmail(statusId)

    End Function
    Public Function GetAllEmployees(ByVal statusId As String) As DataSet

        '   Instantiate DAL component
        Dim empDB As New EmployeesDB

        '   return Dataset
        Return empDB.GetAllEmployees(statusId)

    End Function
    Public Function GetAllEmployeeEmergencyContacts(ByVal empId As String, ByVal statusSelectIndex As Integer) As DataSet

        '   Instantiate DAL component
        With New EmployeeEmergencyContactsDB

            '   get the dataset with all EmployeeEmergencyContacts
            Return .GetAllEmployeeEmergencyContacts(empId, statusSelectIndex)

        End With

    End Function
    Public Function GetEmployeeEmergencyContactInfo(ByVal employeeEmergencyContactId As String) As EmployeeEmergencyContactInfo

        '   get the EmployeeEmergencyContactInfo
        Return (New EmployeeEmergencyContactsDB).GetEmployeeEmergencyContactInfo(employeeEmergencyContactId)

    End Function
    Public Function UpdateEmployeeEmergencyContactInfo(ByVal employeeEmergencyContactInfo As EmployeeEmergencyContactInfo, ByVal user As String) As String

        '   Instantiate DAL component
        With New EmployeeEmergencyContactsDB

            '   If it is a new account do an insert. If not, do an update
            If Not (employeeEmergencyContactInfo.IsInDB = True) Then
                '   return integer with insert results
                Return .AddEmployeeEmergencyContactInfo(employeeEmergencyContactInfo, user)
            Else
                '   return integer with update results
                Return .UpdateEmployeeEmergencyContactInfo(employeeEmergencyContactInfo, user)
            End If

        End With

    End Function
    Public Function DeleteEmployeeEmergencyContactInfo(ByVal EmployeeEmergencyContactId As String, ByVal modDate As DateTime) As String

        '   Instantiate DAL component
        With New EmployeeEmergencyContactsDB

            '   delete EmployeeEmergencyContactInfo ans return string result
            Return .DeleteEmployeeEmergencyContactInfo(EmployeeEmergencyContactId, modDate)

        End With

    End Function
    Public Function DeleteEmployee(ByVal empId As String) As String

        '   Instantiate DAL component
        With New EmployeesDB

            '   delete EmployeeEmergencyContactInfo ans return string result
            Return .DeleteEmployee(empId)

        End With

    End Function
    Public Function GetAllPositions() As DataSet

        '   get the dataset with all Positions
        Return (New PositionsDB).GetAllPositions()

    End Function
    Public Function GetPositionInfo(ByVal PositionId As String) As PositionInfo

        '   get the PositionInfo
        Return (New PositionsDB).GetPositionInfo(PositionId)

    End Function
    Public Function UpdatePositionInfo(ByVal PositionInfo As PositionInfo, ByVal user As String) As String


        '   If it is a new account do an insert. If not, do an update
        If Not (PositionInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New PositionsDB).AddPositionInfo(PositionInfo, user)
        Else
            '   return integer with update results
            Return (New PositionsDB).UpdatePositionInfo(PositionInfo, user)
        End If

    End Function
    Public Function DeletePositionInfo(ByVal PositionId As String, ByVal modDate As DateTime) As String

        '   delete PositionInfo ans return string result
        Return (New PositionsDB).DeletePositionInfo(PositionId, modDate)

    End Function
    Public Function GetAllHRDepartments() As DataSet


        '   get the dataset with all HRDepartments
        Return (New HRDepartmentsDB).GetAllHRDepartments()

    End Function
    Public Function GetHRDepartmentInfo(ByVal HRDepartmentId As String) As HRDepartmentInfo

        '   get the HRDepartmentInfo
        Return (New HRDepartmentsDB).GetHRDepartmentInfo(HRDepartmentId)

    End Function
    Public Function UpdateHRDepartmentInfo(ByVal HRDepartmentInfo As HRDepartmentInfo, ByVal user As String) As String


        '   If it is a new account do an insert. If not, do an update
        If Not (HRDepartmentInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New HRDepartmentsDB).AddHRDepartmentInfo(HRDepartmentInfo, user)
        Else
            '   return integer with update results
            Return (New HRDepartmentsDB).UpdateHRDepartmentInfo(HRDepartmentInfo, user)
        End If

    End Function
    Public Function DeleteHRDepartmentInfo(ByVal HRDepartmentId As String, ByVal modDate As DateTime) As String

        '   delete HRDepartmentInfo ans return string result
        Return (New HRDepartmentsDB).DeleteHRDepartmentInfo(HRDepartmentId, modDate)

    End Function
    Public Function GetAllCertifications() As DataSet


        '   get the dataset with all Certifications
        Return (New CertificationsDB).GetAllCertifications()

    End Function
    Public Function GetCertificationInfo(ByVal CertificationId As String) As CertificationInfo

        '   get the CertificationInfo
        Return (New CertificationsDB).GetCertificationInfo(CertificationId)

    End Function
    Public Function UpdateCertificationInfo(ByVal CertificationInfo As CertificationInfo, ByVal user As String) As String


        '   If it is a new account do an insert. If not, do an update
        If Not (CertificationInfo.IsInDB = True) Then
            '   return integer with insert results
            Return (New CertificationsDB).AddCertificationInfo(CertificationInfo, user)
        Else
            '   return integer with update results
            Return (New CertificationsDB).UpdateCertificationInfo(CertificationInfo, user)
        End If

    End Function
    Public Function DeleteCertificationInfo(ByVal CertificationId As String, ByVal modDate As DateTime) As String

        '   delete CertificationInfo ans return string result
        Return (New CertificationsDB).DeleteCertificationInfo(CertificationId, modDate)

    End Function

End Class

