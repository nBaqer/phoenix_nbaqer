' ===============================================================================
' RoleInfo.vb
' Info classes for managing roles
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Public Class RoleInfo
    Private _IsInDB As Boolean
    Private _RoleId As String
    Private _Code As String
    Private _StatusId As String
    Private _Description As String
    Private _AdvantageRoleId As String
    Private _ModUser As String
    Private _ModDate As DateTime
    Public Property IsInDB() As Boolean
        Get
            Return _IsInDB
        End Get
        Set(ByVal Value As Boolean)
            _IsInDB = Value
        End Set
    End Property

    Public Property RoleId() As String
        Get
            Return _RoleId
        End Get
        Set(ByVal Value As String)
            _RoleId = Value
        End Set
    End Property

    Public Property Code() As String
        Get
            Return _Code
        End Get
        Set(ByVal Value As String)
            _Code = Value
        End Set
    End Property

    Public Property StatusId() As String
        Get
            Return _StatusId
        End Get
        Set(ByVal Value As String)
            _StatusId = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property AdvantageRoleId() As String
        Get
            Return _AdvantageRoleId
        End Get
        Set(ByVal Value As String)
            _AdvantageRoleId = Value
        End Set
    End Property

    Public Property ModUser() As String
        Get
            Return _ModUser
        End Get
        Set(ByVal Value As String)
            _ModUser = Value
        End Set
    End Property

    Public Property ModDate() As DateTime
        Get
            Return _ModDate
        End Get
        Set(ByVal Value As DateTime)
            _ModDate = Value
        End Set
    End Property

    Public Sub New()
        MyBase.New()
        _RoleId = Guid.NewGuid.ToString
        _IsInDB = False
        _Code = ""
        _Description = ""
        _AdvantageRoleId = ""
        _StatusId = ""
        _ModUser = ""
        _ModDate = Date.MinValue
    End Sub

End Class
