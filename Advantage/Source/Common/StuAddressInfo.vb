Public Class StuAddressInfo
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _StudentID As String
    Private _StdAddressId As String
    Private _Address1 As String
    Private _Address2 As String
    Private _City As String
    Private _State As String
    Private _zip As String
    Private _Country As String
    Private _County As String
    Private _AddressStatus As String
    Private _AddressTypeID As String
    Private _AddressType As String
    Private _CountryText As String
    Private _AddTypeText As String
    Private _AddStatusText As String
    Private _StateText As String

    Private _ForeignZip As Integer
    Private _OtherState As String
    Private _StateId As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _Default1 As Integer
#End Region

#Region " Public Constructor"
    Public Sub New()
        _isInDB = False
        _Address1 = ""
        _Address2 = ""
        _City = ""
        _State = ""
        _zip = ""
        _Country = ""
        _County = Guid.Empty.ToString
        _AddressStatus = ""
        _AddressType = ""
        _AddTypeText = ""
        _AddStatusText = ""
        _CountryText = ""
        _StateText = ""
        _StdAddressId = Guid.NewGuid.ToString()

        _ForeignZip = 0
        _Default1 = 0
        _OtherState = ""
        _StateId = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region "Public Properties"
    Public Property StateId() As String
        Get
            Return _StateId
        End Get
        Set(ByVal Value As String)
            _StateId = Value
        End Set
    End Property

    Public Property Default1() As Integer
        Get
            Return _Default1
        End Get
        Set(ByVal Value As Integer)
            _Default1 = Value
        End Set
    End Property
    Public Property ForeignZip() As Integer
        Get
            Return _ForeignZip
        End Get
        Set(ByVal Value As Integer)
            _ForeignZip = Value
        End Set
    End Property
    Public Property OtherState() As String
        Get
            Return _OtherState
        End Get
        Set(ByVal Value As String)
            _OtherState = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property

    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property

    Public Property Address1() As String
        Get
            Return _Address1
        End Get
        Set(ByVal Value As String)
            _Address1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return _Address2
        End Get
        Set(ByVal Value As String)
            _Address2 = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _City
        End Get
        Set(ByVal Value As String)
            _City = Value
        End Set
    End Property
    Public Property State() As String
        Get
            Return _State
        End Get
        Set(ByVal Value As String)
            _State = Value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return _zip
        End Get
        Set(ByVal Value As String)
            _zip = Value
        End Set
    End Property
    Public Property Country() As String
        Get
            Return _Country
        End Get
        Set(ByVal Value As String)
            _Country = Value
        End Set
    End Property
    Public Property County() As String
        Get
            Return _County
        End Get
        Set(ByVal Value As String)
            _County = Value
        End Set
    End Property

    Public Property AddressStatus() As String
        Get
            Return _AddressStatus
        End Get
        Set(ByVal Value As String)
            _AddressStatus = Value
        End Set
    End Property
    Public Property AddressType() As String
        Get
            Return _AddressType
        End Get
        Set(ByVal Value As String)
            _AddressType = Value
        End Set
    End Property
    Public Property StudentID() As String
        Get
            Return _StudentID
        End Get
        Set(ByVal Value As String)
            _StudentID = Value
        End Set
    End Property
    Public Property StdAddressId() As String
        Get
            Return _StdAddressId
        End Get
        Set(ByVal Value As String)
            _StdAddressId = Value
        End Set
    End Property
    Public Property CountryText() As String
        Get
            Return _CountryText
        End Get
        Set(ByVal Value As String)
            _CountryText = Value
        End Set
    End Property
    Public Property StateText() As String
        Get
            Return _StateText
        End Get
        Set(ByVal Value As String)
            _StateText = Value
        End Set
    End Property
    Public Property AddTypeText() As String
        Get
            Return _AddTypeText
        End Get
        Set(ByVal Value As String)
            _AddTypeText = Value
        End Set
    End Property
    Public Property AddStatusText() As String
        Get
            Return _AddStatusText
        End Get
        Set(ByVal Value As String)
            _AddStatusText = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property

#End Region
End Class
