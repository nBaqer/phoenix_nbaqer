
Imports FAME.Advantage.Common

Public Class AdvantageCommonValues
    Public Enum TranscriptTypes
        Traditional = 1
        Traditional_A = 2
        Traditional_B = 4
        Traditional_Numeric = 6
    End Enum
    Public Enum AddressToBePrintedInReceipts
        CorporateAddress = 1
        CampusAddress = 2
    End Enum
    Public Enum TuitionFeeTypes
        Program = 1
        Course = 2
        Term = 4
    End Enum
    Public Enum SchedulingMethods
        Traditional = 1
        ModuleStart = 2
        TraditionalClassesRestrictedByStartDate = 4
    End Enum
    Public Enum SchoolOptions
        Traditional = 1
        ModuleStart = 2
        TraditionalClassesRestrictedByStartDate = 4
        ComCourse = 8
    End Enum
    Public Enum MeetDays
        Monday = 1
        Tuesday = 2
        Wednesday = 4
        Thursday = 8
        Friday = 16
        Saturday = 32
        Sunday = 64
    End Enum
    Public Enum IncludeStatus
        ActiveOnly = 1 'active=true
        InactiveOnly = 2
        ActiveAndInactive = 0 'active=false
    End Enum

    Public Shared ReadOnly Property MaximumInputDecimalValue() As Decimal
        Get
            Return 200000000.0  'two hundred million is the maximum amount
        End Get
    End Property
    Public Shared ReadOnly Property MinimumInputDecimalValue() As Decimal
        Get
            Return -200000000.0  'two hundred million is the maximum amount
        End Get
    End Property

    Public Shared ReadOnly Property MinimumAcceptableEmployeeAge() As Integer
        Get
            Return 16
        End Get
    End Property
    Public Shared ReadOnly Property ActiveGuid() As String
        Get
            Return "f23de1e2-d90a-4720-b4c7-0f6fb09c9965"
        End Get
    End Property
    Public Shared ReadOnly Property InactiveGuid() As String
        Get
            Return "1af592a6-8790-48ec-9916-5412c25ef49f"
        End Get
    End Property
    Public Shared ReadOnly Property PresentAbsentGuid() As String
        Get
            Return "ef5535c2-142c-4223-ae3c-25a50a153cc6"
        End Get
    End Property
    Public Shared ReadOnly Property MinutesGuid() As String
        Get
            Return "a1389c74-0bb9-4bbf-a47f-68428be7fa4d"
        End Get
    End Property
    Public Shared ReadOnly Property ClockHoursGuid() As String
        Get
            Return "b937c92e-fd7a-455e-a731-527a9918c734"
        End Get
    End Property
    Public Shared ReadOnly Property NoneGuid() As String
        Get
            Return "2600592A-9739-4A13-BDCE-7A25FE4A7478"
        End Get
    End Property
    Public Shared ReadOnly Property USAGuid() As String
        Get
            Return "d73bfc65-db8c-48af-8780-583bf9fae42e"
        End Get
    End Property
    Public Shared ReadOnly Property MinDate() As Date
        Get
            Return Date.Parse("1945-01-01")
        End Get
    End Property
    Public Shared ReadOnly Property MaxDate() As Date
        Get
            Return Date.Parse("2050-12-31")
        End Get
    End Property
    Public Shared ReadOnly Property FiveYearsAgo() As Date
        Get
            Return Date.Now.Subtract(New TimeSpan(1826, 0, 0))
        End Get
    End Property
    Public Shared ReadOnly Property FiveYearsFromNow() As Date
        Get
            Return Date.Now.Add(New TimeSpan(1826, 0, 0))
        End Get
    End Property
    Public Shared ReadOnly Property OtherStateGuid() As String
        Get
            Return "5451720d-583d-4364-802a-bf500eda1234"
        End Get
    End Property
    Public Shared ReadOnly Property SAGuid() As String
        Get
            Return "8de3e9a5-d52b-4600-a183-a448528c25c0"
        End Get
    End Property
    Public Shared ReadOnly Property BetweenOperatorValue() As Integer
        Get
            Return 6
        End Get
    End Property
    Public Shared ReadOnly Property EqualToOperatorValue() As Integer
        Get
            Return 1
        End Get
    End Property
    Public Shared ReadOnly Property ChildWindowSettingsAdReqs() As String
        Get
            Return "status=yes,resizable=yes,scrollbars=yes,width=900px,height=570px"
        End Get
    End Property
    Public Shared ReadOnly Property ChildWindowSettingsLarge() As String
        Get
            Return "status=yes,resizable=yes,scrollbars=yes,width=1280,height=800px"
        End Get
    End Property
    Public Shared ReadOnly Property ChildWindowSettingsFull() As String
        Get
            Return "status=yes,resizable=yes,scrollbars=yes,width=1024px,height=550px"
        End Get
    End Property
    Public Shared ReadOnly Property ChildWindowSettingsMedium() As String
        Get
            Return "toolbar=no,status=no,resizable=yes,scrollbars=yes,width=900px,height=470px"
        End Get
    End Property
    Public Shared ReadOnly Property ChildWindowSettingsSmall() As String
        Get
            Return "toolbar=no,status=no,width=780px,height=300px"
        End Get
    End Property
    Public Shared ReadOnly Property ChildWindowSettingsSmaller() As String
        Get
            Return "toolbar=no,status=no,width=780px,height=150px"
        End Get
    End Property
    Public Shared ReadOnly Property NavigationRoles() As String
        Get
            Return "792A4999-542D-41DA-8B6A-3E994C852794"
        End Get
    End Property
    Public Shared ReadOnly Property ADResourceRelation() As Integer
        Get
            Return 189
        End Get
    End Property
    Public Shared ReadOnly Property ARResourceRelation() As Integer
        Get
            Return 188
        End Get
    End Property
    Public Shared ReadOnly Property PLResourceRelation() As Integer
        Get
            Return 193
        End Get
    End Property
    Public Shared ReadOnly Property SAResourceRelation() As Integer
        Get
            Return 194
        End Get
    End Property
    Public Shared ReadOnly Property HRResourceRelation() As Integer
        Get
            Return 192
        End Get
    End Property
    Public Shared ReadOnly Property FACResourceRelation() As Integer
        Get
            Return 300
        End Get
    End Property
    Public Shared ReadOnly Property FinAidResourceRelation() As Integer
        Get
            Return 191
        End Get
    End Property
    Public Shared ReadOnly Property SysResourceRelation() As Integer
        Get
            Return 195
        End Get
    End Property
    Public Shared ReadOnly Property StartDateGuid() As Integer
        Get
            Return 3
        End Get
    End Property
    Public Shared ReadOnly Property ARStudentRelation() As Integer
        Get
            Return 108
        End Get
    End Property
    Public Shared ReadOnly Property PLStudentRelation() As Integer
        Get
            Return 198
        End Get
    End Property
    Public Shared ReadOnly Property SAStudentRelation() As Integer
        Get
            Return 231
        End Get
    End Property
    Public Shared ReadOnly Property FinAidStudentRelation() As Integer
        Get
            Return 308
        End Get
    End Property
    Public Shared ReadOnly Property Gender() As Integer
        Get
            Return 1
        End Get
    End Property
    Public Shared ReadOnly Property MaritalStatus() As Integer
        Get
            Return 2
        End Get
    End Property
    Public Shared ReadOnly Property EducationLevel() As Integer
        Get
            Return 3
        End Get
    End Property
    Public Shared ReadOnly Property DependancyType() As Integer
        Get
            Return 4
        End Get
    End Property
    Public Shared ReadOnly Property GeographicTypeDescrip() As Integer
        Get
            Return 5
        End Get
    End Property
    Public Shared ReadOnly Property EthnicCode() As Integer
        Get
            Return 6
        End Get
    End Property
    Public Shared ReadOnly Property Citizenship() As Integer
        Get
            Return 7
        End Get
    End Property
    Public Shared ReadOnly Property DegCertSeeking() As Integer
        Get
            Return 8
        End Get
    End Property
    Public Shared ReadOnly Property AdminCriteria() As Integer
        Get
            Return 9
        End Get
    End Property
    Public Shared ReadOnly Property Housing() As Integer
        Get
            Return 10
        End Get
    End Property
    Public Shared ReadOnly Property DropReason() As Integer
        Get
            Return 12
        End Get
    End Property
    Public Shared ReadOnly Property AttendType() As Integer
        Get
            Return 13
        End Get
    End Property
    Public Shared ReadOnly Property ProgType() As Integer
        Get
            Return 14
        End Get
    End Property
    Public Shared ReadOnly Property FundSources() As Integer
        Get
            Return 15
        End Get
    End Property
    Public Shared ReadOnly Property TransactionCodes() As Integer
        Get
            Return 16
        End Get
    End Property
    Public Shared ReadOnly Property Terms() As Integer
        Get
            Return 17
        End Get
    End Property
    Public Shared ReadOnly Property Degrees() As Integer
        Get
            Return 21
        End Get
    End Property
    Public Shared ReadOnly Property TuitionCategories() As Integer
        Get
            Return 22
        End Get
    End Property
    Public Shared ReadOnly Property IncomeLevel() As Integer
        Get
            Return 23
        End Get
    End Property
    Public Shared ReadOnly Property CredentialLevel() As Integer
        Get
            Return 25
        End Get
    End Property
    Public Shared ReadOnly Property GetCountofReportingAgenciesApplicableToSchool() As Integer
        Get
            ''strREGENT,
            Dim strIPEDS, strNACCAS, strACCSCT, strISIR, strUniTek As String
            Dim intRptAgenciesAppToSchool As Integer = 0

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            strIPEDS = MyAdvAppSettings.AppSettings("IPEDS")
            strNACCAS = MyAdvAppSettings.AppSettings("NACCAS")
            strACCSCT = MyAdvAppSettings.AppSettings("ACCSCT")
            strISIR = MyAdvAppSettings.AppSettings("ISIR")
            strUniTek = "" 'SingletonAppSettings.AppSettings("UniTek")

            'strREGENT = SingletonAppSettings.AppSettings("REGENTAGENCY")

            If strIPEDS.ToLower = "yes" Then
                intRptAgenciesAppToSchool += 1
            End If

            If strNACCAS.ToLower = "yes" Then
                intRptAgenciesAppToSchool += 1
            End If

            If strACCSCT.ToLower = "yes" Then
                intRptAgenciesAppToSchool += 1
            End If

            If strISIR.ToLower = "yes" Then
                intRptAgenciesAppToSchool += 1
            End If

            'If strUniTek.ToLower = "yes" Then
            '    intRptAgenciesAppToSchool += 1
            'End If

            'If strREGENT.ToLower = "yes" Then
            '    intRptAgenciesAppToSchool += 1
            'End If

            Return intRptAgenciesAppToSchool
        End Get
    End Property
    Public Shared ReadOnly Property CheckIfReportingAgenciesExistForSchool() As Boolean
        Get
            Dim intRptAgenciesAppToSchool As Integer = GetCountofReportingAgenciesApplicableToSchool()
            If intRptAgenciesAppToSchool >= 1 Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property
    Public Shared ReadOnly Property GetReportingAgenciesApplicableToSchool() As String
        Get
            Dim strIPEDS, strNACCAS, strACCSCT, strISIR, strRegent, strUniTek As String
            Dim strReportingAgencies As String = ""
            Dim strCommaOperator As String = ","

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            strIPEDS = MyAdvAppSettings.AppSettings("IPEDS")
            strNACCAS = MyAdvAppSettings.AppSettings("NACCAS")
            strACCSCT = MyAdvAppSettings.AppSettings("ACCSCT")
            strISIR = MyAdvAppSettings.AppSettings("ISIR")
            strUniTek = ""  'MyAdvAppSettings.AppSettings("Unitek")
            strRegent = MyAdvAppSettings.AppSettings("REGENTAGENCY")

            If strIPEDS.ToLower = "yes" Then
                strReportingAgencies &= "1"
            End If

            If strNACCAS.ToLower = "yes" Then
                If Not (strReportingAgencies.Length = 0) Then
                    strReportingAgencies &= strCommaOperator
                End If
                strReportingAgencies &= "2"
            End If

            If strACCSCT.ToLower = "yes" Then
                If Not (strReportingAgencies.Length = 0) Then
                    strReportingAgencies &= strCommaOperator
                End If
                strReportingAgencies &= "3"
            End If

            If strISIR.ToLower = "yes" Then
                If Not (strReportingAgencies.Length = 0) Then
                    strReportingAgencies &= strCommaOperator
                End If
                strReportingAgencies &= "4"
            End If

            'If strUniTek.ToLower = "yes" Then
            '    If Not (strReportingAgencies.Length = 0) Then
            '        strReportingAgencies &= strCommaOperator
            '    End If
            '    strReportingAgencies &= "6"
            'End If

            'If strRegent.ToLower = "yes" Then
            '    If Not (strReportingAgencies.Length = 0) Then
            '        strReportingAgencies &= strCommaOperator
            '    End If
            '    strReportingAgencies &= "5"
            'End If
            Return strReportingAgencies
        End Get
    End Property
    Public Shared ReadOnly Property GetReportingAgencyNames(ByVal intRptAgencyId As Integer) As String
        Get
            Select Case intRptAgencyId
                Case 1
                    Return "IPEDS"
                Case 2
                    Return "NACCAS"
                Case 3
                    Return "ACCSCT"
                Case 4
                    Return "ISIR"
                    'Case 5
                    '    Return "REGENT"
                Case 6
                    Return "Freedom Code"
            End Select
            Return ""
        End Get
    End Property
    Public Shared ReadOnly Property MaxNoOfGradeBookWeightings() As Integer
        Get
            Return 20
        End Get
    End Property
    Public Shared Function GetCollectionOfDates(ByVal startDate As Date, ByVal endDate As Date, ByVal meetDays As Integer, ByVal selectWeek As Integer) As List(Of Date)
        'instantiate the list to be returned
        Dim listOfDates As List(Of Date) = New List(Of Date)()

        'we must count days from the beggining of a week. Not from the start date
        Dim dayCount As Integer = CType(startDate.DayOfWeek, Integer)

        'loop through out all the date range
        Dim currentDate As Date = startDate
        While currentDate <= endDate
            'check if this date is a "meet date"
            If ConvertDayOfWeek(currentDate.DayOfWeek) And meetDays Then
                Select Case selectWeek

                    Case 0
                        'process all weeks
                        listOfDates.Add(currentDate)
                    Case 1
                        'process even weeks
                        If Decimal.Ceiling((dayCount + 1) / 7) Mod 2 = 1 Then
                            listOfDates.Add(currentDate)
                        End If
                    Case 2
                        'process odd weeks
                        If Decimal.Ceiling((dayCount + 1) / 7) Mod 2 = 0 Then
                            listOfDates.Add(currentDate)
                        End If
                End Select
            End If

            'point to the next day
            currentDate = currentDate.AddDays(1)
            dayCount += 1
        End While

        'return collection of dates
        Return listOfDates

    End Function
    Public Shared Function GetCollectionOfDates_New(ByVal startDate As Date, ByVal endDate As Date, ByVal meetDays As Integer, ByVal selectWeek As Integer) As List(Of Date)
        'instantiate the list to be returned
        Dim listOfDates As List(Of Date) = New List(Of Date)()

        'we must count days from the beggining of a week. Not from the start date
        Dim dayCount As Integer = CType(startDate.DayOfWeek, Integer)

        'loop through out all the date range
        Dim currentDate As Date = startDate
        While currentDate <= endDate
            'check if this date is a "meet date"
            If ConvertDayOfWeek(currentDate.DayOfWeek) And meetDays Then
                Select Case selectWeek

                    Case 0
                        'process all weeks
                        listOfDates.Add(currentDate)
                    Case 1
                        'process even weeks
                        If Decimal.Ceiling((dayCount + 1) / 7) Mod 2 = 1 Then
                            listOfDates.Add(currentDate)
                        End If
                    Case 2
                        'process odd weeks
                        If Decimal.Ceiling((dayCount + 1) / 7) Mod 2 = 0 Then
                            listOfDates.Add(currentDate)
                        End If
                End Select
            End If

            'point to the next day
            currentDate = currentDate.AddDays(1)
            dayCount += 1
        End While

        'return collection of dates
        Return listOfDates

    End Function
    Private Shared Function ConvertDayOfWeek(ByVal dayOfWeek As DayOfWeek) As Integer
        'sunday is 0 in .NET
        If dayOfWeek = 0 Then Return 64
        Return 2 ^ (dayOfWeek - 1)
    End Function
    Public Shared Function GetWorkDaysIdsFromMeetDays(ByVal meetdays As Integer) As String
        If meetdays = 0 Then Return ""
        Dim workDaysIds As String() = {"996150ef-4643-4bce-91d9-042ce9eadd61", "4a36e421-b3f0-475e-918a-09cef282e424", "1d9ecc21-efd2-4cc7-8698-0fd707ce4f6f", "aad38dfb-e850-4754-8c79-2b2f7ae469c3", "16f3957c-91f3-4bdd-9bda-50470ac5d184", "0b59d0a1-3d97-4814-b54b-c4d4e1a60007", "35dda197-9f4d-400f-8d93-dac055bf429f"}
        Dim str As String = ""

        For i As Integer = 0 To 6
            If meetdays >= 2 ^ (6 - i) Then
                str += workDaysIds(6 - i) + ";"
                meetdays -= 2 ^ (6 - i)
            End If
        Next

        'the last item should not have a ";" separator
        Return str.Substring(0, str.Length - 1)
    End Function
    Public Shared ReadOnly Property StringToIndicateInactiveItemsInAdvantageDDLs() As String
        Get
            'Return String.Empty
            Return "(X)"
        End Get
    End Property
    Public Shared Function getStudentBatchFileName() As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Dim strBatchFileName As String = MyAdvAppSettings.AppSettings("batchuploadfolder") + "StudentBatch_" + Format(Date.Now, "MMddyyyy") + ".xml"
        Return strBatchFileName
    End Function
    Public Shared Function getEnforceNumberOfGradingComponents() As String
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings.AppSettings("EnforceNumberOfGradingComponents").Trim.ToString.ToLower
    End Function
End Class
