Public Class NavigationNodeInfo
#Region " Private Variables and Objects"
    Private _strADModified As Boolean
    Private _strARModified As Boolean
    Private _strPLModified As Boolean
    Private _strSAModified As Boolean
    Private _strFACModified As Boolean
    Private _strFinAidModified As Boolean
    Private _strHRModified As Boolean
    Private _strSysModified As Boolean

    Private _strADParentId As String
    Private _strARParentId As String
    Private _strPLParentId As String
    Private _strSAParentId As String
    Private _strFACParentId As String
    Private _strFinAidParentId As String
    Private _strHRParentId As String
    Private _strSysParentId As String
    Private _strResourceId As Integer
    Private _strHierarchyName As String
    Private _strModDate As String
    Private _HierarchyIndex As String

    Private _strADDeleted As Boolean
    Private _strARDeleted As Boolean
    Private _strPLDeleted As Boolean
    Private _strSADeleted As Boolean
    Private _strFACDeleted As Boolean
    Private _strFinAidDeleted As Boolean
    Private _strHRDeleted As Boolean
    Private _strSysDeleted As Boolean


#End Region
#Region " Public Constructors "
    Public Sub New()
        _strADModified = False
        _strARModified = False
        _strPLModified = False
        _strSAModified = False
        _strFACModified = False
        _strFinAidModified = False
        _strHRModified = False
        _strSysModified = False

        _strADParentId = ""
        _strARParentId = ""
        _strPLParentId = ""
        _strSAParentId = ""
        _strFACParentId = ""
        _strFinAidParentId = ""
        _strHRParentId = ""
        _strSysParentId = ""

        _strResourceId = 0
        _strHierarchyName = ""

        _strADDeleted = False
        _strARDeleted = False
        _strPLDeleted = False
        _strSADeleted = False
        _strFACDeleted = False
        _strFinAidDeleted = False
        _strHRDeleted = False
        _strSysDeleted = False

        _HierarchyIndex = ""
    End Sub
#End Region

#Region " Public Property "
    Public Property StrADModified() As Boolean
        Get
            Return _strADModified
        End Get
        Set(ByVal Value As Boolean)
            _strADModified = Value
        End Set
    End Property
    Public Property StrSysModified() As Boolean
        Get
            Return _strSysModified
        End Get
        Set(ByVal Value As Boolean)
            _strSysModified = Value
        End Set
    End Property
    Public Property StrPLModified() As Boolean
        Get
            Return _strPLModified
        End Get
        Set(ByVal Value As Boolean)
            _strPLModified = Value
        End Set
    End Property
    Public Property StrARModified() As Boolean
        Get
            Return _strARModified
        End Get
        Set(ByVal Value As Boolean)
            _strARModified = Value
        End Set
    End Property
    Public Property StrSAModified() As Boolean
        Get
            Return _strSAModified
        End Get
        Set(ByVal Value As Boolean)
            _strSAModified = Value
        End Set
    End Property
    Public Property StrFACModified() As Boolean
        Get
            Return _strFACModified
        End Get
        Set(ByVal Value As Boolean)
            _strFACModified = Value
        End Set
    End Property
    Public Property StrFinAidModified() As Boolean
        Get
            Return _strFinAidModified
        End Get
        Set(ByVal Value As Boolean)
            _strFinAidModified = Value
        End Set
    End Property
    Public Property StrHRModified() As Boolean
        Get
            Return _strHRModified
        End Get
        Set(ByVal Value As Boolean)
            _strHRModified = Value
        End Set
    End Property
    Public Property strModDate() As String
        Get
            Return _strModDate
        End Get
        Set(ByVal Value As String)
            _strModDate = Value
        End Set
    End Property
    Public Property HierarchyIndex() As String
        Get
            Return _HierarchyIndex
        End Get
        Set(ByVal Value As String)
            _HierarchyIndex = Value
        End Set
    End Property
    Public Property strADParentId() As String
        Get
            Return _strADParentId
        End Get
        Set(ByVal Value As String)
            _strADParentId = Value
        End Set
    End Property
    Public Property strSysParentId() As String
        Get
            Return _strSysParentId
        End Get
        Set(ByVal Value As String)
            _strSysParentId = Value
        End Set
    End Property
    Public Property strARParentId() As String
        Get
            Return _strARParentId
        End Get
        Set(ByVal Value As String)
            _strARParentId = Value
        End Set
    End Property
    Public Property strPLParentId() As String
        Get
            Return _strPLParentId
        End Get
        Set(ByVal Value As String)
            _strPLParentId = Value
        End Set
    End Property
    Public Property strFACParentId() As String
        Get
            Return _strFACParentId
        End Get
        Set(ByVal Value As String)
            _strFACParentId = Value
        End Set
    End Property
    Public Property strFinAidParentId() As String
        Get
            Return _strFinAidParentId
        End Get
        Set(ByVal Value As String)
            _strFinAidParentId = Value
        End Set
    End Property
    Public Property strHRParentId() As String
        Get
            Return _strHRParentId
        End Get
        Set(ByVal Value As String)
            _strHRParentId = Value
        End Set
    End Property
    Public Property strSAParentId() As String
        Get
            Return _strSAParentId
        End Get
        Set(ByVal Value As String)
            _strSAParentId = Value
        End Set
    End Property
    Public Property strResourceId() As Integer
        Get
            Return _strResourceId
        End Get
        Set(ByVal Value As Integer)
            _strResourceId = Value
        End Set
    End Property
    Public Property strHierarchyName() As String
        Get
            Return _strHierarchyName
        End Get
        Set(ByVal Value As String)
            _strHierarchyName = Value
        End Set
    End Property
    Public Property StrADDeleted() As Boolean
        Get
            Return _strADDeleted
        End Get
        Set(ByVal Value As Boolean)
            _strADDeleted = Value
        End Set
    End Property
    Public Property StrARDeleted() As Boolean
        Get
            Return _strARDeleted
        End Get
        Set(ByVal Value As Boolean)
            _strARDeleted = Value
        End Set
    End Property
    Public Property StrSysDeleted() As Boolean
        Get
            Return _strSysDeleted
        End Get
        Set(ByVal Value As Boolean)
            _strSysDeleted = Value
        End Set
    End Property
    Public Property StrPLDeleted() As Boolean
        Get
            Return _strPLDeleted
        End Get
        Set(ByVal Value As Boolean)
            _strPLDeleted = Value
        End Set
    End Property
    Public Property StrSADeleted() As Boolean
        Get
            Return _strSADeleted
        End Get
        Set(ByVal Value As Boolean)
            _strSADeleted = Value
        End Set
    End Property
    Public Property StrHRDeleted() As Boolean
        Get
            Return _strHRDeleted
        End Get
        Set(ByVal Value As Boolean)
            _strHRDeleted = Value
        End Set
    End Property
    Public Property StrFACDeleted() As Boolean
        Get
            Return _strFACDeleted
        End Get
        Set(ByVal Value As Boolean)
            _strFACDeleted = Value
        End Set
    End Property
    Public Property StrFinAidDeleted() As Boolean
        Get
            Return _strFinAidDeleted
        End Get
        Set(ByVal Value As Boolean)
            _strFinAidDeleted = Value
        End Set
    End Property
#End Region
End Class
