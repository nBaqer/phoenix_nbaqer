<System.Runtime.InteropServices.GuidAttribute("F448E4CF-12F6-47CE-9A50-3CAA215B8BD5")> Public Class TranscriptInfo
#Region " Private Variables and Objects"
    Private _TotalClasses As Integer
    Private _TotalCreditsAttempted As Decimal
    Private _TotalHoursScheduled As Decimal
    Private _GPAAverageLable As String
    Private _GPAAverageValue As Decimal
    Private _TotalCreditsEarned As Decimal
    Private _TotalHoursEarned As Decimal
    Private _GradePoints As Decimal
    Private _CompletedSAPRequirements As String
    Private _IsMakingSAP As Boolean
    Private _AcademicType As String
    Private _PrgVersionTrackCredits As Boolean
#End Region
#Region " Public Constructors"
    Public Sub New()
        _TotalClasses = 0
        _TotalCreditsAttempted = 0.0
        _TotalHoursScheduled = 0.0
        _GPAAverageLable = "Cumulative GPA:"
        _GPAAverageValue = 0.0
        _TotalCreditsEarned = 0.0
        _TotalHoursEarned = 0.0
        _GradePoints = 0.0
        _CompletedSAPRequirements = String.Empty
        _IsMakingSAP = False
        _AcademicType = String.Empty
        _PrgVersionTrackCredits = False
    End Sub
    Public Sub New(ByVal pTotClasses As Integer _
                  , ByVal pTotCreditAttempted As Decimal _
                  , ByVal pTotHoursScheduled As Decimal _
                  , ByVal pGPAAverageLable As String _
                  , ByVal pGPAAverageValue As Decimal _
                  , ByVal pTotCreditsEarned As Decimal _
                  , ByVal pTotHoursEarned As Decimal _
                  , ByVal pGradePoints As Decimal _
                  , ByVal pIsMakingSAP As Boolean _
                  , ByVal pAcademicType As String _
                  , ByVal pPrgVersionTrackCredits As Boolean _
                  )
        _TotalClasses = pTotClasses
        _TotalCreditsAttempted = pTotCreditAttempted
        _TotalHoursScheduled = pTotHoursScheduled
        _GPAAverageLable = pGPAAverageLable
        _GPAAverageValue = pGPAAverageValue
        _TotalCreditsEarned = pTotCreditsEarned
        _TotalHoursEarned = pTotHoursEarned
        _GradePoints = pGradePoints
        _IsMakingSAP = pIsMakingSAP
        _AcademicType = pAcademicType
        _PrgVersionTrackCredits = pPrgVersionTrackCredits
    End Sub
#End Region
#Region " Public Properties"
    Public Property TotalClasses() As Integer
        Get
            Return _TotalClasses
        End Get
        Set(ByVal value As Integer)
            _TotalClasses = value
        End Set
    End Property
    Public Property TotalCreditsAttempted() As Decimal
        Get
            Return _TotalCreditsAttempted
        End Get
        Set(ByVal value As Decimal)
            _TotalCreditsAttempted = value
        End Set
    End Property
    Public Property TotalHoursScheduled() As Decimal
        Get
            Return _TotalHoursScheduled
        End Get
        Set(ByVal value As Decimal)
            _TotalHoursScheduled = value
        End Set
    End Property
    Public Property GPAAverageLable() As String
        Get
            Return _GPAAverageLable
        End Get
        Set(ByVal value As String)
            _GPAAverageLable = value
        End Set
    End Property
    Public Property GPAAverageValue() As Decimal
        Get
            Return _GPAAverageValue
        End Get
        Set(ByVal value As Decimal)
            _GPAAverageValue = value
        End Set
    End Property
    Public Property TotalCreditsEarned() As Decimal
        Get
            Return _TotalCreditsEarned
        End Get
        Set(ByVal value As Decimal)
            _TotalCreditsEarned = value
        End Set
    End Property
    Public Property TotalHoursEarned() As Decimal
        Get
            Return _TotalHoursEarned
        End Get
        Set(ByVal value As Decimal)
            _TotalHoursEarned = value
        End Set
    End Property
    Public Property GradePoints() As Decimal
        Get
            Return _GradePoints
        End Get
        Set(ByVal value As Decimal)
            _GradePoints = value
        End Set
    End Property
    Public ReadOnly Property CompletedSAPRequirements() As String
        Get
            Return _CompletedSAPRequirements
        End Get
    End Property
    Public Property IsMakingSAP() As Boolean
        Get
            Return _IsMakingSAP
        End Get
        Set(ByVal value As Boolean)
            _IsMakingSAP = value
            If (_IsMakingSAP) Then
                _CompletedSAPRequirements = "Yes"
            Else
                _CompletedSAPRequirements = "No"
            End If
        End Set
    End Property
    Public Property AcademicType() As String
        Get
            Return _AcademicType
        End Get
        Set(ByVal value As String)
            _AcademicType = value
        End Set
    End Property
    Public Property PrgVersionTrackCredits() As Boolean
        Get
            Return _PrgVersionTrackCredits
        End Get
        Set(ByVal value As Boolean)
            _PrgVersionTrackCredits = value

        End Set
    End Property
#End Region
End Class
