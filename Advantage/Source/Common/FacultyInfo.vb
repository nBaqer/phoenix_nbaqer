' ===============================================================================
' FacultyInfo.vb
' Info classes for Faculty
' ===============================================================================
' Copyright (C) 2006, 2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Namespace FA

#Region "Grade Posting Info"
    Public Class GrdPostingsInfo
#Region "Private Variable Declaration"
        Private _isInDB As Boolean
        Private _grdBkResultId As String
        Private _clsSectId As String
        Private _stuEnrollId As String
        Private _grdBkWgtDetailId As String
        Private _score As Decimal
        Private _resNum As Integer
        Private _comments As String
        Private _modUser As String
        Private _modDate As DateTime
        Private _postDate As DateTime
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _grdBkResultId = Guid.NewGuid.ToString
            _modDate = DateTime.MinValue
            _postDate = DateTime.MinValue
        End Sub
#End Region
#Region "Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property GrdBkResultId() As String
            Get
                Return _grdBkResultId
            End Get
            Set(ByVal Value As String)
                _grdBkResultId = Value
            End Set
        End Property
        Public Property ClsSectId() As String
            Get
                Return _clsSectId
            End Get
            Set(ByVal Value As String)
                _clsSectId = Value
            End Set
        End Property
        Public Property StuEnrollId() As String
            Get
                Return _stuEnrollId
            End Get
            Set(ByVal Value As String)
                _stuEnrollId = Value
            End Set
        End Property
        Public Property GrdBkWgtDetailId() As String
            Get
                Return _grdBkWgtDetailId
            End Get
            Set(ByVal Value As String)
                _grdBkWgtDetailId = Value
            End Set
        End Property
        Public Property Score() As Decimal
            Get
                Return _score
            End Get
            Set(ByVal Value As Decimal)
                _score = Value
            End Set
        End Property
        Public Property ResNum() As Integer
            Get
                Return _resNum
            End Get
            Set(ByVal Value As Integer)
                _resNum = Value
            End Set
        End Property
        Public Property Comments() As String
            Get
                Return _comments
            End Get
            Set(ByVal Value As String)
                _comments = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As Date
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
        Public Property PostDate() As Date
            Get
                Return _postDate
            End Get
            Set(ByVal Value As Date)
                _postDate = Value
            End Set
        End Property
#End Region
    End Class
#End Region

End Namespace