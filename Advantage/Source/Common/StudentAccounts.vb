' ===============================================================================
' FAME.AdvantageV1.BusinessEntities
'
' StudentAccounts.vb
'
' Students Accounts Common Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Imports System.ComponentModel

Public Class BankInfo
    '
    '   BankInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _bankId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _name As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _address1 As String
    Private _address2 As String
    Private _city As String
    Private _stateId As String
    Private _state As String
    Private _zip As String
    Private _phone As String
    Private _fax As String
    Private _email As String
    Private _contactFirstName As String
    Private _contactLastName As String
    Private _contactTitle As String
    Private _foreignPhone As Boolean
    Private _foreignFax As Boolean
    Private _modUser As String
    Private _modDate As DateTime
    Private _ForeignZip As Boolean
    Private _OtherState As String
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _bankId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _name = ""
        _campGrpId = Guid.Empty.ToString
        _campGrpDescrip = ""
        _address1 = ""
        _address2 = ""
        _city = ""
        _stateId = Guid.Empty.ToString
        _zip = ""
        _phone = ""
        _fax = ""
        _email = ""
        _contactFirstName = ""
        _contactLastName = ""
        _contactTitle = ""
        _foreignPhone = False
        _foreignFax = False
        _modUser = ""
        _modDate = Date.MinValue
        _ForeignZip = False
        _OtherState = ""
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property BankId() As String
        Get
            Return _bankId
        End Get
        Set(ByVal Value As String)
            _bankId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal Value As String)
            _name = Value
        End Set
    End Property

    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return _address1
        End Get
        Set(ByVal Value As String)
            _address1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return _address2
        End Get
        Set(ByVal Value As String)
            _address2 = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _city
        End Get
        Set(ByVal Value As String)
            _city = Value
        End Set
    End Property
    Public Property StateId() As String
        Get
            Return _stateId
        End Get
        Set(ByVal Value As String)
            _stateId = Value
        End Set
    End Property
    Public Property State() As String
        Get
            Return _state
        End Get
        Set(ByVal Value As String)
            _state = Value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return _zip
        End Get
        Set(ByVal Value As String)
            _zip = Value
        End Set
    End Property
    Public Property Phone() As String
        Get
            Return _phone
        End Get
        Set(ByVal Value As String)
            _phone = Value
        End Set
    End Property
    Public Property Fax() As String
        Get
            Return _fax
        End Get
        Set(ByVal Value As String)
            _fax = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal Value As String)
            _email = Value
        End Set
    End Property
    Public Property ContactFirstName() As String
        Get
            Return _contactFirstName
        End Get
        Set(ByVal Value As String)
            _contactFirstName = Value
        End Set
    End Property
    Public Property ContactLastName() As String
        Get
            Return _contactLastName
        End Get
        Set(ByVal Value As String)
            _contactLastName = Value
        End Set
    End Property
    Public Property ContactTitle() As String
        Get
            Return _contactTitle
        End Get
        Set(ByVal Value As String)
            _contactTitle = Value
        End Set
    End Property
    Public Property OtherState() As String
        Get
            Return _OtherState
        End Get
        Set(ByVal Value As String)
            _OtherState = Value
        End Set
    End Property
    Public Property ForeignPhone() As Boolean
        Get
            Return _foreignPhone
        End Get
        Set(ByVal Value As Boolean)
            _foreignPhone = Value
        End Set
    End Property
    Public Property ForeignZip() As Boolean
        Get
            Return _ForeignZip
        End Get
        Set(ByVal Value As Boolean)
            _ForeignZip = Value
        End Set
    End Property
    Public Property ForeignFax() As Boolean
        Get
            Return _foreignFax
        End Get
        Set(ByVal Value As Boolean)
            _foreignFax = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Class BankAcctInfo
    '
    '   BankAccountInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _bankAcctId As String
    Private _bankId As String
    Private _statusId As String
    Private _status As String
    Private _bankAcctDescrip As String
    Private _bankAcctNumber As String
    Private _bankRoutingNumber As String
    Private _isDefaultBankAcct As Boolean
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _bankAcctId = Guid.NewGuid.ToString
        _bankId = Guid.Empty.ToString
        _statusId = Guid.Empty.ToString
        _status = ""
        _bankAcctDescrip = ""
        _bankAcctNumber = ""
        _bankRoutingNumber = ""
        _isDefaultBankAcct = False
        _campGrpId = Guid.Empty.ToString
        _campGrpDescrip = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property BankAcctId() As String
        Get
            Return _bankAcctId
        End Get
        Set(ByVal Value As String)
            _bankAcctId = Value
        End Set
    End Property
    Public Property BankId() As String
        Get
            Return _bankId
        End Get
        Set(ByVal Value As String)
            _bankId = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property BankAcctDescrip() As String
        Get
            Return _bankAcctDescrip
        End Get
        Set(ByVal Value As String)
            _bankAcctDescrip = Value
        End Set
    End Property
    Public Property BankAcctNumber() As String
        Get
            Return _bankAcctNumber
        End Get
        Set(ByVal Value As String)
            _bankAcctNumber = Value
        End Set
    End Property
    Public Property BankRoutingNumber() As String
        Get
            Return _bankRoutingNumber
        End Get
        Set(ByVal Value As String)
            _bankRoutingNumber = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property IsDefaultBankAcct() As Boolean
        Get
            Return _isDefaultBankAcct
        End Get
        Set(ByVal Value As Boolean)
            _isDefaultBankAcct = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
Public Class AdmissionDepositInfo
    '
    '   AdmissionDepositInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _admDepositId As String
    Private _prospectId As String
    Private _prospectDescrip As String
    Private _stuEnrollId As String
    Private _enrollmentDescrip As String
    Private _admDepositDescrip As String
    Private _paidById As String
    Private _paidByDescrip As String
    Private _depositDate As Date
    Private _amount As Decimal
    Private _paymentType As PaymentType
    Private _checkNumber As String
    Private _creditCardTypeId As String
    Private _creditCardType As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _admDepositId = Guid.NewGuid.ToString
        _prospectId = Guid.Empty.ToString
        _stuEnrollId = Guid.Empty.ToString
        _enrollmentDescrip = ""
        _admDepositDescrip = ""
        _paidById = Guid.Empty.ToString
        _paidByDescrip = ""
        _depositDate = Date.Now.ToString("d")
        _amount = 0.0
        _paymentType = Common.PaymentType.Cash
        _checkNumber = ""
        _creditCardTypeId = Guid.Empty.ToString
        _creditCardType = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property AdmissionDepositId() As String
        Get
            Return _admDepositId
        End Get
        Set(ByVal Value As String)
            _admDepositId = Value
        End Set
    End Property
    Public Property ProspectId() As String
        Get
            Return _prospectId
        End Get
        Set(ByVal Value As String)
            _prospectId = Value
        End Set
    End Property
    Public Property ProspectDescription() As String
        Get
            Return _prospectDescrip
        End Get
        Set(ByVal Value As String)
            _prospectDescrip = Value
        End Set
    End Property
    Public Property StuEnrollId() As String
        Get
            Return _stuEnrollId
        End Get
        Set(ByVal Value As String)
            _stuEnrollId = Value
        End Set
    End Property
    Public Property EnrollmentDescription() As String
        Get
            Return _enrollmentDescrip
        End Get
        Set(ByVal Value As String)
            _enrollmentDescrip = Value
        End Set
    End Property
    Public Property AdmissionDepositDescription() As String
        Get
            Return _admDepositDescrip
        End Get
        Set(ByVal Value As String)
            _admDepositDescrip = Value
        End Set
    End Property

    Public Property PaidById() As String
        Get
            Return _paidById
        End Get
        Set(ByVal Value As String)
            _paidById = Value
        End Set
    End Property
    Public Property PaidByDescription() As String
        Get
            Return _paidByDescrip
        End Get
        Set(ByVal Value As String)
            _paidByDescrip = Value
        End Set
    End Property
    Public Property DepositDate() As Date
        Get
            Return _depositDate
        End Get
        Set(ByVal Value As Date)
            _depositDate = Value
        End Set
    End Property
    Public Property Amount() As Decimal
        Get
            Return _amount
        End Get
        Set(ByVal Value As Decimal)
            _amount = Value
        End Set
    End Property
    Public Property PaymentType() As PaymentType
        Get
            Return _paymentType
        End Get
        Set(ByVal Value As PaymentType)
            _paymentType = Value
        End Set
    End Property
    Public Property CheckNumber() As String
        Get
            Return _checkNumber
        End Get
        Set(ByVal Value As String)
            _checkNumber = Value
        End Set
    End Property
    Public Property CreditCardTypeId() As String
        Get
            Return _creditCardTypeId
        End Get
        Set(ByVal Value As String)
            _creditCardTypeId = Value
        End Set
    End Property
    Public Property CreditCardType() As String
        Get
            Return _creditCardType
        End Get
        Set(ByVal Value As String)
            _creditCardType = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
Public Enum PaymentType
    [Select] = 0
    Cash = 1
    Check = 2
    Credit_Card = 3
    EFT = 4
    Money_Order = 5
    Non_Cash = 6
End Enum
Public Enum RefundType
    ToStudent = 0
    ToFinancialAidSource = 1
End Enum
Public Enum DisbursementType
    [Select] = 0
    Financial_Aid = 1
    Payment_Plan = 2
End Enum
Public Class CreditCardTypeInfo
    '
    '   CreditCardTypeInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _creditCardTypeId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _creditCardTypeId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property CreditCardTypeId() As String
        Get
            Return _creditCardTypeId
        End Get
        Set(ByVal Value As String)
            _creditCardTypeId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Class PaidByInfo
    '
    '   PaidByInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _paidById As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _paidById = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property PaidById() As String
        Get
            Return _paidById
        End Get
        Set(ByVal Value As String)
            _paidById = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
Public Class AcademicYearInfo
    '
    '   AcademicYearInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _AcademicYearId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _IsModule As Boolean
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _AcademicYearId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property AcademicYearId() As String
        Get
            Return _AcademicYearId
        End Get
        Set(ByVal Value As String)
            _AcademicYearId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Class TermInfo
    '
    '   TermInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _termId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _startDate As Date
    Private _endDate As Date
    Private _shiftId As String
    Private _shiftDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _IsModule As Boolean
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _termId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _campGrpId = Guid.Empty.ToString
        _campGrpDescrip = ""
        _startDate = Date.MinValue
        _endDate = Date.MaxValue
        _shiftId = Guid.Empty.ToString
        _shiftDescrip = ""
        _modUser = ""
        _modDate = Date.MinValue
        _ismodule = False
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property TermId() As String
        Get
            Return _termId
        End Get
        Set(ByVal Value As String)
            _termId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property StartDate() As Date
        Get
            Return _startDate
        End Get
        Set(ByVal Value As Date)
            _startDate = Value
        End Set
    End Property
    Public Property EndDate() As Date
        Get
            Return _endDate
        End Get
        Set(ByVal Value As Date)
            _endDate = Value
        End Set
    End Property
    Public Property ShiftId() As String
        Get
            Return _shiftId
        End Get
        Set(ByVal Value As String)
            _shiftId = Value
        End Set
    End Property
    Public Property ShiftDescrip() As String
        Get
            Return _shiftDescrip
        End Get
        Set(ByVal Value As String)
            _shiftDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property IsModule() As Boolean
        Get
            Return _IsModule
        End Get
        Set(ByVal Value As Boolean)
            _IsModule = Value
        End Set
    End Property
#End Region

End Class

Public Class TermEnrollSummary

#Region "Private Variable Declaration"
    Private _tESummId As String
    Private _termId As String
    Private _stuEnrollId As String
    Private _descripXTranscript As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructor"
    Public Sub New()
        _tESummId = Guid.NewGuid.ToString
        _termId = Guid.Empty.ToString
        _stuEnrollId = Guid.Empty.ToString
        _descripXTranscript = String.Empty
        _modUser = String.Empty
        _modDate = Date.MinValue
        End Sub

#End Region

#Region "Class Section Properties"



    Public Property TESummId() As String
        Get
            Return _tESummId
        End Get
        Set(ByVal value As String)
            _tESummId = value
        End Set
    End Property
    Public Property TermId() As String
        Get
            Return _termId
        End Get
        Set(ByVal value As String)
            _termId = value
        End Set
    End Property
    Public Property StuEnrollId() As String
        Get
            Return _stuEnrollId
        End Get
        Set(ByVal value As String)
            _stuEnrollId = value
        End Set
    End Property
    Public Property DescripXTranscript() As String
        Get
            Return _descripXTranscript
        End Get
        Set(ByVal value As String)
            _descripXTranscript = value
        End Set
    End Property

    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property

#End Region

End Class
Public Class TransCodeInfo
    '
    '   TransCodeInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _transCodeId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _transCodeDescrip As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _billTypeId As String
    Private _billTypeDescrip As String
    Private _defEarnings As Boolean
    Private _isInstCharge As Boolean
    Private _is1098T As Boolean
    Private _sysTransCodeId As Integer
    Private _modUser As String
    Private _modDate As DateTime
    Private _nonrefundable As Boolean
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _transCodeId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _transCodeDescrip = ""
        _campGrpId = Guid.Empty.ToString
        _campGrpDescrip = ""
        _billTypeId = Guid.Empty.ToString
        _billTypeDescrip = ""
        _defEarnings = False
        _isInstCharge = False
        _is1098T = False
        _sysTransCodeId = 0
        _modUser = ""
        _modDate = Date.MinValue
        _nonrefundable = False
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property TransCodeId() As String
        Get
            Return _transCodeId
        End Get
        Set(ByVal Value As String)
            _transCodeId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _transCodeDescrip
        End Get
        Set(ByVal Value As String)
            _transCodeDescrip = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property BillTypeId() As String
        Get
            Return _billTypeId
        End Get
        Set(ByVal Value As String)
            _billTypeId = Value
        End Set
    End Property
    Public Property BillTypeDescrip() As String
        Get
            Return _billTypeDescrip
        End Get
        Set(ByVal Value As String)
            _billTypeDescrip = Value
        End Set
    End Property
    Public Property DeferEarnings() As Boolean
        Get
            Return _defEarnings
        End Get
        Set(ByVal Value As Boolean)
            _defEarnings = Value
        End Set
    End Property
    Public Property IsInstCharge() As Boolean
        Get
            Return _isInstCharge
        End Get
        Set(ByVal Value As Boolean)
            _isInstCharge = Value
        End Set
    End Property
    Public Property Is1098T() As Boolean
        Get
            Return _is1098T
        End Get
        Set(ByVal Value As Boolean)
            _is1098T = Value
        End Set
    End Property
    Public Property NonRefundable() As Boolean
        Get
            Return _nonrefundable
        End Get
        Set(ByVal Value As Boolean)
            _nonrefundable = Value
        End Set
    End Property
    Public Property SysTransCodeId() As Integer
        Get
            Return _sysTransCodeId
        End Get
        Set(ByVal Value As Integer)
            _sysTransCodeId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Class TransactionTypeInfo
    '
    '   TransactionTypeInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _transactionTypeId As String
    Private _statusId As String
    Private _status As String
    Private _transactionTypeDescrip As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _xmlGLDistributions As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _transactionTypeId = Guid.NewGuid.ToString
        _statusId = Guid.Empty.ToString
        _status = ""
        _transactionTypeDescrip = ""
        _campGrpId = Guid.Empty.ToString
        _campGrpDescrip = ""
        _xmlGLDistributions = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property transactionTypeId() As String
        Get
            Return _transactionTypeId
        End Get
        Set(ByVal Value As String)
            _transactionTypeId = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _transactionTypeDescrip
        End Get
        Set(ByVal Value As String)
            _transactionTypeDescrip = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property XmlGLDistributions() As String
        Get
            Return _xmlGLDistributions
        End Get
        Set(ByVal Value As String)
            _xmlGLDistributions = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
Public Class CommonTransactionInfo
    '
    '   CommonTransactionInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _commonTransactionId As String
    Private _studentId As String
    Private _studentName As String
    Private _stuEnrollId As String
    Private _enrollmentDescrip As String
    Private _transCodeId As String
    Private _transCodeDescrip As String
    Private _commonTransactionDescrip As String
    Private _commonTransactionDate As Date
    Private _amount As Decimal
    Private _reference As String
    Private _academicYearId As String
    Private _academicYearDescrip As String
    Private _termId As String
    Private _termDescrip As String
    Private _transTypeId As Integer
    Private _transTypeDescrip As String
    Private _isPosted As Boolean
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _commonTransactionId = Guid.NewGuid.ToString
        _studentId = Guid.Empty.ToString
        _studentName = ""
        _stuEnrollId = Guid.Empty.ToString
        _enrollmentDescrip = ""
        _commonTransactionDescrip = ""
        _transCodeId = Guid.Empty.ToString
        _transCodeDescrip = ""
        _commonTransactionDate = Date.Now.ToString("d")
        _amount = 0.0
        _reference = ""
        _academicYearId = Guid.Empty.ToString
        _academicYearDescrip = ""
        _termId = Guid.Empty.ToString
        _termDescrip = ""
        _isPosted = False
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property CommonTransactionId() As String
        Get
            Return _commonTransactionId
        End Get
        Set(ByVal Value As String)
            _commonTransactionId = Value
        End Set
    End Property
    Public Property StudentId() As String
        Get
            Return _studentId
        End Get
        Set(ByVal Value As String)
            _studentId = Value
        End Set
    End Property
    Public Property StudentName() As String
        Get
            Return _studentName
        End Get
        Set(ByVal Value As String)
            _studentName = Value
        End Set
    End Property
    Public Property StuEnrollId() As String
        Get
            Return _stuEnrollId
        End Get
        Set(ByVal Value As String)
            _stuEnrollId = Value
        End Set
    End Property
    Public Property EnrollmentDescription() As String
        Get
            Return _enrollmentDescrip
        End Get
        Set(ByVal Value As String)
            _enrollmentDescrip = Value
        End Set
    End Property
    Public Property TransCodeId() As String
        Get
            Return _transCodeId
        End Get
        Set(ByVal Value As String)
            _transCodeId = Value
        End Set
    End Property
    Public Property TransCodeDescription() As String
        Get
            Return _transCodeDescrip
        End Get
        Set(ByVal Value As String)
            _transCodeDescrip = Value
        End Set
    End Property
    Public Property CommonTransactionDescription() As String
        Get
            Return _commonTransactionDescrip
        End Get
        Set(ByVal Value As String)
            _commonTransactionDescrip = Value
        End Set
    End Property
    Public Property CommonTransactionDate() As Date
        Get
            Return _commonTransactionDate
        End Get
        Set(ByVal Value As Date)
            _commonTransactionDate = Value
        End Set
    End Property
    Public Property Amount() As Decimal
        Get
            Return _amount
        End Get
        Set(ByVal Value As Decimal)
            _amount = Value
        End Set
    End Property
    Public Property Reference() As String
        Get
            Return _reference
        End Get
        Set(ByVal Value As String)
            _reference = Value
        End Set
    End Property
    Public Property AcademicYearId() As String
        Get
            Return _academicYearId
        End Get
        Set(ByVal Value As String)
            _academicYearId = Value
        End Set
    End Property
    Public Property AcademicYearDescrip() As String
        Get
            Return _academicYearDescrip
        End Get
        Set(ByVal Value As String)
            _academicYearDescrip = Value
        End Set
    End Property
    Public Property TermId() As String
        Get
            Return _termId
        End Get
        Set(ByVal Value As String)
            _termId = Value
        End Set
    End Property
    Public Property TermDescrip() As String
        Get
            Return _termDescrip
        End Get
        Set(ByVal Value As String)
            _termDescrip = Value
        End Set
    End Property
    Public Property TransTypeId() As Int16
        Get
            Return _transTypeId
        End Get
        Set(ByVal Value As Int16)
            _transTypeId = Value
        End Set
    End Property
    Public ReadOnly Property TransTypeDescrip() As String
        Get
            If _transTypeId = 0 Then
                Return "Charge"
            Else
                Return "Adjustment"
            End If
        End Get
    End Property
    Public Property IsPosted() As Boolean
        Get
            Return _isPosted
        End Get
        Set(ByVal Value As Boolean)
            _isPosted = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
Public Class PostChargeInfo
    '
    '   PostChargeInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _postChargeId As String
    Private _studentId As String
    Private _studentName As String
    Private _stuEnrollId As String
    Private _campusId As String
    Private _enrollmentDescrip As String
    Private _transCodeId As String
    Private _transCodeDescrip As String
    Private _postChargeDescrip As String
    Private _postChargeDate As Date
    Private _amount As Decimal
    Private _reference As String
    Private _academicYearId As String
    Private _academicYearDescrip As String
    Private _termId As String
    Private _termDescrip As String
    Private _earliestAllowedDate As Date
    Private _latestAllowedDate As Date
    Private _transTypeId As Integer
    Private _transTypeDescrip As String
    Private _isPosted As Boolean
    Private _modUser As String
    Private _modDate As DateTime
    Private _isReversal As Boolean
    Private _originalTransactionId As String
    Private _feeLevelId As Integer
    Private _feeId As String
    ''Added by Saraswathi lakshmanan on may 28 2010
    ''Added to know the charge period sequence i.e. academic year 1, etc
    Private _StuEnrollPayPeriodId As String
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _postChargeId = Guid.NewGuid.ToString
        _studentId = Guid.Empty.ToString
        _studentName = ""
        _stuEnrollId = Guid.Empty.ToString
        _campusId = Guid.Empty.ToString
        _enrollmentDescrip = ""
        _postChargeDescrip = ""
        _transCodeId = Guid.Empty.ToString
        _transCodeDescrip = ""
        _postChargeDate = Date.Now.ToString("d")
        _amount = 0.0
        _reference = ""
        _academicYearId = Guid.Empty.ToString
        _academicYearDescrip = ""
        _termId = Guid.Empty.ToString
        _termDescrip = ""
        _earliestAllowedDate = Date.Now.Subtract(New TimeSpan(1826, 0, 0, 0, 0))
        _latestAllowedDate = Date.Now.Add(New TimeSpan(365, 0, 0, 0, 0))
        _transTypeId = 0
        _isPosted = True
        _modUser = ""
        _modDate = Date.MinValue
        _isReversal = False
        _originalTransactionId = Guid.Empty.ToString
        _StuEnrollPayPeriodId = Guid.Empty.ToString
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property PostChargeId() As String
        Get
            Return _postChargeId
        End Get
        Set(ByVal Value As String)
            _postChargeId = Value
        End Set
    End Property
    Public Property StudentId() As String
        Get
            Return _studentId
        End Get
        Set(ByVal Value As String)
            _studentId = Value
        End Set
    End Property
    Public Property StudentName() As String
        Get
            Return _studentName
        End Get
        Set(ByVal Value As String)
            _studentName = Value
        End Set
    End Property
    Public Property StuEnrollId() As String
        Get
            Return _stuEnrollId
        End Get
        Set(ByVal Value As String)
            _stuEnrollId = Value
        End Set
    End Property
    Public Property CampusId() As String
        Get
            Return _campusId
        End Get
        Set(ByVal value As String)
            _campusId = value
        End Set
    End Property
    Public Property EnrollmentDescription() As String
        Get
            Return _enrollmentDescrip
        End Get
        Set(ByVal Value As String)
            _enrollmentDescrip = Value
        End Set
    End Property
    Public Property TransCodeId() As String
        Get
            Return _transCodeId
        End Get
        Set(ByVal Value As String)
            _transCodeId = Value
        End Set
    End Property
    Public Property TransCodeDescription() As String
        Get
            Return _transCodeDescrip
        End Get
        Set(ByVal Value As String)
            _transCodeDescrip = Value
        End Set
    End Property
    Public Property PostChargeDescription() As String
        Get
            Return _postChargeDescrip
        End Get
        Set(ByVal Value As String)
            _postChargeDescrip = Value
        End Set
    End Property
    Public Property PostChargeDate() As Date
        Get
            Return _postChargeDate
        End Get
        Set(ByVal Value As Date)
            _postChargeDate = Value
        End Set
    End Property
    Public Property Amount() As Decimal
        Get
            Return _amount
        End Get
        Set(ByVal Value As Decimal)
            _amount = Value
        End Set
    End Property
    Public Property Reference() As String
        Get
            Return _reference
        End Get
        Set(ByVal Value As String)
            _reference = Value
        End Set
    End Property
    Public Property AcademicYearId() As String
        Get
            Return _academicYearId
        End Get
        Set(ByVal Value As String)
            _academicYearId = Value
        End Set
    End Property
    Public Property AcademicYearDescrip() As String
        Get
            Return _academicYearDescrip
        End Get
        Set(ByVal Value As String)
            _academicYearDescrip = Value
        End Set
    End Property
    Public Property TermId() As String
        Get
            Return _termId
        End Get
        Set(ByVal Value As String)
            _termId = Value
        End Set
    End Property
    Public Property TermDescrip() As String
        Get
            Return _termDescrip
        End Get
        Set(ByVal Value As String)
            _termDescrip = Value
        End Set
    End Property
    Public Property TransTypeId() As Int16
        Get
            Return _transTypeId
        End Get
        Set(ByVal Value As Int16)
            _transTypeId = Value
        End Set
    End Property
    Public ReadOnly Property TransTypeDescrip() As String
        Get
            If _transTypeId = 0 Then
                Return "Charge"
            ElseIf _transTypeId = 2 Then
                Return "Payment"
            Else
                Return "Adjustment"
            End If
        End Get
    End Property
    Public Property EarliestAllowedDate() As Date
        Get
            Return _earliestAllowedDate
        End Get
        Set(ByVal Value As Date)
            _earliestAllowedDate = Value
        End Set
    End Property
    Public Property LatestAllowedDate() As Date
        Get
            Return _latestAllowedDate
        End Get
        Set(ByVal Value As Date)
            _latestAllowedDate = Value
        End Set
    End Property
    Public Property IsPosted() As Boolean
        Get
            Return _isPosted
        End Get
        Set(ByVal Value As Boolean)
            _isPosted = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property IsReversal() As Boolean
        Get
            Return _isReversal
        End Get
        Set(ByVal value As Boolean)
            _isReversal = value
        End Set
    End Property
    Public Property OriginalTransactionId() As String
        Get
            Return _originalTransactionId
        End Get
        Set(ByVal value As String)
            _originalTransactionId = value
        End Set
    End Property
    Public Property FeeLevelId() As Integer
        Get
            Return _feeLevelId
        End Get
        Set(ByVal value As Integer)
            _feeLevelId = value
        End Set
    End Property
    Public Property FeeId() As String
        Get
            Return _feeId
        End Get
        Set(ByVal value As String)
            _feeId = value
        End Set
    End Property
    Public Property StuEnrollPayPeriodId() As String
        Get
            Return _StuEnrollPayPeriodId
        End Get
        Set(ByVal value As String)
            _StuEnrollPayPeriodId = value
        End Set
    End Property
#End Region

End Class
Public Class PostPaymentInfo
    '
    '   PostPaymentInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _postPaymentId As String
    Private _studentId As String
    Private _studentName As String
    Private _stuEnrollId As String
    Private _campusId As String
    Private _enrollmentDescrip As String
    Private _transCodeId As String
    Private _transCodeDescrip As String
    Private _postPaymentDescrip As String
    Private _postPaymentDate As Date
    Private _amount As Decimal
    Private _reference As String
    Private _academicYearId As String
    Private _academicYearDescrip As String
    Private _termId As String
    Private _termDescrip As String
    Private _earliestAllowedDate As Date
    Private _latestAllowedDate As Date
    Private _transTypeId As Integer
    Private _transTypeDescrip As String
    Private _isPosted As Boolean
    Private _paymentTypeId As String
    Private _checkNumber As String
    Private _isDeposited As Boolean
    Private _scheduledPayment As Boolean
    Private _useScheduledPayment As Boolean
    'Private _studentAwardId As String
    'Private _awardDescrip As String
    Private _bankAcctId As String
    Private _bankAcctDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _modUser1 As String
    Private _modDate1 As DateTime
    Private _isReversal As Boolean
    Private _originalTransactionId As String
    Private _feeLevelId As String
    Private _feeId As String
    Private _isVoided As Boolean
    ''Added by Saraswathi lakshmanan on April 16 2010
    ''For mantis case 15222 Post payments
    Private _PaymentCodeID As String
    Private _FundSourceID As String
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _postPaymentId = Guid.NewGuid.ToString
        _studentId = Guid.Empty.ToString
        _studentName = ""
        _stuEnrollId = Guid.Empty.ToString
        _campusId = Guid.Empty.ToString
        _enrollmentDescrip = ""
        _postPaymentDescrip = "Payment"
        _transCodeId = Guid.Empty.ToString
        _transCodeDescrip = ""
        _postPaymentDate = Date.Now.ToString("d")
        _amount = 0.0
        _reference = ""
        _academicYearId = Guid.Empty.ToString
        _academicYearDescrip = ""
        _termId = Guid.Empty.ToString
        _termDescrip = ""
        _earliestAllowedDate = Date.Now.Subtract(New TimeSpan(1826, 0, 0, 0, 0))
        _latestAllowedDate = Date.Now.Add(New TimeSpan(365, 0, 0, 0, 0))
        _transTypeId = 2
        _isPosted = True
        _paymentTypeId = 0
        _checkNumber = ""
        _isDeposited = False
        _scheduledPayment = False
        _useScheduledPayment = False
        '_studentAwardId = Guid.Empty.ToString
        '_awardDescrip = ""
        _bankAcctId = Guid.Empty.ToString
        _bankAcctDescrip = ""
        _modUser = ""
        _modDate = Date.MinValue
        _modUser1 = ""
        _modDate1 = Date.MinValue
        _isReversal = False
        _originalTransactionId = Guid.Empty.ToString
        _isVoided = False
        _feeId = Guid.Empty.ToString
        _PaymentCodeID = Guid.Empty.ToString
        _FundSourceID = Guid.Empty.ToString
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property PostPaymentId() As String
        Get
            Return _postPaymentId
        End Get
        Set(ByVal Value As String)
            _postPaymentId = Value
        End Set
    End Property
    Public Property StudentId() As String
        Get
            Return _studentId
        End Get
        Set(ByVal Value As String)
            _studentId = Value
        End Set
    End Property
    Public Property StudentName() As String
        Get
            Return _studentName
        End Get
        Set(ByVal Value As String)
            _studentName = Value
        End Set
    End Property
    Public Property StuEnrollId() As String
        Get
            Return _stuEnrollId
        End Get
        Set(ByVal Value As String)
            _stuEnrollId = Value
        End Set
    End Property
    Public Property CampusId() As String
        Get
            Return _campusId
        End Get
        Set(ByVal value As String)
            _campusId = value
        End Set
    End Property
    Public Property EnrollmentDescription() As String
        Get
            Return _enrollmentDescrip
        End Get
        Set(ByVal Value As String)
            _enrollmentDescrip = Value
        End Set
    End Property
    Public Property TransCodeId() As String
        Get
            Return _transCodeId
        End Get
        Set(ByVal Value As String)
            _transCodeId = Value
        End Set
    End Property
    Public Property TransCodeDescription() As String
        Get
            Return _transCodeDescrip
        End Get
        Set(ByVal Value As String)
            _transCodeDescrip = Value
        End Set
    End Property
    Public Property PostPaymentDescription() As String
        Get
            Return _postPaymentDescrip
        End Get
        Set(ByVal Value As String)
            _postPaymentDescrip = Value
        End Set
    End Property
    Public Property PostPaymentDate() As Date
        Get
            Return _postPaymentDate
        End Get
        Set(ByVal Value As Date)
            _postPaymentDate = Value
        End Set
    End Property
    Public Property Amount() As Decimal
        Get
            Return _amount
        End Get
        Set(ByVal Value As Decimal)
            _amount = Value
        End Set
    End Property
    Public Property Reference() As String
        Get
            Return _reference
        End Get
        Set(ByVal Value As String)
            _reference = Value
        End Set
    End Property
    Public Property AcademicYearId() As String
        Get
            Return _academicYearId
        End Get
        Set(ByVal Value As String)
            _academicYearId = Value
        End Set
    End Property
    Public Property AcademicYearDescrip() As String
        Get
            Return _academicYearDescrip
        End Get
        Set(ByVal Value As String)
            _academicYearDescrip = Value
        End Set
    End Property
    Public Property TermId() As String
        Get
            Return _termId
        End Get
        Set(ByVal Value As String)
            _termId = Value
        End Set
    End Property
    Public Property TermDescrip() As String
        Get
            Return _termDescrip
        End Get
        Set(ByVal Value As String)
            _termDescrip = Value
        End Set
    End Property
    Public Property EarliestAllowedDate() As Date
        Get
            Return _earliestAllowedDate
        End Get
        Set(ByVal Value As Date)
            _earliestAllowedDate = Value
        End Set
    End Property
    Public Property LatestAllowedDate() As Date
        Get
            Return _latestAllowedDate
        End Get
        Set(ByVal Value As Date)
            _latestAllowedDate = Value
        End Set
    End Property
    Public Property TransTypeId() As Integer
        Get
            Return _transTypeId
        End Get
        Set(ByVal Value As Integer)
            _transTypeId = Value
        End Set
    End Property
    Public ReadOnly Property TransTypeDescrip() As String
        Get
            If _transTypeId = 0 Then
                Return "Charge"
            ElseIf _transTypeId = 2 Then
                Return "Payment"
            Else
                Return "Adjustment"
            End If
        End Get
    End Property
    Public Property IsPosted() As Boolean
        Get
            Return _isPosted
        End Get
        Set(ByVal Value As Boolean)
            _isPosted = Value
        End Set
    End Property
    Public Property PaymentTypeId() As String
        Get
            Return _paymentTypeId
        End Get
        Set(ByVal Value As String)
            _paymentTypeId = Value
        End Set
    End Property
    Public Property IsDeposited() As Boolean
        Get
            Return _isDeposited
        End Get
        Set(ByVal Value As Boolean)
            _isDeposited = Value
        End Set
    End Property
    Public Property CheckNumber() As String
        Get
            Return _checkNumber
        End Get
        Set(ByVal Value As String)
            _checkNumber = Value
        End Set
    End Property
    Public Property ScheduledPayment() As Boolean
        Get
            Return _scheduledPayment
        End Get
        Set(ByVal Value As Boolean)
            _scheduledPayment = Value
        End Set
    End Property
    Public Property UseScheduledPayment() As Boolean
        Get
            Return _useScheduledPayment
        End Get
        Set(ByVal Value As Boolean)
            _useScheduledPayment = Value
        End Set
    End Property
    'Public Property StudentAwardId()
    '    Get
    '        Return _studentAwardId
    '    End Get
    '    Set(ByVal Value)
    '        _studentAwardId = Value
    '    End Set
    'End Property
    'Public Property AwardDescrip()
    '    Get
    '        Return _awardDescrip
    '    End Get
    '    Set(ByVal Value)
    '        _awardDescrip = Value
    '    End Set
    'End Property
    Public Property BankAcctId() As String
        Get
            Return _bankAcctId
        End Get
        Set(ByVal Value As String)
            _bankAcctId = Value
        End Set
    End Property
    Public Property BankAcctDescrip() As String
        Get
            Return _bankAcctDescrip
        End Get
        Set(ByVal Value As String)
            _bankAcctDescrip = Value
        End Set
    End Property
    Public ReadOnly Property UniqueId() As String
        Get
            Return Integer.Parse(_postPaymentId.Substring(0, 6), System.Globalization.NumberStyles.HexNumber).ToString()
        End Get
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property ModUser1() As String
        Get
            Return _modUser1
        End Get
        Set(ByVal Value As String)
            _modUser1 = Value
        End Set
    End Property
    Public Property ModDate1() As Date
        Get
            Return _modDate1
        End Get
        Set(ByVal Value As Date)
            _modDate1 = Value
        End Set
    End Property
    Public Property IsReversal() As Boolean
        Get
            Return _isReversal
        End Get
        Set(ByVal value As Boolean)
            _isReversal = value
        End Set
    End Property
    Public Property OriginalTransactionId() As String
        Get
            Return _originalTransactionId
        End Get
        Set(ByVal value As String)
            _originalTransactionId = value
        End Set
    End Property
    Public Property FeeLevelId() As String
        Get
            Return _feeLevelId
        End Get
        Set(ByVal value As String)
            _feeLevelId = value
        End Set
    End Property
    Public Property FeeId() As String
        Get
            Return _feeId
        End Get
        Set(ByVal value As String)
            _feeId = value
        End Set
    End Property
    Public Property IsVoided() As Boolean
        Get
            Return _isVoided
        End Get
        Set(ByVal Value As Boolean)
            _isVoided = Value
        End Set
    End Property
    Public Property PaymentCodeID() As String
        Get
            Return _PaymentCodeID
        End Get
        Set(ByVal value As String)
            _PaymentCodeID = value
        End Set
    End Property
    Public Property FundSourceID() As String
        Get
            Return _FundSourceID
        End Get
        Set(ByVal value As String)
            _FundSourceID = value
        End Set
    End Property

#End Region
End Class
Public Class PostRefundInfo
    '
    '   PostRefundInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _postRefundId As String
    Private _studentId As String
    Private _studentName As String
    Private _stuEnrollId As String
    Private _campusId As String
    Private _enrollmentDescrip As String
    Private _transCodeId As String
    Private _transCodeDescrip As String
    Private _postRefundDescrip As String
    Private _postRefundDate As Date
    Private _amount As Decimal
    Private _reference As String
    Private _academicYearId As String
    Private _academicYearDescrip As String
    Private _termId As String
    Private _termDescrip As String
    Private _earliestAllowedDate As Date
    Private _latestAllowedDate As Date
    Private _transTypeId As Integer
    Private _transTypeDescrip As String
    Private _isPosted As Boolean
    Private _refundTypeId As String
    Private _isInstCharge As Boolean
    Private _studentAwardId As String
    Private _fundSourceId As String
    Private _awardDescrip As String
    Private _bankAcctId As String
    Private _bankAcctDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _modUser1 As String
    Private _modDate1 As DateTime
    Private _isReversal As Boolean
    Private _originalTransactionId As String
    Private _AwardScheduleId As String
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _postRefundId = Guid.NewGuid.ToString
        _studentId = Guid.Empty.ToString
        _studentName = ""
        _stuEnrollId = Guid.Empty.ToString
        _campusId = Guid.Empty.ToString
        _enrollmentDescrip = ""
        _postRefundDescrip = "Refund"
        _transCodeId = Guid.Empty.ToString
        _transCodeDescrip = ""
        _postRefundDate = Date.Now.ToString("d")
        _amount = 0.0
        _reference = ""
        _academicYearId = Guid.Empty.ToString
        _academicYearDescrip = ""
        _termId = Guid.Empty.ToString
        _termDescrip = ""
        _earliestAllowedDate = Date.Now.Subtract(New TimeSpan(1826, 0, 0, 0, 0))
        _latestAllowedDate = Date.Now.Add(New TimeSpan(365, 0, 0, 0, 0))
        _transTypeId = 1
        _isPosted = False
        _refundTypeId = 0
        _isInstCharge = False
        _studentAwardId = Guid.Empty.ToString
        _fundSourceId = Guid.Empty.ToString
        _awardDescrip = ""
        _bankAcctId = Guid.Empty.ToString
        _bankAcctDescrip = ""
        _modUser = ""
        _modDate = Date.MinValue
        _modUser1 = ""
        _modDate1 = Date.MinValue
        _isReversal = False
        _originalTransactionId = Guid.Empty.ToString
        _AwardScheduleId = Guid.Empty.ToString
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property PostRefundId() As String
        Get
            Return _postRefundId
        End Get
        Set(ByVal Value As String)
            _postRefundId = Value
        End Set
    End Property
    Public Property StudentId() As String
        Get
            Return _studentId
        End Get
        Set(ByVal Value As String)
            _studentId = Value
        End Set
    End Property
    Public Property StudentName() As String
        Get
            Return _studentName
        End Get
        Set(ByVal Value As String)
            _studentName = Value
        End Set
    End Property
    Public Property StuEnrollId() As String
        Get
            Return _stuEnrollId
        End Get
        Set(ByVal Value As String)
            _stuEnrollId = Value
        End Set
    End Property
    Public Property CampusId() As String
        Get
            Return _campusId
        End Get
        Set(ByVal value As String)
            _campusId = value
        End Set
    End Property
    Public Property EnrollmentDescription() As String
        Get
            Return _enrollmentDescrip
        End Get
        Set(ByVal Value As String)
            _enrollmentDescrip = Value
        End Set
    End Property
    Public Property TransCodeId() As String
        Get
            Return _transCodeId
        End Get
        Set(ByVal Value As String)
            _transCodeId = Value
        End Set
    End Property
    Public Property TransCodeDescription() As String
        Get
            Return _transCodeDescrip
        End Get
        Set(ByVal Value As String)
            _transCodeDescrip = Value
        End Set
    End Property
    Public Property PostRefundDescription() As String
        Get
            Return _postRefundDescrip
        End Get
        Set(ByVal Value As String)
            _postRefundDescrip = Value
        End Set
    End Property
    Public Property PostRefundDate() As Date
        Get
            Return _postRefundDate
        End Get
        Set(ByVal Value As Date)
            _postRefundDate = Value
        End Set
    End Property
    Public Property Amount() As Decimal
        Get
            Return _amount
        End Get
        Set(ByVal Value As Decimal)
            _amount = Value
        End Set
    End Property
    Public Property Reference() As String
        Get
            Return _reference
        End Get
        Set(ByVal Value As String)
            _reference = Value
        End Set
    End Property
    Public Property AcademicYearId() As String
        Get
            Return _academicYearId
        End Get
        Set(ByVal Value As String)
            _academicYearId = Value
        End Set
    End Property
    Public Property AcademicYearDescrip() As String
        Get
            Return _academicYearDescrip
        End Get
        Set(ByVal Value As String)
            _academicYearDescrip = Value
        End Set
    End Property
    Public Property TermId() As String
        Get
            Return _termId
        End Get
        Set(ByVal Value As String)
            _termId = Value
        End Set
    End Property
    Public Property TermDescrip() As String
        Get
            Return _termDescrip
        End Get
        Set(ByVal Value As String)
            _termDescrip = Value
        End Set
    End Property
    Public Property EarliestAllowedDate() As Date
        Get
            Return _earliestAllowedDate
        End Get
        Set(ByVal Value As Date)
            _earliestAllowedDate = Value
        End Set
    End Property
    Public Property LatestAllowedDate() As Date
        Get
            Return _latestAllowedDate
        End Get
        Set(ByVal Value As Date)
            _latestAllowedDate = Value
        End Set
    End Property
    Public Property TransTypeId() As Integer
        Get
            Return _transTypeId
        End Get
        Set(ByVal Value As Integer)
            _transTypeId = Value
        End Set
    End Property
    Public ReadOnly Property TransTypeDescrip() As String
        Get
            If _transTypeId = 0 Then
                Return "Charge"
            ElseIf _transTypeId = 2 Then
                Return "Payment"
            Else
                Return "Adjustment"
            End If
        End Get
    End Property
    Public Property IsPosted() As Boolean
        Get
            Return _isPosted
        End Get
        Set(ByVal Value As Boolean)
            _isPosted = Value
        End Set
    End Property
    Public Property RefundTypeId() As String
        Get
            Return _refundTypeId
        End Get
        Set(ByVal Value As String)
            _refundTypeId = Value
        End Set
    End Property
    Public Property IsInstCharge() As Boolean
        Get
            Return _isInstCharge
        End Get
        Set(ByVal Value As Boolean)
            _isInstCharge = Value
        End Set
    End Property
    Public Property StudentAwardId()
        Get
            Return _studentAwardId
        End Get
        Set(ByVal Value)
            _studentAwardId = Value
        End Set
    End Property
    Public Property FundSourceId() As String
        Get
            Return _fundSourceId
        End Get
        Set(ByVal Value As String)
            _fundSourceId = Value
        End Set
    End Property
    Public Property AwardDescrip() As String
        Get
            Return _awardDescrip
        End Get
        Set(ByVal Value As String)
            _awardDescrip = Value
        End Set
    End Property
    Public Property BankAcctId() As String
        Get
            Return _bankAcctId
        End Get
        Set(ByVal Value As String)
            _bankAcctId = Value
        End Set
    End Property
    Public Property BankAcctDescrip() As String
        Get
            Return _bankAcctDescrip
        End Get
        Set(ByVal Value As String)
            _bankAcctDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property ModUser1() As String
        Get
            Return _modUser1
        End Get
        Set(ByVal Value As String)
            _modUser1 = Value
        End Set
    End Property
    Public Property ModDate1() As Date
        Get
            Return _modDate1
        End Get
        Set(ByVal Value As Date)
            _modDate1 = Value
        End Set
    End Property
    Public Property IsReversal() As Boolean
        Get
            Return _isReversal
        End Get
        Set(ByVal value As Boolean)
            _isReversal = value
        End Set
    End Property
    Public Property OriginalTransactionId() As String
        Get
            Return _originalTransactionId
        End Get
        Set(ByVal value As String)
            _originalTransactionId = value
        End Set
    End Property
    Public Property AwardScheduleId() As String
        Get
            Return _AwardScheduleId
        End Get
        Set(ByVal value As String)
            _AwardScheduleId = value
        End Set
    End Property

#End Region
End Class
Public Class FundSourceInfo
    '
    '   FundSourceInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _FundSourceId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _AwardTypeId As Integer
    Private _TitleIV As Integer
    Private _isUsedInFA As Boolean
    Private _AdvFundSourceId As String
    Private _AwardCodeId As String
    Private _AwardSubCodeId As String
    'New Code Added By Vijay Ramteke On June 09, 2010
    Private _cutoffDate As DateTime
    'New Code Added By Vijay Ramteke On June 09, 2010
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _FundSourceId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _modUser = ""
        _modDate = Date.MinValue
        _AwardTypeId = 0
        _isUsedInFA = False
        _AdvFundSourceId = ""
        _AwardCodeId = ""
        _AwardSubCodeId = ""
        'New Code Added By Vijay Ramteke On June 09, 2010
        _cutoffDate = Nothing
        'New Code Added By Vijay Ramteke On June 09, 2010
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property FundSourceId() As String
        Get
            Return _FundSourceId
        End Get
        Set(ByVal Value As String)
            _FundSourceId = Value
        End Set
    End Property
    Public Property AdvFundSourceId() As String
        Get
            Return _AdvFundSourceId
        End Get
        Set(ByVal value As String)
            _AdvFundSourceId = value
        End Set
    End Property
    Public Property AwardCodeId() As String
        Get
            Return _AwardCodeId
        End Get
        Set(ByVal value As String)
            _AwardCodeId = value
        End Set
    End Property
    Public Property AwardSubCodeId() As String
        Get
            Return _AwardSubCodeId
        End Get
        Set(ByVal value As String)
            _AwardSubCodeId = value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property AwardTypeId() As Integer
        Get
            Return _AwardTypeId
        End Get
        Set(ByVal Value As Integer)
            _AwardTypeId = Value
        End Set
    End Property
    Public Property TitleIV() As Integer
        Get
            Return _TitleIV
        End Get
        Set(ByVal Value As Integer)
            _TitleIV = Value
        End Set
    End Property
    Public Property IsUsedInFA() As Boolean
        Get
            Return _isUsedInFA
        End Get
        Set(ByVal Value As Boolean)
            _isUsedInFA = Value
        End Set
    End Property
    'New Code Added By Vijay Ramteke On June 09, 2010
    Public Property CutoffDate() As Date
        Get
            Return _cutoffDate
        End Get
        Set(ByVal Value As Date)
            _cutoffDate = Value
        End Set
    End Property
    'New Code Added By Vijay Ramteke On June 09, 2010
#End Region
End Class
Public Class GLDistributionInfo
    '
    '   GLDistributionInfo Class
    '
#Region " Private Variables and Objects"
    Private _glDistributionId As String
    Private _glDate As Date
    Private _glAmount As Decimal
    Private _glAccountId As String
    Private _glDescrip As String
    Private _viewOrder As Integer
    Private _transactionId As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _glDistributionId = Guid.NewGuid.ToString
        _glDate = Date.MinValue
        _glAmount = 0.0
        _glAccountId = Guid.Empty.ToString
        _glDescrip = ""
        _viewOrder = 0
        _transactionId = Guid.Empty.ToString
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property GLDistributionId() As String
        Get
            Return _glDistributionId
        End Get
        Set(ByVal Value As String)
            _glDistributionId = Value
        End Set
    End Property
    Public Property GLDate() As Date
        Get
            Return _glDate
        End Get
        Set(ByVal Value As Date)
            _glDate = Value
        End Set
    End Property
    Public Property GLAmount() As Decimal
        Get
            Return _glAmount
        End Get
        Set(ByVal Value As Decimal)
            _glAmount = Value
        End Set
    End Property
    Public Property GLAccountId() As String
        Get
            Return _glAccountId
        End Get
        Set(ByVal Value As String)
            _glAccountId = Value
        End Set
    End Property
    Public Property GLDescription() As String
        Get
            Return _glDescrip
        End Get
        Set(ByVal Value As String)
            _glDescrip = Value
        End Set
    End Property
    Public Property ViewOrder() As Integer
        Get
            Return _viewOrder
        End Get
        Set(ByVal Value As Integer)
            _viewOrder = Value
        End Set
    End Property
    Public Property TransactionId() As String
        Get
            Return _transactionId
        End Get
        Set(ByVal Value As String)
            _transactionId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
Public Class GLDistributionEntry
    '
    '   GLDist GLDistributionEntry
    '
#Region " Private Variables and Objects"
    Private _account As String
    Private _date As Date
    Private _descrip As String
    Private _amount As Decimal
#End Region
#Region " Public Constructors "
    Public Sub New()
        _account = ""
        _date = Date.MinValue
        _descrip = ""
        _amount = 0.0
    End Sub
    Public Sub New(ByVal account As String, ByVal glDate As Date, ByVal descrip As String, ByVal amount As Decimal)
        _account = account
        _date = glDate
        _descrip = descrip
        _amount = amount
    End Sub
#End Region
#Region " Public Properties"
    Public Property Account() As String
        Get
            Return _account
        End Get
        Set(ByVal Value As String)
            _account = Value
        End Set
    End Property
    Public Property GLDate() As Date
        Get
            Return _date
        End Get
        Set(ByVal Value As Date)
            _date = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _descrip
        End Get
        Set(ByVal Value As String)
            _descrip = Value
        End Set
    End Property
    Public Property Amount() As Decimal
        Get
            Return _amount
        End Get
        Set(ByVal Value As Decimal)
            _amount = Value
        End Set
    End Property
#End Region
End Class
Public Class GLDistCollection
    '
    '   GLDistCollection Class
    '
    Inherits CollectionBase
#Region " Public Constructors "
    Public Sub New()
        MyBase.New()
    End Sub
#End Region
#Region " Public Properties"
    Public ReadOnly Property Items(ByVal index As Integer) As GLDistributionEntry
        Get
            Return CType(list.Item(index), GLDistributionEntry)
        End Get
    End Property
#End Region
#Region " Public Methods"
    Public Sub Add(ByVal glDist As GLDistributionEntry)
        list.Add(glDist)
    End Sub
#End Region
End Class
Public Class GLDistributionEntryCollection
    '
    '   GLDistribution Class
    '
#Region " Private Variables and Objects"
    Private _glDist As GLDistCollection
#End Region
#Region " Public Constructors "
    Public Sub New()
        _glDist = New GLDistCollection
    End Sub
#End Region
#Region " Public Properties"
    Public ReadOnly Property Count() As Integer
        Get
            Return _glDist.Count
        End Get
    End Property
#End Region
#Region " Public Methods"
    Public Sub Add(ByVal account As String, ByVal glDate As Date, ByVal descrip As String, ByVal amount As Decimal)
        Dim t As New GLDistributionEntry(account, glDate, descrip, amount)
        Me.Add(t)
    End Sub
    Public Sub Add(ByVal glEntry As GLDistributionEntry)
        _glDist.Add(glEntry)
    End Sub
    Public Function Items(ByVal index As Integer) As GLDistributionEntry
        Return CType(_glDist.Items(index), GLDistributionEntry)
    End Function
#End Region
End Class
Public Class GLAccountInfo
    '
    '   GLAccountInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _glAccountId As String
    Private _glAccountCode As String
    Private _statusId As String
    Private _status As String
    Private _glAccountDescription As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _glAccountId = Guid.NewGuid.ToString
        _glAccountCode = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _glAccountDescription = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property GLAccountId() As String
        Get
            Return _glAccountId
        End Get
        Set(ByVal Value As String)
            _glAccountId = Value
        End Set
    End Property
    Public Property GLAccountCode() As String
        Get
            Return _glAccountCode
        End Get
        Set(ByVal Value As String)
            _glAccountCode = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property GLAccountDescription() As String
        Get
            Return _glAccountDescription
        End Get
        Set(ByVal Value As String)
            _glAccountDescription = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
Public Class TuitionEarningsInfo
    '
    '   TuitionEarningsInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _TuitionEarningId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _revenueBasisIdx As Int16
    Private _percentageRangeBasisIdx As Int16
    Private _percentToEarnIdx As Int16
    Private _fullMonthBeforeDay As Int16
    Private _halfMonthBeforeDay As Int16
    Private _isAttendanceRequired As Boolean
    Private _requiredAttendancePercent As Int16
    Private _requiredCumulativeHoursAttended As Int16
    Private _modUser As String
    Private _modDate As DateTime
    Private _TakeHolidays As Boolean
    Private _defRevenueMethod As Boolean
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _TuitionEarningId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _revenueBasisIdx = 0
        _percentageRangeBasisIdx = 0
        _percentToEarnIdx = 0
        _fullMonthBeforeDay = 0
        _halfMonthBeforeDay = 0
        _isAttendanceRequired = False
        _requiredAttendancePercent = 0
        _requiredCumulativeHoursAttended = 0
        _modUser = ""
        _modDate = Date.MinValue
        _TakeHolidays = 0
        _defRevenueMethod = 0
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property TuitionEarningId() As String
        Get
            Return _TuitionEarningId
        End Get
        Set(ByVal Value As String)
            _TuitionEarningId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property RevenueBasis() As Int16
        Get
            Return _revenueBasisIdx
        End Get
        Set(ByVal Value As Int16)
            _revenueBasisIdx = Value
        End Set
    End Property
    Public Property PercentageRangeBasis() As Int16
        Get
            Return _percentageRangeBasisIdx
        End Get
        Set(ByVal Value As Int16)
            _percentageRangeBasisIdx = Value
        End Set
    End Property
    Public Property PercentToEarn() As Int16
        Get
            Return _percentToEarnIdx
        End Get
        Set(ByVal Value As Int16)
            _percentToEarnIdx = Value
        End Set
    End Property
    Public Property FullMonthBeforeDay() As Int16
        Get
            Return _fullMonthBeforeDay
        End Get
        Set(ByVal Value As Int16)
            _fullMonthBeforeDay = Value
        End Set
    End Property
    Public Property HalfMonthBeforeDay() As Int16
        Get
            Return _halfMonthBeforeDay
        End Get
        Set(ByVal Value As Int16)
            _halfMonthBeforeDay = Value
        End Set
    End Property
    Public Property IsAttendanceRequired() As Boolean
        Get
            Return _isAttendanceRequired
        End Get
        Set(ByVal Value As Boolean)
            _isAttendanceRequired = Value
        End Set
    End Property
    Public Property RequiredAttendancePercent() As Int16
        Get
            Return _requiredAttendancePercent
        End Get
        Set(ByVal Value As Int16)
            _requiredAttendancePercent = Value
        End Set
    End Property
    Public Property RequiredCumulativeHoursAttended() As Int16
        Get
            Return _requiredCumulativeHoursAttended
        End Get
        Set(ByVal Value As Int16)
            _requiredCumulativeHoursAttended = Value
        End Set
    End Property

    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    ''Added by Saraswathi On july 1 2009
    Public Property TakeHolidays() As Boolean
        Get
            Return _TakeHolidays
        End Get
        Set(ByVal Value As Boolean)
            _TakeHolidays = Value
        End Set
    End Property
    ''Added by Saraswathi On May 19 2010
    Public Property defRevenueMethod() As Boolean
        Get
            Return _defRevenueMethod
        End Get
        Set(ByVal Value As Boolean)
            _defRevenueMethod = Value
        End Set
    End Property
#End Region

End Class
Public Class TuitionCategoryInfo
    '
    '   TuitionCategoryInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _TuitionCategoryId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _TuitionCategoryId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property TuitionCategoryId() As String
        Get
            Return _TuitionCategoryId
        End Get
        Set(ByVal Value As String)
            _TuitionCategoryId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
<Serializable()>
Public Class BillingMethodInfo
    '
    '   BillingMethodInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _billingMethodInUse As Boolean
    Private _incrementTypeInUse As Boolean
    Private _billingMethodId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _billingMethod As BillingMethod
    Private _autoBill As Boolean
    Private _modUser As String
    Private _modDate As DateTime

#End Region
#Region " Private Methods "

#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _billingMethodInUse = False
        _incrementTypeInUse = False
        _billingMethodId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _billingMethod = FAME.AdvantageV1.Common.BillingMethod.ProgramVersion  'Default Value
        _autoBill = False
        _modUser = ""
        _modDate = DateTime.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property BillingMethodInUse() As Boolean
        Get
            Return _billingMethodInUse
        End Get
        Set(ByVal Value As Boolean)
            _billingMethodInUse = Value
        End Set
    End Property
    Public Property IncrementTypeInUse() As Boolean
        Get
            Return _incrementTypeInUse
        End Get
        Set(ByVal Value As Boolean)
            _incrementTypeInUse = Value
        End Set
    End Property
    Public Property BillingMethodId() As String
        Get
            Return _billingMethodId
        End Get
        Set(ByVal Value As String)
            _billingMethodId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property BillingMethod() As BillingMethod
        Get
            Return _billingMethod
        End Get
        Set(ByVal Value As BillingMethod)
            _billingMethod = Value
        End Set
    End Property
    Public Property AutoBill() As Boolean
        Get
            Return _autoBill
        End Get
        Set(ByVal Value As Boolean)
            _autoBill = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property

#End Region

End Class
Public Enum BillingMethod
    <Description("Program Version")> ProgramVersion = 0
    <Description("Course")> Course = 1
    <Description("Term")> Term = 2
    <Description("Payment Period")> PaymentPeriod = 3
    <Description("Academic Year")> AcademicYear = 4
End Enum
Public Enum IncrementType
    <Description("Not Apply")> NotApply = -1
    <Description("Actual Hours")> ActualHours = 0
    <Description("Scheduled Hours")> ScheduledHours = 1
    <Description("Credits Attempted")> CreditsAttempted = 2
    <Description("Credits Earned")> CreditsEarned = 3
End Enum

<Serializable()>
Public Class IncrementInfo
    '
    '   IncrementInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _IncrementId As String
    Private _billingMethodId As String
    Private _incrementType As IncrementType
    Private _incrementName As String
    Private _effectiveDate As DateTime
    Private _excAbsencesPercent As Decimal
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Private Methods "

#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _IncrementId = Guid.NewGuid.ToString()
        _billingMethodId = Guid.Empty.ToString
        _incrementType = Common.IncrementType.NotApply  'Default Value  it is NULL in DB
        _incrementName = ([Enum].GetName(GetType(IncrementType), _incrementType)).ToString()
        _effectiveDate = DateTime.Now
        _excAbsencesPercent = 0.1 'Defalult value 10% = 0.1
        _modUser = ""
        _modDate = DateTime.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property IncrementId() As String
        Get
            Return _IncrementId
        End Get
        Set(ByVal Value As String)
            _IncrementId = Value
        End Set
    End Property
    Public Property BillingMethodId() As String
        Get
            Return _billingMethodId
        End Get
        Set(ByVal Value As String)
            _billingMethodId = Value
        End Set
    End Property
    Public Property IncrementType() As IncrementType
        Get
            Return _incrementType
        End Get
        Set(ByVal Value As IncrementType)
            _incrementType = Value
            '_incrementName = ([Enum].GetName(GetType(IncrementType), _incrementType)).ToString()
            _incrementName = Utilities.EnumDescription(_incrementType)
        End Set
    End Property
    Public ReadOnly Property IncrementName() As String
        Get
            Return _incrementName
        End Get
    End Property
    Public Property EffectiveDate() As Date
        Get
            Return _effectiveDate
        End Get
        Set(ByVal Value As Date)
            _effectiveDate = Value
        End Set
    End Property
    Public Property ExcAbsencesPercent() As Decimal
        Get
            Return _excAbsencesPercent
        End Get
        Set(ByVal Value As Decimal)
            _excAbsencesPercent = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
<Serializable()>
Public Class TotalIncrementValue
    Private _total As List(Of Double)
    Public Sub New()
        _total = New List(Of Double)
    End Sub
    Public Property Total() As List(Of Double)
        Get
            Return _total
        End Get
        Set(ByVal Value As List(Of Double))
            _total = Value
        End Set
    End Property
End Class

Public Class PmtPeriodInfo
    '
    '   PmtPeriodInfo Class
    '
#Region " Private Variables and Objects"
    Private _pmtPeriodId As String
    Private _incrementId As String
    Private _periodNumber As Integer
    Private _incrementValue As Decimal
    Private _cumulativeValue As Decimal
    Private _chargeAmount As Decimal
    Private _isCharged As Boolean
    Private _modUser As String
    Private _modDate As DateTime
    Private _transactionCodeId As Guid
#End Region
#Region " Private Methods "

#End Region
#Region " Public Constructors "
    Public Sub New()
        _pmtPeriodId = Guid.NewGuid.ToString()
        _incrementId = Guid.Empty.ToString
        _periodNumber = 0
        _incrementValue = 0.0
        _cumulativeValue = 0.0
        _chargeAmount = 0.0
        _isCharged = False
        _modUser = ""
        _modDate = DateTime.MinValue
        _transactionCodeId = Guid.Empty
    End Sub
#End Region
#Region " Public Properties"
    Public Property PmtPeriodId() As String
        Get
            Return _pmtPeriodId
        End Get
        Set(ByVal Value As String)
            _pmtPeriodId = Value
        End Set
    End Property
    Public Property IncrementId() As String
        Get
            Return _incrementId
        End Get
        Set(ByVal Value As String)
            _incrementId = Value
        End Set
    End Property
    Public Property PeriodNumber() As Integer
        Get
            Return _periodNumber
        End Get
        Set(ByVal Value As Integer)
            _periodNumber = Value
        End Set
    End Property
    Public Property IncrementValue() As Decimal
        Get
            Return _incrementValue
        End Get
        Set(ByVal Value As Decimal)
            _incrementValue = Value
        End Set
    End Property
    Public Property CumulativeValue() As Decimal
        Get
            Return _cumulativeValue
        End Get
        Set(ByVal Value As Decimal)
            _cumulativeValue = Value
        End Set
    End Property
    Public Property ChargeAmount() As Decimal
        Get
            Return _chargeAmount
        End Get
        Set(ByVal Value As Decimal)
            _chargeAmount = Value
        End Set
    End Property
    Public Property IsCharged() As Boolean
        Get
            Return _isCharged
        End Get
        Set(ByVal value As Boolean)
            _isCharged = value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property TransactionCodeId() As Guid
        Get
            Return _transactionCodeId
        End Get
        Set(ByVal Value As Guid)
            _transactionCodeId = Value
        End Set
    End Property
#End Region
End Class

Public Class PeriodicFeeInfo
    '
    '   PeriodicFeeInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _PeriodicFeeId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _termId As String
    Private _term As String
    Private _transCodeId As String
    Private _transCode As String
    Private _tuitionCategoryId As String
    Private _tuitionCategory As String
    Private _amount As Decimal
    Private _rateScheduleId As String
    Private _rateSchedule As String
    Private _unitId As UnitType
    Private _unit As String
    Private _applyTo As Integer
    Private _prgVerId As String
    Private _prgVerDescrip As String
    Private _progTypeId As String
    Private _progType As String
    Private _termStartDate As DateTime
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _PeriodicFeeId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _termId = Guid.Empty.ToString
        _term = ""
        _transCodeId = Guid.Empty.ToString
        _transCode = ""
        _tuitionCategoryId = Guid.Empty.ToString
        _tuitionCategory = ""
        _amount = 0.0
        _rateScheduleId = Guid.Empty.ToString
        _rateSchedule = ""
        _unitId = 0
        _applyTo = 0
        _prgVerId = Guid.Empty.ToString
        _prgVerDescrip = ""
        _progTypeId = Guid.Empty.ToString
        _progType = ""
        _termStartDate = Date.MinValue
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property PeriodicFeeId() As String
        Get
            Return _PeriodicFeeId
        End Get
        Set(ByVal Value As String)
            _PeriodicFeeId = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property TermId() As String
        Get
            Return _termId
        End Get
        Set(ByVal Value As String)
            _termId = Value
        End Set
    End Property
    Public Property Term() As String
        Get
            Return _term

        End Get
        Set(ByVal Value As String)
            _term = Value
        End Set
    End Property
    Public Property TransCodeId() As String
        Get
            Return _transCodeId
        End Get
        Set(ByVal Value As String)
            _transCodeId = Value
        End Set
    End Property
    Public Property TransCode() As String
        Get
            Return _transCode
        End Get
        Set(ByVal Value As String)
            _transCode = Value
        End Set
    End Property
    Public Property TuitionCategoryId() As String
        Get
            Return _tuitionCategoryId
        End Get
        Set(ByVal Value As String)
            _tuitionCategoryId = Value
        End Set
    End Property
    Public Property TuitionCategory() As String
        Get
            Return _tuitionCategory
        End Get
        Set(ByVal Value As String)
            _tuitionCategory = Value
        End Set
    End Property
    Public Property Amount() As Decimal
        Get
            Return _amount
        End Get
        Set(ByVal Value As Decimal)
            _amount = Value
        End Set
    End Property
    Public Property RateScheduleId() As String
        Get
            Return _rateScheduleId
        End Get
        Set(ByVal Value As String)
            _rateScheduleId = Value
        End Set
    End Property
    Public Property rateSchedule() As String
        Get
            Return _rateSchedule
        End Get
        Set(ByVal Value As String)
            _rateSchedule = Value
        End Set
    End Property
    Public Property UnitId() As UnitType
        Get
            Return _unitId
        End Get
        Set(ByVal Value As UnitType)
            _unitId = Value
        End Set
    End Property
    Public ReadOnly Property Unit() As String
        Get
            If _unitId = UnitType.CreditHour Then Return "Credit Hour" Else Return "Clock Hour"
        End Get
    End Property
    Public Property ApplyTo() As Integer
        Get
            Return _applyTo
        End Get
        Set(ByVal Value As Integer)
            _applyTo = Value
        End Set
    End Property
    Public Property PrgVerId() As String
        Get
            Return _prgVerId
        End Get
        Set(ByVal Value As String)
            _prgVerId = Value
        End Set
    End Property
    Public Property PrgVerDescrip() As String
        Get
            Return _prgVerDescrip
        End Get
        Set(ByVal Value As String)
            _prgVerDescrip = Value
        End Set
    End Property
    Public Property ProgTypeId() As String
        Get
            Return _progTypeId
        End Get
        Set(ByVal Value As String)
            _progTypeId = Value
        End Set
    End Property
    Public Property ProgType() As String
        Get
            Return _progType
        End Get
        Set(ByVal Value As String)
            _progType = Value
        End Set
    End Property

    Public Property TermStartDate() As Date
        Get
            Return _termStartDate
        End Get
        Set(ByVal value As Date)
            _termStartDate = value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Enum ApplyFeeToPrograms
    All_Programs = 0
    Program_Type = 1
    ProgramVersion = 2
End Enum
Public Enum UnitType
    CreditHour = 0
    ClockHour = 1
End Enum
Public Class RateScheduleInfo
    '
    '   RateScheduleInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _RateScheduleId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _RateScheduleId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property RateScheduleId() As String
        Get
            Return _RateScheduleId
        End Get
        Set(ByVal Value As String)
            _RateScheduleId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Class PrgVersionFeeInfo
    '
    '   PrgVersionFeeInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _prgVerFeeId As String
    Private _statusId As String
    Private _status As String
    Private _prgVerId As String
    Private _programVersion As String
    Private _transCodeId As String
    Private _transCode As String
    Private _tuitionCategoryId As String
    Private _tuitionCategory As String
    Private _amount As Decimal
    Private _rateScheduleId As String
    Private _rateSchedule As String
    Private _unitId As UnitType
    Private _unit As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _prgVerFeeId = Guid.NewGuid.ToString
        _statusId = Guid.Empty.ToString
        _status = ""
        _prgVerId = Guid.Empty.ToString
        _programVersion = ""
        _transCodeId = Guid.Empty.ToString
        _transCode = ""
        _tuitionCategoryId = Guid.Empty.ToString
        _tuitionCategory = ""
        _amount = 0.0
        _rateScheduleId = Guid.Empty.ToString
        _rateSchedule = ""
        _unitId = 0
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property PrgVerFeeId() As String
        Get
            Return _prgVerFeeId
        End Get
        Set(ByVal Value As String)
            _prgVerFeeId = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property PrgVerId() As String
        Get
            Return _prgVerId
        End Get
        Set(ByVal Value As String)
            _prgVerId = Value
        End Set
    End Property
    Public Property ProgramVersion() As String
        Get
            Return _programVersion

        End Get
        Set(ByVal Value As String)
            _programVersion = Value
        End Set
    End Property
    Public Property TransCodeId() As String
        Get
            Return _transCodeId
        End Get
        Set(ByVal Value As String)
            _transCodeId = Value
        End Set
    End Property
    Public Property TransCode() As String
        Get
            Return _transCode
        End Get
        Set(ByVal Value As String)
            _transCode = Value
        End Set
    End Property
    Public Property TuitionCategoryId() As String
        Get
            Return _tuitionCategoryId
        End Get
        Set(ByVal Value As String)
            _tuitionCategoryId = Value
        End Set
    End Property
    Public Property TuitionCategory() As String
        Get
            Return _tuitionCategory
        End Get
        Set(ByVal Value As String)
            _tuitionCategory = Value
        End Set
    End Property
    Public Property Amount() As Decimal
        Get
            Return _amount
        End Get
        Set(ByVal Value As Decimal)
            _amount = Value
        End Set
    End Property
    Public Property RateScheduleId() As String
        Get
            Return _rateScheduleId
        End Get
        Set(ByVal Value As String)
            _rateScheduleId = Value
        End Set
    End Property
    Public Property rateSchedule() As String
        Get
            Return _rateSchedule
        End Get
        Set(ByVal Value As String)
            _rateSchedule = Value
        End Set
    End Property
    Public Property UnitId() As UnitType
        Get
            Return _unitId
        End Get
        Set(ByVal Value As UnitType)
            _unitId = Value
        End Set
    End Property
    Public ReadOnly Property Unit() As String
        Get
            If _unitId = UnitType.CreditHour Then Return "Credit Hour" Else Return "Clock Hour"
        End Get
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
Public Class CourseFeeInfo
    '
    '   CourseFeeInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _courseFeeId As String
    Private _statusId As String
    Private _status As String
    Private _courseId As String
    Private _course As String
    Private _transCodeId As String
    Private _transCode As String
    Private _tuitionCategoryId As String
    Private _tuitionCategory As String
    Private _amount As Decimal
    Private _rateScheduleId As String
    Private _rateSchedule As String
    Private _unitId As UnitType
    Private _unit As String
    Private _startDate As Date
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _courseFeeId = Guid.NewGuid.ToString
        _statusId = Guid.Empty.ToString
        _status = ""
        _courseId = Guid.Empty.ToString
        _course = ""
        _transCodeId = Guid.Empty.ToString
        _transCode = ""
        _tuitionCategoryId = Guid.Empty.ToString
        _tuitionCategory = ""
        _amount = 0.0
        _rateScheduleId = Guid.Empty.ToString
        _rateSchedule = ""
        _unitId = 0
        _startDate = Date.MinValue
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property CourseFeeId() As String
        Get
            Return _courseFeeId
        End Get
        Set(ByVal Value As String)
            _courseFeeId = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property CourseId() As String
        Get
            Return _courseId
        End Get
        Set(ByVal Value As String)
            _courseId = Value
        End Set
    End Property
    Public Property Course() As String
        Get
            Return _course
        End Get
        Set(ByVal Value As String)
            _course = Value
        End Set
    End Property
    Public Property TransCodeId() As String
        Get
            Return _transCodeId
        End Get
        Set(ByVal Value As String)
            _transCodeId = Value
        End Set
    End Property
    Public Property TransCode() As String
        Get
            Return _transCode
        End Get
        Set(ByVal Value As String)
            _transCode = Value
        End Set
    End Property
    Public Property TuitionCategoryId() As String
        Get
            Return _tuitionCategoryId
        End Get
        Set(ByVal Value As String)
            _tuitionCategoryId = Value
        End Set
    End Property
    Public Property TuitionCategory() As String
        Get
            Return _tuitionCategory
        End Get
        Set(ByVal Value As String)
            _tuitionCategory = Value
        End Set
    End Property
    Public Property Amount() As Decimal
        Get
            Return _amount
        End Get
        Set(ByVal Value As Decimal)
            _amount = Value
        End Set
    End Property
    Public Property RateScheduleId() As String
        Get
            Return _rateScheduleId
        End Get
        Set(ByVal Value As String)
            _rateScheduleId = Value
        End Set
    End Property
    Public Property rateSchedule() As String
        Get
            Return _rateSchedule
        End Get
        Set(ByVal Value As String)
            _rateSchedule = Value
        End Set
    End Property
    Public Property UnitId() As UnitType
        Get
            Return _unitId
        End Get
        Set(ByVal Value As UnitType)
            _unitId = Value
        End Set
    End Property
    Public ReadOnly Property Unit() As String
        Get
            If _unitId = UnitType.CreditHour Then Return "Credit Hour" Else Return "Clock Hour"
        End Get
    End Property
    Public Property StartDate() As Date
        Get
            Return _startDate
        End Get
        Set(ByVal Value As Date)
            _startDate = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
Public Class BatchPaymentInfo
    '
    '   BatchPaymentInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _batchPaymentId As String
    Private _batchNumber As String
    Private _fundSourceId As String
    Private _fundSource As String
    Private _batchDate As Date
    Private _batchReference As String
    Private _batchAmount As Decimal
    Private _userId As String
    Private _employee As String
    Private _isPosted As Boolean
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _batchPaymentId = Guid.NewGuid.ToString
        _batchNumber = ""
        _fundSourceId = Guid.Empty.ToString
        _fundSource = ""
        _batchDate = Date.Now
        _batchReference = ""
        _batchAmount = 0.0
        _userId = Guid.Empty.ToString
        _employee = ""
        _isPosted = False
        _modUser = ""
        _modDate = Date.Now
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property BatchPaymentId() As String
        Get
            Return _batchPaymentId
        End Get
        Set(ByVal Value As String)
            _batchPaymentId = Value
        End Set
    End Property
    Public Property BatchNumber() As String
        Get
            Return _batchNumber
        End Get
        Set(ByVal Value As String)
            _batchNumber = Value
        End Set
    End Property
    Public Property FundSourceId() As String
        Get
            Return _fundSourceId
        End Get
        Set(ByVal Value As String)
            _fundSourceId = Value
        End Set
    End Property
    Public Property FundSource() As String
        Get
            Return _fundSource
        End Get
        Set(ByVal Value As String)
            _fundSource = Value
        End Set
    End Property
    Public Property BatchDate() As Date
        Get
            Return _batchDate
        End Get
        Set(ByVal Value As Date)
            _batchDate = Value
        End Set
    End Property
    Public Property BatchReference() As String
        Get
            Return _batchReference
        End Get
        Set(ByVal Value As String)
            _batchReference = Value
        End Set
    End Property
    Public Property BatchAmount() As Decimal
        Get
            Return _batchAmount
        End Get
        Set(ByVal Value As Decimal)
            _batchAmount = Value
        End Set
    End Property
    Public Property UserId() As String
        Get
            Return _userId
        End Get
        Set(ByVal Value As String)
            _userId = Value
        End Set
    End Property
    Public Property Employee() As String
        Get
            Return _employee
        End Get
        Set(ByVal Value As String)
            _employee = Value
        End Set
    End Property
    Public Property IsPosted() As Boolean
        Get
            Return _isPosted
        End Get
        Set(ByVal Value As Boolean)
            _isPosted = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
Public Class ScheduledDisbInfo
    '
    '   ScheduledDisbInfo
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _pmtDisbRelId As String
    Private _transactionId As String
    Private _amount As Decimal
    Private _awardScheduleId As String
    Private _payPlanScheduleId As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _pmtDisbRelId = Guid.NewGuid.ToString
        _transactionId = Guid.Empty.ToString
        _amount = 0.0
        _awardScheduleId = Guid.Empty.ToString
        _payPlanScheduleId = Guid.Empty.ToString
        _modUser = ""
        _modDate = Date.Now
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property PmtDisbRelId() As String
        Get
            Return _pmtDisbRelId
        End Get
        Set(ByVal Value As String)
            _pmtDisbRelId = Value
        End Set
    End Property
    Public Property Amount() As Decimal
        Get
            Return _amount
        End Get
        Set(ByVal value As Decimal)
            _amount = value
        End Set
    End Property
    Public Property TransactionId() As String
        Get
            Return _transactionId
        End Get
        Set(ByVal Value As String)
            _transactionId = Value
        End Set
    End Property
    Public Property AwardScheduleId() As String
        Get
            Return _awardScheduleId
        End Get
        Set(ByVal Value As String)
            _awardScheduleId = Value
        End Set
    End Property
    Public Property PayPlanScheduleId() As String
        Get
            Return _payPlanScheduleId
        End Get
        Set(ByVal Value As String)
            _payPlanScheduleId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class

Public Class ReceiptAddress
#Region " Private Variables and Objects"
    Private _schoolName As String
    Private _schoolAddress1 As String
    Private _schoolAddress2 As String
    Private _city As String
    Private _state As String
    Private _zip As String
    Private _country As String
#End Region

#Region " Public Constructors "
    Public Sub New()
        _schoolName = String.Empty
        _schoolAddress1 = String.Empty
        _schoolAddress2 = String.Empty
        _city = String.Empty
        _state = String.Empty
        _zip = String.Empty
        _country = String.Empty
    End Sub
    Public Sub New(ByVal schoolName As String, ByVal schoolAddress1 As String, ByVal schoolAddress2 As String, ByVal city As String, ByVal state As String, ByVal zip As String, ByVal country As String)

    End Sub
#End Region

#Region " Public Properties "

    Property SchoolName() As String
        Get
            Return _schoolName
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then _schoolName = value Else _schoolName = String.Empty
        End Set
    End Property
    Property SchoolAddress1() As String
        Get
            Return _schoolAddress1
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then _schoolAddress1 = value Else _schoolAddress1 = String.Empty
        End Set
    End Property
    Property SchoolAddress2() As String
        Get
            Return _schoolAddress2
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then _schoolAddress2 = value Else _schoolAddress2 = String.Empty
        End Set
    End Property
    Property City() As String
        Get
            Return _city
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then _city = value Else _city = String.Empty
        End Set
    End Property
    Property State() As String
        Get
            Return _state
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then _state = value Else _state = String.Empty
        End Set
    End Property
    Property Zip() As String
        Get
            Return _zip
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then _zip = value Else _zip = String.Empty
        End Set
    End Property
    Property Country() As String
        Get
            Return _country
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then _country = value Else _country = String.Empty
        End Set
    End Property
#End Region

End Class



