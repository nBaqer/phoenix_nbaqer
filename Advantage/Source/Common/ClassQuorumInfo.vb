﻿''' <summary>
''' USed to get the information about the class quorum
''' </summary>
Public Class ClassQuorumInfo

    ''' <summary>
    ''' Quantity of student registered
    ''' </summary>
    Property RegisterStudent() As Integer

    ''' <summary>
    ''' Maximum students allowed in class
    ''' </summary>
    Property MaxStudentInClass As Integer

    ''' <summary>
    ''' True if exists seat available in the class
    ''' </summary>
    Property IsSeatAvailable As Boolean

    ''' <summary>
    ''' The Class Id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property ClsSectionId As String
End Class
