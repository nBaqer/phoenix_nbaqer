' ===============================================================================
' StudentInfo.vb
' Info classes for Students
' ===============================================================================
' Copyright (C) 2006, 2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Namespace AR

#Region "Student Master Info"

    <Serializable()> _
    Public Class StudentMasterInfo
#Region " Private Variables and Objects"

        Private _isInDB As Boolean
        Private _StudentID As String
        Private _statusId As String
        Private _LastName As String
        Private _FirstName As String
        Private _MiddleName As String
        Private _PrefixId As String
        Private _SuffixId As String
        Private _BirthDate As String
        Private _Age As String
        Private _sponsor As String
        Private _AdmissionsRep As String
        Private _Gender As String
        Private _Race As String
        Private _MaritalStatus As String
        Private _Children As String
        Private _FamilyIncome As String

        Private _Title As String
        Private _Phone As String
        Private _PhoneType As String
        Private _PhoneStatus As String

        Private _WorkEmail As String
        Private _HomeEmail As String

        Private _Address1 As String
        Private _Address2 As String
        Private _City As String
        Private _State As String
        Private _zip As String
        Private _Country As String
        Private _County As String
        Private _AddressStatus As String
        Private _AddressTypeID As String
        Private _LeadId As String

        Private _SourceCategory As String
        Private _SourceDate As String
        Private _SourceType As String

        Private _Area As String
        Private _ExpectedStart As String
        Private _ProgramId As String
        Private _ShiftID As String

        Private _AddressType As String

        Private _Nationality As String
        Private _DriverLicState As String
        Private _Citizen As String
        Private _DriverLicNumber As String
        Private _SSN As String
        Private _AlienNumber As String

        Private _Notes As String
        Private _SourceAdvertisement As String
        Private _AssignmentDate As String
        Private _CampusId As String

        Private _PrgVerId As String
        Private _EnrollmentDate As String
        Private _StartDate As String
        Private _MidPointDate As String
        Private _ExpGradDate As String
        Private _TransferDate As String
        Private _LastDateAttended As String
        Private _StatusCodeId As String
        Private _AcademicAdvisor As String
        Private _GradeLevel As String
        Private _ChargingMethod As String
        Private _EnrollmentId As String
        Private _Education As String
        Private _StudentNumber As String
        Private _YearNumber As Integer
        Private _MonthNumber As Integer
        Private _DateNumber As Integer
        Private _LNameNumber As Integer
        Private _FNameNumber As Integer
        Private _SeqNumber As Integer
        Private _SeqStartNumber As Integer
        Private _FormatType As String
        Private _Objective As String
        Private _ForeignPhone As Integer
        Private _ForeignZip As Integer
        Private _OtherState As String
        Private _StateId As String
        Private _ModDate As DateTime
        Private _StudentGrpId As String
        Private _DependencyTypeId As String
        Private _AdminCriteriaId As String
        Private _GeographicTypeId As String
        Private _HousingId As String

        Private _StateDescrip As String
        Private _AddressTypeDescrip As String
        Private _PrefixDescrip As String
        Private _SuffixDescrip As String
        Private _AdminCriteriaDescrip As String
        Private _GenderDescrip As String
        Private _EthCodeDescrip As String
        Private _MaritalStatusDescrip As String
        Private _FamilyIncomeDescrip As String
        Private _PhoneTypeDescrip As String
        Private _ShiftDescrip As String
        Private _NationalityDescrip As String
        Private _CitizenDescrip As String
        Private _DriverLicenseStateDescrip As String
        Private _CountryDescrip As String
        Private _CountyDescrip As String
        Private _EdLvlDescrip As String
        Private _AddressStatusDescrip As String
        Private _PhoneStatusDescrip As String
        Private _DependencyTypeDescrip As String
        Private _GeographicTypeDescrip As String
        Private _SponsorTypeDescrip As String
        Private _StudentStatusDescrip As String
        Private _HousingDescrip As String
#End Region
#Region " Public Constructor"
        Public Sub New()
            _isInDB = False
            _Education = ""
            _StudentID = Guid.NewGuid.ToString()
            _LastName = ""
            _FirstName = ""
            _MiddleName = ""
            _statusId = Guid.Empty.ToString()
            _PrefixId = Guid.Empty.ToString()
            _SuffixId = Guid.Empty.ToString()
            _Title = Guid.Empty.ToString()
            _BirthDate = ""
            _Age = ""
            _sponsor = Guid.Empty.ToString()
            _AdmissionsRep = Guid.Empty.ToString
            _Gender = Guid.Empty.ToString()
            _Race = Guid.Empty.ToString()
            _MaritalStatus = Guid.Empty.ToString()
            _Children = ""
            _FamilyIncome = Guid.Empty.ToString()
            _LeadId = ""

            _Phone = ""
            _PhoneType = Guid.Empty.ToString()
            _PhoneStatus = Guid.Empty.ToString()

            _WorkEmail = ""
            _HomeEmail = ""

            _Address1 = ""
            _Address2 = ""
            _City = ""
            _State = Guid.Empty.ToString
            _zip = ""
            _Country = ""
            _County = Guid.Empty.ToString
            _AddressStatus = Guid.Empty.ToString
            _AddressType = Guid.Empty.ToString


            _SourceCategory = Guid.Empty.ToString
            _SourceDate = ""
            _SourceType = Guid.Empty.ToString

            _Area = Guid.Empty.ToString
            _ExpectedStart = ""
            _ProgramId = Guid.Empty.ToString
            _ShiftID = Guid.Empty.ToString
            _StudentGrpId = ""

            _Nationality = Guid.Empty.ToString
            _Citizen = Guid.Empty.ToString
            _SSN = ""
            _DriverLicState = Guid.Empty.ToString
            _DriverLicNumber = ""
            _AlienNumber = ""

            _Notes = ""

            _SourceAdvertisement = Guid.Empty.ToString

            _AssignmentDate = ""
            _CampusId = Guid.Empty.ToString

            _PrgVerId = Guid.Empty.ToString
            _StartDate = ""
            _MidPointDate = ""
            _ExpGradDate = ""
            _TransferDate = ""
            _LastDateAttended = ""
            _StatusCodeId = Guid.Empty.ToString
            _AcademicAdvisor = Guid.Empty.ToString
            _GradeLevel = Guid.Empty.ToString
            _ChargingMethod = Guid.Empty.ToString
            _EnrollmentDate = ""
            _EnrollmentId = ""


            _YearNumber = 0
            _MonthNumber = 0
            _DateNumber = 0
            _LNameNumber = 0
            _FNameNumber = 0
            _SeqNumber = 0

            _SeqStartNumber = 0
            _FormatType = ""
            _StudentNumber = ""
            _Objective = ""

            _ForeignPhone = 0
            _ForeignZip = 0
            _OtherState = ""
            _StateId = ""
            _ModDate = Date.MinValue

            _DependencyTypeId = ""
            _GeographicTypeId = ""
            _AdminCriteriaId = ""
            _HousingId = ""

            _StateDescrip = ""
            _AddressTypeDescrip = ""
            _PrefixDescrip = ""
            _SuffixDescrip = ""
            _AdminCriteriaDescrip = ""
            _GenderDescrip = ""
            _EthCodeDescrip = ""
            _MaritalStatusDescrip = ""
            _FamilyIncomeDescrip = ""
            _PhoneTypeDescrip = ""
            _ShiftDescrip = ""
            _NationalityDescrip = ""
            _CitizenDescrip = ""
            _DriverLicenseStateDescrip = ""
            _CountryDescrip = ""
            _CountyDescrip = ""
            _EdLvlDescrip = ""
            _AddressStatusDescrip = ""
            _PhoneStatusDescrip = ""
            _StudentStatusDescrip = ""
            _DependencyTypeDescrip = ""
            _GeographicTypeDescrip = ""
            _SponsorTypeDescrip = ""
            _HousingDescrip = ""
        End Sub
#End Region
#Region "Public Properties"
        Public Property DependencyTypeId() As String
            Get
                Return _DependencyTypeId
            End Get
            Set(ByVal Value As String)
                _DependencyTypeId = Value
            End Set
        End Property
        Public Property GeographicTypeId() As String
            Get
                Return _GeographicTypeId
            End Get
            Set(ByVal Value As String)
                _GeographicTypeId = Value
            End Set
        End Property
        Public Property AdminCriteriaId() As String
            Get
                Return _AdminCriteriaId
            End Get
            Set(ByVal Value As String)
                _AdminCriteriaId = Value
            End Set
        End Property
        Public Property HousingId() As String
            Get
                Return _HousingId
            End Get
            Set(ByVal Value As String)
                _HousingId = Value
            End Set
        End Property
        Public Property StateId() As String
            Get
                Return _StateId
            End Get
            Set(ByVal Value As String)
                _StateId = Value
            End Set
        End Property
        Public Property LeadId() As String
            Get
                Return _LeadId
            End Get
            Set(ByVal Value As String)
                _LeadId = Value
            End Set
        End Property
        Public Property StudentGrpId() As String
            Get
                Return _StudentGrpId
            End Get
            Set(ByVal Value As String)
                _StudentGrpId = Value
            End Set
        End Property
        Public Property ForeignPhone() As Integer
            Get
                Return _ForeignPhone
            End Get
            Set(ByVal Value As Integer)
                _ForeignPhone = Value
            End Set
        End Property
        Public Property ForeignZip() As Integer
            Get
                Return _ForeignZip
            End Get
            Set(ByVal Value As Integer)
                _ForeignZip = Value
            End Set
        End Property
        Public Property OtherState() As String
            Get
                Return _OtherState
            End Get
            Set(ByVal Value As String)
                _OtherState = Value
            End Set
        End Property
        Public Property ModDate() As Date
            Get
                Return _ModDate
            End Get
            Set(ByVal Value As Date)
                _ModDate = Value
            End Set
        End Property
        Public Property YearNumber() As Integer
            Get
                Return _YearNumber
            End Get
            Set(ByVal Value As Integer)
                _YearNumber = Value
            End Set
        End Property
        Public Property MonthNumber() As Integer
            Get
                Return _MonthNumber
            End Get
            Set(ByVal Value As Integer)
                _MonthNumber = Value
            End Set
        End Property
        Public Property DateNumber() As Integer
            Get
                Return _DateNumber
            End Get
            Set(ByVal Value As Integer)
                _DateNumber = Value
            End Set
        End Property
        Public Property LNameNumber() As Integer
            Get
                Return _LNameNumber
            End Get
            Set(ByVal Value As Integer)
                _LNameNumber = Value
            End Set
        End Property
        Public Property FNameNumber() As Integer
            Get
                Return _FNameNumber
            End Get
            Set(ByVal Value As Integer)
                _FNameNumber = Value
            End Set
        End Property
        Public Property SeqNumber() As Integer
            Get
                Return _SeqNumber
            End Get
            Set(ByVal Value As Integer)
                _SeqNumber = Value
            End Set
        End Property
        Public Property SeqStartNumber() As Integer
            Get
                Return _SeqStartNumber
            End Get
            Set(ByVal Value As Integer)
                _SeqStartNumber = Value
            End Set
        End Property
        Public Property FormatType() As String
            Get
                Return _FormatType
            End Get
            Set(ByVal Value As String)
                _FormatType = Value
            End Set
        End Property
        Public Property Objective() As String
            Get
                Return _Objective
            End Get
            Set(ByVal Value As String)
                _Objective = Value
            End Set
        End Property
        Public Property PrgVerId() As String
            Get
                Return _PrgVerId
            End Get
            Set(ByVal Value As String)
                _PrgVerId = Value
            End Set
        End Property
        Public Property StatusCodeId() As String
            Get
                Return _StatusCodeId
            End Get
            Set(ByVal Value As String)
                _StatusCodeId = Value
            End Set
        End Property
        Public Property Education() As String
            Get
                Return _Education
            End Get
            Set(ByVal Value As String)
                _Education = Value
            End Set
        End Property
        Public Property AcademicAdvisor() As String
            Get
                Return _AcademicAdvisor
            End Get
            Set(ByVal Value As String)
                _AcademicAdvisor = Value
            End Set
        End Property
        Public Property StudentNumber() As String
            Get
                Return _StudentNumber
            End Get
            Set(ByVal Value As String)
                _StudentNumber = Value
            End Set
        End Property
        Public Property GradeLevel() As String
            Get
                Return _GradeLevel
            End Get
            Set(ByVal Value As String)
                _GradeLevel = Value
            End Set
        End Property
        Public Property ChargingMethod() As String
            Get
                Return _ChargingMethod
            End Get
            Set(ByVal Value As String)
                _ChargingMethod = Value
            End Set
        End Property
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property StudentID() As String
            Get
                Return _StudentID
            End Get
            Set(ByVal Value As String)
                _StudentID = Value
            End Set
        End Property
        Public Property SourceAdvertisement() As String
            Get
                Return _SourceAdvertisement
            End Get
            Set(ByVal Value As String)
                _SourceAdvertisement = Value
            End Set
        End Property
        Public Property WorkEmail() As String
            Get
                Return _WorkEmail
            End Get
            Set(ByVal Value As String)
                _WorkEmail = Value
            End Set
        End Property
        Public Property HomeEmail() As String
            Get
                Return _HomeEmail
            End Get
            Set(ByVal Value As String)
                _HomeEmail = Value
            End Set
        End Property
        Public Property LastName() As String
            Get
                Return _LastName
            End Get
            Set(ByVal Value As String)
                _LastName = Value
            End Set
        End Property
        Public Property FirstName() As String
            Get
                Return _FirstName
            End Get
            Set(ByVal Value As String)
                _FirstName = Value
            End Set
        End Property
        Public Property MiddleName() As String
            Get
                Return _MiddleName
            End Get
            Set(ByVal Value As String)
                _MiddleName = Value
            End Set
        End Property
        Public Property Status() As String
            Get
                Return _statusId
            End Get
            Set(ByVal Value As String)
                _statusId = Value
            End Set
        End Property

        Public Property Prefix() As String
            Get
                Return _PrefixId
            End Get
            Set(ByVal Value As String)
                _PrefixId = Value
            End Set
        End Property
        Public Property Suffix() As String
            Get
                Return _SuffixId
            End Get
            Set(ByVal Value As String)
                _SuffixId = Value
            End Set
        End Property
        Public Property Title() As String
            Get
                Return _Title
            End Get
            Set(ByVal Value As String)
                _Title = Value
            End Set
        End Property
        Public Property EnrollmentDate() As String
            Get
                Return _EnrollmentDate
            End Get
            Set(ByVal Value As String)
                _EnrollmentDate = Value
            End Set
        End Property
        Public Property StartDate() As String
            Get
                Return _StartDate
            End Get
            Set(ByVal Value As String)
                _StartDate = Value
            End Set
        End Property
        Public Property MidPointDate() As String
            Get
                Return _MidPointDate
            End Get
            Set(ByVal Value As String)
                _MidPointDate = Value
            End Set
        End Property
        Public Property ExpGradDate() As String
            Get
                Return _ExpGradDate
            End Get
            Set(ByVal Value As String)
                _ExpGradDate = Value
            End Set
        End Property
        Public Property TransferDate() As String
            Get
                Return _TransferDate
            End Get
            Set(ByVal Value As String)
                _TransferDate = Value
            End Set
        End Property
        Public Property LastDateAttended() As String
            Get
                Return _LastDateAttended
            End Get
            Set(ByVal Value As String)
                _LastDateAttended = Value
            End Set
        End Property
        Public Property BirthDate() As String
            Get
                Return _BirthDate
            End Get
            Set(ByVal Value As String)
                _BirthDate = Value
            End Set
        End Property
        Public Property Sponsor() As String
            Get
                Return _sponsor
            End Get
            Set(ByVal Value As String)
                _sponsor = Value
            End Set
        End Property
        Public Property AdmissionsRep() As String
            Get
                Return _AdmissionsRep
            End Get
            Set(ByVal Value As String)
                _AdmissionsRep = Value
            End Set
        End Property
        Public Property Gender() As String
            Get
                Return _Gender
            End Get
            Set(ByVal Value As String)
                _Gender = Value
            End Set
        End Property
        Public Property CampusId() As String
            Get
                Return _CampusId
            End Get
            Set(ByVal Value As String)
                _CampusId = Value
            End Set
        End Property
        Public Property Race() As String
            Get
                Return _Race
            End Get
            Set(ByVal Value As String)
                _Race = Value
            End Set
        End Property
        Public Property MaritalStatus() As String
            Get
                Return _MaritalStatus
            End Get
            Set(ByVal Value As String)
                _MaritalStatus = Value
            End Set
        End Property
        Public Property FamilyIncome() As String
            Get
                Return _FamilyIncome
            End Get
            Set(ByVal Value As String)
                _FamilyIncome = Value
            End Set
        End Property
        Public Property Children() As String
            Get
                Return _Children
            End Get
            Set(ByVal Value As String)
                _Children = Value
            End Set
        End Property
        Public Property Phone() As String
            Get
                Return _Phone
            End Get
            Set(ByVal Value As String)
                _Phone = Value
            End Set
        End Property
        Public Property PhoneType() As String
            Get
                Return _PhoneType
            End Get
            Set(ByVal Value As String)
                _PhoneType = Value
            End Set
        End Property
        Public Property PhoneStatus() As String
            Get
                Return _PhoneStatus
            End Get
            Set(ByVal Value As String)
                _PhoneStatus = Value
            End Set
        End Property
        Public Property Age() As String
            Get
                Return _Age
            End Get
            Set(ByVal Value As String)
                _Age = Value
            End Set
        End Property
        Public Property Address1() As String
            Get
                Return _Address1
            End Get
            Set(ByVal Value As String)
                _Address1 = Value
            End Set
        End Property
        Public Property Address2() As String
            Get
                Return _Address2
            End Get
            Set(ByVal Value As String)
                _Address2 = Value
            End Set
        End Property
        Public Property City() As String
            Get
                Return _City
            End Get
            Set(ByVal Value As String)
                _City = Value
            End Set
        End Property
        Public Property State() As String
            Get
                Return _State
            End Get
            Set(ByVal Value As String)
                _State = Value
            End Set
        End Property
        Public Property Zip() As String
            Get
                Return _zip
            End Get
            Set(ByVal Value As String)
                _zip = Value
            End Set
        End Property
        Public Property Country() As String
            Get
                Return _Country
            End Get
            Set(ByVal Value As String)
                _Country = Value
            End Set
        End Property
        Public Property County() As String
            Get
                Return _County
            End Get
            Set(ByVal Value As String)
                _County = Value
            End Set
        End Property
        Public Property AddressType() As String
            Get
                Return _AddressType
            End Get
            Set(ByVal Value As String)
                _AddressType = Value
            End Set
        End Property
        Public Property AddressStatus() As String
            Get
                Return _AddressStatus
            End Get
            Set(ByVal Value As String)
                _AddressStatus = Value
            End Set
        End Property
        Public Property SourceCategory() As String
            Get
                Return _SourceCategory
            End Get
            Set(ByVal Value As String)
                _SourceCategory = Value
            End Set
        End Property
        Public Property SourceDate() As String
            Get
                Return _SourceDate
            End Get
            Set(ByVal Value As String)
                _SourceDate = Value
            End Set
        End Property
        Public Property SourceType() As String
            Get
                Return _SourceType
            End Get
            Set(ByVal Value As String)
                _SourceType = Value
            End Set
        End Property
        Public Property Area() As String
            Get
                Return _Area
            End Get
            Set(ByVal Value As String)
                _Area = Value
            End Set
        End Property
        Public Property ExpectedStart() As String
            Get
                Return _ExpectedStart
            End Get
            Set(ByVal Value As String)
                _ExpectedStart = Value
            End Set
        End Property
        Public Property ProgramID() As String
            Get
                Return _ProgramId
            End Get
            Set(ByVal Value As String)
                _ProgramId = Value
            End Set
        End Property
        Public Property ShiftID() As String
            Get
                Return _ShiftID
            End Get
            Set(ByVal Value As String)
                _ShiftID = Value
            End Set
        End Property
        Public Property Nationality() As String
            Get
                Return _Nationality
            End Get
            Set(ByVal Value As String)
                _Nationality = Value
            End Set
        End Property
        Public Property Citizen() As String
            Get
                Return _Citizen
            End Get
            Set(ByVal Value As String)
                _Citizen = Value
            End Set
        End Property
        Public Property SSN() As String
            Get
                Return _SSN
            End Get
            Set(ByVal Value As String)
                _SSN = Value
            End Set
        End Property
        Public Property DriverLicState() As String
            Get
                Return _DriverLicState
            End Get
            Set(ByVal Value As String)
                _DriverLicState = Value
            End Set
        End Property
        Public Property DriverLicNumber() As String
            Get
                Return _DriverLicNumber
            End Get
            Set(ByVal Value As String)
                _DriverLicNumber = Value
            End Set
        End Property
        Public Property AlienNumber() As String
            Get
                Return _AlienNumber
            End Get
            Set(ByVal Value As String)
                _AlienNumber = Value
            End Set
        End Property
        Public Property Notes() As String
            Get
                Return _Notes
            End Get
            Set(ByVal Value As String)
                _Notes = Value
            End Set
        End Property
        Public Property EnrollmentId() As String
            Get
                Return _EnrollmentId
            End Get
            Set(ByVal Value As String)
                _EnrollmentId = Value
            End Set
        End Property
        Public Property AssignmentDate() As String
            Get
                Return _AssignmentDate
            End Get
            Set(ByVal Value As String)
                _AssignmentDate = Value
            End Set
        End Property
        Public Property StateDescrip() As String
            Get
                Return _StateDescrip
            End Get
            Set(ByVal Value As String)
                _StateDescrip = Value
            End Set
        End Property

        Public Property AddressTypeDescrip() As String
            Get
                Return _AddressTypeDescrip
            End Get
            Set(ByVal Value As String)
                _AddressTypeDescrip = Value
            End Set
        End Property

        Public Property PrefixDescrip() As String
            Get
                Return _PrefixDescrip
            End Get
            Set(ByVal Value As String)
                _PrefixDescrip = Value
            End Set
        End Property

        Public Property SuffixDescrip() As String
            Get
                Return _SuffixDescrip
            End Get
            Set(ByVal Value As String)
                _SuffixDescrip = Value
            End Set
        End Property

        Public Property AdminCriteriaDescrip() As String
            Get
                Return _AdminCriteriaDescrip
            End Get
            Set(ByVal Value As String)
                _AdminCriteriaDescrip = Value
            End Set
        End Property

        Public Property GenderDescrip() As String
            Get
                Return _GenderDescrip
            End Get
            Set(ByVal Value As String)
                _GenderDescrip = Value
            End Set
        End Property
        Public Property EthCodeDescrip() As String
            Get
                Return _EthCodeDescrip
            End Get
            Set(ByVal Value As String)
                _EthCodeDescrip = Value
            End Set
        End Property
        Public Property MaritalStatusDescrip() As String
            Get
                Return _MaritalStatusDescrip
            End Get
            Set(ByVal Value As String)
                _MaritalStatusDescrip = Value
            End Set
        End Property
        Public Property FamilyIncomeDescrip() As String
            Get
                Return _FamilyIncomeDescrip
            End Get
            Set(ByVal Value As String)
                _FamilyIncomeDescrip = Value
            End Set
        End Property
        Public Property PhoneTypeDescrip() As String
            Get
                Return _PhoneTypeDescrip
            End Get
            Set(ByVal Value As String)
                _PhoneTypeDescrip = Value
            End Set
        End Property

        Public Property NationalityDescrip() As String
            Get
                Return _NationalityDescrip
            End Get
            Set(ByVal Value As String)
                _NationalityDescrip = Value
            End Set
        End Property
        Public Property CitizenDescrip() As String
            Get
                Return _CitizenDescrip
            End Get
            Set(ByVal Value As String)
                _CitizenDescrip = Value
            End Set
        End Property
        Public Property DriverLicenseStateDescrip() As String
            Get
                Return _DriverLicenseStateDescrip
            End Get
            Set(ByVal Value As String)
                _DriverLicenseStateDescrip = Value
            End Set
        End Property
        Public Property CountryDescrip() As String
            Get
                Return _CountryDescrip
            End Get
            Set(ByVal Value As String)
                _CountryDescrip = Value
            End Set
        End Property

        Public Property EdLvlDescrip() As String
            Get
                Return _EdLvlDescrip
            End Get
            Set(ByVal Value As String)
                _EdLvlDescrip = Value
            End Set
        End Property
        Public Property AddressStatusDescrip() As String
            Get
                Return _AddressStatusDescrip
            End Get
            Set(ByVal Value As String)
                _AddressStatusDescrip = Value
            End Set
        End Property
        Public Property PhoneStatusDescrip() As String
            Get
                Return _PhoneStatusDescrip
            End Get
            Set(ByVal Value As String)
                _PhoneStatusDescrip = Value
            End Set
        End Property
        Public Property StudentStatusDescrip() As String
            Get
                Return _StudentStatusDescrip
            End Get
            Set(ByVal Value As String)
                _StudentStatusDescrip = Value
            End Set
        End Property
        Public Property DependencyTypeDescrip() As String
            Get
                Return _DependencyTypeDescrip
            End Get
            Set(ByVal Value As String)
                _DependencyTypeDescrip = Value
            End Set
        End Property
        Public Property GeographicTypeDescrip() As String
            Get
                Return _GeographicTypeDescrip
            End Get
            Set(ByVal Value As String)
                _GeographicTypeDescrip = Value
            End Set
        End Property
        Public Property SponsorTypeDescrip() As String
            Get
                Return _SponsorTypeDescrip
            End Get
            Set(ByVal Value As String)
                _SponsorTypeDescrip = Value
            End Set
        End Property
        Public Property HousingDescrip() As String
            Get
                Return _HousingDescrip
            End Get
            Set(ByVal Value As String)
                _HousingDescrip = Value
            End Set
        End Property
#End Region
    End Class

#End Region

#Region "Student Status Codes"
    Public Enum StudentStatusCodes
        Future_Start = 7
        No_Start = 8
        Currently_Attending = 9
        LOA = 10
        Suspended = 11
        Dropped = 12
        'Reentry = 13
        Graduated = 14
        Transfer_Out = 19
        Probation = 20
        Suspension = 21
        Externship = 22
    End Enum
#End Region

#Region "Attendance"
    Public Class ClockHourAttendanceInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _stuEnrollId As String
        Private _scheduleId As String
        Private _recordDate As DateTime
        Private _schedHours As Decimal
        Private _actualHours As Decimal
        Private _modUser As String
        Private _modDate As DateTime
        Private _unittypedescrip As String
        Private _isTardy As Integer

        Private _TotalHoursPresent As Decimal
        Private _TotalHoursSched As Decimal
        Private _TotalHoursAbsent As Decimal
        Private _TotalMakeUpHours As Decimal
        Private _TotalSchedHoursByWeek As Decimal
        Private _LDA As DateTime
        Private _Source As String
        Private _PostByException As String
        Private _AttendancePercentage As Decimal

        Private _actualdaysabsent As Decimal
        Private _daysstudentwastardy As Decimal
        Private _numberoftardiesmaking1absence As Decimal
        Private _actualtardyhours As Decimal
        Private _numberoftardy As Decimal
        Private _adjustedactual As Decimal
        Private _adjustedabsent As Decimal
        Private _adjustedtardy As Decimal
        Private _actualdayspresent As Decimal
        Private _TardyProcessed As Integer
        Private _TransferHours As Decimal

#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _recordDate = Date.MinValue
            _schedHours = 0.0
            _actualHours = 0.0
            _unittypedescrip = ""
            _isTardy = 0
            _TotalHoursPresent = 0.0
            _TotalHoursSched = 0.0
            _TotalHoursAbsent = 0.0
            _TotalMakeUpHours = 0.0
            _TotalSchedHoursByWeek = 0.0
            _LDA = "1/1/1900"
            _Source = ""
            _PostByException = ""
            _AttendancePercentage = 0.0
            _actualdaysabsent = 0.0
            _daysstudentwastardy = 0.0
            _numberoftardiesmaking1absence = 0.0
            _actualtardyhours = 0.0
            _numberoftardy = 0.0
            _adjustedactual = 0.0
            _adjustedabsent = 0.0
            _adjustedtardy = 0.0
            _actualdayspresent = 0.0
            _TardyProcessed = 0
            _TransferHours = 0
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property AttendancePercentage() As Decimal
            Get
                Return _AttendancePercentage
            End Get
            Set(ByVal value As Decimal)
                _AttendancePercentage = value
            End Set
        End Property
        Public Property ActualDaysPresent() As Decimal
            Get
                Return _actualdayspresent
            End Get
            Set(ByVal value As Decimal)
                _actualdayspresent = value
            End Set
        End Property
        Public Property Source() As String
            Get
                Return _Source
            End Get
            Set(ByVal Value As String)
                _Source = Value
            End Set
        End Property
        Public Property PostByException() As String
            Get
                Return _PostByException
            End Get
            Set(ByVal Value As String)
                _PostByException = Value
            End Set
        End Property
        Public Property StuEnrollId() As String
            Get
                Return _stuEnrollId
            End Get
            Set(ByVal Value As String)
                _stuEnrollId = Value
            End Set
        End Property
        Public Property ScheduleId() As String
            Get
                Return _scheduleId
            End Get
            Set(ByVal Value As String)
                _scheduleId = Value
            End Set
        End Property
        Public Property RecordDate() As DateTime
            Get
                Return _recordDate
            End Get
            Set(ByVal Value As DateTime)
                _recordDate = Value
            End Set
        End Property
        Public Property SchedHours() As Decimal
            Get
                Return _schedHours
            End Get
            Set(ByVal Value As Decimal)
                _schedHours = Value
            End Set
        End Property
        Public Property ActualHours() As Decimal
            Get
                Return _actualHours
            End Get
            Set(ByVal Value As Decimal)
                _actualHours = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
        Public Property UnitTypeDescrip() As String
            Get
                Return _unittypedescrip
            End Get
            Set(ByVal Value As String)
                _unittypedescrip = Value
            End Set
        End Property
        Public Property Tardy() As Integer
            Get
                Return _isTardy
            End Get
            Set(ByVal value As Integer)
                _isTardy = value
            End Set
        End Property
        Public Property TotalHoursPresent() As Decimal
            Get
                Return _TotalHoursPresent
            End Get
            Set(ByVal value As Decimal)
                _TotalHoursPresent = value
            End Set
        End Property
        Public Property TotalHoursSched() As Decimal
            Get
                Return _TotalHoursSched
            End Get
            Set(ByVal value As Decimal)
                _TotalHoursSched = value
            End Set
        End Property
        Public Property TotalHoursAbsent() As Decimal
            Get
                Return _TotalHoursAbsent
            End Get
            Set(ByVal value As Decimal)
                _TotalHoursAbsent = value
            End Set
        End Property
        Public Property TotalMakeUpHours() As Decimal
            Get
                Return _TotalMakeUpHours
            End Get
            Set(ByVal value As Decimal)
                _TotalMakeUpHours = value
            End Set
        End Property
        Public Property TotalSchedHoursByWeek() As Decimal
            Get
                Return _TotalSchedHoursByWeek
            End Get
            Set(ByVal value As Decimal)
                _TotalSchedHoursByWeek = value
            End Set
        End Property
        Public Property LDA() As DateTime
            Get
                Return _LDA
            End Get
            Set(ByVal value As DateTime)
                _LDA = value
            End Set
        End Property
        Public Property actualdaysabsent() As Decimal
            Get
                Return _actualdaysabsent
            End Get
            Set(ByVal value As Decimal)
                _actualdaysabsent = value
            End Set
        End Property
        Public Property daysstudentwastardy() As Decimal
            Get
                Return _daysstudentwastardy
            End Get
            Set(ByVal value As Decimal)
                _daysstudentwastardy = value
            End Set
        End Property
        Public Property numberoftardiesmaking1absence() As Decimal
            Get
                Return _numberoftardiesmaking1absence
            End Get
            Set(ByVal value As Decimal)
                _numberoftardiesmaking1absence = value
            End Set
        End Property
        Public Property actualtardyhours() As Decimal
            Get
                Return _actualtardyhours
            End Get
            Set(ByVal value As Decimal)
                _actualtardyhours = value
            End Set
        End Property

        Public Property numberoftardy() As Decimal
            Get
                Return _numberoftardy
            End Get
            Set(ByVal value As Decimal)
                _numberoftardy = value
            End Set
        End Property

        Public Property adjustedactual() As Decimal
            Get
                Return _adjustedactual
            End Get
            Set(ByVal value As Decimal)
                _adjustedactual = value
            End Set
        End Property

        Public Property adjustedabsent() As Decimal
            Get
                Return _adjustedabsent
            End Get
            Set(ByVal value As Decimal)
                _adjustedabsent = value
            End Set
        End Property
        Public Property adjustedtardy() As Decimal
            Get
                Return _adjustedtardy
            End Get
            Set(ByVal value As Decimal)
                _adjustedtardy = value
            End Set
        End Property
        Public Property TardyProcessed() As Integer
            Get
                Return _TardyProcessed
            End Get
            Set(ByVal value As Integer)
                _TardyProcessed = value
            End Set
        End Property
        Public Property TransferHours() As Decimal
            Get
                Return _TransferHours
            End Get
            Set(ByVal value As Decimal)
                _TransferHours = value
            End Set
        End Property

#End Region
    End Class
    Public Class TardyAttendanceUnitInfo
#Region " Private Variables and Objects"
        Private _TardiesMakingAbsence As Integer
        Private _UnitTypeId As String
        Private _PresentUnitAfterTardy As Decimal
        Private _AbsentUnitAfterTardy As Decimal
        Private _TardyUnit As Integer

        Private _actualdaysabsent As Decimal
        Private _daysstudentwastardy As Decimal
        Private _numberoftardiesmaking1absence As Decimal
        Private _actualtardyhours As Decimal
        Private _numberoftardy As Decimal
        Private _adjustedactual As Decimal
        Private _adjustedabsent As Decimal
        Private _adjustedtardy As Decimal
        Private _UseTimeClock As Boolean
#End Region

#Region " Public Constructors "
        Public Sub New()
            _TardiesMakingAbsence = 0
            _UnitTypeId = ""
            _PresentUnitAfterTardy = 0
            _AbsentUnitAfterTardy = 0
            _TardyUnit = 0

            _actualdaysabsent = 0.0
            _daysstudentwastardy = 0.0
            _numberoftardiesmaking1absence = 0.0
            _actualtardyhours = 0.0
            _numberoftardy = 0.0
            _adjustedactual = 0.0
            _adjustedabsent = 0.0
            _adjustedtardy = 0.0
            _UseTimeClock = False
        End Sub
#End Region

#Region " Public Properties"
        Public Property TardiesMakingAbsence() As Integer
            Get
                Return _TardiesMakingAbsence
            End Get
            Set(ByVal Value As Integer)
                _TardiesMakingAbsence = Value
            End Set
        End Property
        ''Modified by Saraswathi lakshmanan
        ''on April 6 2009 
        ''Changed from integer to decim al
        Public Property PresentUnitAfterTardy() As Decimal
            Get
                Return _PresentUnitAfterTardy
            End Get
            Set(ByVal Value As Decimal)
                _PresentUnitAfterTardy = Value
            End Set
        End Property
        ''Modified by Saraswathi lakshmanan
        ''on April 6 2009 
        ''Changed from integer to decim al
        Public Property AbsentUnitAfterTardy() As Decimal
            Get
                Return _AbsentUnitAfterTardy
            End Get
            Set(ByVal Value As Decimal)
                _AbsentUnitAfterTardy = Value
            End Set
        End Property
        Public Property TardyUnit() As Integer
            Get
                Return _TardyUnit
            End Get
            Set(ByVal Value As Integer)
                _TardyUnit = Value
            End Set
        End Property
        Public Property UseTimeClock() As Boolean
            Get
                Return _UseTimeClock
            End Get
            Set(ByVal Value As Boolean)
                _UseTimeClock = Value
            End Set
        End Property
        Public Property UnitTypeId() As String
            Get
                Return _UnitTypeId
            End Get
            Set(ByVal Value As String)
                _UnitTypeId = Value
            End Set
        End Property
        Public Property actualdaysabsent() As Decimal
            Get
                Return _actualdaysabsent
            End Get
            Set(ByVal value As Decimal)
                _actualdaysabsent = value
            End Set
        End Property
        Public Property daysstudentwastardy() As Decimal
            Get
                Return _daysstudentwastardy
            End Get
            Set(ByVal value As Decimal)
                _daysstudentwastardy = value
            End Set
        End Property
        Public Property numberoftardiesmaking1absence() As Decimal
            Get
                Return _numberoftardiesmaking1absence
            End Get
            Set(ByVal value As Decimal)
                _numberoftardiesmaking1absence = value
            End Set
        End Property
        Public Property actualtardyhours() As Decimal
            Get
                Return _actualtardyhours
            End Get
            Set(ByVal value As Decimal)
                _actualtardyhours = value
            End Set
        End Property

        Public Property numberoftardy() As Decimal
            Get
                Return _numberoftardy
            End Get
            Set(ByVal value As Decimal)
                _numberoftardy = value
            End Set
        End Property
        Public Property adjustedactual() As Decimal
            Get
                Return _adjustedactual
            End Get
            Set(ByVal value As Decimal)
                _adjustedactual = value
            End Set
        End Property
        Public Property adjustedabsent() As Decimal
            Get
                Return _adjustedabsent
            End Get
            Set(ByVal value As Decimal)
                _adjustedabsent = value
            End Set
        End Property
        Public Property adjustedtardy() As Decimal
            Get
                Return _adjustedtardy
            End Get
            Set(ByVal value As Decimal)
                _adjustedtardy = value
            End Set
        End Property
#End Region
    End Class
#End Region
End Namespace