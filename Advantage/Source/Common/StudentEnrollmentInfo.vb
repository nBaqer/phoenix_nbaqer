<Serializable()> Public Class StudentEnrollmentInfo

#Region "Constructor"

    Public Sub New()
        StudentID = ""
        ProgramVersionID = (Guid.Empty).ToString()
        AcadamicAdvisor = (Guid.Empty).ToString()
        FaAdvisor = (Guid.Empty).ToString()
        AdmissionsRep = (Guid.Empty).ToString()
        EnrollmentId = ""
        EnrollDate = ""
        LDA = ""
        DateDetermined = ""
        ExpStartDate = ""
        TransferDate = ""
        MidPtDate = ""
        ExpGradDate = ""
        ContractedGradDate = ""
        StartDate = ""
        CampusId = ""
        TuitionCategory = ""
        DropReasonId = ""
        AttendTypeId = ""
        DegCertSeekingId = ""
        ModUser = ""
        ModDate = Date.MinValue
        GrdLvlId = Guid.Empty.ToString
        GradeLevel = ""
        ReenrollDate = ""
        CohortStartDate = ""
        SAPId = ""
        TransferHours = 0
        GraduatedOrReceivedDate = ""
        BadgeNumber = ""
        UseTimeClock = False
        DisableAutoCharge = False
        ThirdPartyContract = False
        DistanceEdStatus = ""
        ProgramVersionTypeId = 0
        LeadID = (Guid.Empty).ToString()
        TotalTransferHours = 0
        TotalTransferHoursFromThisSchool = 0
        TransferHoursFromProgramId= (Guid.Empty).ToString()

       LicensureWrittenAllParts = False
        LicensureLastPartWrittenOn = Nothing
        LicensurePassedAllParts = False
    End Sub

#End Region

#Region "Property"

    ''' <summary>
    ''' Get the id of the Program Version type
    ''' SUVA Property
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ProgramVersionTypeId As Integer

    Public Property EntranceInterviewDate As DateTime?

    Public Property IsDisabled As Integer?

    Public Property IsFirstTimeInSchool As Boolean

    Public Property IsFirstTimePostSecSchool As Boolean

    Public Property StudentID As String

    Public Property UseTimeClock As Boolean

    Public Property ProgramVersionID As String

    Public Property AcadamicAdvisor As String

    Public Property FaAdvisor As String

    Public Property AdmissionsRep As String

    Public Property EnrollmentId As String

    Public Property BillingMethodId As String

    Public Property DisableAutoCharge As Boolean

    Public Property ShiftId As String

    Public Property CampusId As String

    Public Property GrdLvlId As String

    Public Property DropReasonId As String

    Public Property EnrollDate As String

    Public Property LDA As String

    Public Property DateDetermined As String

    Public Property ExpStartDate As String

    Public Property MidPtDate As String

    Public Property ExpGradDate As String

    Public Property TransferDate As String

    Public Property StartDate As String

    Public Property StatusCodeId As String

    Public Property TuitionCategory As String

    Public Property AttendTypeId As String

    Public Property DegCertSeekingId As String

    Public Property ModUser As String

    Public Property ModDate As Date

    Public Property ProgramVersion As String

    Public Property Program As String

    Public Property AcadadvisorText As String

    Public Property FAadvisorText As String

    Public Property AdminRepText As String

    Public Property BillingMethod As String

    Public Property StatusCode As String

    Public Property Shift As String

    Public Property GradeLevel As String

    Public Property Campus As String

    Public Property TuitionCatText As String

    Public Property DropReason As String

    Public Property AttendType As String

    Public Property DegCertSeekingText As String

    Public Property ReenrollDate As String

    Public Property CohortStartDate As String

    Public Property GraduatedOrReceivedDate As String

    Public Property SAPId As String

    Public Property ContractedGradDate As String

    Public Property TransferHours As Decimal
    Public Property TotalTransferHours As Decimal
    Public Property TotalTransferHoursFromThisSchool As Decimal

    Public Property BadgeNumber As String

    Public Property DistanceEdStatus As String

    Public Property StuEnrollmentId As String
    Public Property ScheduleId As String 
    Public Property LeadId As String
    Public Property ThirdPartyContract As Boolean

    Public Property TransferHoursFromProgramId As String

    Public Property TracksTransfer As Boolean


    Public Property LicensureWrittenAllParts As Boolean
    Public Property LicensureLastPartWrittenOn As DateTime?
    Public Property LicensurePassedAllParts As Boolean
#End Region
End Class
