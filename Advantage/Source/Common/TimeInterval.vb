Public Class TimeIntervalInfo
    '
    '   TimeIntervalInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _TimeIntervalId As String
    Private _statusId As String
    Private _status As String
    Private _timeIntervalDescrip As Date
    Private _isBeingUsed As Boolean
    Private _modUser As String
    Private _modDate As DateTime
    Private _campGrpId As String
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _TimeIntervalId = Guid.NewGuid.ToString
        _statusId = Guid.Empty.ToString
        _modUser = ""
        _modDate = Date.MinValue
        _campGrpId = Guid.Empty.ToString
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property TimeIntervalId() As String
        Get
            Return _TimeIntervalId
        End Get
        Set(ByVal Value As String)
            _TimeIntervalId = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property TimeIntervalDescrip() As Date
        Get
            Return _timeIntervalDescrip
        End Get
        Set(ByVal Value As Date)
            _timeIntervalDescrip = Value
        End Set
    End Property
    Public ReadOnly Property TimeInterval() As String
        Get
            Return _timeIntervalDescrip.ToString("hh:mm tt")
        End Get
    End Property
    Public Property IsBeingUsed() As Boolean
        Get
            Return _isBeingUsed
        End Get
        Set(ByVal Value As Boolean)
            _isBeingUsed = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
#End Region
#Region "Public Methods"
    Public Function ConvertTimeDataToDate(ByVal text As String) As Date
        Dim d As Date = Date.Parse(text)
        Return Date.Parse("1899-12-30 " + d.Hour.ToString("00") + ":" + d.Minute.ToString("00"))
    End Function
#End Region


End Class
