Public Class ReqInfo
    Private _MinScore As Decimal
    Private _ReqId As String
    Private _ReqGrpId As String
    Private _ReqTypeId As Integer
    Private _ReqDescription As String


    Public Property ReqId() As String
        Get
            Return _ReqId
        End Get
        Set(ByVal Value As String)
            _ReqId = Value
        End Set
    End Property
    Public Property ReqGrpId() As String
        Get
            Return _ReqGrpId
        End Get
        Set(ByVal Value As String)
            _ReqGrpId = Value
        End Set
    End Property

    Public Property ReqTypeId() As Integer
        Get
            Return _ReqTypeId
        End Get
        Set(ByVal Value As Integer)
            _ReqTypeId = Value
        End Set
    End Property
    Public Property MinScore() As Decimal
        Get
            Return _MinScore
        End Get
        Set(ByVal Value As Decimal)
            _MinScore = Value
        End Set
    End Property

    Public Property ReqDescription() As String
        Get
            Return _ReqDescription
        End Get
        Set(ByVal Value As String)
            _ReqDescription = Value
        End Set
    End Property

End Class
