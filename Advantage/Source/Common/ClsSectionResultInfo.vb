Public Class ClsSectionResultInfo
    '
    '   Class Section Result Info Class
    '
#Region " Private Variables and Objects"
    Private _Exists As Boolean
    Private _NotExists As Boolean
    Private _modDate As DateTime
    Private _errorString As String '(used for UpdateClassSection() only)
#End Region

#Region " Public Properties"

    Public Property Exists() As Boolean
        Get
            Return _Exists
        End Get
        Set(ByVal Value As Boolean)
            _Exists = Value
        End Set
    End Property
    Public Property NotExists() As Boolean
        Get
            Return _NotExists
        End Get
        Set(ByVal Value As Boolean)
            _NotExists = Value
        End Set
    End Property

    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property

    Public Property ErrorString() As String
        Get
            Return _errorString
        End Get
        Set(ByVal Value As String)
            _errorString = Value
        End Set
    End Property

#End Region

End Class
