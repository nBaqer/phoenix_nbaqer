Public Class StuAwardInfo
    '
    '   StuAwardInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _StudentAwardId As String
    Private _stuEnrollId As String
    Private _awardTypeId As String
    Private _awardType As String
    Private _academicYearId As String
    Private _academicYear As String
    Private _lenderId As String
    Private _lender As String
    Private _servicerId As String
    Private _servicer As String
    Private _guarantorId As String
    Private _guarantor As String
    Private _grossAmount As Decimal
    Private _loanFees As Decimal
    Private _netLoanAmount As Decimal
    Private _awardStartDate As String
    Private _awardEndDate As String
    Private _disbursements As Integer
    Private _loanId As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _FAID As String
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _StudentAwardId = Guid.NewGuid.ToString
        _stuEnrollId = Guid.Empty.ToString
        _awardTypeId = Guid.Empty.ToString
        _academicYearId = Guid.Empty.ToString
        _lenderId = Guid.Empty.ToString
        _servicerId = Guid.Empty.ToString
        _guarantorId = Guid.Empty.ToString
        _grossAmount = 0.0
        _loanFees = 0.0
        _netLoanAmount = 0.0
        _awardStartDate = ""
        _awardEndDate = ""
        _disbursements = 0
        _loanId = ""
        _modUser = ""
        _modDate = Date.MinValue
        _FAID = ""
    End Sub
    Public Sub New(ByVal studentId As String)
        Me.New()
        '_isInDB = False
        '_StudentAwardId = Guid.NewGuid.ToString
        '_stuEnrollId = Guid.Empty.ToString
        '_awardTypeId = Guid.Empty.ToString
        '_academicYearId = Guid.Empty.ToString
        '_lenderId = Guid.Empty.ToString
        '_servicerId = Guid.Empty.ToString
        '_guarantorId = Guid.Empty.ToString
        '_grossAmount = 0.0
        '_loanFees = 0.0
        '_netLoanAmount = 0.0
        '_awardStartDate = ""
        '_awardEndDate = ""
        '_disbursements = 0
        '_loanId = ""
        '_modUser = ""
        '_modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property StudentAwardId() As String
        Get
            Return _StudentAwardId
        End Get
        Set(ByVal Value As String)
            _StudentAwardId = Value
        End Set
    End Property
    Public Property StuEnrollId() As String
        Get
            Return _stuEnrollId
        End Get
        Set(ByVal Value As String)
            _stuEnrollId = Value
        End Set
    End Property
    Public Property AwardTypeId() As String
        Get
            Return _awardTypeId
        End Get
        Set(ByVal Value As String)
            _awardTypeId = Value
        End Set
    End Property
    Public Property AwardType() As String
        Get
            Return _awardType
        End Get
        Set(ByVal Value As String)
            _awardType = Value
        End Set
    End Property
    Public Property AcademicYearId() As String
        Get
            Return _academicYearId
        End Get
        Set(ByVal Value As String)
            _academicYearId = Value
        End Set
    End Property
    Public Property AcademicYear() As String
        Get
            Return _academicYear
        End Get
        Set(ByVal Value As String)
            _academicYear = Value
        End Set
    End Property
    Public Property LenderId() As String
        Get
            Return _lenderId
        End Get
        Set(ByVal Value As String)
            _lenderId = Value
        End Set
    End Property
    Public Property Lender() As String
        Get
            Return _lender
        End Get
        Set(ByVal Value As String)
            _lender = Value
        End Set
    End Property
    Public Property ServicerId() As String
        Get
            Return _servicerId
        End Get
        Set(ByVal Value As String)
            _servicerId = Value
        End Set
    End Property
    Public Property Servicer() As String
        Get
            Return _servicer
        End Get
        Set(ByVal Value As String)
            _servicer = Value
        End Set
    End Property
    Public Property GuarantorId() As String
        Get
            Return _guarantorId
        End Get
        Set(ByVal Value As String)
            _guarantorId = Value
        End Set
    End Property
    Public Property Guarantor() As String
        Get
            Return _guarantor
        End Get
        Set(ByVal Value As String)
            _guarantorId = Value
        End Set
    End Property
    Public Property GrossAmount() As Decimal
        Get
            Return _grossAmount
        End Get
        Set(ByVal Value As Decimal)
            _grossAmount = Value
        End Set
    End Property
    Public Property LoanFees() As Decimal
        Get
            Return _loanFees
        End Get
        Set(ByVal Value As Decimal)
            _loanFees = Value
        End Set
    End Property
    Public Property NetLoanAmount() As Decimal
        Get
            Return _netLoanAmount
        End Get
        Set(ByVal Value As Decimal)
            _netLoanAmount = Value
        End Set
    End Property
    Public Property FAID() As String
        Get
            Return _FAID
        End Get
        Set(ByVal Value As String)
            _FAID = Value
        End Set
    End Property
    Public Property AwardStartDate() As String
        Get
            Return _awardStartDate
        End Get
        Set(ByVal Value As String)
            _awardStartDate = Value
        End Set
    End Property
    Public Property AwardEndDate() As String
        Get
            Return _awardEndDate
        End Get
        Set(ByVal Value As String)
            _awardEndDate = Value
        End Set
    End Property
    Public Property Disbursements() As Integer
        Get
            Return _disbursements
        End Get
        Set(ByVal Value As Integer)
            _disbursements = Value
        End Set
    End Property
    Public Property LoanId() As String
        Get
            Return _loanId
        End Get
        Set(ByVal Value As String)
            _loanId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class

Public Class PaymentPlanInfo
    '
    '   PaymentPlanInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _paymentPlanId As String
    Private _stuEnrollId As String
    Private _payPlanDescrip As String
    Private _academicYearId As String
    Private _academicYear As String
    Private _totalAmountDue As Decimal
    Private _disbursements As Integer
    Private _payPlanStartDate As String
    Private _payPlanEndDate As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _paymentPlanId = Guid.NewGuid.ToString
        _stuEnrollId = Guid.Empty.ToString
        _payPlanDescrip = ""
        _academicYearId = Guid.Empty.ToString
        _totalAmountDue = 0.0
        _disbursements = 0
        _payPlanStartDate = ""
        _payPlanEndDate = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
    Public Sub New(ByVal studentId As String)
        _isInDB = False
        _paymentPlanId = Guid.NewGuid.ToString
        _stuEnrollId = Guid.Empty.ToString
        _academicYearId = Guid.Empty.ToString
        _totalAmountDue = 0.0
        _disbursements = 0
        _payPlanStartDate = ""
        _payPlanEndDate = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property PaymentPlanId() As String
        Get
            Return _paymentPlanId
        End Get
        Set(ByVal Value As String)
            _paymentPlanId = Value
        End Set
    End Property
    Public Property StuEnrollId() As String
        Get
            Return _stuEnrollId
        End Get
        Set(ByVal Value As String)
            _stuEnrollId = Value
        End Set
    End Property
    Public Property PayPlanDescrip() As String
        Get
            Return _payPlanDescrip
        End Get
        Set(ByVal Value As String)
            _payPlanDescrip = Value
        End Set
    End Property
    Public Property AcademicYearId() As String
        Get
            Return _academicYearId
        End Get
        Set(ByVal Value As String)
            _academicYearId = Value
        End Set
    End Property
    Public Property AcademicYear() As String
        Get
            Return _academicYear
        End Get
        Set(ByVal Value As String)
            _academicYear = Value
        End Set
    End Property
    Public Property TotalAmountDue() As Decimal
        Get
            Return _totalAmountDue
        End Get
        Set(ByVal Value As Decimal)
            _totalAmountDue = Value
        End Set
    End Property
    Public Property Disbursements() As Decimal
        Get
            Return _disbursements
        End Get
        Set(ByVal Value As Decimal)
            _disbursements = Value
        End Set
    End Property
    Public Property PayPlanStartDate() As String
        Get
            Return _payPlanStartDate
        End Get
        Set(ByVal Value As String)
            _payPlanStartDate = Value
        End Set
    End Property
    Public Property PayPlanEndDate() As String
        Get
            Return _payPlanEndDate
        End Get
        Set(ByVal Value As String)
            _payPlanEndDate = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
