Public Class ClsSectMeetingInfo

    ' 08/22/2011 JRobinson Clock Hour Project - Add new fields for ClsSectMeetings table: InstructionTypeId

#Region "Private Variable Declaration"
    Private _ClsSectMtgId As Guid
    Private _ClsSectId As Guid
    Private _StartTime As Guid
    Private _Day As Guid = (Guid.Empty)
    Private _Room As Guid
    Private _EndTime As Guid
    Private _STimeDescrip As String
    Private _ETimeDescrip As String
    Private _InstructorId As String
    Private _StartDate As Date
    Private _EndDate As Date
    Private _modUser As String
    Private _modDate As DateTime
    Private _fetchedFromDB As Boolean
    Private _periodId As Guid
    Private _periodDescrip As String
    Private _altPeriodId As Guid
    Private _altPeriodDescrip As String
    Private _RoomDescrip As String
    Private _cmdType As Integer
    Private _InstructionTypeId As Guid
    Private _InstructionTypeDescrip As String
    Private _attUsedCnt As Integer
    Private _breakDuration As Integer
#End Region

#Region " Public Constructor"
    Public Sub New()

        _modUser = ""
        _modDate = Date.MinValue
        _fetchedFromDB = False
        _StartDate = Date.MinValue
        _EndDate = Date.MaxValue
        _InstructionTypeId = Guid.Empty
       
    End Sub
#End Region
#Region "Class Section Meeting Properties"
    Public Property ClsSectMtgId() As Guid
        Get
            ClsSectMtgId = _ClsSectMtgId
        End Get
        Set(ByVal Value As Guid)
            _ClsSectMtgId = Value
        End Set
    End Property
    Public Property ClsSectId() As Guid
        Get
            ClsSectId = _ClsSectId
        End Get
        Set(ByVal Value As Guid)
            _ClsSectId = Value
        End Set
    End Property
    Public Property Day() As Guid
        Get
            Day = _Day
        End Get
        Set(ByVal Value As Guid)
            _Day = Value
        End Set
    End Property
    Public Property Room() As Guid
        Get
            Room = _Room
        End Get
        Set(ByVal Value As Guid)
            _Room = Value
        End Set
    End Property

    Public Property StartTime() As Guid
        Get
            StartTime = _StartTime
        End Get
        Set(ByVal Value As Guid)
            _StartTime = Value
        End Set
    End Property
    Public Property EndTime() As Guid
        Get
            EndTime = _EndTime
        End Get
        Set(ByVal Value As Guid)
            _EndTime = Value
        End Set
    End Property
    Public Property STimeDescrip() As String
        Get
            STimeDescrip = _STimeDescrip
        End Get
        Set(ByVal Value As String)
            _STimeDescrip = Value
        End Set
    End Property
    Public Property ETimeDescrip() As String
        Get
            ETimeDescrip = _ETimeDescrip
        End Get
        Set(ByVal Value As String)
            _ETimeDescrip = Value
        End Set
    End Property

    Public Property InstructorId() As String
        Get
            InstructorId = _InstructorId
        End Get
        Set(ByVal Value As String)
            _InstructorId = Value
        End Set
    End Property
    Public Property StartDate() As Date
        Get
            StartDate = _StartDate
        End Get
        Set(ByVal Value As Date)
            _StartDate = Value
        End Set
    End Property
    Public Property EndDate() As Date
        Get
            EndDate = _EndDate
        End Get
        Set(ByVal Value As Date)
            _EndDate = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property fetchedFromDB() As Boolean
        Get
            Return _fetchedFromDB
        End Get
        Set(ByVal Value As Boolean)
            _fetchedFromDB = Value
        End Set
    End Property
    Public Property PeriodId() As Guid
        Get
            Return _periodId
        End Get
        Set(ByVal Value As Guid)
            _periodId = Value
        End Set
    End Property
    Public Property PeriodDescrip() As String
        Get
            Return _periodDescrip
        End Get
        Set(ByVal Value As String)
            _periodDescrip = Value
        End Set
    End Property
    Public Property AltPeriodId() As Guid
        Get
            Return _altPeriodId
        End Get
        Set(ByVal Value As Guid)
            _altPeriodId = Value
        End Set
    End Property
    Public Property AltPeriodDescrip() As String
        Get
            Return _altPeriodDescrip
        End Get
        Set(ByVal Value As String)
            _altPeriodDescrip = Value
        End Set
    End Property
    Public Property RoomDescrip() As String
        Get
            Return _RoomDescrip
        End Get
        Set(ByVal Value As String)
            _RoomDescrip = Value
        End Set
    End Property
    Public Property CmdType() As Integer
        Get
            Return _cmdType
        End Get
        Set(ByVal Value As Integer)
            _cmdType = Value
        End Set
    End Property
    Public Property InstructionTypeId() As Guid
        Get
            Return _InstructionTypeId
        End Get
        Set(ByVal Value As Guid)
            _InstructionTypeId = Value
        End Set
    End Property
    Public Property InstructionTypeDescrip() As String
        Get
            InstructionTypeDescrip = _InstructionTypeDescrip
        End Get
        Set(ByVal Value As String)
            _InstructionTypeDescrip = Value
        End Set
    End Property
    Public Property AttUsedCnt() As Integer
        Get
            Return _attUsedCnt
        End Get
        Set(ByVal Value As Integer)
            _attUsedCnt = Value
        End Set
    End Property

    Public Property BreakDuration() As Integer
        Get
            Return _breakDuration
        End Get
        Set(ByVal Value As Integer)
            _breakDuration = Value
        End Set
    End Property
   
#End Region
End Class

