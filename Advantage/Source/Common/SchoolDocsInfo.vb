Public Class SchoolDocsInfo
#Region "Variables"
    Private _FirstName As String
    Private _LastName As String
    Private _SSN As String
    Private _StudentStatus As String
    Private _DOB As Date
    Private _StudentGender As String
    Private _StudentRace As String
    Private _StudentNumber As String
    Private _Sponsor As String
    Private _StudentAgency As String
    Private _AdmissionsRep As String
    Private _Campuses As String
    Private _StartDate As Date
    Private _DateDetermined As Date
    Private _DropReason As String
    Private _EnrollDate As Date
    Private _ExpGradDate As Date
    Private _LDA As Date
    Private _ProgramName As String
    Private _StatusDropped As String
    Private _StatusGraduated As String
#End Region
#Region "Constructor"
    Public Sub New()
        _FirstName = ""
        _LastName = ""
        _SSN = ""
        _StudentStatus = ""
        _DOB = "01/01/1900"
        _StudentGender = ""
        _StudentRace = ""
        _StudentNumber = ""
        _Sponsor = ""
        _StudentAgency = ""
        _AdmissionsRep = ""
        _Campuses = ""
        _StartDate = "01/01/1900"
        _DateDetermined = "01/01/1900"
        _DropReason = ""
        _EnrollDate = "01/01/1900"
        _ExpGradDate = "01/01/1900"
        _LDA = "01/01/1900"
        _ProgramName = ""
        _StatusDropped = ""
        _StatusGraduated = ""
    End Sub
#End Region
#Region "Property"
    Public Property FirstName() As String
        Get
            Return _FirstName
        End Get
        Set(ByVal Value As String)
            _FirstName = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return _LastName
        End Get
        Set(ByVal Value As String)
            _LastName = Value
        End Set
    End Property
    Public Property SSN() As String
        Get
            Return _SSN
        End Get
        Set(ByVal Value As String)
            _SSN = Value
        End Set
    End Property
    Public Property StudentStatus() As String
        Get
            Return _StudentStatus
        End Get
        Set(ByVal Value As String)
            _StudentStatus = Value
        End Set
    End Property
    Public Property DOB() As Date
        Get
            Return _DOB
        End Get
        Set(ByVal Value As Date)
            _DOB = Value
        End Set
    End Property
    Public Property StudentGender() As String
        Get
            Return _StudentGender
        End Get
        Set(ByVal Value As String)
            _StudentGender = Value
        End Set
    End Property
    Public Property StudentRace() As String
        Get
            Return _StudentRace
        End Get
        Set(ByVal Value As String)
            _StudentRace = Value
        End Set
    End Property
    Public Property StudentNumber() As String
        Get
            Return _StudentNumber
        End Get
        Set(ByVal Value As String)
            _StudentNumber = Value
        End Set
    End Property
    Public Property Sponsor() As String
        Get
            Return _Sponsor
        End Get
        Set(ByVal Value As String)
            _Sponsor = Value
        End Set
    End Property
    Public Property StudentAgency() As String
        Get
            Return _StudentAgency
        End Get
        Set(ByVal Value As String)
            _StudentAgency = Value
        End Set
    End Property
    Public Property AdmissionsRep() As String
        Get
            Return _AdmissionsRep
        End Get
        Set(ByVal Value As String)
            _AdmissionsRep = Value
        End Set
    End Property
    Public Property Campuses() As String
        Get
            Return _Campuses
        End Get
        Set(ByVal Value As String)
            _Campuses = Value
        End Set
    End Property
    Public Property StartDate() As Date
        Get
            Return _StartDate
        End Get
        Set(ByVal Value As Date)
            _StartDate = Value
        End Set
    End Property
    Public Property DateDetermined() As Date
        Get
            Return _DateDetermined
        End Get
        Set(ByVal Value As Date)
            _DateDetermined = Value
        End Set
    End Property
    Public Property DropReason() As String
        Get
            Return _DropReason
        End Get
        Set(ByVal Value As String)
            _DropReason = Value
        End Set
    End Property
    Public Property EnrollDate() As Date
        Get
            Return _EnrollDate
        End Get
        Set(ByVal Value As Date)
            _EnrollDate = Value
        End Set
    End Property
    Public Property ExpGradDate() As Date
        Get
            Return _ExpGradDate
        End Get
        Set(ByVal Value As Date)
            _ExpGradDate = Value
        End Set
    End Property
    Public Property LDA() As Date
        Get
            Return _LDA
        End Get
        Set(ByVal Value As Date)
            _LDA = Value
        End Set
    End Property
    Public Property ProgramName() As String
        Get
            Return _ProgramName
        End Get
        Set(ByVal Value As String)
            _ProgramName = Value
        End Set
    End Property
    Public Property StatusDropped() As String
        Get
            Return _StatusDropped
        End Get
        Set(ByVal Value As String)
            _StatusDropped = Value
        End Set
    End Property
    Public Property StatusGraduated() As String
        Get
            Return _StatusGraduated
        End Get
        Set(ByVal Value As String)
            _StatusGraduated = Value
        End Set
    End Property

#End Region
End Class
