Public Class TermModuleInfo
#Region "Private Variable Declaration"
    Private _TermInfo As TermInfo
    Private _ClassSectionInfo As ClassSectionInfo
#End Region
#Region " Public Constructor"
    Public Sub New(ByVal termInfo As TermInfo, ByVal classSectInfo As ClassSectionInfo)
        _TermInfo = termInfo
        _ClassSectionInfo = classSectInfo

    End Sub

    Public Sub New()

    End Sub
#End Region

#Region "Class Section Properties"
    Public Property TermInfo() As TermInfo
        Get
            Return _TermInfo
        End Get
        Set(ByVal Value As TermInfo)
            _TermInfo = Value
        End Set
    End Property

    Public Property ClsSectionInfo() As ClassSectionInfo
        Get
            Return _ClassSectionInfo
        End Get
        Set(ByVal Value As ClassSectionInfo)
            _ClassSectionInfo = Value
        End Set
    End Property

#End Region

End Class
