﻿Public Class FERPAInfo
    Private _isInDB As Boolean
    Private _FERPAId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _name As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
    Public Sub New()
        _isInDB = False
        _FERPAId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _name = ""
        _campGrpId = Guid.Empty.ToString
        _campGrpDescrip = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub


    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property FERPAId() As String
        Get
            Return _FERPAId
        End Get
        Set(ByVal Value As String)
            _FERPAId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal Value As String)
            _name = Value
        End Set
    End Property

    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
End Class
Public Class FERPAPolicyInfo
    Private _FERPAPolicyId As String
    Private _FERPAEntityId As String
    Private _FERPACategoryId() As String
    Private _StudentId As String
    Private _modUser As String
    Private _modDate As DateTime
    Public Sub New()
     
    End Sub
    Public Property FERPAPolicyId() As String
        Get
            Return _FERPAPolicyId
        End Get
        Set(ByVal Value As String)
            _FERPAPolicyId = Value
        End Set
    End Property
    Public Property FERPAEntityId() As String
        Get
            Return _FERPAEntityId
        End Get
        Set(ByVal Value As String)
            _FERPAEntityId = Value
        End Set
    End Property
    Public Property FERPACategoryId() As String()
        Get
            Return _FERPACategoryId
        End Get
        Set(ByVal Value As String())
            _FERPACategoryId = Value
        End Set
    End Property
    Public Property StudentId() As String
        Get
            Return _StudentId
        End Get
        Set(ByVal Value As String)
            _StudentId = Value
        End Set
    End Property
    
 
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
End Class

