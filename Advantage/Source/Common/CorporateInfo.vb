Public Class CorporateInfo
    '
    '   CorporateInfo Class
    '
#Region " Private Variables and Objects"
    Private _CorporateName As String
    Private _Address1 As String
    Private _Address2 As String
    Private _City As String
    Private _State As String
    Private _OtherState As String
    Private _Zip As String
    Private _ForeignZip As Boolean
    Private _Country As String
    Private _Phone As String
    Private _ForeignPhone As String
    Private _Fax As String
    Private _TranscriptAuthznTitle As String
    Private _TranscriptAuthznName As String
    Private _CampDescrip As String
    Private _Website As String
    Private _IsClockHour As Boolean

#End Region
#Region " Public Constructors "
    Public Sub New()
        _CorporateName = ""
        _Address1 = ""
        _Address2 = ""
        _City = ""
        _State = ""
        _OtherState = ""
        _Zip = ""
        _Country = ""
        _Fax = ""
        _Phone = ""
        _ForeignPhone = ""
        '_ForeignZip = ""
    End Sub
#End Region
#Region " Public Properties"
    Public Property CorporateName() As String
        Get
            Return _CorporateName
        End Get
        Set(ByVal Value As String)
            _CorporateName = Value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return _Address1
        End Get
        Set(ByVal Value As String)
            _Address1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return _Address2
        End Get
        Set(ByVal Value As String)
            _Address2 = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _City
        End Get
        Set(ByVal Value As String)
            _City = Value
        End Set
    End Property
    Public Property State() As String
        Get
            Return _State
        End Get
        Set(ByVal Value As String)
            _State = Value
        End Set
    End Property
    Public Property OtherState() As String
        Get
            Return _OtherState
        End Get
        Set(ByVal Value As String)
            _OtherState = Value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return _Zip
        End Get
        Set(ByVal Value As String)
            _Zip = Value
        End Set
    End Property
   
    Public Property Country() As String
        Get
            Return _Country
        End Get
        Set(ByVal Value As String)
            _Country = Value
        End Set
    End Property
    Public Property Phone() As String
        Get
            Return _Phone
        End Get
        Set(ByVal Value As String)
            _Phone = Value
        End Set
    End Property
    Public Property ForeignPhone() As String
        Get
            Return _ForeignPhone
        End Get
        Set(ByVal Value As String)
            _ForeignPhone = Value
        End Set
    End Property
    Public Property Fax() As String
        Get
            Return _Fax
        End Get
        Set(ByVal Value As String)
            _Fax = Value
        End Set
    End Property

    Public Property TranscriptAuthznTitle() As String
        Get
            Return _TranscriptAuthznTitle
        End Get
        Set(ByVal value As String)
            _TranscriptAuthznTitle = value
        End Set
    End Property

    Public Property TranscriptAuthznName() As String
        Get
            Return _TranscriptAuthznName
        End Get
        Set(ByVal value As String)
            _TranscriptAuthznName = value
        End Set
    End Property

    Public Property Website() As String
        Get
            Return _Website
        End Get
        Set(ByVal value As String)
            _Website = value
        End Set
    End Property
  
    Public Property IsClockHour() As Boolean
        Get
            Return _IsClockHour
        End Get
        Set(ByVal value As Boolean)
            _IsClockHour = value
        End Set
    End Property
    Public Property ForeignZip() As Boolean
        Get
            Return _ForeignZip
        End Get
        Set(ByVal value As Boolean)
            _ForeignZip = value
        End Set
    End Property
#End Region

End Class
