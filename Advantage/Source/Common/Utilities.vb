Imports System.ComponentModel

Public Class Utilities
    Public Shared Function GetTotalClassesFromStudentResultsDT(ByVal dt As DataTable) As String
        If dt.Rows.Count > 0 Then
            Dim obj As Object

            obj = dt.Compute("Count(Descrip)", Nothing)
            If Not (obj Is System.DBNull.Value) Then
                Return Convert.ToInt32(obj).ToString
            End If
            Return ""
        Else
            Return ""
        End If
    End Function

    Public Shared Function GetCreditsAttemptedFromStudentResultsDT(ByVal dt As DataTable) As String
        If dt.Rows.Count > 0 Then
            Dim obj As Object

            obj = dt.Compute("Sum(Credits)", "IsCreditsAttempted=1")
            If Not (obj Is System.DBNull.Value) Then
                Return System.Math.Round(Convert.ToDecimal(obj), 2).ToString("##.00")
            End If
            Return ""
        Else
            Return ""
        End If
    End Function

    Public Shared Function GetCreditsEarnedFromStudentResultsDT(ByVal dt As DataTable) As String
        If dt.Rows.Count > 0 Then
            'Dim obj As Object
            Dim lastReqId As String = ""
            'Dim aRows() As DataRow
            Dim allRows() As DataRow
            Dim credits As Decimal

            allRows = dt.Select("", "ReqId,Grade DESC")

            For Each dr As DataRow In allRows
                If lastReqId = "" Or lastReqId <> dr("ReqId").ToString Then
                    If dr("IsCreditsEarned") Then
                        credits += dr("Credits")
                    End If
                    lastReqId = dr("ReqId").ToString
                End If
            Next

            If credits <> 0 Then
                Return System.Math.Round(credits, 2).ToString("##.00")
            Else
                Return ""
            End If

            'obj = dt.Compute("Sum(Credits)", "IsCreditsEarned=1")
            'If Not (obj Is System.DBNull.Value) Then
            '    Return System.Math.Round(Convert.ToDecimal(obj), 2).ToString("##.00")
            'End If
            'Return ""

        Else
            Return ""
        End If
    End Function

    Public Shared Function GetAccumGPAFromStudentResultsDT(ByVal dt As DataTable) As String
        Dim dr As DataRow
        'Dim TotalCreds As Double
        'Dim GrdWeight As Double
        'Dim TotalCreds2 As Double
        'Dim GrdWeight2 As Double
        Dim result As String = ""
        Dim numClasses As Integer
        Dim sumGPA As Double

        'This code is for a weighted average GPA. The program version screen will have to be modified to
        'include a checkbox for the user to indicate the version uses weighted average GPA.
        'For Each dr In dt.Rows
        '    If Not dr.IsNull("IsInGPA") Then
        '        If dr("IsInGPA") = 1 Or dr("IsInGPA") = True Then
        '            TotalCreds2 += dr("Credits")
        '            If Not dr.IsNull("GPA") Then
        '                GrdWeight2 += dr("GPA") * dr("Credits")
        '            End If
        '        End If

        '    End If
        'Next
        'If GrdWeight2 > 0 And TotalCreds2 > 0 Then
        '    result = System.Math.Round((GrdWeight2 / TotalCreds2), 2).ToString("##.00")
        'End If

        'This code is for a simple average GPA.
        For Each dr In dt.Rows
            If Not dr.IsNull("IsInGPA") Then
                If dr("IsInGPA") = 1 Or dr("IsInGPA") = True Then
                    If Not dr.IsNull("GPA") Then
                        numClasses += 1
                        sumGPA += dr("GPA")
                    End If
                End If
            End If
        Next

        If numClasses > 0 And sumGPA > 0 Then
            result = System.Math.Round((sumGPA / numClasses), 2).ToString("##.00")
        End If

        Return result
    End Function

    Public Shared Function GetHoursFromStudentResultsDT(ByVal dt As DataTable) As String
        If dt.Rows.Count > 0 Then
            'Dim obj As Object
            Dim lastReqId As String = ""
            'Dim aRows() As DataRow
            Dim allRows() As DataRow
            Dim hours As Decimal

            allRows = dt.Select("", "ReqId,Grade DESC")

            For Each dr As DataRow In allRows
                If lastReqId = "" Or lastReqId <> dr("ReqId").ToString Then
                    If Not dr("IsDrop") Then
                        hours += dr("Hours")
                    End If
                    lastReqId = dr("ReqId").ToString
                End If
            Next

            If hours <> 0 Then
                Return System.Math.Round(hours, 2).ToString("##.00")
            Else
                Return ""
            End If

            'obj = dt.Compute("Sum(Hours)", "")
            'If Not (obj Is System.DBNull.Value) Then
            '    Return System.Math.Round(Convert.ToDecimal(obj), 2).ToString("##.00")
            'End If
            'Return ""
        Else
            Return ""
        End If
    End Function
    Public Shared Function GetAdvantageDBDateTime(ByVal dTime As DateTime) As Date
        Dim strnow As Date

        Dim sDate As String = dTime.Date.ToShortDateString
        Dim sHour As String = dTime.Hour.ToString
        Dim sMinute As String = dTime.Minute.ToString
        Dim iSecs As Integer = dTime.Second

        strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)

        Return strnow

    End Function
    Public Shared Function GetAdvantageDBSmartDateTime(ByVal dTime As DateTime) As SmartDate
        Dim strnow As Date

        Dim sDate As String = dTime.Date.ToShortDateString
        Dim sHour As String = dTime.Hour.ToString
        Dim sMinute As String = dTime.Minute.ToString
        Dim iSecs As Integer = dTime.Second

        strnow = Convert.ToDateTime(sDate + " " + sHour + ":" + sMinute + ":" + iSecs.ToString)

        Return New SmartDate(strnow, True)

    End Function
    ''This procedure gets the <Description> attribute of an enum constant, if any.
    ''Otherwise it gets the string name of the enum member.
    ' Source: http: //www.vbforums.com/showthread.php?550710-Enum-to-represent-String-constants
    ' this funciton use System.ComponentModel
    Public Shared Function EnumDescription(ByVal EnumConstant As [Enum]) As String
        Dim fi As Reflection.FieldInfo = EnumConstant.GetType().GetField(EnumConstant.ToString())
        Dim aattr() As DescriptionAttribute = DirectCast(fi.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())
        If aattr.Length > 0 Then
            Return aattr(0).Description
        Else
            Return EnumConstant.ToString()
        End If
    End Function


End Class
