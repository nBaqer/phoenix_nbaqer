Public Class EmployerJobsInfo
#Region "Declare Variables"
    Private _isInDB As Boolean
    Private _EmployerJobId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _JobTitleId As String
    Private _jobTitle As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _JobGroupId As String
    Private _jobGroup As String
    Private _ContactId As String
    Private _contact As String
    Private _TypeId As String
    Private _type As String
    Private _AreaId As String
    Private _area As String
    Private _Workdays As String
    Private _FeeId As String
    Private _fee As String
    Private _NumberOpen As Integer
    Private _NumberFilled As Integer
    Private _OpenedFrom As String
    Private _OpenedTo As String
    Private _Start As String
    Private _SalaryFrom As String
    Private _SalaryTo As String
    Private _BenefitsId As String
    Private _benefits As String
    Private _ScheduleId As String
    Private _schedule As String
    Private _HoursFrom As String
    Private _HoursTo As String
    Private _JobDescrip As String
    Private _Notes As String
    'Private _JobTitleDescription As String
    Private _EmployerId As String
    'Private _JobRequirements As String
    Private _salaryTypeId As String
    Private _SalaryType As String
    Private _Requirement As String
    Private _ModDate As DateTime
    Private _EmployerJobTitle As String
    Private _JobPostedDate As String
    Private _ExpertiseLevelId As String
    Private _expertiseLevel As String
#End Region
#Region " Constructors"
    Public Sub New()
        _isInDB = False
        _EmployerJobId = Guid.NewGuid.ToString()
        _code = ""
        _statusId = Guid.Empty.ToString()
        _status = ""
        _JobTitleId = Guid.Empty.ToString()
        _jobTitle = ""
        _campGrpId = Guid.Empty.ToString()
        _campGrpDescrip = ""
        _JobGroupId = Guid.Empty.ToString()
        _jobGroup = ""
        _ContactId = Guid.Empty.ToString()
        _contact = ""
        _TypeId = Guid.Empty.ToString()
        _type = ""
        _AreaId = Guid.Empty.ToString()
        _area = ""
        _Workdays = ""
        _FeeId = Guid.Empty.ToString()
        _fee = ""
        _NumberOpen = 0
        _NumberFilled = 0
        _OpenedFrom = ""
        _OpenedTo = ""
        _Start = ""
        _SalaryFrom = ""
        _SalaryTo = ""
        _BenefitsId = Guid.Empty.ToString()
        _benefits = ""
        _ScheduleId = Guid.Empty.ToString()
        _schedule = ""
        _HoursFrom = ""
        _HoursTo = ""
        _JobDescrip = ""
        _Notes = ""
        '_JobTitleDescription = ""
        _EmployerId = Guid.Empty.ToString()
        '_JobRequirements = ""
        _salaryTypeId = Guid.Empty.ToString()
        _SalaryType = ""
        _Requirement = ""
        _ModDate = Date.MinValue
        _EmployerJobTitle = ""
        _JobPostedDate = ""
        _ExpertiseLevelId = Guid.Empty.ToString()
        _expertiseLevel = ""
    End Sub
#End Region

#Region "Property"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property EmployerJobId() As String
        Get
            Return _EmployerJobId
        End Get
        Set(ByVal Value As String)
            _EmployerJobId = Value
        End Set
    End Property
    Public Property EmployerJobTitle() As String
        Get
            Return _EmployerJobTitle
        End Get
        Set(ByVal Value As String)
            _EmployerJobTitle = Value
        End Set
    End Property
    Public Property JobPostedDate() As String
        Get
            Return _JobPostedDate
        End Get
        Set(ByVal Value As String)
            _JobPostedDate = Value
        End Set
    End Property
    Public Property ExpertiseLevelId() As String
        Get
            Return _ExpertiseLevelId
        End Get
        Set(ByVal Value As String)
            _ExpertiseLevelId = Value
        End Set
    End Property
    Public Property ExpertiseLevel() As String
        Get
            Return _expertiseLevel
        End Get
        Set(ByVal Value As String)
            _expertiseLevel = Value
        End Set
    End Property
    Public Property EmployerId() As String
        Get
            Return _EmployerId
        End Get
        Set(ByVal Value As String)
            _EmployerId = Value
        End Set
    End Property
    Public Property Requirement() As String
        Get
            Return _Requirement
        End Get
        Set(ByVal Value As String)
            _Requirement = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property FeeId() As String
        Get
            Return _FeeId
        End Get
        Set(ByVal Value As String)
            _FeeId = Value
        End Set
    End Property
    Public Property Fee() As String
        Get
            Return _fee
        End Get
        Set(ByVal Value As String)
            _fee = Value
        End Set
    End Property
    Public Property JobTitleId() As String
        Get
            Return _JobTitleId
        End Get
        Set(ByVal Value As String)
            _JobTitleId = Value
        End Set
    End Property
    Public Property JobTitle() As String
        Get
            Return _jobTitle
        End Get
        Set(ByVal Value As String)
            _JobTitle = Value
        End Set
    End Property
    Public Property JobGroupId() As String
        Get
            Return _JobGroupId
        End Get
        Set(ByVal Value As String)
            _JobGroupId = Value
        End Set
    End Property
    Public Property JobGroup() As String
        Get
            Return _jobGroup
        End Get
        Set(ByVal Value As String)
            _jobGroup = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
    Public Property JobDescription() As String
        Get
            Return _JobDescrip
        End Get
        Set(ByVal Value As String)
            _JobDescrip = Value
        End Set
    End Property

    Public Property SalaryType() As String
        Get
            Return _SalaryType
        End Get
        Set(ByVal Value As String)
            _SalaryType = Value
        End Set
    End Property
    Public Property SalaryTypeId() As String
        Get
            Return _salaryTypeId
        End Get
        Set(ByVal Value As String)
            _salaryTypeId = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ContactId() As String
        Get
            Return _ContactId
        End Get
        Set(ByVal Value As String)
            _ContactId = Value
        End Set
    End Property
    Public Property Contact() As String
        Get
            Return _contact
        End Get
        Set(ByVal Value As String)
            _contact = Value
        End Set
    End Property
    Public Property TypeId() As String
        Get
            Return _TypeId
        End Get
        Set(ByVal Value As String)
            _TypeId = Value
        End Set
    End Property
    Public Property Type() As String
        Get
            Return _Type
        End Get
        Set(ByVal Value As String)
            _type = Value
        End Set
    End Property
    Public Property AreaId() As String
        Get
            Return _AreaId
        End Get
        Set(ByVal Value As String)
            _AreaId = Value
        End Set
    End Property
    Public Property Area() As String
        Get
            Return _area
        End Get
        Set(ByVal Value As String)
            _area = Value
        End Set
    End Property
    Public Property NumberOpen() As Integer
        Get
            Return _NumberOpen
        End Get
        Set(ByVal Value As Integer)
            _NumberOpen = Value
        End Set
    End Property
    Public Property NumberFilled() As Integer
        Get
            Return _NumberFilled
        End Get
        Set(ByVal Value As Integer)
            _NumberFilled = Value
        End Set
    End Property
    Public Property OpenedFrom() As String
        Get
            Return _OpenedFrom
        End Get
        Set(ByVal Value As String)
            _OpenedFrom = Value
        End Set
    End Property
    Public Property OpenedTo() As String
        Get
            Return _OpenedTo
        End Get
        Set(ByVal Value As String)
            _OpenedTo = Value
        End Set
    End Property
    Public Property Start() As String
        Get
            Return _Start
        End Get
        Set(ByVal Value As String)
            _Start = Value
        End Set
    End Property
    Public Property SalaryTo() As String
        Get
            Return _SalaryTo
        End Get
        Set(ByVal Value As String)
            _SalaryTo = Value
        End Set
    End Property
    Public Property SalaryFrom() As String
        Get
            Return _SalaryFrom
        End Get
        Set(ByVal Value As String)
            _SalaryFrom = Value
        End Set
    End Property
    Public Property BenefitsId() As String
        Get
            Return _BenefitsId
        End Get
        Set(ByVal Value As String)
            _BenefitsId = Value
        End Set
    End Property
    Public Property Benefits() As String
        Get
            Return _benefits
        End Get
        Set(ByVal Value As String)
            _benefits = Value
        End Set
    End Property
    Public Property ScheduleId() As String
        Get
            Return _ScheduleId
        End Get
        Set(ByVal Value As String)
            _ScheduleId = Value
        End Set
    End Property
    Public Property Schedule() As String
        Get
            Return _schedule
        End Get
        Set(ByVal Value As String)
            _schedule = Value
        End Set
    End Property
    Public Property HoursFrom() As String
        Get
            Return _HoursFrom
        End Get
        Set(ByVal Value As String)
            _HoursFrom = Value
        End Set
    End Property
    Public Property HoursTo() As String
        Get
            Return _HoursTo
        End Get
        Set(ByVal Value As String)
            _HoursTo = Value
        End Set
    End Property
    'Public Property JObTitleDescription()
    '    Get
    '        Return _JobTitleDescription
    '    End Get
    '    Set(ByVal Value)
    '        _JobTitleDescription = Value
    '    End Set
    'End Property
    Public Property ModDate() As Date
        Get
            Return _ModDate
        End Get
        Set(ByVal Value As Date)
            _ModDate = Value
        End Set
    End Property
#End Region
End Class
