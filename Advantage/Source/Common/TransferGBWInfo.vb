Public Class TransferGbwInfo
    '
    '   TransferGBWInfo Class
    '

#Region " Public Constructors "

    Public Sub New()
        GrdSysDetailId = Guid.NewGuid.ToString
        StuEnrollId = Guid.Empty.ToString
        ClsSectionId = Guid.Empty.ToString
        Score = 0.0
        Pass = 0
        Grade = String.Empty
    End Sub

#End Region

#Region " Public Properties"

    Public Property GrdSysDetailId As String
    Public Property StuEnrollId As String
    Public Property ClsSectionId As String
    Public Property Score As Decimal
    Public Property Pass As Integer
    Public Property Grade As String

#End Region
End Class
