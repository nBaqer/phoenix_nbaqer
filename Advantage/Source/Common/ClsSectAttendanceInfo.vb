﻿Public Class ClsSectAttendanceInfo
#Region "Private Variable Declaration"

    Private _ClsSectionId As Guid
    Private _ClsSectMeetingId As Guid
    Private _MeetDate As DateTime
    Private _Actual As String
    Private _Tardy As Boolean
    Private _Excused As Boolean
    Private _Comments As String
    Private _StuEnrollId As Guid
    Private _Scheduled As String
    Private _StudentId As Guid
    Private _UnitTypeId As String
  

#End Region
#Region " Public Constructor"
    Public Sub New()

        _MeetDate = DateTime.Now
        _Actual = "9999"
        _Tardy = False
        _Excused = False
        _Comments = ""
        _Scheduled = "0"
    End Sub
#End Region

#Region "Class Section Properties"

    Public Property ClsSectionId() As Guid
        Get
            ClsSectionId = _ClsSectionId
        End Get
        Set(ByVal Value As Guid)
            _ClsSectionId = Value
        End Set
    End Property
    Public Property ClsSectMeetingId() As Guid
        Get
            ClsSectMeetingId = _ClsSectMeetingId
        End Get
        Set(ByVal Value As Guid)
            _ClsSectMeetingId = Value
        End Set
    End Property
    Public Property MeetDate() As DateTime
        Get
            MeetDate = _MeetDate
        End Get
        Set(ByVal Value As DateTime)
            _MeetDate = Value
        End Set
    End Property
    Public Property Actual() As String
        Get
            Actual = _Actual
        End Get
        Set(ByVal Value As String)
            _Actual = Value
        End Set
    End Property
    Public Property Tardy() As Boolean
        Get
            Tardy = _Tardy
        End Get
        Set(ByVal Value As Boolean)
            _Tardy = Value
        End Set
    End Property
    Public Property Excused() As Boolean
        Get
            Excused = _Excused
        End Get
        Set(ByVal Value As Boolean)
            _Excused = Value
        End Set
    End Property
    Public Property Scheduled() As String
        Get
            Scheduled = _Scheduled
        End Get
        Set(ByVal Value As String)
            _Scheduled = Value
        End Set
    End Property
    Public Property Comments() As String
        Get
            Comments = _Comments
        End Get
        Set(ByVal Value As String)
            _Comments = Value
        End Set
    End Property
    Public Property StuEnrollId() As Guid
        Get
            StuEnrollId = _StuEnrollId
        End Get
        Set(ByVal Value As Guid)
            _StuEnrollId = Value
        End Set
    End Property
    Public Property StudentID() As Guid
        Get
            StudentID = _StudentId
        End Get
        Set(ByVal Value As Guid)
            _StudentId = Value
        End Set
    End Property

    Public Property UnitTypeId() As String
        Get
            UnitTypeId = _UnitTypeId
        End Get
        Set(ByVal Value As String)
            _UnitTypeId = Value
        End Set
    End Property

#End Region
End Class
