Public Class AppliedPaymentInfo
#Region "Private Variable Declaration"
    Private _AppliedPaymentId As String
    Private _PaymentId As String
    Private _ChargeId As String
    Private _Amount As Decimal
    Private _modUser As String
    Private _modDate As DateTime
    Private _fetchedFromDB As Boolean
#End Region

#Region " Public Constructor"
    Public Sub New()

        _modUser = ""
        _modDate = Date.MinValue
        _fetchedFromDB = False
    End Sub
#End Region

#Region " Public Properties"
    Public Property AppliedPaymentId() As String
        Get
            Return _AppliedPaymentId
        End Get
        Set(ByVal Value As String)
            _AppliedPaymentId = Value
        End Set
    End Property

    Public Property PaymentId() As String
        Get
            Return _PaymentId
        End Get
        Set(ByVal Value As String)
            _PaymentId = Value
        End Set
    End Property

    Public Property ChargeId() As String
        Get
            Return _ChargeId
        End Get
        Set(ByVal Value As String)
            _ChargeId = Value
        End Set
    End Property

    Public Property Amount() As Decimal
        Get
            Return _Amount
        End Get
        Set(ByVal Value As Decimal)
            _Amount = Value
        End Set
    End Property

    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property

    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property

    Public Property fetchedFromDB() As Boolean
        Get
            Return _fetchedFromDB
        End Get
        Set(ByVal Value As Boolean)
            _fetchedFromDB = Value
        End Set
    End Property

#End Region
End Class
