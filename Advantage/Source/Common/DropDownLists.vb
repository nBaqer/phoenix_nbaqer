Public MustInherit Class AdvantageDropDownListMetadata
#Region "Protected Variables"
    Protected m_pullActiveItemsOnly As Boolean
    Protected m_campusId As String
#End Region
#Region "Public Constructors"
    Public Sub New()
        m_pullActiveItemsOnly = False
    End Sub
#End Region
#Region "Public Methods"
    Public Sub Bind(ByVal ddl As System.Web.UI.WebControls.DropDownList, ByVal ds As DataSet)
        Dim dt As DataTable = ds.Tables(Me.DropDownListName)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim row As DataRow = dt.Rows(i)
            Dim item As New System.Web.UI.WebControls.ListItem(row(1), CType(row(0), String).ToLower, row(2))
            ddl.Items.Add(item)
        Next
    End Sub
    Public Sub Bind(ByVal ddl As System.Web.UI.WebControls.DropDownList, ByVal ds As DataSet, ByVal insertSelectItem As Boolean)
        'bind ddl
        Bind(ddl, ds)

        'insert select item at the beginning
        If insertSelectItem Then
            ddl.Items.Insert(0, New System.Web.UI.WebControls.ListItem("Select", Guid.Empty.ToString))
        End If

    End Sub
    Public Sub Bind(ByVal ddl As System.Web.UI.WebControls.DropDownList, ByVal ds As DataSet, ByVal insertSelectItem As Boolean, ByVal selectValue As String)
        'bind ddl
        Bind(ddl, ds)

        'insert select item at the beginning
        If insertSelectItem Then
            ddl.Items.Insert(0, New System.Web.UI.WebControls.ListItem("Select", selectValue))
        End If

    End Sub
#End Region
#Region "Public Properties"
    Public MustOverride ReadOnly Property DropDownListName() As String
    Public MustOverride ReadOnly Property TableName() As String
    Public MustOverride ReadOnly Property ValueField() As String
    Public MustOverride ReadOnly Property TextField() As String
    Public Overridable ReadOnly Property OverrideSqlStatement() As String
        Get
            Return Nothing
        End Get
    End Property
    Public Overridable Property PullActiveItemsOnly() As Boolean
        Get
            Return m_pullActiveItemsOnly
        End Get
        Set(ByVal value As Boolean)
            m_pullActiveItemsOnly = value
        End Set
    End Property
    Public Property CampusId() As String
        Get
            Return m_campusId
        End Get
        Set(ByVal value As String)
            m_campusId = value
        End Set
    End Property
    Public Overridable Sub SetSQLStatementForImportLeadsPage()

    End Sub
#End Region
End Class