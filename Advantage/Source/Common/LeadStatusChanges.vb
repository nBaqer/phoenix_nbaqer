' ===============================================================================
' FAME.AdvantageV1.BusinessEntities
'
' LeadStatusChanges.vb
'
' Lead Status Changes Common Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2005 FAME Inc.
' All rights reserved.
' ===============================================================================


Public Class LeadStatusChangeInfo
    '
    '   Lead Status Change Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _LeadStatusChangeId As String
    Private _OrigStatusId As String
    Private _NewStatusId As String
    Private _CampGrpId As String
    Private _isReversal As Boolean
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _LeadStatusChangeId = Guid.NewGuid.ToString
        _OrigStatusId = Guid.Empty.ToString
        _NewStatusId = Guid.Empty.ToString
        _CampGrpId = Guid.Empty.ToString
        _isReversal = False
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property LeadStatusChangeId() As String
        Get
            Return _LeadStatusChangeId
        End Get
        Set(ByVal Value As String)
            _LeadStatusChangeId = Value
        End Set
    End Property
    Public Property OrigStatusId() As String
        Get
            Return _OrigStatusId
        End Get
        Set(ByVal Value As String)
            _OrigStatusId = Value
        End Set
    End Property
    Public Property NewStatusId() As String
        Get
            Return _NewStatusId
        End Get
        Set(ByVal Value As String)
            _NewStatusId = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _CampGrpId
        End Get
        Set(ByVal Value As String)
            _CampGrpId = Value
        End Set
    End Property
    Public Property IsReversal() As Boolean
        Get
            Return _isReversal
        End Get
        Set(ByVal Value As Boolean)
            _isReversal = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class

Public Class LeadStatusesInfo
    '
    '   Lead Statuses Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _StatusChangeId As String
    Private _LeadId As String
    Private _OrigStatusId As String
    Private _NewStatusId As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _StatusChangeId = Guid.NewGuid.ToString
        _OrigStatusId = Guid.Empty.ToString
        _NewStatusId = Guid.Empty.ToString
        _LeadId = Guid.Empty.ToString
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property StatusChangeId() As String
        Get
            Return _StatusChangeId
        End Get
        Set(ByVal Value As String)
            _StatusChangeId = Value
        End Set
    End Property
    Public Property OrigStatusId() As String
        Get
            Return _OrigStatusId
        End Get
        Set(ByVal Value As String)
            _OrigStatusId = Value
        End Set
    End Property
    Public Property NewStatusId() As String
        Get
            Return _NewStatusId
        End Get
        Set(ByVal Value As String)
            _NewStatusId = Value
        End Set
    End Property
    Public Property LeadId() As String
        Get
            Return _LeadId
        End Get
        Set(ByVal Value As String)
            _LeadId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region


End Class

