﻿''' <summary>
''' Store the GL Info.
''' </summary>
''' <remarks></remarks>
Public Class GeneralLedgerInfo

    ''' <summary>
    ''' GL account id. Use this to identify the account GL because description can be repeated.
    ''' and not unique.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property GlAccountId As String

    ''' <summary>
    ''' Create Date
    ''' </summary>
    ''' <value></value>
    Public Property CreateDate As String

    ''' <summary>
    ''' Use only in summary report
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property EndDate As String

    ''' <summary>
    ''' Transaction Code + Transaction Description + First Name + Last Name
    ''' </summary>
    ''' <value></value>
    Public Property Transaction As String

    ''' <summary>
    ''' Only the trans code description. This is used in summary view
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    Public Property TransactionDescrip As String

    ''' <summary>
    ''' General Ledge Account description
    ''' </summary>
    ''' <value></value>
    Public Property GlAccount As String

    ''' <summary>
    ''' The Transaction is a Debit
    ''' </summary>
    ''' <value></value>
    Public Property Debit As Decimal?

    ''' <summary>
    ''' The Transaction is a credit
    ''' </summary>
    ''' <value></value>
    Public Property Credit As Decimal?
End Class
