Public Class StuPhoneInfo
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _StudentID As String
    Private _StdPhoneId As String
    Private _Phone As String
    Private _phoneTypeId As String
    Private _phoneType As String
    Private _PhoneStatus As String
    Private _ForeignPhone As Integer
    Private _modUser As String
    Private _modDate As DateTime
    Private _Default1 As Integer
    Private _Extension As String
    Private _BestTime As String
#End Region
    Public Sub New()
        _isInDB = False
        _StdPhoneId = Guid.NewGuid.ToString()
        _Phone = ""
        _phoneTypeId = Guid.Empty.ToString()
        _PhoneType = ""
        _PhoneStatus = Guid.Empty.ToString()
        _Extension = ""
        _BestTime = ""
        _ForeignPhone = 0
        _Default1 = 0
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
    Public Property Phone() As String
        Get
            Return _Phone
        End Get
        Set(ByVal Value As String)
            _Phone = Value
        End Set
    End Property
    Public Property Extension() As String
        Get
            Return _Extension
        End Get
        Set(ByVal Value As String)
            _Extension = Value
        End Set
    End Property
    Public Property BestTime() As String
        Get
            Return _BestTime
        End Get
        Set(ByVal Value As String)
            _BestTime = Value
        End Set
    End Property
    Public Property PhoneType() As String
        Get
            Return _phoneType
        End Get
        Set(ByVal Value As String)
            _phoneType = Value
        End Set
    End Property
    Public Property PhoneTypeId() As String
        Get
            Return _phoneTypeId
        End Get
        Set(ByVal Value As String)
            _phoneTypeId = Value
        End Set
    End Property
    Public Property PhoneStatus() As String
        Get
            Return _PhoneStatus
        End Get
        Set(ByVal Value As String)
            _PhoneStatus = Value
        End Set
    End Property
    Public Property Default1() As Integer
        Get
            Return _Default1
        End Get
        Set(ByVal Value As Integer)
            _Default1 = Value
        End Set
    End Property

    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property

    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property StdPhoneId() As String
        Get
            Return _StdPhoneId
        End Get
        Set(ByVal Value As String)
            _StdPhoneId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property

    Public Property ForeignPhone() As Integer
        Get
            Return _ForeignPhone
        End Get
        Set(ByVal Value As Integer)
            _ForeignPhone = Value
        End Set
    End Property
    Public Property StudentID() As String
        Get
            Return _StudentID
        End Get
        Set(ByVal Value As String)
            _StudentID = Value
        End Set
    End Property
End Class
