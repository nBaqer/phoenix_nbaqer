﻿Public Class PeriodClassRoom
    'Implements IComparable
#Region "Private Variables"
    Private _periodId As String
    Private _periodDescrip As String
    Private _roomId As String
    Private _roomDescrip As String
#End Region
#Region "Public Constructors"
    Public Sub New()
    End Sub
    Public Sub New(ByVal pPeriodId As String, ByVal pPeriodDescrip As String, ByVal pRoomId As String, ByVal pRoomDescrip As String)
        _periodId = pPeriodId
        _periodDescrip = pPeriodDescrip
        _roomId = pRoomId
        _roomDescrip = pRoomDescrip
    End Sub
#End Region
#Region "Public Properties"
    Public Property PeriodId() As String
        Get
            Return _periodId
        End Get
        Set(ByVal value As String)
            _periodId = value
        End Set
    End Property
    Public Property PeriodDescrip() As String
        Get
            Return _periodDescrip
        End Get
        Set(ByVal value As String)
            _periodDescrip = value
        End Set
    End Property
    Public Property RoomId() As String
        Get
            Return _roomId
        End Get
        Set(ByVal value As String)
            _roomId = value
        End Set
    End Property
    Public Property RoomDescrip() As String
        Get
            Return _roomDescrip
        End Get
        Set(ByVal value As String)
            _roomDescrip = value
        End Set
    End Property

#End Region
#Region "Public Functions"
    'Public Overloads Function CompareTo(ByVal obj As Object) As Integer _
    'Implements IComparable.CompareTo
    '    Throw New ArgumentException("object is not an Period-ClassRoom")
    'End Function

#End Region
End Class

