Public Class ResultInfo
    '
    '   ResultInfo Class
    '
#Region " Private Variables and Objects"
    Private _errorString As String
    Private _updatedObject As Object
#End Region

#Region " Public Properties"

    Public Property ErrorString() As String
        Get
            Return _errorString
        End Get
        Set(ByVal Value As String)
            If UpdatedObject Is Nothing Then
                _errorString = Value
            Else
                'Do not allow it. 
                'ResultInfo can either have ErrorString or UpdatedObject set.
                Throw New System.Exception("ResultInfo object can either have ErrorString or UpdatedObject set, not both.")
            End If
        End Set
    End Property

    Public Property UpdatedObject() As Object
        Get
            Return _updatedObject
        End Get
        Set(ByVal Value As Object)
            If ErrorString = "" Then
                _updatedObject = Value
            Else
                'Do not allow it. 
                'ResultInfo can either have ErrorString or UpdatedObject set.
                Throw New System.Exception("ResultInfo object can either have ErrorString or UpdatedObject set, not both.")
            End If
        End Set
    End Property
#End Region

End Class
