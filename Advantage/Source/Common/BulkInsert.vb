﻿Imports System.Runtime.CompilerServices
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data.SqlClient

Public Module BulkInsert
    <Extension()>
    Public Function AsDataTable(Of T)(ByVal data As IEnumerable(Of T), Optional columnTypeMapping As Dictionary(Of String, String) = Nothing) As DataTable
        Dim properties As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))
        Dim table As DataTable = New DataTable()
        For Each prop As PropertyDescriptor In properties
            If (columnTypeMapping Is Nothing) Then
                table.Columns.Add(prop.Name, IIf(Nullable.GetUnderlyingType(prop.PropertyType) Is Nothing, prop.PropertyType, Nullable.GetUnderlyingType(prop.PropertyType)))
            ElseIf (Not columnTypeMapping.Keys.Contains(prop.Name)) Then
                table.Columns.Add(prop.Name, IIf(Nullable.GetUnderlyingType(prop.PropertyType) Is Nothing, prop.PropertyType, Nullable.GetUnderlyingType(prop.PropertyType)))
            Else
                table.Columns.Add(prop.Name, GetTypeFromString(columnTypeMapping(prop.Name)))
            End If
        Next
        For Each item As T In data
            Dim row As DataRow = table.NewRow()
            For Each prop As PropertyDescriptor In properties
                If (columnTypeMapping Is Nothing) Then
                    row(prop.Name) = IIf(prop.GetValue(item) Is Nothing, DBNull.Value, prop.GetValue(item))
                ElseIf (Not columnTypeMapping.Keys.Contains(prop.Name)) Then
                    row(prop.Name) = IIf(prop.GetValue(item) Is Nothing, DBNull.Value, prop.GetValue(item))
                Else
                    row(prop.Name) = GetCastedValueFromTypeString(prop.GetValue(item), columnTypeMapping(prop.Name))
                End If
            Next
            table.Rows.Add(row)
        Next
        Return table
    End Function

    Private Function GetTypeFromString(ByVal type As String) As Type
        Select Case type.ToLower()
            Case "string"
                Return GetType(String)
            Case "bool"
                Return GetType(Boolean)
            Case "guid"
                Return GetType(Guid)
            Case "integer"
                Return GetType(Integer)
            Case "long"
                Return GetType(Long)
            Case "datetime"
                Return GetType(DateTime)
            Case "shortdatetime"
                Return GetType(String)

        End Select
        Return GetType(Object)
    End Function
    Private Function GetCastedValueFromTypeString(ByVal value As Object, ByVal type As String) As Object
        If (value Is Nothing) Then
            Return DBNull.Value
        End If
        Select Case type.ToLower()
            Case "string"
                Dim returnValue = CType(value, String)
                If (String.IsNullOrEmpty(returnValue)) Then
                    Return DBNull.Value
                Else
                    Return returnValue
                End If
            Case "bool"
                Return Boolean.Parse(value)
            Case "guid"
                Return Guid.Parse(value)
            Case "integer"
                Return Integer.Parse(value)
            Case "long"
                Return Long.Parse(value)
            Case "datetime"
                Return DateTime.Parse(value)
            Case "shortdatetime"
                Return DateTime.Parse(value.ToString()).ToShortDateString()
        End Select
        Return value
    End Function

    Public Sub BuilkInsertDataTable(ByVal tableName As String, ByVal dataTable As DataTable, ByVal connection As SqlConnection, Optional tableMapping As Dictionary(Of String, String) = Nothing, Optional transaction As SqlTransaction = Nothing, Optional shouldCommitTransaction As Boolean = True)

        If (connection.State <> ConnectionState.Open) Then
            connection.Open()
        End If

        If (transaction Is Nothing) Then
            transaction = connection.BeginTransaction()
        End If
        Dim bulkCopy As SqlBulkCopy = New SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction)
        Using (bulkCopy)
            bulkCopy.BatchSize = 100
            bulkCopy.DestinationTableName = tableName
            Dim isThereAnError As Boolean = False
            Try
                If Not tableMapping Is Nothing Then
                    For Each key As String In tableMapping.Keys
                        bulkCopy.ColumnMappings.Add(tableMapping(key), key)
                    Next
                End If

                bulkCopy.WriteToServer(dataTable)
                If (shouldCommitTransaction) Then
                    transaction.Commit()
                End If
            Catch exeption As Exception
                transaction.Rollback()
                isThereAnError = True
                Throw
            Finally
                If (shouldCommitTransaction Or isThereAnError) Then
                    connection.Close()
                End If
            End Try

        End Using

    End Sub
End Module
