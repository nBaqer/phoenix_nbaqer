﻿Namespace Tables

    ''' <summary>
    ''' Class to register the information of one student
    ''' This register, grade components, grade final and Attendance
    ''' </summary>
    Public Class CorrectionRegInfo

        ''' <summary>
        ''' Create a instance of CorrectionRegInfo
        ''' </summary>
        ''' <returns></returns>
        Public Shared Function Factory() As CorrectionRegInfo
            Dim reg As CorrectionRegInfo = New CorrectionRegInfo()
            reg.ArInfoList = New List(Of ArResultsClass)()
            reg.AttendanceList = New List(Of AtClsSectAttendance)()
            reg.GradeBookList = New List(Of GradeBookResultInfo)()
            Return reg
        End Function

        ''' <summary>
        ''' The record in ArResults
        ''' </summary>
        Public Property ArInfoList As IList(Of ArResultsClass)

      ''' <summary>
        ''' The records in Grade-book.
        ''' </summary>
        Public Property GradeBookList As IList(Of GradeBookResultInfo)

        ''' <summary>
        ''' The attendance Records
        ''' </summary>
        Public Property AttendanceList As IList(Of AtClsSectAttendance)

        ''' <summary>
        ''' This field is only used to store the enrollments referred to this class
        ''' </summary>
        Public Property Enrollment As String
    End Class
End Namespace