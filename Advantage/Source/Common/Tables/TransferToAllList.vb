﻿Namespace Tables
    ''' <summary>
    ''' Info for copy all transfer information from one enrollment to other.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class TransferToAllList
        Public Property EnrollmentList() As List(Of String)
        Public Property TransferRecord As ArTransferGrades
    End Class
End Namespace