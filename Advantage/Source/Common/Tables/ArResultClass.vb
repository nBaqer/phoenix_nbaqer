﻿Namespace Tables
    <Serializable> _
    Public Class ArResultsClass
        ''' <summary>
        ''' Record where we take the information
        ''' </summary>
        Public Property TestId As String

        Public Property Score As Decimal?

        Public Property GrdSysDetailId As String

        Public Property Cnt As Integer?

        Public Property Hours As Integer?

        Public Property EnrollmentId As String

        Public Property IsIncomplete As Boolean?

        Public Property DroppedInAddDrop As Boolean

        Public Property ModUser As String

        Public Property ModDate As DateTime?

        Public Property IsTransferred As Boolean?

        Public Property IsClinicsSatisfied As Boolean?

        Public Property DateDetermined As Date?

        Public Property IsCourseCompleted As Boolean

        Public Property IsGradeOverriden As Boolean

        Public Property GradeOverridenBy As String

        Public Property GradeOverridenDate As Date?

        ' This two property does not go to the ArResults table
        Public Property CourseCode As String

        Public Property Grade As String

        Public Property TermCode As String

        Public Property TermDescription As String

        public Property  ResultId () As String
        
    End Class
End Namespace