﻿Namespace Tables
    ''' <summary>
    '''  Hold information from ArGrdBkResults Table.
    ''' </summary>
    <Serializable>
    Public Class GradeBookResultInfo
        Public Property ClsSectionId As Guid

        Public Property InstrGrdBkWgtDetailId As Guid

        Public Property Score As Decimal?

        Public Property Comments As String

        Public Property StuEnrollId As Guid

        Public Property ModUser As String

        Public Property ModDate As Date

        Public Property ResNum As Integer

        Public Property PostDate As Date?

        Public Property IsCompGraded As Boolean?

        Public Property IsCourseCredited As Boolean?

        Public Property GrdBkResultId As Guid
    End Class

End Namespace