﻿Namespace Tables
    <Serializable()>
    Public Class TransferRequirement

        Public Property ReqId As String
        Public Property TermId As String
        Public Property StuEnrollId As String
        Public Property Grade As String
        Public Property Score As Decimal
        Public Property ChangeData As DateTime
        Public Property ReqCode As String
        Public Property StudentId As String
        Public Property CourseDescription As String


        ''' <summary>
        ''' This property is used to know if the item is selected in the interface
        ''' </summary>
        Public Property IsChecked As Boolean

        ''' <summary>
        ''' User Name as String
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        Public Property ModName As String

        Public Property MessageToWorld As String

    End Class
End Namespace