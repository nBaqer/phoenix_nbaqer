﻿Namespace Tables
    ''' <summary>
    ''' Store a record of the table AtClsSectAtendance
    ''' </summary>
    <Serializable> _
    Public Class AtClsSectAttendance

        Public Property ClsSectAttId As String

        Public Property ClsSectionId As String

        Public Property ClsSectMeetingId As String

        Public Property MeetDate As Date

        Public Property Actual As Decimal

        Public Property Tardy As Boolean

        Public Property Excused As Boolean

        Public Property Comments As String

        Public Property StuEnrollId As String

        Public Property Scheduled As Decimal?
    End Class
End Namespace