﻿Namespace Tables
    <Serializable()>
    Public Class ArTransferGrades
        ''' <summary>
        ''' Record where we take the information
        ''' </summary>
        Public Property TransferId As String

        Public Property EnrollmentId As String

        Public Property ReqId As String

        Public Property GrdSysDetailId As String

        Public Property TermId As String

        Public Property ModDate As Date

        Public Property ModUser As String

        Public Property Score As Decimal?

        Public Property IsTransferred As Boolean

        Public Property IsClinicsSatisfied As Boolean

        Public Property IsCourseCompleted As Boolean

        Public Property IsGradeOverriden As Boolean

        Public Property GradeOverridenBy As String

        Public Property GradeOverridenDate As Date


        Public Property CourseCode As String

        Public Property Grade As String

        Public Property TermCode As String

        Public Property TermDescription As String
    End Class

End Namespace