﻿Imports System.IO
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary

Namespace Tables

    ''' <summary>
    ''' Reference Article http://www.codeproject.com/KB/tips/SerializedObjectCloner.aspx
    ''' Provides a method for performing a deep copy of an object.
    ''' Binary Serialization is used to perform the copy.
    ''' </summary>
    Public NotInheritable Class ObjectCopier
        Private Sub New()
        End Sub
        ''' <summary>
        ''' Perform a deep Copy of the object.
        ''' </summary>
        ''' <typeparam name="T">The type of object being copied.</typeparam>
        ''' <param name="source">The object instance to copy.</param>
        ''' <returns>The copied object.</returns>
       Public Shared Function Clone(Of T)(source As T) As T
            If Not GetType(T).IsSerializable Then
                Throw New ArgumentException("The type must be serialize.", "source")
            End If

            ' Don't serialize a null object, simply return the default for that object
            If ReferenceEquals(source, Nothing) Then
                Return Nothing
            End If

            Dim formatter As IFormatter = New BinaryFormatter()
            Dim stream As Stream = New MemoryStream()
            Using stream
                formatter.Serialize(stream, source)
                stream.Seek(0, SeekOrigin.Begin)
                Return DirectCast(formatter.Deserialize(stream), T)
            End Using
        End Function
    End Class
End Namespace