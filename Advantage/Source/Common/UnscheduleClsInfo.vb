Public Class UnscheduleClsInfo
#Region "Private Variable Declaration"
    Private _UnschedClosureId As String
    Private _ClsSectId As String
    Private _StartDate As Date
    Private _EndDate As Date
    Private _modUser As String
    Private _modDate As DateTime
#End Region

#Region "UnschedClsInfo Properties"
    Public Property UnschedClosureId() As String
        Get
            Return _UnschedClosureId
        End Get
        Set(ByVal Value As String)
            _UnschedClosureId = Value
        End Set
    End Property

    Public Property ClsSectId() As String
        Get
            Return _ClsSectId
        End Get
        Set(ByVal Value As String)
            _ClsSectId = Value
        End Set
    End Property


    Public Property StartDate() As Date
        Get
            Return _StartDate
        End Get
        Set(ByVal Value As Date)
            _StartDate = Value
        End Set
    End Property

    Public Property EndDate() As Date
        Get
            Return _EndDate
        End Get
        Set(ByVal Value As Date)
            _EndDate = Value
        End Set
    End Property

    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
