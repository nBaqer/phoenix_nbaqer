Public Class DateFrame
#Region "Private Members"

    Private m_refDate As DateTime

#End Region
#Region "Constructor"

    Public Sub New(ByVal RefDate As DateTime)
        MyBase.New()
        m_refDate = RefDate
    End Sub

#End Region
#Region "Public Properties"

    Public ReadOnly Property ReferenceDate() As DateTime
        Get
            Return m_refDate
        End Get
    End Property

    Public ReadOnly Property StartDate() As DateTime
        Get
            Return New DateTime(ReferenceDate.Year, ReferenceDate.Month, 1)
        End Get
    End Property

    Public ReadOnly Property EndDate() As DateTime
        Get
            Dim LastDayOfMonth As Integer = DateTime.DaysInMonth(ReferenceDate.Year, ReferenceDate.Month)
            Return New DateTime(ReferenceDate.Year, ReferenceDate.Month, LastDayOfMonth)
        End Get
    End Property

#End Region
End Class
