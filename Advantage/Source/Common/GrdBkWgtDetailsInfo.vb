Public Class GrdBkWgtDetailsInfo
#Region "Private Variable Declaration"
    Private _Code As String
    Private _Descrip As String
    Private _Weight As Integer
    Private _GrdBkWgtDetailId As String
    Private _GrdCompTypeId As String
    Private _GrdBkWgtId As String
    Private _Seq As Integer
    Private _modUser As String
    Private _modDate As DateTime
    Private _CreditsPerService As Decimal
#End Region
#Region " Public Constructor"
    Public Sub New()

        _modUser = ""
        _modDate = Date.MinValue
        _CreditsPerService = 0.0
    End Sub
#End Region
#Region "GrdBk Wgts Properties"


    Public Property GrdBkWgtId() As String
        Get
            Return _GrdBkWgtId
        End Get
        Set(ByVal Value As String)
            _GrdBkWgtId = Value
        End Set
    End Property
    Public Property GrdBkWgtDetailId() As String
        Get
            Return _GrdBkWgtDetailId
        End Get
        Set(ByVal Value As String)
            _GrdBkWgtDetailId = Value
        End Set
    End Property
    Public Property GrdCompTypeId() As String
        Get
            Return _GrdCompTypeId
        End Get
        Set(ByVal Value As String)
            _GrdCompTypeId = Value
        End Set
    End Property

    Public Property Code() As String
        Get
            Return _Code
        End Get
        Set(ByVal Value As String)
            _Code = Value
        End Set
    End Property
    Public Property Descrip() As String
        Get
            Return _Descrip
        End Get
        Set(ByVal Value As String)
            _Descrip = Value
        End Set
    End Property
    Public Property weight() As Integer
        Get
            Return _Weight
        End Get
        Set(ByVal Value As Integer)
            _Weight = Value
        End Set
    End Property

    Public Property Seq() As Integer
        Get
            Return _Seq
        End Get
        Set(ByVal Value As Integer)
            _Seq = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
   
#End Region
End Class
