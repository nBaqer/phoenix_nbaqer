﻿Namespace Reports
    Public Class ReportServerCatalog

        ''' <summary>
        ''' Gets or sets Real path of the report inside report server
        ''' </summary>
        ''' <returns>path to report server</returns>
        Public Property Path() As String

        ''' <summary>
        ''' Gets or sets Report or folder name
        ''' </summary>
        ''' <returns></returns>
        Public Property Name() As String

        ''' <summary>
        ''' Gets or sets Type 1: Folder 2: Report
        ''' </summary>
        ''' <returns></returns>
        Public Property Type() As Int32

        ''' <summary>
        ''' Report Description
        ''' </summary>
        ''' <returns></returns>
        Public Property Description() As String

    End Class

End Namespace