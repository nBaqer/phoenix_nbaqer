
Public Class CourseGrpInfo
#Region "Private Variable Declarations"
    Private _CourseGrpId As Guid
    Private _CourseGrpDescrip As String
    Private _CourseGrpCode As String
    Private _CourseGrpHours As Decimal
    Private _CourseGrpCredits As Decimal
    Private _CampGrpId As Guid
    Private _StatusId As Guid
    Private _ReqTypeId As Integer
    Private _modUser As String
    Private _modDate As DateTime
    Private _CampGrpDescrip As String
#End Region
#Region " Public Constructor"
    Public Sub New()
        _modUser = ""
        _modDate = Date.MinValue
        _CampGrpDescrip = ""
    End Sub
#End Region
#Region "CourseGrp Info Properties"

    Public Property CampGrpId() As Guid
        Get
            Return _CampGrpId
        End Get
        Set(ByVal Value As Guid)
            _CampGrpId = Value
        End Set
    End Property
    Public Property ReqTypeId() As Integer
        Get
            Return _ReqTypeId
        End Get
        Set(ByVal Value As Integer)
            _ReqTypeId = Value
        End Set
    End Property
    Public Property CourseGrpId() As Guid
        Get
            Return _CourseGrpId
        End Get
        Set(ByVal Value As Guid)
            _CourseGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _CampGrpDescrip
        End Get
        Set(ByVal Value As String)
            _CampGrpDescrip = Value
        End Set
    End Property
    Public Property CourseGrpCode() As String
        Get
            Return _CourseGrpCode
        End Get
        Set(ByVal Value As String)
            _CourseGrpCode = Value
        End Set
    End Property
    Public Property CourseGrpDescrip() As String
        Get
            Return _CourseGrpDescrip
        End Get
        Set(ByVal Value As String)
            _CourseGrpDescrip = Value
        End Set
    End Property
    Public Property CourseGrpHours() As Decimal
        Get
            Return _CourseGrpHours
        End Get
        Set(ByVal Value As Decimal)
            _CourseGrpHours = Value
        End Set
    End Property

    Public Property CourseGrpCredits() As Decimal
        Get
            Return _CourseGrpCredits
        End Get
        Set(ByVal Value As Decimal)
            _CourseGrpCredits = Value
        End Set
    End Property
    Public Property StatusId() As Guid
        Get
            Return _StatusId
        End Get
        Set(ByVal Value As Guid)
            _StatusId = Value
        End Set
    End Property
    Public Property ModDate() As DateTime
        Get
            Return _modDate
        End Get
        Set(ByVal Value As DateTime)
            _modDate = Value
        End Set
    End Property

    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
#End Region
End Class

