' ===============================================================================
' TMInfo.vb
' Info classes for the TM project
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' 11/2/06 - (BEN) Added support for StartGap to ResultTaskInfo

Namespace TM

#Region "Categories"
    ''' <summary>
    ''' Info class for tmCategories
    ''' </summary>
    ''' <remarks></remarks>
    Public Class CategoryInfo
#Region " Variables "
        Private _isInDB As Boolean
        Private _categoryId As String    ' guid
        Private _code As String
        Private _descrip As String
        Private _campGroupId As String     ' guid, link to campus group
        Private _modDate As DateTime
        Private _modUser As String
        Private _active As Boolean
#End Region
#Region " Constructor "
        Public Sub New()
            _isInDB = False
            _categoryId = Guid.NewGuid.ToString ' create one by default
            _active = True
        End Sub
#End Region
#Region " Properties Get/Set "
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property CategoryId() As String
            Get
                Return Me._categoryId
            End Get
            Set(ByVal value As String)
                Me._categoryId = value
            End Set
        End Property
        Public Property Code() As String
            Get
                Return Me._code
            End Get
            Set(ByVal value As String)
                Me._code = value
            End Set
        End Property
        Public Property Descrip() As String
            Get
                Return Me._descrip
            End Get
            Set(ByVal value As String)
                Me._descrip = value
            End Set
        End Property
        Public Property CampGroupId() As String
            Get
                Return Me._campGroupId
            End Get
            Set(ByVal value As String)
                Me._campGroupId = value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As DateTime)
                Me._modDate = value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return Me._active
            End Get
            Set(ByVal value As Boolean)
                Me._active = value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "Permissions"
    ''' <summary>
    ''' Info class for the tmPermissions table
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Public Class PermissionsInfo

#Region "Variables "
        Private _isInDB As Boolean
        Private _sID As String           ' guid
        Private _userId As String        ' guid
        Private _roleId As String        ' guid
        Private _campGrpId As String     ' guid
#End Region

#Region " Constructor "
        Public Sub New()
            _isInDB = False
            _sID = Guid.NewGuid.ToString
        End Sub
#End Region

#Region " Properties Get/Set "

        Public Property IsInDB() As Boolean
            Get
                Return Me._isInDB
            End Get
            Set(ByVal value As Boolean)
                Me._isInDB = value
            End Set
        End Property
        Public Property SID() As String
            Get
                Return Me._sID
            End Get
            Set(ByVal value As String)
                Me._sID = value
            End Set
        End Property

        Public Property UserId() As String
            Get
                Return Me._userId
            End Get
            Set(ByVal value As String)
                Me._userId = value
            End Set
        End Property

        Public Property CampGrpId() As String
            Get
                Return Me._campGrpId
            End Get
            Set(ByVal value As String)
                Me._campGrpId = value
            End Set
        End Property


        Public Property RoleId() As String
            Get
                Return Me._roleId
            End Get
            Set(ByVal value As String)
                Me._roleId = value
            End Set
        End Property
#End Region

    End Class
#End Region

#Region "Results"
    Public Class ResultInfo
#Region " Variables "
        Private _isInDB As Boolean
        Private _resultId As String          ' guid
        Private _code As String
        Private _descrip As String       ' 
        Private _resultActionId As String    ' to update ext tables
        Private _resultActionTableName As String
        Private _resultActionColumnName As String
        Private _resultActionRowGuid As String
        Private _resultActionValue As String    ' to update ext tables
        Private _active As Boolean
        Private _modDate As DateTime
        Private _modUser As String
#End Region

#Region " Constructor "
        Public Sub New()
            _isInDB = False
            _resultId = Guid.NewGuid.ToString
            _descrip = Nothing
            _active = True
        End Sub
#End Region

#Region " Properties Get/Set "
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property ResultId() As String
            Get
                Return Me._resultId
            End Get
            Set(ByVal value As String)
                Me._resultId = value
            End Set
        End Property
        Public Property Code() As String
            Get
                Return Me._code
            End Get
            Set(ByVal value As String)
                Me._code = value
            End Set
        End Property
        Public Property Descrip() As String
            Get
                Return Me._descrip
            End Get
            Set(ByVal value As String)
                Me._descrip = value
            End Set
        End Property
        Public Property ResultActionId() As String
            Get
                Return Me._resultActionId
            End Get
            Set(ByVal value As String)
                Me._resultActionId = value
            End Set
        End Property
        Public Property ResultActionTableName() As String
            Get
                Return Me._resultActionTableName
            End Get
            Set(ByVal value As String)
                Me._resultActionTableName = value
            End Set
        End Property
        Public Property ResultActionColumnName() As String
            Get
                Return Me._resultActionColumnName
            End Get
            Set(ByVal value As String)
                Me._resultActionColumnName = value
            End Set
        End Property
        Public Property ResultActionValue() As String
            Get
                Return Me._resultActionValue
            End Get
            Set(ByVal value As String)
                Me._resultActionValue = value
            End Set
        End Property
        Public Property ResultActionRowGuid() As String
            Get
                Return Me._resultActionRowGuid
            End Get
            Set(ByVal value As String)
                Me._resultActionRowGuid = value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As DateTime)
                Me._modDate = value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return Me._active
            End Get
            Set(ByVal value As Boolean)
                Me._active = value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "ResultTaskInfo"
    Public Class ResultTaskInfo
#Region " Variables "
        Private _isInDB As Boolean
        Private _resultTaskId As String
        Private _resultId As String
        Private _taskId As String
        Private _taskDescrip As String
        Private _taskOrder As Integer
        Private _startGap As Integer
        Private _startGapUnit As String
        Private _resultDescrip As String
        Private _resultActionId As String
        Private _resultActionTableName As String
        Private _resultActionColumnName As String
        Private _resultActionValue As String
        Private _active As Boolean
#End Region
#Region " Constructor "
        Public Sub New()
            _isInDB = False
            _resultTaskId = Guid.NewGuid.ToString
            _startGap = 0
            _startGapUnit = "day"
        End Sub
#End Region
#Region " Protperties Get / Set "
        Public Property IsInDB() As Boolean
            Get
                Return Me._isInDB
            End Get
            Set(ByVal value As Boolean)
                Me._isInDB = value
            End Set
        End Property
        Public Property ResultTaskId() As String
            Get
                Return Me._resultTaskId
            End Get
            Set(ByVal value As String)
                Me._resultTaskId = value
            End Set
        End Property
        Public Property ResultId() As String
            Get
                Return Me._resultId
            End Get
            Set(ByVal value As String)
                Me._resultId = value
            End Set
        End Property
        Public Property TaskId() As String
            Get
                Return Me._taskId
            End Get
            Set(ByVal value As String)
                Me._taskId = value
            End Set
        End Property
        Public Property TaskDescrip() As String
            Get
                Return Me._taskDescrip
            End Get
            Set(ByVal value As String)
                Me._taskDescrip = value
            End Set
        End Property
        Public Property TaskOrder() As Integer
            Get
                Return Me._taskOrder
            End Get
            Set(ByVal value As Integer)
                Me._taskOrder = value
            End Set
        End Property
        Public Property StartGap() As Integer
            Get
                Return Me._startGap
            End Get
            Set(ByVal value As Integer)
                Me._startGap = value
            End Set
        End Property
        Public Property StartGapUnit() As String
            Get
                Return Me._startGapUnit
            End Get
            Set(ByVal value As String)
                Me._startGapUnit = value
            End Set
        End Property
        Public Property ResultDescrip() As String
            Get
                Return Me._resultDescrip
            End Get
            Set(ByVal value As String)
                Me._resultDescrip = value
            End Set
        End Property
        Public Property ResultActionId() As String
            Get
                Return Me._resultActionId
            End Get
            Set(ByVal value As String)
                Me._resultActionId = value
            End Set
        End Property
        Public Property ResultActionTableName() As String
            Get
                Return Me._resultActionTableName
            End Get
            Set(ByVal value As String)
                Me._resultActionTableName = value
            End Set
        End Property
        Public Property ResultActionColumnName() As String
            Get
                Return Me._resultActionColumnName
            End Get
            Set(ByVal value As String)
                Me._resultActionColumnName = value
            End Set
        End Property
        Public Property ResultActionValue() As String
            Get
                Return Me._resultActionValue
            End Get
            Set(ByVal value As String)
                Me._resultActionValue = value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return Me._active
            End Get
            Set(ByVal value As Boolean)
                Me._active = value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "TaskResultInfo"
    Public Class TaskResultInfo
#Region " Variables "
        Private _isInDB As Boolean
        Private _taskResultId As String  ' guid
        Private _taskId As String        ' guid, link to tmTask
        Private _resultId As String      ' guid, link to tmResults
        Private _resultActionId As String
        Private _resultActionValue As String
        Private _descrip As String
        Private _externalAction As String
        Private _active As Boolean
#End Region

#Region " Constructor "
        Public Sub New()
            _isInDB = False
            _taskResultId = Guid.NewGuid.ToString
            _active = True
        End Sub
#End Region

#Region " Properties - Get/Set "
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property TaskResultId() As String
            Get
                Return Me._taskResultId
            End Get
            Set(ByVal value As String)
                Me._taskResultId = value
            End Set
        End Property
        Public Property TaskId() As String
            Get
                Return Me._taskId
            End Get
            Set(ByVal value As String)
                Me._taskId = value
            End Set
        End Property
        Public Property ResultId() As String
            Get
                Return Me._resultId
            End Get
            Set(ByVal value As String)
                Me._resultId = value
            End Set
        End Property
        Public Property Descrip() As String
            Get
                Return Me._descrip
            End Get
            Set(ByVal value As String)
                Me._descrip = value
            End Set
        End Property
        Public Property ResultActionId() As String
            Get
                Return Me._resultActionId
            End Get
            Set(ByVal value As String)
                Me._resultActionId = value
            End Set
        End Property
        Public Property ResultActionValue() As String
            Get
                Return Me._resultActionValue
            End Get
            Set(ByVal value As String)
                Me._resultActionValue = value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return Me._active
            End Get
            Set(ByVal value As Boolean)
                Me._active = value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "Tasks"
    Public Class TaskInfo

#Region " Variables "
        Private _isInDB As Boolean
        Private _taskId As String        'guid
        Private _code As String
        Private _descrip As String
        Private _campGroupId As String 'guid, link to Campus group
        Private _categoryId As String 'guid, link to tmCategory
        Private _categoryDescrip As String
        Private _moduleEntityId As String
        Private _moduleId As String 'guid, link to syModules
        Private _moduleName As String
        Private _entityId As String 'guid, link to syModuleEntity
        Private _entityName As String
        Private _modDate As DateTime
        Private _modUser As String
        Private _active As Boolean
        Private _children As List(Of TaskInfo)
        Private _childrenResults As List(Of String)
        Private _statusId As String 'guid, link to syStatusCodes
        Private _statusName As String
#End Region

#Region " Constructor "
        Public Sub New()
            _isInDB = False
            _taskId = Guid.NewGuid.ToString ' create one by default            
            _active = True

            _children = New List(Of TaskInfo)
            _childrenResults = New List(Of String)
        End Sub
#End Region

#Region " Properties - Get/Set "
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property TaskId() As String
            Get
                Return Me._taskId
            End Get
            Set(ByVal value As String)
                Me._taskId = value
            End Set
        End Property
        Public Property Code() As String
            Get
                Return Me._code
            End Get
            Set(ByVal value As String)
                Me._code = value
            End Set
        End Property
        Public Property Descrip() As String
            Get
                Return Me._descrip
            End Get
            Set(ByVal value As String)
                Me._descrip = value
            End Set
        End Property
        Public Property CampGroupId() As String
            Get
                Return Me._campGroupId
            End Get
            Set(ByVal value As String)
                Me._campGroupId = value
            End Set
        End Property
        Public Property CategoryId() As String
            Get
                Return Me._categoryId
            End Get
            Set(ByVal value As String)
                Me._categoryId = value
            End Set
        End Property
        Public Property CategoryDescrip() As String
            Get
                Return Me._categoryDescrip
            End Get
            Set(ByVal value As String)
                Me._categoryDescrip = value
            End Set
        End Property
        Public Property ModuleEntityId() As String
            Get
                Return Me._moduleEntityId
            End Get
            Set(ByVal value As String)
                Me._moduleEntityId = value
            End Set
        End Property
        Public Property ModuleId() As String
            Get
                Return Me._moduleId
            End Get
            Set(ByVal value As String)
                Me._moduleId = value
            End Set
        End Property
        Public Property ModuleName() As String
            Get
                Return Me._moduleName
            End Get
            Set(ByVal value As String)
                Me._moduleName = value
            End Set
        End Property
        Public Property EntityId() As String
            Get
                Return Me._entityId
            End Get
            Set(ByVal value As String)
                Me._entityId = value
            End Set
        End Property
        Public Property EntityName() As String
            Get
                Return Me._entityName
            End Get
            Set(ByVal value As String)
                Me._entityName = value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As DateTime)
                Me._modDate = value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return Me._active
            End Get
            Set(ByVal value As Boolean)
                Me._active = value
            End Set
        End Property
        Public ReadOnly Property Children() As List(Of TaskInfo)
            Get
                Return _children
            End Get
        End Property
        Public ReadOnly Property ChildrenResults() As List(Of String)
            Get
                Return _childrenResults
            End Get
        End Property
        Public Property StatusId() As String
            Get
                Return Me._statusId
            End Get
            Set(ByVal value As String)
                Me._statusId = value
            End Set
        End Property
        Public Property StatusName() As String
            Get
                Return Me._statusName
            End Get
            Set(ByVal value As String)
                Me._statusName = value
            End Set
        End Property
#End Region

    End Class
#End Region

#Region "UserTasks"
    Public Enum TaskStatus
        Pending = 0
        Cancelled = 1
        Completed = 2
        None = -1
    End Enum

    Public Class UserTaskInfo
#Region " Variables "
        Private _isInDB As Boolean
        Private _userTaskId As String    ' guid 
        Private _taskId As String        ' guid
        Private _taskDescrip As String
        Private _startDate As String
        Private _endDate As String
        Private _priority As Integer
        Private _message As String
        Private _reId As String          ' guid
        Private _reName As String
        Private _ownerId As String       ' guid
        Private _ownerName As String
        Private _assignedById As String  ' guid
        Private _assignedByName As String
        Private _prevUserTaskId As String    ' guid
        Private _resultId As String  ' guid
        Private _resultDescrip As String
        Private _categoryId As String
        Private _categoryDescrip As String
        Private _prevStatus As String
        Private _status As TaskStatus
        Private _moduleEntityId As String
        Private _moduleId As String
        Private _moduleName As String
        Private _entityId As String
        Private _entityName As String
        Private _modDate As DateTime
        Private _modUser As String
        Private _campGrpID As String
#End Region
#Region " Constructor"
        Public Sub New()
            _isInDB = False
            _userTaskId = Guid.NewGuid.ToString
            _priority = 0
            _startDate = Date.MinValue
            _endDate = Date.MinValue
        End Sub
#End Region
#Region " Properties Get/Set "
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property UserTaskId() As String
            Get
                Return Me._userTaskId
            End Get
            Set(ByVal value As String)
                Me._userTaskId = value
            End Set
        End Property
        Public Property TaskId() As String
            Get
                Return Me._taskId
            End Get
            Set(ByVal value As String)
                Me._taskId = value
            End Set
        End Property
        Public Property TaskDescrip() As String
            Get
                Return Me._taskDescrip
            End Get
            Set(ByVal value As String)
                Me._taskDescrip = value
            End Set
        End Property
        Public Property StartDate() As String
            Get
                Return Me._startDate
            End Get
            Set(ByVal value As String)
                Me._startDate = value
            End Set
        End Property
        Public Property EndDate() As String
            Get
                Return Me._endDate
            End Get
            Set(ByVal value As String)
                Me._endDate = value
            End Set
        End Property
        Public Property Priority() As Integer
            Get
                Return Me._priority
            End Get
            Set(ByVal value As Integer)
                Me._priority = value
            End Set
        End Property
        Public Property Message() As String
            Get
                Return Me._message
            End Get
            Set(ByVal value As String)
                Me._message = value
            End Set
        End Property
        Public Property ReId() As String
            Get
                Return Me._reId
            End Get
            Set(ByVal value As String)
                Me._reId = value
            End Set
        End Property
        Public Property ReName() As String
            Get
                Return Me._reName
            End Get
            Set(ByVal value As String)
                Me._reName = value
            End Set
        End Property
        Public Property OwnerId() As String
            Get
                Return Me._ownerId
            End Get
            Set(ByVal value As String)
                Me._ownerId = value
            End Set
        End Property
        Public Property OwnerName() As String
            Get
                Return Me._ownerName
            End Get
            Set(ByVal value As String)
                Me._ownerName = value
            End Set
        End Property
        Public Property AssignedById() As String
            Get
                Return Me._assignedById
            End Get
            Set(ByVal value As String)
                Me._assignedById = value
            End Set
        End Property
        Public Property AssignedByName() As String
            Get
                Return Me._assignedByName
            End Get
            Set(ByVal value As String)
                Me._assignedByName = value
            End Set
        End Property
        Public Property PrevUserTaskId() As String
            Get
                Return Me._prevUserTaskId
            End Get
            Set(ByVal value As String)
                Me._prevUserTaskId = value
            End Set
        End Property
        Public Property ResultId() As String
            Get
                Return Me._resultId
            End Get
            Set(ByVal value As String)
                Me._resultId = value
            End Set
        End Property
        Public Property ResultDescrip() As String
            Get
                Return Me._resultDescrip
            End Get
            Set(ByVal value As String)
                Me._resultDescrip = value
            End Set
        End Property
        Public Property CategoryId() As String
            Get
                Return Me._categoryId
            End Get
            Set(ByVal value As String)
                Me._categoryId = value
            End Set
        End Property
        Public Property CategoryDescrip() As String
            Get
                Return Me._categoryDescrip
            End Get
            Set(ByVal value As String)
                Me._categoryDescrip = value
            End Set
        End Property
        Public Property Status() As TaskStatus
            Get
                Return Me._status
            End Get
            Set(ByVal value As TaskStatus)
                Me._status = value
            End Set
        End Property
        Public ReadOnly Property StatusString() As String
            Get
                If _status = TaskStatus.Cancelled Then
                    Return "Cancelled"
                ElseIf _status = TaskStatus.Pending Then
                    Return "Pending"
                ElseIf _status = TaskStatus.Completed Then
                    Return "Completed"
                End If
                Return ""
            End Get
        End Property
        Public Property PrevStatus() As TaskStatus
            Get
                Return Me._prevStatus
            End Get
            Set(ByVal value As TaskStatus)
                Me._prevStatus = value
            End Set
        End Property
        Public Property ModuleEntityId() As String
            Get
                Return Me._moduleEntityId
            End Get
            Set(ByVal value As String)
                Me._moduleEntityId = value
            End Set
        End Property
        Public Property ModuleId() As String
            Get
                Return Me._moduleId
            End Get
            Set(ByVal value As String)
                Me._moduleId = value
            End Set
        End Property
        Public Property ModuleName() As String
            Get
                Return Me._moduleName
            End Get
            Set(ByVal value As String)
                Me._moduleName = value
            End Set
        End Property
        Public Property EntityId() As String
            Get
                Return Me._entityId
            End Get
            Set(ByVal value As String)
                Me._entityId = value
            End Set
        End Property
        Public Property EntityName() As String
            Get
                Return Me._entityName
            End Get
            Set(ByVal value As String)
                Me._entityName = value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As DateTime)
                Me._modDate = value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property
        Public Property CampGrpID() As String
            Get
                Return _campGrpID
            End Get
            Set(ByVal value As String)
                _campGrpID = value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "Workflow"
    ''' <summary>
    ''' Info class for the tmWorkflow table
    ''' </summary>
    ''' <remarks></remarks>
    Public Class WorkflowInfo
#Region " Variables "
        Private _isInDB As Boolean
        Private _workflowId As String        ' guid
        Private _name As String
        Private _descrip As String
        Private _cmpGroupId As String     ' guid, link to the campusgroup tbl
        Private _moduleEntityId As String    ' guid, link to syModuleEntities
        Private _startTaskId As String       ' guid, link to tmTask
        Private _modDate As DateTime
        Private _modUser As String
        Private _active As Boolean
#End Region
#Region " Constructor "
        Public Sub New()
            _workflowId = Guid.NewGuid.ToString
            _name = String.Empty
            _descrip = String.Empty
            _cmpGroupId = String.Empty
            _moduleEntityId = String.Empty
            _startTaskId = String.Empty
            _modDate = Nothing
            _modUser = String.Empty
            _active = False
        End Sub
#End Region
#Region " Properties Get/Set "

        Public Property IsInDB() As Boolean
            Get
                Return Me._isInDB
            End Get
            Set(ByVal value As Boolean)
                Me._isInDB = value
            End Set
        End Property
        Public Property WorkflowId() As String
            Get
                Return Me._workflowId
            End Get
            Set(ByVal value As String)
                Me._workflowId = value
            End Set
        End Property

        Public Property Name() As String
            Get
                Return Me._name
            End Get
            Set(ByVal value As String)
                Me._name = value
            End Set
        End Property

        Public Property Description() As String
            Get
                Return Me._descrip
            End Get
            Set(ByVal value As String)
                Me._descrip = value
            End Set
        End Property

        Public Property CampusGroupId() As String
            Get
                Return Me._cmpGroupId
            End Get
            Set(ByVal value As String)
                Me._cmpGroupId = value
            End Set
        End Property

        Public Property ModuleEntityId() As String
            Get
                Return Me._moduleEntityId
            End Get
            Set(ByVal value As String)
                Me._moduleEntityId = value
            End Set
        End Property

        Public Property StartTaskId() As String
            Get
                Return Me._startTaskId
            End Get
            Set(ByVal value As String)
                Me._startTaskId = value
            End Set
        End Property

        Public Property ModifyDate() As DateTime
            Get
                Return Me._modDate
            End Get
            Set(ByVal value As DateTime)
                Me._modDate = value
            End Set
        End Property

        Public Property ModifyUser() As String
            Get
                Return Me._modUser
            End Get
            Set(ByVal value As String)
                Me._modUser = value
            End Set
        End Property

        Public Property IsActive() As Boolean
            Get
                Return Me._active
            End Get
            Set(ByVal value As Boolean)
                Me._active = value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "Input Masks"
    Public Class InputMaskInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _InputMaskId As String
        Private _Item As String
        Private _Mask As String
        Private _modUser As String
        Private _modDate As DateTime
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _InputMaskId = Guid.NewGuid.ToString
            _Item = ""
            _Mask = ""
            _modUser = ""
            _modDate = Date.MinValue
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property InputMaskId() As String
            Get
                Return _InputMaskId
            End Get
            Set(ByVal Value As String)
                _InputMaskId = Value
            End Set
        End Property
        Public Property Item() As String
            Get
                Return _Item
            End Get
            Set(ByVal Value As String)
                _Item = Value
            End Set
        End Property
        Public Property Mask() As String
            Get
                Return _Mask
            End Get
            Set(ByVal Value As String)
                _Mask = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As Date
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
#End Region
    End Class
#End Region

End Namespace