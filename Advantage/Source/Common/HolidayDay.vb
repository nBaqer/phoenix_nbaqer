 ''' <summary>
    ''' Hold definition of a day of holiday
    ''' </summary>
    ''' <remarks></remarks>
    Public Class HolidayDay

        Public Property Description As String


        Public Property StartDate As DateTime


        Public Property EndDate As DateTime


        Public Property AllDay As Boolean


        Public Property CampusGroupId As Guid


        Public Property StatusOption As String


        Public Property StartTimeInterval As DateTime


        Public Property EndTimeInterval As DateTime

    End Class
