﻿''' <summary>
''' Hold the object with the status of Score for one specific ClassId
''' Can have different activities related to score as Lab test and other.
''' </summary>
''' <remarks></remarks>
Public Class GrdRecordsPoco

    Public Property GrdBkResultId() As String
    Public Property ClsSectionId() As String
    Public Property InstrGrdBkWgtDetailId() As String
    Public Property StuEnrollId() As String
    Public Property Score() As String
    Public Property Comments() As String
    Public Property Descrip() As String
    Public Property IsIncomplete() As Boolean
    Public Property UserName() As String
    Public Property DroppedInAddDrop As Boolean
    Public Property DateDetermined() As DateTime?
    Public Property GradeOverridenBy() As String

    ''' <summary>
    Public Property ResNum() As Integer
    Public Property StudentId As String

    ''' <summary>
    ''' Used only in Final Grade (ArFinalGradeClass)
    ''' get a Guid as string with the student grade
    ''' in NOT Numeric format schools
    ''' </summary>
    ''' <value>Guid as string. can be NULL</value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Grade() As String


    ''' <summary>
    ''' Used only in Final Grade (ArFinalGradeClass)
    ''' Get if School Grade Format is Numeric or not
    ''' Exists only two states numeric or not
    ''' </summary>
    ''' <value>true if numeric</value>
    ''' <returns></returns>
    Public Property IsFormatNumeric() As Boolean

    'Public Property Weight() As Decimal
    'Public Property Seq() As Integer
    'Public Property FullName() As String
    Public Property DateCompleted() As DateTime?

End Class
