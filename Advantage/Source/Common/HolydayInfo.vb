Public Class HolidayInfo
    '
    '   HolidayInfo Class
    '
#Region " Private Variables and Objects"

    #End Region

#Region " Public Constructors "

    Public Sub New()
        IsInDB = False
        HolidayId = Guid.NewGuid.ToString
        Code = ""
        StatusId = Guid.Empty.ToString
        Status = ""
        Description = ""
        HolidayStartDate = Date.Parse(Date.Now.ToShortDateString) ' begining of the day
        HolidayEndDate = Date.Parse(HolidayStartDate.ToShortDateString + " 11:59:59 PM") ' end of the day
        AllDay = False
        StartTimeId = Guid.Empty.ToString
        EndTimeId = Guid.Empty.ToString
        ModUser = ""
        ModDate = Date.MinValue
    End Sub

#End Region

#Region " Public Properties"

    Public Property IsInDB As Boolean

    Public Property HolidayId As String

    Public Property Code As String

    Public Property StatusId As String

    Public Property Status As String

    Public Property Description As String

    Public Property CampGrpId As String

    Public Property CampGrpDescrip As String

    Public Property HolidayStartDate As Date

    Public Property HolidayEndDate As Date

    Public Property AllDay As Boolean

    Public Property StartTimeId As String

    Public Property EndTimeId As String

    Public Property ModUser As String

    Public Property ModDate As Date

#End Region
End Class
Public Class AdvantageHoliday
    Implements IComparable
#Region "Private Variables"

    #End Region

#Region "Public Constructors"

    Public Sub New()
    End Sub

    Public Sub New(ByVal holidayDate As DateTime, ByVal duration As Integer)
        HolidayDateAndTime = holidayDate
        Me.Duration = duration
    End Sub

#End Region

#Region "Public Properties"

    Public Property HolidayDateAndTime As Date

    Public Property Duration As Integer

#End Region

#Region "Public Functions"

    Public Overloads Function CompareTo(ByVal obj As Object) As Integer Implements IComparable.CompareTo

        Dim ah As AdvantageHoliday = TryCast(obj, AdvantageHoliday)
        If (ah IsNot Nothing)
            Return HolidayDateAndTime.CompareTo(ah.HolidayDateAndTime)
        End If

        Throw New ArgumentException("object is not an AdvantageHoliday")
    End Function

#End Region
End Class