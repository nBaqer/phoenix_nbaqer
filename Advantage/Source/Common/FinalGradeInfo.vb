Public Class FinalGradeInfo
#Region "Private Variable Declaration"

    Private _ClsSectId As String
    Private _ReqId As String
    Private _TermId As String
    Private _StuEnrollId As String
    Private _Grade As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _score As Decimal
    'variable added by Priyanka on date 26th April 2009
    Private _dateDetermined As String
#End Region
#Region " Public Constructor"
    Public Sub New()
        _modUser = ""
        _modDate = Date.MinValue
        _score = 0.0
    End Sub
#End Region
#Region "FinalGradeInfo Properties"

    Public Property ClsSectId() As String
        Get
            Return _ClsSectId
        End Get
        Set(ByVal Value As String)
            _ClsSectId = Value
        End Set
    End Property
    Public Property TermId() As String
        Get
            Return _TermId
        End Get
        Set(ByVal Value As String)
            _TermId = Value
        End Set
    End Property
    Public Property ReqId() As String
        Get
            Return _ReqId
        End Get
        Set(ByVal Value As String)
            _ReqId = Value
        End Set
    End Property
    Public Property StuEnrollId() As String
        Get
            Return _StuEnrollId
        End Get
        Set(ByVal Value As String)
            _StuEnrollId = Value
        End Set
    End Property
    Public Property Grade() As String
        Get
            Return _Grade
        End Get
        Set(ByVal Value As String)
            _Grade = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property Score() As Decimal
        Get
            Return _score
        End Get
        Set(ByVal value As Decimal)
            _score = value
        End Set
    End Property
    'Property added by Priyanka on date 26th April 2009
    Public Property DateDetermined() As String
        Get
            Return _dateDetermined
        End Get
        Set(ByVal Value As String)
            _dateDetermined = Value
        End Set
    End Property
#End Region
End Class
