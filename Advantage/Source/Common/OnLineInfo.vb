' ===============================================================================
' FAME.AdvantageV123
'
' OnLineInfo.vb
'
' OnLine Common Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2007 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class OnLineInfo

#Region "Private Variable"
    Private _url As String = "http://devel.comcourse.com/demo/webservice/200705.php?developer_token=A2E75AE483C2EFB7B8F7EC99FE8ABA5B6546&function="
    Private _stuEnrollId As String
    Private _isInDB As Boolean
    Private _onLineStudentId As Integer
    Private _userId As String
    Private _username As String
    Private _password As String
    Private _programId As String
    Private _lastName As String
    Private _firstName As String
    Private _middleInitial As String
    Private _address1 As String
    Private _address2 As String
    Private _city As String
    Private _state As String
    Private _zipCode As String
    Private _country As String
    Private _phone1 As String
    Private _phone2 As String
    Private _email As String
    Private _onLineCourses() As OnLineCourse
    Private _modUser As String
    Private _modDate As Date
#End Region

#Region "Constructors"
    Public Sub New()

    End Sub
    Public Sub New(ByVal onLineUrl As String)
        Me.New()
        If Not (onLineUrl Is Nothing) Then _url = onLineUrl
    End Sub
#End Region
#Region "Public Methods"
    Public Function create_user() As String
        Return (ParameterUsername + ParameterPassword + ParameterFirstName + ParameterLastName + ParameterEmail + ParameterCity + ParameterPhone1 + ParameterPhone2 + ParameterAddress + ParameterCountry)
    End Function
    Public Function get_courses(ByVal courseId As Integer, ByVal students_course As Boolean) As String
        Return "&courseid=" + courseId.ToString + "&students_course=" + students_course.ToString.ToLower
    End Function
    Public Function get_courses(ByVal students_course As Boolean) As String
        Return "&students_course=" + students_course.ToString.ToLower
    End Function
    Public Function get_courses(ByVal shortName As String, ByVal students_course As Boolean) As String
        Return "&shortname=" + shortName + "&students_course=" + students_course.ToString.ToLower
    End Function
    Public Function get_terms() As String
        Return String.Empty
    End Function
    Public Function get_users(ByVal courses_student As Boolean, ByVal courses_student_course_info As Boolean) As String
        Return "&userid=" + Me.OnLineStudentId.ToString.ToLower + "&courses_student=" + courses_student.ToString.ToLower + "&courses_student_course_info=" + courses_student_course_info.ToString()
    End Function
    Public Function get_users(ByVal userId As Integer, ByVal courses_student As Boolean, ByVal courses_student_course_info As Boolean) As String
        Return "&userid=" + userId + "&courses_student=" + courses_student.ToString.ToLower + "&courses_student_course_info=" + courses_student_course_info.ToString.ToLower()
    End Function
    Public Function update_user() As String
        Return (ParameterId + ParameterUsername + ParameterPassword + ParameterFirstName + ParameterLastName + ParameterEmail + ParameterCity + ParameterPhone1 + ParameterPhone2 + ParameterAddress + ParameterCountry)
        'Return (ParameterId + ParameterFirstName + ParameterLastName + ParameterEmail + ParameterCity + ParameterPhone1 + ParameterPhone2 + ParameterAddress + ParameterCountry)
    End Function
    Public Function set_user_enrollment(ByVal return_course_infos As Boolean) As String
        Return (ParameterUserId + ParameterCourseIds + "&return_course_infos=" + return_course_infos.ToString)
    End Function
    Public Shared Function ConvertOnLineGradeToInteger(ByVal onLineGrade As String) As Integer
        Dim part() As String = onLineGrade.Split("/")
        Dim numerator As String = Double.Parse(part(0).Substring(1))
        Dim denominator As Double = Double.Parse(part(1).Substring(0, part(1).Length - 1))
        If denominator = 0 Then Return 0
        Return CType(numerator / denominator * 100.0 + 0.5, Integer)
    End Function
#End Region

#Region "Public Properties"
    Property Url() As String
        Get
            Return _url
        End Get
        Set(ByVal value As String)
            _url = value
        End Set
    End Property
    Property StuEnrollId() As String
        Get
            Return _stuEnrollId
        End Get
        Set(ByVal value As String)
            _stuEnrollId = value
        End Set
    End Property
    Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal value As Boolean)
            _isInDB = value
        End Set
    End Property
    Property OnLineStudentId() As Integer
        Get
            Return _onLineStudentId
        End Get
        Set(ByVal value As Integer)
            _onLineStudentId = value
        End Set
    End Property
    Property UserId() As String
        Get
            Return _userId
        End Get
        Set(ByVal value As String)
            _userId = value
        End Set
    End Property
    Property UserName() As String
        Get
            Return _username
        End Get
        Set(ByVal value As String)
            _username = value
        End Set
    End Property
    Property Password() As String
        Get
            Return _password
        End Get
        Set(ByVal value As String)
            _password = value
        End Set
    End Property
    Property ProgramId() As String
        Get
            Return _programId
        End Get
        Set(ByVal value As String)
            _programId = value
        End Set
    End Property
    Property LastName() As String
        Get
            Return _lastName
        End Get
        Set(ByVal value As String)
            _lastName = value
        End Set
    End Property
    Property FirstName() As String
        Get
            Return _firstName
        End Get
        Set(ByVal value As String)
            _firstName = value
        End Set
    End Property
    Property MiddleInitial() As String
        Get
            Return _middleInitial
        End Get
        Set(ByVal value As String)
            _middleInitial = value
        End Set
    End Property
    Property Address1() As String
        Get
            Return _address1
        End Get
        Set(ByVal value As String)
            _address1 = value
        End Set
    End Property
    Property Address2() As String
        Get
            Return _address2
        End Get
        Set(ByVal value As String)
            _address2 = value
        End Set
    End Property
    Property City() As String
        Get
            Return _city
        End Get
        Set(ByVal value As String)
            _city = value
        End Set
    End Property
    Property State() As String
        Get
            Return _state
        End Get
        Set(ByVal value As String)
            _state = value
        End Set
    End Property
    Property ZipCode() As String
        Get
            Return _zipCode
        End Get
        Set(ByVal value As String)
            _zipCode = value
        End Set
    End Property
    Property Country() As String
        Get
            Return _country
        End Get
        Set(ByVal value As String)
            _country = value
        End Set
    End Property
    Property Phone1() As String
        Get
            Return _phone1
        End Get
        Set(ByVal value As String)
            _phone1 = value
        End Set
    End Property
    Property Phone2() As String
        Get
            If _phone1 = _phone2 Then Return String.Empty Else Return _phone2
        End Get
        Set(ByVal value As String)
            _phone2 = value
        End Set
    End Property
    Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property
    Property OnLineCourses() As OnLineCourse()
        Get
            Return _onLineCourses
        End Get
        Set(ByVal value As OnLineCourse())
            _onLineCourses = value
        End Set
    End Property
    Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal value As String)
            _modUser = value
        End Set
    End Property
    Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal value As Date)
            _modDate = value
        End Set
    End Property
    ReadOnly Property ParameterId() As String
        Get
            Return "&id=" + _onLineStudentId.ToString()
        End Get
    End Property
    ReadOnly Property ParameterUserId() As String
        Get
            Return "&userid=" + _onLineStudentId.ToString()
        End Get
    End Property
    ReadOnly Property ParameterUsername() As String
        Get
            Return "&username=" + _username
        End Get
    End Property
    ReadOnly Property ParameterPassword() As String
        Get
            Return "&password=" + _password
        End Get
    End Property
    ReadOnly Property ParameterFirstName() As String
        Get
            Return "&firstname=" + _firstName
        End Get
    End Property
    ReadOnly Property ParameterLastName() As String
        Get
            Return "&lastname=" + _lastName
        End Get
    End Property
    ReadOnly Property ParameterEmail() As String
        Get
            Return "&email=" + _email
        End Get
    End Property
    ReadOnly Property ParameterCity() As String
        Get
            Return "&city=" + _city
        End Get
    End Property
    ReadOnly Property ParameterPhone1() As String
        Get
            Return "&phone1=" + _phone1
        End Get
    End Property
    ReadOnly Property ParameterPhone2() As String
        Get
            Return "&phone2=" + _phone2
        End Get
    End Property
    ReadOnly Property ParameterAddress() As String
        Get
            Return "&address=" + _address1 + _address2
        End Get
    End Property
    ReadOnly Property ParameterCountry() As String
        Get
            Return "&country=" + _country
        End Get
    End Property
    ReadOnly Property ParameterCourseIds() As String
        Get
            Return "&courseids=" + GetCourseShortNames()
        End Get
    End Property
    ReadOnly Property IsRegisteredInOnLineDB() As Boolean
        Get
            Return _onLineStudentId > 0
        End Get
    End Property
#End Region
#Region "Private Methods"
    Private Function GetCourseShortNames() As String
        Dim sb As New System.Text.StringBuilder()
        For i As Integer = 0 To OnLineCourses.Length - 1
            Dim onLineCourse As OnLineCourse = OnLineCourses(i)
            If i > 0 Then sb.Append(";")
            sb.Append(onLineCourse.CourseShortName)
        Next
        Return sb.ToString
    End Function
#End Region
End Class
Public Class OnLineCourse
#Region "Private Variables"
    Private _stuEnrollId As String
    Private _reqId As String
    Private _courseShortName As String
    'Private _onLineCourseId As Integer
    Private _dateEntered As Date
    Private _dateChanged As Date
    Private _isDrop As Boolean
    Private _modUser As String
    Private _modDate As Date
#End Region
#Region "Public Constructors"
    Public Sub New()
    End Sub
#End Region
#Region "Public Properties"
    Property StuEnrollId() As String
        Get
            Return _stuEnrollId
        End Get
        Set(ByVal value As String)
            _stuEnrollId = value
        End Set
    End Property
    Property ReqId() As String
        Get
            Return _reqId
        End Get
        Set(ByVal value As String)
            _reqId = value
        End Set
    End Property
    Property CourseShortName() As String
        Get
            Return _courseShortName
        End Get
        Set(ByVal value As String)
            _courseShortName = value
        End Set
    End Property
    'ReadOnly Property OnLineCourseId() As Integer
    '    Get
    '        Return _onLineCourseId
    '    End Get
    'End Property
    Property DateEntered() As Date
        Get
            Return _dateEntered
        End Get
        Set(ByVal value As Date)
            _dateEntered = value
        End Set
    End Property
    Property DateChanged() As Date
        Get
            Return _dateChanged
        End Get
        Set(ByVal value As Date)
            _dateChanged = value
        End Set
    End Property
    Property IsDrop() As Boolean
        Get
            Return _isDrop
        End Get
        Set(ByVal value As Boolean)
            _isDrop = value
        End Set
    End Property
    Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal value As String)
            _modUser = value
        End Set
    End Property
    Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal value As Date)
            _modDate = value
        End Set
    End Property
#End Region
End Class
<Serializable()> _
Public Class OnLineCourseAttendance
    Private _courseShortName As String
    Private _onLineTermId As Integer
    Private _onLineStudentAttendanceCollection As List(Of OnLineStudentAttendance)
    Sub New(ByVal courseShortName As String, ByVal onLineTermId As Integer)
        _courseShortName = courseShortName
        _onLineTermId = onLineTermId
    End Sub
    ReadOnly Property CourseShortName() As String
        Get
            Return _courseShortName
        End Get
    End Property
    ReadOnly Property OnLineTermId() As Integer
        Get
            Return _onLineTermId
        End Get
    End Property
    Property OnLineStudentAttendanceCollection() As List(Of OnLineStudentAttendance)
        Get
            Return _onLineStudentAttendanceCollection
        End Get
        Set(ByVal value As List(Of OnLineStudentAttendance))
            _onLineStudentAttendanceCollection = value
        End Set
    End Property
    Public Function GetStudentAttendance(ByVal onLineStudentId As Integer) As OnLineStudentAttendance
        For Each onLineStudentAttendance As OnLineStudentAttendance In _onLineStudentAttendanceCollection
            If onLineStudentAttendance.StudentId = onLineStudentId Then Return onLineStudentAttendance
        Next
        Return Nothing
    End Function
End Class
<Serializable()> _
Public Class OnLineStudentAttendance
    Private _studentId As Integer
    Private _lastName As String
    Private _firstName As String
    Private _onLineCourseAttendance As OnLineCourseAttendance
    Private _attendedDatesCollection As List(Of OnLineAttendedDate)
    Sub New(ByVal studentId As Integer, ByVal lastName As String, ByVal FirstName As String, ByVal onLineCourseAttendance As OnLineCourseAttendance)
        _studentId = studentId
        _lastName = lastName
        _firstName = FirstName
        _onLineCourseAttendance = OnLineCourseAttendance
    End Sub
    ReadOnly Property StudentId() As Integer
        Get
            Return _studentId
        End Get
    End Property
    ReadOnly Property LastName() As String
        Get
            Return _lastName
        End Get
    End Property
    ReadOnly Property FirstName() As String
        Get
            Return _firstName
        End Get
    End Property
    ReadOnly Property Parent() As OnLineCourseAttendance
        Get
            Return _onLineCourseAttendance
        End Get
    End Property
    Property AttendedDatesCollection() As List(Of OnLineAttendedDate)
        Get
            Return _attendedDatesCollection
        End Get
        Set(ByVal value As List(Of OnLineAttendedDate))
            _attendedDatesCollection = value
        End Set
    End Property
    Public Function GetAttendanceByDate(ByVal d As Date) As OnLineAttendedDate
        For Each onLineAttendedDate As OnLineAttendedDate In _attendedDatesCollection
            If onLineAttendedDate.ActualDate = d Then Return onLineAttendedDate
        Next
        Return Nothing
    End Function
    Public Function GetLastAttendedDate() As OnLineAttendedDate
        If _attendedDatesCollection Is Nothing Then Return Nothing
        If _attendedDatesCollection.Count < 1 Then Return Nothing
        Return _attendedDatesCollection.Item(_attendedDatesCollection.Count - 1)
    End Function
End Class
<Serializable()> _
Public Class OnLineAttendedDate
    Private _actualDate As Date
    Private _minutes As Integer
    Sub New(ByVal actualDate As Date, ByVal minutes As Integer)
        _actualDate = actualDate
        _minutes = minutes
    End Sub
    ReadOnly Property ActualDate() As Date
        Get
            Return _actualDate
        End Get
    End Property
    ReadOnly Property Minutes() As Integer
        Get
            Return _minutes
        End Get
    End Property
End Class
<Serializable()> _
Public Class OnLineTermAttendance
    Private _onLineTermId As Integer
    Private _onLineTermStartDate As Date
    Private _onLineCourseAttendanceCollection As List(Of OnLineCourseAttendance)
    Sub New(ByVal onLineTermId As Integer, ByVal onLineTermStartDate As Date, ByVal onLineCourseAttendanceCollection As List(Of OnLineCourseAttendance))
        _onLineTermId = OnLineTermId
        _onLineTermStartDate = OnLineTermStartDate
        _onLineCourseAttendanceCollection = OnLineCourseAttendanceCollection
    End Sub
    ReadOnly Property OnLineTermId() As Integer
        Get
            Return _onLineTermId
        End Get
    End Property
    ReadOnly Property OnLineTermStartDate() As Date
        Get
            Return _onLineTermStartDate
        End Get
    End Property
    ReadOnly Property OnLineCourseAttendanceCollection() As List(Of OnLineCourseAttendance)
        Get
            Return _onLineCourseAttendanceCollection
        End Get
    End Property
End Class