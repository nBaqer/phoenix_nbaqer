


Public Class ProgBL
    Public Enum ValResult
        Success ' Validation successful
        FailHrsCrdtsNtMet 'Validation Failed - Hrs and creds
        'of courses/coursegrps selected is less than the 
        'hrs and creds of the Program version
        FailReqHrsCrdsOver 'Validation Failed - Hrs and creds
        'of req courses/coursegrps is greater than the hrs
        'and creds of the program version
    End Enum
    Public Function ValHrsCreds(ByVal ProgVerHours As Decimal, ByVal ProgVerCredits As Decimal, ByVal ChildHours As Decimal, ByVal ChildCredits As Decimal, ByVal ds As DataSet) As ValResult

        Dim dt As New DataTable
        Dim coursedt As New DataTable
        Dim row As DataRow
        Dim reqHours As Decimal
        Dim reqCredits As Decimal
        Dim rowcount As Integer

        'If the ChildHours and ChildCredits passed in are both 0 then we can simply return success since this means that the user is
        'simply clearing out all the selected items.
        If ChildHours = 0.0 And ChildCredits = 0.0 Then
            Return ValResult.Success
        End If

        'Retrieve datatables from the dataset that was passed in.
        dt = ds.Tables("FldsSelected")
        coursedt = ds.Tables("Courses")

        'Scenario 1 : Hours and credits of the Courses/CourseGrps selected cannot
        'be less than the hours and credits of the program version.

        If ((ChildHours < ProgVerHours) Or (ChildCredits < ProgVerCredits)) Then
            Return ValResult.FailHrsCrdtsNtMet
        End If

        'Scenario 2 : The hours and credits for Required Courses
        'cannot exceed the hours and credits for the program version.

        'To get the hours and credits for required courses
        'loop through the datatable and add up the necessary info.
        rowcount = dt.Rows.Count
        For Each row In dt.Rows
            If (row.RowState <> DataRowState.Deleted) Then
                If (row("IsRequired") = True) Then

                    'Dim row2 As DataRow = coursedt.Rows.Find(row("ReqId"))
                    'Populate the hours and credits for the coursegrps
                    'selected.
                    reqHours += row("Hours")
                    reqCredits += row("Credits")

                End If
            End If
        Next
        If ((reqHours > ProgVerHours) Or (reqCredits > ProgVerCredits)) Then
            Return ValResult.FailReqHrsCrdsOver
        Else
            Return ValResult.Success
        End If

    End Function
End Class


