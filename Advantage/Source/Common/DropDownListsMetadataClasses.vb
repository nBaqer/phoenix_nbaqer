Imports FAME.Advantage.Common

Public Enum AdvantageDropDownListName
    AcreditationAgencies = 1
    Campuses = 2
    Countries = 3
    States = 4
    Statuses = 5
    Calendars = 6
    CourseTypes = 7
    Degrees = 8
    GradeLevels = 9
    Program_Groups = 10
    Shifts = 11
    Terms = 12
    CampGrps = 13
    Dept_Ed_Id = 14
    Sponsors = 15
    Citizen = 16
    Colleges = 17
    EducationLvl = 18
    Races = 19
    Genders = 20
    HighSchools = 21
    Industries = 22
    Job_Categories = 23
    Locations = 24
    Counties = 25
    Areas = 26
    MaritalStatus = 27
    Nationality = 28
    Titles = 29
    JobTitles = 30
    AttUnitTypes = 31
    Programs = 32
    LeadPrograms = 33
    SAPs = 34
    ProgramVersions = 35
    SAPCriteria = 36
    Activities = 37
    ActivityTyps = 38
    Documents = 39
    DocStatuses = 40
    DocumentStatus = 41
    SysDocStatuses = 42
    Modules = 43
    Prefixes = 44
    Suffixes = 45
    EmpContacts = 46
    Employees = 47
    Priorities = 48
    Positions = 49
    Departments = 50
    Bill_Type = 51
    Trans_Codes = 52
    Billing_Codes = 53
    TestingModels = 54
    Grade_Types = 55
    Time_Interval = 56
    Books = 57
    Categories = 58
    Prog_Types = 59
    AcademicYears = 60
    Crd_Card_Types = 61
    Paid_By = 62
    Bank_Accounts = 63
    Req_Types = 64
    Grade_Scales = 65
    RptCourses = 66
    ReqGroup = 67
    Requirements = 68
    GradCompTyp = 69
    Drop_Reasons = 70
    LOA_Reasons = 71
    Probation_Type = 72
    Lead_Groups = 73
    Student_Groups = 74
    Program_Types = 75
    Grade_Systems = 76
    Employers = 77
    JobTypes = 78
    Benefits = 79
    JobSchedule = 80
    JobStatus = 81
    FieldStudy = 82
    Interview = 83
    SalaryType = 84
    SchoolStatus = 85
    Transportation = 86
    CollegeDivs = 87
    LeadStatus = 88
    Status_Codes = 89
    AdvStatus = 90
    Adv_Statuses = 91
    Lenders = 92
    Address_Types = 93
    AddressStatuses = 94
    Phone_Types = 95
    Phone_Statuses = 96
    Earning_Method = 97
    Enrollments = 98
    Tuition_Cats = 99
    Billing_Methods = 100
    Fund_Sources = 101
    LeadCategory = 102
    LeadAdv = 103
    AdmissionsRep = 104
    LeadSourceTypes = 105
    AdvInterval = 106
    Status_Levels = 107
    SkillGroups = 108
    ExtCurrGrp = 109
    Employer_Fees = 110
    Fees = 111
    Award_Years = 112
    Instructor = 113
    FamilyIncome = 114
    HRDepartments = 115
    PlacedBy = 116
    FullPartTime = 117
    Employer_Jobs = 118
    ExpertiseLevel = 119
    Users = 120
    Schema = 121
    DTypeId = 122
    AttendanceType = 123
    HousingType = 124
    DependencyType = 125
    GeographicType = 126
    DegCertSeekingType = 127
    AdminCriteriaType = 128
    Tests = 129
    GradePolicies = 130
    Award_Types = 131
    Servicers = 132
    Guarantors = 133
    Relations = 134
    Courses = 135
    Periods = 136
    Instructors = 137
    Campus = 138
    Employer_Groups = 139
    Academic_Advisors = 140
    Financial_Advisors = 141
    CourseCategories = 142
    Students = 143
    StudentsWithEnrollments = 144
    LeadEnrollStatusCodes = 145
    Admission_Reps = 146
    Rate_Schedules = 147
    System_Transaction_Codes = 148
    CohortStartDate = 149
    CredentialLevel = 150
    FASAPs = 151
End Enum
Public Class AcreditationAgenciesDDLMetadata
    Inherits AdvantageDropDownListMetadata

    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "AcreditationAgencies"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syRptAgencies"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "RptAgencyId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Return "Select RptAgencyId, Descrip, 1 from syRptAgencies"
        End Get
    End Property
End Class
Public Class CampusesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Campuses"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syCampuses"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CampusId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "CampDescrip"
        End Get
    End Property
End Class
Public Class CountriesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Countries"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adCountries"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CountryId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "CountryDescrip"
        End Get
    End Property
End Class
Public Class StatesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "States"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syStates"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "StateId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "StateDescrip"
        End Get
    End Property
End Class
Public Class StatusesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Statuses"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syStatuses"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "StatusId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Status"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Return "Select StatusId, Status, 1 from syStatuses"
        End Get
    End Property
End Class
'Public Class CalendarsDDLMetadata
'    Inherits AdvantageDropDownListMetadata
'    Public Sub New()
'        MyBase.New()
'    End Sub
'    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
'        MyBase.New()
'        m_pullActiveItemsOnly = pullActiveItemsOnly
'    End Sub
'    Public Overrides ReadOnly Property DropDownListName() As String
'        Get
'            Return "Calendars"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property TableName() As String
'        Get
'            Return "arCalendar"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property ValueField() As String
'        Get
'            Return "CalId"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property TextField() As String
'        Get
'            Return "CalDescrip"
'        End Get
'    End Property
'End Class
Public Class CourseTypesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "CourseTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arCourseType"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CourseTypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "CourseTypeDescrip"
        End Get
    End Property
End Class
Public Class DegreesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Degrees"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arDegrees"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "DegreeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "DegreeDescrip"
        End Get
    End Property
End Class
'Public Class GradeLevelsDDLMetadata
'    Inherits AdvantageDropDownListMetadata
'    Public Sub New()
'        MyBase.New()
'    End Sub
'    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
'        MyBase.New()
'        m_pullActiveItemsOnly = pullActiveItemsOnly
'    End Sub
'    Public Overrides ReadOnly Property DropDownListName() As String
'        Get
'            Return "GradeLevels"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property TableName() As String
'        Get
'            Return "arGrdLvl"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property ValueField() As String
'        Get
'            Return "GrdLvlId"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property TextField() As String
'        Get
'            Return "GrdLvlDescrip"
'        End Get
'    End Property
'End Class
Public Class Program_GroupsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Program Groups"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arPrgGrp"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "PrgGrpId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "PrgGrpDescrip"
        End Get
    End Property
End Class
Public Class ShiftsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Shifts"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arShifts"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ShiftId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "ShiftDescrip"
        End Get
    End Property
End Class
Public Class TermsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Terms"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arTerm"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "TermId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "TermDescrip"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim sb As New System.Text.StringBuilder()
            With sb
                .Append("SELECT Distinct ")
                .Append(Me.ValueField)
                .Append(",")
                .Append(Me.TextField)
                .Append(",")
                .Append("(Case S.Status when 'Active' then 1 else 0 end) As Status,T.StartDate,T.TermDescrip  ")
                .Append(" FROM ")
                .Append(Me.TableName & " T, arPrograms B, arPrgVersions C, ")
                If Me.PullActiveItemsOnly Then
                    .Append(" syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")
                Else
                    .Append(" syStatuses S WHERE T.StatusId=S.StatusId ")
                End If
                .Append("AND (T.ProgId = B.ProgId  or T.ProgId is null) ")

                .Append("AND B.ProgId = C.ProgId ")

                .Append("AND T.CampGrpId IN ( ")
                .Append(" SELECT Distinct CampGrpId FROM syCmpGrpCmps WHERE CampusId = '")
                .Append(Me.CampusId)
                .Append("' ")
                .Append(" AND CampGrpId not in (SELECT Distinct CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
                .Append(" Union ")
                .Append(" SELECT Distinct CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
                .Append(" order by T.StartDate,T.TermDescrip ")
            End With

            '.Append("FROM syCmpGrpCmps ")

            '.Append("WHERE CampusId = '")
            '.Append(Me.CampusId)
            '.Append("') ")
            '.Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            '.Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
            Return sb.ToString
        End Get
    End Property
End Class
Public Class CampGrpsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "CampGrps"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syCampGrps"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CampGrpId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "CampGrpDescrip"
        End Get
    End Property
End Class
'Public Class Dept_Ed_IdDDLMetadata
'    Inherits AdvantageDropDownListMetadata
'    Public Sub New()
'        MyBase.New()
'    End Sub
'    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
'        MyBase.New()
'        m_pullActiveItemsOnly = pullActiveItemsOnly
'    End Sub
'    Public Overrides ReadOnly Property DropDownListName() As String
'        Get
'            Return "Dept Ed Id"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property TableName() As String
'        Get
'            Return "syDeptEdIds"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property ValueField() As String
'        Get
'            Return "DeptEdId"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property TextField() As String
'        Get
'            Return "DeptEdCode"
'        End Get
'    End Property
'End Class
Public Class SponsorsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Sponsors"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adAgencySponsors"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "AgencySpId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "AgencySpDescrip"
        End Get
    End Property
End Class
Public Class CitizenDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Citizen"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adCitizenships"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CitizenshipId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "CitizenshipDescrip"
        End Get
    End Property
End Class
Public Class CollegesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Colleges"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adColleges"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CollegeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "CollegeName"
        End Get
    End Property
End Class
Public Class EducationLvlDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "EducationLvl"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adEdLvls"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "EdLvlId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "EdLvlDescrip"
        End Get
    End Property
End Class
Public Class RacesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Races"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adEthCodes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "EthCodeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "EthCodeDescrip"
        End Get
    End Property
End Class
Public Class Rate_SchedulesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Rate Schedules"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "saRateSchedules"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "RateScheduleId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "RateScheduleDescrip"
        End Get
    End Property
End Class
Public Class GendersDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Genders"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adGenders"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "GenderId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "GenderDescrip"
        End Get
    End Property
End Class
Public Class HighSchoolsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "HighSchools"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syInstitutions"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "HSId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "HSName"
        End Get
    End Property
End Class
Public Class IndustriesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Industries"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plIndustries"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "IndustryId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "IndustryDescrip"
        End Get
    End Property
End Class
Public Class Job_CategoriesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Job Categories"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plJobCats"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "JobCatId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "JobCatDescrip"
        End Get
    End Property
End Class
Public Class LocationsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Locations"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plLocations"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "LocationId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "LocationDescrip"
        End Get
    End Property
End Class
Public Class CountiesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Counties"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adCounties"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CountyId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "CountyDescrip"
        End Get
    End Property
End Class
Public Class AreasDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Areas"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adCounties"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CountyId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "CountyDescrip"
        End Get
    End Property
End Class
Public Class MaritalStatusDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "MaritalStatus"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adMaritalStatus"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "MaritalStatId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "MaritalStatDescrip"
        End Get
    End Property
End Class
Public Class NationalityDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Nationality"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adNationalities"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "NationalityId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "NationalityDescrip"
        End Get
    End Property
End Class
Public Class TitlesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Titles"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adTitles"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "TitleId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "TitleDescrip"
        End Get
    End Property
End Class
Public Class JobTitlesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "JobTitles"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adTitles"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "TitleId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "TitleDescrip"
        End Get
    End Property
End Class
Public Class AttUnitTypesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "AttUnitTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arAttUnitType"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "UnitTypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "UnitTypeDescrip"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Return "Select UnitTypeId, UnitTypeDescrip, 1 from arAttUnitType"
        End Get
    End Property
End Class
Public Class ProgramsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Programs"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arPrograms"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ProgId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "ProgDescrip"
        End Get
    End Property
End Class
Public Class LeadProgramsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "LeadPrograms"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arPrograms"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ProgId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "ProgDescrip"
        End Get
    End Property
End Class
Public Class SAPsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "SAPs"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arSAP"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "SAPId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "SAPDescrip"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Return "Select distinct  SAPId, SAPDescrip, 1 as Status from arSAP WHERE FASAPPolicy=0"
        End Get
    End Property
End Class

Public Class FASAPsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "FASAPs"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arSAP"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "SAPId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "SAPDescrip"
        End Get
    End Property

    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Return "Select distinct  SAPId, SAPDescrip, 1 as Status from arSAP WHERE FASAPPolicy=1 and StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'"
        End Get
    End Property
End Class
Public Class ProgramVersionsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "ProgramVersions"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arPrgVersions"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "PrgVerId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "PrgVerDescrip"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim sb As New System.Text.StringBuilder()
            With sb
                .Append("SELECT Distinct ")
                .Append(Me.ValueField)
                .Append(",")
                .Append(Me.TextField)
                .Append(",")
                .Append("(Case S.Status when 'Active' then 1 else 0 end) As Status ")
                .Append(" FROM ")
                .Append(Me.TableName & " T, ")
                If Me.PullActiveItemsOnly Then
                    .Append(" syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")
                Else
                    .Append(" syStatuses S WHERE T.StatusId=S.StatusId ")
                End If
                If Not Me.CampusId Is Nothing Then
                    .Append("AND T.CampGrpId IN(SELECT CampGrpId ")

                    .Append("FROM syCmpGrpCmps ")

                    .Append("WHERE CampusId = '")
                    .Append(Me.CampusId)
                    .Append("') ")
                End If
                .Append(" AND ( CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
                .Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
                .Append("ORDER BY ")
                .Append(Me.TextField)
            End With
            Return sb.ToString
        End Get
    End Property

End Class
'Public Class SAPCriteriaDDLMetadata
'    Inherits AdvantageDropDownListMetadata
'    Public Sub New()
'        MyBase.New()
'    End Sub
'    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
'        MyBase.New()
'        m_pullActiveItemsOnly = pullActiveItemsOnly
'    End Sub
'    Public Overrides ReadOnly Property DropDownListName() As String
'        Get
'            Return "SAPCriteria"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property TableName() As String
'        Get
'            Return "arSAPCriteria"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property ValueField() As String
'        Get
'            Return "SAPCriteriaId"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property TextField() As String
'        Get
'            Return "Criteria"
'        End Get
'    End Property
'End Class
Public Class ActivitiesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Activities"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "cmActivities"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ActivityId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "ActivityDescrip"
        End Get
    End Property
End Class
Public Class ActivityTypsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "ActivityTyps"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "cmActivityTyps"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ActivityTypId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "ActivityTypDescrip"
        End Get
    End Property
End Class
Public Class DocumentsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Documents"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adReqs"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "adReqId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim sb As New System.Text.StringBuilder()
            With sb
                .Append("SELECT ")
                .Append(Me.ValueField)
                .Append(",")
                .Append(" '(' + Code + ') ' + Descrip ")
                .Append(",")
                .Append("(Case S.Status when 'Active' then 1 else 0 end) As Status ")
                .Append(" FROM ")
                .Append(Me.TableName)
                If Me.PullActiveItemsOnly Then
                    .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")

                    If Not Me.CampusId Is Nothing Then

                        .Append("AND T.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")

                        .Append(Me.CampusId)

                        .Append("') ")

                    End If

                Else
                    .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId ")

                    If Not Me.CampusId Is Nothing Then

                        .Append("AND T.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")

                        .Append(Me.CampusId)

                        .Append("') ")

                    End If

                End If
                .Append(" and adReqTypeId = 3 ")
            End With
            Return sb.ToString
        End Get
    End Property
End Class
Public Class DocStatusesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "DocStatuses"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syDocStatuses"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "DocStatusId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "DocStatusDescrip"
        End Get
    End Property
End Class
Public Class DocumentStatusDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "DocumentStatus"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "cmDocumentStatus"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "DocStatusId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "DocStatusDescrip"
        End Get
    End Property
End Class
Public Class SysDocStatusesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "SysDocStatuses"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "sySysDocStatuses"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "SysDocStatusId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "DocStatusDescrip"
        End Get
    End Property
End Class
Public Class ModulesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub

    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Modules"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syModules"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ModuleId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "ModuleName"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Return "Select ModuleId, ModuleName, 1 from syModules"
        End Get
    End Property
End Class
Public Class PrefixesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Prefixes"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syPrefixes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "PrefixId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "PrefixDescrip"
        End Get
    End Property
End Class
Public Class SuffixesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Suffixes"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "sySuffixes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "SuffixId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "SuffixDescrip"
        End Get
    End Property
End Class
Public Class EmpContactsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "EmpContacts"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plEmployerContact"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "EmployerContactId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "LastName"
        End Get
    End Property
End Class
Public Class EmployeesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Employees"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "hrEmployees"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "EmpId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "LastName"
        End Get
    End Property
End Class
Public Class PrioritiesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Priorities"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "cmPriority"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "PriorityId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "PriorityDescrip"
        End Get
    End Property
End Class
Public Class PositionsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Positions"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syPositions"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "PositionId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "PositionDescrip"
        End Get
    End Property
End Class
Public Class DepartmentsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Departments"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arDepartments"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "DeptId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "DeptDescrip"
        End Get
    End Property
End Class
Public Class Bill_TypeDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Bill Type"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "saBillTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "BillTypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "BillTypeDescrip"
        End Get
    End Property
End Class
Public Class Trans_CodesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Trans Codes"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "saTransCodes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "TransCodeID"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "TransCodeDescrip"
        End Get
    End Property
End Class
Public Class Billing_CodesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Billing Codes"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "saTransCodes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "TransCodeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "TransCodeDescrip"
        End Get
    End Property
End Class
Public Class TestingModelsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "TestingModels"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arTestingModels"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "TestingModelId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "TestingModelDescrip"
        End Get
    End Property
End Class
Public Class Grade_TypesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Grade Types"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arGrdTyps"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "GrdTypId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "GrdTypDescrip"
        End Get
    End Property
End Class
Public Class Time_IntervalDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Time Interval"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "cmTimeInterval"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "TimeIntervalId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "TimeIntervalDescrip"
        End Get
    End Property
End Class
Public Class BooksDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Books"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arBooks"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "BkId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "BkTitle"
        End Get
    End Property
End Class
Public Class CategoriesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Categories"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arBkCategories"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CategoryId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "CategoryDescrip"
        End Get
    End Property
End Class
Public Class Prog_TypesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Prog Types"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arProgTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ProgTypId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Description"
        End Get
    End Property
End Class
Public Class AcademicYearsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Acad. Years"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "saAcademicYears"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "AcademicYearId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "AcademicYearDescrip"
        End Get
    End Property
End Class
Public Class Crd_Card_TypesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Crd Card Types"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "saCreditCardTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CreditCardTypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "CreditCardTypeDescrip"
        End Get
    End Property
End Class
Public Class Paid_ByDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Paid By"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "saPaidBys"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "PaidById"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "PaidByDescrip"
        End Get
    End Property
End Class
Public Class Bank_AccountsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Bank Accounts"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "saBankAccounts"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "BankAcctId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "BankAcctDescrip"
        End Get
    End Property
End Class
Public Class Req_TypesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Req Types"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arReqTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ReqTypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class
Public Class Grade_ScalesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Grade Scales"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arGradeScales"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "GrdScaleId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class
Public Class RptCoursesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "RptCourses"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arReqs"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ReqId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class
Public Class ReqGroupDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "ReqGroup"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arReqs"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ReqId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class
Public Class RequirementsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Requirements"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arReqs"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ReqId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class
Public Class GradCompTypDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "GradCompTyp"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arGrdComponentTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "GrdComponentTypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class
Public Class Drop_ReasonsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Drop Reasons"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arDropReasons"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "DropReasonId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class
Public Class LOA_ReasonsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "LOA Reasons"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arLOAReasons"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "LOAReasonId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class
Public Class Probation_TypeDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Probation Type"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arProbWarningTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ProbWarningTypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class

Public Class Lead_GroupsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Lead Groups"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adLeadGroups"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "LeadGrpId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class
Public Class Student_GroupsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Student Groups"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arStudent"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "LeadGrpId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class
Public Class Program_TypesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Program Types"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arProgTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ProgTypId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Description"
        End Get
    End Property
End Class
Public Class Grade_SystemsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Grade Systems"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arGradeSystems"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "GrdSystemId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class
Public Class EmployersDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Employers"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plEmployers"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "EmployerId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "EmployerDescrip"
        End Get
    End Property
End Class
Public Class JobTypesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "JobTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plJobType"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "JobGroupId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "JobGroupDescrip"
        End Get
    End Property
End Class
Public Class BenefitsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Benefits"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plJobBenefit"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "JobBenefitId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "JobBenefitDescrip"
        End Get
    End Property
End Class
Public Class JobScheduleDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "JobSchedule"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plJobSchedule"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "JobScheduleId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "JobScheduleDescrip"
        End Get
    End Property
End Class
Public Class JobStatusDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "JobStatus"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plJobStatus"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "JobStatusId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "JobStatusDescrip"
        End Get
    End Property
End Class
Public Class FieldStudyDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "FieldStudy"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plFldStudy"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "FldStudyId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "FldStudyDescrip"
        End Get
    End Property
End Class
Public Class InterviewDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Interview"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plInterview"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "InterviewId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "InterviewDescrip"
        End Get
    End Property
End Class
Public Class SalaryTypeDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "SalaryType"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plSalaryType"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "SalaryTypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "SalaryTypeDescrip"
        End Get
    End Property
End Class
'Public Class SchoolStatusDDLMetadata
'    Inherits AdvantageDropDownListMetadata
'    Public Sub New()
'        MyBase.New()
'    End Sub
'    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
'        MyBase.New()
'        m_pullActiveItemsOnly = pullActiveItemsOnly
'    End Sub
'    Public Overrides ReadOnly Property DropDownListName() As String
'        Get
'            Return "SchoolStatus"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property TableName() As String
'        Get
'            Return "plSchoolStatus"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property ValueField() As String
'        Get
'            Return "ScStatusId"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property TextField() As String
'        Get
'            Return "ScStatusDescrip"
'        End Get
'    End Property
'End Class
Public Class TransportationDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Transportation"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plTransportation"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "TransportationId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "TransportationDescrip"
        End Get
    End Property
End Class
Public Class CollegeDivsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "CollegeDivs"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arCollegeDivisions"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CollegeDivId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "CollegeDivDescrip"
        End Get
    End Property
End Class
Public Class LeadStatusDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Private m_SQLStatement As String


    Public Sub New()
        MyBase.New()
        SetSQLStatement()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
        SetSQLStatement()
    End Sub

    Private Sub SetSQLStatement()
        Dim sb As New System.Text.StringBuilder()
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        With sb
            .Append(" SELECT DISTINCT ")
            .Append("			t1.StatusCodeId as StatusCodeId,t1.StatusCodeDescrip as StatusCodeDescrip,1 as Status")
            .Append(" FROM			  ")
            .Append("			syStatusCodes t1,sySysStatus t2,syCmpGrpCmps t3 ")
            .Append(" WHERE			  ")
            .Append("			t1.SysStatusId = t2.SysStatusId  AND t2.StatusLevelId = 1 ")
            If Me.PullActiveItemsOnly Then
                .Append("		AND	t1.StatusId='")
                .Append(strActiveGUID)
                .Append("' ")
            End If
            .Append(" AND t1.CampGrpId = t3.CampGrpId ")

            If Not IsNothing(Me.CampusId) And Me.CampusId <> "" Then
                .Append(" AND t3.CampusId='")
                .Append(Me.CampusId)
                .Append("' ")
            End If

            .Append(" Order by t1.StatusCodeDescrip ")

            m_SQLStatement = sb.ToString

        End With
    End Sub

    Public Overrides Sub SetSQLStatementForImportLeadsPage()
        MyBase.SetSQLStatementForImportLeadsPage()

        Dim sb As New System.Text.StringBuilder()
        Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid
        With sb
            .Append(" SELECT DISTINCT ")
            .Append("			t1.StatusCodeId as StatusCodeId,t1.StatusCodeDescrip as StatusCodeDescrip,1 as Status")
            .Append(" FROM			  ")
            .Append("			syStatusCodes t1,sySysStatus t2,syCmpGrpCmps t3 ")
            .Append(" WHERE			  ")
            .Append("			t1.SysStatusId = t2.SysStatusId  AND t2.StatusLevelId = 1 ")

            'For the leads import page we want to exclude any lead status that is mapped to the
            'Advantage system status of Enrolled
            .Append("           AND t2.SysStatusId <> 6 ")

            If Me.PullActiveItemsOnly Then
                .Append("		AND	t1.StatusId='")
                .Append(strActiveGUID)
                .Append("' ")
            End If
            .Append(" AND t1.CampGrpId = t3.CampGrpId ")

            If Not IsNothing(Me.CampusId) And Me.CampusId <> "" Then
                .Append(" AND t3.CampusId='")
                .Append(Me.CampusId)
                .Append("' ")
            End If

            .Append(" Order by t1.StatusCodeDescrip ")

            m_SQLStatement = sb.ToString

        End With
    End Sub

    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "LeadStatus"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "rptLeadStatus"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "StatusCodeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "StatusCodeDescrip"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Return m_SQLStatement
        End Get
    End Property
End Class
Public Class Status_CodesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Status Codes"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syStatusCodes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "StatusCodeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "StatusCodeDescrip"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim sb As New System.Text.StringBuilder()
            With sb
                .Append("SELECT ")
                .Append(Me.ValueField)
                .Append(",")
                .Append(Me.TextField)
                .Append(",")
                .Append("(Case S.Status when 'Active' then 1 else 0 end) As Status ")
                .Append(" FROM ")
                .Append(Me.TableName & " T, sySysStatus B,syStatusLevels D, ")
                If Me.PullActiveItemsOnly Then
                    .Append(" syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")
                Else
                    .Append(" syStatuses S WHERE T.StatusId=S.StatusId ")
                End If
                .Append(" AND T.sysStatusID = B.sysStatusID and B.StatusLevelId = D.StatusLevelId ")
                .Append(" AND D.StatusLevelId = 2 ")
                .Append(" ORDER BY T.StatusCodeDescrip ")
            End With
            Return sb.ToString
        End Get
    End Property
End Class
Public Class LeadEnrollStatusCodesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "LeadEnrollStatusCodes"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syStatusCodes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "StatusCodeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "StatusCodeDescrip"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim sb As New System.Text.StringBuilder
            Dim strActiveGUID As String = AdvantageCommonValues.ActiveGuid


            With sb
                .Append(" Select  DISTINCT ")
                .Append("   A.StatusCodeID as StatusCodeID,A.StatusCodeDescrip AS StatusCodeDescrip,  ")
                .Append(" 1 as Status ")
                .Append(" FROM  syStatusCodes A,sySysStatus B,syStatusLevels D,syCmpGrpCmps G ")
                .Append(" WHERE A.sysStatusID = B.sysStatusID ")
                If Me.PullActiveItemsOnly Then
                    .Append(" AND A.StatusId='")
                    .Append(strActiveGUID)
                    .Append("' ")
                End If
                .Append("   AND B.StatusLevelId=D.StatusLevelId ")
                .Append("   AND D.StatusLevelId=1 ")
                .Append("   AND B.SysStatusId=6 ")
                .Append("   AND G.CampGrpId=A.CampGrpId ")
                .Append("   AND G.CampusId='")
                .Append(Me.CampusId)
                .Append("' ")
                .Append("   ORDER BY A.StatusCodeDescrip ")

                '.Append("   select Distinct t3.StatusCodeId as StatusCodeId,t3.StatusCodeDescrip as StatusCodeDescrip,1 as Status from syLeadStatusChanges t1,syLeadStatusChangePermissions t2,syStatusCodes t3 where ")
                '.Append("   t1.OrigStatusId = t3.StatusCodeId and t1.NewStatusId in (select Distinct StatusCodeId from syStatusCodes where SysStatusId=6) ")
                '.Append("   and t1.CampGrpId in ")
                '.Append("   (select Distinct CampGrpId from syCmpGrpCmps where CampusId='")
                '.Append(Me.CampusId)
                '.Append("') ")
                '.Append("   and ")
                '.Append("   t1.LeadStatusChangeId = t2.LeadStatusChangeId and RoleId in  ")
                '.Append("   (select RoleId from syUsersRolesCampGrps t4,syCmpGrpCmps t5 where t4.CampGrpId=t5.CampGrpId and ")
                '.Append("   t4.UserId='" & UserId & "' and CampusId=CampusId='")
                '.Append(Me.CampusId)
                '.Append("') ")
            End With
            Return sb.ToString
        End Get
    End Property
End Class
Public Class AdvStatusDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "AdvStatus"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "sySysStatus"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "SysStatusId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "SysStatusDescrip"
        End Get
    End Property
End Class
Public Class Adv_StatusesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Adv Statuses"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "sySysStatus"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "SysStatusId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "SysStatusDescrip"
        End Get
    End Property
End Class
Public Class LendersDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Lenders"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "faLenders"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "LenderId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "LenderDescrip"
        End Get
    End Property

End Class
Public Class Address_TypesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Address Types"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plAddressTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "AddressTypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "AddressDescrip"
        End Get
    End Property
End Class
Public Class AddressStatusesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "AddressStatuses"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syAddressStatuses"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "AddressStatusId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "AddressStatusDescrip"
        End Get
    End Property
End Class
Public Class Phone_TypesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Phone Types"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syPhoneType"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "PhoneTypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "PhoneTypeDescrip"
        End Get
    End Property
End Class
Public Class Phone_StatusesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Phone Statuses"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syPhoneStatuses"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "PhoneStatusID"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "PhoneStatusDescrip"
        End Get
    End Property
End Class
Public Class Earning_MethodDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Earning Method"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "saTuitionEarnings"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "TuitionEarningId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "TuitionEarningsDescrip"
        End Get
    End Property
End Class
Public Class EnrollmentsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Enrollments"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arStuEnrollments"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "StuEnrollId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "StuEnrollId"
        End Get
    End Property
End Class
Public Class Tuition_CatsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Tuition Cats"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "saTuitionCategories"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "TuitionCategoryId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "TuitionCategoryDescrip"
        End Get
    End Property
End Class
Public Class Billing_MethodsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Billing Methods"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "saBillingMethods"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "BillingMethodId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "BillingMethodDescrip"
        End Get
    End Property
End Class
Public Class Fund_SourcesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Fund Sources"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "saFundSources"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "FundSourceId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "FundSourceDescrip"
        End Get
    End Property
End Class
Public Class LeadCategoryDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "LeadCategory"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adSourceCatagory"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "SourceCatagoryID"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "SourceCatagoryDescrip"
        End Get
    End Property
End Class
Public Class LeadAdvDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "LeadAdv"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adSourceAdvertisement"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "sourceadvid"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "sourceadvdescrip"
        End Get
    End Property
End Class
Public Class AdmissionsRepDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "AdmissionsRep"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            'Return "rptAdmissionsRep"
            Return "syUsers"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            'Return "AdmissionsRepID"
            Return "UserId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            'Return "AdmissionsRepDescrip"
            Return "FullName"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim sb As New System.Text.StringBuilder()


            With sb

                .Append("SELECT distinct ")

                .Append("T." & Me.ValueField)

                .Append(",")

                .Append("T." & Me.TextField)

                .Append(",")

                .Append("(1) As Status ")

                .Append(" FROM ")

                .Append(Me.TableName & " T,syUsersRolesCampGrps b, syRoles c, sySysRoles d ")

                'If Me.PullActiveItemsOnly Then

                '    '.Append(" , syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")
                '    If Not Me.CampusId Is Nothing Then

                '        .Append(" WHERE CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")

                '        .Append(Me.CampusId)

                '        .Append("') ")

                '    End If

                'Else

                '    ' .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId ")

                '    If Not Me.CampusId Is Nothing Then

                '        .Append("WHERE CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")

                '        .Append(Me.CampusId)

                '        .Append("') ")

                '    End If

                'End If
                .Append("WHERE T.UserId = b.UserId ")
                .Append("AND b.RoleId = c.RoleId ")
                .Append("AND c.SysRoleId = d.SysRoleId ")
                .Append("AND d.SysRoleId = 3 ")
                .Append("ORDER BY Status, ")
                .Append(Me.TextField)

            End With

            Return sb.ToString
        End Get
    End Property
End Class
Public Class LeadSourceTypesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "LeadSourceTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adSourceType"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "SourceTypeID"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "SourceTypeDescrip"
        End Get
    End Property
End Class
Public Class AdvIntervalDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "AdvInterval"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adAdvInterval"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "AdvIntervalId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "AdvIntervalDescrip"
        End Get
    End Property
End Class
Public Class Status_LevelsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Status Levels"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syStatusLevels"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "StatusLevelId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "StatusLevelDescrip"
        End Get
    End Property
End Class
Public Class SkillGroupsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "SkillGroups"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plSkillGroups"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "SkillGrpId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "SkillGrpName"
        End Get
    End Property
End Class
Public Class ExtCurrGrpDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "ExtCurrGrp"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adExtraCurrGrp"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ExtraCurrGrpId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "ExtraCurrGrpDescrip"
        End Get
    End Property
End Class
Public Class Employer_FeesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Employer Fees"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plFee"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "FeeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "FeeDescrip"
        End Get
    End Property
End Class
Public Class FeesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Fees"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plFee"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "FeeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "FeeDescrip"
        End Get
    End Property
End Class
'Public Class Award_YearsDDLMetadata
'    Inherits AdvantageDropDownListMetadata
'    Public Sub New()
'        MyBase.New()
'    End Sub
'    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
'        MyBase.New()
'        m_pullActiveItemsOnly = pullActiveItemsOnly
'    End Sub
'    Public Overrides ReadOnly Property DropDownListName() As String
'        Get
'            Return "Award Years"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property TableName() As String
'        Get
'            Return "faAwardYears"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property ValueField() As String
'        Get
'            Return "AwardYearId"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property TextField() As String
'        Get
'            Return "AwardYearDescrip"
'        End Get
'    End Property
'End Class
Public Class InstructorDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Instructor"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "rptInstructor"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "InstructorId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "InstructorDescrip"
        End Get
    End Property
End Class
Public Class FamilyIncomeDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "FamilyIncome"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syFamilyIncome"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "FamilyIncomeID"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "FamilyIncomeDescrip"
        End Get
    End Property
End Class
Public Class HRDepartmentsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "HRDepartments"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syHRDepartments"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "HRDepartmentId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "HRDepartmentDescrip"
        End Get
    End Property
End Class
Public Class PlacedByDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "PlacedBy"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plHowPlaced"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "HowPlacedId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "HowPlacedDescrip"
        End Get
    End Property
End Class
Public Class FullPartTimeDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "FullPartTime"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adFullPartTime"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "FullTimeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "FullPartTimeDescrip"
        End Get
    End Property
End Class
Public Class Employer_JobsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Employer Jobs"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plEmployerJobs"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "EmployerJobId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "EmployerJobTitle"
        End Get
    End Property
End Class
Public Class ExpertiseLevelDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "ExpertiseLevel"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adExpertiseLevel"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ExpertiseId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "ExpertiseDescrip"
        End Get
    End Property
End Class
Public Class UsersDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Users"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syUsers"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "UserId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "FullName"
        End Get
    End Property
End Class
Public Class SchemaDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Schema"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syMessageSchemas"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "MessageTemplateId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "TemplateDescrip"
        End Get
    End Property
End Class
Public Class DTypeIdDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "DTypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "sySdfDTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "DtypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Dtype"
        End Get
    End Property
End Class
Public Class AttendanceTypeDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Attendance Types"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arAttendTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "AttendTypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class

Public Class HousingTypeDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Housing Types"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arHousing"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "HousingId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class

Public Class DependencyTypeDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Dependency Typs"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adDependencyTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "DependencyTypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class

Public Class GeographicTypeDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Geographic Typs"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adGeographicTypes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "GeographicTypeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class

Public Class DegCertSeekingTypeDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "DegreeCert Typs"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adDegCertSeeking"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "DegCertSeekingId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class

Public Class AdminCriteriaTypeDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "admin criteria"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adAdminCriteria"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "AdminCriteriaId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class
Public Class TestsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Entrance Tests"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "adReqs"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "adReqId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Return "Select adReqId, Descrip, 1 from adReqs where adreqTypeId=1"
        End Get
    End Property
End Class
Public Class GradePoliciesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Grade Policies"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syGrdPolicies"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "GrdPolicyId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class
'Public Class Award_TypesDDLMetadata
'    Inherits AdvantageDropDownListMetadata
'    Public Sub New()
'        MyBase.New()
'    End Sub
'    Public Overrides ReadOnly Property DropDownListName() As String
'        Get
'            Return "AwardTypes"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property TableName() As String
'        Get
'            Return "faAwardTypes"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property ValueField() As String
'        Get
'            Return "AwardTypeId"
'        End Get
'    End Property
'    Public Overrides ReadOnly Property TextField() As String
'        Get
'            Return "AwardTypedescrip"
'        End Get
'    End Property
'End Class
Public Class ServicersDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Servicers"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "faLenders"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "LenderId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "LenderDescrip"
        End Get
    End Property

End Class
Public Class GuarantorsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Guarantors"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "faLenders"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "LenderId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "LenderDescrip"
        End Get
    End Property

End Class
Public Class RelationsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Relations"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syRelations"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "RelationId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "RelationDescrip"
        End Get
    End Property
End Class
Public Class CoursesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Courses"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arReqs"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "ReqId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If




            Dim sb As New System.Text.StringBuilder()
            With sb
                .Append("SELECT ")
                .Append(Me.ValueField)
                .Append(",")
                .Append(" '(' + Code + ') ' + Descrip ")
                .Append(",")
                .Append("(Case S.Status when 'Active' then 1 else 0 end) As Status ")
                .Append(" FROM ")
                .Append(Me.TableName)
                If Me.PullActiveItemsOnly Then
                    .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")

                    If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                        '.Append(" AND T.Code not like 'SH%'  ")
                        'Code modified to assume that all courses mapped to Dictation/Speed Test Component will be SH Courses
                        .Append(" and T.ReqId not in (Select Distinct ReqId from arBridge_GradeComponentTypes_Courses)    ")
                    End If

                    If Not Me.CampusId Is Nothing Then

                        .Append("AND T.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")

                        .Append(Me.CampusId)

                        .Append("') ")

                    End If

                Else
                    .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId ")
                    If MyAdvAppSettings.AppSettings("ShowCollegeofCourtReportingPages").ToString.ToLower = "yes" Then
                        '.Append(" AND T.Code not like 'SH%'  ")
                        'Code modified to assume that all courses mapped to Dictation/Speed Test Component will be SH Courses
                        .Append(" and T.ReqId not in (Select Distinct ReqId from arBridge_GradeComponentTypes_Courses)    ")
                    End If
                    If Not Me.CampusId Is Nothing Then

                        .Append("AND T.CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")

                        .Append(Me.CampusId)

                        .Append("') ")

                    End If

                End If
                .Append(" and ReqTypeId = 1 ")
                .Append("ORDER BY Descrip,Code ")
                '.Append(Me.TextField)
            End With
            Return sb.ToString
        End Get
    End Property

End Class
Public Class CampusDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Campuses"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syCampuses"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CampusId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "CampDescrip"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim sb As New System.Text.StringBuilder()
            'With sb
            '    .Append("SELECT distinct a.UserId,a.FullName ")
            '    .Append("FROM syUsers a, syUsersRolesCampGrps b, syRoles c ")
            '    .Append("WHERE a.UserId = b.UserId ")
            '    .Append("AND b.RoleId = c.RoleId ")
            '    .Append("AND c.SysRoleId = 2 ")
            '    .Append("AND	CampGrpId IN (Select CampGrpId from syCmpGrpCmps where CampusId = ? ) ")
            '    .Append("ORDER BY a.FullName ")
            'End With



            With sb

                .Append("SELECT distinct ")

                .Append("T." & Me.ValueField)

                .Append(",")

                .Append("T." & Me.TextField)

                .Append(",")

                .Append("(1) As Status ")

                .Append(" FROM ")

                .Append(Me.TableName & " T ")

                If Me.PullActiveItemsOnly Then

                    '.Append(" , syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")

                    If Not Me.CampusId Is Nothing Then

                        .Append("WHERE T.CampusId='")

                        .Append(Me.CampusId)

                        .Append("' ")

                    End If

                Else

                    If Not Me.CampusId Is Nothing Then

                        .Append("WHERE T.CampusId='")

                        .Append(Me.CampusId)

                        .Append("' ")

                    End If

                End If

                .Append("ORDER BY Status, ")
                .Append(Me.TextField)

            End With

            Return sb.ToString
        End Get
    End Property
End Class
Public Class PeriodsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Periods"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syPeriods"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "PeriodId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "PeriodDescrip"
        End Get
    End Property
End Class
Public Class InstructorsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Instructors"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syUsers"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "UserId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "FullName"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim sb As New System.Text.StringBuilder()


            With sb

                .Append("SELECT distinct ")

                .Append("T." & Me.ValueField)

                .Append(",")

                .Append("T." & Me.TextField)

                .Append(",")

                .Append("(1) As Status ")

                .Append(" FROM ")

                .Append(Me.TableName & " T,syUsersRolesCampGrps b, syRoles c ")

                If Me.PullActiveItemsOnly Then

                    '.Append(" , syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")
                    .Append(" where T.AccountActive=1 ")

                    If Not Me.CampusId Is Nothing Then

                        .Append(" and CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")

                        .Append(Me.CampusId)

                        .Append("') ")

                    End If

                Else

                    ' .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId ")

                    If Not Me.CampusId Is Nothing Then

                        .Append("WHERE CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")

                        .Append(Me.CampusId)

                        .Append("') ")

                    End If

                End If
                .Append("AND T.UserId = b.UserId ")
                .Append("AND b.RoleId = c.RoleId ")
                .Append("AND c.SysRoleId = 2 ")
                .Append("ORDER BY Status, ")
                .Append(Me.TextField)

            End With

            Return sb.ToString
        End Get
    End Property

End Class
Public Class Employer_GroupsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "EmployerGroups"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "plEmployers"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "EmployerId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "EmployerDescrip"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim sb As New System.Text.StringBuilder()
            With sb
                .Append("SELECT ")
                .Append(Me.ValueField)
                .Append(",")
                .Append(Me.TextField)
                .Append(",")
                .Append("(Case S.Status when 'Active' then 1 else 0 end) As Status ")
                .Append(" FROM ")
                .Append(Me.TableName)
                If Me.PullActiveItemsOnly Then
                    .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")
                Else
                    .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId ")
                End If
                .Append(" and GroupName=1 ")
            End With
            Return sb.ToString
        End Get
    End Property
End Class
Public Class Academic_AdvisorsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Academic Advisors"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syUsers"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "UserId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "FullName"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim sb As New System.Text.StringBuilder()


            With sb

                .Append("SELECT distinct ")

                .Append("T." & Me.ValueField)

                .Append(",")

                .Append("T." & Me.TextField)

                .Append(",")

                .Append("(1) As Status ")

                .Append(" FROM ")

                .Append(Me.TableName & " T,syUsersRolesCampGrps b, syRoles c, sySysRoles d ")

                If Me.PullActiveItemsOnly Then

                    '.Append(" , syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")
                    If Not Me.CampusId Is Nothing Then

                        .Append(" WHERE CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")

                        .Append(Me.CampusId)

                        .Append("') ")

                    End If

                Else

                    ' .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId ")

                    If Not Me.CampusId Is Nothing Then

                        .Append("WHERE CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")

                        .Append(Me.CampusId)

                        .Append("') ")

                    End If

                End If
                .Append("AND T.UserId = b.UserId ")
                .Append("AND b.RoleId = c.RoleId ")
                .Append("AND c.SysRoleId = d.SysRoleId ")
                .Append("AND d.SysRoleId = 4 ")
                .Append("ORDER BY Status, ")
                .Append(Me.TextField)

            End With

            Return sb.ToString
        End Get
    End Property

End Class
Public Class Financial_AdvisorsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Financial Advisors"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syUsers"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "UserId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "FullName"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim sb As New System.Text.StringBuilder()


            With sb

                .Append("SELECT distinct ")

                .Append("T." & Me.ValueField)

                .Append(",")

                .Append("T." & Me.TextField)

                .Append(",")

                .Append("(1) As Status ")

                .Append(" FROM ")

                .Append(Me.TableName & " T,syUsersRolesCampGrps b, syRoles c, sySysRoles d ")

                If Me.PullActiveItemsOnly Then

                    '.Append(" , syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")

                    If Not Me.CampusId Is Nothing Then

                        .Append("WHERE CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")

                        .Append(Me.CampusId)

                        .Append("') ")

                    End If

                Else

                    ' .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId ")

                    If Not Me.CampusId Is Nothing Then

                        .Append("WHERE CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")

                        .Append(Me.CampusId)

                        .Append("') ")

                    End If

                End If
                .Append("AND T.UserId = b.UserId ")
                .Append("AND b.RoleId = c.RoleId ")
                .Append("AND c.SysRoleId = d.SysRoleId ")
                .Append("AND d.SysRoleId = 7 ")
                .Append("ORDER BY Status, ")
                .Append(Me.TextField)

            End With

            Return sb.ToString
        End Get
    End Property

End Class
Public Class CourseCategoriesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "CourseCategories"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arCourseCategories"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CourseCategoryId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Descrip"
        End Get
    End Property
End Class
Public Class Admission_RepsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Admission Reps"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "syUsers"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "UserId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "FullName"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim sb As New System.Text.StringBuilder()


            With sb

                .Append("SELECT distinct ")

                .Append("T." & Me.ValueField)

                .Append(",")

                .Append("T." & Me.TextField)

                .Append(",")

                .Append("(1) As Status ")

                .Append(" FROM ")

                .Append(Me.TableName & " T,syUsersRolesCampGrps b, syRoles c, sySysRoles d ")

                If Me.PullActiveItemsOnly Then

                    '.Append(" , syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")

                    If Not Me.CampusId Is Nothing Then

                        .Append(" WHERE CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")

                        .Append(Me.CampusId)

                        .Append("') ")

                    End If

                Else

                    ' .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId ")

                    If Not Me.CampusId Is Nothing Then

                        .Append("WHERE CampGrpId IN (SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId='")

                        .Append(Me.CampusId)

                        .Append("') ")

                    End If

                End If
                .Append("AND T.UserId = b.UserId ")
                .Append("AND b.RoleId = c.RoleId ")
                .Append("AND c.SysRoleId = d.SysRoleId ")
                .Append("AND d.SysRoleId = 3 ")
                .Append("ORDER BY Status, ")
                .Append(Me.TextField)

            End With

            Return sb.ToString
        End Get
    End Property

End Class
Public Class StudentsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Students"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arStudent"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "StudentId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "FullName"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim sb As New System.Text.StringBuilder()
            With sb
                .Append("SELECT ")
                .Append(Me.ValueField)
                .Append(",")
                .Append("FullName = (Case when (MiddleName is NULL) then (FirstName + ' ' + LastName) ")
                .Append("               Else (FirstName + ' ' + LastName + ' ' + MiddleName) End) ")
                .Append(",")
                .Append("(Case S.Status when 'Active' then 1 else 0 end) As Status ")
                .Append(" FROM ")
                .Append(Me.TableName)
                If Me.PullActiveItemsOnly Then
                    .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")
                Else
                    .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId ")
                End If
            End With
            Return sb.ToString
        End Get
    End Property
End Class
Public Class StudentsWithEnrollmentsDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Students"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arStudent"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "StudentId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "FullName"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String
        Get
            Dim sb As New System.Text.StringBuilder()
            With sb
                .Append("SELECT ")
                .Append(Me.ValueField)
                .Append(",")
                .Append("FullName = (Case when (MiddleName is NULL) then (FirstName + ' ' + LastName) ")
                .Append("               Else (FirstName + ' ' + LastName + ' ' + MiddleName) End) ")
                .Append(",")
                .Append("(Case S.Status when 'Active' then 1 else 0 end) As Status ")
                .Append(" FROM ")
                .Append(Me.TableName & " T,arStuEnrollments U ")
                If Me.PullActiveItemsOnly Then
                    .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")
                Else
                    .Append(" T, syStatuses S WHERE T.StatusId=S.StatusId ")
                End If
                .Append(" AND T.StudentId = U.StudentId ")
            End With
            Return sb.ToString
        End Get
    End Property
End Class
Public Class System_Transaction_CodesDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "SystemTransactionCodes"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "saSysTransCodes"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "SysTransCodeId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "Description"
        End Get
    End Property
End Class
Public Class CohortStartDateDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "CohortStartDate"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arClassSections"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CohortStartDate"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "CohortStartDate"
        End Get
    End Property
    Public Overrides ReadOnly Property OverrideSqlStatement() As String

        Get
            Dim sb As New System.Text.StringBuilder()
            With sb
                .Append("SELECT Distinct ")
                .Append(Me.ValueField)
                .Append(",")
                .Append(" Convert(varchar(10),CS.CohortStartDate,101)as CohortStartDate  ")
                '.Append(Me.TextField)
                .Append(",")
                .Append("(Case S.Status when 'Active' then 1 else 0 end) As Status  ")
                .Append(" ,year(CS.CohortStartDate) ")
                .Append(" FROM ")
                .Append(Me.TableName & "  CS,arterm T, arPrograms B, arPrgVersions C, ")
                If Me.PullActiveItemsOnly Then
                    .Append(" syStatuses S WHERE T.StatusId=S.StatusId AND S.Status='Active' ")
                Else
                    .Append(" syStatuses S WHERE T.StatusId=S.StatusId ")
                End If
                .Append("AND CS.TermId=T.TermId and CS.CohortStartDAte is not null ")

                .Append("AND (T.ProgId = B.ProgId  or T.ProgId is null) ")

                .Append("AND B.ProgId = C.ProgId ")

                .Append("AND T.CampGrpId IN ( ")
                .Append(" SELECT Distinct CampGrpId FROM syCmpGrpCmps WHERE CampusId = '")
                .Append(Me.CampusId)
                .Append("' ")
                .Append(" AND CampGrpId not in (SELECT Distinct CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
                .Append(" Union ")
                .Append(" SELECT Distinct CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
                .Append(" order by year(CS.CohortStartDate) ")
            End With

            '.Append("FROM syCmpGrpCmps ")

            '.Append("WHERE CampusId = '")
            '.Append(Me.CampusId)
            '.Append("') ")
            '.Append("AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) ")
            '.Append("OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') ")
            Return sb.ToString
        End Get
    End Property
End Class

Public Class CredentialLevelDDLMetadata
    Inherits AdvantageDropDownListMetadata
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal pullActiveItemsOnly As Boolean)
        MyBase.New()
        m_pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Overrides ReadOnly Property DropDownListName() As String
        Get
            Return "Credential Level"
        End Get
    End Property
    Public Overrides ReadOnly Property TableName() As String
        Get
            Return "arProgCredential"
        End Get
    End Property
    Public Overrides ReadOnly Property ValueField() As String
        Get
            Return "CredentialId"
        End Get
    End Property
    Public Overrides ReadOnly Property TextField() As String
        Get
            Return "CredentialDescription"
        End Get
    End Property
End Class