Public Class NameValueListInfo
    Private _DDLName As DDLLists

    Public Enum DDLLists
        Countries
        States
        Statuses
    End Enum

    Public Sub New(ByVal ddlName As DDLLists)
        _DDLName = ddlName
    End Sub

    Public Overrides Function ToString() As String

        Return _DDLName.ToString

    End Function


End Class
