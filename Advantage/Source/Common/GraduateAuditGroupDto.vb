﻿Imports System.Globalization

''' <summary>
''' DTO for GraduatedAuditGroupDto
''' </summary>
Public Class GraduateAuditGroupDto

    ''' <summary>
    ''' Create as initialization GraduateAuditGroupDto.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>To use this with GraduateAudit Level must be Nothing</remarks>
    public Shared Function Factory() As  GraduateAuditGroupDto
        Dim r As GraduateAuditGroupDto = New GraduateAuditGroupDto()
        r.Credits = 0
        r.CreditsEarned = 0
        r.CreditsRemaining = 0 
        r.Hours = 0
        r.HoursCompleted = 0 
        r.HoursRemaining = 0
        r.Score = 0
        return r
    End Function


    Public Property PrgVerId As String
    Public property Descrip as String
    Public Property EquivCourseDesc As String
    Public property Code As String
    Public Property ReqId As String
    Public property StartDate As DateTime?
    Public Property ReqTypeId As Int32?
    'Public Property ParentId As String
    Public Property Level As Int32?
    Public Property IsCreditsEarned As Boolean
    Public Property Credits As Decimal
    Public Property Hours As Decimal
    Public Property Score As Decimal?
    Public Property Grade As String
    Public Property CreditsEarned As Decimal?
    Public Property CreditsRemaining As Decimal?
    Public Property HoursCompleted As Decimal?
    Public Property HoursRemaining As Decimal?
    Public Property GrdSysDetailId As String
    Public Property IsContinuingEd As Boolean
    Public Property IsTransferGrade As Boolean

End Class

Public Class GraduateAuditGroupRow

    ''' <summary>
    ''' Create as initialize GraduateAuditGroupRow.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>To use this with GraduateAudit Level must be Nothing</remarks>
    public Shared Function Factory() As  GraduateAuditGroupRow
        Dim r As GraduateAuditGroupRow = New GraduateAuditGroupRow()
        return r
    End Function

    public Shared Function AddGraduateAuditGroupDto(dto As GraduateAuditGroupDto) As GraduateAuditGroupRow
        Dim r As GraduateAuditGroupRow = New GraduateAuditGroupRow()
        r.Code = dto.Code
        r.Credits = dto.Credits.ToString("0.00", CultureInfo.InvariantCulture)
        r.CreditsEarned = dto.CreditsEarned.GetValueOrDefault(0).ToString("0.00", CultureInfo.InvariantCulture)
        r.CreditsRemaining = dto.CreditsRemaining.GetValueOrDefault(0).ToString("0.00", CultureInfo.InvariantCulture)
        r.Descrip = dto.Descrip
        r.EquivCourseDesc = dto.EquivCourseDesc
        r.Grade = dto.Grade
        r.GrdSysDetailId = dto.GrdSysDetailId
        r.Hours = dto.Hours.ToString("0.00", CultureInfo.InvariantCulture)
        r.HoursCompleted = dto.HoursCompleted.GetValueOrDefault(0).ToString("0.00", CultureInfo.InvariantCulture)
        r.HoursRemaining = dto.HoursRemaining.GetValueOrDefault(0).ToString("0.00", CultureInfo.InvariantCulture)
        r.Level = dto.Level.ToString()
        r.Score = dto.Score.ToString()
        return r
       
    End Function


    Public Property PrgVerId As String
    Public property Descrip as String
    Public Property EquivCourseDesc As String
    Public property Code As String
    Public Property ReqId As String
    Public property StartDate As String
    Public Property ReqTypeId As String
    Public Property Level As String
    Public Property IsCreditsEarned As String
    Public Property Credits As String
    Public Property Hours As String
    Public Property Score As String
    Public Property Grade As String
    Public Property CreditsEarned As String
    Public Property CreditsRemaining As String
    Public Property HoursCompleted As String
    Public Property HoursRemaining As String
    Public Property GrdSysDetailId As String
    Public Property IsContinuingEd As String
    Public Property IsTransferGrade As String

End Class

