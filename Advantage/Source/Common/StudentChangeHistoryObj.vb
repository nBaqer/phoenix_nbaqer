﻿
''' <summary>
''' Hold the field of the table StudentChangeHistory.
''' </summary>
''' <remarks>
''' Use this to go and for from database and table ArStudentStatusChange 
'''</remarks>
Public Class StudentChangeHistoryObj
    Property StuEnrollId() As String

    ''' <summary>
    ''' Initial Student School Status Guid as String 
    ''' </summary>
    ''' <value>A Guid as String</value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' Original School defined student status
    ''' </remarks>
    Property OrigStatusGuid() As String

    ''' <summary>
    ''' Destination Student School Status Guid as String 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property NewStatusGuid() As String

    Property CampusId() As String
    Property ModDate() As DateTime
    Property ModUser() As String
    Property IsRevelsal() As Integer

    ''' <summary>
    ''' Use only in Student Status Change Table to fill Drop Reason
    ''' if you are reported other state different to Drop leave it to Nothing.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property DropReasonGuid() As String
    Property DateOfChange() As DateTime?
    ''' <summary>
    ''' LDA [Last Attendance Date]
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property Lda() As DateTime?

    'Extra fields other functionality.....

    ''' <summary>
    ''' Used to Go to Current Attending.
    ''' When a student is re- enrolled
    ''' </summary>
    ''' <value>Date</value>
    ''' <returns>Date</returns>
    ''' <remarks>It is used only when the student is re-enrolled</remarks>
    Property DateOfReenrollment() As DateTime?


    ''' <summary>
    ''' Used in Graduate to Active
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property ExpGradDate As DateTime

    ''' <summary>
    ''' Date Determined: 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property DateDetermined() As DateTime?

    ''' <summary>
    ''' This is a Guid and can be Active o Inactive
    ''' The value comes from syStatuses
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property StudentStatus() As String

    ''' <summary>
    ''' Used By Change Status To Suspension
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property StartDate() As Date?

    ''' <summary>
    ''' Used By Change Status To Suspension
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property EndDate() As DateTime?

    ''' <summary>
    ''' Used only in LOA
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property LOARequestDate() As DateTime?

    ''' <summary>
    ''' This is used only for Probation Status and give the level of the warning
    ''' </summary>
    ''' <value>
    ''' 1 Academic Probation
    ''' 2 Disciplinary Probation
    ''' 3 Warning</value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property ProbWarningTypeId() As Integer

    ''' <summary>
    ''' Arbitrary Reason of somethings
    ''' Use in Probation
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property Reason() As String

    ''' <summary>
    ''' Hold the Guid as String of the possible LOA Reason
    ''' </summary>
    ''' <value>Can be a Guid as string or Nothing</value>
    ''' <returns>The Guid as String or Nothing</returns>
    ''' <remarks>Use this to fill the LOA reason field.</remarks>
    Property LoaReasonGuid() As String

    ''' <summary>
    ''' Original or previous System Student Status ID (int)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property OldSysStatusId() As Integer

    ''' <summary>
    ''' Destination System Student Status Id (int)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property NewSysStatusId() As Integer

    ''' <summary>
    ''' previous Student Status Change Id as string
    ''' used to update the get info from previous Probation (arStuProbWarnings), LOA (arStudentLOAs), or Suspension (arStdSuspensions)
    ''' in the transaction update change status
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property PreviousStudentStatusChangeId() As String

    ''' <summary>
    ''' The SAP Period (only use by SAP and FASAP)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SapPeriod() As Integer

    ''' <summary>
    ''' The User Who Requested the Status Change
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RequestedBy As String

    ''' <summary>
    ''' The Case Number associated with the Requested Status Change
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CaseNumber() As String

End Class
