Public Class AnnouncementInfo
    '
    '   AnnouncementInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _AnnouncementId As String
    Private _moduleId As Integer
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _AnnouncementId = Guid.NewGuid.ToString
        _moduleId = 0
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property AnnouncementId() As String
        Get
            Return _AnnouncementId
        End Get
        Set(ByVal Value As String)
            _AnnouncementId = Value
        End Set
    End Property
    Public Property ModuleId() As Integer
        Get
            Return _moduleId
        End Get
        Set(ByVal Value As Integer)
            _moduleId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class

