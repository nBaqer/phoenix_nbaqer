Public Class OverridenConflictInfo

#Region "Private Variable Declaration"

    Private _overridenConflictId As String
    Private _leftClsSectionId As String
    Private _rightClsSectionId As String
    Private _modUser As String
    Private _modDate As DateTime

#End Region
#Region " Public Constructor"
    Public Sub New()
        _overridenConflictId = System.Guid.NewGuid.ToString
        _leftClsSectionId = System.Guid.Empty.ToString
        _rightClsSectionId = System.Guid.Empty.ToString
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region

#Region "Class Section Properties"

    Public Property OverridenConflictId() As String
        Get
            OverridenConflictId = _overridenConflictId
        End Get
        Set(ByVal Value As String)
            _overridenConflictId = Value
        End Set
    End Property
    Public Property LeftClsSectionId() As String
        Get
            LeftClsSectionId = _leftClsSectionId
        End Get
        Set(ByVal Value As String)
            _leftClsSectionId = Value
        End Set
    End Property
    Public Property RightClsSectionId() As String
        Get
            RightClsSectionId = _rightClsSectionId
        End Get
        Set(ByVal Value As String)
            _rightClsSectionId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
