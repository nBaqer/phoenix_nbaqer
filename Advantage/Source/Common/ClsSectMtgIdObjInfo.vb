Public Class ClsSectMtgIdObjInfo
#Region "Private Variable Declaration"
    Private _ClsSectMtgId As Guid
    Private _count As Integer
    Private _txtboxname As String
    Private _dbmsg As String
    Private _modDate As DateTime
    Private _errorString As String '(used for UpdateClassSection() only)
#End Region

#Region "Class Section Meeting Properties"
    Public Property ClsSectMtgId() As Guid
        Get
            ClsSectMtgId = _ClsSectMtgId
        End Get
        Set(ByVal Value As Guid)
            _ClsSectMtgId = Value
        End Set
    End Property
    Public Property count() As Integer
        Get
            count = _count
        End Get
        Set(ByVal Value As Integer)
            _count = Value
        End Set
    End Property

    Public Property txtboxname() As String
        Get
            txtboxname = _txtboxname
        End Get
        Set(ByVal Value As String)
            _txtboxname = Value
        End Set
    End Property
    Public Property dbmsg() As String
        Get
            dbmsg = _dbmsg
        End Get
        Set(ByVal Value As String)
            _dbmsg = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property

    Public Property ErrorString() As String
        Get
            Return _errorString
        End Get
        Set(ByVal Value As String)
            _errorString = Value
        End Set
    End Property
#End Region

End Class
