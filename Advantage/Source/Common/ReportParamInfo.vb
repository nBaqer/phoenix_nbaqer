<Serializable()>
Public Class ReportParamInfo

#Region "Private Variables"

    Private m_LogoFilePath As String
    Private m_TermStartDate As Date
    '' New Variable Add on 13-03-2009 By Vijay Ramteke
    '' New Variable Add By Vijay Ramteke on May, 11 2009

    '' New Variable Added By Kamalesh Ahuja on Oct, 20 2009

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '' New Variable Added By Kamalesh Ahuja on Oct, 08 2010
    Private m_BaseCAOnRegClasses As Boolean

    '*New Variables Added by Dennis 8/19/09 to support StudentGPAReport
    Private m_SORT_CumGPA, m_SORT_LastName, m_SORT_TermGPA As Boolean
    Private m_CumGPARangeBegin, m_CumGPARangeEnd As Decimal
    Private m_TermCreditsRangeBegin, m_TermCreditsRangeEnd As Decimal
    Private m_TermGPARangeBegin, m_TermGPARangeEnd As Decimal


    Private m_studentid As String

    '' New Variable Add By Vijay Ramteke on Feb 08, 2010
    Private m_ShowCosts As Boolean
    Private m_ShowExpectedFunding As Boolean
    Private m_ShowCategoryBreakdown As Boolean
    Private m_ShowTermOrModule As Boolean
    '' New Variable Add By Vijay Ramteke on Feb 08, 2010
    '' New Variable Add By Vijay Ramteke on March 05, 2010
    Private m_ShowGroupByEnrollment As Boolean
    Private m_ShowDisbNotBeenPaid As Boolean
    Private m_ShowTotalCost As Boolean
    Private m_ShowCurrentBalance As Boolean
    '' New Variable Add By Vijay Ramteke on March 05, 2010
    '' New Variable Add By Kamalesh Ahuja on May 31, 2010 to resolve mantis issue id 18868
    Private m_ShowLegalDisclaimer As Boolean
    '' New Variable Add By Kamalesh Ahuja on May 31, 2010 to resolve mantis issue id 18868
    '' New Variable Add By Vijay Ramteke On July 01, 2010 For Mantis ID 19264
    '' New Variable Add By Vijay Ramteke On July 01, 2010 For Mantis ID 19264
    '' New Variable Add By Vijay Ramteke On July 08, 2010 For Mantis ID 11856
    Private m_NumberConsecutiveAbsentDays As String
    Private m_NumberConsecutiveAbsentDays2 As String
    Private m_UsedScheduledDays As Boolean
    Private m_Operator As String
    '' New Variable Add By Vijay Ramteke On July 08, 2010 For Mantis ID 11856
    '' New Variable Add By Vijay Ramteke On September 28, 2010 For Mantis ID 17685
    Private m_ShowTransferCampus As Boolean
    Private m_ShowTransferProgran As Boolean
    Private m_ShowLDA As Boolean
    '' New Variable Add By Vijay Ramteke On September 28, 2010 For Mantis ID 17685
    '' New Property Add By Vijay Ramteke On October 27, 2010 For Rally Id US1178
    Private m_ShowUseStuCurrStatus As Boolean
    '' New Property Add By Vijay Ramteke On October 27, 2010 For Rally Id US1178

    ''Added to add the signature line for Attendance reports -- Clock project
    Private m_ShowUseSignLineForAttnd As Boolean

    Private m_ShowOfficialTranscript As Boolean

    Private m_ShowTermProgressDescription As Boolean

#End Region

#Region "Public Properties"

    ''' <summary>
    ''' Export type pdf, wpf. this is passed to report to explain the exprot type.
    ''' it is use in suvareport (majorminor)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ExportType() As String

    ''' <summary>
    ''' Two values actually. TAKEBEST and TAKELAST
    ''' TAKELAST instruct the report to considerer the last exam done.
    ''' TAKEBEST instruct the report to considerer the best exam done.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    Public Property SettingRetakeTestPolicy() As String

    Public Property ResId As Integer

    Public Property Classdate As String

    Public Property CohortStartDate As String

    Public Property SqlId As Integer

    Public Property ObjId As Integer

    Public Property OrderBy As String

    Public Property FilterList As String

    Public Property FilterListString As String

    Public Property FilterOther As String

    Public Property FilterOtherString As String

    Public Property ShowFilters As Boolean

    Public Property OrderByString As String

    Public Property ShowSortBy As Boolean

    Public Property ShowRptDescription As Boolean

    Public Property ShowRptInstructions As Boolean

    Public Property ShowRptNotes As Boolean

    Public Property RptTitle As String

    Public Property SchoolLogo As Byte()

    Public Property CampGrpId As String

    Public Property PrgVerId As String

    Public Property StatusCodeId As String

    Public Property PrgVerDescrip As String

    Public Property TermStartDate() As Date

    Public Property TermEndDate As Date

    Public Property StartDate As Date

    Public Property EndDate As Date

    ''' <summary>
    ''' Only some values are appropiated
    ''' </summary>
    ''' <value>NULL, Empty no filter. 
    ''' Operations:BETWEEN, EQUALTO, NOTEQUALTO, LESSTHAN, GREATERTHAN, ISNULL
    '''  </value>
    ''' <returns></returns>
    ''' <remarks>If other value is used none returned in a query.</remarks>
    Public Property ExpectedGraduationOperation() As String

    Public Property FirstName As String

    Public Property LastName As String

    Public Property TermId As String

    Public Property TermDescrip As String

    Public Property CampusId As String

    Public Property CutoffDate As String

    Public Property ShowRptStudentGroup As Boolean
    Public Property FiltersByStudentGroup As Boolean

    Public Property ShowRptDateIssue As Boolean

    Public Property SuppressHeader As Boolean

    Public Property GroupByClass As Boolean

    Public Property HideRptProgramVersion As Boolean

    Public Property ShowRptClassDates As Boolean

    Public Property ShowRptProjExceedBal As Boolean

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' New Property Added By Kamalesh Ahuja on Oct, 08 2010
    Public Property BaseCAOnRegClasses() As Boolean
        Get
            Return m_BaseCAOnRegClasses
        End Get
        Set(ByVal Value As Boolean)
            m_BaseCAOnRegClasses = Value
        End Set
    End Property

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Property TestType As String

    Public Property Speed As String

    Public Property Accuracy As String

    Public Property StudentId() As String
        Get
            Return m_studentid
        End Get
        Set(ByVal Value As String)
            m_studentid = Value
        End Set
    End Property
    ' New Property Added By Vijay Ramteke on Feb 08, 2009
    Public Property ShowRptCosts() As Boolean
        Get
            Return m_ShowCosts
        End Get
        Set(ByVal Value As Boolean)
            m_ShowCosts = Value
        End Set
    End Property

    Public Property ShowRptExpectedFunding() As Boolean
        Get
            Return m_ShowExpectedFunding
        End Get
        Set(ByVal Value As Boolean)
            m_ShowExpectedFunding = Value
        End Set
    End Property

    Public Property ShowRptCategoryBreakdown() As Boolean
        Get
            Return m_ShowCategoryBreakdown
        End Get
        Set(ByVal Value As Boolean)
            m_ShowCategoryBreakdown = Value
        End Set
    End Property

    Public Property ShowTermOrModule() As Boolean
        Get
            Return m_ShowTermOrModule
        End Get
        Set(ByVal Value As Boolean)
            m_ShowTermOrModule = Value
        End Set
    End Property

    ' New Property Added By Vijay Ramteke on Feb 08, 2009
    ' New Property Added By Vijay Ramteke on March 05, 2009
    Public Property ShowRptGroupByEnrollment() As Boolean
        Get
            Return m_ShowGroupByEnrollment
        End Get
        Set(ByVal Value As Boolean)
            m_ShowGroupByEnrollment = Value
        End Set
    End Property

    Public Property ShowRptDisbNotBeenPaid() As Boolean
        Get
            Return m_ShowDisbNotBeenPaid
        End Get
        Set(ByVal Value As Boolean)
            m_ShowDisbNotBeenPaid = Value
        End Set
    End Property

    Public Property ShowCurrentBalance() As Boolean
        Get
            Return m_ShowCurrentBalance
        End Get
        Set(ByVal Value As Boolean)
            m_ShowCurrentBalance = Value
        End Set
    End Property

    Public Property ShowTotalCost() As Boolean
        Get
            Return m_ShowTotalCost
        End Get
        Set(ByVal Value As Boolean)
            m_ShowTotalCost = Value
        End Set
    End Property
    ' New Property Added By Kamalesh Ahuja on May, 31 2010 to resolve mantis issue id 18868
    Public Property ShowLegalDisclaimer() As Boolean
        Get
            Return m_ShowLegalDisclaimer
        End Get
        Set(ByVal Value As Boolean)
            m_ShowLegalDisclaimer = Value
        End Set
    End Property

    Public Property StuEnrollId As String

    '' New Property Add By Vijay Ramteke On July 08, 2010 For Mantis ID 11856
    Public Property NumberOfConsecutiveAbsentDays() As String
        Get
            Return m_NumberConsecutiveAbsentDays
        End Get
        Set(ByVal Value As String)
            m_NumberConsecutiveAbsentDays = Value
        End Set
    End Property

    Public Property NumberOfConsecutiveAbsentDays2() As String
        Get
            Return m_NumberConsecutiveAbsentDays2
        End Get
        Set(ByVal Value As String)
            m_NumberConsecutiveAbsentDays2 = Value
        End Set
    End Property

    Public Property CompOperator() As String
        Get
            Return m_Operator
        End Get
        Set(ByVal Value As String)
            m_Operator = Value
        End Set
    End Property

    Public Property UsedScheduledDays() As Boolean
        Get
            Return m_UsedScheduledDays
        End Get
        Set(ByVal Value As Boolean)
            m_UsedScheduledDays = Value
        End Set
    End Property
    '' New Property Add By Vijay Ramteke On July 08, 2010 For Mantis ID 11856
    '' New Property Add By Vijay Ramteke On September 28, 2010 For Mantis ID 17685
    Public Property ShowTransferCampus() As Boolean
        Get
            Return m_ShowTransferCampus
        End Get
        Set(ByVal Value As Boolean)
            m_ShowTransferCampus = Value
        End Set
    End Property

    Public Property ShowTransferProgram() As Boolean
        Get
            Return m_ShowTransferProgran
        End Get
        Set(ByVal Value As Boolean)
            m_ShowTransferProgran = Value
        End Set
    End Property

    Public Property ShowLDA() As Boolean
        Get
            Return m_ShowLDA
        End Get
        Set(ByVal Value As Boolean)
            m_ShowLDA = Value
        End Set
    End Property
    '' New Property Add By Vijay Ramteke On September 28, 2010 For Mantis ID 17685
    '' New Property Add By Vijay Ramteke On October 27, 2010 For Rally Id US1178
    Public Property ShowUseStuCurrStatus() As Boolean
        Get
            Return m_ShowUseStuCurrStatus
        End Get
        Set(ByVal Value As Boolean)
            m_ShowUseStuCurrStatus = Value
        End Set
    End Property
    '' New Property Add By Vijay Ramteke On October 27, 2010 For Rally Id US1178

    Public Property ShowUseSignLineForAttnd() As Boolean
        Get
            Return m_ShowUseSignLineForAttnd
        End Get
        Set(ByVal Value As Boolean)
            m_ShowUseSignLineForAttnd = Value
        End Set
    End Property
    'DE8390 3/7/2013 Janet Robinson
    Public Property ShowOfficialTranscript() As Boolean
        Get
            Return m_ShowOfficialTranscript
        End Get
        Set(ByVal Value As Boolean)
            m_ShowOfficialTranscript = Value
        End Set
    End Property

    Public Property ShowTermProgressDescription() As Boolean
        Get
            Return m_ShowTermProgressDescription
        End Get
        Set(ByVal Value As Boolean)
            m_ShowTermProgressDescription = Value
        End Set
    End Property

#End Region

    Public Sub New()
    End Sub

#Region "StudentGPAReport"

    Public Property CumGPARangeBegin() As Decimal
        Get
            Return m_CumGPARangeBegin
        End Get
        Set(ByVal value As Decimal)
            m_CumGPARangeBegin = value
        End Set
    End Property

    Public Property CumGPARangeEnd() As Decimal
        Get
            Return m_CumGPARangeEnd
        End Get
        Set(ByVal value As Decimal)
            m_CumGPARangeEnd = value
        End Set
    End Property

    Public Property TermCreditsRangeBegin() As Decimal
        Get
            Return m_TermCreditsRangeBegin
        End Get
        Set(ByVal value As Decimal)
            m_TermCreditsRangeBegin = value
        End Set
    End Property

    Public Property TermCreditsRangeEnd() As Decimal
        Get
            Return m_TermCreditsRangeEnd
        End Get
        Set(ByVal value As Decimal)
            m_TermCreditsRangeEnd = value
        End Set
    End Property

    Public Property TermGPARangeBegin() As Decimal
        Get
            Return m_TermGPARangeBegin
        End Get
        Set(ByVal value As Decimal)
            m_TermGPARangeBegin = value
        End Set
    End Property

    Public Property TermGPARangeEnd() As Decimal
        Get
            Return m_TermGPARangeEnd
        End Get
        Set(ByVal value As Decimal)
            m_TermGPARangeEnd = value
        End Set
    End Property

    Public Property SORT_CumGPA() As Boolean
        Get
            Return m_SORT_CumGPA
        End Get
        Set(ByVal value As Boolean)
            m_SORT_CumGPA = value
        End Set
    End Property

    Public Property SORT_LastName() As Boolean
        Get
            Return m_SORT_LastName
        End Get
        Set(ByVal value As Boolean)
            m_SORT_LastName = value
        End Set
    End Property

    Public Property SORT_TermGPA() As Boolean
        Get
            Return m_SORT_TermGPA
        End Get
        Set(ByVal value As Boolean)
            m_SORT_TermGPA = value
        End Set
    End Property

#End Region
End Class
