' ===============================================================================
' MSGInfo.vb
' Info classes for the MSG project
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Namespace MSG

#Region "Groups"
    Public Class GroupInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _groupId As String
        Private _code As String
        Private _descrip As String
        Private _campGroupId As String
        Private _campGroupDescrip As String
        Private _modUser As String
        Private _modDate As DateTime
        Private _active As Boolean
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _groupId = Guid.NewGuid.ToString
            _active = True
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property GroupId() As String
            Get
                Return _groupId
            End Get
            Set(ByVal Value As String)
                _groupId = Value
            End Set
        End Property
        Public Property Code() As String
            Get
                Return _code
            End Get
            Set(ByVal Value As String)
                _code = Value
            End Set
        End Property
        Public Property Descrip() As String
            Get
                Return _descrip
            End Get
            Set(ByVal Value As String)
                _descrip = Value
            End Set
        End Property
        Public Property CampGroupId() As String
            Get
                Return _campGroupId
            End Get
            Set(ByVal Value As String)
                _campGroupId = Value
            End Set
        End Property
        Public Property CampGroupDescrip() As String
            Get
                Return _campGroupDescrip
            End Get
            Set(ByVal Value As String)
                _campGroupDescrip = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return _active
            End Get
            Set(ByVal Value As Boolean)
                _active = Value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "Messages"
    <Serializable()> Public Class MessageInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _messageId As String
        Private _templateId As String
        Private _templateDescrip As String
        Private _templateData As String
        Private _fromId As String
        Private _mailFrom As String
        Private _mailTo As String
        Private _groupId As String
        Private _groupDescrip As String
        Private _deliveryType As String
        Private _recipientType As String
        Private _recipientId As String
        Private _recipientName As String
        Private _reType As String
        Private _reId As String
        Private _reName As String
        Private _changeId As String
        Private _msgContent As String
        Private _createdDate As DateTime
        Private _deliveryDate As DateTime
        Private _lastDeliveryAttempt As DateTime
        Private _lastDeliveryMsg As String
        Private _dateDelivered As DateTime
        Private _modDate As DateTime
        Private _modUser As String
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _messageId = Guid.NewGuid.ToString
            _deliveryDate = Date.MinValue
            _modDate = Date.MinValue
            _createdDate = Date.Now
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property MessageId() As String
            Get
                Return _messageId
            End Get
            Set(ByVal Value As String)
                _messageId = Value
            End Set
        End Property
        Public Property TemplateId() As String
            Get
                Return _templateId
            End Get
            Set(ByVal Value As String)
                _templateId = Value
            End Set
        End Property
        Public Property FromId() As String
            Get
                Return _fromId
            End Get
            Set(ByVal Value As String)
                _fromId = Value
            End Set
        End Property
        Public Property MailFrom() As String
            Get
                Return _mailFrom
            End Get
            Set(ByVal Value As String)
                _mailFrom = Value
            End Set
        End Property
        Public Property MailTo() As String
            Get
                Return _mailTo
            End Get
            Set(ByVal Value As String)
                _mailTo = Value
            End Set
        End Property
        Public Property TemplateDescrip() As String
            Get
                Return _templateDescrip
            End Get
            Set(ByVal Value As String)
                _templateDescrip = Value
            End Set
        End Property
        Public Property TemplateData() As String
            Get
                Return _templateData
            End Get
            Set(ByVal Value As String)
                _templateData = Value
            End Set
        End Property
        Public Property GroupId() As String
            Get
                Return _groupId
            End Get
            Set(ByVal Value As String)
                _groupId = Value
            End Set
        End Property
        Public Property GroupDescrip() As String
            Get
                Return _groupDescrip
            End Get
            Set(ByVal Value As String)
                _groupDescrip = Value
            End Set
        End Property
        Public Property DeliveryType() As String
            Get
                Return _deliveryType
            End Get
            Set(ByVal Value As String)
                _deliveryType = Value
            End Set
        End Property
        Public Property RecipientId() As String
            Get
                Return _recipientId
            End Get
            Set(ByVal Value As String)
                _recipientId = Value
            End Set
        End Property
        Public Property RecipientType() As String
            Get
                Return _recipientType
            End Get
            Set(ByVal Value As String)
                _recipientType = Value
            End Set
        End Property
        Public Property RecipientName() As String
            Get
                Return _recipientName
            End Get
            Set(ByVal Value As String)
                _recipientName = Value
            End Set
        End Property
        Public Property ReId() As String
            Get
                Return _reId
            End Get
            Set(ByVal Value As String)
                _reId = Value
            End Set
        End Property
        Public Property ReType() As String
            Get
                Return _reType
            End Get
            Set(ByVal Value As String)
                _reType = Value
            End Set
        End Property
        Public Property ReName() As String
            Get
                Return _reName
            End Get
            Set(ByVal Value As String)
                _reName = Value
            End Set
        End Property
        Public Property MsgContent() As String
            Get
                Return _msgContent
            End Get
            Set(ByVal Value As String)
                _msgContent = Value
            End Set
        End Property
        Public Property CreatedDate() As DateTime
            Get
                Return _createdDate
            End Get
            Set(ByVal Value As DateTime)
                _createdDate = Value
            End Set
        End Property
        Public Property DeliveryDate() As DateTime
            Get
                Return _deliveryDate
            End Get
            Set(ByVal Value As DateTime)
                _deliveryDate = Value
            End Set
        End Property
        Public Property LastDeliveryAttempt() As DateTime
            Get
                Return _lastDeliveryAttempt
            End Get
            Set(ByVal Value As DateTime)
                _lastDeliveryAttempt = Value
            End Set
        End Property
        Public Property LastDeliveryMsg() As String
            Get
                Return _lastDeliveryMsg
            End Get
            Set(ByVal Value As String)
                _lastDeliveryMsg = Value
            End Set
        End Property
        Public Property DateDelivered() As DateTime
            Get
                Return _dateDelivered
            End Get
            Set(ByVal Value As DateTime)
                _dateDelivered = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return _modDate
            End Get
            Set(ByVal Value As DateTime)
                _modDate = Value
            End Set
        End Property
        Public Property ChangeId() As String
            Get
                Return Me._changeId
            End Get
            Set(ByVal Value As String)
                Me._changeId = Value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "Rules"
    <Serializable()> Public Class RuleInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _ruleId As String
        Private _type As Integer
        Private _code As String
        Private _descrip As String
        Private _campGroupId As String
        Private _ruleSql As String
        Private _templateId As String
        Private _recipientType As String
        Private _reType As String
        Private _deliveryType As String
        Private _lastRun As DateTime
        Private _lastRunError As String
        Private _modDate As DateTime
        Private _modUser As String
        Private _active As Boolean
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _ruleId = Guid.NewGuid.ToString
            _type = 0
            _modUser = ""
            _lastRun = Date.MinValue
            _modDate = Date.MinValue
            _active = False
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property RuleId() As String
            Get
                Return _ruleId
            End Get
            Set(ByVal Value As String)
                _ruleId = Value
            End Set
        End Property
        Public Property Type() As Integer
            Get
                Return _type
            End Get
            Set(ByVal Value As Integer)
                _type = Value
            End Set
        End Property
        Public Property Code() As String
            Get
                Return _code
            End Get
            Set(ByVal value As String)
                _code = value
            End Set
        End Property
        Public Property Descrip() As String
            Get
                Return _descrip
            End Get
            Set(ByVal Value As String)
                _descrip = Value
            End Set
        End Property
        Public Property CampGroupId() As String
            Get
                Return _campGroupId
            End Get
            Set(ByVal Value As String)
                _campGroupId = Value
            End Set
        End Property
        Public Property RuleSql() As String
            Get
                Return _ruleSql
            End Get
            Set(ByVal Value As String)
                _ruleSql = Value
            End Set
        End Property
        Public Property TemplateId() As String
            Get
                Return _templateId
            End Get
            Set(ByVal Value As String)
                _templateId = Value
            End Set
        End Property
        Public Property RecipientType() As String
            Get
                Return _recipientType
            End Get
            Set(ByVal Value As String)
                _recipientType = Value
            End Set
        End Property
        Public Property ReType() As String
            Get
                Return _reType
            End Get
            Set(ByVal Value As String)
                _reType = Value
            End Set
        End Property
        Public Property DeliveryType() As String
            Get
                Return _deliveryType
            End Get
            Set(ByVal Value As String)
                _deliveryType = Value
            End Set
        End Property
        Public Property LastRun() As DateTime
            Get
                Return _lastRun
            End Get
            Set(ByVal Value As DateTime)
                _lastRun = Value
            End Set
        End Property
        Public Property LastRunError() As String
            Get
                Return _lastRunError
            End Get
            Set(ByVal Value As String)
                _lastRunError = Value
            End Set
        End Property

        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return _modDate
            End Get
            Set(ByVal Value As DateTime)
                _modDate = Value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return _active
            End Get
            Set(ByVal Value As Boolean)
                _active = Value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "Templates"
    Public Class TemplateInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _templateId As String
        Private _groupId As String
        Private _groupDescrip As String
        Private _moduleEntityId As String
        Private _moduleId As String 'guid, link to syModules
        Private _moduleName As String
        Private _entityId As String 'guid, link to syModuleEntity
        Private _entityName As String
        Private _campGroupId As String
        Private _campGroupDescrip As String
        Private _code As String
        Private _descrip As String
        Private _data As String
        Private _modUser As String
        Private _modDate As DateTime
        Private _active As Boolean
        Private _campusid As String
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _templateId = Guid.NewGuid.ToString
            _active = True
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property TemplateId() As String
            Get
                Return _templateId
            End Get
            Set(ByVal Value As String)
                _templateId = Value
            End Set
        End Property
        Public Property GroupId() As String
            Get
                Return _groupId
            End Get
            Set(ByVal Value As String)
                _groupId = Value
            End Set
        End Property
        Public Property GroupDescrip() As String
            Get
                Return _groupDescrip
            End Get
            Set(ByVal Value As String)
                _groupDescrip = Value
            End Set
        End Property
        Public Property Code() As String
            Get
                Return _code
            End Get
            Set(ByVal Value As String)
                _code = Value
            End Set
        End Property
        Public Property Descrip() As String
            Get
                Return _descrip
            End Get
            Set(ByVal Value As String)
                _descrip = Value
            End Set
        End Property
        Public Property ModuleEntityId() As String
            Get
                Return Me._moduleEntityId
            End Get
            Set(ByVal value As String)
                Me._moduleEntityId = value
            End Set
        End Property
        Public Property ModuleId() As String
            Get
                Return Me._moduleId
            End Get
            Set(ByVal value As String)
                Me._moduleId = value
            End Set
        End Property
        Public Property ModuleName() As String
            Get
                Return Me._moduleName
            End Get
            Set(ByVal value As String)
                Me._moduleName = value
            End Set
        End Property
        Public Property EntityId() As String
            Get
                Return Me._entityId
            End Get
            Set(ByVal value As String)
                Me._entityId = value
            End Set
        End Property
        Public Property EntityName() As String
            Get
                Return Me._entityName
            End Get
            Set(ByVal value As String)
                Me._entityName = value
            End Set
        End Property
        Public Property CampGroupId() As String
            Get
                Return _campGroupId
            End Get
            Set(ByVal Value As String)
                _campGroupId = Value
            End Set
        End Property
        Public Property CampGroupDescrip() As String
            Get
                Return _campGroupDescrip
            End Get
            Set(ByVal Value As String)
                _campGroupDescrip = Value
            End Set
        End Property
        Public Property CampusId() As String
            Get
                Return _campusid
            End Get
            Set(ByVal Value As String)
                _campusid = Value
            End Set
        End Property
        Public Property Data() As String
            Get
                Return _data
            End Get
            Set(ByVal Value As String)
                _data = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return _active
            End Get
            Set(ByVal Value As Boolean)
                _active = Value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "Speicla Fields / Entity to FieldGroups"
    <Serializable()> Public Class EntityFieldGroupInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _entityFieldGroupId As String
        Private _entityId As String
        Private _fieldGroupName As String
        Private _modDate As DateTime
        Private _modUser As String
        Private _active As Boolean
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _entityFieldGroupId = Guid.NewGuid.ToString
            _modDate = Date.MinValue
            _active = False
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property EntityFieldGroupId() As String
            Get
                Return _entityFieldGroupId
            End Get
            Set(ByVal Value As String)
                _entityFieldGroupId = Value
            End Set
        End Property
        Public Property EntityId() As String
            Get
                Return _entityId
            End Get
            Set(ByVal Value As String)
                _entityId = Value
            End Set
        End Property
        Public Property FieldGroupName() As String
            Get
                Return _fieldGroupName
            End Get
            Set(ByVal Value As String)
                _fieldGroupName = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return _modDate
            End Get
            Set(ByVal Value As DateTime)
                _modDate = Value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return _active
            End Get
            Set(ByVal Value As Boolean)
                _active = Value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "Input Masks"
    <Serializable()> Public Class InputMaskInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _InputMaskId As String
        Private _Item As String
        Private _Mask As String
        Private _modUser As String
        Private _modDate As DateTime
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _InputMaskId = Guid.NewGuid.ToString
            _Item = ""
            _Mask = ""
            _modUser = ""
            _modDate = Date.MinValue
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property InputMaskId() As String
            Get
                Return _InputMaskId
            End Get
            Set(ByVal Value As String)
                _InputMaskId = Value
            End Set
        End Property
        Public Property Item() As String
            Get
                Return _Item
            End Get
            Set(ByVal Value As String)
                _Item = Value
            End Set
        End Property
        Public Property Mask() As String
            Get
                Return _Mask
            End Get
            Set(ByVal Value As String)
                _Mask = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As Date
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
#End Region
    End Class
#End Region

End Namespace
