Public Class AdRequirementInfo
    '
    '   Admission Requirement Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _reqId As String
    Private _code As String
    Private _descrip As String
    Private _statusId As String
    Private _campGrpId As String
    Private _moduleId As Integer
    Private _reqTypeId As Integer
    Private _minScore As Integer
    Private _maxScore As Integer
    Private _appliesToAll As Boolean
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _ReqId = Guid.NewGuid.ToString
        _Code = ""
        _Descrip = ""
        _StatusId = FAME.AdvantageV1.Common.AdvantageCommonValues.ActiveGuid
        _CampGrpId = Guid.Empty.ToString
        _ModuleId = 0
        _ReqTypeId = 0
        _MinScore = 0
        _maxScore = 0
        _appliesToAll = False
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property ReqId() As String
        Get
            Return _reqId
        End Get
        Set(ByVal Value As String)
            _reqId = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property Descrip() As String
        Get
            Return _descrip
        End Get
        Set(ByVal Value As String)
            _descrip = Value
        End Set
    End Property
    Public Property ModuleId() As Integer
        Get
            Return _moduleId
        End Get
        Set(ByVal Value As Integer)
            _moduleId = Value
        End Set
    End Property
    Public Property ReqTypeId() As Integer
        Get
            Return _reqTypeId
        End Get
        Set(ByVal Value As Integer)
            _reqTypeId = Value
        End Set
    End Property
    Public Property MinScore() As Integer
        Get
            Return _minScore
        End Get
        Set(ByVal Value As Integer)
            _minScore = Value
        End Set
    End Property
    Public Property MaxScore() As Integer
        Get
            Return _maxScore
        End Get
        Set(ByVal Value As Integer)
            _maxScore = Value
        End Set
    End Property
    Public Property AppliesToAll() As Boolean
        Get
            Return _appliesToAll
        End Get
        Set(ByVal Value As Boolean)
            _appliesToAll = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Class AdReqLeadGrpInfo
    '
    '   Admission Requirement Lead Group Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _ReqLeadGrpId As String
    Private _ReqId As String
    Private _LeadGrpId As String
    Private _IsRequired As Boolean
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _ReqLeadGrpId = Guid.NewGuid.ToString
        _ReqId = Guid.Empty.ToString
        _LeadGrpId = Guid.Empty.ToString
        _IsRequired = False
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property ReqLeadGrpId() As String
        Get
            Return _ReqLeadGrpId
        End Get
        Set(ByVal Value As String)
            _ReqLeadGrpId = Value
        End Set
    End Property
    Public Property ReqId() As String
        Get
            Return _ReqId
        End Get
        Set(ByVal Value As String)
            _ReqId = Value
        End Set
    End Property
    Public Property LeadGrpId() As String
        Get
            Return _LeadGrpId
        End Get
        Set(ByVal Value As String)
            _LeadGrpId = Value
        End Set
    End Property
    Public Property IsRequired() As Boolean
        Get
            Return _IsRequired
        End Get
        Set(ByVal Value As Boolean)
            _IsRequired = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
