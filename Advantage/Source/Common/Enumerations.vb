﻿Public Class Enumerations
    Public Enum SystemStatuses
        NewLead = 1
        ApplicationReceived = 2
        ApplicationNotAccepted = 3
        InterviewScheduled = 4
        Interviewed = 5
        Enrolled = 6
        FutureStart = 7
        NoStart = 8
        CurrentlyAttending = 9
        LeaveOfAbsence = 10
        Suspension = 11
        Dropped = 12
        Graduated = 14
        Active = 15
        InActive = 16
        DeadLead = 17
        WillEnrollIntheFuture = 18
        TransferOut = 19
        AcademicProbationSap = 20
        Externship = 22
        DisciplinaryProbation = 23
        WarningProbation = 24
    End Enum

    Public Enum ResponseTypes
        Success = 1
        Warning = 2
        ExceptionHappened = 3
    End Enum


    Public Enum StatusChangeValidationMode
        Insert = 1
        Edit = 2
        Delete = 3
    End Enum

    Public enum AcademicCalendars
        NonStandard = 1
        Quarter = 2
        Semester = 3
        Trimester = 4
        ClockHour = 5
        NonTerm = 6
    End enum
End Class
