Public Class StudentGroupStudentInfo

    Private _SSGroupsRelId As Guid
    Private _SGroupId As Guid
    Private _StudentId As Guid
    Private _StudentFullName As String
    Private _StudentProgramId As Guid
    Private _StudentProgramDescrip As String
    Private _StudentStatusId As Guid
    Private _StudentStatusDescrip As String
    Private _StudentCampusId As Guid
    Private _StudentCampusDescrip As String
    Private _DateAdded As DateTime
    Private _AddedBy As Guid
    Private _AddedByName As String
    Private _DateRemoved As DateTime
    Private _RemovedBy As Guid
    Private _RemovedByName As String
    Public Property SSGroupsRelId() As Guid
        Get
            SSGroupsRelId = _SSGroupsRelId
        End Get
        Set(ByVal Value As Guid)
            _SSGroupsRelId = Value
        End Set
    End Property
    Public Property SGroupId() As Guid
        Get
            SGroupId = _SGroupId
        End Get
        Set(ByVal Value As Guid)
            _SGroupId = Value
        End Set
    End Property

    Public Property StudentId() As Guid
        Get
            StudentId = _StudentId
        End Get
        Set(ByVal Value As Guid)
            _StudentId = Value
        End Set
    End Property
    Public Property StudentFullName() As String
        Get
            StudentFullName = _StudentFullName
        End Get
        Set(ByVal Value As String)
            _StudentFullName = Value
        End Set
    End Property
    Public Property StudentProgramId() As Guid
        Get
            StudentProgramId = _StudentProgramId
        End Get
        Set(ByVal Value As Guid)
            _StudentProgramId = Value
        End Set
    End Property
    Public Property StudentProgramDescrip() As String
        Get
            StudentProgramDescrip = _StudentProgramDescrip
        End Get
        Set(ByVal Value As String)
            _StudentProgramDescrip = Value
        End Set
    End Property
    Public Property StudentStatusId() As Guid
        Get
            StudentStatusId = _StudentStatusId
        End Get
        Set(ByVal Value As Guid)
            _StudentStatusId = Value
        End Set
    End Property
    Public Property StudentStatusDescrip() As String
        Get
            StudentStatusDescrip = _StudentStatusDescrip
        End Get
        Set(ByVal Value As String)
            _StudentStatusDescrip = Value
        End Set
    End Property
    Public Property StudentCampusId() As Guid
        Get
            StudentCampusId = _StudentCampusId
        End Get
        Set(ByVal Value As Guid)
            _StudentCampusId = Value
        End Set
    End Property
    Public Property StudentCampusDescrip() As String
        Get
            StudentCampusDescrip = _StudentCampusDescrip
        End Get
        Set(ByVal Value As String)
            _StudentCampusDescrip = Value
        End Set
    End Property
    Public Property DateAdded() As DateTime
        Get
            DateAdded = _DateAdded
        End Get
        Set(ByVal Value As DateTime)
            _DateAdded = Value
        End Set
    End Property
    Public Property AddedBy() As Guid
        Get
            AddedBy = _AddedBy
        End Get
        Set(ByVal Value As Guid)
            _AddedBy = Value
        End Set
    End Property
    Public Property AddedByName() As String
        Get
            AddedByName = _AddedByName
        End Get
        Set(ByVal Value As String)
            _AddedByName = Value
        End Set
    End Property
    Public Property DateRemoved() As DateTime
        Get
            DateRemoved = _DateRemoved
        End Get
        Set(ByVal Value As DateTime)
            _DateRemoved = Value
        End Set
    End Property
    Public Property RemovedBy() As Guid
        Get
            RemovedBy = _RemovedBy
        End Get
        Set(ByVal Value As Guid)
            _RemovedBy = Value
        End Set
    End Property
    Public Property RemovedByName() As String
        Get
            RemovedByName = _RemovedByName
        End Get
        Set(ByVal Value As String)
            _RemovedByName = Value
        End Set
    End Property

    Public Sub New()
        MyBase.new()
    End Sub

    Public Sub New(ByVal NewSSGroupsRelId As Guid, ByVal NewSGroupId As Guid, ByVal NewStudentId As Guid, ByVal NewStudentFullName As String, ByVal NewStudentProgramId As Guid, _
    ByVal NewStudentProgramDescrip As String, ByVal NewStudentStatusId As Guid, ByVal NewStudentStatusDescrip As String, _
    ByVal NewStudentCampusId As Guid, ByVal NewStudentCampusDescrip As String, ByVal NewDateAdded As DateTime, _
    ByVal NewAddedBy As Guid, ByVal NewDateRemoved As DateTime, ByVal NewRemovedBy As Guid, ByVal NewAddedByName As String, ByVal NewRemovedByName As String)

        _SSGroupsRelId = NewSSGroupsRelId
        _SGroupId = NewSGroupId
        _StudentId = NewStudentId
        _StudentFullName = NewStudentFullName
        _StudentProgramId = NewStudentProgramId
        _StudentProgramDescrip = NewStudentProgramDescrip
        _StudentStatusId = NewStudentStatusId
        _StudentStatusDescrip = NewStudentStatusDescrip
        _StudentCampusId = NewStudentCampusId
        _StudentCampusDescrip = NewStudentCampusDescrip
        _DateAdded = NewDateAdded
        _AddedBy = NewAddedBy
        _AddedByName = NewAddedByName
        _DateRemoved = NewDateRemoved
        _RemovedBy = NewRemovedBy
        _RemovedByName = NewRemovedByName

    End Sub

End Class
