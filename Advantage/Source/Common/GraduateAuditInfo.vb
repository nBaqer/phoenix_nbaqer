﻿Imports System.Web.UI.WebControls

Public Class GraduateAuditInfo

    Public Property StuEnrollmentId() As String
    Public Property ItemCollection() As ListItemCollection
    Public Property GradeReps() As String
    Public Property IncludeHours() As Boolean
    Public Property StrGradesFormat() As String
    Public Property TranscripType() As String
    Public Property Addcreditsbyservice() As String
    Public Property Dsgradesysdetailids As List(Of String)
End Class
