Option Strict On
Public Class ActivityStatusInfo

#Region "Activity Status Info Packages"

#Region "Private Variable Declaration"

    Private _ActivityStatusId As Int32
    Private _ActivityStatusCode As String
    Private _StatusId As Guid
    Private _ActivityStatusDescrip As String
    Private _CampGrpId As Guid

#End Region

#Region "Activity Assignment Properties"

    Public Property ActivityStatusId() As Int32
        Get
            ActivityStatusId = _ActivityStatusId
        End Get
        Set(ByVal Value As Int32)
            _ActivityStatusId = Value
        End Set
    End Property
    Public Property ActivityStatusCode() As String
        Get
            ActivityStatusCode = _ActivityStatusCode
        End Get
        Set(ByVal Value As String)
            _ActivityStatusCode = Value
        End Set
    End Property
    Public Property StatusId() As Guid
        Get
            StatusId = _StatusId
        End Get
        Set(ByVal Value As Guid)
            _StatusId = Value
        End Set
    End Property
    Public Property ActivityStatusDescrip() As String
        Get
            ActivityStatusDescrip = _ActivityStatusDescrip
        End Get
        Set(ByVal Value As String)
            _ActivityStatusDescrip = Value
        End Set
    End Property
    Public Property CampGrpId() As Guid
        Get
            CampGrpId = _CampGrpId
        End Get
        Set(ByVal Value As Guid)
            _CampGrpId = Value
        End Set
    End Property
  
#End Region

#Region "New Instance of the Class Declaration"
    Public Sub New(ByVal NewActivityStatusId As Int32, ByVal NewActivityStatusCode As String, ByVal NewStatusId As Guid, ByVal NewActivityStatusDescrip As String, ByVal NewCampGrpId As Guid)

        _ActivityStatusId = NewActivityStatusId
        _ActivityStatusCode = NewActivityStatusCode
        _StatusId = NewStatusId
        _ActivityStatusDescrip = NewActivityStatusDescrip
        _CampGrpId = NewCampGrpId

    End Sub

    Public Sub New()
        'MyBase.new()
    End Sub

#End Region
#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
