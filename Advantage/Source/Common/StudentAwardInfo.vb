Public Class StudentAwardInfo
    Private _StudentAwardId As Guid
    Private _StudentId As Guid
    Private _AwardType As Guid
    Private _AwardYear As Guid
    Private _GrossAmount As Double
    Private _LoanFees As Double
    Private _NetLoanAmount As Double
    Private _Lender As Guid
    Private _Servicer As Guid
    Private _Guarantor As Guid
    Private _AmountReceived As Double
    Private _AmountRefunded As Double
    Private _TotalRemaining As Double

    Public Property StudentAwardId() As Guid
        Get
            StudentAwardId = _StudentAwardId
        End Get
        Set(ByVal Value As Guid)
            _StudentAwardId = Value
        End Set
    End Property
    Public Property StudentId() As Guid
        Get
            StudentId = _StudentId
        End Get
        Set(ByVal Value As Guid)
            _StudentId = Value
        End Set
    End Property
    Public Property AwardType() As Guid
        Get
            AwardType = _AwardType
        End Get
        Set(ByVal Value As Guid)
            _AwardType = Value
        End Set
    End Property
    Public Property AwardYear() As Guid
        Get
            AwardYear = _AwardYear
        End Get
        Set(ByVal Value As Guid)
            _AwardYear = Value
        End Set
    End Property
    Public Property GrossAmount() As Double
        Get
            GrossAmount = _GrossAmount
        End Get
        Set(ByVal Value As Double)
            _GrossAmount = Value
        End Set
    End Property
    Public Property LoanFees() As Double
        Get
            LoanFees = _LoanFees
        End Get
        Set(ByVal Value As Double)
            _LoanFees = Value
        End Set
    End Property
    Public Property NetLoanAmount() As Double
        Get
            NetLoanAmount = _NetLoanAmount
        End Get
        Set(ByVal Value As Double)
            _NetLoanAmount = Value
        End Set
    End Property
    Public Property Lender() As Guid
        Get
            Lender = _Lender
        End Get
        Set(ByVal Value As Guid)
            _Lender = Value
        End Set
    End Property
    Public Property Servicer() As Guid
        Get
            Servicer = _Servicer
        End Get
        Set(ByVal Value As Guid)
            _Servicer = Value
        End Set
    End Property
    Public Property Guarantor() As Guid
        Get
            Guarantor = _Guarantor
        End Get
        Set(ByVal Value As Guid)
            _Guarantor = Value
        End Set
    End Property
    Public Property AmountReceived() As Double
        Get
            AmountReceived = _AmountReceived
        End Get
        Set(ByVal Value As Double)
            _AmountReceived = Value
        End Set
    End Property
    Public Property AmountRefunded() As Double
        Get
            AmountRefunded = _AmountRefunded
        End Get
        Set(ByVal Value As Double)
            _AmountRefunded = Value
        End Set
    End Property
    Public Property TotalRemaining() As Double
        Get
            TotalRemaining = _TotalRemaining
        End Get
        Set(ByVal Value As Double)
            _TotalRemaining = Value
        End Set
    End Property
    Public Sub New()
        MyBase.new()
    End Sub
    Public Sub New(ByVal NewStudentAwardId As Guid, ByVal NewStudentId As Guid, ByVal NewAwardType As Guid, _
    ByVal NewAwardYear As Guid, ByVal NewGrossAmount As Double, ByVal NewLoanFees As Double, _
    ByVal NewNetLoanAmount As Double, ByVal NewLender As Guid, ByVal NewServicer As Guid, ByVal NewGuarantor As Guid, _
    ByVal NewAmountReceived As Double, ByVal NewAmountRefunded As Double, ByVal NewTotalRemaining As Double)

        _StudentAwardId = NewStudentAwardId
        _StudentId = NewStudentId
        _AwardType = NewAwardType
        _AwardYear = NewAwardYear
        _GrossAmount = NewGrossAmount
        _LoanFees = NewLoanFees
        _NetLoanAmount = NewNetLoanAmount
        _Lender = NewLender
        _Servicer = NewServicer
        _Guarantor = NewGuarantor
        _AmountReceived = NewAmountReceived
        _AmountRefunded = NewAmountRefunded
        _TotalRemaining = NewTotalRemaining
    End Sub
End Class
