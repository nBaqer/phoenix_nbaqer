' ===============================================================================
' FAME.AdvantageV1.Common
'
' FLMessageInfo.vb
'
' Classes for Importing FameLink Messages.  All messages are placed in a collection of messages until processed.
' >>FameLink Message Information class
' >>FameLink Message Information class collection
' ===============================================================================
' Copyright (C) 2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Public Class FLMessageInfo
    'message class for FameLink messages - all formats are supported with this single class
#Region " Variables and Objects"
    'generic variables for all types
    Private _strFileId As String 'guid (FLFileProcessedId) for the file that the record/exception belongs to
    Private _strYYYY As String  'year in yyyy format
    Private _strAwdyr As String  'award year 
    Private _blnIsInDB As Boolean  'set to true when data message has been processed and stored in the database
    Private _blnIsParsed As Boolean  'set to true when message has been retrieved from a given file and successfully parsed
    Private _dateModDate As DateTime
    Private _strModUser As String
    Private _strMsgType As String   'type of message - currently limited to FAID/HEAD/CHNG/REFU/DISB/
    Private _strFund As String   'fund source type
    Private _strFAID As String   'faid number - guid
    Private _strSSN As String    'social security number
    Private _strError As String  'error messgae string associated with exception or other controlled error
    Private _strFileNameSource As String  'original source filename for given message 
    Private _strMessage As String
    Private _blnShow As Boolean
    Private _RecordPosition As Integer  'This is the row position of the record in the FAMELink file. This starts at 1.

    'specific variables to be shared among differing message types
    Private _dateDate1 As DateTime   'date variable - dependent upon message type
    Private _dateDate2 As DateTime   'date variable - dependent upon message type
    Private _decAmount1 As Decimal   'amount variable - dependent upon message type
    Private _decAmount2 As Decimal   'amount variable - dependent upon message type
    Private _strChgCode As String    'change code associated with CHNG message type
    Private _strCheckNo As String    'check number associated with DISB message type

#End Region
#Region " Public Constructor"
    Public Sub New(ByVal FileId As String, ByVal strMsgType As String, ByVal strRecord As String, Optional ByVal strFileName As String = "", Optional ByVal strMessage As String = "", Optional ByVal blnShow As Boolean = True, Optional ByVal intRecordPosition As Integer = 0)
        _blnIsInDB = False
        _dateModDate = Date.Now
        _strModUser = "famelink"
        _strMsgType = strMsgType
        _strFund = ""
        _strFAID = ""
        _dateDate1 = Date.MinValue
        _dateDate2 = Date.MinValue
        _strAwdyr = ""
        _strYYYY = ""
        _strSSN = ""
        _decAmount1 = 0.0
        _decAmount2 = 0.0
        _strChgCode = ""
        _strCheckNo = ""
        _strError = ""
        _strFileId = FileId
        _strFileNameSource = strFileName
        _strMessage = strMessage
        _blnShow = blnShow
        _RecordPosition = intRecordPosition
        _blnIsParsed = ParseMessage(strMsgType, strRecord)
    End Sub
#End Region
#Region "Public Properties"
    Public Property blnIsInDB() As Boolean
        Get
            Return _blnIsInDB
        End Get
        Set(ByVal Value As Boolean)
            _blnIsInDB = Value
        End Set
    End Property
    Public Property strYYYY() As String
        Get
            Return _strYYYY
        End Get
        Set(ByVal Value As String)
            Dim strMM As String, strYY As String
            Try
                If String.IsNullOrEmpty(Value) Then Throw New ArgumentNullException(Value, "Null parameter value provided for yyyy.")
                If Len(Value) <> 7 Then Throw New ArgumentOutOfRangeException("Value", Value, "yyyy field must have at least 7 chars")
                strMM = Left(Value, 2)
                strYY = Right(Value, 2)
                If strYY = "00" Then _strYYYY = "20" & strYY Else _strYYYY = strMM & strYY
            Catch e As ArgumentNullException
                _strError = e.Message
            Catch e As ArgumentOutOfRangeException
                _strError = e.Message
            Catch e As System.exception
                _strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strAwdyr() As String
        Get
            Return _strAwdyr
        End Get
        Set(ByVal Value As String)
            _strAwdyr = Value
        End Set
    End Property
    Public Property blnIsParsed() As Boolean
        Get
            Return _blnIsParsed
        End Get
        Set(ByVal Value As Boolean)
            _blnIsParsed = Value
        End Set
    End Property
    Public ReadOnly Property dateModDate() As DateTime
        Get
            Return _dateModDate
        End Get
    End Property
    Public ReadOnly Property strModUser() As String
        Get
            Return _strModUser
        End Get
    End Property
    Public ReadOnly Property strMsgType() As String
        Get
            Return _strMsgType
        End Get
    End Property
    Public ReadOnly Property strSSN() As String
        Get
            Return _strSSN
        End Get
    End Property
    Public ReadOnly Property strFund() As String
        Get
            Return _strFund
        End Get
    End Property
    Public ReadOnly Property strFAID() As String
        Get
            Return _strFAID
        End Get
    End Property
    Public ReadOnly Property dateDate1() As DateTime
        Get
            Return _dateDate1
        End Get
    End Property
    Public ReadOnly Property dateDate2() As DateTime
        Get
            Return _dateDate2
        End Get
    End Property
    Public ReadOnly Property decAmount1() As Decimal
        Get
            Return _decAmount1
        End Get
    End Property
    Public ReadOnly Property decAmount2() As Decimal
        Get
            Return _decAmount2
        End Get
    End Property
    Public ReadOnly Property strChgCode() As String
        Get
            Return _strChgCode
        End Get
    End Property
    Public ReadOnly Property strCheckNo() As String
        Get
            Return _strCheckNo
        End Get
    End Property
    Public ReadOnly Property strFileNameSource() As String
        Get
            Return _strFileNameSource
        End Get
    End Property
    Public Property strError() As String
        Get
            Return _strError
        End Get
        Set(ByVal Value As String)
            _strError = Value
        End Set
    End Property
    Public ReadOnly Property FileId() As String
        Get
            Return _strFileId
        End Get
    End Property
    Public Property Message() As String
        Get
            Return _strMessage
        End Get
        Set(ByVal value As String)
            _strMessage = value
        End Set
    End Property
    Public Property Show() As Boolean
        Get
            Return _blnShow
        End Get
        Set(ByVal value As Boolean)
            _blnShow = value
        End Set
    End Property
    Public Property RecordPosition() As Integer
        Get
            Return _RecordPosition
        End Get
        Set(ByVal value As Integer)
            _RecordPosition = value
        End Set
    End Property

   

#End Region
#Region " Methods"

    Private Function ParseRecordDate(ByRef RecordDate As Date, ByVal intStart As Integer, ByVal strRecord As String) As Boolean
        Dim strTmp As String
        Dim strDate As String
        Dim dateValue As Date
        Dim inty, intm, intd As Integer

        ParseRecordDate = False
        If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record provided")
        If intStart <= 0 Then Throw New ArgumentOutOfRangeException("intStart", intStart, "Start index must be > 0")

        Try
            _strError = ""
            RecordDate = Date.MinValue
            strTmp = Trim(Mid(strRecord, intStart + 4, 2))
            If Integer.TryParse(strTmp, intm) Then
                strDate = strTmp & "/"
                strTmp = Trim(Mid(strRecord, intStart + 6, 2))
                If Integer.TryParse(strTmp, intd) Then
                    strDate += strTmp & "/"
                    strTmp = Trim(Mid(strRecord, intStart, 4))
                    If Integer.TryParse(strTmp, inty) Then
                        strDate += strTmp
                        If DateTime.TryParse(strDate, dateValue) Then
                            dateValue = DateSerial(inty, intm, intd)
                            RecordDate = dateValue
                            ParseRecordDate = True
                        Else
                            Throw New ArgumentOutOfRangeException("strDate", strDate, "invalid date derived")
                        End If
                    Else
                        Throw New ArgumentOutOfRangeException("strTmp", strTmp, "invalid date derived - year")
                    End If
                Else
                    Throw New ArgumentOutOfRangeException("strTmp", strTmp, "invalid date derived - day")
                End If
            Else
                Throw New ArgumentOutOfRangeException("strTmp", strTmp, "invalid date derived - month")
            End If

        Catch e As ArgumentNullException
            _strError = e.Message
        Catch e As ArgumentOutOfRangeException
            _strError = e.Message
        Catch e As System.exception
            _strError = e.Message
            Throw
        End Try
    End Function
    Public Function ParseMessage(ByVal strMsgType As String, ByVal strRecord As String) As Boolean
        Dim tmpStr As String
        Dim intMsgSize As Integer

        ParseMessage = False
        If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided")
        If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strMsgType", "Empty message type parameter provided")
        _strError = ""
        Try
            intMsgSize = GetMessageSize(_strMsgType)
            If _strMsgType = "CHNG" Then
                If Len(strRecord) = 71 Then
                    intMsgSize = 71
                End If
            End If
            If Len(strRecord) < intMsgSize Then
                intMsgSize = Len(strRecord)
            End If
            If Len(strRecord) = intMsgSize Then
                tmpStr = Trim(Mid(LTrim(strRecord), 1, 9))
                If String.IsNullOrEmpty(tmpStr) Then Throw New ArgumentNullException("tmpStr", "Null SSN value derived")
                _strSSN = tmpStr

                tmpStr = Trim(Mid(strRecord, 10, 10))
                If String.IsNullOrEmpty(tmpStr) Then Throw New ArgumentNullException("tmpStr", "Null Fund value derived")
                _strFund = tmpStr

                If Len(_strFund) > 2 Then
                    _strFund = Mid(_strFund, 2)
                End If

                Select Case Trim(_strFund)
                    Case "02", "03", "05", "04", "06", "07", "08", "09", "10", "11", "12", "13"
                    Case Else
                        Throw New ArgumentOutOfRangeException("_strFund", Trim(_strFund), "Invalid Fund Value")
                End Select
                Select Case strMsgType
                    Case "FAID"
                        tmpStr = Trim(Mid(strRecord, 20, 7))  'awdyr
                        If String.IsNullOrEmpty(tmpStr) Then Throw New ArgumentNullException("tmpStr", "Null Awdyr value derived")
                        _strAwdyr = tmpStr
                        strYYYY = _strAwdyr
                        tmpStr = Trim(Mid(strRecord, 28, 20))  'faid
                        If String.IsNullOrEmpty(tmpStr) Then Throw New ArgumentNullException("tmpStr", "Null FAID value derived")
                        _strFAID = tmpStr
                        ParseMessage = ParseFAIDMessage(strRecord)
                        If Not ParseMessage Then
                            _strError = " Failure while parsing FAID Message: " & _strError
                            Exit Function
                        End If
                    Case "HEAD"
                        tmpStr = Trim(Mid(strRecord, 20, 7))  'awdyr
                        If String.IsNullOrEmpty(tmpStr) Then Throw New ArgumentNullException("tmpStr", "Null Awdyr value derived")
                        _strAwdyr = tmpStr
                        strYYYY = _strAwdyr
                        tmpStr = Trim(Mid(strRecord, 28, 20)) 'faid
                        If String.IsNullOrEmpty(tmpStr) Then Throw New ArgumentNullException("tmpStr", "Null FAID value derived")
                        _strFAID = tmpStr
                        ParseMessage = ParseHEADMessage(strRecord)
                        If Not ParseMessage Then
                            _strError = " Failure while parsing HEAD Message: " & _strError
                            Exit Function
                        End If
                    Case "DISB"
                        tmpStr = Trim(Mid(strRecord, 20, 20))  'data validation required
                        If String.IsNullOrEmpty(tmpStr) Then Throw New ArgumentNullException("tmpStr", "Null FAID value derived")
                        _strFAID = tmpStr
                        ParseMessage = ParseDISBMessage(strRecord)
                        If Not ParseMessage Then
                            _strError = " Failure while parsing DISB Message : " & _strError
                            Exit Function
                        End If
                    Case "CHNG"
                        tmpStr = Trim(Mid(strRecord, 20, 20))  'data validation required
                        If String.IsNullOrEmpty(tmpStr) Then Throw New ArgumentNullException("tmpStr", "Null FAID value derived")
                        _strFAID = tmpStr
                        ParseMessage = ParseCHNGMessage(strRecord)
                        If Not ParseMessage Then
                            _strError = " Failure while parsing  CHNG Message: " & _strError
                            Exit Function
                        End If
                    Case "RCVD"
                        tmpStr = Trim(Mid(strRecord, 20, 20))  'data validation required
                        If String.IsNullOrEmpty(tmpStr) Then Throw New ArgumentNullException("tmpStr", "Null FAID value derived")
                        _strFAID = tmpStr
                        ParseMessage = ParseRCVDMessage(strRecord)
                        If Not ParseMessage Then
                            _strError = " Failure while parsing RCVD Message: " & _strError
                            Exit Function
                        End If
                    Case "REFU"
                        tmpStr = Trim(Mid(strRecord, 20, 20))  'data validation required
                        If String.IsNullOrEmpty(tmpStr) Then Throw New ArgumentNullException("tmpStr", "Null FAID value derived")
                        _strFAID = tmpStr
                        ParseMessage = ParseREFUMessage(strRecord)
                        If Not ParseMessage Then
                            _strError = " Failure while parsing REFU Message: " & _strError
                            Exit Function
                        End If
                End Select
            Else
                Throw New ArgumentOutOfRangeException("intMsgSize", intMsgSize, "Invalid Record Size")
            End If

        Catch e As ArgumentNullException
            _strError = e.Message
            Throw
        Catch e As ArgumentOutOfRangeException
            _strError = e.Message
            Throw
        Catch e As System.Exception
            _strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseFAIDMessage(ByVal strRecord As String) As Boolean
        Dim tmpStr As String

        ParseFAIDMessage = False
        If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided for FAID message parse")
        Try
            _strError = ""
            Select Case Trim(_strFund)
                Case "02", "03", "05"
                    'do nothing
                Case "04", "06", "07", "08", "09", "10", "11", "12", "13"
                    'loan begin 
                    If Not ParseRecordDate(_dateDate1, 48, strRecord) Then
                        'Do Nothing
                        'Exit Function
                    End If
                    'loan end
                    If Not ParseRecordDate(_dateDate2, 56, strRecord) Then
                        'Do Nothing
                        'Exit Function
                    End If
                Case Else
                    Throw New ArgumentOutOfRangeException("_strFund", Trim(_strFund), "Invalid fund code found in FAID message")
            End Select

            'Gross Amount
            tmpStr = Trim(Mid(strRecord, 64, 8))
            If Not Decimal.TryParse(tmpStr, _decAmount1) Then _decAmount1 = 0.0
            ParseFAIDMessage = True

        Catch e As ArgumentNullException
            _strError = e.Message
        Catch e As ArgumentOutOfRangeException
            _strError = e.Message
        Catch e As System.exception
            _strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseHEADMessage(ByVal strRecord As String) As Boolean
        Dim tmpStr As String

        ParseHEADMessage = False
        If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided for HEAD message parse")
        Try
            _strError = ""
            Select Case Trim(_strFund)
                Case "02", "03", "05"
                    'do nothing
                Case "04", "06", "07", "08", "09", "10", "11", "12", "13"
                    'loan begin 
                    If Not ParseRecordDate(_dateDate1, 48, strRecord) Then
                        'Do Nothing
                        'Exit Function
                    End If
                    'loan end
                    If Not ParseRecordDate(_dateDate2, 56, strRecord) Then
                        'Do Nothing
                        'Exit Function
                    End If
                Case Else
                    Throw New ArgumentOutOfRangeException("_strFund", Trim(_strFund), "Invalid fund code found in HEAD message")
            End Select
            'Gross Amount
            tmpStr = Trim(Mid(strRecord, 81, 8))
            If Not Decimal.TryParse(tmpStr, _decAmount1) Then _decAmount1 = 0.0
            'Net Amount
            tmpStr = Trim(Mid(strRecord, 89, 8))
            If Not Decimal.TryParse(tmpStr, _decAmount2) Then _decAmount2 = 0.0
            ParseHEADMessage = True

        Catch e As ArgumentNullException
            _strError = e.Message
        Catch e As ArgumentOutOfRangeException
            _strError = e.Message
        Catch e As System.exception
            _strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseCHNGMessage(ByVal strRecord As String) As Boolean
        Dim tmpStr As String
        ParseCHNGMessage = False
        If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided for CHNG message parse")

        Try
            _strError = ""
            _strChgCode = Mid(strRecord, 40, 1)
            Select Case _strChgCode
                Case "C"
                    If Not ParseRecordDate(_dateDate1, 41, strRecord) Then
                        'Exit Function 'origdate
                        'Do nothing
                    End If
                    tmpStr = Trim(Mid(strRecord, 49, 8))         'origamt 
                    If Not Decimal.TryParse(tmpStr, _decAmount1) Then _decAmount1 = 0.0
                    If Not ParseRecordDate(_dateDate2, 57, strRecord) Then
                        'Exit Function 'newdate
                        'Do nothing
                    End If
                    tmpStr = Trim(Mid(strRecord, 65, 8))         'newamt
                    If Not Decimal.TryParse(tmpStr, _decAmount2) Then _decAmount2 = 0.0
                    ParseCHNGMessage = True
                Case "D"
                    If Not ParseRecordDate(_dateDate1, 41, strRecord) Then
                        'Exit Function 'origdate
                        'Do nothing
                    End If
                    tmpStr = Trim(Mid(strRecord, 49, 8))         'origamt
                    If Not Decimal.TryParse(tmpStr, _decAmount1) Then _decAmount1 = 0.0
                    ParseCHNGMessage = True
                Case "N"
                    If Not ParseRecordDate(_dateDate2, 57, strRecord) Then
                        'Exit Function 'newdate
                        'Do nothing
                    End If

                    tmpStr = Trim(Mid(strRecord, 65, 8))         'newamt
                    If Not Decimal.TryParse(tmpStr, _decAmount2) Then _decAmount2 = 0.0
                    ParseCHNGMessage = True
                    'Case "X"  'not yet supported
                Case Else
                    Throw New ArgumentOutOfRangeException("_strChgCode", _strChgCode, "Invalid fund code found in CHNG message")
            End Select
        Catch e As ArgumentNullException
            _strError = e.Message
        Catch e As ArgumentOutOfRangeException
            _strError = e.Message
        Catch e As System.exception
            _strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseRCVDMessage(ByVal strRecord As String) As Boolean
        Dim tmpStr As String
        ParseRCVDMessage = False
        If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided for RCVD message parse")
        Try
            _strError = ""
            If Not ParseRecordDate(_dateDate1, 40, strRecord) Then Exit Function 'newdate
            'disamt
            tmpStr = Trim(Mid(strRecord, 48, 8))
            If Not Decimal.TryParse(tmpStr, _decAmount1) Then _decAmount1 = 0.0
            _strCheckNo = Mid(strRecord, 67, 10)
            ParseRCVDMessage = True

        Catch e As ArgumentNullException
            _strError = e.Message
        Catch e As ArgumentOutOfRangeException
            _strError = e.Message
        Catch e As System.Exception
            _strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseREFUMessage(ByVal strRecord As String) As Boolean
        Dim tmpStr As String

        ParseREFUMessage = False
        If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided for REFU message parse")

        Try
            _strError = ""
            Select Case Trim(_strFund)
                Case "02", "03", "05"
                    'do nothing
                    If Not ParseRecordDate(_dateDate1, 40, strRecord) Then
                        'Do nothing

                    End If
                Case "04", "06", "07", "08", "09", "10", "11", "12", "13"
                    'do nothing
                    If Not ParseRecordDate(_dateDate1, 40, strRecord) Then
                        'Do nothing
                    End If
                    'strYYYY = Format(dateDate1, "YYYY")
                Case Else
                    Throw New ArgumentOutOfRangeException("_strFund", Trim(_strFund), "Invalid fund code found in REFU message")
            End Select
            tmpStr = Trim(Mid(strRecord, 48, 8))
            If Not Decimal.TryParse(tmpStr, _decAmount1) Then _decAmount1 = 0.0
            ParseREFUMessage = True

        Catch e As ArgumentNullException
            _strError = e.Message
        Catch e As ArgumentOutOfRangeException
            _strError = e.Message
        Catch e As System.exception
            _strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseDISBMessage(ByVal strRecord As String) As Boolean
        Dim tmpStr As String

        ParseDISBMessage = False
        If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided for DISB message parse")

        Try
            _strError = ""
            If Not ParseRecordDate(_dateDate1, 40, strRecord) Then Exit Function 'disamt
            tmpStr = Trim(Mid(strRecord, 48, 8))
            If Not Decimal.TryParse(tmpStr, _decAmount1) Then _decAmount1 = 0.0
            ParseDISBMessage = True

        Catch e As ArgumentNullException
            _strError = e.Message
        Catch e As System.Exception
            _strError = e.Message
            Throw
        End Try
    End Function
    Public Function GetMessageSize(ByVal strMsgType As String) As Integer
        Const constCHNGsz As Integer = 72
        Const constDISBsz As Integer = 65
        Const constFAIDsz As Integer = 71
        Const constHEADsz As Integer = 104
        Const constRCVDsz As Integer = 86
        Const constREFUsz As Integer = 115
        GetMessageSize = 0
        If String.IsNullOrEmpty(strMsgType) Then Exit Function
        Select Case strMsgType
            Case "FAID"
                GetMessageSize = constFAIDsz
            Case "HEAD"
                GetMessageSize = constHEADsz
            Case "CHNG"
                GetMessageSize = constCHNGsz
            Case "RCVD"
                GetMessageSize = constRCVDsz
            Case "DISB"
                GetMessageSize = constDISBsz
            Case "REFU"
                GetMessageSize = constREFUsz
        End Select

    End Function
#End Region
End Class

Public Class FLCollectionMessageInfo
    Inherits CollectionBase
#Region " Public Constructors "
    Public Sub New()
        MyBase.New()
    End Sub
#End Region
#Region " Public Properties"
    Public ReadOnly Property Items(ByVal index As Integer) As FLMessageInfo
        Get
            Try
                Return CType(List.Item(index), FLMessageInfo)
            Catch e As System.Exception
                Throw
            End Try
        End Get
    End Property
#End Region
#Region " Public Methods"
    Public Sub Add(ByVal clsFameLinkMessage As FLMessageInfo)
        List.Add(clsFameLinkMessage)
    End Sub
#End Region
End Class

Public Class FLCollectionMessageInfos
#Region " Private Variables and Objects"
    Private _clsCollectionMessage As FLCollectionMessageInfo
#End Region
#Region " Public Constructors "
    Public Sub New()
        _clsCollectionMessage = New FLCollectionMessageInfo
    End Sub
#End Region
#Region " Public Properties"
    Public ReadOnly Property Count() As Integer
        Get
            Return _clsCollectionMessage.Count
        End Get
    End Property
#End Region
#Region " Public Methods"
    Public Sub Add(ByVal FileId As String, ByVal strMsgType As String, ByVal strRecord As String, Optional ByVal strFileName As String = "", Optional ByVal intRecordPosition As Integer = 0)
        Try
            If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided")
            If String.IsNullOrEmpty(strMsgType) Then Throw New ArgumentNullException("strMsgType", "Empty message type parameter provided")
            Dim clsFameLinkMessage As New FLMessageInfo(FileId, strMsgType, strRecord, strFileName, , , intRecordPosition)
            Me.Add(clsFameLinkMessage)

        Catch e As ArgumentNullException
            Throw
        Catch e As ArgumentOutOfRangeException
            Throw
        Catch e As System.Exception
            Throw
        End Try
    End Sub
    Public Sub Add(ByVal clsMsgEntry As FLMessageInfo)
        _clsCollectionMessage.Add(clsMsgEntry)
    End Sub
    Public Function Items(ByVal index As Integer) As FLMessageInfo
        Try
            Return CType(_clsCollectionMessage.Items(index), FLMessageInfo)
        Catch e As System.Exception
            Throw
        End Try
    End Function
#End Region
End Class
