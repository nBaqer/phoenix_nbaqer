Public Class StudentDocsInfo
#Region "Variables"
    Private _StudentId As String
    Private _LeadId As String
    Private _IsInDb As Boolean
    Private _DocumentId As String
    Private _DocStatusId As String
    Private _RequestDate As String
    Private _ReceiveDate As String
    Private _ScannedId As String
    Private _DocumentDescrip As String
    Private _ModDate As Date
    Private _ModUser As String
    Private _StudentDocsId As String
    Private _LastName As String
    Private _FirstName As String
    Private _SDF As String
    Private _Module As String
    Private _Pages As String
    Private _MiddleName As String
    Private _ModuleId As Integer
    Private _FormatType As String
    Private _YearNumber As Integer
    Private _MonthNumber As Integer
    Private _DateNumber As Integer
    Private _LNameNumber As Integer
    Private _FNameNumber As Integer
    Private _SeqNumber As Integer
    Private _SeqStartNumber As Integer
    Private _OverRide As Boolean
    Private _DocumentStatus As String
    Private _TrackCode As String
    Private _DueDate As String
    Private _TransDate As String
    Private _CompletedDate As String
    Private _NotificationCode As String
    Private _DocDescrip As String
#End Region
#Region "Constructor"
    Public Sub New()
        _IsInDb = False
        _StudentId = Guid.Empty.ToString
        _DocumentId = Guid.Empty.ToString
        _DocStatusId = Guid.Empty.ToString
        _LeadId = Guid.Empty.ToString
        _ReceiveDate = ""
        _RequestDate = ""
        _ScannedId = ""
        _DocumentDescrip = ""
        '_ModDate = ""
        '_ModUser = ""
        '_StudentDocsId = Guid.NewGuid.ToString()
        _FirstName = ""
        _LastName = ""
        _SDF = Guid.Empty.ToString
        _Module = ""
        _Pages = ""
        _MiddleName = ""
        _ModuleId = 0
        _OverRide = False
        _DocumentStatus = ""
        _TrackCode = ""
        _DueDate = ""
        _TransDate = ""
        _CompletedDate = ""
        _NotificationCode = ""
        _DocDescrip = ""
    End Sub
#End Region
#Region "Property"
    Public Property StudentDocsId() As String
        Get
            Return _StudentDocsId
        End Get
        Set(ByVal Value As String)
            _StudentDocsId = Value
        End Set
    End Property
    Public Property DocDescrip() As String
        Get
            Return _DocDescrip
        End Get
        Set(ByVal Value As String)
            _DocDescrip = Value
        End Set
    End Property
    Public Property TrackCode() As String
        Get
            Return _TrackCode
        End Get
        Set(ByVal Value As String)
            _TrackCode = Value
        End Set
    End Property
    Public Property DueDate() As String
        Get
            Return _DueDate
        End Get
        Set(ByVal Value As String)
            _DueDate = Value
        End Set
    End Property
    Public Property TransDate() As String
        Get
            Return _TransDate
        End Get
        Set(ByVal Value As String)
            _TransDate = Value
        End Set
    End Property
    Public Property CompletedDate() As String
        Get
            Return _CompletedDate
        End Get
        Set(ByVal Value As String)
            _CompletedDate = Value
        End Set
    End Property
    Public Property NotificationCode() As String
        Get
            Return _NotificationCode
        End Get
        Set(ByVal Value As String)
            _NotificationCode = Value
        End Set
    End Property
    Public Property ModuleId() As Integer
        Get
            Return _ModuleId
        End Get
        Set(ByVal Value As Integer)
            _ModuleId = Value
        End Set
    End Property
    Public Property DocumentStatus() As String
        Get
            Return _DocumentStatus
        End Get
        Set(ByVal Value As String)
            _DocumentStatus = Value
        End Set
    End Property
    Public Property MiddleName() As String
        Get
            Return _MiddleName
        End Get
        Set(ByVal Value As String)
            _MiddleName = Value
        End Set
    End Property
    Public Property SDF() As String
        Get
            Return _SDF
        End Get
        Set(ByVal Value As String)
            _SDF = Value
        End Set
    End Property
    Public Property Pages() As String
        Get
            Return _Pages
        End Get
        Set(ByVal Value As String)
            _Pages = Value
        End Set
    End Property
    Public Property ModuleName() As String
        Get
            Return _Module
        End Get
        Set(ByVal Value As String)
            _Module = Value
        End Set
    End Property

    Public Property IsInDb() As Boolean
        Get
            Return _IsInDb
        End Get
        Set(ByVal Value As Boolean)
            _IsInDb = Value
        End Set
    End Property
    Public Property Override() As Boolean
        Get
            Return _OverRide
        End Get
        Set(ByVal Value As Boolean)
            _OverRide = Value
        End Set
    End Property
    Public Property StudentId() As String
        Get
            Return _StudentId
        End Get
        Set(ByVal Value As String)
            _StudentId = Value
        End Set
    End Property
    Public Property LeadId() As String
        Get
            Return _LeadId
        End Get
        Set(ByVal Value As String)
            _LeadId = Value
        End Set
    End Property
    Public Property DocumentId() As String
        Get
            Return _DocumentId
        End Get
        Set(ByVal Value As String)
            _DocumentId = Value
        End Set
    End Property
    Public Property DocumentDescrip() As String
        Get
            Return _DocumentDescrip
        End Get
        Set(ByVal Value As String)
            _DocumentDescrip = Value
        End Set
    End Property
    Public Property DocStatusId() As String
        Get
            Return _DocStatusId
        End Get
        Set(ByVal Value As String)
            _DocStatusId = Value
        End Set
    End Property
    Public Property RequestDate() As String
        Get
            Return _RequestDate
        End Get
        Set(ByVal Value As String)
            _RequestDate = Value
        End Set
    End Property
    Public Property ReceiveDate() As String
        Get
            Return _ReceiveDate
        End Get
        Set(ByVal Value As String)
            _ReceiveDate = Value
        End Set
    End Property
    Public Property ScannedId() As String
        Get
            Return _ScannedId
        End Get
        Set(ByVal Value As String)
            _ScannedId = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _ModDate
        End Get
        Set(ByVal Value As Date)
            _ModDate = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _ModUser
        End Get
        Set(ByVal Value As String)
            _ModUser = Value
        End Set
    End Property
    Public Property FirstName() As String
        Get
            Return _FirstName
        End Get
        Set(ByVal Value As String)
            _FirstName = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return _LastName
        End Get
        Set(ByVal Value As String)
            _LastName = Value
        End Set
    End Property
#End Region
End Class
