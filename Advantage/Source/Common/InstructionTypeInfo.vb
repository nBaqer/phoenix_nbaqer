﻿Public Class InstructionTypeInfo

    ' 08/25/2011 JRobinson Clock Hour Project - Add new class for arInstructionType table


    '
    '   InstructionTypeInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _InstructionTypeId As String
    Private _origInstructionTypeId As String
    Private _InstructionTypeCode As String
    Private _statusId As String
    Private _status As String
    Private _InstructionTypeDescrip As String
    Private _isDefault As Boolean
    Private _campGrpId As String
    Private _modUser As String
    Private _modDate As DateTime

#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _InstructionTypeId = Guid.NewGuid.ToString
        _origInstructionTypeId = Guid.Empty.ToString
        _InstructionTypeCode = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _InstructionTypeDescrip = ""
        _isDefault = False
        _campGrpId = Guid.Empty.ToString
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property InstructionTypeId() As String
        Get
            Return _InstructionTypeId
        End Get
        Set(ByVal Value As String)
            _InstructionTypeId = Value
        End Set
    End Property
    Public Property OrigInstructionTypeId() As String
        Get
            Return _origInstructionTypeId
        End Get
        Set(ByVal Value As String)
            _origInstructionTypeId = Value
        End Set
    End Property
    Public Property InstructionTypeCode() As String
        Get
            Return _InstructionTypeCode
        End Get
        Set(ByVal Value As String)
            _InstructionTypeCode = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property InstructionTypeDescrip() As String
        Get
            Return _InstructionTypeDescrip
        End Get
        Set(ByVal Value As String)
            _InstructionTypeDescrip = Value
        End Set
    End Property
    Public Property IsDefault() As Boolean
        Get
            Return _isDefault
        End Get
        Set(ByVal Value As Boolean)
            _isDefault = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region


End Class
