' ===============================================================================
' TimeClockInfo.vb
' Info classes for Time Clocks
' ===============================================================================
' Copyright (C) 2006, 2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
Namespace TimeClock
    Public Enum TimeClockPunchType
        PunchIn = 1
        PunchOut = 2
    End Enum
    Public Enum TimeClockDataType
        ZON580
        CS2000
    End Enum

    Public Enum PunchStatus
        ''The student punched time which is not processed due to some reasons like early punch in or late punch out, tardy etc
        NotProcessed = 0
        ''When the punch is processed And the punches with status 1 are the actual punches.
        Ok = 1
        ''Due to exception when the punch is not valid. eg: punch in with no punch out, missing punches,  etc
        Invalid = 2

        Lunch = 3
        ''if no extra hours and punch is after the scheduled time out
        TooLate = 4
        ''When the record is read from the database, they are inserted into the databse with status inserted
        Inserted = 5
        ''When the student is not scheduled and the student punches in , eg: Loa, early start, after grad, after transferredout,
        NotScheduled = 6
        ''When Punch In and Punch out is not in order
        OutofOrder = 7
        ''while reading punches from the import file, if punches for the record already exists and again if, they are imported, then, the records which already exits in the database are set to status 8
        Duplicate = 8

        'For ByClass Schools, When we are not able to identify the Class Section Meeting, it is an exception.

        UnIdentifiedMeeting = 9

        ''When the first set of PUnches are valid and processed and then a new set of punches are inserted and if those are invalid then the Actual hours earned for the same day and ClsSectmeeting will be reset. So creating a new exception to throw all the valid and invalid punches as exception.

        ValidPunchesandInValidPunches = 10


    End Enum
    Public Class TimeClockPunchInfo
#Region "Attributes"
        Private _isInDb As Boolean
        Private _punchType As TimeClockPunchType
        Private _clockId As String
        Private _punchTime As DateTime
        Private _badgeId As String
        Private _stuEnrollId As String
        Private _status As PunchStatus
        Private _adjPunchTime As DateTime
        'Added by Saraswathi to find if the entry was posted by User or by System
        Private _FromSystem As Boolean
        ''Added by Saraswathi For ByClass Schools
        Private _ClsSectMeetingId As String
        Private _SpecialCode As String
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDb = False
            _punchTime = Date.MinValue
            _adjPunchTime = Date.MinValue
            _status = PunchStatus.NotProcessed
            'Added by Saraswathi to find if the entry was posted by User or by System
            _FromSystem = False
        End Sub
#End Region
#Region "Methods"
        Public Property PunchType() As TimeClockPunchType
            Get
                Return _punchType
            End Get
            Set(ByVal value As TimeClockPunchType)
                _punchType = value
            End Set
        End Property
        Public Property ClockId() As String
            Get
                Return _clockId
            End Get
            Set(ByVal value As String)
                _clockId = value
            End Set
        End Property
        Public Property PunchTime() As DateTime
            Get
                Return _punchTime
            End Get
            Set(ByVal value As DateTime)
                _punchTime = value
            End Set
        End Property
        Public Property BadgeId() As String
            Get
                Return _badgeId
            End Get
            Set(ByVal value As String)
                _badgeId = value
            End Set
        End Property
        Public Property StuEnrollId() As String
            Get
                Return _stuEnrollId
            End Get
            Set(ByVal value As String)
                _stuEnrollId = value
            End Set
        End Property
        Public Property Status() As PunchStatus
            Get
                Return _status
            End Get
            Set(ByVal value As PunchStatus)
                _status = value
            End Set
        End Property
        Public Property AdjPunchTime() As DateTime
            Get
                Return _adjPunchTime
            End Get
            Set(ByVal value As DateTime)
                _adjPunchTime = value
            End Set
        End Property
        ''Added by Saraswathi
        Public Property FromSystem() As Boolean
            Get
                Return _FromSystem
            End Get
            Set(ByVal value As Boolean)
                _FromSystem = value
            End Set
        End Property

        Public Property ClsSectMeetingId() As String
            Get
                Return _ClsSectMeetingId
            End Get
            Set(ByVal value As String)
                _ClsSectMeetingId = value
            End Set
        End Property

        Public Property SpecialCode() As String
            Get
                Return _SpecialCode
            End Get
            Set(ByVal value As String)
                _SpecialCode = value
            End Set
        End Property
#End Region
    End Class

    ' 08/04/2011 JRobinson Clock Hour Project - Add new class for TimeClockSpecialCode table
    ' 09/21/2011 JRobinson Clock Hour Project - Modfied class for table changes
    Public Class TimeClockSpecialCodeInfo
        '
        '   TimeClockSpecialCodeInfo Class
        '
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _TCSId As String
        Private _origTCSId As String
        Private _TCSpecialCode As String
        Private _statusId As String
        Private _status As String
        Private _punchType As Integer
        Private _campusId As String
        Private _campDescrip As String
        Private _instructionTypeId As String
        Private _instructionTypeDescrip As String
        Private _modUser As String
        Private _modDate As DateTime

#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _TCSId = Guid.NewGuid.ToString
            _origTCSId = Guid.Empty.ToString
            _TCSpecialCode = ""
            _statusId = Guid.Empty.ToString
            _status = ""
            _punchType = 0
            _campusId = Guid.Empty.ToString
            _campDescrip = ""
            _instructionTypeId = Guid.Empty.ToString
            _instructionTypeDescrip = ""
            _modUser = ""
            _modDate = Date.MinValue
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property TCSId() As String
            Get
                Return _TCSId
            End Get
            Set(ByVal Value As String)
                _TCSId = Value
            End Set
        End Property
        Public Property OrigTCSId() As String
            Get
                Return _origTCSId
            End Get
            Set(ByVal Value As String)
                _origTCSId = Value
            End Set
        End Property
        Public Property TCSpecialCode() As String
            Get
                Return _TCSpecialCode
            End Get
            Set(ByVal Value As String)
                _TCSpecialCode = Value
            End Set
        End Property
        Public Property StatusId() As String
            Get
                Return _statusId
            End Get
            Set(ByVal Value As String)
                _statusId = Value
            End Set
        End Property
        Public Property Status() As String
            Get
                Return _status
            End Get
            Set(ByVal Value As String)
                _status = Value
            End Set
        End Property
        Public Property PunchType() As Integer
            Get
                Return _punchType
            End Get
            Set(ByVal Value As Integer)
                _punchType = Value
            End Set
        End Property
        Public Property CampusId() As String
            Get
                Return _campusId
            End Get
            Set(ByVal Value As String)
                _campusId = Value
            End Set
        End Property
        Public Property CampDescrip() As String
            Get
                Return _campDescrip
            End Get
            Set(ByVal Value As String)
                _campDescrip = Value
            End Set
        End Property
        Public Property InstructionTypeId() As String
            Get
                Return _instructionTypeId
            End Get
            Set(ByVal Value As String)
                _instructionTypeId = Value
            End Set
        End Property
        Public Property InstructionTypeDescrip() As String
            Get
                Return InstructionTypeDescrip
            End Get
            Set(ByVal Value As String)
                _instructionTypeDescrip = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As Date
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
#End Region

    End Class

End Namespace

