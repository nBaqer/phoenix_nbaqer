<CLSCompliant(True)>
<Serializable()>
Public Class UserPagePermissionInfo

    Public Property ResourceId() As Int32
    Public Property HasFull() As Boolean
    Public Property HasEdit() As Boolean
    Public Property HasAdd() As Boolean
    Public Property HasDelete() As Boolean
    Public Property HasDisplay As Boolean
    Public Property HasNone As Boolean
    Public Property Url As String

    Public Sub New(ByVal newResourceId As Int32, ByVal newHasFull As Boolean,
      ByVal newHasAdd As Boolean, ByVal newHasEdit As Boolean,
      ByVal newHasDelete As Boolean, ByVal newHasDisplay As Boolean)
        HasNone = True
        Url = String.Empty
        ResourceId = newResourceId
        HasFull = newHasFull
        HasAdd = newHasAdd
        HasEdit = newHasEdit
        HasDelete = newHasDelete
        HasDisplay = newHasDisplay

    End Sub

    Public Sub New()
        HasNone = True
        Url = String.Empty
        HasDisplay = False
        HasEdit = False
        HasAdd = False
        HasDelete = False
        HasFull = False
    End Sub
End Class
