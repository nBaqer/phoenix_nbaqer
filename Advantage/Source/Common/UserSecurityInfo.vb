Imports System.Security.Principal
Imports System.Web

<Serializable()>
Public Class UserSecurityInfo
    Private _UserId As String
    Private _isInDB As Boolean
    Private _UserSecurity As GenericIdentity
    Private _UserPrincipal As GenericPrincipal
    Private _Context As HttpContext
    Private _UserName As String
    Private _UserPassword As String      ' The UserPassword is hashed
    Private _ConfirmUserPassword As String
    Private _Salt As String                   ' The Salt for the UserPassword
    Private _AccountActive As Boolean
    Private _UserRoles(300) As String        'This should be removed later. Only being used not to break code.
    Private _Roles As DataTable
    Private _UserRoleArrayNumber As Int32 ' This is the total amount of elements in the UserRoles array
    Private _FullName As String
    Private _LastName As String
    Private _FirstName As String
    Private _MI As String
    Private _Email As String
    Private _CampusId As String
    Private _ModuleId As Integer
    Private _BirthDate As DateTime
    Private _Question1 As Guid
    Private _Question2 As Guid
    Private _Question3 As Guid
    Private _Answer1 As String
    Private _Answer2 As String
    Private _Answer3 As String
    Private _EmpId As Guid
    Private _modUser As String
    Private _modDate As DateTime
    Private _isAdvantageSuperUser As Boolean
    Private _isDefaultAdminRep As Boolean


    Public Property UserId() As String
        Get
            Return _UserId
        End Get
        Set(ByVal Value As String)
            _UserId = Value
        End Set
    End Property
    Public Property UserSecurity() As GenericIdentity
        Get
            UserSecurity = _UserSecurity
        End Get
        Set(ByVal Value As GenericIdentity)
            _UserSecurity = Value
        End Set
    End Property
    Public Property UserPrincipal() As GenericPrincipal
        Get
            UserPrincipal = _UserPrincipal
        End Get
        Set(ByVal Value As GenericPrincipal)
            _UserPrincipal = Value
        End Set
    End Property
    Public Property Context() As HttpContext
        Get
            Context = _Context
        End Get
        Set(ByVal Value As HttpContext)
            _Context = Value
        End Set
    End Property
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property UserName() As String
        Get
            UserName = _UserName
        End Get
        Set(ByVal Value As String)
            _UserName = Value
        End Set
    End Property
    Public Property UserPassword() As String
        Get
            UserPassword = _UserPassword
        End Get
        Set(ByVal Value As String)
            _UserPassword = Value
        End Set
    End Property
    Public Property ConfirmUserPassword() As String
        Get
            Return _ConfirmUserPassword
        End Get
        Set(ByVal Value As String)
            _ConfirmUserPassword = Value
        End Set
    End Property
    Public Property Salt() As String
        Get
            Salt = _Salt
        End Get
        Set(ByVal Value As String)
            _Salt = Value
        End Set
    End Property
    Public Property AccountActive() As Boolean
        Get
            AccountActive = _AccountActive
        End Get
        Set(ByVal Value As Boolean)
            _AccountActive = Value
        End Set
    End Property
    Public Property UserRoles() As Array
        Get
            UserRoles = _UserRoles
        End Get
        Set(ByVal Value As Array)
            _UserRoles = Value
        End Set
    End Property
    Public Property UserRoleArrayNumber() As Int32
        Get
            UserRoleArrayNumber = _UserRoleArrayNumber
        End Get
        Set(ByVal Value As Int32)
            _UserRoleArrayNumber = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            LastName = _LastName
        End Get
        Set(ByVal Value As String)
            _LastName = Value
        End Set
    End Property
    Public Property FirstName() As String
        Get
            FirstName = _FirstName
        End Get
        Set(ByVal Value As String)
            _FirstName = Value
        End Set
    End Property
    Public Property MI() As String
        Get
            MI = _MI
        End Get
        Set(ByVal Value As String)
            _MI = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal Value As String)
            _Email = Value
        End Set
    End Property
    Public Property CampusId() As String
        Get
            Return _CampusId
        End Get
        Set(ByVal Value As String)
            _CampusId = Value
        End Set
    End Property
    Public Property ModuleId() As String
        Get
            Return _ModuleId
        End Get
        Set(ByVal Value As String)
            _ModuleId = Value
        End Set
    End Property
    Public Property BirthDate() As DateTime
        Get
            BirthDate = _BirthDate
        End Get
        Set(ByVal Value As DateTime)
            _BirthDate = BirthDate
        End Set
    End Property
    Public Property Question1() As Guid
        Get
            Question1 = _Question1
        End Get
        Set(ByVal Value As Guid)
            _Question1 = Value
        End Set
    End Property
    Public Property Question2() As Guid
        Get
            Question2 = _Question2
        End Get
        Set(ByVal Value As Guid)
            _Question2 = Value
        End Set
    End Property
    Public Property Question3() As Guid
        Get
            Question3 = _Question3
        End Get
        Set(ByVal Value As Guid)
            _Question3 = Value
        End Set
    End Property
    Public Property Answer1() As String
        Get
            Answer1 = _Answer1
        End Get
        Set(ByVal Value As String)
            _Answer1 = Value
        End Set
    End Property
    Public Property FullName() As String
        Get
            Return _FullName
        End Get
        Set(ByVal Value As String)
            _FullName = Value
        End Set
    End Property
    Public Property Answer2() As String
        Get
            Answer2 = _Answer2
        End Get
        Set(ByVal Value As String)
            _Answer2 = Value
        End Set
    End Property
    Public Property Answer3() As String
        Get
            Answer3 = _Answer3
        End Get
        Set(ByVal Value As String)
            _Answer3 = Value
        End Set
    End Property
    Public Property EmpId() As Guid
        Get
            EmpId = _EmpId
        End Get
        Set(ByVal Value As Guid)
            _EmpId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property Roles() As DataTable
        Get
            Return _Roles
        End Get
        Set(ByVal Value As DataTable)
            _Roles = Value
        End Set
    End Property
    Public Property IsAdvantageSuperUser() As Boolean
        Get
            Return _isAdvantageSuperUser
        End Get
        Set(ByVal Value As Boolean)
            _isAdvantageSuperUser = Value
        End Set
    End Property
    Public Property IsDefaultAdminRep() As Boolean
        Get
            Return _isDefaultAdminRep
        End Get
        Set(ByVal value As Boolean)
            _isDefaultAdminRep = value
        End Set
    End Property

    Public Sub New(ByVal NewUserSecurity As GenericIdentity, ByVal NewUserPrincipal As GenericPrincipal, _
          ByVal NewContext As HttpContext, ByVal NewUserRoles As Array, ByVal NewUserRoleArrayNumber As Int32, _
          ByVal NewUserName As String, ByVal NewUserPassword As String, ByVal NewSalt As String, ByVal NewAccountActive As Boolean, _
          ByVal NewLastName As String, ByVal NewFirstName As String, ByVal NewMI As String, ByVal NewBirthDate As DateTime, _
          ByVal NewQuestion1 As Guid, ByVal NewQuestion2 As Guid, ByVal NewQuestion3 As Guid, _
          ByVal NewAnswer1 As String, ByVal NewAnswer2 As String, ByVal NewAnswer3 As String, ByVal NewEmpId As Guid)

        _UserSecurity = NewUserSecurity
        _UserPrincipal = NewUserPrincipal
        _Context = NewContext
        _UserName = NewUserName
        _UserPassword = NewUserPassword
        _Salt = NewSalt
        _AccountActive = NewAccountActive
        _UserRoles = NewUserRoles
        _UserRoleArrayNumber = NewUserRoleArrayNumber
        _FirstName = NewFirstName
        _LastName = NewLastName
        _MI = NewMI
        _BirthDate = NewBirthDate
        _Question1 = NewQuestion1
        _Question2 = NewQuestion2
        _Question3 = NewQuestion3
        _Answer1 = NewAnswer1
        _Answer2 = NewAnswer2
        _Answer3 = NewAnswer3
        _EmpId = NewEmpId
        _isAdvantageSuperUser = False
    End Sub

    Public Sub New()
        MyBase.New()
        _UserId = Guid.NewGuid.ToString
        _isInDB = False
        _FullName = ""
        _UserName = ""
        _UserPassword = ""
        _AccountActive = False
        _CampusId = ""
        _Email = ""
        _modUser = ""
        _modDate = Date.MinValue
        _isAdvantageSuperUser = False
    End Sub

    Public Sub ResizeArray(ByVal arraySize As Integer)
        ReDim Preserve _UserRoles(arraySize)
    End Sub
End Class
