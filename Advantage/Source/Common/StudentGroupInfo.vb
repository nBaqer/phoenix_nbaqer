Public Class StudentGroupInfo
    Private _SGroupId As Guid
    Private _StatusId As Guid
    Private _Descrip As String
    Private _EmpId As Guid
    Private _IsPublic As Boolean
    Private _SGroupTypeId As Int32
    Private _SQLStatement As String
    Private _Students As StudentGroupStudentInfo()
    Public Property SGroupId() As Guid
        Get
            SGroupId = _SGroupId
        End Get
        Set(ByVal Value As Guid)
            _SGroupId = Value
        End Set
    End Property
    Public Property StatusId() As Guid
        Get
            StatusId = _StatusId
        End Get
        Set(ByVal Value As Guid)
            _StatusId = Value
        End Set
    End Property
    Public Property Descrip() As String
        Get
            Descrip = _Descrip
        End Get
        Set(ByVal Value As String)
            _Descrip = Value
        End Set
    End Property
    Public Property EmpId() As Guid
        Get
            EmpId = _EmpId
        End Get
        Set(ByVal Value As Guid)
            _EmpId = Value
        End Set
    End Property
    Public Property IsPublic() As Boolean
        Get
            IsPublic = _IsPublic
        End Get
        Set(ByVal Value As Boolean)
            _IsPublic = Value
        End Set
    End Property
    Public Property SGroupTypeId() As Int32
        Get
            SGroupTypeId = _SGroupTypeId
        End Get
        Set(ByVal Value As Int32)
            _SGroupTypeId = Value
        End Set
    End Property
    Public Property SQLStatement() As String
        Get
            SQLStatement = _SQLStatement
        End Get
        Set(ByVal Value As String)
            _SQLStatement = Value
        End Set
    End Property
    Public Property Students() As StudentGroupStudentInfo()
        Get
            Students = _Students
        End Get
        Set(ByVal Value As StudentGroupStudentInfo())
            _Students = Value

        End Set
    End Property
    Public Sub New()
        MyBase.new()
    End Sub
    Public Sub New(ByVal NewSGroupId As Guid, ByVal NewStatusId As Guid, ByVal NewDescrip As String, ByVal NewEmpId As Guid, ByVal NewIsPublic As Boolean, ByVal NewSGroupTypeid As Int32, _
    ByVal NewSQLStatement As String, ByVal NewStudents As StudentGroupStudentInfo())

        _SGroupId = NewSGroupId
        _StatusId = NewStatusId
        _Descrip = NewDescrip
        _EmpId = NewEmpId
        _IsPublic = NewIsPublic
        _SGroupTypeId = NewSGroupTypeid
        _SQLStatement = NewSQLStatement
        _Students = NewStudents

    End Sub
End Class
