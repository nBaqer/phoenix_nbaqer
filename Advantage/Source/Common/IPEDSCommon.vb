Public Class IPEDSCommon
	Public Const AgencyName As String = "IPEDS"

    Public Const RptScriptTimeout As Integer = 300

	Public Const RptId_PartADetail As Integer = 410
	Public Const RptId_PartASummary As Integer = 411
	Public Const RptId_PartBDetail As Integer = 412
	Public Const RptId_PartBSummary As Integer = 413
	Public Const RptId_PartCDetail As Integer = 414
	Public Const RptId_PartCSummary As Integer = 415
	Public Const RptId_PartD As Integer = 416
	Public Const RptId_PartEDetail As Integer = 417
	Public Const RptId_PartESummary As Integer = 418
	Public Const RptId_PartF As Integer = 419
	Public Const RptId_PartG As Integer = 420
	Public Const RptId_GR2YR3Detail As Integer = 422
	Public Const RptId_GR2YR3Summary As Integer = 423
	Public Const RptId_GRLT2YR3Detail As Integer = 424
	Public Const RptId_GRLT2YR3Summary As Integer = 425
	Public Const RptId_GR4YR1Detail As Integer = 446
	Public Const RptId_GR4YR1Summary As Integer = 447
	Public Const RptId_GR4YR2Detail As Integer = 448
	Public Const RptId_GR4YR2Summary As Integer = 449
	Public Const RptId_GR4YR3Detail As Integer = 450
	Public Const RptId_GR4YR3Summary As Integer = 451
	Public Const RptId_FinAll As Integer = 421
	Public Const RptId_SFAPRDetail As Integer = 427
	Public Const RptId_SFAPRSummary As Integer = 428
	Public Const RptId_SFAAYRPDetail As Integer = 466
	Public Const RptId_SFAAYRPSummary As Integer = 467
	Public Const RptId_FCompDetail As Integer = 444
	Public Const RptId_FCompSummary As Integer = 445
	Public Const RptId_FICC3Detail As Integer = 452
	Public Const RptId_FICC3Summary As Integer = 458
	Public Const RptId_FICC4Detail As Integer = 459
	Public Const RptId_FICC4Summary As Integer = 460

	Public Const SchoolProgramReporter As String = "Program Reporter"
	Public Const SchoolAcademicYearReporter As String = "Academic Year Reporter"

	Public Const CohortFullYear As String = "Full Year"
	Public Const CohortFall As String = "Fall"

	Public Const SortByStudentStudentId As String = "StudentIdentifier"
	Public Const SortByStudentLastName As String = "LastName"

	Public Const SortByProgramProgCode As String = "ProgramCode"
	Public Const SortByProgramProgName As String = "ProgramName"

    Public Const PageReportViewer As String = "ReportViewer"
    Public Const PageReportFriendlyPrint As String = "ReportFriendlyPrint"
    Public Const PageReportExportViewer As String = "ReportExportViewer"

	Public Const LastNameSize As Integer = 20
	Public Const FirstNameSize As Integer = 15
	Public Const MiddleNameSize As Integer = 1
	Public Const StudentIdSize As Integer = 13

	Public Const StateDomesticSort As Integer = 1
	Public Const StateUnknownDescrip As String = "State Unknown"
	Public Const StateUnknownFIPSCode As String = "57"
	Public Const StateUnknownSort As Integer = 2
	Public Const StateNotDomesticSort As Integer = 3
	Public Const StateForeignCountryDescrip As String = "Foreign Countries"
	Public Const StateForeignCountryFIPSCode As String = "90"
	Public Const StateForeignCountrySort As Integer = 4
	Public Const StateResUnreportedDescrip As String = "Residence unknown/unreported"
	Public Const StateResUnreportedFIPSCode As String = "98"
	Public Const StateResUnreportedSort As Integer = 5

	Public Const DropReasonsExclude As String = "'Deceased'," & _
												"'Disabled'," & _
												"'Joined Armed Forces'," & _
												"'Foreign Aid Service'," & _
												"'Offical Church Mission'"

	Public Const ProgTypeUndergraduate As String = "Undergraduate"
	Public Const ProgTypeGraduate As String = "Graduate"
	Public Const ProgTypeFirstProf As String = "First Professional"

	Public Const AttendTypeFullTime As String = "Full Time"
	Public Const AttendTypePartTime As String = "Part Time"

	Public Const DegCertSeekingFirstAnyInst As String = "First-time, any Institution"
	Public Const DegCertSeekingFirstThisInst As String = "First-time, this Institution"
	Public Const DegCertSeekingFirstTime As String = "First-time"
	Public Const DegCertSeekingOtherDegree As String = "Other degree/certificate seeking"
	Public Const DegCertSeekingNonDegree As String = "Non-degree/certificate seeking"

	Public Const GradRates2YrYearsPrior As Integer = 4
	Public Const GradRates4YrYearsPrior As Integer = 7

	' standard column name used for Student Identifer (i.e. SSN, Student Number, or EnrollmentId) 
	'	in report DataSets
	Public Const RptStuIdentifierColName As String = "StudentIdentifier"

	Public Const TblNameStudents As String = "Students"
	Public Const TblNameEnrollments As String = "Enrollments"
	Public Const TblNameLeads As String = "Leads"
	Public Const TblNameScores As String = "Scores"

	Public Const EntrTestSAT1Verbal As String = "SAT 1 Verbal"
	Public Const EntrTestSAT1Math As String = "SAT 1 Math"
	Public Const EntrTestACTComp As String = "ACT Composite"
	Public Const EntrTestACTEnglish As String = "ACT English"
	Public Const EntrTestACTMath As String = "ACT Math"

	Public Enum AuditHistEvents
		Any
		InsertOrUpdate
		InsertOrDelete
		UpdateOrDelete
		Insert
		Update
		Delete
	End Enum

	Public Shared Function FmtRptDateDisplay(ByVal DateVal As Date) As String
		Return Format(DateVal, "MMMM d, yyyy").ToUpper
	End Function

	Public Shared Function FmtRptDateParam(ByVal DateVal As Date) As String
		Return Format(DateVal, "MM/dd/yyyy")
	End Function

	Public Shared Function StateRptSortGroup(ByVal StateDescrip As String) As Integer
		Select Case StateDescrip
			Case StateUnknownDescrip
				Return StateUnknownSort
			Case StateForeignCountryDescrip
				Return StateForeignCountrySort
			Case StateResUnreportedDescrip
				Return StateResUnreportedSort
			Case Else
				Return IIf(StateIsDomestic(StateDescrip), StateDomesticSort, StateNotDomesticSort)
		End Select
	End Function

	Public Shared Function StateIsDomestic(ByVal StateDescrip As String) As Boolean
		StateDescrip = StateDescrip.ToUpper
		Return Not (StateDescrip = "AMERICAN SAMOA" Or _
					StateDescrip = "FEDERATED STATES OF MICRONESIA" Or _
					StateDescrip = "GUAM" Or _
					StateDescrip = "MARSHALL ISLANDS" Or _
					StateDescrip = "NORTHERN MARIANAS" Or _
					StateDescrip = "PALAU" Or _
					StateDescrip = "PUERTO RICO" Or _
					StateDescrip = "VIRGIN ISLANDS")
	End Function

	Public Shared Function PutDBQuotes(ByVal Val As String) As String
		If Not Val.StartsWith("'") Then
			Val = "'" & Val
		End If

		If Not Val.EndsWith("'") Then
			Val &= "'"
		End If

		Return Val
	End Function

	Public Shared Function PutDBQuotesList(ByVal ValueList As String) As String
		With New System.Text.StringBuilder
			For Each Val As String In ValueList.Split(",")
				.Append(PutDBQuotes(Val) & ",")
			Next
			Return .ToString.TrimEnd(",".ToCharArray) ' get rid of extra "," at end
		End With
	End Function

End Class
