Public Class StudentAwardScheduleInfo

    Private _StudentAwardScheduleId As Guid
    Private _StudentAwardId As Guid
    Private _ExpectedDate As DateTime
    Private _PaymentPeriod As String
    Private _Amount As Double

    Public Property StudentAwardScheduleId() As Guid
        Get
            StudentAwardScheduleId = _StudentAwardScheduleId
        End Get
        Set(ByVal Value As Guid)
            _StudentAwardScheduleId = Value
        End Set
    End Property
    Public Property StudentAwardId() As Guid
        Get
            StudentAwardId = _StudentAwardId
        End Get
        Set(ByVal Value As Guid)
            _StudentAwardId = Value
        End Set
    End Property
    Public Property ExpectedDate() As DateTime
        Get
            ExpectedDate = _ExpectedDate
        End Get
        Set(ByVal Value As DateTime)
            _ExpectedDate = Value
        End Set
    End Property
    Public Property PaymentPeriod() As String
        Get
            PaymentPeriod = _PaymentPeriod
        End Get
        Set(ByVal Value As String)
            _PaymentPeriod = Value
        End Set
    End Property
    Public Property Amount() As Double
        Get
            Amount = _Amount
        End Get
        Set(ByVal Value As Double)
            _Amount = Value
        End Set
    End Property
    Public Sub New()
        MyBase.new()
    End Sub
    Public Sub New(ByVal NewStudentAwardScheduleId As Guid, ByVal NewStudentAwardId As Guid, _
    ByVal NewExpectedDate As DateTime, ByVal NewPaymentPeriod As String, ByVal NewAmount As Double)

        _StudentAwardScheduleId = NewStudentAwardScheduleId
        _StudentAwardId = NewStudentAwardId
        _ExpectedDate = NewExpectedDate
        _PaymentPeriod = NewPaymentPeriod
        _Amount = NewAmount
    End Sub
End Class
