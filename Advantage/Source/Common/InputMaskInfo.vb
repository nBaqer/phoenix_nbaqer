Public Class InputMaskInfo
    '
    '   InputMaskInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _InputMaskId As String
    Private _Item As String
    Private _Mask As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _InputMaskId = Guid.NewGuid.ToString
        _Item = ""
        _Mask = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property InputMaskId() As String
        Get
            Return _InputMaskId
        End Get
        Set(ByVal Value As String)
            _InputMaskId = Value
        End Set
    End Property
    Public Property Item() As String
        Get
            Return _Item
        End Get
        Set(ByVal Value As String)
            _Item = Value
        End Set
    End Property
    Public Property Mask() As String
        Get
            Return _Mask
        End Get
        Set(ByVal Value As String)
            _Mask = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
