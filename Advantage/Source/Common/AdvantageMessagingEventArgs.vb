Public Class AdvantageMessagingEventArgs
    Inherits EventArgs

    Public Sub New(ByVal advantageEvent As AdvantageEvent, ByVal campusId As String, ByVal sqlParameters As ArrayList)
        MyBase.New()
        EventId = advantageEvent
        Me.CampusId = campusId
        Me.SqlParameters = sqlParameters
    End Sub

    Public Property EventId As AdvantageEvent
    Public Property CampusId As String
    Public Property SqlParameters As ArrayList

End Class