﻿Public Class PostLeadPaymentInfo
    '
    '   PostLeadPaymentInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _leadPaymentId As String
    Private _leadTransactionId As String
    Private _paymentTransactionId As String
    Private _leadId As String
    Private _leadName As String
    Private _transCodeId As String
    Private _transCodeDescrip As String
    Private _leadTransactionDate As Date
    Private _amount As Decimal
    Private _reference As String
    Private _descrip As String
    Private _transTypeId As Integer
    Private _transTypeDescrip As String
    Private _isEnrolled As Boolean
    Private _createdDate As DateTime
    Private _isVoided As Boolean
    Private _paymentTypeId As String
    Private _paymentTypeDescrip As String
    Private _checkNumber As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _campusId As String
    Private _paymentReference As Integer
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _leadPaymentId = Guid.NewGuid.ToString
        _leadTransactionId = Guid.NewGuid.ToString
        _paymentTransactionId = Guid.NewGuid.ToString
        _leadId = Guid.Empty.ToString
        _leadName = ""
        _transCodeId = Guid.Empty.ToString
        _transCodeDescrip = ""
        _leadTransactionDate = Date.Now.ToString("d")
        _amount = 0.0
        _reference = ""
        _descrip = ""
        _transTypeId = 0
        _transTypeDescrip = ""
        _isEnrolled = False
        _createdDate = DateTime.MinValue
        _isVoided = False
        _paymentTypeId = 0
        _checkNumber = ""
        _modUser = ""
        _modDate = Date.MinValue
        _campusId = Guid.Empty.ToString
        _paymentReference = 0

    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property LeadTransactionId() As String
        Get
            Return _leadTransactionId
        End Get
        Set(ByVal Value As String)
            _leadTransactionId = Value
        End Set
    End Property
    Public Property LeadPaymentId() As String
        Get
            Return _leadPaymentId
        End Get
        Set(ByVal Value As String)
            _leadPaymentId = Value
        End Set
    End Property
    Public Property PaymentTransactionId() As String
        Get
            Return _paymentTransactionId
        End Get
        Set(ByVal Value As String)
            _paymentTransactionId = Value
        End Set
    End Property
    Public Property LeadId() As String
        Get
            Return _leadId
        End Get
        Set(ByVal Value As String)
            _leadId = Value
        End Set
    End Property
    Public Property LeadName() As String
        Get
            Return _leadName
        End Get
        Set(ByVal Value As String)
            _leadName = Value
        End Set
    End Property
    Public Property TransCodeId() As String
        Get
            Return _transCodeId
        End Get
        Set(ByVal Value As String)
            _transCodeId = Value
        End Set
    End Property
    Public Property TransCodeDescription() As String
        Get
            Return _transCodeDescrip
        End Get
        Set(ByVal Value As String)
            _transCodeDescrip = Value
        End Set
    End Property
    Public Property LeadTransactionDate() As Date
        Get
            Return _leadTransactionDate
        End Get
        Set(ByVal Value As Date)
            _leadTransactionDate = Value
        End Set
    End Property
    Public Property Amount() As Decimal
        Get
            Return _amount
        End Get
        Set(ByVal Value As Decimal)
            _amount = Value
        End Set
    End Property
    Public Property Reference() As String
        Get
            Return _reference
        End Get
        Set(ByVal Value As String)
            _reference = Value
        End Set
    End Property
    Public Property Descrip() As String
        Get
            Return _descrip
        End Get
        Set(ByVal Value As String)
            _descrip = Value
        End Set
    End Property
    Public Property TransTypeId() As Int16
        Get
            Return _transTypeId
        End Get
        Set(ByVal Value As Int16)
            _transTypeId = Value
        End Set
    End Property
    Public ReadOnly Property TransTypeDescrip() As String
        Get
            If _transTypeId = 0 Then
                Return "Charge"
            ElseIf _transTypeId = 2 Then
                Return "Payment"
            Else
                Return 0
            End If
        End Get
    End Property
    Public Property IsEnrolled() As Boolean
        Get
            Return _isEnrolled
        End Get
        Set(ByVal Value As Boolean)
            _isEnrolled = Value
        End Set
    End Property
    Public Property CreatedDate() As Date
        Get
            Return _createdDate
        End Get
        Set(ByVal Value As Date)
            _createdDate = Value
        End Set
    End Property
    Public Property IsVoided() As Boolean
        Get
            Return _isVoided
        End Get
        Set(ByVal Value As Boolean)
            _isVoided = Value
        End Set
    End Property
    Public Property PaymentTypeId() As String
        Get
            Return _paymentTypeId
        End Get
        Set(ByVal Value As String)
            _paymentTypeId = Value
        End Set
    End Property
    Public Property CheckNumber() As String
        Get
            Return _checkNumber
        End Get
        Set(ByVal Value As String)
            _checkNumber = Value
        End Set
    End Property
    Public ReadOnly Property UniqueId() As String
        Get
            Return Integer.Parse(_leadPaymentId.Substring(0, 6), System.Globalization.NumberStyles.HexNumber).ToString()
        End Get
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property CampusId() As String
        Get
            Return _campusId
        End Get
        Set(ByVal Value As String)
            _campusId = Value
        End Set
    End Property
    Public Property PaymentReference() As Integer
        Get
            Return _paymentReference
        End Get
        Set(ByVal Value As Integer)
            _paymentReference = Value
        End Set
    End Property
#End Region
End Class