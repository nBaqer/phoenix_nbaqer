Public Class QuickLinkInfo
    '
    '   QuickLinkInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _QuickLinkID As String
    Private _ModuleID As Integer
    Private _statusId As String
    Private _status As String
    Private _Description As String
    Private _URL As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _QuickLinkId = Guid.NewGuid.ToString
        _statusId = Guid.Empty.ToString
        _status = ""
        _Description = ""
        _URL = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property QuickLinkID() As String
        Get
            Return _QuickLinkID
        End Get
        Set(ByVal Value As String)
            _QuickLinkID = Value
        End Set
    End Property
    Public Property ModuleID() As Integer
        Get
            Return _ModuleID
        End Get
        Set(ByVal Value As Integer)
            _ModuleID = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property
    Public Property URL() As String
        Get
            Return _URL
        End Get
        Set(ByVal Value As String)
            _URL = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class