Public Class StudentContactInfo
    '
    '   StudentContactInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _studentContactId As String
    Private _statusId As String
    Private _status As String
    Private _studentId As String
    Private _relationId As String
    Private _relation As String
    Private _firstName As String
    Private _MI As String
    Private _lastName As String
    Private _preffixId As String
    Private _preffix As String
    Private _suffixId As String
    Private _suffix As String
    Private _comments As String
    Private _email As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _studentContactId = Guid.NewGuid.ToString
        _statusId = FAME.AdvantageV1.Common.AdvantageCommonValues.ActiveGuid
        _status = "Active"
        _studentId = Guid.Empty.ToString
        _relationId = Guid.Empty.ToString
        _relation = ""
        _firstName = ""
        _MI = ""
        _lastName = ""
        _preffixId = Guid.Empty.ToString
        _preffix = ""
        _suffixId = Guid.Empty.ToString
        _suffix = ""
        _comments = ""
        _email = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
    Public Sub New(ByVal studentId As String)
        _isInDB = False
        _studentContactId = Guid.NewGuid.ToString
        _statusId = FAME.AdvantageV1.Common.AdvantageCommonValues.ActiveGuid
        _status = "Active"
        _studentId = studentId
        _relationId = Guid.Empty.ToString
        _relation = ""
        _firstName = ""
        _MI = ""
        _lastName = ""
        _preffixId = Guid.Empty.ToString
        _preffix = ""
        _suffixId = Guid.Empty.ToString
        _suffix = ""
        _comments = ""
        _email = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property StudentContactId() As String
        Get
            Return _studentContactId
        End Get
        Set(ByVal Value As String)
            _studentContactId = Value
        End Set
    End Property
    Public Property StudentId() As String
        Get
            Return _studentId
        End Get
        Set(ByVal Value As String)
            _studentId = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property RelationId() As String
        Get
            Return _relationId
        End Get
        Set(ByVal Value As String)
            _relationId = Value
        End Set
    End Property
    Public Property Relation() As String
        Get
            Return _relation
        End Get
        Set(ByVal Value As String)
            _relation = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return _lastName
        End Get
        Set(ByVal Value As String)
            _lastName = Value
        End Set
    End Property
    Public Property FirstName() As String
        Get
            Return _firstName
        End Get
        Set(ByVal Value As String)
            _firstName = Value
        End Set
    End Property
    Public Property MI() As String
        Get
            Return _MI
        End Get
        Set(ByVal Value As String)
            _MI = Value
        End Set
    End Property
    Public Property PrefixId() As String
        Get
            Return _preffixId
        End Get
        Set(ByVal Value As String)
            _preffixId = Value
        End Set
    End Property
    Public Property Prefix() As String
        Get
            Return _preffix
        End Get
        Set(ByVal Value As String)
            _preffix = Value
        End Set
    End Property
    Public Property SuffixId() As String
        Get
            Return _suffixId
        End Get
        Set(ByVal Value As String)
            _suffixId = Value
        End Set
    End Property
    Public Property Suffix() As String
        Get
            Return _suffix
        End Get
        Set(ByVal Value As String)
            _suffix = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Email = _email
        End Get
        Set(ByVal Value As String)
            _email = Value
        End Set
    End Property
    Public Property Comments() As String
        Get
            Comments = _comments
        End Get
        Set(ByVal Value As String)
            _comments = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class

Public Class StudentContactAddressInfo
    '
    '   StudentContactAddressInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _studentContactAddressId As String
    Private _statusId As String
    Private _status As String
    Private _studentContactId As String
    Private _addrTypId As String
    Private _address1 As String
    Private _address2 As String
    Private _city As String
    Private _stateId As String
    Private _countryId As String
    Private _zip As String
    Private _otherState As String
    Private _foreignZip As Boolean
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _studentContactAddressId = Guid.NewGuid.ToString
        _statusId = FAME.AdvantageV1.Common.AdvantageCommonValues.ActiveGuid
        _studentContactId = Guid.Empty.ToString
        _addrTypId = Guid.Empty.ToString
        _address1 = ""
        _address2 = ""
        _city = ""
        _stateId = Guid.Empty.ToString
        _countryId = AdvantageCommonValues.USAGuid.ToString
        _zip = ""
        _otherState = ""
        _foreignZip = False
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
    Public Sub New(ByVal studentContactId As String)
        _isInDB = False
        _studentContactAddressId = Guid.NewGuid.ToString
        _statusId = FAME.AdvantageV1.Common.AdvantageCommonValues.ActiveGuid
        _studentContactId = studentContactId
        _addrTypId = Guid.Empty.ToString
        _address1 = ""
        _address2 = ""
        _city = ""
        _stateId = Guid.Empty.ToString
        _countryId = AdvantageCommonValues.USAGuid.ToString
        _zip = ""
        _otherState = ""
        _foreignZip = False
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property StudentContactAddressId() As String
        Get
            Return _studentContactAddressId
        End Get
        Set(ByVal Value As String)
            _studentContactAddressId = Value
        End Set
    End Property
    Public Property StudentContactId() As String
        Get
            Return _studentContactId
        End Get
        Set(ByVal Value As String)
            _studentContactId = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property AddrTypId() As String
        Get
            Return _addrTypId
        End Get
        Set(ByVal Value As String)
            _addrTypId = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _city
        End Get
        Set(ByVal Value As String)
            _city = Value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return _address1
        End Get
        Set(ByVal Value As String)
            _address1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return _address2
        End Get
        Set(ByVal Value As String)
            _address2 = Value
        End Set
    End Property
    Public Property StateId() As String
        Get
            Return _stateId
        End Get
        Set(ByVal Value As String)
            _stateId = Value
        End Set
    End Property
    Public Property CountryId() As String
        Get
            Return _countryId
        End Get
        Set(ByVal Value As String)
            _countryId = Value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return _zip
        End Get
        Set(ByVal Value As String)
            _zip = Value
        End Set
    End Property
    Public Property OtherState() As String
        Get
            Return _otherState
        End Get
        Set(ByVal Value As String)
            _otherState = Value
        End Set
    End Property
    Public Property ForeignZip() As Integer
        Get
            Return _foreignZip
        End Get
        Set(ByVal Value As Integer)
            _foreignZip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Class StudentContactPhoneInfo
    '
    '   StudentContactPhoneInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _studentContactPhoneId As String
    Private _statusId As String
    Private _status As String
    Private _studentContactId As String
    Private _phoneTypeId As String
    Private _phoneType As String
    Private _phone As String
    Private _ext As String
    Private _bestTime As String
    Private _foreignPhone As Boolean
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _studentContactPhoneId = Guid.NewGuid.ToString
        _statusId = FAME.AdvantageV1.Common.AdvantageCommonValues.ActiveGuid
        _studentContactId = Guid.Empty.ToString
        _phoneTypeId = Guid.Empty.ToString
        _phoneType = ""
        _phone = ""
        _ext = ""
        _bestTime = ""
        _foreignPhone = False
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
    Public Sub New(ByVal studentContactId As String)
        _isInDB = False
        _studentContactPhoneId = Guid.NewGuid.ToString
        _statusId = FAME.AdvantageV1.Common.AdvantageCommonValues.ActiveGuid
        _studentContactId = studentContactId
        _phoneTypeId = Guid.Empty.ToString
        _phoneType = ""
        _phone = ""
        _ext = ""
        _bestTime = ""
        _foreignPhone = False
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property StudentContactPhoneId() As String
        Get
            Return _studentContactPhoneId
        End Get
        Set(ByVal Value As String)
            _studentContactPhoneId = Value
        End Set
    End Property
    Public Property StudentContactId() As String
        Get
            Return _studentContactId
        End Get
        Set(ByVal Value As String)
            _studentContactId = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property PhoneTypeId() As String
        Get
            Return _phoneTypeId
        End Get
        Set(ByVal Value As String)
            _phoneTypeId = Value
        End Set
    End Property
    Public Property PhoneType() As String
        Get
            Return _phoneType
        End Get
        Set(ByVal Value As String)
            _phoneType = Value
        End Set
    End Property
    Public Property BestTime() As String
        Get
            Return _bestTime
        End Get
        Set(ByVal Value As String)
            _bestTime = Value
        End Set
    End Property
    Public Property Phone() As String
        Get
            Return _phone
        End Get
        Set(ByVal Value As String)
            _phone = Value
        End Set
    End Property
    Public Property Ext() As String
        Get
            Return _ext
        End Get
        Set(ByVal Value As String)
            _ext = Value
        End Set
    End Property
    Public Property ForeignPhone() As Boolean
        Get
            Return _foreignPhone
        End Get
        Set(ByVal Value As Boolean)
            _foreignPhone = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class


