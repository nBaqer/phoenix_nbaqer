Public Class StudentProgressInfo
#Region "Variables"
    Private _GPA As Decimal
    Private _CredsEarned As Integer
    Private _CredsAttmptd As Integer

#End Region

#Region "Property"
    Public Property GPA() As Decimal
        Get
            Return _GPA
        End Get
        Set(ByVal Value As Decimal)
            _GPA = Value
        End Set
    End Property

    Public Property CredsEarned() As Integer
        Get
            Return _CredsEarned
        End Get
        Set(ByVal Value As Integer)
            _CredsEarned = Value
        End Set
    End Property
    Public Property CredsAttmptd() As Integer
        Get
            Return _CredsAttmptd
        End Get
        Set(ByVal Value As Integer)
            _CredsAttmptd = Value
        End Set
    End Property
#End Region
End Class
