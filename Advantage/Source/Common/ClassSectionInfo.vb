Public Class ClassSectionInfo

    ' 08/22/2011 JRobinson Clock Hour Project - Add new fields from the ClsSectionTimeClockPolicy table

#Region "Private Variable Declaration"

    Private _ClsSectId As Guid
    Private _TermId As Guid
    Private _Term As String
    Private _CourseId As Guid
    Private _Course As String
    Private _CampusId As Guid
    Private _Campus As String
    Private _InstructorId As Guid
    Private _InstructorId2 As String
    Private _Instructor As String
    Private _ShiftId As Guid
    Private _ShiftId2 As String
    Private _Shift As String
    Private _Section As String
    Private _StartDate As Date
    Private _EndDate As Date
    Private _MaxStud As Integer
    Private _GrdScaleId As Guid
    Private _GrdScale As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _fetchedFromDB As Boolean
    Private _allowOverrideConflicts As Boolean
    Private _originalTermId As String
    Private _StudentStartDate As DateTime
    Private _LeadGrpId As Guid
    Private _LeadGrp As String
    Private _CohortStartDate As DateTime
    Private _InstrGrdBkWgtId As Guid
    Private _AllowEarlyIn As Boolean
    Private _AllowLateOut As Boolean
    Private _AllowExtraHours As Boolean
    Private _CheckTardyIn As Boolean
    Private _MaxInBeforeTardy As DateTime
    Private _AssignTardyInTime As DateTime


#End Region
#Region " Public Constructor"
    Public Sub New()

        _modUser = ""
        _modDate = Date.MinValue
        _fetchedFromDB = False
        _allowOverrideConflicts = False
        _originalTermId = System.Guid.Empty.ToString
        _ShiftId2 = ""
        _InstructorId2 = ""
        _Instructor = ""
        _Shift = ""
        _GrdScale = ""
        _Campus = ""
        _AllowEarlyIn = False
        _AllowLateOut = False
        _AllowExtraHours = False
        _CheckTardyIn = False
        _MaxInBeforeTardy = Date.MinValue
        _AssignTardyInTime = Date.MinValue
        '_StudentStartDate = ""
    End Sub
#End Region

#Region "Class Section Properties"

    Public Property ClsSectId() As Guid
        Get
            ClsSectId = _ClsSectId
        End Get
        Set(ByVal Value As Guid)
            _ClsSectId = Value
        End Set
    End Property
    Public Property TermId() As Guid
        Get
            TermId = _TermId
        End Get
        Set(ByVal Value As Guid)
            _TermId = Value
        End Set
    End Property
    Public Property CourseId() As Guid
        Get
            CourseId = _CourseId
        End Get
        Set(ByVal Value As Guid)
            _CourseId = Value
        End Set
    End Property
    Public Property CampusId() As Guid
        Get
            CampusId = _CampusId
        End Get
        Set(ByVal Value As Guid)
            _CampusId = Value
        End Set
    End Property
    Public Property InstructorId() As Guid
        Get
            InstructorId = _InstructorId
        End Get
        Set(ByVal Value As Guid)
            _InstructorId = Value
        End Set
    End Property
    Public Property ShiftId() As Guid
        Get
            ShiftId = _ShiftId
        End Get
        Set(ByVal Value As Guid)
            _ShiftId = Value
        End Set
    End Property
    Public Property ShiftId2() As String
        Get
            ShiftId2 = _ShiftId2
        End Get
        Set(ByVal Value As String)
            _ShiftId2 = Value
        End Set
    End Property
    Public Property InstructorId2() As String
        Get
            InstructorId2 = _InstructorId2
        End Get
        Set(ByVal Value As String)
            _InstructorId2 = Value
        End Set
    End Property
    Public Property GrdScaleId() As Guid
        Get
            GrdScaleId = _GrdScaleId
        End Get
        Set(ByVal Value As Guid)
            _GrdScaleId = Value
        End Set
    End Property
    Public Property Section() As String
        Get
            Section = _Section
        End Get
        Set(ByVal Value As String)
            _Section = Value
        End Set
    End Property
    Public Property StartDate() As Date
        Get
            StartDate = _StartDate
        End Get
        Set(ByVal Value As Date)
            _StartDate = Value
        End Set
    End Property
    Public Property EndDate() As Date
        Get
            EndDate = _EndDate
        End Get
        Set(ByVal Value As Date)
            _EndDate = Value
        End Set
    End Property
    Public Property StudentStartDate() As Date
        Get
            StudentStartDate = _StudentStartDate
        End Get
        Set(ByVal Value As Date)
            _StudentStartDate = Value
        End Set
    End Property
    Public Property MaxStud() As Integer
        Get
            MaxStud = _MaxStud
        End Get
        Set(ByVal Value As Integer)
            _MaxStud = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property fetchedFromDB() As Boolean
        Get
            Return _fetchedFromDB
        End Get
        Set(ByVal Value As Boolean)
            _fetchedFromDB = Value
        End Set
    End Property
    Public Property AllowOverrideConflicts() As Boolean
        Get
            Return _allowOverrideConflicts
        End Get
        Set(ByVal Value As Boolean)
            _allowOverrideConflicts = Value
        End Set
    End Property
    Public Property OriginalTermId() As String
        Get
            OriginalTermId = _originalTermId
        End Get
        Set(ByVal Value As String)
            _originalTermId = Value
        End Set
    End Property
    Public Property Term() As String
        Get
            Term = _Term
        End Get
        Set(ByVal Value As String)
            _Term = Value
        End Set
    End Property
    Public Property Course() As String
        Get
            Course = _Course
        End Get
        Set(ByVal Value As String)
            _Course = Value
        End Set
    End Property
    Public Property Instructor() As String
        Get
            Instructor = _Instructor
        End Get
        Set(ByVal Value As String)
            _Instructor = Value
        End Set
    End Property
    Public Property Shift() As String
        Get
            Shift = _Shift
        End Get
        Set(ByVal Value As String)
            _Shift = Value
        End Set
    End Property
    Public Property GrdScale() As String
        Get
            GrdScale = _GrdScale
        End Get
        Set(ByVal Value As String)
            _GrdScale = Value
        End Set
    End Property
    Public Property Campus() As String
        Get
            Campus = _Campus
        End Get
        Set(ByVal Value As String)
            _Campus = Value
        End Set
    End Property

    Public Property LeadGrpId() As Guid
        Get
            LeadGrpId = _LeadGrpId
        End Get
        Set(ByVal Value As Guid)
            _LeadGrpId = Value
        End Set
    End Property
    Public Property LeadGrp() As String
        Get
            LeadGrp = _LeadGrp
        End Get
        Set(ByVal Value As String)
            _LeadGrp = Value
        End Set
    End Property
    Public Property CohortStartDate() As Date
        Get
            Return _CohortStartDate
        End Get
        Set(ByVal value As Date)
            _CohortStartDate = value
        End Set
    End Property
    Public Property InstrGrdBkWgtId() As Guid
        Get
            Return _InstrGrdBkWgtId
        End Get
        Set(ByVal Value As Guid)
            _InstrGrdBkWgtId = Value
        End Set
    End Property
    Public Property AllowEarlyIn() As Boolean
        Get
            Return _AllowEarlyIn
        End Get
        Set(ByVal Value As Boolean)
            _AllowEarlyIn = Value
        End Set
    End Property
    Public Property AllowLateOut() As Boolean
        Get
            Return _AllowLateOut
        End Get
        Set(ByVal Value As Boolean)
            _AllowLateOut = Value
        End Set
    End Property
    Public Property AllowExtraHours() As Boolean
        Get
            Return _AllowExtraHours
        End Get
        Set(ByVal Value As Boolean)
            _AllowExtraHours = Value
        End Set
    End Property
    Public Property CheckTardyIn() As Boolean
        Get
            Return _CheckTardyIn
        End Get
        Set(ByVal Value As Boolean)
            _CheckTardyIn = Value
        End Set
    End Property
    Public Property MaxInBeforeTardy() As Date
        Get
            Return _MaxInBeforeTardy
        End Get
        Set(ByVal value As Date)
            _MaxInBeforeTardy = value
        End Set
    End Property
    Public Property AssignTardyInTime() As Date
        Get
            Return _AssignTardyInTime
        End Get
        Set(ByVal value As Date)
            _AssignTardyInTime = value
        End Set
    End Property
#End Region

End Class




Public Class ClsSectionMeetingScheduleInfo
#Region "ClsSectionMeetingScheduleInfo"
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _ClsSectMeetingID As String
    Private _ClsSectionID As String
    Private _code As String
    Private _descrip As String
    Protected _ClassStartDate As DateTime
    Protected _ClassEndDate As DateTime
    '  Private _academicCalendarType As AR.AcademicCalendarTypes
    Private _unitType As String

    'Private _active As Boolean
    'Private _modUser As String
    'Private _modDate As DateTime
    Private _details() As ClsSectMeetingScheduledetailsInfo
    Private _detailsDT As DataTable
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _ClsSectMeetingID = Guid.NewGuid.ToString
        '  _academicCalendarType = AR.AcademicCalendarTypes.Credit ' default to credit school
        _unitType = Guid.NewGuid.ToString
        ' _active = True
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property ClsSectMeetingID() As String
        Get
            Return _ClsSectMeetingID
        End Get
        Set(ByVal Value As String)
            _ClsSectMeetingID = Value
        End Set
    End Property

    Public Property ClsSectionID() As String
        Get
            Return _ClsSectionID
        End Get
        Set(ByVal Value As String)
            _ClsSectionID = Value
        End Set
    End Property
  
  

    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property Descrip() As String
        Get
            Return _descrip
        End Get
        Set(ByVal Value As String)
            _descrip = Value
        End Set
    End Property
    'Public Property AcademicCalendarType() As AR.AcademicCalendarTypes
    '    Get
    '        Return _academicCalendarType
    '    End Get
    '    Set(ByVal Value As AR.AcademicCalendarTypes)
    '        _academicCalendarType = Value
    '    End Set
    'End Property
    'Public Property UseTimeClock() As Boolean
    '    Get
    '        Return _useTimeClock
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        _useTimeClock = Value
    '    End Set
    'End Property
   

    'Public Property Active() As Boolean
    '    Get
    '        Return _active
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        _active = Value
    '    End Set
    'End Property
    Public Property UnitType() As String
        Get
            Return _unitType
        End Get
        Set(ByVal Value As String)
            _unitType = Value
        End Set
    End Property
    'Public Property ModDate() As DateTime
    '    Get
    '        Return _modDate
    '    End Get
    '    Set(ByVal Value As Date)
    '        _modDate = Value
    '    End Set
    'End Property
    Public Property Details() As ClsSectMeetingScheduledetailsInfo()
        Get
            Return _details
        End Get
        Set(ByVal Value As ClsSectMeetingScheduledetailsInfo())
            _details = Value
        End Set
    End Property
    Public Property ClassStartDate() As DateTime
        Get
            Return _ClassStartDate
        End Get
        Set(ByVal Value As DateTime)
            _ClassStartDate = Value
        End Set
    End Property
    Public Property ClassEndDate() As DateTime
        Get
            Return _ClassEndDate
        End Get
        Set(ByVal Value As DateTime)
            _ClassEndDate = Value
        End Set
    End Property
    Public Property DetailsDT() As DataTable
        Get
            Return _detailsDT
        End Get
        Set(ByVal Value As DataTable)
            _detailsDT = Value
        End Set
    End Property
#End Region
#End Region
End Class


#Region " Cls Section Schedule details Info"



Public Class ClsSectMeetingScheduledetailsInfo
#Region " Private Variables and Objects"

    Protected _ClsSectMeetingID As String
   
    Protected _dw As Integer
    Protected _dwDescrip As String
    Protected _total As Decimal
    Protected _timein As DateTime
    Protected _timeout As DateTime
    Protected _allow_earlyin As Boolean
    Protected _allow_lateout As Boolean
    Protected _allow_extrahours As Boolean
    Protected _check_tardyin As Boolean
    Protected _max_beforetardy As DateTime
    Protected _tardy_intime As DateTime
    Protected _useTimeClock As Boolean
#End Region
#Region "Public Constructors "
    Public Sub New()
        _dw = 0
        _total = 0
        _timein = DateTime.MinValue
        _timeout = DateTime.MinValue
        _allow_earlyin = False
        _allow_lateout = False
        _allow_extrahours = False
        _check_tardyin = False
        _max_beforetardy = DateTime.MinValue
        _tardy_intime = DateTime.MinValue
        _useTimeClock = False
    End Sub
#End Region
#Region " Public Properties"

    Public Property ClsSectMeetingID() As String
        Get
            Return _ClsSectMeetingID
        End Get
        Set(ByVal Value As String)
            _ClsSectMeetingID = Value
        End Set
    End Property
   
    Public Property dw() As Integer
        Get
            Return _dw
        End Get
        Set(ByVal Value As Integer)
            _dw = Value
        End Set
    End Property
    Public Property dwDescrip() As String
        Get
            Return _dwDescrip
        End Get
        Set(ByVal Value As String)
            _dwDescrip = Value
        End Set
    End Property
    Public Property Total() As Decimal
        Get
            Return _total
        End Get
        Set(ByVal Value As Decimal)
            _total = Value
        End Set
    End Property
    Public ReadOnly Property CalculatedTotal() As Decimal
        Get
            Dim tot As Long = 0.0
            Dim tot_lunch As Long = 0.0
            If TimeIn <> DateTime.MinValue AndAlso TimeOut <> DateTime.MinValue AndAlso TimeOut >= TimeIn Then
                tot = DateDiff(DateInterval.Minute, TimeIn, TimeOut)
            End If


            Return tot
        End Get
    End Property
    Public Property TimeIn() As DateTime
        Get
            Return _timein
        End Get
        Set(ByVal Value As DateTime)
            _timein = Value
        End Set
    End Property

    Public Property TimeOut() As DateTime
        Get
            Return _timeout
        End Get
        Set(ByVal Value As DateTime)
            _timeout = Value
        End Set
    End Property
    Public Property Allow_EarlyIn() As Boolean
        Get
            Return _allow_earlyin
        End Get
        Set(ByVal Value As Boolean)
            _allow_earlyin = Value
        End Set
    End Property
    Public Property Allow_LateOut() As Boolean
        Get
            Return _allow_lateout
        End Get
        Set(ByVal Value As Boolean)
            _allow_lateout = Value
        End Set
    End Property
    Public Property Allow_ExtraHours() As Boolean
        Get
            Return _allow_extrahours
        End Get
        Set(ByVal Value As Boolean)
            _allow_extrahours = Value
        End Set
    End Property
    Public Property Check_TardyIn() As Boolean
        Get
            Return _check_tardyin
        End Get
        Set(ByVal Value As Boolean)
            _check_tardyin = Value
        End Set
    End Property
    Public Property MaxBeforeTardy() As DateTime
        Get
            Return _max_beforetardy
        End Get
        Set(ByVal Value As DateTime)
            _max_beforetardy = Value
        End Set
    End Property
    Public Property TardyInTime() As DateTime
        Get
            Return _tardy_intime
        End Get
        Set(ByVal Value As DateTime)
            _tardy_intime = Value
        End Set
    End Property
#End Region
End Class
#End Region


