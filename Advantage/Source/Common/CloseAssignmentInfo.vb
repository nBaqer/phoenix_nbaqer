Public Class CloseAssignmentInfo
#Region "Private Variable Declaration"

    Private _ActivityAssignmentId As Guid
    Private _Comments As String
    Private _DateCompleted As DateTime
    Private _ActivityResult As Guid


#End Region

#Region "Activity Assignment Properties"

    Public Property ActivityAssignmentId() As Guid
        Get
            ActivityAssignmentId = _ActivityAssignmentId
        End Get
        Set(ByVal Value As Guid)
            _ActivityAssignmentId = Value
        End Set
    End Property
    Public Property Comments() As String
        Get
            Comments = _Comments
        End Get
        Set(ByVal Value As String)
            _Comments = Value
        End Set
    End Property
    Public Property DateCompleted() As DateTime
        Get
            DateCompleted = _DateCompleted
        End Get
        Set(ByVal Value As DateTime)
            _DateCompleted = Value
        End Set
    End Property
    Public Property ActivityResult() As Guid
        Get
            ActivityResult = _ActivityResult
        End Get
        Set(ByVal Value As Guid)
            _ActivityResult = Value
        End Set
    End Property
#End Region

#Region "New Instance of the Class Declaration"
    Public Sub New(ByVal NewActivityAssignmentId As Guid, ByVal NewComments As String, ByVal NewDateCompleted As DateTime, ByVal NewActivityResult As Guid)

        _ActivityAssignmentId = NewActivityAssignmentId
        _Comments = NewComments
        _DateCompleted = NewDateCompleted
        _ActivityResult = NewActivityResult
    End Sub

    Public Sub New()
        MyBase.new()
    End Sub

#End Region
End Class
