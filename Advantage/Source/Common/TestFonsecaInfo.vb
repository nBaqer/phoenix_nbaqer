Public Class TestFonsecaInfo
    '
    '   TestFonsecaInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _TestFonsecaId As String
    Private _code As String
    Private _statusId As String
    Private _description As String
    Private _campGrpId As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _TestFonsecaId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _campGrpId = Guid.Empty.ToString
        _description = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property TestFonsecaId() As String
        Get
            Return _TestFonsecaId
        End Get
        Set(ByVal Value As String)
            _TestFonsecaId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class


