﻿Public Class FASAPInfo
#Region "Private Variable Declarations"
    Private _SAPDetailId As Guid
    Private _SAPId As Guid
    Private _Seq As Integer
    Private _TrigValue As Integer
    Private _TrigUnit As String
    Private _TrigOffsetTyp As String
    Private _TrigOffsetSeq As Integer
    Private _TrigDescrip As String
    Private _Consequence As String
    Private _QualMinValue As Decimal
    Private _QuantMinValue As Decimal
    Private _MinAttendanceValue As Decimal
    Private _QualType As String
    Private _QuantMinUnitType As String
    Private _MinCredsCompltd As Decimal
    Private _Term As String
    Private _TermSDate As Date
    Private _TermEDate As Date
    Private _TrigUnitTypId As Integer
    Private _TrigOffsetTypId As Integer
    Private _QuantMinUnitTypId As Integer
    Private _QualMinTypId As Integer
    Private _ConsequenceTypId As Integer
    Private _validationSequence As String
    Private _IsSAPPolicyUsed As Boolean
    Private _Descrip As String
    Private _Code As String
    Private _StatusId As Guid
    Private _CampGrpId As String
    Private _OffsetDate As Date
    Private _ModDate As DateTime
    Private _ModUser As String
    Private _CredsAttempted As Decimal
    Private _CredsEarned As Decimal
    Private _FinCredsEarned As Decimal
    Private _GPA As Decimal
    Private _PercCredsEarned As Decimal
    Private _TerminateProbCnt As Integer
    Private _Period As Integer
    Private _MinTermGPA As Decimal
    Private _TermGPAOver As Decimal
    Private _TermGPA As Decimal
    Private _SchedHours As Decimal
    Private _ActHours As Decimal
    Private _ActHoursAdjusted As Decimal
    Private _OverallAverage As Decimal
    Private _TrackExternAttendance As Boolean
    Private _TrackIfStudentPassedSAPSpeedSkills As Boolean
    Private _Accuracy As Decimal
    Private _attType As String
    Private _FaSapPolicy As Integer = 0 'The Property return 0 or 1
    'Public _QuantMinValueByInsTypes() As QuantMinValueByInsType
    ''Add SAP TransferHours
    ''Added by Saraswathi Lakshmanan On June 8 2010 fgfsdgsdgsdgsdhgsdfh

    Private _IncludeTransferHours As Boolean
    Private _OverallAttendance As Boolean

#End Region
#Region " Public Constructor"
    Public Sub New()
        _modUser = ""
        _ModDate = Date.MinValue
        _Accuracy = 0

    End Sub
#End Region
#Region "SAP Info Properties"


    'Public Property QuantMinValueByInsTypes() As QuantMinValueByInsType()
    '    Get
    '        Return _QuantMinValueByInsTypes
    '    End Get
    '    Set(ByVal Value As QuantMinValueByInsType())
    '        _QuantMinValueByInsTypes = Value
    '    End Set
    'End Property


    Private _QuantMinValueByInsTypes As New List(Of FAQuantMinValueByInsType)

    Public Sub AddQuantMinValueByInsType(newQuantMinValueByInsType As FAQuantMinValueByInsType)
        _QuantMinValueByInsTypes.Add(newQuantMinValueByInsType)
    End Sub
    Public Property QuantMinValueByInsTypes As IEnumerable(Of FAQuantMinValueByInsType)
        Get
            Return Me._QuantMinValueByInsTypes
        End Get
        Set(value As IEnumerable(Of FAQuantMinValueByInsType))
            _QuantMinValueByInsTypes = value
        End Set
    End Property


    Public Property SAPDetailId() As Guid
        Get
            Return _SAPDetailId
        End Get
        Set(ByVal Value As Guid)
            _SAPDetailId = Value
        End Set
    End Property
    Public Property Seq() As Integer
        Get
            Return _Seq
        End Get
        Set(ByVal Value As Integer)
            _Seq = Value
        End Set
    End Property
    Public Property FaSapPolicy() As Integer
        Get
            Return _FaSapPolicy
        End Get
        Set(ByVal Value As Integer)
            _FaSapPolicy = Value
        End Set
    End Property
    Public Property SAPId() As Guid
        Get
            Return _SAPId
        End Get
        Set(ByVal Value As Guid)
            _SAPId = Value
        End Set
    End Property
    Public Property TrigValue() As Integer
        Get
            Return _TrigValue
        End Get
        Set(ByVal Value As Integer)
            _TrigValue = Value
        End Set
    End Property
    Public Property TrigUnit() As String
        Get
            Return _TrigUnit
        End Get
        Set(ByVal Value As String)
            _TrigUnit = Value
        End Set
    End Property

    Public Property TrigOffsetTyp() As String
        Get
            Return _TrigOffsetTyp
        End Get
        Set(ByVal Value As String)
            _TrigOffsetTyp = Value
        End Set
    End Property

    Public Property TrigOffsetSeq() As Integer
        Get
            Return _TrigOffsetSeq
        End Get
        Set(ByVal Value As Integer)
            _TrigOffsetSeq = Value
        End Set
    End Property

    Public Property TrigDescrip() As String
        Get
            Return _TrigDescrip
        End Get
        Set(ByVal Value As String)
            _TrigDescrip = Value
        End Set
    End Property

    Public Property Consequence() As String
        Get
            Return _Consequence
        End Get
        Set(ByVal Value As String)
            _Consequence = Value
        End Set
    End Property

    Public Property MinAttendanceValue() As Decimal
        Get
            Return _MinAttendanceValue
        End Get
        Set(ByVal Value As Decimal)
            _MinAttendanceValue = Value
        End Set
    End Property
    Public Property QualMinValue() As Decimal
        Get
            Return _QualMinValue
        End Get
        Set(ByVal Value As Decimal)
            _QualMinValue = Value
        End Set
    End Property

    Public Property QuantMinValue() As Decimal
        Get
            Return _QuantMinValue
        End Get
        Set(ByVal Value As Decimal)
            _QuantMinValue = Value
        End Set
    End Property
    Public Property QualType() As String
        Get
            Return _QualType
        End Get
        Set(ByVal Value As String)
            _QualType = Value
        End Set
    End Property
    Public Property QuantMinUnitType() As String
        Get
            Return _QuantMinUnitType
        End Get
        Set(ByVal Value As String)
            _QuantMinUnitType = Value
        End Set
    End Property
    Public Property MinCredsCompltd() As Decimal
        Get
            Return _MinCredsCompltd
        End Get
        Set(ByVal Value As Decimal)
            _MinCredsCompltd = Value
        End Set
    End Property
    Public Property TermSDate() As Date
        Get
            Return _TermSDate
        End Get
        Set(ByVal Value As Date)
            _TermSDate = Value
        End Set
    End Property

    Public Property TermEDate() As Date
        Get
            Return _TermEDate
        End Get
        Set(ByVal Value As Date)
            _TermEDate = Value
        End Set
    End Property
    Public Property TrigUnitTypId() As Integer
        Get
            Return _TrigUnitTypId
        End Get
        Set(ByVal Value As Integer)
            _TrigUnitTypId = Value
        End Set
    End Property
    Public Property TrigOffsetTypId() As Integer
        Get
            Return _TrigOffsetTypId
        End Get
        Set(ByVal Value As Integer)
            _TrigOffsetTypId = Value
        End Set
    End Property
    Public Property QuantMinUnitTypId() As Integer
        Get
            Return _QuantMinUnitTypId
        End Get
        Set(ByVal Value As Integer)
            _QuantMinUnitTypId = Value
        End Set
    End Property
    Public Property QualMinTypId() As Integer
        Get
            Return _QualMinTypId
        End Get
        Set(ByVal Value As Integer)
            _QualMinTypId = Value
        End Set
    End Property
    Public Property ConsequenceTypId() As Integer
        Get
            Return _ConsequenceTypId
        End Get
        Set(ByVal Value As Integer)
            _ConsequenceTypId = Value
        End Set
    End Property
    Public Property ValidationSequence() As String
        Get
            Return _validationSequence
        End Get
        Set(ByVal Value As String)
            _validationSequence = Value
        End Set
    End Property
    Public Property IsSAPPolicyUsed() As Boolean
        Get
            Return _IsSAPPolicyUsed
        End Get
        Set(ByVal Value As Boolean)
            _IsSAPPolicyUsed = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _Code
        End Get
        Set(ByVal Value As String)
            _Code = Value
        End Set
    End Property
    Public Property Descrip() As String
        Get
            Return _Descrip
        End Get
        Set(ByVal Value As String)
            _Descrip = Value
        End Set
    End Property
    Public Property StatusId() As Guid
        Get
            Return _StatusId
        End Get
        Set(ByVal Value As Guid)
            _StatusId = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _CampGrpId
        End Get
        Set(ByVal Value As String)
            _CampGrpId = Value
        End Set
    End Property
    Public Property ModDate() As DateTime
        Get
            Return _ModDate
        End Get
        Set(ByVal Value As DateTime)
            _ModDate = Value
        End Set
    End Property

    Public Property ModUser() As String
        Get
            Return _ModUser
        End Get
        Set(ByVal Value As String)
            _ModUser = Value
        End Set
    End Property
    Public Property OffsetDate() As Date
        Get
            Return _OffsetDate
        End Get
        Set(ByVal Value As Date)
            _OffsetDate = Value
        End Set
    End Property
    Public Property CredsEarned() As Decimal
        Get
            Return _CredsEarned
        End Get
        Set(ByVal Value As Decimal)
            _CredsEarned = Value
        End Set
    End Property
    Public Property FinCredsEarned() As Decimal
        Get
            Return _FinCredsEarned
        End Get
        Set(ByVal Value As Decimal)
            _FinCredsEarned = Value
        End Set
    End Property

    Public Property GPA() As Decimal
        Get
            Return _GPA
        End Get
        Set(ByVal Value As Decimal)
            _GPA = Value
        End Set
    End Property

    Public Property CredsAttempted() As Decimal
        Get
            Return _CredsAttempted
        End Get
        Set(ByVal Value As Decimal)
            _CredsAttempted = Value
        End Set
    End Property
    Public Property PercCredsEarned() As Decimal
        Get
            Return _PercCredsEarned
        End Get
        Set(ByVal Value As Decimal)
            _PercCredsEarned = Value
        End Set
    End Property

    Public Property TerminateProbCnt() As Integer
        Get
            Return _TerminateProbCnt
        End Get
        Set(ByVal value As Integer)
            _TerminateProbCnt = value
        End Set
    End Property
    Public Property Period() As Integer
        Get
            Return _Period
        End Get
        Set(ByVal Value As Integer)
            _Period = Value
        End Set
    End Property
    Public Property MinTermGPA() As Decimal
        Get
            Return _MinTermGPA
        End Get
        Set(ByVal value As Decimal)
            _MinTermGPA = value
        End Set
    End Property
    Public Property TermGPAOver() As Decimal
        Get
            Return _TermGPAOver
        End Get
        Set(ByVal value As Decimal)
            _TermGPAOver = value
        End Set
    End Property
    Public Property Term() As String
        Get
            Return _Term
        End Get
        Set(ByVal value As String)
            _Term = value
        End Set
    End Property
    Public Property TermGPA() As Decimal
        Get
            Return _TermGPA
        End Get
        Set(ByVal value As Decimal)
            _TermGPA = value
        End Set
    End Property
    Public Property SchedHours() As Decimal
        Get
            Return _SchedHours
        End Get
        Set(ByVal Value As Decimal)
            _SchedHours = Value
        End Set
    End Property
    Public Property ActHours() As Decimal
        Get
            Return _ActHours
        End Get
        Set(ByVal Value As Decimal)
            _ActHours = Value
        End Set
    End Property
    Public Property ActHoursAdjusted() As Decimal
        Get
            Return _ActHoursAdjusted
        End Get
        Set(ByVal Value As Decimal)
            _ActHoursAdjusted = Value
        End Set
    End Property
    Public Property OverallAverage() As Decimal
        Get
            Return _OverallAverage
        End Get
        Set(ByVal Value As Decimal)
            _OverallAverage = Value
        End Set
    End Property
    Public Property TrackExternAttendance() As Boolean
        Get
            Return _TrackExternAttendance
        End Get
        Set(ByVal Value As Boolean)
            _TrackExternAttendance = Value
        End Set
    End Property
    Public Property TrackIfStudentPassedSAPSpeedSkills() As Boolean
        Get
            Return _TrackIfStudentPassedSAPSpeedSkills
        End Get
        Set(ByVal Value As Boolean)
            _TrackIfStudentPassedSAPSpeedSkills = Value
        End Set
    End Property
    Public Property Accuracy() As Decimal
        Get
            Return _Accuracy
        End Get
        Set(ByVal value As Decimal)
            _Accuracy = value
        End Set
    End Property
    Public Property IncludeTransferHours() As Boolean
        Get
            Return _IncludeTransferHours
        End Get
        Set(ByVal value As Boolean)
            _IncludeTransferHours = value
        End Set
    End Property
    Public Property attType() As String
        Get
            Return _attType
        End Get
        Set(ByVal Value As String)
            _attType = Value
        End Set
    End Property

    Public Property OverallAttendance() As Boolean
        Get
            Return _OverallAttendance
        End Get
        Set(ByVal Value As Boolean)
            _OverallAttendance = Value
        End Set
    End Property
#End Region

End Class
Public Class FAQuantMinValueByInsType
    Public SAPQuantInsTypeID As Guid
    Public QuantMinValue As Decimal
    Public SAPDetailId As Guid
    Public InstructionTypeId As Guid
    Public InstructionTypeDescrip As String
    Public moddate As DateTime
    Public attDetailsInfo As Common.AttendancePercentageInfo
    Public PercCompltd As Decimal
End Class
