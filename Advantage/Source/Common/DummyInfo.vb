Public Class DummyInfo
    Private _DummyId As Guid
    Private _Descrip As String
    Private _DummyCode As String
    Private _Status As Guid
    Private _StateId As Guid
    Private _CampGrpId As Guid
    Private _ModUser As String
    Private _ModDate As DateTime

    Public Property DummyId() As Guid
        Get
            DummyId = _DummyId
        End Get
        Set(ByVal Value As Guid)
            _DummyId = Value
        End Set
    End Property
    Public Property Descrip() As String
        Get
            Descrip = _Descrip
        End Get
        Set(ByVal Value As String)
            _Descrip = Value
        End Set
    End Property
    Public Property DummyCode() As String
        Get
            DummyCode = _DummyCode
        End Get
        Set(ByVal Value As String)
            _DummyCode = Value
        End Set
    End Property
    Public Property Status() As Guid
        Get
            Status = _Status
        End Get
        Set(ByVal Value As Guid)
            _Status = Value
        End Set
    End Property
    Public Property StateId() As Guid
        Get
            StateId = _StateId
        End Get
        Set(ByVal Value As Guid)
            _StateId = Value
        End Set
    End Property
    Public Property CampGrpId() As Guid
        Get
            CampGrpId = _CampGrpId
        End Get
        Set(ByVal Value As Guid)
            _CampGrpId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            ModUser = _ModUser
        End Get
        Set(ByVal Value As String)
            _ModUser = Value
        End Set
    End Property
    Public Property ModDate() As DateTime
        Get
            ModDate = _ModDate
        End Get
        Set(ByVal Value As DateTime)
            _ModDate = Value
        End Set
    End Property
End Class
