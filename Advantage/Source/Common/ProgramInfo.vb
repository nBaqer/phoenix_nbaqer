' ===============================================================================
' ProgramInfo.vb
' Info classes for Programs
' ===============================================================================
' Copyright (C) 2006, 2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Namespace AR

#Region "Academic Calendars"
    ''' <summary>
    ''' Represents possible values from syAcademicCalendars
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum AcademicCalendarTypes
        Credit = 0
        Credit_Non_Term = 1
        Clock = 2
    End Enum
#End Region

#Region "Unit Types"
    Public Enum UnitType
        CreditHour = 0
        ClockHour = 1
        Program = 2
    End Enum
#End Region

#Region "Attendance Unit Types"
    Public Class AttendanceUnitTypes
#Region "Properties"
        Public Shared ReadOnly Property PresentAbsentGuid() As String
            Get
                Return "ef5535c2-142c-4223-ae3c-25a50a153cc6"
            End Get
        End Property
        Public Shared ReadOnly Property MinutesGuid() As String
            Get
                Return "a1389c74-0bb9-4bbf-a47f-68428be7fa4d"
            End Get
        End Property
        Public Shared ReadOnly Property UseTimeClockGuid() As String
            Get
                Return "b937c92e-fd7a-455e-a731-527a9918c734"
            End Get
        End Property
#End Region
#Region " Private Variables and Objects"
        Private _unitTypeId As String
        Private _descrip As String
#End Region
    End Class
#End Region

#Region "Program"
    Public Class ProgramInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _progId As String
        Private _code As String
        Private _active As Boolean
        Private _descrip As String
        Private _progTypeId As String
        Private _progTypeDescrip As String
        Private _modUser As String
        Private _modDate As DateTime
        Private _degreeId As String
        Private _degreeDescrip As String
        'Private _weeks As Integer
        'Private _terms As Integer
        'Private _hours As Integer
        'Private _credits As Integer
        Private _calendarTypeId As String
        Private _calendarTypeDescrip As String
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _progId = Guid.NewGuid.ToString
            _active = True
            '_weeks = 0
            '_terms = 0
            '_hours = 0
            '_credits = 0
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property ProgId() As String
            Get
                Return _progId
            End Get
            Set(ByVal Value As String)
                _progId = Value
            End Set
        End Property
        Public Property Code() As String
            Get
                Return _code
            End Get
            Set(ByVal Value As String)
                _code = Value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return _active
            End Get
            Set(ByVal Value As Boolean)
                _active = Value
            End Set
        End Property
        Public Property Descrip() As String
            Get
                Return _descrip
            End Get
            Set(ByVal Value As String)
                _descrip = Value
            End Set
        End Property
        Public Property ProgTypeId() As String
            Get
                Return _progTypeId
            End Get
            Set(ByVal Value As String)
                _progTypeId = Value
            End Set
        End Property
        Public Property ProgTypeDescrip() As String
            Get
                Return _progTypeDescrip
            End Get
            Set(ByVal Value As String)
                _progTypeDescrip = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
        Public Property DegreeId() As String
            Get
                Return _degreeId
            End Get
            Set(ByVal Value As String)
                _degreeId = Value
            End Set
        End Property
        Public Property DegreeDescrip() As String
            Get
                Return _degreeDescrip
            End Get
            Set(ByVal Value As String)
                _degreeDescrip = Value
            End Set
        End Property
        Public Property CalendarTypeId() As String
            Get
                Return _calendarTypeId
            End Get
            Set(ByVal Value As String)
                _calendarTypeId = Value
            End Set
        End Property
        Public Property CalendarTypeDescrip() As String
            Get
                Return _calendarTypeDescrip
            End Get
            Set(ByVal Value As String)
                _calendarTypeDescrip = Value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "Program Version"
    Public Class PrgVersionInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _prgVerId As String
        Private _progId As String
        Private _progDescrip As String
        Private _prgVerCode As String
        Private _prgVerDescrip As String
        Private _active As Boolean
        Private _campGrpId As String
        Private _campGrpDescrip As String
        Private _prgGrpId As String
        Private _prgGrpDescrip As String
        'private _degreeId as String
        'private _degreeDescrip as String
        Private _sapId As String
        Private _sapDescrip As String
        Private _grdScaleId As String
        Private _grdScaleDescrip As String
        Private _testingModelId As String
        Private _weeks As Integer
        Private _terms As Integer
        Private _hours As Integer
        Private _credits As Integer
        Private _ltHalfTime As Integer
        Private _halfTime As Integer
        Private _threeQuartTime As Integer
        Private _fullTime As Boolean
        Private _deptId As String
        Private _deptDescrip As String
        Private _grdSystemId As String
        Private _grdSystemDescrip As String
        Private _weightedGPA As Boolean
        Private _billingMethodId As String
        Private _billingMethodDescrip As String
        Private _tuitionEarningId As String
        Private _tuitionEarningsDescrip As String
        Private _modUser As String
        Private _modDate As DateTime
        Private _attendanceLevel As Boolean
        Private _customAttendance As Boolean
        Private _progTypId As String
        Private _progTypDescrip As String
        Private _isContinuingEd As Boolean
        Private _unitTypeId As String
        Private _trackTardies As Boolean
        Private _tardiesMakingAbsense As Integer
        Private _schedMethodId As Integer
        Private _schedMethodDescrip As String
        Private _useTimeClock As Boolean
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _prgVerId = Guid.NewGuid.ToString
            _active = True
            _weeks = 0
            _terms = 0
            _hours = 0
            _credits = 0
            _ltHalfTime = 0
            _halfTime = 0
            _threeQuartTime = 0
            _fullTime = True
            _attendanceLevel = False
            _customAttendance = False
            _isContinuingEd = False
            _trackTardies = False
            _tardiesMakingAbsense = 0
            _schedMethodId = 0
            _useTimeClock = False
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property PrgVerId() As String
            Get
                Return _prgVerId
            End Get
            Set(ByVal Value As String)
                _prgVerId = Value
            End Set
        End Property
        Public Property ProgId() As String
            Get
                Return _progId
            End Get
            Set(ByVal Value As String)
                _progId = Value
            End Set
        End Property
        Public Property ProgDescrip() As String
            Get
                Return _progDescrip
            End Get
            Set(ByVal Value As String)
                _progDescrip = Value
            End Set
        End Property
        Public Property PrgVerCode() As String
            Get
                Return _prgVerCode
            End Get
            Set(ByVal Value As String)
                _prgVerCode = Value
            End Set
        End Property
        Public Property PrgVerDescrip() As String
            Get
                Return _prgVerDescrip
            End Get
            Set(ByVal Value As String)
                _prgVerDescrip = Value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return _active
            End Get
            Set(ByVal Value As Boolean)
                _active = Value
            End Set
        End Property
        Public Property CampGrpId() As String
            Get
                Return _campGrpId
            End Get
            Set(ByVal Value As String)
                _campGrpId = Value
            End Set
        End Property
        Public Property CampGrpDescrip() As String
            Get
                Return _campGrpDescrip
            End Get
            Set(ByVal Value As String)
                _campGrpDescrip = Value
            End Set
        End Property
        Public Property PrgGrpId() As String
            Get
                Return _prgGrpId
            End Get
            Set(ByVal Value As String)
                _prgGrpId = Value
            End Set
        End Property
        Public Property PrgGrpDescrip() As String
            Get
                Return _prgGrpDescrip
            End Get
            Set(ByVal Value As String)
                _prgGrpDescrip = Value
            End Set
        End Property
        Public Property SAPId() As String
            Get
                Return _sapId
            End Get
            Set(ByVal Value As String)
                _sapId = Value
            End Set
        End Property
        Public Property SAPDescrip() As String
            Get
                Return _sapDescrip
            End Get
            Set(ByVal Value As String)
                _sapDescrip = Value
            End Set
        End Property
        'Public Property GrdScaleId() As String
        '    Get
        '        Return _grdScaleId
        '    End Get
        '    Set(ByVal Value As String)
        '        _grdScaleId = Value
        '    End Set
        'End Property
        'Public Property GrdScaleDescrip() As String
        '    Get
        '        Return _grdScaleDescrip
        '    End Get
        '    Set(ByVal Value As String)
        '        _grdScaleDescrip = Value
        '    End Set
        'End Property
        Public Property TestingModelId() As String
            Get
                Return _testingModelId
            End Get
            Set(ByVal Value As String)
                _testingModelId = Value
            End Set
        End Property
        Public Property Weeks() As Integer
            Get
                Return _weeks
            End Get
            Set(ByVal Value As Integer)
                _weeks = Value
            End Set
        End Property
        Public Property Terms() As Integer
            Get
                Return _terms
            End Get
            Set(ByVal Value As Integer)
                _terms = Value
            End Set
        End Property
        Public Property Hours() As Integer
            Get
                Return _hours
            End Get
            Set(ByVal Value As Integer)
                _hours = Value
            End Set
        End Property
        Public Property Credits() As Integer
            Get
                Return _credits
            End Get
            Set(ByVal Value As Integer)
                _credits = Value
            End Set
        End Property
        Public Property LTHalfTime() As Integer
            Get
                Return _ltHalfTime
            End Get
            Set(ByVal Value As Integer)
                _ltHalfTime = Value
            End Set
        End Property
        Public Property HalfTime() As Integer
            Get
                Return _halfTime
            End Get
            Set(ByVal Value As Integer)
                _halfTime = Value
            End Set
        End Property
        Public Property ThreeQuartTime() As Integer
            Get
                Return _threeQuartTime
            End Get
            Set(ByVal Value As Integer)
                _threeQuartTime = Value
            End Set
        End Property
        Public Property FullTime() As Boolean
            Get
                Return _fullTime
            End Get
            Set(ByVal Value As Boolean)
                _fullTime = Value
            End Set
        End Property
        Public Property DeptId() As String
            Get
                Return _deptId
            End Get
            Set(ByVal Value As String)
                _deptId = Value
            End Set
        End Property
        Public Property DeptDescrip() As String
            Get
                Return _deptDescrip
            End Get
            Set(ByVal Value As String)
                _deptDescrip = Value
            End Set
        End Property
        Public Property GrdSystemId() As String
            Get
                Return _grdSystemId
            End Get
            Set(ByVal Value As String)
                _grdSystemId = Value
            End Set
        End Property
        Public Property GrdSystemDescrip() As String
            Get
                Return _grdSystemDescrip
            End Get
            Set(ByVal Value As String)
                _grdSystemDescrip = Value
            End Set
        End Property
        Public Property WeightedGPA() As Boolean
            Get
                Return _weightedGPA
            End Get
            Set(ByVal Value As Boolean)
                _weightedGPA = Value
            End Set
        End Property
        Public Property BillingMethodId() As String
            Get
                Return _billingMethodId
            End Get
            Set(ByVal Value As String)
                _billingMethodId = Value
            End Set
        End Property
        Public Property BillingMethodDescrip() As String
            Get
                Return _billingMethodDescrip
            End Get
            Set(ByVal Value As String)
                _billingMethodDescrip = Value
            End Set
        End Property
        Public Property TuitionEarningId() As String
            Get
                Return _tuitionEarningId
            End Get
            Set(ByVal Value As String)
                _tuitionEarningId = Value
            End Set
        End Property
        Public Property TuitionEarningsDescrip() As String
            Get
                Return _tuitionEarningsDescrip
            End Get
            Set(ByVal Value As String)
                _tuitionEarningsDescrip = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
        Public Property AttendanceLevel() As Boolean
            Get
                Return _attendanceLevel
            End Get
            Set(ByVal Value As Boolean)
                _attendanceLevel = Value
            End Set
        End Property
        Public Property CustomAttendance() As Boolean
            Get
                Return _customAttendance
            End Get
            Set(ByVal Value As Boolean)
                _customAttendance = Value
            End Set
        End Property
        Public Property ProgTypeId() As String
            Get
                Return _progTypId
            End Get
            Set(ByVal Value As String)
                _progTypId = Value
            End Set
        End Property
        Public Property ProgTypeDescrip() As String
            Get
                Return _progTypDescrip
            End Get
            Set(ByVal Value As String)
                _progTypDescrip = Value
            End Set
        End Property
        Public Property IsContinuingEd() As Boolean
            Get
                Return _isContinuingEd
            End Get
            Set(ByVal Value As Boolean)
                _isContinuingEd = Value
            End Set
        End Property
        Public Property UnitTypeId() As String
            Get
                Return _unitTypeId
            End Get
            Set(ByVal Value As String)
                _unitTypeId = Value
            End Set
        End Property
        Public Property TrackTardies() As Boolean
            Get
                Return _trackTardies
            End Get
            Set(ByVal Value As Boolean)
                _trackTardies = Value
            End Set
        End Property
        Public Property TardiesMakingAbsence() As Integer
            Get
                Return _tardiesMakingAbsense
            End Get
            Set(ByVal Value As Integer)
                _tardiesMakingAbsense = Value
            End Set
        End Property
        Public Property SchedMethodId() As Integer
            Get
                Return _schedMethodId
            End Get
            Set(ByVal Value As Integer)
                _schedMethodId = Value
            End Set
        End Property
        Public Property SchedMethodDescrip() As String
            Get
                Return _schedMethodDescrip
            End Get
            Set(ByVal Value As String)
                _schedMethodDescrip = Value
            End Set
        End Property
        Public Property UseTimeClock() As Boolean
            Get
                Return _useTimeClock
            End Get
            Set(ByVal Value As Boolean)
                _useTimeClock = Value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "Fees"
    Public Class PrgVersionFeeInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _prgVerFeeId As String
        Private _active As Boolean
        Private _progId As String
        Private _progDescrip As String
        Private _prgVerId As String
        Private _prgVerDescrip As String
        Private _transCodeId As String
        Private _transCodeDescrip As String
        Private _tuitionCategoryId As String
        Private _tuitionCategoryDescrip As String
        Private _amount As Decimal
        Private _rateScheduleId As String
        Private _rateScheduleDescrip As String
        Private _unitId As UnitType
        Private _unitDescrip As String
        Private _modUser As String
        Private _modDate As DateTime
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _prgVerFeeId = Guid.NewGuid.ToString
            _active = True
            _amount = 0.0
            _unitId = 0
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property PrgVerFeeId() As String
            Get
                Return _prgVerFeeId
            End Get
            Set(ByVal Value As String)
                _prgVerFeeId = Value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return _active
            End Get
            Set(ByVal Value As Boolean)
                _active = Value
            End Set
        End Property
        Public Property PrgVerId() As String
            Get
                Return _prgVerId
            End Get
            Set(ByVal Value As String)
                _prgVerId = Value
            End Set
        End Property
        Public Property PrgVerDescrip() As String
            Get
                Return _prgVerDescrip

            End Get
            Set(ByVal Value As String)
                _prgVerDescrip = Value
            End Set
        End Property
        Public Property ProgId() As String
            Get
                Return _progId
            End Get
            Set(ByVal Value As String)
                _progId = Value
            End Set
        End Property
        Public Property ProgDescrip() As String
            Get
                Return _progDescrip

            End Get
            Set(ByVal Value As String)
                _progDescrip = Value
            End Set
        End Property
        Public Property TransCodeId() As String
            Get
                Return _transCodeId
            End Get
            Set(ByVal Value As String)
                _transCodeId = Value
            End Set
        End Property
        Public Property TransCodeDescrip() As String
            Get
                Return _transCodeDescrip
            End Get
            Set(ByVal Value As String)
                _transCodeDescrip = Value
            End Set
        End Property
        Public Property TuitionCategoryId() As String
            Get
                Return _tuitionCategoryId
            End Get
            Set(ByVal Value As String)
                _tuitionCategoryId = Value
            End Set
        End Property
        Public Property TuitionCategoryDescrip() As String
            Get
                Return _tuitionCategoryDescrip
            End Get
            Set(ByVal Value As String)
                _tuitionCategoryDescrip = Value
            End Set
        End Property
        Public Property Amount() As Decimal
            Get
                Return _amount
            End Get
            Set(ByVal Value As Decimal)
                _amount = Value
            End Set
        End Property
        Public Property RateScheduleId() As String
            Get
                Return _rateScheduleId
            End Get
            Set(ByVal Value As String)
                _rateScheduleId = Value
            End Set
        End Property
        Public Property RateScheduleDescrip() As String
            Get
                Return _rateScheduleDescrip
            End Get
            Set(ByVal Value As String)
                _rateScheduleDescrip = Value
            End Set
        End Property
        Public Property UnitId() As UnitType
            Get
                Return _unitId
            End Get
            Set(ByVal Value As UnitType)
                _unitId = Value
            End Set
        End Property
        Public Property UnitDescrip() As String
            Get
                Return _unitDescrip
            End Get
            Set(ByVal Value As String)
                _unitDescrip = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As Date
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "Schedules"
    Public Class ScheduleInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _scheduleId As String
        Private _code As String
        Private _descrip As String
        Private _prgVerId As String
        Private _prgVerDescrip As String
        Private _progId As String
        Private _progDescrip As String
        Private _academicCalendarType As AcademicCalendarTypes
        Private _useTimeClock As Boolean
        Private _useFlexTime As Boolean
        Private _active As Boolean
        Private _modUser As String
        Private _modDate As DateTime
        Private _details() As ScheduleDetailInfo
        Private _detailsDT As DataTable
        Private _numStudentsUsing As Integer
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _scheduleId = Guid.NewGuid.ToString
            _academicCalendarType = AcademicCalendarTypes.Credit ' default to credit school
            _useTimeClock = False
            _useFlexTime = False
            _active = True
            _numStudentsUsing = 0
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property ScheduleId() As String
            Get
                Return _scheduleId
            End Get
            Set(ByVal Value As String)
                _scheduleId = Value
            End Set
        End Property
        Public Property Code() As String
            Get
                Return _code
            End Get
            Set(ByVal Value As String)
                _code = Value
            End Set
        End Property
        Public Property Descrip() As String
            Get
                Return _descrip
            End Get
            Set(ByVal Value As String)
                _descrip = Value
            End Set
        End Property
        Public Property PrgVerId() As String
            Get
                Return _prgVerId
            End Get
            Set(ByVal Value As String)
                _prgVerId = Value
            End Set
        End Property
        Public Property PrgVerDescrip() As String
            Get
                Return _prgVerDescrip
            End Get
            Set(ByVal Value As String)
                _prgVerDescrip = Value
            End Set
        End Property
        Public Property ProgId() As String
            Get
                Return _progId
            End Get
            Set(ByVal Value As String)
                _progId = Value
            End Set
        End Property
        Public Property ProgDescrip() As String
            Get
                Return _progDescrip
            End Get
            Set(ByVal Value As String)
                _progDescrip = Value
            End Set
        End Property
        Public Property AcademicCalendarType() As AcademicCalendarTypes
            Get
                Return _academicCalendarType
            End Get
            Set(ByVal Value As AcademicCalendarTypes)
                _academicCalendarType = Value
            End Set
        End Property
        Public Property UseTimeClock() As Boolean
            Get
                Return _useTimeClock
            End Get
            Set(ByVal Value As Boolean)
                _useTimeClock = Value
            End Set
        End Property
        Public Property UseFlexTime() As Boolean
            Get
                Return _useFlexTime
            End Get
            Set(ByVal Value As Boolean)
                _useFlexTime = Value
            End Set
        End Property

        Public Property Active() As Boolean
            Get
                Return _active
            End Get
            Set(ByVal Value As Boolean)
                _active = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
        Public Property Details() As ScheduleDetailInfo()
            Get
                Return _details
            End Get
            Set(ByVal Value As ScheduleDetailInfo())
                _details = Value
            End Set
        End Property
        Public Property DetailsDT() As DataTable
            Get
                Return _detailsDT
            End Get
            Set(ByVal Value As DataTable)
                _detailsDT = Value
            End Set
        End Property
        Public Property NumStudentsUsing() As Integer
            Get
                Return _numStudentsUsing
            End Get
            Set(ByVal Value As Integer)
                _numStudentsUsing = Value
            End Set
        End Property
#End Region
    End Class

    Public Class ScheduleDetailInfo
#Region " Private Variables and Objects"
        Protected _isInDB As Boolean
        Protected _scheduleDetailId As String
        Protected _scheduleId As String
        Protected _dw As Integer
        Protected _dwDescrip As String
        Protected _total As Decimal
        Protected _timein As DateTime
        Protected _lunchout As DateTime
        Protected _lunchin As DateTime
        Protected _timeout As DateTime
        Protected _maxnolunch As Decimal
        Protected _allow_earlyin As Boolean
        Protected _allow_lateout As Boolean
        Protected _allow_extrahours As Boolean
        Protected _check_tardyin As Boolean
        Protected _max_beforetardy As DateTime
        Protected _tardy_intime As DateTime
        Protected _useTimeClock As Boolean
        Protected _minHoursConsideredPresent As Decimal
#End Region
#Region "Public Constructors "
        Public Sub New()
            _isInDB = False
            _scheduleDetailId = Guid.NewGuid.ToString
            _dw = 0
            _total = 0
            _maxnolunch = 0
            _timein = DateTime.MinValue
            _lunchout = DateTime.MinValue
            _lunchin = DateTime.MinValue
            _timeout = DateTime.MinValue
            _allow_earlyin = False
            _allow_lateout = False
            _allow_extrahours = False
            _check_tardyin = False
            _max_beforetardy = DateTime.MinValue
            _tardy_intime = DateTime.MinValue
            _useTimeClock = False
            _minHoursConsideredPresent = 0
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property ScheduleDetailId() As String
            Get
                Return _scheduleDetailId
            End Get
            Set(ByVal Value As String)
                _scheduleDetailId = Value
            End Set
        End Property
        Public Property ScheduleId() As String
            Get
                Return _scheduleId
            End Get
            Set(ByVal Value As String)
                _scheduleId = Value
            End Set
        End Property
        Public Property dw() As Integer
            Get
                Return _dw
            End Get
            Set(ByVal Value As Integer)
                _dw = Value
            End Set
        End Property
        Public Property dwDescrip() As String
            Get
                Return _dwDescrip
            End Get
            Set(ByVal Value As String)
                _dwDescrip = Value
            End Set
        End Property
        Public Property Total() As Decimal
            Get
                Return _total
            End Get
            Set(ByVal Value As Decimal)
                _total = Value
            End Set
        End Property
        Public ReadOnly Property CalculatedTotal() As Decimal
            Get
                Dim tot As Long = 0.0
                Dim tot_lunch As Long = 0.0
                If TimeIn <> DateTime.MinValue AndAlso TimeOut <> DateTime.MinValue AndAlso TimeOut >= TimeIn Then
                    tot = DateDiff(DateInterval.Minute, TimeIn, TimeOut)
                End If
                If LunchOut <> DateTime.MinValue AndAlso LunchIn <> DateTime.MinValue AndAlso LunchIn >= LunchOut Then
                    tot_lunch = DateDiff(DateInterval.Minute, LunchOut, LunchIn)
                End If

                Return (tot - tot_lunch) / 60.0
            End Get
        End Property
        Public Property TimeIn() As DateTime
            Get
                Return _timein
            End Get
            Set(ByVal Value As DateTime)
                _timein = Value
            End Set
        End Property
        Public Property LunchOut() As DateTime
            Get
                Return _lunchout
            End Get
            Set(ByVal Value As DateTime)
                _lunchout = Value
            End Set
        End Property
        Public Property LunchIn() As DateTime
            Get
                Return _lunchin
            End Get
            Set(ByVal Value As DateTime)
                _lunchin = Value
            End Set
        End Property
        Public Property TimeOut() As DateTime
            Get
                Return _timeout
            End Get
            Set(ByVal Value As DateTime)
                _timeout = Value
            End Set
        End Property
        ''Changed from Int to decimal
        Public Property MaxNoLunch() As Decimal
            Get
                Return _maxnolunch
            End Get
            Set(ByVal Value As Decimal)
                _maxnolunch = Value
            End Set
        End Property
        Public Property Allow_EarlyIn() As Boolean
            Get
                Return _allow_earlyin
            End Get
            Set(ByVal Value As Boolean)
                _allow_earlyin = Value
            End Set
        End Property
        Public Property Allow_LateOut() As Boolean
            Get
                Return _allow_lateout
            End Get
            Set(ByVal Value As Boolean)
                _allow_lateout = Value
            End Set
        End Property
        Public Property Allow_ExtraHours() As Boolean
            Get
                Return _allow_extrahours
            End Get
            Set(ByVal Value As Boolean)
                _allow_extrahours = Value
            End Set
        End Property
        Public Property Check_TardyIn() As Boolean
            Get
                Return _check_tardyin
            End Get
            Set(ByVal Value As Boolean)
                _check_tardyin = Value
            End Set
        End Property
        Public Property MaxBeforeTardy() As DateTime
            Get
                Return _max_beforetardy
            End Get
            Set(ByVal Value As DateTime)
                _max_beforetardy = Value
            End Set
        End Property
        Public Property TardyInTime() As DateTime
            Get
                Return _tardy_intime
            End Get
            Set(ByVal Value As DateTime)
                _tardy_intime = Value
            End Set
        End Property
         Public Property MinHoursConsideredPresent() As Decimal
            Get
                Return _minHoursConsideredPresent
            End Get
            Set(ByVal Value As Decimal)
                _minHoursConsideredPresent = Value
            End Set
        End Property
#End Region
    End Class

    Public Class StudentScheduleInfo
        Inherits ScheduleInfo
#Region "Private Variables and Objects"
        Protected _stuEnrollid As String
        Protected _badgeNumber As String
        Protected _startDate As DateTime
        Protected _endDate As DateTime
#End Region
#Region " Public Constructors "
        Public Sub New()
            MyBase.New()
        End Sub
#End Region
#Region " Public Properties"
        Public Property StuEnrollId() As String
            Get
                Return _stuEnrollid
            End Get
            Set(ByVal Value As String)
                _stuEnrollid = Value
            End Set
        End Property
        Public Property BadgeNumber() As String
            Get
                Return _badgeNumber
            End Get
            Set(ByVal Value As String)
                _badgeNumber = Value
            End Set
        End Property
        Public Property StartDate() As DateTime
            Get
                Return _startDate
            End Get
            Set(ByVal Value As DateTime)
                _startDate = Value
            End Set
        End Property
        Public Property EndDate() As DateTime
            Get
                Return _endDate
            End Get
            Set(ByVal Value As DateTime)
                _endDate = Value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "Grade Book"
    ''' <summary>
    ''' From syResourses where ResourceTypeId = 10
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum SysGradeBookComponentTypes
        Homework = 499
        LabWork = 500
        Exam = 501
        Final = 502
        LabHours = 503
        Externship = 544
    End Enum

    Public Class GradeBookComponentInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _grdComponentTypeId As String
        Private _code As String
        Private _descrip As String
        Private _campGrpId As String
        Private _campGrpDescrip As String
        Private _active As Boolean
        Private _modUser As String
        Private _modDate As DateTime
        Private _sysComponentTypeId As SysGradeBookComponentTypes
        Private _sysComponentTypeDescrip As String
        Private _courseId As ArrayList
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _grdComponentTypeId = Guid.NewGuid.ToString
            _sysComponentTypeId = SysGradeBookComponentTypes.Homework
            _active = True
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property GrdComponentTypeId() As String
            Get
                Return _grdComponentTypeId
            End Get
            Set(ByVal Value As String)
                _grdComponentTypeId = Value
            End Set
        End Property
        Public Property Code() As String
            Get
                Return _code
            End Get
            Set(ByVal Value As String)
                _code = Value
            End Set
        End Property
        Public Property Descrip() As String
            Get
                Return _descrip
            End Get
            Set(ByVal Value As String)
                _descrip = Value
            End Set
        End Property
        Public Property CampGrpId() As String
            Get
                Return _campGrpId
            End Get
            Set(ByVal Value As String)
                _campGrpId = Value
            End Set
        End Property
        Public Property CampGrpDescrip() As String
            Get
                Return _campGrpDescrip
            End Get
            Set(ByVal Value As String)
                _campGrpDescrip = Value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return _active
            End Get
            Set(ByVal Value As Boolean)
                _active = Value
            End Set
        End Property
        Public Property ModUser()
            Get
                Return _modUser
            End Get
            Set(ByVal Value)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
        Public Property SysComponentTypeId() As SysGradeBookComponentTypes
            Get
                Return _sysComponentTypeId
            End Get
            Set(ByVal Value As SysGradeBookComponentTypes)
                _sysComponentTypeId = Value
            End Set
        End Property
        Public Property SysComponentTypeDescrip() As String
            Get
                Return _sysComponentTypeDescrip
            End Get
            Set(ByVal Value As String)
                _sysComponentTypeDescrip = Value
            End Set
        End Property
        Public Property Course() As ArrayList
            Get
                Return _courseId
            End Get
            Set(ByVal value As ArrayList)
                _courseId = value
            End Set
        End Property
#End Region
    End Class

    Public Class GrdBkWgtsInfo
#Region "Private Variable Declaration"
        Private _isInDB As Boolean
        Private _grdBkWgtId As String
        Private _instructorId As String
        Private _descrip As String
        Private _active As Boolean
        Private _reqId As String
        Private _effectiveDate As Date
        Private _modUser As String
        Private _modDate As DateTime
        Private _detailsDT As DataTable
        Private _details() As GrdBkWgtDetailsInfo
#End Region
#Region "Public Constructor"
        Public Sub New()
            _isInDB = False
            _grdBkWgtId = Guid.NewGuid.ToString
            _active = True
        End Sub
#End Region
#Region "GrdBk Wgts Properties"
        Public Property IsInDb() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal value As Boolean)
                _isInDB = value
            End Set
        End Property
        Public Property GrdBkWgtId() As String
            Get
                Return _grdBkWgtId
            End Get
            Set(ByVal Value As String)
                _grdBkWgtId = Value
            End Set
        End Property
        Public Property InstructorId() As String
            Get
                Return _instructorId
            End Get
            Set(ByVal Value As String)
                _instructorId = Value
            End Set
        End Property

        Public Property Active() As Boolean
            Get
                Return _active
            End Get
            Set(ByVal Value As Boolean)
                _active = Value
            End Set
        End Property
        Public Property Descrip() As String
            Get
                Return _descrip
            End Get
            Set(ByVal Value As String)
                _descrip = Value
            End Set
        End Property
        Public Property EffectiveDate() As Date
            Get
                Return _effectiveDate
            End Get
            Set(ByVal Value As Date)
                _effectiveDate = Value
            End Set
        End Property
        Public Property ReqId() As String
            Get
                Return _reqId
            End Get
            Set(ByVal Value As String)
                _reqId = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As Date
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
        Public Property DetailsDT() As DataTable
            Get
                Return _detailsDT
            End Get
            Set(ByVal value As DataTable)
                _detailsDT = value
            End Set
        End Property
        Public Property Details() As GrdBkWgtDetailsInfo()
            Get
                Return _details
            End Get
            Set(ByVal Value As GrdBkWgtDetailsInfo())
                _details = Value
            End Set
        End Property
#End Region
    End Class

    Public Class GrdBkWgtDetailsInfo
#Region "Private Variable Declaration"
        Private _code As String
        Private _descrip As String
        Private _weight As Decimal
        Private _number As Decimal
        Private _parameter As Integer
        Private _grdBkWgtDetailId As String
        Private _grdCompTypeId As String
        Private _grdSysCompTypeId As Integer
        Private _grdBkWgtId As String
        Private _grdPolicyTypeId As Integer
        Private _seq As Integer
        Private _required As Boolean
        Private _mustpass As Boolean
        Private _modUser As String
        Private _modDate As DateTime
        Private _creditsperservice As Decimal
#End Region
#Region " Public Constructor"
        Public Sub New()
            _grdBkWgtDetailId = Guid.NewGuid.ToString()
            _modDate = Date.MinValue
            _weight = 0.0
            _number = 0
            _parameter = 0
            _grdPolicyTypeId = 0
            _seq = 0
            _required = False
            _mustpass = False
            _creditsperservice = 0.0
        End Sub
#End Region
#Region "GrdBk Wgts Properties"
        Public Property GrdBkWgtId() As String
            Get
                Return _grdBkWgtId
            End Get
            Set(ByVal Value As String)
                _grdBkWgtId = Value
            End Set
        End Property
        Public Property GrdBkWgtDetailId() As String
            Get
                Return _grdBkWgtDetailId
            End Get
            Set(ByVal Value As String)
                _grdBkWgtDetailId = Value
            End Set
        End Property
        Public Property GrdCompTypeId() As String
            Get
                Return _grdCompTypeId
            End Get
            Set(ByVal Value As String)
                _grdCompTypeId = Value
            End Set
        End Property
        Public Property GrdSysCompTypeId() As Integer
            Get
                Return _grdSysCompTypeId
            End Get
            Set(ByVal Value As Integer)
                _grdSysCompTypeId = Value
            End Set
        End Property
        Public Property GrdPolicyTypeId() As Integer
            Get
                Return _grdPolicyTypeId
            End Get
            Set(ByVal Value As Integer)
                _grdPolicyTypeId = Value
            End Set
        End Property
        Public Property Code() As String
            Get
                Return _code
            End Get
            Set(ByVal Value As String)
                _code = Value
            End Set
        End Property
        Public Property Descrip() As String
            Get
                Return _descrip
            End Get
            Set(ByVal Value As String)
                _descrip = Value
            End Set
        End Property
        Public Property weight() As Decimal
            Get
                Return _weight
            End Get
            Set(ByVal Value As Decimal)
                _weight = Value
            End Set
        End Property
        Public Property Number() As Decimal
            Get
                Return _number
            End Get
            Set(ByVal Value As Decimal)
                _number = Value
            End Set
        End Property
        Public Property Parameter() As Integer
            Get
                Return _parameter
            End Get
            Set(ByVal Value As Integer)
                _parameter = Value
            End Set
        End Property
        Public Property Seq() As Integer
            Get
                Return _seq
            End Get
            Set(ByVal Value As Integer)
                _seq = Value
            End Set
        End Property
        Public Property CreditsPerService() As Decimal
            Get
                Return _CreditsPerService
            End Get
            Set(ByVal value As Decimal)
                _CreditsPerService = value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As Date
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
        Public Property Required() As Boolean
            Get
                Return _required
            End Get
            Set(ByVal value As Boolean)
                _required = value
            End Set
        End Property
        Public Property mustPass() As Boolean
            Get
                Return _mustpass
            End Get
            Set(ByVal value As Boolean)
                _mustpass = value
            End Set
        End Property
#End Region
    End Class

#End Region

#Region "Courses"
    Public Class CourseInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _reqId As String
        Private _code As String
        Private _descrip As String
        Private _active As Boolean
        Private _campGrpId As String
        Private _campGrpDescrip As String
        Private _credits As String
        Private _isExternship As Boolean
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _active = True
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property ReqId() As String
            Get
                Return _reqId
            End Get
            Set(ByVal Value As String)
                _reqId = Value
            End Set
        End Property
        Public Property Code() As String
            Get
                Return _code
            End Get
            Set(ByVal Value As String)
                _code = Value
            End Set
        End Property
        Public Property Descrip() As String
            Get
                Return _Descrip
            End Get
            Set(ByVal Value As String)
                _Descrip = Value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return _active
            End Get
            Set(ByVal Value As Boolean)
                _active = Value
            End Set
        End Property
        Public Property CampGrpId() As String
            Get
                Return _campGrpId
            End Get
            Set(ByVal Value As String)
                _campGrpId = Value
            End Set
        End Property
        Public Property CampGrpDescrip() As String
            Get
                Return _campGrpDescrip
            End Get
            Set(ByVal Value As String)
                _campGrpDescrip = Value
            End Set
        End Property
        Public Property Credits() As String
            Get
                Return _credits
            End Get
            Set(ByVal Value As String)
                _credits = Value
            End Set
        End Property
        Public Property IsExternship() As Boolean
            Get
                Return _isExternship
            End Get
            Set(ByVal Value As Boolean)
                _isExternship = Value
            End Set
        End Property
#End Region
    End Class
#End Region




End Namespace