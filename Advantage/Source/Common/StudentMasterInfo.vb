<Serializable()> _
Public Class StudentMasterInfo
#Region " Private Variables and Objects"

    Private phonepri As String

    Private _Country As String
    Private _County As String
    Private _AddressStatus As String
    Private _AddressTypeID As String

    Private _SourceCategory As String
    Private _SourceDate As String
    Private _SourceType As String

    Private _Area As String
    Private _ExpectedStart As String
    Private _ProgramId As String
    Private _ShiftID As String

    Private _AddressType As String

    Private _Nationality As String
    Private _DriverLicState As String
    Private _Citizen As String
    Private _DriverLicNumber As String
    Private _SSN As String
    Private _AlienNumber As String

    Private _LicensureWrittenAllParts As Boolean
    Private _LicensureLastPartWrittenOn As DateTime?
    Private _LicensurePassedAllParts As Boolean

    Private _Notes As String
    Private _AssignmentDate As String

    Private _EnrollmentId As String

    Private _StateDescrip As String
    Private _AddressTypeDescrip As String
    Private _PrefixDescrip As String
    Private _SuffixDescrip As String
    Private _AdminCriteriaDescrip As String
    Private _GenderDescrip As String
    Private _EthCodeDescrip As String
    Private _MaritalStatusDescrip As String
    Private _FamilyIncomeDescrip As String
    Private _PhoneTypeDescrip As String
    Private _ShiftDescrip As String
    Private _NationalityDescrip As String
    Private _CitizenDescrip As String
    Private _DriverLicenseStateDescrip As String
    Private _CountryDescrip As String
    Private _CountyDescrip As String
    Private _EdLvlDescrip As String
    Private _AddressStatusDescrip As String
    Private _PhoneStatusDescrip As String
    Private _DependencyTypeDescrip As String
    Private _GeographicTypeDescrip As String
    Private _SponsorTypeDescrip As String
    Private _StudentStatusDescrip As String
    Private _HousingDescrip As String

    Private _ApportionedCreditBalanceAY1 As String
    Private _ApportionedCreditBalanceAY2 As String


#End Region

#Region " Public Constructor"

    Public Sub New()
        IsInDB = False
        Education = ""
        StudentID = Guid.NewGuid.ToString()
        LastName = ""
        FirstName = ""
        MiddleName = ""
        Status = Guid.Empty.ToString()
        Prefix = Guid.Empty.ToString()
        Suffix = Guid.Empty.ToString()
        Title = Guid.Empty.ToString()
        BirthDate = ""
        Age = ""
        Sponsor = Guid.Empty.ToString()
        AdmissionsRep = Guid.Empty.ToString
        Gender = Guid.Empty.ToString()
        Race = Guid.Empty.ToString()
        MaritalStatus = Guid.Empty.ToString()
        Children = ""
        FamilyIncome = Guid.Empty.ToString()
        LeadId = ""

        phonepri = ""
        PhoneType = Guid.Empty.ToString()
        PhoneStatus = Guid.Empty.ToString()

        WorkEmail = ""
        HomeEmail = ""

        Address1 = ""
        Address2 = ""
        City = ""
        State = Guid.Empty.ToString
        Zip = ""
        _Country = ""
        _County = Guid.Empty.ToString
        _AddressStatus = Guid.Empty.ToString
        _AddressType = Guid.Empty.ToString


        _SourceCategory = Guid.Empty.ToString
        _SourceDate = ""
        _SourceType = Guid.Empty.ToString

        _Area = Guid.Empty.ToString
        _ExpectedStart = ""
        _ProgramId = Guid.Empty.ToString
        _ShiftID = Guid.Empty.ToString
        StudentGrpId = ""

        _Nationality = Guid.Empty.ToString
        _Citizen = Guid.Empty.ToString
        _SSN = ""
        _DriverLicState = Guid.Empty.ToString
        _DriverLicNumber = ""
        _AlienNumber = ""

        _LicensureWrittenAllParts = False
        _LicensureLastPartWrittenOn = Nothing
        _LicensurePassedAllParts = False

        _Notes = ""

        SourceAdvertisement = Guid.Empty.ToString

        _AssignmentDate = ""
        CampusId = Guid.Empty.ToString

        PrgVerId = Guid.Empty.ToString
        StartDate = ""
        MidPointDate = ""
        ExpGradDate = ""
        TransferDate = ""
        LastDateAttended = ""
        StatusCodeId = Guid.Empty.ToString
        AcademicAdvisor = Guid.Empty.ToString
        GradeLevel = Guid.Empty.ToString
        ChargingMethod = Guid.Empty.ToString
        EnrollmentDate = ""
        _EnrollmentId = ""


        YearNumber = 0
        MonthNumber = 0
        DateNumber = 0
        LNameNumber = 0
        FNameNumber = 0
        SeqNumber = 0

        SeqStartNumber = 0
        FormatType = ""
        StudentNumber = ""
        Objective = ""

        ForeignPhone = 0
        ForeignZip = 0
        OtherState = ""
        StateId = ""
        ModDate = Date.MinValue

        DependencyTypeId = ""
        GeographicTypeId = ""
        AdminCriteriaId = ""
        HousingId = ""

        _StateDescrip = ""
        _AddressTypeDescrip = ""
        _PrefixDescrip = ""
        _SuffixDescrip = ""
        _AdminCriteriaDescrip = ""
        _GenderDescrip = ""
        _EthCodeDescrip = ""
        _MaritalStatusDescrip = ""
        _FamilyIncomeDescrip = ""
        _PhoneTypeDescrip = ""
        _ShiftDescrip = ""
        _NationalityDescrip = ""
        _CitizenDescrip = ""
        _DriverLicenseStateDescrip = ""
        _CountryDescrip = ""
        _CountyDescrip = ""
        _EdLvlDescrip = ""
        _AddressStatusDescrip = ""
        _PhoneStatusDescrip = ""
        _StudentStatusDescrip = ""
        _DependencyTypeDescrip = ""
        _GeographicTypeDescrip = ""
        _SponsorTypeDescrip = ""
        _HousingDescrip = ""
        EntranceInterviewDate = "01/01/1900"
        HighSchoolProgramCode = ""

        _ApportionedCreditBalanceAY1 = ""
        _ApportionedCreditBalanceAY2 = ""
    End Sub

#End Region

#Region "Public Properties"

    Public Property EntranceInterviewDate As Date

    Public Property HighSchoolProgramCode As String

    Public Property DependencyTypeId As String

    Public Property GeographicTypeId As String

    Public Property AdminCriteriaId As String

    Public Property HousingId As String

    Public Property StateId As String

    Public Property LeadId As String

    Public Property StudentGrpId As String

    Public Property ForeignPhone As Integer

    Public Property ForeignZip As Integer

    Public Property OtherState As String

    Public Property ModDate As Date

    Public Property YearNumber As Integer

    Public Property MonthNumber As Integer

    Public Property DateNumber As Integer

    Public Property LNameNumber As Integer

    Public Property FNameNumber As Integer

    Public Property SeqNumber As Integer

    Public Property SeqStartNumber As Integer

    Public Property FormatType As String

    Public Property Objective As String

    Public Property PrgVerId As String

    Public Property StatusCodeId As String

    Public Property Education As String

    Public Property AcademicAdvisor As String

    Public Property StudentNumber As String

    Public Property GradeLevel As String

    Public Property ChargingMethod As String

    Public Property IsInDB As Boolean

    Public Property StudentID As String

    Public Property SourceAdvertisement As String

    Public Property WorkEmail As String

    Public Property HomeEmail As String

    Public Property LastName As String

    Public Property FirstName As String

    Public Property MiddleName As String

    Public Property Status As String

    Public Property Prefix As String

    Public Property Suffix As String

    Public Property Title As String

    Public Property EnrollmentDate As String

    Public Property StartDate As String

    Public Property MidPointDate As String

    Public Property ExpGradDate As String

    Public Property TransferDate As String

    Public Property LastDateAttended As String

    Public Property BirthDate As String

    Public Property Sponsor As String

    Public Property AdmissionsRep As String

    Public Property Gender As String

    Public Property CampusId As String

    Public Property Race As String

    Public Property MaritalStatus As String

    Public Property FamilyIncome As String

    Public Property Children As String


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks> The software has corrupt database when enter a phone number in database
    ''' Then we need to crrect the situation and also avoit to enter corrupted information,
    ''' This situation happen when the phone number is international.
    ''' </remarks>
    Public Property Phone() As String
        Get
            phonepri = phonepri.Replace("(", "")
            phonepri = phonepri.Replace(")", "")
            phonepri = phonepri.Replace("-", "")
            phonepri = phonepri.Replace(".", "")
            phonepri = phonepri.Replace(" ", "")
            Return phonepri
        End Get
        Set(ByVal value As String)
            'Clean the value when it is setting
            value = value.Replace("(", "")
            value = value.Replace(")", "")
            value = value.Replace("-", "")
            value = value.Replace(".", "")
            value = value.Replace(" ", "")
            phonepri = value
        End Set
    End Property

    Public Property PhoneType As String

    Public Property PhoneStatus As String

    Public Property Age As String

    Public Property Address1 As String

    Public Property Address2 As String

    Public Property City As String

    Public Property State As String

    ''' <summary>
    ''' To support the is international new field
    ''' </summary>
    ''' <returns></returns>
    Public Property IsInternational As Boolean
        
    Public Property Zip As String

    Public Property Country() As String
        Get
            Return _Country
        End Get
        Set
            _Country = Value
        End Set
    End Property

    Public Property County() As String
        Get
            Return _County
        End Get
        Set(ByVal Value As String)
            _County = Value
        End Set
    End Property

    Public Property AddressType() As String
        Get
            Return _AddressType
        End Get
        Set(ByVal Value As String)
            _AddressType = Value
        End Set
    End Property

    Public Property AddressStatus() As String
        Get
            Return _AddressStatus
        End Get
        Set(ByVal Value As String)
            _AddressStatus = Value
        End Set
    End Property

    Public Property SourceCategory() As String
        Get
            Return _SourceCategory
        End Get
        Set(ByVal Value As String)
            _SourceCategory = Value
        End Set
    End Property

    Public Property SourceDate() As String
        Get
            Return _SourceDate
        End Get
        Set(ByVal Value As String)
            _SourceDate = Value
        End Set
    End Property

    Public Property SourceType() As String
        Get
            Return _SourceType
        End Get
        Set(ByVal Value As String)
            _SourceType = Value
        End Set
    End Property

    Public Property Area() As String
        Get
            Return _Area
        End Get
        Set(ByVal Value As String)
            _Area = Value
        End Set
    End Property

    Public Property ExpectedStart() As String
        Get
            Return _ExpectedStart
        End Get
        Set(ByVal Value As String)
            _ExpectedStart = Value
        End Set
    End Property

    Public Property ProgramID() As String
        Get
            Return _ProgramId
        End Get
        Set(ByVal Value As String)
            _ProgramId = Value
        End Set
    End Property

    Public Property ShiftID() As String
        Get
            Return _ShiftID
        End Get
        Set(ByVal Value As String)
            _ShiftID = Value
        End Set
    End Property

    Public Property Nationality() As String
        Get
            Return _Nationality
        End Get
        Set(ByVal Value As String)
            _Nationality = Value
        End Set
    End Property

    Public Property Citizen() As String
        Get
            Return _Citizen
        End Get
        Set(ByVal Value As String)
            _Citizen = Value
        End Set
    End Property

    Public Property SSN() As String
        Get
            Return _SSN
        End Get
        Set(ByVal Value As String)
            _SSN = Value
        End Set
    End Property

    Public Property DriverLicState() As String
        Get
            Return _DriverLicState
        End Get
        Set(ByVal Value As String)
            _DriverLicState = Value
        End Set
    End Property

    Public Property DriverLicNumber() As String
        Get
            Return _DriverLicNumber
        End Get
        Set(ByVal Value As String)
            _DriverLicNumber = Value
        End Set
    End Property

    Public Property AlienNumber() As String
        Get
            Return _AlienNumber
        End Get
        Set(ByVal Value As String)
            _AlienNumber = Value
        End Set
    End Property

     Public Property LicensureWrittenAllParts() As Boolean
        Get
            Return _LicensureWrittenAllParts
        End Get
        Set(ByVal Value As Boolean)
            _LicensureWrittenAllParts = Value
        End Set
    End Property
     Public Property LicensureLastPartWrittenOn() As DateTime?
        Get
            Return _LicensureLastPartWrittenOn
        End Get
        Set(ByVal Value As DateTime?)
            _LicensureLastPartWrittenOn = Value
        End Set
    End Property
     Public Property LicensurePassedAllParts() As Boolean
        Get
            Return _LicensurePassedAllParts
        End Get
        Set(ByVal Value As Boolean)
            _LicensurePassedAllParts = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property

    Public Property EnrollmentId() As String
        Get
            Return _EnrollmentId
        End Get
        Set(ByVal Value As String)
            _EnrollmentId = Value
        End Set
    End Property

    Public Property AssignmentDate() As String
        Get
            Return _AssignmentDate
        End Get
        Set(ByVal Value As String)
            _AssignmentDate = Value
        End Set
    End Property

    Public Property StateDescrip() As String
        Get
            Return _StateDescrip
        End Get
        Set(ByVal Value As String)
            _StateDescrip = Value
        End Set
    End Property

    Public Property AddressTypeDescrip() As String
        Get
            Return _AddressTypeDescrip
        End Get
        Set(ByVal Value As String)
            _AddressTypeDescrip = Value
        End Set
    End Property

    Public Property PrefixDescrip() As String
        Get
            Return _PrefixDescrip
        End Get
        Set(ByVal Value As String)
            _PrefixDescrip = Value
        End Set
    End Property

    Public Property SuffixDescrip() As String
        Get
            Return _SuffixDescrip
        End Get
        Set(ByVal Value As String)
            _SuffixDescrip = Value
        End Set
    End Property

    Public Property AdminCriteriaDescrip() As String
        Get
            Return _AdminCriteriaDescrip
        End Get
        Set(ByVal Value As String)
            _AdminCriteriaDescrip = Value
        End Set
    End Property

    Public Property GenderDescrip() As String
        Get
            Return _GenderDescrip
        End Get
        Set(ByVal Value As String)
            _GenderDescrip = Value
        End Set
    End Property

    Public Property EthCodeDescrip() As String
        Get
            Return _EthCodeDescrip
        End Get
        Set(ByVal Value As String)
            _EthCodeDescrip = Value
        End Set
    End Property

    Public Property MaritalStatusDescrip() As String
        Get
            Return _MaritalStatusDescrip
        End Get
        Set(ByVal Value As String)
            _MaritalStatusDescrip = Value
        End Set
    End Property

    Public Property FamilyIncomeDescrip() As String
        Get
            Return _FamilyIncomeDescrip
        End Get
        Set(ByVal Value As String)
            _FamilyIncomeDescrip = Value
        End Set
    End Property

    Public Property PhoneTypeDescrip() As String
        Get
            Return _PhoneTypeDescrip
        End Get
        Set(ByVal Value As String)
            _PhoneTypeDescrip = Value
        End Set
    End Property

    Public Property NationalityDescrip() As String
        Get
            Return _NationalityDescrip
        End Get
        Set(ByVal Value As String)
            _NationalityDescrip = Value
        End Set
    End Property

    Public Property CitizenDescrip() As String
        Get
            Return _CitizenDescrip
        End Get
        Set(ByVal Value As String)
            _CitizenDescrip = Value
        End Set
    End Property

    Public Property DriverLicenseStateDescrip() As String
        Get
            Return _DriverLicenseStateDescrip
        End Get
        Set(ByVal Value As String)
            _DriverLicenseStateDescrip = Value
        End Set
    End Property

    Public Property CountyDescrip() As String
        Get
            Return _CountyDescrip
        End Get
        Set(ByVal Value As String)
            _CountyDescrip = Value
        End Set
    End Property

    Public Property CountryDescrip() As String
        Get
            Return _CountryDescrip
        End Get
        Set(ByVal Value As String)
            _CountryDescrip = Value
        End Set
    End Property

    Public Property EdLvlDescrip() As String
        Get
            Return _EdLvlDescrip
        End Get
        Set(ByVal Value As String)
            _EdLvlDescrip = Value
        End Set
    End Property

    Public Property AddressStatusDescrip() As String
        Get
            Return _AddressStatusDescrip
        End Get
        Set(ByVal Value As String)
            _AddressStatusDescrip = Value
        End Set
    End Property

    Public Property PhoneStatusDescrip() As String
        Get
            Return _PhoneStatusDescrip
        End Get
        Set(ByVal Value As String)
            _PhoneStatusDescrip = Value
        End Set
    End Property

    Public Property StudentStatusDescrip() As String
        Get
            Return _StudentStatusDescrip
        End Get
        Set(ByVal Value As String)
            _StudentStatusDescrip = Value
        End Set
    End Property

    Public Property DependencyTypeDescrip() As String
        Get
            Return _DependencyTypeDescrip
        End Get
        Set(ByVal Value As String)
            _DependencyTypeDescrip = Value
        End Set
    End Property

    Public Property GeographicTypeDescrip() As String
        Get
            Return _GeographicTypeDescrip
        End Get
        Set(ByVal Value As String)
            _GeographicTypeDescrip = Value
        End Set
    End Property

    Public Property SponsorTypeDescrip() As String
        Get
            Return _SponsorTypeDescrip
        End Get
        Set(ByVal Value As String)
            _SponsorTypeDescrip = Value
        End Set
    End Property

    Public Property HousingDescrip() As String
        Get
            Return _HousingDescrip
        End Get
        Set(ByVal Value As String)
            _HousingDescrip = Value
        End Set
    End Property

    Public Property ApportionedCreditBalanceAY1() As String
        Get
            Return _ApportionedCreditBalanceAY1
        End Get
        Set(ByVal Value As String)
            _ApportionedCreditBalanceAY1 = Value
        End Set
    End Property

    Public Property ApportionedCreditBalanceAY2() As String
        Get
            Return _ApportionedCreditBalanceAY2
        End Get
        Set(ByVal Value As String)
            _ApportionedCreditBalanceAY2 = Value
        End Set
    End Property

#End Region
End Class
