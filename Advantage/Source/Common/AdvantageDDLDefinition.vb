Imports System.Web.UI.WebControls

Public Class AdvantageDDLDefinition
    Private _ddl As DropDownList
    Private _ddlName As String
    Private _campusId As String
    Private _insertSelect As Boolean
    Private _selectValue As String
    Private _pullActiveItemsOnly As Boolean
    Private _instance As AdvantageDropDownListMetadata
    Private _source As String = ""

    Public Sub New(ByVal webControl As DropDownList, ByVal ddlName As AdvantageDropDownListName, ByVal campusId As String, ByVal insertSelect As Boolean, ByVal pullActiveItemsOnly As Boolean)
        _ddl = webControl
        _ddlName = [Enum].GetName(GetType(AdvantageDropDownListName), ddlName)
        _campusId = campusId
        _insertSelect = insertSelect
        _selectValue = Guid.Empty.ToString
        _pullActiveItemsOnly = pullActiveItemsOnly
    End Sub
    Public Sub New(ByVal webControl As DropDownList, ByVal ddlName As AdvantageDropDownListName, ByVal campusId As String)
        Me.New(webControl, ddlName, campusId, False, False)
    End Sub
    Public Sub New(ByVal webControl As DropDownList, ByVal ddlName As AdvantageDropDownListName, ByVal campusId As String, ByVal insertSelect As Boolean)
        Me.New(webControl, ddlName, campusId, insertSelect, False)
    End Sub
    Public Sub New(ByVal webControl As DropDownList, ByVal ddlName As AdvantageDropDownListName, ByVal campusId As String, ByVal insertSelect As Boolean, ByVal pullActiveItemsOnly As Boolean, ByVal selectValue As String)
        Me.New(webControl, ddlName, campusId, insertSelect, pullActiveItemsOnly)
        _selectValue = selectValue
    End Sub

    Public Sub New(ByVal webControl As DropDownList, ByVal ddlMetadataName As String, ByVal campusId As String, ByVal insertSelect As Boolean, ByVal pullActiveItemsOnly As Boolean, ByVal selectValue As String, ByVal source As String)
        _ddl = webControl
        _ddlName = ddlMetadataName
        _campusId = campusId
        _insertSelect = insertSelect
        If selectValue = "" Then
            _selectValue = Guid.Empty.ToString
        Else
            _selectValue = selectValue
        End If

        _pullActiveItemsOnly = pullActiveItemsOnly
        _source = source
    End Sub

    Public ReadOnly Property WebControl() As DropDownList
        Get
            Return _ddl
        End Get
    End Property
    Public ReadOnly Property DDLName() As String
        Get
            Return _ddlName
        End Get
    End Property
    Public ReadOnly Property CampusId() As String
        Get
            Return _campusId
        End Get
    End Property
    Public ReadOnly Property InsertSelect() As Boolean
        Get
            Return _insertSelect
        End Get
    End Property
    Public ReadOnly Property SelectValue() As String
        Get
            Return _selectValue
        End Get
    End Property
    Public ReadOnly Property PullActiveItemsOnly() As Boolean
        Get
            Return _pullActiveItemsOnly
        End Get
    End Property
    Public Property Instance() As AdvantageDropDownListMetadata
        Get
            Return _instance
        End Get
        Set(ByVal value As AdvantageDropDownListMetadata)
            _instance = value
        End Set
    End Property
    Public ReadOnly Property Source() As String
        Get
            Return _source
        End Get
    End Property
End Class