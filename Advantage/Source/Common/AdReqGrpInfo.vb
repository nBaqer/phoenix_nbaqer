Public Class AdReqGrpInfo
    '
    '   Admission Requirement Group Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _ReqGrpId As String
    Private _LeadGrpId As String
    Private _Code As String
    Private _Descrip As String
    Private _StatusId As String
    Private _CampGrpId As String
    Private _IsMandatoryReqGrp As Boolean
    Private _modUser As String
    Private _modDate As DateTime
    Private _adReqTypeId As Integer
    Private _NumReqs As Integer

    'New Code Added on May 08, 2010
    Private _StuGrpId As String
    Private _GrpTypeId As String
    Private _OwnerId As String
    Private _IsPublic As Boolean
    Private _StudentId As String
    'New Code Added on May 08, 2010
    '' Code Added by Kamalesh Ahuja on June 09 2010 to resolve Mantis Issue Id 18141
    Private _IsRegHold As Boolean
    Private _IsTransHold As Boolean
    ''
    ''New Code Added By Vijay Ramteke On June 19 2010 For Mantis Id 19200
    Private _StuEnrollId As String
    ''
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _ReqGrpId = Guid.NewGuid.ToString
        _Code = ""
        _Descrip = ""
        _StatusId = FAME.AdvantageV1.Common.AdvantageCommonValues.ActiveGuid
        _CampGrpId = Guid.Empty.ToString
        _IsMandatoryReqGrp = False
        _modUser = ""
        _modDate = Date.MinValue
        _adReqTypeId = 0
        _NumReqs = 0
        _LeadGrpId = Guid.NewGuid.ToString

        'New Code Added on May 08, 2010
        _StuGrpId = Guid.NewGuid.ToString
        _GrpTypeId = Guid.Empty.ToString
        _OwnerId = Guid.Empty.ToString
        _IsPublic = False
        _StudentId = Guid.Empty.ToString
        'New Code Added on May 08, 2010
        '' Code Added by Kamalesh Ahuja on June 09 2010 to resolve Mantis Issue Id 18141
        _IsRegHold = False
        _IsTransHold = False
        ''
        ''New Code Added By Vijay Ramteke On June 19 2010 For Mantis Id 19200
        _StuEnrollId = ""
        ''
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property ReqGrpId() As String
        Get
            Return _ReqGrpId
        End Get
        Set(ByVal Value As String)
            _ReqGrpId = Value
        End Set
    End Property
    Public Property LeadgrpId() As String
        Get
            Return _LeadGrpId
        End Get
        Set(ByVal Value As String)
            _LeadGrpId = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _StatusId
        End Get
        Set(ByVal Value As String)
            _StatusId = Value
        End Set
    End Property
    Public Property adReqTypeId() As String
        Get
            Return _adReqTypeId
        End Get
        Set(ByVal Value As String)
            _adReqTypeId = Value
        End Set
    End Property
    Public Property NumReqs() As Integer
        Get
            Return _NumReqs
        End Get
        Set(ByVal Value As Integer)
            _NumReqs = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _CampGrpId
        End Get
        Set(ByVal Value As String)
            _CampGrpId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _Code
        End Get
        Set(ByVal Value As String)
            _Code = Value
        End Set
    End Property
    Public Property Descrip() As String
        Get
            Return _Descrip
        End Get
        Set(ByVal Value As String)
            _Descrip = Value
        End Set
    End Property
    Public Property IsMandatoryReqGrp() As Boolean
        Get
            Return _IsMandatoryReqGrp
        End Get
        Set(ByVal Value As Boolean)
            _IsMandatoryReqGrp = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    'New Code Added on May 08, 2010
    Public Property StuGrpId() As String
        Get
            Return _StuGrpId
        End Get
        Set(ByVal Value As String)
            _StuGrpId = Value
        End Set
    End Property
    Public Property GrpTypeId() As String
        Get
            Return _GrpTypeId
        End Get
        Set(ByVal Value As String)
            _GrpTypeId = Value
        End Set
    End Property
    Public Property OwnerId() As String
        Get
            Return _OwnerId
        End Get
        Set(ByVal Value As String)
            _OwnerId = Value
        End Set
    End Property
    Public Property IsPublic() As Boolean
        Get
            Return _IsPublic
        End Get
        Set(ByVal Value As Boolean)
            _IsPublic = Value
        End Set
    End Property
    Public Property StudentId() As String
        Get
            Return _StudentId
        End Get
        Set(ByVal Value As String)
            _StudentId = Value
        End Set
    End Property
    'New Code Added on May 08, 2010
    '' Code Added by Kamalesh Ahuja on June 09 2010 to resolve Mantis Issue Id 18141
    Public Property IsRegHold() As Boolean
        Get
            Return _IsRegHold
        End Get
        Set(ByVal Value As Boolean)
            _IsRegHold = Value
        End Set
    End Property
    Public Property IsTransHold() As Boolean
        Get
            Return _IsTransHold
        End Get
        Set(ByVal Value As Boolean)
            _IsTransHold = Value
        End Set
    End Property
    ''
    ''New Code Added By Vijay Ramteke On June 19, 2010 Form Mantis Id 19199
    Public Property StuEnrollId() As String
        Get
            Return _StuEnrollId
        End Get
        Set(ByVal Value As String)
            _StuEnrollId = Value
        End Set
    End Property
    ''New Code Added By Vijay Ramteke On June 19, 2010 Form Mantis Id 19199
#End Region

End Class
