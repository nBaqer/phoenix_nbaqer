﻿Public Class FAMELinkFileInfo
#Region " Private Variables and Objects"
    Private _FileName As String 'includes the file extension such as clfahead.dat and cffahead.001
    Private _FileType As String
    Private _CreationDate As Date
    Private _RecordCount As Integer
    Private _Amount As Decimal
#End Region

#Region " Public Constructors "

#End Region

#Region " Public Properties"
    Public Property FileName() As String
        Get
            Return _FileName
        End Get
        Set(ByVal value As String)
            _FileName = value
        End Set
    End Property

    Public Property FileType() As String
        Get
            Return _FileType
        End Get
        Set(ByVal Value As String)
            _FileType = Value
        End Set
    End Property

    Public Property CreationDate() As Date
        Get
            Return _CreationDate
        End Get
        Set(ByVal value As Date)
            _CreationDate = value
        End Set
    End Property

    Public Property RecordCount() As Integer
        Get
            Return _RecordCount
        End Get
        Set(ByVal value As Integer)
            _RecordCount = value
        End Set
    End Property

    Public Property Amount() As Decimal
        Get
            Return _Amount
        End Get
        Set(ByVal value As Decimal)
            _Amount = value
        End Set
    End Property


#Region " Public Methods"
    Public Overrides Function ToString() As String
        Return Me.FileName
    End Function
#End Region



#End Region

End Class
