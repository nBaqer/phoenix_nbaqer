Public Class ReportingServices
    Inherits System.ComponentModel.Component

#Region " Component Designer generated code "

    Public Sub New(Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    Friend WithEvents OleDbDataAdapter1 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents OleDbSelectCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbConnection1 As System.Data.OleDb.OleDbConnection
    Friend WithEvents DsLeadMasterDetail1 As FAME.AdvantageV1.Common.dsLeadMasterDetail
    Friend WithEvents OleDbDataAdapter2 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents OleDbSelectCommand2 As System.Data.OleDb.OleDbCommand
    Friend WithEvents DsInquiryByDay1 As FAME.AdvantageV1.Common.dsInquiryByDay
    Friend WithEvents OleDbDataAdapter3 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents OleDbSelectCommand3 As System.Data.OleDb.OleDbCommand
    Friend WithEvents DsCostPerLead1 As FAME.AdvantageV1.Common.dsCostPerLead
    Friend WithEvents OleDbDataAdapter4 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents OleDbSelectCommand4 As System.Data.OleDb.OleDbCommand
    Friend WithEvents DsEmployerMaster1 As FAME.AdvantageV1.Common.dsEmployerMaster
    Friend WithEvents OleDbDataAdapter5 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents OleDbSelectCommand5 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbDataAdapter6 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents OleDbSelectCommand6 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbDataAdapter7 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents OleDbSelectCommand7 As System.Data.OleDb.OleDbCommand
    Friend WithEvents DsPlacementStats1 As FAME.AdvantageV1.Common.dsPlacementStats
    Friend WithEvents OleDbDataAdapter8 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents OleDbSelectCommand8 As System.Data.OleDb.OleDbCommand
    Friend WithEvents DsStatsByField1 As FAME.AdvantageV1.Common.dsStatsByField
    Friend WithEvents OleDbDataAdapter9 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents OleDbSelectCommand9 As System.Data.OleDb.OleDbCommand
    Friend WithEvents DsMiscStats1 As FAME.AdvantageV1.Common.dsMiscStats
    Friend WithEvents OleDbDataAdapter10 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents OleDbSelectCommand10 As System.Data.OleDb.OleDbCommand
    Friend WithEvents DsLeadMasterSummary1 As FAME.AdvantageV1.Common.dsLeadMasterSummary
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.OleDbDataAdapter1 = New System.Data.OleDb.OleDbDataAdapter
        Me.OleDbSelectCommand1 = New System.Data.OleDb.OleDbCommand
        Me.OleDbConnection1 = New System.Data.OleDb.OleDbConnection
        Me.DsLeadMasterDetail1 = New FAME.AdvantageV1.Common.dsLeadMasterDetail
        Me.OleDbDataAdapter2 = New System.Data.OleDb.OleDbDataAdapter
        Me.OleDbSelectCommand2 = New System.Data.OleDb.OleDbCommand
        Me.DsInquiryByDay1 = New FAME.AdvantageV1.Common.dsInquiryByDay
        Me.OleDbDataAdapter3 = New System.Data.OleDb.OleDbDataAdapter
        Me.OleDbSelectCommand3 = New System.Data.OleDb.OleDbCommand
        Me.DsCostPerLead1 = New FAME.AdvantageV1.Common.dsCostPerLead
        Me.OleDbDataAdapter4 = New System.Data.OleDb.OleDbDataAdapter
        Me.OleDbSelectCommand4 = New System.Data.OleDb.OleDbCommand
        Me.DsEmployerMaster1 = New FAME.AdvantageV1.Common.dsEmployerMaster
        Me.OleDbDataAdapter5 = New System.Data.OleDb.OleDbDataAdapter
        Me.OleDbSelectCommand5 = New System.Data.OleDb.OleDbCommand
        Me.OleDbDataAdapter6 = New System.Data.OleDb.OleDbDataAdapter
        Me.OleDbSelectCommand6 = New System.Data.OleDb.OleDbCommand
        Me.OleDbDataAdapter7 = New System.Data.OleDb.OleDbDataAdapter
        Me.OleDbSelectCommand7 = New System.Data.OleDb.OleDbCommand
        Me.DsPlacementStats1 = New FAME.AdvantageV1.Common.dsPlacementStats
        Me.OleDbDataAdapter8 = New System.Data.OleDb.OleDbDataAdapter
        Me.OleDbSelectCommand8 = New System.Data.OleDb.OleDbCommand
        Me.DsStatsByField1 = New FAME.AdvantageV1.Common.dsStatsByField
        Me.OleDbDataAdapter9 = New System.Data.OleDb.OleDbDataAdapter
        Me.OleDbSelectCommand9 = New System.Data.OleDb.OleDbCommand
        Me.DsMiscStats1 = New FAME.AdvantageV1.Common.dsMiscStats
        Me.OleDbDataAdapter10 = New System.Data.OleDb.OleDbDataAdapter
        Me.OleDbSelectCommand10 = New System.Data.OleDb.OleDbCommand
        Me.DsLeadMasterSummary1 = New FAME.AdvantageV1.Common.dsLeadMasterSummary
        CType(Me.DsLeadMasterDetail1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsInquiryByDay1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsCostPerLead1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsEmployerMaster1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsPlacementStats1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsStatsByField1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsMiscStats1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsLeadMasterSummary1, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'OleDbDataAdapter1
        '
        Me.OleDbDataAdapter1.SelectCommand = Me.OleDbSelectCommand1
        Me.OleDbDataAdapter1.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "adLeads", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("LeadName", "LeadName"), New System.Data.Common.DataColumnMapping("City", "City"), New System.Data.Common.DataColumnMapping("StateId", "StateId"), New System.Data.Common.DataColumnMapping("StateDescrip", "StateDescrip"), New System.Data.Common.DataColumnMapping("Zip", "Zip"), New System.Data.Common.DataColumnMapping("CreatedDate", "CreatedDate"), New System.Data.Common.DataColumnMapping("Country", "Country"), New System.Data.Common.DataColumnMapping("HomePhone", "HomePhone"), New System.Data.Common.DataColumnMapping("LeadStatus", "LeadStatus"), New System.Data.Common.DataColumnMapping("StatusCodeDescrip", "StatusCodeDescrip"), New System.Data.Common.DataColumnMapping("ExpectedStart", "ExpectedStart"), New System.Data.Common.DataColumnMapping("ProgramID", "ProgramID"), New System.Data.Common.DataColumnMapping("Program", "Program"), New System.Data.Common.DataColumnMapping("SourceCategoryID", "SourceCategoryID"), New System.Data.Common.DataColumnMapping("SourceCateg", "SourceCateg"), New System.Data.Common.DataColumnMapping("SourceTypeID", "SourceTypeID"), New System.Data.Common.DataColumnMapping("SourceType", "SourceType"), New System.Data.Common.DataColumnMapping("SourceAdvertisement", "SourceAdvertisement"), New System.Data.Common.DataColumnMapping("SourceAdvert", "SourceAdvert"), New System.Data.Common.DataColumnMapping("AdmissionsRep", "AdmissionsRep"), New System.Data.Common.DataColumnMapping("AdmissionsRepName", "AdmissionsRepName")})})
        '
        'OleDbSelectCommand1
        '
        Me.OleDbSelectCommand1.CommandText = "SELECT adLeads.LastName + ', ' + adLeads.FirstName + ' ' + adLeads.MiddleName AS " & _
        "LeadName, adLeads.City, adLeads.StateId, syStates.StateDescrip, adLeads.Zip, adL" & _
        "eads.CreatedDate, (SELECT CountryDescrip FROM syCountries WHERE CountryId = adLe" & _
        "ads.Country) AS Country, adLeads.HomePhone, adLeads.LeadStatus, syStatusCodes.St" & _
        "atusCodeDescrip, adLeads.ExpectedStart, adLeads.ProgramID, (SELECT ProgDescrip F" & _
        "ROM arPrograms WHERE ProgId = adLeads.ProgramID) AS Program, adLeads.SourceCateg" & _
        "oryID, (SELECT SourceCatagoryDescrip FROM adSourceCatagory WHERE SourceCatagoryI" & _
        "D = adLeads.SourceCategoryID) AS SourceCateg, adLeads.SourceTypeID, (SELECT Sour" & _
        "ceTypeDescrip FROM adSourceType WHERE SourceTypeID = adLeads.SourceTypeID) AS So" & _
        "urceType, adLeads.SourceAdvertisement, (SELECT sourceadvdescrip FROM adSourceAdv" & _
        "ertisement WHERE sourceadvid = adLeads.SourceAdvertisement) AS SourceAdvert, adL" & _
        "eads.AdmissionsRep, (SELECT LastName + ', ' + FirstName + ' ' + MI FROM hrEmploy" & _
        "ees WHERE EmpID = adLeads.AdmissionsRep) AS AdmissionsRepName FROM adLeads INNER" & _
        " JOIN syStatusCodes ON adLeads.LeadStatus = syStatusCodes.StatusCodeId INNER JOI" & _
        "N syStates ON adLeads.StateId = syStates.StateId WHERE (syStatusCodes.StatusCode" & _
        "Descrip <> 'Enrolled')"
        Me.OleDbSelectCommand1.Connection = Me.OleDbConnection1
        '
        'OleDbConnection1
        '
        Me.OleDbConnection1.ConnectionString = "Integrated Security=SSPI;Packet Size=4096;Data Source=FAMEDEV;Tag with column col" & _
        "lation when possible=False;Initial Catalog=AdvantageV1;Use Procedure for Prepare" & _
        "=1;Auto Translate=True;Persist Security Info=False;Provider=""SQLOLEDB.1"";Worksta" & _
        "tion ID=PARRALES;Use Encryption for Data=False"
        '
        'DsLeadMasterDetail1
        '
        Me.DsLeadMasterDetail1.DataSetName = "dsLeadMasterDetail"
        Me.DsLeadMasterDetail1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'OleDbDataAdapter2
        '
        Me.OleDbDataAdapter2.SelectCommand = Me.OleDbSelectCommand2
        Me.OleDbDataAdapter2.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "adSourceAdvertisement", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SourceCatagoryDescrip", "SourceCatagoryDescrip"), New System.Data.Common.DataColumnMapping("SourceTypeDescrip", "SourceTypeDescrip"), New System.Data.Common.DataColumnMapping("SourceAdvDescrip", "SourceAdvDescrip"), New System.Data.Common.DataColumnMapping("CreatedDate", "CreatedDate"), New System.Data.Common.DataColumnMapping("Inquiries", "Inquiries")})})
        '
        'OleDbSelectCommand2
        '
        Me.OleDbSelectCommand2.CommandText = "SELECT adSourceCatagory.SourceCatagoryDescrip, adSourceType.SourceTypeDescrip, ad" & _
        "SourceAdvertisement.SourceAdvDescrip, adLeads.CreatedDate, COUNT(*) AS Inquiries" & _
        " FROM adSourceAdvertisement INNER JOIN adLeads ON adSourceAdvertisement.SourceAd" & _
        "vId = adLeads.SourceAdvertisement INNER JOIN adSourceType ON adSourceAdvertiseme" & _
        "nt.SourceTypeId = adSourceType.SourceTypeId INNER JOIN adSourceCatagory ON adSou" & _
        "rceType.SourceCatagoryId = adSourceCatagory.SourceCatagoryId GROUP BY adSourceCa" & _
        "tagory.SourceCatagoryDescrip, adSourceType.SourceTypeDescrip, adSourceAdvertisem" & _
        "ent.SourceAdvDescrip, adLeads.CreatedDate ORDER BY adSourceCatagory.SourceCatago" & _
        "ryDescrip, adSourceType.SourceTypeDescrip, adSourceAdvertisement.SourceAdvDescri" & _
        "p, adLeads.CreatedDate"
        Me.OleDbSelectCommand2.Connection = Me.OleDbConnection1
        '
        'DsInquiryByDay1
        '
        Me.DsInquiryByDay1.DataSetName = "dsInquiryByDay"
        Me.DsInquiryByDay1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'OleDbDataAdapter3
        '
        Me.OleDbDataAdapter3.SelectCommand = Me.OleDbSelectCommand3
        Me.OleDbDataAdapter3.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "adSourceAdvertisement", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SourceCatagoryDescrip", "SourceCatagoryDescrip"), New System.Data.Common.DataColumnMapping("SourceTypeDescrip", "SourceTypeDescrip"), New System.Data.Common.DataColumnMapping("SourceAdvDescrip", "SourceAdvDescrip"), New System.Data.Common.DataColumnMapping("cost", "cost"), New System.Data.Common.DataColumnMapping("startdate", "startdate"), New System.Data.Common.DataColumnMapping("enddate", "enddate"), New System.Data.Common.DataColumnMapping("AdvIntervalDescrip", "AdvIntervalDescrip"), New System.Data.Common.DataColumnMapping("Status", "Status"), New System.Data.Common.DataColumnMapping("NumberOfLeads", "NumberOfLeads")})})
        '
        'OleDbSelectCommand3
        '
        Me.OleDbSelectCommand3.CommandText = "SELECT adSourceCatagory.SourceCatagoryDescrip, adSourceType.SourceTypeDescrip, ad" & _
        "SourceAdvertisement.SourceAdvDescrip, adSourceAdvertisement.cost, adSourceAdvert" & _
        "isement.startdate, adSourceAdvertisement.enddate, adAdvInterval.AdvIntervalDescr" & _
        "ip, syStatuses.Status, (SELECT COUNT(*) FROM adLeads WHERE SourceAdvertisement =" & _
        " adSourceAdvertisement.sourceadvid) AS NumberOfLeads FROM adSourceAdvertisement " & _
        "INNER JOIN adSourceType ON adSourceAdvertisement.SourceTypeId = adSourceType.Sou" & _
        "rceTypeId INNER JOIN adSourceCatagory ON adSourceType.SourceCatagoryId = adSourc" & _
        "eCatagory.SourceCatagoryId INNER JOIN adAdvInterval ON adSourceAdvertisement.Adv" & _
        "IntervalId = adAdvInterval.AdvIntervalId INNER JOIN syStatuses ON adAdvInterval." & _
        "StatusId = syStatuses.StatusId ORDER BY adSourceCatagory.SourceCatagoryDescrip, " & _
        "adSourceType.SourceTypeDescrip, adSourceAdvertisement.SourceAdvDescrip"
        Me.OleDbSelectCommand3.Connection = Me.OleDbConnection1
        '
        'DsCostPerLead1
        '
        Me.DsCostPerLead1.DataSetName = "dsCostPerLead"
        Me.DsCostPerLead1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'OleDbDataAdapter4
        '
        Me.OleDbDataAdapter4.SelectCommand = Me.OleDbSelectCommand4
        Me.OleDbDataAdapter4.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "plEmployers", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("EmployerId", "EmployerId"), New System.Data.Common.DataColumnMapping("EmployerDescrip", "EmployerDescrip"), New System.Data.Common.DataColumnMapping("Code", "Code"), New System.Data.Common.DataColumnMapping("Address1", "Address1"), New System.Data.Common.DataColumnMapping("ParentId", "ParentId"), New System.Data.Common.DataColumnMapping("Parent", "Parent"), New System.Data.Common.DataColumnMapping("IndustryDescrip", "IndustryDescrip"), New System.Data.Common.DataColumnMapping("Phone", "Phone"), New System.Data.Common.DataColumnMapping("Fax", "Fax"), New System.Data.Common.DataColumnMapping("Email", "Email"), New System.Data.Common.DataColumnMapping("ModDate", "ModDate"), New System.Data.Common.DataColumnMapping("StateDescrip", "StateDescrip"), New System.Data.Common.DataColumnMapping("CampGrpDescrip", "CampGrpDescrip")})})
        '
        'OleDbSelectCommand4
        '
        Me.OleDbSelectCommand4.CommandText = "SELECT DISTINCT plEmployers.EmployerId, plEmployers.EmployerDescrip, plEmployers." & _
        "Code, plEmployers.Address1, plEmployers.ParentId, (SELECT A.EmployerDescrip FROM" & _
        " plEmployers A WHERE A.EmployerID = plEmployers.ParentID) AS Parent, plIndustrie" & _
        "s.IndustryDescrip, plEmployers.Phone, plEmployers.Fax, plEmployers.Email, plEmpl" & _
        "oyers.ModDate, syStates.StateDescrip, syCampGrps.CampGrpDescrip FROM plEmployers" & _
        " INNER JOIN syStates ON plEmployers.StateId = syStates.StateId INNER JOIN syStat" & _
        "uses ON plEmployers.StatusId = syStatuses.StatusId INNER JOIN plIndustries ON pl" & _
        "Employers.IndustryId = plIndustries.IndustryId INNER JOIN syCampGrps ON plEmploy" & _
        "ers.CampGrpId = syCampGrps.CampGrpId"
        Me.OleDbSelectCommand4.Connection = Me.OleDbConnection1
        '
        'DsEmployerMaster1
        '
        Me.DsEmployerMaster1.DataSetName = "dsEmployerMaster"
        Me.DsEmployerMaster1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'OleDbDataAdapter5
        '
        Me.OleDbDataAdapter5.SelectCommand = Me.OleDbSelectCommand5
        Me.OleDbDataAdapter5.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "plEmployerJobs", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("StatusId", "StatusId"), New System.Data.Common.DataColumnMapping("EmployerJobId", "EmployerJobId"), New System.Data.Common.DataColumnMapping("Status", "Status"), New System.Data.Common.DataColumnMapping("Code", "Code"), New System.Data.Common.DataColumnMapping("JobTitleDescrip", "JobTitleDescrip"), New System.Data.Common.DataColumnMapping("JobTitleId", "JobTitleId"), New System.Data.Common.DataColumnMapping("JobDescription", "JobDescription"), New System.Data.Common.DataColumnMapping("EmployerCode", "EmployerCode"), New System.Data.Common.DataColumnMapping("EmployerDescrip", "EmployerDescrip"), New System.Data.Common.DataColumnMapping("JobCatDescrip", "JobCatDescrip"), New System.Data.Common.DataColumnMapping("JobGroupId", "JobGroupId"), New System.Data.Common.DataColumnMapping("FirstName", "FirstName"), New System.Data.Common.DataColumnMapping("MiddleName", "MiddleName"), New System.Data.Common.DataColumnMapping("LastName", "LastName"), New System.Data.Common.DataColumnMapping("ContactId", "ContactId"), New System.Data.Common.DataColumnMapping("TypeId", "TypeId"), New System.Data.Common.DataColumnMapping("AreaId", "AreaId"), New System.Data.Common.DataColumnMapping("WorkDays", "WorkDays"), New System.Data.Common.DataColumnMapping("NumberOpen", "NumberOpen"), New System.Data.Common.DataColumnMapping("NumberFilled", "NumberFilled"), New System.Data.Common.DataColumnMapping("OpenedFrom", "OpenedFrom"), New System.Data.Common.DataColumnMapping("OpenedTo", "OpenedTo"), New System.Data.Common.DataColumnMapping("SalaryFrom", "SalaryFrom"), New System.Data.Common.DataColumnMapping("SalaryTo", "SalaryTo"), New System.Data.Common.DataColumnMapping("HoursFrom", "HoursFrom"), New System.Data.Common.DataColumnMapping("HoursTo", "HoursTo"), New System.Data.Common.DataColumnMapping("Notes", "Notes"), New System.Data.Common.DataColumnMapping("Start", "Start"), New System.Data.Common.DataColumnMapping("FeeDescrip", "FeeDescrip"), New System.Data.Common.DataColumnMapping("FeeId", "FeeId"), New System.Data.Common.DataColumnMapping("BenefitsId", "BenefitsId"), New System.Data.Common.DataColumnMapping("ScheduleDescrip", "ScheduleDescrip"), New System.Data.Common.DataColumnMapping("ScheduleId", "ScheduleId"), New System.Data.Common.DataColumnMapping("EmployerId", "EmployerId"), New System.Data.Common.DataColumnMapping("CampGrpId", "CampGrpId"), New System.Data.Common.DataColumnMapping("CampGrpDescrip", "CampGrpDescrip"), New System.Data.Common.DataColumnMapping("JobRequirements", "JobRequirements"), New System.Data.Common.DataColumnMapping("SalaryTypeID", "SalaryTypeID")})})
        '
        'OleDbSelectCommand5
        '
        Me.OleDbSelectCommand5.CommandText = "SELECT DISTINCT plEmployerJobs.StatusId, plEmployerJobs.EmployerJobId, (SELECT TO" & _
        "P 1 Status FROM syStatuses WHERE StatusId = plEmployerJobs.StatusId) AS Status, " & _
        "plEmployerJobs.Code AS Code, (SELECT TOP 1 JobTitleDescrip FROM plJobTitles WHER" & _
        "E JobTitleId = plEmployerJobs.JobTitleID) AS JobTitleDescrip, plEmployerJobs.Job" & _
        "TitleId, plEmployerJobs.JobDescription, (SELECT TOP 1 Code FROM plEmployers WHER" & _
        "E EmployerID = plEmployerJobs.EmployerID) AS EmployerCode, (SELECT TOP 1 Employe" & _
        "rDescrip FROM plEmployers WHERE EmployerID = plEmployerJobs.EmployerID) AS Emplo" & _
        "yerDescrip, (SELECT TOP 1 JobCatDescrip FROM plJobCats WHERE JobCatID = plEmploy" & _
        "erJobs.JobGroupID) AS JobCatDescrip, plEmployerJobs.JobGroupId, (SELECT TOP 1 Fi" & _
        "rstName FROM plEmployerContact WHERE EmployerContactID = plEmployerJobs.ContactI" & _
        "D) AS FirstName, (SELECT TOP 1 MiddleName FROM plEmployerContact WHERE EmployerC" & _
        "ontactID = plEmployerJobs.ContactID) AS MiddleName, (SELECT TOP 1 LastName FROM " & _
        "plEmployerContact WHERE EmployerContactID = plEmployerJobs.ContactID) AS LastNam" & _
        "e, plEmployerJobs.ContactId, plEmployerJobs.TypeId, plEmployerJobs.AreaId, plEmp" & _
        "loyerJobs.WorkDays, plEmployerJobs.NumberOpen, plEmployerJobs.NumberFilled, plEm" & _
        "ployerJobs.OpenedFrom, plEmployerJobs.OpenedTo, plEmployerJobs.SalaryFrom, plEmp" & _
        "loyerJobs.SalaryTo, plEmployerJobs.HoursFrom, plEmployerJobs.HoursTo, plEmployer" & _
        "Jobs.Notes, plEmployerJobs.Start, (SELECT TOP 1 FeeDescrip FROM plFee WHERE FeeI" & _
        "D = plEmployerJobs.FeeID) AS FeeDescrip, plEmployerJobs.FeeId, plEmployerJobs.Be" & _
        "nefitsId, (SELECT TOP 1 JobScheduleDescrip FROM plJobSchedule WHERE ScheduleID =" & _
        " plEmployerJobs.ScheduleID) AS ScheduleDescrip, plEmployerJobs.ScheduleId, plEmp" & _
        "loyerJobs.EmployerId, plEmployerJobs.CampGrpId, (SELECT TOP 1 CampGrpDescrip FRO" & _
        "M syCampGrps WHERE CampGrpId = plEmployerJobs.CampGrpId) AS CampGrpDescrip, plEm" & _
        "ployerJobs.JobRequirements, plEmployerJobs.SalaryTypeID FROM plEmployerJobs INNE" & _
        "R JOIN syStatuses ON plEmployerJobs.StatusId = syStatuses.StatusId INNER JOIN pl" & _
        "JobTitles ON plEmployerJobs.JobTitleId = plJobTitles.JobTitleId CROSS JOIN plJob" & _
        "Cats"
        Me.OleDbSelectCommand5.Connection = Me.OleDbConnection1
        '
        'OleDbDataAdapter6
        '
        Me.OleDbDataAdapter6.SelectCommand = Me.OleDbSelectCommand6
        Me.OleDbDataAdapter6.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "plEmployerJobs", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("EmployerDescrip", "EmployerDescrip"), New System.Data.Common.DataColumnMapping("Code", "Code"), New System.Data.Common.DataColumnMapping("EmployerId", "EmployerId"), New System.Data.Common.DataColumnMapping("JobTitleDescrip", "JobTitleDescrip"), New System.Data.Common.DataColumnMapping("Address1", "Address1"), New System.Data.Common.DataColumnMapping("Address2", "Address2"), New System.Data.Common.DataColumnMapping("City", "City"), New System.Data.Common.DataColumnMapping("State", "State"), New System.Data.Common.DataColumnMapping("Zip", "Zip"), New System.Data.Common.DataColumnMapping("FirstName", "FirstName"), New System.Data.Common.DataColumnMapping("MiddleName", "MiddleName"), New System.Data.Common.DataColumnMapping("LastName", "LastName"), New System.Data.Common.DataColumnMapping("WorkPhone", "WorkPhone"), New System.Data.Common.DataColumnMapping("HomePhone", "HomePhone"), New System.Data.Common.DataColumnMapping("CellPhone", "CellPhone"), New System.Data.Common.DataColumnMapping("Beeper", "Beeper"), New System.Data.Common.DataColumnMapping("WorkEmail", "WorkEmail"), New System.Data.Common.DataColumnMapping("HomeEmail", "HomeEmail"), New System.Data.Common.DataColumnMapping("ContactId", "ContactId")})})
        '
        'OleDbSelectCommand6
        '
        Me.OleDbSelectCommand6.CommandText = "SELECT DISTINCT plEmployers.EmployerDescrip, plEmployers.Code, plEmployerJobs.Emp" & _
        "loyerId, plJobTitles.JobTitleDescrip, (SELECT Address1 FROM plEmployerContact WH" & _
        "ERE EmployerContactID = plEmployerJobs.ContactID) AS Address1, (SELECT Address2 " & _
        "FROM plEmployerContact WHERE EmployerContactID = plEmployerJobs.ContactID) AS Ad" & _
        "dress2, (SELECT City FROM plEmployerContact WHERE EmployerContactID = plEmployer" & _
        "Jobs.ContactID) AS City, (SELECT State FROM plEmployerContact WHERE EmployerCont" & _
        "actID = plEmployerJobs.ContactID) AS State, (SELECT Zip FROM plEmployerContact W" & _
        "HERE EmployerContactID = plEmployerJobs.ContactID) AS Zip, (SELECT FirstName FRO" & _
        "M plEmployerContact WHERE EmployerContactID = plEmployerJobs.ContactID) AS First" & _
        "Name, (SELECT MiddleName FROM plEmployerContact WHERE EmployerContactID = plEmpl" & _
        "oyerJobs.ContactID) AS MiddleName, (SELECT LastName FROM plEmployerContact WHERE" & _
        " EmployerContactID = plEmployerJobs.ContactID) AS LastName, (SELECT WorkPhone FR" & _
        "OM plEmployerContact WHERE EmployerContactID = plEmployerJobs.ContactID) AS Work" & _
        "Phone, (SELECT HomePhone FROM plEmployerContact WHERE EmployerContactID = plEmpl" & _
        "oyerJobs.ContactID) AS HomePhone, (SELECT CellPhone FROM plEmployerContact WHERE" & _
        " EmployerContactID = plEmployerJobs.ContactID) AS CellPhone, (SELECT Beeper FROM" & _
        " plEmployerContact WHERE EmployerContactID = plEmployerJobs.ContactID) AS Beeper" & _
        ", (SELECT WorkEmail FROM plEmployerContact WHERE EmployerContactID = plEmployerJ" & _
        "obs.ContactID) AS WorkEmail, (SELECT HomeEmail FROM plEmployerContact WHERE Empl" & _
        "oyerContactID = plEmployerJobs.ContactID) AS HomeEmail, plEmployerJobs.ContactId" & _
        " FROM plEmployerJobs INNER JOIN plEmployers ON plEmployerJobs.EmployerId = plEmp" & _
        "loyers.EmployerId INNER JOIN plJobTitles ON plEmployerJobs.JobTitleId = plJobTit" & _
        "les.JobTitleId CROSS JOIN plEmployerContact"
        Me.OleDbSelectCommand6.Connection = Me.OleDbConnection1
        '
        'OleDbDataAdapter7
        '
        Me.OleDbDataAdapter7.SelectCommand = Me.OleDbSelectCommand7
        Me.OleDbDataAdapter7.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "PlStudentsPlaced", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("EnrollmentId", "EnrollmentId"), New System.Data.Common.DataColumnMapping("PrgVerDescrip", "PrgVerDescrip"), New System.Data.Common.DataColumnMapping("PrgVerCode", "PrgVerCode"), New System.Data.Common.DataColumnMapping("PlacedGraduates", "PlacedGraduates"), New System.Data.Common.DataColumnMapping("ActualGraduates", "ActualGraduates"), New System.Data.Common.DataColumnMapping("ScheduledGraduates", "ScheduledGraduates"), New System.Data.Common.DataColumnMapping("ExitInterView", "ExitInterView"), New System.Data.Common.DataColumnMapping("InEligibleGraduates", "InEligibleGraduates"), New System.Data.Common.DataColumnMapping("EligibleGraduates", "EligibleGraduates"), New System.Data.Common.DataColumnMapping("WaivedGraduates", "WaivedGraduates")})})
        '
        'OleDbSelectCommand7
        '
        Me.OleDbSelectCommand7.CommandText = "SELECT DISTINCT PlStudentsPlaced.EnrollmentId, arPrgVersions.PrgVerDescrip, arPrg" & _
        "Versions.PrgVerCode, (SELECT COUNT(*) FROM plStudentsPlaced WHERE EnrollmentID =" & _
        " PlStudentsPlaced.EnrollmentID) AS PlacedGraduates, (SELECT COUNT(*) FROM plStud" & _
        "entsPlaced WHERE datediff(d , GraduatedDate , Placeddate) >= 1 AND EnrollmentID " & _
        "= PlStudentsPlaced.EnrollmentID) AS ActualGraduates, (SELECT COUNT(*) FROM plStu" & _
        "dentsPlaced WHERE datediff(d , GraduatedDate , Placeddate) < 1 AND EnrollmentID " & _
        "= PlStudentsPlaced.EnrollmentID) AS ScheduledGraduates, (SELECT COUNT(*) FROM pl" & _
        "ExitInterView WHERE EnrollmentID = PlStudentsPlaced.EnrollmentID) AS ExitInterVi" & _
        "ew, (SELECT COUNT(*) FROM plExitInterView WHERE EnrollmentID = PlStudentsPlaced." & _
        "EnrollmentID AND Eligible = 'No') AS InEligibleGraduates, (SELECT COUNT(*) FROM " & _
        "plExitInterView WHERE EnrollmentID = PlStudentsPlaced.EnrollmentID AND Eligible " & _
        "= 'Yes') AS EligibleGraduates, (SELECT COUNT(*) FROM plExitInterView WHERE Enrol" & _
        "lmentID = PlStudentsPlaced.EnrollmentID AND WaiverSigned = 'Yes') AS WaivedGradu" & _
        "ates FROM PlStudentsPlaced INNER JOIN arPrgVersions ON PlStudentsPlaced.Enrollme" & _
        "ntId = arPrgVersions.PrgVerId ORDER BY arPrgVersions.PrgVerDescrip"
        Me.OleDbSelectCommand7.Connection = Me.OleDbConnection1
        '
        'DsPlacementStats1
        '
        Me.DsPlacementStats1.DataSetName = "dsPlacementStats"
        Me.DsPlacementStats1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'OleDbDataAdapter8
        '
        Me.OleDbDataAdapter8.SelectCommand = Me.OleDbSelectCommand8
        Me.OleDbDataAdapter8.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "PlStudentsPlaced", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("EnrollmentId", "EnrollmentId"), New System.Data.Common.DataColumnMapping("PrgVerDescrip", "PrgVerDescrip"), New System.Data.Common.DataColumnMapping("PrgVerCode", "PrgVerCode"), New System.Data.Common.DataColumnMapping("EligibleGraduates", "EligibleGraduates"), New System.Data.Common.DataColumnMapping("PlacedGraduates", "PlacedGraduates"), New System.Data.Common.DataColumnMapping("InFieldOfStudy", "InFieldOfStudy"), New System.Data.Common.DataColumnMapping("OutFieldOfStudy", "OutFieldOfStudy"), New System.Data.Common.DataColumnMapping("RelatedFieldOfStudy", "RelatedFieldOfStudy")})})
        '
        'OleDbSelectCommand8
        '
        Me.OleDbSelectCommand8.CommandText = "SELECT DISTINCT PlStudentsPlaced.EnrollmentId, arPrgVersions.PrgVerDescrip, arPrg" & _
        "Versions.PrgVerCode, (SELECT COUNT(*) FROM plExitInterView WHERE EnrollmentID = " & _
        "plStudentsPlaced.EnrollmentID AND Eligible = 'Yes') AS EligibleGraduates, (SELEC" & _
        "T COUNT(*) FROM plStudentsPlaced WHERE EnrollmentID = plStudentsPlaced.Enrollmen" & _
        "tID) AS PlacedGraduates, (SELECT COUNT(*) FROM plStudentsPlaced WHERE Enrollment" & _
        "ID = plStudentsPlaced.EnrollmentID AND FldStudyID IN (SELECT FldStudyId FROM plF" & _
        "ldStudy WHERE FldStudyDescrip = 'Yes')) AS InFieldOfStudy, (SELECT COUNT(*) FROM" & _
        " plStudentsPlaced WHERE EnrollmentID = plStudentsPlaced.EnrollmentID AND FldStud" & _
        "yID IN (SELECT FldStudyId FROM plFldStudy WHERE FldStudyDescrip = 'No')) AS OutF" & _
        "ieldOfStudy, (SELECT COUNT(*) FROM plStudentsPlaced WHERE EnrollmentID = plStude" & _
        "ntsPlaced.EnrollmentID AND FldStudyID IN (SELECT FldStudyId FROM plFldStudy WHER" & _
        "E FldStudyDescrip = 'Related')) AS RelatedFieldOfStudy FROM PlStudentsPlaced INN" & _
        "ER JOIN arPrgVersions ON PlStudentsPlaced.EnrollmentId = arPrgVersions.PrgVerId " & _
        "ORDER BY arPrgVersions.PrgVerDescrip"
        Me.OleDbSelectCommand8.Connection = Me.OleDbConnection1
        '
        'DsStatsByField1
        '
        Me.DsStatsByField1.DataSetName = "dsStatsByField"
        Me.DsStatsByField1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'OleDbDataAdapter9
        '
        Me.OleDbDataAdapter9.SelectCommand = Me.OleDbSelectCommand9
        Me.OleDbDataAdapter9.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "PlStudentsPlaced", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("EnrollmentId", "EnrollmentId"), New System.Data.Common.DataColumnMapping("PrgVerDescrip", "PrgVerDescrip"), New System.Data.Common.DataColumnMapping("PrgVerCode", "PrgVerCode"), New System.Data.Common.DataColumnMapping("EnrollmentCount", "EnrollmentCount"), New System.Data.Common.DataColumnMapping("Salary", "Salary"), New System.Data.Common.DataColumnMapping("SalaryTypeId", "SalaryTypeId"), New System.Data.Common.DataColumnMapping("HourWage", "HourWage"), New System.Data.Common.DataColumnMapping("YearWage", "YearWage"), New System.Data.Common.DataColumnMapping("HowPlacedId", "HowPlacedId"), New System.Data.Common.DataColumnMapping("HowPlaced", "HowPlaced"), New System.Data.Common.DataColumnMapping("ScheduleId", "ScheduleId"), New System.Data.Common.DataColumnMapping("JobSchedule", "JobSchedule")})})
        '
        'OleDbSelectCommand9
        '
        Me.OleDbSelectCommand9.CommandText = "SELECT DISTINCT PlStudentsPlaced.EnrollmentId, arPrgVersions.PrgVerDescrip, arPrg" & _
        "Versions.PrgVerCode, (SELECT COUNT(*) FROM plStudentsPlaced WHERE EnrollmentID =" & _
        " plStudentsPlaced.EnrollmentID) AS EnrollmentCount, PlStudentsPlaced.Salary, PlS" & _
        "tudentsPlaced.SalaryTypeId, (SELECT AVG(Salary) FROM plStudentsPlaced WHERE Sala" & _
        "ryTypeID = plStudentsPlaced.SalaryTypeID AND SalaryTypeId IN (SELECT SalaryTypeI" & _
        "D FROM plSalaryType WHERE SalaryTypeDescrip = 'Per Hour')) AS HourWage, (SELECT " & _
        "AVG(Salary) FROM plStudentsPlaced WHERE SalaryTypeID = plStudentsPlaced.SalaryTy" & _
        "peID AND SalaryTypeId IN (SELECT SalaryTypeID FROM plSalaryType WHERE SalaryType" & _
        "Descrip = 'Per Year')) AS YearWage, PlStudentsPlaced.HowPlacedId, (SELECT HowPla" & _
        "cedDescrip FROM plHowPlaced WHERE HowPlacedID = plStudentsPlaced.HowPlacedID) AS" & _
        " HowPlaced, PlStudentsPlaced.ScheduleId, (SELECT JobScheduleDescrip FROM plJobSc" & _
        "hedule WHERE JobScheduleID = plStudentsPlaced.ScheduleID) AS JobSchedule FROM Pl" & _
        "StudentsPlaced INNER JOIN arPrgVersions ON PlStudentsPlaced.EnrollmentId = arPrg" & _
        "Versions.PrgVerId"
        Me.OleDbSelectCommand9.Connection = Me.OleDbConnection1
        '
        'DsMiscStats1
        '
        Me.DsMiscStats1.DataSetName = "dsMiscStats"
        Me.DsMiscStats1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'OleDbDataAdapter10
        '
        Me.OleDbDataAdapter10.SelectCommand = Me.OleDbSelectCommand10
        Me.OleDbDataAdapter10.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "adLeads", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("LeadName", "LeadName"), New System.Data.Common.DataColumnMapping("HomePhone", "HomePhone"), New System.Data.Common.DataColumnMapping("LeadStatus", "LeadStatus"), New System.Data.Common.DataColumnMapping("StatusCodeDescrip", "StatusCodeDescrip"), New System.Data.Common.DataColumnMapping("ProgramID", "ProgramID"), New System.Data.Common.DataColumnMapping("Program", "Program"), New System.Data.Common.DataColumnMapping("ExpectedStart", "ExpectedStart")})})
        '
        'OleDbSelectCommand10
        '
        Me.OleDbSelectCommand10.CommandText = "SELECT adLeads.LastName + ', " & Microsoft.VisualBasic.ChrW(13) & Microsoft.VisualBasic.ChrW(10) & "    ' + adLeads.FirstName + ' ' + adLeads.MiddleNa" & _
        "me AS LeadName, adLeads.HomePhone, adLeads.LeadStatus, syStatusCodes.StatusCodeD" & _
        "escrip, adLeads.ProgramID, (SELECT ProgDescrip FROM arPrograms WHERE ProgId = ad" & _
        "Leads.ProgramID) AS Program, adLeads.ExpectedStart FROM adLeads INNER JOIN sySta" & _
        "tusCodes ON adLeads.LeadStatus = syStatusCodes.StatusCodeId WHERE (syStatusCodes" & _
        ".StatusCodeDescrip <> 'Enrolled')"
        Me.OleDbSelectCommand10.Connection = Me.OleDbConnection1
        '
        'DsLeadMasterSummary1
        '
        Me.DsLeadMasterSummary1.DataSetName = "dsLeadMasterSummary"
        Me.DsLeadMasterSummary1.Locale = New System.Globalization.CultureInfo("en-US")
        CType(Me.DsLeadMasterDetail1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsInquiryByDay1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsCostPerLead1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsEmployerMaster1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsPlacementStats1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsStatsByField1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsMiscStats1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsLeadMasterSummary1, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

End Class
