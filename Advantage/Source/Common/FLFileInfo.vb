' ===============================================================================
' FAME.AdvantageV1.Common
'
' FLFileInfo.vb
'
' file info / import classes for FameLink Files
' Checks given filename and determines validity as to contents for FLMessages
' Based on spec'd size of given message type, determines if integer number of
' message are contained in given file, and if so, then assumes N message records.
' Upon error, class returns iErrCode and description of error in StrError.
' ===============================================================================
' Copyright (C) 2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
Imports System
Imports System.IO
Imports System.Net
Public Class FLFileInfo
#Region " Private Variables and Objects"
    Private _strFileId As String
    Private _strFileName As String      'full path required / must be on local computer
    Private _strMsgType As String       'type of famelink message found in the file
    Private _strError As String
    Private _iRecordCount As Integer    'number of messages found in the file based on filesize and message size
    Private _iMessageSize As Integer     'relative size of each message of MsgType
    Private _iFileSize As Long          'the size of the file
    Private _networkuser As String
    Private _networkpassword As String
    Private _datafromremotecomputer As Boolean
    Private _dtCreationDate As Date
    Private _strFileShortName As String
#End Region

#Region " Public Constructor"
    Public Sub New()
        _iFileSize = 0
        _iRecordCount = 0
        _iMessageSize = 0
        _strMsgType = ""
        _strFileId = Guid.NewGuid.ToString
        _strFileName = ""
        _strFileShortName = ""
        _strError = ""
        _networkuser = ""
        _networkpassword = ""
        _datafromremotecomputer = False
        _dtCreationDate = Date.MinValue

    End Sub


#End Region

#Region " Methods"
    Public Function ProcessMessageFile(ByVal strFileName As String) As Boolean
        'the given filename must have an integer number of records.  this is derived
        'by dividing the filesize by the expected record size of any given message type.
        'the number of records in the given file name is returned.
        'three types of user defined errors are considered: 
        'this function assumes that the file is not open
        Dim lngRemainder As Long
        ProcessMessageFile = False
        _iRecordCount = 0
        _iFileSize = 0
        If String.IsNullOrEmpty(strFileName) Then Throw New ArgumentNullException("strFileName", "Empty or Null filename provided.")
        Try
            If _iMessageSize > 0 Then
                '_iFileSize = FileLen(strFileName)
                Dim Request As WebRequest
                Dim ResponseStream As Stream
                Dim Response1 As WebResponse
                Dim myCredentials As NetworkCredential
                If datafromremotecomputer = True Then
                    myCredentials = New NetworkCredential("", _networkuser, _networkpassword)
                End If
                Request = System.Net.FileWebRequest.Create(strFileName) 'WebRequest.Create(strFileName) 'System.Net.FileWebRequest.Create("\\qaw2k\ESPFileIn\CLFAHEAD.000") 
                Request.Headers.Add("Translate: f")
                Request.Method = "GET"
                If datafromremotecomputer = True Then
                    Request.Credentials = myCredentials
                Else
                    Request.Credentials = CredentialCache.DefaultCredentials
                End If

                Response1 = CType(Request.GetResponse, WebResponse)
                ResponseStream = Response1.GetResponseStream
                _iFileSize = Response1.ContentLength
                '_iFileSize = FileLen(strFileName)
                ResponseStream.Flush()
                ResponseStream.Close()
                Response1.Close()

                Select Case _strMsgType
                    Case "CHNG"
                        'lngRemainder = _iFileSize Mod (_iMessageSize + 2)
                        '_iRecordCount = CInt(_iFileSize \ (_iMessageSize + 2))
                        'If lngRemainder >= 1 Then
                        lngRemainder = _iFileSize Mod (_iMessageSize + 1)
                        _iRecordCount = CInt(_iFileSize \ (_iMessageSize + 1))
                        'End If
                    Case "DISB", "HEAD", "FAID", "RCVD", "REFU"
                        lngRemainder = _iFileSize Mod (_iMessageSize + 1)
                        _iRecordCount = CInt(_iFileSize \ (_iMessageSize + 1))
                    Case Else
                        Throw New ArgumentOutOfRangeException("_strMsgType", _strMsgType, "Message type provided is not recognized")
                End Select
                If lngRemainder > 0 Then
                    Throw New ArgumentOutOfRangeException("_iFileSize", _iFileSize, "Invalid file length for given message type")
                End If
            Else
                Throw New ArgumentOutOfRangeException("_strMsgType", _strMsgType, "Message type provided is not recognized")
            End If
        Catch e As ArgumentNullException
            _iRecordCount = 0
            _iFileSize = 0
            _strError = e.Message
        Catch e As ArgumentOutOfRangeException
            _iRecordCount = 0
            _iFileSize = 0
            _strError = e.Message
        Catch e As System.Exception
            _iRecordCount = 0
            _iFileSize = 0
            _strError = e.Message
            Throw
        Finally
            ProcessMessageFile = (_iRecordCount <> 0)
        End Try
    End Function
    Public Function ProcessFileName(ByVal strFileName As String) As Boolean
        'the message type to be processed should be embedded in the name of the given FameLink filename.
        'the type of famelink messages is derived from the file name and message size set.
        'if the message type string is found in the given filename, the expected message size is returned
        'otherwise, a user defined error is set to indicate that the filename does not meet specs.  

        Dim fname As String
        Dim iMsgSize As Integer

        ProcessFileName = False
        If String.IsNullOrEmpty(strFileName) Then Throw New ArgumentNullException("strFileName", "Empty or Null filename provided.")

        Try
            'fname = UCase(strFileName)
            fname = UCase(System.IO.Path.GetFileName(strFileName))
            _dtCreationDate = New FileInfo(Path.GetFullPath(strFileName)).CreationTime

            If (InStr(1, fname, "FAID", CompareMethod.Text) > 0) Then
                _strMsgType = "FAID"
                iMsgSize = GetMessageSize(_strMsgType)
                If iMsgSize > 0 Then
                    _iMessageSize = iMsgSize
                    ProcessFileName = True
                End If
                Exit Function
            End If
            If (InStr(1, fname, "HEAD", CompareMethod.Text) > 0) Then
                _strMsgType = "HEAD"
                iMsgSize = GetMessageSize(_strMsgType)
                If iMsgSize > 0 Then
                    _iMessageSize = iMsgSize
                    ProcessFileName = True
                End If
                Exit Function
            End If
            If (InStr(1, fname, "CHNG", CompareMethod.Text) > 0) Then
                _strMsgType = "CHNG"
                iMsgSize = GetMessageSize(_strMsgType)
                If iMsgSize > 0 Then
                    _iMessageSize = iMsgSize
                    ProcessFileName = True
                End If
                Exit Function
            End If
            If (InStr(1, fname, "DISB", CompareMethod.Text) > 0) Then
                _strMsgType = "DISB"
                iMsgSize = GetMessageSize(_strMsgType)
                If iMsgSize > 0 Then
                    _iMessageSize = iMsgSize
                    ProcessFileName = True
                End If
                Exit Function
            End If
            If (InStr(1, fname, "REFU", CompareMethod.Text) > 0) Then
                _strMsgType = "REFU"
                iMsgSize = GetMessageSize(_strMsgType)
                If iMsgSize > 0 Then
                    _iMessageSize = iMsgSize
                    ProcessFileName = True
                End If
                Exit Function
            End If
            If (InStr(1, fname, "RCVD", CompareMethod.Text) > 0) Then
                _strMsgType = "RCVD"
                iMsgSize = GetMessageSize(_strMsgType)
                If iMsgSize > 0 Then
                    _iMessageSize = iMsgSize
                    ProcessFileName = True
                End If
                Exit Function
            End If
            Throw New ArgumentOutOfRangeException("strFileName", fname, "Filename does not contain embedded message type")

        Catch e As ArgumentNullException
            _strError = e.Message
        Catch e As ArgumentOutOfRangeException
            _strError = e.Message
        Catch e As System.Exception
            _strError = e.Message
            Throw
        End Try
    End Function
    Public Function GetMessageSize(ByVal strMsgType As String) As Integer
        'returns the expected message record size given the message type
        Const constCHNGsz As Integer = 72
        Const constDISBsz As Integer = 65
        Const constFAIDsz As Integer = 71
        Const constHEADsz As Integer = 104
        Const constRCVDsz As Integer = 86
        Const constREFUsz As Integer = 115

        GetMessageSize = 0
        If String.IsNullOrEmpty(strMsgType) Then Exit Function
        Select Case strMsgType
            Case "FAID"
                GetMessageSize = constFAIDsz
            Case "HEAD"
                GetMessageSize = constHEADsz
            Case "CHNG"
                GetMessageSize = constCHNGsz
            Case "RCVD"
                GetMessageSize = constRCVDsz
            Case "DISB"
                GetMessageSize = constDISBsz
            Case "REFU"
                GetMessageSize = constREFUsz
        End Select
    End Function

#End Region

#Region "Public Properties"
    Public ReadOnly Property FileId As String
        Get
            Return _strFileId
        End Get
    End Property
    Public Property strFileName() As String
        'when the filename property is set, the filename is first processed to determine the type of message
        'then the filelength and record count is verified given the message type's record size
        'finally the file is opened for processing.  Upon error, the filename is cleared and errCode set
        Get
            Return _strFileName
        End Get
        Set(ByVal Value As String)
            Dim blnResult As Boolean = False
            _iFileSize = 0
            _iRecordCount = 0
            _iMessageSize = 0
            _strMsgType = ""
            _strFileName = ""
            _strError = ""
            Try
                If String.IsNullOrEmpty(Value) Then Throw New ArgumentNullException("Value", "Empty or Null filename provided.")
                If ProcessFileName(Value) Then
                    _strFileName = Value
                    _strFileShortName = Path.GetFileName(Value)
                    If Not ProcessMessageFile(_strFileName) Then
                        _iFileSize = 0
                        _iRecordCount = 0
                        _iMessageSize = 0
                        _strMsgType = ""
                    End If
                Else
                    _iFileSize = 0
                    _iRecordCount = 0
                    _iMessageSize = 0
                    _strMsgType = ""
                End If
            Catch e As ArgumentNullException
                _strError = e.Message
            Catch e As ArgumentOutOfRangeException
                _strError = e.Message
            Catch e As System.Exception
                _strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public ReadOnly Property strFileShortName() As String
        Get
            Return _strFileShortName
        End Get
    End Property
    Public ReadOnly Property iFileSize() As Long
        'filesize of the given filename provided in strFileName
        Get
            Return _iFileSize
        End Get
    End Property
    Public Property networkuser() As String
        Get
            Return _networkuser
        End Get
        Set(ByVal value As String)
            _networkuser = value
        End Set
    End Property
    Public Property networkPassword() As String
        Get
            Return _networkpassword
        End Get
        Set(ByVal value As String)
            _networkpassword = value
        End Set
    End Property
    Public Property datafromremotecomputer() As Boolean
        Get
            Return _datafromremotecomputer
        End Get
        Set(ByVal value As Boolean)
            _datafromremotecomputer = value
        End Set
    End Property
    Public ReadOnly Property iRecordCount() As Integer
        'based on the message size of the given message type derived
        'from the given filename, iRecordCount reflects the integer
        'number of records expected in the given filename.  If non-integer
        'value is derived then an error is noted.
        Get
            Return _iRecordCount
        End Get
    End Property
    Public ReadOnly Property iMessageSize() As Integer
        'number of bytes for the given derived message type
        Get
            Return _iMessageSize
        End Get
    End Property
    Public ReadOnly Property strMsgType() As String
        'message type for the given filename.  six are currently supported.
        Get
            Return _strMsgType
        End Get
    End Property
    Public ReadOnly Property strError() As String
        'upon trapped error an related string error is provide.
        Get
            Return _strError
        End Get
    End Property
    Public ReadOnly Property CreationDate As Date
        Get
            Return _dtCreationDate
        End Get
    End Property

#End Region
End Class
Public Class TCFileInfo
#Region " Private Variables and Objects"
    Private _strFileName As String      'full path required / must be on local computer
    Private _strError As String
    Private _networkuser As String
    Private _networkpassword As String
    Private _datafromremotecomputer As Boolean
#End Region

#Region " Public Constructor"
    Public Sub New()

        _strFileName = ""
        _strError = ""
        _networkuser = ""
        _networkpassword = ""
        _datafromremotecomputer = False
    End Sub
#End Region

#Region " Methods"
    Public Function ProcessMessageFile(ByVal strFileName As String) As Boolean
        'the given filename must have an integer number of records.  this is derived
        'by dividing the filesize by the expected record size of any given message type.
        'the number of records in the given file name is returned.
        'three types of user defined errors are considered: 
        'this function assumes that the file is not open
        ProcessMessageFile = False
        If String.IsNullOrEmpty(strFileName) Then Throw New ArgumentNullException("strFileName", "Empty or Null filename provided.")
        Try

            Dim Request As WebRequest
            Dim ResponseStream As Stream
            Dim Response1 As WebResponse
            Dim myCredentials As NetworkCredential
            If datafromremotecomputer = True Then
                myCredentials = New NetworkCredential("", _networkuser, _networkpassword)
            End If
            Request = System.Net.FileWebRequest.Create(strFileName) 'WebRequest.Create(strFileName) 'System.Net.FileWebRequest.Create("\\qaw2k\ESPFileIn\CLFAHEAD.000") 
            Request.Headers.Add("Translate: f")
            Request.Method = "GET"
            If datafromremotecomputer = True Then
                Request.Credentials = myCredentials
            Else
                Request.Credentials = CredentialCache.DefaultCredentials
            End If

            Response1 = CType(Request.GetResponse, WebResponse)
            ResponseStream = Response1.GetResponseStream
            '   _iFileSize = Response1.ContentLength
            '_iFileSize = FileLen(strFileName)
            ResponseStream.Flush()
            ResponseStream.Close()
            Response1.Close()




        Catch e As System.Exception
            _strError = e.Message
            Throw
        Finally
        End Try
    End Function


#End Region

#Region "Public Properties"
    Public Property strFileName() As String
        'when the filename property is set, the filename is first processed to determine the type of message
        'then the filelength and record count is verified given the message type's record size
        'finally the file is opened for processing.  Upon error, the filename is cleared and errCode set
        Get
            Return _strFileName
        End Get
        Set(ByVal Value As String)
            Dim blnResult As Boolean = False
            _strFileName = ""
            _strError = ""
            Try
                _strFileName = Value

            Catch e As System.Exception
                _strError = e.Message
                Throw
            End Try
        End Set
    End Property

    Public Property networkuser() As String
        Get
            Return _networkuser
        End Get
        Set(ByVal value As String)
            _networkuser = value
        End Set
    End Property

    Public Property networkPassword() As String
        Get
            Return _networkpassword
        End Get
        Set(ByVal value As String)
            _networkpassword = value
        End Set
    End Property

    Public Property datafromremotecomputer() As Boolean
        Get
            Return _datafromremotecomputer
        End Get
        Set(ByVal value As Boolean)
            _datafromremotecomputer = value
        End Set
    End Property

    Public ReadOnly Property strError() As String
        'upon trapped error an related string error is provide.
        Get
            Return _strError
        End Get
    End Property

   

#End Region
End Class