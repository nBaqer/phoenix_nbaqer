﻿
Public Class SystemStatusChangeModel
    ''' <summary>
    ''' Effective Date is the Date of Change in the DataBase
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property EffectiveDate As DateTime
    ''' <summary>
    ''' The Previous Status Code Id (Also known as Original Status Code Id)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PreviousStatusCodeId As Guid
    ''' <summary>
    ''' The Previous System Status Id (Also known as Original Status Id)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PreviousStatusId As Integer
    ''' <summary>
    '''  The System Status Code Id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property StatusCodeId As Guid
    ''' <summary>
    '''  The System Status Id mapped to the System Status Code 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property StatusId As Integer
    ''' <summary>
    ''' The Reason of the change of Status (Guid if is LOA or DROPPED, String if is a Suppension)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Reason As String
    ''' <summary>
    ''' The User who made the change
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PostedBy As String
    ''' <summary>
    ''' Flag returned by the validation to know if this status change is allowed by the business rules defined in SystemStatusWorkflowRules
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property AllowInsert As Boolean
    ''' <summary>
    '''  Flag returned by the validation to know if this status change is allowed by the business rules defined in SystemStatusWorkflowRules
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property AllowEdit As Boolean
    ''' <summary>
    ''' Flag returned by the validation to know if this status change is breaking the secuence of a status change
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ShouldConfirmAction As Boolean
    ''' <summary>
    '''  Flag returned by the validation to know if this status change is allowed by the business rules defined in SystemStatusWorkflowRules
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property AllowDelete As Boolean
    ''' <summary>
    ''' The End Date if is (LOA, Suspension, or Probation)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property EndDate As String
    ''' <summary>
    ''' The Student Enrollment Associated with this status change
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property StudentEnrollmentId As String
    ''' <summary>
    ''' The Campus Associated with this status change
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CampusId As String
    ''' <summary>
    ''' The User Id who requested the Status Change (Filled by Support member only)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RequestedBy As Guid
    ''' <summary>
    ''' The Case Number (Ticket) with the request of the Status Change (Filled by Support member only)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CaseNumber As String
    ''' <summary>
    ''' The User Id that made the status change
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property UserId As String
    ''' <summary>
    ''' The Type of Validation that will be made. Insert(=1), Edit(=2) or Delete(=3)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValidationMode As Integer
    ''' <summary>
    ''' DateTime value that mark the date and time when the change was made. This is filled automatilly with the DateTime.Now
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property DateOfChange As DateTime
    ''' <summary>
    ''' Student Enrollment Start Date
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property StudentEnrollmentStartDate As String
    ''' <summary>
    ''' Student Enrollment Graduation Date
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property StudentEnrollmentGraduationDate As String


End Class
