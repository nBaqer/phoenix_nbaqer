
Imports Microsoft.VisualBasic

Public Class AuditTriggerTemplates

#Region "Private Variables and objects"
    Private TemplINSHDR As String
    Private TemplINSCOL As String
    Private TemplINSFTR As String
    Private TemplUPDHDR As String
    Private TemplUPDCOL As String
    Private TemplUPDFTR As String
    Private TemplDELHDR As String
    Private TemplDELCOL As String
    Private TemplDELFTR As String
#End Region

#Region "Private Methods"
    Private Function BuildTemplINSHDR() As String
        Dim sb As New System.Text.StringBuilder
        With sb
            .Append("CREATE TRIGGER %TABLENAME%_Audit_Insert ON [%TABLENAME%] ")
            .Append(vbCrLf)
            .Append("FOR INSERT ")
            .Append(vbCrLf)
            .Append("AS ")
            .Append(vbCrLf)
            .Append("DECLARE @AuditHistId AS uniqueidentifier ")
            .Append(vbCrLf)
            .Append("DECLARE @EventRows AS int ")
            .Append(vbCrLf)
            .Append("DECLARE @EventDate AS datetime ")
            .Append(vbCrLf)
            .Append("DECLARE @UserName AS varchar(50) ")
            .Append(vbCrLf)
            .Append("SET @AuditHistId = NEWID() ")
            .Append(vbCrLf)
            .Append("SET @EventRows = (SELECT COUNT(*) FROM Inserted) ")
            .Append(vbCrLf)
            .Append("SET @EventDate = (SELECT TOP 1 ModDate FROM Inserted) ")
            .Append(vbCrLf)
            .Append("SET @UserName = (SELECT TOP 1 ModUser FROM Inserted) ")
            .Append(vbCrLf)
            .Append("IF @EventRows > 0 ")
            .Append(vbCrLf)
            .Append("BEGIN ")
            .Append(vbCrLf)
            .Append("EXEC fmAuditHistAdd @AuditHistId,'%TABLENAME%', 'I', @EventRows, @EventDate, @UserName ")
            .Append(vbCrLf)
            .Append("BEGIN ")
            .Append(vbCrLf)
        End With
        Return sb.ToString
    End Function

    Private Function BuildTemplINSCOL() As String
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("INSERT INTO  syAuditHistDetail (AuditHistId, RowId, ColumnName, NewValue) ")
            .Append(vbCrLf)
            .Append("SELECT @AuditHistId, New.[%ROWID%], '%COLUMNNAME%', CONVERT(varchar(8000), New.[%COLUMNNAME%], 121) ")
            .Append(vbCrLf)
            .Append("FROM Inserted New ")
            .Append(vbCrLf)
        End With
        Return sb.ToString
    End Function

    Private Function BuildTemplINSFTR() As String
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("END ")
            .Append(vbCrLf)
            .Append("END ")
        End With
        Return sb.ToString
    End Function

    Private Function BuildTemplUPDHDR() As String
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("CREATE TRIGGER %TABLENAME%_Audit_Update ON [%TABLENAME%] ")
            .Append(vbCrLf)
            .Append("FOR UPDATE ")
            .Append(vbCrLf)
            .Append("AS ")
            .Append(vbCrLf)
            .Append("DECLARE @AuditHistId AS uniqueidentifier ")
            .Append(vbCrLf)
            .Append("DECLARE @EventRows AS int ")
            .Append(vbCrLf)
            .Append("DECLARE @EventDate AS datetime ")
            .Append(vbCrLf)
            .Append("DECLARE @UserName AS varchar(50)")
            .Append(vbCrLf)
            .Append("SET @AuditHistId = NEWID() ")
            .Append(vbCrLf)
            .Append("SET @EventRows = (SELECT COUNT(*) FROM Inserted) ")
            .Append(vbCrLf)
            .Append("SET @EventDate = (SELECT TOP 1 ModDate FROM Inserted) ")
            .Append(vbCrLf)
            .Append("SET @UserName = (SELECT TOP 1 ModUser FROM Inserted) ")
            .Append(vbCrLf)
            .Append("IF @EventRows > 0 ")
            .Append(vbCrLf)
            .Append("BEGIN ")
            .Append(vbCrLf)
            .Append("EXEC fmAuditHistAdd @AuditHistId,'%TABLENAME%', 'U', @EventRows, @EventDate, @UserName ")
            .Append(vbCrLf)
            .Append("BEGIN ")
            .Append(vbCrLf)
        End With

        Return sb.ToString
    End Function

    Private Function BuildTemplUPDCOL() As String
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("IF UPDATE ([%COLUMNNAME%]) ")
            .Append(vbCrLf)
            .Append("INSERT INTO  syAuditHistDetail (AuditHistId, RowId, ColumnName, OldValue, NewValue) ")
            .Append(vbCrLf)
            .Append("SELECT @AuditHistId, New.[%ROWID%], '%COLUMNNAME%', CONVERT(varchar(8000), Old.[%COLUMNNAME%], 121), CONVERT(varchar(8000), New.[%COLUMNNAME%], 121) ")
            .Append(vbCrLf)
            .Append("FROM Inserted New FULL OUTER JOIN Deleted Old  ON Old.[%ROWID%] = New.[%ROWID%] ")
            .Append(vbCrLf)
            .Append("WHERE Old.[%COLUMNNAME%] <> New.[%COLUMNNAME%] OR (Old.[%COLUMNNAME%] IS NULL AND New.[%COLUMNNAME%] IS NOT NULL) OR (New.[%COLUMNNAME%] IS NULL AND Old.[%COLUMNNAME%] IS NOT NULL) ")
            .Append(vbCrLf)
        End With
        Return sb.ToString
    End Function

    Private Function BuildTemplUPDFTR() As String
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("END ")
            .Append(vbCrLf)
            .Append("END ")
        End With
        Return sb.ToString
    End Function

    Private Function BuildTemplDELHDR() As String
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("CREATE TRIGGER %TABLENAME%_Audit_Delete ON [%TABLENAME%] ")
            .Append(vbCrLf)
            .Append("FOR DELETE ")
            .Append(vbCrLf)
            .Append("AS ")
            .Append(vbCrLf)
            .Append("DECLARE @AuditHistId AS uniqueidentifier ")
            .Append(vbCrLf)
            .Append("DECLARE @EventRows AS int ")
            .Append(vbCrLf)
            .Append("DECLARE @EventDate AS datetime ")
            .Append(vbCrLf)
            .Append("DECLARE @UserName AS varchar(50) ")
            .Append(vbCrLf)
            .Append("SET @AuditHistId = NEWID() ")
            .Append("SET @EventRows = (SELECT COUNT(*) FROM Deleted) ")
            .Append(vbCrLf)
            .Append("SET @EventDate = (SELECT TOP 1 ModDate FROM Deleted) ")
            .Append(vbCrLf)
            .Append("SET @UserName = (SELECT TOP 1 ModUser FROM Deleted) ")
            .Append(vbCrLf)
            .Append("IF @EventRows > 0 ")
            .Append(vbCrLf)
            .Append("BEGIN ")
            .Append(vbCrLf)
            .Append("EXEC fmAuditHistAdd @AuditHistId,'%TABLENAME%', 'D', @EventRows, @EventDate, @UserName ")
            .Append(vbCrLf)
            .Append("BEGIN ")
            .Append(vbCrLf)
        End With

        Return sb.ToString
    End Function

    Private Function BuildTemplDELCOL() As String
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("INSERT INTO  syAuditHistDetail (AuditHistId, RowId, ColumnName, OldValue) ")
            .Append(vbCrLf)
            .Append("SELECT @AuditHistId, Old.[%ROWID%], '%COLUMNNAME%', CONVERT(varchar(8000), Old.[%COLUMNNAME%], 121) ")
            .Append(vbCrLf)
            .Append("FROM Deleted Old ")
            .Append(vbCrLf)

        End With
        Return sb.ToString
    End Function

    Private Function BuildTemplDELFTR() As String
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("END ")
            .Append(vbCrLf)
            .Append("END ")
        End With
        Return sb.ToString
    End Function

    Private Function BuildTemplDROPINS() As String
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("IF EXISTS(SELECT * FROM sysobjects WHERE id = OBJECT_ID('[dbo].[%TABLENAME%_Audit_Insert]') AND (OBJECTPROPERTY(id, 'IsTrigger') = 1)) ")
            .Append(vbCrLf)
            .Append("DROP TRIGGER [dbo].[%TABLENAME%_Audit_Insert] ")
        End With
        Return sb.ToString
    End Function

    Private Function BuildTemplDROPUPD() As String
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("IF EXISTS(SELECT * FROM sysobjects WHERE id = OBJECT_ID('[dbo].[%TABLENAME%_Audit_Update]') AND (OBJECTPROPERTY(id, 'IsTrigger') = 1)) ")
            .Append(vbCrLf)
            .Append("DROP TRIGGER [dbo].[%TABLENAME%_Audit_Update] ")
        End With
        Return sb.ToString
    End Function

    Private Function BuildTemplDROPDEL() As String
        Dim sb As New System.Text.StringBuilder

        With sb
            .Append("IF EXISTS(SELECT * FROM sysobjects WHERE id = OBJECT_ID('[dbo].[%TABLENAME%_Audit_Delete]') AND (OBJECTPROPERTY(id, 'IsTrigger') = 1)) ")
            .Append(vbCrLf)
            .Append("DROP TRIGGER [dbo].[%TABLENAME%_Audit_Delete] ")
        End With
        Return sb.ToString
    End Function


#End Region

#Region "Public Methods"
    Public Function GetTriggerTemplate(ByVal TemplateID As String) As String
        Dim TemplText As String
        Select Case TemplateID
            Case "INSHDR"
                TemplText = BuildTemplINSHDR()
            Case "INSCOL"
                TemplText = BuildTemplINSCOL()
            Case "INSFTR"
                TemplText = BuildTemplINSFTR()
            Case "UPDHDR"
                TemplText = BuildTemplUPDHDR()
            Case "UPDCOL"
                TemplText = BuildTemplUPDCOL()
            Case "UPDFTR"
                TemplText = BuildTemplUPDFTR()
            Case "DELHDR"
                TemplText = BuildTemplDELHDR()
            Case "DELCOL"
                TemplText = BuildTemplDELCOL()
            Case "DELFTR"
                TemplText = BuildTemplDELFTR()
            Case "DROPINS"
                TemplText = BuildTemplDROPINS()
            Case "DROPUPD"
                TemplText = BuildTemplDROPUPD()
            Case "DROPDEL"
                TemplText = BuildTemplDROPDEL()
            Case Else
                Throw New System.Exception("No trigger template exists for the ID specified.")

        End Select
        Return TemplText
    End Function
#End Region
End Class

