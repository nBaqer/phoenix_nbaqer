Public Class StatusChangeInfo
    Private _OriginalStatus As Guid
    Private _NewStatus As Guid
    Private _StatusChangeId As Guid
    Private _Role As Guid
    Private _Status As Guid
    Public Property OriginalStatus() As Guid
        Get
            OriginalStatus = _OriginalStatus
        End Get
        Set(ByVal Value As Guid)
            _OriginalStatus = Value
        End Set
    End Property
    Public Property NewStatus() As Guid
        Get
            NewStatus = _NewStatus
        End Get
        Set(ByVal Value As Guid)
            _NewStatus = Value
        End Set
    End Property
    Public Property StatusChangeId() As Guid
        Get
            StatusChangeId = _StatusChangeId
        End Get
        Set(ByVal Value As Guid)
            _StatusChangeId = Value
        End Set
    End Property
    Public Property Role() As Guid
        Get
            Role = _Role
        End Get
        Set(ByVal Value As Guid)
            _Role = Value
        End Set
    End Property
    Public Property Status() As Guid
        Get
            Status = _Status
        End Get
        Set(ByVal Value As Guid)
            _Status = Value
        End Set
    End Property
    Public Sub New(ByVal NewOriginalStatus As Guid, ByVal NewNewStatus As Guid, ByVal NewStatusChangeId As Guid, ByVal NewRole As Guid, ByVal NewStatus As Guid)
        _OriginalStatus = NewOriginalStatus
        _NewStatus = NewNewStatus
        _StatusChangeId = NewStatusChangeId
        _Role = NewRole
        _Status = NewStatus
    End Sub
    Public Sub New()
        MyBase.new()
    End Sub
End Class
