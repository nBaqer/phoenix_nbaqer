Public Enum StateEnum
    CurrentlyAttending = 9
    Dropped = 12
    Graduated = 14
    Suspended = 11
    LeaveOfAbsence = 10
    FutureStart = 7
    TransferOut = 19
    NoStart = 8
    'ReEntry = 13
    AcademicProbation = 20
    Externship = 22
    DisciplinaryProbation = 23
    WarningProbation = 24
End Enum
