Public Class CourseInfo
#Region "Private Variable Declarations"
    Private _CourseId As Guid
    Private _CourseDescrip As String
    Private _CourseCode As String
    Private _CourseHours As Decimal
    Private _CourseCredits As Decimal
    Private _finAidCredits As Decimal
    Private _ReqTypeId As Integer
    Private _StatusId As Guid
    Private _GrdLvlId As String
    Private _UnitTypeId As Guid
    Private _CampGrpId As Guid
    Private _CourseCatalog As String
    Private _CourseComments As String
    Private _ModDate As DateTime
    Private _ModUser As String
    Private _SU As Integer
    Private _PF As Integer
    Private _CourseCatId As String
    Private _AreStdsReg As Boolean
    Private _trackTardies As Boolean
    Private _isOnLine As Boolean
    Private _tardiesMakingAbsence As Integer
    Private _IsCourseInPrgVerDef As Boolean
    Private _DeptId As Guid
    Private _IsExternship As Boolean
    Private _IsUseTimeClock As Boolean
    Private _IsAttendanceOnly As Boolean
    Private _AllowCompletedCourseRetake As Boolean
#End Region
#Region " Public Constructor"
    Public Sub New()
        _modUser = ""
        _ModDate = Date.MinValue
        _AreStdsReg = False
        _trackTardies = False
        _tardiesMakingAbsence = 0
        _isOnLine = False
        _IsExternship = False
        _IsAttendanceOnly = False
        _AllowCompletedCourseRetake = False
    End Sub
#End Region
#Region "Course Info Properties"


    Public Property CourseId() As Guid
        Get
            Return _CourseId
        End Get
        Set(ByVal Value As Guid)
            _CourseId = Value
        End Set
    End Property
    Public Property StatusId() As Guid
        Get
            Return _StatusId
        End Get
        Set(ByVal Value As Guid)
            _StatusId = Value
        End Set
    End Property
    Public Property GrdLvlId() As String
        Get
            Return _GrdLvlId
        End Get
        Set(ByVal Value As String)
            _GrdLvlId = Value
        End Set
    End Property
    Public Property CourseCatId() As String
        Get
            Return _CourseCatId
        End Get
        Set(ByVal Value As String)
            _CourseCatId = Value
        End Set
    End Property
    Public Property UnitTypeId() As Guid
        Get
            Return _UnitTypeId
        End Get
        Set(ByVal Value As Guid)
            _UnitTypeId = Value
        End Set
    End Property
    Public Property CampGrpId() As Guid
        Get
            Return _CampGrpId
        End Get
        Set(ByVal Value As Guid)
            _CampGrpId = Value
        End Set
    End Property
    Public Property ReqTypeId() As Integer
        Get
            Return _ReqTypeId
        End Get
        Set(ByVal Value As Integer)
            _ReqTypeId = Value
        End Set
    End Property

    Public Property CourseCode() As String
        Get
            Return _CourseCode
        End Get
        Set(ByVal Value As String)
            _CourseCode = Value
        End Set
    End Property
    Public Property CourseDescrip() As String
        Get
            Return _CourseDescrip
        End Get
        Set(ByVal Value As String)
            _CourseDescrip = Value
        End Set
    End Property
    Public Property CourseHours() As Decimal
        Get
            Return _CourseHours
        End Get
        Set(ByVal Value As Decimal)
            _CourseHours = Value
        End Set
    End Property

    Public Property CourseCredits() As Decimal
        Get
            Return _CourseCredits
        End Get
        Set(ByVal Value As Decimal)
            _CourseCredits = Value
        End Set
    End Property
    Public Property FinAidCredits() As Decimal
        Get
            Return _FinAidCredits
        End Get
        Set(ByVal Value As Decimal)
            _FinAidCredits = Value
        End Set
    End Property
    Public Property CourseCatalog() As String
        Get
            Return _CourseCatalog
        End Get
        Set(ByVal Value As String)
            _CourseCatalog = Value
        End Set
    End Property
    Public Property CourseComments() As String
        Get
            Return _CourseComments
        End Get
        Set(ByVal Value As String)
            _CourseComments = Value
        End Set
    End Property

    Public Property ModDate() As DateTime
        Get
            Return _ModDate
        End Get
        Set(ByVal Value As DateTime)
            _ModDate = Value
        End Set
    End Property

    Public Property ModUser() As String
        Get
            Return _ModUser
        End Get
        Set(ByVal Value As String)
            _ModUser = Value
        End Set
    End Property
    Public Property PF() As Integer
        Get
            Return _PF
        End Get
        Set(ByVal Value As Integer)
            _PF = Value
        End Set
    End Property
    Public Property SU() As Integer
        Get
            Return _SU
        End Get
        Set(ByVal Value As Integer)
            _SU = Value
        End Set
    End Property
    Public Property AreStdsReg() As Boolean
        Get
            Return _AreStdsReg
        End Get
        Set(ByVal Value As Boolean)
            _AreStdsReg = Value
        End Set
    End Property
    Public Property IsCourseInPrgVerDef() As Boolean
        Get
            Return _IsCourseInPrgVerDef
        End Get
        Set(ByVal Value As Boolean)
            _IsCourseInPrgVerDef = Value
        End Set
    End Property
    Public Property Externship() As Boolean
        Get
            Return _IsExternship
        End Get
        Set(ByVal Value As Boolean)
            _IsExternship = Value
        End Set
    End Property
    Public Property TrackTardies() As Boolean
        Get
            Return _trackTardies
        End Get
        Set(ByVal Value As Boolean)
            _trackTardies = Value
        End Set
    End Property
    Public Property IsOnLine() As Boolean
        Get
            Return _isOnLine
        End Get
        Set(ByVal Value As Boolean)
            _isOnLine = Value
        End Set
    End Property
    Public Property TardiesMakingAbsence() As Integer
        Get
            Return _tardiesMakingAbsence
        End Get
        Set(ByVal Value As Integer)
            _tardiesMakingAbsence = Value
        End Set
    End Property
    Public Property DeptId() As Guid
        Get
            Return _DeptId
        End Get
        Set(ByVal Value As Guid)
            _DeptId = Value
        End Set
    End Property
    Public Property IsUseTimeClock() As Boolean
        Get
            Return _IsUseTimeClock
        End Get
        Set(ByVal Value As Boolean)
            _IsUseTimeClock = Value
        End Set
    End Property

    Public Property IsAttendanceOnly() As Boolean
        Get
            Return _IsAttendanceOnly
        End Get
        Set(value As Boolean)
            _IsAttendanceOnly = value
        End Set
    End Property

    Public Property AllowCompletedCourseRetake() As Boolean
        Get
            Return _AllowCompletedCourseRetake
        End Get
        Set(value As Boolean)
            _AllowCompletedCourseRetake = value
        End Set
    End Property


#End Region
End Class
