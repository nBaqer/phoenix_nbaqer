Public Class BasisDetailInfo
    Private _BasisDetailId As Guid
    Private _BasisDetailValue As String
    Private _BasisWhenId As Guid
    Public Property BasisDetailId() As Guid
        Get
            BasisDetailId = _BasisDetailId
        End Get
        Set(ByVal Value As Guid)
            _BasisDetailId = Value
        End Set
    End Property
    Public Property BasisDetailValue() As String
        Get
            BasisDetailValue = _BasisDetailValue
        End Get
        Set(ByVal Value As String)
            _BasisDetailValue = Value
        End Set
    End Property
    Public Property BasisWhenId() As Guid
        Get
            BasisWhenId = _BasisWhenId
        End Get
        Set(ByVal Value As Guid)
            _BasisWhenId = Value
        End Set
    End Property
    Public Sub New(ByVal NewBasisDetailId As Guid, ByVal NewBasisDetailValue As String, ByVal NewBasisWhenId As Guid)
        _BasisDetailId = NewBasisDetailId
        _BasisDetailValue = NewBasisDetailValue
        _BasisWhenId = NewBasisWhenId
    End Sub
    Public Sub New()
        'MyBase.new()
    End Sub
End Class
