Public Class ReportParamInfoIPEDS
	Private m_ResourceId As Integer
	Private m_RptTitle As String
	Private m_ObjId As Integer

	Private m_CohortYear As Integer
	Private m_SchoolRptType As String
	Private m_CohortType As String
	Private m_RptEndDate As Date
	Private m_CohortStartDate As Date
	Private m_CohortEndDate As Date
	Private m_FilterCampusID As String
	Private m_FilterProgramIDs As String
	Private m_FilterProgramDescrips As String
	Private m_FilterProgramIDsMult1 As String
	Private m_FilterProgramDescripsMult1 As String
	Private m_FilterProgramIDsMult2 As String
	Private m_FilterProgramDescripsMult2 As String
	Private m_FilterProgramIDsMult3 As String
	Private m_FilterProgramDescripsMult3 As String
	Private m_FilterFundSourceIds As String
	Private m_FilterFundSourceDescrips As String
	Private m_ShowTuition As Boolean
	Private m_ShowFees As Boolean
	Private m_SortByProg As String
	Private m_SortByStu As String
	Private m_StudentIDCaption As String
	Private m_CustomPrefInfo As DataTable

	Private m_StuList_InclNonDegCertSeeking As Boolean
	Private m_StuList_InclPartTime As Boolean
	Private m_StuList_InclTransferIn As Boolean

	Public Sub New()
		m_ResourceId = 0
		m_RptTitle = Nothing
		m_ObjId = 0
		m_CohortYear = Nothing
		m_SchoolRptType = Nothing
		m_CohortType = Nothing
		m_RptEndDate = Nothing
		m_CohortStartDate = Nothing
		m_CohortEndDate = Nothing
		m_FilterCampusID = Nothing
		m_FilterProgramIDs = Nothing
		m_FilterProgramDescrips = Nothing
		m_FilterProgramIDsMult1 = Nothing
		m_FilterProgramDescripsMult1 = Nothing
		m_FilterProgramIDsMult2 = Nothing
		m_FilterProgramDescripsMult2 = Nothing
		m_FilterProgramIDsMult3 = Nothing
		m_FilterProgramDescripsMult3 = Nothing
		m_FilterFundSourceIds = Nothing
		m_FilterFundSourceDescrips = Nothing
		m_ShowTuition = False
		m_ShowFees = False
		m_SortByProg = Nothing
		m_SortByStu = Nothing
		m_StudentIDCaption = Nothing
		m_CustomPrefInfo = Nothing

		m_StuList_InclNonDegCertSeeking = False
		m_StuList_InclPartTime = False
		m_StuList_InclTransferIn = False
	End Sub

	Public Property ResourceId() As Integer
		Get
			Return m_ResourceId
		End Get
		Set(ByVal Value As Integer)
			m_ResourceId = Value
		End Set
	End Property

	Public Property RptTitle() As String
		Get
			Return m_RptTitle
		End Get
		Set(ByVal Value As String)
			m_RptTitle = Value
		End Set
	End Property

	Public Property ObjId() As Integer
		Get
			Return m_ObjId
		End Get
		Set(ByVal Value As Integer)
			m_ObjId = Value
		End Set
	End Property

	Public Property CohortYear() As Integer
		Get
			Return m_CohortYear
		End Get
		Set(ByVal Value As Integer)
			m_CohortYear = Value
		End Set
	End Property

	Public Property SchoolRptType() As String
		Get
			Return m_SchoolRptType
		End Get
		Set(ByVal Value As String)
			m_SchoolRptType = Value
		End Set
	End Property

	Public Property CohortType() As String
		Get
			Return m_CohortType
		End Get
		Set(ByVal Value As String)
			m_CohortType = Value
		End Set
	End Property


	Public Property RptEndDate() As Date
		Get
			Return m_RptEndDate
		End Get
		Set(ByVal Value As Date)
			m_RptEndDate = Value
		End Set
	End Property

	Public Property CohortStartDate() As Date
		Get
			Return m_CohortStartDate
		End Get
		Set(ByVal Value As Date)
			m_CohortStartDate = Value
		End Set
	End Property

	Public Property CohortEndDate() As Date
		Get
			Return m_CohortEndDate
		End Get
		Set(ByVal Value As Date)
			m_CohortEndDate = Value
		End Set
	End Property

	Public Property FilterCampusID() As String
		Get
			Return m_FilterCampusID
		End Get
		Set(ByVal Value As String)
			m_FilterCampusID = Value
		End Set
	End Property

	Public Property FilterProgramIDs() As String
		Get
			Return m_FilterProgramIDs
		End Get
		Set(ByVal Value As String)
			m_FilterProgramIDs = Value
		End Set
	End Property

	Public Property FilterProgramDescrips() As String
		Get
			Return m_FilterProgramDescrips
		End Get
		Set(ByVal Value As String)
			m_FilterProgramDescrips = Value
		End Set
	End Property

	Public Property FilterProgramIDsMult1() As String
		Get
			Return m_FilterProgramIDsMult1
		End Get
		Set(ByVal Value As String)
			m_FilterProgramIDsMult1 = Value
		End Set
	End Property

	Public Property FilterProgramDescripsMult1() As String
		Get
			Return m_FilterProgramDescripsMult1
		End Get
		Set(ByVal Value As String)
			m_FilterProgramDescripsMult1 = Value
		End Set
	End Property

	Public Property FilterProgramIDsMult2() As String
		Get
			Return m_FilterProgramIDsMult2
		End Get
		Set(ByVal Value As String)
			m_FilterProgramIDsMult2 = Value
		End Set
	End Property

	Public Property FilterProgramDescripsMult2() As String
		Get
			Return m_FilterProgramDescripsMult2
		End Get
		Set(ByVal Value As String)
			m_FilterProgramDescripsMult2 = Value
		End Set
	End Property

	Public Property FilterProgramIDsMult3() As String
		Get
			Return m_FilterProgramIDsMult3
		End Get
		Set(ByVal Value As String)
			m_FilterProgramIDsMult3 = Value
		End Set
	End Property

	Public Property FilterProgramDescripsMult3() As String
		Get
			Return m_FilterProgramDescripsMult3
		End Get
		Set(ByVal Value As String)
			m_FilterProgramDescripsMult3 = Value
		End Set
	End Property

	Public Property FilterFundSourceIds() As String
		Get
			Return m_FilterFundSourceIds
		End Get
		Set(ByVal Value As String)
			m_FilterFundSourceIds = Value
		End Set
	End Property

	Public Property FilterFundSourceDescrips() As String
		Get
			Return m_FilterFundSourceDescrips
		End Get
		Set(ByVal Value As String)
			m_FilterFundSourceDescrips = Value
		End Set
	End Property

	Public Property ShowTuition() As Boolean
		Get
			Return m_ShowTuition
		End Get
		Set(ByVal Value As Boolean)
			m_ShowTuition = Value
		End Set
	End Property

	Public Property ShowFees() As Boolean
		Get
			Return m_ShowFees
		End Get
		Set(ByVal Value As Boolean)
			m_ShowFees = Value
		End Set
	End Property

	Public Property SortByProg() As String
		Get
			Return m_SortByProg
		End Get
		Set(ByVal Value As String)
			m_SortByProg = Value
		End Set
	End Property

	Public Property SortByStu() As String
		Get
			Return m_SortByStu
		End Get
		Set(ByVal Value As String)
			m_SortByStu = Value
		End Set
	End Property

	Public Property StudentIDCaption() As String
		Get
			Return m_StudentIDCaption
		End Get
		Set(ByVal Value As String)
			m_StudentIDCaption = Value
		End Set
	End Property

	Public Property CustomPrefInfo() As DataTable
		Get
			Return m_CustomPrefInfo
		End Get
		Set(ByVal Value As DataTable)
			m_CustomPrefInfo = Value
		End Set
	End Property

	Public Property StuList_InclNonDegCertSeeking() As Boolean
		Get
			Return m_StuList_InclNonDegCertSeeking
		End Get
		Set(ByVal Value As Boolean)
			m_StuList_InclNonDegCertSeeking = Value
		End Set
	End Property

	Public Property StuList_InclPartTime() As Boolean
		Get
			Return m_StuList_InclPartTime
		End Get
		Set(ByVal Value As Boolean)
			m_StuList_InclPartTime = Value
		End Set
	End Property

	Public Property StuList_InclTransferIn() As Boolean
		Get
			Return m_StuList_InclTransferIn
		End Get
		Set(ByVal Value As Boolean)
			m_StuList_InclTransferIn = Value
		End Set
	End Property

End Class
