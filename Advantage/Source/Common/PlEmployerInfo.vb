' ===============================================================================
' FAME.AdvantageV1.BusinessEntities
'
' PlEmployerInfo.vb
'
' Placement EmployerInfo Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class plEmployerInfo
    '
    '   EmployerInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _EmployerId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _Description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _address1 As String
    Private _address2 As String
    Private _city As String
    Private _stateId As String
    Private _state As String
    Private _zip As String
    Private _phone As String
    Private _fax As String
    Private _email As String
    Private _county As String
    Private _countyId As String
    Private _location As String
    Private _locationId As String
    Private _contactTitle As String
    Private _feeId As String
    Private _fee As String
    Private _IndustryId As String
    Private _industry As String
    Private _JobsCatId As String
    Private _ParentId As String
    Private _parent As String
    Private _Group As Boolean
    Private _CountryId As String
    Private _country As String
    Private _ForeignPhone As Boolean
    Private _ForeignFax As Boolean
    Private _ForeignZip As Boolean
    Private _OtherState As String
    Private _ModDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _EmployerId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _Description = ""
        _campGrpId = Guid.Empty.ToString
        _campGrpDescrip = ""
        _address1 = ""
        _address2 = ""
        _city = ""
        _stateId = Guid.Empty.ToString
        _state = ""
        _zip = ""
        _phone = ""
        _fax = ""
        _email = ""
        _countyId = Guid.Empty.ToString
        _county = ""
        _locationId = Guid.Empty.ToString
        _location = ""
        _feeId = Guid.Empty.ToString
        _fee = ""
        _IndustryId = Guid.Empty.ToString
        _industry = ""
        _JobsCatId = Guid.Empty.ToString
        _ParentId = Guid.Empty.ToString
        _parent = ""
        _Group = False
        _CountryId = AdvantageCommonValues.USAGuid.ToString
        _country = "USA"
        _ForeignPhone = False
        _ForeignFax = False
        _ForeignZip = False
        _OtherState = ""
        _ModDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property OtherState() As String
        Get
            Return _OtherState
        End Get
        Set(ByVal Value As String)
            _OtherState = Value
        End Set
    End Property
    Public Property ForeignPhone() As Boolean
        Get
            Return _ForeignPhone
        End Get
        Set(ByVal Value As Boolean)
            _ForeignPhone = Value
        End Set
    End Property
    Public Property ForeignFax() As Boolean
        Get
            Return _ForeignFax
        End Get
        Set(ByVal Value As Boolean)
            _ForeignFax = Value
        End Set
    End Property
    Public Property ForeignZip() As Boolean
        Get
            Return _ForeignZip
        End Get
        Set(ByVal Value As Boolean)
            _ForeignZip = Value
        End Set
    End Property
    Public Property EmployerId() As String
        Get
            Return _EmployerId
        End Get
        Set(ByVal Value As String)
            _EmployerId = Value
        End Set
    End Property
    Public Property [Group]() As Boolean
        Get
            Return _Group
        End Get
        Set(ByVal Value As Boolean)
            _Group = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property CountryId() As String
        Get
            Return _CountryId
        End Get
        Set(ByVal Value As String)
            _CountryId = Value
        End Set
    End Property
    Public Property Country() As String
        Get
            Return _country
        End Get
        Set(ByVal Value As String)
            _country = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property FeeId() As String
        Get
            Return _feeId
        End Get
        Set(ByVal Value As String)
            _feeId = Value
        End Set
    End Property
    Public Property Fee() As String
        Get
            Return _fee
        End Get
        Set(ByVal Value As String)
            _fee = Value
        End Set
    End Property
    Public Property IndustryId() As String
        Get
            Return _IndustryId
        End Get
        Set(ByVal Value As String)
            _IndustryId = Value
        End Set
    End Property
    Public Property Industry() As String
        Get
            Return _industry
        End Get
        Set(ByVal Value As String)
            _industry = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return _address1
        End Get
        Set(ByVal Value As String)
            _address1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return _address2
        End Get
        Set(ByVal Value As String)
            _address2 = Value
        End Set
    End Property
    Public Property ParentId() As String
        Get
            Return _ParentId
        End Get
        Set(ByVal Value As String)
            _ParentId = Value
        End Set
    End Property
    Public Property Parent() As String
        Get
            Return _parent
        End Get
        Set(ByVal Value As String)
            _parent = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _city
        End Get
        Set(ByVal Value As String)
            _city = Value
        End Set
    End Property
    Public Property StateId() As String
        Get
            Return _stateId
        End Get
        Set(ByVal Value As String)
            _stateId = Value
        End Set
    End Property
    Public Property State() As String
        Get
            Return _state
        End Get
        Set(ByVal Value As String)
            _state = Value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return _zip
        End Get
        Set(ByVal Value As String)
            _zip = Value
        End Set
    End Property
    Public Property Phone() As String
        Get
            Return _phone
        End Get
        Set(ByVal Value As String)
            _phone = Value
        End Set
    End Property
    Public Property Fax() As String
        Get
            Return _fax
        End Get
        Set(ByVal Value As String)
            _fax = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal Value As String)
            _email = Value
        End Set
    End Property
    Public Property CountyId() As String
        Get
            Return _countyId
        End Get
        Set(ByVal Value As String)
            _countyId = Value
        End Set
    End Property
    Public Property County() As String
        Get
            Return _county
        End Get
        Set(ByVal Value As String)
            _county = Value
        End Set
    End Property
    Public Property LocationId() As String
        Get
            Return _locationId
        End Get
        Set(ByVal Value As String)
            _locationId = Value
        End Set
    End Property
    Public Property Location() As String
        Get
            Return _location
        End Get
        Set(ByVal Value As String)
            _location = Value
        End Set
    End Property
    Public Property JobsCatId() As String
        Get
            Return _JobsCatId
        End Get
        Set(ByVal Value As String)
            _JobsCatId = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class


