Public Class SAPCheckInfo
#Region "Private Variable Declarations"
    Private _StudentId As Guid
    Private _StuEnrollId As Guid
    Private _SAPDetailId As Guid
    Private _Period As Integer
    Private _IsMakingSAP As Boolean
    Private _HrsAttended As Decimal
    Private _HrsEarned As Decimal
    Private _CredsAttempted As Decimal
    Private _CredsEarned As Decimal
    Private _FinCredsEarned As Decimal
    Private _GPA As Decimal
    Private _DatePerf As Date
    Private _Comments As String
    Private _PercCredsEarned As Decimal
    Private _PercAttendance As Decimal
    Private _CheckPointDate As Date
    Private _TermStartDate As Date
#End Region
#Region "SAP Info Properties"

    Public Property StudentId() As Guid
        Get
            Return _StudentId
        End Get
        Set(ByVal Value As Guid)
            _StudentId = Value
        End Set
    End Property
    Public Property StuEnrollId() As Guid
        Get
            Return _StuEnrollId
        End Get
        Set(ByVal Value As Guid)
            _StuEnrollId = Value
        End Set
    End Property
    Public Property SAPDetailId() As Guid
        Get
            Return _SAPDetailId
        End Get
        Set(ByVal Value As Guid)
            _SAPDetailId = Value
        End Set
    End Property
    Public Property Period() As Integer
        Get
            Return _Period
        End Get
        Set(ByVal Value As Integer)
            _Period = Value
        End Set
    End Property
    Public Property IsMakingSAP() As Boolean
        Get
            Return _IsMakingSAP
        End Get
        Set(ByVal Value As Boolean)
            _IsMakingSAP = Value
        End Set
    End Property
    Public Property HrsAttended() As Decimal
        Get
            Return _HrsAttended
        End Get
        Set(ByVal Value As Decimal)
            _HrsAttended = Value
        End Set
    End Property
    Public Property HrsEarned() As Decimal
        Get
            Return _HrsEarned
        End Get
        Set(ByVal Value As Decimal)
            _HrsEarned = Value
        End Set
    End Property
    Public Property CredsAttempted() As Decimal
        Get
            Return _CredsAttempted
        End Get
        Set(ByVal Value As Decimal)
            _CredsAttempted = Value
        End Set
    End Property
    Public Property Comments() As String
        Get
            Return _Comments
        End Get
        Set(ByVal Value As String)
            _Comments = Value
        End Set
    End Property
    Public Property CredsEarned() As Decimal
        Get
            Return _CredsEarned
        End Get
        Set(ByVal Value As Decimal)
            _CredsEarned = Value
        End Set
    End Property
    Public Property FinCredsEarned() As Decimal
        Get
            Return _FinCredsEarned
        End Get
        Set(ByVal Value As Decimal)
            _FinCredsEarned = Value
        End Set
    End Property
    Public Property GPA() As Decimal
        Get
            Return _GPA
        End Get
        Set(ByVal Value As Decimal)
            _GPA = Value
        End Set
    End Property

    Public Property DatePerf() As Date
        Get
            Return _DatePerf
        End Get
        Set(ByVal Value As Date)
            _DatePerf = Value
        End Set
    End Property
    Public Property PercCredsEarned() As Decimal
        Get
            Return _PercCredsEarned
        End Get
        Set(ByVal Value As Decimal)
            _PercCredsEarned = Value
        End Set
    End Property
    Public Property PercAttendance() As Decimal
        Get
            Return _PercAttendance
        End Get
        Set(ByVal Value As Decimal)
            _PercAttendance = Value
        End Set
    End Property
    Public Property CheckPointDate() As Date
        Get
            Return _CheckPointDate
        End Get
        Set(ByVal Value As Date)
            _CheckPointDate = Value
        End Set
    End Property
    Public Property TermStartDate() As Date
        Get
            Return _TermStartDate
        End Get
        Set(ByVal Value As Date)
            _TermStartDate = Value
        End Set
    End Property
#End Region
End Class
