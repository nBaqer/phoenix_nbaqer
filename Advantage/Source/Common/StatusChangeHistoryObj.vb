﻿''' <summary>
''' This class hold the data to be show in StuStatusChangeHist
''' </summary>
''' <remarks></remarks>
<Serializable>
Public Class StatusChangeHistoryObj

    Property DateOfChange() As DateTime
    Property Lda() As DateTime?
    Property Status() As String
    Property Details() As String
    Property ModUser() As String
    Property ModDate() As DateTime
    Property StudentStatusChangeId() As Guid
    Property RequestedBy() As String
    Property CaseNumber() As String
End Class
