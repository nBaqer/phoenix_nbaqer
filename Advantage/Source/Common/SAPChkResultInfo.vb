Public Class SAPChkResultInfo
    Private _StuEnrollId As String
    Private _Increment As Integer
    Private _IsMakingSap As Boolean
    Private _CreditsAttempted As Decimal
    Private _CreditsEarned As Decimal
    Private _GPA As Decimal
    Private _DatePerformed As DateTime
    Private _Comments As String
    Private _ModUser As String
    Private _ModDate As DateTime

    Public Property StuEnrollId() As String
        Get
            Return _StuEnrollId
        End Get
        Set(ByVal Value As String)
            _StuEnrollId = Value
        End Set
    End Property

    Public Property Increment() As Integer
        Get
            Return _Increment
        End Get
        Set(ByVal Value As Integer)
            _Increment = Value
        End Set
    End Property

    Public Property IsMakingSAP() As Boolean
        Get
            Return _IsMakingSap
        End Get
        Set(ByVal Value As Boolean)
            _IsMakingSap = Value
        End Set
    End Property

    Public Property CreditsAttempted() As Decimal
        Get
            Return _CreditsAttempted
        End Get
        Set(ByVal Value As Decimal)
            _CreditsAttempted = Value
        End Set
    End Property

    Public Property CreditsEarned() As Decimal
        Get
            Return _CreditsEarned
        End Get
        Set(ByVal Value As Decimal)
            _CreditsEarned = Value
        End Set
    End Property

    Public Property GPA() As Decimal
        Get
            Return _GPA
        End Get
        Set(ByVal Value As Decimal)
            _GPA = Value
        End Set
    End Property

    Public Property DatePerformed() As DateTime
        Get
            Return _DatePerformed
        End Get
        Set(ByVal Value As DateTime)
            _DatePerformed = Value
        End Set
    End Property

    Public Property Comments() As String
        Get
            Return _Comments
        End Get
        Set(ByVal Value As String)
            _Comments = Value
        End Set
    End Property

    Public Property ModUser() As String
        Get
            Return _ModUser
        End Get
        Set(ByVal Value As String)
            _ModUser = Value
        End Set
    End Property

    Public Property ModDate() As DateTime
        Get
            Return _ModDate
        End Get
        Set(ByVal Value As DateTime)
            _ModDate = Value
        End Set
    End Property

End Class
