Public Class AgingBalance

    Private m_refDate As DateTime
    Private m_goForward As Boolean = False

    Public Property ReferenceDate() As DateTime
        Get
            ReferenceDate = m_refDate
        End Get
        Set(ByVal Value As DateTime)
            m_refDate = Value
        End Set
    End Property

    Public Property GoForward() As Boolean
        Get
            GoForward = m_goForward
        End Get
        Set(ByVal Value As Boolean)
            m_goForward = Value
        End Set
    End Property

    Public ReadOnly Property CurrentStart() As DateTime
        Get
            If Not m_goForward Then
                CurrentStart = m_refDate.AddDays(-30)
            Else
                CurrentStart = m_refDate
            End If
        End Get
    End Property

    Public ReadOnly Property CurrentEnd() As DateTime
        Get
            If Not m_goForward Then
                CurrentEnd = m_refDate
            Else
                CurrentEnd = m_refDate.AddDays(30)
            End If
        End Get
    End Property

    Public ReadOnly Property Start31To60DayPeriod() As DateTime
        Get
            If Not m_goForward Then
                Start31To60DayPeriod = m_refDate.AddDays(-60)
            Else
                Start31To60DayPeriod = m_refDate.AddDays(31)
            End If
        End Get
    End Property

    Public ReadOnly Property End31To60DayPeriod() As DateTime
        Get
            If Not m_goForward Then
                End31To60DayPeriod = CurrentStart.AddDays(-1)
            Else
                End31To60DayPeriod = CurrentStart.AddDays(60)
            End If
        End Get
    End Property

    Public ReadOnly Property Start61To90DayPeriod() As DateTime
        Get
            If Not m_goForward Then
                Start61To90DayPeriod = m_refDate.AddDays(-90)
            Else
                Start61To90DayPeriod = CurrentStart.AddDays(61)
            End If
        End Get
    End Property

    Public ReadOnly Property End61To90DayPeriod() As DateTime
        Get
            If Not m_goForward Then
                End61To90DayPeriod = Start31To60DayPeriod.AddDays(-1)
            Else
                End61To90DayPeriod = CurrentStart.AddDays(90)
            End If
        End Get
    End Property

    Public ReadOnly Property Start91To120Period() As DateTime
        Get
            If Not m_goForward Then
                Start91To120Period = m_refDate.AddDays(-120)
            Else
                Start91To120Period = CurrentStart.AddDays(91)
            End If
        End Get
    End Property

    Public ReadOnly Property End91To120Period() As DateTime
        Get
            If Not m_goForward Then
                End91To120Period = Start61To90DayPeriod.AddDays(-1)
            Else
                End91To120Period = CurrentStart.AddDays(120)
            End If
        End Get
    End Property

    Public ReadOnly Property Over120DayPeriod() As DateTime
        Get
            If Not m_goForward Then
                Over120DayPeriod = Start91To120Period.AddDays(-1)
            Else
                Over120DayPeriod = CurrentStart.AddDays(121)
            End If
        End Get
    End Property

    Public Function GetPeriodRange(ByVal dDate As DateTime) As Byte

        If CurrentStart <= dDate And dDate <= CurrentEnd Then
            Return 1
        ElseIf Start31To60DayPeriod <= dDate And dDate <= End31To60DayPeriod Then
            Return 2
        ElseIf Start61To90DayPeriod <= dDate And dDate <= End61To90DayPeriod Then
            Return 3
        ElseIf Start91To120Period <= dDate And dDate <= End91To120Period Then
            Return 4
        ElseIf dDate <= Over120DayPeriod Then
            Return 5
        Else
            Return 0
        End If

    End Function

End Class
