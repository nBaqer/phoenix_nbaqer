﻿Public Class EDMessageInfo
    'message class for Ed Express messages - all formats are supported with this single class
#Region " Variables and Objects"
    'variable to set default values
    Private _blnIsInDB As Boolean
    Private _blnIsParsed As String
    Private _dateModDate As DateTime
    Private _strModUser As String
    Private _strMsgType As String
    Private _strError As String
    Private _strFileNameSource As String

    'common variable in all the file type Pell, ACG, SMART, Teach and DL
    Private _dataBaseIndicator As String
    Private _filter As String
    Private _originalSSN As String
    Private _academicYearId As String
    Private _awardYearStartDate As String
    Private _awardYearEndDate As String

    ' generic variables for Pell, ACG, SMART and Teach For Student and Disbursement Table
    Private _acceptedDisbursementAmount As String
    Private _disbursementDate As String
    Private _disbursementNumber As String
    Private _disbursementRealeaseIndicator As String
    Private _disbursementSequenceNumber As String
    Private _submittedDisbursementAmount As String
    Private _actionStatsDisbursement As String
    Private _awardAmountForEntireYear As String
    Private _awardId As String
    Private _grantType As String
    Private _pellAddDate As String
    Private _pellAddTime As String
    Private _pellUpdateDate As String
    Private _pellUpdateTime As String
    Private _originationStatus As String
    Private _awardAmountForEntireSchoolYear As String
    Private _teachAddDate As String
    Private _teachAddTime As String
    Private _teachUpdateDate As String
    Private _teachUpdateTime As String

    'generic variables for DL
    Private _loanAddDate As String
    Private _loanAddTime As String
    Private _loanUpdateDate As String
    Private _loanUpdateTime As String
    Private _loanAmountApproved As String
    Private _loanFeePercentage As String
    Private _loanId As String
    Private _loanPeriodEndDate As String
    Private _loanPeriodStartDate As String
    Private _loanStatus As String
    Private _loanType As String
    Private _mpnStatus As String
    Private _actualDisbursementDate As String
    Private _actualDisbursementGrossAmount As String
    Private _actualDisbursementInterestRebateAmount As String
    Private _actualDisbursementLoanFeeAmount As String
    Private _actualDisbursementNetAdjustmentAmount As String
    Private _actualDisbursementNetAmount As String
    Private _actualDisbursementNumber As String
    Private _actualDisbursementSequenceNumber As String
    Private _actualDisbursementStatus As String
    Private _actualDisbursementType As String


    Private _anticipatedDisbursementDate As String
    Private _anticipatedDisbursementFeeAmount As String
    Private _anticipatedDisbursementGrossAmount As String
    Private _anticipatedDisbursementInterestRebateAmount As String
    Private _anticipatedDisbursementNetAmount As String
    Private _anticipatedDisbursementNumber As String
    Private _anticipatedDisbursementRealeaseIndicator As String

#End Region
#Region " Public Constructor"
    Public Sub New(ByVal strMsgType As String, ByVal strRecord As String, Optional ByVal strFileName As String = "")

        _blnIsInDB = False
        _dateModDate = Date.Now
        _strModUser = "sa"
        _strMsgType = strMsgType
        _strError = ""
        _strFileNameSource = strFileName
        _academicYearId = strAcademicYearId
        _awardYearStartDate = ""
        _awardYearEndDate = ""

        'common variable in all the file type Pell, ACG, SMART, Teach and DL
        _dataBaseIndicator = ""
        _filter = ""
        _originalSSN = ""
        _pellAddDate = ""
        _pellAddTime = ""
        _pellUpdateDate = ""
        _pellUpdateTime = ""
        _teachAddDate = ""
        _teachAddTime = ""
        _teachUpdateDate = ""
        _teachUpdateTime = ""
        _disbursementDate = ""
        _disbursementNumber = ""
        _disbursementSequenceNumber = ""
        _actionStatsDisbursement = ""

        'generic variables for Pell, ACG, SMART and Teach
        _acceptedDisbursementAmount = ""
        _disbursementRealeaseIndicator = ""
        _submittedDisbursementAmount = ""
        _awardAmountForEntireYear = ""
        _awardAmountForEntireSchoolYear = ""
        _awardId = ""
        _grantType = ""
        _originationStatus = ""

        'generic variables for DL
        _loanAddDate = ""
        _loanAddTime = ""
        _loanUpdateDate = ""
        _loanUpdateTime = ""
        _loanAmountApproved = ""
        _loanFeePercentage = ""
        _loanId = ""
        _loanPeriodEndDate = ""
        _loanPeriodStartDate = ""
        _loanStatus = ""
        _loanType = ""
        _mpnStatus = ""

        _actualDisbursementDate = ""
        _actualDisbursementGrossAmount = ""
        _actualDisbursementInterestRebateAmount = ""
        _actualDisbursementLoanFeeAmount = ""
        _actualDisbursementNetAdjustmentAmount = ""
        _actualDisbursementNetAmount = ""
        _actualDisbursementNumber = ""
        _actualDisbursementSequenceNumber = ""
        _actualDisbursementStatus = ""
        _actualDisbursementType = ""

        _anticipatedDisbursementDate = ""
        _anticipatedDisbursementFeeAmount = ""
        _anticipatedDisbursementGrossAmount = ""
        _anticipatedDisbursementInterestRebateAmount = ""
        _anticipatedDisbursementNetAmount = ""
        _anticipatedDisbursementNumber = ""
        _anticipatedDisbursementRealeaseIndicator = ""

        _blnIsParsed = ParseMessage(strMsgType, strRecord)
    End Sub
#End Region
#Region "Public Properties"
    Public Property blnIsInDB() As Boolean
        Get
            Return _blnIsInDB
        End Get
        Set(ByVal Value As Boolean)
            _blnIsInDB = Value
        End Set
    End Property
    Public ReadOnly Property dateModDate() As DateTime
        Get
            Return _dateModDate
        End Get
    End Property
    Public ReadOnly Property strModUser() As String
        Get
            Return _strModUser
        End Get
    End Property
    Public Property strAcademicYearId() As String
        Get
            Return _academicYearId
        End Get
        Set(ByVal Value As String)
            _academicYearId = Value
        End Set
    End Property
    Public ReadOnly Property strMsgType() As String
        Get
            Return _strMsgType
        End Get
    End Property
    Public ReadOnly Property strFileNameSource() As String
        Get
            Return _strFileNameSource
        End Get
    End Property
    Public Property strAwardYearEndDate() As String
        Get
            Return _awardYearEndDate
        End Get
        Set(ByVal Value As String)
            _awardYearEndDate = Value
        End Set
    End Property
    Public Property strAwardYearStartDate() As String
        Get
            Return _awardYearStartDate
        End Get
        Set(ByVal Value As String)
            _awardYearStartDate = Value
        End Set
    End Property
    Public Property strError() As String
        Get
            Return _strError
        End Get
        Set(ByVal Value As String)
            _strError += Value
        End Set
    End Property
    Public Property blnIsParsed() As Boolean
        Get
            Return _blnIsParsed
        End Get
        Set(ByVal Value As Boolean)
            _blnIsParsed = Value
        End Set
    End Property
    Public Property strDatabaseIndicator() As String
        Get
            Return _dataBaseIndicator
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then _strError = "Null parameter value provided for Database Indicator."
                _dataBaseIndicator = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strFilter() As String
        Get
            Return _filter
        End Get
        Set(ByVal Value As String)
            _filter = Value
        End Set
    End Property
    Public Property strOriginalSSN() As String
        Get
            Return _originalSSN
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Original SSN."
                _originalSSN = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strPellAddDate() As String
        Get
            Return _pellAddDate
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then _strError = "Null parameter value provided for Add Date."
                _pellAddDate = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strAnticipatedDisbursementDate() As String
        Get
            Return _anticipatedDisbursementDate
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then _strError = "Null parameter value provided for Add Date."
                _anticipatedDisbursementDate = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strPellAddTime() As String
        Get
            Return _pellAddTime
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Add Time."
                _pellAddTime = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property

    Public Property strPellUpdateDate() As String
        Get
            Return _pellUpdateDate
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then _strError = "Null parameter value provided for Update Date."
                _pellUpdateDate = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strPellUpdateTime() As String
        Get
            Return _pellUpdateTime
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Update Time."
                _pellUpdateTime = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strTeachAddDate() As String
        Get
            Return _teachAddDate
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then _strError = "Null parameter value provided for Add Date."
                _teachAddDate = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strTeachAddTime() As String
        Get
            Return _teachAddTime
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Add Time."
                _teachAddTime = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property

    Public Property strTeachUpdateDate() As String
        Get
            Return _teachUpdateDate
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then _strError = "Null parameter value provided for Update Date."
                _teachUpdateDate = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strTeachUpdateTime() As String
        Get
            Return _teachUpdateTime
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Update Time."
                _teachUpdateTime = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strActualDisbursementDate() As String
        Get
            Return _actualDisbursementDate
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Update Time."
                _actualDisbursementDate = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strLoanAddDate() As String
        Get
            Return _loanAddDate
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then _strError = "Null parameter value provided for Add Date."
                _loanAddDate = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strLoanAddTime() As String
        Get
            Return _loanAddTime
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Add Time."
                _loanAddTime = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property

    Public Property strLoanUpdateDate() As String
        Get
            Return _loanUpdateDate
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then _strError = "Null parameter value provided for Update Date."
                _loanUpdateDate = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strLoanUpdateTime() As String
        Get
            Return _loanUpdateTime
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Update Time."
                _loanUpdateTime = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strDisbursementDate() As String
        Get
            Return _disbursementDate
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Date."
                _disbursementDate = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strDisbursementNumber() As String
        Get
            Return _disbursementNumber
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Number."
                _disbursementNumber = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strDisbursementSequenceNumber() As String
        Get
            Return _disbursementSequenceNumber
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Sequence Number."
                _disbursementSequenceNumber = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strActualDisbursementNumber() As String
        Get
            Return _actualDisbursementNumber
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Number."
                _actualDisbursementNumber = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strActualDisbursementStatus() As String
        Get
            Return _actualDisbursementStatus
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Number."
                _actualDisbursementStatus = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strActualDisbursementSequenceNumber() As String
        Get
            Return _actualDisbursementSequenceNumber
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Sequence Number."
                _actualDisbursementSequenceNumber = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strActionStatusDisbursement() As String
        Get
            Return _actionStatsDisbursement
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbusrement Status."
                _actionStatsDisbursement = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strAcceptedDisbursementAmount() As String
        Get
            Return _acceptedDisbursementAmount
        End Get
        Set(ByVal Value As String)
            Try
                _acceptedDisbursementAmount = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strDisbursementRealeaseIndicator() As String
        Get
            Return _disbursementRealeaseIndicator
        End Get
        Set(ByVal Value As String)
            Try
                'If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Release Indicator."
                _disbursementRealeaseIndicator = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strSubmittedDisbursementAmount() As String
        Get
            Return _submittedDisbursementAmount
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Submitted Disbursement Amount."
                _submittedDisbursementAmount = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strAwardAmountForEntireYear() As String
        Get
            Return _awardAmountForEntireYear
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Award Year Amount."
                _awardAmountForEntireYear = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strAwardAmountForEntireSchoolYear() As String
        Get
            Return _awardAmountForEntireSchoolYear
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Award Year Amount."
                _awardAmountForEntireSchoolYear = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strAwardId() As String
        Get
            Return _awardId
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Award Year ID."
                _awardId = Value
            Catch e As ArgumentNullException
                _strError = e.Message
            Catch e As ArgumentOutOfRangeException
                _strError = e.Message
            Catch e As System.Exception
                _strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strGrandType() As String
        Get
            Return _grantType
        End Get
        Set(ByVal Value As String)
            Try
                _grantType = Value
            Catch e As ArgumentNullException
                _strError = e.Message
            Catch e As ArgumentOutOfRangeException
                _strError = e.Message
            Catch e As System.Exception
                _strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strOriginationStatus() As String
        Get
            Return _originationStatus
        End Get
        Set(ByVal Value As String)
            Try
                'If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Origination Status."
                _originationStatus = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strLoanAmountApproved() As String
        Get
            Return _loanAmountApproved
        End Get
        Set(ByVal Value As String)
            Try
                'If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Loan Amount Approved."
                _loanAmountApproved = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strLoanFeePercentage() As String
        Get
            Return _loanFeePercentage
        End Get
        Set(ByVal Value As String)
            Try
                'If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Loan Fee Percentage."
                _loanFeePercentage = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strLoanId() As String
        Get
            Return _loanId
        End Get
        Set(ByVal Value As String)
            Try
                'If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Loan ID."
                _loanId = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strLoanPeriodEndDate() As String
        Get
            Return _loanPeriodEndDate
        End Get
        Set(ByVal Value As String)
            Try
                'If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Loan Period End Date."
                _loanPeriodEndDate = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strLoanPeriodStartDate() As String
        Get
            Return _loanPeriodStartDate
        End Get
        Set(ByVal Value As String)
            Try
                'If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Loan Period Start Date."
                _loanPeriodStartDate = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property

    Public Property strLoanStatus() As String
        Get
            Return _loanStatus
        End Get
        Set(ByVal Value As String)
            Try
                'If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Loan Status."
                _loanStatus = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strLoanType() As String
        Get
            Return _loanType
        End Get
        Set(ByVal Value As String)
            Try
                'If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Loan Type."
                _loanType = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strMPNStatus() As String
        Get
            Return _mpnStatus
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for MPN Status."
                _mpnStatus = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strActualDisbursementGrossAmount() As String
        Get
            Return _actualDisbursementGrossAmount
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Gross Amount."
                _actualDisbursementGrossAmount = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strAnticipatedDisbursementFeeAmount() As String
        Get
            Return _anticipatedDisbursementFeeAmount
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Gross Amount."
                _anticipatedDisbursementFeeAmount = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strAnticipatedDisbursementGrossAmount() As String
        Get
            Return _anticipatedDisbursementGrossAmount
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Gross Amount."
                _anticipatedDisbursementGrossAmount = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strAnticipatedDisbursementInterestRebateAmount() As String
        Get
            Return _anticipatedDisbursementInterestRebateAmount
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Gross Amount."
                _anticipatedDisbursementInterestRebateAmount = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strAnticipatedDisbursementNetAmount() As String
        Get
            Return _anticipatedDisbursementNetAmount
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Gross Amount."
                _anticipatedDisbursementNetAmount = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strAnticipatedDisbursementNumber() As String
        Get
            Return _anticipatedDisbursementNumber
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Gross Amount."
                _anticipatedDisbursementNumber = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strAnticipatedDisbursementRealeaseIndicator() As String
        Get
            Return _anticipatedDisbursementRealeaseIndicator
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Gross Amount."
                _anticipatedDisbursementRealeaseIndicator = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strActualDisbursementInterestRebateAmount() As String
        Get
            Return _actualDisbursementInterestRebateAmount
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Intrest Rebate Amount."
                _actualDisbursementInterestRebateAmount = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strActualDisbursementLoanFeeAmount() As String
        Get
            Return _actualDisbursementLoanFeeAmount
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Loan Fee Amount."
                _actualDisbursementLoanFeeAmount = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strActualDisbursementNetAdjustmentAmount() As String
        Get
            Return _actualDisbursementNetAdjustmentAmount
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbursement Net Adjustemnt Amount."
                _actualDisbursementNetAdjustmentAmount = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public Property strActualDisbursementNetAmount() As String
        Get
            Return _actualDisbursementNetAmount
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbusrement Net Amount."
                _actualDisbursementNetAmount = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property

    Public Property strActualDisbursementType() As String
        Get
            Return _actualDisbursementType
        End Get
        Set(ByVal Value As String)
            Try
                ''If String.IsNullOrEmpty(Value) Then strError = "Null parameter value provided for Disbusrement Type."
                _actualDisbursementType = Value
            Catch e As ArgumentNullException
                strError = e.Message
            Catch e As ArgumentOutOfRangeException
                strError = e.Message
            Catch e As System.Exception
                strError = e.Message
                Throw
            End Try
        End Set
    End Property
#End Region
#Region " Methods"
    Public Function ParseMessage(ByVal strMsgType As String, ByVal strRecord As String) As Boolean

        Dim strProcessRecord As String

        ParseMessage = False
        If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided")
        If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strMsgType", "Empty message type parameter provided")
        _strError = ""
        Try
            If Len(strRecord) > 0 Then
                Dim ArrRecord() As String
                Dim splitChar As Char = ","
                If strRecord.Length > 1 Then
                    ArrRecord = strRecord.Split(splitChar)
                Else
                    ArrRecord = strRecord.Split(" ")
                End If
                If strMsgType = "PELL" Then
                    If ArrRecord.Length > 0 Then
                        strProcessRecord = ArrRecord(0)
                        Select Case strProcessRecord.Trim()
                            Case "T"
                                ParseMessage = ParseTMessage(ArrRecord)
                            Case "S"
                                ParseMessage = ParseSMessage(ArrRecord)
                            Case "C"
                                ParseMessage = ParseCMessage(ArrRecord)
                            Case "H"
                                ParseMessage = ParseHMessage(ArrRecord)
                        End Select
                    End If
                Else
                    If ArrRecord.Length > 0 Then
                        strProcessRecord = ArrRecord(0)
                        Select Case strProcessRecord.Trim()
                            Case "A"
                                ParseMessage = ParseAMessage(ArrRecord)
                            Case "B"
                                ParseMessage = ParseBMessage(ArrRecord)
                            Case "D"
                                ParseMessage = ParseDMessage(ArrRecord)
                            Case "M"
                                ParseMessage = ParseMMessage(ArrRecord)
                            Case "N"
                                ParseMessage = ParseNMessage(ArrRecord)
                        End Select
                    End If
                End If
            End If

        Catch e As ArgumentNullException
            strError = e.Message
            Throw
        Catch e As ArgumentOutOfRangeException
            strError = e.Message
            Throw
        Catch e As System.Exception
            strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseTMessage(ByVal strArrRecord() As String) As Boolean

        ParseTMessage = False
        'If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided for FAID message parse")
        Try
            strError = ""
            If strArrRecord.Length = 1 Then
                strDatabaseIndicator = strArrRecord(0).Trim()
                ParseTMessage = False
                Exit Function
            Else
                'strDatabaseIndicator = strArrRecord(0).Trim()
                'strFilter = strArrRecord(1).Trim()
                'strAwardAmountForEntireYear = strArrRecord(2).Trim()
                'strAwardId = strArrRecord(3).Trim()
                'strGrandType = strArrRecord(4).Trim()
                'strPellAddDate = strArrRecord(5).Trim()
                'strPellAddTime = strArrRecord(6).Trim()
                'strOriginalSSN = strArrRecord(7).Trim()
                'strPellUpdateDate = strArrRecord(8).Trim()
                'strPellUpdateTime = strArrRecord(9).Trim()
                'strOriginationStatus = strArrRecord(10).Trim()

                'As per Troy Mail Send Thursday, August 20, 2009 made change in the position of filed ORIGINATION STATUS

                strDatabaseIndicator = strArrRecord(0).Trim()
                strFilter = strArrRecord(1).Trim()
                strAwardAmountForEntireYear = strArrRecord(2).Trim()
                strAwardId = strArrRecord(3).Trim()
                strGrandType = strArrRecord(4).Trim()
                strOriginationStatus = strArrRecord(5).Trim()
                strPellAddDate = strArrRecord(6).Trim()
                strPellAddTime = strArrRecord(7).Trim()
                strOriginalSSN = strArrRecord(8).Trim()
                strPellUpdateDate = strArrRecord(9).Trim()
                strPellUpdateTime = strArrRecord(10).Trim()

            End If
            ParseTMessage = True
        Catch e As ArgumentNullException
            strError = e.Message
        Catch e As ArgumentOutOfRangeException
            strError = e.Message
        Catch e As System.Exception
            strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseSMessage(ByVal strArrRecord() As String) As Boolean

        ParseSMessage = False
        Try
            strError = ""
            If strArrRecord.Length = 1 Then
                strDatabaseIndicator = strArrRecord(0).Trim()
                ParseSMessage = False
                Exit Function
            Else
                'strDatabaseIndicator = strArrRecord(0).Trim()
                'strFilter = strArrRecord(1).Trim()
                'strAcceptedDisbursementAmount = strArrRecord(2).Trim()
                'strDisbursementDate = strArrRecord(3).Trim()
                'strDisbursementNumber = strArrRecord(4).Trim()
                'strDisbursementRealeaseIndicator = strArrRecord(5).Trim()
                'strDisbursementSequenceNumber = strArrRecord(6).Trim()
                'strSubmittedDisbursementAmount = strArrRecord(7).Trim()
                'strActionStatusDisbursement = strArrRecord(8).Trim()

                'As per Troy Mail Send Thursday, August 20, 2009 made change in the position of filed ACTION STATUS - DISBURSEMENT

                strDatabaseIndicator = strArrRecord(0).Trim()
                strFilter = strArrRecord(1).Trim()
                strAcceptedDisbursementAmount = strArrRecord(2).Trim()
                strActionStatusDisbursement = strArrRecord(3).Trim()
                strDisbursementDate = strArrRecord(4).Trim()
                strDisbursementNumber = strArrRecord(5).Trim()
                strDisbursementRealeaseIndicator = strArrRecord(6).Trim()
                strDisbursementSequenceNumber = strArrRecord(7).Trim()
                strSubmittedDisbursementAmount = strArrRecord(8).Trim()

            End If
            ParseSMessage = True
        Catch e As ArgumentNullException
            strError = e.Message
        Catch e As ArgumentOutOfRangeException
            strError = e.Message
        Catch e As System.Exception
            strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseCMessage(ByVal strArrRecord() As String) As Boolean
        ParseCMessage = False
        Try
            strError = ""
            If strArrRecord.Length = 1 Then
                strDatabaseIndicator = strArrRecord(0).Trim()
                ParseCMessage = False
                Exit Function
            Else
                strDatabaseIndicator = strArrRecord(0).Trim()
                strFilter = strArrRecord(1)
                strAcceptedDisbursementAmount = strArrRecord(2).Trim()
                strDisbursementDate = strArrRecord(3).Trim()
                strDisbursementNumber = strArrRecord(4).Trim()
                strDisbursementRealeaseIndicator = strArrRecord(5).Trim()
                strDisbursementSequenceNumber = strArrRecord(6).Trim()
                strActionStatusDisbursement = strArrRecord(7).Trim()
                strSubmittedDisbursementAmount = strArrRecord(8).Trim()
            End If
            ParseCMessage = True
        Catch e As ArgumentNullException
            strError = e.Message
        Catch e As ArgumentOutOfRangeException
            strError = e.Message
        Catch e As System.Exception
            strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseHMessage(ByVal strArrRecord() As String) As Boolean

        ParseHMessage = False
        Try
            _strError = ""
            If strArrRecord.Length = 1 Then
                strDatabaseIndicator = strArrRecord(0).Trim()
                ParseHMessage = False
                Exit Function
            Else
                strDatabaseIndicator = strArrRecord(0).Trim()
                strFilter = strArrRecord(1).Trim()
                strAwardAmountForEntireSchoolYear = strArrRecord(2).Trim()
                strAwardId = strArrRecord(3).Trim()
                strOriginationStatus = strArrRecord(4).Trim()
                strTeachAddDate = strArrRecord(5).Trim()
                strTeachAddTime = strArrRecord(6).Trim()
                strOriginalSSN = strArrRecord(7).Trim()
                strTeachUpdateDate = strArrRecord(8).Trim()
                strTeachUpdateTime = strArrRecord(9).Trim()
                strGrandType = "TT"
            End If
            ParseHMessage = True
        Catch e As ArgumentNullException
            strError = e.Message
        Catch e As ArgumentOutOfRangeException
            strError = e.Message
        Catch e As System.Exception
            strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseAMessage(ByVal strArrRecord() As String) As Boolean
        ParseAMessage = False
        Try
            strError = ""
            strDatabaseIndicator = strArrRecord(0).Trim()
            ParseAMessage = False
        Catch e As ArgumentNullException
            strError = e.Message
        Catch e As ArgumentOutOfRangeException
            strError = e.Message
        Catch e As System.Exception
            strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseBMessage(ByVal strArrRecord() As String) As Boolean
        ParseBMessage = False
        'If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided for FAID message parse")
        Try
            strError = ""
            strDatabaseIndicator = strArrRecord(0).Trim()
            ParseBMessage = False
        Catch e As ArgumentNullException
            strError = e.Message
        Catch e As ArgumentOutOfRangeException
            strError = e.Message
        Catch e As System.Exception
            strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseDMessage(ByVal strArrRecord() As String) As Boolean
        ParseDMessage = False
        Try
            strError = ""
            If strArrRecord.Length = 1 Then
                strDatabaseIndicator = strArrRecord(0).Trim()
                ParseDMessage = False
                Exit Function
            Else
                strDatabaseIndicator = strArrRecord(0).Trim()
                strFilter = strArrRecord(1).Trim()
                strLoanAddDate = strArrRecord(2).Trim()
                strLoanAddTime = strArrRecord(3).Trim()
                strLoanAmountApproved = strArrRecord(4).Trim()
                strLoanFeePercentage = strArrRecord(5).Trim()
                strLoanId = strArrRecord(6).Trim()
                strLoanPeriodEndDate = strArrRecord(7).Trim()
                strLoanPeriodStartDate = strArrRecord(8).Trim()
                strLoanStatus = strArrRecord(9).Trim()
                strLoanType = strArrRecord(10).Trim()
                strMPNStatus = strArrRecord(11).Trim()
                strOriginalSSN = strArrRecord(12).Trim()
                strLoanUpdateDate = strArrRecord(13).Trim()
                strLoanUpdateTime = strArrRecord(14).Trim()
            End If
            ParseDMessage = True
        Catch e As ArgumentNullException
            strError = e.Message
        Catch e As ArgumentOutOfRangeException
            strError = e.Message
        Catch e As System.Exception
            strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseMMessage(ByVal strArrRecord() As String) As Boolean

        ParseMMessage = False
        Try
            strError = ""
            If strArrRecord.Length = 1 Then
                strDatabaseIndicator = strArrRecord(0).Trim()
                ParseMMessage = False
                Exit Function
            Else
                strDatabaseIndicator = strArrRecord(0).Trim()
                strFilter = strArrRecord(1).Trim()
                strActualDisbursementDate = strArrRecord(2).Trim()
                strActualDisbursementGrossAmount = strArrRecord(3).Trim()
                strActualDisbursementInterestRebateAmount = strArrRecord(4).Trim()
                strActualDisbursementLoanFeeAmount = strArrRecord(5).Trim()
                strActualDisbursementNetAdjustmentAmount = strArrRecord(6).Trim()
                strActualDisbursementNetAmount = strArrRecord(7).Trim()
                strActualDisbursementNumber = strArrRecord(8).Trim()
                strActualDisbursementSequenceNumber = strArrRecord(9).Trim()
                strActualDisbursementStatus = strArrRecord(10).Trim()
                strActualDisbursementType = strArrRecord(11).Trim()
                strLoanId = strArrRecord(12).Trim()
            End If
            ParseMMessage = True
        Catch e As ArgumentNullException
            strError = e.Message
        Catch e As ArgumentOutOfRangeException
            strError = e.Message
        Catch e As System.Exception
            strError = e.Message
            Throw
        End Try
    End Function
    Private Function ParseNMessage(ByVal strArrRecord() As String) As Boolean

        ParseNMessage = False
        'If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided for FAID message parse")
        Try
            strError = ""
            If strArrRecord.Length = 1 Then
                strDatabaseIndicator = strArrRecord(0).Trim()
                ParseNMessage = False
                Exit Function
            Else
                strDatabaseIndicator = strArrRecord(0).Trim()
                strFilter = strArrRecord(1).Trim()
                strAnticipatedDisbursementDate = strArrRecord(2).Trim()
                strAnticipatedDisbursementFeeAmount = strArrRecord(3).Trim()
                strAnticipatedDisbursementGrossAmount = strArrRecord(4).Trim()
                strAnticipatedDisbursementInterestRebateAmount = strArrRecord(5).Trim()
                strLoanId = strArrRecord(6).Trim()
                strAnticipatedDisbursementNetAmount = strArrRecord(7).Trim()
                strAnticipatedDisbursementNumber = strArrRecord(8).Trim()
                Try
                    strAnticipatedDisbursementRealeaseIndicator = strArrRecord(9).Trim()
                Catch ex As Exception
                    strAnticipatedDisbursementRealeaseIndicator = ""
                End Try

            End If
            ParseNMessage = True
        Catch e As ArgumentNullException
            strError = e.Message
        Catch e As ArgumentOutOfRangeException
            strError = e.Message
        Catch e As System.Exception
            strError = e.Message
            Throw
        End Try
    End Function
#End Region
End Class
Public Class EDCollectionMessageInfos
#Region " Private Variables and Objects"
    Private _clsCollectionMessage As EDCollectionMessageInfo
#End Region
#Region " Public Constructors "
    Public Sub New()
        _clsCollectionMessage = New EDCollectionMessageInfo
    End Sub
#End Region
#Region " Public Properties"
    Public ReadOnly Property Count() As Integer
        Get
            Return _clsCollectionMessage.Count
        End Get
    End Property
#End Region
#Region " Public Methods"
    Public Sub Add(ByVal strMsgType As String, ByVal strRecord As String, Optional ByVal strFileName As String = "")
        Try
            If String.IsNullOrEmpty(strRecord) Then Throw New ArgumentNullException("strRecord", "Empty message record parameter provided")
            If String.IsNullOrEmpty(strMsgType) Then Throw New ArgumentNullException("strMsgType", "Empty message type parameter provided")
            Dim clsFameLinkMessage As New EDMessageInfo(strMsgType, strRecord, strFileName)
            Me.Add(clsFameLinkMessage)

        Catch e As ArgumentNullException
            Throw
        Catch e As ArgumentOutOfRangeException
            Throw
        Catch e As System.Exception
            Throw
        End Try
    End Sub
    Public Sub Add(ByVal clsMsgEntry As EDMessageInfo)
        _clsCollectionMessage.Add(clsMsgEntry)
    End Sub
    Public Function Items(ByVal index As Integer) As EDMessageInfo
        Try
            Return CType(_clsCollectionMessage.Items(index), EDMessageInfo)
        Catch e As System.Exception
            Throw
        End Try
    End Function

#End Region
End Class
Public Class EDCollectionMessageInfo
    Inherits CollectionBase
#Region " Public Constructors "
    Public Sub New()
        MyBase.New()
    End Sub
#End Region
#Region " Public Properties"
    Public ReadOnly Property Items(ByVal index As Integer) As EDMessageInfo
        Get
            Try
                Return CType(List.Item(index), EDMessageInfo)
            Catch e As System.Exception
                Throw
            End Try
        End Get
    End Property
#End Region
#Region " Public Methods"
    Public Sub Add(ByVal clsFameLinkMessage As EDMessageInfo)
        List.Add(clsFameLinkMessage)
    End Sub
#End Region
End Class
