Public Class LenderInfo
    '
    '   LenderInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _LenderId As String
    Private _statusId As String
    Private _status As String
    Private _campusgroupId As String
    Private _campusgroup As String
    Private _Code As String
    Private _LenderDescrip As String
    Private _ForeignAddress As Boolean
    Private _Address1 As String
    Private _Address2 As String
    Private _City As String
    Private _StateId As String
    Private _State As String
    Private _OtherState As String
    Private _Zip As String
    Private _CountryId As String
    Private _country As String
    Private _PrimaryContact As String
    Private _OtherContact As String
    Private _email As String
    Private _IsLender As Boolean
    Private _IsServicer As Boolean
    Private _IsGuarantor As Boolean
    Private _ForeignPayAddress As Boolean
    Private _PayAddress1 As String
    Private _PayAddress2 As String
    Private _PayCity As String
    Private _PayStateId As String
    Private _payState As String
    Private _OtherPayState As String
    Private _PayZip As String
    Private _PayCountryId As String
    Private _payCountry As String
    Private _CustService As String
    Private _ForeignCustService As Boolean
    Private _Fax As String
    Private _ForeignFax As Boolean
    Private _PreClaim As String
    Private _ForeignPreClaim As Boolean
    Private _PostClaim As String
    Private _ForeignPostClaim As Boolean
    Private _Comments As String
    Private _IsUsedAsLender As Boolean
    Private _IsUsedAsServicer As Boolean
    Private _IsUsedAsGuarantor As Boolean
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _LenderId = Guid.NewGuid.ToString
        _statusId = FAME.AdvantageV1.Common.AdvantageCommonValues.ActiveGuid
        _Code = ""
        _LenderDescrip = ""
        _ForeignAddress = 0
        _Address1 = ""
        _Address2 = ""
        _City = ""
        _StateId = Guid.Empty.ToString
        _State = "Select"
        _campusgroupId = Guid.Empty.ToString
        _campusgroup = "Select"
        _OtherState = ""
        _Zip = ""
        _CountryId = Guid.Empty.ToString
        _country = ""
        _PrimaryContact = ""
        _OtherContact = ""
        _email = ""
        _IsLender = 0
        _IsServicer = 0
        _IsGuarantor = 0
        _ForeignPayAddress = 0
        _PayAddress1 = ""
        _PayAddress2 = ""
        _PayCity = ""
        _PayStateId = Guid.Empty.ToString
        _payState = ""
        _OtherPayState = ""
        _PayZip = ""
        _PayCountryId = Guid.Empty.ToString
        _payCountry = ""
        _CustService = ""
        _ForeignCustService = 0
        _Fax = ""
        _ForeignFax = 0
        _PreClaim = ""
        _ForeignPreClaim = 0
        _PostClaim = ""
        _ForeignPostClaim = 0
        _Comments = ""
        _IsUsedAsLender = False
        _IsUsedAsServicer = False
        _IsUsedAsGuarantor = False
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
    'Public Sub New(ByVal studentId As String)
    '    _isInDB = False
    '    _LenderId = Guid.NewGuid.ToString
    '    _statusId = FAME.AdvantageV1.Common.AdvantageCommonValues.ActiveGuid
    '    _Code = ""
    '    _LenderDescrip = ""
    '    _ForeignAddress = 0
    '    _Address1 = ""
    '    _Address2 = ""
    '    _City = ""
    '    _StateId = Guid.Empty.ToString
    '    _OtherState = ""
    '    _Zip = ""
    '    _CountryId = Guid.Empty.ToString
    '    _PrimaryContact = ""
    '    _OtherContact = ""
    '    _email = ""
    '    _IsLender = 0
    '    _IsServicer = 0
    '    _IsGuarantor = 0
    '    _ForeignPayAddress = 0
    '    _PayAddress1 = ""
    '    _PayAddress2 = ""
    '    _PayCity = ""
    '    _PayStateId = Guid.Empty.ToString
    '    _OtherPayState = ""
    '    _PayZip = ""
    '    _PayCountryId = Guid.Empty.ToString
    '    _CustService = ""
    '    _ForeignCustService = 0
    '    _Fax = ""
    '    _ForeignFax = 0
    '    _PreClaim = ""
    '    _ForeignPreClaim = 0
    '    _PostClaim = ""
    '    _ForeignPostClaim = 0
    '    _modDate = Date.MinValue
    'End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property LenderId() As String
        Get
            Return _LenderId
        End Get
        Set(ByVal Value As String)
            _LenderId = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property CampusGroupId() As String
        Get
            Return _campusgroupId
        End Get
        Set(ByVal Value As String)
            _campusgroupId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _Code
        End Get
        Set(ByVal Value As String)
            _Code = Value
        End Set
    End Property
    Public Property LenderDescrip() As String
        Get
            Return _LenderDescrip
        End Get
        Set(ByVal Value As String)
            _LenderDescrip = Value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return _Address1
        End Get
        Set(ByVal Value As String)
            _Address1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return _Address2
        End Get
        Set(ByVal Value As String)
            _Address2 = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _City
        End Get
        Set(ByVal Value As String)
            _City = Value
        End Set
    End Property
    Public Property StateId() As String
        Get
            Return _StateId
        End Get
        Set(ByVal Value As String)
            _StateId = Value
        End Set
    End Property
    Public Property State() As String
        Get
            Return _State
        End Get
        Set(ByVal Value As String)
            _State = Value
        End Set
    End Property
    Public Property OtherState() As String
        Get
            Return _OtherState
        End Get
        Set(ByVal Value As String)
            _OtherState = Value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return _Zip
        End Get
        Set(ByVal Value As String)
            _Zip = Value
        End Set
    End Property
    Public Property ForeignAddress() As Boolean
        Get
            Return _ForeignAddress
        End Get
        Set(ByVal Value As Boolean)
            _ForeignAddress = Value
        End Set
    End Property
    Public Property CountryId() As String
        Get
            Return _CountryId
        End Get
        Set(ByVal Value As String)
            _CountryId = Value
        End Set
    End Property
    Public Property Country() As String
        Get
            Return _country
        End Get
        Set(ByVal Value As String)
            _country = Value
        End Set
    End Property
    Public Property PrimaryContact() As String
        Get
            Return _PrimaryContact
        End Get
        Set(ByVal Value As String)
            _PrimaryContact = Value
        End Set
    End Property
    Public Property OtherContact() As String
        Get
            Return _OtherContact
        End Get
        Set(ByVal Value As String)
            _OtherContact = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Email = _email
        End Get
        Set(ByVal Value As String)
            _email = Value
        End Set
    End Property
    Public Property IsLender() As Boolean
        Get
            Return _IsLender
        End Get
        Set(ByVal Value As Boolean)
            _IsLender = Value
        End Set
    End Property
    Public Property IsServicer() As Boolean
        Get
            Return _IsServicer
        End Get
        Set(ByVal Value As Boolean)
            _IsServicer = Value
        End Set
    End Property
    Public Property IsGuarantor() As Boolean
        Get
            Return _IsGuarantor
        End Get
        Set(ByVal Value As Boolean)
            _IsGuarantor = Value
        End Set
    End Property
    Public Property PayAddress1() As String
        Get
            Return _PayAddress1
        End Get
        Set(ByVal Value As String)
            _PayAddress1 = Value
        End Set
    End Property
    Public Property PayAddress2() As String
        Get
            Return _PayAddress2
        End Get
        Set(ByVal Value As String)
            _PayAddress2 = Value
        End Set
    End Property
    Public Property PayCity() As String
        Get
            Return _PayCity
        End Get
        Set(ByVal Value As String)
            _PayCity = Value
        End Set
    End Property
    Public Property PayStateId() As String
        Get
            Return _PayStateId
        End Get
        Set(ByVal Value As String)
            _PayStateId = Value
        End Set
    End Property
    Public Property PayState() As String
        Get
            Return _payState
        End Get
        Set(ByVal Value As String)
            _payState = Value
        End Set
    End Property
    Public Property OtherPayState() As String
        Get
            Return _OtherPayState
        End Get
        Set(ByVal Value As String)
            _OtherPayState = Value
        End Set
    End Property
    Public Property PayZip() As String
        Get
            Return _PayZip
        End Get
        Set(ByVal Value As String)
            _PayZip = Value
        End Set
    End Property
    Public Property ForeignPayAddress() As Boolean
        Get
            Return _ForeignPayAddress
        End Get
        Set(ByVal Value As Boolean)
            _ForeignPayAddress = Value
        End Set
    End Property
    Public Property PayCountryId() As String
        Get
            Return _PayCountryId
        End Get
        Set(ByVal Value As String)
            _PayCountryId = Value
        End Set
    End Property
    Public Property PayCountry() As String
        Get
            Return _payCountry
        End Get
        Set(ByVal Value As String)
            _payCountry = Value
        End Set
    End Property
    Public Property CustService() As String
        Get
            Return _CustService
        End Get
        Set(ByVal Value As String)
            _CustService = Value
        End Set
    End Property
    Public Property ForeignCustService() As Boolean
        Get
            Return _ForeignCustService
        End Get
        Set(ByVal Value As Boolean)
            _ForeignCustService = Value
        End Set
    End Property
    Public Property Fax() As String
        Get
            Return _Fax
        End Get
        Set(ByVal Value As String)
            _Fax = Value
        End Set
    End Property
    Public Property ForeignFax() As Boolean
        Get
            Return _ForeignFax
        End Get
        Set(ByVal Value As Boolean)
            _ForeignFax = Value
        End Set
    End Property
    Public Property PreClaim() As String
        Get
            Return _PreClaim
        End Get
        Set(ByVal Value As String)
            _PreClaim = Value
        End Set
    End Property
    Public Property ForeignPreClaim() As Boolean
        Get
            Return _ForeignPreClaim
        End Get
        Set(ByVal Value As Boolean)
            _ForeignPreClaim = Value
        End Set
    End Property
    Public Property PostClaim() As String
        Get
            Return _PostClaim
        End Get
        Set(ByVal Value As String)
            _PostClaim = Value
        End Set
    End Property
    Public Property ForeignPostClaim() As Boolean
        Get
            Return _ForeignPostClaim
        End Get
        Set(ByVal Value As Boolean)
            _ForeignPostClaim = Value
        End Set
    End Property
    Public Property Comments() As String
        Get
            Comments = _Comments
        End Get
        Set(ByVal Value As String)
            _Comments = Value
        End Set
    End Property
    Public Property IsUsedAsLender() As Boolean
        Get
            Return _IsUsedAsLender
        End Get
        Set(ByVal Value As Boolean)
            _IsUsedAsLender = Value
        End Set
    End Property
    Public Property IsUsedAsServicer() As Boolean
        Get
            Return _IsUsedAsServicer
        End Get
        Set(ByVal Value As Boolean)
            _IsUsedAsServicer = Value
        End Set
    End Property
    Public Property IsUsedAsGuarantor() As Boolean
        Get
            Return _IsUsedAsGuarantor
        End Get
        Set(ByVal Value As Boolean)
            _IsUsedAsGuarantor = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
