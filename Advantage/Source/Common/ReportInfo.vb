Public Class ReportInfo
#Region "Private Variables"
    Private m_ResourceID As Integer
    Private m_SqlId As Integer
    Private m_ObjId As Integer
    Private m_Url As String
    Private m_Resource As String
#End Region

#Region "Public Properties"

    Public Property ResourceID() As Integer
        Get
            Return m_ResourceID
        End Get
        Set(ByVal Value As Integer)
            m_ResourceID = Value
        End Set
    End Property

    Public Property SqlId() As Integer
        Get
            Return m_SqlId
        End Get
        Set(ByVal Value As Integer)
            m_SqlId = Value
        End Set
    End Property

    Public Property ObjId() As Integer
        Get
            Return m_ObjId
        End Get
        Set(ByVal Value As Integer)
            m_ObjId = Value
        End Set
    End Property

    Public Property Url() As String
        Get
            Return m_Url
        End Get
        Set(ByVal Value As String)
            m_Url = Value
        End Set
    End Property

    Public Property Resource() As String
        Get
            Return m_Resource
        End Get
        Set(ByVal Value As String)
            m_Resource = Value
        End Set
    End Property
#End Region
End Class
Public Class AdvantageLogoImage
#Region "Private Variables"
    Private _schoolId As Integer
    Private _imgLen As Integer
    Private _image As Byte()
    Private _contentType As String
#End Region
#Region "Public Constructors"
    Public Sub New()

    End Sub
#End Region
#Region "Public Properties"
    Public Property SchoolId() As Integer
        Get
            Return _schoolId
        End Get
        Set(ByVal Value As Integer)
            _schoolId = Value
        End Set
    End Property
    Public Property ImageLength() As Integer
        Get
            Return _imgLen
        End Get
        Set(ByVal Value As Integer)
            _imgLen = Value
        End Set
    End Property
    Public Property Image() As Byte()
        Get
            Return _image
        End Get
        Set(ByVal Value As Byte())
            _image = Value
        End Set
    End Property
    Public Property ContentType() As String
        Get
            Return _contentType
        End Get
        Set(ByVal Value As String)
            _contentType = Value
        End Set
    End Property
#End Region
End Class
