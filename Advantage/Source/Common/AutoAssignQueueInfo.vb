Public Class AutoAssignQueueInfo
    'test
    Private _AutoAssignQueueId As Guid
    Private _AutoAssignRuleId As Guid
    Private _BasisValuePKId As Guid
    Private _ObjectName As String
    Private _DateToCreate As DateTime

#Region "Activity Assignment Properties"

    Public Property AutoAssignQueueId() As Guid
        Get
            AutoAssignQueueId = _AutoAssignQueueId
        End Get
        Set(ByVal Value As Guid)
            _AutoAssignQueueId = Value
        End Set
    End Property
    Public Property AutoAssignRuleId() As Guid
        Get
            AutoAssignRuleId = _AutoAssignRuleId
        End Get
        Set(ByVal Value As Guid)
            _AutoAssignRuleId = Value
        End Set
    End Property
    Public Property BasisValuePKId() As Guid
        Get
            BasisValuePKId = _BasisValuePKId
        End Get
        Set(ByVal Value As Guid)
            _BasisValuePKId = Value
        End Set
    End Property
    Public Property ObjectName() As String
        Get
            ObjectName = _ObjectName
        End Get
        Set(ByVal Value As String)
            _ObjectName = Value
        End Set
    End Property
    Public Property DateToCreate() As DateTime
        Get
            DateToCreate = _DateToCreate
        End Get
        Set(ByVal Value As DateTime)
            _DateToCreate = Value
        End Set
    End Property

#End Region

#Region "New Instance of the Class Declaration"
    Public Sub New(ByVal NewAutoAssignQueueId As Guid, _
    ByVal NewAutoAssignRuleId As Guid, _
    ByVal NewBasisValuePKId As Guid, _
    ByVal NewObjectName As String, _
    ByVal NewDateToCreate As DateTime)

        _AutoAssignQueueId = NewAutoAssignQueueId
        _AutoAssignRuleId = NewAutoAssignRuleId
        _BasisValuePKId = NewBasisValuePKId
        _ObjectName = NewObjectName
        _DateToCreate = NewDateToCreate


    End Sub

    Public Sub New()
        'MyBase.new()
    End Sub

#End Region

End Class
