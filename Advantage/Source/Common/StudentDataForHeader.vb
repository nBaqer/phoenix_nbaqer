Public Class StudentDataForHeaderInfo
    '
    '   StudentDataForHeaderInfo Class
    '
#Region " Private Variables and Objects"
    Private _studentId As String
    Private _statusCode As String
    Private _name As String
    Private _programVersion As String
    Private _startDate As Date
    Private _graduationDate As Date
    Private _lastNoteDate As Date
    Private _cohortStartDate As Date
#End Region
#Region " Public Constructors "
    Public Sub New()

    End Sub
    Public Sub New(ByVal studentId As String)
        _studentId = studentId 
    End Sub
#End Region
#Region " Public Properties"
  
    Public Property StudentId() As String
        Get
            Return _studentId
        End Get
        Set(ByVal Value As String)
            _studentId = Value
        End Set
    End Property
    Public Property StatusCode() As String
        Get
            Return _statusCode
        End Get
        Set(ByVal Value As String)
            _statusCode = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal Value As String)
            _name = Value
        End Set
    End Property
    Public Property ProgramVersion() As String
        Get
            Return _programVersion
        End Get
        Set(ByVal Value As String)
            _programVersion = Value
        End Set
    End Property
    Public Property StartDate() As Date
        Get
            Return _startDate
        End Get
        Set(ByVal Value As Date)
            _startDate = Value
        End Set
    End Property
    Public Property CohortStartDate() As Date
        Get
            Return _cohortStartDate
        End Get
        Set(ByVal Value As Date)
            _cohortStartDate = Value
        End Set
    End Property
    Public Property GraduationDate() As Date
        Get
            Return _graduationDate
        End Get
        Set(ByVal Value As Date)
            _graduationDate = Value
        End Set
    End Property
    Public Property LastNoteDate() As Date
        Get
            Return _lastNoteDate
        End Get
        Set(ByVal Value As Date)
            _lastNoteDate = Value
        End Set
    End Property
#End Region

End Class

Public Class LeadDataForHeaderInfo
    '
    '   LeadDataForHeaderInfo Class
    '
#Region " Private Variables and Objects"
    Private _leadId As String
    Private _leadStatus As String
    Private _name As String
    Private _dateReceived As String
    Private _dateReceivedCaption As String
    Private _admissionsRep As String
    Private _admissionsRepCaption As String
#End Region
#Region " Public Constructors "
    Public Sub New()

    End Sub
    Public Sub New(ByVal leadId As String)
        _leadId = leadId
        _dateReceivedCaption = "Date Assigned"
        _admissionsRepCaption = "Admissions Rep"
    End Sub
#End Region
#Region " Public Properties"

    Public Property LeadId() As String
        Get
            Return _leadId
        End Get
        Set(ByVal Value As String)
            _leadId = Value
        End Set
    End Property
    Public Property LeadStatus() As String
        Get
            Return _leadStatus
        End Get
        Set(ByVal Value As String)
            _leadStatus = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal Value As String)
            _name = Value
        End Set
    End Property
    Public Property DateReceived() As Date
        Get
            Return _dateReceived
        End Get
        Set(ByVal Value As Date)
            _dateReceived = Value
        End Set
    End Property
    Public Property AdmissionsRep() As String
        Get
            Return _admissionsRep
        End Get
        Set(ByVal Value As String)
            _admissionsRep = Value
        End Set
    End Property
    Public Property DateReceivedCaption() As String
        Get
            Return _dateReceivedCaption
        End Get
        Set(ByVal Value As String)
            _dateReceivedCaption = Value
        End Set
    End Property
    Public Property AdmissionsRepCaption() As String
        Get
            Return _admissionsRepCaption
        End Get
        Set(ByVal Value As String)
            _admissionsRepCaption = Value
        End Set
    End Property
#End Region
End Class

