Public Class LeadStudentSearchInfo
    Private _FirstName As String
    Private _LastName As String
    Private _DOB As String
    Private _SSN As String
    Private _PhoneNumber As String
    Private _GetStudents As Boolean
    Private _FirstNameExact As Int32
    Private _LastNameExact As Int32
    Private _MaximumToMatch As Int32
    Private _ConditionType As Boolean

    Public Property FirstName() As String
        Get
            FirstName = _FirstName
        End Get
        Set(ByVal Value As String)
            _FirstName = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            LastName = _LastName
        End Get
        Set(ByVal Value As String)
            _LastName = Value
        End Set
    End Property
    Public Property DOB() As String
        Get
            DOB = _DOB
        End Get
        Set(ByVal Value As String)
            _DOB = Value
        End Set
    End Property
    Public Property SSN() As String
        Get
            SSN = _SSN
        End Get
        Set(ByVal Value As String)
            _SSN = Value
        End Set
    End Property
    Public Property PhoneNumber() As String
        Get
            PhoneNumber = _PhoneNumber
        End Get
        Set(ByVal Value As String)
            _PhoneNumber = Value
        End Set
    End Property
    Public Property GetStudents() As Boolean
        Get
            GetStudents = _GetStudents
        End Get
        Set(ByVal Value As Boolean)
            _GetStudents = Value
        End Set
    End Property
    Public Property LastNameExact() As Int32
        Get
            LastNameExact = _LastNameExact
        End Get
        Set(ByVal Value As Int32)
            _LastNameExact = Value
        End Set
    End Property
    Public Property FirstNameExact() As Int32
        Get
            FirstNameExact = _FirstNameExact
        End Get
        Set(ByVal Value As Int32)
            _FirstNameExact = Value
        End Set
    End Property
    Public Property MaximumToMatch() As Int32
        Get
            MaximumToMatch = _MaximumToMatch
        End Get
        Set(ByVal Value As Int32)
            _MaximumToMatch = Value
        End Set
    End Property
    Public Property ConditionType() As Boolean
        Get
            ConditionType = _ConditionType
        End Get
        Set(ByVal Value As Boolean)
            _ConditionType = Value
        End Set
    End Property
    Public Sub New()
        MyBase.new()
    End Sub
    Public Sub CalculateMaxCondition()
        Dim currentvalue As Int32 = 0
        If FirstName <> "" Then
            currentvalue += 1
        End If
        If LastName <> "" Then
            currentvalue += 1
        End If
        If DOB <> "" Then
            currentvalue += 1
        End If
        If SSN <> "" Then
            currentvalue += 1
        End If
        If PhoneNumber <> "" Then
            currentvalue += 1
        End If
        MaximumToMatch = currentvalue
    End Sub
    Public Sub New(ByVal NewFirstName As String, ByVal NewLastName As String, ByVal NewDOB As String, ByVal NewSSN As String, ByVal NewPhoneNumber As String, ByVal NewGetStudents As Boolean, _
    ByVal NewFirstNameExact As Int32, ByVal NewLastNameExact As Int32, ByVal NewMaximumToMatch As Int32, ByVal NewConditionType As Boolean)
        _FirstName = NewFirstName
        _LastName = NewLastName
        _DOB = NewDOB
        _SSN = NewSSN
        _PhoneNumber = NewPhoneNumber
        _GetStudents = NewGetStudents
        _FirstNameExact = NewFirstNameExact
        _LastNameExact = NewLastNameExact
        _MaximumToMatch = NewMaximumToMatch
        _ConditionType = NewConditionType
    End Sub
End Class
