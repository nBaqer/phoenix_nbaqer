Public Class CourseBkInfo
#Region "Private Variable Declaration"

    Private _BookId As String
    Private _StartDate As DateTime
    Private _EndDate As DateTime
    Private _IsReq As Boolean
#End Region

#Region "CourseFee Info Properties"


    Public Property BookId() As String
        Get
            Return _BookId
        End Get
        Set(ByVal Value As String)
            _BookId = Value
        End Set
    End Property

    Public Property StartDate() As DateTime
        Get
            Return _StartDate
        End Get
        Set(ByVal Value As DateTime)
            _StartDate = Value
        End Set
    End Property
    Public Property EndDate() As DateTime
        Get
            Return _EndDate
        End Get
        Set(ByVal Value As DateTime)
            _EndDate = Value
        End Set
    End Property
    Public Property IsReq() As Boolean
        Get
            Return _IsReq
        End Get
        Set(ByVal Value As Boolean)
            _IsReq = Value
        End Set
    End Property

#End Region
End Class
