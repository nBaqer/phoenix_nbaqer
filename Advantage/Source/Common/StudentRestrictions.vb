' ===============================================================================
' FAME.AdvantageV1.BusinessEntities
'
' StudentRestrictions.vb
'
' Students Retrictions Common Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================

Public Class StudentRestrictionTypeInfo
    '
    '   StudentRestrictionTypeInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _StudentRestrictionTypeId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _reason As String
    Private _enableAlertButton As Boolean
    Private _blkGradeBook As Boolean
    Private _blkAttendancePosting As Boolean
    Private _blkFA As Boolean
    Private _blkStuPortalClsSched As Boolean
    Private _blkStuPortalGrdBk As Boolean
    Private _blkStuPortalResume As Boolean
    Private _blkStuPortalEntire As Boolean
    Private _blkStuPortalCareer As Boolean
    Private _raiseToRestrictionId As String
    Private _raiseToRestriction As String
    Private _delayDays As Int16
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _StudentRestrictionTypeId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _campGrpId = Guid.Empty.ToString
        _campGrpDescrip = ""
        _reason = ""
        _enableAlertButton = False
        _blkGradeBook = False
        _blkAttendancePosting = False
        _blkFA = False
        _blkStuPortalClsSched = False
        _blkStuPortalGrdBk = False
        _blkStuPortalResume = False
        _blkStuPortalEntire = False
        _blkStuPortalCareer = False
        _raiseToRestrictionId = Guid.Empty.ToString
        _raiseToRestriction = ""
        _delayDays = 0
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property StudentRestrictionTypeId() As String
        Get
            Return _StudentRestrictionTypeId
        End Get
        Set(ByVal Value As String)
            _StudentRestrictionTypeId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property Reason() As String
        Get
            Return _reason
        End Get
        Set(ByVal Value As String)
            _reason = Value
        End Set
    End Property
    Public Property EnableAlertButton() As Boolean
        Get
            Return _enableAlertButton
        End Get
        Set(ByVal Value As Boolean)
            _enableAlertButton = Value
        End Set
    End Property
    Public Property BlkGradeBook() As Boolean
        Get
            Return _blkGradeBook
        End Get
        Set(ByVal Value As Boolean)
            _blkGradeBook = Value
        End Set
    End Property
    Public Property BlkAttendancePosting() As Boolean
        Get
            Return _blkAttendancePosting
        End Get
        Set(ByVal Value As Boolean)
            _blkAttendancePosting = Value
        End Set
    End Property
    Public Property BlkFA() As Boolean
        Get
            Return _blkFA
        End Get
        Set(ByVal Value As Boolean)
            _blkFA = Value
        End Set
    End Property

    Public Property BlkStuPortalClsSched() As Boolean
        Get
            Return _blkStuPortalClsSched
        End Get
        Set(ByVal Value As Boolean)
            _blkStuPortalClsSched = Value
        End Set
    End Property
    Public Property BlkStuPortalGrdBk() As Boolean
        Get
            Return _blkStuPortalGrdBk
        End Get
        Set(ByVal Value As Boolean)
            _blkStuPortalGrdBk = Value
        End Set
    End Property
    Public Property BlkStuPortalResume() As Boolean
        Get
            Return _blkStuPortalResume
        End Get
        Set(ByVal Value As Boolean)
            _blkStuPortalResume = Value
        End Set
    End Property
    Public Property BlkStuPortalEntire() As Boolean
        Get
            Return _blkStuPortalEntire
        End Get
        Set(ByVal Value As Boolean)
            _blkStuPortalEntire = Value
        End Set
    End Property
    Public Property BlkStuPortalCareer() As Boolean
        Get
            Return _blkStuPortalCareer
        End Get
        Set(ByVal Value As Boolean)
            _blkStuPortalCareer = Value
        End Set
    End Property
    Public Property RaiseToRestrictionId() As String
        Get
            Return _raiseToRestrictionId
        End Get
        Set(ByVal Value As String)
            _raiseToRestrictionId = Value
        End Set
    End Property
    Public Property RaiseToRestriction() As String
        Get
            Return _raiseToRestriction
        End Get
        Set(ByVal Value As String)
            _raiseToRestriction = Value
        End Set
    End Property
    Public Property DelayDays() As Int16
        Get
            Return _delayDays
        End Get
        Set(ByVal Value As Int16)
            _delayDays = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Class GrpStuRestrictionInfo
    '
    '   GrpStuRestrictionInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _stuRestrictionId As String
    Private _descrip As String
    Private _sgroupId As String
    Private _sgroup As String
    Private _restrictionTypeId As String
    Private _restrictionType As String
    Private _reason As String
    Private _departmentId As String
    Private _department As String
    Private _userId As String
    Private _user As String
    Private _createDate As Date
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _stuRestrictionId = Guid.NewGuid.ToString
        _descrip = ""
        _sgroupId = Guid.Empty.ToString
        _sgroup = ""
        _restrictionTypeId = Guid.Empty.ToString
        _restrictionType = ""
        _reason = ""
        _departmentId = Guid.Empty.ToString
        _department = ""
        _userId = Guid.Empty.ToString
        _user = ""
        _createDate = Now
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
    Public Sub New(ByVal userId As String, ByVal user As String)
        _isInDB = False
        _stuRestrictionId = Guid.NewGuid.ToString
        _descrip = ""
        _sgroupId = Guid.Empty.ToString
        _sgroup = ""
        _restrictionTypeId = Guid.Empty.ToString
        _restrictionType = ""
        _reason = ""
        _departmentId = Guid.Empty.ToString
        _department = ""
        _userId = userId
        _user = user
        _createDate = Now
        _modUser = ""
        _modDate = Date.Now
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property StuRestrictionId() As String
        Get
            Return _stuRestrictionId
        End Get
        Set(ByVal Value As String)
            _stuRestrictionId = Value
        End Set
    End Property
    Public Property Descrip() As String
        Get
            Return _descrip
        End Get
        Set(ByVal Value As String)
            _descrip = Value
        End Set
    End Property
    Public Property SGroupId() As String
        Get
            Return _sgroupId
        End Get
        Set(ByVal Value As String)
            _sgroupId = Value
        End Set
    End Property
    Public Property SGroup() As String
        Get
            Return _sgroup
        End Get
        Set(ByVal Value As String)
            _sgroup = Value
        End Set
    End Property
    Public Property RestrictionTypeId() As String
        Get
            Return _restrictionTypeId
        End Get
        Set(ByVal Value As String)
            _restrictionTypeId = Value
        End Set
    End Property
    Public Property RestrictionType() As String
        Get
            Return _restrictionType
        End Get
        Set(ByVal Value As String)
            _restrictionType = Value
        End Set
    End Property
    Public Property Reason() As String
        Get
            Return _reason
        End Get
        Set(ByVal Value As String)
            _reason = Value
        End Set
    End Property
    Public Property DepartmentId() As String
        Get
            Return _departmentId
        End Get
        Set(ByVal Value As String)
            _departmentId = Value
        End Set
    End Property
    Public Property Department() As String
        Get
            Return _department
        End Get
        Set(ByVal Value As String)
            _department = Value
        End Set
    End Property
    Public Property UserId() As String
        Get
            Return _userId
        End Get
        Set(ByVal Value As String)
            _userId = Value
        End Set
    End Property
    Public Property User() As String
        Get
            Return _user
        End Get
        Set(ByVal Value As String)
            _user = Value
        End Set
    End Property
    Public Property CreateDate() As Date
        Get
            Return _createDate
        End Get
        Set(ByVal Value As Date)
            _createDate = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
