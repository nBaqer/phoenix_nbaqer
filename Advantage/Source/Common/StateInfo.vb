<Serializable()>
Public Class AdvantageStateInfo
    Private _nameCaption As String
    Private _nameValue As String
    Private _MRUDS As DataSet
    Private _MRUId As String
    Private _campusId As String
    Private _campusDescrip As String
    Private _phone As String
    Private _address1 As String
    Private _address2 As String
    Private _city As String
    Private _state As String
    Private _zip As String
    Private _studentId As String
    Private _studentGrpId As String
    Private _studentIdentifier As String
    Private _studentIdentifierCaption As String
    Private _prgVerId As String
    Private _prgVerDescrip As String
    Private _prgVerTypeDescrip As String
    Private _prgVerExtendedDescrip As String
    Private _enrollmentStatus As String
    Private _startDate As DateTime
    Private _gradDate As DateTime
    Private _enrollmentDetails As List(Of EnrollmentStateInfo)
    Private _leadId As String
    Private _leadStatus As String
    Private _assignedDate As DateTime
    Private _admissionsRep As String
    Private _systemStatus As Integer
    Private _employerId As String
    Private _employeeId As String
    Private _stuEnrollId As String
    Private _titleIVStatus As String

    Public Property CampusId() As String
        Get
            Return _campusId
        End Get
        Set(ByVal value As String)
            _campusId = value
        End Set
    End Property
    Public Property StudentIdentifier() As String
        Get
            Return _studentIdentifier
        End Get
        Set(ByVal value As String)
            _studentIdentifier = value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return _address1
        End Get
        Set(ByVal value As String)
            _address1 = value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return _address2
        End Get
        Set(ByVal value As String)
            _address2 = value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _city
        End Get
        Set(ByVal value As String)
            _city = value
        End Set
    End Property
    Public Property State() As String
        Get
            Return _state
        End Get
        Set(ByVal value As String)
            _state = value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return _zip
        End Get
        Set(ByVal value As String)
            _zip = value
        End Set
    End Property
    Public Property Phone() As String
        Get
            Return _phone
        End Get
        Set(ByVal value As String)
            _phone = value
        End Set
    End Property
    Public Property MRUID() As String
        Get
            Return _MRUId
        End Get
        Set(ByVal value As String)
            _MRUId = value
        End Set
    End Property
    Public Property StudentIdentifierCaption() As String
        Get
            Return _studentIdentifierCaption
        End Get
        Set(ByVal value As String)
            _studentIdentifierCaption = value
        End Set
    End Property
    Public Property PrgVerId() As String
        Get
            Return _prgVerId
        End Get
        Set(ByVal value As String)
            _prgVerId = value
        End Set
    End Property
    Public Property PrgVerDescrip() As String
        Get
            Return _prgVerDescrip
        End Get
        Set(ByVal value As String)
            _prgVerDescrip = value
        End Set
    End Property
    Public Property PrgVerTypeDescrip() As String
        Get
            Return _prgVerTypeDescrip
        End Get
        Set(ByVal value As String)
            _prgVerTypeDescrip = value
        End Set
    End Property
    Public Property PrgVerExtendedDescrip() As String
        Get
            Return _prgVerExtendedDescrip
        End Get
        Set(ByVal value As String)
            _prgVerExtendedDescrip = value
        End Set
    End Property
    Public Property EnrollmentStatus() As String
        Get
            Return _enrollmentStatus
        End Get
        Set(ByVal value As String)
            _enrollmentStatus = value
        End Set
    End Property
    Public Property StartDate() As DateTime
        Get
            Return _startDate
        End Get
        Set(ByVal value As DateTime)
            _startDate = value
        End Set
    End Property
    Public Property GradDate() As DateTime
        Get
            Return _gradDate
        End Get
        Set(ByVal value As DateTime)
            _gradDate = value
        End Set
    End Property
    Public Property EmployerId() As String
        Get
            Return _employerId
        End Get
        Set(ByVal Value As String)
            _employerId = Value
        End Set
    End Property
    Public Property StudentId() As String
        Get
            Return _studentId
        End Get
        Set(ByVal value As String)
            _studentId = value
        End Set
    End Property
    Public Property EmployeeId() As String
        Get
            Return _employeeId
        End Get
        Set(ByVal value As String)
            _employeeId = value
        End Set
    End Property
    Public Property LeadId() As String
        Get
            Return _leadId
        End Get
        Set(ByVal value As String)
            _leadId = value
        End Set
    End Property
    Public Property NameCaption() As String
        Get
            Return _nameCaption
        End Get
        Set(ByVal value As String)
            _nameCaption = value
        End Set
    End Property
    Public Property NameValue() As String
        Get
            Return _nameValue
        End Get
        Set(ByVal value As String)
            _nameValue = value
        End Set
    End Property
    Public Property MRUDS() As DataSet
        Get
            Return _MRUDS
        End Get
        Set(ByVal value As DataSet)
            _MRUDS = value
        End Set
    End Property
    Public Property StudentGrpId() As String
        Get
            Return _studentGrpId
        End Get
        Set(ByVal value As String)
            _studentGrpId = value
        End Set
    End Property
    Public Property EnrollmentDetails() As List(Of EnrollmentStateInfo)
        Get
            Return _enrollmentDetails
        End Get
        Set(ByVal value As List(Of EnrollmentStateInfo))
            _enrollmentDetails = value
        End Set
    End Property
    Public Property AssignedDate() As String
        Get
            Return _assignedDate
        End Get
        Set(ByVal value As String)
            If value Is Nothing Then
                _assignedDate = "1/1/1900"
            Else
                _assignedDate = value
            End If
        End Set
    End Property
    Public Property AdmissionsRep() As String
        Get
            Return _admissionsRep
        End Get
        Set(ByVal value As String)
            _admissionsRep = value
        End Set
    End Property
    Public Property CampusDescrip() As String
        Get
            Return _campusDescrip
        End Get
        Set(ByVal value As String)
            _campusDescrip = value
        End Set
    End Property
    Public Property LeadStatus() As String
        Get
            Return _leadStatus
        End Get
        Set(ByVal value As String)
            _leadStatus = value
        End Set
    End Property
    Public Property SystemStatus() As Integer
        Get
            Return _systemStatus
        End Get
        Set(ByVal value As Integer)
            _systemStatus = value
        End Set
    End Property
    Public Property StuEnrollId() As String
        Get
            Return _stuEnrollId
        End Get
        Set(ByVal value As String)
            _stuEnrollId = value
        End Set
    End Property
    Public Property TitleIVStatustatus() As String
        Get
            Return _titleIVStatus
        End Get
        Set(ByVal value As String)
            _titleIVStatus = value
        End Set
    End Property



End Class
<Serializable()>
Public Class EnrollmentStateInfo
    Private _stuEnrollId As String
    Private _campusId As String
    Private _campusDescrip As String
    Private _prgVerId As String
    Private _prgVerDescrip As String
    Private _prgVerTypeDescrip As String
    Private _prgVerExtendedDescrip As String
    Private _startDate As DateTime
    Private _gradDate As DateTime
    Private _enrollmentStatus As String
    Private _systemStatus As Integer

    Public Property StuEnrollId() As String
        Get
            Return _stuEnrollId
        End Get
        Set(ByVal value As String)
            _stuEnrollId = value
        End Set
    End Property
    Public Property CampusId() As String
        Get
            Return _campusId
        End Get
        Set(ByVal value As String)
            _campusId = value
        End Set
    End Property
    Public Property CampusDescription() As String
        Get
            Return _campusDescrip
        End Get
        Set(ByVal value As String)
            _campusDescrip = value
        End Set
    End Property
    Public Property PrgVerId() As String
        Get
            Return _prgVerId
        End Get
        Set(ByVal value As String)
            _prgVerId = value
        End Set
    End Property
    Public Property PrgVerDescrip() As String
        Get
            Return _prgVerDescrip
        End Get
        Set(ByVal value As String)
            _prgVerDescrip = value
        End Set
    End Property
    Public Property PrgVerTypeDescrip() As String
        Get
            Return _prgVerTypeDescrip
        End Get
        Set(ByVal value As String)
            _prgVerTypeDescrip = value
        End Set
    End Property
    Public Property PrgVerExtendedDescrip() As String
        Get
            Return _prgVerExtendedDescrip
        End Get
        Set(ByVal value As String)
            _prgVerExtendedDescrip = value
        End Set
    End Property
    Public Property StartDate() As DateTime
        Get
            Return _startDate
        End Get
        Set(ByVal value As DateTime)
            _startDate = value
        End Set
    End Property
    Public Property GradDate() As DateTime
        Get
            Return _gradDate
        End Get
        Set(ByVal value As DateTime)
            _gradDate = value
        End Set
    End Property
    Public Property EnrollmentStatus() As String
        Get
            Return _enrollmentStatus
        End Get
        Set(ByVal value As String)
            _enrollmentStatus = value
        End Set
    End Property
    Public Property SystemStatus() As Integer
        Get
            Return _systemStatus
        End Get
        Set(ByVal value As Integer)
            _systemStatus = value
        End Set
    End Property
End Class
