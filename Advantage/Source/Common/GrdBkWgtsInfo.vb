Public Class GrdBkWgtsInfo
#Region "Private Variable Declaration"
    Private _GrdBkWgtId As String
    Private _Descrip As String
    Private _StatusId As String
    Private _InstructorId As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructor"
    Public Sub New()

        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region "GrdBk Wgts Properties"


    Public Property GrdBkWgtId() As String
        Get
            Return _GrdBkWgtId
        End Get
        Set(ByVal Value As String)
            _GrdBkWgtId = Value
        End Set
    End Property
    Public Property InstructorId() As String
        Get
            Return _InstructorId
        End Get
        Set(ByVal Value As String)
            _InstructorId = Value
        End Set
    End Property

    Public Property StatusId() As String
        Get
            Return _StatusId
        End Get
        Set(ByVal Value As String)
            _StatusId = Value
        End Set
    End Property
    Public Property Descrip() As String
        Get
            Return _Descrip
        End Get
        Set(ByVal Value As String)
            _Descrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class

Public Class InstrGrdBkWgtsInfo
#Region "Private Variable Declaration"
    Private _isInDB As Boolean
    Private _instrGrdBkWgtId As String
    Private _instructorId As String
    Private _descrip As String
    Private _statusId As String
    Private _status As String
    Private _reqId As String
    Private _effectiveDate As Date
    Private _gradebookWeightingDetailsDS As DataSet
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructor"
    Public Sub New()
        _statusId = AdvantageCommonValues.ActiveGuid.ToString
        _status = "Active"
        _modUser = ""
        _modDate = Date.MinValue
        _effectiveDate = Date.Now()
    End Sub
#End Region
#Region "GrdBk Wgts Properties"
    Public Property IsInDb() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal value As Boolean)
            _isInDB = value
        End Set
    End Property
    Public Property InstrGrdBkWgtId() As String
        Get
            Return _instrGrdBkWgtId
        End Get
        Set(ByVal Value As String)
            _instrGrdBkWgtId = Value
        End Set
    End Property
    Public Property InstructorId() As String
        Get
            Return _instructorId
        End Get
        Set(ByVal Value As String)
            _instructorId = Value
        End Set
    End Property

    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _descrip
        End Get
        Set(ByVal Value As String)
            _descrip = Value
        End Set
    End Property
    Public Property GradebookWeightingDetailsDS() As DataSet
        Get
            Return _gradebookWeightingDetailsDS
        End Get
        Set(ByVal value As DataSet)
            _gradebookWeightingDetailsDS = value
        End Set
    End Property
    Public Property EffectiveDate() As Date
        Get
            Return _effectiveDate
        End Get
        Set(ByVal Value As Date)
            _effectiveDate = Value
        End Set
    End Property
    Public Property ReqId() As String
        Get
            Return _reqId
        End Get
        Set(ByVal Value As String)
            _reqId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class

