Public Class LeadMasterInfo

#Region " Public Constructor"

    Public Sub New()
        IsInDB = False
        LeadMasterID = Guid.NewGuid.ToString()
        LastName = ""
        FirstName = ""
        MiddleName = ""
        Status = ""
        Prefix = ""
        Suffix = ""
        Title = ""
        BirthDate = ""
        Age = ""
        Sponsor = ""
        AdmissionsRep = ""
        Gender = ""
        Race = ""
        MaritalStatus = ""
        Children = ""
        FamilyIncome = ""

        Phone = ""
        PhoneType = ""
        PhoneStatus = ""

        WorkEmail = ""
        HomeEmail = ""

        Address1 = ""
        Address2 = ""
        City = ""
        State = ""
        Zip = ""
        Country = ""
        County = ""
        AddressStatus = ""
        AddressType = ""
        
        SourceCategory = ""
        SourceDate = ""
        SourceType = ""

        Area = ""
        ExpectedStart = ""
        ProgramID = ""
        ShiftID = ""

        Nationality = ""
        Citizen = ""
        SSN = ""
        DriverLicState = ""
        DriverLicNumber = ""
        AlienNumber = ""

        Notes = ""

        SourceAdvertisement = ""

        AssignmentDate = ""
        AppliedDate = ""
        CampusId = ""

        PrgVerId = ""
        StartDate = ""
        MidPointDate = ""
        ExpGradDate = ""
        TransferDate = ""
        LastDateAttended = ""
        StatusCodeId = ""
        AcademicAdvisor = ""
        GradeLevel = ""
        ChargingMethod = ""
        EnrollmentDate = Date.Now.ToShortDateString
        EnrollmentId = ""

        PreviousEducation = ""
        StudentId = Guid.NewGuid.ToString
        TuitionCategory = ""

        OtherState = ""
        ForeignPhone = 0
        ForeignZip = 0
        ModDate = Date.MinValue
        StatusDescrip = ""
        StudentNumber = ""

        LeadGrpId = ""
        DependencyTypeId = ""
        GeographicTypeId = ""
        AdminCriteriaId = ""

        StateDescrip = ""
        AddressTypeDescrip = ""
        PrefixDescrip = ""
        SuffixDescrip = ""
        AdmissionRepsDescrip = ""
        GenderDescrip = ""
        EthCodeDescrip = ""
        MaritalStatusDescrip = ""
        FamilyIncomeDescrip = ""
        PhoneTypeDescrip = ""
        PhoneTypeDescrip2 = ""
        ShiftDescrip = ""
        NationalityDescrip = ""
        CitizenDescrip = ""
        DriverLicenseStateDescrip = ""
        CountryDescrip = ""
        CountyDescrip = ""
        EdLvlDescrip = ""
        AddressStatusDescrip = ""
        PhoneStatusDescrip = ""
        PhoneStatusDescrip2 = ""
        StatusDescrip = ""
        DependencyTypeDescrip = ""
        GeographicTypeDescrip = ""
        SponsorTypeDescrip = ""

        CampusDescrip = ""
        ChargingMethodDescrip = ""
        AcademicAdvisorDescrip = ""
        PrgVersionDescrip = ""
        PrgVerShiftDescrip = ""
        TuitionCategoryDescrip = ""
        EdLvlId = ""
        InquiryTime = ""
        AdvertisementNote = ""
        HousingTypeId = ""
        HousingTypeDescrip = ""
        AdminCriteriaDescrip = ""
        IsStatusEnrolled = False

        EntranceInterviewDate = "01/01/1900"
        HighSchoolProgramCode = ""
        TransferHours = 0
        DegCertSeekingId = ""
        DegCertSeekingDescrip = ""

        '''' Code added by Kamalesh Ahuja on 08 sept 2010 to resolve mantis issue id 07555
        FAAdvisorId = ""
        Attendtypeid = ""
        AttendtypeDescrip = ""
        FAAdvisorDescrip = ""

        IPEDSValue = ""

        InternationalLead = False
    End Sub

#End Region

#Region "Public Properties"

    ''' <summary>
    ''' Added to support SUVA Major/Minor/concentration types.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ProgramVersionType As Int32

    Public Property EntranceInterviewDate As DateTime?

    Public Property IsDisabled As Integer?

    Public Property IsFirstTimeInSchool As Boolean

    Public Property IsFirstTimePostSecSchool As Boolean

    Public ReadOnly Property ProgramIdProperty() As String
        Get
            Return ProgramID
        End Get
    End Property

    Public Property HighSchoolProgramCode As String

    Public Property DependencyTypeId As String

    Public Property IsStatusEnrolled As Boolean

    Public Property HousingTypeId As String

    Public Property GeographicTypeId As String

    Public Property DegCertSeekingId As String

    Public Property DegCertSeekingDescrip As String

    Public Property AdminCriteriaId As String

    Public Property PrgVerId As String

    Public Property ForeignPhone As Integer

    Public Property ForeignPhone2 As Integer

    Public Property DefaultPhone As Integer

    Public Property StudentNumber As String

    Public Property ForeignZip As Integer

    Public Property OtherState As String

    Public Property LeadGrpId As String

    Public Property StudentId As String

    Public Property ModDate As Date

    Public Property StatusCodeId As String

    Public Property PreviousEducation As String

    Public Property TuitionCategory As String

    Public Property AcademicAdvisor As String

    Public Property GradeLevel As String

    Public Property ChargingMethod As String

    Public Property IsInDB As Boolean

    Public Property LeadMasterID As String

    Public Property SourceAdvertisement As String

    Public Property WorkEmail As String

    Public Property HomeEmail As String

    Public Property LastName As String

    Public Property FirstName As String

    Public Property MiddleName As String

    Public Property Status As String

    Public Property Prefix As String

    Public Property Suffix As String

    Public Property Title As String

    Public Property EnrollmentDate As String

    Public Property StartDate As String

    Public Property MidPointDate As String

    Public Property ExpGradDate As String

    Public Property TransferDate As String

    Public Property LastDateAttended As String

    Public Property BirthDate As String

    Public Property Sponsor As String

    Public Property AdmissionsRep As String

    Public Property Gender As String

    Public Property CampusId As String

    Public Property Race As String

    Public Property MaritalStatus As String

    Public Property FamilyIncome As String

    Public Property Children As String

    Public Property Phone As String

    Public Property Phone2 As String

    Public Property PhoneType As String

    Public Property PhoneType2 As String

    Public Property PhoneStatus As String

    Public Property PhoneStatus2 As String

    Public Property Age As String

    Public Property Address1 As String

    Public Property Address2 As String

    Public Property City As String

    Public Property State As String

    Public Property Zip As String

    Public Property Country As String

    Public Property County As String

    Public Property AddressType As String

    Public Property AddressStatus As String

    Public Property SourceCategory As String

    Public Property SourceDate As String

    Public Property SourceType As String

    Public Property Area As String

    Public Property ExpectedStart As String

    Public Property ProgramID As String

    Public Property ShiftID As String

    Public Property Nationality As String

    Public Property Citizen As String

    Public Property SSN As String

    Public Property DriverLicState As String

    Public Property DriverLicNumber As String

    Public Property AlienNumber As String

    Public Property Notes As String

    Public Property EnrollmentId As String

    Public Property AssignmentDate As String

    Public Property AppliedDate As String

    Public Property StateDescrip As String

    Public Property AddressTypeDescrip As String

    Public Property PrefixDescrip As String

    Public Property SuffixDescrip As String

    Public Property AdmissionRepsDescrip As String

    Public Property GenderDescrip As String

    Public Property EthCodeDescrip As String

    Public Property MaritalStatusDescrip As String

    Public Property FamilyIncomeDescrip As String

    Public Property PhoneTypeDescrip As String

    Public Property PhoneTypeDescrip2 As String

    Public Property ShiftDescrip As String

    Public Property NationalityDescrip As String

    Public Property CitizenDescrip As String

    Public Property DriverLicenseStateDescrip As String

    Public Property CountryDescrip As String

    Public Property CountyDescrip As String

    Public Property EdLvlDescrip As String

    Public Property AddressStatusDescrip As String

    Public Property PhoneStatusDescrip As String

    Public Property PhoneStatusDescrip2 As String

    Public Property StatusDescrip As String

    Public Property DependencyTypeDescrip As String

    Public Property AdminCriteriaDescrip As String

    Public Property HousingTypeDescrip As String

    Public Property GeographicTypeDescrip As String

    Public Property SponsorTypeDescrip As String

    Public Property CampusDescrip As String

    Public Property ChargingMethodDescrip As String

    Public Property AcademicAdvisorDescrip As String

    Public Property PrgVersionDescrip As String

    Public Property PrgVerShiftDescrip As String

    Public Property TuitionCategoryDescrip As String

    Public Property EdLvlId As String

    Public Property InquiryTime As String

    Public Property AdvertisementNote As String

    Public Property TransferHours As Decimal

    Public Property FAAdvisorId As String

    Public Property Attendtypeid As String

    Public Property FAAdvisorDescrip As String

    Public Property AttendtypeDescrip As String

    Public Property IPEDSValue As String

    Public Property BadgeNumber As String

    Public Property InternationalLead As Boolean

#End Region
End Class
