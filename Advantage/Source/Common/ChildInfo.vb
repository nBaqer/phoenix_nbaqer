

Public Class ChildInfo
#Region "Private Variable Declaration"

    Private _ChildId As String
    Private _ChildType As String
    Private _ChildSeq As Integer
    Private _Req As Integer
    Private _Hours As Decimal
    Private _Credits As Decimal
    Private _TrkForCompletion As Integer
    Private _Cnt As Integer
    Private _GradeOverride As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region

#Region "Child Info Properties"


    Public Property ChildId() As String
        Get
            Return _ChildId
        End Get
        Set(ByVal Value As String)
            _ChildId = Value
        End Set
    End Property

    Public Property ChildType() As String
        Get
            Return _ChildType
        End Get
        Set(ByVal Value As String)
            _ChildType = Value
        End Set
    End Property
    Public Property ChildSeq() As Integer
        Get
            Return _ChildSeq
        End Get
        Set(ByVal Value As Integer)
            _ChildSeq = Value
        End Set
    End Property
    Public Property Req() As Integer
        Get
            Return _Req
        End Get
        Set(ByVal Value As Integer)
            _Req = Value
        End Set
    End Property
    Public Property Hours() As Decimal
        Get
            Return _Hours
        End Get
        Set(ByVal Value As Decimal)
            _Hours = Value
        End Set
    End Property
    Public Property Credits() As Decimal
        Get
            Return _Credits
        End Get
        Set(ByVal Value As Decimal)
            _Credits = Value
        End Set
    End Property

    Public Property TrkForCompletion() As Integer
        Get
            Return _TrkForCompletion
        End Get
        Set(ByVal Value As Integer)
            _TrkForCompletion = Value
        End Set
    End Property
    Public Property Cnt() As Integer
        Get
            Return _Cnt
        End Get
        Set(ByVal Value As Integer)
            _Cnt = Value
        End Set
    End Property

    Public Property GradeOverride() As String
        Get
            Return _GradeOverride
        End Get
        Set(ByVal Value As String)
            _GradeOverride = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
#End Region
End Class

