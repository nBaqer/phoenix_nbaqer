Public Class UDFInfo

#Region " Private Variables and Objects"
    Private _SDFId As String
    Private _SDFDescrip As String
    Private _DtypeId As Integer
    Private _Len As Integer
    Private _Decimals As Integer
    Private _StatusId As String
    Private _Comments As String
    Private _IsInDB As Boolean
    Private _ValidationType As Integer
    Private _MinValue As String
    Private _MaxValue As String
    Private _ValueList As String
    Private _ModDate As DateTime
    Private _isRequired As Boolean
    Private _inUse As Boolean
    Private _hasData As Boolean
    Private _StatusCode As String

#End Region
#Region " Public Constructor"
    Public Sub New()
        _SDFId = Guid.NewGuid.ToString
        _SDFDescrip = ""
        _DtypeId = 0
        _Len = 0
        _Decimals = 0
        _StatusId = ""
        _Comments = ""
        _IsInDB = False
        _ValidationType = 0
        _MinValue = ""
        _MaxValue = ""
        _ValueList = ""
        _ModDate = Date.Now
        _isRequired = False
        _inUse = False
        _hasData = False
        _StatusCode = "A"
    End Sub
#End Region
#Region "Public Properties"
    Public Property SDFId() As String
        Get
            Return _SDFId
        End Get
        Set(ByVal Value As String)
            _SDFId = Value
        End Set
    End Property
    Public Property MinValue() As String
        Get
            Return _MinValue
        End Get
        Set(ByVal Value As String)
            _MinValue = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _ModDate
        End Get
        Set(ByVal Value As Date)
            _ModDate = Value
        End Set
    End Property
    Public Property ValueList() As String
        Get
            Return _ValueList
        End Get
        Set(ByVal Value As String)
            _ValueList = Value
        End Set
    End Property
    Public Property MaxValue() As String
        Get
            Return _MaxValue
        End Get
        Set(ByVal Value As String)
            _MaxValue = Value
        End Set
    End Property
    Public Property ValidationType() As Integer
        Get
            Return _ValidationType
        End Get
        Set(ByVal Value As Integer)
            _ValidationType = Value
        End Set
    End Property
    Public Property SDFDescrip() As String
        Get
            Return _SDFDescrip
        End Get
        Set(ByVal Value As String)
            _SDFDescrip = Value
        End Set
    End Property
    Public Property DtypeId() As Integer
        Get
            Return _DtypeId
        End Get
        Set(ByVal Value As Integer)
            _DtypeId = Value
        End Set
    End Property
    Public Property Len() As Integer
        Get
            Return _Len
        End Get
        Set(ByVal Value As Integer)
            _Len = Value
        End Set
    End Property
    Public Property Decimals() As Integer
        Get
            Return _Decimals
        End Get
        Set(ByVal Value As Integer)
            _Decimals = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _StatusId
        End Get
        Set(ByVal Value As String)
            _StatusId = Value
        End Set
    End Property
    Public Property Comments() As String
        Get
            Return _Comments
        End Get
        Set(ByVal Value As String)
            _Comments = Value
        End Set
    End Property
    Public Property IsInDB() As Boolean
        Get
            Return _IsInDB
        End Get
        Set(ByVal Value As Boolean)
            _IsInDB = Value
        End Set
    End Property
    Public Property IsRequired() As Boolean
        Get
            Return _isRequired
        End Get
        Set(ByVal Value As Boolean)
            _isRequired = Value
        End Set
    End Property
    Public Property InUse() As Boolean
        Get
            Return _inUse
        End Get
        Set(ByVal Value As Boolean)
            _inUse = Value
        End Set
    End Property
    Public Property HasData() As Boolean
        Get
            Return _hasData
        End Get
        Set(ByVal Value As Boolean)
            _hasData = Value
        End Set
    End Property
    Public Property StatusCode() As String
        Get
            Return _StatusCode
        End Get
        Set(ByVal Value As String)
            _StatusCode = Value
        End Set
    End Property
#End Region
End Class
