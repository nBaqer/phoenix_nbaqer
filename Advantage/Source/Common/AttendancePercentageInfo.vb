Public Class AttendancePercentageInfo
    Private _totalPresent As Integer
    Private _totalAbsent As Integer
    Private _totalTardies As Integer
    Private _totalExcused As Integer
    Private _totalPresentAdjusted As Decimal
    Private _totalAbsentAdjusted As Decimal
    Private _totalTardiesAdjusted As Decimal
    Private _totalExcusedAdjusted As Decimal
    Private _tardiesMakingAbsence As Integer
    Private _attType As String
    Private _totalScheduled As Integer
    Private _totalMakeUp As Integer
    Public Sub New(ByVal totalPresent As Integer, ByVal totalAbsent As Integer, ByVal totalTardies As Integer, ByVal totalExcused As Integer, ByVal totalPresentAdjusted As Decimal, ByVal totalAbsentAdjusted As Decimal, ByVal totalTardiesAdjusted As Decimal, ByVal totalExcusedAdjusted As Decimal, ByVal tardiesMakingAbsence As Integer, ByVal attType As String, totalScheduled As Integer, totalMakeUp As Integer)
        _totalPresent = totalPresent
        _totalAbsent = totalAbsent
        _totalTardies = totalTardies
        _totalExcused = totalExcused
        _totalPresentAdjusted = totalPresentAdjusted
        _totalAbsentAdjusted = totalAbsentAdjusted
        _totalTardiesAdjusted = totalTardiesAdjusted
        _totalExcusedAdjusted = totalExcusedAdjusted
        _tardiesMakingAbsence = tardiesMakingAbsence
        _attType = attType
        _totalScheduled = totalScheduled
        _totalMakeUp = totalMakeUp
    End Sub
    Public Property TotalPresent() As Integer
        Get
            Return _totalPresent
        End Get
        Set(value As Integer)
            _totalPresent = value
        End Set
    End Property

    Public Property TotalAbsent() As Integer
        Get
            Return _totalAbsent
        End Get
        Set(value As Integer)
            _totalAbsent = value
        End Set
    End Property
    Public ReadOnly Property TotalTardies() As Integer
        Get
            Return _totalTardies
        End Get
    End Property
    Public ReadOnly Property TotalExcused() As Integer
        Get
            Return _totalExcused
        End Get
    End Property
    Public ReadOnly Property TotalPresentAdjusted() As Decimal
        Get
            Return _totalPresentAdjusted
        End Get
    End Property
    Public ReadOnly Property TotalAbsentAdjusted() As Decimal
        Get
            Return _totalAbsentAdjusted
        End Get
    End Property
    Public ReadOnly Property TotalTardiesAdjusted() As Decimal
        Get
            Return _totalTardiesAdjusted
        End Get
    End Property
    Public ReadOnly Property TotalExcusedAdjusted() As Decimal
        Get
            Return _totalExcusedAdjusted
        End Get
    End Property
    Public ReadOnly Property TotalMakeUp() As Integer
        Get
            Return _totalMakeUp
        End Get
    End Property
    Public Property TotalScheduled() As Integer
        Get
            Return _totalScheduled
        End Get
        Set(value As Integer)
            _totalScheduled = value
        End Set
    End Property

    Public ReadOnly Property PercentageAttendance() As Decimal
        Get
            If (_attType = "Minutes" Or _attType = "Clock Hours") Then
                If Me.TotalScheduled > 0 Then
                    'Return Me.TotalPresentAdjusted / (Me.TotalPresentAdjusted() + Me.TotalAbsentAdjusted() + Me.TotalTardiesAdjusted() - Me._totalMakeUp) * 100.0
                    Return Me.TotalPresentAdjusted / (Me.TotalScheduled) * 100.0
                Else
                    Return 0.0
                End If
            Else

                'attendance type
                'DE8883
                If (Me.TotalPresentAdjusted() + Me.TotalTardiesAdjusted() + Me.TotalAbsentAdjusted()) <> 0 Then
                    Return (Me.TotalPresentAdjusted + Me.TotalTardiesAdjusted) / (Me.TotalPresentAdjusted() + Me.TotalAbsentAdjusted() + Me.TotalTardiesAdjusted()) * 100.0
                Else
                    Return 0.0
                End If

                'If Me.TotalPresentAdjusted() + Me.TotalAbsentAdjusted() > 0 Then
                '    Return (Me.TotalPresentAdjusted + Me.TotalTardiesAdjusted) / (Me.TotalPresentAdjusted() + Me.TotalAbsentAdjusted() + Me.TotalTardiesAdjusted()) * 100.0
                'Else
                '    Return 0.0
                'End If

            End If

        End Get
    End Property
    Public ReadOnly Property TardiesMakingAbsence() As Integer
        Get
            Return _tardiesMakingAbsence
        End Get
    End Property
    Public ReadOnly Property AttType() As String
        Get
            Return _attType
        End Get
    End Property
End Class


