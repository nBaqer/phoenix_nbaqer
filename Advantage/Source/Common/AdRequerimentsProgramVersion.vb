﻿Public Class AdRequerimentsProgramVersion

    Property Description() As String
    Property TypeReq As Int32
    Property MinScore() As Decimal
    Property ActualScore() As Decimal
    Property Override() As Boolean
    Property DocReceived() As Int32
    Property DocStatusCode() As String

    'Calculate
    Public ReadOnly Property TestPassed() As Boolean
        Get
            'If override is active pass True
            If Override Then Return True
            Dim nopassed As Boolean = (ActualScore < MinScore)
            Return Not nopassed
        End Get
    End Property


    Public ReadOnly Property DocPassed() As Boolean
        Get
            'If override is active pass True
            If Override Then Return True
            ' Document can be received and rejected, this cover the case
            Dim status As String = "NONE"
            If IsNothing(DocStatusCode) = False Then
                status = DocStatusCode.ToUpper()
            End If
            If (status = "RECEIVED" OrElse status = "NAPRRV" OrElse status = "PENDING") Then
                Return False
            End If
            'Normal case document received.
            Return (DocReceived > 0)

        End Get
    End Property

End Class
