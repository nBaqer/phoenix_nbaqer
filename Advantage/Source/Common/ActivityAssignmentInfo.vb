Option Strict On

#Region "Activity Assignment Info Packages"

Public Class ActivityAssignmentInfo

#Region "Private Variable Declaration"

    Private _ActivityAssignmentId As Guid
    Private _EmployeeId As Guid
    Private _StudentId As Guid
    Private _ActivityId As Guid
    Private _Subject As String
    Private _DueDate As Date
    Private _StartTime As Guid
    Private _EndTime As Guid
    Private _ActivityStatusId As Int32
    Private _PriorityId As Guid
    Private _Comments As String
    Private _FirstLastName As String
    Private _DateCompleted As Date
    Private _ActivityResultId As Guid
    Private _ChildId As Guid
    Private _ParentId As Guid
    Private _DateCreated As Date
    Private _LeadId As Guid
    Private _ModUser As String
    Private _ModDate As Date

#End Region

#Region "Activity Assignment Properties"

    Public Property ActivityAssignmentId() As Guid
        Get
            ActivityAssignmentId = _ActivityAssignmentId
        End Get
        Set(ByVal Value As Guid)
            _ActivityAssignmentId = Value
        End Set
    End Property
    Public Property EmployeeId() As Guid
        Get
            EmployeeId = _EmployeeId
        End Get
        Set(ByVal Value As Guid)
            _EmployeeId = Value
        End Set
    End Property
    Public Property StudentId() As Guid
        Get
            StudentId = _StudentId
        End Get
        Set(ByVal Value As Guid)
            _StudentId = Value
        End Set
    End Property
    Public Property ActivityId() As Guid
        Get
            ActivityId = _ActivityId
        End Get
        Set(ByVal Value As Guid)
            _ActivityId = Value
        End Set
    End Property
    Public Property Subject() As String
        Get
            Subject = _Subject
        End Get
        Set(ByVal Value As String)
            _Subject = Value
        End Set
    End Property
    Public Property DueDate() As Date
        Get
            DueDate = _DueDate
        End Get
        Set(ByVal Value As Date)
            _DueDate = Value
        End Set
    End Property
    Public Property StartTime() As Guid
        Get
            StartTime = _StartTime
        End Get
        Set(ByVal Value As Guid)
            _StartTime = Value
        End Set
    End Property
    Public Property EndTime() As Guid
        Get
            EndTime = _EndTime
        End Get
        Set(ByVal Value As Guid)
            _EndTime = Value
        End Set
    End Property
    Public Property ActivityStatusId() As Int32
        Get
            ActivityStatusId = _ActivityStatusId
        End Get
        Set(ByVal Value As Int32)
            _ActivityStatusId = Value
        End Set
    End Property
    Public Property PriorityId() As Guid
        Get
            PriorityId = _PriorityId
        End Get
        Set(ByVal Value As Guid)
            _PriorityId = Value
        End Set
    End Property
    Public Property Comments() As String
        Get
            Comments = _Comments
        End Get
        Set(ByVal Value As String)
            _Comments = Value
        End Set
    End Property
    Public Property DateCompleted() As Date
        Get
            DateCompleted = _DateCompleted
        End Get
        Set(ByVal Value As Date)
            _DateCompleted = Value
        End Set
    End Property
    Public Property ActivityResultId() As Guid
        Get
            ActivityResultId = _ActivityResultId
        End Get
        Set(ByVal Value As Guid)
            _ActivityResultId = Value
        End Set
    End Property
    Public Property ChildId() As Guid
        Get
            ChildId = _ChildId
        End Get
        Set(ByVal Value As Guid)
            _ChildId = Value
        End Set
    End Property
    Public Property ParentId() As Guid
        Get
            ParentId = _ParentId
        End Get
        Set(ByVal Value As Guid)
            _ParentId = Value
        End Set
    End Property
    Public Property DateCreated() As Date
        Get
            DateCreated = _DateCreated
        End Get
        Set(ByVal Value As Date)
            _DateCreated = Value
        End Set
    End Property
    Public Property LeadId() As Guid
        Get
            LeadId = _LeadId
        End Get
        Set(ByVal Value As Guid)
            _LeadId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            ModUser = _ModUser
        End Get
        Set(ByVal Value As String)
            _ModUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            ModDate = _ModDate
        End Get
        Set(ByVal Value As Date)
            _ModDate = Value
        End Set
    End Property
#End Region

#Region "New Instance of the Class Declaration"
    Public Sub New(ByVal NewActivityAssignmentId As Guid, ByVal NewEmployeeId As Guid, _
         ByVal NewStudentId As Guid, ByVal NewActivityId As Guid, _
         ByVal NewSubject As String, ByVal NewDueDate As Date, _
         ByVal NewStartTime As Guid, ByVal NewEndTime As Guid, _
         ByVal NewActivityStatusId As Int32, ByVal NewPriorityId As Guid, ByVal NewComments As String, ByVal NewDateCompleted As Date, ByVal NewActivityResultId As Guid, ByVal NewChildId As Guid, ByVal NewParentId As Guid, ByVal NewLeadId As Guid, _
         ByVal NewModUser As String, ByVal NewModDate As Date)

        _ActivityAssignmentId = NewActivityAssignmentId
        _EmployeeId = NewEmployeeId
        _StudentId = NewStudentId
        _ActivityId = NewActivityId
        _Subject = NewSubject
        _DueDate = NewDueDate
        _StartTime = NewStartTime
        _EndTime = NewEndTime
        _ActivityStatusId = NewActivityStatusId
        _PriorityId = NewPriorityId
        _Comments = NewComments
        _DateCompleted = NewDateCompleted
        _ActivityResultId = NewActivityResultId
        _ChildId = NewChildId
        _ParentId = NewParentId
        _LeadId = NewLeadId
        _ModDate = NewModDate
        _ModUser = NewModUser
    End Sub

    Public Sub New()
        'MyBase.new()
    End Sub

#End Region

End Class

#End Region