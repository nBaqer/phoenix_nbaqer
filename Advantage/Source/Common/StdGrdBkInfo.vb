''' <summary>
''' Store the info to upgrade Grading 
''' </summary>
''' <remarks></remarks>
Public Class StdGrdBkInfo

#Region "GrdPostingsInfo Properties"

    Public Property GrdBkResultId As String

    Public Property ClassId As String

    Public Property StuEnrollId As String

    Public Property CampusId() As String

    Public Property GrdBkWgtDetailId As String

    Public Property Score As Double

    Public Property Comments As String

    Public Property ModUser As String

    Public Property ModDate As Date

    Public Property ResNum As Integer

    Public Property IsIncomplete As Integer

    Public Property IsCompGraded As Boolean

    Public Property DateCompleted As Date

    Public Property Adjustment As Double
    Public Property PostDate As Date

#End Region

End Class
