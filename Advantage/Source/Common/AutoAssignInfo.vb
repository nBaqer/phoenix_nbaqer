Public Class AutoAssignInfo
#Region "Private Variable Declaration"

    Private _AutoAssignRuleId As Guid
    Private _AutoAssignRuleDescrip As String
    Private _BasisId As Guid
    Private _TaskId As Int32
    Private _StatusId As Guid
    Private _CampusGroupId As Guid
    Private _BasisWhenId As Guid
    Private _BasisChangeId As Guid
    Private _ActivityId As Guid
    Private _Subject As String
    Private _Note As String
    Private _DueDateDays As Int32
    Private _DueDateCriteriaId As Guid
    Private _AssignToId As Guid
    Private _EmpId As Guid
    Private _WhenQuantity As Int32
    Private _DateCriteriaId As Guid

#End Region

#Region "Activity Assignment Properties"

    Public Property AutoAssignRuleId() As Guid
        Get
            AutoAssignRuleId = _AutoAssignRuleId
        End Get
        Set(ByVal Value As Guid)
            _AutoAssignRuleId = Value
        End Set
    End Property
    Public Property AutoAssignRuleDescrip() As String
        Get
            AutoAssignRuleDescrip = _AutoAssignRuleDescrip
        End Get
        Set(ByVal Value As String)
            _AutoAssignRuleDescrip = Value
        End Set
    End Property
    Public Property BasisId() As Guid
        Get
            BasisId = _BasisId
        End Get
        Set(ByVal Value As Guid)
            _BasisId = Value
        End Set
    End Property
    Public Property TaskId() As Int32
        Get
            TaskId = _TaskId
        End Get
        Set(ByVal Value As Int32)
            _TaskId = Value
        End Set
    End Property
    Public Property StatusId() As Guid
        Get
            StatusId = _StatusId
        End Get
        Set(ByVal Value As Guid)
            _StatusId = Value
        End Set
    End Property
    Public Property CampusGroupId() As Guid
        Get
            CampusGroupId = _CampusGroupId
        End Get
        Set(ByVal Value As Guid)
            _CampusGroupId = Value
        End Set
    End Property
    Public Property BasisWhenId() As Guid
        Get
            BasisWhenId = _BasisWhenId
        End Get
        Set(ByVal Value As Guid)
            _BasisWhenId = Value
        End Set
    End Property
    Public Property BasisChangeId() As Guid
        Get
            BasisChangeId = _BasisChangeId
        End Get
        Set(ByVal Value As Guid)
            _BasisChangeId = Value
        End Set
    End Property
    Public Property ActivityId() As Guid
        Get
            ActivityId = _ActivityId
        End Get
        Set(ByVal Value As Guid)
            _ActivityId = Value
        End Set
    End Property
    Public Property Subject() As String
        Get
            Subject = _Subject
        End Get
        Set(ByVal Value As String)
            _Subject = Value
        End Set
    End Property
    Public Property Note() As String
        Get
            Note = _Note
        End Get
        Set(ByVal Value As String)
            _Note = Value
        End Set
    End Property
    Public Property DueDateDays() As Int32
        Get
            DueDateDays = _DueDateDays
        End Get
        Set(ByVal Value As Int32)
            _DueDateDays = Value
        End Set
    End Property
    Public Property DueDateCriteriaId() As Guid
        Get
            DueDateCriteriaId = _DueDateCriteriaId
        End Get
        Set(ByVal Value As Guid)
            _DueDateCriteriaId = Value
        End Set
    End Property
    Public Property AssignToId() As Guid
        Get
            AssignToId = _AssignToId
        End Get
        Set(ByVal Value As Guid)
            _AssignToId = Value
        End Set
    End Property
    Public Property EmpId() As Guid
        Get
            EmpId = _EmpId
        End Get
        Set(ByVal Value As Guid)
            _EmpId = Value
        End Set
    End Property
    Public Property WhenQuantity() As Int32
        Get
            WhenQuantity = _WhenQuantity
        End Get
        Set(ByVal Value As Int32)
            _WhenQuantity = Value
        End Set
    End Property
    Public Property DateCriteriaId() As Guid
        Get
            DateCriteriaId = _DateCriteriaId
        End Get
        Set(ByVal Value As Guid)
            _DateCriteriaId = Value
        End Set
    End Property
#End Region

#Region "New Instance of the Class Declaration"
    Public Sub New(ByVal NewAutoAssignRuleId As Guid, _
    ByVal NewAutoAssignRuleDescrip As String, _
    ByVal NewBasisId As Guid, _
    ByVal NewTaskId As Int32, _
    ByVal NewStatusId As Guid, _
    ByVal NewCampusGroupId As Guid, _
    ByVal NewBasisWhenId As Guid, _
    ByVal NewBasisChangeId As Guid, _
    ByVal NewActivityId As Guid, _
    ByVal NewSubject As String, _
    ByVal NewNote As String, _
    ByVal NewDueDateDays As Int32, _
    ByVal NewDueDateCriteriaId As Guid, _
    ByVal NewAssignToId As Guid, _
    ByVal NewEmpId As Guid, _
    ByVal NewWhenQuantity As Int32, _
    ByVal NewDateCriteriaId As Guid)


        _AutoAssignRuleId = NewAutoAssignRuleId
        _AutoAssignRuleDescrip = NewAutoAssignRuleDescrip
        _BasisId = NewBasisId
        _TaskId = NewTaskId
        _StatusId = NewStatusId
        _CampusGroupId = NewCampusGroupId
        _BasisWhenId = NewBasisWhenId
        _BasisChangeId = NewBasisChangeId
        _ActivityId = NewActivityId
        _Subject = NewSubject
        _Note = NewNote
        _DueDateDays = NewDueDateDays
        _DueDateCriteriaId = NewDueDateCriteriaId
        _AssignToId = NewAssignToId
        _EmpId = NewEmpId
        _WhenQuantity = NewWhenQuantity
        _DateCriteriaId = NewDateCriteriaId

    End Sub

    Public Sub New()
        'MyBase.new()
    End Sub

#End Region
End Class
