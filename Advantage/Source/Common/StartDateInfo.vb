Public Class StartDateInfo
    '
    '   StartDateInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _StartDateId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _shiftId As String
    Private _progId As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _startDate As String
    Private _MidPtDate As String
    Private _EndDate As String
    Private _AllowedMaxGradDate As String
    Private _ExpStarts As Integer
    Private _MaxExpStarts As Integer
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _StartDateId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property StartDateId() As String
        Get
            Return _StartDateId
        End Get
        Set(ByVal Value As String)
            _StartDateId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property StartDate() As String
        Get
            Return _startDate
        End Get
        Set(ByVal Value As String)
            _startDate = Value
        End Set
    End Property
    Public Property EndDate() As String
        Get
            Return _EndDate
        End Get
        Set(ByVal Value As String)
            _EndDate = Value
        End Set
    End Property
    Public Property MidPtDate() As String
        Get
            Return _MidPtDate
        End Get
        Set(ByVal Value As String)
            _MidPtDate = Value
        End Set
    End Property
    Public Property AllowedMaxGradDate() As String
        Get
            Return _AllowedMaxGradDate
        End Get
        Set(ByVal Value As String)
            _AllowedMaxGradDate = Value
        End Set
    End Property
    Public Property ExpStarts() As Integer
        Get
            Return _ExpStarts
        End Get
        Set(ByVal Value As Integer)
            _ExpStarts = Value
        End Set
    End Property

    Public Property MaxExpStarts() As Integer
        Get
            Return _MaxExpStarts
        End Get
        Set(ByVal Value As Integer)
            _MaxExpStarts = Value
        End Set
    End Property
    Public Property ShiftId() As String
        Get
            Return _shiftId
        End Get
        Set(ByVal Value As String)
            _shiftId = Value
        End Set
    End Property

    Public Property ProgId() As String
        Get
            Return _progId
        End Get
        Set(ByVal Value As String)
            _progId = Value
        End Set
    End Property
#End Region
End Class
