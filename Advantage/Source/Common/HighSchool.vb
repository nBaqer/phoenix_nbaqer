Public Class HighSchool
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _Code As String
    Private _StatusId As String
    Private _Name As String
    Private _Address1 As String
    Private _Address2 As String
    Private _City As String
    Private _StateId As String
    Private _OtherState As String
    Private _CountryId As String
    Private _Phone As String
    Private _cmpGrpId As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _hsId As String
    Private _zip As String
    Private _status As String
    Private _foreignPhone As Integer
    Private _foreignzip As Integer
    Private _source As Integer
    Private _InstitutionType As Integer 
    Private _CountyId As String 
    Private _ContactTitle As String
    Private _ContactFirstName As String
    Private _ContactMI As String
    Private _ContactLastName As String
    Private _ContactPhone As String 
    Private _ContactEmail As String 
    Private _ContactStatusId As String
    Private _PrefixId As String 
    Private _SuffixId As String 
    Private _AddressTypeId As String
    Private _ContactPhoneExt As String
    Private _InstitutionAddressId As String
    Private _InstitutionPhoneId As String
    Private _InstitutionContactId As String
    Private _IsDefaultAdd As Integer
    Private _IsDefaultPhone As Integer
    Private _IsDefaultContact As Integer
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _Code = ""
        _StatusId = ""
        _Name = ""
        _Address1 = ""
        _Address2 = ""
        _City = ""
        _StateId = ""
        _OtherState = ""
        _CountryId = ""
        _Phone = ""
        _cmpGrpId = ""
        _modUser = ""
        _modDate = Date.MinValue
        _hsId = ""
        _zip = ""
        _status = ""
        _foreignPhone = 0
        _foreignzip = 0
        _source = 0
        _InstitutionType = 0
        _CountyId  = ""
        _ContactTitle = ""
        _ContactFirstName = ""
        _ContactMI = ""
        _ContactLastName = ""
        _ContactPhone  = ""
        _ContactEmail  = ""
        _ContactStatusId = ""
        _PrefixId = ""
        _SuffixId = ""
        _AddressTypeId = ""
        _ContactPhoneExt = ""
        _InstitutionAddressId = ""
        _InstitutionPhoneId = ""
        _InstitutionContactId = ""
        _IsDefaultAdd = 0
        _IsDefaultContact = 0
        _IsDefaultPhone = 0
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property PrefixId() As String
        Get
            Return _PrefixId
        End Get
        Set(ByVal Value As String)
            _PrefixId = Value
        End Set
    End Property
     Public Property AddressTypeId() As String
        Get
            Return _AddressTypeId
        End Get
        Set(ByVal Value As String)
            _AddressTypeId = Value
        End Set
    End Property
     Public Property SuffixId() As String
        Get
            Return _SuffixId
        End Get
        Set(ByVal Value As String)
            _SuffixId = Value
        End Set
    End Property
    Public Property CountyId() As String
        Get
            Return _CountyId
        End Get
        Set(ByVal Value As String)
            _CountyId = Value
        End Set
    End Property
     Public Property ContactTitle() As String
        Get
            Return _ContactTitle
        End Get
        Set(ByVal Value As String)
            _ContactTitle = Value
        End Set
    End Property
     Public Property ContactFirstName() As String
        Get
            Return _ContactFirstName
        End Get
        Set(ByVal Value As String)
            _ContactFirstName = Value
        End Set
    End Property
     Public Property ContactLastName() As String
        Get
            Return _ContactLastName
        End Get
        Set(ByVal Value As String)
            _ContactLastName = Value
        End Set
    End Property
     Public Property ContactMiddleName() As String
        Get
            Return _ContactMI
        End Get
        Set(ByVal Value As String)
            _ContactMI = Value
        End Set
    End Property
    Public Property ContactPhone() As String
        Get
            Return _ContactPhone
        End Get
        Set(ByVal Value As String)
            _ContactPhone = Value
        End Set
    End Property
    Public Property ContactEmail() As String
        Get
            Return _ContactEmail
        End Get
        Set(ByVal Value As String)
            _ContactEmail = Value
        End Set
    End Property
     Public Property ContactStatus() As String
        Get
            Return _ContactStatusId
        End Get
        Set(ByVal Value As String)
            _ContactStatusId = Value
        End Set
    End Property
    Public Property ForeignPhone() As Integer
        Get
            Return _foreignPhone
        End Get
        Set(ByVal Value As Integer)
            _foreignPhone = Value
        End Set
    End Property
      Public Property InstitutionType() As Integer
        Get
            Return _InstitutionType
        End Get
        Set(ByVal Value As Integer)
            _InstitutionType = Value
        End Set
    End Property
    Public Property ForeignZip() As Integer
        Get
            Return _foreignzip
        End Get
        Set(ByVal Value As Integer)
            _foreignzip = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _Code
        End Get
        Set(ByVal Value As String)
            _Code = Value
        End Set
    End Property
    Public Property OtherState() As String
        Get
            Return _OtherState
        End Get
        Set(ByVal Value As String)
            _OtherState = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return _zip
        End Get
        Set(ByVal Value As String)
            _zip = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _StatusId
        End Get
        Set(ByVal Value As String)
            _StatusId = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal Value As String)
            _Name = Value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return _Address1
        End Get
        Set(ByVal Value As String)
            _Address1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return _Address2
        End Get
        Set(ByVal Value As String)
            _Address2 = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _City
        End Get
        Set(ByVal Value As String)
            _City = Value
        End Set
    End Property
    Public Property StateId() As String
        Get
            Return _StateId
        End Get
        Set(ByVal Value As String)
            _StateId = Value
        End Set
    End Property
    Public Property CountryId() As String
        Get
            Return _CountryId
        End Get
        Set(ByVal Value As String)
            _CountryId = Value
        End Set
    End Property
    Public Property Phone() As String
        Get
            Return _Phone
        End Get
        Set(ByVal Value As String)
            _Phone = Value
        End Set
    End Property
    Public Property CampusGroup() As String
        Get
            Return _cmpGrpId
        End Get
        Set(ByVal Value As String)
            _cmpGrpId = Value
        End Set
    End Property
    Public Property HsId() As String
        Get
            Return _hsId
        End Get
        Set(ByVal Value As String)
            _hsId = Value
        End Set
    End Property
    Public Property modDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property Source() As Integer
        Get
            Return _source
        End Get
        Set(ByVal Value As Integer)
            _source = Value
        End Set
    End Property
    Public Property ContactPhoneExt() As String
        Get
            Return _ContactPhoneExt
        End Get
        Set(ByVal Value As String)
            _ContactPhoneExt = Value
        End Set
    End Property
    Public Property InstitutionAddressId() As String
        Get
            Return _InstitutionAddressId
        End Get
        Set(ByVal Value As String)
            _InstitutionAddressId = Value
        End Set
    End Property
    Public Property InstitutionPhoneId() As String
        Get
            Return _InstitutionPhoneId
        End Get
        Set(ByVal Value As String)
            _InstitutionPhoneId = Value
        End Set
    End Property
    Public Property InstitutionContactId() As String
        Get
            Return _InstitutionContactId
        End Get
        Set(ByVal Value As String)
            _InstitutionContactId = Value
        End Set
    End Property
    Public Property IsDefaultAdd() As Integer
        Get
            Return _IsDefaultAdd
        End Get
        Set(ByVal Value As Integer)
            _IsDefaultAdd = Value
        End Set
    End Property
    Public Property IsDefaultPhone() As Integer
        Get
            Return _IsDefaultPhone
        End Get
        Set(ByVal Value As Integer)
            _IsDefaultPhone = Value
        End Set
    End Property
    Public Property IsDefaultContact() As Integer
        Get
            Return _IsDefaultContact
        End Get
        Set(ByVal Value As Integer)
            _IsDefaultContact = Value
        End Set
    End Property
#End Region
End Class
