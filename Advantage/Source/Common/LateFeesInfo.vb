Public Class LateFeesInfo
    '
    '   LateFeesInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _lateFeesId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _transCodeId As String
    Private _effectiveDate As DateTime
    Private _flatAmount As Decimal
    Private _rate As Decimal
    Private _gracePeriod As Integer
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _LateFeesId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _effectiveDate = Date.Today
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property LateFeesId() As String
        Get
            Return _LateFeesId
        End Get
        Set(ByVal Value As String)
            _LateFeesId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property TransCodeId() As String
        Get
            Return _transCodeId
        End Get
        Set(ByVal value As String)
            _transCodeId = value
        End Set
    End Property
    Public Property EffectiveDate() As Date
        Get
            Return _effectiveDate
        End Get
        Set(ByVal value As Date)
            _effectiveDate = value
        End Set
    End Property
    Public Property FlatAmount() As Decimal
        Get
            Return _flatAmount
        End Get
        Set(ByVal value As Decimal)
            _flatAmount = value
        End Set
    End Property
    Public Property Rate() As Decimal
        Get
            Return _rate
        End Get
        Set(ByVal value As Decimal)
            _rate = value
        End Set
    End Property
    Public Property GracePeriod() As Integer
        Get
            Return _gracePeriod
        End Get
        Set(ByVal value As Integer)
            _gracePeriod = value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class