Public Class PrgMinorCertInfo
#Region "Private Variable Declaration"

    Private _ParentId As String
    Private _ChildId As String

#End Region

#Region "Prg Minor Cert Info Properties"


    Public Property ParentId() As String
        Get
            Return _ParentId
        End Get
        Set(ByVal Value As String)
            _ParentId = Value
        End Set
    End Property
    Public Property ChildId() As String
        Get
            Return _ChildId
        End Get
        Set(ByVal Value As String)
            _ChildId = Value
        End Set
    End Property

#End Region
End Class
