Option Strict On

#Region "Message Info Packages"

Public Class MessageInfo

#Region "Private Variable Declaration"

    Private _MessageId As Guid
    Private _ToId As Guid
    Private _FromId As Guid
    Private _SentDate As DateTime
    Private _Subject As String
    Private _Message As String
    Private _Deleted As Boolean
    Private _ParentMessageId As Guid
    Private _ChildMessageId As Guid
    Private _ToFullName As String
    Private _FromFullName As String
    Private _OriginalMessage As String
    Private _DeletedByTo As Boolean
    Private _DeletedByFrom As Boolean
    Private _MessageRead As Boolean


#End Region

#Region "Activity Assignment Properties"

    Public Property MessageId() As Guid
        Get
            MessageId = _MessageId
        End Get
        Set(ByVal Value As Guid)
            _MessageId = Value
        End Set
    End Property
    Public Property ToId() As Guid
        Get
            ToId = _ToId
        End Get
        Set(ByVal Value As Guid)
            _ToId = Value
        End Set
    End Property
    Public Property FromId() As Guid
        Get
            FromId = _FromId
        End Get
        Set(ByVal Value As Guid)
            _FromId = Value
        End Set
    End Property
    Public Property SentDate() As DateTime
        Get
            SentDate = _SentDate
        End Get
        Set(ByVal Value As DateTime)
            _SentDate = Value
        End Set
    End Property
    Public Property Subject() As String
        Get
            Subject = _Subject
        End Get
        Set(ByVal Value As String)
            _Subject = Value
        End Set
    End Property
    Public Property Message() As String
        Get
            Message = _Message
        End Get
        Set(ByVal Value As String)
            _Message = Value
        End Set
    End Property
    Public Property Deleted() As Boolean
        Get
            Deleted = _Deleted
        End Get
        Set(ByVal Value As Boolean)
            _Deleted = Value
        End Set
    End Property
    Public Property ParentMessageId() As Guid
        Get
            ParentMessageId = _ParentMessageId
        End Get
        Set(ByVal Value As Guid)
            _ParentMessageId = Value
        End Set
    End Property
    Public Property ChildMessageId() As Guid
        Get
            ChildMessageId = _ChildMessageId
        End Get
        Set(ByVal Value As Guid)
            _ChildMessageId = Value
        End Set
    End Property
    Public Property ToFullName() As String
        Get
            ToFullName = _ToFullName
        End Get
        Set(ByVal Value As String)
            _ToFullName = Value
        End Set
    End Property
    Public Property FromFullName() As String
        Get
            FromFullName = _FromFullName
        End Get
        Set(ByVal Value As String)
            _FromFullName = Value
        End Set
    End Property
    Public Property OriginalMessage() As String
        Get
            OriginalMessage = _OriginalMessage
        End Get
        Set(ByVal Value As String)
            _OriginalMessage = Value
        End Set
    End Property
    Public Property DeletedByTo() As Boolean
        Get
            DeletedByTo = _DeletedByTo
        End Get
        Set(ByVal Value As Boolean)
            _DeletedByTo = Value
        End Set
    End Property
    Public Property DeletedByFrom() As Boolean
        Get
            DeletedByFrom = _DeletedByFrom
        End Get
        Set(ByVal Value As Boolean)
            _DeletedByFrom = Value
        End Set
    End Property
    Public Property MessageRead() As Boolean
        Get
            MessageRead = _MessageRead
        End Get
        Set(ByVal Value As Boolean)
            _MessageRead = Value
        End Set
    End Property
#End Region

#Region "New Instance of the Class Declaration"
    Public Sub New(ByVal NewMessageId As Guid, ByVal NewToId As Guid, _
         ByVal NewFromId As Guid, ByVal NewSentDate As Date, _
         ByVal NewSubject As String, ByVal NewMessage As String, _
         ByVal NewDeleted As Boolean, ByVal NewParentMessageId As Guid, ByVal NewChildMessageId As Guid, ByVal NewToFullName As String, ByVal NewFromFullName As String, ByVal NewOriginalMessage As String, ByVal NewDeletedByTo As Boolean, ByVal NewDeletedByFrom As Boolean, ByVal NewMessageRead As Boolean)

        _MessageId = NewMessageId
        _ToId = NewToId
        _FromId = NewFromId
        _SentDate = NewSentDate
        _Subject = NewSubject
        _Message = NewMessage
        _Deleted = NewDeleted
        _ParentMessageId = NewParentMessageId
        _ChildMessageId = NewChildMessageId
        _ToFullName = NewToFullName
        _FromFullName = NewFromFullName
        _OriginalMessage = NewOriginalMessage
        _DeletedByTo = NewDeletedByTo
        _DeletedByFrom = NewDeletedByFrom
        _MessageRead = NewMessageRead


    End Sub

    Public Sub New()
        MyBase.new()
        _MessageId = Guid.NewGuid
    End Sub

#End Region

End Class

#End Region
