Public Class AtPostingOptionsInfo
    Private _CampusId As Guid
    Private _PostingMethod As String
    Private _SortBy As String
    Private _PostingPeriod As String
    Private _AttendanceType As String
    Private _AllUnpostedStudents As Boolean
    Private _InactiveStudents As Boolean
    Private _ActiveStudentStatus As Guid
    Private _SelectedGuid As Guid
    Private _NumberPostedDates As Int32
    Public Property CampusId() As Guid
        Get
            CampusId = _CampusId
        End Get
        Set(ByVal Value As Guid)
            _CampusId = Value
        End Set
    End Property
    Public Property PostingMethod() As String
        Get
            PostingMethod = _PostingMethod
        End Get
        Set(ByVal Value As String)
            _PostingMethod = Value
        End Set
    End Property
    Public Property SortBy() As String
        Get
            SortBy = _SortBy
        End Get
        Set(ByVal Value As String)
            _SortBy = Value
        End Set
    End Property
    Public Property PostingPeriod() As String
        Get
            PostingPeriod = _PostingPeriod
        End Get
        Set(ByVal Value As String)
            _PostingPeriod = Value
        End Set
    End Property
    Public Property AttendanceType() As String
        Get
            AttendanceType = _AttendanceType
        End Get
        Set(ByVal Value As String)
            _AttendanceType = Value
        End Set
    End Property
    Public Property AllUnpostedStudents() As Boolean
        Get
            AllUnpostedStudents = _AllUnpostedStudents
        End Get
        Set(ByVal Value As Boolean)
            _AllUnpostedStudents = Value
        End Set
    End Property
    Public Property InactiveStudents() As Boolean
        Get
            InactiveStudents = _InactiveStudents
        End Get
        Set(ByVal Value As Boolean)
            _InactiveStudents = Value
        End Set
    End Property
    Public Property ActiveStudentStatus() As Guid
        Get
            ActiveStudentStatus = _ActiveStudentStatus
        End Get
        Set(ByVal Value As Guid)
            _ActiveStudentStatus = Value
        End Set
    End Property
    Public Property SelectedGuid() As Guid
        Get
            SelectedGuid = _SelectedGuid
        End Get
        Set(ByVal Value As Guid)
            _SelectedGuid = Value
        End Set
    End Property
    Public Property NumberPostedDates() As Int32
        Get
            NumberPostedDates = _NumberPostedDates
        End Get
        Set(ByVal Value As Int32)
            _NumberPostedDates = Value
        End Set
    End Property
End Class
