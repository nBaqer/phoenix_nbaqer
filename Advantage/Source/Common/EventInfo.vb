
Public Class EventInfo
    '
    '   EventInfo Class
    '
#Region " Private Variables and Objects"
    Private _IsInDB As Boolean
    Private _EventID As String
    Private _statusId As String
    Private _status As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _ModuleID As String
    Private _EventDate As DateTime
    Private _ShowDate As DateTime
    Private _Title As String
    Private _WhereWhen As String
    Private _Description As String
    Private _ModUser As String
    Private _ModDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _IsInDB = False
        _statusId = Guid.Empty.ToString
        _status = ""
        _campGrpId = Guid.Empty.ToString
        _campGrpDescrip = ""
        _EventID = Guid.NewGuid.ToString
        _EventDate = Date.Today
        _ShowDate = Date.Today
        _ModDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _IsInDB
        End Get
        Set(ByVal Value As Boolean)
            _IsInDB = Value
        End Set
    End Property
    Public Property EventID() As String
        Get
            Return _EventID
        End Get
        Set(ByVal Value As String)
            _EventID = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModuleID() As String
        Get
            Return _ModuleID
        End Get
        Set(ByVal Value As String)
            _ModuleID = Value
        End Set
    End Property
    Public Property EventDate() As Date
        Get
            Return _EventDate
        End Get
        Set(ByVal Value As Date)
            _EventDate = Value
        End Set
    End Property
    Public Property ShowDate() As Date
        Get
            Return _ShowDate
        End Get
        Set(ByVal Value As Date)
            _ShowDate = Value
        End Set
    End Property
    Public Property Title() As String
        Get
            Return _Title
        End Get
        Set(ByVal Value As String)
            _Title = Value
        End Set
    End Property
    Public Property WhereWhen() As String
        Get
            Return _WhereWhen
        End Get
        Set(ByVal Value As String)
            _WhereWhen = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _ModUser
        End Get
        Set(ByVal Value As String)
            _ModUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _ModDate
        End Get
        Set(ByVal Value As Date)
            _ModDate = Value
        End Set
    End Property
#End Region

End Class

