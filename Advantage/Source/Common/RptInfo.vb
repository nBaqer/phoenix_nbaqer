' ===============================================================================
' ReportInfo.vb
' Info classes for the Reports
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Partially Developed by ThinkTron Corporation
' ===============================================================================
' History
' 9/2006
'   ThinkTron - added support for AdHoc Reports
'   ThinkTron - moved ReportParamInfo class to this file
' 10/20/06
'   ThinkTron - added support for UDF

Namespace Reports
#Region "Params"
    ''' <summary>
    ''' List of opertors that can be used in params
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum OpType
        None = 0
        Equal = 1
        NotEqual = 2
        GreaterThan = 3
        LessThan = 4
        IsIn = 5
        Between = 6
        IsLike = 7
        EqualBlankString = 8
        IsNull = 9
        DateExact = 10
        DateMin = 11
        DateMax = 12
    End Enum

    ''' <summary>
    ''' Holds information on one filter
    ''' </summary>
    ''' <remarks></remarks>
    Public Class FilterInfo
#Region " Private Variables and Objects"
        Private _filterId As String ' syRptUserPrefs.PrefId
        Private _prefId As String ' syRptUserPrefs.PrefId
        Private _rptParamId As String
        Private _adHocFieldId As String
        Private _opId As OpType
        Private _opValue As String
#End Region
#Region " Public Constructors "
        Public Sub New()
            _filterId = Guid.NewGuid.ToString()
            _opId = OpType.None
        End Sub
#End Region
#Region " Public Properties"
        Public Property FilterId() As String
            Get
                Return _filterId
            End Get
            Set(ByVal Value As String)
                _filterId = Value
            End Set
        End Property
        Public Property PrefId() As String
            Get
                Return _prefId
            End Get
            Set(ByVal Value As String)
                _prefId = Value
            End Set
        End Property
        Public Property RptParamId() As String
            Get
                Return _rptParamId
            End Get
            Set(ByVal Value As String)
                _rptParamId = Value
            End Set
        End Property
        Public Property AdHocFieldId() As String
            Get
                Return _adHocFieldId
            End Get
            Set(ByVal Value As String)
                _adHocFieldId = Value
            End Set
        End Property
        Public Property OpId() As OpType
            Get
                Return _opId
            End Get
            Set(ByVal Value As OpType)
                _opId = Value
            End Set
        End Property
        Public Property Value() As String
            Get
                Return _opValue
            End Get
            Set(ByVal Value As String)
                _opValue = Value
            End Set
        End Property
#End Region
    End Class

    ''' <summary>
    ''' Holds info on syRptUserPrefs.  It also includes
    ''' info on the filters, filter(others) and sorts
    ''' </summary>
    ''' <remarks></remarks>
    Public Class UserPrefsInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _isAdHoc As Boolean
        Private _prefId As String ' syRptUserPrefs.PrefId
        Private _userId As String
        Private _userName As String
        Private _reportId As String ' syRptUserPrefs.ResourceId
        Private _reportName As String
        Private _prefName As String
        Private _filters() As FilterInfo
#End Region
#Region " Public Constructors "
        Public Sub New()
            _prefId = Guid.NewGuid.ToString()
            _isAdHoc = False
            _filters = Nothing
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property IsAdHoc() As Boolean
            Get
                Return _isAdHoc
            End Get
            Set(ByVal Value As Boolean)
                _isAdHoc = Value
            End Set
        End Property
        Public Property PrefId() As String
            Get
                Return _prefId
            End Get
            Set(ByVal Value As String)
                _prefId = Value
            End Set
        End Property
        Public Property UserId() As String
            Get
                Return _userId
            End Get
            Set(ByVal Value As String)
                _userId = Value
            End Set
        End Property
        Public Property UserName() As String
            Get
                Return _userName
            End Get
            Set(ByVal Value As String)
                _userName = Value
            End Set
        End Property
        Public Property ReportId() As String
            Get
                Return _reportId
            End Get
            Set(ByVal Value As String)
                _reportId = Value
            End Set
        End Property
        Public Property ReportName() As String
            Get
                Return _reportName
            End Get
            Set(ByVal Value As String)
                _reportName = Value
            End Set
        End Property
        Public Property PrefName() As String
            Get
                Return _prefName
            End Get
            Set(ByVal Value As String)
                _prefName = Value
            End Set
        End Property
        Public Property Filters() As FilterInfo()
            Get
                Return _filters
            End Get
            Set(ByVal Value() As FilterInfo)
                _filters = Value
            End Set
        End Property
#End Region
    End Class
#End Region

#Region "Pre-defined Reports"
    <Serializable()>
    Public Class ReportInfo
#Region "Private Variables"
        Private m_ResourceID As Integer
        Private m_SqlId As Integer
        Private m_ObjId As Integer
        Private m_Url As String
        Private m_Resource As String
#End Region

#Region "Public Properties"
        Public Property ResourceID() As Integer
            Get
                Return m_ResourceID
            End Get
            Set(ByVal Value As Integer)
                m_ResourceID = Value
            End Set
        End Property

        Public Property SqlId() As Integer
            Get
                Return m_SqlId
            End Get
            Set(ByVal Value As Integer)
                m_SqlId = Value
            End Set
        End Property

        Public Property ObjId() As Integer
            Get
                Return m_ObjId
            End Get
            Set(ByVal Value As Integer)
                m_ObjId = Value
            End Set
        End Property

        Public Property Url() As String
            Get
                Return m_Url
            End Get
            Set(ByVal Value As String)
                m_Url = Value
            End Set
        End Property

        Public Property Resource() As String
            Get
                Return m_Resource
            End Get
            Set(ByVal Value As String)
                m_Resource = Value
            End Set
        End Property
#End Region
    End Class


    <Serializable()>
    Public Class FilterListPrefsInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _filterListPrefid As String ' syRptUserPrefs.PrefId        
#End Region
#Region " Public Constructors "
        Public Sub New()
            _filterListPrefid = Guid.NewGuid.ToString()
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property FilterListPrefId() As String
            Get
                Return _filterListPrefid
            End Get
            Set(ByVal Value As String)
                _filterListPrefid = Value
            End Set
        End Property
#End Region
    End Class

    <Serializable()>
    Public Class AdvantageLogoImage
#Region "Private Variables"
        Private _schoolId As Integer
        Private _imgLen As Integer
        Private _image As Byte()
        Private _contentType As String
#End Region

#Region "Public Constructors"
        Public Sub New()

        End Sub
#End Region

#Region "Public Properties"
        Public Property SchoolId() As Integer
            Get
                Return _schoolId
            End Get
            Set(ByVal Value As Integer)
                _schoolId = Value
            End Set
        End Property
        Public Property ImageLength() As Integer
            Get
                Return _imgLen
            End Get
            Set(ByVal Value As Integer)
                _imgLen = Value
            End Set
        End Property
        Public Property Image() As Byte()
            Get
                Return _image
            End Get
            Set(ByVal Value As Byte())
                _image = Value
            End Set
        End Property
        Public Property ContentType() As String
            Get
                Return _contentType
            End Get
            Set(ByVal Value As String)
                _contentType = Value
            End Set
        End Property
#End Region
    End Class

    <Serializable()>
    Public Class ReportParamInfo
#Region "Private Variables"
        Private m_SqlId As Integer
        Private m_ObjId As Integer
        Private m_OrderBy As String
        Private m_OrderByString As String
        Private m_FilterList As String
        Private m_FilterListString As String
        Private m_FilterOther As String
        Private m_FilterOtherString As String
        Private m_ShowFilters As Boolean
        Private m_ShowSortBy As Boolean
        Private m_ShowRptDescription As Boolean
        Private m_ShowRptInstructions As Boolean
        Private m_ShowRptNotes As Boolean
        Private m_RptTitle As String
        Private m_LogoFilePath As String
        Private m_SchoolLogo As Byte()

        Private m_IPEDS_CohortYear As Integer
        Private m_IPEDS_RptDateMonth As Integer
        Private m_IPEDS_RptDateDay As Integer
        Private m_IPEDS_FilterCampusID As String
        Private m_IPEDS_FilterProgramIDs As String
        Private m_IPEDS_FilterProgramDescrips As String
        Private m_IPEDS_SortBy As String
        Private m_IPEDS_StudentIDCaption As String
#End Region

#Region "Public Properties"
        Public Property SqlId() As Integer
            Get
                Return m_SqlId
            End Get
            Set(ByVal Value As Integer)
                m_SqlId = Value
            End Set
        End Property

        Public Property ObjId() As Integer
            Get
                Return m_ObjId
            End Get
            Set(ByVal Value As Integer)
                m_ObjId = Value
            End Set
        End Property

        Public Property OrderBy() As String
            Get
                Return m_OrderBy
            End Get
            Set(ByVal Value As String)
                m_OrderBy = Value
            End Set
        End Property

        Public Property FilterList() As String
            Get
                Return m_FilterList
            End Get
            Set(ByVal Value As String)
                m_FilterList = Value
            End Set
        End Property

        Public Property FilterListString() As String
            Get
                Return m_FilterListString
            End Get
            Set(ByVal Value As String)
                m_FilterListString = Value
            End Set
        End Property

        Public Property FilterOther() As String
            Get
                Return m_FilterOther
            End Get
            Set(ByVal Value As String)
                m_FilterOther = Value
            End Set
        End Property

        Public Property FilterOtherString() As String
            Get
                Return m_FilterOtherString
            End Get
            Set(ByVal Value As String)
                m_FilterOtherString = Value
            End Set
        End Property

        Public Property ShowFilters() As Boolean
            Get
                Return m_ShowFilters
            End Get
            Set(ByVal Value As Boolean)
                m_ShowFilters = Value
            End Set
        End Property

        Public Property OrderByString() As String
            Get
                Return m_OrderByString
            End Get
            Set(ByVal Value As String)
                m_OrderByString = Value
            End Set
        End Property

        Public Property ShowSortBy() As Boolean
            Get
                Return m_ShowSortBy
            End Get
            Set(ByVal Value As Boolean)
                m_ShowSortBy = Value
            End Set
        End Property

        Public Property ShowRptDescription() As Boolean
            Get
                Return m_ShowRptDescription
            End Get
            Set(ByVal Value As Boolean)
                m_ShowRptDescription = Value
            End Set
        End Property

        Public Property ShowRptInstructions() As Boolean
            Get
                Return m_ShowRptInstructions
            End Get
            Set(ByVal Value As Boolean)
                m_ShowRptInstructions = Value
            End Set
        End Property

        Public Property ShowRptNotes() As Boolean
            Get
                Return m_ShowRptNotes
            End Get
            Set(ByVal Value As Boolean)
                m_ShowRptNotes = Value
            End Set
        End Property

        Public Property RptTitle() As String
            Get
                Return m_RptTitle
            End Get
            Set(ByVal Value As String)
                m_RptTitle = Value
            End Set
        End Property

        Public Property SchoolLogo() As Byte()
            Get
                Return m_SchoolLogo
            End Get
            Set(ByVal Value As Byte())
                m_SchoolLogo = Value
            End Set
        End Property

        Public Property IPEDS_CohortYear() As Integer
            Get
                Return m_IPEDS_CohortYear
            End Get
            Set(ByVal Value As Integer)
                m_IPEDS_CohortYear = Value
            End Set
        End Property

        Public Property IPEDS_RptDateMonth() As Integer
            Get
                Return m_IPEDS_RptDateMonth
            End Get
            Set(ByVal Value As Integer)
                m_IPEDS_RptDateMonth = Value
            End Set
        End Property

        Public Property IPEDS_RptDateDay() As Integer
            Get
                Return m_IPEDS_RptDateDay
            End Get
            Set(ByVal Value As Integer)
                m_IPEDS_RptDateDay = Value
            End Set
        End Property

        Public Property IPEDS_FilterCampusID() As String
            Get
                Return m_IPEDS_FilterCampusID
            End Get
            Set(ByVal Value As String)
                m_IPEDS_FilterCampusID = Value
            End Set
        End Property

        Public Property IPEDS_FilterProgramIDs() As String
            Get
                Return m_IPEDS_FilterProgramIDs
            End Get
            Set(ByVal Value As String)
                m_IPEDS_FilterProgramIDs = Value
            End Set
        End Property

        Public Property IPEDS_FilterProgramDescrips() As String
            Get
                Return m_IPEDS_FilterProgramDescrips
            End Get
            Set(ByVal Value As String)
                m_IPEDS_FilterProgramDescrips = Value
            End Set
        End Property

        Public Property IPEDS_SortBy() As String
            Get
                Return m_IPEDS_SortBy
            End Get
            Set(ByVal Value As String)
                m_IPEDS_SortBy = Value
            End Set
        End Property

        Public Property IPEDS_StudentIDCaption() As String
            Get
                Return m_IPEDS_StudentIDCaption
            End Get
            Set(ByVal Value As String)
                m_IPEDS_StudentIDCaption = Value
            End Set
        End Property

#End Region
    End Class
#End Region

#Region "AdHoc Reports"
    ''' <summary>
    ''' Holds all information to describe an Adhoc report
    ''' </summary>
    ''' <remarks></remarks>
    <Serializable()>
    Public Class AdHocRptInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _reportId As String ' syUserResources.ResourceId
        Private _reportName As String ' syUserResources.Resource
        Private _reportType As Integer
        Private _reportTypeDescrip As String
        Private _permissions() As AdHocPermissionInfo
        Private _fields() As AdHocFieldInfo
        Private _filter As String
        Private _campGrpId As String
        Private _campGrpDescrip As String
        Private _options As Integer
        Private _isPublic As Boolean
        Private _modUser As String
        Private _modDate As DateTime
        Private _active As Boolean
        Private _useLeftJoin As Boolean
#End Region
#Region " Public Constructors "
        Public Sub New()
            _isInDB = False
            _active = True
            _options = 0
            _isPublic = True
            _filter = ""
            _useLeftJoin = False
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property ReportId() As String
            Get
                Return _reportId
            End Get
            Set(ByVal Value As String)
                _reportId = Value
            End Set
        End Property
        Public Property ReportName() As String
            Get
                Return _reportName
            End Get
            Set(ByVal Value As String)
                _reportName = Value
            End Set
        End Property
        Public Property ReportType() As Integer
            Get
                Return _reportType
            End Get
            Set(ByVal Value As Integer)
                _reportType = Value
            End Set
        End Property
        Public Property ReportTypeDescrip() As String
            Get
                Return _reportTypeDescrip
            End Get
            Set(ByVal Value As String)
                _reportTypeDescrip = Value
            End Set
        End Property
        Public Property Permissions() As AdHocPermissionInfo()
            Get
                Return _permissions
            End Get
            Set(ByVal Value As AdHocPermissionInfo())
                _permissions = Value
            End Set
        End Property
        Public Property Fields() As AdHocFieldInfo()
            Get
                Return _fields
            End Get
            Set(ByVal Value As AdHocFieldInfo())
                _fields = Value
            End Set
        End Property
        Public Property Filter() As String
            Get
                Return _filter
            End Get
            Set(ByVal Value As String)
                _filter = Value
            End Set
        End Property
        Public Property CampGrpId() As String
            Get
                Return _campGrpId
            End Get
            Set(ByVal Value As String)
                _campGrpId = Value
            End Set
        End Property
        Public Property CampGrpDescrip() As String
            Get
                Return _campGrpDescrip
            End Get
            Set(ByVal Value As String)
                _campGrpDescrip = Value
            End Set
        End Property
        Public Property Options() As Integer
            Get
                Return _options
            End Get
            Set(ByVal value As Integer)
                _options = value
            End Set
        End Property
        Public Property ShowSummary() As Boolean
            Get
                Return (_options And ReportOptions.ShowSummary) <> 0
            End Get
            Set(ByVal Value As Boolean)
                If Value Then
                    _options = _options Or ReportOptions.ShowSummary
                Else
                    _options = _options And (Not ReportOptions.ShowSummary)
                End If
            End Set
        End Property
        Public Property ShowGroupBySummary() As Boolean
            Get
                Return (_options And ReportOptions.ShowGroupBySummary) <> 0
            End Get
            Set(ByVal Value As Boolean)
                If Value Then
                    _options = _options Or ReportOptions.ShowGroupBySummary
                Else
                    _options = _options And (Not ReportOptions.ShowGroupBySummary)
                End If
            End Set
        End Property
        Public Property IsPublic() As Boolean
            Get
                Return _isPublic
            End Get
            Set(ByVal Value As Boolean)
                _isPublic = Value
            End Set
        End Property
        Public Property UseLeftJoin() As Boolean
            Get
                Return _useLeftJoin
            End Get
            Set(ByVal Value As Boolean)
                _useLeftJoin = Value
            End Set
        End Property
        Public Property ModUser() As String
            Get
                Return _modUser
            End Get
            Set(ByVal Value As String)
                _modUser = Value
            End Set
        End Property
        Public Property ModDate() As DateTime
            Get
                Return _modDate
            End Get
            Set(ByVal Value As Date)
                _modDate = Value
            End Set
        End Property
        Public Property Active() As Boolean
            Get
                Return _active
            End Get
            Set(ByVal Value As Boolean)
                _active = Value
            End Set
        End Property

        Public Property CreatedBy() As String


#End Region
    End Class

    ''' <summary>
    ''' Summary types used in AdHocFieldInfo to 
    ''' describe the bitmap "_summaryType"
    ''' </summary>
    ''' <remarks></remarks>
    <Serializable()>
    Public Class SummaryTypes
        Public Const Count As Integer = 1
        Public Const Sum As Integer = 2
        Public Const Max As Integer = 4
        Public Const Min As Integer = 8
        Public Const Avg As Integer = 16
        Public Const Merge As Integer = 32
    End Class

    ''' <summary>
    ''' Options defs used to define 
    ''' </summary>
    ''' <remarks></remarks>
    <Serializable()>
    Public Class ReportOptions
        Public Const ShowSummary As Integer = 1
        Public Const ShowGroupBySummary As Integer = 2
    End Class

    ''' <summary>
    ''' Describes a field type in AdHocFieldInfo
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum FieldType
        _Unknown = 0
        _Smallint = 2
        _Int = 3
        _Float = 5
        _Money = 6
        _Bit = 11
        _TinyInt = 17
        _Uniqueidentifier = 72
        _Char = 129
        _Decimal = 131
        _DataTime = 135
        _Varchar = 200
    End Enum

    ''' <summary>
    ''' Describes a field within an Adhoc report
    ''' </summary>
    ''' <remarks></remarks>
    <Serializable()>
    Public Class AdHocFieldInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _adHocFieldId As String
        Private _reportId As String ' syResources.ResourceId
        Private _tblFldsId As String
        Private _tblId As String
        Private _tableName As String
        Private _tableId As String
        Private _fldId As String
        Private _fieldName As String
        Private _colOrder As Integer
        Private _header As String
        Private _visible As Boolean
        Private _formatString As String
        Private _width As Integer
        Private _showInGroupBy As Boolean
        Private _showInSummary As Boolean
        Private _summaryType As Integer
        Private _groupBy As Boolean
        Private _sortBy As Boolean
        Private _isParam As Boolean
        Private _isRequired As Boolean
        Private _fldTypeId As FieldType
        Private _fldType As String
        Private _fkColDescrip As String
        Private _calculationSql As String
        Private _sdfId As String

#End Region
#Region " Public Constructors "
        Public Sub New()
            _adHocFieldId = Guid.NewGuid.ToString()
            _colOrder = -1
            _visible = True
            _showInGroupBy = False
            _showInSummary = False
            _summaryType = 0
            _groupBy = False
            _sortBy = False
            _isParam = False
            _isRequired = False
            _fldTypeId = Reports.FieldType._Unknown
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property AdHocFieldId() As String
            Get
                Return _adHocFieldId
            End Get
            Set(ByVal Value As String)
                _adHocFieldId = Value
            End Set
        End Property
        Public Property ReportId() As String
            Get
                Return _reportId
            End Get
            Set(ByVal Value As String)
                _reportId = Value
            End Set
        End Property
        Public Property TblFldsId() As String
            Get
                Return _tblFldsId
            End Get
            Set(ByVal Value As String)
                _tblFldsId = Value
            End Set
        End Property
        Public Property TblId() As String
            Get
                Return _tblId
            End Get
            Set(ByVal Value As String)
                _tblId = Value
            End Set
        End Property
        Public Property TableId() As String
            Get
                Return _tableId
            End Get
            Set(ByVal Value As String)
                _tableId = Value
            End Set
        End Property
        Public Property TableName() As String
            Get
                Return _tableName
            End Get
            Set(ByVal Value As String)
                _tableName = Value
            End Set
        End Property
        Public Property FldId() As String
            Get
                Return _fldId
            End Get
            Set(ByVal Value As String)
                _fldId = Value
            End Set
        End Property
        Public Property FieldName() As String
            Get
                Return _fieldName
            End Get
            Set(ByVal Value As String)
                _fieldName = Value
            End Set
        End Property
        Public Property ColOrder() As Integer
            Get
                Return _colOrder
            End Get
            Set(ByVal Value As Integer)
                _colOrder = Value
            End Set
        End Property
        Public Property Header() As String
            Get
                Return _header
            End Get
            Set(ByVal Value As String)
                _header = Value
            End Set
        End Property
        Public Property Visible() As Boolean
            Get
                Return _visible
            End Get
            Set(ByVal Value As Boolean)
                _visible = Value
            End Set
        End Property
        Public Property FormatString() As String
            Get
                Return _formatString
            End Get
            Set(ByVal Value As String)
                _formatString = Value
            End Set
        End Property
        Public Property Width() As Integer
            Get
                Return _width
            End Get
            Set(ByVal Value As Integer)
                _width = Value
            End Set
        End Property
        Public Property ShowInGroupBy() As Boolean
            Get
                Return _showInGroupBy
            End Get
            Set(ByVal Value As Boolean)
                _showInGroupBy = Value
            End Set
        End Property
        Public Property ShowInSummary() As Boolean
            Get
                Return _showInSummary
            End Get
            Set(ByVal Value As Boolean)
                _showInSummary = Value
            End Set
        End Property
        Public Property SummaryType() As Integer
            Get
                Return _summaryType
            End Get
            Set(ByVal Value As Integer)
                _summaryType = Value
            End Set
        End Property
        Public Property GroupBy() As Boolean
            Get
                Return _groupBy
            End Get
            Set(ByVal Value As Boolean)
                _groupBy = Value
            End Set
        End Property
        Public Property SortBy() As Boolean
            Get
                Return _sortBy
            End Get
            Set(ByVal Value As Boolean)
                _sortBy = Value
            End Set
        End Property
        Public Property IsParam() As Boolean
            Get
                Return _isParam
            End Get
            Set(ByVal Value As Boolean)
                _isParam = Value
            End Set
        End Property
        Public Property IsRequired() As Boolean
            Get
                Return _isRequired
            End Get
            Set(ByVal Value As Boolean)
                _isRequired = Value
            End Set
        End Property
        Public Property FldTypeId() As FieldType
            Get
                Return _fldTypeId
            End Get
            Set(ByVal Value As FieldType)
                _fldTypeId = Value
            End Set
        End Property
        Public Property FieldType() As String
            Get
                Return _fldType
            End Get
            Set(ByVal Value As String)
                _fldType = Value
            End Set
        End Property
        Public Property FKColDescrip() As String
            Get
                Return _fkColDescrip
            End Get
            Set(ByVal Value As String)
                _fkColDescrip = Value
            End Set
        End Property
        Public Property SDFId() As String
            Get
                Return _sdfId
            End Get
            Set(ByVal Value As String)
                _sdfId = Value
            End Set
        End Property
        Public Property CalculationSql() As String
            Get
                Return _calculationSql
            End Get
            Set(ByVal Value As String)
                _calculationSql = Value
            End Set
        End Property
#End Region
    End Class


    ''' <summary>
    ''' Holds a permission mapping for a report to a module
    ''' </summary>
    ''' <remarks></remarks>
    <Serializable()>
    Public Class AdHocPermissionInfo
#Region " Private Variables and Objects"
        Private _isInDB As Boolean
        Private _resPermissionId As String
        Private _reportId As String ' syResources.ResourceId
        Private _moduleId As String ' syResources.ResourceId
        Private _moduleName As String
        Private _permission As Boolean
#End Region
#Region " Public Constructors "
        Public Sub New()
            _resPermissionId = Guid.NewGuid.ToString()
        End Sub
#End Region
#Region " Public Properties"
        Public Property IsInDB() As Boolean
            Get
                Return _isInDB
            End Get
            Set(ByVal Value As Boolean)
                _isInDB = Value
            End Set
        End Property
        Public Property ResPermissionId() As String
            Get
                Return _resPermissionId
            End Get
            Set(ByVal Value As String)
                _resPermissionId = Value
            End Set
        End Property
        Public Property ReportId() As String
            Get
                Return _reportId
            End Get
            Set(ByVal Value As String)
                _reportId = Value
            End Set
        End Property
        Public Property ModuleId() As String
            Get
                Return _moduleId
            End Get
            Set(ByVal Value As String)
                _moduleId = Value
            End Set
        End Property
        Public Property ModuleName() As String
            Get
                Return _moduleName
            End Get
            Set(ByVal Value As String)
                _moduleName = Value
            End Set
        End Property
        Public Property Permission() As Boolean
            Get
                Return _permission
            End Get
            Set(ByVal Value As Boolean)
                _permission = Value
            End Set
        End Property
#End Region
    End Class
#End Region
End Namespace
