' ===============================================================================
' FAME.AdvantageV1.BusinessEntities
'
' StudentNoteInfo.vb
'
' Student Notes Common Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
Public Class StudentNoteInfo
    '
    '   StudentNoteInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _studentNoteId As String
    Private _statusId As String
    Private _status As String
    Private _studentId As String
    Private _studentName As String
    Private _studentNoteDescrip As String
    Private _moduleCode As String
    Private _userId As String
    Private _userName As String
    Private _createdDate As Date
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _studentNoteId = Guid.NewGuid.ToString
        _statusId = FAME.AdvantageV1.Common.AdvantageCommonValues.ActiveGuid
        _status = "Active"
        _studentId = Guid.Empty.ToString
        _studentName = ""
        _studentNoteDescrip = ""
        _moduleCode = "UK"
        _userId = Guid.Empty.ToString
        _userName = ""
        _createdDate = Now
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
    Public Sub New(ByVal studentId As String, ByVal userId As String)
        _isInDB = False
        _studentNoteId = Guid.NewGuid.ToString
        _statusId = FAME.AdvantageV1.Common.AdvantageCommonValues.ActiveGuid
        _status = "Active"
        _studentId = studentId
        _studentName = ""
        _studentNoteDescrip = ""
        _moduleCode = "UK"
        _userId = userId
        _userName = ""
        _createdDate = Now
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property StudentNoteId() As String
        Get
            Return _studentNoteId
        End Get
        Set(ByVal Value As String)
            _studentNoteId = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property StudentId() As String
        Get
            Return _studentId
        End Get
        Set(ByVal Value As String)
            _studentId = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _studentNoteDescrip
        End Get
        Set(ByVal Value As String)
            _studentNoteDescrip = Value
        End Set
    End Property
    Public Property UserId() As String
        Get
            Return _userId
        End Get
        Set(ByVal Value As String)
            _userId = Value
        End Set
    End Property
    Public Property ModuleCode() As String
        Get
            Return _moduleCode
        End Get
        Set(ByVal Value As String)
            _moduleCode = Value
        End Set
    End Property
    Public Property UserName() As String
        Get
            Return _userName
        End Get
        Set(ByVal Value As String)
            _userName = Value
        End Set
    End Property
    Public Property CreatedDate() As Date
        Get
            Return _createdDate
        End Get
        Set(ByVal Value As Date)
            _createdDate = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
