Imports System.Xml
'Imports XfoDotNetCtl  ' for Antenna House Xsl formatter

Public Class MessagingInfo
    '
    '   MessageInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _messageId As String
    Private _messageTemplateId As String
    Private _templateDescrip As String
    Private _recipientId As String
    Private _studentId As String
    Private _xmlContent As String
    Private _createdDate As DateTime
    Private _actuallySentOn As DateTime
    Private _messageSchema As String
    Private _messageSchemaDescrip As String
    Private _messageSchemaURL As String
    Private _advantageEventId As AdvantageEvent
    Private _recipientTypeId As MessageRecipientType
    Private _sqlStatement As String
    Private _xslt_Html As String
    Private _xslt_Rtf As String
    Private _xsl_Fo As String
    Private _spsData As String
    Private _messageTypeId As MessageType
    Private _messageFormatId As MessageFormat
    Private _sendImmediately As Boolean
    Private _keepHistory As Boolean
    Private _delayTimeSpanUnitId As AdvantageTimeSpanUnit
    Private _delayTimeSpanNumberOfUnits As Integer
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _messageId = Guid.NewGuid.ToString
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property MessageId() As String
        Get
            Return _messageId
        End Get
        Set(ByVal Value As String)
            _messageId = Value
        End Set
    End Property
    Public Property MessageTemplateId() As String
        Get
            Return _messageTemplateId
        End Get
        Set(ByVal Value As String)
            _messageTemplateId = Value
        End Set
    End Property
    Public Property TemplateDescrip() As String
        Get
            Return _templateDescrip
        End Get
        Set(ByVal Value As String)
            _templateDescrip = Value
        End Set
    End Property
    Public Property RecipientId() As String
        Get
            Return _recipientId
        End Get
        Set(ByVal Value As String)
            _recipientId = Value
        End Set
    End Property
    Public Property StudentId() As String
        Get
            Return _studentId
        End Get
        Set(ByVal Value As String)
            _studentId = Value
        End Set
    End Property
    Public Property XmlContent() As String
        Get
            Return _xmlContent
        End Get
        Set(ByVal Value As String)
            _xmlContent = Value
        End Set
    End Property
    Public Property CreatedDate() As Date
        Get
            Return _createdDate
        End Get
        Set(ByVal Value As Date)
            _createdDate = Value
        End Set
    End Property
    Public Property ActuallySentOn() As Date
        Get
            Return _actuallySentOn
        End Get
        Set(ByVal Value As Date)
            _actuallySentOn = Value
        End Set
    End Property
    Public Property MessageSchema() As String
        Get
            Return _messageSchema
        End Get
        Set(ByVal Value As String)
            _messageSchema = Value
        End Set
    End Property
    Public Property MessageSchemaDescrip() As String
        Get
            Return _messageSchemaDescrip
        End Get
        Set(ByVal Value As String)
            _messageSchemaDescrip = Value
        End Set
    End Property
    Public Property MessageSchemaURL() As String
        Get
            Return _messageSchemaURL
        End Get
        Set(ByVal Value As String)
            _messageSchemaURL = Value
        End Set
    End Property
    Public Property advantageEventId() As AdvantageEvent
        Get
            Return _advantageEventId
        End Get
        Set(ByVal Value As AdvantageEvent)
            _advantageEventId = Value
        End Set
    End Property
    Public Property RecipientTypeId() As MessageRecipientType
        Get
            Return _recipientTypeId
        End Get
        Set(ByVal Value As MessageRecipientType)
            _recipientTypeId = Value
        End Set
    End Property
    Public Property SqlStatement() As String
        Get
            Return _sqlStatement
        End Get
        Set(ByVal Value As String)
            _sqlStatement = Value
        End Set
    End Property
    Public Property Xslt_Html() As String
        Get
            Return _xslt_Html
        End Get
        Set(ByVal Value As String)
            _xslt_Html = Value
        End Set
    End Property
    Public Property Xslt_Rtf() As String
        Get
            Return _xslt_Rtf
        End Get
        Set(ByVal Value As String)
            _xslt_Rtf = Value
        End Set
    End Property
    Public Property SpsData() As String
        Get
            Return _spsData
        End Get
        Set(ByVal Value As String)
            _spsData = Value
        End Set
    End Property
    Public Property Xsl_Fo() As String
        Get
            Return _xsl_Fo
        End Get
        Set(ByVal Value As String)
            _xsl_Fo = Value
        End Set
    End Property
    Public Property MessageTypeId() As MessageType
        Get
            Return _messageTypeId
        End Get
        Set(ByVal Value As MessageType)
            _messageTypeId = Value
        End Set
    End Property
    Public Property MessageFormatId() As MessageFormat
        Get
            Return _messageFormatId
        End Get
        Set(ByVal Value As MessageFormat)
            _messageFormatId = Value
        End Set
    End Property
    Public Property SendImmediately() As Boolean
        Get
            Return _sendImmediately
        End Get
        Set(ByVal Value As Boolean)
            _sendImmediately = Value
        End Set
    End Property
    Public Property KeepHistory() As Boolean
        Get
            Return _keepHistory
        End Get
        Set(ByVal Value As Boolean)
            _keepHistory = Value
        End Set
    End Property
    Public Property DelayTimeSpanUnitId() As AdvantageTimeSpanUnit
        Get
            Return _delayTimeSpanUnitId
        End Get
        Set(ByVal Value As AdvantageTimeSpanUnit)
            _delayTimeSpanUnitId = Value
        End Set
    End Property
    Public Property DelayTimeSpanNumberOfUnits() As Integer
        Get
            Return _delayTimeSpanNumberOfUnits
        End Get
        Set(ByVal Value As Integer)
            _delayTimeSpanNumberOfUnits = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
#Region "Public Methods"

#End Region

End Class
Public Class MessageSchemaInfo
    '
    '   MessageSchemaInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _messageSchemaId As String
    Private _messageSchemaDescrip As String
    Private _statusId As String
    Private _status As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _messageSchemaURL As String
    Private _messageSchema As String
    Private _sqlStatement As String
    Private _numberOfMessageTemplates As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _messageSchemaId = Guid.NewGuid.ToString
        _statusId = Guid.Empty.ToString
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region

#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property MessageSchemaId() As String
        Get
            Return _messageSchemaId
        End Get
        Set(ByVal Value As String)
            _messageSchemaId = Value
        End Set
    End Property
    Public Property MessageSchemaDescrip() As String
        Get
            Return _messageSchemaDescrip
        End Get
        Set(ByVal Value As String)
            _messageSchemaDescrip = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property MessageSchemaURL() As String
        Get
            Return _messageSchemaURL
        End Get
        Set(ByVal Value As String)
            _messageSchemaURL = Value
        End Set
    End Property
    Public Property MessageSchema() As String
        Get
            Return _messageSchema
        End Get
        Set(ByVal Value As String)
            _messageSchema = Value
        End Set
    End Property
    Public Property SqlStatement() As String
        Get
            Return _sqlStatement
        End Get
        Set(ByVal Value As String)
            _sqlStatement = Value
        End Set
    End Property
    Public Property NumberOfMessageTemplates() As Integer
        Get
            Return _numberOfMessageTemplates
        End Get
        Set(ByVal Value As Integer)
            _numberOfMessageTemplates = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Class MessageGroupSchemaInfo

    '
    '   MessageGroupSchemaInfo Class
    '
    Inherits MessageSchemaInfo
#Region " Private Variables and Objects"
    Private _messageSchemaInfo As MessageSchemaInfo
    Private _spsData As String
#End Region
#Region " Public Constructors "
    Public Sub New()
        MyBase.New()
    End Sub
#End Region
#Region " Public Properties"
    Public Property SpsData() As String
        Get
            Return _spsData
        End Get
        Set(ByVal Value As String)
            _spsData = Value
        End Set
    End Property
#End Region
End Class
Public Class MessageTemplateInfo
    '
    '   MessageTemplateInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _messageTemplateId As String
    Private _templateDescrip As String
    Private _statusId As String
    Private _status As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _messageSchemaId As String
    Private _messageSchemaDescrip As String
    Private _messageSchemaURL As String
    Private _messageSchema As String
    Private _advantageEventId As AdvantageEvent
    Private _recipientTypeId As MessageRecipientType
    Private _sqlStatement As String
    Private _xslt_Html As String
    Private _xslt_Rtf As String
    Private _xsl_Fo As String
    Private _spsData As String
    Private _messageTypeId As MessageType
    Private _messageFormatId As MessageFormat
    Private _sendImmediately As Boolean
    Private _keepHistory As Boolean
    Private _delayTimeSpanUnitId As AdvantageTimeSpanUnit
    Private _delayTimeSpanNumberOfUnits As Integer
    Private _numberOfMessages As Integer
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _messageTemplateId = Guid.NewGuid.ToString
        _statusId = Guid.Empty.ToString
        _advantageEventId = AdvantageEvent.On_Demand
        _recipientTypeId = MessageRecipientType.Students
        _messageTypeId = MessageType.Email
        _messageFormatId = MessageFormat.HTML
        _sendImmediately = False
        _keepHistory = True
        _delayTimeSpanUnitId = AdvantageTimeSpanUnit.Seconds
        _delayTimeSpanNumberOfUnits = 0
        _numberOfMessages = 0
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region

#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property MessageTemplateId() As String
        Get
            Return _messageTemplateId
        End Get
        Set(ByVal Value As String)
            _messageTemplateId = Value
        End Set
    End Property
    Public Property TemplateDescrip() As String
        Get
            Return _templateDescrip
        End Get
        Set(ByVal Value As String)
            _templateDescrip = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property MessageSchemaId() As String
        Get
            Return _messageSchemaId
        End Get
        Set(ByVal Value As String)
            _messageSchemaId = Value
        End Set
    End Property
    Public Property MessageSchemaDescrip() As String
        Get
            Return _messageSchemaDescrip
        End Get
        Set(ByVal Value As String)
            _messageSchemaDescrip = Value
        End Set
    End Property
    Public Property MessageSchemaURL() As String
        Get
            Return _messageSchemaURL
        End Get
        Set(ByVal Value As String)
            _messageSchemaURL = Value
        End Set
    End Property
    Public Property MessageSchema() As String
        Get
            Return _messageSchema
        End Get
        Set(ByVal Value As String)
            _messageSchema = Value
        End Set
    End Property
    Public Property AdvantageEventId() As AdvantageEvent
        Get
            Return _advantageEventId
        End Get
        Set(ByVal Value As AdvantageEvent)
            _advantageEventId = Value
        End Set
    End Property
    Public Property RecipientTypeId() As MessageRecipientType
        Get
            Return _recipientTypeId
        End Get
        Set(ByVal Value As MessageRecipientType)
            _recipientTypeId = Value
        End Set
    End Property
    Public Property SqlStatement() As String
        Get
            Return _sqlStatement
        End Get
        Set(ByVal Value As String)
            _sqlStatement = Value
        End Set
    End Property
    Public Property Xslt_Html() As String
        Get
            Return _xslt_Html
        End Get
        Set(ByVal Value As String)
            _xslt_Html = Value
        End Set
    End Property
    Public Property Xslt_Rtf() As String
        Get
            Return _xslt_Rtf
        End Get
        Set(ByVal Value As String)
            _xslt_Rtf = Value
        End Set
    End Property
    Public Property SpsData() As String
        Get
            Return _spsData
        End Get
        Set(ByVal Value As String)
            _spsData = Value
        End Set
    End Property
    Public Property Xsl_Fo() As String
        Get
            Return _xsl_Fo
        End Get
        Set(ByVal Value As String)
            _xsl_Fo = Value
        End Set
    End Property
    Public Property MessageTypeId() As MessageType
        Get
            Return _messageTypeId
        End Get
        Set(ByVal Value As MessageType)
            _messageTypeId = Value
        End Set
    End Property
    Public Property MessageFormatId() As MessageFormat
        Get
            Return _messageFormatId
        End Get
        Set(ByVal Value As MessageFormat)
            _messageFormatId = Value
        End Set
    End Property
    Public Property SendImmediately() As Boolean
        Get
            Return _sendImmediately
        End Get
        Set(ByVal Value As Boolean)
            _sendImmediately = Value
        End Set
    End Property
    Public Property KeepHistory() As Boolean
        Get
            Return _keepHistory
        End Get
        Set(ByVal Value As Boolean)
            _keepHistory = Value
        End Set
    End Property
    Public Property DelayTimeSpanUnitId() As AdvantageTimeSpanUnit
        Get
            Return _delayTimeSpanUnitId
        End Get
        Set(ByVal Value As AdvantageTimeSpanUnit)
            _delayTimeSpanUnitId = Value
        End Set
    End Property
    Public Property DelayTimeSpanNumberOfUnits() As Integer
        Get
            Return _delayTimeSpanNumberOfUnits
        End Get
        Set(ByVal Value As Integer)
            _delayTimeSpanNumberOfUnits = Value
        End Set
    End Property
    Public Property NumberOfMessages() As Integer
        Get
            Return _numberOfMessages
        End Get
        Set(ByVal Value As Integer)
            _numberOfMessages = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Class FreedomTemplateInfo
    '
    '   FreedomTemplateInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _freedomTemplateId As String
    Private _templateDescrip As String
    Private _statusId As String
    Private _status As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _messageSchemaId As String
    Private _messageSchemaDescrip As String
    Private _messageSchemaURL As String
    Private _messageSchema As String
    Private _sqlStatement As String
    Private _xslt_Rtf As String
    Private _spsData As String
    Private _keepHistory As Boolean
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _freedomTemplateId = Guid.NewGuid.ToString
        _statusId = Guid.Empty.ToString
        _keepHistory = True
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region

#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property freedomTemplateId() As String
        Get
            Return _freedomTemplateId
        End Get
        Set(ByVal Value As String)
            _freedomTemplateId = Value
        End Set
    End Property
    Public Property TemplateDescrip() As String
        Get
            Return _templateDescrip
        End Get
        Set(ByVal Value As String)
            _templateDescrip = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property MessageSchemaId() As String
        Get
            Return _messageSchemaId
        End Get
        Set(ByVal Value As String)
            _messageSchemaId = Value
        End Set
    End Property
    Public Property MessageSchemaDescrip() As String
        Get
            Return _messageSchemaDescrip
        End Get
        Set(ByVal Value As String)
            _messageSchemaDescrip = Value
        End Set
    End Property
    Public Property MessageSchemaURL() As String
        Get
            Return _messageSchemaURL
        End Get
        Set(ByVal Value As String)
            _messageSchemaURL = Value
        End Set
    End Property
    Public Property MessageSchema() As String
        Get
            Return _messageSchema
        End Get
        Set(ByVal Value As String)
            _messageSchema = Value
        End Set
    End Property
    Public Property SqlStatement() As String
        Get
            Return _sqlStatement
        End Get
        Set(ByVal Value As String)
            _sqlStatement = Value
        End Set
    End Property
    Public Property SpsData() As String
        Get
            Return _spsData
        End Get
        Set(ByVal Value As String)
            _spsData = Value
        End Set
    End Property
    Public Property Xslt_Rtf() As String
        Get
            Return _xslt_Rtf
        End Get
        Set(ByVal Value As String)
            _xslt_Rtf = Value
        End Set
    End Property
    Public Property KeepHistory() As Boolean
        Get
            Return _keepHistory
        End Get
        Set(ByVal Value As Boolean)
            _keepHistory = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Class MessageGroupInfo
    '
    '   MessageGroupInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _messageGroupId As String
    Private _GroupDescrip As String
    Private _statusId As String
    Private _status As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _messageSchemaId As String
    Private _messageSchemaDescrip As String
    Private _messageSchemaURL As String
    Private _messageSchema As String
    Private _spsData As String
    Private _html_Xslt As String
    Private _xmlData As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _messageGroupId = Guid.NewGuid.ToString
        _statusId = Guid.Empty.ToString
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property MessageGroupId() As String
        Get
            Return _messageGroupId
        End Get
        Set(ByVal Value As String)
            _messageGroupId = Value
        End Set
    End Property
    Public Property GroupDescrip() As String
        Get
            Return _GroupDescrip
        End Get
        Set(ByVal Value As String)
            _GroupDescrip = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property MessageSchemaId() As String
        Get
            Return _messageSchemaId
        End Get
        Set(ByVal Value As String)
            _messageSchemaId = Value
        End Set
    End Property
    Public Property MessageSchemaDescrip() As String
        Get
            Return _messageSchemaDescrip
        End Get
        Set(ByVal Value As String)
            _messageSchemaDescrip = Value
        End Set
    End Property
    Public Property MessageSchemaURL() As String
        Get
            Return _messageSchemaURL
        End Get
        Set(ByVal Value As String)
            _messageSchemaURL = Value
        End Set
    End Property
    Public Property MessageSchema() As String
        Get
            Return _messageSchema
        End Get
        Set(ByVal Value As String)
            _messageSchema = Value
        End Set
    End Property
    Public Property SpsData() As String
        Get
            Return _spsData
        End Get
        Set(ByVal Value As String)
            _spsData = Value
        End Set
    End Property
    Public Property Html_Xslt() As String
        Get
            Return _html_Xslt
        End Get
        Set(ByVal Value As String)
            _html_Xslt = Value
        End Set
    End Property
    Public Property XmlData() As String
        Get
            Return _xmlData
        End Get
        Set(ByVal Value As String)
            _xmlData = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class

Public Delegate Sub AdvantageMessagingEventHandler(ByVal sender As Object, ByVal e As AdvantageMessagingEventArgs)
Public Class AdvantageMessage
    ' The event member that is of type AlarmEventHandler.
    '
    Public Event AdvantageMessaging As AdvantageMessagingEventHandler

    ' The protected OnAdvantageMessaging method raises the event by invoking 
    ' the delegates. The sender is always me, the current instance 
    ' of the class.
    '
    Protected Overridable Sub OnAdvantageMessaging(ByVal e As AdvantageMessagingEventArgs)
        RaiseEvent AdvantageMessaging(Me, e)
    End Sub
    Public Sub RaiseAdvantageMessagingEvent(ByVal advantageEvent As AdvantageEvent, ByVal campusId As String, ByVal sqlParameters As ArrayList)

        OnAdvantageMessaging(New AdvantageMessagingEventArgs(advantageEvent, campusId, sqlParameters))

    End Sub
End Class
Public Enum AdvantageEvent
    On_Demand = 0
    Query = 1
    Registration = 2
    Address_Change = 3
    Name_Change = 4
    WithDrawal = 5
    Graduation = 6
    Federal_Loan_Past_Due = 7
    SAP_Check = 8
End Enum
Public Enum AdvantageTimeSpanUnit
    Seconds = 1
    Minutes = 2
    Hours = 3
    Days = 4
    Weeks = 5
    Months = 6
    Years = 7
End Enum
Public Enum MessageRecipientType
    Students = 1
    Lenders = 2
    Parents = 3
End Enum
Public Enum MessageType
    Email = 1
    Print = 2
    Msg_to_Cell_Phone = 3
End Enum
Public Enum MessageFormat
    HTML = 1
    RTF = 2
    PDF = 3
End Enum

Public Class AdvantageMessagingSqlParameter
    Private _parameterName As String
    Private _parameterValue As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal parameterName As Object, ByVal parameterValue As Object)
        _parameterName = parameterName
        _parameterValue = parameterValue
    End Sub
    Public Property ParameterName() As String
        Get
            Return _parameterName
        End Get
        Set(ByVal Value As String)
            _parameterName = Value
        End Set
    End Property
    Public Property ParameterValue() As String
        Get
            Return _parameterValue
        End Get
        Set(ByVal Value As String)
            _parameterValue = Value
        End Set
    End Property
End Class
Public Class AdvantageMessageMemoryStream
    Private _messageId As String
    Private _contentType As String
    Private _documentMemoryStream As System.IO.MemoryStream
    Public Sub New()

    End Sub
    Public Sub New(ByVal contentType As String, ByVal documentMemoryStream As System.IO.MemoryStream)
        _messageId = MessageId
        _contentType = contentType
        _documentMemoryStream = documentMemoryStream
    End Sub
    Public Property MessageId() As String
        Get
            Return _messageId
        End Get
        Set(ByVal Value As String)
            _messageId = Value
        End Set
    End Property
    Public Property ContentType() As String
        Get
            Return _contentType
        End Get
        Set(ByVal Value As String)
            _contentType = Value
        End Set
    End Property
    Public Property DocumentMemoryStream() As System.IO.MemoryStream
        Get
            Return _documentMemoryStream
        End Get
        Set(ByVal Value As System.IO.MemoryStream)
            _documentMemoryStream = Value
        End Set
    End Property
End Class
'Public MustInherit Class PrintAdvantageMessage
'    Private _messageIds() As String
'    Private _formattedMessage As FormattedMessage
'    Public Sub New()
'    End Sub
'    Public Sub New(ByVal messageIds() As String)
'        _messageIds = messageIds
'    End Sub
'    Public MustOverride Function Deliver() As String
'End Class
'Public Class DeliverPrintedMessage
'    Inherits PrintAdvantageMessage
'    Public Overrides Function Deliver() As String
'    End Function
'    Private Function DeliverHTMLMessage(ByVal htmlFormattedMessage As HTMLFormattedMessage) As String
'    End Function
'    Private Function DeliverRTFMessage(ByVal rtfFormattedMessage As RTFFormattedMessage) As String
'    End Function
'    Private Function DeliverPDFMessage(ByVal pdfFormattedMessage As PDFFormattedMessage) As String
'    End Function
'End Class
Public MustInherit Class FormattedMessage
    Protected m_xmlContent As String
    Protected m_xsltTransform() As String
    Protected m_messageType As MessageType
    Protected Sub New(ByVal xmlContent As String, ByVal xsltTransform() As String, ByVal messageType As MessageType)
        m_xmlContent = xmlContent
        m_xsltTransform = xsltTransform
        m_messageType = messageType
    End Sub
    Public MustOverride ReadOnly Property ContentType() As String
    Public ReadOnly Property MessageType() As MessageType
        Get
            Return m_messageType
        End Get
    End Property
    Public Overridable Function GetMemoryStream() As System.IO.MemoryStream
        'this is the object that makes the transform
        Dim xslt As New System.Xml.Xsl.XslCompiledTransform

        'this is the Xml content. Xml content must be Navigable
        Dim xpathdocument As New System.Xml.XPath.XPathDocument(New System.IO.StringReader(m_xmlContent))

        'define writer
        Dim documentMemoryStream As New System.IO.MemoryStream
        Dim writer As New XmlTextWriter(documentMemoryStream, Nothing)
        writer.Formatting = Formatting.Indented
        For i As Integer = 0 To m_xsltTransform.Length - 1
            'Xsl stylesheet must be navigable
            xslt.Load(New System.Xml.XPath.XPathDocument(New System.IO.StringReader(m_xsltTransform(i))), Nothing, Nothing)

            'transform
            xslt.Transform(xpathdocument, Nothing, writer, Nothing)
        Next

        'return memory stream
        Return documentMemoryStream

    End Function
End Class
Public Class HTMLFormattedMessage
    Inherits FormattedMessage
    Public Sub New(ByVal xmlContent As String, ByVal xsltTransform() As String, ByVal messageType As MessageType)
        MyBase.New(xmlContent, xsltTransform, messageType)
    End Sub
    Public Overrides ReadOnly Property ContentType() As String
        Get
            Return "text/html; charset=UTF-8"
        End Get
    End Property
End Class
Public Class PDFFormattedMessage
    Inherits FormattedMessage
    Public Sub New(ByVal xmlContent As String, ByVal xsltTransform() As String, ByVal messageType As MessageType)
        MyBase.New(xmlContent, xsltTransform, messageType)
    End Sub
    Public Overrides Function GetMemoryStream() As System.IO.MemoryStream

        'this is the object that makes the transform
        Dim xslt As New System.Xml.Xsl.XslCompiledTransform

        'this is the Xml content. Xml content must be Navigable
        Dim xpathdocument As New System.Xml.XPath.XPathDocument(New System.IO.StringReader(m_xmlContent))

        'define writer
        Dim documentMemoryStream As New System.IO.MemoryStream
        Dim writer As New XmlTextWriter(documentMemoryStream, Nothing)
        writer.Formatting = Formatting.Indented

        For i As Integer = 0 To m_xsltTransform.Length - 1
            'Xsl stylesheet must be navigable
            xslt.Load(New System.Xml.XPath.XPathDocument(New System.IO.StringReader(m_xsltTransform(i))), Nothing, Nothing)

            'transform
            xslt.Transform(xpathdocument, Nothing, writer, Nothing)
        Next

        ''   generate document in PDF using Antenna formatter
        ''XfoDotNetCtl.XfoObj.Initialize()
        ''Dim doc As New XfoDotNetCtl.XfoObj
        'Dim pdfStream As New System.IO.MemoryStream
        'doc.Render(documentMemoryStream, pdfStream)
        'doc.Dispose()
        'XfoDotNetCtl.XfoObj.Terminate()
        ''end of code for Antenna Formatter

        'return memory stream
        Return Nothing 'pdfStream

    End Function
    Public Overrides ReadOnly Property ContentType() As String
        Get
            Return "application/pdf"
        End Get
    End Property
End Class
Public Class RTFFormattedMessage
    Inherits FormattedMessage
    Public Sub New(ByVal xmlContent As String, ByVal xsltTransform() As String, ByVal messageType As MessageType)
        MyBase.New(xmlContent, xsltTransform, messageType)
    End Sub
    Public Overrides ReadOnly Property ContentType() As String
        Get
            Return "application/msword"
        End Get
    End Property
End Class
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Class MessagingEventArgs
    Inherits EventArgs
    Private _advMessage As AdvMessage
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal advMessage As AdvMessage)
        MyBase.New()
        _advMessage = advMessage
    End Sub
    Public Property AdvMessage() As AdvMessage
        Get
            Return _advMessage
        End Get
        Set(ByVal Value As AdvMessage)
            _advMessage = Value
        End Set
    End Property
End Class
Public Delegate Sub DisplayingEventHandler(ByVal sender As Object, ByVal e As MessagingEventArgs)
Public Delegate Sub DeliveringEventHandler(ByVal sender As Object, ByVal e As MessagingEventArgs)
Public MustInherit Class AdvMessage
    Private _address As String
    Private _messageIds() As String
    Private _content As Content
    Public Event Displaying As DisplayingEventHandler
    Public Event Delivering As DeliveringEventHandler
    Protected Sub New()
    End Sub
    Protected Sub New(ByVal address As String, ByVal messageIds() As String, ByVal Content As Content)
        Me.New(messageIds, Content)
        _address = address
    End Sub
    Protected Sub New(ByVal messageIds() As String, ByVal Content As Content)
        _messageIds = messageIds
        _content = Content
    End Sub
    Public ReadOnly Property Content() As Content
        Get
            Return _content
        End Get
    End Property
    Public Property Address() As String
        Get
            Return _address
        End Get
        Set(ByVal Value As String)
            _address = Value
        End Set
    End Property
    Public Property MessageIds() As String()
        Get
            Return _messageIds
        End Get
        Set(ByVal Value As String())
            _messageIds = Value
        End Set
    End Property
    Public MustOverride Sub Deliver()
    Public MustOverride Sub Display()
    Public MustOverride ReadOnly Property MessageType() As MessageType
    ' The protected OnDisplaying method raises the event by invoking 
    ' the delegates. The sender is always me, the current instance 
    ' of the class.
    '
    Protected Overridable Sub OnDisplaying(ByVal e As MessagingEventArgs)
        RaiseEvent Displaying(Me, e)
    End Sub
    ' The protected OnDelivering method raises the event by invoking 
    ' the delegates. The sender is always me, the current instance 
    ' of the class.
    '
    Protected Overridable Sub OnDelivering(ByVal e As MessagingEventArgs)
        RaiseEvent Delivering(Me, e)
    End Sub
    Protected Sub RaiseDisplayingEvent(ByVal advMessage As AdvMessage)
        OnDisplaying(New MessagingEventArgs(advMessage))
    End Sub
    Protected Sub RaiseDeliveringEvent(ByVal advMessage As AdvMessage)
        OnDelivering(New MessagingEventArgs(advMessage))
    End Sub
End Class
Public Class PrintAdvantageMessage
    Inherits AdvMessage
    Public Sub New(ByVal messageIds() As String, ByVal Content As Content)
        MyBase.New(messageIds, Content)
    End Sub
    Public Overrides Sub Display()
        RaiseDisplayingEvent(Me)
    End Sub
    Public Overrides Sub Deliver()
        RaiseDeliveringEvent(Me)
    End Sub
    Public Overrides ReadOnly Property MessageType() As MessageType
        Get
            Return MessageType.Print
        End Get
    End Property
End Class
Public Class EmailAdvantageMessage
    Inherits AdvMessage
    Public Sub New(ByVal address As String, ByVal messageIds() As String, ByVal Content As Content)
        MyBase.New(address, messageIds, Content)
    End Sub
    Public Overrides Sub Display()
        RaiseDisplayingEvent(Me)
    End Sub
    Public Overrides Sub Deliver()
        RaiseDeliveringEvent(Me)
    End Sub
    Public Function EmailMessage(ByVal fromEmailAddress As String) As System.Web.Mail.MailMessage
        '   build the message
        Dim em As New System.Web.Mail.MailMessage
        With em
            .From = fromEmailAddress
            .To = Me.Address
            .Subject = Me.Content.Name
            .Body = Me.Content.Body
            .BodyFormat = Me.Content.BodyFormat
            'attach file attachments
            .Attachments.Add(Me.Content.Attachment)
        End With

        '   return the message
        Return em

    End Function
    Public Shared Function GetEmailAddressFromContent(ByVal content As Content) As String
        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(content.ContentData)
        If xmlDoc.FirstChild.Attributes("email") Is Nothing Then
            Return ""
        Else
            Return xmlDoc.FirstChild.Attributes("email").Value()
        End If
    End Function
    Public Overrides ReadOnly Property MessageType() As MessageType
        Get
            Return MessageType.Email
        End Get
    End Property
End Class
Public MustInherit Class Content
    Private _contentData As String
    Private _name As String
    Protected Sub New()
    End Sub
    Protected Sub New(ByVal name As String, ByVal contentData As String)
        _name = name
        _contentData = contentData
    End Sub
    Public Property ContentData() As String
        Get
            Return _contentData
        End Get
        Set(ByVal Value As String)
            _contentData = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal Value As String)
            _name = Value
        End Set
    End Property
    Public MustOverride ReadOnly Property Body() As String
    Public MustOverride ReadOnly Property BodyFormat() As System.Web.Mail.MailFormat
    Public MustOverride ReadOnly Property Attachment() As System.Web.Mail.MailAttachment
    Public MustOverride ReadOnly Property ContentType() As String
    Public MustOverride ReadOnly Property FileType() As String
    Public MustOverride ReadOnly Property MessageFormat() As MessageFormat
    Protected Function ApplyStyleSheet(ByVal xmlContent As String, ByVal XsltStyleSheet As String) As String
        'this is the object that makes the transform
        Dim xslt As New System.Xml.Xsl.XslCompiledTransform

        'this is the Xml content. Xml content must be Navigable
        Dim xpathdocument As New System.Xml.XPath.XPathDocument(New System.IO.StringReader(xmlContent))

        'define writer
        Dim ms As New System.IO.MemoryStream

        Dim enc As New System.Text.UTF8Encoding
        Dim writer As New System.Xml.XmlTextWriter(ms, enc)
        writer.Formatting = System.Xml.Formatting.Indented

        'Xsl stylesheet must be navigable
        xslt.Load(New System.Xml.XPath.XPathDocument(New System.IO.StringReader(XsltStyleSheet)), Nothing, Nothing)

        'transform
        xslt.Transform(xpathdocument, Nothing, writer, Nothing)

        'this is the transformed string
        Return enc.GetString(ms.ToArray)

    End Function
    Protected Overridable ReadOnly Property FileAttachment() As String
        Get
            'this is the name of the temporary file
            Dim tempFileName As String = System.IO.Path.GetTempPath() + Me.Name.Replace(" ", "_") + "." + Me.FileType

            'create the file in the temporary folder
            Dim sr As System.IO.FileStream = System.IO.File.Create(tempFileName, Me.ContentData.Length)
            Dim enc As New System.Text.UTF8Encoding
            Dim buffer() As Byte = enc.GetBytes(Me.ContentData)
            sr.Write(buffer, 0, Me.ContentData.Length)
            sr.Close()

            'return filename with path
            Return tempFileName
        End Get
    End Property
 
End Class
Public Class HTMLContent
    Inherits Content
    Public Sub New(ByVal xmlContent As String, ByVal XsltStyleSheet As String)
        MyBase.New()
        'this is the transformed string
        MyBase.ContentData = ApplyStyleSheet(xmlContent, XsltStyleSheet)
    End Sub
    Public Overrides ReadOnly Property ContentType() As String
        Get
            Return "text/html; charset=UTF-8"
        End Get
    End Property
    Public Overrides ReadOnly Property FileType() As String
        Get
            Return "htm"
        End Get
    End Property
    Public Overrides ReadOnly Property Body() As String
        Get
            Return Me.ContentData
        End Get
    End Property
    Public Overrides ReadOnly Property BodyFormat() As System.Web.Mail.MailFormat
        Get
            Return Web.Mail.MailFormat.Html
        End Get
    End Property
    Public Overrides ReadOnly Property Attachment() As System.Web.Mail.MailAttachment
        Get
            Return Nothing
        End Get
    End Property
    Public Overrides ReadOnly Property MessageFormat() As MessageFormat
        Get
            Return MessageFormat.HTML
        End Get
    End Property
End Class
Public Class PDFContent
    Inherits Content
    Public Sub New(ByVal xmlContent As String, ByVal XsltStyleSheet As String)
        MyBase.New()
        'this is the transformed string
        Dim pdf As New AntennaFormater
        MyBase.ContentData = pdf.ConvertToPDF(ApplyStyleSheet(xmlContent, XsltStyleSheet))
    End Sub
    Public Overrides ReadOnly Property ContentType() As String
        Get
            Return "application/pdf"
        End Get
    End Property
    Public Overrides ReadOnly Property FileType() As String
        Get
            Return "pdf"
        End Get
    End Property
    Public Overrides ReadOnly Property Body() As String
        Get
            Return "Attached please find a " + Me.Name
        End Get
    End Property
    Public Overrides ReadOnly Property BodyFormat() As System.Web.Mail.MailFormat
        Get
            Return Web.Mail.MailFormat.Html
        End Get
    End Property
    Public Overrides ReadOnly Property Attachment() As System.Web.Mail.MailAttachment
        Get
            Return New System.Web.Mail.MailAttachment(Me.FileAttachment)
        End Get
    End Property
    Public Overrides ReadOnly Property MessageFormat() As MessageFormat
        Get
            Return MessageFormat.PDF
        End Get
    End Property
End Class
Public Class RTFContent
    Inherits Content
    Public Sub New(ByVal xmlContent As String, ByVal XsltStyleSheet As String)
        MyBase.New()
        'this is the transformed string
        MyBase.ContentData = ApplyStyleSheet(xmlContent, XsltStyleSheet)
    End Sub
    Public Overrides ReadOnly Property ContentType() As String
        Get
            Return "application/msword"
        End Get
    End Property
    Public Overrides ReadOnly Property FileType() As String
        Get
            Return "doc"
        End Get
    End Property
    Public Overrides ReadOnly Property Body() As String
        Get
            Return "Attached please find a " + Me.Name
        End Get
    End Property
    Public Overrides ReadOnly Property BodyFormat() As System.Web.Mail.MailFormat
        Get
            Return Web.Mail.MailFormat.Html
        End Get
    End Property
    Public Overrides ReadOnly Property Attachment() As System.Web.Mail.MailAttachment
        Get
            Return New System.Web.Mail.MailAttachment(Me.FileAttachment)
        End Get
    End Property
    Public Overrides ReadOnly Property MessageFormat() As MessageFormat
        Get
            Return MessageFormat.RTF
        End Get
    End Property
End Class
Public MustInherit Class PDFFormatter
    Public Sub New()
    End Sub
    Public MustOverride Function ConvertToPDF(ByVal xmltext As String) As String
End Class
Public Class AntennaFormater
    Inherits PDFFormatter
    Public Overrides Function ConvertToPDF(ByVal xmltext As String) As String

        '   generate document in PDF using Antenna formatter
        ''XfoDotNetCtl.XfoObj.Initialize()
        'Dim doc As New XfoDotNetCtl.XfoObj
        'Dim pdfStream As New System.IO.MemoryStream
        'doc.Render(New System.Xml.XmlTextReader(New System.IO.StringReader(xmltext)), pdfStream)
        'doc.Dispose()
        'XfoDotNetCtl.XfoObj.Terminate()
        ''end of code for Antenna Formatter

        'return pdf data
        Return Nothing '(New System.Text.UTF8Encoding).GetString(pdfStream.ToArray)

    End Function
End Class

