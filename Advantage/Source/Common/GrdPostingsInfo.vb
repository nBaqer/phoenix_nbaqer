Public Class GrdPostingsInfo
#Region "Private Variable Declaration"
    Private _GrdBkResultId As String
    Private _ClsSectId As String
    Private _StuEnrollId As String
    Private _GrdBkWgtDetailId As String
    Private _Score As Double
    Private _ResNum As Integer
    Private _Comments As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _IsInDB As Boolean
    Private _GradeDescription As String
    Private _GradeQuality As String
    Private _GrdSystemId As String
    Private _GrdSysDetailId As String
    Private _Grade As String
    Private _IsCompGraded As Boolean
#End Region

#Region "GrdPostingsInfo Properties"
    Public Property GrdBkResultId() As String
        Get
            Return _GrdBkResultId
        End Get
        Set(ByVal Value As String)
            _GrdBkResultId = Value
        End Set
    End Property
    Public Property IsInDB() As Boolean
        Get
            Return _IsInDB
        End Get
        Set(ByVal Value As Boolean)
            _IsInDB = Value
        End Set
    End Property
    Public Property GrdSysDetailId() As String
        Get
            Return _GrdSysDetailId
        End Get
        Set(ByVal Value As String)
            _GrdSysDetailId = Value
        End Set
    End Property
    Public Property GradeDescription() As String
        Get
            Return _GradeDescription
        End Get
        Set(ByVal Value As String)
            _GradeDescription = Value
        End Set
    End Property
    Public Property GrdSystemId() As String
        Get
            Return _GrdSystemId
        End Get
        Set(ByVal Value As String)
            _GrdSystemId = Value
        End Set
    End Property
    Public Property GradeQuality() As String
        Get
            Return _GradeQuality
        End Get
        Set(ByVal Value As String)
            _GradeQuality = Value
        End Set
    End Property
    Public Property ClsSectId() As String
        Get
            Return _ClsSectId
        End Get
        Set(ByVal Value As String)
            _ClsSectId = Value
        End Set
    End Property

    Public Property StuEnrollId() As String
        Get
            Return _StuEnrollId
        End Get
        Set(ByVal Value As String)
            _StuEnrollId = Value
        End Set
    End Property
    Public Property GrdBkWgtDetailId() As String
        Get
            Return _GrdBkWgtDetailId
        End Get
        Set(ByVal Value As String)
            _GrdBkWgtDetailId = Value
        End Set
    End Property
    Public Property Score() As Double
        Get
            Return _Score
        End Get
        Set(ByVal Value As Double)
            _Score = Value
        End Set
    End Property
    Public Property ResNum() As Integer
        Get
            Return _ResNum
        End Get
        Set(ByVal Value As Integer)
            _ResNum = Value
        End Set
    End Property
    Public Property Comments() As String
        Get
            Return _Comments
        End Get
        Set(ByVal Value As String)
            _Comments = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property Grade() As String
        Get
            Return _Grade
        End Get
        Set(ByVal Value As String)
            _Grade = Value
        End Set
    End Property
    Public Property IsCompGraded() As Boolean
        Get
            Return _IsCompGraded
        End Get
        Set(ByVal Value As Boolean)
            _IsCompGraded = Value
        End Set
    End Property
#End Region
End Class
