
Public Class plEmployerContact
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _EmployerContactId As String
    Private _LastName As String
    Private _statusId As String
    Private _status As String
    Private _FirstName As String
    Private _MiddleName As String
    Private _PrefixId As String
    Private _prefix As String
    Private _SuffixId As String
    Private _suffix As String
    Private _Title As String
    Private _WorkPhone As String
    Private _HomePhone As String
    Private _CellPhone As String
    Private _Beeper As String
    Private _WorkEmail As String
    Private _HomeEmail As String
    Private _Address1 As String
    Private _Address2 As String
    Private _City As String
    Private _State As String
    Private _stateId As String
    Private _zip As String
    Private _Country As String
    Private _countryId As String
    Private _WorkExt As String
    Private _WorkBestTime As String
    Private _HomeExt As String
    Private _HomeBestTime As String
    Private _CellBestTime As String
    Private _PinNumber As String
    Private _Notes As String
    Private _AddressTypeId As String
    Private _addressType As String
    Private _EmployerID As String
    Private _ForeignHomePhone As String
    Private _ForeignCellPhone As String
    Private _ForeignWorkPhone As String
    Private _ForeignFax As String
    Private _OtherState As String
    Private _ForeignZip As String
    Private _ModDate As DateTime
#End Region
#Region " Public Constructor"
    Public Sub New()
        _isInDB = False
        _LastName = ""
        _FirstName = ""
        _MiddleName = ""
        _statusId = Guid.Empty.ToString()
        _status = "Select"
        _PrefixId = Guid.Empty.ToString()
        _prefix = ""
        _SuffixId = Guid.Empty.ToString()
        _suffix = ""
        _Title = ""
        _WorkPhone = ""
        _HomePhone = ""
        _CellPhone = ""
        _Beeper = ""
        _WorkEmail = ""
        _HomeEmail = ""
        _EmployerContactId = Guid.NewGuid.ToString
        _Address1 = ""
        _Address2 = ""
        _City = ""
        _StateId = Guid.Empty.ToString
        _State = ""
        _zip = ""
        _countryId = Guid.Empty.ToString
        _Country = ""
        _WorkExt = ""
        _WorkBestTime = ""
        _HomeExt = ""
        _HomeBestTime = ""
        _CellBestTime = ""
        _PinNumber = ""
        _Notes = ""
        _AddressTypeId = Guid.Empty.ToString
        _addressType = ""
        _EmployerID = Guid.Empty.ToString
        _ForeignHomePhone = ""
        _ForeignCellPhone = ""
        _ForeignFax = ""
        _ForeignWorkPhone = ""
        _OtherState = ""
        _ForeignZip = ""
        _ModDate = Date.MinValue
    End Sub
#End Region

#Region "Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return _LastName
        End Get
        Set(ByVal Value As String)
            _LastName = Value
        End Set
    End Property
    Public Property AddressTypeId() As String
        Get
            Return _AddressTypeId
        End Get
        Set(ByVal Value As String)
            _AddressTypeId = Value
        End Set
    End Property
    Public Property AddressType() As String
        Get
            Return _addressType
        End Get
        Set(ByVal Value As String)
            _addressType = Value
        End Set
    End Property
    Public Property EmployerId() As String
        Get
            Return _EmployerID
        End Get
        Set(ByVal Value As String)
            _EmployerID = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
    Public Property FirstName() As String
        Get
            Return _FirstName
        End Get
        Set(ByVal Value As String)
            _FirstName = Value
        End Set
    End Property
    Public Property MiddleName() As String
        Get
            Return _MiddleName
        End Get
        Set(ByVal Value As String)
            _MiddleName = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property ForeignZip() As String
        Get
            Return _ForeignZip
        End Get
        Set(ByVal Value As String)
            _ForeignZip = Value
        End Set
    End Property
    Public Property ForeignHomePhone() As String
        Get
            Return _ForeignHomePhone
        End Get
        Set(ByVal Value As String)
            _ForeignHomePhone = Value
        End Set
    End Property
    Public Property ForeignCellPhone() As String
        Get
            Return _ForeignCellPhone
        End Get
        Set(ByVal Value As String)
            _ForeignCellPhone = Value
        End Set
    End Property
    Public Property ForeignWorkPhone() As String
        Get
            Return _ForeignWorkPhone
        End Get
        Set(ByVal Value As String)
            _ForeignWorkPhone = Value
        End Set
    End Property
    Public Property ForeignFax() As String
        Get
            Return _ForeignFax
        End Get
        Set(ByVal Value As String)
            _ForeignFax = Value
        End Set
    End Property
    Public Property OtherState() As String
        Get
            Return _OtherState
        End Get
        Set(ByVal Value As String)
            _OtherState = Value
        End Set
    End Property
    Public Property Prefix() As String
        Get
            Return _prefix
        End Get
        Set(ByVal Value As String)
            _prefix = Value
        End Set
    End Property
    Public Property Suffix() As String
        Get
            Return _suffix
        End Get
        Set(ByVal Value As String)
            _suffix = Value
        End Set
    End Property
    Public Property PrefixID() As String
        Get
            Return _PrefixId
        End Get
        Set(ByVal Value As String)
            _PrefixId = Value
        End Set
    End Property
    Public Property SuffixId() As String
        Get
            Return _SuffixId
        End Get
        Set(ByVal Value As String)
            _SuffixId = Value
        End Set
    End Property
    Public Property Title() As String
        Get
            Return _Title
        End Get
        Set(ByVal Value As String)
            _Title = Value
        End Set
    End Property

    Public Property WorkPhone() As String
        Get
            Return _WorkPhone
        End Get
        Set(ByVal Value As String)
            _WorkPhone = Value
        End Set
    End Property

    Public Property HomePhone() As String
        Get
            Return _HomePhone
        End Get
        Set(ByVal Value As String)
            _HomePhone = Value
        End Set
    End Property
    Public Property CellPhone() As String
        Get
            Return _CellPhone
        End Get
        Set(ByVal Value As String)
            _CellPhone = Value
        End Set
    End Property
    Public Property Beeper() As String
        Get
            Return _Beeper
        End Get
        Set(ByVal Value As String)
            _Beeper = Value
        End Set
    End Property
    Public Property WorkEmail() As String
        Get
            Return _WorkEmail
        End Get
        Set(ByVal Value As String)
            _WorkEmail = Value
        End Set
    End Property

    Public Property HomeEmail() As String
        Get
            Return _HomeEmail
        End Get
        Set(ByVal Value As String)
            _HomeEmail = Value
        End Set
    End Property
    Public Property EmployerContactId() As String
        Get
            Return _EmployerContactId
        End Get
        Set(ByVal Value As String)
            _EmployerContactId = Value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return _Address1
        End Get
        Set(ByVal Value As String)
            _Address1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return _Address2
        End Get
        Set(ByVal Value As String)
            _Address2 = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _City
        End Get
        Set(ByVal Value As String)
            _City = Value
        End Set
    End Property
    Public Property State() As String
        Get
            Return _State
        End Get
        Set(ByVal Value As String)
            _State = Value
        End Set
    End Property
    Public Property StateId() As String
        Get
            Return _StateId
        End Get
        Set(ByVal Value As String)
            _StateId = Value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return _zip
        End Get
        Set(ByVal Value As String)
            _zip = Value
        End Set
    End Property
    Public Property CountryId() As String
        Get
            Return _countryId
        End Get
        Set(ByVal Value As String)
            _countryId = Value
        End Set
    End Property
    Public Property Country() As String
        Get
            Return _Country
        End Get
        Set(ByVal Value As String)
            _Country = Value
        End Set
    End Property
    Public Property WorkExt() As String
        Get
            Return _WorkExt
        End Get
        Set(ByVal Value As String)
            _WorkExt = Value
        End Set
    End Property
    Public Property WorkBestTime() As String
        Get
            Return _WorkBestTime
        End Get
        Set(ByVal Value As String)
            _WorkBestTime = Value
        End Set
    End Property
    Public Property HomeBestTime() As String
        Get
            Return _HomeBestTime
        End Get
        Set(ByVal Value As String)
            _HomeBestTime = Value
        End Set
    End Property
    Public Property CellBestTime() As String
        Get
            Return _CellBestTime
        End Get
        Set(ByVal Value As String)
            _CellBestTime = Value
        End Set
    End Property
    Public Property PinNumber() As String
        Get
            Return _PinNumber
        End Get
        Set(ByVal Value As String)
            _PinNumber = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
#End Region
End Class
