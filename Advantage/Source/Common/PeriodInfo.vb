Public Class PeriodInfo
    '
    '   PeriodInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _PeriodId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _periodDescrip As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _meetDays As Byte
    Private _startTimeId As String
    Private _startTime As Date
    Private _endTimeId As String
    Private _endTime As Date
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _PeriodId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _PeriodDescrip = ""
        _meetDays = 0
        _startTimeId = Guid.Empty.ToString
        _startTime = Date.MinValue
        _endTimeId = Guid.Empty.ToString
        _endTime = Date.MaxValue
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property PeriodId() As String
        Get
            Return _PeriodId
        End Get
        Set(ByVal Value As String)
            _PeriodId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _periodDescrip
        End Get
        Set(ByVal Value As String)
            _periodDescrip = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property MeetDays() As Byte
        Get
            Return _meetDays
        End Get
        Set(ByVal Value As Byte)
            _meetDays = Value
        End Set
    End Property
    Public Property StartTimeId() As String
        Get
            Return _startTimeId
        End Get
        Set(ByVal Value As String)
            _startTimeId = Value
        End Set
    End Property
    Public Property StartTime() As Date
        Get
            Return _startTime
        End Get
        Set(ByVal Value As Date)
            _startTime = Value
        End Set
    End Property
    Public Property EndTimeId() As String
        Get
            Return _endTimeId
        End Get
        Set(ByVal Value As String)
            _endTimeId = Value
        End Set
    End Property
    Public Property EndTime() As Date
        Get
            Return _endTime
        End Get
        Set(ByVal Value As Date)
            _endTime = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Class AdvantageClassSectionMeeting
    Implements IComparable
#Region "Private Variables"
    Private _meetingDate As DateTime
    Private _duration As Integer
#End Region
#Region "Public Constructors"
    Public Sub New()
    End Sub
    Public Sub New(ByVal meetingDate As DateTime, ByVal duration As Integer)
        _meetingDate = meetingDate
        _duration = duration
    End Sub
#End Region
#Region "Public Properties"
    Public Property MeetingDateAndTime() As DateTime
        Get
            Return _meetingDate
        End Get
        Set(ByVal value As DateTime)
            _meetingDate = value
        End Set
    End Property
    Public Property Duration() As Integer
        Get
            Return _duration
        End Get
        Set(ByVal value As Integer)
            _duration = value
        End Set
    End Property
#End Region
#Region "Public Functions"
    Public Overloads Function CompareTo(ByVal obj As Object) As Integer _
    Implements IComparable.CompareTo

        If TypeOf obj Is AdvantageClassSectionMeeting Then
            Dim acsm As AdvantageClassSectionMeeting = CType(obj, AdvantageClassSectionMeeting)

            Return _meetingDate.CompareTo(acsm.MeetingDateAndTime)
        End If

        Throw New ArgumentException("object is not an AdvantageClassSectionMeeting")
    End Function

#End Region
End Class
Public Class AdvantageClassSectionMeetingAttendance
    Implements IComparable
#Region "Private Variables"
    Private _meetingDate As DateTime
    Private _actual As Decimal
    Private _tardy As Boolean
    Private _excused As Boolean
#End Region
#Region "Public Constructors"
    Public Sub New()
    End Sub
    Public Sub New(ByVal meetingDate As DateTime, ByVal actual As Decimal, ByVal tardy As Boolean, ByVal excused As Boolean)
        _meetingDate = meetingDate
        _actual = actual
        _tardy = tardy
        _excused = excused
    End Sub
#End Region
#Region "Public Properties"
    Public Property MeetingDateAndTime() As DateTime
        Get
            Return _meetingDate
        End Get
        Set(ByVal value As DateTime)
            _meetingDate = value
        End Set
    End Property
    Public Property Actual() As Decimal
        Get
            Return _actual
        End Get
        Set(ByVal value As Decimal)
            _actual = value
        End Set
    End Property
    Public Property Tardy() As Boolean
        Get
            Return _tardy
        End Get
        Set(ByVal value As Boolean)
            _tardy = value
        End Set
    End Property
    Public Property Excused() As Boolean
        Get
            Return _excused
        End Get
        Set(ByVal value As Boolean)
            _excused = value
        End Set
    End Property
#End Region
#Region "Public Functions"
    Public Overloads Function CompareTo(ByVal obj As Object) As Integer _
    Implements IComparable.CompareTo

        If TypeOf obj Is AdvantageClassSectionMeetingAttendance Then
            Dim acsma As AdvantageClassSectionMeetingAttendance = CType(obj, AdvantageClassSectionMeetingAttendance)

            Return _meetingDate.CompareTo(acsma.MeetingDateAndTime)
        End If

        Throw New ArgumentException("object is not an AdvantageClassSectionMeetingAttendance")
    End Function

#End Region
End Class

