Public Class RequirementTypeInfo
#Region "Private Variable Declarations"
    Private _Code As String
    Private _StatusId As Guid
    Private _Descrip As String
    Private _CampGrpId As Guid
    Private _TrkGrade As Integer
    Private _TrkHours As Integer
    Private _TrkCount As Integer
    Private _IsGroup As Integer
    Private _ChildType As Integer
    Private _ReqTypeId As Integer

#End Region
#Region "Requirement Type Info Properties"


    Public Property Code() As String
        Get
            Return _Code
        End Get
        Set(ByVal Value As String)
            _Code = Value
        End Set
    End Property

    Public Property StatusId() As Guid
        Get
            Return _StatusId
        End Get
        Set(ByVal Value As Guid)
            _StatusId = Value
        End Set
    End Property
    Public Property Descrip() As String
        Get
            Return _Descrip
        End Get
        Set(ByVal Value As String)
            _Descrip = Value
        End Set
    End Property
    Public Property CampGrpId() As Guid
        Get
            Return _CampGrpId
        End Get
        Set(ByVal Value As Guid)
            _CampGrpId = Value
        End Set
    End Property

    Public Property TrkGrade() As Integer
        Get
            Return _TrkGrade
        End Get
        Set(ByVal Value As Integer)
            _TrkGrade = Value
        End Set
    End Property

    Public Property TrkHours() As Integer
        Get
            Return _TrkHours
        End Get
        Set(ByVal Value As Integer)
            _TrkHours = Value
        End Set
    End Property

    Public Property TrkCount() As Integer
        Get
            Return _TrkCount
        End Get
        Set(ByVal Value As Integer)
            _TrkCount = Value
        End Set
    End Property

    Public Property IsGroup() As Integer
        Get
            Return _IsGroup
        End Get
        Set(ByVal Value As Integer)
            _IsGroup = Value
        End Set
    End Property

    Public Property ChildType() As Integer
        Get
            Return _ChildType
        End Get
        Set(ByVal Value As Integer)
            _ChildType = Value
        End Set
    End Property

    Public Property ReqTypeId() As Integer
        Get
            Return _ReqTypeId
        End Get
        Set(ByVal Value As Integer)
            _ReqTypeId = Value
        End Set
    End Property
#End Region
End Class
