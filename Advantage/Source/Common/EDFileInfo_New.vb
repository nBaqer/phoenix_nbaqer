﻿Imports System
Imports System.IO
Imports System.Net
Public Class EDFileInfo_New
#Region " Private Variables and Objects"
    Private _strFileName As String      'full path required / must be on local computer
    Private _strMsgType As String       'type of famelink message found in the file
    Private _strError As String
    Private _iRecordCount As Integer    'number of messages found in the file based on filesize and message size
    Private _iMessageSize As Integer     'relative size of each message of MsgType
    Private _iFileSize As Long          'the size of the file
    Private _networkuser As String
    Private _networkpassword As String
    Private _datafromremotecomputer As Boolean
    Private _assignFileType As Boolean
    Private _iTotalCount As Integer
    Private _iAwardCount As Integer
    Private _iDisbCount As Integer
    Private _iActualDisbCount As Integer
#End Region

#Region " Public Constructor"
    Public Sub New()
        _iFileSize = 0
        _iRecordCount = 0
        _iMessageSize = 0
        _iTotalCount = 0
        _iAwardCount = 0
        _iDisbCount = 0
        _iActualDisbCount = 0
        _strMsgType = ""
        _strFileName = ""
        _strError = ""
        _networkuser = ""
        _networkpassword = ""
        _datafromremotecomputer = False
    End Sub
#End Region

#Region " Methods"
    Public Function ProcessMessageFile(ByVal strFileName As String) As Boolean
        'the given filename must have an integer number of records.  this is derived
        'by dividing the filesize by the expected record size of any given message type.
        'the number of records in the given file name is returned.
        'three types of user defined errors are considered: 
        'this function assumes that the file is not open
        ProcessMessageFile = False
        _assignFileType = False
        _iRecordCount = 0
        _iFileSize = 0
        If String.IsNullOrEmpty(strFileName) Then Throw New ArgumentNullException("strFileName", "Empty or Null filename provided.")
        Try
            Dim Request As WebRequest
            Dim ResponseStream As Stream
            Dim Response1 As WebResponse
            Dim myCredentials As NetworkCredential
            If datafromremotecomputer = True Then
                myCredentials = New NetworkCredential(_networkuser, _networkpassword, "")
            End If
            Request = System.Net.FileWebRequest.Create(strFileName) 'WebRequest.Create(strFileName) 'System.Net.FileWebRequest.Create("\\qaw2k\ESPFileIn\CLFAHEAD.000") 
            Request.Headers.Add("Translate: f")
            Request.Method = "GET"
            If datafromremotecomputer = True Then
                Request.Credentials = myCredentials
            Else
                Request.Credentials = CredentialCache.DefaultCredentials
            End If

            Response1 = CType(Request.GetResponse, WebResponse)
            ResponseStream = Response1.GetResponseStream
            _iFileSize = Response1.ContentLength
            Dim objIn As StreamReader
            objIn = New StreamReader(ResponseStream, System.Text.Encoding.Default)
            Dim intLoop As Integer = 0
            Dim intAWR As Integer = 0
            Dim intDR As Integer = 0
            Dim contents As String
            Dim strDataRow As String
            Do
                Try
                    contents = objIn.ReadLine().Trim()
                    Dim ArrRecord() As String
                    Dim splitChar As Char = ","
                    If Not contents = "" Then


                        If contents.Substring(0, 1).ToUpper().Contains("T") Or contents.Substring(0, 1).ToUpper().Contains("S") Or contents.Substring(0, 1).ToUpper().Contains("H") Or contents.Substring(0, 1).ToUpper().Contains("C") Then
                            _strMsgType = "PELL"
                            If contents.Length > 1 Then
                                strDataRow = contents.Substring(0, 1).ToUpper()
                                ArrRecord = contents.Split(splitChar)
                                If strDataRow = "T" Then
                                    intAWR = intAWR + 1
                                    _iTotalCount += 1
                                    _iAwardCount += 1
                                    If ArrRecord.Length <> 11 Then
                                        _strError = "File is not in proper Format."
                                    End If
                                ElseIf strDataRow = "S" Then
                                    intDR = intDR + 1
                                    _iTotalCount += 1
                                    _iDisbCount += 1
                                    If ArrRecord.Length <> 9 Then
                                        _strError = "File is not in proper Format."
                                    End If
                                ElseIf strDataRow = "H" Then
                                    intAWR = intAWR + 1
                                    _iTotalCount += 1
                                    _iAwardCount += 1
                                    If ArrRecord.Length <> 10 Then
                                        _strError = "File is not in proper Format."
                                    End If
                                Else
                                    intDR = intDR + 1
                                    _iTotalCount += 1
                                    _iDisbCount += 1
                                    If ArrRecord.Length <> 9 Then
                                        _strError = "File is not in proper Format."
                                    End If
                                End If
                            End If
                        End If
                        If contents.Substring(0, 1).ToUpper().Contains("A") Or contents.Substring(0, 1).ToUpper().Contains("B") Or contents.Substring(0, 1).ToUpper().Contains("D") Or contents.Substring(0, 1).ToUpper().Contains("M") Or contents.Substring(0, 1).ToUpper().Contains("N") Then
                            _strMsgType = "DL"
                            If contents.Length > 1 Or contents.Length > 2 Then
                                strDataRow = contents.Substring(0, 1).ToUpper()
                                ArrRecord = contents.Split(splitChar)
                                If strDataRow = "D" Then
                                    _iTotalCount += 1
                                    _iAwardCount += 1
                                    intAWR = intAWR + 1
                                    If ArrRecord.Length > 3 And Not (ArrRecord.Length = 1 Or ArrRecord.Length = 2) Then
                                        If ArrRecord.Length <> 13 Then
                                            _strError = "File is not in proper Format."
                                        End If
                                    End If
                                ElseIf strDataRow = "M" Then
                                    _iTotalCount += 1
                                    _iActualDisbCount += 1
                                    intAWR = intAWR + 1
                                    If ArrRecord.Length > 3 And Not (ArrRecord.Length = 1 Or ArrRecord.Length = 2) Then
                                        If ArrRecord.Length <> 13 Then
                                            _strError = "File is not in proper Format."
                                        End If
                                    End If
                                ElseIf strDataRow = "N" And Not (ArrRecord.Length = 1 Or ArrRecord.Length = 2) Then
                                    intDR = intDR + 1
                                    _iTotalCount += 1
                                    _iDisbCount += 1
                                    If ArrRecord.Length > 3 Then
                                        If ArrRecord.Length <> 10 Then
                                            _strError = "File is not in proper Format."
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                Catch ex As Exception
                    contents = Nothing
                    Exit Do
                End Try
                intLoop = intLoop + 1
            Loop Until contents Is Nothing
            objIn.Close()
            If _strError <> "" Then
                _strError = intAWR.ToString + ":" + intDR.ToString + ":" + _strError
            End If
            Select Case _strMsgType
                Case "PELL"
                    _iRecordCount = intLoop
                Case "DL"
                    _iRecordCount = intLoop
                Case Else
                    Throw New ArgumentOutOfRangeException("_strMsgType", _strMsgType, "Message type provided is not recognized")
            End Select

            ''ResponseStream.Flush()
            ResponseStream.Close()
            Response1.Close()

        Catch e As ArgumentNullException
            _iRecordCount = 0
            _iFileSize = 0
            _iTotalCount = 0
            _iAwardCount = 0
            _iDisbCount = 0
            _iActualDisbCount = 0
            _strError = e.Message
        Catch e As ArgumentOutOfRangeException
            _iRecordCount = 0
            _iFileSize = 0
            _iTotalCount = 0
            _iAwardCount = 0
            _iDisbCount = 0
            _iActualDisbCount = 0
            _strError = e.Message
        Catch e As System.Exception
            _iRecordCount = 0
            _iFileSize = 0
            _iTotalCount = 0
            _iAwardCount = 0
            _iDisbCount = 0
            _iActualDisbCount = 0
            _strError = e.Message
            Throw
        Finally
            ProcessMessageFile = (_iRecordCount <> 0)
        End Try
    End Function
    Public Function ProcessFileName(ByVal strFileName As String) As Boolean
        'the message type to be processed should be embedded in the name of the given FameLink filename.
        'the type of famelink messages is derived from the file name and message size set.
        'if the message type string is found in the given filename, the expected message size is returned
        'otherwise, a user defined error is set to indicate that the filename does not meet specs.  

        Dim fname As String
        ''  Dim iMsgSize As Integer

        ProcessFileName = False
        If String.IsNullOrEmpty(strFileName) Then Throw New ArgumentNullException("strFileName", "Empty or Null filename provided.")

        Try

            fname = UCase(System.IO.Path.GetFileName(strFileName))
            ProcessFileName = True
            'If (InStr(1, fname, "PELL", CompareMethod.Text) > 0) Then
            '    _strMsgType = "PELL"
            '    ProcessFileName = True
            '    Exit Function
            'End If
            'If (InStr(1, fname, "DL", CompareMethod.Text) > 0) Then
            '    _strMsgType = "DL"
            '    ProcessFileName = True
            '    Exit Function
            'End If
            'Throw New ArgumentOutOfRangeException("strFileName", fname, "Filename does not contain embedded message type")

        Catch e As ArgumentNullException
            _strError = e.Message
        Catch e As ArgumentOutOfRangeException
            _strError = e.Message
        Catch e As System.Exception
            _strError = e.Message
            Throw
        End Try
    End Function
#End Region

#Region "Public Properties"
    Public Property strFileName() As String
        'when the filename property is set, the filename is first processed to determine the type of message
        'then the filelength and record count is verified given the message type's record size
        'finally the file is opened for processing.  Upon error, the filename is cleared and errCode set
        Get
            Return _strFileName
        End Get
        Set(ByVal Value As String)
            Dim blnResult As Boolean = False
            _iFileSize = 0
            _iRecordCount = 0
            _iMessageSize = 0
            _iTotalCount = 0
            _iAwardCount = 0
            _iDisbCount = 0
            _iActualDisbCount = 0
            _strMsgType = ""
            _strFileName = ""
            _strError = ""
            Try
                If String.IsNullOrEmpty(Value) Then Throw New ArgumentNullException("Value", "Empty or Null filename provided.")
                If ProcessFileName(Value) Then
                    _strFileName = Value
                    If Not ProcessMessageFile(_strFileName) Then
                        _iFileSize = 0
                        _iRecordCount = 0
                        _iMessageSize = 0
                        _iTotalCount = 0
                        _iAwardCount = 0
                        _iDisbCount = 0
                        _iActualDisbCount = 0
                        _strMsgType = ""
                    End If
                Else
                    _iFileSize = 0
                    _iRecordCount = 0
                    _iMessageSize = 0
                    _iTotalCount = 0
                    _iAwardCount = 0
                    _iDisbCount = 0
                    _iActualDisbCount = 0
                    _strMsgType = ""
                End If
            Catch e As ArgumentNullException
                _strError = e.Message
            Catch e As ArgumentOutOfRangeException
                _strError = e.Message
            Catch e As System.Exception
                _strError = e.Message
                Throw
            End Try
        End Set
    End Property
    Public ReadOnly Property iFileSize() As Long
        'filesize of the given filename provided in strFileName
        Get
            Return _iFileSize
        End Get
    End Property
    Public Property networkuser() As String
        Get
            Return _networkuser
        End Get
        Set(ByVal value As String)
            _networkuser = value
        End Set
    End Property
    Public Property networkPassword() As String
        Get
            Return _networkpassword
        End Get
        Set(ByVal value As String)
            _networkpassword = value
        End Set
    End Property
    Public Property datafromremotecomputer() As Boolean
        Get
            Return _datafromremotecomputer
        End Get
        Set(ByVal value As Boolean)
            _datafromremotecomputer = value
        End Set
    End Property
    Public ReadOnly Property iRecordCount() As Integer
        'based on the message size of the given message type derived
        'from the given filename, iRecordCount reflects the integer
        'number of records expected in the given filename.  If non-integer
        'value is derived then an error is noted.
        Get
            Return _iRecordCount
        End Get
    End Property
    Public ReadOnly Property iTotalCount() As Integer
        'based on the message size of the given message type derived
        'from the given filename, iRecordCount reflects the integer
        'number of records expected in the given filename.  If non-integer
        'value is derived then an error is noted.
        Get
            Return _iTotalCount
        End Get
    End Property
    Public ReadOnly Property iAwardCount() As Integer
        'based on the message size of the given message type derived
        'from the given filename, iRecordCount reflects the integer
        'number of records expected in the given filename.  If non-integer
        'value is derived then an error is noted.
        Get
            Return _iAwardCount
        End Get
    End Property
    Public ReadOnly Property iDisbCount() As Integer
        'based on the message size of the given message type derived
        'from the given filename, iRecordCount reflects the integer
        'number of records expected in the given filename.  If non-integer
        'value is derived then an error is noted.
        Get
            Return _iDisbCount
        End Get
    End Property
    Public ReadOnly Property iActualDisbCount() As Integer
        'based on the message size of the given message type derived
        'from the given filename, iRecordCount reflects the integer
        'number of records expected in the given filename.  If non-integer
        'value is derived then an error is noted.
        Get
            Return _iActualDisbCount
        End Get
    End Property
    Public ReadOnly Property iMessageSize() As Integer
        'number of bytes for the given derived message type
        Get
            Return _iMessageSize
        End Get
    End Property
    Public ReadOnly Property strMsgType() As String
        'message type for the given filename.  six are currently supported.
        Get
            Return _strMsgType
        End Get
    End Property
    Public ReadOnly Property strError() As String
        'upon trapped error an related string error is provide.
        Get
            Return _strError
        End Get
    End Property
#End Region
End Class