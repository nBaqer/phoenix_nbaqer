﻿Namespace smtp
    Public Class SmtpSendMessageInputModel
        Public Property Body64 As String

        Public Property Subject As String

        Public Property [To] As String

        Public Property Cc As String

        Public Property From As String

        ''' <summary>
        ''' Leave blank for plain text
        ''' Use "text/html" to send html aware message
        ''' </summary>
        Public Property ContentType As String

        ''' <summary>
        ''' 1: Send emails without Attachments
        ''' 2: Test if OAuth is configured
        ''' (other options not implemented)
        ''' </summary>
        Public Property Command As Integer
    End Class
End Namespace
