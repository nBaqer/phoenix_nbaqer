﻿Namespace smtp
    ''' <summary>
    ''' Use this to generic output in controllers
    ''' </summary>
    Public Interface IGenericOutput
        Property IsPassed() As Integer
        Property Note() As String
    End Interface
End Namespace