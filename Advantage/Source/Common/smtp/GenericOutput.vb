﻿Namespace smtp
    ''' <summary>
    ''' Use this to generic output in controllers
    ''' </summary>
    Public Class GenericOutput
        Implements IGenericOutput
        Public Shared Function Factory() As IGenericOutput
            Dim ig = New GenericOutput()
            Return ig
        End Function


        Public Property Note As String Implements IGenericOutput.Note
        Public Property IsPassed As Integer Implements IGenericOutput.IsPassed

    End Class
End Namespace
