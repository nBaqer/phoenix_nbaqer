' ===============================================================================
' FAME.AdvantageV1.BusinessEntities
'
' Employees.vb
'
' Employees Business Entities Logic. 
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
<Serializable()>
Public Class EmployeeContactInfo
    '
    '   EmployeeContactInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _empContactInfoId As String
    Private _empId As String
    Private _workPhone As String
    Private _homePhone As String
    Private _cellPhone As String
    Private _beeper As String
    Private _workEmail As String
    Private _homeEmail As String
    Private _ForeignHomephone As Boolean
    Private _ForeignWorkphone As Boolean
    Private _ForeignCellphone As Boolean
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors"
    Public Sub New()
        _isInDB = False
        _empContactInfoId = Guid.NewGuid.ToString
        _empId = Guid.Empty.ToString
        _workPhone = ""
        _homePhone = ""
        _cellPhone = ""
        _beeper = ""
        _workEmail = ""
        _homeEmail = ""
        _ForeignHomephone = False
        _ForeignCellphone = False
        _ForeignWorkphone = False
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
    Public Sub New(ByVal empId As String)
        _isInDB = False
        _empContactInfoId = Guid.NewGuid.ToString
        _empId = empId
        _workPhone = ""
        _homePhone = ""
        _cellPhone = ""
        _beeper = ""
        _workEmail = ""
        _homeEmail = ""
        _ForeignHomephone = False
        _ForeignCellphone = False
        _ForeignWorkphone = False
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property EmpContactInfoId() As String
        Get
            Return _empContactInfoId
        End Get
        Set(ByVal Value As String)
            _empContactInfoId = Value
        End Set
    End Property
    Public Property EmpId() As String
        Get
            Return _empId
        End Get
        Set(ByVal Value As String)
            _empId = Value
        End Set
    End Property
    Public Property ForeignHomePhone() As Boolean
        Get
            Return _ForeignHomephone
        End Get
        Set(ByVal Value As Boolean)
            _ForeignHomephone = Value
        End Set
    End Property
    Public Property ForeignCellPhone() As Boolean
        Get
            Return _ForeignCellphone
        End Get
        Set(ByVal Value As Boolean)
            _ForeignCellphone = Value
        End Set
    End Property
    Public Property ForeignWorkPhone() As Boolean
        Get
            Return _ForeignWorkphone
        End Get
        Set(ByVal Value As Boolean)
            _ForeignWorkphone = Value
        End Set
    End Property
    Public Property WorkPhone() As String
        Get
            Return _workPhone
        End Get
        Set(ByVal Value As String)
            _workPhone = Value
        End Set
    End Property
    Public Property HomePhone() As String
        Get
            Return _homePhone
        End Get
        Set(ByVal Value As String)
            _homePhone = Value
        End Set
    End Property
    Public Property CellPhone() As String
        Get
            Return _cellPhone
        End Get
        Set(ByVal Value As String)
            _cellPhone = Value
        End Set
    End Property
    Public Property Beeper() As String
        Get
            Return _beeper
        End Get
        Set(ByVal Value As String)
            _beeper = Value
        End Set
    End Property
    Public Property WorkEmail() As String
        Get
            Return _workEmail
        End Get
        Set(ByVal Value As String)
            _workEmail = Value
        End Set
    End Property
    Public Property HomeEmail() As String
        Get
            Return _homeEmail
        End Get
        Set(ByVal Value As String)
            _homeEmail = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
<Serializable()>
Public Class EmployeeInfo
    '
    '   EmployeeInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _empId As String
    Private _status As String
    Private _statusId As String
    Private _lastName As String
    Private _firstName As String
    Private _mI As String
    Private _prefix As String
    Private _prefixId As String
    Private _suffix As String
    Private _suffixId As String
    Private _sSN As String
    Private _iD As String
    Private _birthDate As Date
    Private _race As String
    Private _raceId As String
    Private _gender As String
    Private _genderId As String
    Private _maritalStatus As String
    Private _maritalStatId As String
    Private _address1 As String
    Private _address2 As String
    Private _city As String
    Private _stateId As String
    Private _state As String
    Private _zip As String
    Private _countryId As String
    Private _country As String
    Private _foreignzip As Boolean
    Private _otherstate As String
    Private _campusId As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors"
    Public Sub New()
        _isInDB = False
        _empId = Guid.NewGuid.ToString
        _status = ""
        _statusId = Guid.Empty.ToString
        _lastName = ""
        _firstName = ""
        _mI = ""
        _prefix = ""
        _prefixId = Guid.Empty.ToString
        _suffix = ""
        _suffixId = Guid.Empty.ToString
        _sSN = ""
        _iD = ""
        _birthDate = Date.MaxValue
        _race = ""
        _raceId = Guid.Empty.ToString
        _gender = ""
        _genderId = Guid.Empty.ToString
        _maritalStatus = ""
        _maritalStatId = Guid.Empty.ToString
        _address1 = ""
        _address2 = ""
        _city = ""
        _stateId = Guid.Empty.ToString
        _state = ""
        _zip = ""
        _countryId = Guid.Empty.ToString
        _country = ""
        _foreignzip = False
        _otherstate = ""
        _campusId = Guid.Empty.ToString
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
    Public Sub New(ByVal empId As String)
        _isInDB = False
        _empId = empId
        _status = ""
        _statusId = Guid.Empty.ToString
        _lastName = ""
        _firstName = ""
        _mI = ""
        _prefix = ""
        _prefixId = Guid.Empty.ToString
        _suffix = ""
        _suffixId = Guid.Empty.ToString
        _sSN = ""
        _iD = ""
        _birthDate = Date.MaxValue
        _race = ""
        _raceId = Guid.Empty.ToString
        _gender = ""
        _genderId = Guid.Empty.ToString
        _maritalStatus = ""
        _maritalStatId = Guid.Empty.ToString
        _address1 = ""
        _address2 = ""
        _city = ""
        _stateId = Guid.Empty.ToString
        _state = ""
        _zip = ""
        _countryId = Guid.Empty.ToString
        _country = ""
        _foreignzip = False
        _otherstate = ""
        _campusId = Guid.Empty.ToString
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property ForeignZip() As Boolean
        Get
            Return _foreignzip
        End Get
        Set(ByVal Value As Boolean)
            _foreignzip = Value
        End Set
    End Property
    Public Property OtherState() As String
        Get
            Return _otherstate
        End Get
        Set(ByVal Value As String)
            _otherstate = Value
        End Set
    End Property
    Public Property EmpId() As String
        Get
            Return _empId
        End Get
        Set(ByVal Value As String)
            _empId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return _lastName
        End Get
        Set(ByVal Value As String)
            _lastName = Value
        End Set
    End Property
    Public Property FirstName() As String
        Get
            Return _firstName
        End Get
        Set(ByVal Value As String)
            _firstName = Value
        End Set
    End Property
    Public Property MI() As String
        Get
            Return _mI
        End Get
        Set(ByVal Value As String)
            _mI = Value
        End Set
    End Property
    Public Property Prefix() As String
        Get
            Return _prefix
        End Get
        Set(ByVal Value As String)
            _prefix = Value
        End Set
    End Property
    Public Property PrefixId() As String
        Get
            Return _prefixId
        End Get
        Set(ByVal Value As String)
            _prefixId = Value
        End Set
    End Property
    Public Property Suffix() As String
        Get
            Return _suffix
        End Get
        Set(ByVal Value As String)
            _suffix = Value
        End Set
    End Property
    Public Property SuffixId() As String
        Get
            Return _suffixId
        End Get
        Set(ByVal Value As String)
            _suffixId = Value
        End Set
    End Property
    Public Property SSN() As String
        Get
            Return _sSN
        End Get
        Set(ByVal Value As String)
            _sSN = Value
        End Set
    End Property
    Public Property ID() As String
        Get
            Return _iD
        End Get
        Set(ByVal Value As String)
            _iD = Value
        End Set
    End Property
    Public Property BirthDate() As Date
        Get
            Return _birthDate
        End Get
        Set(ByVal Value As Date)
            _birthDate = Value
        End Set
    End Property
    Public Property Race() As String
        Get
            Return _race
        End Get
        Set(ByVal Value As String)
            _race = Value
        End Set
    End Property
    Public Property RaceId() As String
        Get
            Return _raceId
        End Get
        Set(ByVal Value As String)
            _raceId = Value
        End Set
    End Property
    Public Property Gender() As String
        Get
            Return _gender
        End Get
        Set(ByVal Value As String)
            _gender = Value
        End Set
    End Property
    Public Property GenderId() As String
        Get
            Return _genderId
        End Get
        Set(ByVal Value As String)
            _genderId = Value
        End Set
    End Property
    Public Property MaritalStat() As String
        Get
            Return _maritalStatus
        End Get
        Set(ByVal Value As String)
            _maritalStatus = Value
        End Set
    End Property
    Public Property MaritalStatId() As String
        Get
            Return _maritalStatId
        End Get
        Set(ByVal Value As String)
            _maritalStatId = Value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return _address1
        End Get
        Set(ByVal Value As String)
            _address1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return _address2
        End Get
        Set(ByVal Value As String)
            _address2 = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _city
        End Get
        Set(ByVal Value As String)
            _city = Value
        End Set
    End Property
    Public Property StateId() As String
        Get
            Return _stateId
        End Get
        Set(ByVal Value As String)
            _stateId = Value
        End Set
    End Property
    Public Property State() As String
        Get
            Return _state
        End Get
        Set(ByVal Value As String)
            _state = Value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return _zip
        End Get
        Set(ByVal Value As String)
            _zip = Value
        End Set
    End Property
    Public Property CountryId() As String
        Get
            Return _countryId
        End Get
        Set(ByVal Value As String)
            _countryId = Value
        End Set
    End Property
    Public Property Country() As String
        Get
            Return _country
        End Get
        Set(ByVal Value As String)
            _country = Value
        End Set
    End Property
    Public Property CampusId() As String
        Get
            Return _campusId
        End Get
        Set(ByVal Value As String)
            _campusId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
#Region "Public Methods"
    Public Shared Function IsDOBValid(ByVal txt As String) As Boolean
        If Date.Parse(txt).AddYears(AdvantageCommonValues.MinimumAcceptableEmployeeAge).CompareTo(Date.Now) < 0 Then Return True Else Return False
    End Function
    Public Shared Function IsDOBValid(ByVal dat As Date) As Boolean
        If dat.AddYears(AdvantageCommonValues.MinimumAcceptableEmployeeAge).CompareTo(Date.Now) < 0 Then Return True Else Return False
    End Function
#End Region
End Class
<Serializable()>
Public Class EmployeeHRInfo
    '
    '   EmployeeHRInfo Class   
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _empHRInfoId As String
    Private _empId As String
    Private _hireDate As Date
    Private _positionId As String
    Private _position As String
    Private _departmentId As String
    Private _department As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors"
    Public Sub New()
        _isInDB = False
        _empHRInfoId = Guid.NewGuid.ToString
        _empId = Guid.Empty.ToString
        _hireDate = Date.MaxValue
        _positionId = Guid.Empty.ToString
        _position = ""
        _departmentId = Guid.Empty.ToString
        _department = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
    Public Sub New(ByVal empId As String)
        _isInDB = False
        _empHRInfoId = Guid.NewGuid.ToString
        _empId = empId
        _hireDate = Date.MaxValue
        _positionId = Guid.Empty.ToString
        _position = ""
        _departmentId = Guid.Empty.ToString
        _department = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property EmpHRInfoId() As String
        Get
            Return _empHRInfoId
        End Get
        Set(ByVal Value As String)
            _empHRInfoId = Value
        End Set
    End Property
    Public Property EmpId() As String
        Get
            Return _empId
        End Get
        Set(ByVal Value As String)
            _empId = Value
        End Set
    End Property
    Public Property HireDate() As Date
        Get
            Return _hireDate
        End Get
        Set(ByVal Value As Date)
            _hireDate = Value
        End Set
    End Property
    Public Property PositionId() As String
        Get
            Return _positionId
        End Get
        Set(ByVal Value As String)
            _positionId = Value
        End Set
    End Property
    Public Property Position() As String
        Get
            Return _position
        End Get
        Set(ByVal Value As String)
            _position = Value
        End Set
    End Property
    Public Property DepartmentId() As String
        Get
            Return _departmentId
        End Get
        Set(ByVal Value As String)
            _departmentId = Value
        End Set
    End Property
    Public Property Department() As String
        Get
            Return _department
        End Get
        Set(ByVal Value As String)
            _department = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
<Serializable()>
Public Class EmployeeUserInfo
    '
    '   EmployeeUserInfo class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _empUserInfoId As String
    Private _empId As String
    Private _userName As String
    Private _password As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors"
    Public Sub New()
        _isInDB = False
        _empUserInfoId = Guid.NewGuid.ToString
        _empId = Guid.Empty.ToString
        _userName = ""
        _password = ""
        _campGrpId = Guid.Empty.ToString
        _campGrpDescrip = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
    Public Sub New(ByVal empId As String)
        _isInDB = False
        _empUserInfoId = Guid.NewGuid.ToString
        _empId = empId
        _userName = ""
        _password = ""
        _campGrpId = Guid.Empty.ToString
        _campGrpDescrip = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property EmpUserInfoId() As String
        Get
            Return _empUserInfoId
        End Get
        Set(ByVal Value As String)
            _empUserInfoId = Value
        End Set
    End Property
    Public Property EmpId() As String
        Get
            Return _empId
        End Get
        Set(ByVal Value As String)
            _empId = Value
        End Set
    End Property
    Public Property UserName() As String
        Get
            Return _userName
        End Get
        Set(ByVal Value As String)
            _userName = Value
        End Set
    End Property
    Public Property Password() As String
        Get
            Return _password
        End Get
        Set(ByVal Value As String)
            _password = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
Public Class EmployeeEmergencyContactInfo
    '
    '   EmployeeEmergencyContactInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _EmployeeEmergencyContactId As String
    Private _name As String
    Private _statusId As String
    Private _status As String
    Private _empId As String
    Private _relationId As String
    Private _relation As String
    Private _workPhone As String
    Private _homePhone As String
    Private _cellPhone As String
    Private _beeper As String
    Private _workEmail As String
    Private _homeEmail As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _ForeignHomePhone As Boolean
    Private _ForeignCellPhone As Boolean
    Private _ForeignWorkPhone As Boolean
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _EmployeeEmergencyContactId = Guid.NewGuid.ToString
        _name = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _empId = Guid.Empty.ToString
        _relationId = Guid.Empty.ToString
        _relation = "Select"
        _workPhone = ""
        _homePhone = ""
        _cellPhone = ""
        _beeper = ""
        _workEmail = ""
        _homeEmail = ""
        _modUser = ""
        _modDate = Date.MinValue
        _ForeignHomePhone = False
        _ForeignCellPhone = False
        _ForeignWorkPhone = False
    End Sub
    Public Sub New(ByVal empId As String)
        _isInDB = False
        _EmployeeEmergencyContactId = Guid.NewGuid.ToString
        _name = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _empId = empId
        _relationId = Guid.Empty.ToString
        _relation = "Select"
        _workPhone = ""
        _homePhone = ""
        _cellPhone = ""
        _beeper = ""
        _workEmail = ""
        _homeEmail = ""
        _modUser = ""
        _modDate = Date.MinValue
        _ForeignHomePhone = False
        _ForeignCellPhone = False
        _ForeignWorkPhone = False
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property EmployeeEmergencyContactId() As String
        Get
            Return _EmployeeEmergencyContactId
        End Get
        Set(ByVal Value As String)
            _EmployeeEmergencyContactId = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal Value As String)
            _name = Value
        End Set
    End Property
    Public Property ForeignHomePhone() As Boolean
        Get
            Return _ForeignHomePhone
        End Get
        Set(ByVal Value As Boolean)
            _ForeignHomePhone = Value
        End Set
    End Property
    Public Property ForeignWorkPhone() As Boolean
        Get
            Return _ForeignWorkPhone
        End Get
        Set(ByVal Value As Boolean)
            _ForeignWorkPhone = Value
        End Set
    End Property
    Public Property ForeignCellPhone() As Boolean
        Get
            Return _ForeignCellPhone
        End Get
        Set(ByVal Value As Boolean)
            _ForeignCellPhone = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property EmpId() As String
        Get
            Return _empId
        End Get
        Set(ByVal Value As String)
            _empId = Value
        End Set
    End Property
    Public Property Relation() As String
        Get
            Return _relation
        End Get
        Set(ByVal Value As String)
            _relation = Value
        End Set
    End Property
    Public Property RelationId() As String
        Get
            Return _relationId
        End Get
        Set(ByVal Value As String)
            _relationId = Value
        End Set
    End Property
    Public Property WorkPhone() As String
        Get
            Return _workPhone
        End Get
        Set(ByVal Value As String)
            _workPhone = Value
        End Set
    End Property
    Public Property HomePhone() As String
        Get
            Return _homePhone
        End Get
        Set(ByVal Value As String)
            _homePhone = Value
        End Set
    End Property
    Public Property CellPhone() As String
        Get
            Return _cellPhone
        End Get
        Set(ByVal Value As String)
            _cellPhone = Value
        End Set
    End Property
    Public Property Beeper() As String
        Get
            Return _beeper
        End Get
        Set(ByVal Value As String)
            _beeper = Value
        End Set
    End Property
    Public Property WorkEmail() As String
        Get
            Return _workEmail
        End Get
        Set(ByVal Value As String)
            _workEmail = Value
        End Set
    End Property
    Public Property HomeEmail() As String
        Get
            Return _homeEmail
        End Get
        Set(ByVal Value As String)
            _homeEmail = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region
End Class
Public Enum Relationship
    Aunt = 1
    Brother = 2
    Cousin = 3
    Father = 4
    Husband = 5
    Mother = 6
    Sister = 7
    Uncle = 8
    Wife = 9
    Other = 10
    [Friend] = 11
    Ex_Spouse = 12
    Girlfriend = 13
    Grandmother = 14
    Landlord = 15
    Niece = 16
    Partner = 17
    Roommate = 18
    Son = 19
    Spouse = 20
    In_Law = 21
    Brother_In_Law = 22
    Daughter = 23
    Best_Friend = 24
    Sister_In_Law = 25
    Boyfriend = 26
    Employer = 27

End Enum
Public Class PositionInfo
    '
    '   PositionInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _PositionId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _PositionId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property PositionId() As String
        Get
            Return _PositionId
        End Get
        Set(ByVal Value As String)
            _PositionId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Class HRDepartmentInfo
    '
    '   HRDepartmentInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _HRDepartmentId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _HRDepartmentId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property HRDepartmentId() As String
        Get
            Return _HRDepartmentId
        End Get
        Set(ByVal Value As String)
            _HRDepartmentId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class
Public Class CertificationInfo
    '
    '   CertificationInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _CertificationId As String
    Private _code As String
    Private _statusId As String
    Private _status As String
    Private _description As String
    Private _campGrpId As String
    Private _campGrpDescrip As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _CertificationId = Guid.NewGuid.ToString
        _code = ""
        _statusId = Guid.Empty.ToString
        _status = ""
        _description = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property CertificationId() As String
        Get
            Return _CertificationId
        End Get
        Set(ByVal Value As String)
            _CertificationId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal Value As String)
            _code = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _statusId
        End Get
        Set(ByVal Value As String)
            _statusId = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CampGrpId() As String
        Get
            Return _campGrpId
        End Get
        Set(ByVal Value As String)
            _campGrpId = Value
        End Set
    End Property
    Public Property CampGrpDescrip() As String
        Get
            Return _campGrpDescrip
        End Get
        Set(ByVal Value As String)
            _campGrpDescrip = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class


