Public Class BuildingInfo
#Region "txtStudentPhoneIdVariables and Objects"
    Private _isInDB As Boolean
    Private _BldgId As String
    Private _StatusId As String
    Private _CampGrpId As String
    Private _CampGrp As String
    Private _CampusId As String
    Private _Campus As String
    Private _Code As String
    Private _Descrip As String
    Private _NoOfRooms As Integer
    Private _Address1 As String
    Private _Address2 As String
    Private _City As String
    Private _State As String
    Private _zip As String
    Private _Country As String
    Private _Name As String
    Private _Title As String
    Private _Email As String
    Private _Comments As String
    Private _BldgOpen As String
    Private _BldgClose As String
    Private _Sun As Integer
    Private _Mon As Integer
    Private _Tue As Integer
    Private _Wed As Integer
    Private _Thur As Integer
    Private _Fri As Integer
    Private _Sat As Integer
    Private _ForeignPhone As Integer
    Private _ForeignFax As Integer
    Private _ForeignZip As Integer
    Private _OtherState As String
    Private _StateId As String
    Private _modUser As String
    Private _modDate As DateTime
    Private _Default1 As Integer
    Private _Phone As String
    Private _Fax As String
#End Region

#Region " Public Constructor"
    Public Sub New()

        _isInDB = False
        _StatusId = Guid.Empty.ToString
        _CampGrpId = Guid.Empty.ToString
        _CampusId = Guid.Empty.ToString
        _Code = ""
        _Descrip = ""
        _NoOfRooms = 0
        _Name = ""
        _Title = ""
        _Email = ""
        _Comments = ""
        _BldgOpen = ""
        _BldgClose = ""
        _Sun = 0
        _Mon = 0
        _Tue = 0
        _Wed = 0
        _Thur = 0
        _Fri = 0
        _Sat = 0
        _Address1 = ""
        _Address2 = ""
        _City = ""
        _State = ""
        _zip = ""
        _Phone = ""
        _Fax = ""
        _BldgId = Guid.NewGuid.ToString()

        _ForeignZip = 0
        _ForeignPhone = 0
        _ForeignFax = 0
        _Default1 = 0
        _OtherState = ""
        _StateId = Guid.Empty.ToString()
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region "Public Properties"
    Public Property StateId() As String
        Get
            Return _StateId
        End Get
        Set(ByVal Value As String)
            _StateId = Value
        End Set
    End Property

    Public Property Default1() As Integer
        Get
            Return _Default1
        End Get
        Set(ByVal Value As Integer)
            _Default1 = Value
        End Set
    End Property
    Public Property ForeignZip() As Integer
        Get
            Return _ForeignZip
        End Get
        Set(ByVal Value As Integer)
            _ForeignZip = Value
        End Set
    End Property
    Public Property ForeignPhone() As Integer
        Get
            Return _ForeignPhone
        End Get
        Set(ByVal Value As Integer)
            _ForeignPhone = Value
        End Set
    End Property
    Public Property ForeignFax() As Integer
        Get
            Return _ForeignFax
        End Get
        Set(ByVal Value As Integer)
            _ForeignFax = Value
        End Set
    End Property
    Public Property OtherState() As String
        Get
            Return _OtherState
        End Get
        Set(ByVal Value As String)
            _OtherState = Value
        End Set
    End Property
    Public Property Sun() As Integer
        Get
            Return _Sun
        End Get
        Set(ByVal Value As Integer)
            _Sun = Value
        End Set
    End Property
    Public Property Mon() As Integer
        Get
            Return _Mon
        End Get
        Set(ByVal Value As Integer)
            _Mon = Value
        End Set
    End Property
    Public Property Tue() As Integer
        Get
            Return _Tue
        End Get
        Set(ByVal Value As Integer)
            _Tue = Value
        End Set
    End Property
    Public Property Wed() As Integer
        Get
            Return _Wed
        End Get
        Set(ByVal Value As Integer)
            _Wed = Value
        End Set
    End Property
    Public Property Thur() As Integer
        Get
            Return _Thur
        End Get
        Set(ByVal Value As Integer)
            _Thur = Value
        End Set
    End Property
    Public Property Fri() As Integer
        Get
            Return _Fri
        End Get
        Set(ByVal Value As Integer)
            _Fri = Value
        End Set
    End Property
    Public Property Sat() As Integer
        Get
            Return _Sat
        End Get
        Set(ByVal Value As Integer)
            _Sat = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property

    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property NoOfRooms() As Integer
        Get
            Return _NoOfRooms
        End Get
        Set(ByVal Value As Integer)
            _NoOfRooms = Value
        End Set
    End Property

    Public Property Address1() As String
        Get
            Return _Address1
        End Get
        Set(ByVal Value As String)
            _Address1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return _Address2
        End Get
        Set(ByVal Value As String)
            _Address2 = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _Code
        End Get
        Set(ByVal Value As String)
            _Code = Value
        End Set
    End Property
    Public Property Descrip() As String
        Get
            Return _Descrip
        End Get
        Set(ByVal Value As String)
            _Descrip = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal Value As String)
            _Name = Value
        End Set
    End Property
    Public Property Title() As String
        Get
            Return _Title
        End Get
        Set(ByVal Value As String)
            _Title = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal Value As String)
            _Email = Value
        End Set
    End Property
    Public Property Comments() As String
        Get
            Return _Comments
        End Get
        Set(ByVal Value As String)
            _Comments = Value
        End Set
    End Property
    Public Property BldgOpen() As String
        Get
            Return _BldgOpen
        End Get
        Set(ByVal Value As String)
            _BldgOpen = Value
        End Set
    End Property
    Public Property BldgClose() As String
        Get
            Return _BldgClose
        End Get
        Set(ByVal Value As String)
            _BldgClose = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _City
        End Get
        Set(ByVal Value As String)
            _City = Value
        End Set
    End Property
    Public Property State() As String
        Get
            Return _State
        End Get
        Set(ByVal Value As String)
            _State = Value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return _zip
        End Get
        Set(ByVal Value As String)
            _zip = Value
        End Set
    End Property

    Public Property CampGrpId() As String
        Get
            Return _CampGrpId
        End Get
        Set(ByVal Value As String)
            _CampGrpId = Value
        End Set
    End Property
    Public Property CampGrp() As String
        Get
            Return _CampGrp
        End Get
        Set(ByVal Value As String)
            _CampGrp = Value
        End Set
    End Property
    Public Property CampusId() As String
        Get
            Return _CampusId
        End Get
        Set(ByVal Value As String)
            _CampusId = Value
        End Set
    End Property
    Public Property Campus() As String
        Get
            Return _Campus
        End Get
        Set(ByVal Value As String)
            _Campus = Value
        End Set
    End Property

    Public Property StatusId() As String
        Get
            Return _StatusId
        End Get
        Set(ByVal Value As String)
            _StatusId = Value
        End Set
    End Property
    Public Property BldgId() As String
        Get
            Return _BldgId
        End Get
        Set(ByVal Value As String)
            _BldgId = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property Phone() As String
        Get
            Return _Phone
        End Get
        Set(ByVal Value As String)
            _Phone = Value
        End Set
    End Property
    Public Property Fax() As String
        Get
            Return _Fax
        End Get
        Set(ByVal Value As String)
            _Fax = Value
        End Set
    End Property
#End Region
End Class
