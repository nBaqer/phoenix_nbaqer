﻿Public Class AdvStagingPaymentPeriodsV1
    Public Property AdvStagingPaymentPeriodID As Long
    Public Property LocationCMSID As String
    Public Property IDStudent As Long
    Public Property SISEnrollmentID As Guid

    Public Property AcadYearSeqNo As Integer

    Public Property PaymentPeriodName As String

    Public Property StartDate As DateTime

    Public Property EndDate As DateTime

    Public Property WeeksOfInstructionalTime As Decimal

    Public Property HoursCreditEnrolled As Decimal

    Public Property EnrollmentStatusDescription As String

    Public Property HoursCreditEarned As Decimal

    Public Property EffectiveDate As DateTime

    Public Property SAP As String

    Public Property Deleted As Boolean
    Public Property Processed As Boolean

    Public Property SessionKey As String
    Public Property DateCreated As DateTime

    Public Property UserCreated As String

    Public Property DateUpdated As DateTime?
    Public Property UserUpdated As String

End Class
