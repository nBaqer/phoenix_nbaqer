Public Class PlacementHistoryInfo
#Region "Declare Variables"
    Private _isInDB As Boolean
    Private _PlacementId As String
    Private _Supervisor As String
    Private _Fee As String
    Private _StartDate As String
    Private _Salary As String
    Private _TerminationReason As String
    Private _TerminationDate As String
    Private _Notes As String
    Private _JobTitleId As String
    Private _JobTypeId As String
    Private _Workdays As String
    Private _BenefitsId As String
    Private _ScheduleId As String
    Private _JobDescrip As String
    Private _HowPlaced As String
    Private _PlacementRep As String
    Private _Interview As String
    Private _Reason As String
    Private _Employer As String
    Private _StudentName As String
    Private _SSN As String
    Private _JobStatusId As String
    Private _JobStatus As String
    Private _firstname As String
    Private _lastname As String
    Private _middlename As String
    Private _SalaryType As String
    Private _PlacedDate As String
    Private _Enrollment As String
    Private _FldStudy As String
#End Region
#Region " Constructors"
    Public Sub New()
        _isInDB = False
        _PlacementId = ""
        _Interview = ""
        _Supervisor = ""
        _Fee = Guid.Empty.ToString
        _StartDate = ""
        _Salary = ""
        _TerminationReason = ""
        _TerminationDate = ""
        _Notes = ""
        _JobTitleId = Guid.Empty.ToString
        _JobTypeId = Guid.Empty.ToString
        _Workdays = ""
        _BenefitsId = Guid.Empty.ToString
        _ScheduleId = Guid.Empty.ToString
        _JobDescrip = ""
        _HowPlaced = Guid.Empty.ToString
        _PlacementRep = ""
        _Reason = ""
        _Employer = ""
        _StudentName = ""
        _SSN = ""
        _JobStatusId = Guid.Empty.ToString
        _JobStatus = ""
        _firstname = ""
        _middlename = ""
        _lastname = ""
        _SalaryType = ""
        _FldStudy = ""
        _PlacedDate = ""
        _Enrollment = ""
    End Sub
#End Region

#Region "Property"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property PlacementId() As String
        Get
            Return _PlacementId
        End Get
        Set(ByVal Value As String)
            _PlacementId = Value
        End Set
    End Property
    Public Property SalaryType() As String
        Get
            Return _SalaryType
        End Get
        Set(ByVal Value As String)
            _SalaryType = Value
        End Set
    End Property
    Public Property PlacedDate() As String
        Get
            Return _PlacedDate
        End Get
        Set(ByVal Value As String)
            _PlacedDate = Value
        End Set
    End Property
    Public Property Enrollment() As String
        Get
            Return _Enrollment
        End Get
        Set(ByVal Value As String)
            _Enrollment = Value
        End Set
    End Property
    Public Property FldStudy() As String
        Get
            Return _FldStudy
        End Get
        Set(ByVal Value As String)
            _FldStudy = Value
        End Set
    End Property
    Public Property Interview() As String
        Get
            Return _Interview
        End Get
        Set(ByVal Value As String)
            _Interview = Value
        End Set
    End Property
    Public Property FirstName() As String
        Get
            Return _firstname
        End Get
        Set(ByVal Value As String)
            _firstname = Value
        End Set
    End Property
    Public Property MiddleName() As String
        Get
            Return _middlename
        End Get
        Set(ByVal Value As String)
            _middlename = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return _lastname
        End Get
        Set(ByVal Value As String)
            _lastname = Value
        End Set
    End Property
    Public Property Supervisor() As String
        Get
            Return _Supervisor
        End Get
        Set(ByVal Value As String)
            _Supervisor = Value
        End Set
    End Property
    Public Property Fee() As String
        Get
            Return _Fee
        End Get
        Set(ByVal Value As String)
            _Fee = Value
        End Set
    End Property
    Public Property StartDate() As String
        Get
            Return _StartDate
        End Get
        Set(ByVal Value As String)
            _StartDate = Value
        End Set
    End Property
    Public Property Salary() As String
        Get
            Return _Salary
        End Get
        Set(ByVal Value As String)
            _Salary = Value
        End Set
    End Property
    Public Property JobStatus() As String
        Get
            Return _JobStatus
        End Get
        Set(ByVal Value As String)
            _JobStatus = Value
        End Set
    End Property
    Public Property TerminationReason() As String
        Get
            Return _TerminationReason
        End Get
        Set(ByVal Value As String)
            _TerminationReason = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property


    Public Property TerminationDate() As String
        Get
            Return _TerminationDate
        End Get
        Set(ByVal Value As String)
            _TerminationDate = Value
        End Set
    End Property
    Public Property JobTypeId() As String
        Get
            Return _JobTypeId
        End Get
        Set(ByVal Value As String)
            _JobTypeId = Value
        End Set
    End Property
    Public Property JobTitleId() As String
        Get
            Return _JobTitleId
        End Get
        Set(ByVal Value As String)
            _JobTitleId = Value
        End Set
    End Property

    Public Property BenefitsId() As String
        Get
            Return _BenefitsId
        End Get
        Set(ByVal Value As String)
            _BenefitsId = Value
        End Set
    End Property
    Public Property ScheduleId() As String
        Get
            Return _ScheduleId
        End Get
        Set(ByVal Value As String)
            _ScheduleId = Value
        End Set
    End Property
    Public Property HowPlaced() As String
        Get
            Return _HowPlaced
        End Get
        Set(ByVal Value As String)
            _HowPlaced = Value
        End Set
    End Property
    Public Property PlacementRep() As String
        Get
            Return _PlacementRep
        End Get
        Set(ByVal Value As String)
            _PlacementRep = Value
        End Set
    End Property
    Public Property Employer() As String
        Get
            Return _Employer
        End Get
        Set(ByVal Value As String)
            _Employer = Value
        End Set
    End Property

    Public Property StudentName() As String
        Get
            Return _StudentName
        End Get
        Set(ByVal Value As String)
            _StudentName = Value
        End Set
    End Property
    Public Property SSN() As String
        Get
            Return _SSN
        End Get
        Set(ByVal Value As String)
            _SSN = Value
        End Set
    End Property
    Public Property JobStatusId() As String
        Get
            Return _JobStatusId
        End Get
        Set(ByVal Value As String)
            _JobStatusId = Value
        End Set
    End Property
    Public Property JobDescrip() As String
        Get
            Return _JobDescrip
        End Get
        Set(ByVal Value As String)
            _JobDescrip = Value
        End Set
    End Property
#End Region
End Class
