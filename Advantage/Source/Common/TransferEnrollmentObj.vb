Imports System.Data.SqlClient

''' <summary>
''' This class hold the information necessary to create a enrollment and transfer out from the other
''' </summary>
''' <remarks></remarks>
Public Class TransferEnrollmentObj
    Property StuEnrollId() As String
    Property StudentID() As String
    Property LeadId() As String
    Property CampusId() As String
    Property PrgVerId() As String
    Property User() As String
    Property SourcePrgVerId() As String
    Property EnrollDate() As String
    Property ExpStartDate() As String
    Property ExpGradDate() As String
    Property EnrollId() As String
    Property SourceCampusId() As String
    Property ShiftId() As String
    '' DE 12167  -- The column ScheduleId was removed from arStuEnrollments table so we don't need this information
    '' Property ScheduleID() As String
    Property TransferHours() As Decimal

    Property OldStuEnrollId() As String
    Property SchoolGuidCurrentState As String
    Property SchoolGuidForTransferOut() As String
    Property SchoolGuidForFutureStart() As String




End Class