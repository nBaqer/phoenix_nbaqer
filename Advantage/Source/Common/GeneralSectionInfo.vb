Public Class GeneralSectionInfo
    '
    '   GeneralSectionInfo Class
    '
#Region " Private Variables and Objects"
    Private _isInDB As Boolean
    Private _GeneralSectionID As String
    Private _ModuleID As Integer
    Private _Description As String
    Private _modUser As String
    Private _modDate As DateTime
#End Region
#Region " Public Constructors "
    Public Sub New()
        _isInDB = False
        _GeneralSectionID = Guid.NewGuid.ToString
        _Description = ""
        _modUser = ""
        _modDate = Date.MinValue
    End Sub
#End Region
#Region " Public Properties"
    Public Property IsInDB() As Boolean
        Get
            Return _isInDB
        End Get
        Set(ByVal Value As Boolean)
            _isInDB = Value
        End Set
    End Property
    Public Property GeneralSectionID() As String
        Get
            Return _GeneralSectionID
        End Get
        Set(ByVal Value As String)
            _GeneralSectionID = Value
        End Set
    End Property
    Public Property ModuleID() As Integer
        Get
            Return _ModuleID
        End Get
        Set(ByVal Value As Integer)
            _ModuleID = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _modUser
        End Get
        Set(ByVal Value As String)
            _modUser = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _modDate
        End Get
        Set(ByVal Value As Date)
            _modDate = Value
        End Set
    End Property
#End Region

End Class