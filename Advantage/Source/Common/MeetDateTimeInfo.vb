Public Class MeetDateTimeInfo
    Private m_MeetDay As String
    Private m_StartTime As String
    Private m_EndTime As String
    Private schedLength As System.TimeSpan

    Public Property MeetDay() As String
        Get
            Return m_MeetDay
        End Get
        Set(ByVal Value As String)
            m_MeetDay = Value
        End Set
    End Property

    Public Property StartTime() As String
        Get
            Return m_StartTime
        End Get
        Set(ByVal Value As String)
            m_StartTime = Value
        End Set
    End Property

    Public Property EndTime() As String
        Get
            Return m_EndTime
        End Get
        Set(ByVal Value As String)
            m_EndTime = Value
        End Set
    End Property

    Public ReadOnly Property Hours() As Decimal
        Get
            schedLength = Convert.ToDateTime(m_EndTime).Subtract(Convert.ToDateTime(m_StartTime))
            Return (schedLength.Hours) + Math.Round((schedLength.Minutes / 60), 1)
        End Get
    End Property

    Public Sub New(ByVal meetDay As String, ByVal startTime As String, ByVal endTime As String)
        m_MeetDay = meetDay
        m_StartTime = startTime
        m_EndTime = endTime
    End Sub
End Class
