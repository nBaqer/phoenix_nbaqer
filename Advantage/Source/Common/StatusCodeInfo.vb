Public Class StatusCodeInfo
    Private _Code As String
    Private _Descrip As String
    Private _Status As Guid
    Private _SystemStatus As Integer
    Private _CampusGroup As Guid
    Private _StatusLevelId As Integer
    Private _StatusCodeId As Guid
    Private _StatusId As Guid
    Private _ModUser As String
    Private _ModDate As DateTime
    Private _isInUse As Boolean
    Private _isDefaultLeadStatus As Boolean
    Private _AcadStat As String

    'Private _AcadProbation As Integer
    'Private _DiscProbation As Integer

    Public Property Code() As String
        Get
            Code = _Code
        End Get
        Set(ByVal Value As String)
            _Code = Value
        End Set
    End Property
    Public Property AcadStat() As String
        Get
            Code = _AcadStat
        End Get
        Set(ByVal Value As String)
            _AcadStat = Value
        End Set
    End Property
    Public Property Descrip() As String
        Get
            Descrip = _Descrip
        End Get
        Set(ByVal Value As String)
            _Descrip = Value
        End Set
    End Property
    Public Property Status() As Guid
        Get
            Status = _Status
        End Get
        Set(ByVal Value As Guid)
            _Status = Value
        End Set
    End Property
    Public Property SystemStatus() As Integer
        Get
            SystemStatus = _SystemStatus
        End Get
        Set(ByVal Value As Integer)
            _SystemStatus = Value
        End Set
    End Property
    Public Property StatusLevelId() As Integer
        Get
            StatusLevelId = _StatusLevelId
        End Get
        Set(ByVal Value As Integer)
            _StatusLevelId = Value
        End Set
    End Property
    'Public Property AcadProbation() As Integer
    '    Get
    '        AcadProbation = _AcadProbation
    '    End Get
    '    Set(ByVal Value As Integer)
    '        _AcadProbation = Value
    '    End Set
    'End Property
    'Public Property DiscProbation() As Integer
    '    Get
    '        DiscProbation = _DiscProbation
    '    End Get
    '    Set(ByVal Value As Integer)
    '        _DiscProbation = Value
    '    End Set
    'End Property
    Public Property StatusId() As Guid
        Get
            StatusId = _StatusId
        End Get
        Set(ByVal Value As Guid)
            _StatusId = Value
        End Set
    End Property
    Public Property StatusCodeId() As Guid
        Get
            StatusCodeId = _StatusCodeId
        End Get
        Set(ByVal Value As Guid)
            _StatusCodeId = Value
        End Set
    End Property
    Public Property CampusGroup() As Guid
        Get
            CampusGroup = _CampusGroup
        End Get
        Set(ByVal Value As Guid)
            _CampusGroup = Value
        End Set
    End Property
    Public Property ModUser() As String
        Get
            Return _ModUser
        End Get
        Set(ByVal Value As String)
            _ModUser = Value
        End Set
    End Property

    Public Property ModDate() As DateTime
        Get
            Return _ModDate
        End Get
        Set(ByVal Value As DateTime)
            _ModDate = Value
        End Set
    End Property
    Public Property IsInUse() As Boolean
        Get
            Return _isInUse
        End Get
        Set(ByVal Value As Boolean)
            _isInUse = Value
        End Set
    End Property
    Public Property IsDefaultLeadStatus() As Boolean
        Get
            Return _isDefaultLeadStatus
        End Get
        Set(ByVal value As Boolean)
            _isDefaultLeadStatus = value
        End Set
    End Property

    Public Sub New(ByVal NewCode As String, ByVal NewDescrip As String, ByVal NewStatus As Guid, ByVal NewSystemStatus As Integer, ByVal NewCampusGroup As Guid)
        _Code = NewCode
        _Descrip = NewDescrip
        _Status = NewStatus
        _SystemStatus = NewSystemStatus
        _CampusGroup = NewCampusGroup
        _ModUser = ""
        _ModDate = Date.MinValue
        _isInUse = False
        _AcadStat = ""
        '_AcadProbation = 0
        '_DiscProbation = 0
    End Sub
    Public Sub New()
        MyBase.new()
    End Sub
End Class

Public Enum StatusLevel
    Lead = 1
    StudentEnrollment = 2
    Student = 3
End Enum

Public Enum SystemStatus_LeadLevel
    NewLead = 1
    ApplicationReceived = 2
    ApplicationNotAccepted = 3
    InterviewScheduled = 4
    Interviewed = 5
    Enrolled = 6
End Enum

Public Enum SystemStatus_StudentEnrollmentLevel
    FutureStart = 7
    NoStart = 8
    CurrentlyAttending = 9
    LeaveOfAbsence = 10
    Suspended = 11
    Dropped = 12
    Graduated = 14
    DeadLead = 17
    WillEnrollInTheFuture = 18
    TransferOut = 19
    AcademicProbation = 20
    Suspension = 21
    Externship = 22
End Enum

Public Enum SystemStatus_StudentLevel
    Active = 15
    InActive = 16
End Enum