﻿''' <summary>
''' This class is used for fill enrollment combobox
''' </summary>
''' <remarks>
''' In use in GraduateAuditGroup.
''' </remarks>
Public Class EnrollmentItemsDto
    ''' <summary>
    ''' This should hold a Guid
    ''' </summary>
    ''' <value></value>
    Public property PrgVerId as String
    ''' <summary>
    ''' This should hold a Guid
    ''' </summary>
    Public Property StuEnrollId As String
    ''' <summary>
    ''' This hold a description
    ''' </summary>
    Public property PrgVerDescrip As String
   
End Class
