Public Class PlacementInfo
#Region "Variables"
    Private _StrGrpId As String
    Private _IsInDb As Boolean
    Private _Code As String
    Private _Description As String
    Private _CampGrpId As String
    Private _ExtracurricularId As String
    Private _StatusId As String
    Private _ExtracurricularGrpId As String
    Private _Address1 As String
    Private _Address2 As String
    Private _City As String
    Private _State As String
    Private _Zip As String
    Private _StudentId As String
    Private _EducationInstId As String
    Private _StateId As String
    Private _GraduationDate As String
    Private _FinalGrade As String
    Private _Certificate As String
    Private _Comments As String
    Private _EducationInstType As String
    Private _EmployerId As String
    Private _jobStatusId As String
    Private _JobStatus As String
    Private _FromDate As String
    Private _ToDate As String
    Private _JobTitleId As String
    Private _StEmploymentId As String
    Private _Major As String
    Private _PlacementRep As String
    Private _Supervisor As String
    Private _PlacedDate As String
    Private _FeeId As String
    Private _fee As String
    Private _Salary As String
    Private _SalaryTypeId As String
    Private _salaryType As String
    Private _ScheduleId As String
    Private _schedule As String
    Private _BenefitsId As String
    Private _benefits As String
    Private _howPlacedId As String
    Private _HowPlaced As String
    Private _InterviewId As String
    Private _interview As String
    Private _FldStudyId As String
    Private _fldStudy As String
    Private _TerminationReason As String
    Private _EnrollmentId As String
    Private _PlacementId As String
    Private _firstname As String
    Private _middlename As String
    Private _lastname As String
    Private _SchoolStatusId As String
    Private _WantAssistance As String
    Private _WaiverSigned As String
    Private _Eligible As String
    Private _Reason As String
    Private _AreaId As String
    Private _area As String
    Private _AvailableDate As String
    Private _AvailableDays As String
    Private _AvailableHours As String
    Private _LowSalary As String
    Private _HighSalary As String
    Private _TransportationId As String
    Private _transportation As String
    Private _getCount As String
    Private _EnrollmentDescrip As String
    Private _LeadID As String
    Private _JobResponsibilities As String
    Private _ExpertiseLevelId As String
    Private _FullPartTime As String
    Private _EmployerName As String
    Private _EmployerJobTitle As String
    Private _EmployerJobTitleId As String
    Private _ModDate As DateTime
    Private _OtherCollegeNotListed As String
    Private _CertificateText As String
    Private _EducationInst As String
    Private _CertificateDescrip As String
    Private _JobTitleDescrip As String
    Private _JobStatusDescrip As String
    Private _InelReasonId As String
    Private _ExitInterviewDate As String
#End Region
#Region "Constructor"
    Public Sub New()
        _Code = ""
        _IsInDb = False
        _Description = ""
        _CampGrpId = Guid.Empty.ToString()
        _ExtracurricularId = Guid.NewGuid.ToString()
        _StatusId = Guid.Empty.ToString()
        _ExtracurricularGrpId = Guid.Empty.ToString()
        _StudentId = Guid.Empty.ToString()
        _EducationInstId = Guid.Empty.ToString()
        _StateId = Guid.Empty.ToString()
        _GraduationDate = ""
        _FinalGrade = ""
        _Certificate = ""
        _Comments = ""
        _EducationInstType = ""
        _EmployerId = Guid.Empty.ToString()
        _jobStatusId = Guid.Empty.ToString
        _JobStatus = ""
        _FromDate = ""
        _ToDate = ""
        _JobTitleId = Guid.Empty.ToString()
        _StEmploymentId = Guid.NewGuid.ToString()
        _Major = ""
        _PlacementRep = ""
        _Supervisor = ""
        _PlacedDate = ""
        _FeeId = Guid.Empty.ToString()
        _fee = ""
        _Salary = ""
        _SalaryTypeId = Guid.Empty.ToString()
        _salaryType = ""
        _ScheduleId = Guid.Empty.ToString()
        _schedule = ""
        _BenefitsId = Guid.Empty.ToString()
        _benefits = ""
        _InterviewId = Guid.Empty.ToString()
        _interview = ""
        _FldStudyId = Guid.Empty.ToString()
        _fldStudy = ""
        _TerminationReason = ""
        _EnrollmentId = ""
        _howPlacedId = Guid.Empty.ToString()
        _HowPlaced = ""
        _PlacementId = Guid.NewGuid.ToString()
        _firstname = ""
        _middlename = ""
        _lastname = ""
        _SchoolStatusId = Guid.Empty.ToString()
        _WantAssistance = ""
        _WaiverSigned = ""
        _Eligible = ""
        _Reason = ""
        _AreaId = Guid.Empty.ToString()
        _AvailableDate = ""
        _AvailableDays = ""
        _AvailableHours = ""
        _LowSalary = ""
        _HighSalary = ""
        _TransportationId = Guid.Empty.ToString()
        _transportation = ""
        _getCount = ""
        _EnrollmentDescrip = ""
        _LeadID = ""
        _JobResponsibilities = ""
        _ExpertiseLevelId = ""
        _FullPartTime = ""
        _EmployerName = ""
        _EmployerJobTitleId = Guid.Empty.ToString()
        _EmployerJobTitle = ""
        _ModDate = Date.MinValue
        _OtherCollegeNotListed = ""
        _CertificateDescrip = ""
        _JobTitleDescrip = ""
        _JobStatusDescrip = ""
        _EducationInst = ""
        _InelReasonId = ""
        _ExitInterviewDate = ""
    End Sub
#End Region
#Region "Property"
    Public Property IsInDb() As Boolean
        Get
            Return _IsInDb
        End Get
        Set(ByVal Value As Boolean)
            _IsInDb = Value
        End Set
    End Property
    Public Property ModDate() As Date
        Get
            Return _ModDate
        End Get
        Set(ByVal Value As Date)
            _ModDate = Value
        End Set
    End Property
    Public Property ExpertiseLevelId() As String
        Get
            Return _ExpertiseLevelId
        End Get
        Set(ByVal Value As String)
            _ExpertiseLevelId = Value
        End Set
    End Property
    Public Property OtherCollegeNotListed() As String
        Get
            Return _OtherCollegeNotListed
        End Get
        Set(ByVal Value As String)
            _OtherCollegeNotListed = Value
        End Set
    End Property
    Public Property FullPartTime() As String
        Get
            Return _FullPartTime
        End Get
        Set(ByVal Value As String)
            _FullPartTime = Value
        End Set
    End Property
    Public Property EmployerName() As String
        Get
            Return _EmployerName
        End Get
        Set(ByVal Value As String)
            _EmployerName = Value
        End Set
    End Property
    Public Property EmployerJobTitleId() As String
        Get
            Return _EmployerJobTitleId
        End Get
        Set(ByVal Value As String)
            _EmployerJobTitleId = Value
        End Set
    End Property
    Public Property EmployerJobTitle() As String
        Get
            Return _EmployerJobTitle
        End Get
        Set(ByVal Value As String)
            _EmployerJobTitle = Value
        End Set
    End Property
    Public Property JobResponsibilities() As String
        Get
            Return _JobResponsibilities
        End Get
        Set(ByVal Value As String)
            _JobResponsibilities = Value
        End Set
    End Property
    Public Property getCount() As String
        Get
            Return _getCount
        End Get
        Set(ByVal Value As String)
            _getCount = Value
        End Set
    End Property
    Public Property CampGrpid() As String
        Get
            Return _CampGrpId
        End Get
        Set(ByVal Value As String)
            _CampGrpId = Value
        End Set
    End Property
    Public Property ExtracurricularId() As String
        Get
            Return _ExtracurricularId
        End Get
        Set(ByVal Value As String)
            _ExtracurricularId = Value
        End Set
    End Property
    Public Property PlacementId() As String
        Get
            Return _PlacementId
        End Get
        Set(ByVal Value As String)
            _PlacementId = Value
        End Set
    End Property
    Public Property StatusId() As String
        Get
            Return _StatusId
        End Get
        Set(ByVal Value As String)
            _StatusId = Value
        End Set
    End Property
    Public Property LeadID() As String
        Get
            Return _LeadID
        End Get
        Set(ByVal Value As String)
            _LeadID = Value
        End Set
    End Property
    Public Property ExtracurricularGrpId() As String
        Get
            Return _ExtracurricularGrpId
        End Get
        Set(ByVal Value As String)
            _ExtracurricularGrpId = Value
        End Set
    End Property
    Public Property StrGrpId() As String
        Get
            Return _StrGrpId
        End Get
        Set(ByVal Value As String)
            _StrGrpId = Value
        End Set
    End Property
    Public Property EnrollmentId() As String
        Get
            Return _EnrollmentId
        End Get
        Set(ByVal Value As String)
            _EnrollmentId = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return _Code
        End Get
        Set(ByVal Value As String)
            _Code = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return _Address1
        End Get
        Set(ByVal Value As String)
            _Address1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return _Address2
        End Get
        Set(ByVal Value As String)
            _Address2 = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _City
        End Get
        Set(ByVal Value As String)
            _City = Value
        End Set
    End Property
    Public Property State() As String
        Get
            Return _State
        End Get
        Set(ByVal Value As String)
            _State = Value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return _Zip
        End Get
        Set(ByVal Value As String)
            _Zip = Value
        End Set
    End Property
    Public Property StudentId() As String
        Get
            Return _StudentId
        End Get
        Set(ByVal Value As String)
            _StudentId = Value
        End Set
    End Property
    Public Property EducationInstId() As String
        Get
            Return _EducationInstId
        End Get
        Set(ByVal Value As String)
            _EducationInstId = Value
        End Set
    End Property
    Public Property StateId() As String
        Get
            Return _StateId
        End Get
        Set(ByVal Value As String)
            _StateId = Value
        End Set
    End Property
    Public Property GraduationDate() As String
        Get
            Return _GraduationDate
        End Get
        Set(ByVal Value As String)
            _GraduationDate = Value
        End Set
    End Property
    Public Property FinalGrade() As String
        Get
            Return _FinalGrade
        End Get
        Set(ByVal Value As String)
            _FinalGrade = Value
        End Set
    End Property
    Public Property Certificate() As String
        Get
            Return _Certificate
        End Get
        Set(ByVal Value As String)
            _Certificate = Value
        End Set
    End Property
    Public Property CertificateDescrip() As String
        Get
            Return _CertificateDescrip
        End Get
        Set(ByVal Value As String)
            _CertificateDescrip = Value
        End Set
    End Property
    Public Property Comments() As String
        Get
            Return _Comments
        End Get
        Set(ByVal Value As String)
            _Comments = Value
        End Set
    End Property
    Public Property EducationInstType() As String
        Get
            Return _EducationInstType
        End Get
        Set(ByVal Value As String)
            _EducationInstType = Value
        End Set
    End Property
    Public Property EmployerId() As String
        Get
            Return _EmployerId
        End Get
        Set(ByVal Value As String)
            _EmployerId = Value
        End Set
    End Property
    Public Property JobStatus() As String
        Get
            Return _JobStatus
        End Get
        Set(ByVal Value As String)
            _JobStatus = Value
        End Set
    End Property
    Public Property JobStatusId() As String
        Get
            Return _jobStatusId
        End Get
        Set(ByVal Value As String)
            _jobStatusId = Value
        End Set
    End Property
    Public Property StartDate() As String
        Get
            Return _FromDate
        End Get
        Set(ByVal Value As String)
            _FromDate = Value
        End Set
    End Property
    Public Property EndDate() As String
        Get
            Return _ToDate
        End Get
        Set(ByVal Value As String)
            _ToDate = Value
        End Set
    End Property
    Public Property JobTitleId() As String
        Get
            Return _JobTitleId
        End Get
        Set(ByVal Value As String)
            _JobTitleId = Value
        End Set
    End Property
    Public Property StEmploymentId() As String
        Get
            Return _StEmploymentId
        End Get
        Set(ByVal Value As String)
            _StEmploymentId = Value
        End Set
    End Property
    Public Property Major() As String
        Get
            Return _Major
        End Get
        Set(ByVal Value As String)
            _Major = Value
        End Set
    End Property
    Public Property PlacementRep() As String
        Get
            Return _PlacementRep
        End Get
        Set(ByVal Value As String)
            _PlacementRep = Value
        End Set
    End Property
    Public Property Supervisor() As String
        Get
            Return _Supervisor
        End Get
        Set(ByVal Value As String)
            _Supervisor = Value
        End Set
    End Property
    Public Property PlacedDate() As String
        Get
            Return _PlacedDate
        End Get
        Set(ByVal Value As String)
            _PlacedDate = Value
        End Set
    End Property
    Public Property FeeId() As String
        Get
            Return _FeeId
        End Get
        Set(ByVal Value As String)
            _FeeId = Value
        End Set
    End Property
    Public Property Fee() As String
        Get
            Return _fee
        End Get
        Set(ByVal Value As String)
            _fee = Value
        End Set
    End Property
    Public Property Salary() As String
        Get
            Return _Salary
        End Get
        Set(ByVal Value As String)
            _Salary = Value
        End Set
    End Property
    Public Property SalaryTypeId() As String
        Get
            Return _SalaryTypeId
        End Get
        Set(ByVal Value As String)
            _SalaryTypeId = Value
        End Set
    End Property
    Public Property SalaryType() As String
        Get
            Return _salaryType
        End Get
        Set(ByVal Value As String)
            _salaryType = Value
        End Set
    End Property
    Public Property ScheduleId() As String
        Get
            Return _ScheduleId
        End Get
        Set(ByVal Value As String)
            _ScheduleId = Value
        End Set
    End Property
    Public Property Schedule() As String
        Get
            Return _schedule
        End Get
        Set(ByVal Value As String)
            _schedule = Value
        End Set
    End Property
    Public Property BenefitsId() As String
        Get
            Return _BenefitsId
        End Get
        Set(ByVal Value As String)
            _BenefitsId = Value
        End Set
    End Property
    Public Property Benefits() As String
        Get
            Return _benefits
        End Get
        Set(ByVal Value As String)
            _benefits = Value
        End Set
    End Property
    Public Property InterviewId() As String
        Get
            Return _InterviewId
        End Get
        Set(ByVal Value As String)
            _InterviewId = Value
        End Set
    End Property
    Public Property Interview() As String
        Get
            Return _interview
        End Get
        Set(ByVal Value As String)
            _interview = Value
        End Set
    End Property
    Public Property FldStudyId() As String
        Get
            Return _FldStudyId
        End Get
        Set(ByVal Value As String)
            _FldStudyId = Value
        End Set
    End Property
    Public Property FldStudy() As String
        Get
            Return _fldStudy
        End Get
        Set(ByVal Value As String)
            _fldStudy = Value
        End Set
    End Property
    Public Property TerminationReason() As String
        Get
            Return _TerminationReason
        End Get
        Set(ByVal Value As String)
            _TerminationReason = Value
        End Set
    End Property
    Public Property HowPlaced() As String
        Get
            Return _HowPlaced
        End Get
        Set(ByVal Value As String)
            _HowPlaced = Value
        End Set
    End Property
    Public Property HowPlacedId() As String
        Get
            Return _howPlacedId
        End Get
        Set(ByVal Value As String)
            _howPlacedId = Value
        End Set
    End Property
    Public Property firstname() As String
        Get
            Return _firstname
        End Get
        Set(ByVal Value As String)
            _firstname = Value
        End Set
    End Property
    Public Property MiddleName() As String
        Get
            Return _middlename
        End Get
        Set(ByVal Value As String)
            _middlename = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return _lastname
        End Get
        Set(ByVal Value As String)
            _lastname = Value
        End Set
    End Property
    Public Property SchoolStatusId() As String
        Get
            Return _SchoolStatusId
        End Get
        Set(ByVal Value As String)
            _SchoolStatusId = Value
        End Set
    End Property
    Public Property WantAssistance() As String
        Get
            Return _WantAssistance
        End Get
        Set(ByVal Value As String)
            _WantAssistance = Value
        End Set
    End Property
    Public Property WaiverSigned() As String
        Get
            Return _WaiverSigned
        End Get
        Set(ByVal Value As String)
            _WaiverSigned = Value
        End Set
    End Property
    Public Property Eligible() As String
        Get
            Return _Eligible
        End Get
        Set(ByVal Value As String)
            _Eligible = Value
        End Set
    End Property
    Public Property Reason() As String
        Get
            Return _Reason
        End Get
        Set(ByVal Value As String)
            _Reason = Value
        End Set
    End Property
    Public Property AreaId() As String
        Get
            Return _AreaId
        End Get
        Set(ByVal Value As String)
            _AreaId = Value
        End Set
    End Property
    Public Property Area() As String
        Get
            Return _area
        End Get
        Set(ByVal Value As String)
            _area = Value
        End Set
    End Property
    Public Property AvailableDate() As String
        Get
            Return _AvailableDate
        End Get
        Set(ByVal Value As String)
            _AvailableDate = Value
        End Set
    End Property
    Public Property ExitInterviewDate() As String
        Get
            Return _ExitInterviewDate
        End Get
        Set(ByVal Value As String)
            _ExitInterviewDate = Value
        End Set
    End Property
    Public Property AvailableDays() As String
        Get
            Return _AvailableDays
        End Get
        Set(ByVal Value As String)
            _AvailableDays = Value
        End Set
    End Property
    Public Property AvailableHours() As String
        Get
            Return _AvailableHours
        End Get
        Set(ByVal Value As String)
            _AvailableHours = Value
        End Set
    End Property
    Public Property HighSalary() As String
        Get
            Return _HighSalary
        End Get
        Set(ByVal Value As String)
            _HighSalary = Value
        End Set
    End Property
    Public Property LowSalary() As String
        Get
            Return _LowSalary
        End Get
        Set(ByVal Value As String)
            _LowSalary = Value
        End Set
    End Property
    Public Property TransportationId() As String
        Get
            Return _TransportationId
        End Get
        Set(ByVal Value As String)
            _TransportationId = Value
        End Set
    End Property
    Public Property Transportation() As String
        Get
            Return _transportation
        End Get
        Set(ByVal Value As String)
            _transportation = Value
        End Set
    End Property
    Public Property EnrollmentDescrip() As String
        Get
            Return _EnrollmentDescrip
        End Get
        Set(ByVal Value As String)
            _EnrollmentDescrip = Value
        End Set
    End Property
    Public Property CertificateText() As String
        Get
            Return _CertificateText
        End Get
        Set(ByVal Value As String)
            _CertificateText = Value
        End Set
    End Property
    Public Property EducationInst() As String
        Get
            Return _EducationInst
        End Get
        Set(ByVal Value As String)
            _EducationInst = Value
        End Set
    End Property
    Public Property JobTitleDescrip() As String
        Get
            Return _JobTitleDescrip
        End Get
        Set(ByVal Value As String)
            _JobTitleDescrip = Value
        End Set
    End Property
    Public Property JobStatusDescrip() As String
        Get
            Return _JobStatusDescrip
        End Get
        Set(ByVal Value As String)
            _JobStatusDescrip = Value
        End Set
    End Property
    Public Property InelReasonId() As String
        Get
            Return _InelReasonId
        End Get
        Set(ByVal Value As String)
            _InelReasonId = Value
        End Set
    End Property
#End Region
End Class
