Public MustInherit Class SAPDetailsInfo
    Private _SAPDetailId As Guid
    Private _SAPId As Guid
    Private _Increment As Integer
    Private _TriggerUnitTypeId As Integer
    Private _TriggerValue As Integer
    Private _QualitativeMinValue As Decimal
    Private _ConsequenceTypeId As Integer
    Private _MinimumCreditsToComplete As Integer



    Public Property SAPDetailId() As Guid
        Get
            Return _SAPDetailId
        End Get
        Set(ByVal Value As Guid)
            _SAPDetailId = Value
        End Set
    End Property
    Public Property increment() As Integer
        Get
            Return _Increment
        End Get
        Set(ByVal Value As Integer)
            _Increment = Value
        End Set
    End Property
    Public Property SAPId() As Guid
        Get
            Return _SAPId
        End Get
        Set(ByVal Value As Guid)
            _SAPId = Value
        End Set
    End Property
    Public Property TriggerUnitTypeId() As Integer
        Get
            Return _TriggerUnitTypeId
        End Get
        Set(ByVal Value As Integer)
            _TriggerUnitTypeId = Value
        End Set
    End Property
    Public Property TriggerValue() As Integer
        Get
            Return _TriggerValue
        End Get
        Set(ByVal Value As Integer)
            _TriggerValue = Value
        End Set
    End Property
    Public Property QualitativeMinValue() As Decimal
        Get
            Return _QualitativeMinValue
        End Get
        Set(ByVal Value As Decimal)
            _QualitativeMinValue = Value
        End Set
    End Property
    Public Property MinimumCreditsToComplete() As Integer
        Get
            Return _minimumcreditstocomplete
        End Get
        Set(ByVal Value As Integer)
            _MinimumCreditsToComplete = Value
        End Set
    End Property

End Class

Public Class SAPDetailsInfoPercentagePoints
    Inherits SAPDetailsInfo


End Class
