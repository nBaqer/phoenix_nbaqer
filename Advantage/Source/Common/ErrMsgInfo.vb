Public Class ErrMsgInfo
#Region "Private Variable Declaration"
    Private _ErrMsg As String
#End Region

#Region "Class Section Meeting Properties"

    Public Property ErrMsg() As String
        Get
            ErrMsg = _ErrMsg
        End Get
        Set(ByVal Value As String)
            _ErrMsg = Value
        End Set
    End Property

#End Region
End Class
