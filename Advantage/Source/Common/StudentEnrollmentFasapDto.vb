﻿Public Class StudentEnrollmentFasapDto
    
    Public Property Increment As String
    Public Property TriggeredAt As String
    Public Property sapcheckDate As DateTime
    Public Property IsMakingSap As Boolean
    Public Property IsMakSap As String  ' This hold the same as IsMakingSAP, but is YES if True and NO if false.
    Public Property Reason As String
 End Class
