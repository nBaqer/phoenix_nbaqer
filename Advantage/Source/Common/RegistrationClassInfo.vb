﻿Public Class RegistrationClassInfo

    Sub New(studentid As String, classId As String, stuEnrollmentId As String)

        Me.StuEnrollmentId = stuEnrollmentId
        Me.ClassId = classId
        Me.StudentId = studentid
    End Sub


    Property StudentId() As String
    Property ClassId() As String
    Property StuEnrollmentId As String
    Property UserName() As String
    Property CampusId() As String

End Class
