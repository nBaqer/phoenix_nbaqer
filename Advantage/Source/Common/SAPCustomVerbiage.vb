<Serializable()>
Public Class SAPCustomVerbiage
#Region "Private Variable Declarations"

#End Region

#Region " Public Constructor"

    Public Sub New()
        SapId = Guid.Empty
        Message = ""
        Code = ""
    End Sub

#End Region

#Region "SAP Custom Verbiage Properties"

    Public Property SapId As Guid

    Public Property Message As String

    Public Property Code As String

#End Region
End Class
