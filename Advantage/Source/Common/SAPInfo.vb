Public Class SAPInfo
#Region "Private Variable Declarations"

    'The Property return 0 or 1
    'Public _QuantMinValueByInsTypes() As QuantMinValueByInsType
    ''Add SAP TransferHours

#End Region

#Region " Public Constructor"

    Public Sub New()
        FaSapPolicy = 0
        ModUser = ""
        ModDate = Date.MinValue
        Accuracy = 0
    End Sub

#End Region

#Region "SAP Info Properties"

    
    Public Sub AddQuantMinValueByInsType(newQuantMinValueByInsType As QuantMinValueByInsType)
        QuantMinValueByInsTypes.Add(newQuantMinValueByInsType)
    End Sub

    Public Property QuantMinValueByInsTypes As List(Of QuantMinValueByInsType)

    Public Property TitleIVSAPCustomVerbiage As List(Of SAPCustomVerbiage)

    Public Property SAPDetailId As Guid

    Public Property Seq As Integer

    Public Property FaSapPolicy As Integer

    Public Property SAPId As Guid

    Public Property TrigValue As Integer

    Public Property TrigUnit As String

    Public Property TrigOffsetTyp As String

    Public Property TrigOffsetSeq As Integer

    Public Property TrigDescrip As String

    Public Property Consequence As String

    Public Property MinAttendanceValue As Decimal

    Public Property QualMinValue As Decimal

    Public Property QuantMinValue As Decimal

    Public Property QualType As String

    Public Property QuantMinUnitType As String

    Public Property MinCredsCompltd As Decimal

    Public Property TermSDate As Date

    Public Property TermEDate As Date

    Public Property TrigUnitTypId As Integer

    Public Property TrigOffsetTypId As Integer

    Public Property QuantMinUnitTypId As Integer

    Public Property QualMinTypId As Integer

    Public Property ConsequenceTypId As Integer

    Public Property ValidationSequence As String

    Public Property IsSAPPolicyUsed As Boolean

    Public Property Code As String

    Public Property Descrip As String

    Public Property StatusId As Guid

    Public Property CampGrpId As String

    Public Property ModDate As Date

    Public Property ModUser As String

    Public Property OffsetDate As Date

    Public Property CredsEarned As Decimal

    Public Property FinCredsEarned As Decimal

    Public Property GPA As Decimal

    Public Property CredsAttempted As Decimal

    Public Property PercCredsEarned As Decimal

    Public Property TerminateProbCnt As Integer

    Public Property Period As Integer

    Public Property MinTermGPA As Decimal

    Public Property TermGPAOver As Decimal

    Public Property Term As String

    Public Property TermGPA As Decimal

    Public Property SchedHours As Decimal

    Public Property ActHours As Decimal

    Public Property ActHoursAdjusted As Decimal

    Public Property OverallAverage As Decimal

    Public Property TrackExternAttendance As Boolean

    Public Property TrackIfStudentPassedSAPSpeedSkills As Boolean

    Public Property Accuracy As Decimal

    Public Property IncludeTransferHours As Boolean

    Public Property attType As String

    Public Property OverallAttendance As Boolean

    Public Property PayOnProbation As Boolean?
#End Region
End Class
Public Class QuantMinValueByInsType
    Public SAPQuantInsTypeID As Guid
    Public QuantMinValue As Decimal
    Public SAPDetailId As Guid
    Public InstructionTypeId As Guid
    Public InstructionTypeDescrip As String
    Public moddate As DateTime
    Public attDetailsInfo As AttendancePercentageInfo
    Public PercCompltd As Decimal
End Class