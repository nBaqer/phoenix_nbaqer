﻿// This has all references to API.WEB in Advantage.....
module PL {

    export class Urls {

        // Maintenance Select UDFs per page........
        static xgetServiceLayerMaintenanceUdfPages = "../proxy/api/SystemStuff/Maintenance/Udf/Get";
        static xpostServiceLayerUdfPagePosition = "../proxy/api/SystemStuff/Maintenance/Udf/Post";
        // 
       
    }

    export class SessionCache {
        static xleadInfoDllsRequiredItems = "captionRequirementdb";
        static xPriorWorkSdfFields = "PriorWorkSdfFields";
        static xPriorWorkSdfFieldsStudent = "PriorWorkSdfFieldsStudent";
        static xPriorEduSdfFields = "PriorEduSdfFields";
        static xPriorEduSdfFieldsStudent = "PriorEduSdfFieldsStudent";
    }
}