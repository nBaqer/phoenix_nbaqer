﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../fame.advantage.client.masterpage/commonfunctions.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../fame.advantage.client.masterpage/advantagevalidation.ts" />
/// <reference path="../../fame.advantage.client.masterpage/advantagenotification.ts" />
module PL {

    export class SdfPages {
        bo: SdfPagesBo;

        constructor() {
            this.bo = new SdfPagesBo();

            // Matrix to position the UDF fields
            let matrix = $("#udfPositionTable")
                .kendoListView({
                    dataSource: this.bo.matrixDs,
                    template: kendo.template($("#PositionTemplate").html())
                }).data("kendoListView") as kendo.ui.ListView;

            // Create the Entity combo box 
            var entitiesDd = $("#udfcbEntity")
                .kendoDropDownList({
                    optionLabel: "Select Entity",
                    dataSource: this.bo.entityDataSource,
                    dataTextField: "Description",
                    dataValueField: "ID",
                    change: (e) => {
                        let val = e.sender.value();
                        let previousEntity = entitiesDd.element.attr("previous");
                        //let entity = $("#udfcbEntity").data("kendoDropDownList").value();
                        let previousPage = $("#udfcbPages").data("kendoDropDownList").element.attr("previous");

                        if (previousEntity !== "" && previousPage !== "") {
                            this.bo.warningSave(this.bo.changeFlag, +previousEntity, +previousPage);
                        }
                        this.bo.getPagesDataSource(val);

                        entitiesDd.element.attr("previous", entitiesDd.value());
                        $("#udfPage").css("display", "none");
                    }

                }).data("kendoDropDownList") as kendo.ui.DropDownList;
            entitiesDd.element.attr("previous", "");

            // Drop down of Pages that can have UDF
            let ddPages = $("#udfcbPages")
                .kendoDropDownList({
                    autoBind: false,
                    optionLabel: "Select Page",
                    dataTextField: "Description",
                    dataValueField: "ID",
                    dataSource: [],
                    enable: false,
                    change: (e) => {
                        let val = e.sender.value();
                        let previousEntity = entitiesDd.element.attr("previous");
                        let previousPage = ddPages.element.attr("previous");

                        if (previousEntity !== "" && previousPage !== "") {
                            this.bo.warningSave(this.bo.changeFlag, +previousEntity, +previousPage, val);
                        } else {
                            this.bo.getUdfFiles(val);
                        }
                        ddPages.element.attr("previous", ddPages.value());
                    }
                }).data("kendoDropDownList") as kendo.ui.DropDownList;

            ddPages.element.attr("previous", "");

            // VIEW LIST ORIGIN:  Unassigned to page UDF list.........................................
            var sourceDrag = $("#udfUnassigned")
                .kendoListView({
                    dataSource: this.bo.sourceDs,
                    template: kendo.template($("#UnassignedTemplate").html())
                }).data("kendoListView") as kendo.ui.ListView;

            $("#udfUnassigned")
                .kendoDraggable({
                    filter: ".udfItem",
                    hint: element => {
                        return element.clone();
                    }
                    //dragstart: (e: any) => {
                    //    //var draggedElement = e.currentTarget; //get the DOM element that is being dragged
                    //    //this.sourceItem = sourceDrag.dataSource.getByUid(draggedElement.data("uid"));
                    //}
                });

            $("#udfUnassigned").kendoDropTarget({
                drop: (e: any) => {
                    // Get the from is the drop
                    let originTarget = e.draggable;
                    let origin = originTarget.hint[0].className;
                    if (origin === "positionCell") {
                        // Get the original position
                        let originPosition = +originTarget.hint[0].getAttribute("data-position");
                        // Get from the original dataSource the item
                        let data = matrix.dataSource.data();
                        let item = data[originPosition - 1];
                        // Create a clone to be go to the other table
                        let clone = new UdfDto();
                        clone.Position = 0;
                        clone.Description = item.Description;
                        clone.SdfVisibility = item.SdfVisibility;
                        clone.Id = item.Id;

                        // Add in the destination list
                        sourceDrag.dataSource.add(clone);
                        // Remove from Origin List
                        data[originPosition - 1].Description = "";
                        data[originPosition - 1].Id = "00000000-0000-0000-0000-000000000000";
                        data[originPosition - 1].SdfVisibility = true;

                        // Update dataSource
                        matrix.dataSource.data(data);
                        this.bo.changeFlag = true;
                    }
                }
            });

            $("#udfPositionTable").kendoDropTarget();
            $("#udfPositionTable").kendoDropTargetArea({
                filter: ".positionCell",
                dragenter: (e: any) => {
                    e.draggable.hint.css("opacity", 0.6);
                },
                dragleave: (e: any) => {
                    e.draggable.hint.css("opacity", 1);
                },
                drop: (e: any) => {
                    // Get the origin of the drag operation..........
                    let originTarget = e.draggable;
                    let uidOrigin = originTarget.currentTarget[0].dataset.uid;
                    let item: any = sourceDrag.dataSource.getByUid(uidOrigin);
                    let origin = originTarget.hint[0].className;
                    if (origin === "positionCell") {
                        // We are dragging inside the Matrix
                        let originPosition = +originTarget.hint[0].getAttribute("data-position");
                        let dropTarget = e.dropTarget;
                        let position: number = +dropTarget[0].getAttribute("data-position");
                        let ds = matrix.dataSource.data();
                        originPosition = originPosition - 1;
                        position = position - 1;

                        // Move Description
                        let tem1 = ds[originPosition].Description;
                        ds[originPosition].Description = ds[position].Description;
                        ds[position].Description = tem1;

                        //Move ID
                        let tem2 = ds[originPosition].Id;
                        ds[originPosition].Id = ds[position].Id;
                        ds[position].Id = tem2;

                        //Move SdfVisibility
                        ds[originPosition].SdfVisibility = true;
                        ds[position].SdfVisibility = true;
                        matrix.dataSource.data(ds);

                    } else {
                        // Dropping from No Assigned UDF to Matrix...........................
                        let dropTarget = e.dropTarget;
                        let position: number = +dropTarget[0].getAttribute("data-position");

                        // Get the data-source item from matrix
                        let ds = matrix.dataSource.data();

                        // Scan all items....
                        for (let i = 0; i < ds.length; i++) {
                            if (ds[i].Position === position) {
                                if (ds[i].Description === "") {

                                    // Accept the Drop. Clone the node here
                                    ds[i].Description = item.Description;
                                    ds[i].Id = item.Id;
                                    ds[i].SdfVisibility = item.SdfVisibility;

                                    // Eliminate the node from the unassigned list
                                    sourceDrag.dataSource.remove(item);
                                    matrix.dataSource.data(ds);
                                    break;
                                }
                            }
                        }
                    }
                    this.bo.changeFlag = true;
                }
            });

            $("#udfPositionTable")
                .kendoDraggable({
                    filter: ".positionCell",
                    hint: element => {
                        return element.clone();
                    },
                    dragstart: (e: any) => {
                        var position = +e.currentTarget[0].getAttribute("data-position");
                        let ds = matrix.dataSource.data();
                        for (let i = 0; i < ds.length; i++) {
                            if (ds[i].Position === position) {
                                if (ds[i].Description === "") {
                                    // Cancel the drag Operation. 
                                    e.preventDefault();
                                    return false;
                                }
                            }
                        }
                        return true;
                    }
                });

            $("#udfSave").off("click");

            $('#udfSave')
                .click((e) => {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    let page: number = +ddPages.value();
                    let entity: number = +entitiesDd.value();
                    this.bo.saveUdfToDatabase(matrix.dataSource.data(), entity, page);
                    this.bo.changeFlag = false;
                    return false;
                });
        }
    }
}
