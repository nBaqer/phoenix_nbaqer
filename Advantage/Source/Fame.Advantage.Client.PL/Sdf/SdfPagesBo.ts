﻿module PL {

    declare var XMASTER_PAGE_USER_OPTIONS_USERID: string;
    
    export class SdfPagesBo {

        db: SdfPagesDb;
        public entityDataSource: kendo.data.DataSource;
        //public pagesDataSource: kendo.data.DataSource;
        public sourceDs: kendo.data.DataSource;
        public matrixDs: kendo.data.DataSource;
        changeFlag: boolean;

        xSaveSucessfully = "Custom fields were successfully assigned to page"; 
        xSaveChanges = "Do you want to save your changes?";
        xPositionNotFound = "Position Not Found";

        constructor() {
            this.changeFlag = false;
            this.db = new SdfPagesDb();
            this.sourceDs = new kendo.data.DataSource({
                schema: {
                    model: {
                        id: "Id",
                        fields: {
                            Id: { type: "string" },
                            Position: { type: "number" },
                            Description: { type: "string" },
                            SdfVisibility: { type: "boolean" }
                        }
                    }
                }
            });
            this.entityDataSource = this.db.getEntityDataSource();
            this.matrixDs = this.db.getUdfPositionEmptyPageDataSource();
            this.entityDataSource.read()
                .then(
                () => {
                    var entitiesDd = $("#udfcbEntity").data("kendoDropDownList");
                    entitiesDd.select(3);
                    entitiesDd.trigger("change");
                }
                );
        }

        /**
         * Get the data-source for the drop-down list of Pages.
         * @param val (The page ID)
         */
        getPagesDataSource(val: string | number) {
            let dd = $("#udfcbEntity").data("kendoDropDownList");
            let dds = $("#udfcbPages").data("kendoDropDownList");
            if (val === null || val === "") {
                dds.setDataSource(new kendo.data.DataSource());
                dds.enable(false);
            } else {
                this.db.getPagesDataSource(parseInt(dd.value()), this)
                    .done(msg => {
                        if (msg != undefined) {

                            // Set the data source
                            dds.enable(true);
                            let ldata = msg.EntityList;
                            dds.setDataSource(ldata);
                            dds.value("");
                        }
                    })
                    .fail(msg => {
                        MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    });
            }
        }

        /**
         * Get the UDF list assigned and no assigned
         * @param val
         */
        getUdfFiles(val: string) {
            let lv = $("#udfUnassigned").data("kendoListView");
            let lva = $("#udfPositionTable").data("kendoListView");
            let dd = $("#udfcbEntity").data("kendoDropDownList");
            //this.matrixDs = null;
            this.cleanDataSource();
            if (val === null || val === "") {
                // hidden the drag and drop
                $("#udfPage").css("display", "none");
            } else {
                this.db.getUdfFieldsDataSource(parseInt(val), parseInt(dd.value()), this)
                    .done(msg => {
                        if (msg != undefined) {

                            // Set the data source
                            let ldata = msg.UnAssignedToPageUdfList;
                            this.sourceDs.data(ldata);
                            lv.setDataSource(this.sourceDs);
                            //lv.dataSource.read();
                            lv.refresh();

                            let ldat = msg.AssignedToPageUdfList as UdfDto[];

                            // Create the list of assigned positions
                            let assigned = new Array<number>();

                            for (let k = 0; k < ldat.length; k++) {
                                if (ldat[k].Position > 0) {
                                    assigned.push(ldat[k].Position);
                                }
                            }

                            // Set a position value to all assigned UDF
                            for (let i = 0; i < ldat.length; i++) {
                                if (ldat[i].Position === 0) {
                                    // Use a free slot
                                    for (let j = 1; j < 16; j++) {
                                        if (assigned.indexOf(j) === -1) {
                                            ldat[i].Position = j;
                                            assigned.push(j);
                                            break;
                                        }
                                    }
                                }
                            }

                            // Put the UDF in field
                            let ds = this.matrixDs.data();
                            for (let l = 0; l < ldat.length; l++) {
                                ds[ldat[l].Position - 1].Description = ldat[l].Description;
                                ds[ldat[l].Position - 1].Id = ldat[l].Id;
                                ds[ldat[l].Position - 1].Position = ldat[l].Position;
                                ds[ldat[l].Position - 1].SdfVisibility = ldat[l].SdfVisibility;
                            }

                            this.matrixDs.data(ds);
                            lva.setDataSource(this.matrixDs);
                            $("#udfPage").css("display", "block");
                            lva.refresh();
                        }
                    })
                    .fail(msg => {
                        $("#udfPage").css("display", "none");
                        MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                    });
            }
        }

        /**
         * Clear all items in the matrix position 
         */
        cleanDataSource() {
            let data = this.matrixDs.data();
            for (var i = 0; i < data.length; i++) {
                data[i].Id = "00000000-0000-0000-0000-000000000000";
                data[i].Description = "";
                data[i].Position = i + 1;
                data[i].SdfVisibility = false;
            }
        }

        /**
         * / Save information in the server
         * @param udfs
         * @param entity
         * @param page
         */
        saveUdfToDatabase(udfs: kendo.data.ObservableArray, entity: number, page: number) {
           
            // Prepare DTO to send to server. This clone each element and
            // Put them in a output list
            let list: UdfDto[] = new Array<UdfDto>();
            for (let k = 0; k < udfs.length; k++) {
                let dto = new UdfDto();
                dto.Position = udfs[k].Position;
                dto.Id = udfs[k].Id;
                dto.SdfVisibility = udfs[k].SdfVisibility;
                dto.Description = udfs[k].Description;
                list.push(dto);
            }

            // Create Output DTO
            let output = new UdfOutputModel();

            // Prepare filters and add to the output. The entity and Pages Id are loaded
            let filter = new UdfInputModel();
            filter.Command = 1;
            filter.LeadEntity = entity;
            filter.PageResourceId = page;
            filter.UserId = XMASTER_PAGE_USER_OPTIONS_USERID;
            output.Filter = filter;

            // Fill the output with the assigned entities
            output.AssignedToPageUdfList = new Array<UdfDto>();
            for (let l = 0; l < list.length; l++) {
                if (list[l].Id !== "00000000-0000-0000-0000-000000000000") {
                    output.AssignedToPageUdfList.push(list[l]);
                }
            }
            
            // Go to server
            this.db.postUdfFieldsToServer(output, this)
                .done(msg => {
                    if (msg != undefined) {
                        // Clean the session of Lead Info Page to read the new changes
                        sessionStorage.removeItem(SessionCache.xleadInfoDllsRequiredItems);
                        sessionStorage.removeItem(SessionCache.xPriorWorkSdfFields);
                        sessionStorage.removeItem(SessionCache.xPriorWorkSdfFieldsStudent);
                        sessionStorage.removeItem(SessionCache.xPriorEduSdfFields);
                        sessionStorage.removeItem(SessionCache.xPriorEduSdfFieldsStudent);
                        MasterPage.SHOW_INFO_WINDOW(this.xSaveSucessfully);
                    }
                })
                .fail(msg => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(msg);
                });
        }

        /**
         * 
         * @param flag true changes were done. you should save
         * @param entity the selected entity
         * @param pages the selected page
         * @param actualPage optional only used with Pages Drop-down
         */
        warningSave(flag: boolean, entity:number, pages:number, actualpage:string = null) {
            // See if flag is true
            if (flag) {

                // Show confirmation dialog
                $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(this.xSaveChanges))
                    .then(confirmed => {
                        if (confirmed) {
                            let matrix = $("#udfPositionTable").data("kendoListView");
                            $.when(this.saveUdfToDatabase(matrix.dataSource.data(), entity, pages))
                                .then(() => {
                                    if (actualpage !== null) {
                                        this.getUdfFiles(actualpage);
                                    }
                                });
                        } else {
                            if (actualpage !== null) {
                                this.getUdfFiles(actualpage);
                            }
                        }
                    });
                this.changeFlag = false;
            } else {
                if (actualpage !== null) {
                    this.getUdfFiles(actualpage);
                }
            }
        }
    }
}