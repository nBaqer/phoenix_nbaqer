﻿module PL {
// ReSharper disable InconsistentNaming
    /*
     * The UDF Input Model
     */
    export interface IUdfInputModel {
        /*
         * The command
         */
        Command: number;

        /*
         * Selected Entity
         */
        LeadEntity: number;

        /*
         * The Page Id that we need to get the related UDF.
         */
        PageResourceId: number;

        /**
         * The user Id
         */
        UserId: string;
    }

    /*
     * The UDF Input Model
     */
    export class UdfInputModel {
        /*
         * The command
         */
        Command: number;

        /*
         * Selected Entity
         */
        LeadEntity: number;

        /*
         * The Page Id that we need to get the related UDF.
         */
        PageResourceId: number;

        /**
         * The user Id
        */
        UserId: string;
    }
 // ReSharper restore InconsistentNaming
}
