﻿/// <reference path="../../fame.advantage.client.masterpage/advantagemessageboxs.ts" />
module PL {

    export class SdfPagesDb {

        getEntityDataSource(): kendo.data.DataSource {
            var db = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: Urls.xgetServiceLayerMaintenanceUdfPages,
                        dataType: "json",
                        type: "GET",
                        data: {
                            Command: 1 // Get entities
                        }
                    }
                },
                schema: {
                    data: "EntityList"
                },
                error: (e) => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(e);
                }
            });
            return db;
        }

        // Data Source for pages drop down list
        getPagesDataSource(entity: number, mcontext: any) {
            return $.ajax({
                context: mcontext,
                url: Urls.xgetServiceLayerMaintenanceUdfPages + "?Command=2&LeadEntity=" + entity,
                type: "GET",
                timeout: 20000,
                dataType: "json"
            });
        }

        // Get UDF list from Server
        getUdfFieldsDataSource(pageId: number, entityId:number, mcontext: any) {

            return $.ajax({
                context: mcontext,
                url: Urls.xgetServiceLayerMaintenanceUdfPages + "?Command=3&PageResourceId=" + pageId + "&LeadEntity=" +entityId,
                type: "GET",
                timeout: 20000,
                dataType: "json"
            });
        }

        // Get the data Source for Position Matrix List View
        getUdfPositionEmptyPageDataSource() {

            return new kendo.data.DataSource(
                {
                    data: [
                        { Position: 1, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false },
                        { Position: 2, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false },
                        { Position: 3, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false },
                        { Position: 4, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false },
                        { Position: 5, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false },
                        { Position: 6, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false },
                        { Position: 7, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false },
                        { Position: 8, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false },
                        { Position: 9, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false },
                        { Position: 10, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false },
                        { Position: 11, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false },
                        { Position: 12, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false },
                        { Position: 13, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false },
                        { Position: 14, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false },
                        { Position: 15, Description: "", Id: "00000000-0000-0000-0000-000000000000", SdfVisibility: false }
                    ],
                    schema: {
                        model: {
                            id: "Id",
                            fields: {
                                Id: { type: "string" },
                                Position: { type: "number" },
                                Description: { type: "string" },
                                SdfVisibility: { type: "boolean" }
                            }
                        }
                    }

                });
        }

        postUdfFieldsToServer(udfOutputModel: UdfOutputModel, context: any) {
            return $.ajax({
                context: context,
                url: Urls.xpostServiceLayerUdfPagePosition,
                type: "POST",
                dataType: "text",
                timeout: 20000,
                data: JSON.stringify(udfOutputModel)
            });

        }
    }
}