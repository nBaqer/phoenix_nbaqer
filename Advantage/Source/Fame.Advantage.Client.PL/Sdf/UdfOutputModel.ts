﻿module PL {

    /*
 * 
 */
    export interface IDropDownIntegerOutputModel {
        /*
         * 
         */
        ID: number;

        /*
         * 
         */
        Description: string;
    }

    /*
     * 
     */
    export class DropDownIntegerOutputModel {
        /*
         * 
         */
        ID: number;

        /*
         * 
         */
        Description: string;

    }

    /*
     * The UDF DTO
     */
    export interface IUdfDto {
        /*
         * UDF ID (GUID)
         */
        Id: string;

        /*
         * SDF Description
         */
        Description: string;

        /*
         * The position a number from 1 to 15 (0 is not position)
         */
        Position: number;

        /*
         * SDF Visibility (Edit:true / View)
         */
        SdfVisibility: boolean;

    }

    /*
     * The UDF DTO
     */
    export class UdfDto {
        /*
         * UDF ID (GUID)
         */
        Id: string;

        /*
         * SDF Description
         */
        Description: string;

        /*
         * The position a number from 1 to 12 (0 is not position)
         */
        Position: number;

        /*
         * SDF Visibility (Edit:true / View)
         */
        SdfVisibility: boolean;

    }


    /*
     * 
     */
    export interface IUdfOutputModel {
        /*
         * 
         */
        EntityList: DropDownIntegerOutputModel[];

        /*
         * The DTO
         */
        AssignedToPageUdfList: UdfDto[];

        /*
         * 
         */
        UnAssignedToPageUdfList: UdfDto[];

        /*
         * 
         */
        Filter: UdfInputModel;

    }

    /*
     * 
     */
    export class UdfOutputModel {
        /*
         * 
         */
        EntityList: DropDownIntegerOutputModel[];

        /*
         * 
         */
        AssignedToPageUdfList: UdfDto[];

        /*
         * 
         */
        UnAssignedToPageUdfList: UdfDto[];

        /*
         * 
         */
        Filter: UdfInputModel;
    }
}

