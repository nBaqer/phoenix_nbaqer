/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />


module StudentAttendance1 {

    export class StudentAttendance1 {

        //#region Variables 

        private viewModel: StudentAttendance1ViewModel;

        //#endregion

        // Constructor de la clase
        constructor() {

            this.viewModel = new StudentAttendance1ViewModel;

            //#region Definition TabStop.
            $("#StudentAttendancetabstrip").kendoTabStrip({
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                },

                //resize: (e) => {
                //    kendo.resize(splitter);
                //}
            });

            //#endregion

            //var splitter = $('#CourseVertical').kendoSplitter({
            //    orientation: 'horizontal',
            //    panes:[{},{}]
            //});



            //#region Init Panel Bar Header

            // Panel Bar Controller ----------------------------------------------------------------------
            var tabStrip = $("#StudentAttendancePanelBar").kendoPanelBar({
                animation: {
                    expand: { duration: 200, effects: "fadeIn" },
                    //collapse: { duration: 200, effects: "fadeOut" },
                },
                expand: (e) => {
                    //var ex = <any>e;
                    //if (ex.item.id == "EnrollmentSummary") {
                    //    this.viewModel.GetAttendanceSummaryExpand();
                    //}
                }

            });
            //#endregion
            
            //#region Panel Bar: Filter Region -------------------------------------------------------------------------

            // create the KENDO DropDownList for Enrollment List
            var cbfilterEnrollment = $("#StudentAttendanceFilterEnrollment").kendoDropDownList({
                dataTextField: "Description",
                dataValueField: "ID",
                //databind: this.viewModel,
                //value: selectedEnrollment,
                dataSource: this.viewModel.EnrollmentDropDownItems,
                index: 0,
                change: () => {
                    $("#StudentAttendanceFilterTerm").data("kendoDropDownList").value(null);

                    $("#StudentAttendanceFilterTerm").data("kendoDropDownList").trigger("change");
                    // Hidden Grids...
                    $("#StudentAttendanceAbsentPresentGrid").css("display", "none");
                    $("#StudentAttendanceGridAbsentPresentCourseSummary").css("display", "none");
                    $("#StudentAttendanceAbsentPresentGridWeek").css("display", "none");
                    // Hidden Enrollment Summary
                    $("#StudentAttendanceSummaryContainer").css("visibility", "hidden");
                    $("#lowPane").css("visibility", "hidden");
                    // Collapse Panel Bar Enrolment Summary
                    var panelbar = $("#StudentAttendancePanelBar").data("kendoPanelBar");
                    panelbar.collapse($("#EnrollmentSummary"), true);
                },
            });

            var studentAttendanceViewModel = this.viewModel;

            // create DropDownList from input HTML element
            var cbfilterTerm = $("#StudentAttendanceFilterTerm").kendoDropDownList({
                dataTextField: "Description",
                dataValueField: "ID",
                cascadeFrom: "StudentAttendanceFilterEnrollment",
                dataSource: this.viewModel.TermDropDownItems,
                index: 0,
                autoBind: false,
                change: () => {
                    studentAttendanceViewModel.CourseClassDropDownItems.read();
                    // Hidden Grids...
                    $("#StudentAttendanceAbsentPresentGrid").css("display", "none");
                    $("#lowPane").css("visibility", "hidden");
                    $("#StudentAttendanceGridAbsentPresentCourseSummary").css("display", "none");
                    $("#StudentAttendanceAbsentPresentGridWeek").css("display", "none");
                }
                //databound: e=> {
                //    var dropdownlist = $("#StudentAttendanceFilterClass").data("kendoDropDownList");
                //    dropdownlist.refresh();
                //}

            });

            // create DropDownList from input HTML element
            var cbfilterClass = $("#StudentAttendanceFilterClass").kendoDropDownList({
                dataTextField: "RequerimentDescription",
                dataValueField: "ClassSectionGuid",
                cascadeFrom: "StudentAttendanceFilterTerm",
                dataSource: this.viewModel.CourseClassDropDownItems,
                index: 0,
                autoBind: false,
                valuePrimitive: true,
                value: this.viewModel.SelectedCourseClass,
                cascade: () => {
                    var datasection = $("#StudentAttendanceFilterClass").data("kendoDropDownList");
                    var selectedItem = datasection.dataItem(datasection.select());
                    // Be sure that exists a class selected
                    if (selectedItem != null) {
                        if (selectedItem.UnitTypeDescription == "Present Absent") {
                            $("#OnlyAbsentPresent").css("display", "block");
                            $("#NoAbsentPresent").css("display", "none");
                        } else {
                            $("#OnlyAbsentPresent").css("display", "none");
                            $("#NoAbsentPresent").css("display", "block");
                        }

                        // Add class description in Post Makeup header.
                        var desc = selectedItem.RequerimentDescription;
                        var index = desc.indexOf("[");
                        if (index > 0) {
                            desc = desc.substr(0, index - 1);
                        }

                        $("#makeupclass").text(desc);

                        // Determine how is show the post makeup....
                        if (selectedItem.UnitTypeDescription.toUpperCase() === StudentAttendance.XATTENDANCE_CLOCKHOURS) {

                            // create NumericTextBox from input HTML element using custom format
                            $("#StudentAttendanceMakeUpTb").kendoNumericTextBox({
                                format: "#.00 Hours",
                                decimals: 2,
                                min: 0.00,
                                step: 0.10,
                                placeholder: "Hours"
                            });
                        }
                        if (selectedItem.UnitTypeDescription.toUpperCase() === StudentAttendance.XATTENDANCE_MINUTES) {
                            // create NumericTextBox from input HTML element using custom format
                            $("#StudentAttendanceMakeUpTb").kendoNumericTextBox({
                                format: "n Minutes",
                                decimals: 0,
                                min: 0,
                                step: 1,
                                placeholder: "Minutes"
                            });
                        }
                    }
                },
                change: () => {
                    // Hidden Grids...
                    $("#StudentAttendanceAbsentPresentGrid").css("display", "none");
                    $("#lowPane").css("visibility", "hidden");
                    $("#StudentAttendanceGridAbsentPresentCourseSummary").css("display", "none");
                    $("#StudentAttendanceAbsentPresentGridWeek").css("display", "none");

                }

            });
          

            //#endregion

            //#region Panel Bar: Reports Section -----------------------------------------------------------------------------------------
            // create DropDownList to get Report link.
            $("#StudentAttendanceFilterReport").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: this.viewModel.AttendanceReportItems,
                index: 0,

            });

            // Button of Get Attendance
            $("#StudentAttendanceReportButton").kendoButton({
                enable: true,
                click: this.viewModel.GoToReportButtonClick
            });


            //#endregion


            //#region Windows pop Up sections-----------------------------------------------------------------------------------

            var studentAttendancePopup = $("#StudentAttendancePopUp").kendoWindow
                ({
                    actions: ["Close"],
                    title: "Advantage: Student LOA, Status Change Details",
                    width: "800px",
                    visible: false

                }
                );

            //#endregion


            kendo.init($("#StudentAttendanceContainer"));
           


        }
    }

  

    $(window).resize(function () {
        var height = $(window).height() - 240;
        $('#StudentAttendanceContainer').css("height", height + "px");
        //var splitter = $('#CourseVertical').data("kendoSplitter");
        //splitter.size($("#CourseLeftPane"), "401px"); 
        //splitter.size($("#CourseLeftPane"), "400px"); 
        //kendo.resize(splitter);
        //kendo.resize($(".k-content"));


        //var top = $('#StudentAttendanceContainer').offset().top;
        //var height = $(window).height() - (top + 30);
        //$('#StudentAttendanceContainer').css("height", height + "px");
        //var splitter = $('#CourseVertical').data("kendoSplitter");
        //var paneheight = $(window).width() - $('#CourseLeftPaneContent').width() - 90;
        //splitter.size('#CourseRightPane', paneheight + "px");
        //splitter.trigger("resize");
    });

}