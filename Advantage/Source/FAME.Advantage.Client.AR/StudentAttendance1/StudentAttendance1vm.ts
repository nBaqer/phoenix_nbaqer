/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />

module StudentAttendance1 {

    export class StudentAttendance1ViewModel extends kendo.Observable {

        //#region Declarations of public and private variables
        // PanelBar: Filters Section...................
        public EnrollmentDropDownItems: kendo.data.DataSource;
        public TermDropDownItems: kendo.data.DataSource;
        public CourseClassDropDownItems: kendo.data.DataSource;

        //Panel Bar Enrollment Summary Section
        public EnrollmentSummaryInfo: kendo.data.DataSource;
        public EnrollmentSummaryInfo_Loa: kendo.data.DataSource;

        // Grid Section
        public ParentGridRowsItems: kendo.data.DataSource;
        public ParentGridAbsentPresent: kendo.data.DataSource;
        public SummaryGridAbsentPresent: kendo.data.DataSource;

        // Report Section
        public AttendanceReportItems: kendo.data.DataSource;


        public SelectedCourseClass: any;
        private selectedEnrollmentId: string;
        // Type Of Report: daily or Weekly....
        public SelectedView: string;
        //#endregion

        constructor() {
            super();
        }


            //#region Get Report Click

         public GoToReportButtonClick = (e) => {
            // Get the selected item in Combobox
            var dataReport = $("#StudentAttendanceFilterReport").data("kendoDropDownList");
            var selectedReport = dataReport.dataItem(dataReport.select());
            window.location.href = selectedReport.url;
        }


        //#endregion


    }

} 