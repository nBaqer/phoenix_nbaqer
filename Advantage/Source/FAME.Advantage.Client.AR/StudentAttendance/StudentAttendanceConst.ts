module StudentAttendance {

    export var XATTENDANCE_MINUTES = "MINUTES";
    export var XATTENDANCE_CLOCKHOURS = "CLOCK HOURS";
    export var XATTENDANCE_PRESENT_ABSENT: string  = "PRESENT ABSENT";
   
}