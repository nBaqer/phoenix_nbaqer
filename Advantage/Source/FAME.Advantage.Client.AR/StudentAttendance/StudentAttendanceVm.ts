/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />

module StudentAttendance {

    // This must be exists in the page to get the studentId
    declare var StudentAttendanceStudentId: string;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    declare var XMASTER_GET_USER_ID: string;
    declare var __doPostBack;

    export class StudentAttendanceViewModel extends kendo.Observable {

        //#region Declarations of public and private variables
        // PanelBar: Filters Section...................
        public EnrollmentDropDownItems:  kendo.data.DataSource;
        public TermDropDownItems: kendo.data.DataSource;
        public CourseClassDropDownItems: kendo.data.DataSource;

        //Panel Bar Enrollment Summary Section
        public EnrollmentSummaryInfo: kendo.data.DataSource;
        public EnrollmentSummaryInfo_Loa: kendo.data.DataSource;

        // Grid Section
        public ParentGridRowsItems: kendo.data.DataSource;
        public ParentGridAbsentPresent: kendo.data.DataSource;
        public SummaryGridAbsentPresent: kendo.data.DataSource;

        // Report Section
        public AttendanceReportItems: kendo.data.DataSource;


        public SelectedCourseClass: any;
        private selectedEnrollmentId: string;
        // Type Of Report: daily or Weekly....
        public SelectedView: string;
        //#endregion

        //#region Constructor

        constructor() {
            super();

            // Initial setting for checkbox type of report
            this.SelectedView = "Daily";
            // Read from DataAccess the info from database.
            //this.EnrollmentDropDownItems = GetDropDownEnrollmentDataSource(kendo, StudentAttendanceStudentId);
            //this.TermDropDownItems = GetDropDownTermsDataSource(kendo);
            ////this.selectedEnrollmentId = $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList").value();
            //this.CourseClassDropDownItems = GetDropDownCourseClassesDataSource(kendo);
            //this.ParentGridRowsItems = GetStudentAttendanceMinutesParentGridDataSource(kendo);
            //this.ParentGridAbsentPresent = GetStudentAttendanceAbsentPresentParentGridDataSource(kendo);
            //this.EnrollmentSummaryInfo = GetStudentAttendanceEnrollmentSummaryDataSource(kendo);
            //this.EnrollmentSummaryInfo_Loa = GetEnrollmentSummaryInfo_Loa(this.EnrollmentSummaryInfo.data);
            //this.AttendanceReportItems = GetAttendanceReportItems(kendo, XMASTER_GET_CURRENT_CAMPUS_ID, XMASTER_GET_USER_ID);

        }
        //#endregion

        //#region Get Attendance Click

        public GetAttendanceClick = (e) => {
            e.preventDefault();
            //var dropdown = $('#StudentAttendanceFilterClass').data('tDropDownList');
            //this.SelectedCourseClass = dropdown.value();
            var sectionId: string = $("#StudentAttendanceFilterClass").data("kendoDropDownList").value();
            var enrollmentId: string = $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList").value();
            var dataView = $("#StudentAttendanceSelectedDataView").prop("checked");
            this.SelectedView = (dataView) ? "Daily" : "Weekly";
            // Determine the type of attendance that the school has
            var datasection = $("#StudentAttendanceFilterClass").data("kendoDropDownList");
            var selectedItem = datasection.dataItem(datasection.select());
            // Be sure that exists a class selected
            if (selectedItem == null) {
                alert("This Student has not yet courses assigned!");
                return;
            }

            if (selectedItem.UnitTypeDescription.toUpperCase() === XATTENDANCE_PRESENT_ABSENT) {
                this.GetAttendanceGridsPresentAbsent(enrollmentId, sectionId);
                $("#StudentAttendanceGridCourseSummary").css("display", "block");
            }
            else {

                this.GetAttendanceClockTime(enrollmentId, sectionId);
                $("#StudentAttendanceGridCourseSummary").css("display", "block");
                //alert("No absent present course. not developed yet");
            }


        } // end GetAttendanceClick
    //#endregion

        //#region Attendance Refactoring

        public GetAttendanceGridsPresentAbsent = (enrollmentId: string, sectionId: string) => {
            $("#lowPane").css("visibility", "visible");
            if (this.SelectedView == "Daily") {
                $("#StudentAttendanceAbsentPresentGridWeek").css("display", "none");
                $("#StudentAttendanceAbsentPresentGrid").data("kendoGrid").dataSource.query(
                    {
                        filter: {
                            "StuEnrollmentId": enrollmentId,
                            "ClsSectionGuid": sectionId,
                            "IsSummary": true,
                            "SelectedView": this.SelectedView,
                            //"SummaryDs": this.SummaryGridAbsentPresent
                        },
                        pageSize: 10, page: 1
                    });

                $("#StudentAttendanceAbsentPresentGrid").css("display", "block");
            } else {
                // Absent present Weekly report........
                $("#StudentAttendanceAbsentPresentGrid").css("display", "none");
                $("#StudentAttendanceAbsentPresentGridWeek").data("kendoGrid").dataSource.query(
                    {
                        filter: {
                            "StuEnrollmentId": enrollmentId,
                            "ClsSectionGuid": sectionId,
                            "IsSummary": true,
                            "SelectedView": this.SelectedView,
                            //"SummaryDs": this.SummaryGridAbsentPresent
                        },
                        pageSize: 10, page: 1
                    });

                $("#StudentAttendanceAbsentPresentGridWeek").css("display", "block");
            }
        }
        
        
        public GetAttendanceClockTime = (enrollId: string, sectionId: string) => {
            $("#lowPane").css("visibility", "visible");
            if (this.SelectedView == "Daily") {
                $("#StudentAttendanceGridWeek").css("display", "none");
                $("#StudentAttendanceGrid").data("kendoGrid").dataSource.query(
                    {
                        filter: {
                            "StuEnrollmentId": enrollId,
                            "ClsSectionGuid": sectionId,
                            "IsSummary": true,
                            "SelectedView": this.SelectedView,
                        },
                        pageSize: 10, page: 1
                    });

                $("#StudentAttendanceGrid").css("display", "block");
            } else {
                // Minutes Weekly report........
                $("#StudentAttendanceGrid").css("display", "none");
                $("#StudentAttendanceGridWeek").data("kendoGrid").dataSource.query(
                    {
                        filter: {
                            "StuEnrollmentId": enrollId,
                            "ClsSectionGuid": sectionId,
                            "IsSummary": true,
                            "SelectedView": this.SelectedView,
                        },
                        pageSize: 10, page: 1
                    });

                $("#StudentAttendanceGridWeek").css("display", "block");
            }
        }

        //#endregion

        //#region Get Report Click

         public GoToReportButtonClick = (e) => {
            // Get the selected item in Combobox
            var dataReport = $("#StudentAttendanceFilterReport").data("kendoDropDownList");
            var selectedReport = dataReport.dataItem(dataReport.select());
            window.location.href = selectedReport.url;
        }


        //#endregion

        //#region Enrollment Attendancee Summary Section
        public GetAttendanceSummaryExpand = () => {

            this.selectedEnrollmentId = $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList").value();
            $("#StudentAttendanceSummaryContainer").css("visibility", "visible");
            $("#StudentAttendanceFSummaryGrid").data("kendoGrid").dataSource.query(
                {
                    filter: {
                        "StuEnrollmentId": this.selectedEnrollmentId,
                        "CampusId": XMASTER_GET_CURRENT_CAMPUS_ID,
                        "LoaInfo": this.EnrollmentSummaryInfo_Loa
                    }
                });

        }


         // Show AttendanceLOA Data for the given enrollment
        public ShowAttendanceLoaPopUpClick = (e) => {
            var window = $("#StudentAttendancePopUp");
            if (!window.data("kendoWindow")) {
                window.kendoWindow(
                    {
                        actions: ["Close"],
                        title: "Advantage Attendance",
                        width: "800px",
                        visible: false

                    }
                    );
            }

            // Open the windows....
            window.data("kendoWindow").open().center();

            //$("#StudentAttendanceLoaGrid").data("kendoGrid").dataSource.query();

        }
        //#endregion Enrollment Attendance Summary Section

        //#region Post MAKE UP Click

         public PostAbsentPresentMakeUpClick = (e) => {
            //Gather the info to be post
            var id = '00000000-0000-0000-0000-000000000000'; // Empty Guid
            var date = $("#StudentAttendanceAbsentPresentDateMakeUp").data("kendoDatePicker").value();
            var enrollmentId = $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList").value();
            var sectionGuidId = $("#StudentAttendanceFilterClass").data("kendoDropDownList").value();
            var enumeration = EnumAttendanceStatus.MakeUp;
            //Post the information
            PostMakeUp(id, date, enrollmentId, sectionGuidId, enumeration, 0);
        }

         public DeleteAbsentPresentMakeUpClick = (e) => {
            //Gather the info to be post
            var id = '00000000-0000-0000-0000-000000000000'; // Empty Guid
            var date = $("#StudentAttendanceAbsentPresentDateMakeUp").data("kendoDatePicker").value();
            var enrollmentId = $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList").value();
            var sectionGuidId = $("#StudentAttendanceFilterClass").data("kendoDropDownList").value();
            var enumeration = EnumAttendanceStatus.MakeUp;
            //Post the information
            DeleteMakeUp(id, date, enrollmentId, sectionGuidId, enumeration, 0);
        }

             public PostMinutesClockMakeUpClick = () => {
            //Gather the info to be post
            var id = '00000000-0000-0000-0000-000000000000'; // Empty Guid
            var date = $("#StudentAttendanceMakeUpDatepicker").data("kendoDatePicker").value();
            var enrollmentId = $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList").value();
            var sectionGuidId = $("#StudentAttendanceFilterClass").data("kendoDropDownList").value();
            var timeMakeup = $("#StudentAttendanceMakeUpTb").data("kendoNumericTextBox").value();
            // Determine if minutes or Clock Hours
            var attentype = GetAttendanceType();
            if (attentype.toUpperCase() === XATTENDANCE_CLOCKHOURS) {
                // Convert to minutes the timeMakeUp...
                timeMakeup = timeMakeup * 60;
            }
            //Post the information
            PostMakeUp(id, date, enrollmentId, sectionGuidId, EnumAttendanceStatus.MakeUp, timeMakeup);
        }

        
            public DeleteMinutesMakeUpClick = () => {
            //Gather the info to be post
            var id = '00000000-0000-0000-0000-000000000000'; // Empty Guid
            var date = $("#StudentAttendanceMakeUpDatepicker").data("kendoDatePicker").value();
            var enrollmentId = $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList").value();
            var sectionGuidId = $("#StudentAttendanceFilterClass").data("kendoDropDownList").value();
            var timeMakeup = $("#StudentAttendanceMakeUpTb").data("kendoNumericTextBox").value();
            // Determine if minutes or Clock Hours
            var attentype = GetAttendanceType();
            if (attentype.toUpperCase() === XATTENDANCE_CLOCKHOURS) {
                // Convert to minutes the timeMakeUp...
                timeMakeup = timeMakeup * 60;
            }
            //Post the information
            DeleteMakeUp(id, date, enrollmentId, sectionGuidId, EnumAttendanceStatus.MakeUp, timeMakeup);
        }

        //#endregion

        //#region Detail Grids

          // Detail Grid for access present Daily Report.....
            public detailAbsentPresentInit(e) {
            var sectionGuid = $("#StudentAttendanceFilterClass").data("kendoDropDownList").value();
                var enrollmentGuid = $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList").value();
                var oldvalue: any;

            var detail = $("<div/>").appendTo(e.detailCell).kendoGrid({
                autoBind: true,
                dataSource: GetStudentAttendanceAbsentPresentDetailGridDataSource(kendo, e, sectionGuid, enrollmentGuid),
                scrollable: false,
                sortable: true,
                pageable: false,
                editable: "inline",
                edit: ex=> {
                    var model = <any>ex.model;
                   oldvalue = model.StatusEnum;
                },
                save: ex=> {
                    ex.preventDefault();
                    //the user changed the name field
                    var models = <any>ex.model;

                    if (oldvalue != models.StatusEnum) {
                        // Value was change send to server....
                        var jsondate = kendo.parseDate(models.AttendanceDate, "ddd, MM/dd/yy");
                        PostAbsentPresentAttendance(models.AttendanceGuid, models.ClassSectionMeetingGuid, jsondate, models.PeriodId, sectionGuid, enrollmentGuid, models.StatusEnum);
                        oldvalue = models.StatusEnum;
                        ex.sender.refresh();
                    }
                },
                columns: [
                    //{ field: "AttendanceDate", title: "Date", attributes: { style: "visibility:collapse" } },
                    { field: "PeriodDescription", title: "Period" },
                    //{ field: "StatusEnum", title: "Attendance Status" }
                    {
                        field: "StatusEnum",
                        title: "Attendance Status",
                        template: "#= StudentAttendance.GetAttendanceStatusDescription(StatusEnum) #",
                        editor: (container, options) => {
                            $('<input data-bind= "value:' + options.field + '"/>')
                                .appendTo(container)
                                .kendoDropDownList({
                                    dataTextField: "name",
                                    dataValueField: "id",
                                    dataSource: GetEnumAttendanceStatus(),
                                });
                        }
                    },
                    { command: ["edit"], title: "&nbsp;", width: "170px" }
                ]
            } as any);
        } // END detailAbsentPresentInit(e)

        // Detail Grid for access present Week Report.....
        public detailAbsentPresentInitWeek(e) {
            var sectionGuid = $("#StudentAttendanceFilterClass").data("kendoDropDownList").value();
            var enrollmentGuid = $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList").value();
            var oldvalue: any;

            var detail = $("<div/>").appendTo(e.detailCell).kendoGrid({
                autoBind: true,
                dataSource: GetStudentAttendanceAbsentPresentDetailGridDataSource(kendo, e, sectionGuid, enrollmentGuid),
                scrollable: false,
                sortable: true,
                pageable: false,
                editable: "inline",
                edit: ex=> {
                    var model = <any>ex.model;
                    oldvalue = model.StatusEnum;
                },
                save: ex=> {
                    ex.preventDefault();
                    //the user changed the name field
                    var models = <any>ex.model;

                    if (oldvalue != models.StatusEnum) {
                        // Value was change send to server....
                        var jsondate = kendo.parseDate(models.AttendanceDate, "ddd, MM/dd/yy");
                        PostAbsentPresentAttendance(models.AttendanceGuid, models.ClassSectionMeetingGuid, jsondate, models.PeriodId, sectionGuid, enrollmentGuid, models.StatusEnum);
                        oldvalue = models.StatusEnum;
                        ex.sender.refresh();
                    }
                },
                columns: [
                    {
                        field: "AttendanceDate", title: "Date", width: "120px"
                    },
                    { field: "PeriodDescription", title: "Period" },
                    //{ field: "StatusEnum", title: "Attendance Status" }
                    {
                        field: "StatusEnum",
                        title: "Attendance Status",
                        template: "#= StudentAttendance.GetAttendanceStatusDescription(StatusEnum) #",
                        editor: (container, options) => {
                            $('<input data-bind= "value:' + options.field + '"/>')
                                .appendTo(container)
                                .kendoDropDownList({
                                    dataTextField: "name",
                                    dataValueField: "id",
                                    dataSource: GetEnumAttendanceStatus(),

                                });
                        }
                    },
                    { command: ["edit"], title: "&nbsp;", width: "170px" }
                ]
            } as any);
        } // END detailAbsentPresentInitWeek(e)

        // Detail Grid Clock .....
        public detailInitClock(e) {
            var sectionGuid = $("#StudentAttendanceFilterClass").data("kendoDropDownList").value();
            var enrollmentGuid = $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList").value();

            var detail = $("<div />").appendTo(e.detailCell).kendoGrid({
                autoBind: true,
                dataSource: GetStudentAttendanceMinutesDetailGridDataSource(kendo, e, sectionGuid, enrollmentGuid),
                scrollable: false,
                sortable: true,
                pageable: false,
                editable: "inline",
                cancel: ex=> {
                    ex.sender.cancelRow();
                },
                save: (ex) => {
                    //the user changed the name field
                    ex.preventDefault();
                    var models = <any>ex.model;
                    var validation = validateUserInput(models);
                    if (validation.status == false) {
                        return;
                    }

                    var jsondate = kendo.parseDate(models.AttendanceDate, "ddd, MM/dd/yy");

                    // Value was change send to server....
                    PostMinutesClockAttendance(
                        models.AttendanceGuid,
                        models.ClassSectionMeetingGuid,
                        jsondate,
                        models.PeriodId,
                        sectionGuid,
                        enrollmentGuid,
                        validation.actual,
                        validation.makeup,
                        models.IsTardy,
                        models.IsExcused,
                        validation.schedule
                        );
                    ex.sender.refresh();
                },
                columns: [
                    //{ field: "AttendanceDate", title: "Date", attributes: { style: "visibility:collapse" } },
                    { field: "PeriodDescription", title: "Period", width: "160px" },
                    { field: "Schedule", title: "Schedule" },
                    { field: "Absent", title: "Absent" },
                    {
                        field: "IsExcused", title: "Excused",
                        template: "#= IsExcused? 1 : 0 #"
                    },
                    {
                        field: "IsTardy", title: "Tardy",
                        template: "#= IsTardy? 1 : 0 #"
                    },
                    { field: "Actual", title: "Present" },
                    { command: ["edit"], title: "&nbsp;", width: "170px" }

                ],

            } as any);
        } // END detailInitClock(e)

        public detailInitWeekClock(e) {
            var sectionGuid = $("#StudentAttendanceFilterClass").data("kendoDropDownList").value();
            var enrollmentGuid = $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList").value();

            var detail = $("<div/>").appendTo(e.detailCell).kendoGrid({
                autoBind: true,
                dataSource: GetStudentAttendanceMinutesDetailGridDataSource(kendo, e, sectionGuid, enrollmentGuid),
                scrollable: false,
                sortable: true,
                pageable: false,
                editable: "inline",
                cancel: ex=> {
                    ex.sender.cancelRow();
                },
                save: ex=> {
                    //the user changed the name field

                    ex.preventDefault();
                    var models = <any>ex.model;
                    // Verify and adjust values in grid

                    var validation = validateUserInput(models);
                    if (validation.status == false) {
                        return;
                    }

                    var jsondate = kendo.parseDate(models.AttendanceDate, "ddd, MM/dd/yy");
                    // Value was change send to server....
                    PostMinutesClockAttendance(
                        models.AttendanceGuid,
                        models.ClassSectionMeetingGuid,
                        jsondate,
                        models.PeriodId,
                        sectionGuid,
                        enrollmentGuid,
                        validation.actual,
                        validation.makeup,
                        models.IsTardy,
                        models.IsExcused,
                        validation.schedule
                        );
                    ex.sender.refresh();
                },
                columns: [
                    { field: "AttendanceDate", title: "Date", width: "110px" },
                    { field: "PeriodDescription", title: "Period", width: "160px" },
                    { field: "Schedule", title: "Schedule" },
                    { field: "Absent", title: "Absent" },
                    {
                        field: "IsExcused", title: "Excused",
                        template: "#= IsExcused? 1 : 0 #"
                    },
                    {
                        field: "IsTardy", title: "Tardy",
                        template: "#= IsTardy? 1 : 0 #"
                    },
                    { field: "Actual", title: "Present" },
                    { command: ["edit"], title: "&nbsp;", width: "170px" }
                ]
            } as any);
        } // END detailInitClock(e)



        //#endregion

    } // END Class StudentAttendanceViewModel

    //#region Helpers Functions
    export function GetAttendanceStatusDescription(statusEnum: any) {

        var res = EnumAttendanceStatus[statusEnum];
        return res;
    }

    //

    //#endregion


}