/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="StudentAttendanceVm.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />


module StudentAttendance {

    // This must be exists in the page to get the studentId
    declare var StudentAttendanceStudentId: string;
    export class StudentAttendance {
        public viewModel: StudentAttendanceViewModel;

        public SelectedEnrollment: kendo.Observable;



        // Constructor de la clase
        constructor() {
            this.viewModel = new StudentAttendanceViewModel;

            //#region Init Panel Bar Header

            // Panel Bar Controller ----------------------------------------------------------------------
            var tabStrip = $("#StudentAttendancePanelBar").kendoPanelBar({
                animation: {
                    expand: { duration: 200, effects: "fadeIn" },
                    //collapse: { duration: 200, effects: "fadeOut" },
                },
                expand: (e) => {
                    var ex = <any>e;
                    if (ex.item.id == "EnrollmentSummary") {
                        this.viewModel.GetAttendanceSummaryExpand();
                    }
                }

            });
            //#endregion

            //#region Panel Bar: Filter Region -------------------------------------------------------------------------

            // create the KENDO DropDownList for Enrollment List
            var cbfilterEnrollment = $("#StudentAttendanceFilterEnrollment").kendoDropDownList({
                dataTextField: "Description",
                dataValueField: "ID",
                //databind: this.viewModel,
                //value: selectedEnrollment,
                dataSource: this.viewModel.EnrollmentDropDownItems,
                index: 0,
                change: () => {
                    $("#StudentAttendanceFilterTerm").data("kendoDropDownList").value(null);

                    $("#StudentAttendanceFilterTerm").data("kendoDropDownList").trigger("change");
                    // Hidden Grids...
                    $("#StudentAttendanceAbsentPresentGrid").css("display", "none");
                    $("#StudentAttendanceGridAbsentPresentCourseSummary").css("display", "none");
                    $("#StudentAttendanceAbsentPresentGridWeek").css("display", "none");
                    // Hidden Enrollment Summary
                    $("#StudentAttendanceSummaryContainer").css("visibility", "hidden");
                    $("#lowPane").css("visibility", "hidden");
                    // Collapse Panel Bar Enrolment Summary
                    var panelbar = $("#StudentAttendancePanelBar").data("kendoPanelBar");
                    panelbar.collapse($("#EnrollmentSummary"), true);
                },
            });

            var studentAttendanceViewModel = this.viewModel;

            // create DropDownList from input HTML element
            var cbfilterTerm = $("#StudentAttendanceFilterTerm").kendoDropDownList({
                dataTextField: "Description",
                dataValueField: "ID",
                cascadeFrom: "StudentAttendanceFilterEnrollment",
                dataSource: this.viewModel.TermDropDownItems,
                index: 0,
                autoBind: false,
                change: () => {
                    studentAttendanceViewModel.CourseClassDropDownItems.read();
                    // Hidden Grids...
                    $("#StudentAttendanceAbsentPresentGrid").css("display", "none");
                    $("#lowPane").css("visibility", "hidden");
                    $("#StudentAttendanceGridAbsentPresentCourseSummary").css("display", "none");
                    $("#StudentAttendanceAbsentPresentGridWeek").css("display", "none");
                }
                //databound: e=> {
                //    var dropdownlist = $("#StudentAttendanceFilterClass").data("kendoDropDownList");
                //    dropdownlist.refresh();
                //}

            });

            // create DropDownList from input HTML element
            var cbfilterClass = $("#StudentAttendanceFilterClass").kendoDropDownList({
                dataTextField: "RequerimentDescription",
                dataValueField: "ClassSectionGuid",
                cascadeFrom: "StudentAttendanceFilterTerm",
                dataSource: this.viewModel.CourseClassDropDownItems,
                index: 0,
                autoBind: false,
                valuePrimitive: true,
                value: this.viewModel.SelectedCourseClass,
                cascade: () => {
                    var datasection = $("#StudentAttendanceFilterClass").data("kendoDropDownList");
                    var selectedItem = datasection.dataItem(datasection.select());
                    // Be sure that exists a class selected
                    if (selectedItem != null) {
                        if (selectedItem.UnitTypeDescription == "Present Absent") {
                            $("#OnlyAbsentPresent").css("display", "block");
                            $("#NoAbsentPresent").css("display", "none");
                        } else {
                            $("#OnlyAbsentPresent").css("display", "none");
                            $("#NoAbsentPresent").css("display", "block");
                        }

                        // Add class description in Post Makeup header.
                        var desc = selectedItem.RequerimentDescription;
                        var index = desc.indexOf("[");
                        if (index > 0) {
                            desc = desc.substr(0,index - 1);
                        }

                        $("#makeupclass").text(desc);

                        // Determine how is show the post makeup....
                        if (selectedItem.UnitTypeDescription.toUpperCase() == XATTENDANCE_CLOCKHOURS) {

                            // create NumericTextBox from input HTML element using custom format
                            $("#StudentAttendanceMakeUpTb").kendoNumericTextBox({
                                format: "#.00 Hours",
                                decimals: 2,
                                min: 0.00,
                                step:0.10,
                                placeholder: "Hours"
                            });
                        }
                        if (selectedItem.UnitTypeDescription.toUpperCase() == XATTENDANCE_MINUTES) {
                            // create NumericTextBox from input HTML element using custom format
                            $("#StudentAttendanceMakeUpTb").kendoNumericTextBox({
                                format: "n Minutes",
                                decimals: 0,
                                min: 0,
                                step: 1,
                                placeholder: "Minutes"
                            });
                        }
                    }
                },
                change: () => {
                    // Hidden Grids...
                    $("#StudentAttendanceAbsentPresentGrid").css("display", "none");
                    $("#lowPane").css("visibility", "hidden");
                    $("#StudentAttendanceGridAbsentPresentCourseSummary").css("display", "none");
                    $("#StudentAttendanceAbsentPresentGridWeek").css("display", "none");

                }

            });


            // Button of Get Attendance
            $("#StudentAttendanceGetButton").kendoButton({
                enable: true,
                click: this.viewModel.GetAttendanceClick
            });

            //#endregion

            //#region Panel Bar: Make Up Hours Section -----------------------------------------------------------------

            // Data picker make up
            $("#StudentAttendanceAbsentPresentDateMakeUp").attr('value', (new Date()).toDateString());
            $("#StudentAttendanceAbsentPresentDateMakeUp").kendoDatePicker({
                format: "ddd, MM/dd/yyyy",
                value: new Date(),
                max: new Date(),
            });

            // DatePicker for minutes and clock hours
            $("#StudentAttendanceMakeUpDatepicker").attr('value', (new Date()).toDateString());
            $("#StudentAttendanceMakeUpDatepicker").kendoDatePicker({
                format: "ddd, MM/dd/yyyy",
                value: new Date(),
                max: new Date(),
            });
 

            // Button Post of Make Up
            $("#StudentAttendancePostMakeUpButton").kendoButton({
                enable: true,
                click: this.viewModel.PostMinutesClockMakeUpClick
            });

            // Button of Delete Make Up
            $("#StudentAttendanceDeleteMakeUpButton").kendoButton({
                enable: true,
                click: this.viewModel.DeleteMinutesMakeUpClick
            });

            // Button Post of Make Up
            $("#StudentAttendanceAbsentPresentPostMakeUp").kendoButton({
                enable: true,
                click: this.viewModel.PostAbsentPresentMakeUpClick
            });

            // Button of Delete Make Up
            $("#StudentAttendanceAbsentPresentDeleteMakeUp").kendoButton({
                enable: true,
                click: this.viewModel.DeleteAbsentPresentMakeUpClick
            });

            //#endregion

            //#region Panel Bar: Reports Section -----------------------------------------------------------------------------------------
            // create DropDownList to get Report link.
            $("#StudentAttendanceFilterReport").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: this.viewModel.AttendanceReportItems,
                index: 0,

            });

            // Button of Get Attendance
            $("#StudentAttendanceReportButton").kendoButton({
                enable: true,
                click: this.viewModel.GoToReportButtonClick
            });


            //#endregion

            //#region Panel Bar: Grid Summary Section -------------------------------------------------------------------------------------
            // Grid Attendance Summary.
            $("#StudentAttendanceFSummaryGrid").kendoGrid({

                dataSource: this.viewModel.EnrollmentSummaryInfo,
                autoBind: false,

                columns: [
                    {
                        field: "LabelField",
                        title: " ",
                        width: "70px"
                    },
                    {
                        field: "Present",
                        title: "Presents",
                    },
                    {
                        field: "Absent",
                        title: "Absents",
                    },
                    {
                        field: "Tardy",
                        title: "Tardies",

                    },
                    {
                        field: "Excused",
                        title: "Excused"
                    }
                ]
            });

            // Button of Get Summary Attendance
            $("#ShowLoaInfo").kendoButton({
                enable: true,
                click: this.viewModel.ShowAttendanceLoaPopUpClick
            });

            //#endregion

            //#region Master Detail Grids Sections ---------------------------------------------------------------------------

            $("#StudentAttendanceGrid").kendoGrid({
                dataSource: this.viewModel.ParentGridRowsItems,
                autoBind: false,
                //height: 500,
                sortable: true,
                pageable: { pageSize: 10, pageSizes: true, refresh: true },
                detailInit: this.viewModel.detailInitClock,
                dataBound: function () {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                columns: [
                    {
                        field: "DateDisplay",
                        title: "Date",
                        width: "110px"
                    },
                    {
                        field: "Schedule",
                        title: "Scheduled",
                    },
                    {
                        field: "Absent",
                        title: "Absent",
                    },
                    {
                        field: "Excused",
                        title: "Excused"
                    },
                    {
                        field: "Tardies",
                        title: "Tardies"
                    },
                    {
                        field: "Actual",
                        title: "Present"
                    },
                    {
                        field: "MakeUp",
                        title: "Make Up",

                    },
                    {
                        field: "Total",
                        title: "Total"
                    }
                ]
            });

            // This grid is used in Student Attendance Minutes Clock Hours Week Report
            $("#StudentAttendanceGridWeek").kendoGrid({
                dataSource: this.viewModel.ParentGridRowsItems,
                autoBind: false,
                //height: 500,
                sortable: true,
                pageable: { pageSize: 10, pageSizes: true, refresh: true },
                detailInit: this.viewModel.detailInitWeekClock, // this.viewModel.detailAbsentPresentInitWeek,
                dataBound: function () {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                columns: [
                    {
                        field: "DateDisplay",
                        title: "Date",
                        width: "110px",
                    },
                    {
                        field: "Schedule",
                        title: "Scheduled",

                    },
                    {
                        field: "Absent",
                        title: "Absent",
                    },
                    {
                        field: "Excused",
                        title: "Excused"
                    },
                    {
                        field: "Tardies",
                        title: "Tardies"
                    },
                    {
                        field: "Actual",
                        title: "Present"
                    },
                    {
                        field: "MakeUp",
                        title: "Make Up",
                    },
                    {
                        field: "Total",
                        title: "Total"
                    }
                ]
            });


            $("#StudentAttendanceGridCourseSummary").kendoGrid({
                //dataSource: this.viewModel.SummaryGridAbsentPresent,
                autoBind: false,
                //height: 500,
                sortable: true,
                pageable: false,
            });

            // Hidde the scroll bar.
            //$("#StudentAttendanceGridAbsentPresentCourseSummary .k-grid-content").css({"overflow-y": "hidden"});


            $("#StudentAttendanceAbsentPresentGrid").kendoGrid({
                dataSource: this.viewModel.ParentGridAbsentPresent,
                autoBind: false,
                sortable: true,
                pageable: { pageSize: 10, pageSizes: true, refresh: true },
                detailInit: this.viewModel.detailAbsentPresentInit,
                dataBound: function () {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                columns: [
                    {
                        field: "DateDisplay",
                        title: "Date",
                        width: "110px",
                    },
                    {
                        field: "Schedule",
                        title: "Scheduled",

                    },
                    {
                        field: "Absent",
                        title: "Absent",
                    },
                    {
                        field: "Excused",
                        title: "Excused"
                    },
                    {
                        field: "Tardies",
                        title: "Tardies"
                    },
                    {
                        field: "Actual",
                        title: "Present"
                    },
                    {
                        field: "MakeUp",
                        title: "Make Up",
                    },
                    {
                        field: "Total",
                        title: "Total"
                    }
                ]
            });

            // This grid is used in Student Attendance Absent Present Week Report
            $("#StudentAttendanceAbsentPresentGridWeek").kendoGrid({
                dataSource: this.viewModel.ParentGridAbsentPresent,
                autoBind: false,
                //height: 500,
                sortable: true,
                pageable: { pageSize: 10, pageSizes: true, refresh: true },
                detailInit: this.viewModel.detailAbsentPresentInitWeek,
                dataBound: function () {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                columns: [
                    {
                        field: "DateDisplay",
                        title: "Date",
                        width: "110px",
                    },
                    {
                        field: "Schedule",
                        title: "Scheduled",

                    },
                    {
                        field: "Absent",
                        title: "Absent",
                    },
                    {
                        field: "Excused",
                        title: "Excused"
                    },
                    {
                        field: "Tardies",
                        title: "Tardies"
                    },
                    {
                        field: "Actual",
                        title: "Present"
                    },
                    {
                        field: "MakeUp",
                        title: "Make Up",
                    },
                    {
                        field: "Total",
                        title: "Total"
                    }
                ]
            });
            //#endregion

            //#region Windows pop Up sections-----------------------------------------------------------------------------------

            var window = $("#StudentAttendancePopUp").kendoWindow
                ({
                    actions: ["Close"],
                    title: "Advantage: Student LOA, Status Change Details",
                    width: "800px",
                    visible: false

                }
                );


            // This grid is used in Student Attendance Absent Present Week Report
            $("#StudentAttendanceLoaGrid").kendoGrid();

            $("#StudentAttendanceEnrollmentChanges").kendoGrid();

            $("#StudentAttendanceSuspendedDays").kendoGrid();

            //#endregion

            // This bind the view model with all.-------------------------------------------------------------------------
            kendo.init($("#StudentAttendanceViewModelRegion"));
            kendo.bind($("#StudentAttendanceViewModelRegion"), this.viewModel);
        }

 }
   

    ////This is in StudentAttendance.aspx page in document ready.....

    //$(document).ready(() => {

    //    var studentAttendance = new StudentAttendance();
    //});


}


