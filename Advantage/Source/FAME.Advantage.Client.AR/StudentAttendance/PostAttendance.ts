/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../Fame.Advantage.API.Client/API/AcademicRecords/PostAttendance.ts" />
/// <reference path="../../Fame.Advantage.API.Client/API/AcademicRecords/Models/IPostAttendanceZero.ts" />
declare var $find: any;
declare var XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID: string;

module PostAttendance {

    export class PostAttendance {

        attendanceApi: Api.AR.PostAttendance;

        constructor() {
            this.attendanceApi = new Api.AR.PostAttendance();
        }

        init() {

            var that = this;
            var kendoWindow = null;

            var beginWeekDate = $("input[id*='hfD0']").first().val(); // begin week date value
            var endWeekDate = $("input[id*='hfD6']").first().val(); // end week date value
            var $postZeroDateDP = $("#postZeroDateWindow"); // window date picker for post zero date
            var $btnpostzero = $('#btnpostzero');   // post zeros for attendance button open window
            var $okButtonAttendanceWindow = $('#okButtonAttendanceWindow'); // btn post zero window ok button
            var $noButtonAttendanceWindow = $('#noButtonAttendanceWindow'); // btn post zero window no button
            var $postZeroDate = $('#postZeroDate'); // hidden post zero date

            $postZeroDateDP.kendoDatePicker({
                format: "MM/dd/yyyy",
                min: new Date(beginWeekDate),
                max: new Date(endWeekDate),
                change: function (e) {
                    var dt = e.sender;
                    var value = dt.value();

                    if (value === null) {
                        dt.value(new Date(beginWeekDate).toLocaleDateString());
                    }

                    if (value < dt.min()) {
                        dt.value(new Date(beginWeekDate).toLocaleDateString());
                    } else if (value > dt.max()) {
                        dt.value(new Date(endWeekDate).toLocaleDateString());
                    }

                    if (value !== null) {
                        dt.value(value.toLocaleDateString());
                        $('#postZeroDate').val($("#postZeroDateWindow").val())
                    }

                }
            });

            $postZeroDateDP.val(new Date(beginWeekDate).toLocaleDateString()); // set initial value of date picker
            $postZeroDate.val(new Date(beginWeekDate).toLocaleDateString()); // set initial value of hidden asp net textbox

            $postZeroDateDP.on('change', function () {

                var $this = $(this);
                var currValue = $this.val();
                var timestamp = Date.parse(currValue);
                var min = new Date(beginWeekDate);
                var max = new Date(endWeekDate);

                if (isNaN(timestamp) == false) {

                    var value = new Date(currValue);

                    if (value < min) {
                        $this.val(beginWeekDate);
                    } else if (value > max) {
                        $this.val(endWeekDate);
                    }


                    $('#postZeroDate').val($("#postZeroDateWindow").val())
                }
            })

            $btnpostzero.on('click', function () {
                kendoWindow = $("#postAttendanceZeroWindow").kendoWindow({
                    actions: ["Close"],
                    title: "Post Zeros For Attendance",
                    width: "325px",
                    modal: true,
                });

                kendoWindow.data('kendoWindow').center().open();
            });

            $okButtonAttendanceWindow.on('click', function () {

                $('#okButtonAttendanceWindowASP').click();

                //var data = {} as API.Models.IPostAttendanceZero;
                //data.campusId = XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID;
                //data.dates = [$('#postZeroDate').val()];
                //data.program = $find('ddlProgram').get_value();
                //data.programVersion = $find('ddlProgramVersion').get_value();
                //data.studentName = $('#txtStudentName').val();
                //data.studentGroupId = $find('ddlStudentGrpId').get_value();
                //data.badgeNumber = $('#txtBadgeNum').val();
                //data.startDate = $("input[id*='txtStart']").first().val();
                //data.endDate = $("input[id*='txtEnd']").first().val();
                //data.inSchoolStatus = $("input[id*='chkAllStudentsInSchoolStatus']").first().is(':checked')
                //data.currentlyAttending = $("input[id*='chkInSchoolStatus_0']").first().is(':checked') ? $("input[id*='chkInSchoolStatus_0']").first().val() : "";
                //data.futureStart = $("input[id*='chkInSchoolStatus_1']").first().is(':checked') ? $("input[id*='chkInSchoolStatus_1']").first().val() : "";
                //data.leaveOfAbsense = $("input[id*='chkInSchoolStatus_2']").first().is(':checked') ? $("input[id*='chkInSchoolStatus_2']").first().val() : "";
                //data.outOfSchoolStatus = $("input[id*='chkAllOutofSchoolStatus']").first().is(':checked');
                //data.droppedOut = $("input[id*='chkStudentOutOfSchoolStatus_0']").first().is(':checked') ? $("input[id*='chkStudentOutOfSchoolStatus_0']").first().val() : "";
                //data.expelled = $("input[id*='chkStudentOutOfSchoolStatus_1']").first().is(':checked') ? $("input[id*='chkStudentOutOfSchoolStatus_1']").first().val() : "";
                //data.graduated = $("input[id*='chkStudentOutOfSchoolStatus_2']").first().is(':checked') ? $("input[id*='chkStudentOutOfSchoolStatus_2']").first().val() : "";
                //data.noShow = $("input[id*='chkStudentOutOfSchoolStatus_3']").first().is(':checked') ? $("input[id*='chkStudentOutOfSchoolStatus_3']").first().val() : "";

                //that.attendanceApi.postAttendance(data, function (e) {

                //    var $postZeroMsg = $('#postZeroMsg');
                //    if (e) {
                //        $('#btnShowPostedValues').click();
                //        kendoWindow.data('kendoWindow').close();
                //        $postZeroMsg.hide();
                //    }
                //    else {
                //        $postZeroMsg.css('color', 'red');
                //        $postZeroMsg.text('Error posting attendance');
                //        $postZeroMsg.show();
                //    }
                //});
            });

            $noButtonAttendanceWindow.on('click', function () {
                kendoWindow.data('kendoWindow').close();
            });
        }
    }
}
