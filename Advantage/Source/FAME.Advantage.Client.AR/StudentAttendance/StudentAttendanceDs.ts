
module StudentAttendance {

    //#region Enum Attendance Status
    export enum EnumAttendanceStatus { "No Attendance Posted" = 0, Present = 1, Absent = 2, Tardy = 3, Excused = 4, MakeUp = 5 }

    export function GetEnumAttendanceStatus() {
        var attendanceStatus = [];
        attendanceStatus.push({ id: EnumAttendanceStatus["No Attendance Posted"], name: EnumAttendanceStatus[EnumAttendanceStatus["No Attendance Posted"]] });
        attendanceStatus.push({ id: EnumAttendanceStatus.Present, name: EnumAttendanceStatus[EnumAttendanceStatus.Present] });
        attendanceStatus.push({ id: EnumAttendanceStatus.Absent, name: EnumAttendanceStatus[EnumAttendanceStatus.Absent] });
        attendanceStatus.push({ id: EnumAttendanceStatus.Tardy, name: EnumAttendanceStatus[EnumAttendanceStatus.Tardy] });
        attendanceStatus.push({ id: EnumAttendanceStatus.Excused, name: EnumAttendanceStatus[EnumAttendanceStatus.Excused] });
        return attendanceStatus;
    }
    //#endregion

    //#region Filters Drop Downs

    export function GetDropDownEnrollmentDataSource(kendo, studentId: string) {
        var url = XGET_STUDENTATTENDANCE_ENROLLMENT_DROPDOWN + "?StudentId=" + studentId + "&PageNumber=1&PageSize=1000";
        var programVersionDataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: url
                },
                dataType: "json",
                type: "GET",
            },
            error: ShowDataSourceError,
        });

        return programVersionDataSource;
    }

    export function GetDropDownTermsDataSource(kendo) {

        var url = XGET_STUDENT_ATTENDANCE_TERM_DROPDOWN;
        var programVersionDataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: url
                },
                parameterMap: (options, operation) => {
                    return { StuEnrollmentId: options.filter.filters[0].value };
                }
            },
            error: ShowDataSourceError,
            dataType: "json",
            type: "GET",
            serverFiltering: true
        });

        return programVersionDataSource;
    }

    export function GetDropDownCourseClassesDataSource(kendo) {

        var url = XGET_STUDENT_ATTENDANCE_COURSE_DROPDOWN;
        var dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: url,
                    cache: true,
                },
                parameterMap: (options, operation) => {
                    return {
                        TermId: $("#StudentAttendanceFilterTerm").data("kendoDropDownList").value(), //  options.filter.filters[0].value,
                        StuEnrollmentId: $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList").value()
                    };
                }
            },
            error: ShowDataSourceError,
            
            dataType: "json",
            type: "GET",
            serverFiltering: true
        });

        return dataSource;
    }
    //#endregion

    //#region Absent Present Grids 

    export function GetStudentAttendanceAbsentPresentParentGridDataSource(kendo) {

        var url = XGET_STUDENT_ATTENDANCE_GRID;
        var optionView: any;

        var dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: url
                }
                ,
                parameterMap: (options, operation) => {
                    if (options == null || options.filter === undefined) {
                        return null;
                    }
                    optionView = options.filter.filters[0].SelectedView;
                    return { "StuEnrollmentId": options.filter.filters[0].StuEnrollmentId, "ClsSectionGuid": options.filter.filters[0].ClsSectionGuid, "IsSummary": options.filter.filters[0].IsSummary };
                }
            },
            schema: {
                parse: response=> {
                    //
                    var attendancelist;
                    if (optionView == "Daily") {
                        attendancelist = ProcessResponsePerDays(response);
                    } else {
                        attendancelist = ProcessResponsePerWeek(response);
                    }


                    $("#StudentAttendanceGridCourseSummary").kendoGrid({
                        dataSource: new kendo.data.DataSource({
                            data: ProcessSummaryPerCourse(attendancelist),
                        }),
                        columns: [
                            {
                                field: "DateDisplay",
                                title: "  ",
                                width: "137px"
                            },
                            {
                                field: "Schedule",
                                title: "Scheduled",

                            },
                            {
                                field: "Absent",
                                title: "Absent",
                            },
                            {
                                field: "Excused",
                                title: "Excused"
                            },
                            {
                                field: "Tardies",
                                title: "Tardies"
                            },
                            {
                                field: "Actual",
                                title: "Present"
                            },
                            {
                                field: "MakeUp",
                                title: "Make Up",
                            },
                            {
                                field: "Total",
                                title: "Total"
                            }
                        ]
                    });

                    return attendancelist;
                }
            },

            dataType: "json",
            type: "GET",
            serverFiltering: true
        });

        dataSource.bind("error", ShowDataSourceError);

        return dataSource;
    }

    //Absent Present Get detail grid values
    export function GetStudentAttendanceAbsentPresentDetailGridDataSource(kendo, e, sectionGuid, enrollmentGuid) {
        var url = XGET_STUDENT_ATTENDANCE_GRID;
        var dataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: url,
                parameterMap: (options, operation) => {
                    return {
                        "FilterIni": options.filter.filters[0].value,
                        "FilterEnd": options.filter.filters[1].value,
                        "ClsSectionGuid": options.filter.filters[2].value,
                        "StuEnrollmentId": options.filter.filters[3].value
                    };
                },
                update: {
                    url: XGET_STUDENT_ATTENDANCE_UPDATE_POST,
                    contentType: "application/json",
                    type: "POST"

                },
            },

            schema: {
                model: {
                    id: "AttendanceGuid",
                    fields: {
                        AttendanceDate: { editable: false },
                        PeriodId: { editable: false, nullable: true },
                        PeriodDescription: { editable: false },
                        StatusEnum: { editable: true }
                    }
                },
                parse: response=> {
                    //
                    for (var i = 0; i < response.length; i++) {
                        var dateu = parseISO8601(response[i].AttendanceDate);
                        response[i].AttendanceDate = kendo.toString(dateu, "ddd, MM/dd/yy");
                    }
                    return response;
                }
            },

            //serverPaging: true,
            //serverSorting: true,
            error: ShowDataSourceError,
            serverFiltering: true,
            pageSize: 10,
            //filter: { field: "IsSummary", operator: "eq", value:false}
            filter: [
                { field: "DateIni", operator: "eq", value: e.data.DateIni },
                { field: "DateEnd", operator: "eq", value: e.data.DateEnd },
                { field: "ClsSectionGuid", operator: "eq", value: sectionGuid },
                { field: "StuEnrollmentId", operator: "eq", value: enrollmentGuid }
            ]
        });

        return dataSource;
    }

    export function PostAbsentPresentAttendance(attendanceGuid: string, classSectionMeetingGuid: string, attendanceDate: any, periodId: string, sectionGuidId: string, enrollmentGuid: string, statusEnum) {
        $.ajax({
            url: XGET_STUDENT_ATTENDANCE_UPDATE_POST,
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({ IdGuid: attendanceGuid, ClassSectionMeetingGuid: classSectionMeetingGuid, MeetDate: attendanceDate, PeriodIdGuid: periodId, ClsSectionIdGuid: sectionGuidId, EnrollmentGuid: enrollmentGuid, StatusEnum: statusEnum }),
            success: data=> {
                alert(data[0] + " " + data[1]);
            },
            error: e=> {
                ShowDataSourceError(e);
            }
        });
    }

    //#endregion

    //#region Attendance Clock Data Source

    export function GetStudentAttendanceMinutesParentGridDataSource(kendo) {

        var url = XGET_STUDENT_ATTENDANCE_GRID_CLOCK;
        var optionView: any;

        var dataSource = new kendo.data.DataSource({
            transport: {
                read: { url: url },
                parameterMap: (options, operation) => {
                    if (options == null || options.filter === undefined) {
                        return null;
                    }
                    optionView = options.filter.filters[0].SelectedView;
                    return { "StuEnrollmentId": options.filter.filters[0].StuEnrollmentId, "ClsSectionGuid": options.filter.filters[0].ClsSectionGuid, "IsSummary": options.filter.filters[0].IsSummary };
                }
            },
            schema: {
                parse: response=> {
                    //Test if return null
                    if (response.length < 1) {
                        alert("The student has not posted scheduled yet.");
                    }

                    var attendancelist;
                    if (optionView == "Daily") {
                        attendancelist = ProcessResponsePerDaysMinutes(response);
                    } else {
                        attendancelist = ProcessResponsePerWeekMinutes(response);
                    }

                    // Determine if we are in minutes or Clock hours

                    $("#StudentAttendanceGridCourseSummary").kendoGrid({
                        dataSource: new kendo.data.DataSource({
                            data: ProcessSummaryPerCourse(attendancelist),
                        }),
                        columns: [
                            {
                                field: "DateDisplay",
                                title: "  ",
                                width: "137px"
                            },
                            {
                                field: "Schedule",
                                title: "Scheduled",

                            },
                            {
                                field: "Absent",
                                title: "Absent",
                            },
                            {
                                field: "Excused",
                                title: "Excused"
                            },
                            {
                                field: "Tardies",
                                title: "Tardies"
                            },
                            {
                                field: "Actual",
                                title: "Actual"
                            },
                            {
                                field: "MakeUp",
                                title: "Make Up",
                            },
                            {
                                field: "Total",
                                title: "Total"
                            }
                        ]
                    });

                    // This create a list with the attendance day from begin to today. The grid does not show attendance after today!
                    var showableAttendanceGrid = [];
                    for (var i = 0; i < attendancelist.length; i++) {
                        // This limit the response to today
                        var today = new Date();
                        if (kendo.parseDate(attendancelist[i].DateIni) > today) {
                            break;
                        }

                        showableAttendanceGrid.push(attendancelist[i]);
                    }

                    // This convert if it is necessary the list in clock list.
                    showableAttendanceGrid = ConvertAttendanceListToHourFormatIfNecessary(showableAttendanceGrid);

                    return showableAttendanceGrid; // list limited to today
                }
            },
            dataType: "json",
            type: "GET",
            serverFiltering: true

        });

        dataSource.bind("error", ShowDataSourceError);

        return dataSource;
    }

    export function GetStudentAttendanceMinutesDetailGridDataSource(kendo, e, sectionGuid, enrollmentGuid) {
        var url = XGET_STUDENT_ATTENDANCE_GRID_CLOCK;
        var dataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: url,
                parameterMap: (options, operation) => {
                    return {
                        "FilterIni": options.filter.filters[0].value,
                        "FilterEnd": options.filter.filters[1].value,
                        "ClsSectionGuid": options.filter.filters[2].value,
                        "StuEnrollmentId": options.filter.filters[3].value
                    };
                },
            },

            schema: {
                model: {
                    id: "AttendanceGuid",
                    fields: {
                        AttendanceDate: { editable: false },
                        PeriodId: { editable: false, nullable: true },
                        PeriodDescription: { editable: false },
                        Schedule: { editable: false },
                        Absent: { editable: false },
                        IsTardy: { type: "boolean" },
                        IsExcused: { type: "boolean" },
                        //StatusEnum: { editable: true }
                    }
                },
                parse: response=> {
                    //
                    var attendanceList = [];
                    var today = new Date();
                    for (var i = 0; i < response.length; i++) {
                        var dateu = kendo.parseDate(response[i].AttendanceDate);
                        if (dateu > today) {
                            break;
                        }
                        response[i].AttendanceDate = kendo.toString(dateu, "ddd, MM/dd/yy");
                        response[i].IsTardy = (response[i].Tardies == 1);
                        response[i].IsExcused = (response[i].Excused == 1);
                        attendanceList.push(response[i]);

                    }
                    attendanceList = ConvertAttendanceListToHourFormatIfNecessary(attendanceList);
                    return attendanceList;
                }
            },

            //serverPaging: true,
            //serverSorting: true,
            error: ShowDataSourceError,
            serverFiltering: true,
            pageSize: 10,
            //filter: { field: "IsSummary", operator: "eq", value:false}
            filter: [
                { field: "DateIni", operator: "eq", value: e.data.DateIni },
                { field: "DateEnd", operator: "eq", value: e.data.DateEnd },
                { field: "ClsSectionGuid", operator: "eq", value: sectionGuid },
                { field: "StuEnrollmentId", operator: "eq", value: enrollmentGuid }
            ]
        });

        return dataSource;
    }

    // Use this to Post minutes to database.
    export function PostMinutesClockAttendance(
        attendanceGuid: string,
        classSectionMeetingGuid: string,
        attendanceDate: any,
        periodId: string,
        sectionGuidId: string,
        enrollmentGuid: string,
        actual: number,
        makeup: number,
        isTardy: boolean,
        isExcused: boolean,
        schedule: number
        ) {
        $.ajax({
            url: XPOST_STUDENT_ATTENDANCE_MINUTES_UPDATE,
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(
                {
                    IdGuid: attendanceGuid,
                    ClassSectionMeetingGuid: classSectionMeetingGuid,
                    MeetDate: attendanceDate,
                    PeriodIdGuid: periodId,
                    ClsSectionIdGuid: sectionGuidId,
                    EnrollmentGuid: enrollmentGuid,
                    IsTardy: isTardy,
                    IsExcused: isExcused,
                    MakeUp: makeup,
                    Actual: actual,
                    Schedule: schedule
                }),
            success: data=> {
                alert(data[0] + " " + data[1]);
            },
            error: e=> {
                ShowDataSourceError(e);
            }
        });
    }

    export function validateUserInput(models) {
        // Try to parse the number according to the type of Attendance
        var datasection = $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList");
        var selectedItem = datasection.dataItem(datasection.select());
        var actual: number;
        var schedule: number;
        var makeup: number;
        var postedActual: number;
        var postedSchedule: number;
        var postedMakeUp: number;

        // Convert to original format
        if (selectedItem.AttendanceType.toUpperCase() == XATTENDANCE_CLOCKHOURS) {
            // Is clock hours
            actual = parseFloat(models.Actual);
            schedule = parseFloat(models.Schedule);
            makeup = parseFloat(models.MakeUp);
            postedActual = Math.floor(actual * 60);
            postedSchedule = Math.floor(schedule * 60);
            postedMakeUp = Math.floor(makeup * 60);


        } else {
            // Is minutes...
            actual = parseInt(models.Actual);
            schedule = parseInt(models.Schedule);
            makeup = parseInt(models.MakeUp);
            postedActual = actual;
            postedSchedule = schedule;
            postedMakeUp = makeup;
        }
        // Determine validation
        if (isNaN(actual) || isNaN(schedule)) {
            alert("Wrong Format in number. Operation is canceled");
            return { status: false, actual: null, schedule: null, makeup: null };
        }

        if (actual > schedule) {
            alert("Present Attendance can not be higher that schedule!");
            return { status: false, actual: null, schedule: null, makeup: null };
        }

        return { status: true, actual: postedActual, schedule: postedSchedule, makeup: postedMakeUp };
    }

    //#endregion

    //#region Enrollment Summary DataSources 
    // Get the dats for the summary
    export function GetStudentAttendanceEnrollmentSummaryDataSource(kendo) {
        var url = XGET_STUDENT_ATTENDANCE_SUMMARY_ENROLLMENT;
        var dataSource = new kendo.data.DataSource({
            type: "json",

            transport: {
                read: url,
                cache: true,
                parameterMap: (options, operation) => {
                    if (options == null || options.filter === undefined) {
                        return null;
                    }

                    return {
                        "StuEnrollmentId": options.filter.filters[0].StuEnrollmentId, "CampusId": options.filter.filters[0].CampusId,
                    };
                },
            },

            schema: {
                parse: response=> {
                    var summary = response.SummaryEnrollmentTableList;
                    $("#StudentAttendanceTotalScheduled").text(response.ScheduleHours);
                    $("#StudentAttendancePercentagePresent").text(response.PercentagePresent.toFixed(2) + " %");

                    if (response.SummaryEnrollmentStatusList.length > 1 && response.SummaryEnrollmentStatusList[0].DateIni.indexOf("-") != -1) {
                        response.SummaryEnrollmentStatusList.sort((a, b) => parseISO8601(a.DateIni).getTime() - parseISO8601(b.DateIni).getTime());
                    };
                    ConvertDateToStrings(response.SummaryEnrollmentLoaList);
                    ConvertDateToStrings(response.SummaryEnrollmentStatusList);
                    ConvertDateToStrings(response.SummarySuspensionList);
                    $("#StudentAttendanceLoaGrid").kendoGrid({
                        dataSource: response.SummaryEnrollmentLoaList,
                        columns: [
                            {
                                field: "DateIni",
                                title: "Begin Date",
                            },
                            {
                                field: "DateEnd",
                                title: "End Date",
                            },
                            {
                                field: "DateRequested",
                                title: "Requested",

                            },
                            {
                                field: "StatusReason",
                                title: "Reason"
                            }
                        ]
                    });
                    $("#StudentAttendanceEnrollmentChanges").kendoGrid({
                        dataSource: response.SummaryEnrollmentStatusList,

                        columns: [
                            {
                                field: "DateIni",
                                title: "Begin Date",
                            },
                            {
                                field: "DateEnd",
                                title: "End Date",
                            },
                            {
                                field: "DateRequested",
                                title: "Requested",

                            },
                            {
                                field: "StatusReason",
                                title: "New Status"
                            }
                        ]
                    });

                    $("#StudentAttendanceSuspendedDays").kendoGrid({
                        dataSource: response.SummarySuspensionList,

                        columns: [
                            {
                                field: "DateIni",
                                title: "Begin Date",
                            },
                            {
                                field: "DateEnd",
                                title: "End Date",
                            },
                            {
                                field: "DateRequested",
                                title: "Requested",

                            },
                            {
                                field: "StatusReason",
                                title: "Reason"
                            }
                        ]
                    });

                    return summary;
                }
            },

            error: (e) => {
                ShowDataSourceError(e);
                $("#StudentAttendanceSummaryContainer").css("visibility", "hidden");
            },
            serverFiltering: true,
            pageSize: 10,
        });

        return dataSource;
    }

    export function GetEnrollmentSummaryInfo_Loa(enrollmentSummaryInfo: any) {
        var dataSource = new kendo.data.DataSource({
            //dataSource: {
                data: enrollmentSummaryInfo,
                schema: {
                    parse: response=> {
                        var summary = response.SummaryEnrollmentLoaList;
                        return summary;
                  //  }
                }
            }
        });
        return dataSource;
    }

    //#endregion

    //#region Make Up Hours

    export function PostMakeUp(id, date, enrollmentId, sectionGuidId, enumeration, makeuptime) {
        $.ajax({
            url: XGET_STUDENT_ATTENDANCE_MAKEUP_POST,
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({ IdGuid: id, MeetDate: date, EnrollmentGuid: enrollmentId, ClsSectionIdGuid: sectionGuidId, StatusEnum: enumeration, MakeUp: makeuptime }),
            success: data=> {
                alert(data[0] + " " + data[1]);
            },
            error: e=> {
                ShowDataSourceError(e);
            }
        });
    }

    export function DeleteMakeUp(id, date, enrollmentId, sectionGuidId, enumeration, makeuptime) {
        $.ajax({
            url: XGET_STUDENT_ATTENDANCE_MAKEUP_DELETE,
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({ IdGuid: id, MeetDate: date, EnrollmentGuid: enrollmentId, ClsSectionIdGuid: sectionGuidId, StatusEnum: enumeration, MakeUp: makeuptime }),
            success: data=> {
                alert(data[0] + " " + data[1]);
            },
            error: e=> {
                ShowDataSourceError(e);
            }
        });
    }


    //#endregion

    //#region Reports...

    export function GetAttendanceReportItems(kendo, campusId: string, userId: string) {
        var url = XGET_STUDENT_ATTENDANCE_REPORTLINK;
        var programVersionDataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: url
                },
                parameterMap: (options, operation) => {
                    return { CampusId: campusId, UserIdGuid: userId };
                }
            },
            error: ShowDataSourceError,
            dataType: "json",
            type: "GET",
            serverFiltering: true
        });

        return programVersionDataSource;
    };

    //#endregion

    //#region Commun function to display errors  .
    export function ShowDataSourceError(e) {
        try {
            if (e.xhr != undefined) {
                if (e.xhr.responseText == undefined) {
                    alert("Error: " + e.xhr.statusText + ", " + e.xhr.responseXML);
                } else {
                    alert("Error: " + e.xhr.statusText + ", " + e.xhr.responseText);
                }
            } else {
                if (e.responseText == undefined) {
                    alert("Error: " + e.statusText + ", " + e.responseXML);
                } else {
                    alert("Error: " + e.statusText + ", " + e.responseText);
                }
            }
        } catch (ex) {
            alert("Server returned an indefined error");
        }
    }



    //#endregion

    //#region Private Process response Absent Present by day and week report
    function ProcessResponsePerDays(response: any) {
        var attendancelist = [];
        var attendance: any = null;
        var datebefore = new Date(1900, 1, 1, 0, 0, 0, 0);
        if (response.length == 0) {
            return attendancelist;
        }

        for (var i = 0; i < response.length; i++) {

            if (i == 0) {

                // Create Record in cero
                attendance = CreateRecordInCero(response[i]);
                // Add values
                attendance = SumAttendance(attendance, response[i].StatusEnum);
                datebefore = response[i].AttendanceDate;
                continue;

            }

            if (datebefore == response[i].AttendanceDate) {
                attendance = SumAttendance(attendance, response[i].StatusEnum);
                datebefore = response[i].AttendanceDate;
                continue;
            }

            // Store Result
            StoreResult(attendancelist, attendance, response[i - 1].AttendanceDate);
            attendance = CreateRecordInCero(response[i]);
            attendance = SumAttendance(attendance, response[i].StatusEnum);
            datebefore = response[i].AttendanceDate;
        }

        StoreResult(attendancelist, attendance, response[response.length - 1].AttendanceDate);

        return attendancelist;
    }

    function ProcessResponsePerWeek(response: any) {
        var attendancelist = [];
        var attendance: any = null;
        var datebefore = "0";
        if (response.length == 0) {
            return attendancelist;
        }

        for (var i = 0; i < response.length; i++) {

            if (i == 0) {

                // Create Record in cero
                attendance = CreateRecordInCero(response[i]);
                // Add values
                attendance = SumAttendance(attendance, response[i].StatusEnum);
                datebefore = getWeekString(response[i].AttendanceDate);
                continue;

            }

            if (datebefore == getWeekString(response[i].AttendanceDate)) {
                attendance = SumAttendance(attendance, response[i].StatusEnum);
                datebefore = getWeekString(response[i].AttendanceDate);
                continue;
            }

            // Store Result
            StoreResultWeek(attendancelist, attendance, response[i - 1].AttendanceDate);
            attendance = CreateRecordInCero(response[i]);
            attendance = SumAttendance(attendance, response[i].StatusEnum);
            datebefore = getWeekString(response[i].AttendanceDate);
        }

        StoreResultWeek(attendancelist, attendance, response[i - 1].AttendanceDate);

        return attendancelist;
    }

    // Process summary per course 
    function ProcessSummaryPerCourse(response: any): any[] {
        var attendancelist = [];
        var attendance: any;
        if (response.length == 0) {
            return attendancelist;
        }

        // Actual record....
        attendance = CreateRecordSummaryInCero();
        for (var i = 0; i < response.length; i++) {
            attendance.Schedule += response[i].Schedule;
            attendance.Absent += response[i].Absent;
            attendance.Excused += response[i].Excused;
            attendance.MakeUp += response[i].MakeUp;
            attendance.Actual += response[i].Actual;
            attendance.Total += response[i].Total;
            attendance.Tardies += response[i].Tardies;
            //attendance.Schedule += response.Schedule;
        }
        attendance.DateDisplay = "Actual";
        attendancelist.push(attendance);
        //Note: Minutes and Clock Hours has not Adjusted or Converted attendance to class level only at Enrollment.

        // If it is clock attendance show information in hours (clock hours)...
        var datasection = $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList");
        var selectedItem = datasection.dataItem(datasection.select());
        if (selectedItem.AttendanceType.toUpperCase() == XATTENDANCE_CLOCKHOURS) {
            attendance.Actual = ConvertMinutesToHourFormat(attendance.Actual);
            attendance.Absent = ConvertMinutesToHourFormat(attendance.Absent);
            attendance.MakeUp = ConvertMinutesToHourFormat(attendance.MakeUp);
            attendance.Total = ConvertMinutesToHourFormat(attendance.Total);
            attendance.Schedule = ConvertMinutesToHourFormat(attendance.Schedule);
        }

        //If present Absent and has tardies politic create a new record.......
        if (selectedItem.AttendanceType.toUpperCase() == XATTENDANCE_PRESENT_ABSENT) {

            // Detect the policy for the class
            var trackconverter = 0;
            var course = $("#StudentAttendanceFilterClass").data("kendoDropDownList");
            var courseitem = course.dataItem(course.select());
            if (courseitem.IsTrackTardies && courseitem.TardiesMakingAbsence > 0) {
                trackconverter = courseitem.TardiesMakingAbsence;
            } else {
                if (selectedItem.TrackTardies == 1 && selectedItem.TardiesMakingAusence > 0) {
                    trackconverter = selectedItem.TardiesMakingAusence;
                }
            }

            if (trackconverter > 0) {

                attendance = CreateRecordSummaryInCero();
                attendance.DateDisplay = "Adjusted";
                var convAbsent = Math.floor(attendancelist[0].Tardies / trackconverter);
                attendance.Tardies = Math.floor(attendancelist[0].Tardies % trackconverter);
                attendance.Absent = attendancelist[0].Absent + convAbsent;
                attendance.MakeUp = attendancelist[0].MakeUp;
                attendance.Excused = attendancelist[0].Excused;
                attendance.Actual = attendancelist[0].Actual - convAbsent;
                attendance.Total = attendance.Actual + attendance.Absent + attendance.MakeUp; 
                attendance.Schedule = attendancelist[0].Schedule;
                attendancelist.push(attendance);

            }



        }


        return attendancelist;
    };

    function CreateRecordInCero(response: any) {
        var dateu = kendo.parseDate(response.AttendanceDate);
        var attendance = {
            //first: 0,
            Schedule: 0,
            Absent: 0,
            Excused: 0,
            MakeUp: 0,
            Actual: 0,
            Total: 0,
            Tardies: 0,
            Unselected: 0,
            DateDisplay: kendo.toString(dateu, "ddd, MM/dd/yy"),
            DateIni: "",
            DateEnd: ""
        };

        return attendance;
    }

    function CreateRecordSummaryInCero() {
        var attendance = {
            //first: 0,
            DateDisplay: "",
            Schedule: 0,
            Absent: 0,
            Excused: 0,
            Tardies: 0,
            Actual: 0,
            MakeUp: 0,
            Total: 0,
        };
        return attendance;
    }

    function SumAttendance(attendance: any, inputdata: any) {
        switch (inputdata) {
            case EnumAttendanceStatus.Absent:
                {
                    attendance.Absent++;
                    break;
                }
            case EnumAttendanceStatus.Excused:
                {
                    // Excused is a Absent with admonistrative status Excused.
                    attendance.Excused++;
                    attendance.Absent++;
                    break;
                }
            case EnumAttendanceStatus.MakeUp:
                {
                    // Make Up
                    attendance.MakeUp++;
                    break;
                }
            case EnumAttendanceStatus.Present:
                {
                    // Present 
                    attendance.Actual++;
                    break;
                }
            case EnumAttendanceStatus.Tardy:
                {
                    // Tardy is a present with administrative status of Tardy
                    attendance.Tardies++;
                    attendance.Actual++;
                    break;
                }
            case EnumAttendanceStatus["No Attendance Posted"]:
                {
                    // This is a non posted attendance.
                    attendance.Unselected++;
                    break;
                }
            default: attendance.Unselected++;
        }

        //if (attendance.first == 0) {
        //    attendance.DateIni = day;
        //    attendance.first = 1;
        //}

        return attendance;
    }

    function StoreResult(list: any, atten: any, day: string) {

        // Calculate the Scheduled 
        atten.Schedule = atten.Absent + atten.Actual + atten.Unselected;
        atten.Total = atten.Absent + atten.Actual+  atten.MakeUp;
        atten.DateIni = day;
        atten.DateEnd = day;
        var dateu = parseISO8601(day);
        atten.DateDisplay = kendo.toString(dateu, "ddd, MM/dd/yy");
        list.push(atten);
    }

    function StoreResultWeek(list: any, atten: any, day: string) {

        // Calculate the Scheduled 
        atten.Schedule = atten.Absent + atten.Actual +  atten.Unselected;
        atten.Total = atten.Absent + atten.Actual + atten.MakeUp;
        var dateu = parseISO8601(day);

        var week = getWeekNumber(dateu);
        var ini = getWeekDate(week[0], week[1], 0);
        var fin = getWeekDate(week[0], week[1], 6);
        atten.DateIni = ini.getUTCFullYear() + '-' + (ini.getUTCMonth() + 1) + '-' + ini.getUTCDate(); //ini.toISOString();
        atten.DateEnd = fin.getUTCFullYear() + '-' + (fin.getUTCMonth() + 1) + '-' + fin.getUTCDate(); //fin.toISOString();
        atten.DateDisplay = kendo.toString(ini, "ddd, MM/dd/yy") + " " + kendo.toString(fin, "ddd, MM / dd / yy");
        list.push(atten);
    }
    //#endregion

    //#region Private Process for Clock and Minutes 

    function ProcessResponsePerDaysMinutes(response: any) {
        var attendancelist = [];
        var attendance: any = null;
        var datebefore = new Date(1900, 1, 1, 0, 0, 0, 0);
        if (response.length == 0) {
            return attendancelist;
        }

        for (var i = 0; i < response.length; i++) {

            if (i == 0) {

                // Create Record in cero
                attendance = CreateRecordInCero(response[i]);
                //attendance.DateDisplay =
                // Add values
                attendance = SumAttendanceMinutes(attendance, response[i]);
                datebefore = response[i].AttendanceDate;
                continue;

            }

            if (datebefore == response[i].AttendanceDate) {
                attendance = SumAttendanceMinutes(attendance, response[i]);
                datebefore = response[i].AttendanceDate;
                continue;
            }

            // Store Result
            StoreResultMinutes(attendancelist, attendance, response[i - 1].AttendanceDate);
            attendance = CreateRecordInCero(response[i]);
            attendance = SumAttendanceMinutes(attendance, response[i]);
            datebefore = response[i].AttendanceDate;

        }

        StoreResultMinutes(attendancelist, attendance, response[response.length - 1].AttendanceDate);

        return attendancelist;
    }

    function ProcessResponsePerWeekMinutes(response: any) {
        var attendancelist = [];
        var attendance: any = null;
        var datebefore = "0";
        if (response.length == 0) {
            return attendancelist;
        }

        for (var i = 0; i < response.length; i++) {

            if (i == 0) {

                // Create Record in cero
                attendance = CreateRecordInCero(response[i]);
                // Add values
                attendance = SumAttendanceMinutes(attendance, response[i]);
                datebefore = getWeekString(response[i].AttendanceDate);
                continue;

            }

            if (datebefore == getWeekString(response[i].AttendanceDate)) {
                attendance = SumAttendanceMinutes(attendance, response[i]);
                datebefore = getWeekString(response[i].AttendanceDate);
                continue;
            }

            // Store Result
            StoreResulMinutestWeek(attendancelist, attendance, response[i - 1].AttendanceDate);
            attendance = CreateRecordInCero(response[i]);
            attendance = SumAttendanceMinutes(attendance, response[i]);
            datebefore = getWeekString(response[i].AttendanceDate);
        }

        StoreResulMinutestWeek(attendancelist, attendance, response[i - 1].AttendanceDate);

        return attendancelist;
    }

    function SumAttendanceMinutes(attendance: any, responsevar: any) {
        attendance.Schedule += responsevar.Schedule;
        attendance.Actual += responsevar.Actual;
        attendance.MakeUp += responsevar.MakeUp;
        attendance.Tardies += responsevar.Tardies;
        attendance.Excused += responsevar.Excused;
        attendance.Absent += responsevar.Absent;
        return attendance;
    }

    function StoreResultMinutes(list: any, atten: any, day: string) {

        // Calculate the Scheduled 
        //atten.Schedule = atten.Absent + atten.Actual + atten.Tardies + atten.Excused + atten.Unselected;
        atten.Total = atten.Actual + atten.MakeUp + atten.Absent;
        atten.DateIni = day;
        atten.DateEnd = day;
        var dateu = kendo.parseDate(day);
        atten.DateDisplay = kendo.toString(dateu, "ddd, MM/dd/yy");
        list.push(atten);
    }

    function StoreResulMinutestWeek(list: any, atten: any, day: string) {

        // Calculate the Scheduled 
        //atten.Schedule = atten.Absent + atten.Actual + atten.Tardies + atten.Excused + atten.Unselected;
        atten.Total = atten.Actual + atten.MakeUp + atten.Absent;
        var dateu = kendo.parseDate(day);

        //var dateu = new Date(day);
        //dateu.setMinutes(new Date(day).getTimezoneOffset());
        var week = getWeekNumber(dateu);
        var ini = getWeekDate(week[0], week[1], 0);
        var fin = getWeekDate(week[0], week[1], 6);
        atten.DateIni = ini.getUTCFullYear() + '-' + (ini.getUTCMonth() + 1) + '-' + ini.getUTCDate(); //ini.toISOString();
        atten.DateEnd = fin.getUTCFullYear() + '-' + (fin.getUTCMonth() + 1) + '-' + fin.getUTCDate(); //fin.toISOString();
        atten.DateDisplay = kendo.toString(ini, "ddd, MM/dd/yy") + " " + kendo.toString(fin, "ddd, MM / dd / yy");
        list.push(atten);
    }

    //function StoreResultWeekMinutes(list: any, atten: any, day: string) {

    //    // Calculate the Scheduled 
    //    //atten.Schedule = atten.Actual + atten.MakeUp + atten.Absent;
    //    atten.Total = atten.Actual + atten.MakeUp + atten.Absent;
    //    var dateu = kendo.parseDate(day);
    //    var week = getWeekNumber(dateu);
    //    var ini = getWeekDate(week[0], week[1], 0);
    //    var fin = getWeekDate(week[0], week[1], 6);
    //    atten.DateIni = ini.getUTCFullYear() + '-' + (ini.getUTCMonth() + 1) + '-' + ini.getUTCDate(); //ini.toISOString();
    //    atten.DateEnd = fin.getUTCFullYear() + '-' + (fin.getUTCMonth() + 1) + '-' + fin.getUTCDate(); //fin.toISOString();
    //    atten.DateDisplay = kendo.toString(ini, "ddd, MM/dd/yy") + " " + kendo.toString(fin, "ddd, MM/dd/yy");
    //    list.push(atten);
    //}


    //#endregion

    //#region Private Help date functions.
    /* For a given date, get the ISO week number. Algorithm is to find nearest thursday, it's year is the year of the week number. 
    *  Then get weeks between that date and the first day of that year.
    * Note that dates in one year can be weeks of previous or next year, overlap is up to 3 days.
    * e.g. 2014/12/29 is Monday in week  1 of 2015
    *      2012/1/1   is Sunday in week 52 of 2011
    */
    function getWeekNumber(d: Date) {
        // Copy date so don't modify original
        d = new Date(+d);
        d.setHours(0, 0, 0);
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setDate(d.getDate() + 4 - (d.getDay() || 7));
        // Get first day of year
        var yearStart = new Date(d.getFullYear(), 0, 1);
        // Calculate full weeks to nearest Thursday
        var dif = d.valueOf() - yearStart.valueOf();
        var weekNo = Math.ceil(((dif / 86400000) + 1) / 7);
        // Return array of year and week number
        //var weekyear = "Week: " + weekNo + " of " + d.getFullYear();
        //return weekyear;
        return [d.getFullYear(), weekNo];
    }

    function getWeekString(d: string) {
        var week = getWeekNumberfromUTCString(d);
        var result = week[0].toString() + week[1].toString();
        return result;
    }

    /**Parses string formatted as YYYY-MM-DD to a Date object.
    * If the supplied string does not match the format, an 
    * invalid Date (value NaN) is returned.
    * @param {string} dateStringInRange format YYYY-MM-DD, with year in
    * range of 0000-9999, inclusive.
    * @return {Date} Date object representing the string.
    */
    function parseISO8601(dateStringInRange) {
        var dateStr = dateStringInRange; //returned from mysql timestamp/datetime field
        var a = dateStr.split("T");
        var d = a[0].split("-");
        var t = a[1].split(":");
        var date = new Date(Number(d[0]), (Number(d[1]) - 1), Number(d[2]), Number(t[0]), Number(t[1]), Number(t[2]));
        return date;
    }

    function getWeekDate(year, weekNumber, dayNumber) {
        var date = new Date(year, 0, 10, 0, 0, 0),
            day = new Date(year, 0, 4, 0, 0, 0),
            month = day.getTime() - date.getDay() * 86400000;
        return new Date(month + ((weekNumber - 1) * 7 + dayNumber) * 86400000);
    }

    // Return the week of the year for a UTC date string.
    function getWeekNumberfromUTCString(dat: string) {
        var dateu = parseISO8601(dat);
        var timeoffset = dateu.getTimezoneOffset();
        dateu.setMinutes(timeoffset);
        var result = getWeekNumber(dateu);
        return result;
    }

    function ConvertDateToStrings(list) {
        if (list.length > 0) {
            for (var i = 0; i < list.length; i++) {
                if (list[i].DateEnd != null && list[i].DateEnd.indexOf("-") != -1) {
                    var dateu = parseISO8601(list[i].DateEnd);
                    list[i].DateEnd = kendo.toString(dateu, "ddd, MM/dd/yy");
                }

                if (list[i].DateRequested != null && list[i].DateRequested.indexOf("-") != -1) {
                    var dater = parseISO8601(list[i].DateRequested);
                    list[i].DateRequested = kendo.toString(dater, "ddd, MM/dd/yy");
                }

                if (list[i].DateIni.indexOf("-") != -1) {
                    var datex = parseISO8601(list[i].DateIni);
                    list[i].DateIni = kendo.toString(datex, "ddd, MM/dd/yy");
                }
            }
        }
        return list;
    };

    function ConvertMinutesToHourFormat(minutes: number) {
        if (minutes == 0) {
            return "0";
        }
        var hours = (minutes / 60).toFixed(2);
        return hours;
    }

    function ConvertAttendanceListToHourFormatIfNecessary(list: any) {
        // If it is clock attendance show information in hours...
        var datasection = $("#StudentAttendanceFilterEnrollment").data("kendoDropDownList");
        var selectedItem = datasection.dataItem(datasection.select());
        if (selectedItem.AttendanceType.toUpperCase() == XATTENDANCE_CLOCKHOURS) {
            for (var i = 0; i < list.length; i++) {
                list[i].Actual = ConvertMinutesToHourFormat(list[i].Actual);
                list[i].Absent = ConvertMinutesToHourFormat(list[i].Absent);
                list[i].MakeUp = ConvertMinutesToHourFormat(list[i].MakeUp);
                list[i].Total = ConvertMinutesToHourFormat(list[i].Total);
                list[i].Schedule = ConvertMinutesToHourFormat(list[i].Schedule);
            }
        }

        return list;
    }

    //#endregion

    //#region Helpers

    // WARNING does not use this function in the initial load, maybe has not the value
    // for use in click button function that you are sure that the data was collected.
    export function GetAttendanceType() {
        var datasection = $("#StudentAttendanceFilterClass").data("kendoDropDownList");
        var selectedItem = datasection.dataItem(datasection.select());
        var setting = selectedItem.UnitTypeDescription;
        return setting;
    }

    //#endregion
} 