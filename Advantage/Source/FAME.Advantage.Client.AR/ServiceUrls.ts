// This has all references to API.WEB in Advantage.....
module StudentAttendance {
  
    // Student Attendance WEB API URLs..........
    export var XGET_STUDENT_ATTENDANCE_TERM_DROPDOWN = "../proxy/api/Students/Enrollments/Terms/GetDdTermItems";
    export var XGET_STUDENT_ATTENDANCE_COURSE_DROPDOWN = "../proxy/api/Students/Enrollments/CourseClass/GetCourseClass";
    export var XGET_STUDENT_ATTENDANCE_GRID = "../proxy/api/Students/Enrollments/StudentAttendance/GetAttendanceAbsentPresent";
    export var XGET_STUDENT_ATTENDANCE_GRID_CLOCK = "../proxy/api/Students/Enrollments/StudentAttendance/GetAttendanceMinutes"; 
    export var XGET_STUDENT_ATTENDANCE_SUMMARY_ENROLLMENT = "../proxy/api/Students/Enrollments/StudentAttendance/GetAttendanceEnrollmentSummary";
    export var XGET_STUDENT_ATTENDANCE_MAKEUP_POST = "../proxy/api/Students/Enrollments/StudentAttendance/PostMakeUp";
    export var XGET_STUDENT_ATTENDANCE_MAKEUP_DELETE = "../proxy/api/Students/Enrollments/StudentAttendance/DeleteMakeUp";
    export var XGET_STUDENT_ATTENDANCE_REPORTLINK = "../proxy/api/Students/Enrollments/StudentAttendance/GetStudentAttendanceReportLink";
    export var XGET_STUDENT_ATTENDANCE_UPDATE_POST = "../proxy/api/Students/Enrollments/StudentAttendance/PostAttendanceAbsentPresent";
    export var XPOST_STUDENT_ATTENDANCE_MINUTES_UPDATE = "../proxy/api/Students/Enrollments/StudentAttendance/PostAttendanceMinutes";
    export var XGET_STUDENTATTENDANCE_ENROLLMENT_DROPDOWN = "../proxy/api/Students/Enrollments/StudentAttendance/GetCbItemsEnrollments";

}