﻿namespace WapiDll.Helpers
{
    /// <summary>
    /// Auxiliary helper routines.
    /// </summary>
    public static class Tools
    {
        /// <summary>
        /// Test if a string is Json or not.
        /// it is not a exhaustive format test, it is only to identify it.
        /// </summary>
        /// <param name="input">The Json to test.</param>
        /// <returns>True if it is Json.</returns>
        public static bool IsJson(string input)
        {
            input = input.Trim();
            return input.StartsWith("{") && input.EndsWith("}")
                   || input.StartsWith("[") && input.EndsWith("]");
        } 

    }
}
