﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Impersonator.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Impersonator file.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll.Helpers
{
    using System.Security.Principal;

    /// <summary>
    /// Impersonator class
    /// </summary>
    public class Impersonator : System.IDisposable
    {
        ///LOGON32_PROVIDER_DEFAULT///
        protected const int LOGON32_PROVIDER_DEFAULT = 0;
        ///LOGON32_LOGON_INTERACTIVE///
        protected const int LOGON32_LOGON_INTERACTIVE = 2;

        ///Identity///
        public WindowsIdentity Identity = null;
        private System.IntPtr m_accessToken;


        [System.Runtime.InteropServices.DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool LogonUser(string lpszUsername, string lpszDomain,
        string lpszPassword, int dwLogonType, int dwLogonProvider, ref System.IntPtr phToken);

        [System.Runtime.InteropServices.DllImport("kernel32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        private extern static bool CloseHandle(System.IntPtr handle);

        ///Constructor///
        public Impersonator()
        {
            this.Identity = WindowsIdentity.GetCurrent();
        }

        ///Constructor///
        public Impersonator(string username, string domain, string password)
        {
            Login(username, domain, password);
        }

        /// <summary>
        /// impersonate user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="domain"></param>
        /// <param name="password"></param>
        public void Login(string username, string domain, string password)
        {
            if (this.Identity != null)
            {
                this.Identity.Dispose();
                this.Identity = null;
            }


            try
            {
                this.m_accessToken = new System.IntPtr(0);
                Logout();

                this.m_accessToken = System.IntPtr.Zero;
                bool logonSuccessfull = LogonUser(
                   username,
                   domain,
                   password,
                   LOGON32_LOGON_INTERACTIVE,
                   LOGON32_PROVIDER_DEFAULT,
                   ref this.m_accessToken);

                if (!logonSuccessfull)
                {
                    int error = System.Runtime.InteropServices.Marshal.GetLastWin32Error();
                    throw new System.ComponentModel.Win32Exception(error);
                }
                Identity = new WindowsIdentity(this.m_accessToken);
            }
            catch
            {
                throw;
            }

        } // End Sub Login 

        /// <summary>
        /// stop impersonating user
        /// </summary>
        public void Logout()
        {
            if (this.m_accessToken != System.IntPtr.Zero)
                CloseHandle(m_accessToken);

            this.m_accessToken = System.IntPtr.Zero;

            if (this.Identity != null)
            {
                this.Identity.Dispose();
                this.Identity = null;
            }

        } // End Sub Logout 


        void System.IDisposable.Dispose()
        {
            Logout();
        } // End Sub Dispose 


    } // End Class WindowsLogin 


} // End Namespace 