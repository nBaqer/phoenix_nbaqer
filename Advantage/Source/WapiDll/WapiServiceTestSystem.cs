﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Timers;
using WapiDll.Data.Contract;

namespace WapiDll
{
    /// <summary>
    /// This is a test operation
    /// </summary>
    public class WapiServiceTestSystem: WapiService 
    {
        #region Overrides of WapiService

        /// <summary>
        /// Use this to create a instance of this class.
        /// Do not use the default constructor
        /// </summary>
        /// <param name="operation">The operation to be executed by the module</param>
        /// <param name="listOfServices">The list of existent module - operations</param>
        /// <returns></returns>
        public static IWapiService Factory(IWapiOperationsSettings operation, ref IList<IWapiService> listOfServices)
        {
            IWapiService service = new WapiServiceTestSystem();
            service.Operation = operation;
            service.Operation.Tracer = (service.Operation.Tracer) ?? new TraceSource("TraceWapiWs");
            // Create the interval of time
            service.ServiceTimer.Interval = PositiveTimerIntervalCalculation(operation) * 1000;
            service.WapiServiceList = listOfServices;
            return service;
        }

        /// <summary>
        /// This test operation does not update the interval of operation to now + interval 
        /// when a On Demand Operation is executed
        /// </summary>
        /// <param name="sender">The timer that raise the event</param>
        /// <param name="e"><see cref="ElapsedEventArgs"/></param>
        protected override void ServiceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            base.ServiceTimer_Elapsed(sender, e);
            var timer = (Timer)sender;
            timer.Stop();
            try
            {
                ExecuteActions();
                Debug.WriteLine(string.Format("Operation: {0} success", Operation.CodeOperation));
                Operation.DateLastExecution = DateTime.Now;
                PostDateLastOperation(Operation.ID, Operation.DateLastExecution);
                PostLogToServer(Operation, string.Format("Operation: {0} success", Operation.CodeOperation),0);
                Operation.Tracer.TraceInformation(string.Format("Operation: {0} success", Operation.CodeOperation));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("Error in Operation: {0} stack trace: {1}{2}", ex.Message, ex.StackTrace, Environment.NewLine));
                Operation.DateLastExecution = Operation.DateLastExecution.AddMinutes(1);
                PostLogToServer(Operation, string.Format("Operation: {0} is delayed 1 minute because throw Exception {1}. ", Operation.CodeOperation, ex.Message), 1);
                Operation.Tracer.TraceData(TraceEventType.Error, 300, string.Format("Error in Operation: {0} stack trace: {1}{2}", ex.Message,  ex.StackTrace, Environment.NewLine));
            }
            finally
            {
                var interval = PositiveTimerIntervalCalculation(Operation);
                timer.Interval = interval * 1000;
                timer.Start();
            }
        }
        
        /// <summary>
        /// This should contain the payload of the time-elapsed code.
        /// to be executed by external node.
        /// </summary>
        public override void ExecuteActions()
        {
            Debug.WriteLine(string.Format("Operation {0} begin", Operation.CodeOperation));
            // Get the info from Proxy....
            var payload = Proxy.GetMessageFromWapi(Operation.CodeWapiServ, string.Empty);
           // var payload = WapiProxyDialogExchangeGet(Operation.CodeWapiServ, string.Empty);
            Debug.WriteLine(payload);
        }

        #endregion
    }
}
