﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiServiceAfaIntegration.cs" company="FAMe INC">
//   2018
// </copyright>
// <summary>
//   Defines the WapiServiceAfaIntegration type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.Api.Library.Models.AcademicRecords;

namespace WapiDll
{
    using Fame.Integration.Adv.Afa.Messages.WebApi.Entities;
    using FAME.Advantage.Api.Library;
    using FAME.Advantage.Api.Library.Admissions;
    using FAME.Advantage.Api.Library.Models;
    using FAME.Advantage.Api.Library.Models.Admissions;
    using FAME.Advantage.Api.Library.Models.AFA.Demographics;
    using FAME.Advantage.Api.Library.Models.StudentAccounts;
    using FAME.Advantage.Api.Library.Models.SystemCatalog;
    using FAME.AFA.Api.Library.AcademicRecords;
    using FAME.AFA.Api.Library.AFA;
    using FAME.Integration.Adv.Afa.Messages.Infrastructure;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Linq;
    using System.Net.Http;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using System.Timers;
    using System.Web.UI.WebControls;

    using FAME.Advantage.Api.Library.Afa;

    using WapiDll.Data.Contract;

    /// <summary>
    /// The WAPI service AFA integration.
    /// </summary>
    public class WapiServiceAfaIntegration : WapiService
    {
        /// <summary>
        /// Use this to create a instance of the class <br/>
        /// Do not use the implicit constructor.
        /// </summary>
        /// <param name="operation">
        /// The name of the external operation
        /// </param>
        /// <param name="listOfServices">
        /// List of object of other services running in the Advantage Windows Service
        /// </param>
        /// <returns>
        /// A instance of the class
        /// </returns>
        public static IWapiService Factory(IWapiOperationsSettings operation, ref IList<IWapiService> listOfServices)
        {
            IWapiService service = new WapiServiceAfaIntegration();
            service.Operation = operation;
            service.Operation.Tracer = service.Operation.Tracer ?? new TraceSource("TraceWapiWs");
            // Create the interval of time
            service.ServiceTimer.Interval = WapiService.PositiveTimerIntervalCalculation(operation) * 1000;
            service.WapiServiceList = listOfServices;
            return service;
        }

        public override void ExecuteActions()
        {
        }
        /// <summary>
        /// The execute actions.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// will implement shortly
        /// </exception>
        public override void ExecuteActions(IWapiOperationsSettings operation)
        {
            //597000
            //List<string> campusesToSync = new List<string>() { "567000", "709300" };// query or operation.
            // get cmsIds from db where afaintegration is on
            IWapiService advantageApiService1 = this.WapiServiceList.FirstOrDefault(x => x.Operation.CodeExtOperationMode == "ADVANTAGE_API_SERVICE" && x.Operation.SecretCodeCompany == operation.SecretCodeCompany);

            if (advantageApiService1 != null)
            {
                List<string> campusesToSync = new List<string>();// { "597000", "709300" }; // query or operation.

                // get api token
                TokenResponse advApiToken1 = this.GetAdvantageApiToken(advantageApiService1.Operation);
                if (advApiToken1.ResponseStatusCode == 200)
                {
                    // call api to get cmsid active
                    var afaRequest = new AfaIntegrationRequest(
                        advantageApiService1.Operation.ExternalUrl,
                        advApiToken1.Token);
                    campusesToSync = afaRequest.GetCmsIdsToSync();

                    if (campusesToSync != null)
                    {
                        if (campusesToSync.Any())
                        {
                            this.PostLogToServer(this.Operation, $"AfaIntegrationRequest returned Campuses to sync at {DateTime.Now}!", 0);
                        }
                        else
                        {
                            this.PostLogToServer(this.Operation, $"AfaIntegrationRequest Failed to returned List of Cms Id to Sync at {DateTime.Now}!", 1);
                        }

                        foreach (var cmsId in campusesToSync)
                        {
                            try
                            {
                                var demoReq = new DemographicsRequest(
                                    Operation.ConsumerKey,
                                    Operation.UserName,
                                    Operation.PrivateKey,
                                    Operation.ExternalUrl);
                                var demographics = demoReq.GetDemographics(cmsId);


                                // process the leads if we get any from the 1st call
                                if (demographics.Any())
                                {
                                    this.PostLogToServer(this.Operation, $"AfaIntegration Demographics Updates were found at {DateTime.Now}!", 0);

                                    // the first call will give you all the AFA students and you have to check to see if it exists or not using the names and SSN. 
                                    // If you find a match with all three fields, then you update the existing adv lead with the AFA Student ID. 
                                    // For anything that doesn't match, you will produce a list (csv or whatever) so we can take a look at it and decide what to do next. 
                                    // After that (second call) you will always create a lead if it doesn't exist, update a lead with the AFA Student ID if it does exist, but has no AFA student ID, or do nothing if the lead already exists and has an AFA student ID already.

                                    IWapiService advantageApiService = advantageApiService1;

                                    if (advantageApiService != null)
                                    {
                                        // get api token
                                        TokenResponse advApiToken =
                                            this.GetAdvantageApiToken(advantageApiService.Operation);
                                        if (advApiToken.ResponseStatusCode == 200)
                                        {
                                            // call api to get catalog details for insert into lead.
                                            var leadRequest = new LeadRequest(
                                                advantageApiService.Operation.ExternalUrl,
                                                advApiToken.Token);
                                            var catalogData = leadRequest.GetCatalog();

                                            if (catalogData != null)
                                            {
                                                this.PostLogToServer(this.Operation, $"AfaIntegration Catalog Data was loaded from the API at {DateTime.Now}!", 0);
                                            }
                                            else
                                            {
                                                this.PostLogToServer(this.Operation, $"AfaIntegration Catalog Data returned null from the API at {DateTime.Now}!", 1);
                                            }

                                            // call api service to check if the lead exists with the AFA studentId
                                            foreach (var lead in demographics)
                                            {
                                                // call to check if the lead exists (first name last name and ssn) for the campusId where cmsid matches.
                                                // insert into Adleads with AFA studentID
                                                LeadRequest lr = new LeadRequest(
                                                    advantageApiService.Operation.ExternalUrl,
                                                    advApiToken.Token);

                                                var stuExist = lr.CheckIfAfaStudentExist(
                                                    lead.FirstName.Trim(),
                                                    lead.LastName.Trim(),
                                                    lead.SSN,
                                                    lead.LocationCMSID,
                                                    lead.IDStudent);

                                                // if flag is false then insert in the exception table
                                                // if flag is true then do nothing
                                                if (stuExist != null)
                                                {
                                                    if (!stuExist.AfaStudentIdExists)
                                                    {
                                                        // update adleads with the AFAStudentId & leadID given only if result status is 'AfaStudentIdNotFoundButLeadFound'
                                                        if (stuExist.ResultStatusMessage.Equals("Afa StudentId not found and Lead found."))
                                                        {
                                                            LeadBase leadUpdate = new LeadBase();
                                                            leadUpdate.AfaStudentId = stuExist.AfaStudentId;
                                                            leadUpdate.LeadId = stuExist.LeadId;
                                                            leadUpdate.FirstName = lead.FirstName.Trim();
                                                            leadUpdate.LastName = lead.LastName.Trim();
                                                            var updateStudentId = lr.UpdateAfaStudentId(leadUpdate);
                                                            if (updateStudentId != null)
                                                            {
                                                                Debug.WriteLine(
                                                                    "Lead " + leadUpdate.LastName + " "
                                                                    + leadUpdate.FirstName
                                                                    + " was updated with AFAStudentId ");
                                                            }
                                                            else
                                                            {
                                                                this.PostLogToServer(this.Operation, $"SyncDemographics:There was error in updating AfaStudentId of {leadUpdate.LastName}, {leadUpdate.FirstName} in Advantage at {DateTime.Now}. {Environment.NewLine}", 1);
                                                            }

                                                            // make a call to sync the enrollment of that lead wih afa if that lead already has got enrollments.
                                                            var enrollments = lr.GetAllLeadEnrollmentsForAfaSync((Guid)stuExist.LeadId).ToList();
                                                            if (enrollments != null)
                                                            {
                                                                if (enrollments.Any())
                                                                {
                                                                    this.PostLogToServer(this.Operation, $"AfaIntegration Enrollments were for student {leadUpdate.LastName}, {leadUpdate.FirstName} and is ready to be sync at {DateTime.Now}!", 0);

                                                                    // update the enrollment to afa
                                                                    var resultUpdateEnrollment = lr.PostAllEnrollmentsToAfa(enrollments);

                                                                    if (!string.IsNullOrEmpty(resultUpdateEnrollment))
                                                                    {
                                                                        this.PostLogToServer(this.Operation, $"AfaIntegration Enrollment Synced for student {leadUpdate.LastName}, {leadUpdate.FirstName} with status result {resultUpdateEnrollment} at {DateTime.Now}!", 1);
                                                                    }

                                                                    // update the attendance to afa
                                                                    foreach (var enrollment in enrollments)
                                                                    {
                                                                        // get attendance
                                                                        var resultAttendance = lr.getAttendanceForAfa(enrollment.SISEnrollmentID);

                                                                        if (resultAttendance != null)
                                                                        {
                                                                            this.PostLogToServer(this.Operation, $"AfaIntegration Attendance Update was found for student {leadUpdate.LastName}, {leadUpdate.FirstName} Enrollment: {enrollment.SISEnrollmentID} and is ready to be sync at {DateTime.Now}!", 0);

                                                                            var resultUpdatedAttendance = lr.PostAttendanceToAfa(new List<Attendance>() { resultAttendance });

                                                                            if (!string.IsNullOrEmpty(
                                                                                    resultUpdatedAttendance))
                                                                            {
                                                                                this.PostLogToServer(this.Operation, $"AfaIntegration Attendance Posted for student {leadUpdate.LastName}, {leadUpdate.FirstName} Enrollment: {enrollment.SISEnrollmentID} with status:{resultUpdatedAttendance} at {DateTime.Now}!", 0);
                                                                            }
                                                                            else
                                                                            {
                                                                                this.PostLogToServer(this.Operation, $"AfaIntegration Attendance Posted but not status was returned for student {leadUpdate.LastName}, {leadUpdate.FirstName} Enrollment: {enrollment.SISEnrollmentID} at {DateTime.Now}!", 1);
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            this.PostLogToServer(this.Operation, $"AfaIntegration No Attendance Update found for student {leadUpdate.LastName}, {leadUpdate.FirstName} Enrollment: {enrollment.SISEnrollmentID}  at {DateTime.Now}!", 0);
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    this.PostLogToServer(this.Operation, $"AfaIntegration Enrollments NOT found for student {leadUpdate.LastName}, {leadUpdate.FirstName} at {DateTime.Now}!", 1);
                                                                }

                                                            }
                                                        }

                                                        if (stuExist.ResultStatusMessage.Equals("Lead not found.") || stuExist.ResultStatusMessage.Equals("New lead created because Lead found with different ssn and afa studentId."))
                                                        {
                                                            this.PostLogToServer(this.Operation, $"AfaIntegration Creating New Lead {lead.LastName}, {lead.FirstName} at {DateTime.Now}!", 0);

                                                            // get campus ID depending on the cmsID being passed
                                                            var campusId =
                                                                leadRequest.GetCampusIdForCmsId(lead.LocationCMSID);

                                                            if (campusId != Guid.Empty)
                                                            {
                                                                // create a lead with the afastudentId
                                                                var leadMap = this.CreateLead(lead, catalogData, campusId);
                                                                if (!string.IsNullOrEmpty(leadMap?.FirstName))
                                                                {
                                                                    var createdLead = lr.CreateLead(leadMap);
                                                                    // log error if null
                                                                    if (createdLead == null)
                                                                    {
                                                                        this.PostLogToServer(this.Operation, $"AfaIntegration SyncDemographics:There was error in creating Lead {leadMap.LastName},  {leadMap.FirstName} in Advantage at {DateTime.Now}. {Environment.NewLine}", 1);
                                                                    }
                                                                    else
                                                                    {
                                                                        this.PostLogToServer(this.Operation, $"AfaIntegration Lead {lead.LastName}, {lead.FirstName} Created at {DateTime.Now}!", 0);
                                                                    }
                                                                    // insert in the excption table 
                                                                    if (stuExist.ResultStatusMessage.Equals("New lead created because Lead found with different ssn and afa studentId.")
                                                                    )
                                                                    {
                                                                        // write logic to insert in exception table
                                                                    }
                                                                }

                                                            }
                                                            else
                                                            {
                                                                this.PostLogToServer(this.Operation, $"AfaIntegration SyncDemographics:There was error in getting campusId for cmsId {lead.LocationCMSID} for creating Lead {lead.LastName},  {lead.FirstName} in Advantage at {DateTime.Now}. {Environment.NewLine}", 1);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        this.PostLogToServer(this.Operation, $"AfaIntegration AFA Student id did not exist for student {lead.LastName},  {lead.FirstName} at {DateTime.Now}!", 1);
                                                    }
                                                }
                                                else
                                                {
                                                    this.PostLogToServer(this.Operation, $"SyncDemographics:There was error in CheckIfAfaStudentIdExists for {lead.LastName},  {lead.FirstName} in Advantage at {DateTime.Now}. {Environment.NewLine}", 1);
                                                }
                                            } // ); // uncomment for parrellel for each
                                        }
                                        else
                                        {
                                            this.PostLogToServer(this.Operation, $"SyncDemographics:There was error in getting Advantage API token at {DateTime.Now}. {Environment.NewLine}", 1);
                                        }
                                    }
                                }
                                else
                                {
                                    this.PostLogToServer(this.Operation, $"AfaIntegration Demographics Updates were NOT found at {DateTime.Now}! ", 0);
                                }

                                //var paymentPeriodReq = new FAME.AFA.Api.Library.AcademicRecords.PaymentPeriodRequest(
                                //    Operation.ConsumerKey,
                                //    Operation.UserName,
                                //    Operation.PrivateKey,
                                //    Operation.ExternalUrl);
                                //var paymentPeriods = paymentPeriodReq.GetPaymentPeriods(cmsId);

                                //if (paymentPeriods.Any())
                                //{

                                //    IWapiService advantageApiService =
                                //        this.WapiServiceList.FirstOrDefault(
                                //            x => x.Operation.CodeExtOperationMode == "ADVANTAGE_API_SERVICE");
                                //    if (advantageApiService != null)
                                //    {
                                //        // get api token
                                //        TokenResponse advApiToken =
                                //            this.GetAdvantageApiToken(advantageApiService.Operation);
                                //        if (advApiToken.ResponseStatusCode == 200)
                                //        {
                                //            // call api to update payment periods.
                                //            var paymentPeriodRequest =
                                //                new FAME.Advantage.Api.Library.AcademicRecords.PaymentPeriodRequest(
                                //                    advantageApiService.Operation.ExternalUrl,
                                //                    advApiToken.Token);
                                //            var transformPP =
                                //                paymentPeriods.Select(
                                //                    pp => new PaymentPeriod()
                                //                              {
                                //                                  AfaStudentId = pp.IDStudent,
                                //                                  StudentEnrollmentID =
                                //                                      pp.SISEnrollmentID,
                                //                                  AcademicYearSeqNo = pp.AcadYearSeqNo,
                                //                                  PaymentPeriodName =
                                //                                      pp.PaymentPeriodName,
                                //                                  StartDate = pp.StartDate,
                                //                                  EndDate = pp.EndDate,
                                //                                  WeeksOfInstructionalTime =
                                //                                      pp.WeeksOfInstructionalTime,
                                //                                  HoursCreditEnrolled =
                                //                                      pp.HoursCreditEnrolled,
                                //                                  EnrollmentStatusDescription =
                                //                                      pp.EnrollmentStatusDescription,
                                //                                  HoursCreditEarned =
                                //                                      pp.HoursCreditEarned,
                                //                                  EffectiveDate = pp.EffectiveDate,
                                //                                  TitleIVSapResult = pp.SAP,
                                //                                  DateCreated = pp.DateCreated,
                                //                                  UserCreated = pp.UserCreated,
                                //                                  DateUpdated = pp.DateUpdated,
                                //                                  UserUpdated = pp.UserUpdated,
                                //                                  DeleteRecord = pp.Deleted
                                //                              });
                                //            var res = Task
                                //                .Run(
                                //                    () => paymentPeriodRequest.UpdateAfaPaymentPeriodsStaging(
                                //                        transformPP)).Result;

                                //        }
                                //    }
                                //}
                                // check for config to call the award sync
                                var disbleAwardsDisbursement =
                                    ConfigurationManager.AppSettings.Get("DiasbleAwardsDisbursements");
                                if (disbleAwardsDisbursement.ToLower().Equals("no"))
                                {
                                    var awardReq = new FAME.AFA.Api.Library.FinancialAid.StudentAwardRequest(
                                        Operation.ConsumerKey,
                                        Operation.UserName,
                                        Operation.PrivateKey,
                                        Operation.ExternalUrl);
                                    var awards = awardReq.GetTitleIVAwards(cmsId);

                                    if (awards.Any())
                                    {
                                        this.PostLogToServer(this.Operation, $"AfaIntegration Awards are found and ready to sync at {DateTime.Now}!", 0);

                                        /*
                                        * TODO:
                                        * Reused the adv service that's been used already in the top 
                                        */
                                        IWapiService advantageApiService = advantageApiService1;
                                        if (advantageApiService != null)
                                        {
                                            // get api token
                                            TokenResponse advApiToken =
                                                this.GetAdvantageApiToken(advantageApiService.Operation);
                                            if (advApiToken.ResponseStatusCode == 200)
                                            {
                                                // call api to create/update awards.
                                                var studentAwardRequest =
                                                    new FAME.Advantage.Api.Library.FinancialAid.StudentAwardRequest(
                                                        advantageApiService.Operation.ExternalUrl,
                                                        advApiToken.Token);
                                                var transformSA = awards.Select(
                                                    sa =>
                                                        new FAME.Advantage.Api.Library.Models.StudentAccounts.AFA.
                                                        StudentAward()
                                                        {
                                                            AfaStudentId = sa.IDStudent.ToString(),
                                                            StudentEnrollmentId = sa.SISEnrollmentID,
                                                            AwardEndDate = sa.AcadYrEnd,
                                                            AwardStartDate = sa.AcadYrStart,
                                                            AwardStatus = sa.AwardStatus,
                                                            AwardYear = sa.AwardYear,
                                                            TitleIvAwardType = sa.FundCode,
                                                            LoanId = sa.LoanID,
                                                            ModDate = sa.DateUpdated,
                                                            ModUser = sa.UserUpdated,
                                                            FinancialAidId = sa.AwardID,
                                                            Deleted = sa.Deleted
                                                        });
                                                var res = studentAwardRequest.CreateUpdateAdvantageStudentAwards(
                                                    transformSA);

                                                if (res == null)
                                                {
                                                    foreach (var trn in transformSA)
                                                    {
                                                        this.PostLogToServer(
                                                            this.Operation,
                                                            $"CreateUpdateAdvantageStudentAwards:There was error in Create/Update Award for enrollment {trn.StudentEnrollmentId} in Advantage at {DateTime.Now}. {Environment.NewLine}",
                                                            1);
                                                    }
                                                }
                                                else
                                                {
                                                    this.PostLogToServer(this.Operation, $"AfaIntegration Awards returned null results at {DateTime.Now}!", 1);
                                                }
                                            }
                                            else
                                            {
                                                this.PostLogToServer(
                                                    this.Operation,
                                                    $"CreateUpdateAdvantageStudentAwards:There was error in getting Advantage API token at {DateTime.Now}. {Environment.NewLine}",
                                                    1);
                                            }
                                        }
                                        else
                                        {
                                            this.PostLogToServer(this.Operation, $"AfaIntegration Advantage Service not found for Awards Sync at {DateTime.Now}!", 0);
                                        }
                                    }
                                    else
                                    {
                                        this.PostLogToServer(this.Operation, $"AfaIntegration Not Awards found to sync at {DateTime.Now}!", 1);
                                    }

                                    var disbursements = awardReq.GetTitleIVAwardDisbursements(cmsId);

                                    if (disbursements.Any())
                                    {
                                        this.PostLogToServer(this.Operation, $"AfaIntegration Disbursements are found and ready to sync at {DateTime.Now}!", 0);
                                        /*
                                         * TODO:
                                         * Reused the adv service that's been used already in the top 
                                         */
                                        IWapiService advantageApiService = advantageApiService1;
                                        if (advantageApiService != null)
                                        {
                                            // get api token
                                            TokenResponse advApiToken =
                                                this.GetAdvantageApiToken(advantageApiService.Operation);
                                            if (advApiToken.ResponseStatusCode == 200)
                                            {
                                                // call api to create/update awards.
                                                var studentAwardRequest =
                                                    new FAME.Advantage.Api.Library.FinancialAid.StudentAwardRequest(
                                                        advantageApiService.Operation.ExternalUrl,
                                                        advApiToken.Token);
                                                var transformDis = disbursements.Select(
                                                    dis =>
                                                        new
                                                        FAME.Advantage.Api.Library.Models.StudentAccounts.AFA.
                                                        Disbursement()
                                                        {
                                                            CmsId = dis.LocationCMSID,
                                                            AfaStudentId = dis.IDStudent.ToString(),
                                                            PaymentPeriodName = dis.PaymentPeriodName,
                                                            AnticipatedDisbursementDate =
                                                                    dis.AnticipatedDisbursementDate,
                                                            AnticipatedNetDisbursementAmt =
                                                                    dis.AnticipatedNetDisbursementAmt,
                                                            AnticipatedGrossAmount = dis.AnticipatedGrossAmount,
                                                            FeeAmount = dis.FeeAmount,
                                                            ActualDisbursementDate = dis.ActualDisbursementDate,
                                                            ActualNetDisbursementAmt = dis.ActualNetDisbursementAmt,
                                                            ActualFeeAmount = dis.ActualFeeAmount,
                                                            ActualGrossAmount = dis.ActualGrossAmount,
                                                            DisbursementStatus = dis.DisbursementStatus,
                                                            SequenceNumber = dis.DisbursementSequenceNumber,
                                                            DateCreated = dis.DateCreated,
                                                            UserCreated = dis.UserCreated,
                                                            ModDate = dis.DateUpdated,
                                                            ModUser = dis.UserUpdated,
                                                            FinancialAidId = dis.AwardID,
                                                            Deleted = dis.Deleted
                                                        });
                                                var res = studentAwardRequest.CreateUpdateAdvantageDisbursements(
                                                    transformDis);

                                                if (res == null)
                                                {
                                                    foreach (var trn in transformDis)
                                                    {
                                                        this.PostLogToServer(
                                                            this.Operation,
                                                            $"CreateUpdateAdvantageDisbursements:There was error in Create/Update Disbursement for FA_Id {trn.FinancialAidId} in Advantage at {DateTime.Now}. {Environment.NewLine}",
                                                            1);
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                this.PostLogToServer(
                                                    this.Operation,
                                                    $"CreateUpdateAdvantageDisbursements:There was error in getting Advantage API token at {DateTime.Now}. {Environment.NewLine}",
                                                    1);
                                            }
                                        }
                                        else
                                        {
                                            this.PostLogToServer(this.Operation, $"AfaIntegration Advantage Service not found for Awards Sync at {DateTime.Now}!", 0);
                                        }
                                    }
                                    else
                                    {
                                        this.PostLogToServer(this.Operation, $"AfaIntegration No Disbursements found to sync at {DateTime.Now}!", 1);
                                    }
                                }
                                else
                                {
                                    this.PostLogToServer(this.Operation, $"AfaIntegration Awards and Disbursements sync is disabled at {DateTime.Now}!", 1);
                                }

                                var enrollmentRequest = new FAME.Advantage.Api.Library.AcademicRecords.EnrollmentRequest(advantageApiService1.Operation.ExternalUrl, advApiToken1.Token);

                                enrollmentRequest.UpdateAfaEnrollmentStatus(cmsId);
                            }
                            catch (Exception e)
                            {
                                this.PostLogToServer(this.Operation, $"Some sync failed for campusCode {cmsId} in Advantage at {DateTime.Now}. {Environment.NewLine} - Error is {e.Message} . Stack Trace: {e.StackTrace}", 1);
                            }
                        }
                    }
                    else
                    {
                        this.PostLogToServer(this.Operation, $"Campus Id was not found in AFA at {DateTime.Now}!", 0);
                    }
                }
                else
                {
                    this.PostLogToServer(this.Operation, $"Wapi windows for AFA Integration:There was error in getting Advantage API token at {DateTime.Now}. {Environment.NewLine}", 1);
                }
            }

        }

        /// <summary>
        /// The service timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void ServiceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            base.ServiceTimer_Elapsed(sender, e);
            var timer = (Timer)sender;
            timer.Stop();
            try
            {
                this.ExecuteActions(this.Operation);
                var message = $"Operation: {this.Operation.CodeOperation} success.";
                Debug.WriteLine(message);
                this.Operation.DateLastExecution = DateTime.Now;
                this.PostDateLastOperation(this.Operation.ID, this.Operation.DateLastExecution);
                this.PostLogToServer(this.Operation, message, 0);
                this.OptionalMessage = message;
                var interval = PositiveTimerIntervalCalculation(this.Operation);
                timer.Interval = interval * 1000;
                timer.Start();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error in Operation: {ex.Message} StackTrace: {ex.StackTrace}{Environment.NewLine}");
                this.Operation.Tracer.TraceData(
                    TraceEventType.Error,
                    200,
                    $"Error in Operation: {ex.Message}  StackTrace: {ex.StackTrace}{Environment.NewLine}");
                this.Operation.DateLastExecution = DateTime.Now; // this.Operation.DateLastExecution.AddMinutes(10);
                this.PostLogToServer(
                    this.Operation,
                    $"Operation: {this.Operation.CodeOperation} is delayed 10 minutes because throw Exception {ex.Message}  StackTrace: {ex.StackTrace}{Environment.NewLine}.",
                    1);

                // Set 10 minutes the next start
                timer.Interval = 600000;
                timer.Start();
            }
        }

        /// <summary>
        /// The post user login.
        /// </summary>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="AfaSession"/>.
        /// </returns>
        private AfaSession PostUserLogin(IWapiOperationsSettings operation)
        {
            // Create the client
            var client = new HttpClient();
            var payload = new AfaSession();
            payload.ClientKey = operation.ConsumerKey;
            payload.UserName = operation.UserName;
            payload.Password = operation.PrivateKey;

            // Create the headers
            client.DefaultRequestHeaders.Add("ClientKey", operation.ConsumerKey);

            var uri = new Uri(operation.ExternalUrl + "/users/userlogin");
            HttpResponseMessage response;

            // Post to Service............
            var task = client.PostAsJsonAsync(uri, payload).ContinueWith(taskwithmsg =>
                {
                    payload = JsonConvert.DeserializeObject<AfaSession>(
                        taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString());
                    if ((int)taskwithmsg.Result.StatusCode == 200)
                    {
                        payload.ClientKey = operation.ConsumerKey;
                        payload.UserName = operation.UserName; // "famedev";
                        payload.Password = operation.PrivateKey; // "pass";
                        response = taskwithmsg.Result;
                        Debug.WriteLine($"Code: {response.StatusCode} - Reason: {response.ReasonPhrase}");
                    }
                });
            task.Wait();

            return payload;
        }


        /// <summary>
        /// The get Advantage API token.
        /// </summary>
        /// <param name="operation">
        /// The operation.
        /// </param>np
        /// <returns>
        /// The <see cref="TokenResponse"/>.
        /// </returns>
        private TokenResponse GetAdvantageApiToken(IWapiOperationsSettings operation)
        {
            TokenResponse tokenResponse = new FAME.Advantage.Api.Library.Token(operation.ExternalUrl).GetToken(operation.ConsumerKey, operation.UserName, operation.PrivateKey);
            return tokenResponse;
        }

        /// <summary>
        /// The create lead.
        /// </summary>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <param name="catalog">
        /// The catalog.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Lead"/>.
        /// </returns>
        private Lead CreateLead(AdvStagingDemographicsV1 lead, IList<Catalog> catalog, Guid campusId)
        {
            if (!campusId.Equals(Guid.Empty))
            {
                Lead newLead = new Lead();
                newLead.FirstName = lead.FirstName.Trim();
                newLead.LastName = lead.LastName.Trim();
                newLead.MiddleName = lead.MiddleInitial.Trim();
                newLead.AfaStudentId = lead.IDStudent;
                newLead.CampusId = campusId;
                newLead.SSN = lead.SSN.Trim();
                var campCatalog = catalog.FirstOrDefault(c => c.Id == campusId);
                if (!string.IsNullOrEmpty(lead.Phone.Trim()))
                {
                    newLead.IsUsaPhoneNumber = true;
                    newLead.PhoneTypeId = campCatalog.PhoneTypes.FirstOrDefault().Value;
                    newLead.Phone = lead.Phone.Trim();
                }

                if (!string.IsNullOrEmpty(lead.Address.Trim()))
                {
                    newLead.AddressTypeId = campCatalog.AddressTypes.FirstOrDefault().Value;
                    newLead.Address1 = lead.Address.Trim();
                    newLead.StateId = campCatalog.States.FirstOrDefault(s => s.Text.ToLower() == lead.State.Trim().ToLower())?.Value;
                    newLead.City = lead.City.Trim();
                    newLead.Zip = lead.ZipCode.Trim();
                }

                if (!string.IsNullOrEmpty(lead.LicenseNumber.Trim()))
                {
                    newLead.DriverLicenseNumber = lead.LicenseNumber.Trim();
                }

                if (!string.IsNullOrEmpty(lead.LicenseState.Trim()))
                {
                    newLead.LicenseState = campCatalog.States.FirstOrDefault(s => s.Text.ToLower() == lead.LicenseState.Trim().ToLower())?.Value;
                }

                if (!string.IsNullOrEmpty(lead.Email.Trim()))
                {
                    newLead.Email = lead.Email.Trim();
                    newLead.EmailTypeId = campCatalog.EmailTypes.FirstOrDefault().Value;
                }

                newLead.BirthDate = lead.DOB;
                if (!string.IsNullOrEmpty(lead.CitizenshipStatus.Trim()))
                {
                    newLead.CitizenShipStatus = campCatalog.CitizenShip
                        .FirstOrDefault(c => string.Equals(c.AfaValue, lead.CitizenshipStatus.Trim(), StringComparison.CurrentCultureIgnoreCase)).Value;
                }

                if (!string.IsNullOrEmpty(lead.MaritialStatus.Trim()))
                {
                    newLead.MaritalStatus = campCatalog.MaritalStatus
                        .FirstOrDefault(c => string.Equals(c.AfaValue, lead.MaritialStatus.Trim(), StringComparison.CurrentCultureIgnoreCase)).Value;
                }

                if (!string.IsNullOrEmpty(lead.Gender.Trim()))
                {
                    newLead.Gender = campCatalog.Genders
                        .FirstOrDefault(c => string.Equals(c.AfaValue, lead.Gender.Trim(), StringComparison.CurrentCultureIgnoreCase)).Value;
                }

                return newLead;
            }
            else
            {
                return new Lead();
            }
        }
    }
}
