﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiServiceKlassApp.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//   Defines the WapiServiceKlassApp type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll
{
    using FAME.Advantage.Api.Library.Models;
    using FAME.Advantage.Api.Library.Models.KlassApp;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Linq;
    using System.Net.Http;
    using System.Timers;
    using WapiDll.Data;
    using WapiDll.Data.Contract;

    /// <summary>
    ///  The WAPI service class application.
    /// </summary>
    public class WapiServiceKlassApp : WapiService
    {
        /// <summary>
        /// The Version of <code>KlassApp</code> API
        /// </summary>
        private string Apiversion = "1.1";

        /// <summary>
        /// The total student sent.
        /// </summary>
        private int totalStudentSentToWs;

        /// <summary>
        /// The real student send to <code>klass app</code>.
        /// </summary>
        private int studentSendToKlassApp;

        /// <summary>
        /// Use this to create a instance of the class <br/>
        /// Do not use the implicit constructor.
        /// </summary>
        /// <param name="operation">The name of the external operation</param>
        /// <param name="listOfServices">List of object of other services running in the Advantage Windows Service</param>
        /// <returns>A instance of the class</returns>
        public static IWapiService Factory(IWapiOperationsSettings operation, ref IList<IWapiService> listOfServices)
        {
            IWapiService service = new WapiServiceKlassApp();
            service.Operation = operation;
            service.Operation.Tracer = service.Operation.Tracer ?? new TraceSource("TraceWapiWs");

            // Create the interval of time
            service.ServiceTimer.Interval = WapiService.PositiveTimerIntervalCalculation(operation) * 1000;
            service.WapiServiceList = listOfServices;
            return service;
        }

        #region Overrides of WapiService

        /// <summary>
        /// The execute actions.
        /// </summary>
        public override void ExecuteActions()
        {
            if (ConfigurationManager.AppSettings.Get("KlassAppVersion") != null)
            {
                this.Apiversion = ConfigurationManager.AppSettings.Get("KlassAppVersion").ToString();
            }
            IWapiService advantageApiService = this.WapiServiceList
                .FirstOrDefault(x => x.Operation.CodeExtOperationMode == "ADVANTAGE_API_SERVICE"
                && x.Operation.SecretCodeCompany == this.Operation.SecretCodeCompany);

            List<KlassAppSync> studentList = null;
            var failMessageList = new List<string>();

            if (advantageApiService != null)
            {
                // get api token
                TokenResponse tokenResponse = new FAME.Advantage.Api.Library.Token(advantageApiService.Operation.ExternalUrl).GetToken(
                    advantageApiService.Operation.ConsumerKey,
                    advantageApiService.Operation.UserName,
                    advantageApiService.Operation.PrivateKey);

                if (tokenResponse.ResponseStatusCode != 200)
                {
                    var message = "KlassApp syncronization was not completed, unable to authenticate Advantage API User. Api returned error: " + tokenResponse.ResponseText;
                    this.Operation.Tracer.TraceInformation(message);
                    this.OptionalMessage = message;
                    Debug.WriteLine("Work Finished");
                    return;
                }

                var klassAppStudentSummaryRequest = new FAME.Advantage.Api.Library.KlassApp.KlassAppStudentSummaryRequest(advantageApiService.Operation.ExternalUrl, tokenResponse.Token);
                studentList = klassAppStudentSummaryRequest.GetStudentsToSync();
            }

            if (studentList != null)
            {
                this.totalStudentSentToWs = studentList.Count;
                this.studentSendToKlassApp = 0;
                foreach (KlassAppSync x in studentList)
                {
                    try
                    {
                        HttpResponseMessage response = null;
                        response = this.PostStudentNotificationToKlassApp(x, this.Operation);
                        this.ErrorAnalysis(response, failMessageList, x, "INSERT");
                    }
                    catch (Exception ex)
                    {
                        failMessageList.Add(
                            $"Student {x.FirstName} {x.LastName} {x.EnrollmentId} was not synced with klass app due to exception: " + ex.Message + ". StackTrace: " + ex.StackTrace);
                    }

                }

                // Analysis if some student get error, if some get errors raise exception with the list of failed students.
                if (failMessageList.Count > 0)
                {
                    var message = string.Empty;
                    foreach (string s in failMessageList)
                    {
                        message += s + "\n\r";
                    }

                    message = message.Length > 999 ? message.Remove(999) : message;
                    this.OptionalMessage = message.Length > 997 ? message.Remove(899) : message;
                    throw new Exception(message);
                }

                Debug.WriteLine("Work done without error");
                this.OptionalMessage = $"{this.studentSendToKlassApp} of {this.totalStudentSentToWs} Student(s) from Advantage was/were sent";
            }
            else
            {
                Debug.WriteLine("No student from Advantage");
                this.OptionalMessage = "No Student From Advantage 0 Student was sent";
            }
        }

        private KlassAppReturnedStudentObject getStudentToUpdateFromDups(KlassAppReturnedStudentObject[] klassAppStudents, KlassAppStudentSummary advantageStudent)
        {
            KlassAppReturnedStudentObject lastFound = null;
            int lastScoredPII = 0;
            foreach (var klassAppStudent in klassAppStudents)
            {
                int scorePII = 0;
                if (klassAppStudent.first_name.Trim().ToUpper() == advantageStudent.first_name.Trim().ToUpper())
                {
                    scorePII += 1;
                }
                if (klassAppStudent.last_name.Trim().ToUpper() == advantageStudent.last_name.Trim().ToUpper())
                {
                    scorePII += 1;
                }
                if (klassAppStudent.date_of_birth.Trim().ToUpper() == advantageStudent.date_of_birth.Trim().ToUpper())
                {
                    scorePII += 1;
                }
                if (klassAppStudent.gender.Trim().ToUpper() == advantageStudent.gender.Trim().ToUpper())
                {
                    scorePII += 1;
                }
                if (klassAppStudent.college_id.Trim().ToUpper() == advantageStudent.college_id.Trim().ToUpper())
                {
                    scorePII += 1;
                }
                if (klassAppStudent.address.Trim().ToUpper() == advantageStudent.address.Trim().ToUpper())
                {
                    scorePII += 1;
                }
                if (klassAppStudent.address_city.Trim().ToUpper() == advantageStudent.address_city.Trim().ToUpper())
                {
                    scorePII += 1;
                }
                if (klassAppStudent.address_state.Trim().ToUpper() == advantageStudent.address_state.Trim().ToUpper())
                {
                    scorePII += 1;
                }
                if (klassAppStudent.address_zip.Trim().ToUpper() == advantageStudent.address_zip.Trim().ToUpper())
                {
                    scorePII += 1;
                }
                if (klassAppStudent.phone.Trim().ToUpper() == advantageStudent.phone.Trim().ToUpper())
                {
                    scorePII += 1;
                }
                if (klassAppStudent.email.Trim().ToUpper() == advantageStudent.email.Trim().ToUpper())
                {
                    scorePII += 1;
                }
                if (scorePII > lastScoredPII)
                {
                    lastScoredPII = scorePII;
                    lastFound = klassAppStudent;
                }
            }

            return lastFound;
        }

        /// <summary>
        /// The service timer elapsed.
        /// </summary>
        /// <param name="sender">
        ///  The sender.
        /// </param>
        /// <param name="e">
        ///  The e.
        /// </param>
        protected override void ServiceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            base.ServiceTimer_Elapsed(sender, e);
            var timer = (Timer)sender;
            timer.Stop();
            try
            {
                this.ExecuteActions();
                var message = $"Operation: {this.Operation.CodeOperation} success. {this.totalStudentSentToWs} was/were sent ";
                Debug.WriteLine(message);
                this.Operation.DateLastExecution = DateTime.Now;
                this.PostDateLastOperation(this.Operation.ID, this.Operation.DateLastExecution);
                this.PostLogToServer(this.Operation, message, 0);
                this.OptionalMessage = message;
                var interval = PositiveTimerIntervalCalculation(this.Operation);
                timer.Interval = interval * 1000;
                timer.Start();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error in Operation: {ex.Message} StackTrace: {ex.StackTrace}{Environment.NewLine}");
                this.Operation.Tracer.TraceData(
                    TraceEventType.Error,
                    200,
                    $"Error in Operation: {ex.Message} StackTrace: {ex.StackTrace}{Environment.NewLine}");
                this.PostLogToServer(
                    this.Operation,
                    $"Operation: {this.Operation.CodeOperation} is delayed 10 minutes because throw Exception {ex.Message} StackTrace: {ex.StackTrace}{Environment.NewLine}.",
                    1);

                // Set 10 minutes the next start
                timer.Interval = 600000;
                timer.Start();
            }
        }

        /// <summary>
        /// Compare two string Dates (only date part).
        /// </summary>
        /// <param name="strDate1">
        /// The string date 1.
        /// </param>
        /// <param name="strDate2">
        /// The string date 2.
        /// </param>
        /// <param name="assert">
        /// The assert.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool AssertDate(string strDate1, string strDate2, bool assert)
        {
            if (string.IsNullOrEmpty(strDate1) | string.IsNullOrEmpty(strDate2))
            {
                assert &= string.IsNullOrEmpty(strDate1) & string.IsNullOrEmpty(strDate2);
            }
            else
            {
                var grd1 = (strDate1.Length > 10) ? strDate1.Substring(0, 10) : strDate1;
                var grd2 = (strDate2.Length > 10) ? strDate2.Substring(0, 10) : strDate2;
                assert &= grd1 == grd2;
            }

            return assert;
        }

        /// <summary>
        /// The assert compare student.
        /// </summary>
        /// <param name="studentka">
        /// The <code>studentka</code>.
        /// </param>
        /// <param name="om">
        /// The om.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool AssertCompareStudent(KlassAppReturnedResponse studentka, KlassAppStudentSummary om)
        {
            var ka = studentka.response[0];
            var assert = true;
            assert &= string.Equals(ka.address, om.address, StringComparison.InvariantCultureIgnoreCase);
            assert &= string.Equals(ka.address_city, om.address_city, StringComparison.InvariantCultureIgnoreCase);
            assert &= string.Equals(ka.address_state, om.address_state, StringComparison.InvariantCultureIgnoreCase);
            assert &= string.Equals(ka.address_zip, om.address_zip, StringComparison.InvariantCultureIgnoreCase);
            assert &= ka.college_id == om.college_id;
            assert = AssertDate(ka.date_of_birth, om.date_of_birth, assert);
            assert &= ka.email == om.email;
            assert &= string.Equals(ka.first_name, om.first_name, StringComparison.InvariantCultureIgnoreCase);
            assert &= string.Equals(ka.gender, om.gender, StringComparison.InvariantCultureIgnoreCase);
            assert = AssertDate(ka.graduation_date, om.graduation_date, assert);

            assert &= string.Equals(ka.last_name, om.last_name, StringComparison.InvariantCultureIgnoreCase);
            assert &= ka.locations != null && ka.locations.Length > 0 && ka.locations[0].id == om.locations;
            try
            {
                KlassAppReturnedMajor major = JsonConvert.DeserializeObject<KlassAppReturnedMajor>(om.major);
                if (major != null)
                {
                    assert &= major.id == om.major;
                }
            }
            catch (Exception ex)
            {
                //unable to parse the json for major, so the value didn't came as an object but as an id
                assert &= ka.major == om.major;
            }

            try
            {
                KlassAppReturnedStatus status = JsonConvert.DeserializeObject<KlassAppReturnedStatus>(om.status);
                if (status != null)
                {
                    assert &= status.Id == om.status;
                }
            }
            catch (Exception ex)
            {
                assert &= ka.status == om.status;
            }



            // Iterate through the dictionary
            var dict3 = ka.custom_fields.Where(entry => om.cf_dictionary.ContainsKey(entry.Key.ToString()) && om.cf_dictionary[entry.Key.ToString()] != entry.Value)
                        .ToDictionary(entry => entry.Key, entry => entry.Value);
            assert &= dict3.Count == 0;
            return assert;
        }

        #endregion

        /// <summary>
        /// The get student from <code>klass-app</code>.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private KlassAppReturnedResponse GetStudentFromKlassApp(KlassAppStudentSummary model, IWapiOperationsSettings operation)
        {
            // Sending information to KlassApp ...........................................
            // Create the client
            var client = new HttpClient();

            // Create the headers
            client.DefaultRequestHeaders.Add("auth_id", operation.ConsumerKey);
            client.DefaultRequestHeaders.Add("auth_token", operation.PrivateKey);

            HttpResponseMessage response;

            var uri = new Uri($"{operation.ExternalUrl}users/{Apiversion}/search?search=" + "{" + $"\"email\":\"{model.email}\"" + "}");
            Debug.WriteLine(uri);
            KlassAppReturnedResponse student = new KlassAppReturnedResponse();

            // Post to Service............
            var task = client.GetAsync(uri).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                Debug.WriteLine($"Function: GetStudentFromKlassApp - Code: {response.StatusCode} - Reason: {response.ReasonPhrase} - StatusSuccessCode:{response.IsSuccessStatusCode}");
                if (response.IsSuccessStatusCode)
                {
                    var studentjson = response.Content.ReadAsStringAsync().Result;
                    student = JsonConvert.DeserializeObject<KlassAppReturnedResponse>(studentjson);
                }

                Debug.WriteLine($"info: {student.info} - success:{student.success} response:{student.response.ToString()} ");
            });

            task.Wait();

            return student;
        }

        /// <summary>
        ///  The post student to KLASSAPP.
        /// </summary>
        /// <param name="payload">
        ///  The payload.
        /// </param>
        /// <param name="operation">
        ///  The operation.
        /// </param>
        /// <returns>
        /// The <see cref="HttpResponseMessage"/>.
        /// </returns>
        private HttpResponseMessage PostStudentNotificationToKlassApp(KlassAppSync payload, IWapiOperationsSettings operation)
        {
            // Sending information to KlassApp ...........................................
            // Create the client
            var client = new HttpClient();

            // Create the headers
            client.DefaultRequestHeaders.Add("auth_id", operation.ConsumerKey);
            client.DefaultRequestHeaders.Add("auth_token", operation.PrivateKey);
            var uri = new Uri(operation.ExternalUrl + "fadv/v1/trigger-student-change");
            HttpResponseMessage response = null;

            // Post to Service............
            var task = client.PostAsJsonAsync(uri, payload).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                Debug.WriteLine($"Function: PostStudentNotificationToKlassApp - Code: {response.StatusCode} - Reason: {response.ReasonPhrase}");
            });
            task.Wait();

            return response;
        }

        /// <summary>
        /// The put student to <code>klass app</code>.
        /// </summary>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <param name="klassAppStudentId">
        /// The KLASSAPP Student ID
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <returns>
        /// The <see cref="HttpResponseMessage"/>.
        /// </returns>
        private HttpResponseMessage PutStudentToKlassApp(KlassAppStudentSummary payload, string klassAppStudentId, IWapiOperationsSettings operation)
        {
            // Create the client
            var client = new HttpClient();

            // Create the headers
            client.DefaultRequestHeaders.Add("auth_id", operation.ConsumerKey);
            client.DefaultRequestHeaders.Add("auth_token", operation.PrivateKey);
            var uri = new Uri(operation.ExternalUrl + "users/" + Apiversion + "/" + klassAppStudentId);
            HttpResponseMessage response = null;

            // Post to Service............
            var task = client.PutAsJsonAsync(uri, payload).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                Debug.WriteLine($"Code: {response.StatusCode} - Reason: {response.ReasonPhrase}");
            });
            task.Wait();

            return response;
        }

        /// <summary>
        /// The error analysis.
        /// </summary>
        /// <param name="response">
        /// The response.
        /// </param>
        /// <param name="failMessageList">
        /// The fail message list.
        /// </param>
        /// <param name="x">
        /// The x.
        /// </param>
        /// <param name="operation">
        /// The operation.
        /// </param>
        /// <exception cref="Exception">
        /// Communication error return exceptions
        /// </exception>
        private void ErrorAnalysis(HttpResponseMessage response, List<string> failMessageList, KlassAppSync x, string operation)
        {
            if (response.IsSuccessStatusCode == false)
            {
                Debug.WriteLine(
                    $"{operation} student to remote server: {x.FirstName} return error: {response.ReasonPhrase} ");
                throw new Exception(
                    $"Error in {operation} student to klassApp. the server respond {response.ReasonPhrase} - stack{response.Content.ReadAsStringAsync()}");
            }

            // Response is true check success flag
            var studentjson = response.Content.ReadAsStringAsync().Result;
            var student = JsonConvert.DeserializeObject<KlassAppReturnedResponseMinimal>(studentjson);
            if (student != null && student.errors != null)
            {
                string error = Convert.ToString(student.errors) ?? string.Empty; // check this test this first ,
                error = error.Replace("{", string.Empty);
                error = error.Replace("}", string.Empty);
                error = error.Replace("[", string.Empty);
                error = error.Replace("]", string.Empty);

                failMessageList.Add(
                    $"{operation} student to remote server: {x.FirstName} {x.LastName} {x.EnrollmentId} return error {error}");
            }
            else
            {
                this.studentSendToKlassApp++;
            }
        }
    }
}
