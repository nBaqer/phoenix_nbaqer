﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWapiService.cs" company="FAME">
//   2015,2017
// </copyright>
// <summary>
//   Interface for Operation with Advantage Windows Service.<br />
//   This have defined the necessaries operations to interact with <br />
//   the Advantage Windows Service and the defined operation to be executed by it.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll
{
    using System.Collections.Generic;
    using System.Timers;

    using WapiDll.Data.Contract;

    /// <summary>
    /// Interface for Operation with Advantage Windows Service.<br/>
    /// This have defined the necessaries operations to interact with <br/>
    /// the Advantage Windows Service and the defined operation to be executed by it.
/// </summary>
    public interface IWapiService
    {
        #region Properties
        /// <summary>
        /// Gets or sets Operation settings 
        /// </summary>
        IWapiOperationsSettings Operation { get; set; }

        /// <summary>
        /// Gets or sets Hold all instanced <code>IWapiService</code> Type 
        /// </summary>
        IList<IWapiService> WapiServiceList { get; set; }

        /// <summary>
        /// Gets or sets the optional message.
        /// </summary>
        string OptionalMessage { get; set; }

        #endregion

        #region Timer Service
        /// <summary>
        /// Gets or sets Operation Timer for the class
        /// </summary>
        Timer ServiceTimer { get; set; }

        /// <summary>
        /// Start The timer
        /// </summary>
        void StartTimer();

        /// <summary>
        /// Stop the Timer
        /// </summary>
        void StopTimer();

        #endregion

        #region Custom Action Method.
        /// <summary>
        /// This should contain the payload of the time elapsed code.<br/>
        /// to be executed by external node.
        /// </summary>
        void ExecuteActions();
        #endregion

        #region Specific Method
        // Gets or sets Specific Implemented Method

        /// <summary>
        /// Post a value to a On Demand Flag
        /// </summary>
        /// <param name="id">The Id of the external operation</param>
        /// <param name="flagVal">1 to active 0 no inactive</param>
        void PostOnDemandFlag(int id, int flagVal);

        /// <summary>
        /// Get poll flags without parameters
        /// </summary>
        /// <param name="id">
        /// Use no parameters or -1 to get all operations flag<br/>
        /// set 1 .. to get specific flag.
        /// </param>
        /// <returns>
        /// The list of demand flag for all operations <br/>
        /// or the individual flag for the selected operation
        /// </returns>
        IList<IOnDemandFlag> GetOnDemandFlags(int id = -1);

        /// <summary>
        /// Send a log information to Advantage Log Table.
        /// </summary>
        /// <param name="op">Setting Operation referred in the log.</param>
        /// <param name="comment">A free comment</param>
        /// <param name="isError">0 if you log a error, 1 if you log a error.</param>
        void PostLogToServer(IWapiOperationsSettings op, string comment, int isError);

        #endregion
    }
}