﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiServiceVoyantCurrent.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   CLass for External Operation: VOYANT Current Attendance Student <br />
//   This class is tailored to executed the operation at regular interval programmed by the operation setting.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using System.Timers;

    using Newtonsoft.Json.Linq;

    using WapiDll.Data.Contract;
    using WapiDll.Helpers;

    using WapiVoyantClient;

    /// <summary>
    /// CLass for External Operation: VOYANT Current Attendance Student <br/>
    /// This class is tailored to executed the operation at regular interval programmed by the operation setting.
    /// </summary>
    /// <remarks>This class is for use with Advantage Windows Service.</remarks>
    public class WapiServiceVoyantCurrent : WapiService
    {
        #region Class Factory
        /// <summary>
        /// Use this to create a instance of the class <br/>
        /// Do not use the implicit constructor.
        /// </summary>
        /// <param name="operation">The name of the external operation</param>
        /// <param name="listOfServices">List of object of other services running in the Advantage Windows Service</param>
        /// <returns>A instance of the class</returns>
        public static IWapiService Factory(IWapiOperationsSettings operation, ref IList<IWapiService> listOfServices)
        {
            IWapiService service = new WapiServiceVoyantCurrent();
            service.Operation = operation;
            service.Operation.Tracer = (service.Operation.Tracer) ?? new TraceSource("TraceWapiWs");
            // Create the interval of time
            service.ServiceTimer.Interval = PositiveTimerIntervalCalculation(operation) * 1000;
            service.WapiServiceList = listOfServices;
            return service;
        }
        #endregion

        #region Implement Abstract Methods
        /// <summary>
        /// The Time-elapsed event of the clock.<br/>
        /// Use this to call ExecuteAction method and also put the logic of control<br/>
        /// of the operation.
        /// </summary>
        /// <param name="sender">The timer that raise the event</param>
        /// <param name="e"><see cref="ElapsedEventArgs"/></param>
        protected override void ServiceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            base.ServiceTimer_Elapsed(sender, e);
            var timer = (System.Timers.Timer)sender;
            timer.Stop();
            
            // Ask if the VOYANT_HISTORICAL is executing
            var isExecuting = WapiServiceList.Any(x => x.Operation.CodeOperation == "VOYANT_HISTORICAL" & x.ServiceTimer.Enabled == false);
            if (isExecuting)
            {
                // The Historical is executing, then reprogramming the interval for all period
                Operation.DateLastExecution = DateTime.Now;
                timer.Interval = PositiveTimerIntervalCalculation(Operation) * 1000;
                PostLogToServer(Operation, string.Format("Operation: VOYANT_CURRENT is reprogrammed because VOYANT_HISTORICAL is Executing"), 0);
                timer.Start();
                return;
            }

            double secondsToNextExecution = PositiveTimerIntervalCalculation(Operation);
            try
            {
                ExecuteActions();
                Debug.WriteLine("Operation: {0} success", Operation.CodeOperation);
                Operation.DateLastExecution = DateTime.Now;
                PostDateLastOperation(Operation.ID, Operation.DateLastExecution);
                secondsToNextExecution = PositiveTimerIntervalCalculation(Operation);
                PostLogToServer(Operation, string.Format("Operation: {0} success", Operation.CodeOperation), 0);

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Operation: {0}", ex.Message);
                Operation.Tracer.TraceData(TraceEventType.Error, 200, string.Format("Error in Operation: {0} StackTrace: {1}{2}", ex.Message, ex.StackTrace, Environment.NewLine));
                Operation.DateLastExecution = Operation.DateLastExecution.AddMinutes(10);
                secondsToNextExecution = PositiveTimerIntervalCalculation(Operation);
                PostLogToServer(Operation, string.Format("Operation: {0} is delayed 10 minutes because throw Exception {1} Stack Trace: {2}{3}. ", Operation.CodeOperation, ex.Message, ex.StackTrace, Environment.NewLine), 1);
            }
            finally
            {
                timer.Interval = secondsToNextExecution * 1000;
                timer.Start();
            }
        }

        /// <summary>
        /// This should contain the payload of the time elapsed code.
        /// to be executed by external node.
        /// </summary>
        public override void ExecuteActions()
        {
            Debug.WriteLine(string.Format("Push operation {0} begin", Operation.CodeOperation));
            // Get the info from Proxy....
            var payload = Proxy.GetMessageFromWapi(Operation.CodeWapiServ, "?RequestHistorical=0");
            if (payload != null)
            {
                payload = payload.Replace("AcademicsDataList", "academics");
                payload = payload.Replace("EnrollmentDataList", "enrollments");
                payload = payload.Replace("FacultyDataList", "fa");
                Debug.WriteLine("Payload length: {0} KB", (payload.Length / 1000));
                Operation.Tracer.TraceInformation("Payload length: {0} KB", (payload.Length / 1000));
                Debug.WriteLine("Payload: {0}", payload);
                // Set for historical data
                Debug.WriteLine(string.Format("payload to remote server: {0}", payload));
                var vy = ApiConnect.Factory(Operation.ConsumerKey, Operation.PrivateKey, Operation.ExternalUrl);
                var parameters = vy.CreateParameters(); // Parameter here must no have ? sign
                var sign = vy.GetSig("", HttpVerb.POST, parameters);
                var parametersplus = string.Format("?{0}", parameters); // Add ? sign to parameters
                var response = vy.PostRequest("", sign, parametersplus, payload);
                Debug.WriteLine(string.Format("Remote Server Respond -> {0}", response));
                Operation.Tracer.TraceInformation("Remote Server Respond -> {0}", response);
                if (response.StartsWith("ERROR"))
                {
                    throw new Exception(response);
                }

                // Poll for model create; The system wait for 3 minutes to get as successfully the operation.
                var initTime = DateTime.Now;
                var endTime = initTime.AddMinutes(3);
                var status = string.Empty;
                while (initTime < endTime)
                {
                    parameters = vy.CreateParameters(); // Parameter here must no have ? sign
                    sign = vy.GetSig(string.Empty, HttpVerb.GET, parameters);
                    var statusResponse = vy.GetRequest("", HttpVerb.GET, sign, parameters);
                    Debug.WriteLine(string.Format("Remote Server Respond to Get -> {0}", statusResponse));
                    Operation.Tracer.TraceInformation("Remote Server Respond to Get -> {0}", statusResponse);
                    if (statusResponse.ToUpper().StartsWith("ERROR"))
                    {
                        throw new Exception(statusResponse);
                    }
                    if (Tools.IsJson(statusResponse))
                    {
                        var result = JObject.Parse(statusResponse);
                        status = (string)result["status"];
                        Debug.WriteLine(string.Format("Remote Server Status -> {0}", status));
                        if (status == "Completed")
                        {
                            //Get the address to setting in database as detail information.
                            var url = (string)result["dashboard"];
                            Debug.WriteLine(string.Format("URL for dashboard: {0}", url));
                            Operation.Tracer.TraceInformation("URL for dashboard: {0}", url);

                            // Get the address of the summary operation
                            var summaryUrl = (string)result["summary"];
                            if (!string.IsNullOrWhiteSpace(summaryUrl))
                            {
                                url += ", " + summaryUrl;
                            }

                            Debug.WriteLine(string.Format("URL summary: {0}", url));
                            payload = "{\"DashboardUrl\":\"" + url + "\"}";

                            Proxy.PostMessageToWapi(Operation.SecondCodeWapiServ, payload, string.Empty);
                            break;
                        }
                        if (status.StartsWith("ERROR"))
                        {
                            throw new Exception(status);
                        }
                    }

                    Thread.Sleep(5000);
                    initTime = DateTime.Now;
                }

                if (status == "Completed")
                {
                    Operation.DateLastExecution = DateTime.Now;
                    PostDateLastOperation(Operation.ID, Operation.DateLastExecution);
                    Debug.WriteLine("Voyant Prediction operation completed successful");
                }
                if (status.StartsWith("ERROR"))
                {
                    throw new Exception(status);
                }

            }
            else
            {
                Debug.WriteLine("Error Accessing Advantage Proxy");
            }

            Operation.Tracer.TraceInformation(string.Format("Push operation {0} finished", Operation.CodeOperation));
            Debug.WriteLine(string.Format("Push operation {0} finished", Operation.CodeOperation));
        }
        #endregion
    }
}
