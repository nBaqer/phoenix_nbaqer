﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppResponse.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//  JAGG - The <code>KLASS</code> application response.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll.Data
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    ///  The <code>klass</code> application response.
    /// </summary>
    [SuppressMessage("ReSharper", "StyleCop.SA1300", Justification = "Here is valid")]
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "Here is valid")]
    public class KlassAppResponse
    {
        /// <summary>
        /// Gets or sets a value indicating whether success.
        /// </summary>
        public bool success { get; set; }

        /// <summary>
        /// Gets or sets the total of item pulled from KLASSAPP.
        /// </summary>
        public int total { get; set; }

        /// <summary>
        /// Gets or sets the response.
        /// </summary>
        public KlassAppOutputModel[] response { get; set; }

        /// <summary>
        /// Gets or sets the errors.
        /// </summary>
        public dynamic errors { get; set; }
    }
}
