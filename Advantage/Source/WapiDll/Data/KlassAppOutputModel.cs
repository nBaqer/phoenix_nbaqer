﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
// JAGG - The KLASS application output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll.Data
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    using WapiDll.Data.Contract;

    /// <summary>
    ///  The KLASS application output model.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:CodeAnalysisSuppressionMustHaveJustification", Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("ReSharper", "StyleCop.SA1300"), SuppressMessage("ReSharper", "InconsistentNaming")]
    public class KlassAppOutputModel : IKlassAppOutputModel
    {
        #region Implementation of IKlassAppOutputModel

        /// <summary>
        /// Gets or sets the id. 
        /// This is the Id that KLASSAPP assign to student
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the college_id.
        /// </summary>
        public string college_id { get; set; }

        /// <summary>
        /// Gets or sets the first_name.
        /// </summary>
        public string first_name { get; set; }

        /// <summary>
        /// Gets or sets the last_name.
        /// </summary>
        public string last_name { get; set; }

        /// <summary>
        /// Gets or sets the role.
        /// The role for now must be student
        /// </summary>
        public string role { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// Just a random 8 digit string
        /// </summary>
        public string password { get; set; }

        /// <summary>
        /// Gets or sets the major.
        /// </summary>
        public string major { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// Gets or sets the date_of_birth.
        /// </summary>
        public string date_of_birth { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        public string gender { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// Gets or sets the phone_other.
        /// </summary>
        public string phone_other { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string address { get; set; }

        /// <summary>
        /// Gets or sets the address_city.
        /// </summary>
        public string address_city { get; set; }

        /// <summary>
        /// Gets or sets the address_state.
        /// </summary>
        public string address_state { get; set; }

        /// <summary>
        /// Gets or sets the address_zip.
        /// </summary>
        public string address_zip { get; set; }

        /// <summary>
        /// Gets or sets the start_date.
        /// </summary>
        public string start_date { get; set; }

        /// <summary>
        /// Gets or sets the graduation_date.
        /// </summary>
        public string graduation_date { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        public string locations { get; set; }

        /// <summary>
        /// Gets or sets the custom_fields.
        /// </summary>
        ////public Dictionary<string , string> custom_fields { get; set; }
        public string custom_fields { get; set; }

        /// <summary>
        /// Gets or sets the custom_fields dictionary from Advantage.
        /// </summary>
        public Dictionary<string, string> cf_dictionary { get; set; }

        #endregion
    }
}
