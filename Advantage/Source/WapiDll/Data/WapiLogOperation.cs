﻿using System;
using WapiDll.Data.Contract;

namespace WapiDll.Data
{
    /// <summary>
    /// Hold the info to be show in the Advantage External Operation Logger <br/>
    /// this logger is show in Advantage -> Maintenance -> Wapi Logger.
    /// </summary>
    public class WapiLogOperation: IWapiLogOperation
    {
        /// <summary>
        /// Use this to create the class. 
        /// Do not use the default constructor.
        /// </summary>
        /// <returns>A instance of IWapiLogOperation</returns>
        public static IWapiLogOperation Factory()
        {
            IWapiLogOperation log = new WapiLogOperation();
            log.DateExecution = DateTime.Now;
            log.IsError = 0;

            return log;
        }
        
        #region Implementation of IWapiLogOperation

        /// <summary>
        /// Id of the log.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// The service used when the log is generated
        /// </summary>
        public string ServiceCode { get; set; }

        /// <summary>
        /// The company secret code under was generated this log.
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// The last date of execution of the operation that generated this log.
        /// </summary>
        public DateTime DateExecution { get; set; }

        /// <summary>
        /// The next planning date, that the operation that generated this log should be executed.
        /// </summary>
        public DateTime NextPlanningDate { get; set; }

        /// <summary>
        /// If this log, is error information or not.
        /// </summary>
        public int IsError { get; set; }

        /// <summary>
        /// A free comment about the log.
        /// </summary>
        public string Comment { get; set; }

        #endregion
    }
}
