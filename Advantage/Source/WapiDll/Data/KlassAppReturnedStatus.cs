﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppReturnedStatus.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The <code>klass app</code> returned status.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll.Data
{
    using Newtonsoft.Json;

    /// <summary>
    /// The <code>klass app</code> returned status.
    /// </summary>
    public class KlassAppReturnedStatus
    {
        /// <summary>
        /// Gets or sets the ID
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
