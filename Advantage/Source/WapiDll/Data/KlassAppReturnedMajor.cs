// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppReturnedMajor.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The KLASSAPP returned major.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll.Data
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// The KLASSAPP returned major.
    /// </summary>
    [SuppressMessage("ReSharper", "StyleCop.SA1300", Justification = "Here is valid")]
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "Here is valid")]
    public class KlassAppReturnedMajor
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string name { get; set; }
    }
}