﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassAppConfigurationModel.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The class application configuration output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace WapiDll.Data
{
    using System;

    /// <summary>
    ///  The class application configuration output model.
    /// </summary>
    public class ClassAppConfigurationModel
    {
        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string AdvantageIdentification { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public int KlassAppIdentification { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string CodeEntity { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string CodeOperation { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public int IsCustomField { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public int IsActive { get; protected set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public char ItemStatus { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string ItemValue { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string ItemLabel { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string ItemValueType { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public DateTime ModDate { get; set; }

        /// <summary>
        /// Gets or sets 
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        /// Gets or sets the last operation log.
        /// Gets or sets the last operation log.
        /// Contains the result of the last operation over <code>Klass_App</code>
        /// configuration system.        
        /// </summary>
        public string LastOperationLog { get; set; }
    }
}