﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppReturnedStudentObject.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the KlassAppReturnedStudentObject type.
//   This is part of the returned object from KLASSAPP
//   When a student is inserted or updated
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll.Data
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    using Newtonsoft.Json.Serialization;

    /// <summary>
    /// The KLASSAPP returned student object.
    /// </summary>
    [SuppressMessage("ReSharper", "StyleCop.SA1300", Justification = "Here is valid")]
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "Here is valid")]
    public class KlassAppReturnedStudentObject
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// Gets or sets the first_name.
        /// </summary>
        public string first_name { get; set; }

        /// <summary>
        /// Gets or sets the last_name.
        /// </summary>
        public string last_name { get; set; }

        /// <summary>
        /// Gets or sets the college_id.
        /// </summary>
        public string college_id { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string address { get; set; }

        /// <summary>
        /// Gets or sets the address_city.
        /// </summary>
        public string address_city { get; set; }

        /// <summary>
        /// Gets or sets the address_state.
        /// </summary>
        public string address_state { get; set; }

        /// <summary>
        /// Gets or sets the address_zip.
        /// </summary>
        public string address_zip { get; set; }

        /// <summary>
        /// Gets or sets the date_of_birth.
        /// </summary>
        public string date_of_birth { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        public string gender { get; set; }

        /// <summary>
        /// Gets or sets the graduation_date.
        /// </summary>
        public string graduation_date { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        //public KlassAppReturnedStatus status { get; set; }

        public object status { get; set; }

        /// <summary>
        /// Gets or sets the major.
        /// </summary>
        ////public string major { get; set; }
        //public KlassAppReturnedMajor major { get; set; }
        public object major { get; set; }

        /// <summary>
        /// Gets or sets the locations.
        /// </summary>
        public KlassAppReturnedLocation[] locations { get; set; }

        /// <summary>
        /// Gets or sets the custom_fields.
        /// </summary>
        public IDictionary<int, string> custom_fields { get; set; }
    }
}