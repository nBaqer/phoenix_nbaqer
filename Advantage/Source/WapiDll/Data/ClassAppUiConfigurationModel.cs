﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassAppUiConfigurationModel.cs" company="FAME">
//  2017
// </copyright>
// <summary>
//   The class application UI configuration output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll.Data
{
    using System.Collections.Generic;

    using WapiDll.Data.Contract;

    /// <summary>
    ///  The class application UI configuration output model.
    /// </summary>
    public class ClassAppUiConfigurationModel : IClassAppUiConfigurationModel
    {
        /// <summary>
        /// Gets or sets the configuration setting.
        /// </summary>
        public IList<ClassAppConfigurationModel> ConfigurationSetting { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        public ClassAppFilter Filter { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is any configuration pending.
        /// </summary>
        public bool IsAnyConfigurationPending { get; set; }

        /// <summary>
        /// The factory.
        /// </summary>
        /// <returns>
        /// The <see cref="IClassAppUiConfigurationModel"/>.
        /// </returns>
        public static IClassAppUiConfigurationModel Factory()
        {
            var om = new ClassAppUiConfigurationModel
            {
                ConfigurationSetting = new List<ClassAppConfigurationModel>()
            };
            return om;
        }
    }
}