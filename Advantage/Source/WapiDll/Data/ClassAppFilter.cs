﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassAppFilter.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The class application input model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll.Data
{
    /// <summary>
    ///  The class application input model.
    /// </summary>
    public class ClassAppFilter
    {
        /// <summary>
        /// Gets or sets the command.
        /// 0 or 1: get all
        /// </summary>
        public int Command { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public string UserId { get; set; }
    }
}
