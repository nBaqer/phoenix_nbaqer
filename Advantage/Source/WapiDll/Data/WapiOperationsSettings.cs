﻿using System;
using System.Diagnostics;
using WapiDll.Data.Contract;

namespace WapiDll.Data
{
    /// <summary>
    /// Hold the settings for a specific operation.
    /// </summary>
    public class WapiOperationsSettings : IWapiOperationsSettings
    {
        /// <summary>
        /// Unique Id
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Name of the operation
        /// </summary>
        public string CodeOperation { get; set; }

        /// <summary>
        /// Code of the company that owner the web service
        /// in the case that the operation access a external
        /// resource. 
        /// </summary>
        public string SecretCodeCompany { get; set; }

        /// <summary>
        /// The type of interaction with the external resource if exists can be:
        /// <ul><li>WAPI_TEST_SYSTEM</li>
        /// <li>VOYANT_CURRENT</li>
        /// <li>VOYANT_HISTORICAL</li></ul> 
        /// </summary>
        public string CodeExtOperationMode { get; set; }

        /// <summary>
        /// External URL to looking for, if exists.
        /// </summary>
        public string ExternalUrl { get; set; }

        /// <summary>
        /// Security key to interact with external service if exists
        /// </summary>
        public string ConsumerKey { get; set; }

        /// <summary>
        /// Security key to interact with external service
        /// if exists.
        /// </summary>
        public string PrivateKey { get; set; }


        /// <summary>
        /// The code of operation in the Wapi
        /// The operation to be invoked by the wapi
        /// </summary>
        public string CodeWapiServ { get; set; }

        /// <summary>
        /// Some operation require two operation to be completed
        /// In this case this field is used.
        /// </summary>
        public string SecondCodeWapiServ { get; set; }


        /// <summary>
        /// Time that a windows service should repeat the operation
        /// if it is needed. -1 if inactive.0 one time operation.
        /// </summary>
        public int OperationSecInterval { get; set; }

        /// <summary>
        /// This property stored the interval from the last time 
        /// that the operation was executed
        /// if LastExecutedSecInterval >= OperationSecInterval the operation should be executed.
        /// </summary>
        public DateTime DateLastExecution { get; set; }

        /// <summary>
        /// Poll Wapi from windows service to look for a configuration change.
        /// </summary>
        public int PollSecOnDemandOperation { get; set; }

        /// <summary>
        /// 0: inactive, 1: active
        /// </summary>
        public int FlagOnDemandOperation { get; set; }

        /// <summary>
        /// 0: inactive, 1: active
        /// </summary>
        public int FlagRefreshConfig { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the first allowed service query string.
        /// </summary>
        public string FirstAllowedServiceQueryString { get; set; }

        #region Specific to communicate with WAPI PROXY and log info

        /// <summary>
        /// The address of the proxy
        /// </summary>
        /// <remarks>
        /// Should be the same for all operations.
        /// but for now is in each operation
        /// </remarks>
        public string UrlProxy { get; set; }

        /// <summary>
        /// The address of the log file
        /// </summary>
        /// <remarks>
        /// Should be the same for all operations.
        /// but for now is in each operation
        /// </remarks>
        public string LogFile { get; set; }

        /// <summary>
        /// The size of the log file
        /// </summary>
        /// <remarks>
        /// Should be the same for all operations.
        /// but for now is in each operation
        /// </remarks>
        public int SizeLogFile { get; set; }

        /// <summary>
        /// You can put here a trace instance to pass trace information
        /// to other class
        /// WARINIG if you don't use this. be sure that the class use
        /// some method to instantiate by default the trace.
        /// </summary>
        public TraceSource Tracer { get; set; }

        #endregion
    }
}
