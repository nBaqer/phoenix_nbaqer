﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IKlassAppOutputModel.cs" company="FAME">
//   2016
// </copyright>
// <summary>
// JAGG - Defines the IKlassAppOutputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll.Data.Contract
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// The <code>IKlassAppOutputModel</code> interface.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:CodeAnalysisSuppressionMustHaveJustification", Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("ReSharper", "StyleCop.SA1300"), SuppressMessage("ReSharper", "InconsistentNaming")]
    public interface IKlassAppOutputModel
    {
        /// <summary>
        /// Gets or sets the college_id.
        /// </summary>
        string college_id { get; set; }

        /// <summary>
        /// Gets or sets the first_name.
        /// </summary>
        string first_name { get; set; }

        /// <summary>
        /// Gets or sets the last_name.
        /// </summary>
        string last_name { get; set; }

        /// <summary>
        /// Gets or sets the role.
        /// The role for now must be student
        /// </summary>
        string role { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// Just a random 8 digit string
        /// </summary>
        string password { get; set; }

        /// <summary>
        /// Gets or sets the major.
        /// </summary>
        string major { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        string status { get; set; }

        /// <summary>
        /// Gets or sets the date_of_birth.
        /// </summary>
        string date_of_birth { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        string gender { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        string phone { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        string email { get; set; }

        /// <summary>
        /// Gets or sets the phone_other.
        /// </summary>
        string phone_other { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        string address { get; set; }

        /// <summary>
        /// Gets or sets the address_city.
        /// </summary>
        string address_city { get; set; }

        /// <summary>
        /// Gets or sets the address_state.
        /// </summary>
        string address_state { get; set; }

        /// <summary>
        /// Gets or sets the address_zip.
        /// </summary>
        string address_zip { get; set; }

        /// <summary>
        /// Gets or sets the start_date.
        /// </summary>
        string start_date { get; set; }

        /// <summary>
        /// Gets or sets the graduation_date.
        /// </summary>
        string graduation_date { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        string locations { get; set; }

        /// <summary>
        /// Gets or sets the custom_fields dictionary from Advantage.
        /// </summary>
        Dictionary<string, string> cf_dictionary { get; set; }

        /// <summary>
        /// Gets or sets the custom_fields to be send to KLASSAPP.
        /// it is received as a JSON object
        /// </summary>
        string custom_fields { get; set; }
    }
}
