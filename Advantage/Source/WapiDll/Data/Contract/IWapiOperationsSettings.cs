﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWapiOperationsSettings.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Interface for External Operation Settings <br />
//   Store all necessary information to configure the external operation.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll.Data.Contract
{
    using System;
    using System.Diagnostics;

    /// <summary>
    /// Interface for External Operation Settings <br/>
    /// Store all necessary information to configure the external operation.
    /// </summary>
    public interface IWapiOperationsSettings
    {
        /// <summary>
        /// Gets or sets Unique ID
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// Gets or sets Name of the operation
        /// </summary>
        string CodeOperation { get; set; }

        /// <summary>
        /// Gets or sets Code of the company that owner the web service
        /// in the case that the operation access a external
        /// resource. 
        /// </summary>
        string SecretCodeCompany { get; set; }

        /// <summary>
        /// Gets or sets The type of interaction with the external resource if exists can be:
        /// <ul><il>WAPI_TEST_SYSTEM</il>
        /// <il>VOYANT_CURRENT</il>
        /// <il>VOYANT_HISTORICAL</il></ul> 
        /// </summary>
        string CodeExtOperationMode { get; set; }

        /// <summary>
        /// Gets or sets External URL to looking for, if exists.
        /// </summary>
        string ExternalUrl { get; set; }

        /// <summary>
        /// Gets or sets Security key to interact with external service if exists
        /// </summary>
        string ConsumerKey { get; set; }

        /// <summary>
        /// Gets or sets Security key to interact with external service
        /// if exists.
        /// </summary>
        string PrivateKey { get; set; }

        /// <summary>
        /// Gets or sets The code of operation in the WAPI
        /// The operation to be invoked by the WAPI
        /// </summary>
        string CodeWapiServ { get; set; }

        /// <summary>
        /// Gets or sets Some operation require two operation to be completed
        /// In this case this field is used.
        /// </summary>
        string SecondCodeWapiServ { get; set; }

        /// <summary>
        /// Gets or sets Time that a windows service should repeat the operation
        /// if it is needed. -1 if inactive.0 one time operation.
        /// </summary>
        int OperationSecInterval { get; set; }

        /// <summary>
        /// Gets or sets This property stored the last point of time  
        /// that the operation was executed
        /// Date + Time.
        /// </summary>
        DateTime DateLastExecution { get; set; }

        /// <summary>
        /// Gets or sets Poll WAPI from windows service to look for a configuration change.
        /// </summary>
        int PollSecOnDemandOperation { get; set; }

        /// <summary>
        /// Gets or sets  0: inactive, 1: active
        /// </summary>
        int FlagOnDemandOperation { get; set; }

        /// <summary>
        /// Gets or sets 0: inactive, 1: active
        /// </summary>
        int FlagRefreshConfig { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        string UserName { get; set; }

        /// <summary>
        /// Gets or sets the first allowed service query string.
        /// </summary>
        string FirstAllowedServiceQueryString { get; set; }


        #region Specific to communicate with WAPI PROXY and log info

        /// <summary>
        /// Gets or sets The address of the proxy
        /// </summary>
        /// <remarks>
        /// Should be the same for all operations.
        /// but for now is in each operation
        /// </remarks>
        string UrlProxy { get; set; }

        /// <summary>
        /// Gets or sets The address of the log file
        /// </summary>
        /// <remarks>
        /// Should be the same for all operations.
        /// but for now is in each operation
        /// </remarks>
        string LogFile { get; set; }

        /// <summary>
        /// Gets or sets The size of the log file
        /// </summary>
        /// <remarks>
        /// Should be the same for all operations.
        /// but for now is in each operation
        /// </remarks>
        int SizeLogFile { get; set; }

        /// <summary>
        /// Gets or sets You can put here a trace instance to pass trace information
        /// to other class
        /// WARINIG if you don't use this. be sure that the class use
        /// some method to instantiate by default the trace.
        /// </summary>
        TraceSource Tracer { get; set; }

        #endregion
    }
}