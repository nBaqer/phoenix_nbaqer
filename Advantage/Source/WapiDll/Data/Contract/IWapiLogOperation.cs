﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWapiLogOperation.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   This class hold the log operation
//   for external operation setting.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll.Data.Contract
{
    using System;

    /// <summary>
    /// This class hold the log operation
    /// for external operation setting.
    /// </summary>
    public interface IWapiLogOperation
    {
        /// <summary>
        /// Gets or sets ID of the log record
        /// </summary>
        long Id { get; set; }

        /// <summary>
        ///  Gets or sets Service Code registered by the log record
        /// </summary>
        string ServiceCode { get; set; }

        /// <summary>
        ///  Gets or sets Company code under was executed the operation.
        /// </summary>
        string CompanyCode { get; set; }

        /// <summary>
        ///  Gets or sets Last date time that operation was executed.
        /// </summary>
        DateTime DateExecution { get; set; }

        /// <summary>
        ///  Gets or sets Next planned date and time that the operation again should be executed.
        /// </summary>
        DateTime NextPlanningDate { get; set; }

        /// <summary>
        ///  Gets or sets 1 if the result of the operation was error. 0 if was successful
        /// </summary>
        int IsError { get; set; }

        /// <summary>
        ///  Gets or sets Free Comment.
        /// </summary>
        string Comment { get; set; }
    }
}
