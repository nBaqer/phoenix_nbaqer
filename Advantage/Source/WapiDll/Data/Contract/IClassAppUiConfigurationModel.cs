﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IClassAppUiConfigurationModel.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The <code>ClassAppUiConfigurationModel</code> interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll.Data.Contract
{
    using System.Collections.Generic;

    /// <summary>
    /// The <code>ClassAppUiConfigurationModel</code> interface.
    /// </summary>
    public interface IClassAppUiConfigurationModel
    {
        /// <summary>
        /// Gets or sets the configuration setting.
        /// </summary>
        IList<ClassAppConfigurationModel> ConfigurationSetting { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        ClassAppFilter Filter { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is any configuration pending.
        /// </summary>
        bool IsAnyConfigurationPending { get; set; }
    }
}