﻿namespace WapiDll.Data.Contract
{
    /// <summary>
    /// Hold the Id of operation and the value of On demand flag.
    /// </summary>
    public interface IOnDemandFlag
    {
        /// <summary>
        /// Operation id
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// Value of Flag On Demand Operation.
        /// </summary>
        int OnDemandOperation { get; set; }
    }
}
