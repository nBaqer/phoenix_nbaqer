﻿using System.Collections.Generic;
using WapiDll.Data.Contract;

namespace WapiDll.Data
{
    /// <summary>
    /// Hold the Id of operation and the value of On demand flag.
    /// </summary>
    public class OnDemandFlag: IOnDemandFlag
    {
        /// <summary>
        /// Use this instead the default constructor to create
        /// a instance of the class.
        /// </summary>
        /// <returns>A instance of IOnDemandFlag</returns>
        public static IOnDemandFlag Factory()
        {
            IOnDemandFlag flag = new OnDemandFlag();
            flag.Id = -1;
            flag.OnDemandOperation = 0;
            return flag;
        }
        
        #region Implementation of IOnDemandFlag

        /// <summary>
        /// Operation id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Value of Flag On Demand Operation.
        /// </summary>
        public int OnDemandOperation { get; set; }

        #endregion
    }
}
