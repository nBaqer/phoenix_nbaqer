﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppReturnedLocation.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The KLASSAPP returned location.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll.Data
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// The KLASSAPP returned location.
    /// </summary>
    [SuppressMessage("ReSharper", "StyleCop.SA1300", Justification = "Here is valid")]
    [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "Here is valid")]
    public class KlassAppReturnedLocation
    {
        /// <summary>
        /// Gets or sets the ID
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string name { get; set; }
    }
}