﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiService.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   This is a abstract base class to implement WAPI Services
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Timers;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using WapiAdvantage;
    using WapiAdvantage.AccessProxy;
    using WapiAdvantage.Data;

    using WapiDll.Data;
    using WapiDll.Data.Contract;

    /// <summary>
    /// This is a abstract base class to implement WAPI Services
    /// </summary>
    public abstract class WapiService : IWapiService
    {
        #region Operation Constants

        /// <summary>
        /// Code to get all operation managed by the windows server.
        /// </summary>
        public const string XWsOperations = "RSETTING";

        /// <summary>
        /// Set data last execution of a specific operation
        /// </summary>
        public const string PostDataLastExecution = "POST_DATA_LAST_EXECUTION";

        /// <summary>
        /// Set the flag on demand for windows service operations
        /// </summary>
        public const string SetOnDemandFlag = "SET_ON_DEMAND_FLAG";

        /// <summary>
        /// Code of service for get the on-demand flags or the on-demand flag for a specific operation.
        /// </summary>
        public const string GetOnDemandFlag = "GET_ON_DEMAND_FLAG";

        /// <summary>
        /// Write the operation log in advantage
        /// </summary>
        public const string Wlog = "WLOG";

        #endregion

        #region Fields

        /// <summary>
        /// This is static and use in all derived class.
        /// </summary>
        private static string publicKey;

        ////private WapiAdvantage.WapiAdvantage proxy;

        /// <summary>
        /// The operation.
        /// </summary>
        private IWapiOperationsSettings operation;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WapiService"/> class. 
        /// Constructor. Create the timer and events <br/>
        /// Initialize the list of instanced WAPI Services.
        /// </summary>
        protected WapiService()
        {
            // proxyKey = Guid.Empty.ToString();
            publicKey = string.Empty;
            this.ServiceTimer = new Timer();
            this.ServiceTimer.Elapsed += this.ServiceTimer_Elapsed;
            this.WapiServiceList = new List<IWapiService>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets Access to the proxy operations.
        /// </summary>
        public WapiAdvantage Proxy { get; set; }

        /// <summary>
        ///  Gets or sets Operation settings 
        /// </summary>
        public IWapiOperationsSettings Operation
        {
            get
            {
                return this.operation;
            }

            set
            {
                this.operation = value;
                var config = new ProxyConfiguration
                {
                    CompanyKey = this.operation.SecretCodeCompany,
                    ProxyUrl = this.operation.UrlProxy,
                    LogFile = this.operation.LogFile,
                    LogKb = this.operation.SizeLogFile
                };
                this.Proxy = new WapiAdvantage(config);
            }
        }

        /// <summary>
        ///  Gets or sets Timer to control the operations
        /// </summary>
        public Timer ServiceTimer { get; set; }

        /// <summary>
        ///  Gets or sets Store the list of all operation active in the system.
        /// </summary>
        public IList<IWapiService> WapiServiceList { get; set; }

        /// <summary>
        /// Gets or sets the optional message.
        /// </summary>
        public string OptionalMessage { get; set; }

        #endregion

        #region Abstract Timer Service

        /// <summary>
        /// Abstract timer elapsed event<br/>
        /// Here should be executed the action of the WAPI Service<br/>
        /// at regular interval of time.
        /// </summary>
        /// <param name="sender">The sender timer</param>
        /// <param name="e">
        /// See Reference <see cref="ElapsedEventArgs"/>
        /// </param>
        protected virtual void ServiceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ////throw new NotImplementedException();
        }

        #endregion

        #region Static method: get List of operation settings from proxy

        /// <summary>
        /// Return the list of assigned operation to Windows Service to the specific CompanyId
        /// </summary>
        /// <param name="secretkey">
        /// The company secret key.
        /// This key is given by Advantage to identify the client.
        /// </param>
        /// <param name="proxyUrl">
        /// The Address of the WAPI Proxy.
        /// </param>
        /// <param name="responseReason">
        /// The response Reason.
        /// </param>
        /// <returns>
        /// The list of Operation setting <see cref="IWapiOperationsSettings"/>
        /// </returns>
        public static IList<IWapiOperationsSettings> GetListOperationsFromProxy(string secretkey, string proxyUrl, out string responseReason)
        {
            // Declare trace values...

            // Retrieve public key....
            var proxy = new ServiceProxy();
            publicKey = proxy.GetPublicKey(proxyUrl);
            if (string.IsNullOrEmpty(publicKey))
            {
                throw new CustomAttributeFormatException(
                    "An unhandle exception has occurred, check that the The Public Key for encryption is not null or empty or that there is not duplicated entries in syWapiAllowedServices. Process finished ProxyUrl is " + proxyUrl);

                ////return null; // Indicate that some error occurred. Here we can log error in the operation
            }

            // Send encrypted companyCode
            var key = proxy.GetAccessKey(secretkey, publicKey, proxyUrl);
            if (string.IsNullOrEmpty(key))
            {
                throw new CustomAttributeFormatException(
                    "The Key to access the proxy is returned null or empty. Process finished");

                //// return null; // Indicate that some error occurred. Here we can log error in the operation
            }

            // if OK send the request to get the operations list
            var result = proxy.SendOperation(XWsOperations, key, proxyUrl + "?IdOperation=0&OnlyActives=1", out responseReason);
            if (responseReason == "OK")
            {
                // It is OK
                IList<IWapiOperationsSettings> resultList = new List<IWapiOperationsSettings>();
                var objects = JArray.Parse(result); // parse as array  
                foreach (JObject root in objects)
                {
                    IWapiOperationsSettings setting = new WapiOperationsSettings();

                    foreach (KeyValuePair<string, JToken> app in root)
                    {
                        switch (app.Key.ToUpper())
                        {
                            case "ID":
                                {
                                    setting.ID = (int)app.Value;
                                    break;
                                }

                            case "CODEEXTOPERATIONMODE":
                                {
                                    setting.CodeExtOperationMode = (string)app.Value;
                                    break;
                                }

                            case "CODEOPERATION":
                                {
                                    setting.CodeOperation = (string)app.Value;
                                    break;
                                }

                            case "CODEWAPISERV":
                                {
                                    setting.CodeWapiServ = (string)app.Value;
                                    break;
                                }

                            case "CONSUMERKEY":
                                {
                                    setting.ConsumerKey = (string)app.Value;
                                    break;
                                }

                            case "PRIVATEKEY":
                                {
                                    setting.PrivateKey = (string)app.Value;
                                    break;
                                }
                            case "USERNAME":
                                {
                                    setting.UserName = (string)app.Value;
                                    break;
                                }
                            case "EXTERNALURL":
                                {
                                    setting.ExternalUrl = (string)app.Value;
                                    break;
                                }

                            case "FLAGONDEMANDOPERATION":
                                {
                                    setting.FlagOnDemandOperation = (int)app.Value;
                                    break;
                                }

                            case "FLAGREFRESHCONFIG":
                                {
                                    setting.FlagRefreshConfig = (int)app.Value;
                                    break;
                                }

                            case "OPERATIONSECINTERVAL":
                                {
                                    setting.OperationSecInterval = (int)app.Value;
                                    break;
                                }

                            case "POLLSECONDEMANDOPERATION":
                                {
                                    setting.PollSecOnDemandOperation = (int)app.Value;
                                    break;
                                }

                            case "SECONDCODEWAPISERV":
                                {
                                    setting.SecondCodeWapiServ = (string)app.Value;
                                    break;
                                }

                            case "CODEEXTCOMPANY":
                                {
                                    setting.SecretCodeCompany = (string)app.Value;
                                    break;
                                }

                            case "DATELASTEXECUTION":
                                {
                                    setting.DateLastExecution = (DateTime)app.Value;
                                    break;
                                }

                            case "FIRSTALLOWEDSERVICEQUERYSTRING":
                                {
                                    setting.FirstAllowedServiceQueryString = (string)app.Value;
                                    break;
                                }

                        }
                    }

                    resultList.Add(setting);
                }

                return resultList;
            }

            return null;
        }

        /// <summary>
        /// Calculate on the fly the interval in seconds between now and the planning executing date <br/>
        /// if the next time is before than now a negative value is returned. this indicate that the
        /// procedure should be executed as soon as possible. 
        /// it is return positive this is the value in SECONDS that should be loaded in the timer
        /// NOTE: the value does not necessary is the real value. if the value in second (converted to ms)
        /// is greater than the max value of the counter, then the maxi value of the counter is loaded.
        /// </summary>
        /// <param name="operation">Store the poll interval and the last date-time execution.</param>
        /// <returns> SECONDS between last time executed and now</returns>
        /// <remarks>
        ///  The value can be negative.
        /// </remarks>
        protected static double TimeIntervalCalculation(IWapiOperationsSettings operation)
        {
            var nexttime = NextPlanningDate(operation, 0);
            var now = DateTime.Now;
            var intervalActual = (nexttime - now).TotalSeconds;
            intervalActual = CheckIfIntervalIsMoreThanMaxInt(intervalActual);
            return intervalActual;
        }

        /// <summary>
        /// Calculate the next execution time based in the time interval and the date of the last execution.
        /// </summary>
        /// <param name="operation">The operation to log.</param>
        /// <param name="isError">1 is error</param>
        /// <returns></returns>
        protected static DateTime NextPlanningDate(IWapiOperationsSettings operation, int isError)
        {
            var lastTime = operation.DateLastExecution;
            DateTime nexttime;
            if (isError == 1)
            {
                nexttime = lastTime.AddSeconds(600);
            }
            else
            {
                var interval = operation.OperationSecInterval;
                nexttime = lastTime.AddSeconds(interval);
            }

            return nexttime;
        }

        /// <summary>
        /// Compute if the date of the next execution is left, in this case return 1 Second 
        /// if the time is not left, then return in Seconds the time to the next execution.
        /// </summary>
        /// <param name="operation">The External Operation Setting <see cref="IWapiOperationsSettings"/></param>
        /// <returns>Calculated interval of Time in seconds</returns>
        protected static double PositiveTimerIntervalCalculation(IWapiOperationsSettings operation)
        {
            var intervalo = TimeIntervalCalculation(operation);
            intervalo = (intervalo < 1) ? 1 : intervalo;
            Debug.WriteLine("Interval calculated {0} seconds", intervalo);
            return intervalo;
        }

        /// <summary>
        /// Check if the value to calculate the interval time is higher  than 
        /// the maximum interval time (Max int 32 value)
        /// </summary>
        /// <param name="intervalActual">The actual interval</param>
        /// <returns>The returned interval</returns>
        private static double CheckIfIntervalIsMoreThanMaxInt(double intervalActual)
        {
            var msinterval = intervalActual * 1000;
            var result = (msinterval > int.MaxValue) ? (int.MaxValue / 1000) : intervalActual;
            return result;
        }



        #endregion

        #region Time Controls Methods

        /// <summary>
        /// Enable and init the timer of the class.
        /// </summary>
        public virtual void StartTimer()
        {
            this.ServiceTimer.Enabled = true;
            this.ServiceTimer.Start();
        }

        /// <summary>
        /// Stop and disable the timer of the class.
        /// </summary>
        public virtual void StopTimer()
        {
            this.ServiceTimer.Enabled = false;
            this.ServiceTimer.Stop();
        }

        /// <summary>
        /// This should contain the payload of the time-elapsed code.<br/>
        /// to be executed by external node.<br/>
        /// Put here the core of the actions, and call it inside the<br/> 
        /// elapsed-timer event. That possibility that the action<br/>
        /// can be called for other object.
        /// </summary>
        public abstract void ExecuteActions();

        public virtual void ExecuteActions(IWapiOperationsSettings operationsSettings)
        {
        }

        #endregion

        #region Specific Advantage Windows Service Management Operations

        /// <summary>
        /// Change the value of O-demand flag for one specific operation.
        /// </summary>
        /// <param name="id">ID of the operation setting to be changed</param>
        /// <param name="flagVal">Flag value 0 Inactive / 1 Active</param>
        public void PostOnDemandFlag(int id, int flagVal)
        {
            this.Proxy.PostMessageToWapi(SetOnDemandFlag, string.Empty, string.Format("?Id={0}&Value={1}", id, flagVal));

            // WapiProxyDialogExchangePost(SET_ON_DEMAND_FLAG, string.Empty, string.Format("?Id={0}&Value={1}", id, flagVal));
        }

        /// <summary>
        /// Get the list of demand flag or the individual flag
        /// </summary>
        /// <param name="id">-1 or not present: return all, present return the on-demand flag for the specific operation ID</param>
        /// <returns>A list of IOnDemandFlag <see cref="IOnDemandFlag"/></returns>
        public IList<IOnDemandFlag> GetOnDemandFlags(int id = -1)
        {
            // var result = WapiProxyDialogExchangeGet(GET_ON_DEMAND_FLAG, string.Format("?Id={0}", id));
            var result = this.Proxy.GetMessageFromWapi(GetOnDemandFlag, string.Format("?Id={0}", id));
            IList<IOnDemandFlag> listFlags = new List<IOnDemandFlag>();
            var objects = JArray.Parse(result); // parse as array  
            foreach (JObject root in objects)
            {
                IOnDemandFlag flag = OnDemandFlag.Factory();

                foreach (KeyValuePair<string, JToken> app in root)
                {
                    switch (app.Key.ToUpper())
                    {
                        case "ID":
                            {
                                flag.Id = (int)app.Value;
                                break;
                            }

                        case "ONDEMANDOPERATION":
                            {
                                flag.OnDemandOperation = (int)app.Value;
                                break;
                            }
                    }

                }

                listFlags.Add(flag);
            }

            return listFlags;
        }

        /// <summary>
        /// Post the date of last execution of the specific operation setting service given in the ID parameter.
        /// </summary>
        /// <param name="id">The ID of the external operation setting.</param>
        /// <param name="operationDate">The date and time of last execution of the operation</param>
        public void PostDateLastOperation(int id, DateTime operationDate)
        {
            this.Proxy.PostMessageToWapi(
                PostDataLastExecution,
                string.Empty,
                $"?Id={id}&Value={operationDate}");

            // WapiProxyDialogExchangePost(POST_DATA_LAST_EXECUTION, string.Empty, string.Format("?Id={0}&Value={1}", id, opDate));
        }

        /// <summary>
        /// Post a log record to server...
        /// This log post the operation to advantage table 
        /// for Operation Setting operation logger.
        /// </summary>
        /// <param name="op">Setting Operation referred in the log.</param>
        /// <param name="comment">A free comment</param>
        /// <param name="isError">0 if you log a error, 1 if you log a error.</param>
        public void PostLogToServer(IWapiOperationsSettings op, string comment, int isError)
        {
            var log = WapiLogOperation.Factory();
            log.CompanyCode = op.SecretCodeCompany;
            log.IsError = isError;
            log.ServiceCode = op.CodeOperation;
            log.NextPlanningDate = NextPlanningDate(op, isError);

            var index = comment.ToLower().IndexOf("<!doctype html>", StringComparison.Ordinal);
            comment = (index > -1) ? comment.Remove(index) : comment;
            log.Comment = comment;

            var payload = JsonConvert.SerializeObject(log);
            this.Proxy.PostMessageToWapi(Wlog, payload, string.Empty);

            // WapiProxyDialogExchangePost(WLOG, payload, string.Empty);
        }

        #endregion
    }
}
