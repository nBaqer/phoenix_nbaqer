﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiServiceVoyantHistorical.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   CLass for External Operation: VOYANT Historical Attendance Student <br />
//   This class is tailored to executed the operation at regular interval programmed by the operation setting.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading;
    using System.Timers;

    using Newtonsoft.Json.Linq;

    using WapiDll.Data.Contract;
    using WapiDll.Helpers;

    using WapiVoyantClient;

    /// <summary>
    /// CLass for External Operation: VOYANT Historical Attendance Student <br/>
    /// This class is tailored to executed the operation at regular interval programmed by the operation setting.
    /// </summary>
    /// <remarks>This class is for use with Advantage Windows Service.</remarks>
    public class WapiServiceVoyantHistorical : WapiService
    {
        #region Class Factory
        /// <summary>
        /// Use this to create a instance of the class <br/>
        /// Do not use the implicit constructor.
        /// </summary>
        public static IWapiService Factory(IWapiOperationsSettings operation, ref IList<IWapiService> listOfServices)
        {
            IWapiService service = new WapiServiceVoyantHistorical();
            if (operation.Tracer == null)
            {
                operation.Tracer = new TraceSource("TraceWapiWs");
            }
            service.Operation = operation;
            service.ServiceTimer.Interval = PositiveTimerIntervalCalculation(operation) * 1000;
            service.WapiServiceList = listOfServices;
            return service;
        }
        #endregion

        #region Overrides of WapiService

        /// <summary>
        /// Here is executed the action of the class
        /// </summary>
        /// <param name="sender">The timer that raise the event</param>
        /// <param name="e"><see cref="ElapsedEventArgs"/></param>
        protected override void ServiceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            base.ServiceTimer_Elapsed(sender, e);
            var timer = (System.Timers.Timer)sender;
            timer.Stop();
            
            // See if the moment of execution is now
            double secondsToNextExecution = PositiveTimerIntervalCalculation(Operation);
            try
            {
                if (secondsToNextExecution < 0.1)
                {
                    //Execute the actions
                    ExecuteActions();
                    Operation.DateLastExecution = DateTime.Now;
                    PostDateLastOperation(Operation.ID, Operation.DateLastExecution);
                    PostLogToServer(Operation, string.Format("Operation: {0} success", Operation.CodeOperation), 0);
                    secondsToNextExecution = PositiveTimerIntervalCalculation(Operation);
                    Operation.Tracer.TraceInformation("Voyant Model(historical) operation completed successful");
                    Debug.WriteLine("Voyant Model(historical) operation completed successful");
                  
                }
                else
                {
                    Debug.WriteLine("Voyant Model(historical) operation no executed because is not the moment to execute");
                }

            }
            catch (Exception ex)
            {
                try
                {
                    // assume that Voyant is down and try again in 10 minutes. try to log in our database the event
                    Operation.Tracer.TraceData(TraceEventType.Error, 100, string.Format("Exception in Model Operation: {0}", ex.Message));
                    Operation.DateLastExecution = Operation.DateLastExecution.AddMinutes(10);
                    secondsToNextExecution = PositiveTimerIntervalCalculation(Operation);
                    var error = string.Format("Operation: {0} is delayed 10 minutes because throw Exception {1}. ", Operation.CodeOperation, ex.Message);
                    PostLogToServer(Operation, error, 1);
                    Operation.Tracer.TraceInformation(error);
                }
                catch (Exception exc)
                {
                    // If we are here our server is down, log the error and add 10 minutes more....
                    Operation.Tracer.TraceData(TraceEventType.Error, 100, string.Format("Fame WAPI is Down: {0}", exc.Message));
                    Operation.DateLastExecution = Operation.DateLastExecution.AddMinutes(10);
                    secondsToNextExecution = PositiveTimerIntervalCalculation(Operation);
                }

            }

            finally
            {
                timer.Interval = secondsToNextExecution * 1000;
                timer.Start();
 
            }
        }

        /// <summary>
        /// This should contain the payload of the time-elapsed code.
        /// to be executed by external node.
        /// </summary>
        public override void ExecuteActions()
        {
            Debug.WriteLine(string.Format("Push operation {0} begin", Operation.CodeOperation));
            // Get the info from Proxy....

            var payload = Proxy.GetMessageFromWapi(Operation.CodeWapiServ, "?RequestHistorical=1");
            string info;
            if (payload != null)
            {
                payload = payload.Replace("AcademicsDataList", "academics");
                payload = payload.Replace("EnrollmentDataList", "enrollments");
                payload = payload.Replace("FacultyDataList", "fa");
                Debug.WriteLine("Payload length: {0} KB", (payload.Length / 1000));
               // Debug.WriteLine("Payload: {0}", payload);
                // Set for historical data
                Debug.WriteLine(string.Format("payload to remote server: {0}", payload));
                var vy = ApiConnect.Factory(Operation.ConsumerKey, Operation.PrivateKey, Operation.ExternalUrl);
                var parameters = vy.CreateParameters(); // Parameter here must no have ? sign
                var sign = vy.GetSig("", HttpVerb.POST, parameters);
                var parametersplus = string.Format("?{0}", parameters); // Add ? sign to parameters
                var response = vy.PostRequest("", sign, parametersplus, payload);
                if (response.StartsWith("ERROR"))
                {
                    PostLogToServer(Operation, response, 1);
                }
                info = string.Format("Remote Server Respond -> {0}", response);
                Debug.WriteLine(info);
                Operation.Tracer.TraceInformation(info);
                // Poll for model create; The system wait for 2 minutes to get as successfully the operation.
                var initTime = DateTime.Now;
                var endTime = initTime.AddMinutes(2);
                while (initTime < endTime)
                {
                    Thread.Sleep(10000);
                    initTime = DateTime.Now;
                    parameters = vy.CreateParameters(); // Parameter here must no have ? sign
                    sign = vy.GetSig(string.Empty, HttpVerb.GET, parameters);
                    var statusResponse = vy.GetRequest("", HttpVerb.GET, sign, parameters);
                    Debug.WriteLine(string.Format("Historical Remote Server Respond to Get -> {0}", statusResponse));
                    if (Tools.IsJson(statusResponse))
                    {
                        var result = JObject.Parse(statusResponse);
                        var status = (string) result["status"];
                        Debug.WriteLine(string.Format("Remote Server Status -> {0}", status));
                        if (status == "Completed")
                        {
                            initTime = endTime.AddMinutes(1);
                        }
                    }
                    else
                    {
                        if (statusResponse.ToUpperInvariant().StartsWith("ERROR"))
                        {
                            var error = statusResponse.Substring(statusResponse.IndexOf(" :", StringComparison.Ordinal));
                            throw new Exception(error);
                        }
                    }
                  
                   
                }

                // Run the method in the other object
                Debug.WriteLine("Operation Count: {0}", WapiServiceList.Count);

                foreach (IWapiService wapiService in WapiServiceList)
                {
                    Debug.WriteLine("Service: {0}", wapiService.Operation.CodeExtOperationMode);

                    if (wapiService.Operation.CodeExtOperationMode == "VOYANT_CURRENT")
                    {
                        if (wapiService.Operation.SecretCodeCompany == Operation.SecretCodeCompany)
                        {
                            try
                            {
                                wapiService.StopTimer();
                                wapiService.ExecuteActions();
                                PostLogToServer(wapiService.Operation, string.Format("Operation invoked by {0} : {1} success", Operation.CodeOperation, wapiService.Operation.CodeOperation), 0);

                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine(string.Format("Voyant Current get a exception -> {0}", ex.Message));
                                PostLogToServer(wapiService.Operation, string.Format("Operation invoked by {0} : {1} FAIL Exception: {2}", Operation.CodeOperation, wapiService.Operation.CodeOperation, ex.Message), 0);

                            }
                            finally
                            {
                                // Re-init the timer to his original time
                                wapiService.ServiceTimer.Start();
                            }

                            break;
                        }
                    }
                }
            }
            else
            {
                const string ERROR = "Error Accessing Advantage Proxy";
                Debug.WriteLine(ERROR);
                Operation.Tracer.TraceData(TraceEventType.Error, 101, ERROR);
            }

            info = string.Format("Push operation {0} finished", Operation.CodeOperation);
            Debug.WriteLine(info);
            Operation.Tracer.TraceInformation(info);
        }

        #endregion
    }
}
