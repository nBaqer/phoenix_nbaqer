﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WapiDll
{
    using System.Diagnostics;

    using WapiDll.Data.Contract;

    public class WapiServiceAdvantageApi : WapiService
    {
        public static IWapiService Factory(IWapiOperationsSettings operation, ref IList<IWapiService> listOfServices)
        {
            IWapiService service = new WapiServiceAfaIntegration();
            service.Operation = operation;
            service.Operation.Tracer = service.Operation.Tracer ?? new TraceSource("TraceWapiWs");

            // Create the interval of time
            service.ServiceTimer.Interval = WapiService.PositiveTimerIntervalCalculation(operation) * 1000;
            service.WapiServiceList = listOfServices;
            return service;
        }

        public override void ExecuteActions()
        {
            // do nothing
        }
    }
}
