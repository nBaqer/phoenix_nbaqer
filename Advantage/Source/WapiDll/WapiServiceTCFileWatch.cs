﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiServiceTCFileWatch.cs" company="FAME">
//   2019
// </copyright>
// <summary>
//   Defines the WapiServiceTCFileWatch type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace WapiDll
{
    using FAME.Advantage.Api.Library.Models;
    using FAME.Advantage.Api.Library.Models.TimeClock;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Timers;
    using WapiDll.Data.Contract;
    using WapiDll.Helpers;

    /// <summary>
    ///  The WAPI service class application.
    /// </summary>
    public class WapiServiceTCFileWatch : WapiService
    {
        /// <summary>
        /// File locations to watch
        /// </summary>
        public List<AutomatedTimeClockLocations> FileLocationsToWatch { get; set; }

        /// <summary>
        /// File watchers
        /// </summary>
        public List<FileSystemWatcher> FileWatchers { get; set; } = new List<FileSystemWatcher>();

        /// <summary>
        /// List of files currently in process
        /// </summary>
        public List<string> ProcessingFiles { get; set; } = new List<string>();

        /// <summary>
        /// Thread enabler
        /// </summary>
        public ManualResetEvent ResetEvent { get; } = new ManualResetEvent(false);

        /// <summary>
        /// Use this to create a instance of the class <br/>
        /// Do not use the implicit constructor.
        /// </summary>
        /// <param name="operation">The name of the external operation</param>
        /// <param name="listOfServices">List of object of other services running in the Advantage Windows Service</param>
        /// <returns>A instance of the class</returns>
        public static IWapiService Factory(IWapiOperationsSettings operation, ref IList<IWapiService> listOfServices)
        {
            IWapiService service = new WapiServiceTCFileWatch();
            service.Operation = operation;
            service.Operation.Tracer = service.Operation.Tracer ?? new TraceSource("TraceWapiWs");

            // Create the interval of time
            service.ServiceTimer.Interval = WapiService.PositiveTimerIntervalCalculation(operation) * 1000;
            service.WapiServiceList = listOfServices;
            return service;
        }

        #region Overrides of WapiService

        /// <summary>
        /// The execute actions.
        /// </summary>
        public override void ExecuteActions()
        {
            try
            {
                this.FileWatchers.ForEach(x =>
                {
                    x.EnableRaisingEvents = false;
                    x.Dispose();
                });
                this.FileWatchers.Clear();

                IWapiService advantageApiService = this.WapiServiceList.FirstOrDefault(x => x.Operation.CodeExtOperationMode == "ADVANTAGE_API_SERVICE"
                && x.Operation.SecretCodeCompany == this.Operation.SecretCodeCompany);

                if (advantageApiService == null)
                {
                    var message = "Advantage api service is required for file watching service to run";
                    this.Operation.Tracer.TraceInformation(message);
                    this.OptionalMessage = message;
                    return;
                }

                // get core api token
                TokenResponse tokenResponse = new FAME.Advantage.Api.Library.Token(advantageApiService.Operation.ExternalUrl).GetToken(
                    advantageApiService.Operation.ConsumerKey,
                    advantageApiService.Operation.UserName,
                    advantageApiService.Operation.PrivateKey);

                if (tokenResponse.ResponseStatusCode != 200)
                {
                    var message = "WapiServiceTCFileWatch service start was not completed, unable to authenticate Advantage API User. Api returned error: " + tokenResponse.ResponseText;
                    this.Operation.Tracer.TraceInformation(message);
                    this.OptionalMessage = message;
                    return;
                }

                var automatedStoragePathRequest = new FAME.Advantage.Api.Library.TimeClock.AutomatedTimeClockPathsRequest(advantageApiService.Operation.ExternalUrl, tokenResponse.Token);
                FileLocationsToWatch = automatedStoragePathRequest.GetAutomatedTimeClockLocations().Result;
                new Thread(this.CreateFileWatchers).Start();
            }
            catch (Exception ex)
            {
                var message = "WapiServiceTCFileWatch service start was not completed, Exception: " + ex.Message;
                this.Operation.Tracer.TraceInformation(message);
                this.OptionalMessage = message;
            }
        }

        private void CreateFileWatchers()
        {
            try
            {
                foreach (var automatedCampusPath in FileLocationsToWatch)
                {
                    try
                    {
                        var isNetworkShare = !string.IsNullOrEmpty(automatedCampusPath.Username) && !string.IsNullOrEmpty(automatedCampusPath.Password);

                        if (isNetworkShare) //network share
                        {
                            new Thread(() =>
                            {
                                try
                                {
                                    var credentials = automatedCampusPath.Username.Split('\\');
                                    using (Impersonator imp = new Impersonator(credentials.Last(), credentials.Reverse().Skip(1).FirstOrDefault(), automatedCampusPath.Password))
                                    {
                                        imp.Identity.Impersonate();
                                        FileSystemWatcher watcher = new FileSystemWatcher();
                                        watcher.Path = automatedCampusPath.Path;
                                        watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName;
                                        watcher.Created += new FileSystemEventHandler(FileFoundEvent);
                                        watcher.Error += new ErrorEventHandler(WatcherError);
                                        watcher.EnableRaisingEvents = true;
                                        this.FileWatchers.Add(watcher);
                                        this.PostLogToServer(this.Operation, $"Began watching directory | {automatedCampusPath.Path} | campusId : {automatedCampusPath.CampusId}", 0);
                                    }
                                }
                                catch (Exception e)
                                {
                                    this.PostLogToServer(this.Operation, $"Error creating file system watcher for path : {automatedCampusPath.Path} campusId : {automatedCampusPath.CampusId} message : {e.Message}", 1);
                                }
                            }).Start();
                        }
                        else // file system
                        {
                            new Thread(() =>
                            {
                                try
                                {
                                    FileSystemWatcher watcher = new FileSystemWatcher();
                                    watcher.Path = automatedCampusPath.Path;
                                    watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName;
                                    watcher.Created += new FileSystemEventHandler(FileFoundEvent);
                                    watcher.Error += new ErrorEventHandler(WatcherError);
                                    watcher.EnableRaisingEvents = true;
                                    this.FileWatchers.Add(watcher);
                                    this.PostLogToServer(this.Operation, $"Began watching directory | {automatedCampusPath.Path} | campusId : {automatedCampusPath.CampusId}", 0);
                                }
                                catch (Exception e)
                                {
                                    this.PostLogToServer(this.Operation, $"Error creating file system watcher for path : {automatedCampusPath.Path}| campusId : {automatedCampusPath.CampusId} | message : {e.Message}", 1);
                                }
                            }).Start();
                        }
                    }
                    catch (Exception e)
                    {
                        this.PostLogToServer(this.Operation, $"Error spawning file watcher threads : {e.Message}", 1);
                    }
                }

                ResetEvent.WaitOne();
            }
            catch (Exception e)
            {
                this.Operation.Tracer.TraceInformation(e.Message);
                this.OptionalMessage = e.Message;
                this.PostLogToServer(this.Operation, $"Error spawning file watcher threads: " + e.Message, 1);
                return;
            }
        }

        private void FileFoundEvent(object source, FileSystemEventArgs e)
        {
            try
            {
                //catches multiple on change events for the same file, ignore if file is already processing
                if (ProcessingFiles.Contains(e.FullPath))
                {
                    return;
                }

                //add file to processing array
                ProcessingFiles.Add(e.FullPath);

                //wait three seconds before continuing
                Thread.Sleep(3000);

                //identify campusId based on path file is located
                var campusId = this.FileLocationsToWatch
                  .Where(a => (Path.GetFullPath(new Uri(a.Path).LocalPath)
                 .TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                 .ToUpperInvariant()) == (Path.GetFullPath(new Uri(Path.GetDirectoryName(e.FullPath)).LocalPath)
                 .TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                 .ToUpperInvariant()))
                  .FirstOrDefault().CampusId;

                // get core api token
                IWapiService advantageApiService = this.WapiServiceList.FirstOrDefault(x => x.Operation.CodeExtOperationMode == "ADVANTAGE_API_SERVICE"
              && x.Operation.SecretCodeCompany == this.Operation.SecretCodeCompany);

                TokenResponse tokenResponse = new FAME.Advantage.Api.Library.Token(advantageApiService.Operation.ExternalUrl).GetToken(
                    advantageApiService.Operation.ConsumerKey,
                    advantageApiService.Operation.UserName,
                    advantageApiService.Operation.PrivateKey);

                //begin request to import time clock file
                var input = new TimeClockImportParams()
                {
                    CampusId = campusId,
                    FilePath = e.FullPath
                };

                new Thread(() =>
                {
                    try
                    {

                        var timeClockImportRequest = new FAME.Advantage.Api.Library.TimeClock.ImportTimeClockFileRequest(advantageApiService.Operation.ExternalUrl, tokenResponse.Token);
                        var importRequestResponse = timeClockImportRequest.ImportTimeClockFile(input);
                        var isError = importRequestResponse.ResultStatus == FAME.Advantage.Api.Library.Models.Common.Enums.ResultStatus.Error;
                        this.PostLogToServer(this.Operation, $"{importRequestResponse.ResultStatusMessage} | File : {input.FilePath} | Campus : {input.CampusId}", 0);

                        //remove file from processing array
                        ProcessingFiles.Remove(e.FullPath);
                    }
                    catch (Exception tEx)
                    {
                        this.PostLogToServer(this.Operation, $"FileFoundEvent exception inside thread | File : {e?.FullPath} | Exception : {tEx.Message}", 1);
                    }
                }).Start();
            }
            catch (Exception ex)
            {
                this.PostLogToServer(this.Operation, $"FileFoundEvent exception | File : {e?.FullPath} | Exception : {ex.Message}", 1);
                ProcessingFiles.Remove(e.FullPath);
            }
        }

        // The error event handler. Attempts to restart watcher 5 times in 60 seconds. 
        private void WatcherError(object source, ErrorEventArgs e)
        {
            var fsw = (FileSystemWatcher)source;

            try
            {
                Exception watchException = e.GetException();
                this.PostLogToServer(this.Operation, $"Watcher at path {fsw.Path} failed to watch, exception : {watchException.Message} : Attempting to restart.", 1);

                int attempts = 0;
                while (!fsw.EnableRaisingEvents && attempts <= 5)
                {
                    try
                    {
                        fsw.EnableRaisingEvents = true;
                        this.PostLogToServer(this.Operation, $"Watcher at path {fsw.Path} successfully restarted.", 0);
                        break;
                    }
                    catch (Exception err)
                    {
                        this.PostLogToServer(this.Operation, $"Watcher at path {fsw.Path} failed to watch, exception : {err.Message} : Attempting to restart.", 1);
                        Thread.Sleep(12000);
                        attempts++;
                    }
                }
            }
            catch (Exception exc)
            {
                this.PostLogToServer(this.Operation, $"Watcher at path {fsw.Path} error event exception. Exception : {exc.Message}", 1);
            }
        }

        /// <summary>
        /// The service timer elapsed.
        /// </summary>
        /// <param name="sender">
        ///  The sender.
        /// </param>
        /// <param name="e">
        ///  The e.
        /// </param>
        protected override void ServiceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            base.ServiceTimer_Elapsed(sender, e);
            var timer = (System.Timers.Timer)sender;
            timer.Stop();
            try
            {
                this.ResetEvent.Set();
                this.ExecuteActions();
                var message = $"File watching operation has started";
                Debug.WriteLine(message);
                this.Operation.DateLastExecution = DateTime.Now;
                this.PostDateLastOperation(this.Operation.ID, this.Operation.DateLastExecution);
                this.PostLogToServer(this.Operation, message, 0);
                this.OptionalMessage = message;
                var interval = PositiveTimerIntervalCalculation(this.Operation);
                timer.Interval = interval * 1000;
                timer.Start();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error in Operation: {ex.Message} StackTrace: {ex.StackTrace}{Environment.NewLine}");
                this.Operation.Tracer.TraceData(
                    TraceEventType.Error,
                    200,
                    $"Error in Operation: {ex.Message} StackTrace: {ex.StackTrace}{Environment.NewLine}");
                this.PostLogToServer(
                    this.Operation,
                    $"Operation: {this.Operation.CodeOperation} is delayed 10 minutes because throw Exception {ex.Message} StackTrace: {ex.StackTrace}{Environment.NewLine}.",
                    1);

                // Set 10 minutes the next start
                timer.Interval = 600000;
                timer.Start();
            }
        }
        #endregion
    }

}
