﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiServiveAutomaticFSAPCheck.cs" company="FAMe INC">
//   2018
// </copyright>
// <summary>
//   Defines the WapiServiveAutomaticFSAPCheck type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace WapiDll
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Net.Http;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using System.Timers;
    using System.Web.UI.WebControls;
    using Fame.Integration.Adv.Afa.Messages.WebApi.Entities;
    using FAME.Advantage.Api.Library;
    using FAME.Advantage.Api.Library.Admissions;
    using FAME.Advantage.Api.Library.Models;
    using FAME.Advantage.Api.Library.AcademicRecords;

    /// <summary>
    /// The wapi servive automatic fsap check.
    /// </summary>
    public class WapiServiceAutomaticFSAPCheck : WapiService
    {
        /// <summary>
        /// The advantage wapi service operation.
        /// </summary>
        private IWapiService advantageWapiServiceOperation;

        /// <summary>
        /// The token response.
        /// </summary>
        private TokenResponse tokenResponse;

        /// <summary>
        /// The service timer.
        /// </summary>
        private Timer timer;

        /// <summary>
        /// The interval.
        /// </summary>
        private int Interval;

        /// <summary>
        /// Initializes a new instance of the <see cref="WapiServiceAutomaticFSAPCheck"/> class.
        /// </summary>
        /// <param name="advantageWapiServiceOperation">
        /// The advantage wapi service operation.
        /// </param>
        public WapiServiceAutomaticFSAPCheck(IWapiService advantageWapiServiceOperation)
        {
            this.advantageWapiServiceOperation = advantageWapiServiceOperation;
            this.tokenResponse = new Token(advantageWapiServiceOperation.Operation.ExternalUrl).GetToken(advantageWapiServiceOperation.Operation.ConsumerKey, "support@fameinc.com", advantageWapiServiceOperation.Operation.PrivateKey);
        }

        /// <summary>
        /// The execute actions.
        /// </summary>
        public override void ExecuteActions()
        {
            this.timer = new Timer(86400000);
            this.timer.Elapsed += this.ServiceTimer_Elapsed;
            this.timer.Start();
            var TitleIVSAPRequest = new TitleIVSAPRequest(tokenResponse.ApiUrl, tokenResponse.Token);
            var temp = TitleIVSAPRequest.RunIVSapCheckForStudents();
        }

        /// <summary>
        /// The service timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void ServiceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            base.ServiceTimer_Elapsed(sender, e);
            timer = (Timer)sender;
            timer.Stop();
            try
            {
                this.ExecuteActions();
                var message = $"Operation: {this.advantageWapiServiceOperation.Operation.CodeOperation} success.";
                Debug.WriteLine(message);
                // 24 hours 
                timer.Interval = 86400000;
                timer.Start();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error in Operation: {ex.Message}");
                this.Operation.Tracer.TraceData(
                    TraceEventType.Error,
                    200,
                    $"Error in Operation: {ex.Message} StackTrace: {ex.StackTrace}{Environment.NewLine}");
                this.Operation.DateLastExecution = DateTime.Now; // this.Operation.DateLastExecution.AddMinutes(10);
                this.PostLogToServer(
                    this.Operation,
                    $"Operation: {this.Operation.CodeOperation} is delayed 10 minutes because throw Exception {ex.Message} StackTrace: {ex.StackTrace}{Environment.NewLine}.",
                    1);

                // Set 10 minutes the next start
                timer.Interval = 600000;
                timer.Start();
            }
        }
    }
}
