﻿/// <reference path="../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" /> 

module Api.Components {
    import IFilterRoles = Api.SystemCatalog.Models.IRoles;

    export class RolesDropDownList  {
        selectedRoleId: string;
        componentId: string;
        userTypeId: string;
        roles : Roles;
        private dropDownList: kendo.ui.DropDownList;

        constructor(componentId: string, userTypeId:string, selectedRoleId: string, onchange: (response) => any) {

            let that = this;
            that.roles = new Roles();

            /**
             * Append # to the component Id
             */
            componentId = "#" + componentId;
            that.componentId = componentId;
            that.userTypeId = userTypeId;
            $(componentId).kendoDropDownList({
                dataSource: that.userTypeId ? that.roles.getRolesByUserType(() => that.onRequestFilterData(that)) : that.roles.getAllRoles(() => {}),
                optionLabel: "Select",
                dataTextField: "text",
                dataValueField: "value"
            }).change((value) => {
                if (value)
                    that.selectedRoleId = $(componentId).data("kendoDropDownList").value();

                onchange(value);
                });

            that.dropDownList = $(componentId).data("kendoDropDownList");

        }

        /**
         * Event that will be before the request is made to append the parameters to the request.
         * This is where the DTO is build that is pass as an input parameter on the API.
         * @param that
         */
        private onRequestFilterData(that: RolesDropDownList): any {
            let filterData: IFilterRoles = {} as IFilterRoles;
            filterData.userTypeId = that.userTypeId;
            return filterData;

        }
        setUserType(userTypeId: string) {
            this.userTypeId = userTypeId;
        }
        reloadDataSource() {
            this.dropDownList.setDataSource(this.userTypeId ? this.roles.getRolesByUserType(() => this.onRequestFilterData(this)) : this.roles.getAllRoles(() => {}));
        }
        reset() {
            this.dropDownList.value("");
        }
        getKendoDropDown() {
            return this.dropDownList;
        }
    }

}
