﻿/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
module Api.Components.SystemCatalog {
    export class StudentAcademicCalendar {
        public isClockHour(enrollmentId: string, onResponseCallback: (response: Response) => any):any {
            let academicCalendarType = new AcademicCalendar();
            return academicCalendarType.isClockHour(enrollmentId, onResponseCallback);
        }

        public isPaymentPeriod(periodTypeId: string, onResponseCallback: (response: Response) => any): any {
            let academicCalendar = new AcademicCalendar();
            return academicCalendar.isPaymentPeriod(periodTypeId, onResponseCallback);
        }
    }
}