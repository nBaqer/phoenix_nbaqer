﻿/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
module Api.Components.SystemCatalog {
    export class UserRoleStatusCheck {
        public isSupportUser(onResponseCallback: (response: Response) => any) {
            let userRoleStatus = new UserRoleStatus();
            let request = new Request();
            return userRoleStatus.isSupportUser(onResponseCallback);
        }
    }
}