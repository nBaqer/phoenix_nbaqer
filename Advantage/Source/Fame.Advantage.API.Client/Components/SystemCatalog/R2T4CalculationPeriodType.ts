﻿/// <reference path="../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
module Api.Components.SystemCatalog {

    export class R2T4CalculationPeriodType {
        componentId: string;
        isRadiobutton: boolean;

        constructor(componentId: string, isRadiobutton: boolean, onLoadComplete: () => any, onChange: () => any) {
            /**
            * Declaring the CalculationPeriodType API object. The CalculationPeriodType API encapsulate the Calculation Period Type Controller.
            * We will use the CalculationPeriodType API object to get the data source for the Period Types action.
            */
            let periodTypes = new CalculationPeriodType();
            this.isRadiobutton = isRadiobutton;

            /**
            * In this step we are concatenating the # with the component id so we can find the HTML control on the using jquery and them with the
            * jquery reference to that DOM object the kendo UI control wil be created.
            */
            this.componentId = "#" + componentId;

            this.isRadiobutton = true;

            $(this.componentId).html("");

            var target = this.componentId;
            var count = 0;

            /* Call the Ajax Method or Data Source data bind to get the newest set of that */
            /*Initialize the kendo radiobuttons the data binding and event binding*/
            periodTypes.getCalculationPeriodTypes(
                (response) => {
                    document.getElementById(componentId).innerHTML = "";
                    $.each(response, (index, value) => {
                        if (count === 0) {
                            $('<span style="margin-left: -2px;"><input type="radio" checked name="periodName"  id=' +
                                value.value +
                                ' disabled><label' +
                                ' for=' +
                                value.value + '>' +
                                value.text +
                                '</label></input></span> ').appendTo(target);
                            count++;
                        } else {
                            $('<span style="margin-left: 10px;"><input type="radio"  name="periodName" id=' +
                                value.value +
                                ' disabled style="pointer-events: none;"><label' +
                                ' for=' +
                                value.value + ' style="background-position-y:0">' +
                                value.text +
                                '</label></input></span> ').appendTo(target);
                        }
                        let comId = document.getElementsByName('periodName');

                        /* on radio button change event unselecting the previous selected radio button. */
                        $(comId).change((e) => {
                            var periodRadioList = document.getElementsByName('periodName');
                            for (let i = 0; i < periodRadioList.length; i++) {
                                if (periodRadioList.item(i).id.toString() === (e.target as HTMLInputElement).id) {
                                    if ((e.target as HTMLInputElement).checked) {
                                        (e.target as HTMLDivElement).setAttribute("checked",
                                            "checked");
                                    }
                                }
                                else {
                                    periodRadioList.item(i).setAttribute("checked",
                                        "false");
                                }
                            }
                            onChange();
                        });
                    });
                    onLoadComplete();
                });
        }
    }
}