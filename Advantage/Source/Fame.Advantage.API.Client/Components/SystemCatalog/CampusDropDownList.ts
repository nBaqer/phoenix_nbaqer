﻿/// <reference path="../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" /> 

module Api.Components {
    import IFilterCampus = Api.SystemCatalog.Models.ICampus;

    export class CampusDropDownList {
        selectedCampusId: string;
        componentId: string;
        private dropDownList: kendo.ui.DropDownList;
        constructor(componentId: string, selectedCampusId: string, onchange: (response) => any) {

            let campus = new Campus();
            let that = this;
            /**
             * Append # to the component Id
             */
            componentId = "#" + componentId;
            that.componentId = componentId;

            $(componentId).kendoDropDownList({
                dataSource: campus.getAllCampuses(() => that.onRequestFilterData(that)),
                optionLabel: "Select",
                dataTextField: "text",
                dataValueField: "value"
            }).change((value) => {
                if (value)
                    that.selectedCampusId = $(componentId).data("kendoDropDownList").value();

                onchange(value);
                });

            that.dropDownList = $(componentId).data("kendoDropDownList");
        }

        /**
         * Event that will be before the request is made to append the parameters to the request.
         * This is where the DTO is build that is pass as an input parameter on the API.
         * @param that
         */
        private onRequestFilterData(that: CampusDropDownList): any {
            //let drpList = $(that.componentId).data("kendoDropDownList") as kendo.ui.DropDownList;
            //let dropStatusResult: IFilterDropStatus = {} as IFilterDropStatus;
            //dropStatusResult.campusId = that.campusId;

            //return dropStatusResult;
        }
        reset() {
            this.dropDownList.value("");
        }
        getKendoDropDown() {
            return this.dropDownList;
        }
    }

}
