﻿;
/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
module Api.Components {
    import IFilterDropStatus = API.SystemCatalog.Models.IDropStatus;

    export class DropStatusCodesDropDownList {
        campusId: string;
        componentId: string;
        selectedStatusId: string;

        constructor(componentId: string, campusId: string, onchange: (response) => any) {
            /**
             * Declaring the StudentStatus API object. The StudentStatus API encapsulate the Student Controller.
             * We will use the Student API object to get the kendo data source for the Student Search action.
             */
            let dropStatus = new DropStatusCode();
            let that = this;
            this.campusId = campusId;
            /**
             * In this step we are concatenating the # with the component id so we can find the HTML control on the using jquery and them with the
             * jquery reference to that DOM object the kendo UI control wil be created.
             */
            componentId = "#" + componentId;

            /**
             * storhing the component id in the global variable scope so it can be referenced on the event when the data source tries to execute the call
             * that is used on the onRequestFilterData event.
             */
            this.componentId = componentId;

            /**
             * declaration of the kendo ui control.
             */
            let dropStatusResult: IFilterDropStatus = {} as IFilterDropStatus;
            dropStatusResult.campusId = that.campusId;

            $("#ddlStatus").kendoDropDownList({
                dataSource: dropStatus.getDropStatusByCampusId(() => that.onRequestFilterData(that)),
                optionLabel: "Select",
                dataTextField: "text",
                dataValueField: "value"
            }).change((value) => {
                if (value)
                    that.selectedStatusId = $("#ddlStatus").data("kendoDropDownList").value();

                onchange(value);
            });

        }

        /**
         * Event that will be before the request is made to append the parameters to the request.
         * This is where the DTO is build that is pass as an input parameter on the API.
         * @param that
         */
        private onRequestFilterData(that: DropStatusCodesDropDownList): IFilterDropStatus {
            let drpList = $(that.componentId).data("kendoDropDownList") as kendo.ui.DropDownList;
            let dropStatusResult: IFilterDropStatus = {} as IFilterDropStatus;
            dropStatusResult.campusId = that.campusId;

            return dropStatusResult;
        }
    }
}