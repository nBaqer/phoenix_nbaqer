﻿/// <reference path="../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls .
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
module Api.Components.SystemCatalog {
    import IStudentDropReason = API.SystemCatalog.Models.IStudentDropReason;

    export class StudentDropReason {
        campusId: string;
        componentId: string;
        selectedDropReason: string;
        constructor(componentId: string, campusId: string, onSelectionChange: (value)=> {}) {
            let dropreasons = new DropReason();
            let that = this;
            this.campusId = campusId;
            componentId = "#" + componentId;
            this.componentId = componentId;
            let studentDropReason: IStudentDropReason = {} as IStudentDropReason;
            studentDropReason.campusId = that.campusId;
            $("#ddlDropReason").kendoDropDownList({
                dataSource: dropreasons.getDropReasonByCampusId(() => that.onRequestFilterData(that)),
                optionLabel: "Select",
                dataTextField: "text",
                dataValueField:"value"
            }).change((value) => {
                if (value)
                    that.selectedDropReason = $("#ddlDropReason").data("kendoDropDownList").value();

                onSelectionChange(value);
            }); 
        }

        private onRequestFilterData(that: StudentDropReason): IStudentDropReason {
            let studentDropReason: IStudentDropReason = {} as IStudentDropReason;
            studentDropReason.campusId = that.campusId;
            return studentDropReason;
        }
    }
}