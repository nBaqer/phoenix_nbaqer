﻿/// <reference path="../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" /> 
/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
module Api.Components {
    export class StatusDropDownList {
        componentId: string;
        selectedStatusId: string;
        private dropDownList: kendo.ui.DropDownList;

        constructor(componentId: string, campusId: string, onchange: (response) => any) {
            /**
             * Declaring the Status API object. The Status API encapsulate the Statueses Controller.
             * We will use the Status API object to get the kendo data source for the Status dropdown.
             */
            let status = new Status();
            let that = this;
            /**
             * In this step we are concatenating the # with the component id so we can find the HTML control on the using jquery and them with the
             * jquery reference to that DOM object the kendo UI control wil be created.
             */
            componentId = "#" + componentId;

            /**
             * storing the component id in the global variable scope so it can be referenced elsewhere
             */
            that.componentId = componentId;

            $(that.componentId).kendoDropDownList({
                dataSource: status.getStatuses(() => that.onRequestFilterData(that)),
                dataTextField: "text",
                dataValueField: "value",
                index: 0
            }).change((value) => {

                if (value)
                    that.selectedStatusId = $(that.componentId).data("kendoDropDownList").value();

                onchange(value);
            });

            that.dropDownList = $(componentId).data("kendoDropDownList");

        }

        private onRequestFilterData(that: StatusDropDownList): any {
        
        }
        reset() {
            this.dropDownList.value("");
        }
        getValue() {
            return this.dropDownList.value();
        }
        getKendoDropDown() {
            return this.dropDownList;
        }

    }
}