﻿/// <reference path="../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" /> 
/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
module Api.Components {
    export class UserTypeDropDownList {
        componentId: string;
        selectedUserTypeId: string;
        private dropDownList: kendo.ui.DropDownList;

        constructor(componentId: string, label: string, campusId: string, onchange: (response) => any) {
            /**
             * Declaring the UserType API object. The UserType API encapsulate the User Type Controller.
             * We will use the UserType API object to get the kendo data source for the UserType dropdown.
             */
            let userType = new UserType();
            let that = this;
            /**
             * In this step we are concatenating the # with the component id so we can find the HTML control on the using jquery and them with the
             * jquery reference to that DOM object the kendo UI control wil be created.
             */
            componentId = "#" + componentId;

            /**
             * storing the component id in the global variable scope so it can be referenced elsewhere
             */
            that.componentId = componentId;
            let optionLabel = label ? label : "Select";
            $(that.componentId).kendoDropDownList({
                dataSource: userType.getUserTypes(() => that.onRequestFilterData(that)),
                optionLabel: optionLabel,
                dataTextField: "text",
                dataValueField: "value"
            }).change((value) => {
                if (value)
                    that.selectedUserTypeId = $(that.componentId).data("kendoDropDownList").value();

                onchange(value);
            });

            that.dropDownList = $(componentId).data("kendoDropDownList");

        }
        private onRequestFilterData(that: UserTypeDropDownList): any {
            //let drpList = $(that.componentId).data("kendoDropDownList") as kendo.ui.DropDownList;
            //let dropStatusResult: IFilterDropStatus = {} as IFilterDropStatus;
            //dropStatusResult.campusId = that.campusId;

            //return dropStatusResult;
        }
        reset() {
            this.dropDownList.value("");
        }
        getValue() {
            return this.dropDownList.value();
        }
        getKendoDropDown() {
            return this.dropDownList;
        }
    }
}