﻿/// <reference path="../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" /> 
/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
module Api.Components {
    import IFilterParams = API.Models.IFilterParameters;
    import ISettings = API.Models.ISettings;
    import IDataProvider = API.Models.IDataProvider;
    export class SearchAutoComplete {
        private componentId: string;
        private selectedId: string;
        private filterParams: IFilterParams;
        private dataProvider: IDataProvider;
        private autoComplete: kendo.ui.AutoComplete;

        constructor(componentId: string, dataProvider: IDataProvider, filterParams?: IFilterParams, settings?: ISettings, onSelected?: (response) => any, onChange?: (response) => any) {
            /**
             * Declaring the UserType API object. The UserType API encapsulate the User Type Controller.
             * We will use the UserType API object to get the kendo data source for the UserType dropdown.
             */
            let that = this;
            /**
             * In this step we are concatenating the # with the component id so we can find the HTML control on the using jquery and them with the
             * jquery reference to that DOM object the kendo UI control wil be created.
             */
            componentId = "#" + componentId;

            /**
             * storing the component id in the global variable scope so it can be referenced elsewhere
             */
            that.componentId = componentId;
            that.dataProvider = dataProvider;
            that.filterParams = filterParams;

            let defaults: Object = {
                dataSource: dataProvider.getDataSource(() => that.onRequestFilterData()),
                dataTextField: "value.displayName",
                placeholder: "Search Name or SSN",
                filter: "contains",
                headerTemplate: "<span style='font-weight:bold;width:65%;display:inline-block;'>Full Name </span><span style='font-weight:bold;width:30%;display:inline-block;'>SSN</span>",
                template: "<span style='width:65%;display:inline-block;'>#=value.displayName#</span> <span style='width:30%;display:inline-block;'>#=value.ssn#</span>",
                footerTemplate: '<div><h5>#: instance.dataSource.total() # items found</h5></div>',
                minLength: 3,
                enforceMinLength: true,
                select: (e) => {
                    $(this.componentId).empty();
                    let selectedStudent =
                        ($(componentId).data("kendoAutoComplete") as kendo.ui.AutoComplete).dataItem(e.item.index());
                    onSelected(selectedStudent);
                }
            };

            if (settings) {
                $.extend(defaults, settings.settings);
            }
            /**
             * declaration of the kendo ui control.
             */
            $(componentId).kendoAutoComplete(defaults).change((value) => {
                if (onChange) {
                    onChange(value);
                }
            });

            that.autoComplete = $(componentId).data("kendoAutoComplete");
            that.autoComplete.bind('filtering', that.autocomplete_filtering);


        }

        private autocomplete_filtering(e) {
            //get filter descriptor

            let filter = e.filter;

            if (filter.value < 3) {
                //prevent filtering if the filter does not value
                e.preventDefault();
            } else {
            }
        }
        private onRequestFilterData(): any {
            let filter: Object = {};
            if (this.filterParams) {
                filter = $.extend(filter, this.filterParams.params);
            }
            filter["filter"] = this.autoComplete.value();
            return filter;
        }
        private setParameters(filterParams: IFilterParams) {
            this.filterParams = filterParams;
        }
        getSelected() {
            return this.selectedId;
        }
        reset() {
            this.autoComplete.value("");
        }
        reload(filterParams?: IFilterParams) {
            if (filterParams) {
                this.setParameters(filterParams);

            }
            this.autoComplete.setDataSource(this.dataProvider.getDataSource(() => this.onRequestFilterData()));
        }
        hasValue() {
            return this.autoComplete.value() !== "";
        }
        getValue() {
            return this.autoComplete.value();
        }
        setValue(newValue: string) {
            this.autoComplete.value(newValue);
        }

    }
}