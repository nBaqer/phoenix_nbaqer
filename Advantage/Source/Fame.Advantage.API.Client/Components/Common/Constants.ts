﻿module Components.Common {
    export const clockHourText: string = "Provide values for Withdrawal date, Hours scheduled to complete and Total hours in the period. They are required for R2T4 calculation." ;
    export const creditHourText: string = "If the school is not required to take attendance for the Enrollment specified under 'Termination Details', select the 'Not required to take attendance' check box and specify values for Start date and End date. <br/>" + " When the school is required to take attendance, Start date, Scheduled end date, Withdrawal date, Completed days and Total days are required.";
    export const titleIvAidMessage: string = "Provide value for aid disbursed or aid that could have been disbursed for any grant or loan program. It is required for R2T4 calculation. ";
    export const institutionalChargeMessage: string = "A value equal to or greater than zero for any one of the charges listed is required.";
    export enum MethodType {
        Save = 1,
        Others = 2
    }
    export const unsubsidizedFfelOrDirectStaffordLoan: string = "UnsubsidizedFFELDirectStaffordLoan";
    export const subsidizedFfelDirectStaffordLoan: string = "SubsidizedFFELDirectStaffordLoan";
    export const perkinsLoan: string = "PerkinsLoan";
    export const fFelDirectPlusStudent: string = "FFELDirectPLUSStudent";
    export const fFelDirectPlusParent: string = "FFELDirectPLUSParent";
    export const pell: string = "Pell";
    export const fSeog: string = "FSEOG";
    export const tEachGrant: string = "TEACHGrant";
    export const iraqAfghanistanServiceGrant: string = "IraqAfghanistanServiceGrant";

    export enum ProgramType {
        ClockHour = 1,
        CreditHour = 0
    }

    export enum ProgramTypeInR2T4 {
        ClockHour = 1,
        CreditHour =0
    }

    export enum ProgramTypeOverride {
        ClockHour = 0,
        CreditHour = 1
    }
    export enum CalculationPeriodType {
        PaymentPeriod = 0,
        PeriodofEnrollment = 1
    }
    export enum SystemStatus {
        CurrentlyAttending = 9,
        AcademicProbation = 20,
        Externship = 22
    }


    export const clockHourTextstep2: string = "Divide the clock hours scheduled to have been completed as of the withdrawal date in the period by the total clock hours in the period.";
    export const creditHourTextstep2: string = "A school that is not required to take attendance may, for a student who withdraws without notification, enter 50% in Box H and proceed to Step 3. Or, the school may enter the last date of attendance at an academically related activity for the “withdrawal date, ” and proceed with the calculation as instructed. For a student who officially withdraws, enter the withdrawal date.";
    export const cancelConfirmationMessage: string = "Are you sure you want to cancel the student termination process? All current data entered, including the previously saved data, will be lost";
    export const r2T4InputChangeNext: string = "Details on 'R2T4 Results' would be updated. Are you sure you want to make changes?";
    export const r2T4InputChangeNextAfterApproveTerm: string = "Details on R2T4 Results and Approve termination would be updated.Are you sure you want to make the changes?";
    export const r2T4InputChangeBack: string = "Details on 'R2T4 Results' would be lost. Are you sure you want to make changes?";
    export const r2T4InputChangeBackOrTab: string = "Details on 'R2T4 Results’ and 'Approve Termination' would be lost. Are you sure you want to make changes?";
    export const r2T4InputModifyApproveTerminationTab: string = "Details on R2T4 Results and Approve termination would be updated. Are you sure you want to make changes?";
    export const r2T4InputModifyOverride: string = "R2T4 results were overridden by Support. Are you sure you want to edit R2T4 input? R2T4 results and details on 'Approve Termination' would be lost.";
    export const r2T4InputModifyOnlyResults: string = "R2T4 results were overridden by Support. Are you sure you want to edit R2T4 input and recalculate R2T4 results?";
    export const r2T4InputModifyOverrideResults: string = "R2T4 results were overridden by Support. Are you sure you want to edit R2T4 input, R2T4 results would be lost?";
    export const r2T4InputModifyApproveTermination: string = "R2T4 results were overridden by Support. Are you sure you want to edit R2T4 input? R2T4 results would be recalculated and details on 'Approve Termination' would be updated.";

    export let isR2T4ResultTabDisabled: boolean = false;
    export let isR2T4ApproveTabDisabled: boolean = false;
    export let isR2T4InputTabDisabled: boolean = false;
    export let periodTypeId: string = "";

    export let r2T4CompletionStatus = {} as API.Models.IR2T4CompletionStatus;
    export let isR2T4ResultsCompleted: boolean = false;
    export let isNonR2T4ApproveTabEnabled: boolean = false;
    export const nullGuid: string = "00000000-0000-0000-0000-000000000000";
    export let isR2T4InputExists: boolean = false; 
    export let isR2T4ResultsExists: boolean = false;
    export let isR2T4OverrideResultsExists: boolean = false; 
    export let studentName: string;
    export let enrollmentName: string;
    export let undoStudentName: string;
    export let undoEnrollmentName: string;
    export let attendanceUnitType: string;

    export const studentTermantionSummaryReport = "StudentTermination/StudentTerminationSummary";
    export const studentTermantionR2T4CreditHrsReport = "StudentTermination/R2T4ResultsCreditHour";
    export const studentTermantionR2T4ClockHrsReport = "StudentTermination/R2T4ResultsClockHour";

    export const r2T4OverrideNext: string =
        "Details on 'Approve Termination' would be updated. Are you sure you want to make changes.";
    export const r2T4OverrideSave: string =
        "Details on 'Approve Termination' would be lost. Are you sure you want to make changes.";
    export let activeTabName: string;
    export const cancelUndoTerminationMessage: string = "Are you sure you want to cancel the Undo student termination?";
    export let statusCodeId: string = "";

    export enum AcademicCalendarProgram {
        NonStandardTerm = 1,
        Quarter = 2,
        Semester = 3,
        Trimester = 4,
        ClockHour = 5,
        NonTerm = 6
    }

    export enum TermType {
        NonTermNotSelfPaced = 1,
        NonStandard = 2,
        NonTermForFailedCoursesNoOfDays = 3,
        NonTermForFailedCoursesEndDate = 4
    }
}
