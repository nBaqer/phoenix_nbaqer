﻿/// <reference path="../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" /> 
/// <reference path="../../../AdvWeb/Kendo/typescript/jquery.d.ts" />

/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
module Api.Components {
    import IFilterParams = API.Models.IFilterParameters;
    import ISettings = API.Models.ISettings;
    import IDataProvider = API.Models.IDataProvider;
    export class MultiSelectDropDownList {
        private componentId: string;
        private element: JQuery;
        private selectedId: string;
        private filterParams: IFilterParams;
        private dataProvider: IDataProvider;
        private dropDownList: kendo.ui.MultiSelect;
        private hasCustomFunctionName: boolean;
        private dataProviderFunctionName: string;

        constructor(element: string | JQuery, dataProvider: IDataProvider, filterParams?: IFilterParams, settings?: ISettings, onchange?: (response) => any, dataProviderFunctionName?: string, selectValue?: any) {
            /**
             * Declaring the UserType API object. The UserType API encapsulate the User Type Controller.
             * We will use the UserType API object to get the kendo data source for the UserType dropdown.
             */
            let that = this;
            /**
             * In this step we are concatenating the # with the component id so we can find the HTML control on the using jquery and them with the
             * jquery reference to that DOM object the kendo UI control wil be created.
             */
            if (typeof element === 'string') {
                that.componentId = "#" + element;
                that.element = $(that.componentId);
            } else {
                that.element = element;
            }
            /**
             * storing the component id in the global variable scope so it can be referenced elsewhere
             */
            that.dataProvider = dataProvider;
            that.filterParams = filterParams;

            that.hasCustomFunctionName = dataProviderFunctionName !== undefined;

            if (that.hasCustomFunctionName) {
                that.dataProviderFunctionName = dataProviderFunctionName;
            }

            let defaults = {
                dataSource: dataProvider.getDataSource(() => that.onRequestFilterData(), null, that.hasCustomFunctionName ? dataProviderFunctionName : null),
                optionLabel: "Select",
                dataTextField: "text",
                dataValueField: "value",
            };



            if (settings) {
                $.extend(defaults, settings.settings);
            }
            /**
             * declaration of the kendo ui control.
             */
            that.element.kendoMultiSelect(defaults).change((value) => {
                if (value)
                    that.selectedId = that.element.data("kendoMultiSelect").value();

                if (onchange)
                    onchange(value);
            });

            that.dropDownList = that.element.data("kendoMultiSelect");
            that.dropDownList.value(selectValue);

        }
        private onRequestFilterData(): any {
            let filter: Object = {};
            if (this.filterParams) {
                filter = $.extend(filter, this.filterParams.params);

            }
            if (this.dropDownList !== undefined) {
                if (this.dropDownList.dataSource !== undefined && this.dropDownList.dataSource.options.serverPaging) {
                    filter = $.extend(filter, {
                        pageSize: this.dropDownList.dataSource.pageSize(),
                    });
                }
            }
            return filter;
        }
        private setParameters(filterParams: IFilterParams) {
            this.filterParams = filterParams;
        }
        getSelected() {
            return this.selectedId;
        }
        fetch(func?: Function) {
            return this.dropDownList.dataSource.fetch(func);
        }
        reset() {
            this.dropDownList.value("");
            this.dropDownList.trigger("change");
        }
        reload(filterParams?: IFilterParams) {
            if (filterParams) {
                this.setParameters(filterParams);

            }
            this.dropDownList.setDataSource(this.dataProvider.getDataSource(() => this.onRequestFilterData(), null, this.hasCustomFunctionName ? this.dataProviderFunctionName : null));
        }
        hasValue() {
            return this.dropDownList.value() !== "";
        }
        getValue() {
            return this.dropDownList.value();
        }

        //getText() {
        //    return this.dropDownList.text();
        //}

        setReadOnly(val) {
            this.dropDownList.readonly(val);
        }

        getData() {
            return this.dropDownList.dataSource.data();
        }

        setDataSource(dataSource: kendo.data.DataSource) {
            this.dropDownList.setDataSource(dataSource);
        }

        setValue(newValue: string) {
            this.dropDownList.value(newValue);
            this.dropDownList.trigger("change");
        }
        getKendoDropDown() {
            return this.dropDownList;
        }

        show() {
            $(this.dropDownList.element).closest(".k-widget").show();
        }

        hide() {
            $(this.dropDownList.element).closest(".k-widget").hide();
        }
        setPageSizeToAll() {
            var length = this.getData().length;
            this.dropDownList.dataSource.pageSize(length);
        }
    }
}