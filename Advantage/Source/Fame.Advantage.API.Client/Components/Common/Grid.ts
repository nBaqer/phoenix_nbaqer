﻿/// <reference path="../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
module Api.Components {
    import IFilterSearch = API.Models.IFilterSearch;
    import IFilterParams = API.Models.IFilterParameters;
    import ISettings = API.Models.ISettings;
    import IDataProvider = API.Models.IDataProvider;

    export class Grid {
        private componentId: string;
        private filterSearch?: IFilterSearch;
        private filterParameters?: IFilterParams;
        private dataGrid: kendo.ui.Grid;
        private dataProvider: IDataProvider;
        private group;
        private isGrouping = false;
        private hasCustomFunctionName: boolean;
        private dataProviderFunctionName: string;
        private gridWidth: number;
        constructor(componentId: string, dataProvider: IDataProvider, searchFilter?: IFilterSearch, paramsFilter?: IFilterParams, gridSettings?: ISettings, onChange?: (response) => any, dataProviderFunctionName?: string) {

            let that = this;
            that.filterSearch = searchFilter;
            that.filterParameters = paramsFilter;
            that.dataProvider = dataProvider;
            /**
             * In this step we are concatenating the # with the component id so we can find the HTML control on the using jquery and them with the
             * jquery reference to that DOM object the kendo UI control wil be created.
             */
            componentId = "#" + componentId;

            /**
             * storhing the component id in the global variable scope so it can be referenced on the event when the data source tries to execute the call
             * that is used on the onRequestFilterData event.
             */
            that.componentId = componentId;

            that.hasCustomFunctionName = dataProviderFunctionName !== undefined;

            if (that.hasCustomFunctionName) {
                that.dataProviderFunctionName = dataProviderFunctionName;
            }

            let ds: kendo.data.DataSource = that.dataProvider.getDataSource(() => that.onRequestFilterData(),
                null,
                that.hasCustomFunctionName ? dataProviderFunctionName : null);


            let defaults: Object = {
                dataSource: ds,
                height: 550,
                groupable: false,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                }
            };

            if (gridSettings) {
                if (gridSettings.settings.hasOwnProperty("group")) {
                    that.isGrouping = true;
                    that.group = gridSettings.settings["group"];
                    delete gridSettings.settings["group"];
                    ds = that.dataProvider.getDataSource(() => that.onRequestFilterData(), this.group);
                    defaults["dataSource"] = ds;
                }
                if (gridSettings.settings.hasOwnProperty("dataSource")) {

                    //ds = {
                    //    ...defaults["dataSource"], ...gridSettings.settings["dataSource"]
                    //};
                    ds = $.extend(defaults["dataSource"]["options"], gridSettings.settings["dataSource"]);
                    defaults["dataSource"]["options"] = ds;
                    delete gridSettings.settings["dataSource"];
                    //(<any>kendo.data.DataSource).fn.init.call(defaults["dataSource"], defaults["dataSource"]["options"]);

                }

                $.extend(defaults, gridSettings.settings);
            }
            /**
             * declaration of the kendo ui control.
             */
            $(componentId).kendoGrid(defaults).change((val) => { if (onChange) { onChange(val) } });

            that.dataGrid = $(that.componentId).data("kendoGrid");
        }

        /**
         * Event that will be before the request is made to append the parameters to the request.
         * This is where the DTO is build that is pass as an input parameter on the API.
         * @param that
         */
        private onRequestFilterData(): any {
            let filter: Object = {};
            if (this.filterSearch) {
                filter = $.extend(filter, this.filterSearch);
            }
            if (this.filterParameters) {
                filter = $.extend(filter, this.filterParameters.params);
            }
            if (this.dataGrid !== undefined) {
                if (this.dataGrid.dataSource !== undefined && this.dataGrid.dataSource.options.serverPaging) {
                    filter = $.extend(filter, {
                        pageSize: this.dataGrid.dataSource.pageSize(),
                        page: this.dataGrid.dataSource.page()
                    });
                }
            }
            return filter;
        }
        private setSearchFilter(searchFilter: IFilterSearch) {
            this.filterSearch = searchFilter;
        }
        setParameters(filterParams: IFilterParams) {
            this.filterParameters = filterParams;
        }
        private setDataSource(dataSource: kendo.data.DataSource) {
            this.dataGrid.setDataSource(dataSource);
        }
        refresh() {
            this.dataGrid.dataSource.fetch();
        }
        reload(filterSearch?: IFilterSearch, filterParams?: IFilterParams) {
            if (filterSearch) {
                this.setSearchFilter(filterSearch);
            }
            if (filterParams) {
                this.setParameters(filterParams);

            }
            if (this.isGrouping) {
                if (this.hasCustomFunctionName) {
                    this.setDataSource(this.dataProvider.getDataSource(() => this.onRequestFilterData(), this.group, this.dataProviderFunctionName));
                } else {

                    this.setDataSource(this.dataProvider.getDataSource(() => this.onRequestFilterData(), this.group));
                }

            } else {
                if (this.hasCustomFunctionName) {
                    this.setDataSource(this.dataProvider.getDataSource(() => this.onRequestFilterData(), undefined, this.dataProviderFunctionName));
                } else {

                    this.setDataSource(this.dataProvider.getDataSource(() => this.onRequestFilterData()));
                }
            }
        }
        clear() {
            this.dataGrid.setDataSource(new kendo.data.DataSource({ data: [] }));
        }
        count() {
            return this.dataGrid.dataSource.total();
        }
        getRowDataItem(row) {
            return this.dataGrid.dataItem(row);
        }
        getSelectedKeyNames() {
            return this.dataGrid.selectedKeyNames();
        }
        getComponentId() {
            return this.componentId;
        }
        fetch(func?: Function) {
            return this.dataGrid.dataSource.fetch(func);
        }
        hideColumn(col: number | string) {
            this.dataGrid.hideColumn(col);
        }
        showColumn(col: number | string) {
            this.dataGrid.showColumn(col);
        }
        getColumns() {
            return this.dataGrid.columns;
        }
        getColumnByName(name: string) {
            return this.dataGrid.columns.filter((col) => { return col.field === name })[0];
          
        }
        getWidth() {
            return this.dataGrid.wrapper.width();
        }
        body() {
            return this.dataGrid.tbody;
        }
        view() {
            return this.dataGrid.dataSource.view();
        }
        bind(event: string, func: Function) {
            return this.dataGrid.bind(event, func);
        }
        change() {
            this.dataGrid.trigger("change");
        }
        getData() {
            return this.dataGrid.dataSource.data();
        }

        getKendoGridObject() {
            return this.dataGrid;
        }

        setPageSizeToAll() {
            var length = this.getData().length;
            this.dataGrid.dataSource.pageSize(length);
        }
        setWidth(width: number) {
            this.gridWidth = width;
        }
        resizeColumns() {
            this.setGridWidth();
        }
        private setGridWidth() {
            var cols = this.dataGrid.columns;
            var currentColWidth = cols.reduce(function (prev, cur) {
                if (!cur.hidden) {
                    return prev += cur.width as number;
                } else {
                    return prev;
                }
            }, 0);
            console.log(currentColWidth);
            if (currentColWidth > this.gridWidth) {
                this.dataGrid.wrapper.width(this.gridWidth);
            } else {
                this.dataGrid.wrapper.width(currentColWidth + kendo.support.scrollbar());
            }
        }
        private print(title): any {
            var gridElement = $(this.componentId),
                printableContent = '',
                win = window.open('', '_blank', 'width=800, height=500, resizable=1, scrollbars=1'),
                doc = win.document.open();

            var htmlStart =
                '<!DOCTYPE html>' +
                '<html>' +
                '<head>' +
                '<meta charset="utf-8" />' +
                '</head>' +
                '<body>';

            var htmlEnd =
                '</body>' +
                '</html>';

            var gridHeader = gridElement.children('.k-grid-header');
            if (gridHeader[0]) {
                var thead = gridHeader.find('thead').clone().addClass('k-grid-header');

                printableContent = gridElement
                    .clone()
                    .children('.k-grid-header').remove()
                    .end()
                    .children('.k-grid-content')
                    .find('table')
                    .first()
                    .children('tbody').before(thead)
                    .end()
                    .end()
                    .end()
                    .end()[0].outerHTML;
            } else {
                printableContent = gridElement.clone()[0].outerHTML;
            }

            doc.write("<html><head><title>" + title + "</title>" +
                //'<link href="http://kendo.cdn.telerik.com/' + kendo.version + '/styles/kendo.common.min.css" rel="stylesheet" /> ' +

                //'<link href="/Advantage/Current/Site/Kendo/styles/kendo.common.min.css" rel="stylesheet">' +
                '<style>' +
                '@media all {.page-break { display: none; }}' +
                '@media screen{ html{display:none;}}' +
                '@media print {.page-break	{ display: block; page-break-before: always; } table td, table th { border: 1px solid #ddd; padding: 8px;}' +
                'tr: nth-child(even){ background-color: #e6f2f8; }' +
                'th {padding-top: 12px; padding-bottom: 12px; text-align: left; border-color:#a3d0e4; background-color: #d9ecf5; color: white;}}' +
                'html { font: 11pt sans-serif; }' +
                '.k-grid { border-top-width: 0; }' +
                '.k-grid, .k-grid-content { height: auto !important; }' +
                '.k-grid-content { overflow: visible !important; }' +
                'div.k-grid table { table-layout: auto; width: 100% !important; }' +
                '.k-grid .k-grid-header th { border-top: 1px solid; }' +
                '.k-grouping-header, .k-grid-toolbar, .k-pager-numbers, .k-pager-sizes, .k-grid-pager > .k-link { display: none; }' +
                '.k-grid-pager { display: none; }' + // optional: hide the whole pager
                '</style>');
            doc.write("</head><body>");
            doc.write(printableContent);
            doc.write(htmlEnd);

            doc.close(); // necessary for IE >= 10
            win.focus(); // necessary for IE >= 10*/

            win.print();
            win.close();
        }
        printGrid(title, all?: boolean) {
            if (all) {
                var pageSize = this.dataGrid.dataSource.pageSize();
                this.dataGrid.dataSource.pageSize(this.dataGrid.dataSource.total());
                this.dataGrid.bind("dataBound", (e) => {
                    this.print(title);
                    this.dataGrid.unbind("dataBound");
                    this.dataGrid.dataSource.pageSize(pageSize);
                });
            } else {
                this.print(title);
            }

        }
    }
}