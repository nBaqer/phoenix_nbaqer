﻿/// <reference path="../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../../Fame.Advantage.Client.AD/Common/Enumerations.ts" />
/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */

module Api.Components {
    import SystemStatus = AD.SystemStatus;
    import ISystemStatus = API.Models.ISystemStatusModel;

    export class StudentEnrollment {
        componentId: string;
        studentId: string;
        enrollments: Enrollments;
        selectedEnrollmentId: string;
        isRadiobutton: boolean;
        onEnrollmentSelection: any;

        constructor(componentId: string, studentId: string, systemStatus, onChangeEvent: () => {}, isValidData: () => {}) {
            this.studentId = studentId;
            componentId = "#" + componentId;
            this.componentId = componentId;
            this.isRadiobutton = true;
            this.onEnrollmentSelection = undefined;
            this.dataBindRadioButtonList(studentId, onChangeEvent, isValidData, systemStatus);
        }

        /**
         * Event to be called when an enrollment is selected and when valid date of determination is specified.
         */
        public static enableOrDisableR2T4Checkbox(enrollments: any, dateOfDetermination: any): void {
            let studentEnrollmentStartDate = this.formatDate(enrollments.startDate);
            let isAwardExists = true;
            if ((dateOfDetermination != undefined && dateOfDetermination !== "") && (new Date(dateOfDetermination) >= new Date(studentEnrollmentStartDate)) && enrollments.systemStatusId !== Number(SystemStatus.FutureStart)) {
                $('#chkClaculationPeriod').removeAttr('disabled');

                let programVersions = new ProgramVersions();
                programVersions.getCampusProgramVersionDetail(enrollments.enrollmentId,
                    (response: any) => {
                        if (!!response && response.resultStatus === true) {
                            if (response.isTitleIv !== undefined) {
                                let inputParameters = { enrollmentId: enrollments.enrollmentId, lastAttendedDate: enrollments.lastDateAttended };
                                programVersions.getStudentAwardsByEnrollmentId(inputParameters,
                                    (studentAwards: any) => {
                                        if (!!studentAwards && studentAwards.length > 0) {
                                            if (studentAwards[0].resultStatus === AD.X_MESSAGE_NO_AWARDS) {
                                                isAwardExists = false;
                                            }
                                        }

                                        if (response.isTitleIv === true && isAwardExists) {
                                            $("#chkClaculationPeriod").prop('checked', true);
                                            (document.getElementById("dvCalculationPeriod") as HTMLDivElement).setAttribute("style", "display:block");
                                            (document.getElementById("periodName") as HTMLDivElement).setAttribute("style", "display:block");
                                        } else {
                                            $("#chkClaculationPeriod").prop('checked', false);
                                            (document.getElementById("dvCalculationPeriod") as HTMLDivElement).setAttribute("style", "display:none");
                                        }
                                    });
                            }
                        }
                    });

                //$("#chkClaculationPeriod").prop('disabled', true);
            } else {
                $('#chkClaculationPeriod').prop('checked', false);
                $('#chkClaculationPeriod').attr('disabled', 'disabled');
                (document.getElementById("dvCalculationPeriod") as HTMLDivElement).setAttribute("style", "display:none");
            }
        }

        /**
         * Method to convert date into MM/dd/yyyy format
         */
        private static formatDate(inputDate: any): string {
            let date = new Date(inputDate);
            // Months use 0 index.
            let month = (1 + date.getMonth()).toString();
            month = month.length > 1 ? month : '0' + month;
            let day = date.getDate().toString();
            day = day.length > 1 ? day : '0' + day;
            return month + '/' + day + '/' + date.getFullYear();
        }

        /**
         * Method to bind the radio buttons
         */
        private dataBindRadioButtonList(studentId: string, onChangeEvent: any, isValidData: any, systemStatus: any): void {
            /* Call the Ajax Method or Data Source data bind to get the newest set of that */
            /*Initialize the kendo radiobuttons the data binding and event binding*/
            let enrollments = new Enrollments();
            var target = this.componentId;
            let enrollmentDetails: ISystemStatus = { StudentId: studentId, IsActiveEnrollments: systemStatus.IsActiveEnrollments, IsDroppedEnrollments: systemStatus.IsDroppedEnrollments };
            enrollments.getEnrollmentsByStudentId(enrollmentDetails,
                (response: any) => {
                    if (response.length > 0) {
                        $(this.componentId).empty();
                        $.each(response,
                            (index, value) => {
                                if (value !== null) {
                                    let effectiveDate = StudentEnrollment.formatDate(value.effectiveDate);
                                    let radioBtn;
                                    if (value.statusCode !== undefined && value.status !== undefined) {
                                        if (enrollmentDetails.IsActiveEnrollments) {
                                            /*&& this.isTerminationApproved(value)*/
                                            if (value.status.toLowerCase() === "active" ) {
                                                radioBtn = $('<input type="radio" id="enrollment' + index + '" name="studentEnrollment" class="studEnrollment" value=' + value.enrollmentId + '><label for="enrollment' + index + '">' + value.programVersionDescription + ' - ' + value.statusCodeDescription + ' ' + effectiveDate + '</label> <br/>');
                                            } else {
                                                radioBtn = $('<input type="radio" id="enrollment' + index + '" disabled="disabled" name="studentEnrollment" class="studEnrollment" value=' + value.enrollmentId + ' style="background-position-y:0"><label for="enrollment" ' + index + ' style="opacity: 0.5;">' + value.programVersionDescription + ' - ' + value.statusCodeDescription + ' ' + effectiveDate + '</label> <br/>');
                                            }
                                        }

                                        else if (enrollmentDetails.IsDroppedEnrollments) {
                                            if (value.status.toLowerCase() === "active") {
                                                radioBtn = $('<input type="radio" id="enrollment' + index + '" name="studentEnrollment" class="studEnrollment" value=' + value.enrollmentId + '><label for="enrollment' + index + '">' + value.programVersionDescription + ' - ' + value.statusCodeDescription + ' ' + effectiveDate + '</label> <br/>');
                                            }
                                        }
                                        radioBtn.appendTo(target);
                                    }
                                    $("#enrollment" + index).change(() => {
                                        onChangeEvent(value);
                                    });
                                    $("#enrollment" + index).click(() => {
                                        return isValidData();
                                    });
                                }
                            });
                    }
                    else {
                        $("#dvUndoEnrollment").css("display", "none");
                        if (response.length !== undefined) {
                            MasterPage.SHOW_INFO_WINDOW(AD.X_MESAGE_NO_RECORD);
                        }

                    }
                });
        }

        // This method is to validate dropstatus code for termination approved students.
        public isTerminationApproved(enrollmentStatus: any): boolean {
            let isStudentTerminationApproved: boolean = false;
            let statusItems: any = $("#ddlStatus").data("kendoDropDownList").items();
            for (var i = 0; i < statusItems.length; i++) {
                if (enrollmentStatus.statusCodeDescription.toLowerCase() !== statusItems[i].innerText.toLowerCase())
                    isStudentTerminationApproved = true;
            }
            return isStudentTerminationApproved;
        }
    }
}