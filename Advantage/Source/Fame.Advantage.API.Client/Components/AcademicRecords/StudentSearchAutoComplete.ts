﻿/// <reference path="../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
module Api.Components {
    import IFilterStudentSearch = API.Models.IFilterStudentSearch;
    export class StudentSearchAutoComplete {
        campusId: string;
        componentId: string;
        constructor(componentId: string, campusId: string, onSelected: (response) => any) {
            /**
             * Declaring the Student API object. The Student API encapsulate the Student Controller.
             * We will use the Student API object to get the kendo data source for the Student Search action.
             */
            let student = new Student();
            let that = this;
            this.campusId = campusId;

            /**
             * In this step we are concatenating the # with the component id so we can find the HTML control on the using jquery and them with the
             * jquery reference to that DOM object the kendo UI control wil be created.
             */
            componentId = "#" + componentId;

            /**
             * storhing the component id in the global variable scope so it can be referenced on the event when the data source tries to execute the call
             * that is used on the onRequestFilterData event.
             */
            this.componentId = componentId;

            /**
             * declaration of the kendo ui control.
             */
            $(componentId).kendoAutoComplete({
                dataTextField: "value.displayName",
                placeholder: "Search Name or SSN",
                filter: "contains",
                headerTemplate: "<span style='font-weight:bold;width:65%;display:inline-block;'>Full Name </span><span style='font-weight:bold;width:30%;display:inline-block;'>SSN</span>",
                template: "<span style='width:65%;display:inline-block;'>#=value.displayName#</span> <span style='width:30%;display:inline-block;'>#=value.ssn#</span>",
                footerTemplate: '<div><h5>#: instance.dataSource.total() # items found</h5></div>',
                minLength: 3,
                enforceMinLength: true,
                /**
                 * Setting up the kendo data source. The student.getStudentSearchKendoDataSource will return a kendo data source with the authorization
                 * parameters alredy configured.
                 * The method expects one event that will be called when the data source needs the filter information. In that event the filter
                 * is created and given back to the datasource.
                 */
                dataSource: student.getStudentSearchKendoDataSource(() => that.onRequestFilterData(that)),
                filtering: function (e) {
                    let filter = e.filter;
                    if (filter.value < 3) {
                        //prevent filtering if the filter does not value
                        e.preventDefault();
                    }
                },
                select: (e) => {
                    $('#studentEnrollment').empty();
                    let selectedStudent =
                        ($(componentId).data("kendoAutoComplete") as kendo.ui.AutoComplete).dataItem(e.item.index());
                    onSelected(selectedStudent);
                }
            });
        }

        /**
         * Event that will be before the request is made to append the parameters to the request.
         * This is where the DTO is build that is pass as an input parameter on the API.
         * @param that
         */
        private onRequestFilterData(that: StudentSearchAutoComplete): IFilterStudentSearch {
            let autoComplete = $(that.componentId).data("kendoAutoComplete") as kendo.ui.AutoComplete;
            let filterStudentSearch: IFilterStudentSearch = {} as IFilterStudentSearch;
            filterStudentSearch.campusId = that.campusId;
            if (autoComplete != undefined) {
                filterStudentSearch.filter = autoComplete.value();    
            }
            return filterStudentSearch;
        }
    }
}