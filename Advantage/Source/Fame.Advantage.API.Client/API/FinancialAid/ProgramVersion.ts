﻿module Api.FinancialAid {
    import ICampus = Api.SystemCatalog.Models.ICampus;
    import IDataProvider = API.Models.IDataProvider;

    export class ProgramVersion implements IDataProvider {
        constructor(dataProvider? : string) {

        }
        getDataSource(onRequestFilterData: () => any, group?: any, functionName?: string): kendo.data.DataSource {

            if (functionName) {
                if (this[functionName]) {
                    return this[functionName](onRequestFilterData)
                }
            }
            return this.getProgramVersionsByCampus(onRequestFilterData);

        }
         getProgramVersionsByCampus = function(onRequestFilterData: () => ICampus): kendo.data.DataSource  {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/AcademicRecords/ProgramVersions/GetTitleIVProgramVersionsByCampus", onRequestFilterData);
        }

         //This API is used to fetch the program version total hours.
         getProgramVersionTotalHours = function (programVersionId: any, onResponseCallback: (response: Response) => any) {
             let programVersion = { programVersionId: programVersionId } as any;
             let request = new Request();
             request.send(RequestType.Post, "v1/AcademicRecords/ProgramVersions/GetProgramVersionTotalHours", programVersion, RequestParameterType.QueryString, onResponseCallback);
         }
         //This API is used to fetch the program version schedule details for the given enrollment Id.
         getScheduleDetailsByProgramVersionId = function (onRequestFilterData: () => ICampus) {
             let request = new Request();
             return request.getRequestAsKendoDataSource("v1/AcademicRecords/ProgramVersions/GetScheduleDetailsByProgramVersionId", onRequestFilterData);
         }
    }
}