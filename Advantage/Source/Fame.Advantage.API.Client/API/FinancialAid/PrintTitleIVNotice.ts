﻿/// <reference path="../Request.ts" />

module Api.FinancialAid {
    import IDataProvider = API.Models.IDataProvider;
    export class PrintTitleIVNotice implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getTitleIVNoticesForPrinting(onRequestFilterData);
        }

        public getTitleIVNoticesForPrinting(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/AcademicRecords/TitleIVSAP/GetTitleIVNoticesForPrinting", onRequestFilterData);


        }

        public printTitleIVNotices(noticeIds: Array<string>, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post, "v1/AcademicRecords/TitleIVSAP/PrintTitleIVNotices", { noticeIds: noticeIds }, RequestParameterType.Body, onResponseCallback);
        }
    }
}