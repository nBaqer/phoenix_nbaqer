﻿module Api {
    import IDropReason = API.SystemCatalog.Models.IStudentDropReason;
    import IDataProvider = API.Models.IDataProvider;
    export class DropReason {
        public getDropReasonByCampusId(onRequestFilterData: () => IDropReason): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/DropReason/GetByCampus", onRequestFilterData);
        }

        public getNaccasDropReason(onRequestFilterData: () => IDropReason): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/DropReason/GetNaccasDropReasons", onRequestFilterData);
        }

        getDataSource(onRequestFilterData: () => any, group?: any, functionName?: string): kendo.data.DataSource {

            let hasCustomFunctionName = functionName !== undefined;
            if (hasCustomFunctionName) {
                if (functionName === "getDropReasonByCampusId") {
                    return this.getDropReasonByCampusId(onRequestFilterData);
                }

                if (functionName === "GetNaccasDropReasons") {
                    return this.getNaccasDropReason(onRequestFilterData);
                }

            }
            return this.getDropReasonByCampusId(onRequestFilterData);;
        }
    }
}