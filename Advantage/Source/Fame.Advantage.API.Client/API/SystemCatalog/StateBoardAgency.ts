﻿module Api {
    import IDataProvider = API.Models.IDataProvider;

    export class StateBoardAgency implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getStateBoardAgencies(onRequestFilterData);
        }
        public getStateBoardAgencies(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/StateBoardAgency/GetByStateId", onRequestFilterData);
        }

        public fetchAgencyDetails(agencyId: any, onResponseCallback: (response: Response) => any) {
            let agency = { agencyId } as any;
            let request = new Request();
            
            request.send(RequestType.Get,
                "v1/SystemCatalog/StateBoardAgency/GetStateBoardAgency",
                agency,
                RequestParameterType.QueryString,
                onResponseCallback);
        }
    }
}