﻿module Api {
    import IRoles = Api.SystemCatalog.Models.IRoles;
    import IDataProvider = API.Models.IDataProvider;

    export class Roles implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getRolesByUserType(onRequestFilterData);
        }

        public getAllRoles(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/Roles/getallRoles", onRequestFilterData);
        }
        public getRolesByUserType(onRequestFilterData: () => IRoles): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSourceWithPageSize("v1/SystemCatalog/Roles/getRolesByUserType", onRequestFilterData,250);
        }
    }
}