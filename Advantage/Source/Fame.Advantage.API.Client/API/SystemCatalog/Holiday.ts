﻿module Api {
    export class Holiday {
        public GetHolidayListByCampus(campusId: any, onResponseCallback: (response: Response) => any){
            let request = new Request();
            let campus = { campusId: campusId }
            request.send(RequestType.Get, "v1/SystemCatalog/Holiday/GetHolidayListByCampus", campus, RequestParameterType.QueryString, onResponseCallback);
        }
    }
}

