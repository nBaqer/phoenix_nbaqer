﻿module API.Models {
    export interface IListItem {
            text: string;
            value: string;
        }
}