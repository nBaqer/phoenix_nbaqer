﻿module API.Models {
    export interface IMaritalStatus {
        text: string;
        value: string;
        afaValue: string;
    }
}