﻿module API.Models {
    export interface IGender {
        text: string;
        value: string;
        afaValue: string;
    }
}