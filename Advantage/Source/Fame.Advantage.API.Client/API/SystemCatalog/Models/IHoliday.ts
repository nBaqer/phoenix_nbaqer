﻿module API.Models {
    export interface IHoliday {
        id: number;
        description: string;
        startDate: Date;
        endDate: Date;

    }
}