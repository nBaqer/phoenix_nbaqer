﻿module Api.SystemCatalog.Models {
    export interface IWidgetSetting {
        widgetId: string;
        code: string;
        hasSetting: boolean;
        isVisible: boolean;
        positionX: number;
        positionY: number;
        columns: number
        rows: number;
    }
}

