﻿module API.Models {
    export interface ICitizenShip {
        text: string;
        value: string;
        afaValue: string;
    }
}