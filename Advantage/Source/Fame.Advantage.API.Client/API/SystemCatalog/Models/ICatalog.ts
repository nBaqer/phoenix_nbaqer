﻿module API.Models {
    export interface ICatalog{
        id: string;
        name: string;
        phoneTypes: Array<IListItem>; // List<ListItem<string, Guid>>
        addressTypes: Array<IListItem>;
        countriesTypes: Array<IListItem>;
        states: Array<IListItem>;
        interestAreas: Array<IListItem>;
        sourceCategories: Array<ISourceCategory>;  // List<SourceCategory> 
        emailTypes: Array<IListItem>;
        admissionsReps: Array<IListItem>;
        leadStatuses: Array<IListItem>;
        genders: Array<IGender>;
        maritalStatus: Array<IMaritalStatus>;
        citizenShip :Array<ICitizenShip>;
    }
}