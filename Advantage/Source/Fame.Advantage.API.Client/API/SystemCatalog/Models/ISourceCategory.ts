﻿module API.Models {
    export interface ISourceCategory {
        text: string;
        value: string;
        sourceType: Array<IListItem>;
    }
}