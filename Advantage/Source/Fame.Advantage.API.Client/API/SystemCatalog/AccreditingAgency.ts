﻿module Api {
    import IDataProvider = API.Models.IDataProvider;

    export class AccreditingAgency implements IDataProvider {

        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getAccreditingAgenciesDS(onRequestFilterData);
        }

        public getAccreditingAgenciesDS(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/AccreditingAgency/GetAccreditingAgencies", onRequestFilterData);
        }

        public getAccreditingAgencies(onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/SystemCatalog/AccreditingAgency/GetAccreditingAgencies", {}, RequestParameterType.QueryString, onResponseCallback);
        }

        public saveOrUpdateNaccasSetting(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post,
                "v1/NaccasSettings/SaveOrUpdateNaccasReportSetting",
                data,
                RequestParameterType.Body,
                onResponseCallback);
        }


        public deleteNaccasSetting(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Delete,
                "v1/NaccasSettings/DeleteNaccasReportSetting",
                data,
                RequestParameterType.Body,
                onResponseCallback);
        }

        public fetchNaccasSettings(campusId: any, onResponseCallback: (response: Response) => any) {
            let params = {} as any;
            let request = new Request();


            params["CampusId"] = campusId;

            request.send(RequestType.Get,
                "v1/NaccasSettings/GetNaccasReportSetting",
                params,
                RequestParameterType.QueryString,
                onResponseCallback);
        }
       
    }
}