﻿module Api {

    //import IWidgetSetting = Api.SystemCatalog.Models.IWidgetSetting;

    export class Widget {
        public getDashboardWidgets(campusId: any, resourceId, onResponseCallback: (response: Response) => any) {
            let params = { campusId, resourceId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/SystemCatalog/Widget/GetUserDashboardWidgets", params, RequestParameterType.QueryString, onResponseCallback);
        }
    }
}