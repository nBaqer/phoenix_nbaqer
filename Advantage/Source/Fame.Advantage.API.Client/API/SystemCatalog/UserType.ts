﻿module Api {
    import IUserType = Api.SystemCatalog.Models.IUserType;
    import IDataProvider = API.Models.IDataProvider;

    /**
   * The API user type class is the encapsulation to the user type Controller.
     * 
   */
    export class UserType implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getUserTypes(onRequestFilterData);
        }
        public getUserTypes(onRequestFilterData: () => IUserType): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/UserType/GetUserTypes", onRequestFilterData);
        }
    }
}