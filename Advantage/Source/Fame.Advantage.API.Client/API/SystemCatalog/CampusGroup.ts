﻿module Api {
    import ICampus = Api.SystemCatalog.Models.ICampus;
    import IDataProvider = API.Models.IDataProvider;

    export class CampusGroup implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getAllCampusGroups(onRequestFilterData);
        }
        public getAllCampusGroups(onRequestFilterData: () => ICampus): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/CampusGroup/Get", onRequestFilterData);
        }
    }
}

