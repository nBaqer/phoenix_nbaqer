﻿module Api {
    import IStates = Api.SystemCatalog.Models.IStates;
    import IDataProvider = API.Models.IDataProvider;

    export class States implements IDataProvider {
        getDataSource(onRequestFilterData: () => any, group?: any, functionName?: string): kendo.data.DataSource {

            let hasCustomFunctionName = functionName !== undefined;

            if (hasCustomFunctionName) {
                if (functionName === "getAllStateBoardStates") {
                    return this.getAllStateBoardStates(onRequestFilterData);
                }
                else if (functionName === "getAllStatesShortNames") {
                    return this.getAllStatesShortNames(onRequestFilterData);
                }
                else if (functionName == "GetStatesByCountryId") {
                    return this.getAllStatesByCountryId(onRequestFilterData);
                }

            }
            return this.getAllStates(onRequestFilterData);
        }
        public getAllStateBoardStates(onRequestFilterData: () => IStates): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSourceWithPageSize("v1/SystemCatalog/States/GetStateBoardStates", onRequestFilterData, 100);
        }

        public getAllStatesShortNames(onRequestFilterData: () => IStates): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSourceWithPageSize("v1/SystemCatalog/States/GetAllStatesShortDescription", onRequestFilterData, 100);
        }

        public getAllStatesByCountryId(onRequestFilterData: () => IStates): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSourceWithPageSize("v1/SystemCatalog/States/GetStatesByCountryId", onRequestFilterData, 100);
        }

        public getAllStates(onRequestFilterData: () => IStates): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSourceWithPageSize("v1/SystemCatalog/States/GetAllStates", onRequestFilterData, 100);
        }

        public getAllStateBoardStatesRequest(onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/SystemCatalog/States/GetStateBoardStates", {}, RequestParameterType.QueryString, onResponseCallback);
        }
    }
}