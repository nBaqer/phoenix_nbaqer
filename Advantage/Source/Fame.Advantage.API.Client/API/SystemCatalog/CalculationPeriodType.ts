﻿module Api {
    /**
   * The API Calculation period type class is the encapsulation to the Calculation period type Controller.
     * 
   */
    export class CalculationPeriodType {
        public getCalculationPeriodTypes(onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Get, "v1/SystemCatalog/CalculationPeriodType/Get",null,null, onResponseCallback);
        }
    }
}