﻿module Api {
    import IDataProvider = API.Models.IDataProvider;

    export class FundSource9010MappingApiCalls implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.Get9010MappingsByCampusId(onRequestFilterData);
        }
        public Get9010MappingsByCampusId(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSourceWithPageSize("v1/Reports/Operations9010/Get9010MappingsByCampusId", onRequestFilterData, 1000);
        }

        public UpdateAwardMappings9010(data: any, campusId: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post, "v1/Reports/Operations9010/UpdateCampusAwardMappings9010?campusId=" + campusId, data, RequestParameterType.Body, onResponseCallback);
        }

        public printCampus9010Mappings(campusId: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.sendFile(RequestType.Post, "v1/Reports/Operations9010/PrintCampus9010Mappings?campusId=" + campusId, {}, FileOutputType.Xlsx,
                "Mappings9010-" + new Date().toISOString().replace('T', ' ').replace(/\..*$/, '').split(' ')[0].split('-').join(''), "xlsx", RequestParameterType.Body, onResponseCallback);
        }
    }

    export class AwardTypes9010 implements IDataProvider {

        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.Get9010MappingsByCampusId(onRequestFilterData);
        }
        public Get9010MappingsByCampusId(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSourceWithPageSize("v1/Reports/Operations9010/Get9010AwardTypes", onRequestFilterData, 1000);
        }
    }
}