﻿module Api {
    import ICampus = Api.SystemCatalog.Models.ICampus;
    import IDataProvider = API.Models.IDataProvider;

    export class Campus implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getAllCampuses(onRequestFilterData);
        }
        public getAllCampuses(onRequestFilterData: () => ICampus): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/Campus/GetAllCampusesByUserId", onRequestFilterData);
        }

        public fetchCampusInformation(campusId: any, onResponseCallback: (response: Response) => any) {
            let campus = { campusId } as any;
            let request = new Request();

            request.send(RequestType.Get,
                "v1/SystemCatalog/School/GetDetailJson",
                campus,
                RequestParameterType.QueryString,
                onResponseCallback);
        }
    }
}