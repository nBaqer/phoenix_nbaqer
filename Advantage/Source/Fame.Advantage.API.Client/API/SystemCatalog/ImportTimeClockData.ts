﻿module Api {
    import IDataProvider = API.Models.IDataProvider;

    export class TimeClockImportLog implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.GetTimeClockImportLogs(onRequestFilterData);
        }
        public GetTimeClockImportLogs(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSourceWithPageSize("v1/AcademicRecords/Attendance/GetTimeClockImportLogsByCampus", onRequestFilterData, 20);
        }
    }
}