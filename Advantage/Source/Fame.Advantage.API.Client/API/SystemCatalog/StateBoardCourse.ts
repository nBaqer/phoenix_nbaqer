﻿module Api {
    import IDataProvider = API.Models.IDataProvider;

    export class StateBoardCourse implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getByStateId(onRequestFilterData);
        }
        public getByStateId(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/StateBoardCourse/GetByStateId", onRequestFilterData);
        }
    }
}