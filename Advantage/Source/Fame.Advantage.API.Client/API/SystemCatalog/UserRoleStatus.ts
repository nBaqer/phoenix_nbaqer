﻿module Api {
    /**
   * The API User Role class is the encapsulation to the user role status validation.
     * 
   */
    export class UserRoleStatus {
        public isSupportUser(onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/SystemCatalog/UserRoleStatus/IsSupportUser", null, null, onResponseCallback);
        }
    }
}