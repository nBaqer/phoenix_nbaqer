﻿module Api {
    /**
   * The User Role class is the encapsulation to the user campus/role validation.
     * 
   */

    export class UserRole {
        campusGroupId: string = "00000000-0000-0000-0000-000000000000";
        roleId: string = "00000000-0000-0000-0000-000000000000";

        public constructor(init?: Partial<UserRole>) {
            (<any>Object).assign(this, init);
        }
    }

}