﻿module Api {
    import IDataProvider = API.Models.IDataProvider;

    export class Program implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getClockHourProgramsByCampus(onRequestFilterData);
        }
        public getClockHourProgramsByCampus(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/Program/GetClockHourProgramsByCampus", onRequestFilterData);
        }
    }
}