﻿module Api {
    import IStatus = Api.SystemCatalog.Models.IStatus;
    import IDataProvider = API.Models.IDataProvider;

    /**
   * The API status class is the encapsulation to the status Controller.
     * 
   */
    export class Status implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getStatuses(onRequestFilterData);
        }
        public getStatuses(onRequestFilterData: () => IStatus): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/Statuses/Get", onRequestFilterData);
        }
    }
}