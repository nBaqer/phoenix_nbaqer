﻿module Api {
    import IDataProvider = API.Models.IDataProvider;

    /**
   * The API resource class is the encapsulation to the resource Controller.
     * 
   */
    export class Resource implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getResources(onRequestFilterData);
        }
        public getResources(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/Resource/GetByTypeId", onRequestFilterData);
        }
    }
}