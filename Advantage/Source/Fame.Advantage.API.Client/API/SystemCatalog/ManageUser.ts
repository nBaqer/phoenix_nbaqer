﻿module Api {
    export class ManageUser {
        public saveUser(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post,
                "v1/SystemCatalog/User/Post",
                data,
                RequestParameterType.Body,
                onResponseCallback);
        }

        public updateUser(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Put,
                "v1/SystemCatalog/User/Put",
                data,
                RequestParameterType.Body,
                onResponseCallback);
        }

        
        //This fetchUserDetails is used to fetch the user details for the given user Id.
        public fetchUserDetails(userId: any, onResponseCallback: (response: Response) => any) {
            let termination = { userId } as any;
            let request = new Request();
            request.send(RequestType.Get,
                "v1/SystemCatalog/User/GetById",
                termination,
                RequestParameterType.QueryString,
                onResponseCallback);
        }
    }
}