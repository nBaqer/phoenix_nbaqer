﻿module Api {
    import IModule = Api.SystemCatalog.Models.IModule;
    import IDataProvider = API.Models.IDataProvider;

    /**
   * The API module class is the encapsulation to the module Controller.
     * 
   */
    export class Module implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getModules(onRequestFilterData);
        }
        public getModules(onRequestFilterData: () => IModule): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/Module/Get", onRequestFilterData);
        }
    }
}