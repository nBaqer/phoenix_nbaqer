﻿module Api {
    import IFilterUserSearch = API.Models.IFilterUserSearch;
    import IDataProvider = API.Models.IDataProvider;
    /**
     * The API User class is the encapsulation to the Usser Controller.
     * In there the request is initialized and is given the path and any other required parameter.
     * For example, if the request is of type Post and if you want to pass the parameters as query string or as part of the body.
     */
    export class User implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getUserSearchKendoDataSource(onRequestFilterData);
        }
        public getUserSearchKendoDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSourceWithPageSize("v1/SystemCatalog/User/SearchUsers", onRequestFilterData, 20);
        }
        public sendUserEmail(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post, "v1/SystemCatalog/User/SendNewUserEmail", data, RequestParameterType.Body, onResponseCallback);
        }
        public sendUserResetPasswordEmail(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post, "v1/SystemCatalog/User/SendResetPasswordEmail", data, RequestParameterType.Body, onResponseCallback);
        }

    }
}