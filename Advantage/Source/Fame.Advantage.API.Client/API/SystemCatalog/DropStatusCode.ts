﻿module Api {
    import IFilterSystemStatus = API.SystemCatalog.Models.IDropStatus;

    export class DropStatusCode {
        public getDropStatusByCampusId(onRequestFilterData: () => IFilterSystemStatus): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/DropStatus/GetDropStatusCode", onRequestFilterData);
        }
    }
} 