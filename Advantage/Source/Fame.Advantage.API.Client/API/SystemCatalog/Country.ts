﻿module Api {
    import IStates = Api.SystemCatalog.Models.IStates;
    import IDataProvider = API.Models.IDataProvider;

    export class Country implements IDataProvider {
        getDataSource(onRequestFilterData: () => any, group?: any, functionName?: string): kendo.data.DataSource {

            let hasCustomFunctionName = functionName !== undefined;

            if (hasCustomFunctionName) {
                //custom calls here

            }
            return this.getAllCountries(onRequestFilterData);
        }
        public getAllCountries(onRequestFilterData: () => IStates): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/Countries/GetAllCountries", onRequestFilterData);
        }
    }
}