﻿module Api.SystemCatalog.Mappings {
    /**
   * The program state board course mapping class is the mapping between program and state board course id.
     * 
   */

    export class ProgramStateBoardCourseMapping {
        programId: string = "00000000-0000-0000-0000-000000000000";
        stateBoardCourseId: string = "0";

        public constructor(init?: Partial<ProgramStateBoardCourseMapping>) {
            (<any>Object).assign(this, init);
        }
    }

}