﻿module Api {
    /**
   * The API Academic Calendar class is the encapsulation to the Academic Calendar type Controller.
     * 
   */
    export class AcademicCalendar {
        public isClockHour(studentEnrollmentId: string, onResponseCallback: (response: Response) => any) {
            let enrollmentId = { studentEnrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/SystemCatalog/AcademicCalendar/IsClockHour", enrollmentId, RequestParameterType.QueryString, onResponseCallback);
        }

        public isPaymentPeriod(periodTypeId: string, onResponseCallback: (response: Response) => any) {
            let calculationperiodTypeId = { periodTypeId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/SystemCatalog/CalculationPeriodType/IsPaymentPeriod", calculationperiodTypeId, RequestParameterType.QueryString, onResponseCallback);
        }
    }
}