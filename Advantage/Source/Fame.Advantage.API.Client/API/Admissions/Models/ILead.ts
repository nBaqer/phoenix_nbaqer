﻿module API.Models {
    export interface ILead extends  ILeadBase {
        PhoneTypeId: string;
        Phone: string;
        PhoneId: string;
        IsUsaPhoneNumber: boolean;
        EmailTypeId: string;
        Email: string;
        EmailId: string;
        AddressTypeId: string;
        AddressId: string;
        Address1: string;
        City: string;
        StateId: string;
        Zip: string;
        DriverLicenseNumber: string;
        LicenseState: string;
        CitizenShipStatus: string;
        MaritalStatus: string;
        Gender: string;
    }
}