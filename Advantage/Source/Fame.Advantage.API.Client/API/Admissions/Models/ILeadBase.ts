﻿module API.Models {
    export interface ILeadBase {
        LeadId: string;
        FirstName: string;
        LastName: string;
        LeadStatusId: string;
        CreateDate: Date;
        SSN: string;
        CampusId: string;
        AfaStudentId: number;
        CmsId: string;
        MiddleName: string;
        BirthDate:Date;
    }
}