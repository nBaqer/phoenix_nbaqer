﻿module Api {
    export class R2T4StudentTermination {
        //this API function is used for saving termination detail
        public saveTermination(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post, "v1/AcademicRecords/StudentTermination/Post", data, RequestParameterType.Body, onResponseCallback);
        }

        //this API function is used for updating termination detail
        public updateTermination(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Put, "v1/AcademicRecords/StudentTermination/Put", data, RequestParameterType.Body, onResponseCallback);
        }

        //this API function is used for deleting termination detail
        public deleteTerminationById(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Delete, "v1/AcademicRecords/StudentTermination/Delete", data, RequestParameterType.QueryString, onResponseCallback);
        }

        //This fetchR2T4Inputdetails is used fetch the R2T4 input details for the given termination ID.
        public fetchR2T4Inputdetails(terminationId: any, onResponseCallback: (response: Response) => any) {
            let termination = { terminationId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/R2T4Input/Get", termination, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the termination details for the given enrollment Id.
        public fetchTerminationByEnrollmentId(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/StudentTermination/Get", data, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used for saving the R2T4 input detail.
        public saveR2T4InputDetail(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post, "v1/AcademicRecords/R2T4Input/Post", data, RequestParameterType.Body, onResponseCallback);
        }

        //This API is used for updating the R2T4 input detail.
        public updateR2T4InputDetail(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Put, "v1/AcademicRecords/R2T4Input/Put", data, RequestParameterType.Body, onResponseCallback);
        }

        // This API is used for deleting the R2T4 Details by termination id
        public deleteR2T4DetailsByTerminationId(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Delete, "v1/AcademicRecords/R2T4Input/Delete", data, RequestParameterType.QueryString, onResponseCallback);
        }

        // This fetchR2T4Resultdetails is used fetch the R2T4 Result details for the given R2T4 input detail.
        public fetchR2T4Resultdetails(r2T4InputDetail: API.Models.IR2T4Input, onResponseCallback: (response: Response) => any) {
            const request = new Request();
            request.send(RequestType.Post, "v1/AcademicRecords/R2T4Result/GetR2T4Result", r2T4InputDetail, RequestParameterType.Body, onResponseCallback);
        }

        // This API is used for saving the R2T4 Result detail.
        public saveR2T4Results(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post, "v1/AcademicRecords/R2T4Result/Post", data, RequestParameterType.Body, onResponseCallback);
        }

        // This API is used to save the R2T4 results in override R2T4 result table.
        public saveOverrideR2T4Results(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post, "v1/AcademicRecords/R2T4Result/PostOverrideR2T4Result", data, RequestParameterType.Body, onResponseCallback);
        }

        // This API is used to update the R2T4 results in override R2T4 result table.
        public updateOverrideR2T4Results(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Put, "v1/AcademicRecords/R2T4Result/PutOverrideResults", data, RequestParameterType.Body, onResponseCallback);
        }

        // This API is used to update the R2T4 results in R2T4 result table.
        public updateR2T4Results(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Put, "v1/AcademicRecords/R2T4Result/Put", data, RequestParameterType.Body, onResponseCallback);
        }

        // This API is used to check whether the R2T4 results exists for the given termination Id in R2T4 results tables.
        public isTerminationIdExists(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post, "v1/AcademicRecords/R2T4Result/GetR2T4ResultId", data, RequestParameterType.Body, onResponseCallback);
        }

        // This API is used to fetch the pre-saved R2T4 results from tables without performing new R2T4 caluclations.
        public getSavedR2T4Results(terminationId: any, onResponseCallback: (response: Response) => any) {
            let termination = { terminationId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/R2T4Result/Get", termination, RequestParameterType.QueryString, onResponseCallback);
        }

        // This API is used to check if R2T4 Results exists in the given R2T4 Result tables like R2T4Reults or R2T4 Override table.
        public isResultsExists(terminationId: any, onResponseCallback: (response: Response) => any) {
            let termination = { terminationId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/R2T4Result/IsResultsExists", termination, RequestParameterType.QueryString, onResponseCallback);
        }

        // This API is used to get R2T4 Override Result for the given termination id.
        public getR2T4OverrideResults(terminationId: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();   
            request.send(RequestType.Get, "v1/AcademicRecords/R2T4Result/GetR2T4OverrideResults", { terminationId } as any, RequestParameterType.QueryString, onResponseCallback);
        }

        // This API is used to get R2T4 Override Result for the given termination id.
        public getR2T4ResultsByTerminationId(terminationId: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/R2T4Result/GetR2T4ResultsByTerminationId", { terminationId } as any, RequestParameterType.QueryString, onResponseCallback);
        }

        // This API is used to check if R2T4 Input data exists in the given R2T4 Input tables like R2T4Input or R2T4 Override Input table.
        public isR2T4InputExists(terminationId: any, onResponseCallback: (response: Response) => any) {
            let termination = { terminationId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/R2T4Input/IsR2T4InputExists", termination, RequestParameterType.QueryString, onResponseCallback);
        }

        // This API is used for deleting the R2T4 Result Details by termination id
        public deleteR2T4ResultByTerminationId(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Delete, "v1/AcademicRecords/R2T4Result/Delete", data, RequestParameterType.QueryString, onResponseCallback);
        }

        // This API is to get the R2T4 tabs completion status from database by terminationId.
        public getR2T4CompletedStatus(terminationId: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/R2T4Input/GetR2T4CompletedStatus", { terminationId } as any, RequestParameterType.QueryString, onResponseCallback);
        }

        // This API is used to update the Enrollment status details when the student is terminated from the specified enrollment.
        public updateEnrollmentStatus(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Put, "v1/AcademicRecords/ApproveTermination/PutEnrollmentDetails", data, RequestParameterType.Body, onResponseCallback);
        }

        // This API is used to save the student status change details.
        public saveStudentStatusChange(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post, "v1/AcademicRecords/ApproveTermination/PostStudentStatusChanges", data, RequestParameterType.Body, onResponseCallback);
        }

        // This API is used generate the student summary report.
        public generateReport(reportRequestData: API.AcademicRecords.Models.IReportRequest, fileName: string, onResponseCallback: (response: Response) => any) {
            let request = new Request(); 
            
            reportRequestData.reportDataSourceType = AD.ReportDataSourceType.RestApi;
            reportRequestData.reportOutput = AD.ReportOutput.Pdf;

            request.sendFile(RequestType.Post,
                "v1/AcademicRecords/ApproveTermination/GenerateStudentSummaryReport",
                reportRequestData,
                Api.FileOutputType.Pdf,
                fileName,
                FileExtension.pdfExtension,
                RequestParameterType.Body,
                onResponseCallback);
        }

        // This API is used to check if R2T4 Results exists in the given R2T4 Result tables like R2T4Reults or R2T4 Override table.
        public isResultOverridden(terminationId: any, onResponseCallback: (response: Response) => any) {
            let termination = { terminationId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/R2T4Result/IsResultOverridden", termination, RequestParameterType.QueryString, onResponseCallback);
        } 

        // This API is used to get the student termination details based on the termination id provided.
        public getTerminationDetails(terminationId: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ApproveTermination/GetTerminationDetails", { terminationId } as any, RequestParameterType.QueryString, onResponseCallback);
        }

        // This API is used to update the isR2T4Resultscompleted field in R2T4Result table for the given termination id.
        public updateR2T4ResultStatus(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Put, "v1/AcademicRecords/R2T4Result/UpdateR2T4ResultStatus", data, RequestParameterType.Body, onResponseCallback);
        }

        // This API is used to undo the student from a specific enrollment for the given enrollment id.
        public UndoTerminationById(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request(); 
            request.send(RequestType.Get, "v1/AcademicRecords/UndoTermination/UndoStudentTermintion", data, RequestParameterType.QueryString, onResponseCallback);
        }

        // This API will return the last attendance date from attendance table or the given enrollment id.
        public getLastDateAttended(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            let enrollment = { enrollmentId } as any;
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramPeriodDetail/GetLastDateAttended", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }
    }
}