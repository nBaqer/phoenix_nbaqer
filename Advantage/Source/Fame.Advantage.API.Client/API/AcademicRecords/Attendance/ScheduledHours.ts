﻿module Api.Attendance {
    import IDataProvider = API.Models.IDataProvider;

    /**
     * The API Student class is the encapsulation to the Student Controller.
     * In there the request is initialized and is given the path and any other required parameter.
     * For example, if the request is of type Post and if you want to pass the parameters as query string or as part of the body.
     */
    export class ScheduledHours implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getStudentsScheduledForDay(onRequestFilterData);
        }

        public getStudentsScheduledForDay(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/AcademicRecords/Attendance/GetStudentsScheduledForDay", onRequestFilterData);
        }

        public adjustScheduledHours(data: any, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Post, "v1/AcademicRecords/Attendance/AdjustScheduledHours", data, RequestParameterType.Body, onResponseCallback);
        }
    }
}