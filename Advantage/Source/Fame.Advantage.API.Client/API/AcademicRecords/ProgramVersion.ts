﻿module Api {
    import ICampus = Api.SystemCatalog.Models.ICampus;
    import IDataProvider = API.Models.IDataProvider;

    export class ProgramVersion implements IDataProvider {
        constructor(dataProvider?: string) {

        }
        getDataSource(onRequestFilterData: () => any, group?: any): kendo.data.DataSource {
            return this.getProgramVersionsByCampus(onRequestFilterData);
        }
        getProgramVersionsByCampus = function (onRequestFilterData: () => ICampus): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSourceWithPageSize("v1/AcademicRecords/ProgramVersions/GetProgramVersionsByCampus", onRequestFilterData, 1000);
        }

    }
}