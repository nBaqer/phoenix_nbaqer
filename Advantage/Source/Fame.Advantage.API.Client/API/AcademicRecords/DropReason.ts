﻿/// <reference path="../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
module Api {
    import IDropReason = API.Models.IStudentDropReason; 
    export class DropReason { 
        public getDropReasonByCampusId(onResponseCallback: () => IDropReason) : kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("AcademicRecords/DropReason/GetDropReasonListForCurrentUser",
                onResponseCallback);
        }
    }
}