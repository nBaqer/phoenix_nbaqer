﻿module Api {
    import IFilterStudentSearch = API.Models.IFilterStudentSearch;
    import IDataProvider = API.Models.IDataProvider;

    /**
     * The API Student class is the encapsulation to the Student Controller.
     * In there the request is initialized and is given the path and any other required parameter.
     * For example, if the request is of type Post and if you want to pass the parameters as query string or as part of the body.
     */
    export class StudentSummary {
        public getAttendanceSummary(enrollmentId: string, onResponseCallback: (response: any) => any, showLoader?: boolean) {
            let request = new Request();
            if (typeof showLoader !== undefined)
                request.shouldShowIsLoading = showLoader;
            var data = { enrollmentId: enrollmentId }
            request.send(RequestType.Get, "v1/AcademicRecords/StudentSummary/GetAttendanceSummary", data, RequestParameterType.QueryString, onResponseCallback);
        }
        public getProgramSummary(enrollmentId: string, onResponseCallback: (response: any) => any, showLoader?: boolean) {
            let request = new Request();
            if (typeof showLoader !== undefined)
                request.shouldShowIsLoading = showLoader;
            var data = { enrollmentId: enrollmentId }
            request.send(RequestType.Get, "v1/AcademicRecords/StudentSummary/GetEnrollmentProgramSummary", data, RequestParameterType.QueryString, onResponseCallback);
        }

        public getContactSummary(enrollmentId: string, onResponseCallback: (response: any) => any, showLoader?: boolean) {
            let request = new Request();
            if (typeof showLoader !== undefined)
                request.shouldShowIsLoading = showLoader;
            var data = { enrollmentId: enrollmentId }
            request.send(RequestType.Get, "v1/AcademicRecords/StudentSummary/GetStudentContactSummary", data, RequestParameterType.QueryString, onResponseCallback);
        }
    }
}