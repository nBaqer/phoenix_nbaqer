﻿module Api {
    import IFilterStudentSearch = API.Models.IFilterStudentSearch;
    import IDataProvider = API.Models.IDataProvider;

    /**
     * The API Student class is the encapsulation to the Student Controller.
     * In there the request is initialized and is given the path and any other required parameter.
     * For example, if the request is of type Post and if you want to pass the parameters as query string or as part of the body.
     */
    export class Student implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getStudentSearchByStatusesKendoDataSource(onRequestFilterData);
        }
        public getStudentSearchKendoDataSource(onRequestFilterData: () => IFilterStudentSearch): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/AcademicRecords/Student/Search", onRequestFilterData);
        }
         
        public getHoldStatusByStudentId(data: any, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/Student/GetHoldStatus", data, RequestParameterType.QueryString, onResponseCallback);
        }

        public getStudentSearchByStatusesKendoDataSource(onRequestFilterData: () => IFilterStudentSearch): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/AcademicRecords/Student/SearchWithStatuses", onRequestFilterData, "post");
        }
    }
}