﻿module Api {
    import IDataProvider = API.Models.IDataProvider;
    export class TitleIVMulti implements IDataProvider {
        getDataSource(onRequestFilterData: () => any, group?: any): kendo.data.DataSource {
            return this.getTitleIVSAPResultsMultiple(onRequestFilterData, group);
        }

        public getTitleIVSAPResultsMultiple(onRequestFilterData: () => any, group?: any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/AcademicRecords/TitleIVSAP/GetTitleIVResultsForMultipleStudents", onRequestFilterData, "post", group);
        }
    }
}