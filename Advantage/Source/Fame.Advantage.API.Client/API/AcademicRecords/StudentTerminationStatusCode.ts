﻿module Api {

    export class StudentTerminationStatusCode {
        public getStudentTerminationStatusCode(onRequestFilterData: () => string): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/AcademicRecords/StudentTermination/GetStudentTerminationStatusCode", onRequestFilterData);
        }
    }
} 