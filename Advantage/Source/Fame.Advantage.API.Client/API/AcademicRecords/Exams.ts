﻿module Api {
    import IDataProvider = API.Models.IDataProvider;
    export class Enrollments implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getEnrollementsByStudentAsDataSource(onRequestFilterData);
        }

        public getEnrollementsByStudentAsDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/AcademicRecords/Enrollment/GetEnrollmentsByStudentId", onRequestFilterData, "post");
        }

        public getEnrollmentsByStudentId(data: any, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Post, "v1/AcademicRecords/Enrollment/GetEnrollmentsByStudentId", data, RequestParameterType.Body, onResponseCallback);
        }

        public getEnrollmentDetail(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/Enrollment/GetEnrollmentDetail", data, RequestParameterType.QueryString, onResponseCallback);
        }
    }
}