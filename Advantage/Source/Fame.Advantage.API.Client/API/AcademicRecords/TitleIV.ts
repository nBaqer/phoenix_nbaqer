﻿module Api {
    import IDataProvider = API.Models.IDataProvider;
    export class TitleIV implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.getTitleIVSAPResults(onRequestFilterData);
        }
        
        public getTitleIVSAPResults(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/AcademicRecords/TitleIVSAP/GetTitleIVResultsForStudent", onRequestFilterData, "post");
        }
        
        public PlaceStudentOnTitleIVPrbation(data: any, onResponseCallback: (response: Response) => any): void {
            let request = new Request();                                                                       
            request.send(RequestType.Get, "v1/AcademicRecords/TitleIVSAP/PlaceStudentOnTitleIVPrbation", data, RequestParameterType.QueryString, onResponseCallback);
        }

        public executeTitleIVSapCheck(data: any, onResponseCallback: (response: Response) => any, showLoader?: boolean) {
            let request = new Request();
            if (showLoader !== undefined) {
                if (showLoader === false) {
                    request.setShouldShowIsLoading(false);
                }
            }
            return request.send(RequestType.Post, "v1/AcademicRecords/TitleIVSAP/ExecuteTitleIVSap", data, RequestParameterType.Body, onResponseCallback);
        }
    }
}