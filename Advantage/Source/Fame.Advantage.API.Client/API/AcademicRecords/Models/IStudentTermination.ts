﻿module API.Models {
    export interface IStudentTermination {
        terminationId: string;
        studentEnrollmentId: string;
        statusCodeId: string;
        dropReasonId: string;
        dateWithdrawalDetermined: Date;
        lastDateAttended: Date;
        isPerformingR2T4Calculator: boolean;
        calculationPeriodType: string;
        createdDate: Date;
        createdBy: string;
        updatedBy: string;
        updatedDate: Date;
        methodType: number;
    }
}