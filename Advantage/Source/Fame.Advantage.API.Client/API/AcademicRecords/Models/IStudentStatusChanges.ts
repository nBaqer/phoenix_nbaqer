﻿module API.Models {
    export interface IStudentStatusChanges {
        studentStatusChangeId: string;  
        studentEnrollmentId: string;  
        originalStatusId: string;  
        newStatusId: string; 
        campusId: string; 
        modifiedDate:Date; 
        modifiedUser: string; 
        isReversal: boolean; 
        dropReasonId: string; 
        dateOfChange: Date; 
        lastdateAttended:Date; 
        caseNumber: string; 
        requestedBy: string; 
        haveBackup: boolean; 
        haveClientConfirmation: boolean; 
        resultStatus: string; 
    }
}