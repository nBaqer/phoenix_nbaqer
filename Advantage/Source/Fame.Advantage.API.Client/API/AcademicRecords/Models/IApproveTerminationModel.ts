﻿module API.Models {
    export interface IApproveTerminationModel {
        id: string;
        campusId: string;
        leadId: string;
        studentId: string;
        userId: string;
    }
}