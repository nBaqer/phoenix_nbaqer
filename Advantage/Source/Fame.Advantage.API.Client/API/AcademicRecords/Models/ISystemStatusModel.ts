﻿module API.Models {
    export interface ISystemStatusModel {
        IsActiveEnrollments: boolean;
        IsDroppedEnrollments: boolean;
        StudentId: string;
    }
}