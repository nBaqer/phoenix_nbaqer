﻿module API.AcademicRecords.Models {
    export interface IReportRequest {
        reportLocation: string;
        parameters: {};
        reportDataSourceType: AD.ReportDataSourceType;
        reportOutput: AD.ReportOutput;
        reportCategory: string;
        reportName: string;
        reportExtension: string;
    }
}