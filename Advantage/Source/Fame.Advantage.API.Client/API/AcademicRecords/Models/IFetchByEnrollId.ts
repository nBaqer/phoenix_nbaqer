﻿module API.Models {
    export interface IFetchByEnrollId {
       enrollmentId: string;
    }
}