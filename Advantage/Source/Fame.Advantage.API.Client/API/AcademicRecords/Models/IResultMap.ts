﻿module API.AcademicRecords.Models {
    //This is an interface for R2T4Input, this is used to create R2T4 input data for API calls for generating model object and passing to API.
    export interface IResultMap {
        terminationId: string;
        tabId : number;
    }
}