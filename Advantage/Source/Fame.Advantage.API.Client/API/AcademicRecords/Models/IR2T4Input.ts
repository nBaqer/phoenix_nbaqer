﻿module API.Models {
    //This is an interface for R2T4Input, this is used to create R2T4 input data for API calls for generating model object and passing to API.
    export interface IR2T4Input {
        r2T4InputId: string;
        terminationId: string;
        programUnitTypeId: number;
        pellGrantDisbursed: number;
        pellGrantCouldDisbursed: number;
        fseogDisbursed: number;
        fseogCouldDisbursed: number;
        teachGrantDisbursed: number;
        teachGrantCouldDisbursed: number;
        iraqAfgGrantDisbursed: number;
        iraqAfgGrantCouldDisbursed: number;
        unsubLoanNetAmountDisbursed: number;
        unsubLoanNetAmountCouldDisbursed: number;
        subLoanNetAmountDisbursed: number;
        subLoanNetAmountCouldDisbursed: number;
        perkinsLoanDisbursed: number;
        perkinsLoanCouldDisbursed: number;
        directGraduatePlusLoanDisbursed: number;
        directGraduatePlusLoanCouldDisbursed: number;
        directParentPlusLoanDisbursed: number;
        directParentPlusLoanCouldDisbursed: number;
        isAttendanceNotRequired: boolean;
        startDate: Date;
        scheduledEndDate: Date;
        withdrawalDate: Date;
        completedTime: number;
        totalTime: number;
        tuitionFee: number;
        roomFee: number;
        boardFee: number;
        otherFee: number;
        isTuitionChargedByPaymentPeriod: boolean;
        creditBalanceRefunded: number;
        createdBy: string;
        createdDate: Date;
        updatedBy: string;
        updatedDate: Date;
        isR2T4InputCompleted: boolean;
        paymentType: number;
    }
}