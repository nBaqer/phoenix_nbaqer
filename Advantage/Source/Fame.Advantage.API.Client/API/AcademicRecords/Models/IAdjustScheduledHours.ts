﻿module API.Models {
    export interface IAdjustScheduledHours {
        campusId: string;
        adjustmentDate: string;
        programVersionId: string;
        studentGroupId: string;
        amountToAdjust: number;
        adjustmentMethod: number;
    }
}