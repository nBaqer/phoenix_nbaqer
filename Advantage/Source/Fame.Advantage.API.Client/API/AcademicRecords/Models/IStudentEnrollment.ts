﻿module API.Models {
    export interface IStudentEnrollment {
        studentId: string;
        enrollmentId: string;
        dateDetermined?: Date;
        lastDateAttended?: Date;
        status?: string;
        dropReasonId?: string;
    }
}