﻿module API.Models {
    export interface IFilterStudentSearch {
        campusId: string;
        filter: string;
    }
}