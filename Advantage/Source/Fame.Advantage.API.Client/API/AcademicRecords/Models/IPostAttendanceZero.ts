﻿module API.Models {
    export interface IPostAttendanceZero {
        campusId: string;
        dates: Array<string>;
        program: string;
        programVersion: string;
        studentName: string;
        studentGroupId: string;
        badgeNumber: string;
        startDate: string;
        endDate: string;
        inSchoolStatus: boolean;
        currentlyAttending: string;
        futureStart: string;
        leaveOfAbsense: string;
        outOfSchoolStatus: boolean;
        droppedOut: string;
        expelled: string;
        graduated: string;
        noShow: string;
    }
}