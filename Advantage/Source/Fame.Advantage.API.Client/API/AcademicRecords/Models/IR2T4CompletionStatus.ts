﻿module API.Models {
    export interface IR2T4CompletionStatus {
        isR2T4InputCompleted: boolean;
        isR2T4ResultsCompleted: boolean;
        isR2T4OverrideResultsCompleted: boolean;
    }
}