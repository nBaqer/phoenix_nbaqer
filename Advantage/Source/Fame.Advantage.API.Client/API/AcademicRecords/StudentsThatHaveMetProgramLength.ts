﻿module Api.AcademicRecords {
    import IDataProvider = API.Models.IDataProvider;
    export class StudentsThatHaveMetProgramLength implements IDataProvider {
        getDataSource(onRequestFilterData: () => any, functionName?: string): kendo.data.DataSource {
            let hasCustomFunctionName = functionName !== undefined;
            if (hasCustomFunctionName) {
                if (functionName === "getHaveMetOptions") {
                    return this.getHaveMetOptions(onRequestFilterData);
                }
            }
            return this.getStudentsThatHaveMetProgramLength(onRequestFilterData);
        }

        public getStudentsThatHaveMetProgramLength(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSourceFull("v1/AcademicRecords/StudentStatusChange/GetStudentsThatHaveMetProgramLength", onRequestFilterData, null, null, null, true, 6 * 10000 * 3);
        }

        public getGraduationStatuses(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/AcademicRecords/StudentStatusChange/GetGraduationStatuses", onRequestFilterData);
        }
        public getStatusesToFilter(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/AcademicRecords/StudentStatusChange/GetStatusesToFilter", onRequestFilterData);
        }

        public getHaveMetOptions(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/AcademicRecords/StudentStatusChange/GetHaveMetOptions", onRequestFilterData);
        }

        public changeStatus(requestData: any, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.setShouldShowIsLoading(true);
            return request.send(RequestType.Post, "v1/AcademicRecords/StudentStatusChange/ChangeStatus", requestData, RequestParameterType.Body, onResponseCallback);
        }

        //    public PlaceStudentOnTitleIVPrbation(data: any, onResponseCallback: (response: Response) => any): void {
        //        let request = new Request();
        //        request.send(RequestType.Get, "v1/AcademicRecords/TitleIVSAP/PlaceStudentOnTitleIVPrbation", data, RequestParameterType.QueryString, onResponseCallback);
        //    }

        //    public executeTitleIVSapCheck(data: any, onResponseCallback: (response: Response) => any, showLoader?: boolean) {
        //        let request = new Request();
        //        if (showLoader !== undefined) {
        //            if (showLoader === false) {
        //                request.setShouldShowIsLoading(false);
        //            }
        //        }
        //        return request.send(RequestType.Post, "v1/AcademicRecords/TitleIVSAP/ExecuteTitleIVSap", data, RequestParameterType.Body, onResponseCallback);
        //    }
    }
}