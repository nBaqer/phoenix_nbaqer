﻿module Api {

    import IDataProvider = API.Models.IDataProvider;
    /**
     * The API ProgramVersions is the encapsulation to the ProgramVersions Controller.
     * In there the request is initialized and is given the path and any other required parameter.
     * For example, if the request is of type Post and if you want to pass the parameters as query string or as part of the body.
     */
    export class ProgramVersions {
        //This API is used to fetch the campus program version details for the given enrollment Id.
        public getCampusProgramVersionDetail(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramVersions/GetCampusProgramVersionDetailsByEnrollmentId", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        getDataSource(onRequestFilterData: () => any, group?: any, functionName?: string): kendo.data.DataSource {

            let hasCustomFunctionName = functionName !== undefined;
            if (hasCustomFunctionName) {
                if (functionName === "getProgramVersionForApprovalDS") {
                    return this.getProgramVersionForApprovalDS(onRequestFilterData);
                }
                if (functionName === "getProgramVersionExams") {
                    return this.getProgramVersionExamsDS(onRequestFilterData);
                }

            }
            return null;
        }

        public getProgramVersionExamsDS(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/AcademicRecords/ProgramVersions/GetProgramVersionExams", onRequestFilterData);
        }
        

        public getProgramVersionForApprovalDS(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/AcademicRecords/ProgramVersions/GetProgramVersionsForNaccas", onRequestFilterData);
        }

        //This API is used to fetch the program version details for the given enrollment Id.
        public getProgramVersionDetailByEnrollmentId(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramVersions/GetProgramVersionDetailByEnrollmentId", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the program version schedule details for the given enrollment Id.
        public getScheduleDetailsByProgramVersionId(programVersionId: any, onResponseCallback: (response: Response) => any) {
            let programVersion = { programVersionId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramVersions/GetScheduleDetailsByProgramVersionId", programVersion, RequestParameterType.QueryString, onResponseCallback);
        }
        //This API is used to fetch the completed days in the period for the given enrollment Id.
        public getCreditHourCompletedDays(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramScheduleDetails/GetCreditHourCompletedDays", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the total days in the period for Non-Term not selfpaced program for the given enrollment Id.
        public getTotalDaysForNonTermNotSelfPacedProgram(withdrawalPeriodDetails: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramPeriodDetail/GetTotalDaysForNonTermNotSelfPacedProgram", withdrawalPeriodDetails, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the Hours Scheduled To Complete for Title IV  clock hour program for the given enrollment Id.
        public getHoursScheduledToComplete(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramVersions/GetHoursScheduledToComplete", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the scheduled and total hours for period of enrollments for the given enrollment Id.
        public getScheduledAndTotalHoursForPeriodOfEnrollment(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramVersions/GetScheduledAndTotalHoursForPeriodOfEnrollment", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the completed days and total days in a payment period for Non-Standard term programs with terms of substantially equal in length for the given enrollment Id.
        public getDetailsForNonStandardTermSubstantiallyEqualLength(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramPeriodDetail/GetDetailsForNonStandardTermSubstantiallyEqualLength", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the completed days and total days in a payment period for Non-Standard term programs with terms of not substantially equal in length for the given enrollment Id.
        public getDetailsForNonStandardTermNotSubstantiallyEqualLength(withdrawalPeriodDetails: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramPeriodDetail/GetDetailsForNonStandardTermNotSubstantiallyEqualLength", withdrawalPeriodDetails, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the completed days and total days Standard term programs for the given enrollment Id.
        public getCompletedAndTotalDaysForStandardTermPrograms(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramPeriodDetail/GetCompletedAndTotalDaysForStandardTermPrograms", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the payment period details for Non-standard term program with terms not substantially equal in length program for the given enrollment Id.
        public getPaymentPeriodForNotSubstantiallyEqualInLength(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramVersions/GetPaymentPeriodForNotSubstantiallyEqualInLength", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the End date of the payment period when a student withdraws from a non term not self paced program or when DL period is used for 
        //calculation for Nonstandard term programs with terms not substantially equal in length for the given enrollment Id.
        public getEndDateOfCreditHourPaymentPeriod(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramScheduleDetails/GetEndDateOfCreditHourPaymentPeriod", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the End date of the Period Of Enrollment when a student withdraws from a Title IV Non-term or Non-Standard term program for the given enrollment Id.
        public getEndDateOfCreditHourPeriodOfEnrollment(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramScheduleDetails/GetEndDateOfCreditHourPeriodOfEnrollment", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the payment period and credits earned for the given enrollment Id.
        public getCreditHoursWithdrawalPaymentPeriod(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramVersions/GetCreditHoursWithdrawalPaymentPeriod", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the withdrawal period of enrollment for credit hours for the given enrollment Id.
        public getNonStandardWithdrawalPeriodOfEnrollment(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramVersions/GetNonStandardWithdrawalPeriodOfEnrollment", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the completed and total days for Period Of Enrollment for the given enrollment Id.
        public getNonTermCompletedAndTotalDaysForPeriodOfEnrollment(withdrawalPeriodDetails: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramPeriodDetail/GetNonTermCompletedAndTotalDaysForPeriodOfEnrollment", withdrawalPeriodDetails, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the term details of the withdrawal period of the enrolled program for the given enrollment Id.
        public getWithdrawalPeriodTermDetails(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramPeriodDetail/GetWithdrawalPeriodTermDetails", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the completed and total days for non-standard program for the given enrollment Id.
        public getNonStandardProgramDaysDetail(withdrawalPeriodDetails: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramPeriodDetail/GetNonStandardProgramDaysDetail", withdrawalPeriodDetails, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the number of days required to complete failed courses for given enrollment id.
        public getDaysRequiredToCompleteFailedCourse(withdrawalPeriodDetails: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramPeriodDetail/GetDaysRequiredToCompleteFailedCourse", withdrawalPeriodDetails, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the withdrawal details of payment period for the given enrollment Id.
        public getWithdrawalPaymentPeriod(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramVersions/GetWithdrawalPaymentPeriod", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch the withdrawal details of period of enrollment for the given enrollment Id.
        public getWithdrawalPeriodOfEnrollment(enrollmentId: any, onResponseCallback: (response: Response) => any) {
            let enrollment = { enrollmentId } as any;
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramVersions/GetWithdrawalPeriodOfEnrollment", enrollment, RequestParameterType.QueryString, onResponseCallback);
        }

        //This API is used to fetch student awards details based on enrollment id.
        public getStudentAwardsByEnrollmentId(inputDetails: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Get, "v1/AcademicRecords/ProgramVersions/GetStudentAwardsByEnrollmentId", inputDetails, RequestParameterType.QueryString, onResponseCallback);
        }
    }
}