﻿/// <reference path="../Request.ts" />

module Api.AR {
    export class PostAttendance {
        public postAttendance(data: any, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Post, "v1/AcademicRecords/Attendance/PostStudentAttendance", data, RequestParameterType.Body, onResponseCallback);
        }
    }
}