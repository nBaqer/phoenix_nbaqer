﻿module Api {
    import afaSession = API.Models.IAfaSession;
    export class AfaRequest {
        sessionKey: string;
        afaUrl: string;
        payload: afaSession;

        constructor(payload: afaSession) {
            this.payload = payload;
            //this.getSessionKey(this.payload, afaUrl,(response: any) => {
            //    this.sessionKey = response.sessionKey;
            //});
            //this.afaUrl = afaUrl;
        }

        /**
         * Send a custom ajax request to the API.
         * You can specify if the request is a POST or Get, the route to call the paramters to pass based on it's type (query string or body) and an event to be fired when the
         * request is ready.
         * This method encapculates error handling and token expiration exception.
         * @param requestType
         * The type of HTTP request (GET, POST, PUT or DELETE)
         * @param route
         * The controller action to be called
         * @param requestData
         * The object to be pass on the request as a parameter based on it's type
         * @param typeOfParameter
         * This is a enum that identify how do you want to pass the requestData to the API. Wether you want to pass it as Query String or as Body.
         * @param onResponseCallback
         * An event that will be called when the response is ready.
         */
        public send(requestType: RequestType,
            route: string,
            sessionKey: string,
            requestData: any,
            typeOfParameter: RequestParameterType,
            onResponseCallback: (response: Response) => any): void {

            let that = this;
            let requestSettings: JQueryAjaxSettings;
            let typeOfRequest: string = "GET";
            let callback = onResponseCallback;
            typeOfRequest = that.getTypeOfRequest(requestType);

            if (typeOfParameter === RequestParameterType.QueryString) {
                let finalRoute: string;
                if (requestData != null) {
                    finalRoute = route + "?" + $.param(requestData); //the url to call
                } else {
                    finalRoute = route;
                }
                requestSettings = {
                    type: typeOfRequest, //GET, POST, PUT
                    url: finalRoute,
                    contentType: "application/json",
                    dataType: 'json',
                    global: false,
                    timeout: 40000,
                    beforeSend: xhr => { //Include the bearer token in header
                        xhr.setRequestHeader("SessionKey", sessionKey);
                    }
                };
            } else {
                requestSettings = {
                    type: typeOfRequest, //GET, POST, PUT
                    url: route, //the url to call
                    data: JSON.stringify(requestData), //Data sent to server
                    contentType: "application/json",
                    dataType: 'json',
                    global: false,
                    timeout: 40000,
                    beforeSend: xhr => { //Include the bearer token in header
                        xhr.setRequestHeader("SessionKey", sessionKey);
                    }
                };
            }
            $.ajax(requestSettings).done((ajaxResponse) => {
                callback(ajaxResponse);

            }).fail((error) => {
            });
        }

        public getSessionKey(payload: afaSession, onResponseCallback: (response: Response) => any): void {
            let callback = onResponseCallback;
            let requestSettings: JQueryAjaxSettings = {
                type: "POST",
                url: "http://advqa.fameinc.com/AFAservices/QA/AFAService.WebApi/users/userlogin",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(payload),
                beforeSend: xhr => { //Include the bearer token in header
                    xhr.setRequestHeader("ClientKey", payload.ClientKey);
                }
            };
            $.ajax(requestSettings).done((ajaxResponse) => {
                this.sessionKey = ajaxResponse.sessionKey;
                callback(ajaxResponse);
            }).fail(()=>{this.sessionKey = null});
        }

        private getTypeOfRequest(requestType: RequestType): string {
            let typeOfRequest: string = "GET";
            if (requestType === RequestType.Get) {
                typeOfRequest = "GET";
            } else if (requestType === RequestType.Post) {
                typeOfRequest = "POST";
            } else if (requestType === RequestType.Put) {
                typeOfRequest = "PUT";
            } else if (requestType === RequestType.Delete) {
                typeOfRequest = "DELETE";
            }
            return typeOfRequest;
        }
    }
}