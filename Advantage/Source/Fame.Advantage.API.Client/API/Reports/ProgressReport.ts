﻿module Api {
    export class ProgressReport {
        public generateReportByStudentEnrollmentId(data: any,
            onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.sendFile(RequestType.Post, "v1/Reports/ProgressReport/GetProgressReport", data, FileOutputType.Pdf, "ProgressReport","pdf", RequestParameterType.Body, onResponseCallback);
        }
    }
}