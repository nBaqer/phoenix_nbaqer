﻿module Api.Reports.Models {
    import IReports = Api.Reports.Models.IReports;
    import IDataProvider = API.Models.IDataProvider;

    export class Reports implements IDataProvider  {
        getDataSource(onRequestFilterData: () => any, group?): kendo.data.DataSource {
            return this.getAllStateBoardReports(onRequestFilterData);
        }

        public getAllStateBoardReports(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/StateBoardSettings/GetStateBoardReports", onRequestFilterData);
        }

        public deleteStatateBoardReportSetting(campusId: any, reportId: any, stateId: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            let model = {
            }

            model["CampusId"] = campusId;
            model["ReportId"] = reportId;
            model["StateId"] = stateId;

            request.send(RequestType.Delete,
                "v1/StateBoardSettings/DeleteStateBoardReportSetting",
                model,
                RequestParameterType.Body,
                onResponseCallback);
        }

        public saveOrUpdateStateBoardSetting(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post,
                "v1/StateBoardSettings/SaveOrUpdateStateBoardReportSetting",
                data,
                RequestParameterType.Body,
                onResponseCallback);
        }

        public fetchStateBoardReport(campusId: any, reportId: any, stateId: any,  onResponseCallback: (response: Response) => any) {
            let params = { } as any;
            let request = new Request();


            params["CampusId"] = campusId;
            params["ReportId"] = reportId;
            params["StateId"] = stateId;

            request.send(RequestType.Get,
                "v1/StateBoardSettings/GetStateBoardReportSetting",
                params,
                RequestParameterType.QueryString,
                onResponseCallback);
        }

    }
}

