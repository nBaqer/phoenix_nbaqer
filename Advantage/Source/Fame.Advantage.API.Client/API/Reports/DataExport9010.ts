﻿/// <reference path="../request.ts" />
module Api {
    export class Data9010 {

        public generateDataExtract(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.sendFile(RequestType.Post, "v1/Reports/Operations9010/GetDataExport9010", data, FileOutputType.Xlsx,
                "9010Report" + new Date().toISOString().replace('T', ' ').replace(/\..*$/, '').split(' ')[0].split('-').join(''), "xlsx", RequestParameterType.Body, onResponseCallback);
        }
    }
}  