﻿module Api {
    export class TitleIvNoticeReport {
        public generateReportEnrollmentIdList(data: any[],
            onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.sendFile(RequestType.Post, "v1/Reports/TitleIVNoticeReport/GetTitleIVNoticeReport", data, FileOutputType.Pdf, "TitleIvNoticeReport","pdf", RequestParameterType.Body, onResponseCallback);
        }
    }
}