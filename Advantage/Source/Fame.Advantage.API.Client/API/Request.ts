﻿/// <reference path="../../Fame.Advantage.Client.MasterPage/AdvantageMessageBoxs.ts" />
/// <reference path="../../AdvWeb/Kendo/typescript/jquery.d.ts" />
/// <reference path="../../Fame.Advantage.Client.AD/Common/Enumerations.ts" />
module Api {
    declare var XMASTER_GET_BASE_URL: string;
    declare var appInsights: any;
    export enum FileOutputType {
        Pdf = 1,
        Xlsx = 2,
    }
    export enum RequestParameterType {
        QueryString = 1,
        Body = 2
    };
    export enum RequestType {
        Get = 1,
        Post = 2,
        Put = 3,
        Delete = 4
    }

    export class ContentType {
        static contentTypePdf = "application/pdf";
        static contentTypeXlsx = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    }

    export class FileExtension {
        static pdfExtension = ".pdf";
    }

    /**
     * The request class encapsulates the JWT Authorization process with the request.
     * The token is retrived from the browser serssion along with the API domain url. Token experation exception handling is also encapsulated on this class.
     */
    export class Request {
        jsonWebToken: string;
        apiDomainName: string;
        shouldDoRequest: boolean;
        shouldShowIsLoading: boolean;
        refreshTick: any;

        constructor() {
            this.shouldDoRequest = true;
            this.shouldShowIsLoading = true;
            this.jsonWebToken = sessionStorage.getItem("AdvantageApiToken") || $("#hdnApiToken").val();
            this.apiDomainName = sessionStorage.getItem("AdvantageApiUrl") || $("#hdnApiUrl").val();
            this.refreshTick = sessionStorage.getItem("refreshTick") as any;

            //if token or domain name are missing, ping session and pull from server synchronously
            if (!this.jsonWebToken || !this.apiDomainName) {
                this.refreshAPIInfo();
            }

            if (this.apiDomainName === undefined ||
                this.jsonWebToken === undefined ||
                this.apiDomainName === null ||
                this.jsonWebToken === null ||
                this.apiDomainName === "" ||
                this.jsonWebToken === "") {
                this.shouldDoRequest = false;
                $.when(MasterPage
                    .SHOW_ERROR_WINDOW_PROMISE(
                    "An error has occured. Unable to access the advantage web api, this may be due to session expiration or invalid configuration. By Pressing Ok, you will be redirected to the login screen."))
                    .then(confirmed => {
                        if (confirmed) {
                            window.location.replace("../Logout.aspx");
                        }
                    });
            } else {
                if (this.apiDomainName.charAt(this.apiDomainName.length - 1) !== "/") {
                    this.apiDomainName = this.apiDomainName + "/";
                }
            }
        }

        public setShouldShowIsLoading(value: boolean) {
            this.shouldShowIsLoading = value;
        }

        /**
         * Send a custom ajax request to the API.
         * You can specify if the request is a POST or Get, the route to call the paramters to pass based on it's type (query string or body) and an event to be fired when the
         * request is ready.
         * This method encapculates error handling and token expiration exception.
         * @param requestType
         * The type of HTTP request (GET, POST, PUT or DELETE)
         * @param route
         * The controller action to be called
         * @param requestData
         * The object to be pass on the request as a parameter based on it's type
         * @param typeOfParameter
         * This is a enum that identify how do you want to pass the requestData to the API. Wether you want to pass it as Query String or as Body.
         * @param onResponseCallback
         * An event that will be called when the response is ready.
         */
        public send(requestType: RequestType,
            route: string,
            requestData: any,
            typeOfParameter: RequestParameterType,
            onResponseCallback: (response: Response, jqXHR?: any) => any): void {

            let that = this;

            if (that.shouldDoRequest) {
                let requestSettings: JQueryAjaxSettings;
                let typeOfRequest: string = "GET";
                let callback = onResponseCallback;
                route = that.apiDomainName + route;

                typeOfRequest = that.getTypeOfRequest(requestType);

                this.showIsPageLoading(this.shouldShowIsLoading);

                if (typeOfParameter === RequestParameterType.QueryString) {
                    requestSettings = {
                        type: typeOfRequest, //GET, POST, PUT
                        url: route + "?" + $.param(requestData), //the url to call
                        contentType: "application/json",
                        dataType: 'json',
                        global: false,
                        timeout: 1200000,
                        beforeSend: xhr => { //Include the bearer token in header
                            xhr.setRequestHeader("Authorization", 'Bearer ' + that.jsonWebToken);
                        }
                    };
                } else {
                    requestSettings = {
                        type: typeOfRequest, //GET, POST, PUT
                        url: route, //the url to call
                        data: JSON.stringify(requestData), //Data sent to server
                        contentType: "application/json",
                        dataType: 'json',
                        global: false,
                        timeout: 1200000,
                        beforeSend: xhr => { //Include the bearer token in header
                            xhr.setRequestHeader("Authorization", 'Bearer ' + that.jsonWebToken);
                        }
                    };
                }

                $.ajax(requestSettings).done((ajaxResponse, textStatus, jqXHR) => {
                    that.showIsPageLoading(false);                
                    callback(ajaxResponse, jqXHR);
                }).fail((error) => {
                    that.showIsPageLoading(false);
                    that.apiExceptionHandler(error, route);
                    kendo.ui.progress($('body'), false);
                });
            }
        }



        /**
         * Send a custom ajax request to the API.
         * You can specify if the request is a POST or Get, the route to call the paramters to pass based on it's type (query string or body) and an event to be fired when the
         * request is ready.
         * This method encapculates error handling and token expiration exception.
         * @param requestType
         * The type of HTTP request (GET, POST, PUT or DELETE)
         * @param route
         * The controller action to be called
         * @param requestData
         * The object to be pass on the request as a parameter based on it's type
         * @param typeOfParameter
         * This is a enum that identify how do you want to pass the requestData to the API. Wether you want to pass it as Query String or as Body.
         * @param onResponseCallback
         * An event that will be called when the response is ready.
         */
        public sendSync(requestType: RequestType,
            route: string,
            requestData: any,
            typeOfParameter: RequestParameterType,
            onResponseCallback: (response: Response) => any): void {

            let that = this;

            if (that.shouldDoRequest) {
                let requestSettings: JQueryAjaxSettings;
                let typeOfRequest: string = "GET";
                let callback = onResponseCallback;
                route = that.apiDomainName + route;

                typeOfRequest = that.getTypeOfRequest(requestType);

                this.showIsPageLoading(this.shouldShowIsLoading);

                if (typeOfParameter === RequestParameterType.QueryString) {
                    requestSettings = {
                        type: typeOfRequest, //GET, POST, PUT
                        url: route + "?" + $.param(requestData), //the url to call
                        contentType: "application/json",
                        dataType: 'json',
                        async: false,
                        global: false,
                        timeout: 1200000,
                        beforeSend: xhr => { //Include the bearer token in header
                            xhr.setRequestHeader("Authorization", 'Bearer ' + that.jsonWebToken);
                        }
                    };
                } else {
                    requestSettings = {
                        type: typeOfRequest, //GET, POST, PUT
                        url: route, //the url to call
                        data: JSON.stringify(requestData), //Data sent to server
                        contentType: "application/json",
                        dataType: 'json',
                        async: false,
                        global: false,
                        timeout: 1200000,
                        beforeSend: xhr => { //Include the bearer token in header
                            xhr.setRequestHeader("Authorization", 'Bearer ' + that.jsonWebToken);
                        }
                    };
                }

                $.ajax(requestSettings).done((ajaxResponse, textStatus, jqXHR) => {
                    that.showIsPageLoading(false);
                    callback(ajaxResponse);
                }).fail((error) => {
                    that.showIsPageLoading(false);
                    that.apiExceptionHandler(error, route);
                    kendo.ui.progress($('body'), false);
                });
            }
        }

        /**
        * Send a custom ajax request to the API.
        * You can specify if the request is a POST or Get, the route to call the paramters to pass based on it's type (query string or body) and an event to be fired when the
        * request is ready.
        * This method encapculates error handling and token expiration exception.
        * @param requestType
        * The type of HTTP request (GET, POST, PUT or DELETE)
        * @param route
        * The controller action to be called
        * @param requestData
        * The object to be pass on the request as a parameter based on it's type
        * @param output
        * This is a enum that identify what type output file do you want to pass the requestData to the API.
        * * @param fileName
        * This is a string identify what name of output file do you want at time of download.
        * @param typeOfParameter
        * This is a enum that identify how do you want to pass the requestData to the API. Wether you want to pass it as Query String or as Body.
        * @param onResponseCallback
        * An event that will be called when the response is ready.
        */
        public sendFile(requestType: RequestType,
            route: string,
            requestData: any,
            output: FileOutputType,
            fileName: string,
            fileExtension: string,
            typeOfParameter: RequestParameterType,
            onResponseCallback: (response: Response) => any): void {
            let that = this;
            route = that.apiDomainName + route;

            var request = new XMLHttpRequest();
            request.open(that.getTypeOfRequest(requestType), route, true);
            request.setRequestHeader("Content-Type", "application/json");
            request.setRequestHeader("Authorization", `Bearer ${that.jsonWebToken}`);
            request.responseType = "blob";

            request.onload = () => {
                if (request.status === 200) {
                    var blob = new Blob([request.response], { type: that.getOutputContentType(output) });
                    //for microsoft IE
                    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                        window.navigator.msSaveOrOpenBlob(blob, `${fileName}.${fileExtension}`);
                    } else {
                        //other browsers
                        var link = document.createElement("a");
                        link.href = window.URL.createObjectURL(blob);
                        link.download = `${fileName}.${fileExtension}`;
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                        onResponseCallback(request.response);
                    }

                    that.showIsPageLoading(false);
                } else {
                    that.showIsPageLoading(false);
                    MasterPage.SHOW_ERROR_WINDOW_PROMISE("Report generation failed due to internal server error, please try again.");
                }
            };
            request.send(JSON.stringify(requestData));
            this.showIsPageLoading(this.shouldShowIsLoading);
        }

        // this function will return content type based on the FileOutputType given as parameter.
        private getOutputContentType(outputType: FileOutputType): string {
            let contentOutputType: string = "";
            //add your new output type of files here 
            switch (outputType) {
                case FileOutputType.Pdf:
                    contentOutputType = ContentType.contentTypePdf;
                    break;

                case FileOutputType.Xlsx:
                    contentOutputType = ContentType.contentTypeXlsx;
                    break;

                default:
                    contentOutputType = "";
            }
            return contentOutputType;
        }

        /**
        * Prepare a Kendo Data Source that encapsulates the JWT Authorization process.
        * This Kendo data source is for a GET Operation that sends the paramters on the query string.
        * You can use this for loading dropdowns, auto completes, grid and other kendo controls that can be bound to a server side data set.
        * @param route
        * The controller action to be called
        * @param onRequestFilterData
        * The event to get the filter parameter that will be pass when the request is made.
        */
        public getRequestAsKendoDataSource(route: string,
            onRequestFilterData: () => any,
            requestType?: string,
            group?: any
        ): kendo.data.DataSource {
            return this.getRequestAsKendoDataSourceFull(route, onRequestFilterData, requestType, group, 20);
        }

        public getRequestAsKendoDataSourceWithPageSize(route: string,
            onRequestFilterData: () => any,
            pageSize: number
        ): kendo.data.DataSource {
            return this.getRequestAsKendoDataSourceFull(route, onRequestFilterData, null, null, pageSize);
        }

        /**
         * Prepare a Kendo Data Source that encapsulates the JWT Authorization process.
         * This Kendo data source is for a GET Operation that sends the paramters on the query string.
         * You can use this for loading dropdowns, auto completes, grid and other kendo controls that can be bound to a server side data set.
         * @param route
         * The controller action to be called
         * @param onRequestFilterData
         * The event to get the filter parameter that will be pass when the request is made.
         */
        public getRequestAsKendoDataSourceFull(route: string, onRequestFilterData: () => any, requestType?: string, group?: any, pageSize?: number, serverPaging?: boolean, timeOut?: number): kendo.data.DataSource {
            let that = this;
            let groupList = [];
            if (group) {
                groupList.push(group);
            }
            route = that.apiDomainName + route;
            if (that.shouldDoRequest) {

                let kendoDataSource = new kendo.data.DataSource({
                    group: groupList,
                    serverFiltering: true,
                    serverPaging: serverPaging === null ? false : serverPaging,
                    pageSize: pageSize,
                    transport: {
                        read: (options) => {
                            $.ajax({
                                type: requestType == null ? "GET" : requestType,
                                url: route,
                                contentType: "application/json",
                                dataType: "json",
                                timeout: timeOut == null ? 60000 : timeOut,
                                global: false,
                                data: requestType == null
                                    ? onRequestFilterData()
                                    : JSON.stringify(onRequestFilterData()),
                                success: (result, textStatus, jqXHR) => {
                                    // notify the data source that the request succeeded
                                    options.success(result); 
                                },
                                error: (result) => {
                                    // notify the data source that the request failed
                                    options.error(result);
                                    that.apiExceptionHandler(result, route);
                                },
                                beforeSend: xhr => { //Include the bearer token in header
                                    xhr.setRequestHeader("Authorization", "Bearer " + that.jsonWebToken);
                                }
                            });
                        }
                    }

                });
                return kendoDataSource;
            } else {
                return new kendo.data.DataSource({
                    serverFiltering: false,
                    serverPaging: false,
                    type: "GET"
                });
            }
        }

        private getTypeOfRequest(requestType: RequestType): string {
            let typeOfRequest: string = "GET";
            if (requestType === RequestType.Get) {
                typeOfRequest = "GET";
            } else if (requestType === RequestType.Post) {
                typeOfRequest = "POST";
            } else if (requestType === RequestType.Put) {
                typeOfRequest = "PUT";
            } else if (requestType === RequestType.Delete) {
                typeOfRequest = "DELETE";
            }
            return typeOfRequest;
        }

        private apiExceptionHandler(error, route = null) {
            var that = this;
            if (error !== undefined) {
                try {
                    if (error.status === 400 && !!error.responseText && (error.responseText !== "" || error.statusText !== "")) {
                        let errorJson = { message: "" };
                        if (error.responseText !== "") {

                            var parsed = JSON.parse(error.responseText);
                            if (parsed && parsed.length > 0) {
                                $.each(parsed,
                                    (i, val) => {
                                        if (val["message"]) {
                                            errorJson.message += val["message"] + "\n";
                                        }
                                    });

                            } else {
                                errorJson.message = error.responseText;
                            }
                        }


                        MasterPage.SHOW_WARNING_WINDOW(errorJson.message);
                    } else if (error.status === 401) {
                        MasterPage.SHOW_ERROR_WINDOW_PROMISE(
                            "Your session has been timed out due to inactivity. By Pressing Ok, you will be redirected to the login screen.")
                            .then(confirmed => {
                                if (confirmed) {
                                    window.location.replace("../Logout.aspx");
                                }
                            });
                    }
                } catch (e) {
                    MasterPage.SHOW_ERROR_WINDOW_PROMISE("An error has ocurred during your request.");
                }
            } else {
                MasterPage.SHOW_ERROR_WINDOW_PROMISE("An error has ocurred during your request.");
            }
            if (error !== undefined && error.statusText === 'timeout') {
                MasterPage.SHOW_ERROR_WINDOW_PROMISE("Your request has timed out please try after some time.");
                return;
            }
        }


        private showIsPageLoading(show: boolean) {
            var pageLoading = $(document).find(".page-loading");
            if (pageLoading.length === 0)
                $(document).find("body").append('<div class="page-loading"></div>');

            kendo.ui.progress($(".page-loading"), show);
        }

        //Pings dash.aspx to pull session api session info
        private refreshAPIInfo() {
            var that = this;
            that.showIsPageLoading(true);
            $.ajax({
                async: false,
                url: XMASTER_GET_BASE_URL + "dash.aspx/GetApiInfo",
                data: {},
                type: "POST",
                timeout: 15000,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                global: false
            }).done((response) => {
                try {
                    var response = JSON.parse(response.d);
                    sessionStorage.setItem("AdvantageApiToken", response.Token);
                    sessionStorage.setItem("AdvantageApiUrl", response.ApiUrl);
                    this.jsonWebToken = sessionStorage.getItem("AdvantageApiToken") || $("#hdnApiToken").val();
                    this.apiDomainName = sessionStorage.getItem("AdvantageApiUrl") || $("#hdnApiUrl").val();
                }
                catch (e) {
                    MasterPage.SHOW_ERROR_WINDOW(e);
                }
                finally {
                    that.showIsPageLoading(false);
                }
            })
                .fail((err) => {
                    MasterPage.SHOW_ERROR_WINDOW(err);
                    that.showIsPageLoading(false);
                });
        }
    }
}
