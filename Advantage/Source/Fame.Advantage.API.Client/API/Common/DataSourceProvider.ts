﻿module Api {

    import IDataProvider = API.Models.IDataProvider;
    /**
    Base data source provider class
     */
    export class DataSourceProvider implements IDataProvider {


        getDataSource(onRequestFilterData: () => any, group?: any, functionName?: string): kendo.data.DataSource {


            if (functionName) {
                if (this[functionName]) {
                    return this[functionName](onRequestFilterData);
                }
            }
            return null;


        }





    }
}