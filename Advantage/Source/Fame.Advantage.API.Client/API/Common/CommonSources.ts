﻿module Api {

    import IDataProvider = API.Models.IDataProvider;
    /**
     * The API ProgramVersions is the encapsulation to the ProgramVersions Controller.
     * In there the request is initialized and is given the path and any other required parameter.
     * For example, if the request is of type Post and if you want to pass the parameters as query string or as part of the body.
     */
    export class CommonSources {
        

        getDataSource(onRequestFilterData: () => any, group?: any, functionName?: string): kendo.data.DataSource {

            let hasCustomFunctionName = functionName !== undefined;
            if (hasCustomFunctionName) {
                if (functionName === "getYesNo") {
                    return this.getYesNo(onRequestFilterData);
                }

                if (functionName === "getNoCampusSelectedOption") {
                    return this.getNoCampusSelectedOption(onRequestFilterData);
                }

            }
            return null;
        }

        public getYesNo(onRequestFilterData: () => any): kendo.data.DataSource {
            var ds = new kendo.data.DataSource({
                data: [
                    { text: "No", value: false },
                    { text: "Yes", value: true }
                ]
            });

            return ds;
        }

        public getNoCampusSelectedOption(onRequestFilterData: () => any): kendo.data.DataSource {
            var ds = new kendo.data.DataSource({
                data: [
                    { text: "Select campus to see more options", value: '' },
                ]
            });

            return ds;
        }


    
    }
}