﻿module API.Enums {
    export enum AdjustmentMethods {
        Remove = 1,
        SetCustom,
        SetToActual,
      
    }

    export enum AdjustableSchedules {
        AllAttending = 1,
        StudentGroup,
        ProgramVersion,
    }
}