﻿module API.Models {
    import DayOfWeek = API.Enums.DayOfWeek
    export interface DailyHours {
        DayOfWeek: DayOfWeek;
        Hours: number;
    }

}