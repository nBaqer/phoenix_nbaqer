﻿module API.Models {
    export interface IDataProvider {
        getDataSource: (onRequestFilterData: () => any, group?: any, functionName?: string) => kendo.data.DataSource;

    }


}