﻿module Api.Tools {

    export class GradDateCalculator {
         
         //This API is used to fetch the program version total hours.
        calculateGraduationDate = function (data: any, onResponseCallback: (response: Response) => any) {
             let request = new Request();
             request.send(RequestType.Post, "v1/Common/GraduationCalculator/CalculateContractedGraduationDate", data, RequestParameterType.Body, onResponseCallback);
         }
        
    }
}