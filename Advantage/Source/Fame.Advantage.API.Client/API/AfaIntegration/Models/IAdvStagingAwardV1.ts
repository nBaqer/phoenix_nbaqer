﻿module API.Models {
    export interface IAdvStagingAwardV1 {
        idAdvStagingAward: number;
        locationCMSID: string;
        idStudent: string;
        sisEnrollmentID: string;
        fundCode: string;
        awardID: string;
        loanID: string;
        awardStatus: string;
        awardYear: string;
        acadYrStart: Date;
        acadYrEnd: Date;
        deleted: boolean;
        processed: boolean;

    }
}