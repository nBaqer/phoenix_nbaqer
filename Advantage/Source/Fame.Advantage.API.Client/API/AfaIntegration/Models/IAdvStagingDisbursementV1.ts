﻿module API.Models {
    export interface IAdvStagingDisbursementV1 {
        idAdvStagingDisbursement: number;
        locationCMSID: string;
        disbursementSequenceNumber: number;
        awardID: string;
        idStudent: string;
        anticipatedDisbursementDate: Date;
        anticipatedGrossAmount: number;
        anticipatedNetDisbursementAmt: number;
        feeAmount: number;
        actualDisbursementDate: Date;
        actualNetDisbursementAmt: number;
        actualGrossAmount: number;
        actualFeeAmount: number;
        disbursementStatus: string;
        paymentPeriodName: string;
        paymentPeriodStartDate: Date;
        PaymentPeriodEndDate: Date;
        originationFeePercentageUsed: number;
        deleted: boolean;
        processed: boolean;

    }
}