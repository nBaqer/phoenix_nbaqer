﻿module API.Models {
    export interface IAdvStagingAttendanceV1 {
        advStagingAttendanceID: number;
        locationCMSID: string;
        idStudent: string;
        sisEnrollmentID: string;
        SSN: string;
        paymentPeriodName: string;
        startDate: Date;
        endDate: Date;
        hoursCreditEarned: number;
        effectiveDate: Date;
        SAP: string;
        processed: boolean;

    }
}