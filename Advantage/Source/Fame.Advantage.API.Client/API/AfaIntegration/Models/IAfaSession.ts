﻿module API.Models {
    export interface IAfaSession {
        ClientKey: string;
        UserName: string;
        Password: string;
        SessionKey: string;
        IdStudent: any;
        }

}