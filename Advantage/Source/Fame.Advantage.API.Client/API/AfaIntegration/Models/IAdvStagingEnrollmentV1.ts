﻿module API.Models {
    export interface IAdvStagingEnrollmentV1 {
        advStagingEnrollmentID: number;
        locationCMSID: string;
        sisEnrollmentID: string;
        programName: string;
        programVersionID: string;
        idStudent: number;
        startDate: Date;
        gradDate: Date;
        processed: boolean;
        dateCreated: Date;
        userCreated: string;
        dateUpdated: Date;
        userUpdated: string;
        LeaveOfAbsenceDate?: Date;
        DroppedDate ?: Date;
        LastDateAttendance ?: Date;
        StatusEffectiveDate ?: Date;
        AdmissionsCriteria: string;
        EnrollmentStatus: string;
        SSN: string;
        ReturnFromLOADate ?: Date;
    }
}