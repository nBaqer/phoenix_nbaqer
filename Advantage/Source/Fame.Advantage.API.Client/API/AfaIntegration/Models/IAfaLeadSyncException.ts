﻿module API.Models {
    export interface IAfaLeadSyncException {
        Id: string;
        LeadId: string;
        AfaStudentId: number;
        FirstName: string;
        LastName: string;
        ExceptionReason: string;
    }
}