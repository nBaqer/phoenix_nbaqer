﻿module API.Models {
    export interface IAdvStagingDemographicsV1 {
        gender: string;
        maritialStatus : string;
        citizenshipStatus : string;
        email : string;
        licenseState : string;
        licenseNumber : string;
        zipCode : string;
        state : string;
        dob: Date;
        city : string;
        phone: string;
        lastName : string;
        middleInitial : string;
        firstName : string;
        ssn: string;
        idStudent: number;
        locationCMSID : string;
        advStagingDemographicID : number;
        address : string;
        processed : boolean;
    }
}