﻿module API.Models {
    export interface IAfaStudentExist {
        leadId: string;
        afaStudentId: number;
        afaStudentIdExists: boolean;
        resultStatus: string;
        resultStatusMessage: string;
    }
}