﻿module API.Models {
    export interface IAdvStagingProgramV1 {
        advStagingProgramID: number;
        locationCMSID: string;
        programVersionID: string;
        programName: string;
        dateCreated: Date;
        userCreated: string;
        dateUpdated: Date;
        userUpdated:string;
    }
}