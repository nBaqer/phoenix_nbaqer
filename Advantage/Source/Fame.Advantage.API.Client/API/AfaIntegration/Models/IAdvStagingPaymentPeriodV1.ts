﻿module API.Models {
    export interface IAdvStagingPaymentPeriodV1 {
        advStagingPaymentPeriodID: number;
        locationCMSID: string;
        idStudent: string;
        sisEnrollmentID: string;
        paymentPeriodName: string;
        startDate: Date;
        endDate: Date;
        hoursCreditEarned: number;
        effectiveDate: Date;
        SAP: string;
        hoursCreditsEnrolled: number;
        enrollmentStatusDescription: string;
        weeksOfInstructionalTime: number;
        acadYearSeqNo: number;
        deleted: boolean;
        processed: boolean;

    }
}