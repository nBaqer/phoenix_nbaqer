﻿module Api {
    import IafaSession = API.Models.IAfaSession;
    import IAdvStagingDemographicsV1 = API.Models.IAdvStagingDemographicsV1;
    import IDataProvider = API.Models.IDataProvider;
    import ILeadBase = API.Models.ILeadBase;
    import ILead = API.Models.ILead;
    import IAfaLeadSyncException = API.Models.IAfaLeadSyncException;
    import IAdvStagingProgramV1 = API.Models.IAdvStagingProgramV1;
    import IAdvStagingEnrollmentV1 = API.Models.IAdvStagingEnrollmentV1;
    import IAdvStagingPaymentPeriodV1 = API.Models.IAdvStagingPaymentPeriodV1;
    import IAdvStagingAttendanceV1 = API.Models.IAdvStagingAttendanceV1;
    import IAdvStagingAwardV1 = API.Models.IAdvStagingAwardV1;
    import IAdvStagingDisbursementV1 = API.Models.IAdvStagingDisbursementV1;


    //export class AfaIntegration implements IDataProvider {
    //    getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
    //        //return this.getUserSearchKendoDataSource(onRequestFilterData);
    //    }
    export class AfaIntegration implements IDataProvider {
        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSourceFull("v1/AFA/Afa/GetAfaLeadSyncException", onRequestFilterData, undefined, 20);
        }

        public getWapiSettings(codeOperation: string, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            let reqData = {};
            reqData["codeOperation"] = codeOperation;
            request.send(RequestType.Get, "v1/Maintenance/Wapi/Wapi/GetWapiSettings", reqData, RequestParameterType.QueryString, onResponseCallback);
        }
        public getAllDemographics(sessionKey: string, campusId : string, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            let reqData = {};
            reqData["sessionKey"] = sessionKey;
            reqData["campusId"] = campusId;
            request.send(RequestType.Get, "v1/AFA/Afa/GetAllLeadDemographics", reqData, RequestParameterType.QueryString, onResponseCallback);
        }

        public getAfaSessionKey(onResponseCallback: (response: Response) => any): void {
            let AfaWapiSettings = this.getWapiSettings("AFA_INTEGRATION", (response: any) => {
                if (response != null) {
                    let payload = {} as IafaSession;
                    payload.ClientKey = response.consumerKey; //"24D41E6A-A42E-48A1-9BAD-A84B04B5505E";
                    payload.Password = response.privateKey; // "pass";
                    payload.UserName = response.userName; // "famedev"
                    let request = new Request();
                    request.send(RequestType.Post, "v1/AFA/Afa/GetAfaSessionKey", payload, RequestParameterType.Body, onResponseCallback);
                }
            });
        }

        public checkIfAfaStudentExist(firstName: string, lastName: string, ssn: string, cmsId: string, afaStudentId: number, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            let reqData = {};
            reqData["firstName"] = firstName;
            reqData["lastName"] = lastName;
            reqData["ssn"] = ssn;
            reqData["cmsId"] = cmsId;
            reqData["afaStudentId"] = afaStudentId;
            request.send(RequestType.Get, "/v1/Leads/CheckIfAfaStudentIdExists", reqData, RequestParameterType.QueryString, onResponseCallback);
        }

        public updateAfaStudentId(lead: ILeadBase, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Put,
                "/v1/Leads/PutAfaStudentId",
                lead,
                RequestParameterType.Body,
                onResponseCallback);
        }

        public getCampusIdForCmsId(cmsId: string, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            let reqData = {};
            reqData["cmsId"] = cmsId;
            request.send(RequestType.Get, "/v1/SystemCatalog/Campus/GetCampusIdForCmsId", reqData, RequestParameterType.QueryString, onResponseCallback);
        }

        public getCmsIdForCampusId(campusId: string, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            let reqData = {};
            reqData["campusId"] = campusId;
            request.send(RequestType.Get, "/v1/SystemCatalog/Campus/GetCmsIdForCampusId", reqData, RequestParameterType.QueryString, onResponseCallback);
        }

        public getCatalog(onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Get, "/v1/SystemCatalog/Catalog/GetCatalog", " ", RequestParameterType.Body, onResponseCallback);
        }

        public createLead(lead: ILead, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Post, "/v1/Leads/PostLeadAfa", lead, RequestParameterType.Body, onResponseCallback);
        }

        public insertLeadSyncException(record: IAfaLeadSyncException, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Post, "/v1/AFA/Afa/InsertAfaLeadSyncException", record, RequestParameterType.Body, onResponseCallback);
        }
        public getAllProgramVersionsForCampus(campusId: string, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            let reqData = {};
            reqData["campusId"] = campusId;
            request.send(RequestType.Get, "/v1/AcademicRecords/ProgramVersions/GetAllProgramVersionByCampus", reqData, RequestParameterType.QueryString, onResponseCallback);
        }

        public postProgramVersionToAfa(prgVersions: Array<IAdvStagingProgramV1>,
            onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Post, "/v1/AFA/Afa/PostAllProgramVersionToAfa", prgVersions, RequestParameterType.Body, onResponseCallback);
        }

        public getAllEnrollmentsForAfaSync(campusId: string, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            let reqData = {};
            reqData["campusId"] = campusId;
            request.send(RequestType.Get, "v1/AcademicRecords/Enrollment/GetEnrollmentsForAfaSync", reqData, RequestParameterType.QueryString, onResponseCallback);
        }
        public getAllEnrollmentIdsForAfa(campusId: string, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            let reqData = {};
            reqData["campusId"] = campusId;
            request.send(RequestType.Get, "/v1/AFA/Afa/GetAfaEnrollmentsForCampus", reqData, RequestParameterType.QueryString, onResponseCallback);
        }
        public postEnrollmentsToAfa(enrollments: Array<IAdvStagingEnrollmentV1>,
            onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Post, "/v1/AFA/Afa/PostAllEnrollmentsToAfa", enrollments, RequestParameterType.Body, onResponseCallback);
        }

        public calculateAttendanceUpdatesForEnrollments(enrollmentIds: Array<string>,
            onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.sendSync(RequestType.Post, "/v1/AFA/Afa/CalculateAttendanceUpdatesForEnrollments", enrollmentIds, RequestParameterType.Body, onResponseCallback);
        }
        public calculateAttendanceUpdatesForEnrollment(enrollmentId: string,
            onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Post, "/v1/AFA/Afa/CalculateAttendanceUpdatesForEnrollment", enrollmentId, RequestParameterType.Body, onResponseCallback);
        }
        public calculateAllAttendanceUpdatesForCampus(campusId: string,
            onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Post, "/v1/AFA/Afa/CalculateAttendanceUpdatesForEnrollment", campusId, RequestParameterType.Body, onResponseCallback);
        }
        public postAttendanceUpdatesToAfa(attendanceUpdates: Array<IAdvStagingAttendanceV1>,
            onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.sendSync(RequestType.Post, "/v1/AFA/Afa/PostAttendanceUpdatesToAfa", attendanceUpdates, RequestParameterType.Body, onResponseCallback);
        }

        public postAllRefundsToAfa(campusId: string,
            onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            //let reqData = {};
            //reqData["campusId"] = campusId;
            request.send(RequestType.Post, "/v1/AFA/Afa/PostAllRefundsToAfa", campusId, RequestParameterType.Body, onResponseCallback);
        }

        public getAllPaymentPeriods(campusId: string, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            let reqData = {};
            reqData["campusId"] = campusId;
            request.send(RequestType.Get, "/v1/AFA/Afa/GetAllPaymentPeriods", reqData, RequestParameterType.QueryString, onResponseCallback);
        }

        public postPaymentPeriods(paymentPeriods: Array<IAdvStagingPaymentPeriodV1>, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Post, "/v1/AFA/Afa/PostPaymentPeriodsToAdvantage", paymentPeriods, RequestParameterType.Body, onResponseCallback);
        }

        public getAllAwards(campusId: string, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            let reqData = {};
            reqData["campusId"] = campusId;
            request.send(RequestType.Get, "/v1/AFA/Afa/GetAllAwards", reqData, RequestParameterType.QueryString, onResponseCallback);
        }
        public postAwards(awards: Array<IAdvStagingAwardV1>, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Post, "/v1/AFA/Afa/PostAwardsToAdvantage", awards, RequestParameterType.Body, onResponseCallback);
        }
        public syncAwards(awards: Array<IAdvStagingAwardV1>, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Post, "/v1/AFA/Afa/SyncAwardsToAdvantage", awards, RequestParameterType.Body, onResponseCallback);
        }
        public getAllDisbursements(campusId: string, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            let reqData = {};
            reqData["campusId"] = campusId;
            request.send(RequestType.Get, "/v1/AFA/Afa/GetAllDisbursements", reqData, RequestParameterType.QueryString, onResponseCallback);
        }
        public postDisbursements(disbursements: Array<IAdvStagingDisbursementV1>, onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Post, "/v1/AFA/Afa/PostDisbursementsToAdvantage", disbursements, RequestParameterType.Body, onResponseCallback);
        }
        public syncDisbursements(disbursements,onResponseCallback: (response: Response) => any): void {
            let request = new Request();
            request.send(RequestType.Post, "/v1/AFA/Afa/SyncDisbursementsToAdvantage", disbursements, RequestParameterType.Body, onResponseCallback);
        }

    }
}