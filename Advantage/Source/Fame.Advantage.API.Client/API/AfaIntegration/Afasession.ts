﻿module API.Models {
    export class AfaSession implements IAfaSession {
        clientKey: string;
        userName: string;
        password: string;
        sessionKey: string;
        idStudent: any;

        constructor(clientKey: string,userName: string, password: string,sessionKey: string, idStudent: any) {
            this.clientKey = clientKey;
            this.userName = userName;
            this.password = password;
            this.sessionKey = sessionKey;
            this.idStudent = idStudent;
    }
    }
}