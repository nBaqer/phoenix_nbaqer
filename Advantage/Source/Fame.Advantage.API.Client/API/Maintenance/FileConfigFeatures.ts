﻿module Api {
    import IDataProvider = API.Models.IDataProvider;

  
    export class FileConfigFeatures implements IDataProvider {

        getDataSource(onRequestFilterData: () => any): kendo.data.DataSource {
            return this.Get(onRequestFilterData);
        }
        public Get(onRequestFilterData: () => any): kendo.data.DataSource {
            let request = new Request();
            return request.getRequestAsKendoDataSource("v1/Maintenance/FileConfigFeatures/Get", onRequestFilterData);
        }
    }
}