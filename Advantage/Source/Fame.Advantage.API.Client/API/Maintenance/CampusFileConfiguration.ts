﻿module Api {
    import IDataProvider = API.Models.IDataProvider;
    import ICampusFileConfig = API.Models.ICampusFileConfig;

    export class CampusFileConfiguration {

        public getCampusFileConfigByCampusGroupId(campusGroupId: string, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            let data = { campusGroupId: campusGroupId };
            request.send(RequestType.Get, "v1/Maintenance/FileConfigCampus/GetByCampusGroup", data, RequestParameterType.QueryString, onResponseCallback);
        }

        public updateCampusFileConfig(data: ICampusFileConfig, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post, "v1/Maintenance/FileConfigCampus/Save", data, RequestParameterType.Body, onResponseCallback);
        }

        public deleteCampusFileConfig(campusFileConfigId: string, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            let data = { campusFileConfigurationId: campusFileConfigId }
            request.send(RequestType.Post, "v1/Maintenance/FileConfigCampus/Delete", campusFileConfigId, RequestParameterType.Body, onResponseCallback);

        }
    }
}