﻿module API.Models {
    import FileStorageType = API.Enums.FileStorageConfigs;
    import CustomFeatureConfig = API.Models.ICustomFileFeatureConfig
    export interface ICampusFileConfig {
        id: string;
        campusGroupId: string;
        fileStorageType: FileStorageType;
        appliesToAllFeatures: boolean;
        userName: string;
        password: string;
        path: string;
        cloudKey: string;
        modUser: string;
        modDate: Date;
        customFeatureConfigurations: Array<CustomFeatureConfig>;
    }
}