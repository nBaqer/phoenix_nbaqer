﻿module API.Models {
    import FileStorageType = API.Enums.FileStorageConfigs;

    export interface ICustomFileFeatureConfig {
        id: string;
        campusFileConfigurationId: string;
        fileStorageType: FileStorageType;
        featureId: number;
        userName: string;
        password: string;
        path: string;
        cloudKey: string;
        modDate: Date;
        modUser: string;

    }
}