﻿/// <reference path="ResizeSensor.d.ts" />

module API.Common {


    export class Splitter {
        myFlexGrid: FlexGrid;
        splitter: kendo.ui.Splitter;
        mainPane: JQuery;
        constructor() {
            let that = this;
            that.myFlexGrid = new FlexGrid()
            that.mainPane =  $(".main-container");

            $("#horizontal").kendoSplitter({
                panes: [
                    { collapsible: true, resizable: false, min: "20%", size: "20%" },
                    { collapsible: false, resizable: false, min: "80%", size: "80%" }
                ]
            });
            that.splitter = $("#horizontal").data("kendoSplitter");
            that.splitter.bind("collapse", () => { that.myFlexGrid.recalculateGridHeight() });
            that.splitter.bind("resize", () => { that.myFlexGrid.recalculateGridHeight() });

            new ResizeSensor(that.mainPane, function () {
                that.splitter.resize();
            });


        }





    }
}

