﻿
module API.Common{


    export class FlexGrid {
        recalculateGridHeight() {
            let fg_cont = $(".flexGrid-container");
            let fg_filter = $(".flexGrid-filter");
            let fg_grouping_header = $(".flexGrid .k-grouping-header");
            let fg_grid_header = $(".flexGrid .k-grid-header");
            let fg_grid_content = $(".flexGrid .k-grid-content");
            let fg_grid_footer = $(".flexGrid .k-grid-pager");
            let fg_grid = $(".flexGrid .k-grid");

            fg_grid.css("height", "");
            if (!fg_cont || !fg_grid_content) {
                return;
            }
            let adjustment_height = 0;
            let grid_content_height = fg_cont.innerHeight();

            if (fg_filter) {
                adjustment_height += fg_filter.innerHeight();
            }
            if (fg_grouping_header) {
                adjustment_height += fg_grouping_header.innerHeight();
            }
            if (fg_grid_header) {
                adjustment_height += fg_grid_header.innerHeight();
            }
            if (fg_grid_footer) {
                adjustment_height += fg_grid_footer.innerHeight();
            }

            grid_content_height -= adjustment_height + 26;
        
            fg_grid_content.innerHeight(grid_content_height);

        }

    }
}