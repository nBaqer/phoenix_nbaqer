/// <reference path="../../AdvWeb/Kendo/typescript/jquery.d.ts" />
declare class ResizeSensor {
    constructor(element: (Element | Element[] | JQuery), callback: Function);
    detach(callback: Function): void;
}

//export = ResizeSensor;
