﻿/// <reference path="../components/systemcatalog/userrolestatuscheck.ts" />
/// <reference path="../components/systemcatalog/studentacademiccalendar.ts" />
/// <reference path="../components/common/constants.ts" />

module API.Common {
    import constants = Components.Common;
    import StudentAcademicCalendar = Api.Components.SystemCatalog.StudentAcademicCalendar;
    import UserRoleStatusCheck = Api.Components.SystemCatalog.UserRoleStatusCheck;

    /**
        * Utility class will provide the common functionalities across the typescript project.        
       */
    export class Utility {
        public static isResultModified: boolean = false;
        public static equalsString = "=";
        public static colonSeparator = ":";
        public static isApproveTabExists: boolean = false;

        //this method convert given number into currency format and return a string with $ symbol.
        public static getCurrencyFormattedValue(value: string): string {
            let numberVal = parseFloat(value !== null && value !== "" ? value : "0.00");
            return `$${Utility.formatNumberAsCurrency(numberVal)}`;
        }

        //this method convert given number into percentage format and return a string with % symbol.
        public static getPercentageFormattedValue(value: string): string {
            let numberVal = parseFloat(value !== null && value !== "" ? value : "0.00");
            return `${Utility.formatNumberAsPercenatgeResults(numberVal)}%`;
        }

        //this method convert given number into currency format and return a string.
        public static formatNumberAsCurrency(value: number): string {
            return value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }

        //this method convert given number into percentage format and return a string.
        public static formatNumberAsPercenatgeResults(value: number): string {
            return value.toFixed(1).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }

        //this method convert given number into currency format and return a string.
        public static formatNumberAsCurrencyPercenatgeTab(value: number): string {
            return value.toFixed(3).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }

        // this method changes the UI element style for the provided css class
        public static changeElementStyle(elementId: string, className: string, isRemove: boolean): void {
            var divTerminationDetail = document.getElementById(elementId) as HTMLElement;
            if (isRemove === false) {
                divTerminationDetail.classList.remove(className);
            } else {
                divTerminationDetail.classList.add(className);
            }
        }

        //checking the selected calculation period type is Payment period or not and
        //setting the selected calculation period type in R2T4Results tab.
        public static isPaymentPeriod(periodTypeId: string, callBackMethod: (response: Response) => any): any {
            let academicCalendarType = new StudentAcademicCalendar();
            if (periodTypeId !== undefined) {
                academicCalendarType.isPaymentPeriod(periodTypeId,
                    (isPaymentPeriod) => {
                        if (isPaymentPeriod) {
                            $("#rdbPayPeriod").prop("checked", "true");
                            $("#rdbPeriodEnroll").attr("disabled", "true");
                        } else {
                            $("#rdbPeriodEnroll").prop("checked", "true");
                            $("#rdbPayPeriod").attr("disabled", "true");
                        }
                    });
            }
        }

        // this method checks the user is support user or not.
        public static isSupportUser(callBackMethod: (response: Response) => any): any {
            //let isSupportUser: boolean = false;
            let userRoleStatus = new UserRoleStatusCheck();
            let isSupportUser = userRoleStatus.isSupportUser(callBackMethod);
            return isSupportUser;
        }

        // this method checks the programtype for the given enrollment id. 
        //returns true if Clockhour and returns false if credit hour.
        public static isClockHour(enrollmentId: string, callBackMethod: (response: Response) => any): any {
            let isClockHour: boolean = false;
            let academicCalendarType = new StudentAcademicCalendar();
            if (enrollmentId !== undefined) {
                isClockHour = academicCalendarType.isClockHour(enrollmentId, callBackMethod);
            }
            return isClockHour;
        }

        // Method to convert date into MM/dd/yyyy format
        public static formatDate(inputDate: any): string {
            if (!!inputDate) {
                let date = new Date(inputDate);
                // Months use 0 index.
                let dateString = date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
                dateString = MasterPage.formatDate(dateString);
                return dateString;
            }
            return null;
        }

        // Method to give the field Ids of duplicated results for setting and getting data
        public static getDuplicateFields() {
            let duplicateFieldList: string[] = [
                "txtEA", "txtEB", "txtFA", "txtFC", "txtStep1FA", "txtStep1FB", "txtStep1FC", "txtStep1FD",
                "txtBoxH", "txtBoxG", "txt4JI", "txt4JE", "txt4KE", "txt4KI", "txt5BoxH", "txt5BoxL", "txt5BoxM",
                "txt7K", "txt7O", "txt8boxB", "txt8P",
                "txt9BoxQ", "txt9BoxR", "txt9BoxF", "txt9Boxs", "txt9BoxT"
            ];
            return duplicateFieldList;
        }

        // Method to give the field Ids of Post withdrawal controls for setting and getting data
        public static getPostWithdrawalFields() {
            let pwdFieldList: string[] = [
                "txtPWD", "txtPWDBox2", "txtPWDOffered", "txtPWDBox2Offered", "txtPWDBox3", "pwdPell3", "pwdPell6",
                "pwdFSEOG3",
                "pwdFSEOG6", "pwdTeach3", "pwdTeach6", "pwdIASG3", "pwdIASG6", "pwdPerkins1", "pwdPerkins2",
                "pwdPerkins3", "pwdPerkins4", "pwdPerkins5", "pwdPerkins6", "pwdSub1", "pwdSub2",
                "pwdSub3", "pwdSub4", "pwdSub5", "pwdSub6", "pwdUnSub1", "pwdUnSub2", "pwdUnSub3", "pwdUnSub4",
                "pwdUnSub5", "pwdUnSub6", "pwdGrad1", "pwdGrad2", "pwdGrad3", "pwdGrad4",
                "pwdGrad5", "pwdGrad6", "pwdParent1", "pwdParent2", "pwdParent3", "pwdParent4", "pwdParent5",
                "pwdParent6",
                "pwdTotal1", "pwdTotal2", "pwdTotal3", "pwdTotal4", "pwdTotal5", "pwdTotal6", "dtPostWithdrwal",
                "dtDeadline", "chkResponseReceived", "dtResponseReceived", "chkResponseNotReceived", "chkNotAccept",
                "dtGrantTransferred", "dtLoanTransferred"
            ];
            return pwdFieldList;
        }

        public static checkMessagesForOverride(isSaveBtn : boolean,  callBack: (response: boolean) => any): any {
            Utility.isSupportUser(
                (response) => {
                    if (response) {
                        let message = constants.r2T4OverrideNext;
                        if (isSaveBtn) {
                            message = constants.r2T4OverrideSave;
                        }
                        if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted && this.isResultModified) {
                            $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(message))
                                .then(confirmOverride => {
                                    if (confirmOverride) {
                                        if (message === constants.r2T4OverrideSave) {
                                            constants.isR2T4ResultsCompleted = false;
                                        }
                                        if (message === constants.r2T4OverrideNext) {
                                            this.isApproveTabExists = true;
                                        } else {
                                            this.isApproveTabExists = false;
                                        }
                                        this.isResultModified = false;
                                        return callBack(true);
                                    } else {
                                        this.isApproveTabExists = true;
                                        this.isResultModified = false;
                                        return callBack(false);
                                    }
                                });
                        } else {
                            this.isApproveTabExists = false;
                            return callBack(true);
                        }
                    }
                });
        }

        //Method To get the current active tab name.  
        public static getTabName() {
            if ($("#liTerminationDetails").hasClass("is-active")) {
                constants.activeTabName = "TerminationDetails";
            }
            else if ($("#liR2T4Input").hasClass("is-active")) {
                constants.activeTabName = "R2T4Input";
            }
            else if ($("#liR2T4Result").hasClass("is-active")) {
                constants.activeTabName = "R2T4Result";
            }
            else if ($("#liApproveTermination").hasClass("is-active")) {
                constants.activeTabName = "ApproveTermination";
            }
        }
    }
}