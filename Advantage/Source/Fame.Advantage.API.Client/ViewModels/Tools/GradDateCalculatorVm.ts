﻿/// <reference path="../../API/SystemCatalog/Holiday.ts" />
/// <reference path="../../API/SystemCatalog/Campus.ts" />
/// <reference path="../../API/FinancialAid/ProgramVersion.ts" />
/// <reference path="../../Components/Common/DropDownList.ts" />
/// <reference path="../../API/Tools/GradDateCalculator.ts" />
module Api.ViewModels.Tools {
    import DropDownList = Api.Components.DropDownList;
    import ISettings = API.Models.ISettings;
    import IFilterSearch = API.Models.IFilterSearch;
    import IFilterParams = API.Models.IFilterParameters;
    import ProgramVersion = Api.FinancialAid.ProgramVersion;
    import Holiday = Api.Holiday;
    import Campus = Api.Campus;
    import IHoliday = API.Models.IHoliday
    import GradDateCalculator = Api.Tools.GradDateCalculator;
    import DailyHours = API.Models.DailyHours;
    import DayOfWeek = API.Enums.DayOfWeek;

    export class GradDateCalculatorVm {
        calculationData: any;
        campus: Campus;
        programVersion: ProgramVersion;
        holiday: Holiday;
        gradDateCalculator: GradDateCalculator;
        startDatePicker: kendo.ui.DatePicker;
        listBox: kendo.ui.ListBox;
        campusDropDownList: DropDownList;
        programVersionDropDownList: DropDownList;
        scheduleDropDownList: DropDownList;
        inpTotalProgramHours: JQuery;
        inpSundayHours: JQuery;
        inpMondayHours: JQuery;
        inpTuesdayHours: JQuery;
        inpWednesdayHours: JQuery;
        inpThursdayHours: JQuery;
        inpFridayHours: JQuery;
        inpSaturdayHours: JQuery;
        holidays: Array<IHoliday>;
        campusId: string;
        programVersionId: string;
        scheduleId: string;
        emptyGuid: string = "00000000-0000-0000-0000-000000000000";
        constructor() {
            this.campus = new Campus();
            this.programVersion = new ProgramVersion();
            this.holiday = new Holiday();
            this.gradDateCalculator = new GradDateCalculator();
            this.inpTotalProgramHours = $("#txtHoursInProgram");
            this.inpSundayHours = $("#txtSundayHours");
            this.inpMondayHours = $("#txtMondayHours");
            this.inpTuesdayHours = $("#txtTuesdayHours");
            this.inpWednesdayHours = $("#txtWednesdayHours");
            this.inpThursdayHours = $("#txtThursdayHours");
            this.inpFridayHours = $("#txtFridayHours");
            this.inpSaturdayHours = $("#txtSaturdayHours");
            this.holidays = [];
            this.calculationData = {};

        }

        public initialize() {
            var that = this;

            // create DatePicker from input HTML element
            $("#dpStudentStartDate").kendoDatePicker({

                open: function() {
                    var calendar = this.dateView.calendar;

                    calendar.wrapper.height(170);
                }

            });



            $("#holidaysListBox").kendoListBox({
                selectable: "multiple",
                dataSource: that.holidays,
                template: kendo.template($("#javascriptTemplate").html()),
                toolbar: {
                    tools: []
                }
            });

            that.listBox = $("#holidaysListBox").data("kendoListBox");
            that.startDatePicker = $("#dpStudentStartDate").data("kendoDatePicker");

            //initialize dropdowns
            that.scheduleDropDownList = new DropDownList("ddlLoadFromSchedule", that.programVersion, { params: { programVersionId: 1 } } as IFilterParams, { settings: { dataValueField: "scheduleId", dataTextField: "description", autoBind: false, enable: false } } as ISettings, () => {

                let scheduleDetails = that.scheduleDropDownList.getKendoDropDown().dataItem(that.scheduleDropDownList.getKendoDropDown().select()).programScheduleDetails;


                $.each(scheduleDetails, (i, val) => {
                    let dayOfWeek = val.dayOfWeek;
                    let total = val.total == 0 ? 0.00 : val.total;
                    switch (dayOfWeek) {
                        case DayOfWeek.Sunday:
                            that.inpSundayHours.val(total);
                            break;
                        case DayOfWeek.Monday:
                            that.inpMondayHours.val(total);
                            break;
                        case DayOfWeek.Tuesday:
                            that.inpTuesdayHours.val(total);
                            break;
                        case DayOfWeek.Wednesday:
                            that.inpWednesdayHours.val(total);
                            break;
                        case DayOfWeek.Thursday:
                            that.inpThursdayHours.val(total);
                            break;
                        case DayOfWeek.Friday:
                            that.inpFridayHours.val(total);
                            break;
                        case DayOfWeek.Saturday:
                            that.inpSaturdayHours.val(total);
                            break;

                    }


                })



            }, "getScheduleDetailsByProgramVersionId");
            that.campusDropDownList = new DropDownList("ddlCampus", that.campus, undefined, {
                settings: {
                    dataValueField: "value"
                }
            } as ISettings, () => {
                that.campusId = that.campusDropDownList.getValue();

                if (that.campusDropDownList.hasValue()) {
                    that.programVersionDropDownList.getKendoDropDown().enable(true);
                    that.holiday.GetHolidayListByCampus(that.campusId, (data: any) => {
                        let holidays = [] as Array<IHoliday>;
                        if (data.length > 0) {
                            that.holidays = <Array<IHoliday>>data;

                            that.listBox.setDataSource(new kendo.data.DataSource({
                                data: that.holidays
                            }));
                        }

                    })
                    that.programVersionDropDownList.reload(
                        { params: { campusId: that.campusDropDownList.getValue() } } as IFilterParams);
                } else {
                    that.programVersionDropDownList.getKendoDropDown().enable(false);
                    that.programVersionDropDownList.getKendoDropDown().select(0);
                    that.programVersionDropDownList.getKendoDropDown().element.trigger("change");
                    that.scheduleDropDownList.getKendoDropDown().enable(false);
                    that.scheduleDropDownList.getKendoDropDown().select(0);

                }

            });

            that.campusDropDownList.getKendoDropDown().bind("dataBound", () => {
                if (that.campusDropDownList.getKendoDropDown().list.length > 0) {
                    that.campusDropDownList.getKendoDropDown().select(1);
                    that.campusDropDownList.getKendoDropDown().element.trigger("change");
                    that.campusId = that.campusDropDownList.getValue();

                }
            })

            that.programVersionDropDownList = new DropDownList("ddlProgramVersion", that.programVersion, undefined, { settings: { dataValueField: "value", autoBind: false, enable: false } } as ISettings,
                () => {
                    let zero = "0.00";
                    that.inpTotalProgramHours.val(zero);
                    that.inpSundayHours.val(zero);
                    that.inpMondayHours.val(zero);
                    that.inpTuesdayHours.val(zero);
                    that.inpWednesdayHours.val(zero);
                    that.inpThursdayHours.val(zero);
                    that.inpFridayHours.val(zero);
                    that.inpSaturdayHours.val(zero);
                    $("#gradDate").html("N/A");
                    kendo.fx($("#gradDate")).fade("in").play()
                    that.programVersionId = that.programVersionDropDownList.getValue();

                    if (that.programVersionDropDownList.hasValue()) {
                        that.scheduleDropDownList.getKendoDropDown().enable(true);
                        that.programVersion.getProgramVersionTotalHours(that.programVersionId, (data: Response) => {
                            let hours = data.toString() + ".00";
                            that.inpTotalProgramHours.val(hours);

                        })
                        that.scheduleDropDownList.reload(
                            { params: { programVersionId: that.programVersionDropDownList.getValue() } } as IFilterParams);
                    } else {
                        that.scheduleDropDownList.getKendoDropDown().enable(false);

                    }

                });


            $("#calculate-btn").on("click", () => {
                let startDate = that.startDatePicker.value() != null ? that.startDatePicker.value().toISOString().split('T')[0] : ""
                that.calculationData = {
                    campusId: that.campusId,
                    studentStartDate: startDate,
                    totalProgramHours: Number(that.inpTotalProgramHours.val()),
                    dailyHours: [{ DayOfWeek: DayOfWeek.Monday, Hours: Number(that.inpMondayHours.val()) }, { DayOfWeek: DayOfWeek.Tuesday, Hours: Number(that.inpTuesdayHours.val()) },
                    { DayOfWeek: DayOfWeek.Wednesday, Hours: Number(that.inpWednesdayHours.val()) }, { DayOfWeek: DayOfWeek.Thursday, Hours: Number(that.inpThursdayHours.val()) },
                    { DayOfWeek: DayOfWeek.Friday, Hours: Number(that.inpFridayHours.val()) }, { DayOfWeek: DayOfWeek.Saturday, Hours: Number(that.inpSaturdayHours.val()) },
                    { DayOfWeek: DayOfWeek.Sunday, Hours: Number(that.inpSundayHours.val()) }] as Array<DailyHours>,
                    mondayHours: Number(that.inpMondayHours.val()), tuesdayHours: Number(that.inpTuesdayHours.val()), wednesdayHours: Number(that.inpWednesdayHours.val()),
                    thursdayHours: Number(that.inpThursdayHours.val()), fridayHours: Number(that.inpFridayHours.val()), saturdayHours: Number(that.inpSaturdayHours.val()), sundayHours: Number(that.inpSundayHours.val())
                }
                that.gradDateCalculator.calculateGraduationDate(that.calculationData, (data: any) => {
                    let result = <string>data.asString == '1/1/0001' ? "N/A" : <string>data.asString;
                    $("#gradDate").html(result);
                    kendo.fx($("#gradDate")).fade("in").play()
                });

            });

        }



    }
}