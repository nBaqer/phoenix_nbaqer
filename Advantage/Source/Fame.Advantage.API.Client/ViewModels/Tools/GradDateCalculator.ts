﻿module Api.ViewModels.Tools {

    export class GradDateCalculator {

        gradDateCalculatorVm: GradDateCalculatorVm;
        constructor() {
            this.gradDateCalculatorVm = new GradDateCalculatorVm();
        }

        public initialize() {
            this.gradDateCalculatorVm.initialize();
        }
    }
}